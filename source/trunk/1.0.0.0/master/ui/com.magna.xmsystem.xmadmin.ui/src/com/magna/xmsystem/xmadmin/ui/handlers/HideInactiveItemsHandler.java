
package com.magna.xmsystem.xmadmin.ui.handlers;

import javax.inject.Inject;

import org.eclipse.core.commands.ParameterizedCommand;
import org.eclipse.e4.core.commands.ECommandService;
import org.eclipse.e4.core.commands.EHandlerService;
import org.eclipse.e4.core.di.annotations.CanExecute;
import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.core.services.events.IEventBroker;
import org.eclipse.e4.ui.model.application.MApplication;
import org.eclipse.e4.ui.model.application.ui.basic.MPart;
import org.eclipse.e4.ui.model.application.ui.menu.MMenuItem;
import org.eclipse.e4.ui.workbench.modeling.EModelService;

import com.magna.xmsystem.xmadmin.ui.parts.IEditablePart;
import com.magna.xmsystem.xmadmin.util.CommonConstants;

/**
 * Hide inactive items handler
 * 
 * @author shashwat.anand
 *
 */
@SuppressWarnings("restriction")
public class HideInactiveItemsHandler {
	@Inject
	ECommandService commandService;

	@Inject
	EHandlerService handlerService;

	/** Member variable 'button status' for {@link Boolean}. */
	private boolean isHideMenuItemSelected;

	/**
	 * Method for Execute.
	 *
	 * @param menuItem
	 *            {@link MMenuItem}
	 * @param eventBroker
	 *            {@link IEventBroker}
	 */
	@Execute
	public void execute(final MMenuItem menuItem, final IEventBroker eventBroker, EModelService modelService,
			MApplication application) {
		if (menuItem != null) {
			isHideMenuItemSelected = menuItem.isSelected() ? true : false;
		}
		eventBroker.send(CommonConstants.EVENT_BROKER.HIDEINACTIVEITEMS, isHideMenuItemSelected);

		ParameterizedCommand cmd = commandService.createCommand("com.magna.xmsystem.xmadmin.ui.command.treereload",
				null);
		if (handlerService.canExecute(cmd)) {
			handlerService.executeHandler(cmd);
		}
	}
	
	@CanExecute
	public boolean canExecute(MApplication application, EModelService eModelService) {

		MPart rightHandMPartOfOldPerspective = (MPart) eModelService.find(CommonConstants.PART_ID.INFORMATION_PART_ID,
				application);
		if (rightHandMPartOfOldPerspective.getObject() instanceof IEditablePart
				&& rightHandMPartOfOldPerspective.isDirty()) {
			return false;
		}
		return true;
	}
}