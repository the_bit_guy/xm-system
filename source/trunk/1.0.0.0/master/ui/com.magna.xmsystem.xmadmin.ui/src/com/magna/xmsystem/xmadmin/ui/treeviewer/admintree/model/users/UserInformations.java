package com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.users;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.UUID;

import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild;

/**
 * The Class UserInformations.
 * 
 * @author subash.janarthanan
 * 
 */
public class UserInformations implements IAdminTreeChild {
	
	/** The parent. */
	private IAdminTreeChild parent;

	/** The user information child. */
	final private Map<String, IAdminTreeChild> userInformationChild;

	/**
	 * Instantiates a new user informations.
	 */
	public UserInformations(final IAdminTreeChild parnet) {
		this.parent = parnet;
		this.userInformationChild = new LinkedHashMap<>();
		this.add(UUID.randomUUID().toString(), new UserInformationStatus(this));
		this.add(UUID.randomUUID().toString(), new UserInformationHistory(this));
	}

	/**
	 * Add.
	 *
	 * @param userInformationChildId
	 *            the user information child id
	 * @param child
	 *            the child
	 * @return the i admin tree child
	 */
	public IAdminTreeChild add(final String userInformationChildId, final IAdminTreeChild child) {
		return this.userInformationChild.put(userInformationChildId, child);
	}

	/**
	 * Remove.
	 *
	 * @param userInformationChildId
	 *            the user information child id
	 * @return the i admin tree child
	 */
	public IAdminTreeChild remove(final String userInformationChildId) {
		return this.userInformationChild.remove(userInformationChildId);
	}

	/**
	 * Gets the user information child collection.
	 *
	 * @return the user information child collection
	 */
	public Collection<IAdminTreeChild> getUserInformationChildCollection() {
		return this.userInformationChild.values();
	}

	/**
	 * Gets the user information child children.
	 *
	 * @return the user information child children
	 */
	public Map<String, IAdminTreeChild> getUserInformationChildChildren() {
		return userInformationChild;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild#
	 * getParent()
	 */
	@Override
	public IAdminTreeChild getParent() {
		return this.parent;
	}

	/*
	 * com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild#
	 * setParent(com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.
	 * IAdminTreeChild)
	 */
	@Override
	public void setParent(final IAdminTreeChild parent) {
		this.parent = parent;
	}

}
