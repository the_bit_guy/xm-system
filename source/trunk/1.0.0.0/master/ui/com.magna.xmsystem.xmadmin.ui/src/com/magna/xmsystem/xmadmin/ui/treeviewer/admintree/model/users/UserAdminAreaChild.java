package com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.users;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;

import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.refs.RelationObj;

/**
 * Class for User admin area child.
 *
 * @author Chiranjeevi.Akula
 */
public class UserAdminAreaChild implements IAdminTreeChild {

	/** Member variable 'parent' for {@link IAdminTreeChild}. */
	private IAdminTreeChild parent;

	/**
	 * Member variable 'user admin area children' for
	 * {@link Map<String,IAdminTreeChild>}.
	 */
	private Map<String, IAdminTreeChild> userAdminAreaChildren;

	/**
	 * Constructor for UserAdminAreaChild Class.
	 *
	 * @param parent
	 *            {@link IAdminTreeChild}
	 */
	public UserAdminAreaChild(final IAdminTreeChild parent) {
		this.parent = parent;
		this.userAdminAreaChildren = new LinkedHashMap<>();
	}

	/**
	 * Method for Adds the fixed children.
	 *
	 * @param relationObj
	 *            {@link RelationObj}
	 */
	public void addFixedChildren(RelationObj relationObj) {
		this.userAdminAreaChildren.put(UserAAUserApplications.class.getName(), new UserAAUserApplications(relationObj));
	}

	/**
	 * Gets the user admin area children collection.
	 *
	 * @return the user admin area children collection
	 */
	public Collection<IAdminTreeChild> getUserAdminAreaChildrenCollection() {
		return this.userAdminAreaChildren.values();
	}

	/**
	 * Gets the user admin area children children.
	 *
	 * @return the user admin area children children
	 */
	public Map<String, IAdminTreeChild> getUserAdminAreaChildren() {
		return userAdminAreaChildren;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild#
	 * getParent()
	 */
	@Override
	public IAdminTreeChild getParent() {
		return this.parent;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild#
	 * setParent(com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.
	 * IAdminTreeChild)
	 */
	@Override
	public void setParent(final IAdminTreeChild parent) {
		this.parent = parent;
	}

}
