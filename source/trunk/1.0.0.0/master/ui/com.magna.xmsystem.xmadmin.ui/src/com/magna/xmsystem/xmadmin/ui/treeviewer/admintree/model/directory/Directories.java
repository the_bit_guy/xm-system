package com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.directory;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild;

/**
 * The Class Directories.
 * 
 * @author shashwat.anand
 */
public class Directories implements IAdminTreeChild {
	/** The parent. */
	private IAdminTreeChild parent;
	
	/** List for storing {@link Directory}. */
	private Map<String, IAdminTreeChild> directoriesChildren;

	/**
	 * Constructor.
	 */
	public Directories() {
		this.parent = null;
		this.directoriesChildren = new LinkedHashMap<>();
	}

	/**
	 * Adds the child.
	 *
	 * @param directoryName {@link String}
	 * @param child {@link IAdminTreeChild}
	 * @return {@link IAdminTreeChild}
	 */
	public IAdminTreeChild add(final String directoryName, final IAdminTreeChild child) {
		child.setParent(this);
		final IAdminTreeChild returnVal = this.directoriesChildren.put(directoryName, child);
		sort();
		return returnVal;
	}

	/**
	 * Removes the child.
	 *
	 * @param directoryName the group name
	 * @return {@link IAdminTreeChild}
	 */
	public IAdminTreeChild remove(final String directoryName) {
		return this.directoriesChildren.remove(directoryName);
	}
	
	/**
	 * Method for Removes the all.
	 */
	public void removeAll() {
		this.directoriesChildren.clear();
	}
	/**
	 * Gets the directories collection.
	 *
	 * @return {@link List} of {@link Directory}
	 */
	public Collection<IAdminTreeChild> getDirectoriesCollection() {
		return this.directoriesChildren.values();
	}

	
	/**
	 * Gets the directories children.
	 *
	 * @return the directories children
	 */
	public Map<String, IAdminTreeChild> getDirectoriesChildren() {
		return directoriesChildren;
	}
	
	/**
	 * Returns null as its parent.
	 *
	 * @return the parent
	 * @see com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild#getParent()
	 */
	@Override
	public IAdminTreeChild getParent() {
		return this.parent;
	}

	/**
	 * Sets the parent.
	 *
	 * @param parent the new parent
	 * @see com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild#setParent(com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild)
	 */
	@Override
	public void setParent(final IAdminTreeChild parent) {
		this.parent = parent;
	}
	
	/**
	 * Sort.
	 */
	public void sort() {
		final LinkedHashMap<String, IAdminTreeChild> collect = this.directoriesChildren.entrySet().stream().sorted(
				(e1, e2) -> ((Directory) e1.getValue()).getName().compareTo(((Directory) e2.getValue()).getName()))
				.collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e1, LinkedHashMap::new));
		this.directoriesChildren = collect;
	}
}

