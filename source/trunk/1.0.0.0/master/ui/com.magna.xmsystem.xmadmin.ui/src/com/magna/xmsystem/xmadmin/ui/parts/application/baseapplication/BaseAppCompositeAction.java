package com.magna.xmsystem.xmadmin.ui.parts.application.baseapplication;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.inject.Inject;

import org.eclipse.core.databinding.Binding;
import org.eclipse.core.databinding.DataBindingContext;
import org.eclipse.core.databinding.UpdateValueStrategy;
import org.eclipse.core.databinding.beans.BeanProperties;
import org.eclipse.core.databinding.observable.Observables;
import org.eclipse.core.databinding.observable.map.IObservableMap;
import org.eclipse.core.databinding.observable.value.IObservableValue;
import org.eclipse.core.databinding.observable.value.IValueChangeListener;
import org.eclipse.core.databinding.observable.value.ValueChangeEvent;
import org.eclipse.e4.core.services.nls.Translation;
import org.eclipse.e4.ui.di.Persist;
import org.eclipse.e4.ui.model.application.ui.MDirtyable;
import org.eclipse.jface.databinding.fieldassist.ControlDecorationSupport;
import org.eclipse.jface.databinding.swt.WidgetProperties;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.fieldassist.ControlDecoration;
import org.eclipse.jface.fieldassist.FieldDecorationRegistry;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.viewers.TreePath;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.VerifyEvent;
import org.eclipse.swt.events.VerifyListener;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Link;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.widgets.ToolItem;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.client.ResourceAccessException;

import com.magna.xmbackend.entities.BaseAppTranslationTbl;
import com.magna.xmbackend.entities.BaseApplicationsTbl;
import com.magna.xmbackend.entities.LanguagesTbl;
import com.magna.xmbackend.vo.baseApplication.BaseApplicationRequest;
import com.magna.xmsystem.dependencies.customExceptions.UnauthorizedAccessException;
import com.magna.xmsystem.dependencies.utils.LANG_ENUM;
import com.magna.xmsystem.dependencies.utils.XMSystemUtil;
import com.magna.xmsystem.ui.controls.widgets.CustomMessageDialog;
import com.magna.xmsystem.xmadmin.message.Message;
import com.magna.xmsystem.xmadmin.message.MessageRegistry;
import com.magna.xmsystem.xmadmin.restclient.application.BaseAppController;
import com.magna.xmsystem.xmadmin.ui.dialogs.BrowseScriptDialog;
import com.magna.xmsystem.xmadmin.ui.dialogs.lang.ControlModel;
import com.magna.xmsystem.xmadmin.ui.dialogs.lang.TextAreaModifyListener;
import com.magna.xmsystem.xmadmin.ui.dialogs.lang.XMAdminLangTextAreaDialog;
import com.magna.xmsystem.xmadmin.ui.dialogs.lang.XMAdminLangTextDialog;
import com.magna.xmsystem.xmadmin.ui.parts.icons.IconDialog;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.AdminTreeviewer;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.AdminTreeFactory;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.baseapplicationmodel.BaseApplication;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.baseapplicationmodel.BaseApplications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.icon.Icon;
import com.magna.xmsystem.xmadmin.ui.validation.NameValidation;
import com.magna.xmsystem.xmadmin.ui.validation.StatusValidation;
import com.magna.xmsystem.xmadmin.ui.validation.SymbolValidation;
import com.magna.xmsystem.xmadmin.util.CommonConstants;
import com.magna.xmsystem.xmadmin.util.MessageType;
import com.magna.xmsystem.xmadmin.util.XMAdminUtil;

/**
 * Class for Base app composite action.
 *
 * @author Chiranjeevi.Akula
 */
public class BaseAppCompositeAction extends BaseAppCompositeUI {

	/** The Constant LOGGER. */
	private static final Logger LOGGER = LoggerFactory.getLogger(BaseAppCompositeAction.class);

	/** Member variable 'base application model' for {@link BaseApplication}. */
	private BaseApplication baseApplicationModel;

	/** Member variable 'registry' for {@link MessageRegistry}. */
	@Inject
	private MessageRegistry registry;

	/** Member variable 'messages' for {@link Message}. */
	@Inject
	@Translation
	transient private Message messages;

	/**
	 * Member variable 'observable platform map' for
	 * {@link IObservableMap<String,Boolean>}.
	 */
	transient private IObservableMap<String, Boolean> observablePlatformMap;

	/** Member variable 'widget value' for {@link IObservableValue<?>}. */
	transient private IObservableValue<?> widgetValue;

	/** Member variable 'model value' for {@link IObservableValue<?>}. */
	transient private IObservableValue<?> modelValue;

	/** Member variable 'data bind context' for {@link DataBindingContext}. */
	final transient DataBindingContext dataBindContext = new DataBindingContext();

	/** Member variable 'old model' for {@link BaseApplication}. */
	private BaseApplication oldModel;

	/** Member variable 'dirty' for {@link MDirtyable}. */
	private MDirtyable dirty;

	/**
	 * Constructor for BaseAppCompositeAction Class.
	 *
	 * @param parent
	 *            {@link Composite}
	 */
	@Inject
	public BaseAppCompositeAction(final Composite parent) {
		super(parent, SWT.NONE);
		initListeners();
	}

	/**
	 * Method for Bind values.
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public void bindValues() {
		try {
			// Name field binding
			widgetValue = WidgetProperties.text(SWT.Modify).observe(this.txtName);
			modelValue = BeanProperties.value(BaseApplication.class, BaseApplication.PROPERTY_NAME).observe(this.baseApplicationModel);
			widgetValue.addValueChangeListener(new IValueChangeListener() {
				/**
				 * handler to update button status
				 */
				@Override
				public void handleValueChange(final ValueChangeEvent event) {
					updateButtonStatus(event);
				}
			});

			// define the UpdateValueStrategy
			final UpdateValueStrategy update = new UpdateValueStrategy();
			update.setAfterGetValidator(new NameValidation(messages, StatusValidation.APPLICATION));
			Binding bindValue = dataBindContext.bindValue(widgetValue, modelValue, update, null);
			ControlDecorationSupport.create(bindValue, SWT.TOP | SWT.LEFT);

			// Active check box binding
			widgetValue = WidgetProperties.selection().observe(this.activeBtn);
			modelValue = BeanProperties.value(BaseApplication.class, BaseApplication.PROPERTY_ACTIVE)
					.observe(this.baseApplicationModel);
			bindValue = dataBindContext.bindValue(widgetValue, modelValue);

			// Symbol toolItem setup
			Icon icon;
			String iconPath;
			if ((icon = this.baseApplicationModel.getIcon()) != null
					&& !XMSystemUtil.isEmpty(iconPath = icon.getIconPath())) {
				if (iconPath.contains("null")) { //$NON-NLS-1$
					iconToolItem.setImage(XMSystemUtil.getInstance().getImage(this.getClass(), "icons/16x16/browse.png"));  //$NON-NLS-1$
				} else {
					iconToolItem.setImage(XMSystemUtil.getInstance().getImage(this.getClass(), iconPath, true, true));
					// Symbol toolItem binding
					widgetValue = WidgetProperties.tooltipText().observe(this.iconToolItem);
					modelValue = BeanProperties.value(BaseApplication.class,
							BaseApplication.PROPERTY_ICON + "." + Icon.PROPERTY_ICONNAME, Icon.class)
							.observe(this.baseApplicationModel);
					bindValue = dataBindContext.bindValue(widgetValue, modelValue);

					// Symbol Field binding
					widgetValue = WidgetProperties.text(SWT.Modify).observe(this.txtSymbol);
					modelValue = BeanProperties.value(BaseApplication.class,
							BaseApplication.PROPERTY_ICON + "." + Icon.PROPERTY_ICONNAME, Icon.class)
							.observe(this.baseApplicationModel);
					final UpdateValueStrategy symbolUpdate = new UpdateValueStrategy();
					symbolUpdate.setAfterGetValidator(new SymbolValidation());
					bindValue = dataBindContext.bindValue(widgetValue, modelValue, symbolUpdate, null);
					ControlDecorationSupport.create(bindValue, SWT.TOP | SWT.LEFT);
				}
			}

			// add some decorations
			ControlDecorationSupport.create(bindValue, SWT.TOP | SWT.LEFT);

			// Program field binding
			widgetValue = WidgetProperties.text(SWT.Modify).observe(this.txtProgram);
			modelValue = BeanProperties.value(BaseApplication.class, BaseApplication.PROPERTY_PROGRAM)
					.observe(this.baseApplicationModel);
			bindValue = dataBindContext.bindValue(widgetValue, modelValue);

			// Platforms field binding
			observablePlatformMap = BeanProperties.map(BaseApplication.class, BaseApplication.PROPERTY_PLATFORM_MAP)
					.observe(baseApplicationModel);
			for (int index = 0; index < this.btnPlatforms.length; index++) {
				widgetValue = WidgetProperties.selection().observe(this.btnPlatforms[index]);

				modelValue = Observables.observeMapEntry(observablePlatformMap,
						BaseApplication.SUPPORTED_PLATFORMS.get(index));
				bindValue = dataBindContext.bindValue(widgetValue, modelValue);
			}
		} catch (Exception e) {
			LOGGER.error("Exception occured while databinding", e);
		}
	}
	
	/**
	 * Method to update the button status.
	 *
	 * @param event
	 *            the event
	 */
	@SuppressWarnings("rawtypes")
	private void updateButtonStatus(final ValueChangeEvent event) {
		final String name = (String) event.getObservableValue().getValue();
		if (this.saveBtn != null) {
			if (XMSystemUtil.isEmpty(name) || name.trim().length() == 0
					|| (!name.matches(CommonConstants.RegularExpressions.ALLOWED_APP_NAME_REGEX))) {
				this.saveBtn.setEnabled(false);
			} else {
				this.saveBtn.setEnabled(true);
			}
		}
	}

	/**
	 * Gets the base application model.
	 *
	 * @return the base application model
	 */
	public BaseApplication getBaseApplicationModel() {
		return baseApplicationModel;
	}

	/**
	 * Sets the base application model.
	 *
	 * @param baseApplicationModel
	 *            the new base application model
	 */
	public void setBaseApplicationModel(final BaseApplication baseApplicationModel) {
		this.baseApplicationModel = baseApplicationModel;
	}

	/**
	 * Method for Inits the listeners.
	 */
	private void initListeners() {
		this.descLink.addSelectionListener(new SelectionAdapter() {

			@Override
			public void widgetSelected(SelectionEvent event) {
				final Link linkWidget = (Link) event.widget;
				openDescDialog(linkWidget.getShell());
			}
		});

		this.remarksLink.addSelectionListener(new SelectionAdapter() {

			@Override
			public void widgetSelected(SelectionEvent event) {
				final Link linkWidget = (Link) event.widget;
				openRemarksDialog(linkWidget.getShell());
			}
		});

		if (this.saveBtn != null) {
			this.saveBtn.addSelectionListener(new SelectionAdapter() {

				@Override
				public void widgetSelected(final SelectionEvent event) {
					saveHandler();
				}
			});
		}

		if (this.cancelBtn != null) {
			this.cancelBtn.addSelectionListener(new SelectionAdapter() {

				@Override
				public void widgetSelected(final SelectionEvent event) {
					cancelHandler();
				}
			});
		}

		this.iconToolItem.addSelectionListener(new SelectionAdapter() {

			@Override
			public void widgetSelected(final SelectionEvent event) {
				if ((boolean) iconToolItem.getData("editable")) {
					final ToolItem widget = (ToolItem) event.widget;
					final IconDialog dialog = new IconDialog(widget.getParent().getShell(),
							messages.browseIconDialogTitle, messages.icontableviewerSecondColumnLabel);

					final int returnVal = dialog.open();
					if (IDialogConstants.OK_ID == returnVal) {
						final Icon checkedIcon = dialog.getCheckedIcon();
						iconToolItem.setImage(XMSystemUtil.getInstance().getImage(this.getClass(), checkedIcon.getIconPath(), true, true));
						txtSymbol.setText(checkedIcon.getIconName());
						baseApplicationModel.setIcon(checkedIcon);
					}
				}
			}
		});
		this.txtRemarks
				.addModifyListener(new TextAreaModifyListener(this.lblRemarksCount, BaseApplication.REMARK_LIMIT));
		this.programToolItem.addSelectionListener(new SelectionAdapter() {

			@Override
			public void widgetSelected(final SelectionEvent event) {
				if ((boolean) programToolItem.getData("editable")) {
					final ToolItem widget = (ToolItem) event.widget;
					BrowseScriptDialog scriptDialog = new BrowseScriptDialog(widget.getParent().getShell(), messages);
					final int returnVal = scriptDialog.open();
					if (IDialogConstants.OK_ID == returnVal) {
						String selectedScript = scriptDialog.getSelectedScript();
						if (selectedScript != null) {
							if (selectedScript.matches("([a-zA-Z]:)?(\\\\[ a-zA-Z0-9_.-]+)+\\\\?")) {
								txtProgram.setText(selectedScript);
							} else {
								String[] scriptName = selectedScript.split("\\.\\s*");
								txtProgram.setText(scriptName[0]);
							}
						}
					}
				}
			}
		});

		this.txtProgram.addModifyListener(new ModifyListener() {
			ControlDecoration decroator = new ControlDecoration(txtProgram, SWT.TOP);

			@Override
			public void modifyText(ModifyEvent event) {
				final Image nameDecoratorImage = FieldDecorationRegistry.getDefault()
						.getFieldDecoration(FieldDecorationRegistry.DEC_ERROR).getImage();
				decroator.setImage(nameDecoratorImage);
				Text text = (Text) event.widget;
				if (text.getText().trim().length() <= 0) {
					decroator.setDescriptionText(messages.appProgramEmptyError);
					decroator.show();
				} else if (text.getText().trim().length() > 0) {
					decroator.hide();
				}
			}
		});

		this.txtRemarks.addVerifyListener(new VerifyListener() {
			@Override
			public void verifyText(VerifyEvent event) {
				String source = ((Text) event.widget).getText();
				final String remarkText = source.substring(0, event.start) + event.text + source.substring(event.end);
				int length = remarkText.length();
				if (length > BaseApplication.REMARK_LIMIT) {
					event.doit = false;
				}
			}
		});
	}

	/**
	 * Method for Register messages.
	 *
	 * @param registry
	 *            {@link MessageRegistry}
	 */
	public void registerMessages(final MessageRegistry registry) {
		registry.register((text) -> {
			if (grpBaseApp != null && !grpBaseApp.isDisposed()) {
				grpBaseApp.setText(text);
			}
		}, (message) -> {
			if (baseApplicationModel != null) {
				String name = this.baseApplicationModel.getName();
				if (baseApplicationModel.getOperationMode() == CommonConstants.OPERATIONMODE.VIEW) {
					return message.objectGroupDisaplyLabel + " \'" + name + "\'";
				} else if (baseApplicationModel.getOperationMode() == CommonConstants.OPERATIONMODE.CHANGE) {
					return message.objectGroupChangeLabel + " \'" + name + "\'";
				} else if (baseApplicationModel.getOperationMode() == CommonConstants.OPERATIONMODE.CREATE) {
					return message.baseAppGroupCreateLabel;
				}
			}
			return CommonConstants.EMPTY_STR;
		});

		registry.register((text) -> {
			if (lblActive != null && !lblActive.isDisposed()) {
				lblActive.setText(text);
			}
		}, (message) -> {
			if (lblActive != null && !lblActive.isDisposed()) {
				return getUpdatedWidgetText(message.objectActiveLabel, lblActive);
			}
			return CommonConstants.EMPTY_STR;
		});

		registry.register((text) -> {
			if (lblSymbol != null && !lblSymbol.isDisposed()) {
				lblSymbol.setText(text);
			}
		}, (message) -> {
			if (lblSymbol != null && !lblSymbol.isDisposed()) {
				return getUpdatedWidgetText(message.objectSymbolLabel, lblSymbol);
			}
			return CommonConstants.EMPTY_STR;
		});

		if (saveBtn != null) {
			registry.register((text) -> {
				if (saveBtn != null && !saveBtn.isDisposed()) {
					saveBtn.setText(text);
				}
			}, (message) -> {
				if (saveBtn != null && !saveBtn.isDisposed()) {
					return getUpdatedWidgetText(message.saveButtonText, saveBtn);
				}
				return CommonConstants.EMPTY_STR;
			});
		}

		if (cancelBtn != null) {
			registry.register((text) -> {
				if (cancelBtn != null && !cancelBtn.isDisposed()) {
					cancelBtn.setText(text);
				}
			}, (message) -> {
				if (cancelBtn != null && !cancelBtn.isDisposed()) {
					return getUpdatedWidgetText(message.cancelButtonText, cancelBtn);
				}
				return CommonConstants.EMPTY_STR;
			});
		}

		registry.register((text) -> {
			if (lblName != null && !lblName.isDisposed()) {
				lblName.setText(text);
			}
		}, (message) -> {
			if (lblName != null && !lblName.isDisposed()) {
				return getUpdatedWidgetText(message.objectNameLanguageLabel, lblName);
			}
			return CommonConstants.EMPTY_STR;
		});

		registry.register((text) -> {
			if (lblDescrition != null && !lblDescrition.isDisposed()) {
				lblDescrition.setText(text);
			}
		}, (message) -> {
			if (lblDescrition != null && !lblDescrition.isDisposed()) {
				return getUpdatedWidgetText(message.objectDescriptionLabel, lblDescrition);
			}
			return CommonConstants.EMPTY_STR;
		});

		registry.register((text) -> {
			if (remarksLink != null && !remarksLink.isDisposed()) {
				remarksLink.setText(text);
			}
		}, (message) -> {
			if (remarksLink != null && !remarksLink.isDisposed()) {
				return getUpdatedWidgetText("<a>" + message.objectTranslationLinkText + "</a>", remarksLink);
			}
			return CommonConstants.EMPTY_STR;
		});

		registry.register((text) -> {
			if (remarksLabel != null && !remarksLabel.isDisposed()) {
				remarksLabel.setText(text);
			}
		}, (message) -> {
			if (remarksLabel != null && !remarksLabel.isDisposed()) {
				return getUpdatedWidgetText(message.objectRemarkLabel, remarksLabel);
			}
			return CommonConstants.EMPTY_STR;
		});

		registry.register((text) -> {
			if (descLink != null && !descLink.isDisposed()) {
				descLink.setText(text);
			}
		}, (message) -> {
			if (descLink != null && !descLink.isDisposed()) {
				return getUpdatedWidgetText("<a>" + message.objectTranslationLinkText + "</a>", descLink);
			}
			return CommonConstants.EMPTY_STR;
		});

		registry.register((text) -> {
			if (this.baseApplicationModel != null && txtName != null && !txtName.isDisposed()) {
				txtName.setText(text);
			}
		}, (message) -> {
			if (this.baseApplicationModel != null && txtName != null && !txtName.isDisposed()) {
				return this.baseApplicationModel.getName() == null ? CommonConstants.EMPTY_STR
						: this.baseApplicationModel.getName();
			}
			return CommonConstants.EMPTY_STR;
		});

		registry.register((text) -> {
			if (this.baseApplicationModel != null && txtDesc != null && !txtDesc.isDisposed()) {
				txtDesc.setText(text);
				updateDescWidget();
			}
		}, (message) -> {
			if (this.baseApplicationModel != null && txtDesc != null && !txtDesc.isDisposed()) {
				LANG_ENUM langEnum = XMAdminUtil.getInstance().getCurrentLocaleEnum();
				return this.baseApplicationModel.getDescription(langEnum) == null ? CommonConstants.EMPTY_STR
						: this.baseApplicationModel.getDescription(langEnum);
			}
			return CommonConstants.EMPTY_STR;
		});

		registry.register((text) -> {
			if (txtRemarks != null && !txtRemarks.isDisposed()) {
				txtRemarks.setText(text);
				updateRemarksWidget();
			}
		}, (message) -> {
			if (this.baseApplicationModel != null && txtRemarks != null && !txtRemarks.isDisposed()) {
				LANG_ENUM langEnum = XMAdminUtil.getInstance().getCurrentLocaleEnum();
				return this.baseApplicationModel.getRemarks(langEnum) == null ? CommonConstants.EMPTY_STR
						: this.baseApplicationModel.getRemarks(langEnum);
			}
			return CommonConstants.EMPTY_STR;
		});

		registry.register((text) -> {
			if (lblPlatforms != null && !lblPlatforms.isDisposed()) {
				lblPlatforms.setText(text);
			}
		}, (message) -> {
			if (lblPlatforms != null && !lblPlatforms.isDisposed()) {
				return getUpdatedWidgetText(message.objectPlatformLabel, lblPlatforms);
			}
			return CommonConstants.EMPTY_STR;
		});

		registry.register((text) -> {
			int index = BaseApplication.SUPPORTED_PLATFORMS.indexOf("WINDOWS32");
			if (this.btnPlatforms[index] != null && !this.btnPlatforms[index].isDisposed()) {
				this.btnPlatforms[index].setText(text);
			}
		}, (message) -> {
			int index = BaseApplication.SUPPORTED_PLATFORMS.indexOf("WINDOWS32");
			if (this.btnPlatforms[index] != null && !this.btnPlatforms[index].isDisposed()) {
				return getUpdatedWidgetText(message.WINDOWS32, this.btnPlatforms[index]);
			}
			return CommonConstants.EMPTY_STR;
		});

		registry.register((text) -> {
			int index = BaseApplication.SUPPORTED_PLATFORMS.indexOf("WINDOWS64");
			if (this.btnPlatforms[index] != null && !this.btnPlatforms[index].isDisposed()) {
				this.btnPlatforms[index].setText(text);
			}
		}, (message) -> {
			int index = BaseApplication.SUPPORTED_PLATFORMS.indexOf("WINDOWS64");
			if (this.btnPlatforms[index] != null && !this.btnPlatforms[index].isDisposed()) {
				return getUpdatedWidgetText(message.WINDOWS64, this.btnPlatforms[index]);
			}
			return CommonConstants.EMPTY_STR;
		});

		registry.register((text) -> {
			int index = BaseApplication.SUPPORTED_PLATFORMS.indexOf("LINUX64");
			if (this.btnPlatforms[index] != null && !this.btnPlatforms[index].isDisposed()) {
				this.btnPlatforms[index].setText(text);
			}
		}, (message) -> {
			int index = BaseApplication.SUPPORTED_PLATFORMS.indexOf("LINUX64");
			if (this.btnPlatforms[index] != null && !this.btnPlatforms[index].isDisposed()) {
				return getUpdatedWidgetText(message.LINUX64, this.btnPlatforms[index]);
			}
			return CommonConstants.EMPTY_STR;
		});

		registry.register((text) -> {
			if (lblProgram != null && !lblProgram.isDisposed()) {
				lblProgram.setText(text);
			}
		}, (message) -> {
			if (lblProgram != null && !lblProgram.isDisposed()) {
				return getUpdatedWidgetText(message.objectProgramLabel, lblProgram);
			}
			return CommonConstants.EMPTY_STR;
		});

	}

	/**
	 * Gets the updated widget text.
	 *
	 * @param message
	 *            {@link String}
	 * @param control
	 *            {@link Control}
	 * @return the updated widget text
	 */
	private String getUpdatedWidgetText(String message, Control control) {
		control.requestLayout();
		control.getParent().redraw();
		control.getParent().getParent().update();
		control.getParent().getParent().getParent().update();
		return message;
	}

	/**
	 * Gets the old model.
	 *
	 * @return the old model
	 */
	public BaseApplication getOldModel() {
		return oldModel;
	}

	/**
	 * Sets the old model.
	 *
	 * @param oldModel
	 *            the new old model
	 */
	public void setOldModel(BaseApplication oldModel) {
		this.oldModel = oldModel;
	}

	/**
	 * Method for Open desc dialog.
	 *
	 * @param shell
	 *            {@link Shell}
	 */
	private void openDescDialog(final Shell shell) {
		if (baseApplicationModel == null) {
			return;
		}
		if (baseApplicationModel.getOperationMode() != CommonConstants.OPERATIONMODE.VIEW) {
			String text = txtDesc.getText();
			LANG_ENUM currentLocaleEnum = XMAdminUtil.getInstance().getCurrentLocaleEnum();
			baseApplicationModel.setDescription(currentLocaleEnum, text);
		}
		Map<LANG_ENUM, String> obModelMap = new HashMap<>();
		obModelMap.put(LANG_ENUM.ENGLISH, this.baseApplicationModel.getDescription(LANG_ENUM.ENGLISH));
		obModelMap.put(LANG_ENUM.GERMAN, this.baseApplicationModel.getDescription(LANG_ENUM.GERMAN));
		boolean isEditable = txtDesc.getEditable();

		ControlModel controlModel = new ControlModel(this.messages.objectDescriptionLabel, obModelMap,
				BaseApplication.DESCRIPTION_LIMIT, false, isEditable);
		controlModel.initDefaultLabels(this.messages);
		final XMAdminLangTextDialog dialogArea = new XMAdminLangTextDialog(shell, controlModel);
		int retVal = dialogArea.open();
		if (retVal == IDialogConstants.OK_ID) {
			Map<LANG_ENUM, String> descriptionMap = this.baseApplicationModel.getDescriptionMap();
			descriptionMap.put(LANG_ENUM.ENGLISH, controlModel.getObjectModel(LANG_ENUM.ENGLISH));
			descriptionMap.put(LANG_ENUM.GERMAN, controlModel.getObjectModel(LANG_ENUM.GERMAN));
			updateDescWidget();
		}
	}

	/**
	 * Method for Open remarks dialog.
	 *
	 * @param shell
	 *            {@link Shell}
	 */
	private void openRemarksDialog(final Shell shell) {
		if (baseApplicationModel == null) {
			return;
		}
		if (baseApplicationModel.getOperationMode() != CommonConstants.OPERATIONMODE.VIEW) {
			String text = txtRemarks.getText();
			LANG_ENUM currentLocaleEnum = XMAdminUtil.getInstance().getCurrentLocaleEnum();
			baseApplicationModel.setRemarks(currentLocaleEnum, text);
		}

		Map<LANG_ENUM, String> obModelMap = new HashMap<>();
		obModelMap.put(LANG_ENUM.ENGLISH, this.baseApplicationModel.getRemarks(LANG_ENUM.ENGLISH));
		obModelMap.put(LANG_ENUM.GERMAN, this.baseApplicationModel.getRemarks(LANG_ENUM.GERMAN));
		boolean isEditable = txtRemarks.getEditable();

		ControlModel controlModel = new ControlModel(this.messages.objectDescriptionLabel, obModelMap,
				BaseApplication.REMARK_LIMIT, false, isEditable);
		controlModel.initDefaultLabels(this.messages);
		final XMAdminLangTextAreaDialog dialogArea = new XMAdminLangTextAreaDialog(shell, controlModel);
		int retVal = dialogArea.open();
		if (retVal == IDialogConstants.OK_ID) {
			Map<LANG_ENUM, String> notesMap = this.baseApplicationModel.getRemarksMap();
			notesMap.put(LANG_ENUM.ENGLISH, controlModel.getObjectModel(LANG_ENUM.ENGLISH));
			notesMap.put(LANG_ENUM.GERMAN, controlModel.getObjectModel(LANG_ENUM.GERMAN));
			updateRemarksWidget();
		}
	}

	/**
	 * Method for Creates the base application operation.
	 */
	private void createBaseApplicationOperation() {
		try {
			BaseAppController baseAppController = new BaseAppController();
			BaseApplicationsTbl baseAppVo = baseAppController.createBaseApplication(mapVOObjectWithModel());
			String baseApplicationId = baseAppVo.getBaseApplicationId();
			if (!XMSystemUtil.isEmpty(baseApplicationId)) {
				baseApplicationModel.setBaseApplicationId(baseApplicationId);
				Collection<BaseAppTranslationTbl> baseAppTranslationTblCollection = baseAppVo
						.getBaseAppTranslationTblCollection();
				for (BaseAppTranslationTbl baseAppTranslationTbl : baseAppTranslationTblCollection) {
					String baseAppTranslationId = baseAppTranslationTbl.getBaseAppTranslationId();
					LanguagesTbl languageCode = baseAppTranslationTbl.getLanguageCode();
					LANG_ENUM langEnum = LANG_ENUM.getLangEnum(languageCode.getLanguageCode());
					this.baseApplicationModel.setTranslationId(langEnum, baseAppTranslationId);
				}
				AdminTreeFactory instance = AdminTreeFactory.getInstance();
				if (oldModel == null) { // Attach this to tree
					setOldModel(baseApplicationModel.deepCopyBaseApplication(false, null));
					instance.getBaseApplications().add(baseApplicationId, getOldModel());
				}
				this.dirty.setDirty(false);
				AdminTreeviewer adminTree = XMAdminUtil.getInstance().getAdminTree();
				adminTree.refresh(true);

				adminTree.setSelection(new StructuredSelection(instance.getApplications()), true);
				TreePath[] selectionPaths = adminTree.getStructuredSelection().getPaths();
				if (selectionPaths != null && selectionPaths.length > 0) {
					adminTree.setExpandedState(selectionPaths[0], true);
				}
				adminTree.setSelection(new StructuredSelection(instance.getBaseApplications()), true);
				selectionPaths = adminTree.getStructuredSelection().getPaths();
				if (selectionPaths != null && selectionPaths.length > 0) {
					adminTree.setExpandedState(selectionPaths[0], true);
				}
				adminTree.setSelection(new StructuredSelection(getOldModel()), true);
				XMAdminUtil.getInstance().updateLogFile(messages.baseApplicationObject + " " + "'"
						+ this.baseApplicationModel.getName() + "'" + " " + messages.objectCreate,
						MessageType.SUCCESS);
			}

		} catch (UnauthorizedAccessException e) {
			CustomMessageDialog.openError(this.getShell(), messages.objectPermissionDialogTitle,
					messages.objectPermissionDialogMsg);
		} catch (Exception e) {
			if (e instanceof ResourceAccessException) {
				CustomMessageDialog.openError(Display.getCurrent().getActiveShell(), messages.errorDialogTitile,
						messages.serverNotReachable);
			}
			LOGGER.warn("Unable to Save data ! " + e);
		}
	}

	/**
	 * Method for Change base application operation.
	 */
	private void changeBaseApplicationOperation() {
		try {
			BaseAppController baseAppController = new BaseAppController();
			boolean isUpdated = baseAppController.updateBaseApplication(mapVOObjectWithModel());
			if (isUpdated) {
				setOldModel(baseApplicationModel.deepCopyBaseApplication(true, getOldModel()));
				this.baseApplicationModel.setOperationMode(CommonConstants.OPERATIONMODE.VIEW);
				setOperationMode();
				this.dirty.setDirty(false);
				final BaseApplications baseApplications = AdminTreeFactory.getInstance().getBaseApplications();
				baseApplications.sort();
				XMAdminUtil.getInstance().getAdminTree().refresh(true);
				XMAdminUtil.getInstance().getAdminTree().setSelection(new StructuredSelection(getOldModel()), true);
				XMAdminUtil.getInstance().updateLogFile(messages.baseApplicationObject + " " + "'"
						+ this.baseApplicationModel.getName() + "'" + " " + messages.objectUpdate,
						MessageType.SUCCESS);
			}
		} catch (UnauthorizedAccessException e) {
			CustomMessageDialog.openError(this.getShell(), messages.objectPermissionDialogTitle,
					messages.objectPermissionDialogMsg);
		} catch (Exception e) {
			if (e instanceof ResourceAccessException) {
				CustomMessageDialog.openError(Display.getCurrent().getActiveShell(), messages.errorDialogTitile,
						messages.serverNotReachable);
			}
			LOGGER.warn("Unable to Update data ! " + e);
		}
	}

	/**
	 * Method for Map VO object with model.
	 *
	 * @return the com.magna.xmbackend.vo.base application. base application
	 *         request {@link BaseApplicationRequest}
	 */
	private com.magna.xmbackend.vo.baseApplication.BaseApplicationRequest mapVOObjectWithModel() {
		com.magna.xmbackend.vo.baseApplication.BaseApplicationRequest baseApplicationRequest = new com.magna.xmbackend.vo.baseApplication.BaseApplicationRequest();

		baseApplicationRequest.setId(baseApplicationModel.getBaseApplicationId());
		baseApplicationRequest.setName(baseApplicationModel.getName());
		baseApplicationRequest.setIconId(baseApplicationModel.getIcon().getIconId());
		baseApplicationRequest.setStatus(baseApplicationModel.isActive() == true ? com.magna.xmbackend.vo.enums.Status.ACTIVE.name()
						: com.magna.xmbackend.vo.enums.Status.INACTIVE.name());
		baseApplicationRequest.setPlatforms(baseApplicationModel.getPlatformString());
		baseApplicationRequest.setProgram(baseApplicationModel.getProgram());

		List<com.magna.xmbackend.vo.baseApplication.BaseApplicationTransulation> baseAppTransulationList = new ArrayList<>();
		LANG_ENUM[] lang_values = LANG_ENUM.values();
		for (int index = 0; index < lang_values.length; index++) {
			com.magna.xmbackend.vo.baseApplication.BaseApplicationTransulation baseAppTransulation = new com.magna.xmbackend.vo.baseApplication.BaseApplicationTransulation();
			baseAppTransulation.setLanguageCode(lang_values[index].getLangCode());
			baseAppTransulation.setRemarks(baseApplicationModel.getRemarks(lang_values[index]));
			baseAppTransulation.setDescription(baseApplicationModel.getDescription(lang_values[index]));
			if (this.baseApplicationModel.getOperationMode() == CommonConstants.OPERATIONMODE.CHANGE) {
				String translationId = baseApplicationModel.getTranslationId(lang_values[index]);
				baseAppTransulation.setId(XMSystemUtil.isEmpty(translationId) ? CommonConstants.EMPTY_STR : translationId);
			}
			baseAppTransulationList.add(baseAppTransulation);
		}

		baseApplicationRequest.setBaseApplicationTransulations(baseAppTransulationList);

		return baseApplicationRequest;
	}

	/**
	 * Method for Sets the operation mode.
	 */
	public void setOperationMode() {
		if (this.baseApplicationModel != null) {
			if (baseApplicationModel.getOperationMode() == CommonConstants.OPERATIONMODE.VIEW) {
				this.txtName.setEditable(false);
				this.txtDesc.setEditable(false);
				this.txtRemarks.setEditable(false);
				this.activeBtn.setEnabled(false);
				this.txtProgram.setEditable(false);
				this.iconToolItem.setData("editable", false);
				this.programToolItem.setData("editable", false);
				for (int index = 0; index < this.btnPlatforms.length; index++) {
					this.btnPlatforms[index].setEnabled(false);
				}
				setShowButtonBar(false);
			} else if (baseApplicationModel.getOperationMode() == CommonConstants.OPERATIONMODE.CREATE) {
				this.saveBtn.setEnabled(false);
				this.txtName.setEditable(true);
				this.txtDesc.setEditable(true);
				this.txtRemarks.setEditable(true);
				this.activeBtn.setEnabled(true);
				this.txtProgram.setEditable(true);
				this.iconToolItem.setData("editable", true);
				this.programToolItem.setData("editable", true);
				for (int index = 0; index < this.btnPlatforms.length; index++) {
					this.btnPlatforms[index].setEnabled(true);
				}
				setShowButtonBar(true);
				this.dirty.setDirty(true);
			} else if (baseApplicationModel.getOperationMode() == CommonConstants.OPERATIONMODE.CHANGE) {
				this.txtName.setEditable(true);
				this.txtDesc.setEditable(true);
				this.txtRemarks.setEditable(true);
				this.activeBtn.setEnabled(true);
				this.txtProgram.setEditable(true);
				this.iconToolItem.setData("editable", true);
				this.programToolItem.setData("editable", true);
				for (int index = 0; index < this.btnPlatforms.length; index++) {
					this.btnPlatforms[index].setEnabled(true);
				}
				setShowButtonBar(true);
				this.dirty.setDirty(true);
			} else {
				this.txtName.setEditable(false);
				this.txtDesc.setEditable(false);
				this.txtRemarks.setEditable(false);
				this.activeBtn.setEnabled(false);
				this.txtProgram.setEditable(false);
				this.iconToolItem.setData("editable", false);
				this.programToolItem.setData("editable", false);
				for (int index = 0; index < this.btnPlatforms.length; index++) {
					this.btnPlatforms[index].setEnabled(false);
				}
				setShowButtonBar(false);
			}
		}
	}

	/**
	 * Method for Save handler.
	 */
	public void saveHandler() {
		// validate the model
		saveDescAndRemarks();
		if (validate()) {
			if (baseApplicationModel.getOperationMode() == CommonConstants.OPERATIONMODE.CREATE) {
				createBaseApplicationOperation();
			} else if (baseApplicationModel.getOperationMode() == CommonConstants.OPERATIONMODE.CHANGE) {
				changeBaseApplicationOperation();
			}
		}
	}

	/**
	 * Method for Save desc and remarks.
	 */
	private void saveDescAndRemarks() {
		final LANG_ENUM currentLocaleEnum = XMAdminUtil.getInstance().getCurrentLocaleEnum();
		final String desc = txtDesc.getText();
		baseApplicationModel.setDescription(currentLocaleEnum, desc);
		final String remarks = txtRemarks.getText();
		baseApplicationModel.setRemarks(currentLocaleEnum, remarks);
	}

	/**
	 * Method for Validate.
	 *
	 * @return true, if successful
	 */
	protected boolean validate() {
		if (!saveBtn.isEnabled()) {
			return false;
		}
		String baseAppName = this.baseApplicationModel.getName();
		String basesupportedplatform = this.baseApplicationModel.getPlatformString();
		Icon icon;
		if (XMSystemUtil.isEmpty(baseAppName) && (icon = this.baseApplicationModel.getIcon()) != null
				&& XMSystemUtil.isEmpty(icon.getIconName())) {
			CustomMessageDialog.openError(this.getShell(), messages.nameErrorTitle, messages.nameError);
			return false;
		}
		if (XMSystemUtil.isEmpty(basesupportedplatform)) {
			CustomMessageDialog.openError(this.getShell(), messages.supportedPlatfErrorTitle,
					messages.supprotedPltfEmptyEnError);
			return false;
		}
		if ((icon = this.baseApplicationModel.getIcon()) != null && XMSystemUtil.isEmpty(icon.getIconName())) {
			CustomMessageDialog.openError(this.getShell(), messages.symbolErrorTitle, messages.symbolError);
			return false;
		}
		final BaseApplications baseApplications = AdminTreeFactory.getInstance().getBaseApplications();
		final Collection<IAdminTreeChild> baseAppsCollection = baseApplications.getBaseAppCollection();

		if (!XMSystemUtil.isEmpty(baseAppName)) {
			final Map<String, Long> result = baseAppsCollection.parallelStream()
					.collect(Collectors.groupingBy(baseApplication -> {
						if (!XMSystemUtil.isEmpty(((BaseApplication) baseApplication).getName())) {
							return ((BaseApplication) baseApplication).getName().toUpperCase();
						}
						return CommonConstants.EMPTY_STR;
					}, Collectors.counting()));
			if (result.containsKey(baseAppName.toUpperCase())) {
				if ((this.baseApplicationModel.getOperationMode() == CommonConstants.OPERATIONMODE.CHANGE
						&& !baseAppName.equalsIgnoreCase(this.oldModel.getName()))
						|| this.baseApplicationModel.getOperationMode() == CommonConstants.OPERATIONMODE.CREATE) {
					CustomMessageDialog.openError(this.getShell(), messages.appNameErrorTitle,
							messages.existingEnNameError);
					return false;
				}
			}
		}
		if (XMSystemUtil.isEmpty(this.txtProgram.getText())) {
			CustomMessageDialog.openError(this.getShell(), messages.appProgramErrorTitle,
					messages.appProgramEmptyError);
			return false;
		}
		return true;
	}

	/**
	 * Sets the dirty object.
	 *
	 * @param dirty
	 *            the new dirty object
	 */
	@Persist
	public void setDirtyObject(final MDirtyable dirty) {
		this.dirty = dirty;
	}

	/**
	 * Method for Update remarks widget.
	 */
	public void updateRemarksWidget() {
		if (this.baseApplicationModel == null) {
			return;
		}
		int operationMode = this.baseApplicationModel.getOperationMode();
		final LANG_ENUM currentLocaleEnum = XMAdminUtil.getInstance().getCurrentLocaleEnum();

		String remarkForCurLocale = this.baseApplicationModel.getRemarks(currentLocaleEnum) == null
				? CommonConstants.EMPTY_STR : this.baseApplicationModel.getRemarks(currentLocaleEnum);
		if (operationMode != CommonConstants.OPERATIONMODE.VIEW) {
			this.txtRemarks.setText(remarkForCurLocale);
			return;
		}

		if (operationMode == CommonConstants.OPERATIONMODE.VIEW) {
			if (!XMSystemUtil.isEmpty(remarkForCurLocale)) {
				this.txtRemarks.setText(remarkForCurLocale);
				return;
			}
			final String remarkEN = this.baseApplicationModel.getRemarks(LANG_ENUM.ENGLISH);
			final String remarkDE = this.baseApplicationModel.getRemarks(LANG_ENUM.GERMAN);
			if (!XMSystemUtil.isEmpty(remarkEN)) {
				this.txtRemarks.setText(remarkEN);
				return;
			}

			if (!XMSystemUtil.isEmpty(remarkDE)) {
				this.txtRemarks.setText(remarkDE);
				return;
			}
		}
	}

	/**
	 * Method for Update desc widget.
	 */
	public void updateDescWidget() {
		if (this.baseApplicationModel == null) {
			return;
		}
		int operationMode = this.baseApplicationModel.getOperationMode();
		LANG_ENUM currentLocaleEnum = XMAdminUtil.getInstance().getCurrentLocaleEnum();
		String descForCurLocale = this.baseApplicationModel.getDescription(currentLocaleEnum) == null
				? CommonConstants.EMPTY_STR : this.baseApplicationModel.getDescription(currentLocaleEnum);
		if (operationMode != CommonConstants.OPERATIONMODE.VIEW) {
			this.txtDesc.setText(descForCurLocale);
			return;
		}
		if (operationMode == CommonConstants.OPERATIONMODE.VIEW) {
			if (!XMSystemUtil.isEmpty(descForCurLocale)) {
				this.txtDesc.setText(descForCurLocale);
				return;
			}
			final String descriptionEN = this.baseApplicationModel.getDescription(LANG_ENUM.ENGLISH);
			final String descriptionDE = this.baseApplicationModel.getDescription(LANG_ENUM.GERMAN);
			if (!XMSystemUtil.isEmpty(descriptionEN)) {
				this.txtDesc.setText(descriptionEN);
				return;
			}

			if (!XMSystemUtil.isEmpty(descriptionDE)) {
				this.txtDesc.setText(descriptionDE);
				return;
			}
		}
	}

	/**
	 * Method for Update name widget.
	 */
	/*public void updateNameWidget() {
		if (this.baseApplicationModel == null) {
			return;
		}
		int operationMode = this.baseApplicationModel.getOperationMode();
		final String nameEN = this.baseApplicationModel.getName(LANG_ENUM.ENGLISH);
		final String nameDE = this.baseApplicationModel.getName(LANG_ENUM.GERMAN);
		final LANG_ENUM currentLocaleEnum = XMAdminUtil.getInstance().getCurrentLocaleEnum();
		String nameForCurLocale = this.baseApplicationModel.getName(currentLocaleEnum) == null
				? CommonConstants.EMPTY_STR : this.baseApplicationModel.getName(currentLocaleEnum);

		if (operationMode != CommonConstants.OPERATIONMODE.VIEW) {
			this.txtName.setText(nameForCurLocale);
			if (currentLocaleEnum == LANG_ENUM.ENGLISH) {
				GridData layoutData = (GridData) this.txtNameGermanLang.getLayoutData();
				GridData layoutData2 = (GridData) this.lblNameGerman1.getLayoutData();
				GridData layoutData3 = (GridData) this.lblNameGerman2.getLayoutData();
				layoutData.exclude = true;
				layoutData2.exclude = true;
				layoutData3.exclude = true;
				this.txtNameGermanLang.setVisible(false);
				this.lblNameGerman1.setVisible(false);
				this.lblNameGerman2.setVisible(false);

				this.txtNameGermanLang.getParent().layout(false);
				this.lblNameGerman1.getParent().layout(false);
				this.lblNameGerman2.getParent().layout(false);
			}

			if (!XMSystemUtil.isEmpty(nameEN)) {
				this.txtNameGermanLang.setText(nameEN);
				return;
			} else if (!XMSystemUtil.isEmpty(nameDE)) {
				this.txtNameGermanLang.setText(nameDE);
				return;
			}
			return;
		}
		if (operationMode == CommonConstants.OPERATIONMODE.VIEW) {
			if (!XMSystemUtil.isEmpty(nameForCurLocale)) {
				this.txtName.setText(nameForCurLocale);
				return;
			}

			if (!XMSystemUtil.isEmpty(nameEN)) {
				this.txtName.setText(nameEN);
				return;
			}

			if (!XMSystemUtil.isEmpty(nameDE)) {
				this.txtName.setText(nameDE);
				return;
			}
		}

	}*/

	/**
	 * Method for Cancel handler.
	 */
	public void cancelHandler() {
		if (baseApplicationModel == null) {
			dirty.setDirty(false);
			return;
		}
		String baseAppId = CommonConstants.EMPTY_STR;
		int operationMode = this.baseApplicationModel.getOperationMode();
		BaseApplication oldModel = getOldModel();
		if (oldModel != null) {
			baseAppId = oldModel.getBaseApplicationId();
		}
		setBaseApplicationModel(null);
		setOldModel(null);
		this.saveBtn.setEnabled(true);
		final AdminTreeviewer adminTree = XMAdminUtil.getInstance().getAdminTree();
		AdminTreeFactory instance = AdminTreeFactory.getInstance();
		final BaseApplications baseApps = instance.getBaseApplications();
		dirty.setDirty(false);
		if (operationMode == CommonConstants.OPERATIONMODE.CHANGE) {
			final IStructuredSelection selection = (IStructuredSelection) adminTree.getSelection();
			final Object firstElement = selection.getFirstElement();
			if (firstElement != null && firstElement.equals(baseApps.getBaseApplications().get(baseAppId))) {
				adminTree.setSelection(new StructuredSelection(baseApps.getBaseApplications().get(baseAppId)), true);
			}
		} else {
			if (!adminTree.getExpandedState(instance.getApplications())) {
				adminTree.setSelection(new StructuredSelection(instance.getApplications()), true);
			} else {
				adminTree.setSelection(new StructuredSelection(baseApps), true);
			}

		}
	}

	/**
	 * Method for Sets the base application.
	 */
	public void setBaseApplication() {
		try {
			final AdminTreeviewer adminTree = XMAdminUtil.getInstance().getAdminTree();
			final Object selectionObj = adminTree.getSelection();
			if (selectionObj instanceof IStructuredSelection) {
				Object firstElement = ((IStructuredSelection) selectionObj).getFirstElement();
				if (firstElement instanceof BaseApplication) {
					setOldModel((BaseApplication) firstElement);
					BaseApplication rightHandObject = getOldModel().deepCopyBaseApplication(false, null);
					setBaseApplicationModel(rightHandObject);
					registerMessages(this.registry);
					bindValues();
					setOperationMode();
				}
			}
		} catch (Exception e) {
			LOGGER.warn("Unable to set baseApplication model selection ! " + e);
		}
	}

	/**
	 * Sets the model.
	 *
	 * @param baseApplication
	 *            the new model
	 */
	public void setModel(BaseApplication baseApplication) {
		try {
			setOldModel(null);
			setBaseApplicationModel(baseApplication);
			registerMessages(this.registry);
			bindValues();
			setOperationMode();
		} catch (Exception e) {
			LOGGER.warn("Unable to set baseApplication model ! " + e);
		}
	}
}
