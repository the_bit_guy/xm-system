package com.magna.xmsystem.xmadmin.ui.handlers;

import javax.inject.Inject;

import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.core.services.nls.Translation;
import org.eclipse.e4.ui.workbench.modeling.ESelectionService;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.TreeItem;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.client.ResourceAccessException;

import com.magna.xmbackend.vo.enums.NotificationEventType;
import com.magna.xmsystem.dependencies.customExceptions.UnauthorizedAccessException;
import com.magna.xmsystem.ui.controls.widgets.CustomMessageDialog;
import com.magna.xmsystem.xmadmin.message.Message;
import com.magna.xmsystem.xmadmin.restclient.notify.NotificationController;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.AdminTreeviewer;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.notification.INotificationEvent;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.notification.NotificationEvtActions;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.notification.ProjectActivateEvt;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.notification.ProjectActivateEvtAction;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.notification.ProjectCreateEvt;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.notification.ProjectCreateEvtAction;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.notification.ProjectDeactivateEvt;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.notification.ProjectDeactivateEvtAction;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.notification.ProjectDeleteEvt;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.notification.ProjectDeleteEvtAction;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.notification.UserProjectRelAssignEvt;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.notification.UserProjectRelAssignEvtAction;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.notification.UserProjectRelRemoveEvt;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.notification.UserProjectRelRemoveEvtAction;
import com.magna.xmsystem.xmadmin.util.MessageType;
import com.magna.xmsystem.xmadmin.util.XMAdminUtil;

public class NotifyActiveMenuHandler {
	
	/** The Constant LOGGER. */
	private static final Logger LOGGER = LoggerFactory.getLogger(NotifyActiveMenuHandler.class);

	/** Member variable 'selection service' for {@link ESelectionService}. */
	@Inject
	private ESelectionService selectionService;

	/** Member variable 'messages' for {@link Message}. */
	@Inject
	@Translation
	private Message messages;

	@Execute
	public void execute() {
		
		final Object selectionObj = selectionService.getSelection();
		if (selectionObj == null) {
			return;
		}
		
		try {
			//if (XMAdminUtil.getInstance().isUpdateStatusAllowed(selectionObj)) {
			if (selectionObj instanceof IStructuredSelection) {
				Object firstElement = ((IStructuredSelection) selectionObj).getFirstElement();
				if (firstElement instanceof ProjectCreateEvt) {
					String event = NotificationEventType.PROJECT_CREATE.name();
					activateNotifyEvent(firstElement, event);
				} else if (firstElement instanceof ProjectDeleteEvt) {
					String event = NotificationEventType.PROJECT_DELETE.name();
					activateNotifyEvent(firstElement, event);
				} else if (firstElement instanceof ProjectDeactivateEvt) {
					String event = NotificationEventType.PROJECT_DEACTIVATE.name();
					activateNotifyEvent(firstElement, event);
				} else if (firstElement instanceof ProjectActivateEvt) {
					String event = NotificationEventType.PROJECT_ACTIVATE.name();
					activateNotifyEvent(firstElement, event);
				} else if (firstElement instanceof ProjectCreateEvtAction) {
					String event = NotificationEventType.PROJECT_CREATE.name();
					activateNotifyAction(firstElement, event);
				} else if (firstElement instanceof ProjectDeleteEvtAction) {
					String event = NotificationEventType.PROJECT_DELETE.name();
					activateNotifyAction(firstElement, event);
				}  else if (firstElement instanceof ProjectDeactivateEvtAction) {
					String event = NotificationEventType.PROJECT_DEACTIVATE.name();
					activateNotifyAction(firstElement, event);
				} else if (firstElement instanceof ProjectActivateEvtAction) {
					String event = NotificationEventType.PROJECT_ACTIVATE.name();
					activateNotifyAction(firstElement, event);
				} else if (firstElement instanceof UserProjectRelAssignEvt) {
					String event = NotificationEventType.USER_PROJECT_RELATION_ASSIGN.name();
					activateNotifyEvent(firstElement, event);
				} else if (firstElement instanceof UserProjectRelAssignEvtAction) {
					String event = NotificationEventType.USER_PROJECT_RELATION_ASSIGN.name();
					activateNotifyAction(firstElement, event);
				} else if (firstElement instanceof UserProjectRelRemoveEvt) {
					String event = NotificationEventType.USER_PROJECT_RELATION_REMOVE.name();
					activateNotifyEvent(firstElement, event);
				} else if (firstElement instanceof UserProjectRelRemoveEvtAction) {
					String event = NotificationEventType.USER_PROJECT_RELATION_REMOVE.name();
					activateNotifyAction(firstElement, event);
				}
			}
			/*} else {
				CustomMessageDialog.openError(Display.getDefault().getActiveShell(), messages.objectPermissionDialogTitle,
						messages.objectPermissionDialogMsg);
			}*/
		} catch (UnauthorizedAccessException e) {
			LOGGER.error("Current user is Unauthorized " + e);
		} catch (Exception e) {
			if (e instanceof ResourceAccessException) {
				CustomMessageDialog.openError(Display.getCurrent().getActiveShell(), messages.errorDialogTitile,
						messages.serverNotReachable);
			}
			LOGGER.error("Exception occured in calling access allowed API " + e);
		}
	}

	private void activateNotifyAction(Object firstElement, String event) {
		try {
			String name = "";
			AdminTreeviewer adminTree = XMAdminUtil.getInstance().getAdminTree();
			TreeItem[] selection = adminTree.getTree().getSelection();
			if (selection != null && selection.length > 0) {
				name = name.concat(selection[0].getText());
			}
			
			String confirmDialogTitle = messages.changeStatusConfirmDialogTitle;
			String confirmDialogMsg = messages.deActivatechangeStatusConfirmDialogMsg + " \'" + name + "' "+messages.actionLbl+ " ?";

			boolean isConfirmed = CustomMessageDialog.openQuestion(adminTree.getControl().getShell(),
					confirmDialogTitle, confirmDialogMsg);
			NotificationEvtActions action = (NotificationEvtActions) firstElement;

			if (isConfirmed) {
				String id = action.getId();
				String status = com.magna.xmbackend.vo.enums.Status.ACTIVE.name();
				NotificationController notificationController = new NotificationController();
				boolean isUpdated = notificationController.updateNotifActionStatus(id, status);
				if (isUpdated) {
					action.setActive(true);
					adminTree.refresh(action);
					adminTree.setSelection(new StructuredSelection(action), true);
					
					XMAdminUtil.getInstance()
					.updateLogFile(messages.objectStatusUpdate + " " + event+" "+messages.actionLbl +" '"
							+ name + "' "+ messages.objectUpdate,
							MessageType.SUCCESS);
				}
				
			}

		} catch (UnauthorizedAccessException e) {
			CustomMessageDialog.openError(Display.getDefault().getActiveShell(), messages.objectPermissionDialogTitle,
					messages.objectPermissionDialogMsg);
		} catch (Exception e) {

			LOGGER.warn("Unable to update data! ", e);
		}
	}
	
	/**
	 * Activate notify event.
	 *
	 * @param firstElement the first element
	 * @param name the name
	 */
	private void activateNotifyEvent(Object firstElement, String name) {
		try {
			AdminTreeviewer adminTree = XMAdminUtil.getInstance().getAdminTree();
			String confirmDialogTitle = messages.changeStatusConfirmDialogTitle;
			String confirmDialogMsg = messages.deActivatechangeStatusConfirmDialogMsg + " \'" + name + "' "+messages.eventLbl+ " ?";

			boolean isConfirmed = CustomMessageDialog.openQuestion(adminTree.getControl().getShell(),
					confirmDialogTitle, confirmDialogMsg);
			INotificationEvent event = (INotificationEvent) firstElement;

			if (isConfirmed) {
				String status = com.magna.xmbackend.vo.enums.Status.ACTIVE.name();
				NotificationController notificationController = new NotificationController();
				boolean isUpdated = notificationController.updateNotificationStatus(name, status);
				if (isUpdated) {
					event.setActive(true);
					adminTree.refresh(event);
					adminTree.setSelection(new StructuredSelection(event), true);
					
					XMAdminUtil.getInstance()
					.updateLogFile(messages.objectStatusUpdate + " " + name + " "+messages.eventLbl + " '"
							+ name + "' "+ messages.objectUpdate,
							MessageType.SUCCESS);
				}
				
			}

		} catch (UnauthorizedAccessException e) {
			CustomMessageDialog.openError(Display.getDefault().getActiveShell(), messages.objectPermissionDialogTitle,
					messages.objectPermissionDialogMsg);
		} catch (Exception e) {

			LOGGER.warn("Unable to update data! ", e);
		}
	}

	/**
	 * Activate pro deactivate action.
	 *
	 * @param firstElement the first element
	 *//*
	private void activateProDeactivateAction(Object firstElement) {
		try {
			String name = "";
			AdminTreeviewer adminTree = XMAdminUtil.getInstance().getAdminTree();
			TreeItem[] selection = adminTree.getTree().getSelection();
			if (selection != null && selection.length > 0) {
				name = name.concat(selection[0].getText());
			}
			
			String confirmDialogTitle = messages.changeStatusConfirmDialogTitle;
			String confirmDialogMsg = messages.deActivatechangeStatusConfirmDialogMsg + " \'" + name + "' "+messages.actionLbl+ " ?";

			boolean isConfirmed = CustomMessageDialog.openQuestion(adminTree.getControl().getShell(),
					confirmDialogTitle, confirmDialogMsg);
			ProjectDeactivateEvtAction action = (ProjectDeactivateEvtAction) firstElement;

			if (isConfirmed) {
				String id = action.getId();
				String status = com.magna.xmbackend.vo.enums.Status.ACTIVE.name();
				NotificationController notificationController = new NotificationController();
				boolean isUpdated = notificationController.updateNotifActionStatus(id, status);
				if (isUpdated) {
					action.setActive(true);
					adminTree.refresh(action);
					adminTree.setSelection(new StructuredSelection(action), true);
					
					XMAdminUtil.getInstance()
					.updateLogFile(messages.objectStatusUpdate + " " +  NotificationEventType.PROJECT_DEACTIVATE.name()+" "+messages.actionLbl +" '"
							+ name + "' "+ messages.objectUpdate,
							MessageType.SUCCESS);
				}
				
			}

		} catch (UnauthorizedAccessException e) {
			CustomMessageDialog.openError(Display.getDefault().getActiveShell(), messages.objectPermissionDialogTitle,
					messages.objectPermissionDialogMsg);
		} catch (Exception e) {

			LOGGER.warn("Unable to update data! ", e);
		}
	}

	*//**
	 * Activate pro delete action.
	 *
	 * @param firstElement the first element
	 *//*
	private void activateProDeleteAction(Object firstElement) {
		try {
			String name = "";
			AdminTreeviewer adminTree = XMAdminUtil.getInstance().getAdminTree();
			TreeItem[] selection = adminTree.getTree().getSelection();
			if (selection != null && selection.length > 0) {
				name = name.concat(selection[0].getText());
			}
			
			String confirmDialogTitle = messages.changeStatusConfirmDialogTitle;
			String confirmDialogMsg = messages.deActivatechangeStatusConfirmDialogMsg + " \'" + name + "' "+messages.actionLbl+ " ?";

			boolean isConfirmed = CustomMessageDialog.openQuestion(adminTree.getControl().getShell(),
					confirmDialogTitle, confirmDialogMsg);
			ProjectDeleteEvtAction action = (ProjectDeleteEvtAction) firstElement;

			if (isConfirmed) {
				String id = action.getId();
				String status = com.magna.xmbackend.vo.enums.Status.ACTIVE.name();
				NotificationController notificationController = new NotificationController();
				boolean isUpdated = notificationController.updateNotifActionStatus(id, status);
				if (isUpdated) {
					action.setActive(true);
					adminTree.refresh(action);
					adminTree.setSelection(new StructuredSelection(action), true);
					
					XMAdminUtil.getInstance()
					.updateLogFile(messages.objectStatusUpdate + " " +  NotificationEventType.PROJECT_DELETE.name()+" "+messages.actionLbl +" '"
							+ name + "' "+ messages.objectUpdate,
							MessageType.SUCCESS);
				}
				
			}

		} catch (UnauthorizedAccessException e) {
			CustomMessageDialog.openError(Display.getDefault().getActiveShell(), messages.objectPermissionDialogTitle,
					messages.objectPermissionDialogMsg);
		} catch (Exception e) {

			LOGGER.warn("Unable to update data! ", e);
		}
	}

	*//**
	 * Activate pro create action.
	 *
	 * @param firstElement the first element
	 *//*
	private void activateProCreateAction(Object firstElement) {
		try {
			String name = "";
			AdminTreeviewer adminTree = XMAdminUtil.getInstance().getAdminTree();
			TreeItem[] selection = adminTree.getTree().getSelection();
			if (selection != null && selection.length > 0) {
				name = name.concat(selection[0].getText());
			}
			
			String confirmDialogTitle = messages.changeStatusConfirmDialogTitle;
			String confirmDialogMsg = messages.deActivatechangeStatusConfirmDialogMsg + " \'" + name + "' "+messages.actionLbl+ " ?";

			boolean isConfirmed = CustomMessageDialog.openQuestion(adminTree.getControl().getShell(),
					confirmDialogTitle, confirmDialogMsg);
			ProjectCreateEvtAction action = (ProjectCreateEvtAction) firstElement;

			if (isConfirmed) {
				String id = action.getId();
				String status = com.magna.xmbackend.vo.enums.Status.ACTIVE.name();
				NotificationController notificationController = new NotificationController();
				boolean isUpdated = notificationController.updateNotifActionStatus(id, status);
				if (isUpdated) {
					action.setActive(true);
					adminTree.refresh(action);
					adminTree.setSelection(new StructuredSelection(action), true);
					
					XMAdminUtil.getInstance()
					.updateLogFile(messages.objectStatusUpdate + " " +  NotificationEventType.PROJECT_CREATE.name()+" "+messages.actionLbl +" '"
							+ name + "' "+ messages.objectUpdate,
							MessageType.SUCCESS);
				}
				
			}

		} catch (UnauthorizedAccessException e) {
			CustomMessageDialog.openError(Display.getDefault().getActiveShell(), messages.objectPermissionDialogTitle,
					messages.objectPermissionDialogMsg);
		} catch (Exception e) {

			LOGGER.warn("Unable to update data! ", e);
		}
	}*/

	
}
