package com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.notification;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.stream.Collectors;

import com.magna.xmbackend.vo.enums.NotificationEventType;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild;

/**
 * The Class UserProjectRelRemoveEvt.
 * 
 * @author shashwat.anand
 */
public class UserProjectRelRemoveEvt extends INotificationEvent {
	
	/** The user pro rel remove evt child. */
	private Map<String, IAdminTreeChild> userProRelRemoveEvtChild;
	
	/** The parent. */
	private IAdminTreeChild parent;
	
	
	
	/**
	 * Instantiates a new user project rel remove evt.
	 */
	public UserProjectRelRemoveEvt() {
		this.parent = null;
		this.eventType = NotificationEventType.USER_PROJECT_RELATION_REMOVE;
		this.userProRelRemoveEvtChild = new LinkedHashMap<>();
	}
	
	
	/**
	 * Add.
	 *
	 * @param id the id
	 * @param child the child
	 * @return the i admin tree child
	 */
	public IAdminTreeChild add(final String id, final IAdminTreeChild child) {
		child.setParent(this);
		IAdminTreeChild returnVal = this.userProRelRemoveEvtChild.put(id, child);
		sort();
		return returnVal;
	}
	
	/**
	 * Remove all.
	 */
	public void removeAll() {
		this.userProRelRemoveEvtChild.clear();
	}

	/* (non-Javadoc)
	 * @see com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild#getParent()
	 */
	@Override
	public IAdminTreeChild getParent() {
		return this.parent;
	}

	/* (non-Javadoc)
	 * @see com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild#setParent(com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild)
	 */
	@Override
	public void setParent(IAdminTreeChild parent) {
		this.parent = parent;
	}
	

	
	/**
	 * Gets the user pro rel remove evt child.
	 *
	 * @return the user pro rel remove evt child
	 */
	public Map<String, IAdminTreeChild> getUserProRelRemoveEvtChild() {
		return userProRelRemoveEvtChild;
	}


	/**
	 * Gets the user pro rel remove evt collection.
	 *
	 * @return the user pro rel remove evt collection
	 */
	public Collection<IAdminTreeChild> getUserProRelRemoveEvtCollection() {
		return this.userProRelRemoveEvtChild.values();
	}
	
	/**
	 * Sort.
	 */
	public void sort() {
		final LinkedHashMap<String, IAdminTreeChild> collect = this.userProRelRemoveEvtChild.entrySet().stream().sorted(
				(e1, e2) -> ((UserProjectRelRemoveEvtAction) e1.getValue()).getName().compareTo(((UserProjectRelRemoveEvtAction) e2.getValue()).getName()))
				.collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e1, LinkedHashMap::new));
		this.userProRelRemoveEvtChild = collect;
	}
}
