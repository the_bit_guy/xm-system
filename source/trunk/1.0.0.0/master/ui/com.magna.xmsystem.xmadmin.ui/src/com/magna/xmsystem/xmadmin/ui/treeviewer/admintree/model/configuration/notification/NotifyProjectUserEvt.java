package com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.notification;

import java.beans.PropertyChangeEvent;

import com.magna.xmbackend.vo.enums.NotificationEventType;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.BeanModel;

/**
 * The Class NotifyProjectUserEvt.
 * 
 * @author shashwat.anand
 */
public class NotifyProjectUserEvt extends BeanModel {
	/** The event type. */
	protected NotificationEventType eventType;
	
	/** The id. */
	private String id;
	
	/** The active. */
	private boolean active;

	/**
	 * Instantiates a new notify project user evt.
	 */
	public NotifyProjectUserEvt() {
		this.eventType = NotificationEventType.EMAIL_NOTIFY_TO_PROJECT_USERS;
	}
	
	/**
	 * Gets the event type.
	 *
	 * @return the eventType
	 */
	public NotificationEventType getEventType() {
		return eventType;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	/**
	 * Checks if is active.
	 *
	 * @return true, if is active
	 */
	public boolean isActive() {
		return active;
	}

	/**
	 * Sets the active.
	 *
	 * @param active the new active
	 */
	public void setActive(boolean active) {
		this.active = active;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.BeanModel#
	 * propertyChange(java.beans.PropertyChangeEvent)
	 */
	@Override
	public void propertyChange(final PropertyChangeEvent event) {
		this.propertyChangeSupport.firePropertyChange(event.getPropertyName(), event.getOldValue(),
				event.getNewValue());
	}
}
