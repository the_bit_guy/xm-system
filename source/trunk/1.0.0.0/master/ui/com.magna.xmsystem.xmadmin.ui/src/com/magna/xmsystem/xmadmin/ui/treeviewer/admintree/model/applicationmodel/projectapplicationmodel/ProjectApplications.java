package com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.projectapplicationmodel;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.userapplicationmodel.UserApplication;

/**
 * Model class for ProjectApplications.
 *
 * @author Deepak upadhyay
 */
public class ProjectApplications implements IAdminTreeChild {
	
	/** The parent. */
	private IAdminTreeChild parent;
	
	/**
	 * List for storing {@link Site}
	 */
	private Map<String, IAdminTreeChild> projectAppChildren;

	/**
	 * Constructor
	 */
	public ProjectApplications() {
		this.projectAppChildren = new LinkedHashMap<>();
	}
	
	/**
	 * Adds the.
	 *
	 * @param projectAppId the project app id
	 * @param child the child
	 * @return the i admin tree child
	 */
	public IAdminTreeChild add(final String projectAppId, final IAdminTreeChild child) {
		child.setParent(this);
		final IAdminTreeChild returnVal = this.projectAppChildren.put(projectAppId, child);
		sort();
		return returnVal;
	}

	/**
	 * Removes the child
	 * 
	 * @param child
	 *            {@link IAdminTreeChild}
	 * @return {@link boolean}
	 */
	public IAdminTreeChild remove(final String id) {
		return this.projectAppChildren.remove(id);
	}
	
	/**
	 * Method for Removes the all.
	 */
	public void removeAll() {
		this.projectAppChildren.clear();
	}
	
	/**
	 * @return {@link List} of {@link UserApplication} 
	 */
	public Collection<IAdminTreeChild> getProjectAppCollection() {
		return this.projectAppChildren.values();
	}
	
	/**
	 * @return {@link List} of {@link Site}
	 */
	public Map<String, IAdminTreeChild> getProjectApplications() {
		return this.projectAppChildren;
	}
	/**
	 * Returns null as its parent
	 * @see com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild#getParent()
	 */
	@Override
	public IAdminTreeChild getParent() {
		return this.parent;
	}

	/**
	 * @see com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild#setParent(com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild)
	 */
	@Override
	public void setParent(final IAdminTreeChild parent) {
		this.parent = parent;
	}
	
	/**
	 * Sort.
	 */
	public void sort() {
		final LinkedHashMap<String, IAdminTreeChild> collect = this.projectAppChildren.entrySet().stream().sorted(
				(e1, e2) -> ((ProjectApplication) e1.getValue()).getName().compareTo(((ProjectApplication) e2.getValue()).getName()))
				.collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e1, LinkedHashMap::new));
		this.projectAppChildren = collect;
	}
}
