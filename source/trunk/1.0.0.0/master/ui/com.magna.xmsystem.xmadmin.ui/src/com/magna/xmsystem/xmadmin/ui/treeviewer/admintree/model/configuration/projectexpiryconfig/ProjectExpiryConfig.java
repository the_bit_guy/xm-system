package com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.projectexpiryconfig;

import java.beans.PropertyChangeEvent;

import com.magna.xmsystem.dependencies.utils.XMSystemUtil;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.BeanModel;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild;
import com.magna.xmsystem.xmadmin.util.CommonConstants;

/**
 * The Class ProjectExpiryConfig.
 */
public class ProjectExpiryConfig extends BeanModel implements IAdminTreeChild {

	/** The Constant PROPERTY_SINGLETONAPP_TIME_ID. */
	public static final String PROPERTY_PROJECT_EXPIRY_ID = "projectExpiryConfigId"; //$NON-NLS-1$

	/** The Constant PROPERTY_PROJECT_EXPIRY_DAYS. */
	public static final String PROPERTY_PROJECT_EXPIRY_DAYS = "projectExpiryDays"; //$NON-NLS-1$

	/** The Constant PROPERTY_OPERATIONMODE. */
	public static final String PROPERTY_OPERATIONMODE = "operationMode"; //$NON-NLS-1$
	
	/** The Constant PROPERTY_PROJECT_GRACE_DAYS. */
	public static final String PROPERTY_PROJECT_GRACE_DAYS = "projectGraceDays"; //$NON-NLS-1$

	/** The singleton app time config id. */
	private String projectExpiryConfigId;

	/** The singleton app time. */
	private String projectExpiryDays;
	
	/** The project grace days. */
	private String projectGraceDays;

	/** The operation mode. */
	private int operationMode;

	/**
	 * Instantiates a new singleton app time config.
	 *
	 * @param projectExpiryConfigId
	 *            the project expiry config id
	 * @param operationMode
	 *            the operation mode
	 */
	public ProjectExpiryConfig(final String projectExpiryConfigId, final int operationMode) {
		this(projectExpiryConfigId, null,null, operationMode);
	}

	
	/**
	 * Instantiates a new project expiry config.
	 *
	 * @param SingletonAppTimeId the singleton app time id
	 * @param projectExpiryDays the project expiry days
	 * @param projectGraceDays the project grace days
	 * @param operationMode the operation mode
	 */
	public ProjectExpiryConfig(final String SingletonAppTimeId, final String projectExpiryDays,final String projectGraceDays,
			final int operationMode) {
		super();
		this.projectExpiryConfigId = SingletonAppTimeId;
		this.projectExpiryDays = projectExpiryDays;
		this.projectGraceDays = projectGraceDays;
		this.operationMode = operationMode;
	}

	/**
	 * Gets the project expiry config id.
	 *
	 * @return the project expiry config id
	 */
	public String getProjectExpiryConfigId() {
		return projectExpiryConfigId;
	}

	/**
	 * Sets the project expiry config id.
	 *
	 * @param projectExpiryConfigId
	 *            the new project expiry config id
	 */
	public void setProjectExpiryConfigId(String projectExpiryConfigId) {
		// this.projectExpiryConfigId = projectExpiryConfigId;
		this.propertyChangeSupport.firePropertyChange(PROPERTY_PROJECT_EXPIRY_ID, this.projectExpiryConfigId,
				this.projectExpiryConfigId = projectExpiryConfigId);
	}

	/**
	 * Gets the project expiry days.
	 *
	 * @return the project expiry days
	 */
	public String getProjectExpiryDays() {
		return projectExpiryDays;
	}

	/**
	 * Sets the project expiry days.
	 *
	 * @param projectExpiryDays
	 *            the new project expiry days
	 */
	public void setProjectExpiryDays(String projectExpiryDays) {
		// this.projectExpiryDate = projectExpiryDate;
		if (projectExpiryDays == null) {
			throw new IllegalArgumentException();
		}
		this.propertyChangeSupport.firePropertyChange(PROPERTY_PROJECT_EXPIRY_DAYS, this.projectExpiryDays,
				this.projectExpiryDays = projectExpiryDays);
	}

	/**
	 * Gets the project grace days.
	 *
	 * @return the project grace days
	 */
	public String getProjectGraceDays() {
		return projectGraceDays;
	}


	/**
	 * Sets the project grace days.
	 *
	 * @param projectGraceDays the new project grace days
	 */
	public void setProjectGraceDays(String projectGraceDays) {
		if (projectGraceDays == null) {
			throw new IllegalArgumentException();
		}
		this.propertyChangeSupport.firePropertyChange(PROPERTY_PROJECT_GRACE_DAYS, this.projectGraceDays,
				this.projectGraceDays = projectGraceDays);		
		//this.projectGraceDays = projectGraceDays;
	}


	/**
	 * Gets the operation mode.
	 *
	 * @return the operation mode
	 */
	public int getOperationMode() {
		return operationMode;
	}

	/**
	 * Sets the operation mode.
	 *
	 * @param operationMode
	 *            the new operation mode
	 */
	public void setOperationMode(int operationMode) {
		this.propertyChangeSupport.firePropertyChange(PROPERTY_OPERATIONMODE, this.operationMode,
				this.operationMode = operationMode);
		// this.operationMode = operationMode;
	}

	/**
	 * Deep copy singleton app time config model.
	 *
	 * @param update
	 *            the update
	 * @param updateThisObject
	 *            the update this object
	 * @return the singleton app time config
	 */
	public ProjectExpiryConfig deepCopyProjectExpiryConfigModel(boolean update, ProjectExpiryConfig updateThisObject) {
		ProjectExpiryConfig clonedProjectExpiryConfig = null;
		try {
			String currentprojectExpiryConfigId = XMSystemUtil.isEmpty(this.getProjectExpiryConfigId())
					? CommonConstants.EMPTY_STR : this.getProjectExpiryConfigId();

			String currentProjectExpiryDate = XMSystemUtil.isEmpty(this.getProjectExpiryDays())
					? CommonConstants.EMPTY_STR : this.getProjectExpiryDays();
			
			String currentProjectGraceDays = XMSystemUtil.isEmpty(this.getProjectGraceDays())
					? CommonConstants.EMPTY_STR : this.getProjectGraceDays();

			if (update) {
				clonedProjectExpiryConfig = updateThisObject;
			} else {
				clonedProjectExpiryConfig = new ProjectExpiryConfig(currentprojectExpiryConfigId,
						currentProjectExpiryDate,currentProjectGraceDays, CommonConstants.OPERATIONMODE.VIEW);
			}
			clonedProjectExpiryConfig.setProjectExpiryConfigId(currentprojectExpiryConfigId);
			clonedProjectExpiryConfig.setProjectExpiryDays(currentProjectExpiryDate);
			clonedProjectExpiryConfig.setProjectGraceDays(currentProjectGraceDays);

			return clonedProjectExpiryConfig;
		} catch (Exception e) {
			return null;
		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.BeanModel#
	 * propertyChange(java.beans.PropertyChangeEvent)
	 */
	@Override
	public void propertyChange(PropertyChangeEvent event) {
		this.propertyChangeSupport.firePropertyChange(event.getPropertyName(), event.getOldValue(),
				event.getNewValue());
	}

}
