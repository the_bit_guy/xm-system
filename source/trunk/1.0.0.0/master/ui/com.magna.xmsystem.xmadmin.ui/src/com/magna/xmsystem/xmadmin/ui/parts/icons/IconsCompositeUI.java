package com.magna.xmsystem.xmadmin.ui.parts.icons;

import org.eclipse.jface.layout.GridDataFactory;
import org.eclipse.jface.layout.GridLayoutFactory;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.magna.xmsystem.ui.controls.util.FilteredTreeControl;
import com.magna.xmsystem.ui.controls.util.PatternFilterTree;

/**
 * Class for UI elements Icons Part UI
 * 
 * @author shashwat.anand
 *
 */
public class IconsCompositeUI extends Composite {
	/**
	 * Logger instance
	 */
	private static final Logger LOGGER = LoggerFactory.getLogger(IconsCompositeUI.class);
	/**
	 * Member variable for group
	 */
	protected Group grpIcons;

	/**
	 * Member variable for {@link IconTreeviewer}
	 */
	protected IconTreeviewer iconTreeviewer;
	/**
	 * Member variable for {@link Button} add icon
	 */
	protected Button btnAddIcon;
	/**
	 * Member variable for {@link Button} remove icons
	 */
	protected Button btnRemoveIcon;

	/**
	 * boolean member variable
	 */
	private boolean isWizard;

	/**
	 * Constructor
	 * 
	 * @param parent
	 *            {@link Composite}
	 * @param style
	 *            int
	 * @param isWizard
	 *            boolean
	 */
	public IconsCompositeUI(final Composite parent, final int style, boolean isWizard) {
		super(parent, style);
		this.isWizard = isWizard;
		initGUI();
	}

	/**
	 * Make the GUI for components of the Site Composite
	 */
	private void initGUI() {
		try {
			GridLayoutFactory.fillDefaults().applyTo(this);
			this.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));

			this.grpIcons = new Group(this, SWT.NONE);
			GridLayoutFactory.fillDefaults().numColumns(1).applyTo(this.grpIcons);
			GridDataFactory.fillDefaults().grab(true, true).span(SWT.FILL, SWT.FILL).applyTo(this.grpIcons);

			final Composite widgetContainer = new Composite(this.grpIcons, SWT.NONE);
			final GridLayout widgetContainerLayout = new GridLayout(1, false);
			widgetContainer.setLayout(widgetContainerLayout);
			widgetContainer.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));

			createTableViewer(widgetContainer);
			if (!this.isWizard) {
				createActionComposite(widgetContainer);
			}
		} catch (Exception ex) {
			LOGGER.error("Unable to crete UI elements", ex); //$NON-NLS-1$
		}
	}

	/**
	 * Create action bar
	 * 
	 * @param widgetContainer
	 */
	private void createActionComposite(final Composite widgetContainer) {
		final Composite container = new Composite(widgetContainer, SWT.NONE);
		final GridLayout containerLayout = new GridLayout(2, false);
		container.setLayout(containerLayout);
		GridData layoutData = new GridData(SWT.FILL, SWT.FILL, true, true);
		layoutData.minimumHeight = 50;
		container.setLayoutData(layoutData);

		this.btnAddIcon = new Button(container, SWT.PUSH);
		this.btnAddIcon.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));
		this.btnRemoveIcon = new Button(container, SWT.PUSH);
		this.btnRemoveIcon.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));
	}

	/**
	 * Create table viewer for icons
	 * 
	 * @param widgetContainer
	 *            {@link Composite}
	 */
	private void createTableViewer(final Composite widgetContainer) {
		FilteredTreeControl iconControl = new FilteredTreeControl(widgetContainer, SWT.NONE, new PatternFilterTree()) {
			/**
			 * Overrides doCreateTreeViewer method
			 */
			@Override
			protected TreeViewer doCreateTreeViewer(final Composite parentL, final int style) {
				IconsCompositeUI.this.iconTreeviewer = new IconTreeviewer(parentL);
				return IconsCompositeUI.this.iconTreeviewer;
			}
		};
		GridData layoutData = new GridData(SWT.FILL, SWT.FILL, true, true);
		layoutData.minimumHeight = 50;
		iconControl.setLayoutData(layoutData);
	}

	/**
	 * @return the iconTreeviewer
	 */
	public IconTreeviewer getIconTreeviewer() {
		return iconTreeviewer;
	}
}
