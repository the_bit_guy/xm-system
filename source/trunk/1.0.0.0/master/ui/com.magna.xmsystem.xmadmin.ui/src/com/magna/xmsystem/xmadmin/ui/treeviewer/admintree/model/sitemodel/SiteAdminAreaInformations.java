package com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.sitemodel;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;

import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild;

/**
 * The Class SiteAdminAreaInformations.
 */
public class SiteAdminAreaInformations implements IAdminTreeChild {
	/** The parent. */
	private IAdminTreeChild parent;
	
	/** The site admin information child. */
	final private Map<String, IAdminTreeChild> siteAdminInformationChild;

	/**
	 * Instantiates a new site admin area informations.
	 *
	 * @param parent the parent
	 */
	public SiteAdminAreaInformations(final IAdminTreeChild parent) {
		this.parent = parent;
		this.siteAdminInformationChild = new LinkedHashMap<>();
		this.siteAdminInformationChild.put(SiteAdminAreaInformationStatus.class.getName(), new SiteAdminAreaInformationStatus(this));
		this.siteAdminInformationChild.put(SiteAdminAreaInformationHistory.class.getName(), new SiteAdminAreaInformationHistory(this));
	}

	/**
	 * Gets the site admin info collection.
	 *
	 * @return the site admin info collection
	 */
	public Collection<IAdminTreeChild> getSiteAdminInfoCollection() {
		return this.siteAdminInformationChild.values();
	}

	/**
	 * Gets the site admin info children.
	 *
	 * @return the site admin info children
	 */
	public Map<String, IAdminTreeChild> getSiteAdminInfoChildren() {
		return siteAdminInformationChild;
	}
	
	/* (non-Javadoc)
	 * @see com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild#getParent()
	 */
	/*
	 * @see com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild#getParent()
	 */
	@Override
	public IAdminTreeChild getParent() {
		return this.parent;
	}

	/* (non-Javadoc)
	 * @see com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild#setParent(com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild)
	 */
	/*
	 * @see com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild#
	 * setParent(com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild)
	 */
	@Override
	public void setParent(IAdminTreeChild parent) {
		this.parent = parent;
	}
}
