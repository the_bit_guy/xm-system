package com.magna.xmsystem.xmadmin.ui.parts.objexpcontextmenu;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.e4.core.services.nls.Translation;
import org.eclipse.e4.ui.model.application.MApplication;
import org.eclipse.e4.ui.model.application.ui.basic.MPart;
import org.eclipse.e4.ui.workbench.modeling.EModelService;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.swt.widgets.Display;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.client.ResourceAccessException;

import com.magna.xmbackend.exception.CannotCreateObjectException;
import com.magna.xmbackend.response.rel.adminareaproject.AdminAreaProjectResponseWrapper;
import com.magna.xmbackend.vo.rel.AdminAreaProjectAppRelRequest;
import com.magna.xmbackend.vo.rel.AdminAreaProjectAppRelResponse;
import com.magna.xmbackend.vo.rel.AdminAreaProjectRelRequest;
import com.magna.xmbackend.vo.rel.AdminAreaStartAppRelRequest;
import com.magna.xmbackend.vo.rel.AdminAreaStartAppRelResponse;
import com.magna.xmbackend.vo.rel.AdminAreaUserAppRelBatchResponse;
import com.magna.xmbackend.vo.rel.AdminAreaUserAppRelRequest;
import com.magna.xmbackend.vo.rel.ProjectStartAppRelBatchResponse;
import com.magna.xmbackend.vo.rel.ProjectStartAppRelRequest;
import com.magna.xmbackend.vo.rel.SiteAdminAreaRelBatchResponse;
import com.magna.xmbackend.vo.rel.SiteAdminAreaRelRequest;
import com.magna.xmbackend.vo.rel.UserProjectRelBatchResponse;
import com.magna.xmbackend.vo.rel.UserProjectRelRequest;
import com.magna.xmbackend.vo.rel.UserStartAppRelBatchResponse;
import com.magna.xmbackend.vo.rel.UserStartAppRelRequest;
import com.magna.xmsystem.dependencies.customExceptions.UnauthorizedAccessException;
import com.magna.xmsystem.ui.controls.widgets.CustomMessageDialog;
import com.magna.xmsystem.xmadmin.message.Message;
import com.magna.xmsystem.xmadmin.restclient.relation.AdminAreaProjectAppRelController;
import com.magna.xmsystem.xmadmin.restclient.relation.AdminAreaProjectRelController;
import com.magna.xmsystem.xmadmin.restclient.relation.AdminAreaStartAppRelController;
import com.magna.xmsystem.xmadmin.restclient.relation.AdminAreaUserAppRelController;
import com.magna.xmsystem.xmadmin.restclient.relation.ProjectStartAppRelController;
import com.magna.xmsystem.xmadmin.restclient.relation.SiteAdminAreaRelController;
import com.magna.xmsystem.xmadmin.restclient.relation.UserProjectRelController;
import com.magna.xmsystem.xmadmin.restclient.relation.UserStartAppRelController;
import com.magna.xmsystem.xmadmin.ui.parts.InformationPart;
import com.magna.xmsystem.xmadmin.ui.parts.objexp.ObjectExpPage;
import com.magna.xmsystem.xmadmin.ui.parts.objexp.ObjectExplorer;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.AdminTreeviewer;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.adminarea.AdminAreaProjectAppFixed;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.adminarea.AdminAreaProjectAppNotFixed;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.adminarea.AdminAreaProjectAppProtected;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.adminarea.AdminAreaProjectChild;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.adminarea.AdminAreaProjectStartApplications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.adminarea.AdminAreaProjects;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.adminarea.AdminAreaStartApplications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.adminarea.AdminAreaUserAppFixed;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.adminarea.AdminAreaUserAppNotFixed;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.adminarea.AdminAreaUserAppProtected;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.adminarea.AdministrationArea;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.projectapplicationmodel.ProjectApplication;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.startapplicationmodel.StartApplication;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.userapplicationmodel.UserApplication;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.projects.Project;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.projects.ProjectAdminAreaChild;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.projects.ProjectAdminAreaProjectAppFixed;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.projects.ProjectAdminAreaProjectAppNotFixed;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.projects.ProjectAdminAreaProjectAppProtected;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.projects.ProjectAdminAreaStartApplications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.projects.ProjectAdminAreas;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.projects.ProjectUserChild;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.projects.ProjectUsers;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.refs.RelationObj;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.sitemodel.Site;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.sitemodel.SiteAdminAreaProjectChild;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.sitemodel.SiteAdminAreaProjectStartApplications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.sitemodel.SiteAdminAreaProjects;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.sitemodel.SiteAdminAreaStartApplications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.sitemodel.SiteAdminAreaUserAppFixed;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.sitemodel.SiteAdminAreaUserAppNotFixed;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.sitemodel.SiteAdminAreaUserAppProtected;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.sitemodel.SiteAdminProjectAppFixed;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.sitemodel.SiteAdminProjectAppNotFixed;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.sitemodel.SiteAdminProjectAppProtected;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.sitemodel.SiteAdministrationChild;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.users.User;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.users.UserProjectChild;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.users.UserProjects;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.users.UserStartApplications;
import com.magna.xmsystem.xmadmin.util.CommonConstants;
import com.magna.xmsystem.xmadmin.util.JobSchedulingRule;
import com.magna.xmsystem.xmadmin.util.MessageType;
import com.magna.xmsystem.xmadmin.util.XMAdminUtil;

/**
 * The Class DeActivateRelAction.
 */
public class DeActivateRelAction extends Action {

	/** The Constant LOGGER. */
	private static final Logger LOGGER = LoggerFactory.getLogger(ActivateRelAction.class);

	/** The messages. */
	@Inject
	@Translation
	private Message messages;

	/** Inject of {@link EModelService}. */
	@Inject
	private EModelService modelService;

	/** Inject of {@link MApplication}. */
	@Inject
	private MApplication application;

	/** The job scheduling rule. */
	private JobSchedulingRule jobSchedulingRule;

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jface.action.Action#run()
	 */
	@Override
	public void run() {
		final MPart mPart = (MPart) this.modelService.find(CommonConstants.PART_ID.INFORMATION_PART_ID, application);
		final InformationPart rightSidePart2 = (InformationPart) mPart.getObject();
		final ObjectExpPage objectExpPage = rightSidePart2.getObjectExpPartUI();
		final ObjectExplorer objExpTableViewer = objectExpPage.getObjExpTableViewer();
		IStructuredSelection selections = (IStructuredSelection) objExpTableViewer.getSelection();
		Object selectionObj = selections.getFirstElement();
		if(selectionObj == null){
		    selections = (IStructuredSelection) XMAdminUtil.getInstance().getAdminTree().getSelection();
			selectionObj = selections.getFirstElement();
		}
		final RelationObj relationObj = (RelationObj) selectionObj;
		final IAdminTreeChild containerObj = relationObj.getContainerObj();
		this.jobSchedulingRule = new JobSchedulingRule();
		if (containerObj instanceof SiteAdministrationChild) {
			deActivateSiteAARel(selections);
		} else if (containerObj instanceof SiteAdminAreaProjectChild || containerObj instanceof AdminAreaProjectChild) {
			deActivateSiteAAProjectRel(selections);
		} else if (containerObj instanceof SiteAdminAreaUserAppNotFixed
				|| containerObj instanceof SiteAdminAreaUserAppFixed
				|| containerObj instanceof AdminAreaUserAppProtected || containerObj instanceof AdminAreaUserAppNotFixed
				|| containerObj instanceof AdminAreaUserAppFixed
				|| containerObj instanceof SiteAdminAreaUserAppProtected) {
			deActivateSiteAAUserAppRel(selections);
		} else if (containerObj instanceof SiteAdminProjectAppNotFixed
				|| containerObj instanceof SiteAdminProjectAppFixed
				|| containerObj instanceof SiteAdminProjectAppProtected
				|| containerObj instanceof AdminAreaProjectAppNotFixed
				|| containerObj instanceof AdminAreaProjectAppFixed
				|| containerObj instanceof AdminAreaProjectAppProtected
				|| containerObj instanceof ProjectAdminAreaProjectAppNotFixed
				|| containerObj instanceof ProjectAdminAreaProjectAppFixed
				|| containerObj instanceof ProjectAdminAreaProjectAppProtected) {
			deActivateSiteAAProjectAppRel(selections);
		} else if (containerObj instanceof SiteAdminAreaStartApplications
				|| containerObj instanceof AdminAreaStartApplications) {
			deActivateSiteAAStartAppRel(selections);
		} else if (containerObj instanceof SiteAdminAreaProjectStartApplications
				|| containerObj instanceof AdminAreaProjectStartApplications
				|| containerObj instanceof ProjectAdminAreaStartApplications) {
			deActivateSiteAAProStartAppRel(selections);
		} else if (containerObj instanceof ProjectUserChild) {
			deActivateProjectUserRel(selections);
		} else if (containerObj instanceof UserProjectChild) {
			deActivateUserProjectRel(selections);
		} else if (containerObj instanceof UserStartApplications) {
			deActivateUserStartAppRel(selections);
		} else if (containerObj instanceof ProjectAdminAreaChild) {
			deActivateProjectAARel(selections);
		}
		Job ruleJob = new Job("refresh..") {

			@Override
			protected IStatus run(IProgressMonitor monitor) {
				Display.getDefault().asyncExec(new Runnable() {
					@Override
					public void run() {
						objExpTableViewer.refresh(true);
					}
				});

				return Status.OK_STATUS;
			}
		};
		ruleJob.setRule(this.jobSchedulingRule);
		ruleJob.schedule();
	}

	
	/**
	 * De activate project AA rel.
	 *
	 * @param selections the selections
	 */
	@SuppressWarnings("unchecked")
	private void deActivateProjectAARel(IStructuredSelection selections) {
		Object selectionObj = selections.getFirstElement();
		RelationObj relationObj = (RelationObj) selectionObj;
		final IAdminTreeChild containerObj = relationObj.getContainerObj();
		try {
			if (!XMAdminUtil.getInstance().isRelObjAccessAllowed(containerObj,
					"PROJECT_TO_ADMINISTRATION_AREA-ACTIVATE_DEACTIVATE")) {
				CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
						messages.objectPermissionDialogTitle, messages.objectPermissionDialogMsg);
				return;
			}
		} catch (Exception e) {
			if (e instanceof ResourceAccessException) {
				CustomMessageDialog.openError(Display.getCurrent().getActiveShell(), messages.errorDialogTitile,
						messages.serverNotReachable);
			}
			LOGGER.error("Exception occured in calling access allowed API " + e);
			return;
		}
		AdminTreeviewer adminTree = XMAdminUtil.getInstance().getAdminTree();
		AdministrationArea adminArea = (AdministrationArea) relationObj.getRefObject();
		String adminAreaProjectRelname = adminArea.getName();
		String projectName = ((Project)relationObj.getParent().getParent()).getName();
		String confirmDialogTitle = messages.changeStatusConfirmDialogTitle;
		String confirmDialogMsg;
		if (selections.size() == 1) {
			confirmDialogMsg = messages.deActivateRelchangeStatusConfirmDialogMsg + " \'" + adminAreaProjectRelname
					+ "\'" + " ?";
		} else {
			confirmDialogMsg = messages.deActivateMultiRelchangeStatusConfirmDialogMsg + " ?";
		}
		boolean isConfirmed = CustomMessageDialog.openQuestion(adminTree.getControl().getShell(), confirmDialogTitle,
				confirmDialogMsg);

		if (isConfirmed) {
			final List<Object> selectionList = selections.toList();
			Job activateSiteAAProjectRelJob = new Job("De-Activating..") {
				@Override
				protected IStatus run(final IProgressMonitor monitor) {
					monitor.beginTask("De-Activating..", 100);
					monitor.worked(30);
					try {
						List<String> objectNames = new ArrayList<>();
						for (int i = 0; i < selectionList.size(); i++) {
							RelationObj relObject = (RelationObj) selectionList.get(i);
							objectNames.add(((AdministrationArea) relObject.getRefObject()).getName());
							String relId = relObject.getRelId();
							String status = com.magna.xmbackend.vo.enums.Status.INACTIVE.name();
							AdminAreaProjectRelController adminAreaProjectRelController = new AdminAreaProjectRelController(
									XMAdminUtil.getInstance().getAAForHeaders(relObject));
							boolean isUpdated = adminAreaProjectRelController.updateAdminAreaProjectRelStatus(relId,
									status);
							if (isUpdated) {
								Display.getDefault().asyncExec(new Runnable() {
									@Override
									public void run() {
										relObject.setActive(false);
										// adminTree.refresh(relObject);
										adminTree.refershBackReference(
												new Class[] { AdminAreaProjects.class, SiteAdminAreaProjects.class });
									}
								});
							}
						}
						Display.getDefault().asyncExec(new Runnable() {
							@Override
							public void run() {
								adminTree.refresh(true);
							}
						});
						for (Object name : objectNames) {
							XMAdminUtil.getInstance()
									.updateLogFile(messages.relationLbl + " " + messages.objectStatusUpdate + " "
											+ messages.administrationAreaObject + " '" + name + "' "
											+ messages.withLbl + " " + messages.projectObject + " '" + projectName
											+ "' " + messages.objectUpdate, MessageType.SUCCESS);
						}
					} catch (UnauthorizedAccessException e) {
						Display.getDefault().asyncExec(new Runnable() {

							@Override
							public void run() {
								CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
										messages.objectPermissionDialogTitle, messages.objectPermissionDialogMsg);
							}
						});
					} catch (Exception e) {
						LOGGER.warn("Unable to De-Activate project admin area  relation model! ", e);
					}
					monitor.worked(70);
					monitor.done();
					return Status.OK_STATUS;
				}
			};
			activateSiteAAProjectRelJob.setUser(true);
			activateSiteAAProjectRelJob.setRule(this.jobSchedulingRule);
			activateSiteAAProjectRelJob.schedule();
		}

	}
	
	/**
	 * De activate user start app rel.
	 *
	 * @param selections
	 *            the selections
	 */
	@SuppressWarnings("unchecked")
	private void deActivateUserStartAppRel(final IStructuredSelection selections) {
		Object selectionObj = selections.getFirstElement();
		RelationObj relationObj = (RelationObj) selectionObj;
		final IAdminTreeChild containerObj = relationObj.getContainerObj();
		try {
			if (!XMAdminUtil.getInstance().isRelObjAccessAllowed(containerObj,
					"START_APPLICATION_TO_USER-ACTIVATE_DEACTIVATE")) {
				CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
						messages.objectPermissionDialogTitle, messages.objectPermissionDialogMsg);
				return;
			}
		} catch (Exception e) {
			if (e instanceof ResourceAccessException) {
				CustomMessageDialog.openError(Display.getCurrent().getActiveShell(), messages.errorDialogTitile,
						messages.serverNotReachable);
			}
			LOGGER.error("Exception occured in calling access allowed API " + e);
			return;
		}
		AdminTreeviewer adminTree = XMAdminUtil.getInstance().getAdminTree();
		StartApplication startApplication = (StartApplication) relationObj.getRefObject();
		String startAppRelname = startApplication.getName();
		IStructuredSelection selection = (IStructuredSelection) adminTree.getSelection();
		String userName = ((User)((IAdminTreeChild)selection.getFirstElement()).getParent()).getName();
		String confirmDialogTitle = messages.changeStatusConfirmDialogTitle;
		String confirmDialogMsg;
		if (selections.size() == 1) {
			confirmDialogMsg = messages.deActivateRelchangeStatusConfirmDialogMsg + " \'" + startAppRelname + "\'"
					+ " ?";
		} else {
			confirmDialogMsg = messages.deActivateMultiRelchangeStatusConfirmDialogMsg + " ?";
		}
		boolean isConfirmed = CustomMessageDialog.openQuestion(adminTree.getControl().getShell(), confirmDialogTitle,
				confirmDialogMsg);
		if (isConfirmed) {
			final List<Object> selectionList = selections.toList();

			Job deActivateUserStartAppJob = new Job("De-Activating..") {

				@Override
				protected IStatus run(final IProgressMonitor monitor) {
					monitor.beginTask("De-Activating..", 100);
					monitor.worked(30);
					try {
						List<UserStartAppRelRequest> userStartAppRelRequestList = new ArrayList<>();
						Map<String, RelationObj> relObjMap = new HashMap<>();
						List<String> objectNames = new ArrayList<>();
						for (int i = 0; i < selectionList.size(); i++) {
							UserStartAppRelRequest userStartAppRelRequest = new UserStartAppRelRequest();
							RelationObj relObject = (RelationObj) selectionList.get(i);
							userStartAppRelRequest.setId(relObject.getRelId());
							userStartAppRelRequest.setStatus(com.magna.xmbackend.vo.enums.Status.INACTIVE.name());
							userStartAppRelRequestList.add(userStartAppRelRequest);
							relObjMap.put(relObject.getRelId(), relObject);
							objectNames.add(((StartApplication) relObject.getRefObject()).getName());
						}
						UserStartAppRelController userStartAppRelController = new UserStartAppRelController();
						UserStartAppRelBatchResponse response = userStartAppRelController
								.multiUpdateUserStartAppRelStatus(userStartAppRelRequestList);
						if (response != null) {
							List<String> statusUpdatationFailedList = response.getStatusUpdatationFailedList();
							if (!statusUpdatationFailedList.isEmpty()) {
								for (String id : statusUpdatationFailedList) {
									Iterator<Map.Entry<String, RelationObj>> iter = relObjMap.entrySet().iterator();
									while (iter.hasNext()) {
										Map.Entry<String, RelationObj> entry = iter.next();
										if (id.equalsIgnoreCase(entry.getKey())) {
											iter.remove();
										}
									}
								}
								Display.getDefault().asyncExec(new Runnable() {
									public void run() {
										CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
												messages.changeStatusConfirmDialogTitle,
												messages.multiStatusUpadteObjectDilaogErrMsg);
									}
								});
							}
							Iterator<Map.Entry<String, RelationObj>> iter = relObjMap.entrySet().iterator();
							while (iter.hasNext()) {
								Map.Entry<String, RelationObj> entry = iter.next();
								RelationObj obj = entry.getValue();
								obj.setActive(false);
							}

							Display.getDefault().asyncExec(new Runnable() {
								@Override
								public void run() {
									adminTree.refresh(true);
								}
							});
							for (Object name : objectNames) {
								XMAdminUtil.getInstance()
										.updateLogFile(messages.relationLbl + " " + messages.objectStatusUpdate + " "
												+ messages.startApplicationObject + " '" + name + "' "
												+ messages.withLbl + " " + messages.userObject + " '" + userName + "' "
												+ messages.objectUpdate, MessageType.SUCCESS);
							}
						}
					} catch (UnauthorizedAccessException e) {
						Display.getDefault().asyncExec(new Runnable() {

							@Override
							public void run() {
								CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
										messages.objectPermissionDialogTitle, messages.objectPermissionDialogMsg);
							}
						});
					} catch (Exception e) {
						LOGGER.warn("Unable to De-Activate user start application relation model! ", e);
					}
					monitor.worked(70);
					monitor.done();
					return Status.OK_STATUS;
				}
			};
			deActivateUserStartAppJob.setUser(true);
			deActivateUserStartAppJob.setRule(this.jobSchedulingRule);
			deActivateUserStartAppJob.schedule();
		}
	}

	/**
	 * De activate user project rel.
	 *
	 * @param selections
	 *            the selections
	 */
	@SuppressWarnings("unchecked")
	private void deActivateUserProjectRel(final IStructuredSelection selections) {
		Object selectionObj = selections.getFirstElement();
		RelationObj relationObj = (RelationObj) selectionObj;
		final IAdminTreeChild containerObj = relationObj.getContainerObj();
		try {
			if (!XMAdminUtil.getInstance().isRelObjAccessAllowed(containerObj,
					"USER_TO_PROJECT-ACTIVATE_DEACTIVATE")) {
				CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
						messages.objectPermissionDialogTitle, messages.objectPermissionDialogMsg);
				return;
			}
		} catch (Exception e) {
			if (e instanceof ResourceAccessException) {
				CustomMessageDialog.openError(Display.getCurrent().getActiveShell(), messages.errorDialogTitile,
						messages.serverNotReachable);
			}
			LOGGER.error("Exception occured in calling access allowed API " + e);
			return;
		}
		AdminTreeviewer adminTree = XMAdminUtil.getInstance().getAdminTree();
		Project project = (Project) relationObj.getRefObject();
		String projectUserRelName = project.getName();
		String userName = ((User)relationObj.getParent().getParent()).getName();
		String confirmDialogTitle = messages.changeStatusConfirmDialogTitle;
		String confirmDialogMsg;
		if (selections.size() == 1) {
			confirmDialogMsg = messages.deActivateRelchangeStatusConfirmDialogMsg + " \'" + projectUserRelName + "\'"
					+ " ?";
		} else {
			confirmDialogMsg = messages.deActivateMultiRelchangeStatusConfirmDialogMsg + " ?";
		}
		boolean isConfirmed = CustomMessageDialog.openQuestion(adminTree.getControl().getShell(), confirmDialogTitle,
				confirmDialogMsg);
		if (isConfirmed) {
			final List<Object> selectionList = selections.toList();

			Job deActivateUserProjectJob = new Job("De-Activating..") {

				@Override
				protected IStatus run(final IProgressMonitor monitor) {
					monitor.beginTask("De-Activating..", 100);
					monitor.worked(30);
					try {
						List<UserProjectRelRequest> userProjectRelRequestList = new ArrayList<>();
						Map<String, RelationObj> relObjMap = new HashMap<>();
						List<String> objectNames = new ArrayList<>();
						//List<String> failedObjectNames = new ArrayList<>();
						for (int i = 0; i < selectionList.size(); i++) {
							UserProjectRelRequest userProjectRelRequest = new UserProjectRelRequest();
							RelationObj relObject = (RelationObj) selectionList.get(i);
							userProjectRelRequest.setId(relObject.getRelId());
							userProjectRelRequest.setStatus(com.magna.xmbackend.vo.enums.Status.INACTIVE.name());
							userProjectRelRequestList.add(userProjectRelRequest);
							relObjMap.put(relObject.getRelId(), relObject);
							objectNames.add(((Project) relObject.getRefObject()).getName());
						}
						UserProjectRelController userProjectRelController = new UserProjectRelController();
						UserProjectRelBatchResponse response = userProjectRelController
								.multiUpdateUserProjectRelStatus(userProjectRelRequestList);
						//TODO
						if (response != null) {
							List<Map<String, String>> statusMaps = response.getStatusMap();
							if (!statusMaps.isEmpty()) {
								Display.getDefault().asyncExec(new Runnable() {
									public void run() {
										String errorMessage = "";
										for (Map<String, String> statusMap : statusMaps) {
											errorMessage += statusMap.get(
													XMAdminUtil.getInstance().getCurrentLocaleEnum().getLangCode());
											errorMessage += "\n";
										}
										if (errorMessage.length() > 0) {
											CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
													messages.dndErrorTitle, errorMessage);
										}
									}
								});
							}
							
							
							List<String> statusUpdatationFailedList = response.getStatusUpdatationFailedList();
							if (!statusUpdatationFailedList.isEmpty()) {
								for (String id : statusUpdatationFailedList) {
									Iterator<Map.Entry<String, RelationObj>> iter = relObjMap.entrySet().iterator();
									while (iter.hasNext()) {
										Map.Entry<String, RelationObj> entry = iter.next();
										if (id.equalsIgnoreCase(entry.getKey())) {
											iter.remove();
										}
									}
								}
								Display.getDefault().asyncExec(new Runnable() {
									public void run() {
										CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
												messages.changeStatusConfirmDialogTitle,
												messages.multiStatusUpadteObjectDilaogErrMsg);
									}
								});
							}
							List<String> updatedRelIds = response.getStatusUpdatationSuccessList();
							Iterator<Map.Entry<String, RelationObj>> iter = relObjMap.entrySet().iterator();
							while (iter.hasNext()) {
								Map.Entry<String, RelationObj> entry = iter.next();
								RelationObj obj = entry.getValue();
								if(!updatedRelIds.isEmpty() && updatedRelIds.contains(obj.getRelId())) {
									obj.setActive(false);
								} else{
									objectNames.remove(((Project) obj.getRefObject()).getName());
									//failedObjectNames.add(((Project) obj.getRefObject()).getName());
								}
							}

							Display.getDefault().asyncExec(new Runnable() {
								@Override
								public void run() {
									adminTree.refresh(true);
									adminTree.refershBackReference(new Class[] { ProjectUsers.class });
								}
							});
							for (Object name : objectNames) {
								XMAdminUtil.getInstance()
										.updateLogFile(messages.relationLbl + " " + messages.objectStatusUpdate + " "
												+ messages.projectObject + " '" + name + "' "
												+ messages.withLbl + " " + messages.userObject + " '" + userName + "' "
												+ messages.objectUpdate, MessageType.SUCCESS);
							}
							Display.getDefault().asyncExec(new Runnable() {
								public void run() {
									for (Map<String, String> statusMap : statusMaps) {
										String errorMessage = statusMap.get(
												XMAdminUtil.getInstance().getCurrentLocaleEnum().getLangCode());
										XMAdminUtil.getInstance().updateLogFile(
												errorMessage, MessageType.FAILURE);
									}
								}
							});
							/*for (Object name : failedObjectNames) {
								XMAdminUtil.getInstance()
											.updateLogFile(messages.relationLbl + " " + messages.projectObject + " '"
													+ name + "' " + messages.relationLbl + " "
													+ messages.withLbl + " " + messages.userObject + " '" + userName
													+ "' ", MessageType.FAILURE);
								
							}*/
						}
					} catch (CannotCreateObjectException e) {
						Display.getDefault().asyncExec(new Runnable() {

							@Override
							public void run() {
								CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
										messages.objectPermissionDialogTitle, messages.objectPermissionDialogMsg);
							}
						});
					} catch (UnauthorizedAccessException e) {
						Display.getDefault().asyncExec(new Runnable() {

							@Override
							public void run() {
								CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
										messages.objectPermissionDialogTitle, messages.objectPermissionDialogMsg);
							}
						});
					} catch (Exception e) {
						LOGGER.warn("Unable to De-Activate user project relation model! ", e);
					}
					monitor.worked(70);
					monitor.done();
					return Status.OK_STATUS;
				}
			};
			deActivateUserProjectJob.setUser(true);
			deActivateUserProjectJob.setRule(this.jobSchedulingRule);
			deActivateUserProjectJob.schedule();
		}
	}

	/**
	 * De activate project user rel.
	 *
	 * @param selections
	 *            the selections
	 */
	@SuppressWarnings("unchecked")
	private void deActivateProjectUserRel(final IStructuredSelection selections) {
		Object selectionObj = selections.getFirstElement();
		RelationObj relationObj = (RelationObj) selectionObj;
		final IAdminTreeChild containerObj = relationObj.getContainerObj();
		try {
			if (!XMAdminUtil.getInstance().isRelObjAccessAllowed(containerObj,
					"USER_TO_PROJECT-ACTIVATE_DEACTIVATE")) {
				CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
						messages.objectPermissionDialogTitle, messages.objectPermissionDialogMsg);
				return;
			}
		} catch (Exception e) {
			if (e instanceof ResourceAccessException) {
				CustomMessageDialog.openError(Display.getCurrent().getActiveShell(), messages.errorDialogTitile,
						messages.serverNotReachable);
			}
			LOGGER.error("Exception occured in calling access allowed API " + e);
			return;
		}
		AdminTreeviewer adminTree = XMAdminUtil.getInstance().getAdminTree();
		User user = (User) relationObj.getRefObject();
		String userRelname = user.getName();
		String projectName = ((Project)relationObj.getParent().getParent()).getName();
		String confirmDialogTitle = messages.changeStatusConfirmDialogTitle;
		String confirmDialogMsg;
		if (selections.size() == 1) {
			confirmDialogMsg = messages.deActivateRelchangeStatusConfirmDialogMsg + " \'" + userRelname + "\'" + " ?";
		} else {
			confirmDialogMsg = messages.deActivateMultiRelchangeStatusConfirmDialogMsg + " ?";
		}
		boolean isConfirmed = CustomMessageDialog.openQuestion(adminTree.getControl().getShell(), confirmDialogTitle,
				confirmDialogMsg);
		if (isConfirmed) {
			final List<Object> selectionList = selections.toList();

			Job deActivateProjectUserJob = new Job("De-Activating..") {

				@Override
				protected IStatus run(final IProgressMonitor monitor) {
					monitor.beginTask("De-Activating..", 100);
					monitor.worked(30);
					try {
						List<UserProjectRelRequest> userProjectRelRequestList = new ArrayList<>();
						Map<String, RelationObj> relObjMap = new HashMap<>();
						List<String> objectNames = new ArrayList<>();
						for (int i = 0; i < selectionList.size(); i++) {
							UserProjectRelRequest userProjectRelRequest = new UserProjectRelRequest();
							RelationObj relObject = (RelationObj) selectionList.get(i);
							userProjectRelRequest.setId(relObject.getRelId());
							userProjectRelRequest.setStatus(com.magna.xmbackend.vo.enums.Status.INACTIVE.name());
							relObjMap.put(relObject.getRelId(), relObject);
							userProjectRelRequestList.add(userProjectRelRequest);
							objectNames.add(((User) relObject.getRefObject()).getName());
						}
						UserProjectRelController userProjectRelController = new UserProjectRelController();
						UserProjectRelBatchResponse response = userProjectRelController
								.multiUpdateUserProjectRelStatus(userProjectRelRequestList);
						//TODO
						if (response != null) {
							List<Map<String, String>> statusMaps = response.getStatusMap();
							if (!statusMaps.isEmpty()) {
								Display.getDefault().asyncExec(new Runnable() {
									public void run() {
										String errorMessage = "";
										for (Map<String, String> statusMap : statusMaps) {
											errorMessage += statusMap.get(
													XMAdminUtil.getInstance().getCurrentLocaleEnum().getLangCode());
											errorMessage += "\n";
										}
										if (errorMessage.length() > 0) {
											CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
													messages.dndErrorTitle, errorMessage);
										}
									}
								});
							}
							
							List<String> statusUpdatationFailedList = response.getStatusUpdatationFailedList();
							if (!statusUpdatationFailedList.isEmpty()) {
								for (String id : statusUpdatationFailedList) {
									Iterator<Map.Entry<String, RelationObj>> iter = relObjMap.entrySet().iterator();
									while (iter.hasNext()) {
										Map.Entry<String, RelationObj> entry = iter.next();
										if (id.equalsIgnoreCase(entry.getKey())) {
											iter.remove();
										}
									}
								}
								Display.getDefault().asyncExec(new Runnable() {
									public void run() {
										CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
												messages.changeStatusConfirmDialogTitle,
												messages.multiStatusUpadteObjectDilaogErrMsg);
									}
								});
							}
							
							List<String> updatedRelIds = response.getStatusUpdatationSuccessList();
							Iterator<Map.Entry<String, RelationObj>> iter = relObjMap.entrySet().iterator();
							while (iter.hasNext()) {
								Map.Entry<String, RelationObj> entry = iter.next();
								RelationObj obj = entry.getValue();
								if(!updatedRelIds.isEmpty() && updatedRelIds.contains(obj.getRelId())) {
									obj.setActive(false);
								} else{
									objectNames.remove(((User) obj.getRefObject()).getName());
								}
							}

							Display.getDefault().asyncExec(new Runnable() {
								@Override
								public void run() {
									adminTree.refresh(true);
									adminTree.refershBackReference(new Class[] { UserProjects.class });
								}
							});
							for (Object name : objectNames) {
								XMAdminUtil.getInstance()
										.updateLogFile(messages.relationLbl + " " + messages.objectStatusUpdate + " "
												+ messages.userObject + " '" + name + "' " + messages.withLbl + " "
												+ messages.projectObject + " '" + projectName + "' "
												+ messages.objectUpdate, MessageType.SUCCESS);
							}
							
							Display.getDefault().asyncExec(new Runnable() {
								public void run() {
									for (Map<String, String> statusMap : statusMaps) {
										String errorMessage = statusMap.get(
												XMAdminUtil.getInstance().getCurrentLocaleEnum().getLangCode());
										XMAdminUtil.getInstance().updateLogFile(
												errorMessage, MessageType.FAILURE);
									}
								}
							});
						}
					} catch (CannotCreateObjectException e) {
						Display.getDefault().asyncExec(new Runnable() {

							@Override
							public void run() {
								CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
										messages.objectPermissionDialogTitle, messages.objectPermissionDialogMsg);
							}
						});
					} catch (UnauthorizedAccessException e) {
						Display.getDefault().asyncExec(new Runnable() {

							@Override
							public void run() {
								CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
										messages.objectPermissionDialogTitle, messages.objectPermissionDialogMsg);
							}
						});
					} catch (Exception e) {
						LOGGER.warn("Unable to De-Activate project user relation model! ", e);
					}
					monitor.worked(70);
					monitor.done();
					return Status.OK_STATUS;
				}
			};
			deActivateProjectUserJob.setUser(true);
			deActivateProjectUserJob.setRule(this.jobSchedulingRule);
			deActivateProjectUserJob.schedule();
		}
	}

	/**
	 * De activate site AA pro start app rel.
	 *
	 * @param selections
	 *            the selections
	 */
	@SuppressWarnings("unchecked")
	private void deActivateSiteAAProStartAppRel(final IStructuredSelection selections) {
		Object selectionObj = selections.getFirstElement();
		RelationObj relationObj = (RelationObj) selectionObj;
		final IAdminTreeChild containerObj = relationObj.getContainerObj();
		try {
			if (!XMAdminUtil.getInstance().isRelObjAccessAllowed(containerObj,
					"START_APPLICATION_TO_PROJECT-ACTIVATE_DEACTIVATE")) {
				CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
						messages.objectPermissionDialogTitle, messages.objectPermissionDialogMsg);
				return;
			}
		} catch (Exception e) {
			if (e instanceof ResourceAccessException) {
				CustomMessageDialog.openError(Display.getCurrent().getActiveShell(), messages.errorDialogTitile,
						messages.serverNotReachable);
			}
			LOGGER.error("Exception occured in calling access allowed API " + e);
			return;
		}
		AdminTreeviewer adminTree = XMAdminUtil.getInstance().getAdminTree();
		StartApplication startApplication = (StartApplication) relationObj.getRefObject();
		String startAppRelname = startApplication.getName();
		IStructuredSelection selection = (IStructuredSelection) adminTree.getSelection();
		String projectName;
		String adminAreaName;
		if(containerObj instanceof SiteAdminAreaProjectStartApplications){
			 projectName = ((Project)((RelationObj)((IAdminTreeChild)selection.getFirstElement()).getParent()).getRefObject()).getName();
			 adminAreaName = ((AdministrationArea)((RelationObj)((IAdminTreeChild)selection.getFirstElement()).getParent().getParent().getParent()).getRefObject()).getName();
		}else if(containerObj instanceof AdminAreaProjectStartApplications){
			 projectName = ((Project)((RelationObj)((IAdminTreeChild)selection.getFirstElement()).getParent()).getRefObject()).getName();
			 adminAreaName = ((AdministrationArea)((IAdminTreeChild)selection.getFirstElement()).getParent().getParent().getParent()).getName();
		}else{
			 adminAreaName = ((AdministrationArea)((RelationObj)((IAdminTreeChild)selection.getFirstElement()).getParent()).getRefObject()).getName();
			 projectName = ((Project)((IAdminTreeChild)selection.getFirstElement()).getParent().getParent().getParent()).getName();
		}
		
		String confirmDialogTitle = messages.changeStatusConfirmDialogTitle;
		String confirmDialogMsg;
		if (selections.size() == 1) {
			confirmDialogMsg = messages.deActivateRelchangeStatusConfirmDialogMsg + " \'" + startAppRelname + "\'"
					+ " ?";
		} else {
			confirmDialogMsg = messages.deActivateMultiRelchangeStatusConfirmDialogMsg + " ?";
		}
		boolean isConfirmed = CustomMessageDialog.openQuestion(adminTree.getControl().getShell(), confirmDialogTitle,
				confirmDialogMsg);
		if (isConfirmed) {
			final List<Object> selectionList = selections.toList();

			Job deActivateSiteAAProStartAppJob = new Job("De-Activating..") {

				@Override
				protected IStatus run(final IProgressMonitor monitor) {
					monitor.beginTask("De-Activating..", 100);
					monitor.worked(30);
					try {
						List<ProjectStartAppRelRequest> projectStartAppRelRequestList = new ArrayList<>();
						Map<String, RelationObj> relObjMap = new HashMap<>();
						List<String> objectNames = new ArrayList<>();
						for (int i = 0; i < selectionList.size(); i++) {
							ProjectStartAppRelRequest projectStartAppRelRequest = new ProjectStartAppRelRequest();
							RelationObj relObject = (RelationObj) selectionList.get(i);
							projectStartAppRelRequest.setId(relObject.getRelId());
							projectStartAppRelRequest.setStatus(com.magna.xmbackend.vo.enums.Status.INACTIVE.name());
							projectStartAppRelRequestList.add(projectStartAppRelRequest);
							relObjMap.put(relObject.getRelId(), relObject);
							objectNames.add(((StartApplication) relObject.getRefObject()).getName());
						}
						IAdminTreeChild parent = ((RelationObj) relationObj).getContainerObj().getParent();
						ProjectStartAppRelController projectStartAppRelController = new ProjectStartAppRelController(
								XMAdminUtil.getInstance().getAAForHeaders(parent));
						ProjectStartAppRelBatchResponse response = projectStartAppRelController
								.multiUpdateProjectStartAppRelStatus(projectStartAppRelRequestList);
						if (response != null) {
							List<String> statusUpdatationFailedList = response.getStatusUpdatationFailedList();
							if (!statusUpdatationFailedList.isEmpty()) {
								for (String id : statusUpdatationFailedList) {
									Iterator<Map.Entry<String, RelationObj>> iter = relObjMap.entrySet().iterator();
									while (iter.hasNext()) {
										Map.Entry<String, RelationObj> entry = iter.next();
										if (id.equalsIgnoreCase(entry.getKey())) {
											iter.remove();
										}
									}
								}
								Display.getDefault().asyncExec(new Runnable() {
									public void run() {
										CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
												messages.changeStatusConfirmDialogTitle,
												messages.multiStatusUpadteObjectDilaogErrMsg);
									}
								});
							}
							Iterator<Map.Entry<String, RelationObj>> iter = relObjMap.entrySet().iterator();
							while (iter.hasNext()) {
								Map.Entry<String, RelationObj> entry = iter.next();
								RelationObj obj = entry.getValue();
								obj.setActive(false);
							}

							Display.getDefault().asyncExec(new Runnable() {
								@Override
								public void run() {
									adminTree.refresh(true);
								}
							});
							for (Object name : objectNames) {
								XMAdminUtil.getInstance()
										.updateLogFile(messages.relationLbl + " " + messages.objectStatusUpdate + " "
												+ messages.startApplicationObject + " '" + name + "' "
												+ messages.withLbl + " " + messages.projectObject + " '" + projectName
												+ "' " + messages.andLbl + " " + messages.administrationAreaObject
												+ " '" + adminAreaName + "' " + messages.objectUpdate,
												MessageType.SUCCESS);
							}
						}

					} catch (UnauthorizedAccessException e) {
						Display.getDefault().asyncExec(new Runnable() {

							@Override
							public void run() {
								CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
										messages.objectPermissionDialogTitle, messages.objectPermissionDialogMsg);
							}
						});
					} catch (Exception e) {
						LOGGER.warn("Unable to De-Activate site admin area project start application relation model! ",
								e);
					}
					monitor.worked(70);
					monitor.done();
					return Status.OK_STATUS;
				}
			};
			deActivateSiteAAProStartAppJob.setUser(true);
			deActivateSiteAAProStartAppJob.setRule(this.jobSchedulingRule);
			deActivateSiteAAProStartAppJob.schedule();
		}
	}

	/**
	 * De activate site AA start app rel.
	 *
	 * @param selections
	 *            the selections
	 */
	@SuppressWarnings("unchecked")
	private void deActivateSiteAAStartAppRel(final IStructuredSelection selections) {
		Object selectionObj = selections.getFirstElement();
		RelationObj relationObj = (RelationObj) selectionObj;
		final IAdminTreeChild containerObj = relationObj.getContainerObj();
		try {
			if (!XMAdminUtil.getInstance().isRelObjAccessAllowed(containerObj,
					"START_APPLICATION_TO_ADMINISTRATION_AREA-ACTIVATE_DEACTIVATE")) {
				CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
						messages.objectPermissionDialogTitle, messages.objectPermissionDialogMsg);
				return;
			}
		} catch (Exception e) {
			if (e instanceof ResourceAccessException) {
				CustomMessageDialog.openError(Display.getCurrent().getActiveShell(), messages.errorDialogTitile,
						messages.serverNotReachable);
			}
			LOGGER.error("Exception occured in calling access allowed API " + e);
			return;
		}
		AdminTreeviewer adminTree = XMAdminUtil.getInstance().getAdminTree();
		StartApplication startApplication = (StartApplication) relationObj.getRefObject();
		String startAppRelname = startApplication.getName();
		IStructuredSelection selection = (IStructuredSelection) adminTree.getSelection();
		String adminAreaName;
		if(containerObj instanceof SiteAdminAreaStartApplications){
			 adminAreaName = ((AdministrationArea)((RelationObj)((IAdminTreeChild)selection.getFirstElement()).getParent()).getRefObject()).getName();
		}else {
			 adminAreaName = ((AdministrationArea)(((IAdminTreeChild)selection.getFirstElement()).getParent())).getName();
		}
		String confirmDialogTitle = messages.changeStatusConfirmDialogTitle;
		String confirmDialogMsg;
		if (selections.size() == 1) {
			confirmDialogMsg = messages.deActivateRelchangeStatusConfirmDialogMsg + " \'" + startAppRelname + "\'"
					+ " ?";
		} else {
			confirmDialogMsg = messages.deActivateMultiRelchangeStatusConfirmDialogMsg + " ?";
		}
		boolean isConfirmed = CustomMessageDialog.openQuestion(adminTree.getControl().getShell(), confirmDialogTitle,
				confirmDialogMsg);
		if (isConfirmed) {
			final List<Object> selectionList = selections.toList();

			Job deActivateSiteAAStartAppRelJob = new Job("De-Activating..") {

				@Override
				protected IStatus run(final IProgressMonitor monitor) {
					monitor.beginTask("De-Activating..", 100);
					monitor.worked(30);
					try {
						List<AdminAreaStartAppRelRequest> adminAreaStartAppRelRequestList = new ArrayList<>();
						Map<String, RelationObj> relObjMap = new HashMap<>();
						List<String> objectNames = new ArrayList<>();
						for (int i = 0; i < selectionList.size(); i++) {
							AdminAreaStartAppRelRequest adminAreaStartAppRelRequest = new AdminAreaStartAppRelRequest();
							RelationObj relObject = (RelationObj) selectionList.get(i);
							adminAreaStartAppRelRequest.setId(relObject.getRelId());
							adminAreaStartAppRelRequest.setStatus(com.magna.xmbackend.vo.enums.Status.INACTIVE.name());
							adminAreaStartAppRelRequestList.add(adminAreaStartAppRelRequest);
							relObjMap.put(relObject.getRelId(), relObject);
							objectNames.add(((StartApplication) relObject.getRefObject()).getName());
						}
						AdminAreaStartAppRelController adminAreaStartAppRelController = new AdminAreaStartAppRelController(
								XMAdminUtil.getInstance().getAAForHeaders(relationObj.getContainerObj()));
						AdminAreaStartAppRelResponse response = adminAreaStartAppRelController
								.multiUpdateAAStartAppRelStatus(adminAreaStartAppRelRequestList);
						if (response != null) {
							List<String> statusUpdatationFailedList = response.getStatusUpdatationFailedList();
							if (!statusUpdatationFailedList.isEmpty()) {
								for (String id : statusUpdatationFailedList) {
									Iterator<Map.Entry<String, RelationObj>> iter = relObjMap.entrySet().iterator();
									while (iter.hasNext()) {
										Map.Entry<String, RelationObj> entry = iter.next();
										if (id.equalsIgnoreCase(entry.getKey())) {
											iter.remove();
										}
									}
								}
								Display.getDefault().asyncExec(new Runnable() {
									public void run() {
										CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
												messages.changeStatusConfirmDialogTitle,
												messages.multiStatusUpadteObjectDilaogErrMsg);
									}
								});
							}
							Iterator<Map.Entry<String, RelationObj>> iter = relObjMap.entrySet().iterator();
							while (iter.hasNext()) {
								Map.Entry<String, RelationObj> entry = iter.next();
								RelationObj obj = entry.getValue();
								obj.setActive(false);
							}

							Display.getDefault().asyncExec(new Runnable() {
								@Override
								public void run() {
									adminTree.refresh(true);
								}
							});
							for (Object name : objectNames) {
								XMAdminUtil.getInstance().updateLogFile(messages.relationLbl + " "
										+ messages.objectStatusUpdate + " " + messages.startApplicationObject + " '"
										+ name + "' " + messages.withLbl + " " + messages.administrationAreaObject
										+ " '" + adminAreaName + "' " + messages.objectUpdate, MessageType.SUCCESS);
							}
						}
					} catch (UnauthorizedAccessException e) {
						Display.getDefault().asyncExec(new Runnable() {

							@Override
							public void run() {
								CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
										messages.objectPermissionDialogTitle, messages.objectPermissionDialogMsg);
							}
						});
					} catch (Exception e) {
						LOGGER.warn("Unable to De-Activate site admin area start application relation model! ", e);
					}
					monitor.worked(70);
					monitor.done();
					return Status.OK_STATUS;
				}
			};
			deActivateSiteAAStartAppRelJob.setUser(true);
			deActivateSiteAAStartAppRelJob.setRule(this.jobSchedulingRule);
			deActivateSiteAAStartAppRelJob.schedule();
		}

	}

	/**
	 * De activate site AA project app rel.
	 *
	 * @param selections
	 *            the selections
	 */
	@SuppressWarnings("unchecked")
	private void deActivateSiteAAProjectAppRel(final IStructuredSelection selections) {
		Object selectionObj = selections.getFirstElement();
		RelationObj relationObj = (RelationObj) selectionObj;
		final IAdminTreeChild containerObj = relationObj.getContainerObj();
		try {
			if (!XMAdminUtil.getInstance().isRelObjAccessAllowed(containerObj,
					"PROJECT_APPLICATION_TO_PROJECT-ACTIVATE_DEACTIVATE")) {
				CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
						messages.objectPermissionDialogTitle, messages.objectPermissionDialogMsg);
				return;
			}
		} catch (Exception e) {
			if (e instanceof ResourceAccessException) {
				CustomMessageDialog.openError(Display.getCurrent().getActiveShell(), messages.errorDialogTitile,
						messages.serverNotReachable);
			}
			LOGGER.error("Exception occured in calling access allowed API " + e);
			return;
		}
		AdminTreeviewer adminTree = XMAdminUtil.getInstance().getAdminTree();
		ProjectApplication projectApplication = (ProjectApplication) relationObj.getRefObject();
		String projectAppRelname = projectApplication.getName();
		IStructuredSelection selection = (IStructuredSelection) adminTree.getSelection();
		String adminAreaName;
		String projectName;
		if (containerObj instanceof SiteAdminProjectAppNotFixed || containerObj instanceof SiteAdminProjectAppFixed
				|| containerObj instanceof SiteAdminProjectAppProtected) {
			adminAreaName = ((AdministrationArea) ((RelationObj) ((IAdminTreeChild) selection.getFirstElement())
					.getParent().getParent().getParent().getParent()).getRefObject()).getName();
			projectName = ((Project) ((RelationObj) ((IAdminTreeChild) selection.getFirstElement()).getParent()
					.getParent()).getRefObject()).getName();
		} else if (containerObj instanceof AdminAreaProjectAppNotFixed
				|| containerObj instanceof AdminAreaProjectAppFixed
				|| containerObj instanceof AdminAreaProjectAppProtected) {
			adminAreaName = ((AdministrationArea) (((IAdminTreeChild) selection.getFirstElement()).getParent()
					.getParent().getParent().getParent())).getName();
			projectName = ((Project) ((RelationObj) ((IAdminTreeChild) selection.getFirstElement()).getParent()
					.getParent()).getRefObject()).getName();
		} else {
			adminAreaName = ((AdministrationArea) ((RelationObj) ((IAdminTreeChild) selection.getFirstElement())
					.getParent().getParent()).getRefObject()).getName();
			projectName = ((Project) (((IAdminTreeChild) selection.getFirstElement()).getParent()
					.getParent().getParent().getParent())).getName();
		}
		String confirmDialogTitle = messages.changeStatusConfirmDialogTitle;
		String confirmDialogMsg;
		if (selections.size() == 1) {
			confirmDialogMsg = messages.deActivateRelchangeStatusConfirmDialogMsg + " \'" + projectAppRelname + "\'"
					+ " ?";
		} else {
			confirmDialogMsg = messages.deActivateMultiRelchangeStatusConfirmDialogMsg + " ?";
		}
		boolean isConfirmed = CustomMessageDialog.openQuestion(adminTree.getControl().getShell(), confirmDialogTitle,
				confirmDialogMsg);
		if (isConfirmed) {
			final List<Object> selectionList = selections.toList();

			Job deActivateSSiteAAProjectAppRelJob = new Job("De-Activating..") {

				@Override
				protected IStatus run(final IProgressMonitor monitor) {
					monitor.beginTask("De-Activating..", 100);
					monitor.worked(30);
					try {
						List<AdminAreaProjectAppRelRequest> adminAreaProjectAppRelRequestList = new ArrayList<>();
						Map<String, RelationObj> relObjMap = new HashMap<>();
						List<String> objectNames = new ArrayList<>();
						for (int i = 0; i < selectionList.size(); i++) {
							AdminAreaProjectAppRelRequest adminAreaProjectAppRelRequest = new AdminAreaProjectAppRelRequest();
							RelationObj relObject = (RelationObj) selectionList.get(i);
							adminAreaProjectAppRelRequest.setId(relObject.getRelId());
							adminAreaProjectAppRelRequest
									.setStatus(com.magna.xmbackend.vo.enums.Status.INACTIVE.name());
							adminAreaProjectAppRelRequestList.add(adminAreaProjectAppRelRequest);
							relObjMap.put(relObject.getRelId(), relObject);
							objectNames.add(((ProjectApplication) relObject.getRefObject()).getName());
						}
						IAdminTreeChild parent = ((RelationObj) relationObj).getContainerObj().getParent().getParent();
						AdminAreaProjectAppRelController adminAreaProjectAppRelController = new AdminAreaProjectAppRelController(
								XMAdminUtil.getInstance().getAAForHeaders(parent));
						AdminAreaProjectAppRelResponse response = adminAreaProjectAppRelController
								.multiUpdateAAProjectAppRelStatus(adminAreaProjectAppRelRequestList);
						if (response != null) {
							List<String> statusUpdatationFailedList = response.getStatusUpdatationFailedList();
							if (!statusUpdatationFailedList.isEmpty()) {
								for (String id : statusUpdatationFailedList) {
									Iterator<Map.Entry<String, RelationObj>> iter = relObjMap.entrySet().iterator();
									while (iter.hasNext()) {
										Map.Entry<String, RelationObj> entry = iter.next();
										if (id.equalsIgnoreCase(entry.getKey())) {
											iter.remove();
										}
									}
								}
								Display.getDefault().asyncExec(new Runnable() {
									public void run() {
										CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
												messages.changeStatusConfirmDialogTitle,
												messages.multiStatusUpadteObjectDilaogErrMsg);
									}
								});
							}
							Iterator<Map.Entry<String, RelationObj>> iter = relObjMap.entrySet().iterator();
							while (iter.hasNext()) {
								Map.Entry<String, RelationObj> entry = iter.next();
								RelationObj obj = entry.getValue();
								obj.setActive(false);
							}

							Display.getDefault().asyncExec(new Runnable() {
								@Override
								public void run() {
									adminTree.refresh(true);
								}
							});
							for (Object name : objectNames) {
								XMAdminUtil.getInstance()
										.updateLogFile(messages.relationLbl + " " + messages.objectStatusUpdate + " "
												+ messages.projectApplicationObject + " '" + name + "' "
												+ messages.withLbl + " " + messages.projectObject + " '" + projectName
												+ "' " + messages.andLbl + messages.administrationAreaObject + " '"
												+ adminAreaName + "' " + messages.objectUpdate, MessageType.SUCCESS);
							}
						}
					} catch (UnauthorizedAccessException e) {
						Display.getDefault().asyncExec(new Runnable() {

							@Override
							public void run() {
								CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
										messages.objectPermissionDialogTitle, messages.objectPermissionDialogMsg);
							}
						});
					} catch (Exception e) {
						LOGGER.warn("Unable to De-Activate site admin area project application relation model! ", e);
					}
					monitor.worked(70);
					monitor.done();
					return Status.OK_STATUS;
				}
			};
			deActivateSSiteAAProjectAppRelJob.setUser(true);
			deActivateSSiteAAProjectAppRelJob.setRule(this.jobSchedulingRule);
			deActivateSSiteAAProjectAppRelJob.schedule();
		}
	}

	/**
	 * De activate site AA user app rel.
	 *
	 * @param selections
	 *            the selections
	 */
	@SuppressWarnings("unchecked")
	private void deActivateSiteAAUserAppRel(final IStructuredSelection selections) {
		Object selectionObj = selections.getFirstElement();
		RelationObj relationObj = (RelationObj) selectionObj;
		final IAdminTreeChild containerObj = relationObj.getContainerObj();
		try {
			if (!XMAdminUtil.getInstance().isRelObjAccessAllowed(containerObj,
					"USER_APPLICATION_TO_ADMINISTRATION_AREA-ACTIVATE_DEACTIVATE")) {
				CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
						messages.objectPermissionDialogTitle, messages.objectPermissionDialogMsg);
				return;
			}
		} catch (Exception e) {
			if (e instanceof ResourceAccessException) {
				CustomMessageDialog.openError(Display.getCurrent().getActiveShell(), messages.errorDialogTitile,
						messages.serverNotReachable);
			}
			LOGGER.error("Exception occured in calling access allowed API " + e);
			return;
		}
		AdminTreeviewer adminTree = XMAdminUtil.getInstance().getAdminTree();
		UserApplication userApplication = (UserApplication) relationObj.getRefObject();
		String userAppRelname = userApplication.getName();
		IStructuredSelection selection = (IStructuredSelection) adminTree.getSelection();
		String adminAreaName;
		if(containerObj instanceof SiteAdminAreaUserAppNotFixed
				|| containerObj instanceof SiteAdminAreaUserAppFixed
				|| containerObj instanceof SiteAdminAreaUserAppProtected){
			adminAreaName = ((AdministrationArea) ((RelationObj) ((IAdminTreeChild) selection.getFirstElement())
					.getParent().getParent()).getRefObject()).getName();
		}else{
			adminAreaName = ((AdministrationArea) (((IAdminTreeChild) selection.getFirstElement())
					.getParent().getParent())).getName();
		}
		String confirmDialogTitle = messages.changeStatusConfirmDialogTitle;
		String confirmDialogMsg;
		if (selections.size() == 1) {
			confirmDialogMsg = messages.deActivateRelchangeStatusConfirmDialogMsg + " \'" + userAppRelname + "\'"
					+ " ?";
		} else {
			confirmDialogMsg = messages.deActivateMultiRelchangeStatusConfirmDialogMsg + " ?";
		}
		boolean isConfirmed = CustomMessageDialog.openQuestion(adminTree.getControl().getShell(), confirmDialogTitle,
				confirmDialogMsg);
		if (isConfirmed) {
			final List<Object> selectionList = selections.toList();

			Job deActivateSiteAAUserAppRelJob = new Job("De-Activating..") {

				@Override
				protected IStatus run(final IProgressMonitor monitor) {
					monitor.beginTask("De-Activating..", 100);
					monitor.worked(30);
					try {
						List<AdminAreaUserAppRelRequest> adminAreaUserAppRelRequestList = new ArrayList<>();
						Map<String, RelationObj> relObjMap = new HashMap<>();
						List<String> objectNames = new ArrayList<>();
						for (int i = 0; i < selectionList.size(); i++) {
							AdminAreaUserAppRelRequest adminAreaUserAppRelRequest = new AdminAreaUserAppRelRequest();
							RelationObj relObject = (RelationObj) selectionList.get(i);
							adminAreaUserAppRelRequest.setId(relObject.getRelId());
							adminAreaUserAppRelRequest.setStatus(com.magna.xmbackend.vo.enums.Status.INACTIVE.name());
							adminAreaUserAppRelRequestList.add(adminAreaUserAppRelRequest);
							relObjMap.put(relObject.getRelId(), relObject);
							objectNames.add(((UserApplication) relObject.getRefObject()).getName());
						}
						AdminAreaUserAppRelController adminAreaUserAppRelController = new AdminAreaUserAppRelController(
								XMAdminUtil.getInstance().getAAForHeaders(relationObj.getContainerObj()));
						AdminAreaUserAppRelBatchResponse response = adminAreaUserAppRelController
								.multiUpdateAAUserAppRelStatus(adminAreaUserAppRelRequestList);
						if (response != null) {
							List<String> statusUpdatationFailedList = response.getStatusUpdatationFailedList();
							if (!statusUpdatationFailedList.isEmpty()) {
								for (String id : statusUpdatationFailedList) {
									Iterator<Map.Entry<String, RelationObj>> iter = relObjMap.entrySet().iterator();
									while (iter.hasNext()) {
										Map.Entry<String, RelationObj> entry = iter.next();
										if (id.equalsIgnoreCase(entry.getKey())) {
											iter.remove();
										}
									}
								}
								Display.getDefault().asyncExec(new Runnable() {
									public void run() {
										CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
												messages.changeStatusConfirmDialogTitle,
												messages.multiStatusUpadteObjectDilaogErrMsg);
									}
								});
							}
							Iterator<Map.Entry<String, RelationObj>> iter = relObjMap.entrySet().iterator();
							while (iter.hasNext()) {
								Map.Entry<String, RelationObj> entry = iter.next();
								RelationObj obj = entry.getValue();
								obj.setActive(false);
							}

							Display.getDefault().asyncExec(new Runnable() {
								@Override
								public void run() {
									adminTree.refresh(true);
								}
							});
							for (Object name : objectNames) {
								XMAdminUtil.getInstance().updateLogFile(messages.relationLbl + " "
										+ messages.objectStatusUpdate + " " + messages.userApplicationObject + " '"
										+ name + "' " + messages.withLbl + " " + messages.administrationAreaObject
										+ " '" + adminAreaName + "' " + messages.objectUpdate, MessageType.SUCCESS);
							}
						}
					} catch (UnauthorizedAccessException e) {
						Display.getDefault().asyncExec(new Runnable() {

							@Override
							public void run() {
								CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
										messages.objectPermissionDialogTitle, messages.objectPermissionDialogMsg);
							}
						});
					} catch (Exception e) {
						LOGGER.warn("Unable to De-Activate site admin area user application relation model! ", e);
					}
					monitor.worked(70);
					monitor.done();
					return Status.OK_STATUS;
				}
			};
			deActivateSiteAAUserAppRelJob.setUser(true);
			deActivateSiteAAUserAppRelJob.setRule(this.jobSchedulingRule);
			deActivateSiteAAUserAppRelJob.schedule();
		}
	}

	/**
	 * De activate site AA project rel.
	 *
	 * @param selections
	 *            the selections
	 */
	@SuppressWarnings("unchecked")
	private void deActivateSiteAAProjectRel(final IStructuredSelection selections) {
		Object selectionObj = selections.getFirstElement();
		RelationObj relationObj = (RelationObj) selectionObj;
		final IAdminTreeChild containerObj = relationObj.getContainerObj();
		try {
			if (!XMAdminUtil.getInstance().isRelObjAccessAllowed(containerObj,
					"PROJECT_TO_ADMINISTRATION_AREA-ACTIVATE_DEACTIVATE")) {
				CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
						messages.objectPermissionDialogTitle, messages.objectPermissionDialogMsg);
				return;
			}
		} catch (Exception e) {
			if (e instanceof ResourceAccessException) {
				CustomMessageDialog.openError(Display.getCurrent().getActiveShell(), messages.errorDialogTitile,
						messages.serverNotReachable);
			}
			LOGGER.error("Exception occured in calling access allowed API " + e);
			return;
		}
		AdminTreeviewer adminTree = XMAdminUtil.getInstance().getAdminTree();
		Project project = (Project) relationObj.getRefObject();
		String adminAreaProjectRelname = project.getName();
		String adminAreaName;
		if (containerObj instanceof SiteAdminAreaProjectChild) {
			adminAreaName = ((AdministrationArea) ((RelationObj) relationObj.getParent().getParent()).getRefObject())
					.getName();
		} else {
			adminAreaName = ((AdministrationArea) (relationObj.getParent().getParent())).getName();
		}
		String confirmDialogTitle = messages.changeStatusConfirmDialogTitle;
		String confirmDialogMsg;
		if (selections.size() == 1) {
			confirmDialogMsg = messages.deActivateRelchangeStatusConfirmDialogMsg + " \'" + adminAreaProjectRelname
					+ "\'" + " ?";
		} else {
			confirmDialogMsg = messages.deActivateMultiRelchangeStatusConfirmDialogMsg + " ?";
		}
		boolean isConfirmed = CustomMessageDialog.openQuestion(adminTree.getControl().getShell(), confirmDialogTitle,
				confirmDialogMsg);
		if (isConfirmed) {
			final List<Object> selectionList = selections.toList();

			Job deActivateSiteAAProjectRelJob = new Job("De-Activating..") {

				@Override
				protected IStatus run(final IProgressMonitor monitor) {
					monitor.beginTask("De-Activating..", 100);
					monitor.worked(30);
					try {
						List<AdminAreaProjectRelRequest> adminAreaProjectRelRequestList = new ArrayList<>();
						Map<String, RelationObj> relObjMap = new HashMap<>();
						List<String> objectNames = new ArrayList<>();
						for (int i = 0; i < selectionList.size(); i++) {
							AdminAreaProjectRelRequest adminAreaProjectRelRequest = new AdminAreaProjectRelRequest();
							RelationObj relObject = (RelationObj) selectionList.get(i);
							adminAreaProjectRelRequest.setId(relObject.getRelId());
							adminAreaProjectRelRequest.setStatus(com.magna.xmbackend.vo.enums.Status.INACTIVE.name());
							adminAreaProjectRelRequestList.add(adminAreaProjectRelRequest);
							relObjMap.put(relObject.getRelId(), relObject);
							objectNames.add(((Project) relObject.getRefObject()).getName());
						}
						AdminAreaProjectRelController adminAreaProjectRelController = new AdminAreaProjectRelController(
								XMAdminUtil.getInstance().getAAForHeaders(relationObj));
						AdminAreaProjectResponseWrapper response = adminAreaProjectRelController
								.multiUpdateAdminAreaProjectRelStatus(adminAreaProjectRelRequestList);
						if (response != null) {

							List<String> statusUpdatationFailedList = response.getStatusUpdatationFailedList();
							if (!statusUpdatationFailedList.isEmpty()) {
								for (String id : statusUpdatationFailedList) {
									Iterator<Map.Entry<String, RelationObj>> iter = relObjMap.entrySet().iterator();
									while (iter.hasNext()) {
										Map.Entry<String, RelationObj> entry = iter.next();
										if (id.equalsIgnoreCase(entry.getKey())) {
											iter.remove();
										}
									}
								}
								Display.getDefault().asyncExec(new Runnable() {
									public void run() {
										CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
												messages.changeStatusConfirmDialogTitle,
												messages.multiStatusUpadteObjectDilaogErrMsg);
									}
								});
							}
							Iterator<Map.Entry<String, RelationObj>> iter = relObjMap.entrySet().iterator();
							while (iter.hasNext()) {
								Map.Entry<String, RelationObj> entry = iter.next();
								RelationObj obj = entry.getValue();
								obj.setActive(false);
							}

							Display.getDefault().asyncExec(new Runnable() {
								@Override
								public void run() {
									adminTree.refresh(true);
									if (relationObj.getParent() instanceof SiteAdminAreaProjects) {
										adminTree.refershBackReference(new Class[] { AdminAreaProjects.class });
									} else if (relationObj.getParent() instanceof AdminAreaProjects) {
										adminTree.refershBackReference(new Class[] { SiteAdminAreaProjects.class });
									}
									adminTree.refershBackReference(new Class[] { ProjectAdminAreas.class });
								}
							});
							for (Object name : objectNames) {
								XMAdminUtil.getInstance()
										.updateLogFile(messages.relationLbl + " " + messages.objectStatusUpdate + " "
												+ messages.projectObject + " '" + name + "' " + messages.withLbl + " "
												+ messages.administrationAreaObject + " '" + adminAreaName + "' "
												+ messages.objectUpdate, MessageType.SUCCESS);
							}
						}
					} catch (UnauthorizedAccessException e) {
						Display.getDefault().asyncExec(new Runnable() {

							@Override
							public void run() {
								CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
										messages.objectPermissionDialogTitle, messages.objectPermissionDialogMsg);
							}
						});
					} catch (Exception e) {
						LOGGER.warn("Unable to De-Activate site admin area project relation model! ", e);
					}
					monitor.worked(70);
					monitor.done();
					return Status.OK_STATUS;
				}
			};
			deActivateSiteAAProjectRelJob.setUser(true);
			deActivateSiteAAProjectRelJob.setRule(this.jobSchedulingRule);
			deActivateSiteAAProjectRelJob.schedule();
		}
	}

	/**
	 * De activate site AA rel.
	 *
	 * @param selections
	 *            the selections
	 */
	@SuppressWarnings("unchecked")
	private void deActivateSiteAARel(final IStructuredSelection selections) {
		Object selectionObj = selections.getFirstElement();
		RelationObj relationObj = (RelationObj) selectionObj;
		final IAdminTreeChild containerObj = relationObj.getContainerObj();
		try {
			if (!XMAdminUtil.getInstance().isRelObjAccessAllowed(containerObj,
					"ADMINISTRATION_AREA_TO_SITE-ACTIVATE_DEACTIVATE")) {
				CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
						messages.objectPermissionDialogTitle, messages.objectPermissionDialogMsg);
				return;
			}
		} catch (Exception e) {
			if (e instanceof ResourceAccessException) {
				CustomMessageDialog.openError(Display.getCurrent().getActiveShell(), messages.errorDialogTitile,
						messages.serverNotReachable);
			}
			LOGGER.error("Exception occured in calling access allowed API " + e);
			return;
		}
		AdminTreeviewer adminTree = XMAdminUtil.getInstance().getAdminTree();
		AdministrationArea administrationArea = (AdministrationArea) relationObj.getRefObject();
		String adminAreaRelname = administrationArea.getName();
		String siteName = ((Site) (relationObj.getParent().getParent())).getName();
		String confirmDialogTitle = messages.changeStatusConfirmDialogTitle;
		String confirmDialogMsg;
		if (selections.size() == 1) {
			confirmDialogMsg = messages.deActivateRelchangeStatusConfirmDialogMsg + " \'" + adminAreaRelname + "\'"
					+ " ?";
		} else {
			confirmDialogMsg = messages.deActivateMultiRelchangeStatusConfirmDialogMsg + " ?";
		}
		boolean isConfirmed = CustomMessageDialog.openQuestion(adminTree.getControl().getShell(), confirmDialogTitle,
				confirmDialogMsg);
		if (isConfirmed) {
			final List<Object> selectionList = selections.toList();

			Job deActivateSiteAARelJob = new Job("De-Activating..") {

				@Override
				protected IStatus run(final IProgressMonitor monitor) {
					monitor.beginTask("De-Activating..", 100);
					monitor.worked(30);
					try {
						List<SiteAdminAreaRelRequest> siteAdminAreaRelRequestList = new ArrayList<>();
						Map<String, RelationObj> relObjMap = new HashMap<>();
						List<String> objectNames = new ArrayList<>();
						for (int i = 0; i < selectionList.size(); i++) {
							SiteAdminAreaRelRequest siteAdminAreaRelRequest = new SiteAdminAreaRelRequest();
							RelationObj relObject = (RelationObj) selectionList.get(i);
							siteAdminAreaRelRequest.setId(relObject.getRelId());
							siteAdminAreaRelRequest.setStatus(com.magna.xmbackend.vo.enums.Status.INACTIVE.name());
							siteAdminAreaRelRequestList.add(siteAdminAreaRelRequest);
							relObjMap.put(relObject.getRelId(), relObject);
							objectNames.add(((AdministrationArea) relObject.getRefObject()).getName());
						}
						SiteAdminAreaRelController siteAdminAreaRelController = new SiteAdminAreaRelController();
						SiteAdminAreaRelBatchResponse response = siteAdminAreaRelController
								.multiUpdateSiteAdminAreaRelStatus(siteAdminAreaRelRequestList);
						if (response != null) {
							List<String> statusUpdatationFailedList = response.getStatusUpdatationFailedList();
							if (!statusUpdatationFailedList.isEmpty()) {
								for (String id : statusUpdatationFailedList) {
									Iterator<Map.Entry<String, RelationObj>> iter = relObjMap.entrySet().iterator();
									while (iter.hasNext()) {
										Map.Entry<String, RelationObj> entry = iter.next();
										if (id.equalsIgnoreCase(entry.getKey())) {
											iter.remove();
										}
									}
								}
								Display.getDefault().asyncExec(new Runnable() {
									public void run() {
										CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
												messages.changeStatusConfirmDialogTitle,
												messages.multiStatusUpadteObjectDilaogErrMsg);
									}
								});
							}
							Iterator<Map.Entry<String, RelationObj>> iter = relObjMap.entrySet().iterator();
							while (iter.hasNext()) {
								Map.Entry<String, RelationObj> entry = iter.next();
								RelationObj obj = entry.getValue();
								obj.setActive(false);
							}

							Display.getDefault().asyncExec(new Runnable() {
								@Override
								public void run() {
									adminTree.refresh(true);
								}
							});
							for (Object name : objectNames) {
								XMAdminUtil.getInstance()
										.updateLogFile(messages.relationLbl + " " + messages.objectStatusUpdate + " "
												+ messages.administrationAreaObject + " '" + name + "' "
												+ messages.withLbl + " " + messages.siteObject + " '" + siteName + "' "
												+ messages.objectUpdate, MessageType.SUCCESS);
							}
						}
					} catch (UnauthorizedAccessException e) {
						Display.getDefault().asyncExec(new Runnable() {

							@Override
							public void run() {
								CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
										messages.objectPermissionDialogTitle, messages.objectPermissionDialogMsg);
							}
						});
					} catch (Exception e) {
						LOGGER.warn("Unable to De-Activate site admin area relation model! ", e);
					}
					monitor.worked(70);
					monitor.done();
					return Status.OK_STATUS;
				}
			};
			deActivateSiteAARelJob.setUser(true);
			deActivateSiteAARelJob.setRule(this.jobSchedulingRule);
			deActivateSiteAARelJob.schedule();
		}

	}
}
