package com.magna.xmsystem.xmadmin.ui.parts.proaarelassignevt;

import javax.inject.Inject;

import org.eclipse.core.databinding.Binding;
import org.eclipse.core.databinding.DataBindingContext;
import org.eclipse.core.databinding.beans.BeanProperties;
import org.eclipse.core.databinding.observable.value.IObservableValue;
import org.eclipse.e4.core.services.nls.Translation;
import org.eclipse.e4.ui.model.application.ui.MDirtyable;
import org.eclipse.jface.databinding.swt.WidgetProperties;
import org.eclipse.jface.fieldassist.ControlDecoration;
import org.eclipse.jface.fieldassist.FieldDecorationRegistry;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.StyleRange;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.magna.xmsystem.xmadmin.message.Message;
import com.magna.xmsystem.xmadmin.message.MessageRegistry;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.AdminTreeviewer;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.notification.ProAARelAssignEvtAction;
import com.magna.xmsystem.xmadmin.util.CommonConstants;
import com.magna.xmsystem.xmadmin.util.XMAdminUtil;

// TODO: Auto-generated Javadoc
/**
 * The Class ProjectAARelAssignEvtCompositeAction.
 */
public class ProjectAARelAssignEvtCompositeAction extends ProjectAARelAssignEvtCompositeUI {

	/** Logger instance. */
	private static final Logger LOGGER = LoggerFactory.getLogger(ProjectAARelAssignEvtCompositeAction.class);

	/** The pro AA rel assign evt model. */
	private ProAARelAssignEvtAction proAARelAssignEvtModel;

	/** The old model. */
	private ProAARelAssignEvtAction oldModel;

	/** Member variable for {@link MessageRegistry}. */
	@Inject
	private MessageRegistry registry;

	/** Member variable for messages. */
	@Inject
	@Translation
	transient private Message messages;

	/** Member variable for widgetValue. */
	transient private IObservableValue<?> widgetValue;

	/** Member variable for modelValue. */
	transient private IObservableValue<?> modelValue;

	/** Member variable for binding. */
	@SuppressWarnings("unused")
	transient private Binding bindValue;

	/** The data bind context. */
	final transient DataBindingContext dataBindContext = new DataBindingContext();

	/** The message info decroator. */
	private ControlDecoration messageInfoDecroator;

	/** The subject info decroator. */
	private ControlDecoration subjectInfoDecroator;

	/** The dirty. */
	private MDirtyable dirty;

	/**
	 * Instantiates a new project AA rel assign evt composite action.
	 *
	 * @param parent
	 *            the parent
	 */
	@Inject
	public ProjectAARelAssignEvtCompositeAction(final Composite parent) {
		super(parent, SWT.NONE);
		initListner();
	}

	/**
	 * Init listner.
	 */
	private void initListner() {
		if (this.radioBtnUserGroup != null && !this.radioBtnUserGroup.isDisposed()) {
			this.radioBtnUserGroup.addSelectionListener(new SelectionAdapter() {
				@Override
				public void widgetSelected(SelectionEvent event) {
					if (radioBtnUserGroup.getSelection()) {
						expandeUserFilters(false);
						expandUserGroupFilter(true);
						stackLayout.topControl = userGrpFilterContainer;
						stackContainer.layout();
					} else {
						expandeUserFilters(true);
						expandUserGroupFilter(false);
						stackLayout.topControl = filtersContainer;
						stackContainer.layout();
					}
				}
			});
		}

	}

	/**
	 * Expand user group filter.
	 *
	 * @param isExpand
	 *            the is expand
	 */
	private void expandUserGroupFilter(boolean isExpand) {
		userGroupPGroup.setExpanded(isExpand);
		pGroupRepaint(userGroupPGroup);
	}

	/**
	 * Expande user filters.
	 *
	 * @param isExpand
	 *            the is expand
	 */
	private void expandeUserFilters(boolean isExpand) {
		userFilerPGroup.setExpanded(isExpand);
		pGroupRepaint(userFilerPGroup);
		siteFilerPGroup.setExpanded(isExpand);
		pGroupRepaint(siteFilerPGroup);
		adminAreaFilerPGroup.setExpanded(isExpand);
		pGroupRepaint(adminAreaFilerPGroup);
		projectFilerPGroup.setExpanded(isExpand);
		pGroupRepaint(projectFilerPGroup);
	}

	/**
	 * Register messages.
	 *
	 * @param registry
	 *            the registry
	 */
	public void registerMessages(final MessageRegistry registry) {

		registry.register((text) -> {
			if (lblName != null && !lblName.isDisposed()) {
				lblName.setText(text);
			}
		}, (message) -> {
			if (lblName != null && !lblName.isDisposed()) {
				return getUpdatedWidgetText(message.objectNameLabel, lblName);
			}
			return CommonConstants.EMPTY_STR;
		});

		registry.register((text) -> {
			if (lblDescription != null && !lblDescription.isDisposed()) {
				lblDescription.setText(text);
			}
		}, (message) -> {
			if (lblDescription != null && !lblDescription.isDisposed()) {
				return getUpdatedWidgetText(message.objectDescriptionLabel, lblDescription);
			}
			return CommonConstants.EMPTY_STR;
		});
		registry.register((text) -> {
			if (lblActive != null && !lblActive.isDisposed()) {
				lblActive.setText(text);
			}
		}, (message) -> {
			if (lblActive != null && !lblActive.isDisposed()) {
				return getUpdatedWidgetText(message.objectActiveLabel, lblActive);
			}
			return CommonConstants.EMPTY_STR;
		});

		registry.register((text) -> {
			if (addtoToBtn != null && !addtoToBtn.isDisposed()) {
				addtoToBtn.setText(text);
			}
		}, (message) -> {
			if (addtoToBtn != null && !addtoToBtn.isDisposed()) {
				return getUpdatedWidgetText("Add to To", addtoToBtn);
			}
			return CommonConstants.EMPTY_STR;
		});

		registry.register((text) -> {
			if (addtoCCBtn != null && !addtoCCBtn.isDisposed()) {
				addtoCCBtn.setText(text);
			}
		}, (message) -> {
			if (addtoCCBtn != null && !addtoCCBtn.isDisposed()) {
				return getUpdatedWidgetText("Add to CC", addtoCCBtn);
			}
			return CommonConstants.EMPTY_STR;
		});

		registry.register((text) -> {
			if (lblToUsers != null && !lblToUsers.isDisposed()) {
				lblToUsers.setText(text);
			}
		}, (message) -> {
			if (lblToUsers != null && !lblToUsers.isDisposed()) {
				return getUpdatedWidgetText(message.notificationToUserLbl, lblToUsers);
			}
			return CommonConstants.EMPTY_STR;
		});

		registry.register((text) -> {
			if (lblCCUsers != null && !lblCCUsers.isDisposed()) {
				lblCCUsers.setText(text);
			}
		}, (message) -> {
			if (lblCCUsers != null && !lblCCUsers.isDisposed()) {
				return getUpdatedWidgetText("CC", lblCCUsers);
			}
			return CommonConstants.EMPTY_STR;
		});

		registry.register((text) -> {
			if (lblMessage != null && !lblMessage.isDisposed()) {
				lblMessage.setText(text);
			}
		}, (message) -> {
			if (lblMessage != null && !lblMessage.isDisposed()) {
				return getUpdatedWidgetText(message.notificationMessageLbl, lblMessage);
			}
			return CommonConstants.EMPTY_STR;
		});

		registry.register((text) -> {
			if (lblSubject != null && !lblSubject.isDisposed()) {
				lblSubject.setText(text);
			}
		}, (message) -> {
			if (lblSubject != null && !lblSubject.isDisposed()) {
				return getUpdatedWidgetText(message.notificationSubjectLbl, lblSubject);
			}
			return CommonConstants.EMPTY_STR;
		});

		registry.register((text) -> {
			if (siteFilerPGroup != null && !siteFilerPGroup.isDisposed()) {
				siteFilerPGroup.setText(text);
			}
		}, (message) -> {
			if (siteFilerPGroup != null && !siteFilerPGroup.isDisposed()) {
				return getUpdatedWidgetText(message.siteFilterLabel, siteFilerPGroup);
			}
			return CommonConstants.EMPTY_STR;
		});

		registry.register((text) -> {
			if (adminAreaFilerPGroup != null && !adminAreaFilerPGroup.isDisposed()) {
				adminAreaFilerPGroup.setText(text);
			}
		}, (message) -> {
			if (adminAreaFilerPGroup != null && !adminAreaFilerPGroup.isDisposed()) {
				return getUpdatedWidgetText(message.adminAreaFilterLabel, adminAreaFilerPGroup);
			}
			return CommonConstants.EMPTY_STR;
		});

		registry.register((text) -> {
			if (projectFilerPGroup != null && !projectFilerPGroup.isDisposed()) {
				projectFilerPGroup.setText(text);
			}
		}, (message) -> {
			if (projectFilerPGroup != null && !projectFilerPGroup.isDisposed()) {
				return getUpdatedWidgetText(message.projectFilterLabel, projectFilerPGroup);
			}
			return CommonConstants.EMPTY_STR;
		});

		registry.register((text) -> {
			if (userFilerPGroup != null && !userFilerPGroup.isDisposed()) {
				userFilerPGroup.setText(text);
			}
		}, (message) -> {
			if (userFilerPGroup != null && !userFilerPGroup.isDisposed()) {
				return getUpdatedWidgetText(message.userFilterLabel, userFilerPGroup);
			}
			return CommonConstants.EMPTY_STR;
		});

		registry.register((text) -> {
			if (userGroupPGroup != null && !userGroupPGroup.isDisposed()) {
				userGroupPGroup.setText(text);
			}
		}, (message) -> {
			if (userGroupPGroup != null && !userGroupPGroup.isDisposed()) {
				return getUpdatedWidgetText(message.userGroupFilterLabel, userGroupPGroup);
			}
			return CommonConstants.EMPTY_STR;
		});

		registry.register((text) -> {
			if (radioBtnUser != null && !radioBtnUser.isDisposed()) {
				radioBtnUser.setText(text);
			}
		}, (message) -> {
			if (radioBtnUser != null && !radioBtnUser.isDisposed()) {
				return getUpdatedWidgetText("Add from site/AdministrationArea/project/user filter", radioBtnUser);
			}
			return CommonConstants.EMPTY_STR;
		});

		registry.register((text) -> {
			if (radioBtnUserGroup != null && !radioBtnUserGroup.isDisposed()) {
				radioBtnUserGroup.setText(text);
			}
		}, (message) -> {
			if (radioBtnUserGroup != null && !radioBtnUserGroup.isDisposed()) {
				return getUpdatedWidgetText("Add from UserGroup filter", radioBtnUserGroup);
			}
			return CommonConstants.EMPTY_STR;
		});

		if (saveBtn != null) {
			registry.register((text) -> {
				if (saveBtn != null && !saveBtn.isDisposed()) {
					saveBtn.setText(text);
				}
			}, (message) -> {
				if (saveBtn != null && !saveBtn.isDisposed()) {
					return getUpdatedWidgetText(message.saveButtonText, saveBtn);
				}
				return CommonConstants.EMPTY_STR;
			});
		}

		if (cancelBtn != null) {
			registry.register((text) -> {
				if (cancelBtn != null && !cancelBtn.isDisposed()) {
					cancelBtn.setText(text);
				}
			}, (message) -> {
				if (cancelBtn != null && !cancelBtn.isDisposed()) {
					return getUpdatedWidgetText(message.cancelButtonText, cancelBtn);
				}
				return CommonConstants.EMPTY_STR;
			});
		}

		registry.register((text) -> {
			if (siteFilterButton != null && !siteFilterButton.isDisposed()) {
				siteFilterButton.setText(text);
			}
		}, (message) -> {
			if (siteFilterButton != null && !siteFilterButton.isDisposed()) {
				return getUpdatedWidgetText(message.allBtnLabel, siteFilterButton);
			}
			return CommonConstants.EMPTY_STR;
		});

		registry.register((text) -> {
			if (adminAreaFilterButton != null && !adminAreaFilterButton.isDisposed()) {
				adminAreaFilterButton.setText(text);
			}
		}, (message) -> {
			if (adminAreaFilterButton != null && !adminAreaFilterButton.isDisposed()) {
				return getUpdatedWidgetText(message.allBtnLabel, adminAreaFilterButton);
			}
			return CommonConstants.EMPTY_STR;
		});
		registry.register((text) -> {
			if (projectFilterButton != null && !projectFilterButton.isDisposed()) {
				projectFilterButton.setText(text);
			}
		}, (message) -> {
			if (projectFilterButton != null && !projectFilterButton.isDisposed()) {
				return getUpdatedWidgetText(message.allBtnLabel, projectFilterButton);
			}
			return CommonConstants.EMPTY_STR;
		});

		registry.register((text) -> {
			if (userFilterButton != null && !userFilterButton.isDisposed()) {
				userFilterButton.setText(text);
			}
		}, (message) -> {
			if (userFilterButton != null && !userFilterButton.isDisposed()) {
				return getUpdatedWidgetText(message.allBtnLabel, userFilterButton);
			}
			return CommonConstants.EMPTY_STR;
		});

		registry.register((text) -> {
			if (userGroupFilterButton != null && !userGroupFilterButton.isDisposed()) {
				userGroupFilterButton.setText(text);
			}
		}, (message) -> {
			if (userGroupFilterButton != null && !userGroupFilterButton.isDisposed()) {
				return getUpdatedWidgetText(message.allBtnLabel, userGroupFilterButton);
			}
			return CommonConstants.EMPTY_STR;
		});

	}

	/**
	 * Bind values.
	 */
	@SuppressWarnings("unchecked")
	public void bindValues() {

		widgetValue = WidgetProperties.text(SWT.Modify).observe(this.txtName);
		modelValue = BeanProperties.value(ProAARelAssignEvtAction.class, ProAARelAssignEvtAction.PROPERTY_NAME)
				.observe(this.proAARelAssignEvtModel);
		bindValue = dataBindContext.bindValue(widgetValue, modelValue);

		widgetValue = WidgetProperties.selection().observe(this.activeBtn);
		modelValue = BeanProperties.value(ProAARelAssignEvtAction.class, ProAARelAssignEvtAction.PROPERTY_ACTIVE)
				.observe(this.proAARelAssignEvtModel);
		bindValue = dataBindContext.bindValue(widgetValue, modelValue);

		widgetValue = WidgetProperties.text(SWT.Modify).observe(this.txtSubject);
		modelValue = BeanProperties.value(ProAARelAssignEvtAction.class, ProAARelAssignEvtAction.PROPERTY_SUBJECT)
				.observe(this.proAARelAssignEvtModel);
		bindValue = dataBindContext.bindValue(widgetValue, modelValue);

		widgetValue = WidgetProperties.text(SWT.Modify).observe(this.txtMessage);
		modelValue = BeanProperties.value(ProAARelAssignEvtAction.class, ProAARelAssignEvtAction.PROPERTY_MESSAGE)
				.observe(this.proAARelAssignEvtModel);
		bindValue = dataBindContext.bindValue(widgetValue, modelValue);

		widgetValue = WidgetProperties.text(SWT.Modify).observe(this.txtDescription);
		modelValue = BeanProperties.value(ProAARelAssignEvtAction.class, ProAARelAssignEvtAction.PROPERTY_DESCRIPTION)
				.observe(this.proAARelAssignEvtModel);
		bindValue = dataBindContext.bindValue(widgetValue, modelValue);

		if (messageInfoDecroator == null) {
			messageInfoDecroator = new ControlDecoration(this.txtMessage, SWT.TOP);
			final Image messageInfoDecoratorImage = FieldDecorationRegistry.getDefault()
					.getFieldDecoration(FieldDecorationRegistry.DEC_INFORMATION).getImage();
			messageInfoDecroator.setImage(messageInfoDecoratorImage);
			messageInfoDecroator.setShowOnlyOnFocus(true);
		}
		// messageInfoDecroator.hide();

		if (subjectInfoDecroator == null) {
			subjectInfoDecroator = new ControlDecoration(this.txtSubject, SWT.TOP);
			final Image messageInfoDecoratorImage = FieldDecorationRegistry.getDefault()
					.getFieldDecoration(FieldDecorationRegistry.DEC_INFORMATION).getImage();
			subjectInfoDecroator.setImage(messageInfoDecoratorImage);
			subjectInfoDecroator.setShowOnlyOnFocus(true);
		}
		// subjectInfoDecroator.hide();

		StyleRange style1 = new StyleRange();
		style1.start = 0;
		style1.length = txtDescription.getText().length();
		style1.fontStyle = SWT.ITALIC;
		txtDescription.setStyleRange(style1);
	}

	
	/**
	 * Save pro AA rel assign action handler.
	 */
	public void saveProAARelAssignActionHandler() {
		// validate the model
		if (!proAARelAssignEvtModel.getName().isEmpty()) {
			if (proAARelAssignEvtModel.getOperationMode() == CommonConstants.OPERATIONMODE.CREATE) {
				createProAARelAssignEvtOperation();
			} else if (proAARelAssignEvtModel.getOperationMode() == CommonConstants.OPERATIONMODE.CHANGE) {
				changeProAARelAssignEvtOperation();
			}
		}
	}

	/**
	 * Change pro AA rel assign evt operation.
	 */
	private void changeProAARelAssignEvtOperation() {

	}

	/**
	 * Create pro AA rel assign evt operation.
	 */
	private void createProAARelAssignEvtOperation() {

	}

	/**
	 * Gets the updated widget text.
	 *
	 * @param message
	 *            the message
	 * @param control
	 *            the control
	 * @return the updated widget text
	 */
	private String getUpdatedWidgetText(final String message, final Control control) {
		control.requestLayout();
		control.getParent().redraw();
		control.getParent().getParent().update();
		control.getParent().getParent().getParent().update();
		return message;
	}

	/**
	 * Set operation mode.
	 */
	public void setOperationMode() {
		if (this.proAARelAssignEvtModel != null) {
			if (proAARelAssignEvtModel.getOperationMode() == CommonConstants.OPERATIONMODE.VIEW) {
				this.txtName.setEditable(false);
				this.activeBtn.setEnabled(false);
				this.txtDescription.setEditable(false);
				this.txtSubject.setEditable(false);
				this.txtMessage.setData("editable", false);
				setShowButtonBar(false);
			} else if (proAARelAssignEvtModel.getOperationMode() == CommonConstants.OPERATIONMODE.CREATE) {
				this.txtName.setEditable(true);
				this.activeBtn.setEnabled(true);
				this.saveBtn.setEnabled(false);
				this.txtDescription.setEditable(true);
				this.txtSubject.setEditable(true);
				this.txtMessage.setData("editable", true);
				setShowButtonBar(true);
				this.dirty.setDirty(true);
			} else if (proAARelAssignEvtModel.getOperationMode() == CommonConstants.OPERATIONMODE.CHANGE) {
				this.txtName.setEditable(true);
				this.activeBtn.setEnabled(true);
				this.txtMessage.setData("editable", true);
				this.txtDescription.setEditable(true);
				this.txtSubject.setEditable(true);
				setShowButtonBar(true);
				this.dirty.setDirty(true);
			} else {
				this.txtName.setEditable(false);
				this.activeBtn.setEnabled(false);
				this.txtDescription.setEditable(false);
				this.txtSubject.setEditable(false);
				this.txtMessage.setData("editable", false);
				setShowButtonBar(false);
			}
		}
	}

	/**
	 * 7 * Sets the dirty object.
	 *
	 * @param dirty
	 *            the new dirty object
	 */
	public void setDirtyObject(final MDirtyable dirty) {
		this.dirty = dirty;
	}

	/**
	 * Set project AA rel assign evt action.
	 */
	public void setProjectAARelAssignEvtAction() {
		try {
			final AdminTreeviewer adminTree = XMAdminUtil.getInstance().getAdminTree();
			final Object selectionObj = adminTree.getSelection();
			if (selectionObj instanceof IStructuredSelection) {
				Object firstElement = ((IStructuredSelection) selectionObj).getFirstElement();
				if (firstElement instanceof ProAARelAssignEvtAction) {
					setOldModel((ProAARelAssignEvtAction) firstElement);
					ProAARelAssignEvtAction rightHandObject = (ProAARelAssignEvtAction) this.getOldModel()
							.deepCopyProAARelAssignEvtAction(false, null);
					setProAARelAssignEvtModel(rightHandObject);
					registerMessages(this.registry);
					bindValues();
					setOperationMode();
				}
			}
		} catch (Exception e) {
			LOGGER.warn("Unable to set projectAARelAssignAction model selection ! " + e);
		}
	}

	/**
	 * Sets the model.
	 *
	 * @param proAARelAssignEvtAction
	 *            the new model
	 */
	public void setModel(ProAARelAssignEvtAction proAARelAssignEvtAction) {
		try {
			setProAARelAssignEvtModel(proAARelAssignEvtAction);
			registerMessages(this.registry);
			bindValues();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Gets the pro AA rel assign evt model.
	 *
	 * @return the pro AA rel assign evt model
	 */
	public ProAARelAssignEvtAction getProAARelAssignEvtModel() {
		return proAARelAssignEvtModel;
	}

	/**
	 * Sets the pro AA rel assign evt model.
	 *
	 * @param proAARelAssignEvtModel
	 *            the new pro AA rel assign evt model
	 */
	public void setProAARelAssignEvtModel(ProAARelAssignEvtAction proAARelAssignEvtModel) {
		this.proAARelAssignEvtModel = proAARelAssignEvtModel;
	}

	/**
	 * Gets the old model.
	 *
	 * @return the old model
	 */
	public ProAARelAssignEvtAction getOldModel() {
		return oldModel;
	}

	/**
	 * Sets the old model.
	 *
	 * @param oldModel
	 *            the new old model
	 */
	public void setOldModel(ProAARelAssignEvtAction oldModel) {
		this.oldModel = oldModel;
	}
}
