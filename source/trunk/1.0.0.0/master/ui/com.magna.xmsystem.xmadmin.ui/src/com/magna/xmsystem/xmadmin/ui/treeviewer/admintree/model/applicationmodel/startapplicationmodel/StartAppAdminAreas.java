package com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.startapplicationmodel;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.stream.Collectors;

import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.adminarea.AdministrationArea;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.refs.RelationObj;

/**
 * Class for Start app admin areas.
 *
 * @author Chiranjeevi.Akula
 */
public class StartAppAdminAreas implements IAdminTreeChild {

	/** Member variable 'parent' for {@link IAdminTreeChild}. */
	private IAdminTreeChild parent;

	/** Member variable 'start app admin areas children' for {@link Map<String,IAdminTreeChild>}. */
	private Map<String, IAdminTreeChild> startAppAdminAreasChildren;

	/**
	 * Constructor for StartAppAdminAreas Class.
	 *
	 * @param parent {@link IAdminTreeChild}
	 */
	public StartAppAdminAreas(IAdminTreeChild parent) {
		this.parent = parent;
		this.startAppAdminAreasChildren = new LinkedHashMap<>();
	}
	
	/**
	 * Method for Adds the.
	 *
	 * @param startAppAdminAreaId {@link String}
	 * @param child {@link IAdminTreeChild}
	 * @return the i admin tree child {@link IAdminTreeChild}
	 */
	public IAdminTreeChild add(final String startAppAdminAreaId, final IAdminTreeChild child) {
		child.setParent(this);
		IAdminTreeChild returnVal = this.startAppAdminAreasChildren.put(startAppAdminAreaId, child);
		if (child instanceof RelationObj && ((RelationObj) child).getRefObject() instanceof AdministrationArea) {
			sort();
		}
		return returnVal;
	}

	/**
	 * Method for Removes the.
	 *
	 * @param startAppAdminAreaId {@link String}
	 * @return the i admin tree child {@link IAdminTreeChild}
	 */
	public IAdminTreeChild remove(final String startAppAdminAreaId) {
		return this.startAppAdminAreasChildren.remove(startAppAdminAreaId);
	}

	/**
	 * Method for Removes the all.
	 */
	public void removeAll() {
		this.startAppAdminAreasChildren.clear();
	}
	
	/**
	 * Gets the start app admin areas children collection.
	 *
	 * @return the start app admin areas children collection
	 */
	public Collection<IAdminTreeChild> getStartAppAdminAreasChildrenCollection() {
		return this.startAppAdminAreasChildren.values();
	}

	/**
	 * Gets the start app admin areas children.
	 *
	 * @return the start app admin areas children
	 */
	public Map<String, IAdminTreeChild> getStartAppAdminAreasChildren() {
		return startAppAdminAreasChildren;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild#
	 * getParent()
	 */
	@Override
	public IAdminTreeChild getParent() {
		return this.parent;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild#
	 * setParent(com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.
	 * IAdminTreeChild)
	 */
	@Override
	public void setParent(IAdminTreeChild parent) {
		this.parent = parent;

	}
	/**
	 * Sort.
	 */
	public void sort() {
		final LinkedHashMap<String, IAdminTreeChild> collect = this.startAppAdminAreasChildren.entrySet().stream().sorted(
				(e1, e2) -> ((AdministrationArea) e1.getValue()).getName().compareTo(((AdministrationArea) e2.getValue()).getName()))
				.collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e1, LinkedHashMap::new));
		this.startAppAdminAreasChildren = collect;
	}


}
