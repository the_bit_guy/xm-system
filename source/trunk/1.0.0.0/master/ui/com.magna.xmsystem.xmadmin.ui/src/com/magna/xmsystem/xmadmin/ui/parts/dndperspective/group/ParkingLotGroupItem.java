package com.magna.xmsystem.xmadmin.ui.parts.dndperspective.group;

import org.eclipse.nebula.widgets.pgroup.PGroup;
import org.eclipse.nebula.widgets.pgroup.PGroupToolItem;

/**
 * The Class ParkingLotGroupItem.
 * 
 * @author shashwat.anand
 */
public class ParkingLotGroupItem extends PGroupToolItem {
	
	/** The parent. */
	private PGroup parent;

	/**
	 * Instantiates a new parking lot group item.
	 *
	 * @param parent the parent
	 * @param style the style
	 */
	public ParkingLotGroupItem(final PGroup parent, final int style) {
		super(parent, style);
		this.parent = parent;
	}

	/**
	 * Gets the parent.
	 *
	 * @return the parent
	 */
	public PGroup getParent() {
		return parent;
	}
}
