package com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.users;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;

import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.refs.RelationObj;

/**
 * The Class UserProjectChild.
 * 
 * @author subash.janarthanan
 * 
 */
public class UserProjectChild implements IAdminTreeChild {
	/** The parent. */
	private IAdminTreeChild parent;

	/** The User project children. */
	private Map<String, IAdminTreeChild> userProjectChildren;

	/**
	 * Instantiates a new user project child.
	 *
	 * @param name
	 *            the name
	 */
	public UserProjectChild(final IAdminTreeChild parent) {
		this.parent = parent;
		this.userProjectChildren = new LinkedHashMap<>();
		//this.userProjectChildren.put(UserProjectChildrenEnum.PROJECTAPPLICATIONS, new UserProjectApplications(this));
	}

	/**
	 * Method for Adds the fixed children.
	 *
	 * @param siteAdministrationChildRel {@link RelationObj}
	 */
	public void addFixedChildren(RelationObj relationObj) {
		this.userProjectChildren.put(UserProjectAdminAreas.class.getName(), new UserProjectAdminAreas(relationObj));
	}
	
	/**
	 * Gets the user project children collection.
	 *
	 * @return the user project children collection
	 */
	public Collection<IAdminTreeChild> getUserProjectChildrenCollection() {
		return this.userProjectChildren.values();
	}

	/**
	 * Gets the user project children children.
	 *
	 * @return the user project children children
	 */
	public Map<String, IAdminTreeChild> getUserProjectChildren() {
		return userProjectChildren;
	}
	
	/**
	 * Returns null as its parent
	 * @see com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild#getParent()
	 */
	@Override
	public IAdminTreeChild getParent() {
		return this.parent;
	}

	/**
	 * @see com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild#setParent(com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild)
	 */
	@Override
	public void setParent(final IAdminTreeChild parent) {
		this.parent = parent;
	}

}
