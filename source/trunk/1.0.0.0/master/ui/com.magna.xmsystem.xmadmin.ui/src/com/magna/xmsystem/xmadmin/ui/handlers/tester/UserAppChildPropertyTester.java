package com.magna.xmsystem.xmadmin.ui.handlers.tester;

import org.eclipse.core.expressions.PropertyTester;

import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.userapplicationmodel.UserApplicationChild;

// TODO: Auto-generated Javadoc
/**
 * The Class UserAppChildPropertyTester.
 * 
 * @author archita.patel
 */
public class UserAppChildPropertyTester extends PropertyTester {

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.core.expressions.IPropertyTester#test(java.lang.Object,
	 * java.lang.String, java.lang.Object[], java.lang.Object)
	 */
	@Override
	public boolean test(Object receiver, String arg1, Object[] arg2, Object arg3) {
		if (receiver instanceof UserApplicationChild) {
			return true;
		} else {
			return false;
		}
	}

}
