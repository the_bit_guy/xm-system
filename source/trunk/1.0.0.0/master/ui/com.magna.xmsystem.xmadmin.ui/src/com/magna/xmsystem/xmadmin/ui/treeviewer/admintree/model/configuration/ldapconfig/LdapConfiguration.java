package com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.ldapconfig;

import java.beans.PropertyChangeEvent;

import com.magna.xmsystem.dependencies.utils.XMSystemUtil;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.BeanModel;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild;
import com.magna.xmsystem.xmadmin.util.CommonConstants;

// TODO: Auto-generated Javadoc
/**
 * The Class LdapConfiguration.
 * 
 * @author archita.patel
 */
public class LdapConfiguration extends BeanModel implements IAdminTreeChild {
	
	/** The Constant PROPERTY_LDAP_ID. */
	public static final String PROPERTY_LDAP_ID = "ldapConfigId"; //$NON-NLS-1$

	/** The Constant PROPERTY_LDAP_URL. */
	public static final String PROPERTY_LDAP_URL = "ldapUrl"; //$NON-NLS-1$

	/** The Constant PROPERTY_LDAP_PORT. */
	public static final String PROPERTY_LDAP_PORT = "ldapPort"; //$NON-NLS-1$

	/** The Constant PROPERTY_LDAP_PORT. */
	public static final String PROPERTY_LDAP_USERNAME = "ldapUserName"; //$NON-NLS-1$

	/** The Constant PROPERTY_LDAP_PORT. */
	public static final String PROPERTY_LDAP_PASSWORD = "ldapPassword"; //$NON-NLS-1$

	/** The Constant PROPERTY_LDAP_PORT. */
	public static final String PROPERTY_LDAP_BASE = "ldapBase"; //$NON-NLS-1$

	/** The Constant PROPERTY_OPERATIONMODE. */
	public static final String PROPERTY_OPERATIONMODE = "operationMode"; //$NON-NLS-1$

	/** The Constant PORT_LIMIT. */
	public static final int PORT_LIMIT = 5;
	
	/**
	 * Constant variable for name text limit
	 */
	public static final int NAME_LIMIT = 30;

	/** The ldap config id. */
	private String ldapConfigId;

	/** The ldap local host. */
	private String ldapUrl;

	/** The ldap search. */
	private String ldapPort;

	/** The ldap user name. */
	private String ldapUserName;

	/** The ldap password. */
	private String ldapPassword;

	/** The ldap base. */
	private String ldapBase;

	/** The operation mode. */
	private int operationMode;

	/**
	 * Instantiates a new ldap configuration.
	 */
	public LdapConfiguration() {

	}

	/**
	 * Instantiates a new ldap configuration.
	 *
	 * @param ldapConfigId
	 *            the ldap config id
	 * @param operationMode
	 *            the operation mode
	 */
	public LdapConfiguration(final String ldapConfigId, final int operationMode) {
		this(ldapConfigId, null, null, null, null, null, operationMode);
	}

	/**
	 * Instantiates a new ldap configuration.
	 *
	 * @param ldapConfigId
	 *            the ldap config id
	 * @param ldapUrl
	 *            the ldap url
	 * @param ldapPort
	 *            the ldap port
	 * @param operationMode
	 *            the operation mode
	 */
	public LdapConfiguration(final String ldapConfigId, final String ldapUrl, String ldapPort, String ldapUserName,
			String ldapPassword, String ldapBase, final int operationMode) {
		super();
		this.ldapConfigId = ldapConfigId;
		this.ldapUrl = ldapUrl;
		this.ldapPort = ldapPort;
		this.ldapUserName = ldapUserName;
		this.ldapPassword = ldapPassword;
		this.ldapBase = ldapBase;
		this.operationMode = operationMode;

	}

	/**
	 * Gets the ldap config id.
	 *
	 * @return the ldap config id
	 */
	public String getLdapConfigId() {
		return ldapConfigId;
	}

	/**
	 * Sets the ldap config id.
	 *
	 * @param ldapConfigId
	 *            the new ldap config id
	 */
	public void setLdapConfigId(String ldapConfigId) {
		if (ldapConfigId == null)
			throw new IllegalArgumentException();
		this.propertyChangeSupport.firePropertyChange(PROPERTY_LDAP_ID, this.ldapConfigId,
				this.ldapConfigId = ldapConfigId);
		// this.ldapConfigId = ldapConfigId;
	}

	/**
	 * Gets the ldap url.
	 *
	 * @return the ldap url
	 */
	public String getLdapUrl() {
		return ldapUrl;
	}

	/**
	 * Sets the ldap url.
	 *
	 * @param ldapUrl
	 *            the new ldap url
	 */
	public void setLdapUrl(String ldapUrl) {
		if (ldapUrl == null)
			throw new IllegalArgumentException();
		this.propertyChangeSupport.firePropertyChange(PROPERTY_LDAP_URL, this.ldapUrl, this.ldapUrl = ldapUrl);
		// this.ldapUrl = ldapUrl;
	}

	/**
	 * Gets the ldap port.
	 *
	 * @return the ldap port
	 */
	public String getLdapPort() {
		return ldapPort;
	}

	/**
	 * Sets the ldap port.
	 *
	 * @param ldapPort
	 *            the new ldap port
	 */
	public void setLdapPort(String ldapPort) {
		if (ldapPort == null)
			throw new IllegalArgumentException();
		this.propertyChangeSupport.firePropertyChange(PROPERTY_LDAP_PORT, this.ldapPort, this.ldapPort = ldapPort);
		// this.ldapPort = ldapPort;
	}

	/**
	 * Gets the ldap user name.
	 *
	 * @return the ldap user name
	 */
	public String getLdapUserName() {
		return ldapUserName;
	}

	public void setLdapUserName(String ldapUserName) {
		if (ldapUserName == null)
			throw new IllegalArgumentException();
		this.propertyChangeSupport.firePropertyChange(PROPERTY_LDAP_USERNAME, this.ldapUserName,
				this.ldapUserName = ldapUserName);
	}

	/**
	 * Gets the ldap password.
	 *
	 * @return the ldap password
	 */
	public String getLdapPassword() {
		return ldapPassword;
	}

	public void setLdapPassword(String ldapPassword) {
		if (ldapPassword == null)
			throw new IllegalArgumentException();
		this.propertyChangeSupport.firePropertyChange(PROPERTY_LDAP_PASSWORD, this.ldapPassword,
				this.ldapPassword = ldapPassword);
	}

	/**
	 * Gets the ldap base.
	 *
	 * @return the ldap base
	 */
	public String getLdapBase() {
		return ldapBase;
	}

	/**
	 * Sets the ldap base.
	 *
	 * @param ldapBase
	 *            the new ldap base
	 */
	public void setLdapBase(String ldapBase) {
		if (ldapBase == null)
			throw new IllegalArgumentException();
		this.propertyChangeSupport.firePropertyChange(PROPERTY_LDAP_BASE, this.ldapBase, this.ldapBase = ldapBase);
	}

	/**
	 * Gets the operation mode.
	 *
	 * @return the operation mode
	 */
	public int getOperationMode() {
		return operationMode;
	}

	/**
	 * Sets the operation mode.
	 *
	 * @param operationMode
	 *            the new operation mode
	 */
	public void setOperationMode(int operationMode) {
		this.propertyChangeSupport.firePropertyChange(PROPERTY_OPERATIONMODE, this.operationMode,
				this.operationMode = operationMode);
		// this.operationMode = operationMode;
	}

	/**
	 * Deep copy ldap config model.
	 *
	 * @param update
	 *            the update
	 * @param updateThisObject
	 *            the update this object
	 * @return the ldap configuration
	 */
	public LdapConfiguration deepCopyLdapConfigModel(boolean update, LdapConfiguration updateThisObject) {

		LdapConfiguration clonedLdapConfig = null;
		try {
			String currentLdapConfigId = XMSystemUtil.isEmpty(this.getLdapConfigId()) ? CommonConstants.EMPTY_STR
					: this.getLdapConfigId();

			String currentLdapUrl = XMSystemUtil.isEmpty(this.getLdapUrl()) ? CommonConstants.EMPTY_STR
					: this.getLdapUrl();

			String currentLdapPort = XMSystemUtil.isEmpty(this.getLdapPort()) ? CommonConstants.EMPTY_STR
					: this.getLdapPort();

			String currentLdapUserName = XMSystemUtil.isEmpty(this.getLdapUserName()) ? CommonConstants.EMPTY_STR
					: this.getLdapUserName();

			String currentLdapPassword = XMSystemUtil.isEmpty(this.getLdapPassword()) ? CommonConstants.EMPTY_STR
					: this.getLdapPassword();

			String currentLdapBase = XMSystemUtil.isEmpty(this.getLdapBase()) ? CommonConstants.EMPTY_STR
					: this.getLdapBase();

			if (update) {
				clonedLdapConfig = updateThisObject;
			} else {
				clonedLdapConfig = new LdapConfiguration(currentLdapConfigId, currentLdapUrl, currentLdapPort,
						currentLdapUserName, currentLdapPassword, currentLdapBase, CommonConstants.OPERATIONMODE.VIEW);
			}
			clonedLdapConfig.setLdapConfigId(currentLdapConfigId);
			clonedLdapConfig.setLdapUrl(currentLdapUrl);
			clonedLdapConfig.setLdapPort(currentLdapPort);
			clonedLdapConfig.setLdapUserName(currentLdapUserName);
			clonedLdapConfig.setLdapPassword(currentLdapPassword);
			clonedLdapConfig.setLdapBase(currentLdapBase);
			return clonedLdapConfig;
		} catch (Exception e) {
			return null;
		}

	}

	/**
	 * Property change.
	 *
	 * @param event
	 *            the event
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.BeanModel#
	 * propertyChange(java.beans.PropertyChangeEvent)
	 */
	@Override
	public void propertyChange(PropertyChangeEvent event) {
		this.propertyChangeSupport.firePropertyChange(event.getPropertyName(), event.getOldValue(),
				event.getNewValue());
	}

}
