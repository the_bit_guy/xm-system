package com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.notification;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.stream.Collectors;

import com.magna.xmbackend.vo.enums.NotificationEventType;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild;

/**
 * The Class ProjectCreateEvt.
 * 
 * @author shashwat.anand
 */
public class ProjectCreateEvt extends INotificationEvent {
	
	/** The Project create evt child. */
	private Map<String, IAdminTreeChild> projectCreateEvtChild;
	
	/** The parent. */
	private IAdminTreeChild parent;
	
	/**
	 * Instantiates a new project create evt.
	 */
	public ProjectCreateEvt() {
		this.parent = null;
		this.eventType = NotificationEventType.PROJECT_CREATE;
		this.projectCreateEvtChild = new LinkedHashMap<>();
	}
	
	/**
	 * Add.
	 *
	 * @param id the id
	 * @param child the child
	 * @return the i admin tree child
	 */
	public IAdminTreeChild add(final String id, final IAdminTreeChild child) {
		child.setParent(this);
		IAdminTreeChild returnVal = this.projectCreateEvtChild.put(id, child);
		sort();
		return returnVal;
	}

	/**
	 * Remove all.
	 */
	public void removeAll() {
		this.projectCreateEvtChild.clear();
	}

	/* (non-Javadoc)
	 * @see com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild#getParent()
	 */
	@Override
	public IAdminTreeChild getParent() {
		return this.parent;
	}

	/* (non-Javadoc)
	 * @see com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild#setParent(com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild)
	 */
	@Override
	public void setParent(IAdminTreeChild parent) {
		this.parent = parent;
	}

	/**
	 * Gets the project create evt child.
	 *
	 * @return the project create evt child
	 */
	public Map<String, IAdminTreeChild> getProjectCreateEvtChild() {
		return projectCreateEvtChild;
	}

	/**
	 * Set project create evt child.
	 *
	 * @param projectCreateEvtChild the project create evt child
	 */
	public void setProjectCreateEvtChild(Map<String, IAdminTreeChild> projectCreateEvtChild) {
		this.projectCreateEvtChild = projectCreateEvtChild;
	}
	
	/**
	 * Gets the project create evt collection.
	 *
	 * @return the project create evt collection
	 */
	public Collection<IAdminTreeChild> getProjectCreateEvtCollection() {
		return this.projectCreateEvtChild.values();
	}
	
	/**
	 * Sort.
	 */
	public void sort() {
		final LinkedHashMap<String, IAdminTreeChild> collect = this.projectCreateEvtChild.entrySet().stream().sorted(
				(e1, e2) -> ((ProjectCreateEvtAction) e1.getValue()).getName().compareTo(((ProjectCreateEvtAction) e2.getValue()).getName()))
				.collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e1, LinkedHashMap::new));
		this.projectCreateEvtChild = collect;
	}
}
