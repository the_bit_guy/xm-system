package com.magna.xmsystem.xmadmin.ui.parts.userproexpgrace;

import org.eclipse.jface.layout.GridDataFactory;
import org.eclipse.jface.layout.GridLayoutFactory;
import org.eclipse.jface.resource.JFaceResources;
import org.eclipse.jface.resource.LocalResourceManager;
import org.eclipse.jface.resource.ResourceManager;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.ScrolledComposite;
import org.eclipse.swt.custom.StyledText;
import org.eclipse.swt.events.ControlAdapter;
import org.eclipse.swt.events.ControlEvent;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.notification.UserProExpGraceEvt;
import com.magna.xmsystem.xmadmin.util.XMAdminUtil;

// TODO: Auto-generated Javadoc
/**
 * The Class UserProjectExpGraceCompositeUI.
 */
public class UserProjectExpGraceCompositeUI extends Composite {

	/** The Constant LOGGER. */
	private static final Logger LOGGER = LoggerFactory.getLogger(UserProjectExpGraceCompositeUI.class);

	/** The resource manager. */
	protected ResourceManager resourceManager;

	/** The grp user pro exp grace. */
	protected Group grpUserProExpGrace;

	/** The lbl description. */
	protected Label lblDescription;

	/** The txt description. */
	protected StyledText txtDescription;

	/** The lbl subject. */
	protected Label lblSubject;

	/** The txt subject. */
	protected Text txtSubject;

	/** The lbl message. */
	protected Label lblMessage;

	/** The txt message. */
	protected Text txtMessage;

	/** The base scrolled composite. */
	protected ScrolledComposite baseScrolledComposite;

	/** The save btn. */
	protected Button saveBtn;

	/** The cancel btn. */
	protected Button cancelBtn;

	/** The manager CC btn. */
	protected Button managerCCBtn;

	/**
	 * Instantiates a new user project exp grace composite UI.
	 *
	 * @param parent
	 *            the parent
	 * @param style
	 *            the style
	 */
	public UserProjectExpGraceCompositeUI(final Composite parent, final int style) {
		super(parent, style);
		this.resourceManager = new LocalResourceManager(JFaceResources.getResources(), this);
		this.initGUI();
	}

	/**
	 * Init GUI.
	 */
	private void initGUI() {
		try {
			GridLayoutFactory.fillDefaults().applyTo(this);
			this.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));

			this.grpUserProExpGrace = new Group(this, SWT.NONE);
			this.grpUserProExpGrace.setBackgroundMode(SWT.INHERIT_FORCE);
			GridLayoutFactory.fillDefaults().numColumns(1).applyTo(this.grpUserProExpGrace);
			GridDataFactory.fillDefaults().grab(true, true).span(SWT.FILL, SWT.FILL).applyTo(this.grpUserProExpGrace);
			baseScrolledComposite = XMAdminUtil.getInstance().createScrolledComposite(this.grpUserProExpGrace);
			baseScrolledComposite.setBackgroundMode(SWT.INHERIT_FORCE);

			final Composite widgetContainer = new Composite(baseScrolledComposite, SWT.NONE);
			final GridLayout widgetContLayout = new GridLayout(3, false);

			widgetContainer.setLayout(widgetContLayout);
			widgetContainer.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));

			this.lblDescription = new Label(widgetContainer, SWT.NONE);
			this.lblDescription.setLayoutData(new GridData(SWT.LEFT, SWT.TOP, false, false));

			this.txtDescription = new StyledText(widgetContainer,
					SWT.BORDER | SWT.WRAP | SWT.MULTI | SWT.H_SCROLL | SWT.V_SCROLL);
			GridData gridData = new GridData(SWT.FILL, SWT.FILL, true, true);
			gridData.minimumHeight = 10;
			gridData.horizontalSpan = 2;
			this.txtDescription.setLayoutData(gridData);
			this.txtDescription.setEditable(false);
			this.txtDescription.setTextLimit(UserProExpGraceEvt.DESC_LIMIT);

			baseScrolledComposite.setContent(widgetContainer);
			baseScrolledComposite.setSize(widgetContainer.getSize());
			baseScrolledComposite.setExpandVertical(true);
			baseScrolledComposite.setExpandHorizontal(true);
			baseScrolledComposite.update();

			baseScrolledComposite.addControlListener(new ControlAdapter() {
				public void controlResized(final ControlEvent e) {
					Rectangle rectangle = baseScrolledComposite.getClientArea();
					baseScrolledComposite.setMinSize(widgetContainer.computeSize(rectangle.width, SWT.DEFAULT));
				}
			});

			this.lblSubject = new Label(widgetContainer, SWT.NONE);
			GridDataFactory.fillDefaults().grab(false, false).span(1, 1).align(SWT.LEFT, SWT.CENTER)
					.applyTo(this.lblSubject);

			this.txtSubject = new Text(widgetContainer, SWT.BORDER);
			this.txtSubject.setTextLimit(UserProExpGraceEvt.SUB_LIMIT);
			GridDataFactory.fillDefaults().grab(true, false).span(2, 1).align(SWT.FILL, SWT.CENTER)
					.applyTo(this.txtSubject);

			this.lblMessage = new Label(widgetContainer, SWT.NONE);
			GridDataFactory.fillDefaults().grab(false, false).span(1, 1).align(SWT.LEFT, SWT.TOP)
					.applyTo(this.lblMessage);

			this.txtMessage = new Text(widgetContainer,
					SWT.BORDER | SWT.WRAP | SWT.MULTI | SWT.H_SCROLL | SWT.V_SCROLL);
			gridData = new GridData(SWT.FILL, SWT.FILL, true, true);
			gridData.minimumHeight = 150;
			gridData.horizontalSpan = 2;
			this.txtMessage.setLayoutData(gridData);
			this.txtMessage.setTextLimit(UserProExpGraceEvt.MESSAGE_LIMIT);
			
			Label emptyLbl_radionBtn = new Label(widgetContainer, SWT.NONE);
			gridData = new GridData();
			emptyLbl_radionBtn.setLayoutData(gridData);
			
			this.managerCCBtn = new Button(widgetContainer, SWT.CHECK);
			GridDataFactory.fillDefaults().grab(true, false).span(2, 1).align(SWT.FILL, SWT.CENTER)
					.applyTo(this.managerCCBtn);

			final Composite buttonBarComp = new Composite(this.grpUserProExpGrace, SWT.NONE);
			final GridLayout btnBarCompLayout = new GridLayout(2, true);
			btnBarCompLayout.marginRight = 0;
			btnBarCompLayout.marginLeft = 0;
			btnBarCompLayout.marginTop = 0;
			btnBarCompLayout.marginBottom = 0;
			btnBarCompLayout.marginWidth = 0;
			buttonBarComp.setLayout(btnBarCompLayout);
			buttonBarComp.setLayoutData(new GridData(SWT.FILL, SWT.DEFAULT, true, false, 3, 1));
			createButtonBar(buttonBarComp);

		} catch (Exception e) {
			LOGGER.error("Unable to crete UI elements", e); //$NON-NLS-1$
		}

	}

	/**
	 * Create button bar.
	 *
	 * @param buttonBarComp
	 *            the button bar comp
	 */
	private void createButtonBar(final Composite buttonBarComp) {
		this.saveBtn = new Button(buttonBarComp, SWT.NONE);
		this.saveBtn.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, true, false));

		this.cancelBtn = new Button(buttonBarComp, SWT.NONE);
		this.cancelBtn.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, true, false));
	}

	/**
	 * Sets the show button bar.
	 *
	 * @param showButtonBar
	 *            the new show button bar
	 */
	protected void setShowButtonBar(final boolean showButtonBar) {
		if (this.saveBtn != null && !this.saveBtn.isDisposed() && this.cancelBtn != null
				&& !this.cancelBtn.isDisposed()) {
			final GridData layoutData = (GridData) this.saveBtn.getParent().getLayoutData();
			layoutData.exclude = !showButtonBar;
			this.saveBtn.setVisible(showButtonBar);
			this.cancelBtn.setVisible(showButtonBar);
			this.saveBtn.getParent().setVisible(showButtonBar);
			this.saveBtn.getParent().requestLayout();
			this.saveBtn.getParent().redraw();
			this.saveBtn.getParent().getParent().update();
		}
	}
}
