package com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.sitemodel;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;

import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.refs.RelationObj;


/**
 * The Class SiteAdministrationChild.
 * 
 * @author subash.janarthanan
 * 
 */
public class SiteAdministrationChild implements IAdminTreeChild {

	/** The parent. */
	private IAdminTreeChild parent;

	/** The site admin area children. */
	final private Map<String, IAdminTreeChild> siteAdminAreaChildren;

	/** The name. */
	private String name;

	public SiteAdministrationChild(IAdminTreeChild parent) {
		this.parent = parent;
		this.siteAdminAreaChildren = new LinkedHashMap<>();
		/*this.siteAdminAreaChildren.put(SiteAdminAreaChildEnum.PROJECTS, new SiteAdminAreaProjects(this));
		this.siteAdminAreaChildren.put(SiteAdminAreaChildEnum.USERAPPLICATIONS,
				new SiteAdminAreaUserApplications(this));
		this.siteAdminAreaChildren.put(SiteAdminAreaChildEnum.STARTAPPLICATIONS,
				new SiteAdminAreaStartApplications(this));
		this.siteAdminAreaChildren.put(SiteAdminAreaChildEnum.INFORMATION, new SiteAdminAreaInformations(this));*/
	}

	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Sets the name.
	 *
	 * @param name
	 *            the new name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Gets the site admin areas collection.
	 *
	 * @return the site admin areas collection
	 */
	public Collection<IAdminTreeChild> getSiteAdminAreasCollection() {
		return this.siteAdminAreaChildren.values();
	}

	/**
	 * Gets the site admin area children.
	 *
	 * @return the site admin area children
	 */
	public Map<String, IAdminTreeChild> getSiteAdminAreaChildren() {
		return siteAdminAreaChildren;
	}

	/**
	 * Method for Adds the.
	 *
	 * @param siteAdminAreaChildId {@link String}
	 * @param child {@link IAdminTreeChild}
	 * @return the i admin tree child {@link IAdminTreeChild}
	 */
	public IAdminTreeChild add(final String siteAdminAreaChildId, final IAdminTreeChild child) {
		child.setParent(this);
		return this.siteAdminAreaChildren.put(siteAdminAreaChildId, child);
	}
	
	/**
	 * Method for Removes the.
	 *
	 * @param siteAdminAreaChildId {@link String}
	 * @return the i admin tree child {@link IAdminTreeChild}
	 */
	public IAdminTreeChild remove(final String siteAdminAreaChildId) {
		return this.siteAdminAreaChildren.remove(siteAdminAreaChildId);
	}
	
	/**
	 * Method for Removes the all.
	 */
	public void removeAll() {
		this.siteAdminAreaChildren.clear();
	}
	
	/**
	 * Method for Adds the fixed children.
	 *
	 * @param siteAdministrationChildRel {@link RelationObj}
	 */
	public void addFixedChildren(RelationObj siteAdministrationChildRel) {
		siteAdminAreaChildren.put(SiteAdminAreaProjects.class.getName(), new SiteAdminAreaProjects(siteAdministrationChildRel));
		siteAdminAreaChildren.put(SiteAdminAreaUserApplications.class.getName(), new SiteAdminAreaUserApplications(siteAdministrationChildRel));
		siteAdminAreaChildren.put(SiteAdminAreaStartApplications.class.getName(), new SiteAdminAreaStartApplications(siteAdministrationChildRel));
	}
	/*
	 * (non-Javadoc)
	 * 
	 * @see com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.BeanModel#
	 * getParent()
	 */
	@Override
	public IAdminTreeChild getParent() {
		return this.parent;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.BeanModel#
	 * setParent(com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.
	 * IAdminTreeChild)
	 */
	@Override
	public void setParent(final IAdminTreeChild parent) {
		this.parent = parent;
	}
	
	

}
