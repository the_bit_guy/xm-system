package com.magna.xmsystem.xmadmin.ui.parts.notificationtemplate;

import java.util.ArrayList;
import java.util.List;
import java.util.TreeSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.inject.Inject;

import org.eclipse.core.databinding.Binding;
import org.eclipse.core.databinding.DataBindingContext;
import org.eclipse.core.databinding.beans.BeanProperties;
import org.eclipse.core.databinding.observable.value.IObservableValue;
import org.eclipse.core.databinding.observable.value.IValueChangeListener;
import org.eclipse.core.databinding.observable.value.ValueChangeEvent;
import org.eclipse.e4.core.services.nls.Translation;
import org.eclipse.e4.ui.model.application.ui.MDirtyable;
import org.eclipse.jface.databinding.swt.WidgetProperties;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.viewers.TreePath;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.client.ResourceAccessException;

import com.magna.xmbackend.entities.EmailTemplateTbl;
import com.magna.xmbackend.vo.enums.NotificationEventType;
import com.magna.xmbackend.vo.enums.NotificationVariables;
import com.magna.xmbackend.vo.notification.EmailTemplateRequest;
import com.magna.xmsystem.dependencies.customExceptions.UnauthorizedAccessException;
import com.magna.xmsystem.dependencies.utils.XMSystemUtil;
import com.magna.xmsystem.ui.controls.widgets.CustomMessageDialog;
import com.magna.xmsystem.xmadmin.message.Message;
import com.magna.xmsystem.xmadmin.message.MessageRegistry;
import com.magna.xmsystem.xmadmin.restclient.notify.NotificationController;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.AdminTreeviewer;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.AdminTreeFactory;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.notification.NotificationTemplate;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.notification.NotificationTemplates;
import com.magna.xmsystem.xmadmin.util.CommonConstants;
import com.magna.xmsystem.xmadmin.util.MessageType;
import com.magna.xmsystem.xmadmin.util.XMAdminUtil;

// TODO: Auto-generated Javadoc
/**
 * The Class TemplateCompositeAction.
 */
public class TemplateCompositeAction extends TemplateCompositeUI {

	/** The Constant LOGGER. */
	private static final Logger LOGGER = LoggerFactory.getLogger(TemplateCompositeAction.class);

	/** The template model. */
	private NotificationTemplate templateModel;

	/** The old model. */
	private NotificationTemplate oldModel;

	/** The registry. */
	@Inject
	private MessageRegistry registry;

	/** The messages. */
	@Inject
	@Translation
	transient private Message messages;

	/** The widget value. */
	transient private IObservableValue<?> widgetValue;

	/** The model value. */
	transient private IObservableValue<?> modelValue;

	/** The bind value. */
	@SuppressWarnings("unused")
	transient private Binding bindValue;

	/** The data bind context. */
	final transient DataBindingContext dataBindContext = new DataBindingContext();

	/** The dirty. */
	private MDirtyable dirty;

	/**
	 * Instantiates a new template composite action.
	 *
	 * @param parent
	 *            the parent
	 */
	@Inject
	public TemplateCompositeAction(Composite parent) {
		super(parent, SWT.NONE);
		initListner();
	}

	/**
	 * Init listner.
	 */
	private void initListner() {
		this.eventList.addSelectionListener(new SelectionAdapter() {

			@Override
			public void widgetSelected(SelectionEvent event) {
				updateVariableText();
			}
		});

		if (this.saveBtn != null) {
			this.saveBtn.addSelectionListener(new SelectionAdapter() {

				/**
				 * Save button handler
				 */
				@Override
				public void widgetSelected(final SelectionEvent event) {
					saveTemplateHandler();
				}
			});
		}
		
		if (this.cancelBtn != null) {
			this.cancelBtn.addSelectionListener(new SelectionAdapter() {

				/**
				 * Cancel button handler
				 */
				@Override
				public void widgetSelected(final SelectionEvent event) {
					cancelTemplateHandler();
				}
			});
		}
		
	}

	/**
	 * Save template handler.
	 */
	public void saveTemplateHandler() {
		if (validate() && validateVariables()) {
			if (!templateModel.getName().isEmpty()) {
				if (templateModel.getOperationMode() == CommonConstants.OPERATIONMODE.CREATE) {
					createTemplateHandler();
				} else if (templateModel.getOperationMode() == CommonConstants.OPERATIONMODE.CHANGE) {
					changeTemplateHandler();
				}
			}
		}

	}

	/**
	 * Change template handler.
	 */
	private void changeTemplateHandler() {
		try {
			NotificationController notificationController = new NotificationController();
			EmailTemplateTbl emailTemplateTbl = notificationController.updateTemplate(mapVOObjectWithModel());
			if (emailTemplateTbl != null) {
				setOldModel(this.templateModel.deepCopyTemplate(true, getOldModel()));
				this.templateModel.setOperationMode(CommonConstants.OPERATIONMODE.VIEW);
				setOperationMode();
				this.dirty.setDirty(false);
				final NotificationTemplates notificationTemplates = AdminTreeFactory.getInstance().getNotifications()
						.getNotificationTemplates();
				notificationTemplates.sort();
				XMAdminUtil.getInstance().getAdminTree().refresh(true);
				XMAdminUtil.getInstance().getAdminTree().setSelection(new StructuredSelection(getOldModel()), true);
				XMAdminUtil.getInstance().updateLogFile(
						messages.templateObject + " '" + this.templateModel.getName() + "' " + messages.objectUpdate,
						MessageType.SUCCESS);
			}
		} catch (UnauthorizedAccessException e) {
			CustomMessageDialog.openError(this.getShell(), messages.objectPermissionDialogTitle,
					messages.objectPermissionDialogMsg);
		}catch (Exception e) {
			if (e instanceof ResourceAccessException) {
				CustomMessageDialog.openError(Display.getCurrent().getActiveShell(), messages.errorDialogTitile,
						messages.serverNotReachable);
			}
			LOGGER.warn("Unable to Update Template data ! " + e);
		}
	}

	/**
	 * Create template handler.
	 */
	private void createTemplateHandler() {
		try {
			NotificationController notificationController = new NotificationController();
			EmailTemplateTbl emailTemplateTbl = notificationController.createTemplate(mapVOObjectWithModel());
			if (emailTemplateTbl != null) {
				String emailTemplateId = emailTemplateTbl.getEmailTemplateId();
				if (!XMSystemUtil.isEmpty(emailTemplateId)) {
					this.templateModel.setTemplateId(emailTemplateId);
					AdminTreeFactory instance = AdminTreeFactory.getInstance();
					if (oldModel == null) {
						setOldModel(templateModel.deepCopyTemplate(false, null));
						(instance.getNotifications().getNotificationTemplates()).add(emailTemplateId, getOldModel());
					}
					this.dirty.setDirty(false);
					AdminTreeviewer adminTree = XMAdminUtil.getInstance().getAdminTree();
					adminTree.refresh(true);

					adminTree.setSelection(new StructuredSelection(instance.getNotifications()), true);
					TreePath[] selectionPaths = adminTree.getStructuredSelection().getPaths();
					if (selectionPaths != null && selectionPaths.length > 0) {
						adminTree.setExpandedState(selectionPaths[0], true);
					}
					adminTree.setSelection(new StructuredSelection(instance.getNotifications().getNotificationTemplates()), true);
					selectionPaths = adminTree.getStructuredSelection().getPaths();
					if (selectionPaths != null && selectionPaths.length > 0) {
						adminTree.setExpandedState(selectionPaths[0], true);
					}
					adminTree.setSelection(new StructuredSelection(getOldModel()), true);
				}
				XMAdminUtil.getInstance().updateLogFile(
						messages.templateObject + " '" + this.templateModel.getName() + "' " + messages.objectCreate,
						MessageType.SUCCESS);
			}
		} catch (UnauthorizedAccessException e) {
			CustomMessageDialog.openError(this.getShell(), messages.objectPermissionDialogTitle,
					messages.objectPermissionDialogMsg);
		}catch (Exception e) {
			if (e instanceof ResourceAccessException) {
				CustomMessageDialog.openError(Display.getCurrent().getActiveShell(), messages.errorDialogTitile,
						messages.serverNotReachable);
			}
			LOGGER.warn("Unable to Save data ! " + e);
		}
	}

	/**
	 * Cancel template handler.
	 */
	public void cancelTemplateHandler() {
		if (this.templateModel == null) {
			dirty.setDirty(false);
			return;
		}
		String templateId = CommonConstants.EMPTY_STR;
		int operationMode = this.templateModel.getOperationMode();
		NotificationTemplate oldModel = getOldModel();
		if (oldModel != null) {
			templateId = oldModel.getTemplateId();
		}
		setTemplateModel(null);
		setOldModel(null);
		this.saveBtn.setEnabled(true);
		dirty.setDirty(false);
		final AdminTreeviewer adminTree = XMAdminUtil.getInstance().getAdminTree();
		final NotificationTemplates templates = AdminTreeFactory.getInstance().getNotifications()
				.getNotificationTemplates();
		if (operationMode == CommonConstants.OPERATIONMODE.CHANGE) {
			final IStructuredSelection selection = (IStructuredSelection) adminTree.getSelection();
			final Object firstElement = selection.getFirstElement();
			if (firstElement != null && firstElement.equals(templates.getTemplatesChildren().get(templateId))) {
				adminTree.setSelection(new StructuredSelection(templates.getTemplatesChildren().get(templateId)), true);
			}
		} else {
			adminTree.setSelection(new StructuredSelection(templates), true);
		}
	}

	/**
	 * Map VO object with model.
	 *
	 * @return the email template request
	 */
	private EmailTemplateRequest mapVOObjectWithModel() {
		com.magna.xmbackend.vo.notification.EmailTemplateRequest emailTemplateRequest = new com.magna.xmbackend.vo.notification.EmailTemplateRequest();

		emailTemplateRequest.setEmailTempalteId(this.templateModel.getTemplateId());
		emailTemplateRequest.setName(this.templateModel.getName());
		emailTemplateRequest.setSubject(this.templateModel.getSubject());
		emailTemplateRequest.setMessage(this.templateModel.getMessage());
		emailTemplateRequest.setTemplateVariables(this.templateModel.getVariables());

		return emailTemplateRequest;
	}

	/**
	 * Validate.
	 *
	 * @return true, if successful
	 */
	private boolean validate() {
		final String templateName = this.templateModel.getName();

		if (XMSystemUtil.isEmpty(templateName)) {
			CustomMessageDialog.openError(this.getShell(), messages.nameErrorTitle, messages.nameError);
			return false;
		}

		if (this.txtSubject.getText().trim().isEmpty()) {
			CustomMessageDialog.openError(this.getShell(), messages.notificationSubjectErrTitle,
					messages.notificationSubjectErrMsg);
			return false;
		}
		if (this.txtMessage.getText().trim().isEmpty()) {
			CustomMessageDialog.openError(this.getShell(), messages.notificationMessageErrTitle,
					messages.notificationMessageErrMsg);
			return false;
		}

		return true;
	}

	/**
	 * Validate variables.
	 *
	 * @return true, if successful
	 */
	private boolean validateVariables() {
		
		String message = this.txtMessage.getText() + this.txtSubject.getText();
		

		String endParanthPattern = "\\%";
		String startParanthPattern = "\\%";

		Pattern startPattern = Pattern.compile(startParanthPattern);
		Pattern endPattern = Pattern.compile(endParanthPattern);
		Matcher matcher = startPattern.matcher(message);
		int startPatternCount = 0;
		int endPatternCount = 0;
		while (matcher.find()) {
			startPatternCount++;
		}

		matcher = endPattern.matcher(message);
		while (matcher.find()) {
			endPatternCount++;
		}
		if (startPatternCount != endPatternCount) {
			return false;
		}
		List<String> varList = new ArrayList<>();
		TreeSet<String> matchedVarList = new TreeSet<>();
		if (startPatternCount == endPatternCount) {
			String varPatternStr = "\\%.*?\\%";
			Pattern varPattern = Pattern.compile(varPatternStr);
			matcher = varPattern.matcher(message);
			while (matcher.find()) {
				varList.add(message.substring(matcher.start(), matcher.end()));
			}
			int count = 0;
			for (String var : varList) {
				if (var.matches(NotificationVariables.TO_USERNAME.toString())
						|| var.matches(NotificationVariables.PROJECT_NAME.toString())
						|| var.matches(NotificationVariables.REMARKS.toString())
						|| var.matches(NotificationVariables.ASSIGNED_USERNAME.toString())
						|| var.matches(NotificationVariables.ADMIN_AREA_NAME.toString())
						|| var.matches(NotificationVariables.PROJECT_EXPIRY_DAYS.toString())
						|| var.matches(NotificationVariables.GRACE_PERIOD.toString())
						|| var.matches(NotificationVariables.PROJECT_CREATE_DATE.toString())) {
					matchedVarList.add(var);
					count++;
				}
			}
			if (count != varList.size()) {
				return false;
			}
		}
		this.templateModel.setVariables(matchedVarList);
		return true;
	}

	/**
	 * Update variable text.
	 */
	private void updateVariableText() {
		String[] selection = eventList.getSelection();
		if (NotificationEventType.PROJECT_CREATE.name().equals(selection[0])) {
			StringBuffer sb = new StringBuffer();
			for (String var : CommonConstants.Notification.PRO_CREATE_VARIABLES) {
				sb.append(var).append("\n");
			}
			txtVariable.setText(sb.toString());
		} else if (NotificationEventType.PROJECT_DELETE.name().equals(selection[0])) {
			StringBuffer sb = new StringBuffer();
			for (String var : CommonConstants.Notification.PRO_DELETE_VARIABLES) {
				sb.append(var).append("\n");
			}
			txtVariable.setText(sb.toString());
		} else if (NotificationEventType.PROJECT_DEACTIVATE.name().equals(selection[0])) {
			StringBuffer sb = new StringBuffer();
			for (String var : CommonConstants.Notification.PRO_DEACTIVATE_VARIABLES) {
				sb.append(var).append("\n");
			}
			txtVariable.setText(sb.toString());
		} else if (NotificationEventType.PROJECT_ACTIVATE.name().equals(selection[0])) {
			StringBuffer sb = new StringBuffer();
			for (String var : CommonConstants.Notification.PRO_ACTIVATE_VARIABLES) {
				sb.append(var).append("\n");
			}
			txtVariable.setText(sb.toString());
		} else if (NotificationEventType.PROJECT_ADMIN_AREA_RELATION_ASSIGN.name().equals(selection[0])) {
			StringBuffer sb = new StringBuffer();
			for (String var : CommonConstants.Notification.PROJECT_ADMIN_AREA_RELATION_ASSIGN_VAR) {
				sb.append(var).append("\n");
			}
			txtVariable.setText(sb.toString());
		} else if (NotificationEventType.PROJECT_ADMIN_AREA_RELATION_REMOVE.name().equals(selection[0])) {
			StringBuffer sb = new StringBuffer();
			for (String var : CommonConstants.Notification.PROJECT_ADMIN_AREA_RELATION_REMOVE_VAR) {
				sb.append(var).append("\n");
			}
			txtVariable.setText(sb.toString());
		} else if (NotificationEventType.USER_PROJECT_RELATION_ASSIGN.name().equals(selection[0])) {
			StringBuffer sb = new StringBuffer();
			for (String var : CommonConstants.Notification.USER_PROJECT_RELATION_ASSIGN_VAR) {
				sb.append(var).append("\n");
			}
			txtVariable.setText(sb.toString());
		} else if (NotificationEventType.USER_PROJECT_RELATION_REMOVE.name().equals(selection[0])) {
			StringBuffer sb = new StringBuffer();
			for (String var : CommonConstants.Notification.USER_PROJECT_RELATION_REMOVE_VAR) {
				sb.append(var).append("\n");
			}
			txtVariable.setText(sb.toString());
		} else if (NotificationEventType.USER_PROJECT_RELATION_EXPIRY.name().equals(selection[0])) {
			StringBuffer sb = new StringBuffer();
			for (String var : CommonConstants.Notification.USER_PROJECT_RELATION_EXPIRY_VAR) {
				sb.append(var).append("\n");
			}
			txtVariable.setText(sb.toString());
		} else if (NotificationEventType.USER_PROJECT_RELATION_GRACE_EXPIRY.name().equals(selection[0])) {
			StringBuffer sb = new StringBuffer();
			for (String var : CommonConstants.Notification.USER_PROJECT_RELATION_GRACE_EXPIRY_VAR) {
				sb.append(var).append("\n");
			}
			txtVariable.setText(sb.toString());
		} else if (NotificationEventType.EMAIL_NOTIFY_TO_AA_PROJECT_USERS.name().equals(selection[0])) {
			StringBuffer sb = new StringBuffer();
			for (String var : CommonConstants.Notification.EMAIL_NOTIFY_TO_AA_PROJECT_USERS_VAR) {
				sb.append(var).append("\n");
			}
			txtVariable.setText(sb.toString());
		} else if (NotificationEventType.EMAIL_NOTIFY_TO_PROJECT_USERS.name().equals(selection[0])) {
			StringBuffer sb = new StringBuffer();
			for (String var : CommonConstants.Notification.EMAIL_NOTIFY_TO_PROJECT_USERS_VAR) {
				sb.append(var).append("\n");
			}
			txtVariable.setText(sb.toString());
		}
	}

	/**
	 * Bind values.
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public void bindValues() {

		widgetValue = WidgetProperties.text(SWT.Modify).observe(this.txtName);
		modelValue = BeanProperties.value(NotificationTemplate.class, NotificationTemplate.PROPERTY_NAME)
				.observe(this.templateModel);
		widgetValue.addValueChangeListener(new IValueChangeListener() {
			/**
			 * handler to update button status
			 */
			@Override
			public void handleValueChange(final ValueChangeEvent event) {
				updateButtonStatus(event);
			}
		});
		bindValue = dataBindContext.bindValue(widgetValue, modelValue);

		widgetValue = WidgetProperties.text(SWT.Modify).observe(this.txtSubject);
		modelValue = BeanProperties.value(NotificationTemplate.class, NotificationTemplate.PROPERTY_SUBJECT)
				.observe(this.templateModel);
		bindValue = dataBindContext.bindValue(widgetValue, modelValue);

		widgetValue = WidgetProperties.text(SWT.Modify).observe(this.txtMessage);
		modelValue = BeanProperties.value(NotificationTemplate.class, NotificationTemplate.PROPERTY_MESSAGE)
				.observe(this.templateModel);
		bindValue = dataBindContext.bindValue(widgetValue, modelValue);
	}

	/**
	 * Update button status.
	 *
	 * @param event
	 *            the event
	 */
	@SuppressWarnings("rawtypes")
	private void updateButtonStatus(final ValueChangeEvent event) {
		final String name = (String) event.getObservableValue().getValue();
		if (this.saveBtn != null) {
			if (XMSystemUtil.isEmpty(name) || name.trim().length() == 0
					|| (!name.matches(CommonConstants.RegularExpressions.ALLOWED_SITE_NAME_REGEXP))) {
				this.saveBtn.setEnabled(false);
			} else {
				this.saveBtn.setEnabled(true);
			}
		}
	}

	/**
	 * Register messages.
	 *
	 * @param registry
	 *            the registry
	 */
	public void registerMessages(final MessageRegistry registry) {
		registry.register((text) -> {
			if (lblName != null && !lblName.isDisposed()) {
				lblName.setText(text);
			}
		}, (message) -> {
			if (lblName != null && !lblName.isDisposed()) {
				return getUpdatedWidgetText(message.objectNameLabel, lblName);
			}
			return CommonConstants.EMPTY_STR;
		});

		registry.register((text) -> {
			if (lblVariables != null && !lblVariables.isDisposed()) {
				lblVariables.setText(text);
			}
		}, (message) -> {
			if (lblVariables != null && !lblVariables.isDisposed()) {
				return getUpdatedWidgetText(message.notificationVariableLbl, lblVariables);
			}
			return CommonConstants.EMPTY_STR;
		});

		registry.register((text) -> {
			if (lblMessage != null && !lblMessage.isDisposed()) {
				lblMessage.setText(text);
			}
		}, (message) -> {
			if (lblMessage != null && !lblMessage.isDisposed()) {
				return getUpdatedWidgetText(message.notificationMessageLbl, lblMessage);
			}
			return CommonConstants.EMPTY_STR;
		});

		registry.register((text) -> {
			if (lblSubject != null && !lblSubject.isDisposed()) {
				lblSubject.setText(text);
			}
		}, (message) -> {
			if (lblSubject != null && !lblSubject.isDisposed()) {
				return getUpdatedWidgetText(message.notificationSubjectLbl, lblSubject);
			}
			return CommonConstants.EMPTY_STR;
		});

		if (saveBtn != null) {
			registry.register((text) -> {
				if (saveBtn != null && !saveBtn.isDisposed()) {
					saveBtn.setText(text);
				}
			}, (message) -> {
				if (saveBtn != null && !saveBtn.isDisposed()) {
					return getUpdatedWidgetText(message.saveButtonText, saveBtn);
				}
				return CommonConstants.EMPTY_STR;
			});
		}

		if (cancelBtn != null) {
			registry.register((text) -> {
				if (cancelBtn != null && !cancelBtn.isDisposed()) {
					cancelBtn.setText(text);
				}
			}, (message) -> {
				if (cancelBtn != null && !cancelBtn.isDisposed()) {
					return getUpdatedWidgetText(message.cancelButtonText, cancelBtn);
				}
				return CommonConstants.EMPTY_STR;
			});
		}
	}

	/**
	 * Set operation mode.
	 */
	public void setOperationMode() {
		if (this.templateModel != null) {
			if (templateModel.getOperationMode() == CommonConstants.OPERATIONMODE.VIEW) {
				this.txtName.setEditable(false);
				this.txtSubject.setEditable(false);
				this.txtMessage.setData("editable", false);
				setShowButtonBar(false);
			} else if (templateModel.getOperationMode() == CommonConstants.OPERATIONMODE.CREATE) {
				this.txtName.setEditable(true);
				this.saveBtn.setEnabled(false);
				this.txtSubject.setEditable(true);
				this.txtMessage.setData("editable", true);
				setShowButtonBar(true);
				this.dirty.setDirty(true);
			} else if (templateModel.getOperationMode() == CommonConstants.OPERATIONMODE.CHANGE) {
				this.txtName.setEditable(true);
				this.txtMessage.setData("editable", true);
				this.txtSubject.setEditable(true);
				setShowButtonBar(true);
				this.dirty.setDirty(true);
			} else {
				this.txtName.setEditable(false);
				this.txtSubject.setEditable(false);
				this.txtMessage.setData("editable", false);
				setShowButtonBar(false);
			}
		}
	}

	/**
	 * Sets the dirty object.
	 *
	 * @param dirty
	 *            the new dirty object
	 */
	public void setDirtyObject(final MDirtyable dirty) {
		this.dirty = dirty;
	}

	/**
	 * Set template.
	 */
	public void setTemplate() {
		try {
			final AdminTreeviewer adminTree = XMAdminUtil.getInstance().getAdminTree();
			final Object selectionObj = adminTree.getSelection();
			if (selectionObj instanceof IStructuredSelection) {
				Object firstElement = ((IStructuredSelection) selectionObj).getFirstElement();
				if (firstElement instanceof NotificationTemplate) {
					setOldModel((NotificationTemplate) firstElement);
					NotificationTemplate rightHandObject = (NotificationTemplate) this.getOldModel()
							.deepCopyTemplate(false, null);
					setTemplateModel(rightHandObject);
					registerMessages(this.registry);
					bindValues();
					setOperationMode();
				}
			}
		} catch (Exception e) {
			LOGGER.warn("Unable to set template model! " + e);
		}
	}

	/**
	 * Sets the model.
	 *
	 * @param templateModel
	 *            the new model
	 */
	public void setModel(NotificationTemplate templateModel) {
		try {
			setOldModel(null);
			setTemplateModel(templateModel);
			registerMessages(this.registry);
			bindValues();
			setOperationMode();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Gets the updated widget text.
	 *
	 * @param message
	 *            the message
	 * @param control
	 *            the control
	 * @return the updated widget text
	 */
	private String getUpdatedWidgetText(final String message, final Control control) {
		control.requestLayout();
		control.getParent().redraw();
		control.getParent().getParent().update();
		control.getParent().getParent().getParent().update();
		return message;
	}

	/**
	 * Gets the template model.
	 *
	 * @return the template model
	 */
	public NotificationTemplate getTemplateModel() {
		return templateModel;
	}

	/**
	 * Sets the template model.
	 *
	 * @param templateModel
	 *            the new template model
	 */
	public void setTemplateModel(NotificationTemplate templateModel) {
		this.templateModel = templateModel;
	}

	/**
	 * Gets the old model.
	 *
	 * @return the old model
	 */
	public NotificationTemplate getOldModel() {
		return oldModel;
	}

	/**
	 * Sets the old model.
	 *
	 * @param oldModel
	 *            the new old model
	 */
	public void setOldModel(NotificationTemplate oldModel) {
		this.oldModel = oldModel;
	}
}
