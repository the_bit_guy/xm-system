package com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.adminarea;

import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild;

/**
 * The Class AdminProAppChildProtected.
 */
public class AdminAreaProjectAppProtected implements IAdminTreeChild {

	/** The parent. */
	private IAdminTreeChild parent;

	/**
	 * Instantiates a new admin pro app child protected.
	 *
	 * @param parent the parent
	 */
	public AdminAreaProjectAppProtected(final IAdminTreeChild parent) {
		this.parent = parent;
	}
	
	/* (non-Javadoc)
	 * @see com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild#getParent()
	 */
	@Override
	public IAdminTreeChild getParent() {
		return this.parent;
	}

	/* (non-Javadoc)
	 * @see com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild#setParent(com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild)
	 */
	@Override
	public void setParent(IAdminTreeChild parent) {
		this.parent = parent;
	}

}
