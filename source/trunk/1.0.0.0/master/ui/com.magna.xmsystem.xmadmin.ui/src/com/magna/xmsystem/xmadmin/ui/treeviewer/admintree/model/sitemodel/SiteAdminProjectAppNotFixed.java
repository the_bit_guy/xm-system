package com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.sitemodel;

import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild;

/**
 * The Class SiteAdminProAppNotFixed.
 * 
 * @author subash.janarthanan
 * 
 */
public class SiteAdminProjectAppNotFixed implements IAdminTreeChild {
	/** The parent. */
	private IAdminTreeChild parent;

	
	/**
	 * Instantiates a new site admin pro app not fixed.
	 *
	 * @param parent the parent
	 */
	public SiteAdminProjectAppNotFixed(final IAdminTreeChild parent) {
		this.parent = parent;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild#
	 * getParent()
	 */
	@Override
	public IAdminTreeChild getParent() {
		return this.parent;
	}

	/*
	 * com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild#
	 * setParent(com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.
	 * IAdminTreeChild)
	 */
	@Override
	public void setParent(final IAdminTreeChild parent) {
		this.parent = parent;
	}
}
