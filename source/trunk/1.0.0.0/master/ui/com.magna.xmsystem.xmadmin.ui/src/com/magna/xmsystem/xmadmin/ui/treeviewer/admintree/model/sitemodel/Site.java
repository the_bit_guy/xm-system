package com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.sitemodel;

import java.beans.PropertyChangeEvent;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import org.eclipse.core.runtime.Assert;

import com.magna.xmbackend.vo.language.Language;
import com.magna.xmsystem.dependencies.utils.LANG_ENUM;
import com.magna.xmsystem.dependencies.utils.XMSystemUtil;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.BeanModel;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.icon.Icon;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.refs.RelationObj;
import com.magna.xmsystem.xmadmin.util.CommonConstants;

/**
 * Model class for Site.
 *
 * @author shashwat.anand
 */
public class Site extends BeanModel implements IAdminTreeChild, Cloneable {
	/**
	 * Constant variable for remark text limit
	 */
	public static final int REMARK_LIMIT = 1500;

	/**
	 * Constant variable for desc text limit
	 */
	public static final int DESC_LIMIT = 240;

	/**
	 * Constant variable for name text limit
	 */
	public static final int NAME_LIMIT = 30;
	/**
	 * PROPERTY_SITEID constant
	 */
	public static final String PROPERTY_SITEID = "siteId"; //$NON-NLS-1$
	/**
	 * PROPERTY_SITENAME constant
	 */
	public static final String PROPERTY_SITENAME = "name"; //$NON-NLS-1$

	/**
	 * PROPERTY_ACTIVE constant
	 */
	public static final String PROPERTY_ACTIVE = "active"; //$NON-NLS-1$

	/**
	 * PROPERTY_DESC_MAP constant
	 */
	public static final String PROPERTY_DESC_MAP = "descriptionMap"; //$NON-NLS-1$

	/**
	 * PROPERTY_REMARKS_MAP constant
	 */
	public static final String PROPERTY_REMARKS_MAP = "remarksMap"; //$NON-NLS-1$

	/**
	 * PROPERTY_OPERATION_MODE constant
	 */
	public static final String PROPERTY_OPERATION_MODE = "operationMode"; //$NON-NLS-1$

	/**
	 * PROPERTY_ICON constant
	 */
	public static final String PROPERTY_ICON = "icon"; //$NON-NLS-1$

	/** Member variable for site id. */
	private String siteId;

	/** Member variable for site name. */
	private String name;

	/** Member variable to store is site is active. */
	private boolean active;

	/** Member variable for site description. */
	private Map<LANG_ENUM, String> descriptionMap;

	/** Member variable for site remarks. */
	private Map<LANG_ENUM, String> remarksMap;

	/** Member variable for mode of operation. */
	private int operationMode;

	/** Member variable for icon. */
	private Icon icon;

	/** Member variable 'site children' for {@link Map<SiteChildrenEnum,IAdminTreeChild>}. */
	private Map<String, IAdminTreeChild> siteChildren;

	/**
	 * Constructor.
	 *
	 * @param siteId
	 *            {@link String}
	 * @param name
	 *            {@link String}
	 * @param isActive
	 *            {@link boolean}
	 * @param icon
	 *            {@link Icon}
	 * @param operationMode
	 *            {@link int}
	 */
	public Site(final String siteId, final String name, final boolean isActive, final Icon icon,
			final int operationMode) {
		this(siteId, name, isActive, new HashMap<>(), new HashMap<>(), icon, operationMode);
	}

	/**
	 * Constructor.
	 *
	 * @param siteId
	 *            {@link String}
	 * @param name
	 *            {@link String}
	 * @param isActive
	 *            boolean
	 * @param descriptionMap
	 *            {@link Map}
	 * @param remarksMap
	 *            {@link Map}
	 * @param icon
	 *            {@link Icon}
	 * @param operationMode
	 */
	public Site(final String siteId, final String name, final boolean isActive,
			final Map<LANG_ENUM, String> descriptionMap, final Map<LANG_ENUM, String> remarksMap, final Icon icon,
			final int operationMode) {
		super();
		this.siteId = siteId;
		this.name = name;
		this.active = isActive;
		this.descriptionMap = descriptionMap;
		this.remarksMap = remarksMap;
		this.icon = icon;
		this.operationMode = operationMode;
		this.siteChildren = new LinkedHashMap<>();
		
		addFixedChildren();
	}

	/**
	 * Method for Adds the fixed children.
	 */
	public void addFixedChildren() {
		siteChildren.put(SiteAdministrations.class.getName(), new SiteAdministrations(this));
	}
	
	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Sets the name.
	 *
	 * @param name
	 *            the name to set
	 */
	public void setName(final String name) {
		if (name == null)
			throw new IllegalArgumentException();
		this.propertyChangeSupport.firePropertyChange(PROPERTY_SITENAME, this.name, this.name = name.trim());
	}

	/**
	 * Checks if is active.
	 *
	 * @return the isActive
	 */
	public boolean isActive() {
		return active;
	}

	/**
	 * Sets the active.
	 *
	 * @param isActive
	 *            the isActive to set
	 */
	public void setActive(final boolean isActive) {
		this.propertyChangeSupport.firePropertyChange(PROPERTY_ACTIVE, this.active, this.active = isActive);
	}

	/**
	 * Gets the description map.
	 *
	 * @return the descriptionMap
	 */
	public Map<LANG_ENUM, String> getDescriptionMap() {
		return descriptionMap;
	}

	/**
	 * Gets the description for given language.
	 *
	 * @param lang
	 *            {@link Language}
	 * @return {@link String}
	 */
	public String getDescription(final LANG_ENUM lang) {
		return this.descriptionMap.get(lang);
	}

	/**
	 * Sets the description for given language.
	 *
	 * @param lang
	 *            {@link Language}
	 * @param description
	 *            {@link String}
	 */
	public void setDescription(final LANG_ENUM lang, final String description) {
		if (lang == null || description == null)
			throw new IllegalArgumentException();
		this.propertyChangeSupport.firePropertyChange(PROPERTY_DESC_MAP, this.descriptionMap,
				this.descriptionMap.put(lang, description.trim()));
	}

	/**
	 * Method for Sets the description map.
	 *
	 * @param descriptionMap
	 *            the descriptionMap to set
	 */
	public void setDescriptionMap(final Map<LANG_ENUM, String> descriptionMap) {
		if (descriptionMap == null)
			throw new IllegalArgumentException();
		this.propertyChangeSupport.firePropertyChange(PROPERTY_DESC_MAP, this.descriptionMap,
				this.descriptionMap = descriptionMap);
	}

	/**
	 * Gets the remarks map.
	 *
	 * @return the remarksMap
	 */
	public Map<LANG_ENUM, String> getRemarksMap() {
		return remarksMap;
	}

	/**
	 * Gets the remarks for given language.
	 *
	 * @param lang
	 *            {@link Language}
	 * @return {@link String}
	 */
	public String getRemarks(final LANG_ENUM lang) {
		return this.remarksMap.get(lang);
	}

	/**
	 * Method for Sets the remarks map.
	 *
	 * @param remarksMap
	 *            the remarks to set
	 */
	public void setRemarksMap(final Map<LANG_ENUM, String> remarksMap) {
		if (remarksMap == null)
			throw new IllegalArgumentException();
		this.propertyChangeSupport.firePropertyChange(PROPERTY_REMARKS_MAP, this.remarksMap,
				this.remarksMap = remarksMap);
	}

	/**
	 * Sets the remarks for given language.
	 *
	 * @param lang
	 *            {@link Language}
	 * @param remarks
	 *            {@link String}
	 */
	public void setRemarks(final LANG_ENUM lang, final String remarks) {
		if (lang == null || remarks == null)
			throw new IllegalArgumentException();
		this.propertyChangeSupport.firePropertyChange(PROPERTY_REMARKS_MAP, this.remarksMap,
				this.remarksMap.put(lang, remarks));
	}

	/**
	 * Gets the site id.
	 *
	 * @return the siteId
	 */
	public String getSiteId() {
		return siteId;
	}

	/**
	 * Sets the site id.
	 *
	 * @param siteId
	 *            the siteId to set
	 */
	public void setSiteId(String siteId) {
		// if (siteId == null) throw new IllegalArgumentException();
		this.propertyChangeSupport.firePropertyChange(PROPERTY_SITEID, this.siteId, this.siteId = siteId);
	}

	/**
	 * Gets the icon.
	 *
	 * @return the icon
	 */
	public Icon getIcon() {
		return icon;
	}

	/**
	 * Sets the icon.
	 *
	 * @param icon
	 *            the icon to set
	 */
	public void setIcon(Icon icon) {
		if (icon == null)
			throw new IllegalArgumentException();
		this.propertyChangeSupport.firePropertyChange(PROPERTY_ICON, this.icon, this.icon = icon);
	}

	/**
	 * Gets the operation mode.
	 *
	 * @return the operation mode
	 */
	public int getOperationMode() {
		return operationMode;
	}

	/**
	 * Sets the operation mode.
	 *
	 * @param operationMode
	 *            the new operation mode
	 */
	public void setOperationMode(int operationMode) {
		this.propertyChangeSupport.firePropertyChange(PROPERTY_OPERATION_MODE, this.operationMode,
				this.operationMode = operationMode);
	}

	/**
	 * Gets the site children.
	 *
	 * @return the site children
	 */
	public Map<String, IAdminTreeChild> getSiteChildren() {
		return siteChildren;
	}

	/**
	 * @param siteChildren
	 *            the siteChildren to set
	 */
	public void setSiteChildren(Map<String, IAdminTreeChild> siteChildren) {
		this.siteChildren = siteChildren;
	}
	
	/**
	 * Adds the child
	 * 
	 * @param child
	 *            {@link IAdminTreeChild}
	 * @return {@link boolean}
	 */
	public IAdminTreeChild add(final String id,final IAdminTreeChild child) {
		child.setParent(this);
		return this.siteChildren.put(id, child);
	}


	/**
	 * Method for Removes the.
	 *
	 * @param child
	 *            {@link IAdminTreeChild}
	 * @return true, if successful
	 */
	public IAdminTreeChild remove(final String id) {
		return this.siteChildren.remove(id);
	}
	
	/**
	 * Method for Removes the all.
	 */
	public void removeAll() {
		this.siteChildren.clear();
	}
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.BeanModel#
	 * propertyChange(java.beans.PropertyChangeEvent)
	 */
	@Override
	public void propertyChange(PropertyChangeEvent event) {
		this.propertyChangeSupport.firePropertyChange(event.getPropertyName(), event.getOldValue(),
				event.getNewValue());
	}

	/**
	 * Method for Deep copy site.
	 *
	 * @param update
	 *            {@link boolean}
	 * @return the site {@link Site}
	 */
	public Site deepCopySite(boolean update, Site updateThisObject) {
		Site clonedSite = null;
		String currentSiteId = XMSystemUtil.isEmpty(this.getSiteId()) ? CommonConstants.EMPTY_STR : this.getSiteId();
		String currentName = XMSystemUtil.isEmpty(this.getName()) ? CommonConstants.EMPTY_STR : this.getName();
		boolean currentIsActive = this.isActive();

		Map<LANG_ENUM, String> currentTranslationIdMap = new HashMap<>();
		Map<LANG_ENUM, String> currentDescriptionMap = new HashMap<>();
		Map<LANG_ENUM, String> currentRemarksMap = new HashMap<>();

		for (LANG_ENUM langEnum : LANG_ENUM.values()) {
			String translationId = this.getTranslationId(langEnum);
			currentTranslationIdMap.put(langEnum,
					XMSystemUtil.isEmpty(translationId) ? CommonConstants.EMPTY_STR : translationId);

			String description = this.getDescription(langEnum);
			currentDescriptionMap.put(langEnum,
					XMSystemUtil.isEmpty(description) ? CommonConstants.EMPTY_STR : description);

			String remarks = this.getRemarks(langEnum);
			currentRemarksMap.put(langEnum, XMSystemUtil.isEmpty(remarks) ? CommonConstants.EMPTY_STR : remarks);
		}

		//Icon currentIcon = this.getIcon() == null ? null : this.getIcon();
		Icon currentIcon = new Icon(this.getIcon().getIconId(), this.getIcon().getIconName(),
				this.getIcon().getIconPath(), this.getIcon().getIconType());
		if (update) {
			clonedSite = updateThisObject;
		} else {
			clonedSite = new Site(currentSiteId, currentName, currentIsActive, currentIcon, CommonConstants.OPERATIONMODE.VIEW);
		}
		clonedSite.setSiteId(currentSiteId);
		clonedSite.setName(currentName);
		clonedSite.setActive(currentIsActive);
		clonedSite.setIcon(currentIcon);
		clonedSite.setName(currentName);
		clonedSite.setTranslationIdMap(currentTranslationIdMap);
		clonedSite.setDescriptionMap(currentDescriptionMap);
		clonedSite.setRemarksMap(currentRemarksMap);
		
		return clonedSite;

	}
	/* (non-Javadoc)
	 * @see com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.BeanModel#getAdapter(java.lang.Class, com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild)
	 */
	@Override
	public <T> RelationObj getAdapter(Class<T> adapterType, IAdminTreeChild parent, String relationId, boolean relationStatus) {
		Assert.isNotNull(adapterType);
		Assert.isNotNull(this);
		return super.getAdapter(adapterType, parent, relationId, relationStatus);
	}
}
