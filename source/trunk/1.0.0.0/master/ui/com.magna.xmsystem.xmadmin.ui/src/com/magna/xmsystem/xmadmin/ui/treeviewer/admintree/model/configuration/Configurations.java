package com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration;

import java.util.LinkedList;
import java.util.List;

import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.icon.Icons;

/**
 * Parent model element to store all configurations
 * 
 * @author shashwat.anand
 *
 */
public class Configurations implements IAdminTreeChild {
	/** The parent. */
	private IAdminTreeChild parent;
	
	/**
	 * List for storing {@link IAdminTreeChild}
	 */
	private transient final List<IAdminTreeChild> configurationChildren;

	/**
	 * Constructor
	 */
	public Configurations() {
		this.parent = null;
		this.configurationChildren = new LinkedList<>();
	}

	/**
	 * Adds the child
	 * @param child {@link IAdminTreeChild}
	 * @return {@link boolean}
	 */
	public boolean add(final IAdminTreeChild child) {
		child.setParent(this);
		return this.configurationChildren.add(child);
	}

	/**
	 * Removes the child
	 * @param child {@link IAdminTreeChild}
	 * @return {@link boolean}
	 */
	public boolean remove(final IAdminTreeChild child) {
		return this.configurationChildren.remove(child);
	}

	/**
	 * @return {@link List} of {@link IAdminTreeChild} 
	 */
	public List<IAdminTreeChild> getAllConfigurations() {
		return this.configurationChildren;
	}

	/**
	 * Get the icons
	 * @return Icons
	 */
	public Icons getIconsNode() {
		for (IAdminTreeChild iAdminTreeChild : configurationChildren) {
			if (iAdminTreeChild instanceof Icons) {
				return (Icons) iAdminTreeChild;
			}
		}
		return null;
	}
	
	/**
	 * Returns null as its parent
	 * @see com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild#getParent()
	 */
	@Override
	public IAdminTreeChild getParent() {
		return this.parent;
	}

	/**
	 * @see com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild#setParent(com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild)
	 */
	@Override
	public void setParent(final IAdminTreeChild parent) {
		this.parent = parent;
	}
}
