/*package com.magna.xmsystem.xmadmin.ui.parts.dnd;

import java.util.Collection;
import java.util.Map;

import org.eclipse.jface.util.LocalSelectionTransfer;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.swt.dnd.DragSourceAdapter;
import org.eclipse.swt.dnd.DragSourceEvent;

import com.magna.xmsystem.xmadmin.ui.displayallobjectpage.DisplayAllObjectPage;

*//**
 * The Class NodeDragListener
 *
 * @author subash.janarthanan
 * 
 *//*
public class DispalyAllObjDragListener extends DragSourceAdapter {

	*//** The tree viewer. *//*
	private DisplayAllObjectPage displayAllObjectPage;
	private LocalSelectionTransfer transfer;

	*//**
	 * Instantiates a new node drag listener.
	 *
	 * @param treeViewer
	 *            the tree viewer
	 * @param transfer 
	 *//*
	public DispalyAllObjDragListener(DisplayAllObjectPage displayAllObjectPage, LocalSelectionTransfer transfer) {
		this.displayAllObjectPage = displayAllObjectPage;
		this.transfer = transfer;
	}


	 (non-Javadoc)
	 * @see org.eclipse.swt.dnd.DragSourceAdapter#dragStart(org.eclipse.swt.dnd.DragSourceEvent)
	 
	@Override
	public void dragStart(DragSourceEvent event) {
		Map<String, StructuredSelection> structuredSelection = displayAllObjectPage.getStructuredSelection();
		Collection<StructuredSelection> values = structuredSelection.values();
		Object[] selections = values.toArray();
		if (selections != null) {
			transfer.setSelection(new StructuredSelection(selections));
		} else {
			event.doit = false;
		}
	}
}*/