package com.magna.xmsystem.xmadmin.util;

import java.io.File;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.inject.Named;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.core.runtime.preferences.IEclipsePreferences;
import org.eclipse.e4.core.contexts.IEclipseContext;
import org.eclipse.e4.core.di.annotations.Creatable;
import org.eclipse.e4.core.di.annotations.Optional;
import org.eclipse.e4.core.services.events.IEventBroker;
import org.eclipse.e4.core.services.nls.Translation;
import org.eclipse.e4.core.services.translation.TranslationService;
import org.eclipse.e4.ui.model.application.MApplication;
import org.eclipse.e4.ui.model.application.ui.basic.MPart;
import org.eclipse.e4.ui.model.application.ui.basic.MPartSashContainer;
import org.eclipse.e4.ui.workbench.modeling.EModelService;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.ScrolledComposite;
import org.eclipse.swt.dnd.Clipboard;
import org.eclipse.swt.dnd.Transfer;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Shell;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.magna.xmbackend.entities.PermissionTbl;
import com.magna.xmbackend.entities.RolePermissionRelTbl;
import com.magna.xmbackend.vo.enums.IDateTimeFormatConst;
import com.magna.xmbackend.vo.enums.PermissionType;
import com.magna.xmbackend.vo.permission.PermissionResponse;
import com.magna.xmbackend.vo.permission.ValidationRequest;
import com.magna.xmsystem.dependencies.utils.LANG_ENUM;
import com.magna.xmsystem.dependencies.utils.XMSystemUtil;
import com.magna.xmsystem.xmadmin.message.Message;
import com.magna.xmsystem.xmadmin.message.MessageRegistry;
import com.magna.xmsystem.xmadmin.restclient.icons.IconController;
import com.magna.xmsystem.xmadmin.restclient.permission.PermissionController;
import com.magna.xmsystem.xmadmin.restclient.role.RolePermissionRelController;
import com.magna.xmsystem.xmadmin.restclient.util.RestClientUtil;
import com.magna.xmsystem.xmadmin.restclient.validation.ValidationController;
import com.magna.xmsystem.xmadmin.ui.logger.admin.ui.AdminHistoryDialog;
import com.magna.xmsystem.xmadmin.ui.logger.user.ui.UserHistoryDialog;
import com.magna.xmsystem.xmadmin.ui.messageconsole.MessageConsoleLogObj;
import com.magna.xmsystem.xmadmin.ui.parts.IEditablePart;
import com.magna.xmsystem.xmadmin.ui.parts.dnd.ObjectTypeTransfer;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.AdminTreeviewer;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.adminarea.AdminAreaProjectAppFixed;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.adminarea.AdminAreaProjectAppNotFixed;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.adminarea.AdminAreaProjectAppProtected;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.adminarea.AdminAreaProjectChild;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.adminarea.AdminAreaProjectStartApplications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.adminarea.AdminAreaStartApplications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.adminarea.AdminAreaUserAppFixed;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.adminarea.AdminAreaUserAppNotFixed;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.adminarea.AdminAreaUserAppProtected;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.adminarea.AdministrationArea;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.baseapplicationmodel.BaseApplication;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.projectapplicationmodel.ProjectApplication;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.startapplicationmodel.StartApplication;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.userapplicationmodel.UserApplication;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.icon.Icon;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.roles.GlobalPermissionObjectType;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.roles.ObjectRelationType;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.roles.ObjectType;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.roles.Role;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.roles.objRelationPermissions.ObjectRelationPermissions;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.roles.objectpermissions.ObjectPermissions;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.projects.Project;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.projects.ProjectAdminAreaChild;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.projects.ProjectAdminAreaProjectAppFixed;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.projects.ProjectAdminAreaProjectAppNotFixed;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.projects.ProjectAdminAreaProjectAppProtected;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.projects.ProjectAdminAreaStartApplications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.projects.ProjectUserAAProjectAppAllowed;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.projects.ProjectUserAAProjectAppForbidden;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.refs.RelationObj;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.sitemodel.Site;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.sitemodel.SiteAdminAreaProjectChild;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.sitemodel.SiteAdminAreaProjectStartApplications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.sitemodel.SiteAdminAreaStartApplications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.sitemodel.SiteAdminAreaUserAppFixed;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.sitemodel.SiteAdminAreaUserAppNotFixed;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.sitemodel.SiteAdminAreaUserAppProtected;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.sitemodel.SiteAdminProjectAppFixed;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.sitemodel.SiteAdminProjectAppNotFixed;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.sitemodel.SiteAdminProjectAppProtected;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.users.User;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.users.UserAAUserAppAllowed;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.users.UserAAUserAppForbidden;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.users.UserProjectAAProjectAppAllowed;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.users.UserProjectAAProjectAppForbidden;

/**
 * Class for XM admin util.
 *
 * @author Chiranjeevi.Akula
 */
@Creatable
public class XMAdminUtil {
	
	/** The Constant LOGGER. */
	private static final Logger LOGGER = LoggerFactory.getLogger(XMAdminUtil.class);

	/** Member variable 'this ref' for {@link XMAdminUtil}. */
	private static XMAdminUtil thisRef;

	/** Member variable 'eclipse context' for {@link IEclipseContext}. */
	@Inject
	private IEclipseContext eclipseContext;

	/** Member variable 'locale' for {@link Locale}. */
	@Inject
	private @Named(TranslationService.LOCALE) Locale locale;

	/** Member variable 'model service' for {@link EModelService}. */
	@Inject
	private EModelService modelService;

	/** Member variable 'application' for {@link MApplication}. */
	@Optional
	@Inject
	private MApplication application;
	
	/** Member variable for messages. */
	@Inject
	@Translation
	transient protected Message messages;
	
	/** The registry. */
	@Inject
	private MessageRegistry registry;
	
	/** The object permission map. */
	private Map<ObjectType, ObjectPermCont> objectPermissionMap;
	
	/** The object rel permission map. */
	private Map<ObjectRelationType, ObjectRelPermCont> objectRelPermissionMap;
	
	/** The object global permission map. */
	private Map<GlobalPermissionObjectType, ObjectGlobalPerm> objectGlobalPermissionMap;

	/** Member variable 'user history dialog' for {@link UserHistoryDialog}. */
	private UserHistoryDialog userHistoryDialog;

	/** Member variable 'admin history dialog' for {@link AdminHistoryDialog}. */
	private AdminHistoryDialog adminHistoryDialog;
	
	/** Member variable 'msg console str buf' for {@link StringBuffer}. */
	//private StringBuffer msgConsoleStrBuf;
	
	/** The event broker. */
	@Inject
	private IEventBroker eventBroker;
	
	/** The msg consol obj list. */
	private List<MessageConsoleLogObj> msgConsolObjList;

	/**
	 * Constructor for XMAdminUtil Class.
	 */
	public XMAdminUtil() {
		setInstance(this);
		objectPermissionMap = new HashMap<>();
		objectRelPermissionMap = new HashMap<>();
		objectGlobalPermissionMap = new HashMap<>();
		//populatePermissionMaps();
	}

	/**
	 * Gets the single instance of XMAdminUtil.
	 *
	 * @return single instance of XMAdminUtil
	 */
	public static XMAdminUtil getInstance() {
		return thisRef;
	}

	/**
	 * Sets the instance.
	 *
	 * @param thisRef
	 *            the new instance
	 */
	public static void setInstance(XMAdminUtil thisRef) {
		XMAdminUtil.thisRef = thisRef;
	}

	/**
	 * Method for Post construct.
	 */
	@PostConstruct
	private void postConstruct() {
		initMsgConsloeBuffer();
	}
	
	/**
	 * Gets the admin tree.
	 *
	 * @return the admin tree
	 */
	public AdminTreeviewer getAdminTree() {
		return eclipseContext.get(AdminTreeviewer.class);
	}

	/**
	 * Gets the current locale enum.
	 *
	 * @return the current locale enum
	 */
	public LANG_ENUM getCurrentLocaleEnum() {
		return LANG_ENUM.getLangEnum(this.locale.getLanguage());
	}

	/**
	 * Gets the default locale enum.
	 *
	 * @return the default locale enum
	 */
	public LANG_ENUM getDefaultLocaleEnum() {
		return LANG_ENUM.ENGLISH;
	}
	
	/**
	 * Sets the default locale.
	 */
	public void setDefaultLocale() {
		final String localeString = XMSystemUtil.getLanguage();
		if(!XMSystemUtil.isEmpty(localeString)) {
			Locale defaultLocale = LANG_ENUM.valueOf(localeString.toUpperCase()).getLocale();
			if(defaultLocale != null) {
				Locale.setDefault(defaultLocale);				
				eclipseContext.set(TranslationService.LOCALE, defaultLocale);
				setLocale(defaultLocale);
				XMSystemUtil.setLanguage(LANG_ENUM.getLangEnum(this.locale.getLanguage()).name().toLowerCase());
			}
		} else {
			XMSystemUtil.setLanguage(LANG_ENUM.getLangEnum(this.locale.getLanguage()).name().toLowerCase());
		}
	}

	/**
	 * Sets the locale.
	 *
	 * @param locale
	 *            the new locale
	 */
	@Inject
	public void setLocale(@Optional @Named(TranslationService.LOCALE) Locale locale) {
		Locale localeP = locale;
		if (locale == null) {
			localeP = Locale.ENGLISH;
			LOGGER.error("changing the application language to english as system langauge is null");
		}
		if (!(locale.getLanguage().equalsIgnoreCase(Locale.ENGLISH.getLanguage())) && 
				!(locale.getLanguage().equalsIgnoreCase(Locale.GERMAN.getLanguage()))) {
			localeP = Locale.ENGLISH;
			LOGGER.error("changing the application language to english as system langauge is neither english nor german");
		}
		this.locale = localeP;
		
	}

	/**
	 * Gets the old perspective sash container.
	 *
	 * @return the old perspective sash container
	 */
	public MPartSashContainer getOldPerspectiveSashContainer() {
		return (MPartSashContainer) modelService.find("com.magna.xmsystem.xmadmin.ui.partsashcontainer", application); //$NON-NLS-1$
	}

	
	/**
	 * Gets the information part.
	 *
	 * @return the information part
	 */
	public MPart getInformationPart() {
		MPart part = (MPart) modelService.find("com.magna.xmsystem.xmadmin.ui.part.informationpart", application); //$NON-NLS-1$
		if (part != null) {
			return part;
		}
		return null;
	}

	/**
	 * Method for Save previous part.
	 */
	public void savePreviousPart() {
		MPart part;
		if ((part = getInformationPart()) != null) {
			if (part.getObject() instanceof IEditablePart) {
				((IEditablePart) part.getObject()).save();
			}
		}
	}

	/**
	 * Method for Discard previous part.
	 */
	public void discardPreviousPart() {
		MPart part;
		if ((part = getInformationPart()) != null) {
			if (part.getObject() instanceof IEditablePart) {
				((IEditablePart) part.getObject()).discard();
			}
		}
	}

	/**
	 * Gets the icon by name.
	 *
	 * @param iconName
	 *            {@link String}
	 * @return the icon by name
	 */
	public Icon getIconByName(String iconName) {
		Icon icon = new Icon();
		File iconFolder;
		com.magna.xmbackend.vo.icon.IkonRequest iconRequest = new com.magna.xmbackend.vo.icon.IkonRequest();
		iconRequest.setIconName(iconName);
		if ((iconFolder = XMSystemUtil.getXMSystemServerIconsFolder()) != null) {
			final IconController iconController = new IconController();
			com.magna.xmbackend.entities.IconsTbl iconVo;
			if ((iconVo = iconController.getIconByName(iconRequest)) != null) {
				icon.setIconId(iconVo.getIconId());
				String name = iconVo.getIconName();
				icon.setIconName(name);
				icon.setIconType(iconVo.getIconType());
				icon.setIconPath(iconFolder.getAbsolutePath() + File.separator + name);
			}
		}
		return icon;
	}

	/**
	 * Gets the AA for headers.
	 *
	 * @param target {@link IAdminTreeChild}
	 * @return the AA for headers
	 */
	public String getAAForHeaders(IAdminTreeChild target) {
		if(target != null) {
			AdministrationArea administrationArea = null;
			if(target instanceof AdministrationArea) {
				administrationArea = (AdministrationArea) target;
			} else if(target instanceof RelationObj) {
				IAdminTreeChild refObject = ((RelationObj) target).getRefObject();
				if(refObject instanceof AdministrationArea) {
					administrationArea = (AdministrationArea) refObject;
				}
			} 
			
			if(administrationArea != null) {
				return administrationArea.getAdministrationAreaId();
			} else {
				return getAAForHeaders(target.getParent());
			}
		}		
		return null;
	}
	/**
	 * Create and return ScrolledComposite.
	 * 
	 * @param parent
	 *            {@link Composite}
	 * @return ScrolledComposite {@link ScrolledComposite}
	 */
	public ScrolledComposite createScrolledComposite(final Composite parent) {
		final ScrolledComposite composite = new ScrolledComposite(parent, SWT.NONE | SWT.H_SCROLL | SWT.V_SCROLL);
		composite.setLayout(new GridLayout());
		composite.setLayoutData(getGridFillData());
		composite.addListener(SWT.Activate, new Listener() {
			public void handleEvent(Event e) {
				if (composite.isFocusControl()) {
					composite.setFocus();
				}
			}
		});
		return composite;
	}
	
	/**
	 * Creates the scrolled composite without activate listener.
	 *
	 * @param parent the parent
	 * @return the scrolled composite
	 */
	public ScrolledComposite createScrolledCompositeWithoutActivateListener(final Composite parent) {
		final ScrolledComposite composite = new ScrolledComposite(parent, SWT.NONE | SWT.H_SCROLL | SWT.V_SCROLL);
		composite.setLayout(new GridLayout());
		composite.setLayoutData(getGridFillData());
		return composite;
	}
	
	/**
	 * Clear clip board contets.
	 */
	public void clearClipBoardContents(){
		Clipboard clipboard = new Clipboard(Display.getCurrent());
		   Object copiedObj = clipboard.getContents(ObjectTypeTransfer.getInstance());
			if (copiedObj instanceof java.util.Collection) {
				Object[] clipBoardData = ((java.util.Collection<?>) copiedObj).toArray();
				clipboard.setContents(new Object[] { clipBoardData },
						new Transfer[] { ObjectTypeTransfer.getInstance() });
				clipboard.clearContents();
			}
	}

	/**
	 * Create and return vertical and horizontal fill GridData.
	 * 
	 * @return GridData
	 */
	public GridData getGridFillData() {
		final GridData fillData = new GridData();
		fillData.horizontalAlignment = GridData.FILL;
		fillData.verticalAlignment = GridData.FILL;
		fillData.grabExcessHorizontalSpace = true;
		fillData.grabExcessVerticalSpace = true;
		return fillData;
	}
	
	/**
	 * Method for Populate permission maps.
	 */
	public void populatePermissionMaps() {
		try {
			PermissionController controller = new PermissionController();
			PermissionResponse objectPermissionResponse = controller.getAllObjectPermission(true);
			if (objectPermissionResponse == null) {
				return;
			}
			Iterable<PermissionTbl> objPermissionTbls = objectPermissionResponse.getPermissionTbls();
			for (PermissionTbl permissionTbl : objPermissionTbls) {
				String name = permissionTbl.getName();
				
				if (GlobalPermissionObjectType.LOGIN_CAX_START_ADMIN.name().equals(name) || GlobalPermissionObjectType.LOGIN_CAX_START_HOTLINE.name().equals(name)
						|| GlobalPermissionObjectType.VIEW_INACTIVE.name().equals(name)) {
					GlobalPermissionObjectType objectType = GlobalPermissionObjectType.getObjectType(name);
					if (this.objectGlobalPermissionMap.get(objectType) == null) {
						this.objectGlobalPermissionMap.put(objectType, new ObjectGlobalPerm());
					}
					ObjectGlobalPerm objGLobalPerm = this.objectGlobalPermissionMap.get(objectType);
					if (GlobalPermissionObjectType.LOGIN_CAX_START_ADMIN.name().equals(name)) {
						objGLobalPerm.setLoginCaxStartAdminPermission(permissionTbl);
					} else if (GlobalPermissionObjectType.LOGIN_CAX_START_HOTLINE.name().equals(name)) {
						objGLobalPerm.setLoginCaxStartHotlinePermission(permissionTbl);
					} else if (GlobalPermissionObjectType.VIEW_INACTIVE.name().equals(name)) {
						objGLobalPerm.setViewInactivePermission(permissionTbl);
					}
					continue;
				}
				String permissionName = name.split("-")[0];
				String permissionCategory = name.split("-")[1];
				ObjectType objectType = ObjectType.getObjectType(permissionName);
				if (this.objectPermissionMap.get(objectType) == null) {
					this.objectPermissionMap.put(objectType, new ObjectPermCont());
				}
				ObjectPermCont objectPermCont = this.objectPermissionMap.get(objectType);
				if (permissionCategory.equals("CREATE")) {
					objectPermCont.setCreatePermission(permissionTbl);
				} else if (permissionCategory.equals("CHANGE")) {
					objectPermCont.setChangePermission(permissionTbl);
				} else if (permissionCategory.equals("DELETE")) {
					objectPermCont.setDeletePermission(permissionTbl);
				} else if (permissionCategory.equals("ACTIVATE_DEACTIVATE")) {
					objectPermCont.setActivatePermission(permissionTbl);
				}
			}
			PermissionResponse objectRelPermissionResponse = controller.getAllObjectRelPermission(true);
			if (objectRelPermissionResponse == null) {
				return;
			}
			Iterable<PermissionTbl> objRelPermissionTbls = objectRelPermissionResponse.getPermissionTbls();
			for (PermissionTbl permissionTbl : objRelPermissionTbls) {
				String name = permissionTbl.getName();
				String permissionName = name.split("-")[0];//.replaceAll(" ", "_");
				String permissionCategory = name.split("-")[1];
				ObjectRelationType objectRelationType = ObjectRelationType.getObjectType(permissionName);
				ObjectRelPermCont objectRelPermCont;
				if ((objectRelPermCont = this.objectRelPermissionMap.get(objectRelationType)) == null) {
					this.objectRelPermissionMap.put(objectRelationType, new ObjectRelPermCont());
				}
				objectRelPermCont = this.objectRelPermissionMap.get(objectRelationType);
				if (permissionCategory.equals("ASSIGN")) {
					objectRelPermCont.setAssignPermission(permissionTbl);
				} else if (permissionCategory.equals("REMOVE")) {
					objectRelPermCont.setRemovePermission(permissionTbl);
				} else if (permissionCategory.equals("ACTIVATE_DEACTIVATE")) {
					objectRelPermCont.setActivatePermission(permissionTbl);
				} else if (permissionCategory.equals("INACTIVE_ASSIGNMENT")) {
					objectRelPermCont.setInactiveAssignmentPermission(permissionTbl);
				}
			}
			
			PermissionResponse aaBasedRelPermissionResponse = controller.getAllAABasedRelPermission(true);
			if (aaBasedRelPermissionResponse == null) {
				return;
			}
			Iterable<PermissionTbl> aaBasedRelPermissionTbls = aaBasedRelPermissionResponse.getPermissionTbls();
			for (PermissionTbl permissionTbl : aaBasedRelPermissionTbls) {
				String name = permissionTbl.getName();
				String permissionName = name.split("-")[0];//.replaceAll(" ", "_");
				String permissionCategory = name.split("-")[1];
				ObjectRelationType objectRelationType = ObjectRelationType.getObjectType(permissionName);
				ObjectRelPermCont objectRelPermCont;
				if ((objectRelPermCont = this.objectRelPermissionMap.get(objectRelationType)) == null) {
					this.objectRelPermissionMap.put(objectRelationType, new ObjectRelPermCont());
				}
				objectRelPermCont = this.objectRelPermissionMap.get(objectRelationType);
				if (permissionCategory.equals("ASSIGN")) {
					objectRelPermCont.setAssignPermission(permissionTbl);
				} else if (permissionCategory.equals("REMOVE")) {
					objectRelPermCont.setRemovePermission(permissionTbl);
				} else if (permissionCategory.equals("ACTIVATE_DEACTIVATE")) {
					objectRelPermCont.setActivatePermission(permissionTbl);
				} else if (permissionCategory.equals("INACTIVE_ASSIGNMENT")) {
					objectRelPermCont.setInactiveAssignmentPermission(permissionTbl);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Gets the object permission map.
	 *
	 * @return the objectPermissionMap
	 */
	public Map<ObjectType, ObjectPermCont> getObjectPermissionMap() {
		return objectPermissionMap;
	}

	/**
	 * Gets the object rel permission map.
	 *
	 * @return the objectRelPermissionMap
	 */
	public Map<ObjectRelationType, ObjectRelPermCont> getObjectRelPermissionMap() {
		return objectRelPermissionMap;
	}

	/**
	 * Gets the object global permission map.
	 *
	 * @return the objectGlobalPermissionMap
	 */
	public Map<GlobalPermissionObjectType, ObjectGlobalPerm> getObjectGlobalPermissionMap() {
		return objectGlobalPermissionMap;
	}
	
	/**
	 * Update permissions.
	 *
	 * @param role the role
	 */
	public void updatePermissions(final Role role) {
		String roleId;
		if (role != null && (roleId = role.getRoleId()) != null && !roleId.isEmpty()) {
			final RolePermissionRelController controller = new RolePermissionRelController();
			Iterable<RolePermissionRelTbl> rolePermssionTbls;
			if ((rolePermssionTbls = controller.getPermissionByRoleId(roleId)) != null) {
				for (RolePermissionRelTbl rolePermissionRelTbl : rolePermssionTbls) {
					PermissionTbl permissionTbl = rolePermissionRelTbl.getPermissionId();
					String permissionType = permissionTbl.getPermissionType();
					String permissionName = permissionTbl.getName();
					if (GlobalPermissionObjectType.LOGIN_CAX_START_ADMIN.name().equals(permissionName)
							|| GlobalPermissionObjectType.LOGIN_CAX_START_HOTLINE.name().equals(permissionName)
							|| GlobalPermissionObjectType.VIEW_INACTIVE.name().equals(permissionName)) {
						continue;
					}
					String name = permissionName.split("-")[0];
					String permissionId = permissionTbl.getPermissionId();
					if (PermissionType.OBJECT_PERMISSION.name().equals(permissionType)) {
						ObjectType objectType = ObjectType.getObjectType(name);
						if (objectType != null) {
							ObjectPermissions objectPermissions = role.getObjPermissonMap().get(name);
							ObjectPermCont objectPermCont = getObjectPermissionMap().get(objectType);
							PermissionTbl createPermission = objectPermCont.getCreatePermission();
							PermissionTbl changePermission = objectPermCont.getChangePermission();
							PermissionTbl deletePermission = objectPermCont.getDeletePermission();
							PermissionTbl activatePermission = objectPermCont.getActivatePermission();
							if (createPermission != null && createPermission.getPermissionId().equals(permissionId)) {
								objectPermissions.setCreate(true);
							} else if (changePermission != null
									&& changePermission.getPermissionId().equals(permissionId)) {
								objectPermissions.setChange(true);
							} else if (deletePermission != null
									&& deletePermission.getPermissionId().equals(permissionId)) {
								objectPermissions.setDelete(true);
							} else if (activatePermission != null
									&& activatePermission.getPermissionId().equals(permissionId)) {
								objectPermissions.setActivate(true);
							}
							continue;
						}
					} else if (PermissionType.RELATION_PERMISSION.name().equals(permissionType)
							|| PermissionType.AA_BASED_RELATION_PERMISSION.name().equals(permissionType)) {
						ObjectRelationType objectType = ObjectRelationType.getObjectType(name);
						if (objectType != null) {
							ObjectRelationPermissions objectRelationPermissions = role.getObjRelationPermissonMap().get(name);
							ObjectRelPermCont objectRelPermCont = getObjectRelPermissionMap().get(objectType);
							PermissionTbl assignPermission = objectRelPermCont.getAssignPermission();
							PermissionTbl removePermission = objectRelPermCont.getRemovePermission();
							PermissionTbl activatePermission = objectRelPermCont.getActivatePermission();
							PermissionTbl inactiveAssignmentPermission = objectRelPermCont.getInactiveAssignmentPermission();
							if (assignPermission != null && assignPermission.getPermissionId().equals(permissionId)) {
								objectRelationPermissions.setAssign(true);
							} else if (removePermission != null
									&& removePermission.getPermissionId().equals(permissionId)) {
								objectRelationPermissions.setRemove(true);
							} else if (activatePermission != null
									&& activatePermission.getPermissionId().equals(permissionId)) {
								objectRelationPermissions.setActivate(true);
							} else if (inactiveAssignmentPermission != null
									&& inactiveAssignmentPermission.getPermissionId().equals(permissionId)) {
								objectRelationPermissions.setInactiveAssignment(true);
							}
							continue;
						}
					}
				}
			}
		}
	}
	
	/**
	 * Method for Open user history.
	 *
	 * @param preferences {@link IEclipsePreferences}
	 */
	public void openUserHistory(final IEclipsePreferences preferences) {
		Job job = new Job("User History...") {

			@Override
			protected IStatus run(IProgressMonitor monitor) {
				monitor.beginTask("User History..", 100);
				monitor.worked(30);
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				Display.getDefault().asyncExec(new Runnable() {
					@Override
					public void run() {
						if (userHistoryDialog == null) {
							userHistoryDialog = new UserHistoryDialog(Display.getDefault().getActiveShell(),
									preferences, registry, messages);
						}
						Shell dialogShell = userHistoryDialog.getShell();
						if (dialogShell == null) {
							userHistoryDialog = new UserHistoryDialog(Display.getDefault().getActiveShell(),
									preferences, registry, messages);
						} else {
							if (dialogShell.getMinimized()) {
								dialogShell.setMinimized(false);
							}
						}
						monitor.worked(30);
						userHistoryDialog.open();
					}
				});

				monitor.worked(40);
				return Status.OK_STATUS;
			}

		};
		job.setUser(true);
		job.schedule();

		/*if (userHistoryDialog == null) {
			userHistoryDialog = new UserHistoryDialog(Display.getDefault().getActiveShell(), preferences,registry, messages);
		}
		Shell dialogShell = userHistoryDialog.getShell();
		if (dialogShell == null) {
			userHistoryDialog = new UserHistoryDialog(Display.getDefault().getActiveShell(), preferences,registry, messages);
		} else {
			if (dialogShell.getMinimized()) {
				dialogShell.setMinimized(false);
			}
		}
		userHistoryDialog.open();*/
	}
	
	/**
	 * Method for Open admin history.
	 *
	 * @param preferences {@link IEclipsePreferences}
	 */
	public void openAdminHistory(final IEclipsePreferences preferences) {
		Job job = new Job("Admin History...") {

			@Override
			protected IStatus run(IProgressMonitor monitor) {
				monitor.beginTask("Admin History..", 100);
				monitor.worked(30);
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				Display.getDefault().asyncExec(new Runnable() {
					@Override
					public void run() {
						if (adminHistoryDialog == null) {
							adminHistoryDialog = new AdminHistoryDialog(Display.getDefault().getActiveShell(),
									preferences, registry, messages);
						}
						Shell dialogShell = adminHistoryDialog.getShell();
						if (dialogShell == null) {
							adminHistoryDialog = new AdminHistoryDialog(Display.getDefault().getActiveShell(),
									preferences, registry, messages);
						} else {
							if (dialogShell.getMinimized()) {
								dialogShell.setMinimized(false);
							}
						}

						monitor.worked(30);
						adminHistoryDialog.open();
					}
				});

				monitor.worked(40);
				return Status.OK_STATUS;
			}

		};
		job.setUser(true);
		job.schedule();
	
		/*if (adminHistoryDialog == null) {
			adminHistoryDialog = new AdminHistoryDialog(Display.getDefault().getActiveShell(), preferences, registry,messages);
		}
		Shell dialogShell = adminHistoryDialog.getShell();
		if (dialogShell == null) {
			adminHistoryDialog = new AdminHistoryDialog(Display.getDefault().getActiveShell(), preferences, registry,messages);
		} else {
			if (dialogShell.getMinimized()) {
				dialogShell.setMinimized(false);
			}
		}
		adminHistoryDialog.open();*/
	}
	
	/**
	 * Checks if is update status allowed.
	 *
	 * @param selection the selection obj
	 * @return true, if is update status allowed
	 * @throws Exception the exception
	 */
	public boolean isUpdateStatusAllowed(Object selection) throws Exception {
		if (selection == null) {
			return false;
		}
		boolean returnVal = true;
		//try {
			ValidationController controller = new ValidationController();
			ValidationRequest request = new ValidationRequest();
			request.setUserName(RestClientUtil.getInstance().getUserName());
			request.setPermissionType(PermissionType.OBJECT_PERMISSION.name());
			if (selection instanceof IStructuredSelection) {
				Object firstElement = ((IStructuredSelection) selection).getFirstElement();
				if (firstElement instanceof Site) {
					request.setPermissionName("SITE-ACTIVATE_DEACTIVATE"); //$NON-NLS-1$
				} else if (firstElement instanceof AdministrationArea) {
					request.setPermissionName("ADMINISTRATION_AREA-ACTIVATE_DEACTIVATE");//$NON-NLS-1$
				} else if (firstElement instanceof User) {
					request.setPermissionName("USER-ACTIVATE_DEACTIVATE");//$NON-NLS-1$
				} else if (firstElement instanceof Project) {
					request.setPermissionName("PROJECT-ACTIVATE_DEACTIVATE");//$NON-NLS-1$
				} else if (firstElement instanceof UserApplication) {
					request.setPermissionName("USER_APPLICATION-ACTIVATE_DEACTIVATE");//$NON-NLS-1$
				} else if (firstElement instanceof ProjectApplication) {
					request.setPermissionName("PROJECT_APPLICATION-ACTIVATE_DEACTIVATE");//$NON-NLS-1$
				} else if (firstElement instanceof StartApplication) {
					request.setPermissionName("START_APPLICATION-ACTIVATE_DEACTIVATE");//$NON-NLS-1$
				} else if (firstElement instanceof BaseApplication) {
					request.setPermissionName("BASE_APPLICATION-ACTIVATE_DEACTIVATE");//$NON-NLS-1$
				} 
			}
			returnVal = controller.getAccessAllowed(request);
		/*} catch (UnauthorizedAccessException e) {
			e.printStackTrace();
			returnVal = false;
		} catch (Exception e) {
			e.printStackTrace();
			returnVal = false;
		}*/
		return returnVal;
	}
	
	/**
	 * Checks if is rel obj update status allowed.
	 *
	 * @param containerObj the container obj
	 * @param permissionName the permission name
	 * @return true, if is rel obj update status allowed
	 * @throws Exception the exception
	 */
	public boolean isRelObjAccessAllowed(Object containerObj,String permissionName) throws Exception{
		boolean returnVal = true;
		ValidationController controller = new ValidationController();
		ValidationRequest request = new ValidationRequest();
		request.setUserName(RestClientUtil.getInstance().getUserName());
		request.setPermissionType(PermissionType.RELATION_PERMISSION.name());
		request.setPermissionName(permissionName);// $NON-NLS-1$
		if (containerObj instanceof SiteAdminAreaProjectChild || containerObj instanceof AdminAreaProjectChild
				|| containerObj instanceof SiteAdminAreaUserAppNotFixed
				|| containerObj instanceof SiteAdminAreaUserAppFixed
				|| containerObj instanceof SiteAdminAreaUserAppProtected
				|| containerObj instanceof AdminAreaUserAppProtected || containerObj instanceof AdminAreaUserAppNotFixed
				|| containerObj instanceof AdminAreaUserAppFixed || containerObj instanceof SiteAdminProjectAppNotFixed
				|| containerObj instanceof SiteAdminProjectAppFixed
				|| containerObj instanceof SiteAdminProjectAppProtected
				|| containerObj instanceof AdminAreaProjectAppNotFixed
				|| containerObj instanceof AdminAreaProjectAppFixed
				|| containerObj instanceof AdminAreaProjectAppProtected
				|| containerObj instanceof ProjectAdminAreaProjectAppNotFixed
				|| containerObj instanceof ProjectAdminAreaProjectAppFixed
				|| containerObj instanceof ProjectAdminAreaProjectAppProtected
				|| containerObj instanceof SiteAdminAreaStartApplications
				|| containerObj instanceof AdminAreaStartApplications
				|| containerObj instanceof SiteAdminAreaProjectStartApplications
				|| containerObj instanceof AdminAreaProjectStartApplications
				|| containerObj instanceof ProjectAdminAreaStartApplications
				|| containerObj instanceof UserProjectAAProjectAppAllowed
				|| containerObj instanceof UserProjectAAProjectAppForbidden
				|| containerObj instanceof ProjectUserAAProjectAppAllowed
				|| containerObj instanceof ProjectUserAAProjectAppForbidden
				|| containerObj instanceof UserAAUserAppAllowed || containerObj instanceof UserAAUserAppForbidden
				|| containerObj instanceof ProjectAdminAreaChild) {
			request.setPermissionType(PermissionType.AA_BASED_RELATION_PERMISSION.name());
		}
		returnVal = controller.getAccessAllowed(request);
		return returnVal;
	}
	
	/**
	 * Checks if is acess allowed.
	 *
	 * @param permissionName the permission name
	 * @return true, if is acess allowed
	 * @throws Exception the exception
	 */
	public boolean isAcessAllowed(String permissionName) throws Exception{
		boolean returnVal = true;
			ValidationController controller = new ValidationController();
			ValidationRequest request = new ValidationRequest();
			request.setUserName(RestClientUtil.getInstance().getUserName());
			request.setPermissionType(PermissionType.OBJECT_PERMISSION.name());
			request.setPermissionName(permissionName); //$NON-NLS-1$
			returnVal = controller.getAccessAllowed(request);
		return returnVal;
	}
	
	/*public StringBuffer getMsgConsoleStrBuf() {
		return msgConsoleStrBuf;
	}*/

	/**
	 * Update log file.
	 *
	 * @param message            the message
	 * @param messageType the message type
	 */
	public void updateLogFile(String message, final MessageType messageType) {
		LOGGER.info(message);
		updateMessageConsole(message, messageType);
	}

	/**
	 * Update message console.
	 *
	 * @param message the message
	 * @param messageType the message type
	 */
	public void updateMessageConsole(final String message, final MessageType messageType) {
		final DateTimeFormatter dateTimeFormatter = DateTimeFormatter
				.ofPattern(IDateTimeFormatConst.UI_DATE_TIME_FORMAT, Locale.ENGLISH);
		String consoleMsg = LocalDateTime.now().format(dateTimeFormatter) + " : " + message + "\n";
		this.eventBroker.send(CommonConstants.EVENT_BROKER.CONSOLE_MESSAGE,
				new MessageConsoleLogObj(messageType, consoleMsg));
		msgConsolObjList.add(new MessageConsoleLogObj(messageType, consoleMsg));
	}

	/**
	 * Method for Inits the msg consloe buffer.
	 */ 
	private void initMsgConsloeBuffer() {
		final DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern(IDateTimeFormatConst.UI_DATE_TIME_FORMAT, Locale.ENGLISH);
		String dateTimeStr = LocalDateTime.now().format(dateTimeFormatter);
		String startMsg = dateTimeStr + " : " + messages.adminStartedMessage + "\n";
		msgConsolObjList = new ArrayList<MessageConsoleLogObj>();
		msgConsolObjList.add(new MessageConsoleLogObj(null, startMsg));
	}

	public List<MessageConsoleLogObj> getMsgConsolObj() {
		return msgConsolObjList;
	}
}
