package com.magna.xmsystem.xmadmin.ui.parts.smtpconfig;

import java.util.EnumMap;
import java.util.List;

import javax.inject.Inject;

import org.eclipse.core.databinding.Binding;
import org.eclipse.core.databinding.DataBindingContext;
import org.eclipse.core.databinding.UpdateValueStrategy;
import org.eclipse.core.databinding.beans.BeanProperties;
import org.eclipse.core.databinding.observable.value.IObservableValue;
import org.eclipse.core.databinding.observable.value.IValueChangeListener;
import org.eclipse.core.databinding.observable.value.ValueChangeEvent;
import org.eclipse.core.runtime.preferences.IEclipsePreferences;
import org.eclipse.e4.core.di.extensions.Preference;
import org.eclipse.e4.core.services.nls.Translation;
import org.eclipse.e4.ui.model.application.ui.MDirtyable;
import org.eclipse.jface.databinding.fieldassist.ControlDecorationSupport;
import org.eclipse.jface.databinding.swt.WidgetProperties;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.client.ResourceAccessException;

import com.magna.xmsystem.dependencies.customExceptions.UnauthorizedAccessException;
import com.magna.xmsystem.dependencies.utils.XMSystemUtil;
import com.magna.xmsystem.ui.controls.widgets.CustomMessageDialog;
import com.magna.xmbackend.entities.PropertyConfigTbl;
import com.magna.xmbackend.vo.enums.SmtpKey;
import com.magna.xmsystem.xmadmin.message.Message;
import com.magna.xmsystem.xmadmin.message.MessageRegistry;
import com.magna.xmsystem.xmadmin.restclient.propertyConfig.PropertyConfigController;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.AdminTreeviewer;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.smtpconfig.SmtpConfiguration;
import com.magna.xmsystem.xmadmin.ui.validation.NameValidation;
import com.magna.xmsystem.xmadmin.ui.validation.StatusValidation;
import com.magna.xmsystem.xmadmin.util.CommonConstants;
import com.magna.xmsystem.xmadmin.util.MessageType;
import com.magna.xmsystem.xmadmin.util.XMAdminUtil;

// TODO: Auto-generated Javadoc
/**
 * The Class SmtpConfigCompositeAction.
 * 
 * @author archita.patel
 */
@SuppressWarnings("restriction")
public class SmtpConfigCompositeAction extends SmtpConfigCompositeUI {

	/** Logger instance. */
	private static final Logger LOGGER = LoggerFactory.getLogger(SmtpConfigCompositeAction.class);

	/** Member variable for widgetValue. */
	transient private IObservableValue<?> widgetValue;

	/** Member variable for modelValue. */
	transient private IObservableValue<?> modelValue;

	/**
	 * Member variable for data binding context the DataBindingContext object
	 * will manage the databindings.
	 */
	final transient DataBindingContext dataBindContext = new DataBindingContext();

	/** Member variable for {@link MessageRegistry}. */
	@Inject
	private MessageRegistry registry;

	/** Member variable for messages. */
	@Inject
	@Translation
	transient private Message messages;

	/** The smtp configuration model. */
	private SmtpConfiguration smtpConfigModel;

	/** The old model. */
	private SmtpConfiguration oldModel;

	/** Member variable for dirty. */
	transient private MDirtyable dirty;

	private EnumMap<SmtpKey, String> smtpKeyValue;

	/**
	 * Instantiates a new smtp config composite action.
	 *
	 * @param parent
	 *            the parent
	 * @param preferences
	 *            the preferences
	 */
	@Inject
	public SmtpConfigCompositeAction(final Composite parent, @Preference IEclipsePreferences preferences) {
		super(parent, SWT.NONE);
		initListener();
	}

	/**
	 * Inits the listener.
	 */
	private void initListener() {
		if (this.btnSave != null) {
			this.btnSave.addSelectionListener(new SelectionAdapter() {

				/**
				 * Save button handler
				 */
				@Override
				public void widgetSelected(final SelectionEvent event) {
					saveSmtpConfiguration();
				}
			});
		}
		if (this.btnCancel != null) {
			this.btnCancel.addSelectionListener(new SelectionAdapter() {

				/**
				 * Save button handler
				 */
				@Override
				public void widgetSelected(final SelectionEvent event) {
					cancelSmtpConfiguration();
				}
			});
		}
	}

	/**
	 * Cancel smtp configuration.
	 */
	public void cancelSmtpConfiguration() {
		if (this.smtpConfigModel == null) {
			dirty.setDirty(false);
			return;
		}
		setSmtpConfigModel(null);
		setOldModel(null);
		dirty.setDirty(false);
		final AdminTreeviewer adminTree = XMAdminUtil.getInstance().getAdminTree();
		final IStructuredSelection selection = (IStructuredSelection) adminTree.getSelection();
		final Object firstElement = selection.getFirstElement();
		adminTree.setSelection(new StructuredSelection(firstElement), true);

	}

	/**
	 * Save smtp configuration.
	 */
	public void saveSmtpConfiguration() {
		// TODO Add in Database
		try {

			PropertyConfigController propertyConfigController = new PropertyConfigController();
			boolean isUpdate = propertyConfigController.updatePropertyValue(mapVOObjectWithModel());
			if (isUpdate) {
				setOldModel(smtpConfigModel.deepCopySmtpConfigModel(true, getOldModel()));
				this.smtpConfigModel.setOperationMode(CommonConstants.OPERATIONMODE.VIEW);
				setOperationMode();
				this.dirty.setDirty(false);
				XMAdminUtil.getInstance().getAdminTree().refresh();
				XMAdminUtil.getInstance().getAdminTree().setSelection(new StructuredSelection(getOldModel()), true);
				XMAdminUtil.getInstance().updateLogFile(messages.smtpConfigNode + " " + messages.objectUpdate,
						MessageType.SUCCESS);
			}

		} catch (UnauthorizedAccessException e) {
			CustomMessageDialog.openError(this.getShell(), messages.objectPermissionDialogTitle,
					messages.objectPermissionDialogMsg);
		} catch (Exception e) {
			if (e instanceof ResourceAccessException) {
				CustomMessageDialog.openError(Display.getCurrent().getActiveShell(), messages.errorDialogTitile,
						messages.serverNotReachable);
			}
			LOGGER.error("Exception occured while saving smtp configuration", e);
		}

	}

	private com.magna.xmbackend.vo.propConfig.SmtpConfigRequest mapVOObjectWithModel() {
		com.magna.xmbackend.vo.propConfig.SmtpConfigRequest smtpConfigRequest = new com.magna.xmbackend.vo.propConfig.SmtpConfigRequest();
		smtpKeyValue = new EnumMap<SmtpKey, String>(SmtpKey.class);
		SmtpKey smtpEmailId = SmtpKey.SMTP_EMAIL_ID;
		SmtpKey smtpPort = SmtpKey.SMTP_PORT_NUMBER;
		SmtpKey smtpServerName = SmtpKey.SMTP_SERVER_NAME;
		SmtpKey smtpPassword = SmtpKey.SMTP_PASSWORD;
		smtpKeyValue.put(smtpEmailId, smtpConfigModel.getSmtpEmail());
		smtpKeyValue.put(smtpPort, smtpConfigModel.getSmtpPort());
		smtpKeyValue.put(smtpServerName, smtpConfigModel.getSmtpServerName());
		smtpKeyValue.put(smtpPassword, smtpConfigModel.getPassword());

		smtpConfigRequest.setSmtpKeyValue(smtpKeyValue);

		return smtpConfigRequest;

	}

	/**
	 * Register messages.
	 *
	 * @param registry
	 *            the registry
	 */
	public void registerMessages(final MessageRegistry registry) {
		registry.register((text) -> {
			if (grpSmtpConfig != null && !grpSmtpConfig.isDisposed()) {
				grpSmtpConfig.setText(text);
			}
		}, (message) -> {
			return message.smtpGroupLabel;
		});

		registry.register((text) -> {
			if (lblSmtpEmail != null && !lblSmtpEmail.isDisposed()) {
				lblSmtpEmail.setText(text);
			}
		}, (message) -> {
			if (lblSmtpEmail != null && !lblSmtpEmail.isDisposed()) {
				return getUpdatedWidgetText(message.smtpEmailLabel, lblSmtpEmail);
			}
			return CommonConstants.EMPTY_STR;
		});

		registry.register((text) -> {
			if (lblPassword != null && !lblPassword.isDisposed()) {
				lblPassword.setText(text);
			}
		}, (message) -> {
			if (lblPassword != null && !lblPassword.isDisposed()) {
				return getUpdatedWidgetText(message.smtpPasswordLabel, lblPassword);
			}
			return CommonConstants.EMPTY_STR;
		});

		registry.register((text) -> {
			if (lblSmtpPort != null && !lblSmtpPort.isDisposed()) {
				lblSmtpPort.setText(text);
			}
		}, (message) -> {
			if (lblSmtpPort != null && !lblSmtpPort.isDisposed()) {
				return getUpdatedWidgetText(message.smtpPortLabel, lblSmtpPort);
			}
			return CommonConstants.EMPTY_STR;
		});

		registry.register((text) -> {
			if (lblSmtpServerName != null && !lblSmtpServerName.isDisposed()) {
				lblSmtpServerName.setText(text);
			}
		}, (message) -> {
			if (lblSmtpServerName != null && !lblSmtpServerName.isDisposed()) {
				return getUpdatedWidgetText(message.smtpServerNameLabel, lblSmtpServerName);
			}
			return CommonConstants.EMPTY_STR;
		});
		
		registry.register((text) -> {
			if (lblMsgConfig != null && !lblMsgConfig.isDisposed()) {
				lblMsgConfig.setText(text);
			}
		}, (message) -> {
			if (lblMsgConfig != null && !lblMsgConfig.isDisposed()) {
				return getUpdatedWidgetText(message.msglabel, lblMsgConfig);
			}
			return CommonConstants.EMPTY_STR;
		});
		registry.register((text) -> {
			if (btnSave != null && !btnSave.isDisposed()) {
				btnSave.setText(text);
			}
		}, (message) -> {
			if (btnSave != null && !btnSave.isDisposed()) {
				return getUpdatedWidgetText(message.saveButtonText, btnSave);
			}
			return CommonConstants.EMPTY_STR;
		});
		registry.register((text) -> {
			if (btnCancel != null && !btnCancel.isDisposed()) {
				btnCancel.setText(text);
			}
		}, (message) -> {
			if (btnCancel != null && !btnCancel.isDisposed()) {
				return getUpdatedWidgetText(message.cancelButtonText, btnCancel);
			}
			return CommonConstants.EMPTY_STR;
		});
	}

	/**
	 * Method for Binding modal to widget.
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public void bindValues() {

		widgetValue = WidgetProperties.text(SWT.Modify).observe(this.txtSmtpEmail);
		modelValue = BeanProperties.value(SmtpConfiguration.class, SmtpConfiguration.PROPERTY_SMTP_EMAIL)
				.observe(this.smtpConfigModel);
		widgetValue.addValueChangeListener(new IValueChangeListener() {
			@Override
			public void handleValueChange(ValueChangeEvent event) {
				updateButtonStatus(event);
			}
		});

		UpdateValueStrategy update = new UpdateValueStrategy();
		update.setAfterGetValidator(new NameValidation(messages, StatusValidation.EMAIL));
		Binding bindValue = dataBindContext.bindValue(widgetValue, modelValue, update, null);
		ControlDecorationSupport.create(bindValue, SWT.TOP | SWT.LEFT);

		widgetValue = WidgetProperties.text(SWT.Modify).observe(this.txtPassword);
		modelValue = BeanProperties.value(SmtpConfiguration.class, SmtpConfiguration.PROPERTY_PASSWORD)
				.observe(this.smtpConfigModel);
		widgetValue.addValueChangeListener(new IValueChangeListener() {
			@Override
			public void handleValueChange(ValueChangeEvent event) {
				updateButtonStatus(event);
			}
		});
	    update = new UpdateValueStrategy();
		update.setAfterGetValidator(new NameValidation(messages, StatusValidation.SMTP_PASSWORD));
		bindValue = dataBindContext.bindValue(widgetValue, modelValue, update, null);
		ControlDecorationSupport.create(bindValue, SWT.TOP | SWT.LEFT);

		widgetValue = WidgetProperties.text(SWT.Modify).observe(this.txtSmtpPort);
		modelValue = BeanProperties.value(SmtpConfiguration.class, SmtpConfiguration.PROPERTY_SMTP_PORT)
				.observe(this.smtpConfigModel);
		widgetValue.addValueChangeListener(new IValueChangeListener() {
			@Override
			public void handleValueChange(ValueChangeEvent event) {
				updateButtonStatus(event);
			}
		});
		update = new UpdateValueStrategy();
		update.setAfterGetValidator(new NameValidation(messages, StatusValidation.PORT_NUMBER));
		bindValue = dataBindContext.bindValue(widgetValue, modelValue, update, null);
		ControlDecorationSupport.create(bindValue, SWT.TOP | SWT.LEFT);

		widgetValue = WidgetProperties.text(SWT.Modify).observe(this.txtSmtpServerName);
		modelValue = BeanProperties.value(SmtpConfiguration.class, SmtpConfiguration.PROPERTY_SMTP_SERVERNAME)
				.observe(this.smtpConfigModel);
		widgetValue.addValueChangeListener(new IValueChangeListener() {
			@Override
			public void handleValueChange(ValueChangeEvent event) {
				updateButtonStatus(event);
			}
		});
		update = new UpdateValueStrategy();
		update.setAfterGetValidator(new NameValidation(messages, StatusValidation.SMTP_SERVER_NAME));
		bindValue = dataBindContext.bindValue(widgetValue, modelValue, update, null);
		ControlDecorationSupport.create(bindValue, SWT.TOP | SWT.LEFT);

	}

	/**
	 * Method to update the button status
	 * 
	 * @param event
	 */
	@SuppressWarnings("rawtypes")
	private void updateButtonStatus(ValueChangeEvent event) {
		final String smtpEmailText = txtSmtpEmail.getText().trim();
		final String smtpPortText = txtSmtpPort.getText().trim();
		final String smtpPassword = txtPassword.getText().trim();
		final String smtpServerName = txtSmtpServerName.getText().trim(); 
		if (this.btnSave != null) {
			if ((XMSystemUtil.isEmpty(smtpEmailText) || smtpEmailText.trim().length() == 0)
					|| (XMSystemUtil.isEmpty(smtpPortText) || smtpPortText.trim().length() == 0)
					|| (XMSystemUtil.isEmpty(smtpPassword) || smtpPortText.trim().length() == 0)
					|| (XMSystemUtil.isEmpty(smtpServerName) || smtpPortText.trim().length() == 0)
					|| (!smtpEmailText.matches(CommonConstants.RegularExpressions.EMAIL_REGEX))
					|| (!smtpPortText.matches(CommonConstants.RegularExpressions.PORT_NUMBER))) {
				this.btnSave.setEnabled(false);
			} else {
				this.btnSave.setEnabled(true);
			}
		}

	}

	/**
	 * Sets the operation mode.
	 */
	public void setOperationMode() {
		if (this.smtpConfigModel != null) {
			if (smtpConfigModel.getOperationMode() == CommonConstants.OPERATIONMODE.VIEW) {
				this.txtSmtpEmail.setEditable(false);
				this.txtPassword.setEditable(false);
				this.txtSmtpPort.setEnabled(false);
				this.txtSmtpServerName.setEditable(false);
				setShowButtonBar(false);
			} else if (smtpConfigModel.getOperationMode() == CommonConstants.OPERATIONMODE.CHANGE) {
				this.txtSmtpEmail.setEditable(true);
				this.txtPassword.setEditable(true);
				this.txtSmtpPort.setEnabled(true);
				this.txtSmtpServerName.setEditable(true);
				setShowButtonBar(true);
				this.dirty.setDirty(true);
			} else {
				this.txtSmtpEmail.setEditable(false);
				this.txtPassword.setEditable(false);
				this.txtSmtpPort.setEnabled(false);
				this.txtSmtpServerName.setEditable(false);
				setShowButtonBar(false);
			}
		}
	}

	/**
	 * Sets the show button bar.
	 *
	 * @param showButtonBar
	 *            the new show button bar
	 */
	protected void setShowButtonBar(final boolean showButtonBar) {
		if (this.btnSave != null && !this.btnSave.isDisposed() && this.btnCancel != null
				&& !this.btnCancel.isDisposed()) {
			final GridData layoutData = (GridData) this.btnSave.getParent().getLayoutData();
			layoutData.exclude = !showButtonBar;
			this.btnSave.setVisible(showButtonBar);
			this.btnCancel.setVisible(showButtonBar);
			this.btnSave.getParent().requestLayout();
			this.btnSave.getParent().redraw();
			this.btnSave.getParent().getParent().update();
		}
	}

	/**
	 * Gets the updated widget text.
	 *
	 * @param message
	 *            the message
	 * @param control
	 *            the control
	 * @return the updated widget text
	 */
	private String getUpdatedWidgetText(final String message, final Control control) {
		control.requestLayout();
		control.getParent().redraw();
		control.getParent().getParent().update();
		control.getParent().getParent().getParent().update();
		return message;
	}

	/**
	 * Sets the dirty object.
	 *
	 * @param dirty
	 *            the new dirty object
	 */
	public void setDirtyObject(final MDirtyable dirty) {
		this.dirty = dirty;
	}

	/**
	 * Gets the smtp config model.
	 *
	 * @return the smtp config model
	 */
	public SmtpConfiguration getSmtpConfigModel() {
		return smtpConfigModel;
	}

	/**
	 * Sets the smtp config model.
	 *
	 * @param smtpConfigModel
	 *            the new smtp config model
	 */
	public void setSmtpConfigModel(SmtpConfiguration smtpConfigModel) {
		this.smtpConfigModel = smtpConfigModel;
	}

	/**
	 * Gets the old model.
	 *
	 * @return the old model
	 */
	public SmtpConfiguration getOldModel() {
		return oldModel;
	}

	/**
	 * Sets the old model.
	 *
	 * @param oldModel
	 *            the new old model
	 */
	public void setOldModel(SmtpConfiguration oldModel) {
		this.oldModel = oldModel;
	}

	/**
	 * Sets the smtp configuration.
	 */
	public void setSmtpConfiguration() {

		final AdminTreeviewer adminTree = XMAdminUtil.getInstance().getAdminTree();
		final Object selectionObj = adminTree.getSelection();
		if (selectionObj instanceof IStructuredSelection) {
			Object firstElement = ((IStructuredSelection) selectionObj).getFirstElement();
			if (firstElement instanceof SmtpConfiguration) {
				smtpConfigModel = (SmtpConfiguration) firstElement;
				PropertyConfigController propertyConfigController = new PropertyConfigController();
				List<PropertyConfigTbl> propVOList = propertyConfigController.getAllSMTPDetails(true);
				for (PropertyConfigTbl propertyTbl : propVOList) {
					String property = propertyTbl.getProperty();
					String propertyValue = propertyTbl.getValue();
					if (SmtpKey.SMTP_EMAIL_ID.toString().equals(property)) {
						this.smtpConfigModel.setSmtpEmail(propertyValue);
					} else if (SmtpKey.SMTP_PORT_NUMBER.toString().equals(property)) {
						this.smtpConfigModel.setSmtpPort(propertyValue);
					} else if (SmtpKey.SMTP_SERVER_NAME.toString().equals(property)) {
						this.smtpConfigModel.setSmtpServerName(propertyValue);
					} else if (SmtpKey.SMTP_PASSWORD.toString().equals(property)) {
						this.smtpConfigModel.setPassword(propertyValue);

					}
				}
				setOldModel(smtpConfigModel);
				setSmtpConfigModel(smtpConfigModel.deepCopySmtpConfigModel(false, getOldModel()));
				registerMessages(this.registry);
				bindValues();
				setOperationMode();

			}
		}
	}
}