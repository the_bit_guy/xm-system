
package com.magna.xmsystem.xmadmin.ui.parts.dynamicmenu;

import java.util.List;

import javax.inject.Inject;

import org.eclipse.e4.core.services.nls.Translation;
import org.eclipse.e4.ui.di.AboutToShow;
import org.eclipse.e4.ui.model.application.ui.menu.MDirectMenuItem;
import org.eclipse.e4.ui.model.application.ui.menu.MMenuElement;
import org.eclipse.e4.ui.model.application.ui.menu.MMenuFactory;
import org.eclipse.e4.ui.workbench.modeling.ESelectionService;
import org.eclipse.jface.viewers.IStructuredSelection;

import com.magna.xmsystem.xmadmin.message.Message;
import com.magna.xmsystem.xmadmin.ui.handlers.ActiveMenuHandler;
import com.magna.xmsystem.xmadmin.ui.handlers.DeActiveMenuHandler;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.adminarea.AdministrationArea;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.baseapplicationmodel.BaseApplication;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.projectapplicationmodel.ProjectApplication;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.startapplicationmodel.StartApplication;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.userapplicationmodel.UserApplication;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.projects.Project;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.sitemodel.Site;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.users.User;
import com.magna.xmsystem.xmadmin.util.CommonConstants;

/**
 * Class for Activate deactivate menu item.
 *
 * @author Chiranjeevi.Akula
 */
public class ActivateDeactivateMenuItem {

	/** Member variable 'selection service' for {@link ESelectionService}. */
	@Inject
	private ESelectionService selectionService;

	/** Member variable 'messages' for {@link Message}. */
	@Inject
	@Translation
	private Message messages;

	/**
	 * Method for About to show.
	 *
	 * @param items
	 *            {@link List<MMenuElement>}
	 */
	@AboutToShow
	public void aboutToShow(List<MMenuElement> items) {
		final Object selectionObj = selectionService.getSelection();
		if (selectionObj == null) {
			return;
		}
		if (selectionObj instanceof IStructuredSelection) {
			Object firstElement = ((IStructuredSelection) selectionObj).getFirstElement();
			if (firstElement instanceof Site) {
				boolean active = ((Site) firstElement).isActive();
				setMenuIteam(items, active);
			} else if (firstElement instanceof AdministrationArea) {
				boolean active = ((AdministrationArea) firstElement).isActive();
				setMenuIteam(items, active);
			} else if (firstElement instanceof User) {
				boolean active = ((User) firstElement).isActive();
				setMenuIteam(items, active);
			} else if (firstElement instanceof Project) {
				boolean active = ((Project) firstElement).isActive();
				setMenuIteam(items, active);
			} /*else if (firstElement instanceof GroupModel) {
				boolean active = ((GroupModel) firstElement).isActive();
				setMenuIteam(items, active);
			} */else if (firstElement instanceof UserApplication) {
				boolean active = ((UserApplication) firstElement).isActive();
				setMenuIteam(items, active);
			} else if (firstElement instanceof ProjectApplication) {
				boolean active = ((ProjectApplication) firstElement).isActive();
				setMenuIteam(items, active);
			} else if (firstElement instanceof StartApplication) {
				boolean active = ((StartApplication) firstElement).isActive();
				setMenuIteam(items, active);
			} else if (firstElement instanceof BaseApplication) {
				boolean active = ((BaseApplication) firstElement).isActive();
				setMenuIteam(items, active);
			}
		}
	}

	/**
	 * Method for Sets the menu iteam.
	 *
	 * @param items
	 *            {@link List<MMenuElement>}
	 * @param active
	 *            {@link boolean}
	 */
	private void setMenuIteam(final List<MMenuElement> items, final boolean active) {
		MDirectMenuItem dynamicItem = MMenuFactory.INSTANCE.createDirectMenuItem();
		if (active) {
			dynamicItem.setLabel(messages.popupmenulabelnodeDeActive);
			dynamicItem.setTooltip(messages.popupmenulabelnodeDeActive);
			dynamicItem.setContributionURI(
					CommonConstants.XMADMIN_UI_BUNDLE + "/" + DeActiveMenuHandler.class.getCanonicalName());

		} else {
			dynamicItem.setLabel(messages.popupmenulabelnodeActive);
			dynamicItem.setTooltip(messages.popupmenulabelnodeActive);
			dynamicItem.setContributionURI(
					CommonConstants.XMADMIN_UI_BUNDLE + "/" + ActiveMenuHandler.class.getCanonicalName());
		}
		items.add(dynamicItem);
	}
}