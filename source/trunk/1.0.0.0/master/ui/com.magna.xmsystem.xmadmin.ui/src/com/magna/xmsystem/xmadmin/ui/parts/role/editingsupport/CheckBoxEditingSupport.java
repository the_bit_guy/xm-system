package com.magna.xmsystem.xmadmin.ui.parts.role.editingsupport;

import org.eclipse.jface.viewers.CellEditor;
import org.eclipse.jface.viewers.EditingSupport;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.jface.viewers.CheckboxCellEditor;

/**
 * The Class CheckBoxEditingSupport
 * 
 * @author shashwat.anand
 */
public abstract class CheckBoxEditingSupport extends EditingSupport {

	/**
	 * Instantiates a new check box editing support.
	 *
	 * @param viewer the viewer
	 */
	public CheckBoxEditingSupport(TableViewer viewer) {
		super(viewer);
	}

	/* (non-Javadoc)
	 * @see org.eclipse.jface.viewers.EditingSupport#getCellEditor(java.lang.Object)
	 */
	@Override
	protected CellEditor getCellEditor(Object element) {
		return new CheckboxCellEditor((Composite) super.getViewer().getControl(), SWT.NONE);
	}

	/* (non-Javadoc)
	 * @see org.eclipse.jface.viewers.EditingSupport#canEdit(java.lang.Object)
	 */
	@Override
	protected boolean canEdit(Object element) {
		return true;
	}
}
