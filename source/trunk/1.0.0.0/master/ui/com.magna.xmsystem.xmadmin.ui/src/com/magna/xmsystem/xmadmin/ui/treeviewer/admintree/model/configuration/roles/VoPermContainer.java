package com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.roles;

import com.magna.xmbackend.vo.roles.ObjectPermission;

/**
 * Class for Vo object perm container.
 *
 * @author Roshan.Ekka
 */
public class VoPermContainer {
	
	/** Member variable 'object permission' for {@link ObjectPermission}. */
	private ObjectPermission objectPermission;
	
	/**
	 * Gets the object permission.
	 *
	 * @return the objectPermission
	 */
	public ObjectPermission getObjectPermission() {
		return objectPermission;
	}
	
	/**
	 * Sets the object permission.
	 *
	 * @param objectPermission the objectPermission to set
	 */
	public void setObjectPermission(final ObjectPermission objectPermission) {
		this.objectPermission = objectPermission;
	}
}
