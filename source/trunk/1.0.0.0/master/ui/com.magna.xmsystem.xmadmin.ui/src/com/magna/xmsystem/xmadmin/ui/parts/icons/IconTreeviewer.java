package com.magna.xmsystem.xmadmin.ui.parts.icons;

import org.eclipse.jface.layout.TreeColumnLayout;
import org.eclipse.jface.viewers.ColumnWeightData;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.jface.viewers.TreeViewerColumn;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Item;
import org.eclipse.swt.widgets.Tree;
import org.eclipse.swt.widgets.TreeItem;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.magna.xmsystem.xmadmin.message.MessageRegistry;

/**
 * Class for Tree viewer for icons
 * 
 * @author shashwat.anand
 *
 */
public class IconTreeviewer extends TreeViewer {
	/**
	 * Logger instance
	 */
	private static final Logger LOGGER = LoggerFactory.getLogger(IconTreeviewer.class);
	/**
	 * Member variable to yellow color
	 */
	final private Color yellow;

	/**
	 * Member variable to white color
	 */
	final private Color white;

	/**
	 * Member variable for firstColoum
	 */
	private TreeViewerColumn firstColoum;

	/**
	 * Member variable for secondColumn
	 */
	//private TreeViewerColumn secondColumn;

	/**
	 * Constructor
	 * 
	 * @param parent
	 *            {@link Composite}
	 */
	public IconTreeviewer(final Composite parent) {
		super(parent, SWT.FULL_SELECTION | SWT.V_SCROLL | SWT.NO_SCROLL | SWT.BORDER | SWT.SINGLE);
		this.init();
		final Display display = parent.getDisplay();
		this.yellow = new Color(display, 255, 255, 235);
		this.white = new Color(display, 255, 255, 255);
		parent.addDisposeListener(new DisposeListener() {
			/**
			 * overriding widget disposed method
			 */
			@Override
			public void widgetDisposed(final DisposeEvent event) {
				IconTreeviewer.this.yellow.dispose();
				IconTreeviewer.this.white.dispose();
			}
		});
	}

	/**
	 * Init method to create table viewer
	 */
	private void init() {
		try {
			this.setAutoExpandLevel(30);
			final Tree tree = this.getTree();
			tree.setLinesVisible(true);
			tree.setHeaderVisible(true);
			this.initColumn();
		} catch (Exception e) {
			LOGGER.error("Execption ocuured at creating the UI", e); //$NON-NLS-1$
		}
	}

	/**
	 * Create table viewer columns
	 */
	private void initColumn() {
		try {
			final TreeColumnLayout layout = new TreeColumnLayout();
			this.getTree().getParent().setLayout(layout);
			this.firstColoum = new TreeViewerColumn(this, SWT.NONE);
			layout.setColumnData(this.firstColoum.getColumn(), new ColumnWeightData(50));
			/*this.secondColumn = new TreeViewerColumn(this, SWT.NONE);
			layout.setColumnData(this.secondColumn.getColumn(), new ColumnWeightData(50));*/
		} catch (Exception e) {
			LOGGER.error("Execption ocuured at creating the columns", e); //$NON-NLS-1$
		}
	}

	/**
	 * Registering the {@link MessageRegistry} to columns headers
	 * 
	 * @param registry
	 *            {@link MessageRegistry}
	 */
	public void registerMessages(final MessageRegistry registry) {
		registry.register(this.firstColoum.getColumn()::setText, (message) -> message.icontableviewerSecondColumnLabel);
		/*registry.register(this.secondColumn.getColumn()::setText,
				(message) -> message.icontableviewerSecondColumnLabel);*/
	}

	/**
	 * overriding doUpdateItem of Treeviewer
	 * 
	 * @param item {@link Item}
	 * @param element {@link Object}
	 */
	@Override
	protected void doUpdateItem(final Item item, final Object element) {
		super.doUpdateItem(item, element);
		this.setColor((TreeItem) item);
	}

	/**
	 * Sets the row color
	 * 
	 * @param item
	 *            TreeItem
	 */
	private void setColor(final TreeItem item) {
		final Color color = this.yellow;
		if ((item.getParentItem() == null ? item.getParent().indexOf(item) : item.getParentItem().indexOf(item))
				% 2 == 0) {
			item.setBackground(color);
		} else {
			item.setBackground(this.white);
		}
	}

	/**
	 * @return the firstColoum
	 */
	public TreeViewerColumn getFirstColoum() {
		return firstColoum;
	}
}
