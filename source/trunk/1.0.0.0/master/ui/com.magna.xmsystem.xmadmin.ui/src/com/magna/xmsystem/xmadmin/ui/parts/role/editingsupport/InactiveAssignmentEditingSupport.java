package com.magna.xmsystem.xmadmin.ui.parts.role.editingsupport;

import com.magna.xmbackend.vo.enums.CreationType;
import com.magna.xmbackend.vo.roles.ObjectPermission;
import com.magna.xmsystem.xmadmin.ui.parts.role.ObjRelationPermissionsTable;
import com.magna.xmsystem.xmadmin.ui.parts.role.ObjectPermissionsTable;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.roles.ObjectRelationType;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.roles.VoPermContainer;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.roles.objRelationPermissions.ObjectRelationPermissions;
import com.magna.xmsystem.xmadmin.util.XMAdminUtil;

/**
 * The Class InactiveAssignmentEditingSupport.
 * 
 * @author shashwat.anand
 */
public class InactiveAssignmentEditingSupport extends CheckBoxEditingSupport {
	/** Member variable 'instance' for {@link XMAdminUtil}. */
	private XMAdminUtil instance;
	
	/** Member variable 'viewer' for {@link ObjectPermissionsTable}. */
	private ObjRelationPermissionsTable viewer;
	/**
	 * Instantiates a new inactive assignment editing support.
	 *
	 * @param viewer the viewer
	 */
	public InactiveAssignmentEditingSupport(final ObjRelationPermissionsTable viewer) {
		super(viewer);
		this.viewer = viewer;
		this.instance = XMAdminUtil.getInstance();
	}

	/* (non-Javadoc)
	 * @see org.eclipse.jface.viewers.EditingSupport#getValue(java.lang.Object)
	 */
	@Override
	protected Object getValue(Object element) {
		if (element instanceof ObjectRelationPermissions) {
			return ((ObjectRelationPermissions) element).isInactiveAssignment();
		}
		return "N";
	}

	/* (non-Javadoc)
	 * @see org.eclipse.jface.viewers.EditingSupport#setValue(java.lang.Object, java.lang.Object)
	 */
	@Override
	protected void setValue(Object element, Object value) {
		if (element instanceof ObjectRelationPermissions) {
			ObjectRelationPermissions elem = (ObjectRelationPermissions) element;
			ObjectRelationType objectRelationType = ObjectRelationType.getObjectType(elem.getPermissionName());
			String permissionId = this.instance.getObjectRelPermissionMap().get(objectRelationType).getInactiveAssignmentPermission().getPermissionId();
			VoPermContainer voPermContainer = new VoPermContainer();
			ObjectPermission objectPermission = new ObjectPermission();
			objectPermission.setPermissionId(permissionId);
			voPermContainer.setObjectPermission(objectPermission);
			if (elem.isInactiveAssignment() && ! (boolean) value) {
				objectPermission.setCreationType(CreationType.DELETE);
				elem.setInactiveAssignmentPermission(voPermContainer);
				//checkAndUpdateForAssignPermission(elem, objectRelationType, CreationType.DELETE);
			} else if (!elem.isInactiveAssignment() && (boolean) value) {
				objectPermission.setCreationType(CreationType.ADD);
				elem.setInactiveAssignmentPermission(voPermContainer);
				checkAndUpdateForAssignPermission(elem, objectRelationType, CreationType.ADD);
			} else {
				return;
			}
			elem.setInactiveAssignment((boolean) value);
			this.viewer.getCachedObjPermissionMap().put(permissionId, voPermContainer);
			getViewer().refresh(element);
		}
	}
	
	private void checkAndUpdateForAssignPermission(ObjectRelationPermissions element,
			ObjectRelationType objectRelationType, CreationType creationType) {
		String permissionId = this.instance.getObjectRelPermissionMap().get(objectRelationType).getAssignPermission().getPermissionId();
		VoPermContainer voObjectPermContainerAssign = new VoPermContainer();
		ObjectPermission objectPermissionAssign = new ObjectPermission();
		objectPermissionAssign.setPermissionId(permissionId);
		voObjectPermContainerAssign.setObjectPermission(objectPermissionAssign);
		objectPermissionAssign.setCreationType(creationType);
		element.setAssignPermission(voObjectPermContainerAssign);
		if (creationType == CreationType.ADD) {
			element.setAssign(true);
		}/* else if (creationType == CreationType.DELETE) {
			element.setAssign(false);
		}*/
		this.viewer.getCachedObjPermissionMap().put(permissionId, voObjectPermContainerAssign);
		getViewer().refresh(element);
	}

	/* (non-Javadoc)
	 * @see com.magna.xmsystem.xmadmin.ui.parts.role.editingsupport.CheckBoxEditingSupport#canEdit(java.lang.Object)
	 */
	@Override
	protected boolean canEdit(final Object element) {
		if (element instanceof ObjectRelationPermissions) {
			String permissionName = ((ObjectRelationPermissions) element).getPermissionName();
			ObjectRelationType objectRelationType = ObjectRelationType.getObjectType(permissionName);
			if (objectRelationType == ObjectRelationType.DIRECTORY_RELATION || objectRelationType == ObjectRelationType.GROUP_RELATION 
					|| objectRelationType == ObjectRelationType.ADMINISTRATION_AREA_TO_ROLE) {
				return false;
			}
		}
		return true;
	}
}
