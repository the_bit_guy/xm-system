package com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.sitemodel;

import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild;

/**
 * The Class SiteAdminUserAppChildProtected.
 * 
 * @author shashwat.anand
 */
public class SiteAdminAreaUserAppProtected implements IAdminTreeChild {
	/** The parent. */
	private IAdminTreeChild parent;

	/**
	 * Instantiates a new site admin user app child protected.
	 * @param parent the parent
	 */
	public SiteAdminAreaUserAppProtected(final IAdminTreeChild parent) {
		this.parent = parent;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild#
	 * getParent()
	 */
	@Override
	public IAdminTreeChild getParent() {
		return this.parent;
	}

	/* (non-Javadoc)
	 * @see com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild#setParent(com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild)
	 */
	/*
	 * com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild#
	 * setParent(com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.
	 * IAdminTreeChild)
	 */
	@Override
	public void setParent(final IAdminTreeChild parent) {
		this.parent = parent;
	}
}
