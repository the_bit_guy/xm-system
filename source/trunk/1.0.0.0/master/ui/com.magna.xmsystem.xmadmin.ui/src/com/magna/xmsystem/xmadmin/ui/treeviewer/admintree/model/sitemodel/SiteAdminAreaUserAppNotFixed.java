package com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.sitemodel;

import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild;

/**
 * The Class SiteAdminUserAppChildNotFixed.
 */
public class SiteAdminAreaUserAppNotFixed implements IAdminTreeChild {
	/** The parent. */
	private IAdminTreeChild parent;

	/**
	 * Instantiates a new site admin user app child not fixed.
	 *
	 * @param parent the parent
	 */
	public SiteAdminAreaUserAppNotFixed(final IAdminTreeChild parent) {
		this.parent = parent;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild#
	 * getParent()
	 */
	@Override
	public IAdminTreeChild getParent() {
		return this.parent;
	}

	/*
	 * com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild#
	 * setParent(com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.
	 * IAdminTreeChild)
	 */
	@Override
	public void setParent(final IAdminTreeChild parent) {
		this.parent = parent;
	}
}
