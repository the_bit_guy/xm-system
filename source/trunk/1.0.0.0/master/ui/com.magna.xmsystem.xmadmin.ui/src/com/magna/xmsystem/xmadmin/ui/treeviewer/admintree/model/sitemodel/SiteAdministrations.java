package com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.sitemodel;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.stream.Collectors;

import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.adminarea.AdministrationArea;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.refs.RelationObj;

/**
 * The Class SiteAdministrations.
 * 
 * @author subash.janarthanan
 * 
 */
public class SiteAdministrations implements IAdminTreeChild {

	/** The admin areas children. */
	private Map<String, IAdminTreeChild> siteAdminAreasChildren;

	/** The parent. */
	private IAdminTreeChild parent;

	/**
	 * Instantiates a new site administrations.
	 *
	 * @param parent
	 *            the parent
	 */
	public SiteAdministrations(IAdminTreeChild parent) {
		this.parent = parent;
		this.siteAdminAreasChildren = new LinkedHashMap<>();
	}

	/**
	 * Add.
	 *
	 * @param adminAreaId
	 *            the admin area id
	 * @param child
	 *            the child
	 * @return the i admin tree child
	 */
	public IAdminTreeChild add(final String adminAreaId, final IAdminTreeChild child) {
		child.setParent(this);
		IAdminTreeChild returnVal = this.siteAdminAreasChildren.put(adminAreaId, child);
		if (child instanceof RelationObj && ((RelationObj) child).getRefObject() instanceof AdministrationArea) {
			sort();
		}
		return returnVal;
	}

	/**
	 * Remove.
	 *
	 * @param adminAreaId
	 *            the admin area id
	 * @return the i admin tree child
	 */
	public IAdminTreeChild remove(final String adminAreaId) {
		return this.siteAdminAreasChildren.remove(adminAreaId);
	}

	/**
	 * Method for Removes the all.
	 */
	public void removeAll() {
		this.siteAdminAreasChildren.clear();
	}
	
	/**
	 * Gets the administration areas collection.
	 *
	 * @return the administration areas collection
	 */
	public Collection<IAdminTreeChild> getSiteAdministrationAreasCollection() {
		return this.siteAdminAreasChildren.values();
	}

	/**
	 * Gets the adminstration areas children.
	 *
	 * @return the adminstration areas children
	 */
	public Map<String, IAdminTreeChild> getSiteAdminstrationAreasChildren() {
		return siteAdminAreasChildren;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild#
	 * getParent()
	 */
	@Override
	public IAdminTreeChild getParent() {
		return this.parent;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild#
	 * setParent(com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.
	 * IAdminTreeChild)
	 */
	@Override
	public void setParent(IAdminTreeChild parent) {
		this.parent = parent;
	}
	
	/**
	 * Sort.
	 */
	public void sort() {
		final LinkedHashMap<String, IAdminTreeChild> collect = this.siteAdminAreasChildren.entrySet().stream().sorted(
				(e1, e2) -> ((AdministrationArea) (((RelationObj) e1.getValue()).getRefObject())).getName()
				.compareTo(((AdministrationArea) (((RelationObj) e2.getValue()).getRefObject())).getName()))
				.collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e1, LinkedHashMap::new));
		this.siteAdminAreasChildren = collect;
	}

}