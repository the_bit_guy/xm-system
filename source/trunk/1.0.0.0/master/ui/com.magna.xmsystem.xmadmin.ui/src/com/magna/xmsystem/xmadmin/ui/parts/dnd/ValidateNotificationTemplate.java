package com.magna.xmsystem.xmadmin.ui.parts.dnd;

import java.util.Arrays;
import java.util.TreeSet;

import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.notification.NotificationTemplate;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.notification.ProjectActivateEvtAction;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.notification.ProjectCreateEvtAction;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.notification.ProjectDeactivateEvtAction;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.notification.ProjectDeleteEvtAction;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.notification.UserProjectRelAssignEvtAction;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.notification.UserProjectRelRemoveEvtAction;
import com.magna.xmsystem.xmadmin.util.CommonConstants;

// TODO: Auto-generated Javadoc
/**
 * The Class ValidateNotificationTemplate.
 */
public class ValidateNotificationTemplate {

	/**
	 * Validate template.
	 *
	 * @param template the template
	 * @param target the target
	 * @return true, if successful
	 */
	public static boolean validateTemplate(NotificationTemplate template, Object target) {
		TreeSet<String> variables = template.getVariables();
		if (target instanceof ProjectCreateEvtAction) {
			String[] Project_Create_Var = CommonConstants.Notification.PRO_CREATE_VARIABLES;
			for (String var : variables) {
				if (!Arrays.asList(Project_Create_Var).contains(var)) {
					return false;
				}
			}
		} else if (target instanceof ProjectDeleteEvtAction) {
			String[] Project_Delete_Var = CommonConstants.Notification.PRO_DELETE_VARIABLES;
			for (String var : variables) {
				if (!Arrays.asList(Project_Delete_Var).contains(var)) {
					return false;
				}
			}
		} else if (target instanceof ProjectDeactivateEvtAction) {
			String[] Project_Deactivate_Var = CommonConstants.Notification.PRO_DEACTIVATE_VARIABLES;
			for (String var : variables) {
				if (!Arrays.asList(Project_Deactivate_Var).contains(var)) {
					return false;
				}
			}
		} else if (target instanceof ProjectActivateEvtAction) {
			String[] Project_Activate_Var = CommonConstants.Notification.PRO_ACTIVATE_VARIABLES;
			for (String var : variables) {
				if (!Arrays.asList(Project_Activate_Var).contains(var)) {
					return false;
				}
			}
		} else if (target instanceof UserProjectRelAssignEvtAction) {
			String[] Project_Activate_Var = CommonConstants.Notification.USER_PROJECT_RELATION_ASSIGN_VAR;
			for (String var : variables) {
				if (!Arrays.asList(Project_Activate_Var).contains(var)) {
					return false;
				}
			}
		} else if (target instanceof UserProjectRelRemoveEvtAction) {
			String[] Project_Activate_Var = CommonConstants.Notification.USER_PROJECT_RELATION_REMOVE_VAR;
			for (String var : variables) {
				if (!Arrays.asList(Project_Activate_Var).contains(var)) {
					return false;
				}
			}
		}
		return true;
	}
}
