package com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.adminarea;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.stream.Collectors;

import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.startapplicationmodel.StartApplication;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.refs.RelationObj;

// TODO: Auto-generated Javadoc
/**
 * Class for Admin area project start applications.
 *
 * @author Chiranjeevi.Akula
 */
public class AdminAreaProjectStartApplications implements IAdminTreeChild {

	/** Member variable 'parent' for {@link IAdminTreeChild}. */
	private IAdminTreeChild parent;
	
	/** Member variable 'admin area project start app child' for {@link Map<String,IAdminTreeChild>}. */
	private Map<String, IAdminTreeChild> adminAreaProjectStartAppChild;


	/**
	 * Constructor for AdminAreaProjectStartApplications Class.
	 *
	 * @param parent {@link IAdminTreeChild}
	 */
	public AdminAreaProjectStartApplications(final IAdminTreeChild parent) {
		this.parent = parent;
		this.adminAreaProjectStartAppChild = new LinkedHashMap<>();
	}
	
	/**
	 * Gets the admin area project start app child.
	 *
	 * @return the admin area project start app child
	 */
	public Map<String, IAdminTreeChild> getAdminAreaProjectStartAppChild() {
		return adminAreaProjectStartAppChild;
	}
	
	/**
	 * Gets the admin area project start app collection.
	 *
	 * @return the admin area project start app collection
	 */
	public Collection<IAdminTreeChild> getAdminAreaProjectStartAppCollection() {
		return this.adminAreaProjectStartAppChild.values();
	}
	
	/**
	 * Method for Adds the.
	 *
	 * @param adminAreaStartAppChildId {@link String}
	 * @param child {@link IAdminTreeChild}
	 * @return the i admin tree child {@link IAdminTreeChild}
	 */
	public IAdminTreeChild add(final String adminAreaStartAppChildId, final IAdminTreeChild child) {
		child.setParent(this);
		IAdminTreeChild returnVal = this.adminAreaProjectStartAppChild.put(adminAreaStartAppChildId, child);
		if (child instanceof RelationObj && ((RelationObj) child).getRefObject() instanceof StartApplication) {
			sort();
		}
		return returnVal;
	}
	
	/**
	 * Method for Removes the.
	 *
	 * @param adminAreaStartAppChildId {@link String}
	 * @return the i admin tree child {@link IAdminTreeChild}
	 */
	public IAdminTreeChild remove(final String adminAreaStartAppChildId) {
		return this.adminAreaProjectStartAppChild.remove(adminAreaStartAppChildId);
	}

	/* (non-Javadoc)
	 * @see com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild#getParent()
	 */
	@Override
	public IAdminTreeChild getParent() {
		return this.parent;
	}

	/* (non-Javadoc)
	 * @see com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild#setParent(com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild)
	 */
	@Override
	public void setParent(final IAdminTreeChild parent) {
		this.parent = parent;
	}
	
	/**
	 * Sort.
	 */
	public void sort() {
		final LinkedHashMap<String, IAdminTreeChild> collect = this.adminAreaProjectStartAppChild.entrySet().stream().sorted(
				(e1, e2) -> ((StartApplication) (((RelationObj) e1.getValue()).getRefObject())).getName()
				.compareTo(((StartApplication) (((RelationObj) e2.getValue()).getRefObject())).getName()))
				.collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e1, LinkedHashMap::new));
		this.adminAreaProjectStartAppChild = collect;
	}

}
