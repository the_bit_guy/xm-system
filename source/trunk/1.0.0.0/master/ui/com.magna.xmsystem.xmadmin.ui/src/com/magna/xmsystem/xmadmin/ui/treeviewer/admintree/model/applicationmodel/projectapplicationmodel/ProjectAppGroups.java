package com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.projectapplicationmodel;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;

import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild;

/**
 * Class for Project app groups.
 *
 * @author Chiranjeevi.Akula
 */
public class ProjectAppGroups implements IAdminTreeChild {

	/** Member variable 'parent' for {@link IAdminTreeChild}. */
	private IAdminTreeChild parent;

	/** Member variable 'project app groups children' for {@link Map<String,IAdminTreeChild>}. */
	final private Map<String, IAdminTreeChild> projectAppGroupsChildren;

	/**
	 * Constructor for ProjectAppGroups Class.
	 *
	 * @param parent {@link IAdminTreeChild}
	 */
	public ProjectAppGroups(final IAdminTreeChild parent) {
		this.parent = parent;
		this.projectAppGroupsChildren = new LinkedHashMap<>();
	}

	/**
	 * Method for Adds the.
	 *
	 * @param projectAppGroupsModelId {@link String}
	 * @param child {@link IAdminTreeChild}
	 * @return the i admin tree child {@link IAdminTreeChild}
	 */
	public IAdminTreeChild add(final String projectAppGroupsModelId, final IAdminTreeChild child) {
		child.setParent(this);
		return this.projectAppGroupsChildren.put(projectAppGroupsModelId, child);
	}

	/**
	 * Method for Removes the.
	 *
	 * @param projectAppGroupsModelId {@link String}
	 * @return the i admin tree child {@link IAdminTreeChild}
	 */
	public IAdminTreeChild remove(final String projectAppGroupsModelId) {
		return this.projectAppGroupsChildren.remove(projectAppGroupsModelId);
	}

	/**
	 * Gets the project app groups children collection.
	 *
	 * @return the project app groups children collection
	 */
	public Collection<IAdminTreeChild> getProjectAppGroupsChildrenCollection() {
		return this.projectAppGroupsChildren.values();
	}

	/**
	 * Method for Ge project app groups children.
	 *
	 * @return the map {@link Map<String,IAdminTreeChild>}
	 */
	public Map<String, IAdminTreeChild> geProjectAppGroupsChildren() {
		return projectAppGroupsChildren;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild#
	 * getParent()
	 */
	@Override
	public IAdminTreeChild getParent() {
		return this.parent;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild#
	 * setParent(com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.
	 * IAdminTreeChild)
	 */
	@Override
	public void setParent(final IAdminTreeChild parent) {
		this.parent = parent;
	}

}
