package com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.projects;

import java.beans.PropertyChangeEvent;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import org.eclipse.core.runtime.Assert;

import com.magna.xmsystem.dependencies.utils.LANG_ENUM;
import com.magna.xmsystem.dependencies.utils.XMSystemUtil;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.BeanModel;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.adminarea.AdminAreaProjectChild;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.projectapplicationmodel.ProjectAppAdminAreaProjects;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.startapplicationmodel.StartAppProjects;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.icon.Icon;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.directory.DirectoryProjects;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.group.projectgroup.ProjectGroupProjects;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.refs.RelationObj;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.sitemodel.SiteAdminAreaProjectChild;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.users.UserProjectChild;
import com.magna.xmsystem.xmadmin.util.CommonConstants;

/**
 * The Class Project.
 * 
 * @author subash.janarthanan
 * 
 */
public class Project extends BeanModel implements IAdminTreeChild, Cloneable {

	/** The Constant REMARK_LIMIT. */
	public static final int REMARK_LIMIT = 1500;

	/** The Constant DESC_LIMIT. */
	public static final int DESC_LIMIT = 240;

	/** The Constant NAME_LIMIT. */
	public static final int NAME_LIMIT = 30;

	/** The Constant PROPERTY_PROJECTID. */
	public static final String PROPERTY_PROJECTID = "projectId"; //$NON-NLS-1$

	/** The Constant PROPERTY_PROJECTNAME. */
	public static final String PROPERTY_PROJECTNAME = "name"; //$NON-NLS-1$

	/** The Constant PROPERTY_ACTIVE. */
	public static final String PROPERTY_ACTIVE = "active"; //$NON-NLS-1$

	/** The Constant PROPERTY_DESC_MAP. */
	public static final String PROPERTY_DESC_MAP = "descriptionMap"; //$NON-NLS-1$

	/** The Constant PROPERTY_REMARKS_MAP. */
	public static final String PROPERTY_REMARKS_MAP = "remarksMap"; //$NON-NLS-1$

	/** The Constant PROPERTY_OPERATION_MODE. */
	public static final String PROPERTY_OPERATION_MODE = "operationMode"; //$NON-NLS-1$

	/** The Constant PROPERTY_ICON. */
	public static final String PROPERTY_ICON = "icon"; //$NON-NLS-1$

	/** The project id. */
	private String projectId;

	/** The name. */
	private String name;

	/** The active. */
	private boolean active;

	/** The description map. */
	private Map<LANG_ENUM, String> descriptionMap;

	/** The remarks map. */
	private Map<LANG_ENUM, String> remarksMap;

	/** The operation mode. */
	private int operationMode;

	/** The icon. */
	private Icon icon;

	/** The project children. */
	private Map<String, IAdminTreeChild> projectChildren;

	/** The relation id. */
	private String relationId;

	/**
	 * Instantiates a new project.
	 *
	 * @param projectId
	 *            the project id
	 * @param name
	 *            the name
	 * @param isActive
	 *            the is active
	 * @param icon
	 *            the icon
	 * @param operationMode
	 *            the operation mode
	 */
	public Project(final String projectId, final String name, final boolean isActive, final Icon icon,
			final int operationMode) {
		this(projectId, name, isActive, new HashMap<>(), new HashMap<>(), icon, operationMode);
	}

	/**
	 * Instantiates a new project.
	 *
	 * @param projectId
	 *            the project id
	 * @param projectName
	 *            the project name
	 * @param isProjectActive
	 *            the is project active
	 * @param projectDescriptionMap
	 *            the project description map
	 * @param projectRemarksMap
	 *            the project remarks map
	 * @param projectIcon
	 *            the project icon
	 * @param projectOperationMode
	 *            the project operation mode
	 */
	public Project(String projectId, String projectName, boolean isProjectActive,
			Map<LANG_ENUM, String> projectDescriptionMap, Map<LANG_ENUM, String> projectRemarksMap, Icon projectIcon,
			int projectOperationMode) {
		super();
		this.projectId = projectId;
		this.name = projectName;
		this.active = isProjectActive;
		this.descriptionMap = projectDescriptionMap;
		this.remarksMap = projectRemarksMap;
		this.icon = projectIcon;
		this.operationMode = projectOperationMode;
		this.projectChildren = new LinkedHashMap<>();
		addFixedChildren();
	}

	/**
	 * Method for Adds the fixed children.
	 */
	public void addFixedChildren() {
		this.projectChildren.put(ProjectAdminAreas.class.getName(), new ProjectAdminAreas(this));
		this.projectChildren.put(ProjectUsers.class.getName(), new ProjectUsers(this));
		this.projectChildren.put(ProjectProjectApplications.class.getName(), new ProjectProjectApplications(this));
		//this.projectChildren.put(ProjectGroups.class.getName(), new ProjectGroups(this));
	}
	
	/**
	 * Gets the project id.
	 *
	 * @return the project id
	 */
	public String getProjectId() {
		return projectId;
	}

	/**
	 * Sets the project id.
	 *
	 * @param projectId
	 *            the new project id
	 */
	public void setProjectId(String projectId) {
		this.propertyChangeSupport.firePropertyChange(PROPERTY_PROJECTID, this.projectId, this.projectId = projectId);
	}

	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Sets the name.
	 *
	 * @param name
	 *            the new name
	 */
	public void setName(final String name) {
		if (name == null)
			throw new IllegalArgumentException();
		this.propertyChangeSupport.firePropertyChange(PROPERTY_PROJECTNAME, this.name, this.name = name.trim());
	}

	/**
	 * Checks if is active.
	 *
	 * @return true, if is active
	 */
	public boolean isActive() {
		return active;
	}

	/**
	 * Sets the active.
	 *
	 * @param isActive
	 *            the new active
	 */
	public void setActive(final boolean isActive) {
		this.propertyChangeSupport.firePropertyChange(PROPERTY_ACTIVE, this.active, this.active = isActive);
	}

	/**
	 * Gets the project sites.
	 *
	 * @return the project sites
	 */
	public Map<String, IAdminTreeChild> getProjectChildren() {
		return this.projectChildren;
	}

	/**
	 * Sets the project sites.
	 *
	 * @param projectChildren the project children
	 */
	public void setProjectChildren(Map<String, IAdminTreeChild> projectChildren) {
		this.projectChildren = projectChildren;
	}

	/**
	 * Gets the description map.
	 *
	 * @return the description map
	 */
	public Map<LANG_ENUM, String> getDescriptionMap() {
		return descriptionMap;
	}

	/**
	 * Gets the description.
	 *
	 * @param lang
	 *            the lang
	 * @return the description
	 */
	public String getDescription(final LANG_ENUM lang) {
		return this.descriptionMap.get(lang);
	}

	/**
	 * Set description.
	 *
	 * @param lang
	 *            the lang
	 * @param description
	 *            the description
	 */
	public void setDescription(final LANG_ENUM lang, final String description) {
		if (lang == null || description == null)
			throw new IllegalArgumentException();
		this.propertyChangeSupport.firePropertyChange(PROPERTY_DESC_MAP, this.descriptionMap,
				this.descriptionMap.put(lang, description.trim()));
	}

	/**
	 * Set description map.
	 *
	 * @param descriptionMap
	 *            the description map
	 */
	public void setDescriptionMap(final Map<LANG_ENUM, String> descriptionMap) {
		if (descriptionMap == null)
			throw new IllegalArgumentException();
		this.propertyChangeSupport.firePropertyChange(PROPERTY_DESC_MAP, this.descriptionMap,
				this.descriptionMap = descriptionMap);
	}

	/**
	 * Gets the remarks map.
	 *
	 * @return the remarks map
	 */
	public Map<LANG_ENUM, String> getRemarksMap() {
		return remarksMap;
	}

	/**
	 * Gets the remarks.
	 *
	 * @param lang
	 *            the lang
	 * @return the remarks
	 */
	public String getRemarks(final LANG_ENUM lang) {
		return this.remarksMap.get(lang);
	}

	/**
	 * Set remarks map.
	 *
	 * @param remarksMap
	 *            the remarks map
	 */
	public void setRemarksMap(final Map<LANG_ENUM, String> remarksMap) {
		if (remarksMap == null)
			throw new IllegalArgumentException();
		this.propertyChangeSupport.firePropertyChange(PROPERTY_REMARKS_MAP, this.remarksMap,
				this.remarksMap = remarksMap);
	}

	/**
	 * Set remarks.
	 *
	 * @param lang
	 *            the lang
	 * @param remarks
	 *            the remarks
	 */
	public void setRemarks(final LANG_ENUM lang, final String remarks) {
		if (lang == null || remarks == null)
			throw new IllegalArgumentException();
		this.propertyChangeSupport.firePropertyChange(PROPERTY_REMARKS_MAP, this.remarksMap,
				this.remarksMap.put(lang, remarks));
	}

	public String getRelationId() {
		return relationId;
	}

	public void setRelationId(String relationId) {
		this.relationId = relationId;
	}

	/**
	 * Gets the operation mode.
	 *
	 * @return the operation mode
	 */
	public int getOperationMode() {
		return operationMode;
	}

	/**
	 * Sets the operation mode.
	 *
	 * @param operationMode
	 *            the new operation mode
	 */
	public void setOperationMode(final int operationMode) {
		this.propertyChangeSupport.firePropertyChange(PROPERTY_OPERATION_MODE, this.operationMode,
				this.operationMode = operationMode);
	}

	/**
	 * Gets the icon.
	 *
	 * @return the icon
	 */
	public Icon getIcon() {
		return icon;
	}

	/**
	 * Sets the project icon.
	 *
	 * @param icon
	 *            the new project icon
	 */
	public void setProjectIcon(final Icon icon) {
		if (icon == null)
			throw new IllegalArgumentException();
		this.propertyChangeSupport.firePropertyChange(PROPERTY_ICON, this.icon, this.icon = icon);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.BeanModel#
	 * propertyChange(java.beans.PropertyChangeEvent)
	 */
	@Override
	public void propertyChange(PropertyChangeEvent event) {
		this.propertyChangeSupport.firePropertyChange(event.getPropertyName(), event.getOldValue(),
				event.getNewValue());
	}

	/**
	 * Deep copy project.
	 *
	 * @param update
	 *            the update
	 * @param updateThisObject
	 *            the update this object
	 * @return the project
	 */
	public Project deepCopyProject(boolean update, Project updateThisObject) {
		Project clonedProject = null;
		try {
			String currentProjectId = XMSystemUtil.isEmpty(this.getProjectId()) ? CommonConstants.EMPTY_STR
					: this.getProjectId();
			String currentProjectName = XMSystemUtil.isEmpty(this.getName()) ? CommonConstants.EMPTY_STR
					: this.getName();
			boolean currentProjectIsActive = this.isActive();
			Map<LANG_ENUM, String> currentTranslationIdMap = new HashMap<>();
			Map<LANG_ENUM, String> currentDescriptionMap = new HashMap<>();
			Map<LANG_ENUM, String> currentRemarksMap = new HashMap<>();

			for (LANG_ENUM langEnum : LANG_ENUM.values()) {
				String translationId = this.getTranslationId(langEnum);
				currentTranslationIdMap.put(langEnum,
						XMSystemUtil.isEmpty(translationId) ? CommonConstants.EMPTY_STR : translationId);

				String description = this.getDescription(langEnum);
				currentDescriptionMap.put(langEnum,
						XMSystemUtil.isEmpty(description) ? CommonConstants.EMPTY_STR : description);

				String remarks = this.getRemarks(langEnum);
				currentRemarksMap.put(langEnum, XMSystemUtil.isEmpty(remarks) ? CommonConstants.EMPTY_STR : remarks);
			}

			//Icon currentProjectIcon = this.getIcon() == null ? null : this.getIcon();
			Icon currentProjectIcon = new Icon(this.getIcon().getIconId(), this.getIcon().getIconName(),
					this.getIcon().getIconPath(), this.getIcon().getIconType());
			if (update) {
				clonedProject = updateThisObject;
			} else {
				clonedProject = new Project(currentProjectId, currentProjectName, currentProjectIsActive,
						currentProjectIcon, CommonConstants.OPERATIONMODE.VIEW);
			}
			clonedProject.setProjectId(currentProjectId);
			clonedProject.setName(currentProjectName);
			clonedProject.setActive(currentProjectIsActive);
			clonedProject.setProjectIcon(currentProjectIcon);
			clonedProject.setTranslationIdMap(currentTranslationIdMap);
			clonedProject.setDescriptionMap(currentDescriptionMap);
			clonedProject.setRemarksMap(currentRemarksMap);
			return clonedProject;
		} catch (Exception e) {
			return null;
		}
	}
	
	/* (non-Javadoc)
	 * @see com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.BeanModel#getAdapter(java.lang.Class, com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild)
	 */
	@Override
	public <T> RelationObj getAdapter(Class<T> adapterType, IAdminTreeChild parent, String relationId, boolean relationStatus) {
		Assert.isNotNull(adapterType);
		Assert.isNotNull(this);
		if (adapterType == SiteAdminAreaProjectChild.class) {
			SiteAdminAreaProjectChild siteAdministrationProjectChild = new SiteAdminAreaProjectChild(parent);
			RelationObj relObj = new RelationObj(relationId, this, siteAdministrationProjectChild, relationStatus, null);
			return relObj;
		} else if (adapterType == AdminAreaProjectChild.class) {
			AdminAreaProjectChild adminAreaProChild = new AdminAreaProjectChild(parent);
			RelationObj relObj = new RelationObj(relationId, this, adminAreaProChild, relationStatus, null);
			return relObj;
		} else if (adapterType == UserProjectChild.class) {
			UserProjectChild userProjectChild = new UserProjectChild(parent);
			RelationObj relObj = new RelationObj(relationId, this, userProjectChild, relationStatus, null);
			return relObj;
		} else if (adapterType == StartAppProjects.class) {
			StartAppProjects startAppProjects = new StartAppProjects(parent);
			RelationObj relObj = new RelationObj(relationId, this, startAppProjects, relationStatus, null);
			return relObj;
		} else if (adapterType == ProjectAppAdminAreaProjects.class) {
			ProjectAppAdminAreaProjects projectAppAdminAreaProjects = new ProjectAppAdminAreaProjects(parent);
			RelationObj relObj = new RelationObj(relationId, this, projectAppAdminAreaProjects, relationStatus, null);
			return relObj;
		} else if (adapterType == DirectoryProjects.class) {
			DirectoryProjects directoryProjects = new DirectoryProjects(parent);
			RelationObj relObj = new RelationObj(relationId, this, directoryProjects, relationStatus, null);
			return relObj;
		}else if (adapterType == ProjectGroupProjects.class) {
			ProjectGroupProjects projectGroupProjects = new ProjectGroupProjects(parent);
			RelationObj relObj = new RelationObj(relationId, this, projectGroupProjects, relationStatus, null);
			return relObj;
		}
		
		return super.getAdapter(adapterType, parent, relationId, relationStatus);
	}
}
