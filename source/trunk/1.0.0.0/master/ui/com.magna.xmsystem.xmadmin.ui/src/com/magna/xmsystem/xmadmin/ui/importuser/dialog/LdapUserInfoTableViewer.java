package com.magna.xmsystem.xmadmin.ui.importuser.dialog;

import org.eclipse.jface.layout.TableColumnLayout;
import org.eclipse.jface.viewers.ColumnWeightData;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TableViewerColumn;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

// TODO: Auto-generated Javadoc

/**
 * The Class LdapUserInfoTableViewer.
 * 
 * @author archita.patel
 */
public class LdapUserInfoTableViewer extends TableViewer {

	/** The Constant LOGGER. */
	private static final Logger LOGGER = LoggerFactory.getLogger(LdapUserInfoTableViewer.class);

	/**
	 * Instantiates a new ldap user info table viewer.
	 *
	 * @param parent
	 *            the parent
	 */
	public LdapUserInfoTableViewer(final Composite parent) {
		super(parent, SWT.FULL_SELECTION | SWT.V_SCROLL | SWT.NO_SCROLL | SWT.BORDER | SWT.SINGLE);
		this.init();
	}

	/**
	 * Inits the.
	 */
	private void init() {
		try {
			final Table table = this.getTable();
			table.setLinesVisible(true);
			table.setHeaderVisible(true);
			this.initColumn();
		} catch (Exception e) {
			LOGGER.error("Execption ocuured at creating the UI", e); //$NON-NLS-1$
		}
	}

	/**
	 * Inits the column.
	 */
	private void initColumn() {
		try {
			final TableColumnLayout layout = new TableColumnLayout();
			this.getTable().getParent().setLayout(layout);
			createTableViewerColumn(layout);
			createTableViewerColumn(layout);
		} catch (Exception e) {
			LOGGER.error("Execption ocuured at creating the columns", e); //$NON-NLS-1$
		}
	}

	
	/**
	 * Creates the table viewer column.
	 *
	 * @param layout the layout
	 */
	private void createTableViewerColumn(final TableColumnLayout layout) {
		final TableViewerColumn viewerCol = new TableViewerColumn(this, SWT.NONE);
		final TableColumn column = viewerCol.getColumn();
		column.setWidth(100);
		layout.setColumnData(column, new ColumnWeightData(50));
	}

}
