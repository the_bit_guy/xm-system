package com.magna.xmsystem.xmadmin.ui.parts.unassignedusermessage;

import org.eclipse.jface.layout.GridDataFactory;
import org.eclipse.jface.layout.GridLayoutFactory;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.StyledText;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.noProjectMessage.NoProjectPopMessage;

/**
 * The Class NoProjectPopMessageCompositeUI.
 */
public class NoProjectPopMessageCompositeUI extends Composite {
	/** Logger instance. */
	private static final Logger LOGGER = LoggerFactory.getLogger(NoProjectPopMessageCompositeUI.class);

	/** The grp Pop Msg config. */
	protected Group grpPopMsgConfig;

	/** The lbl pop msg. */
	protected Label lblPopMsg;

	/** The pop msg styled txt. */
	protected StyledText popMsgStyledTxt;

	/** The btn save. */
	protected Button btnSave;

	/** The btn cancel. */
	protected Button btnCancel;

	/**
	 * Instantiates a new no project pop message composite UI.
	 *
	 * @param parent
	 *            the parent
	 * @param style
	 *            the style
	 */
	public NoProjectPopMessageCompositeUI(Composite parent, int style) {
		super(parent, style);
		this.initGUI(parent);
	}

	/**
	 * Inits the GUI.
	 *
	 * @param widgetContainer the widget container
	 */
	private void initGUI(Composite widgetContainer) {
		
		try {
			GridLayoutFactory.fillDefaults().applyTo(this);
			this.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));

			this.grpPopMsgConfig = new Group(this, SWT.NONE);
			GridLayoutFactory.fillDefaults().numColumns(1).applyTo(this.grpPopMsgConfig);
			GridDataFactory.fillDefaults().grab(true, true).span(SWT.FILL, SWT.FILL).applyTo(this.grpPopMsgConfig);

			widgetContainer = new Composite(this.grpPopMsgConfig, SWT.NONE);
			final GridLayout widgetContLayout = new GridLayout(2, false);
			widgetContLayout.marginTop = 10;
			widgetContLayout.horizontalSpacing = 10;

			widgetContainer.setLayout(widgetContLayout);
			widgetContainer.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));

			this.lblPopMsg = new Label(widgetContainer, SWT.NONE);
			GridDataFactory.fillDefaults().grab(false, false).span(1, 1).align(SWT.LEFT, SWT.TOP)
			.applyTo(this.lblPopMsg);
			this.popMsgStyledTxt = new StyledText(widgetContainer,
					SWT.BORDER | SWT.MULTI  | SWT.V_SCROLL | SWT.WRAP);
			GridDataFactory.fillDefaults().grab(true, false).span(1, 1).hint(40, 100).align(SWT.FILL, SWT.CENTER)
					.applyTo(this.popMsgStyledTxt);
			this.popMsgStyledTxt.setTextLimit(NoProjectPopMessage.DETAILS_LIMIT);
			this.popMsgStyledTxt.setBackground(Display.getDefault().getSystemColor(SWT.COLOR_WHITE));

			final Composite buttonBarComp = new Composite(widgetContainer, SWT.NONE);
			final GridLayout btnBarCompLayout = new GridLayout(2, true);
			btnBarCompLayout.marginRight = 0;
			btnBarCompLayout.marginLeft = 0;
			btnBarCompLayout.marginTop = 0;
			btnBarCompLayout.marginBottom = 0;
			btnBarCompLayout.marginWidth = 0;
			buttonBarComp.setLayout(btnBarCompLayout);
			buttonBarComp.setLayoutData(new GridData(SWT.FILL, SWT.DEFAULT, true, false, 3, 1));
			createButtonBar(buttonBarComp);
		} catch (Exception e) {
			LOGGER.error("Unable to crete UI elements", e);
		}

	}

	/**
	 * Creates the button bar.
	 *
	 * @param buttonBarComp
	 *            the button bar comp
	 */
	private void createButtonBar(final Composite buttonBarComp) {
		this.btnSave = new Button(buttonBarComp, SWT.NONE);
		this.btnSave.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, true, false));

		this.btnCancel = new Button(buttonBarComp, SWT.NONE);
		this.btnCancel.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, true, false));
	}

}
