package com.magna.xmsystem.xmadmin.ui.handlers;

import javax.inject.Inject;

import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.core.services.nls.Translation;
import org.eclipse.e4.ui.workbench.IWorkbench;
import org.eclipse.swt.widgets.Shell;

import com.magna.xmsystem.ui.controls.widgets.CustomMessageDialog;
import com.magna.xmsystem.xmadmin.message.Message;

/**
 * The Class QuitHandler.
 */
public class QuitHandler {
	
	/** Member variable for messages. */
	@Inject
	@Translation
	private Message messages;
	
	/**
	 * Execute.
	 *
	 * @param workbench the workbench
	 * @param shell the shell
	 */
	@Execute
	public void execute(IWorkbench workbench, Shell shell){
		if (CustomMessageDialog.openConfirm(shell, messages.exitDialogTitle,
				messages.exitDialogMessage)) {
			workbench.close();
		}
	}
}
