package com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.projectapplicationmodel;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.stream.Collectors;

import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.adminarea.AdministrationArea;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.refs.RelationObj;

/**
 * Class for Project app admin areas.
 *
 * @author Chiranjeevi.Akula
 */
public class ProjectAppAdminAreas implements IAdminTreeChild{
	
	/** Member variable 'project app admin areas children' for {@link Map<String,IAdminTreeChild>}. */
	private Map<String, IAdminTreeChild> projectAppAdminAreasChildren;
	
	/** Member variable 'parent' for {@link IAdminTreeChild}. */
	private IAdminTreeChild parent;

	/**
	 * Constructor for ProjectAppAdminAreas Class.
	 *
	 * @param parent {@link IAdminTreeChild}
	 */
	public ProjectAppAdminAreas(final IAdminTreeChild parent) {
		this.parent = parent;
		this.projectAppAdminAreasChildren = new LinkedHashMap<>();
	}

	/**
	 * Method for Adds the.
	 *
	 * @param projectAppAdminAreaId {@link String}
	 * @param child {@link IAdminTreeChild}
	 * @return the i admin tree child {@link IAdminTreeChild}
	 */
	public IAdminTreeChild add(final String projectAppAdminAreaId, final IAdminTreeChild child) {
		child.setParent(this);
		IAdminTreeChild returnVal = this.projectAppAdminAreasChildren.put(projectAppAdminAreaId, child);
		if (child instanceof RelationObj && ((RelationObj) child).getRefObject() instanceof AdministrationArea) {
			sort();
		}
		return returnVal;
	}

	/**
	 * Method for Removes the.
	 *
	 * @param projectAppAdminAreaId {@link String}
	 * @return the i admin tree child {@link IAdminTreeChild}
	 */
	public IAdminTreeChild remove(final String projectAppAdminAreaId) {
		return this.projectAppAdminAreasChildren.remove(projectAppAdminAreaId);
	}

	/**
	 * Method for Removes the all.
	 */
	public void removeAll() {
		this.projectAppAdminAreasChildren.clear();
	}
	/**
	 * Gets the project app admin area children collection.
	 *
	 * @return the project app admin area children collection
	 */
	public Collection<IAdminTreeChild> getProjectAppAdminAreaChildrenCollection() {
		return this.projectAppAdminAreasChildren.values();
	}

	/**
	 * Gets the project app admin area children.
	 *
	 * @return the project app admin area children
	 */
	public Map<String, IAdminTreeChild> getProjectAppAdminAreaChildren() {
		return projectAppAdminAreasChildren;
	}

	/* (non-Javadoc)
	 * @see com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild#getParent()
	 */
	@Override
	public IAdminTreeChild getParent() {
		return this.parent;
	}

	/* (non-Javadoc)
	 * @see com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild#setParent(com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild)
	 */
	@Override
	public void setParent(IAdminTreeChild parent) {
		this.parent = parent;
	}

	/**
	 * Sort.
	 */
	public void sort() {
		final LinkedHashMap<String, IAdminTreeChild> collect = this.projectAppAdminAreasChildren.entrySet().stream().sorted(
				(e1, e2) -> ((AdministrationArea) (((RelationObj) e1.getValue()).getRefObject())).getName()
				.compareTo(((AdministrationArea) (((RelationObj) e2.getValue()).getRefObject())).getName()))
				.collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e1, LinkedHashMap::new));
		this.projectAppAdminAreasChildren = collect;
	}

}
