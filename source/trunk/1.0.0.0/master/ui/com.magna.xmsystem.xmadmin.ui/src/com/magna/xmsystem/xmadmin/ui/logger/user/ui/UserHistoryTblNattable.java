package com.magna.xmsystem.xmadmin.ui.logger.user.ui;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.nebula.widgets.nattable.NatTable;
import org.eclipse.nebula.widgets.nattable.config.AbstractRegistryConfiguration;
import org.eclipse.nebula.widgets.nattable.config.ConfigRegistry;
import org.eclipse.nebula.widgets.nattable.config.DefaultNatTableStyleConfiguration;
import org.eclipse.nebula.widgets.nattable.config.IConfigRegistry;
import org.eclipse.nebula.widgets.nattable.data.ListDataProvider;
import org.eclipse.nebula.widgets.nattable.data.convert.DefaultDoubleDisplayConverter;
import org.eclipse.nebula.widgets.nattable.layer.DataLayer;
import org.eclipse.nebula.widgets.nattable.layer.cell.ColumnOverrideLabelAccumulator;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.client.ResourceAccessException;

import com.magna.xmbackend.entities.UserHistoryTbl;
import com.magna.xmbackend.vo.userHistory.UserHistoryRequest;
import com.magna.xmbackend.vo.userHistory.UserHistoryResponseWrapper;
import com.magna.xmsystem.dependencies.customExceptions.UnauthorizedAccessException;
import com.magna.xmsystem.ui.controls.widgets.CustomMessageDialog;
import com.magna.xmsystem.xmadmin.restclient.userhistory.UserHistoryController;
import com.magna.xmsystem.xmadmin.ui.logger.HistoryTableUtil;
import com.magna.xmsystem.xmadmin.util.CommonConstants;

/**
 * The Class UserHistoryTblNattable.
 */
public class UserHistoryTblNattable {
	
	/** The Constant LOGGER. */
	private static final Logger LOGGER = LoggerFactory.getLogger(UserHistoryTblNattable.class);

	/** The underlying layer. */
	private UserHistoryTblGridLayer underlyingLayer;
	
	/** The nat table. */
	private NatTable natTable;
	
	/** The uer history job. */
	private Job uerHistoryJob;

	/**
	 * Creates the user history nat table.
	 *
	 * @param parent the parent
	 * @param status the status
	 * @param userHistoryRequest the user history request
	 * @return the nat table
	 */
	@SuppressWarnings("rawtypes")
	public NatTable createUserHistoryNatTable(final Composite parent, final String status, final UserHistoryRequest userHistoryRequest) {
		IConfigRegistry configRegistry = new ConfigRegistry();

		ArrayList<UserHistoryTbl> userHistoryTableList = retrieveUserHistoryTableList(status,userHistoryRequest);

		underlyingLayer = new UserHistoryTblGridLayer(configRegistry, userHistoryTableList);

		DataLayer bodyDataLayer = underlyingLayer.getBodyDataLayer();
		ListDataProvider dataProvider = underlyingLayer.getBodyDataProvider();

		// NOTE: Register the accumulator on the body data layer.
		// This ensures that the labels are bound to the column index and are
		// unaffected by column order.
		final ColumnOverrideLabelAccumulator columnLabelAccumulator = new ColumnOverrideLabelAccumulator(bodyDataLayer);
		bodyDataLayer.setConfigLabelAccumulator(columnLabelAccumulator);

		natTable = new NatTable(parent, underlyingLayer, false);

		GridData gd_table = new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1);
		gd_table.widthHint = 600;
		gd_table.minimumHeight = 250;
		gd_table.heightHint = 250;
		natTable.setLayoutData(gd_table);

		HistoryTableUtil.getInstance().addCustomStyling(natTable, dataProvider);
		natTable.addConfiguration(new DefaultNatTableStyleConfiguration());
		HistoryTableUtil.getInstance().addColumnHighlight(configRegistry);

		// natTable.addConfiguration(new DebugMenuConfiguration(natTable));
		natTable.addConfiguration(new FilterRowCustomConfiguration());

		natTable.setConfigRegistry(configRegistry);
		natTable.configure();
		
		return natTable;
	}

	/**
	 * Retrieve user history table list.
	 *
	 * @param status the status
	 * @param userHistoryRequest the user history request
	 * @return the array list
	 */
	private ArrayList<UserHistoryTbl> retrieveUserHistoryTableList(String status, UserHistoryRequest userHistoryRequest) {
		ArrayList<UserHistoryTbl> userHistoryTableList = new ArrayList<>();
		UserHistoryResponseWrapper userHistoryResponseWrapper;
		UserHistoryController userHistoryController = new UserHistoryController();
		try {
			if(status.equals(CommonConstants.TRUE)){
				 userHistoryResponseWrapper = userHistoryController.getAllUserStatus(userHistoryRequest);
			}else{
			 userHistoryResponseWrapper = userHistoryController.getAllUserHistory(userHistoryRequest);
			}
			if (userHistoryResponseWrapper != null) {
				List<Map<String, Object>> queryResultSet = userHistoryResponseWrapper.getQueryResultSet();
				for (Map<String, Object> map : queryResultSet) {
					UserHistoryTbl userHistoryTbl = new UserHistoryTbl();
					userHistoryTbl.setId(Long.valueOf(String.valueOf(map.get(UserHistoryColumnEnum.ID.name()))));
					Long object = (Long) map.get(UserHistoryColumnEnum.LOG_TIME.name());
					userHistoryTbl.setLogTime(new Date(object));
					userHistoryTbl.setUserName((String) map.get(UserHistoryColumnEnum.USER_NAME.name()));
					userHistoryTbl.setHost((String) map.get(UserHistoryColumnEnum.HOST.name()));
					userHistoryTbl.setSite((String) map.get(UserHistoryColumnEnum.SITE.name()));
					userHistoryTbl.setAdminArea((String) map.get(UserHistoryColumnEnum.ADMIN_AREA.name()));
					userHistoryTbl.setProject((String) map.get(UserHistoryColumnEnum.PROJECT.name()));
					userHistoryTbl.setApplication((String) map.get(UserHistoryColumnEnum.APPLICATION.name()));
					Object pId = map.get(UserHistoryColumnEnum.PID.name());
					if (pId !=null) {
						userHistoryTbl.setPid((Integer) map.get(UserHistoryColumnEnum.PID.name()));
					}
					userHistoryTbl.setResult((String) map.get(UserHistoryColumnEnum.RESULT.name()));
					userHistoryTbl.setArgs((String) map.get(UserHistoryColumnEnum.ARGS.name()));
					userHistoryTbl.setIsUserStatus((String) map.get(UserHistoryColumnEnum.IS_USER_STATUS.name()));
					userHistoryTableList.add(userHistoryTbl);
				} 
			}
			return userHistoryTableList;
		} catch (NumberFormatException e) {
			LOGGER.error("Exception occured while loading userHistory data " + e);
		} catch (UnauthorizedAccessException e) {
			LOGGER.error("Current user is Unauthorized " + e);
		}catch (Exception e) {
			if (e instanceof ResourceAccessException) {
				/*Display.getDefault().asyncExec(new Runnable() {
					@Override
					public void run() {
						CustomMessageDialog.openError(Display.getDefault().getActiveShell(),"Error",
								"Server not reachable");
					}
				});*/
				throw e;
			
			}
			LOGGER.error("Exception occured while loading userHistory data " + e);
		}
		return userHistoryTableList;
	}

	/**
	 * The Class FilterRowCustomConfiguration.
	 */
	static class FilterRowCustomConfiguration extends AbstractRegistryConfiguration {

		/** The double display converter. */
		final DefaultDoubleDisplayConverter doubleDisplayConverter = new DefaultDoubleDisplayConverter();

		/* (non-Javadoc)
		 * @see org.eclipse.nebula.widgets.nattable.config.IConfiguration#configureRegistry(org.eclipse.nebula.widgets.nattable.config.IConfigRegistry)
		 */
		@Override
		public void configureRegistry(IConfigRegistry configRegistry) {
			// // Configure custom comparator on the rating column
			// configRegistry.registerConfigAttribute(
			// FilterRowConfigAttributes.FILTER_COMPARATOR,
			// getIngnorecaseComparator(),
			// DisplayMode.NORMAL,
			// FilterRowDataLayer.FILTER_ROW_COLUMN_LABEL_PREFIX + 2);
			//
			// // If threshold comparison is used we have to convert the string
			// // entered by the user to the correct underlying type (double),
			// so
			// // that it can be compared
			//
			// // Configure Bid column
			// configRegistry.registerConfigAttribute(
			// FilterRowConfigAttributes.FILTER_DISPLAY_CONVERTER,
			// this.doubleDisplayConverter,
			// DisplayMode.NORMAL,
			// FilterRowDataLayer.FILTER_ROW_COLUMN_LABEL_PREFIX + 5);
			// configRegistry.registerConfigAttribute(
			// FilterRowConfigAttributes.TEXT_MATCHING_MODE,
			// TextMatchingMode.REGULAR_EXPRESSION,
			// DisplayMode.NORMAL,
			// FilterRowDataLayer.FILTER_ROW_COLUMN_LABEL_PREFIX + 5);
			//
			// // Configure Ask column
			// configRegistry.registerConfigAttribute(
			// FilterRowConfigAttributes.FILTER_DISPLAY_CONVERTER,
			// this.doubleDisplayConverter,
			// DisplayMode.NORMAL,
			// FilterRowDataLayer.FILTER_ROW_COLUMN_LABEL_PREFIX + 6);
			// configRegistry.registerConfigAttribute(
			// FilterRowConfigAttributes.TEXT_MATCHING_MODE,
			// TextMatchingMode.REGULAR_EXPRESSION,
			// DisplayMode.NORMAL,
			// FilterRowDataLayer.FILTER_ROW_COLUMN_LABEL_PREFIX + 6);
			//
			// // Configure a combo box on the pricing type column
			//
			// // Register a combo box editor to be displayed in the filter row
			// // cell when a value is selected from the combo, the object is
			// // converted to a string using the converter (registered below)
			// configRegistry.registerConfigAttribute(
			// EditConfigAttributes.CELL_EDITOR,
			// new ComboBoxCellEditor(Arrays.asList(new PricingTypeBean("MN"),
			// new PricingTypeBean("AT"))),
			// DisplayMode.NORMAL,
			// FilterRowDataLayer.FILTER_ROW_COLUMN_LABEL_PREFIX + 4);
			//
			// // The pricing bean object in column is converted to using this
			// // display converter
			// // A 'text' match is then performed against the value from the
			// combo
			// // box
			// configRegistry.registerConfigAttribute(
			// FilterRowConfigAttributes.FILTER_DISPLAY_CONVERTER,
			// new PricingTypeBeanDisplayConverter(),
			// DisplayMode.NORMAL,
			// FilterRowDataLayer.FILTER_ROW_COLUMN_LABEL_PREFIX + 4);
			//
			// configRegistry.registerConfigAttribute(
			// CellConfigAttributes.DISPLAY_CONVERTER,
			// new PricingTypeBeanDisplayConverter(),
			// DisplayMode.NORMAL,
			// FilterRowDataLayer.FILTER_ROW_COLUMN_LABEL_PREFIX + 4);
			//
			// configRegistry.registerConfigAttribute(
			// CellConfigAttributes.DISPLAY_CONVERTER,
			// new PricingTypeBeanDisplayConverter(),
			// DisplayMode.NORMAL,
			// "PRICING_TYPE_PROP_NAME");
		}
	}

	/**
	 * Gets the ingnorecase comparator.
	 *
	 * @return the ingnorecase comparator
	 */
	@SuppressWarnings({ "rawtypes", "unused" })
	private static Comparator getIngnorecaseComparator() {
		return new Comparator() {
			public int compare(String o1, String o2) {
				return o1.compareToIgnoreCase(o2);
			}

			@Override
			public int compare(Object arg0, Object arg1) {
				return arg0.toString().compareToIgnoreCase(arg1.toString());
			}
		};
	};
	
	/**
	 * Update nat table.
	 *
	 * @param status the status
	 * @param userHistoryRequest the user history request
	 */
	public void updateNatTable(String status, UserHistoryRequest userHistoryRequest) {
		
		if (uerHistoryJob != null && uerHistoryJob.getState() == Job.RUNNING) {
			//uerHistoryJob.done(Status.OK_STATUS);
			return;
		}

		uerHistoryJob = new Job(CommonConstants.History.USER_HISTORY_JOB_NAME) {
			@Override
			protected IStatus run(IProgressMonitor monitor) {
				monitor.beginTask(CommonConstants.History.USER_HISTORY_JOB_NAME, 100);
				monitor.worked(30);
				try {
					Display.getDefault().asyncExec(new Runnable() {
						@Override
						public void run() {
							//if (!uerHistoryJob.cancel()) {
							if (underlyingLayer != null) {
								underlyingLayer.getBodyDataProvider().getList().clear();
							}
							//}
						}
					});
					ArrayList<UserHistoryTbl> retrieveUserHistoryTableList = retrieveUserHistoryTableList(status,
							userHistoryRequest);
					Display.getDefault().asyncExec(new Runnable() {
						@Override
						public void run() {
							//if (!uerHistoryJob.cancel()) {
							if (underlyingLayer != null && natTable != null && !natTable.isDisposed()) {
								underlyingLayer.getBodyDataProvider().getList().addAll(retrieveUserHistoryTableList);
								natTable.update();
								natTable.refresh();
							}
							//}
						}
					});

				} catch (Exception e) {
					if (e instanceof ResourceAccessException) {
						Display.getDefault().asyncExec(new Runnable() {
							@Override
							public void run() {
								CustomMessageDialog.openError(Display.getDefault().getActiveShell(), "Error",
										"Server not reachable");
							}
						});
					}
					LOGGER.error(e.getMessage());
				}

				monitor.worked(70);
				return Status.OK_STATUS;
			}
		};
		uerHistoryJob.setUser(true);
		uerHistoryJob.schedule();
	};
}

