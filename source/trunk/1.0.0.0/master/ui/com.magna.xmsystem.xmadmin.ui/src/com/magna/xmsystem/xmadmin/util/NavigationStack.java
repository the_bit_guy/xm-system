package com.magna.xmsystem.xmadmin.util;

import java.util.Stack;

/**
 * The Class NavigationStack.
 *
 * @param <T> the generic type
 * 
 * @author shashwat.anand
 */
public class NavigationStack<T> extends Stack<T> {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The Constant FIXED_SIZE. */
	private static final int FIXED_SIZE = 10;

	/* (non-Javadoc)
	 * @see java.util.Stack#push(java.lang.Object)
	 */
	@Override
	public T push(T item) {
		if (this.size() == FIXED_SIZE) {
			this.removeElementAt(0);
		}
		return super.push(item);
	}
}
