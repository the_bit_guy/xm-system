package com.magna.xmsystem.xmadmin.ui.Application.dialogs;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.layout.GridDataFactory;
import org.eclipse.jface.layout.GridLayoutFactory;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.ITreeSelection;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Tree;
import org.eclipse.swt.widgets.TreeColumn;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.magna.xmsystem.dependencies.utils.LANG_ENUM;
import com.magna.xmsystem.dependencies.utils.XMSystemUtil;
import com.magna.xmsystem.ui.controls.util.FilteredTreeControl;
import com.magna.xmsystem.ui.controls.util.PatternFilterTree;
import com.magna.xmsystem.xmadmin.message.Message;
import com.magna.xmsystem.xmadmin.ui.dialogs.BrowseScriptDialog;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.AdminTreeviewer;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild;
import com.magna.xmsystem.xmadmin.util.CommonConstants;
import com.magna.xmsystem.xmadmin.util.XMAdminUtil;

public class BrowseApplicationDialog extends Dialog {

	/** The Constant LOGGER. */
	private static final Logger LOGGER = LoggerFactory.getLogger(BrowseScriptDialog.class);

	/** Member variable 'parent shell' for {@link Shell}. */
	private Shell parentShell;

	/** Member variable 'script list' for {@link ArrayList<String>}. */
	private ArrayList<Object> postionList;

	/** The messages. */
	transient private Message messages;

	/** Member variable 'proposals map' for {@link Map<String,String>}. */
	protected Map<String, String> Map;

	/** The grp script. */
	protected Group grpScript;

	/** The browse dialog tree viewer. */
	protected BrowseApplicationTreeViewer browseDialogTreeViewer;

	/** Member variable 'selected script' for {@link String}. */
	private Object selectedApplication;

	/** The proposals id name map. */
	protected Map<String, String> proposalsIdNameMap;

	protected List<IAdminTreeChild> dialogList;

	/** The is position. */
	protected boolean isPosition;
	
	/** The parent selection. */
	protected boolean parentSelection;
	
	protected Button okbtn;
	
	/** The Constant OK_LABEL_EN. */
	final static private String OK_LABEL_EN = "OK";
	
	/** The Constant OK_LABEL_DE. */
	final static private String OK_LABEL_DE = "OK";
	
	/** The Constant CANCEL_LABEL_EN. */
	final static private String CANCEL_LABEL_EN = "Cancel";
	
	/** The Constant CANCEL_LABEL_DE. */
	final static private String CANCEL_LABEL_DE = "Abbrechen";

	/**
	 * Instantiates a new browse application dialog.
	 *
	 * @param shell the shell
	 * @param messages the messages
	 * @param dialogList the dialog list
	 * @param isPosition the is position
	 * @param parentSelection the parent selection
	 */
	public BrowseApplicationDialog(final Shell shell, final Message messages, List<IAdminTreeChild> dialogList,
			boolean isPosition, boolean parentSelection) {
		super(shell);
		this.parentShell = shell;
		this.messages = messages;
		this.parentSelection = parentSelection;
		this.isPosition = isPosition;
		this.dialogList = dialogList;

	}

	/**
	 * Configures the Shell.
	 *
	 * @param newShell
	 *            the new shell
	 * @see org.eclipse.jface.window.Window#configureShell(org.eclipse.swt.widgets.Shell)
	 */
	@Override
	protected void configureShell(final Shell newShell) {
		try {
			super.configureShell(newShell);
			newShell.setText(messages.browseScriptDialogTitle);
			newShell.setParent(this.parentShell);
			final Rectangle shellBounds = parentShell.getBounds();
			int width = 450;
			int height = 300;
			newShell.setSize(width, height);
			final Point pos = new Point((shellBounds.width - width) / 2, (shellBounds.height - height) / 2);
			newShell.setLocation(pos.x, pos.y);
		} catch (Exception e) {
			LOGGER.error("Execution occurred while configuring shell!", e); //$NON-NLS-1$
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.jface.dialogs.Dialog#createDialogArea(org.eclipse.swt.widgets
	 * .Composite)
	 */
	@Override
	protected Control createDialogArea(Composite parent) {
		try {
			final Composite widgetContainer = new Composite(parent, SWT.NONE);
			final GridLayout widgetContainerLayout = new GridLayout(1, false);
			widgetContainer.setLayout(widgetContainerLayout);
			widgetContainer.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
			initGUI(widgetContainer);
		} catch (Exception e) {
			LOGGER.error("Unable to create UI elements", e);
		}
		return parent;

	}

	/**
	 * Make the GUI for components of the Site Composite
	 */
	private void initGUI(final Composite parent) {
		try {
			GridLayoutFactory.fillDefaults().applyTo(parent);
			parent.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));

			this.grpScript = new Group(parent, SWT.NONE);
			GridLayoutFactory.fillDefaults().numColumns(1).applyTo(this.grpScript);
			GridDataFactory.fillDefaults().grab(true, true).span(SWT.FILL, SWT.FILL).applyTo(this.grpScript);

			final Composite widgetContainer = new Composite(this.grpScript, SWT.NONE);
			final GridLayout widgetContainerLayout = new GridLayout(1, false);
			widgetContainer.setLayout(widgetContainerLayout);
			widgetContainer.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));

			createTableViewer(widgetContainer);

			init();
			addTreeColumnSelectionListeners();

		} catch (Exception ex) {
			LOGGER.error("Unable to crete UI elements", ex); //$NON-NLS-1$
		}
	}

	private void init() {
		this.browseDialogTreeViewer.setContentProvider(new BrowseApplicationContentProvider());
		this.browseDialogTreeViewer.setLabelProvider(new BrowserApplicationLabelProvider());
		postionList = new ArrayList<Object>(dialogList);
		final AdminTreeviewer adminTree = XMAdminUtil.getInstance().getAdminTree();
		final Object selectionObj = adminTree.getSelection();
		if (selectionObj instanceof IStructuredSelection) {
			//Object firstElement = ((IStructuredSelection) selectionObj).getFirstElement();
			//if (firstElement == null || (!UserApplicationChild.class.getSimpleName().equals(firstElement.getClass().getSimpleName()) && !ProjectApplicationChild.class.getSimpleName().equals(firstElement.getClass().getSimpleName()))) {
				if (isPosition) {
					for (String positionType : CommonConstants.Application.POSITIONS) {
						postionList.add(positionType);
					}
				}
		//	}
		}
		if (parentSelection) {
			postionList.remove(CommonConstants.Application.POSITIONS[1]);
			this.browseDialogTreeViewer.setInput(postionList);
		} else if (!parentSelection) {
			this.browseDialogTreeViewer.setInput(postionList);
		}
		

	}

	/**
	 * Create table viewer for icons
	 * 
	 * @param widgetContainer
	 *            {@link Composite}
	 */
	private void createTableViewer(final Composite widgetContainer) {
		FilteredTreeControl iconControl = new FilteredTreeControl(widgetContainer, SWT.NONE, new PatternFilterTree()) {
			/**
			 * Overrides doCreateTreeViewer method
			 */
			@Override
			protected TreeViewer doCreateTreeViewer(final Composite parentL, final int style) {
				BrowseApplicationDialog.this.browseDialogTreeViewer = new BrowseApplicationTreeViewer(parentL);
				return BrowseApplicationDialog.this.browseDialogTreeViewer;
			}
		};
		GridData layoutData = new GridData(SWT.FILL, SWT.FILL, true, true);
		iconControl.setLayoutData(layoutData);
	}

	/**
	 * Adds the tree column selection listeners.
	 */
	private void addTreeColumnSelectionListeners() {
		TreeColumn column;
		Tree tree = this.browseDialogTreeViewer.getTree();
		tree.setLinesVisible(true);
		tree.setHeaderVisible(true);
		int totalColumn = tree.getColumns().length;
		for (int i = 0; i < totalColumn; i++) {
			column = tree.getColumn(i);
			addColumnSelectionListener(column);
		}
	}

	/**
	 * Adds the column selection listener.
	 *
	 * @param column
	 *            the column
	 */
	private void addColumnSelectionListener(final TreeColumn column) {
		browseDialogTreeViewer.addSelectionChangedListener(new ISelectionChangedListener() {
			
			@Override
			public void selectionChanged(SelectionChangedEvent event) {
				okbtn.setEnabled(true);
			}
		});
		column.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent event) {
				treeColumnClicked((TreeColumn) event.widget);
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent arg0) {

			}
		});

	}

	/**
	 * Tree column clicked.
	 *
	 * @param column
	 *            the column
	 */
	private void treeColumnClicked(TreeColumn column) {
		Tree tree = column.getParent();
		if (column.equals(tree.getSortColumn())) {
			tree.setSortDirection(tree.getSortDirection() == SWT.UP ? SWT.DOWN : SWT.UP);
		} else {
			tree.setSortColumn(column);
			tree.setSortDirection(SWT.UP);
		}
		this.browseDialogTreeViewer.refresh();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jface.dialogs.Dialog#okPressed()
	 */
	@Override
	protected void okPressed() {

		ITreeSelection selection;
		selection = (ITreeSelection) this.browseDialogTreeViewer.getSelection();
		Object element;
		if (selection != null && (element = selection.getFirstElement()) != null) {
			Object Name = element;
			this.selectedApplication = Name;
		}

		super.okPressed();
	}

	@Override
	protected void createButtonsForButtonBar(Composite parent) {
		// TODO Auto-generated method stub
		super.createButtonsForButtonBar(parent);
		/*okbtn = getButton(IDialogConstants.OK_ID);
		okbtn.setText("OK");
		setButtonLayoutData(okbtn);
		okbtn.setEnabled(false);*/
		
		Button cancelButton = getButton(IDialogConstants.CANCEL_ID);
		okbtn = getButton(IDialogConstants.OK_ID);
		LANG_ENUM currentLang = LANG_ENUM.ENGLISH;
		final String localeString = XMSystemUtil.getLanguage();
		if(!XMSystemUtil.isEmpty(localeString)) {
			currentLang = LANG_ENUM.valueOf(localeString.toUpperCase());
		}
		if (currentLang == LANG_ENUM.GERMAN) {
			cancelButton.setText(CANCEL_LABEL_DE);
			okbtn.setText(OK_LABEL_DE);
		}else{
			cancelButton.setText(CANCEL_LABEL_EN);
			okbtn.setText(OK_LABEL_EN);
		}
		setButtonLayoutData(okbtn);
		okbtn.setEnabled(false);
	}

	
	/**
	 * Gets the selected script.
	 *
	 * @return the selected script
	 */
	public Object getSelectedApplication() {
		return selectedApplication;
	}

}
