package com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.roles;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;

import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild;

/**
 * The Class RoleUsers.
 * 
 * @author subash.janarthanan
 * 
 */
public class RoleScopeObjectUsers implements IAdminTreeChild {

	/** The role user children. */
	final private Map<String, IAdminTreeChild> roleUserChildren;

	/** The name. */
	private String name;

	/** The parent. */
	private IAdminTreeChild parent;

	/**
	 * Instantiates a new role users.
	 *
	 * @param parent
	 *            the parent
	 */
	public RoleScopeObjectUsers(IAdminTreeChild parent) {
		this.parent = parent;
		this.roleUserChildren = new LinkedHashMap<>();
	}

	/**
	 * Adds the.
	 *
	 * @param roleUserChildrenId
	 *            the role user children id
	 * @param child
	 *            the child
	 * @return the i admin tree child
	 */
	public IAdminTreeChild add(final String roleUserChildrenId, final IAdminTreeChild child) {
		child.setParent(this);
		return this.roleUserChildren.put(roleUserChildrenId, child);
	}
	/**
	 * Method for Removes the all.
	 */
	public void removeAll() {
		this.roleUserChildren.clear();
	}
	/**
	 * Removes the.
	 *
	 * @param roleUserChildrenId
	 *            the role user children id
	 * @return the i admin tree child
	 */
	public IAdminTreeChild remove(final String roleUserChildrenId) {
		return this.roleUserChildren.remove(roleUserChildrenId);
	}

	/**
	 * Gets the role user children.
	 *
	 * @return the role user children
	 */
	public Map<String, IAdminTreeChild> getRoleUserChildren() {
		return roleUserChildren;
	}

	/**
	 * Gets the role user collection.
	 *
	 * @return the role user collection
	 */
	public Collection<IAdminTreeChild> getRoleUserCollection() {
		return this.roleUserChildren.values();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild#
	 * getParent()
	 */
	@Override
	public IAdminTreeChild getParent() {
		return this.parent;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild#
	 * setParent(com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.
	 * IAdminTreeChild)
	 */
	@Override
	public void setParent(IAdminTreeChild parent) {
		this.parent = parent;
	}

	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Sets the name.
	 *
	 * @param name
	 *            the new name
	 */
	public void setName(String name) {
		this.name = name;
	}

}
