package com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.adminarea;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.stream.Collectors;

import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.projectapplicationmodel.ProjectApplication;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.refs.RelationObj;

public class AdminAreaProjectApplications implements IAdminTreeChild {

	/** Member variable 'parent' for {@link IAdminTreeChild}. */
	private IAdminTreeChild parent;

	/** Member variable 'admin area project app children' for {@link Map<String,IAdminTreeChild>}. */
	private Map<String, IAdminTreeChild> adminAreaProjectAppChildren;

	/**
	 * Constructor for AdminAreaProjectApplications Class.
	 *
	 * @param parent {@link IAdminTreeChild}
	 */
	public AdminAreaProjectApplications(final IAdminTreeChild parent) {
		this.parent = parent;
		this.adminAreaProjectAppChildren = new LinkedHashMap<>();
		this.addFixedChildren();
	}
	
	/**
	 * Method for Adds the fixed children.
	 */
	public void addFixedChildren() {
		adminAreaProjectAppChildren.put(AdminAreaProjectAppNotFixed.class.getName(), new AdminAreaProjectAppNotFixed(
				this));
		adminAreaProjectAppChildren.put(AdminAreaProjectAppFixed.class.getName(), new AdminAreaProjectAppFixed(
				this));
		adminAreaProjectAppChildren.put(AdminAreaProjectAppProtected.class.getName(), new AdminAreaProjectAppProtected(
				this));
	}

	/**
	 * Method for Adds the.
	 *
	 * @param adminAreasChildProAppChildId {@link String}
	 * @param child {@link IAdminTreeChild}
	 * @return the i admin tree child {@link IAdminTreeChild}
	 */
	public IAdminTreeChild add(final String adminAreasChildProAppChildId, final IAdminTreeChild child) {
		child.setParent(this);
		IAdminTreeChild returnVal = this.adminAreaProjectAppChildren.put(adminAreasChildProAppChildId, child);
		if (child instanceof RelationObj && ((RelationObj) child).getRefObject() instanceof ProjectApplication) {
			sort();
		}
		return returnVal;
	}

	/**
	 * Method for Removes the.
	 *
	 * @param adminAreasChildProAppChildId {@link String}
	 * @return the i admin tree child {@link IAdminTreeChild}
	 */
	public IAdminTreeChild remove(final String adminAreasChildProAppChildId) {
		return this.adminAreaProjectAppChildren.remove(adminAreasChildProAppChildId);
	}

	/**
	 * Gets the admin area project app child collection.
	 *
	 * @return the admin area project app child collection
	 */
	public Collection<IAdminTreeChild> getAdminAreaProjectAppChildCollection() {
		return this.adminAreaProjectAppChildren.values();
	}

	/**
	 * Gets the admin area project app child child.
	 *
	 * @return the admin area project app child child
	 */
	public Map<String, IAdminTreeChild> getAdminAreaProjectAppChild() {
		return adminAreaProjectAppChildren;
	}
	
	/* (non-Javadoc)
	 * @see com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild#getParent()
	 */
	@Override
	public IAdminTreeChild getParent() {
		return this.parent;
	}

	/* (non-Javadoc)
	 * @see com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild#setParent(com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild)
	 */
	@Override
	public void setParent(final IAdminTreeChild parent) {
		this.parent = parent;
	}
	
	/**
	 * Sort.
	 */
	public void sort() {
		final LinkedHashMap<String, IAdminTreeChild> collect = this.adminAreaProjectAppChildren.entrySet().stream().sorted(
				(e1, e2) -> ((ProjectApplication) (((RelationObj) e1.getValue()).getRefObject())).getName()
				.compareTo(((ProjectApplication) (((RelationObj) e2.getValue()).getRefObject())).getName()))
				.collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e1, LinkedHashMap::new));
		this.adminAreaProjectAppChildren = collect;
	}

}
