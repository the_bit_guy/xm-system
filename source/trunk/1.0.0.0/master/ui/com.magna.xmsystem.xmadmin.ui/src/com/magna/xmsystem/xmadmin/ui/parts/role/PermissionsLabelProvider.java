package com.magna.xmsystem.xmadmin.ui.parts.role;

import org.eclipse.jface.viewers.ILabelProviderListener;
import org.eclipse.jface.viewers.ITableColorProvider;
import org.eclipse.jface.viewers.ITableLabelProvider;
import org.eclipse.jface.viewers.StyledString;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Image;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.magna.xmsystem.dependencies.utils.XMSystemUtil;
import com.magna.xmsystem.ui.controls.util.IStyledTableLabelProvider;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.roles.ObjectRelationType;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.roles.ObjectType;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.roles.objRelationPermissions.ObjectRelationPermissions;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.roles.objectpermissions.ObjectPermissions;
import com.magna.xmsystem.xmadmin.util.CommonConstants;

/**
 * Class for Permissions label provider.
 *
 * @author shashwat.anand
 */
public class PermissionsLabelProvider implements ITableLabelProvider, IStyledTableLabelProvider, ITableColorProvider {
	
	/** The Constant LOGGER. */
	private static final Logger LOGGER = LoggerFactory.getLogger(PermissionsLabelProvider.class);

	/** Member variable 'yes icon path' for {@link String}. */
	private static final String yesIconPath = "icons/16x16/checkbox_yes.png";
	
	/** Member variable 'no icon path' for {@link String}. */
	private static final String noIconPath = "icons/16x16/checkbox_no.png";
	
	/** Member variable 'na icon path' for {@link String}. */
	private static final String naIconPath = "icons/16x16/NA.png";
	
	/** Member variable 'yes image' for {@link Image}. */
	private Image yesImage;

	/** Member variable 'yes disabled image' for {@link Image}. */
	private Image yesDisabledImage;
	
	/** Member variable 'no image' for {@link Image}. */
	private Image noImage;

	/** Member variable 'no disabled image' for {@link Image}. */
	private Image noDisabledImage;
	
	/** Member variable 'na image' for {@link Image}. */
	private Image naImage;

	/** Member variable 'table viewer' for {@link TableViewer}. */
	private TableViewer tableViewer;

	/**
	 * Constructor for PermissionsLabelProvider Class.
	 * @param  
	 */
	public PermissionsLabelProvider(TableViewer tableViewer) {
		this.tableViewer = tableViewer;
		
		final XMSystemUtil systemUtilInstance = XMSystemUtil.getInstance();
		
		this.yesImage = systemUtilInstance.getImage(this.getClass(), yesIconPath);
		this.noImage = systemUtilInstance.getImage(this.getClass(), noIconPath);
		this.naImage = systemUtilInstance.getImage(this.getClass(), naIconPath);
		
		this.yesDisabledImage = systemUtilInstance.getDisabledImage(this.getClass(), yesIconPath);
		this.noDisabledImage = systemUtilInstance.getDisabledImage(this.getClass(), noIconPath);
	}
	
	/* (non-Javadoc)
	 * @see org.eclipse.jface.viewers.IBaseLabelProvider#addListener(org.eclipse.jface.viewers.ILabelProviderListener)
	 */
	@Override
	public void addListener(ILabelProviderListener listener) {
		// Nothing todo
	}

	/* (non-Javadoc)
	 * @see org.eclipse.jface.viewers.IBaseLabelProvider#dispose()
	 */
	@Override
	public void dispose() {
		// Nothing todo
	}

	/* (non-Javadoc)
	 * @see org.eclipse.jface.viewers.IBaseLabelProvider#isLabelProperty(java.lang.Object, java.lang.String)
	 */
	@Override
	public boolean isLabelProperty(Object element, String property) {
		return false;
	}

	/* (non-Javadoc)
	 * @see org.eclipse.jface.viewers.IBaseLabelProvider#removeListener(org.eclipse.jface.viewers.ILabelProviderListener)
	 */
	@Override
	public void removeListener(ILabelProviderListener listener) {
		// Nothing todo
	}

	/* (non-Javadoc)
	 * @see org.eclipse.jface.viewers.ITableColorProvider#getForeground(java.lang.Object, int)
	 */
	@Override
	public Color getForeground(Object element, int columnIndex) {
		return null;
	}

	/* (non-Javadoc)
	 * @see org.eclipse.jface.viewers.ITableColorProvider#getBackground(java.lang.Object, int)
	 */
	@Override
	public Color getBackground(Object element, int columnIndex) {
		return null;
	}

	/* (non-Javadoc)
	 * @see com.magna.xmsystem.ui.controls.util.IStyledTableLabelProvider#getStyledText(java.lang.Object, int)
	 */
	@Override
	public StyledString getStyledText(Object element, int columnIndex) {
		try {
			if (element instanceof ObjectRelationPermissions) {
				ObjectRelationPermissions item = (ObjectRelationPermissions) element;
				String permissionName = item.getPermissionName();
				/*ObjectRelationType objectRelationType = ObjectRelationType.getObjectType(permissionName);*/
				switch (columnIndex) {
					case 0:
						return new StyledString(permissionName);
					/*case 1:
						return new StyledString(item.isAssign() ? "Y" : "N");
					case 2:
						return new StyledString(item.isRemove() ? "Y" : "N");
					case 3:
						if (objectRelationType == ObjectRelationType.DIRECTORY_RELATION
								|| objectRelationType == ObjectRelationType.GROUP_RELATION
								|| objectRelationType == ObjectRelationType.ADMIN_MENU_CONFIG
								|| objectRelationType == ObjectRelationType.ADMINISTRATION_AREA_TO_ROLE
								|| objectRelationType == ObjectRelationType.USER_APPLICATION_TO_USER
								|| objectRelationType == ObjectRelationType.USER_TO_ROLE
								|| objectRelationType == ObjectRelationType.PROJECT_APPLICATION_TO_USER_PROJECT) {
							return new StyledString("NA");
						}
						return new StyledString(item.isActivate() ? "Y" : "N");
					case 4:
						if (objectRelationType == ObjectRelationType.DIRECTORY_RELATION || objectRelationType == ObjectRelationType.ADMINISTRATION_AREA_TO_ROLE
								|| objectRelationType == ObjectRelationType.GROUP_RELATION) {
							return new StyledString("NA");
						}
						return new StyledString(item.isInactiveAssignment() ? "Y" : "N");*/
				}
			} else if (element instanceof ObjectPermissions) {
				ObjectPermissions item = (ObjectPermissions) element;
				String permissionName = item.getPermissionName();
				/*ObjectType objectType = ObjectType.getObjectType(permissionName);*/
				switch (columnIndex) {
					case 0:
						return new StyledString(permissionName);
					/*case 1:
						if (objectType == ObjectType.NOTIFICATION || objectType == ObjectType.PROPERTY_CONFIG) {
							return new StyledString("NA");
						}
						return new StyledString(item.isCreate() ? "Y" : "N");
					case 2:
						if (objectType == ObjectType.ICON) {
							return new StyledString("NA");
						}
						return new StyledString(item.isChange() ? "Y" : "N");
					case 3:
						if (objectType == ObjectType.NOTIFICATION || objectType == ObjectType.PROPERTY_CONFIG) {
							return new StyledString("NA");
						}
						return new StyledString(item.isDelete() ? "Y" : "N");
					case 4:
						if (objectType == ObjectType.ICON || objectType == ObjectType.GROUP
								|| objectType == ObjectType.DIRECTORY || objectType == ObjectType.ROLE
								|| objectType == ObjectType.NOTIFICATION || objectType == ObjectType.LIVE_MESSAGE || objectType == ObjectType.PROPERTY_CONFIG) {
							return new StyledString("NA");
						}
						return new StyledString(item.isActivate() ? "Y" : "N");*/
				}
			}
		} catch (Exception e) {
			LOGGER.error("Execption occured at setting styled text", e); //$NON-NLS-1$
		}
		return null;
	}

	/* (non-Javadoc)
	 * @see com.magna.xmsystem.ui.controls.util.IStyledTableLabelProvider#getImage(java.lang.Object, int)
	 */
	@Override
	public Image getImage(Object element, int idx) {
		return null;
	}

	/* (non-Javadoc)
	 * @see org.eclipse.jface.viewers.ITableLabelProvider#getColumnImage(java.lang.Object, int)
	 */
	@Override
	public Image getColumnImage(Object element, int columnIndex) {		
		try {
			boolean enableEditorSupport = false;
			if(tableViewer instanceof ObjectPermissionsTable) {
				enableEditorSupport = ((ObjectPermissionsTable)tableViewer).isEnableEditorSupport();
			} else if (tableViewer instanceof ObjRelationPermissionsTable){
				enableEditorSupport = ((ObjRelationPermissionsTable)tableViewer).isEnableEditorSupport();
			}
			
			if (element instanceof ObjectRelationPermissions) {
				ObjectRelationPermissions item = (ObjectRelationPermissions) element;
				String permissionName = item.getPermissionName();
				ObjectRelationType objectRelationType = ObjectRelationType.getObjectType(permissionName);
				switch (columnIndex) {
				case 0:
					return null;
				case 1:
					return item.isAssign() ? enableEditorSupport ? yesImage : yesDisabledImage : enableEditorSupport ? noImage : noDisabledImage;
				case 2:
					return item.isRemove() ? enableEditorSupport ? yesImage : yesDisabledImage : enableEditorSupport ? noImage : noDisabledImage;
				case 3:
					if (objectRelationType == ObjectRelationType.DIRECTORY_RELATION
							|| objectRelationType == ObjectRelationType.GROUP_RELATION
							|| objectRelationType == ObjectRelationType.ADMIN_MENU_CONFIG
							|| objectRelationType == ObjectRelationType.ADMINISTRATION_AREA_TO_ROLE
							|| objectRelationType == ObjectRelationType.USER_APPLICATION_TO_USER
							|| objectRelationType == ObjectRelationType.USER_TO_ROLE
							|| objectRelationType == ObjectRelationType.PROJECT_APPLICATION_TO_USER_PROJECT) {
						return naImage;
					}
					return item.isActivate() ? enableEditorSupport ? yesImage : yesDisabledImage : enableEditorSupport ? noImage : noDisabledImage;
				case 4:
					if (objectRelationType == ObjectRelationType.DIRECTORY_RELATION
							|| objectRelationType == ObjectRelationType.ADMINISTRATION_AREA_TO_ROLE
							|| objectRelationType == ObjectRelationType.GROUP_RELATION) {
						return naImage;
					}
					return item.isInactiveAssignment() ? enableEditorSupport ? yesImage : yesDisabledImage : enableEditorSupport ? noImage : noDisabledImage;
				}
			} else if (element instanceof ObjectPermissions) {
				ObjectPermissions item = (ObjectPermissions) element;
				String permissionName = item.getPermissionName();
				ObjectType objectType = ObjectType.getObjectType(permissionName);
				switch (columnIndex) {
				case 0:
					return null;
				case 1:
					if (objectType == ObjectType.PROPERTY_CONFIG) {
						return naImage;
					}
					return item.isCreate() ? enableEditorSupport ? yesImage : yesDisabledImage : enableEditorSupport ? noImage : noDisabledImage;
				case 2:
					if (objectType == ObjectType.ICON) {
						return naImage;
					}
					return item.isChange() ? enableEditorSupport ? yesImage : yesDisabledImage : enableEditorSupport ? noImage : noDisabledImage;
				case 3:
					if (objectType == ObjectType.PROPERTY_CONFIG) {
						return naImage;
					}
					return item.isDelete() ? enableEditorSupport ? yesImage : yesDisabledImage : enableEditorSupport ? noImage : noDisabledImage;
				case 4:
					if (objectType == ObjectType.ICON || objectType == ObjectType.GROUP
							|| objectType == ObjectType.DIRECTORY || objectType == ObjectType.ROLE
							|| objectType == ObjectType.LIVE_MESSAGE || objectType == ObjectType.PROPERTY_CONFIG) {
						return naImage;
					}
					return item.isActivate() ? enableEditorSupport ? yesImage : yesDisabledImage : enableEditorSupport ? noImage : noDisabledImage;
				}
			}
		} catch (Exception e) {
			LOGGER.error("Execption occured at setting the image", e); //$NON-NLS-1$
		}
		return null;
	}

	/* (non-Javadoc)
	 * @see org.eclipse.jface.viewers.ITableLabelProvider#getColumnText(java.lang.Object, int)
	 */
	@Override
	public String getColumnText(Object element, int columnIndex) {
		return getStyledText(element, columnIndex) == null ? CommonConstants.EMPTY_STR : getStyledText(element, columnIndex).getString();
	}
}
