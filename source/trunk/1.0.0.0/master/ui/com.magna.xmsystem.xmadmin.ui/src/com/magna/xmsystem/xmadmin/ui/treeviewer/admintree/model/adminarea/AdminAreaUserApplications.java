package com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.adminarea;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.stream.Collectors;

import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.userapplicationmodel.UserApplication;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.refs.RelationObj;

/**
 * The Class AdminAdminAreaUserApp.
 */
public class AdminAreaUserApplications implements IAdminTreeChild {

	/** The parent. */
	private IAdminTreeChild parent;
	
	/** The Admin area user app child. */
	private Map<String, IAdminTreeChild> AdminAreaUserAppChild;

	/**
	 * Instantiates a new admin admin area user app.
	 *
	 * @param parent the parent
	 */
	public AdminAreaUserApplications(IAdminTreeChild parent) {
		this.parent = parent;
		this.AdminAreaUserAppChild = new LinkedHashMap<>();
		
		addFixedChildren();
	}

	/**
	 * Method for Adds the fixed children.
	 */
	public void addFixedChildren() {
		AdminAreaUserAppChild.put(AdminAreaUserAppNotFixed.class.getName(), new AdminAreaUserAppNotFixed(
				this));
		AdminAreaUserAppChild.put(AdminAreaUserAppFixed.class.getName(), new AdminAreaUserAppFixed(
				this));
		AdminAreaUserAppChild.put(AdminAreaUserAppProtected.class.getName(), new AdminAreaUserAppProtected(
				this));
	}
	
	/**
	 * Adds the.
	 *
	 * @param adminAreaUserAppChildId the admin area user app child id
	 * @param child the child
	 * @return the i admin tree child
	 */
	public IAdminTreeChild add(final String adminAreaUserAppChildId, final IAdminTreeChild child) {
		child.setParent(this);
		IAdminTreeChild returnVal = this.AdminAreaUserAppChild.put(adminAreaUserAppChildId, child);
		if (child instanceof RelationObj && ((RelationObj) child).getRefObject() instanceof UserApplication) {
			sort();
		}
		return returnVal;
	}

	/**
	 * Removes the.
	 *
	 * @param adminAreaUserAppChildId the admin area user app child id
	 * @return the i admin tree child
	 */
	public IAdminTreeChild remove(final String adminAreaUserAppChildId) {
		return this.AdminAreaUserAppChild.remove(adminAreaUserAppChildId);
	}

	/**
	 * Gets the admin area user app collection.
	 *
	 * @return the admin area user app collection
	 */
	public Collection<IAdminTreeChild> getAdminAreaUserAppCollection() {
		return this.AdminAreaUserAppChild.values();
	}

	/**
	 * Gets the admin area user app child.
	 *
	 * @return the admin area user app child
	 */
	public Map<String, IAdminTreeChild> getAdminAreaUserAppChild() {
		return AdminAreaUserAppChild;
	}
	
	/* (non-Javadoc)
	 * @see com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild#getParent()
	 */
	@Override
	public IAdminTreeChild getParent() {
		return this.parent;
	}

	/* (non-Javadoc)
	 * @see com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild#setParent(com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild)
	 */
	@Override
	public void setParent(final IAdminTreeChild parent) {
		this.parent = parent;
	}
	
	/**
	 * Sort.
	 */
	public void sort() {
		final LinkedHashMap<String, IAdminTreeChild> collect = this.AdminAreaUserAppChild.entrySet().stream().sorted(
				(e1, e2) -> ((UserApplication) (((RelationObj) e1.getValue()).getRefObject())).getName()
				.compareTo(((UserApplication) (((RelationObj) e2.getValue()).getRefObject())).getName()))
				.collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e1, LinkedHashMap::new));
		this.AdminAreaUserAppChild = collect;
	}


}
