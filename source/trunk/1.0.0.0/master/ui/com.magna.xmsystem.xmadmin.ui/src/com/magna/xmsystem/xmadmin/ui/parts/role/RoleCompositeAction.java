package com.magna.xmsystem.xmadmin.ui.parts.role;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import javax.inject.Inject;

import org.eclipse.core.databinding.Binding;
import org.eclipse.core.databinding.DataBindingContext;
import org.eclipse.core.databinding.UpdateValueStrategy;
import org.eclipse.core.databinding.beans.BeanProperties;
import org.eclipse.core.databinding.observable.value.IObservableValue;
import org.eclipse.core.databinding.observable.value.IValueChangeListener;
import org.eclipse.core.databinding.observable.value.ValueChangeEvent;
import org.eclipse.e4.core.services.nls.Translation;
import org.eclipse.e4.ui.model.application.ui.MDirtyable;
import org.eclipse.jface.databinding.fieldassist.ControlDecorationSupport;
import org.eclipse.jface.databinding.swt.WidgetProperties;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.viewers.TreePath;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.client.ResourceAccessException;

import com.magna.xmbackend.entities.RolesTbl;
import com.magna.xmbackend.vo.enums.CreationType;
import com.magna.xmbackend.vo.roles.ObjectPermission;
import com.magna.xmbackend.vo.roles.RoleRequest;
import com.magna.xmsystem.dependencies.customExceptions.UnauthorizedAccessException;
import com.magna.xmsystem.dependencies.utils.XMSystemUtil;
import com.magna.xmsystem.ui.controls.widgets.CustomMessageDialog;
import com.magna.xmsystem.xmadmin.message.Message;
import com.magna.xmsystem.xmadmin.message.MessageRegistry;
import com.magna.xmsystem.xmadmin.restclient.role.RoleController;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.AdminTreeviewer;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.AdminTreeFactory;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.roles.GlobalPermissionObjectType;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.roles.ObjectRelationType;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.roles.ObjectType;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.roles.Role;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.roles.Roles;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.roles.VoPermContainer;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.roles.objRelationPermissions.ObjectRelationPermissions;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.roles.objectpermissions.ObjectPermissions;
import com.magna.xmsystem.xmadmin.ui.validation.NameValidation;
import com.magna.xmsystem.xmadmin.ui.validation.StatusValidation;
import com.magna.xmsystem.xmadmin.util.CommonConstants;
import com.magna.xmsystem.xmadmin.util.MessageType;
import com.magna.xmsystem.xmadmin.util.ObjectGlobalPerm;
import com.magna.xmsystem.xmadmin.util.XMAdminUtil;

/**
 * The Class RoleCompositeAction.
 * 
 * @author archita.patel
 */
public class RoleCompositeAction extends RoleCompositeUI {

	/** The Constant LOGGER. */
	private static final Logger LOGGER = LoggerFactory.getLogger(RoleCompositeAction.class);

	/** The role model. */
	private Role roleModel;

	/** The old model. */
	private Role oldModel;

	/** The registry. */
	/** Member variable for {@link MessageRegistry}. */
	@Inject
	private MessageRegistry registry;

	/** The messages. */
	@Inject
	@Translation
	transient private Message messages;

	/** Member variable for dirty. */
	@Inject
	transient private MDirtyable dirty;

	/** The data bind context. */
	final DataBindingContext dataBindContext = new DataBindingContext();

	/** Member variable for widgetValue. */
	private IObservableValue<?> widgetValue;

	/** Member variable for modelValue. */
	private IObservableValue<?> modelValue;

	/**
	 * Instantiates a new role composite action.
	 *
	 * @param parent
	 *            the parent
	 */
	@Inject
	public RoleCompositeAction(final Composite parent) {
		super(parent, SWT.NONE);
		initListeners();
	}

	/**
	 * Init listeners.
	 */
	private void initListeners() {
		if (this.saveBtn != null) {
			this.saveBtn.addSelectionListener(new SelectionAdapter() {

				/**
				 * Save button handler
				 */
				@Override
				public void widgetSelected(final SelectionEvent event) {
					saveRoleHandler();
				}
			});
		}

		if (this.cancelBtn != null) {
			this.cancelBtn.addSelectionListener(new SelectionAdapter() {

				/**
				 * Cancel button handler
				 */
				@Override
				public void widgetSelected(final SelectionEvent event) {
					cancelRoleHandler();
				}
			});
		}
	}

	/**
	 * Update button status.
	 *
	 * @param event
	 *            the event
	 */
	@SuppressWarnings("rawtypes")
	private void updateButtonStatus(final ValueChangeEvent event) {
		final String name = this.txtName.getText();
		if (this.saveBtn != null) {
			if ((XMSystemUtil.isEmpty(name) || name.trim().length() == 0)
					|| (!name.matches(CommonConstants.RegularExpressions.ALLOWED_SITE_NAME_REGEXP))) {
				this.saveBtn.setEnabled(false);
			} else {
				this.saveBtn.setEnabled(true);
			}
		}
	}

	protected boolean validate() {
		final String roleName = this.roleModel.getRoleName();

		if (XMSystemUtil.isEmpty(roleName)) {
			CustomMessageDialog.openError(this.getShell(), messages.nameErrorTitle, messages.nameError);
			return false;
		}

		final Roles roles = AdminTreeFactory.getInstance().getRoles();
		final Collection<IAdminTreeChild> rolesCollection = roles.getRolesCollection();
		if (roleModel.getOperationMode() == CommonConstants.OPERATIONMODE.CHANGE) {
			if (!roleName.equalsIgnoreCase(this.oldModel.getRoleName())) {
				final Map<String, Long> result = rolesCollection.parallelStream()
						.collect(Collectors.groupingBy(role -> ((Role) role).getRoleName().toUpperCase(), Collectors.counting()));
				if (result.containsKey(roleName.toUpperCase())) {
					CustomMessageDialog.openError(this.getShell(), messages.existingRoleNameTitle,
							messages.existingRoleNameError);
					return false;
				}
			}
		} else if (this.roleModel.getOperationMode() == CommonConstants.OPERATIONMODE.CREATE) {
			for (final IAdminTreeChild role : rolesCollection) {
				if (roleName.equalsIgnoreCase(((Role) role).getRoleName())) {
					CustomMessageDialog.openError(this.getShell(), messages.existingRoleNameTitle,
							messages.existingRoleNameError);
					return false;
				}
			}
		}

		return true;
	}

	/**
	 * Register messages.
	 *
	 * @param registry
	 *            the registry
	 */
	public void registerMessages(final MessageRegistry registry) {
		registry.register((text) -> {
			if (grpRoles != null && !grpRoles.isDisposed()) {
				grpRoles.setText(text);
			}
		}, (message) -> {
			if (roleModel != null) {
				if (roleModel.getOperationMode() == CommonConstants.OPERATIONMODE.CREATE) {
					return message.roleGroupCreateLabel;
				} else if (roleModel.getOperationMode() == CommonConstants.OPERATIONMODE.CHANGE) {
					return message.objectGroupChangeLabel + " \'" + this.roleModel.getRoleName() + "\'";
				} else if (roleModel.getOperationMode() == CommonConstants.OPERATIONMODE.VIEW) {
					return message.objectGroupDisaplyLabel + " \'" + this.roleModel.getRoleName() + "\'";
				}
			}
			return CommonConstants.EMPTY_STR;
		});

		registry.register((text) -> {
			if (lblName != null && !lblName.isDisposed()) {
				lblName.setText(text);
			}
		}, (message) -> {
			if (lblName != null && !lblName.isDisposed()) {
				return getUpdatedWidgetText(message.roleNameLable, lblName);
			}
			return CommonConstants.EMPTY_STR;
		});

		registry.register((text) -> {
			if (lblDescription != null && !lblDescription.isDisposed()) {
				lblDescription.setText(text);
			}
		}, (message) -> {
			if (lblDescription != null && !lblDescription.isDisposed()) {
				return getUpdatedWidgetText(message.descriptionLable, lblDescription);
			}
			return CommonConstants.EMPTY_STR;
		});
		
		registry.register((text) -> {
			if (lblViewInactive != null && !lblViewInactive.isDisposed()) {
				lblViewInactive.setText(text);
			}
		}, (message) -> {
			if (lblViewInactive != null && !lblViewInactive.isDisposed()) {
				return getUpdatedWidgetText(message.roleViewInactiveLable, lblViewInactive);
			}
			return CommonConstants.EMPTY_STR;
		});
		
		registry.register((text) -> {
			if (lblXmAdmin != null && !lblViewInactive.isDisposed()) {
				lblXmAdmin.setText(text);
			}
		}, (message) -> {
			if (lblXmAdmin != null && !lblXmAdmin.isDisposed()) {
				return getUpdatedWidgetText(message.roleXmAdminLable, lblXmAdmin);
			}
			return CommonConstants.EMPTY_STR;
		});
		
		registry.register((text) -> {
			if (lblXmHotline != null && !lblXmHotline.isDisposed()) {
				lblXmHotline.setText(text);
			}
		}, (message) -> {
			if (lblXmHotline != null && !lblXmHotline.isDisposed()) {
				return getUpdatedWidgetText(message.roleXmHotlineLable, lblXmHotline);
			}
			return CommonConstants.EMPTY_STR;
		});
		
		registry.register((text) -> {
			if (pGroupObjectPermission != null && !pGroupObjectPermission.isDisposed()) {
				pGroupObjectPermission.setText(text);
			}
		}, (message) -> {
			if (pGroupObjectPermission != null && !pGroupObjectPermission.isDisposed()) {
				return getUpdatedWidgetText(message.roleObjPermissionLable, pGroupObjectPermission);
			}
			return CommonConstants.EMPTY_STR;
		});

		registry.register((text) -> {
			if (pGroupObjRelationPermission != null && !pGroupObjRelationPermission.isDisposed()) {
				pGroupObjRelationPermission.setText(text);
			}
		}, (message) -> {
			if (pGroupObjRelationPermission != null && !pGroupObjRelationPermission.isDisposed()) {
				return getUpdatedWidgetText(message.roleObjRelationPermissionLable, pGroupObjRelationPermission);
			}
			return CommonConstants.EMPTY_STR;
		});

		registry.register((text) -> {
			if (permissionCheckboxEditMsgLbl != null && !permissionCheckboxEditMsgLbl.isDisposed()) {
				permissionCheckboxEditMsgLbl.setText(text);
			}
		}, (message) -> {
			if (permissionCheckboxEditMsgLbl != null && !permissionCheckboxEditMsgLbl.isDisposed()) {
				return getUpdatedWidgetText(message.permissionCheckboxEditMsgLbl, permissionCheckboxEditMsgLbl);
			}
			return CommonConstants.EMPTY_STR;
		});
		
		if (saveBtn != null) {
			registry.register((text) -> {
				if (saveBtn != null && !saveBtn.isDisposed()) {
					saveBtn.setText(text);
				}
			}, (message) -> {
				if (saveBtn != null && !saveBtn.isDisposed()) {
					return getUpdatedWidgetText(message.saveButtonText, saveBtn);
				}
				return CommonConstants.EMPTY_STR;
			});
		}
		if (cancelBtn != null) {
			registry.register((text) -> {
				if (cancelBtn != null && !cancelBtn.isDisposed()) {
					cancelBtn.setText(text);
				}
			}, (message) -> {
				if (cancelBtn != null && !cancelBtn.isDisposed()) {
					return getUpdatedWidgetText(message.cancelButtonText, cancelBtn);
				}
				return CommonConstants.EMPTY_STR;
			});
		}

		updatePermissionsColumnLabel(messages);
	}

	/**
	 * Update permissions column label.
	 *
	 * @param messages
	 *            the messages
	 */
	private void updatePermissionsColumnLabel(Message messages) {

		this.objectPermissionViewer.getTable().getColumn(0).setText(messages.permissionObjTypeLable);
		this.objectPermissionViewer.getTable().getColumn(1).setText(messages.permissionCreateColLable);
		this.objectPermissionViewer.getTable().getColumn(2).setText(messages.permissionChangeColLable);
		this.objectPermissionViewer.getTable().getColumn(3).setText(messages.permissionDeleteColLable);
		this.objectPermissionViewer.getTable().getColumn(4).setText(messages.permissionActivateColLable);

		this.objRelationPermissionViewer.getTable().getColumn(0).setText(messages.permissionObjTypeLable);
		this.objRelationPermissionViewer.getTable().getColumn(1).setText(messages.permissionAssignColLable);
		this.objRelationPermissionViewer.getTable().getColumn(2).setText(messages.permissionRemoveColLable);
		this.objRelationPermissionViewer.getTable().getColumn(3).setText(messages.permissionActivateColLable);
		this.objRelationPermissionViewer.getTable().getColumn(4).setText(messages.permissionInactiveAssiColLable);
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public void bindValues() {

		widgetValue = WidgetProperties.text(SWT.Modify).observe(this.txtName);
		modelValue = BeanProperties.value(Role.class, Role.PROPERTY_ROLE_NAME).observe(this.roleModel);
		widgetValue.addValueChangeListener(new IValueChangeListener() {
			/**
			 * handler to update button status
			 */
			@Override
			public void handleValueChange(final ValueChangeEvent event) {
				updateButtonStatus(event);
			}
		});

		// define the UpdateValueStrategy
		final UpdateValueStrategy update = new UpdateValueStrategy();
		update.setAfterGetValidator(new NameValidation(messages, StatusValidation.SITE));
		Binding bindValue = dataBindContext.bindValue(widgetValue, modelValue, update, null);
		ControlDecorationSupport.create(bindValue, SWT.TOP | SWT.LEFT);

		widgetValue = WidgetProperties.text(SWT.Modify).observe(this.txtDesc);
		modelValue = BeanProperties.value(Role.class, Role.PROPERTY_ROLE_DESCRIPTION).observe(this.roleModel);
		dataBindContext.bindValue(widgetValue, modelValue);
		
		// view inactive check box binding
		widgetValue = WidgetProperties.selection().observe(this.viewInactiveBtn);
		modelValue = BeanProperties.value(Role.class, Role.PROPERTY_ROLE_VIEW_INACTIVE).observe(this.roleModel);
		bindValue = dataBindContext.bindValue(widgetValue, modelValue);
		
		// xm admin check box binding
		widgetValue = WidgetProperties.selection().observe(this.xmAdminBtn);
		modelValue = BeanProperties.value(Role.class, Role.PROPERTY_ROLE_XM_ADMIN).observe(this.roleModel);
		bindValue = dataBindContext.bindValue(widgetValue, modelValue);

		// xm hotline check box binding
		widgetValue = WidgetProperties.selection().observe(this.xmHotlineBtn);
		modelValue = BeanProperties.value(Role.class, Role.PROPERTY_ROLE_XM_HOTLINE).observe(this.roleModel);
		bindValue = dataBindContext.bindValue(widgetValue, modelValue);
		
		
	}

	/**
	 * Gets the updated widget text.
	 *
	 * @param message
	 *            the message
	 * @param control
	 *            the control
	 * @return the updated widget text
	 */
	private String getUpdatedWidgetText(final String message, final Control control) {
		control.requestLayout();
		control.getParent().redraw();
		control.getParent().getParent().update();
		control.getParent().getParent().getParent().update();
		return message;
	}

	/**
	 * Save role handler.
	 */
	public void saveRoleHandler() {
		if (validate()) {
			if (!roleModel.getRoleName().isEmpty()) {
				if (roleModel.getOperationMode() == CommonConstants.OPERATIONMODE.CREATE) {
					createRoleOperation();
				} else if (roleModel.getOperationMode() == CommonConstants.OPERATIONMODE.CHANGE) {
					changeRoleOperation();
				}
			}
		}
	}

	/**
	 * Method for Change role operation.
	 */
	private void changeRoleOperation() {
		try {
			RoleController roleController = new RoleController();
			boolean isUpdated = roleController.updateRole(mapVOObjectWithModel());
			if (isUpdated) {
				setOldModel(roleModel.deepCopyRole(true, getOldModel()));
				this.roleModel.setOperationMode(CommonConstants.OPERATIONMODE.VIEW);
				setOperationMode();
				this.dirty.setDirty(false);
				final Roles roles = AdminTreeFactory.getInstance().getRoles();
				roles.sort();
				XMAdminUtil.getInstance().getAdminTree().refresh(true);
				XMAdminUtil.getInstance().getAdminTree().setSelection(new StructuredSelection(getOldModel()), true);
				XMAdminUtil.getInstance().updateLogFile(messages.roleObject + " " + "'" + this.roleModel.getRoleName()
						+ "'" + " " + messages.objectUpdate, MessageType.SUCCESS);
			}
		} catch (UnauthorizedAccessException e) {
			CustomMessageDialog.openError(this.getShell(), messages.objectPermissionDialogTitle,
					messages.objectPermissionDialogMsg);
		} catch (Exception e) {
			if (e instanceof ResourceAccessException) {
				CustomMessageDialog.openError(Display.getCurrent().getActiveShell(), messages.errorDialogTitile,
						messages.serverNotReachable);
			}
			LOGGER.warn("Unable to Update Role data ! " + e);
		}
	}

	/**
	 * Create role operation.
	 */
	private void createRoleOperation() {
		try {
			RoleController roleController = new RoleController();
			Iterable<RolesTbl> roles = roleController.createRole(mapVOObjectWithModel());
			RolesTbl rolesVo = roles.iterator().next();
			String roleId = rolesVo.getRoleId();
			if (!XMSystemUtil.isEmpty(roleId)) {
				this.roleModel.setRoleId(roleId);

				AdminTreeFactory instance = AdminTreeFactory.getInstance();
				// Attach model to old model
				if (oldModel == null) { // Attach this to tree
					setOldModel(roleModel.deepCopyRole(false, null));
					instance.getRoles().add(roleId, getOldModel());
				}
				this.dirty.setDirty(false);
				AdminTreeviewer adminTree = XMAdminUtil.getInstance().getAdminTree();
				adminTree.refresh(true);

				adminTree.setSelection(new StructuredSelection(instance.getRoles()), true);
				TreePath[] selectionPaths = adminTree.getStructuredSelection().getPaths();
				if (selectionPaths != null && selectionPaths.length > 0) {
					adminTree.setExpandedState(selectionPaths[0], true);
				}
				adminTree.setSelection(new StructuredSelection(getOldModel()), true);
				XMAdminUtil.getInstance().updateLogFile(messages.roleObject + " " + "'" + this.roleModel.getRoleName()
						+ "'" + " " + messages.objectCreate, MessageType.SUCCESS);
			}
		} catch (UnauthorizedAccessException e) {
			CustomMessageDialog.openError(this.getShell(), messages.objectPermissionDialogTitle,
					messages.objectPermissionDialogMsg);
		} catch (Exception e) {
			if (e instanceof ResourceAccessException) {
				CustomMessageDialog.openError(Display.getCurrent().getActiveShell(), messages.errorDialogTitile,
						messages.serverNotReachable);
			}
			LOGGER.warn("Unable to Save data ! " + e);
		}
	}

	private List<RoleRequest> mapVOObjectWithModel() {
		List<RoleRequest> roleRequests = new ArrayList<>();
		RoleRequest roleRequest = new RoleRequest();

		roleRequest.setRoleId(this.roleModel.getRoleId());
		roleRequest.setName(this.roleModel.getRoleName());
		roleRequest.setDescription(this.roleModel.getRoleDesc());
		boolean isXmAdmin = this.roleModel.getIsXmAdmin();
		Map<GlobalPermissionObjectType, ObjectGlobalPerm> objectGlobalPermissionMap = XMAdminUtil.getInstance().getObjectGlobalPermissionMap();
		
		boolean isXmHotline = this.roleModel.getIsXmHotline();
		boolean isViewInactive = this.roleModel.getIsViewInactive();
		
		List<ObjectPermission> objPermList = new ArrayList<>();
		Map<String, VoPermContainer> objPermissionCachedItems;
		if ((objPermissionCachedItems = this.objectPermissionViewer.getCachedObjPermissionMap()) != null
				&& objPermissionCachedItems.values().size() > 0) {
			for (VoPermContainer item : objPermissionCachedItems.values()) {
				objPermList.add(item.getObjectPermission());
			}
		}
		Map<String, VoPermContainer> objRelPermissionCachedItems;
		if ((objRelPermissionCachedItems = this.objRelationPermissionViewer.getCachedObjPermissionMap()) != null
				&& objRelPermissionCachedItems.values().size() > 0) {
			for (VoPermContainer item : objRelPermissionCachedItems.values()) {
				objPermList.add(item.getObjectPermission());
			}
		}
		if (isXmAdmin) {
			ObjectPermission objectPermission = new ObjectPermission();
			ObjectGlobalPerm objectGlobalPerm = objectGlobalPermissionMap.get(GlobalPermissionObjectType.LOGIN_CAX_START_ADMIN);
			String permissionId = objectGlobalPerm.getLoginCaxStartAdminPermission().getPermissionId();
			objectPermission.setPermissionId(permissionId);
			objectPermission.setCreationType(CreationType.ADD);
			objPermList.add(objectPermission);
		} else {
			ObjectPermission objectPermission = new ObjectPermission();
			ObjectGlobalPerm objectGlobalPerm = objectGlobalPermissionMap.get(GlobalPermissionObjectType.LOGIN_CAX_START_ADMIN);
			String permissionId = objectGlobalPerm.getLoginCaxStartAdminPermission().getPermissionId();
			objectPermission.setPermissionId(permissionId);
			objectPermission.setCreationType(CreationType.DELETE);
			objPermList.add(objectPermission);
		}
		if (isXmHotline) {
			ObjectPermission objectPermission = new ObjectPermission();
			ObjectGlobalPerm objectGlobalPerm = objectGlobalPermissionMap.get(GlobalPermissionObjectType.LOGIN_CAX_START_HOTLINE);
			String permissionId = objectGlobalPerm.getLoginCaxStartHotlinePermission().getPermissionId();
			objectPermission.setPermissionId(permissionId);
			objectPermission.setCreationType(CreationType.ADD);
			objPermList.add(objectPermission);
		} else {
			ObjectPermission objectPermission = new ObjectPermission();
			ObjectGlobalPerm objectGlobalPerm = objectGlobalPermissionMap.get(GlobalPermissionObjectType.LOGIN_CAX_START_HOTLINE);
			String permissionId = objectGlobalPerm.getLoginCaxStartHotlinePermission().getPermissionId();
			objectPermission.setPermissionId(permissionId);
			objectPermission.setCreationType(CreationType.DELETE);
			objPermList.add(objectPermission);
		}
		if (isViewInactive) {
			ObjectPermission objectPermission = new ObjectPermission();
			ObjectGlobalPerm objectGlobalPerm = objectGlobalPermissionMap.get(GlobalPermissionObjectType.VIEW_INACTIVE);
			String permissionId = objectGlobalPerm.getViewInactivePermission().getPermissionId();
			objectPermission.setPermissionId(permissionId);
			objectPermission.setCreationType(CreationType.ADD);
			objPermList.add(objectPermission);
		} else {
			ObjectPermission objectPermission = new ObjectPermission();
			ObjectGlobalPerm objectGlobalPerm = objectGlobalPermissionMap.get(GlobalPermissionObjectType.VIEW_INACTIVE);
			String permissionId = objectGlobalPerm.getViewInactivePermission().getPermissionId();
			objectPermission.setPermissionId(permissionId);
			objectPermission.setCreationType(CreationType.DELETE);
			objPermList.add(objectPermission);
		}
		roleRequest.setObjectPermissionList(objPermList);
		roleRequests.add(roleRequest);
		this.objectPermissionViewer.getCachedObjPermissionMap().clear();
		this.objRelationPermissionViewer.getCachedObjPermissionMap().clear();
		return roleRequests;
	}

	/**
	 * Set operation mode.
	 */
	public void setOperationMode() {
		if (this.roleModel != null) {
			if (roleModel.getOperationMode() == CommonConstants.OPERATIONMODE.VIEW) {
				this.txtName.setEditable(false);
				this.txtDesc.setEditable(false);
				this.viewInactiveBtn.setEnabled(false);
				this.xmAdminBtn.setEnabled(false);
				this.xmHotlineBtn.setEnabled(false);
				this.permissionCheckboxEditMsgLbl.setVisible(false);
				setShowButtonBar(false);
			} else if (roleModel.getOperationMode() == CommonConstants.OPERATIONMODE.CREATE) {
				this.txtName.setEditable(true);
				this.txtDesc.setEditable(true);
				this.saveBtn.setEnabled(false);
				this.viewInactiveBtn.setEnabled(true);
				this.xmAdminBtn.setEnabled(true);
				this.xmHotlineBtn.setEnabled(true);
				this.permissionCheckboxEditMsgLbl.setVisible(true);
				setShowButtonBar(true);
				this.dirty.setDirty(true);
			} else if (roleModel.getOperationMode() == CommonConstants.OPERATIONMODE.CHANGE) {
				this.txtName.setEditable(true);
				this.txtDesc.setEditable(true);
				this.viewInactiveBtn.setEnabled(true);
				this.xmAdminBtn.setEnabled(true);
				this.xmHotlineBtn.setEnabled(true);
				this.permissionCheckboxEditMsgLbl.setVisible(true);
				this.objectPermissionViewer.setEnableEditorSupport(true);
				this.objRelationPermissionViewer.setEnableEditorSupport(true);
				setShowButtonBar(true);
				this.dirty.setDirty(true);
			} else {
				this.txtName.setEditable(false);
				this.txtDesc.setEditable(false);
				this.viewInactiveBtn.setEnabled(false);
				this.xmAdminBtn.setEnabled(false);
				this.xmHotlineBtn.setEnabled(false);
				this.permissionCheckboxEditMsgLbl.setVisible(false);
			}
			this.objectPermissionViewer.refresh(true);
			this.objRelationPermissionViewer.refresh(true);
			final GridData msgLayoutData = (GridData) this.permissionCheckboxEditMsgLbl.getLayoutData();
			msgLayoutData.exclude = !this.permissionCheckboxEditMsgLbl.getVisible();
		}
	}

	/**
	 * Sets the role model.
	 *
	 * @param roleModel
	 *            the new role model
	 */
	public void setRoleModel(Role roleModel) {
		this.roleModel = roleModel;
	}

	/**
	 * Sets the old model.
	 *
	 * @param oldModel
	 *            the new old model
	 */
	public void setOldModel(Role oldModel) {
		this.oldModel = oldModel;
	}

	/**
	 * Gets the old model.
	 *
	 * @return the old model
	 */
	public Role getOldModel() {
		return oldModel;
	}

	/**
	 * Gets the role model.
	 *
	 * @return the role model
	 */
	public Role getRoleModel() {
		return roleModel;
	}

	/**
	 * Cancel role handler.
	 */
	public void cancelRoleHandler() {
		if (roleModel == null) {
			dirty.setDirty(false);
			return;
		}
		String roleId = CommonConstants.EMPTY_STR;
		int operationMode = this.roleModel.getOperationMode();
		Role oldModel = getOldModel();
		if (oldModel != null) {
			roleId = oldModel.getRoleId();
		}
		this.objectPermissionViewer.getCachedObjPermissionMap().clear();
		this.objRelationPermissionViewer.getCachedObjPermissionMap().clear();
		this.roleModel.initRolePermission();
		setRoleModel(null);
		setOldModel(null);
		this.saveBtn.setEnabled(true);
		dirty.setDirty(false);
		final AdminTreeviewer adminTree = XMAdminUtil.getInstance().getAdminTree();
		final Roles roles = AdminTreeFactory.getInstance().getRoles();
		if (operationMode == CommonConstants.OPERATIONMODE.CHANGE) {
			final IStructuredSelection selection = (IStructuredSelection) adminTree.getSelection();
			final Object firstElement = selection.getFirstElement();
			if (firstElement != null && firstElement.equals(roles.getRolesChildren().get(roleId))) {
				adminTree.setSelection(new StructuredSelection(roles.getRolesChildren().get(roleId)), true);
			}
		} else {
			adminTree.setSelection(new StructuredSelection(roles), true);
		}
	}

	/**
	 * Set role.
	 */
	public void setRole() {
		final XMAdminUtil instance = XMAdminUtil.getInstance();
		final AdminTreeviewer adminTree = instance.getAdminTree();
		final Object selectionObj = adminTree.getSelection();
		if (selectionObj instanceof IStructuredSelection) {
			Object firstElement = ((IStructuredSelection) selectionObj).getFirstElement();
			if (firstElement instanceof Role) {
				setOldModel((Role) firstElement);
				Role rightHandObject = getOldModel().deepCopyRole(false, null);
				setRoleModel(rightHandObject);
				this.roleModel.initRolePermission();
				registerMessages(this.registry);
				bindValues();
				setOperationMode();
				String roleName = this.roleModel.getRoleName();
				if (messages.superAdminNode.toString().equalsIgnoreCase(roleName)) {
					this.objectPermissionViewer.setEnableEditorSupport(false);
					this.objRelationPermissionViewer.setEnableEditorSupport(false);
				} else {
					if (roleModel.getOperationMode() == CommonConstants.OPERATIONMODE.VIEW) {
						// this.objectPermissionViewer.getTable().setEnabled(false);
						this.objectPermissionViewer.setEnableEditorSupport(false);
						this.objRelationPermissionViewer.setEnableEditorSupport(false);
					} else {
						this.objectPermissionViewer.setEnableEditorSupport(true);
						this.objRelationPermissionViewer.setEnableEditorSupport(true);
					}
				}
				this.objectPermissionViewer.setInput(this.roleModel.getObjPermissonMap().values());
				this.objRelationPermissionViewer.setInput(this.roleModel.getObjRelationPermissonMap().values());
				this.scrolledComposite.setOrigin(SWT.DEFAULT, SWT.DEFAULT);
				this.scrolledComposite.notifyListeners(SWT.Resize, new Event());
				instance.updatePermissions(this.roleModel);
				
				this.objectPermissionViewer.refresh();
				this.objRelationPermissionViewer.refresh();
			}
		}
	}

	/**
	 * Update permissions.
	 */
	/*private void updatePermissions() {
		String roleId;
		XMAdminUtil instance = XMAdminUtil.getInstance();
		if (this.roleModel != null && (roleId = this.roleModel.getRoleId()) != null && !roleId.isEmpty()) {
			final RolePermissionRelController controller = new RolePermissionRelController();
			Iterable<RolePermissionRelTbl> rolePermssionTbls;
			if ((rolePermssionTbls = controller.getPermissionByRoleId(roleId)) != null) {
				for (RolePermissionRelTbl rolePermissionRelTbl : rolePermssionTbls) {
					PermissionTbl permissionTbl = rolePermissionRelTbl.getPermissionId();
					String permissionType = permissionTbl.getPermissionType();
					String permissionName = permissionTbl.getName();
					if (GlobalPermissionObjectType.LOGIN_CAX_START_ADMIN.name().equals(permissionName)
							|| GlobalPermissionObjectType.LOGIN_CAX_START_HOTLINE.name().equals(permissionName)
							|| GlobalPermissionObjectType.VIEW_INACTIVE.name().equals(permissionName)) {
						continue;
					}
					String name = permissionName.split("-")[0];
					String permissionId = permissionTbl.getPermissionId();
					if (PermissionType.OBJECT_PERMISSION.name().equals(permissionType)) {
						ObjectType objectType = ObjectType.getObjectType(name);
						ObjectPermissions objectPermissions = this.roleModel.getObjPermissonMap().get(name);
						ObjectPermCont objectPermCont = instance.getObjectPermissionMap().get(objectType);
						PermissionTbl createPermission = objectPermCont.getCreatePermission();
						PermissionTbl changePermission = objectPermCont.getChangePermission();
						PermissionTbl deletePermission = objectPermCont.getDeletePermission();
						PermissionTbl activatePermission = objectPermCont.getActivatePermission();
						if (createPermission != null && createPermission.getPermissionId().equals(permissionId)) {
							objectPermissions.setCreate(true);
						} else if (changePermission != null
								&& changePermission.getPermissionId().equals(permissionId)) {
							objectPermissions.setChange(true);
						} else if (deletePermission != null
								&& deletePermission.getPermissionId().equals(permissionId)) {
							objectPermissions.setDelete(true);
						} else if (activatePermission != null
								&& activatePermission.getPermissionId().equals(permissionId)) {
							objectPermissions.setActivate(true);
						}
						continue;
					} else if (PermissionType.RELATION_PERMISSION.name().equals(permissionType)
							|| PermissionType.AA_BASED_RELATION_PERMISSION.name().equals(permissionType)) {
						ObjectRelationType objectType = ObjectRelationType.getObjectType(name);
						ObjectRelationPermissions objectRelationPermissions = this.roleModel
								.getObjRelationPermissonMap().get(name);
						ObjectRelPermCont objectRelPermCont = instance.getObjectRelPermissionMap().get(objectType);
						PermissionTbl assignPermission = objectRelPermCont.getAssignPermission();
						PermissionTbl removePermission = objectRelPermCont.getRemovePermission();
						PermissionTbl activatePermission = objectRelPermCont.getActivatePermission();
						PermissionTbl inactiveAssignmentPermission = objectRelPermCont.getInactiveAssignmentPermission();
						if (assignPermission != null && assignPermission.getPermissionId().equals(permissionId)) {
							objectRelationPermissions.setAssign(true);
						} else if (removePermission != null
								&& removePermission.getPermissionId().equals(permissionId)) {
							objectRelationPermissions.setRemove(true);
						} else if (activatePermission != null
								&& activatePermission.getPermissionId().equals(permissionId)) {
							objectRelationPermissions.setActivate(true);
						} else if (inactiveAssignmentPermission != null
								&& inactiveAssignmentPermission.getPermissionId().equals(permissionId)) {
							objectRelationPermissions.setInactiveAssignment(true);
						}
						continue;
					}
				}
			}
		}
	}*/

	/**
	 * Sets the model.
	 * @param role            the new model
	 * @param createAsFlag the create as flag
	 */
	public void setModel(final Role role, final boolean createAsFlag) {
		try {
			setOldModel(null);
			setRoleModel(role);
			registerMessages(this.registry);
			bindValues();
			setOperationMode();
			String roleName = this.roleModel.getRoleName();
			if (CommonConstants.SuperAdminRole.NAME.equalsIgnoreCase(roleName)) {
				this.objectPermissionViewer.setEnableEditorSupport(false);
				this.objRelationPermissionViewer.setEnableEditorSupport(false);
			} else {
				this.objectPermissionViewer.setEnableEditorSupport(true);
				this.objRelationPermissionViewer.setEnableEditorSupport(true);
			}
			this.objectPermissionViewer.setInput(this.roleModel.getObjPermissonMap().values());
			this.objRelationPermissionViewer.setInput(this.roleModel.getObjRelationPermissonMap().values());
			populateForCreateAs();
			this.scrolledComposite.setOrigin(SWT.DEFAULT, SWT.DEFAULT);
			this.scrolledComposite.notifyListeners(SWT.Resize, new Event());
		} catch (Exception e) {
			LOGGER.warn("Unable to set Role model ! " + e);
		}
	}
	
	/**
	 * Populate for create as.
	 */
	private void populateForCreateAs() {
		if (this.roleModel == null) {
			return;
		}
		final XMAdminUtil instance = XMAdminUtil.getInstance();
		final Map<String, ObjectPermissions> objectTypePermMap;
		final Map<String, ObjectRelationPermissions> objectRelPermMap;
		if ((objectTypePermMap = this.roleModel.getObjPermissonMap()) != null 
				&& (objectRelPermMap = this.roleModel.getObjRelationPermissonMap()) != null) {
			Set<String> objectPermKeys = objectTypePermMap.keySet();
			for (final String objectPermKey : objectPermKeys) {
				ObjectPermissions objectPermissions = objectTypePermMap.get(objectPermKey);
				if (objectPermissions.isCreate()) {
					String permissionId = instance.getObjectPermissionMap().get(ObjectType.getObjectType(objectPermKey)).getCreatePermission().getPermissionId();
					ObjectPermission objectPermission = new ObjectPermission();
					VoPermContainer voObjectPermContainer = new VoPermContainer();
					objectPermission.setPermissionId(permissionId);
					objectPermission.setCreationType(CreationType.ADD);
					voObjectPermContainer.setObjectPermission(objectPermission);
					objectPermissions.setCreatePermission(voObjectPermContainer);
					this.objectPermissionViewer.getCachedObjPermissionMap().put(permissionId, voObjectPermContainer);
				}
				if (objectPermissions.isChange()) {
					String permissionId = instance.getObjectPermissionMap().get(ObjectType.getObjectType(objectPermKey)).getChangePermission().getPermissionId();
					ObjectPermission objectPermission = new ObjectPermission();
					VoPermContainer voObjectPermContainer = new VoPermContainer();
					objectPermission.setPermissionId(permissionId);
					objectPermission.setCreationType(CreationType.ADD);
					voObjectPermContainer.setObjectPermission(objectPermission);
					objectPermissions.setChangePermission(voObjectPermContainer);
					this.objectPermissionViewer.getCachedObjPermissionMap().put(permissionId, voObjectPermContainer);
				}
				if (objectPermissions.isDelete()) {
					String permissionId = instance.getObjectPermissionMap().get(ObjectType.getObjectType(objectPermKey)).getDeletePermission().getPermissionId();
					ObjectPermission objectPermission = new ObjectPermission();
					VoPermContainer voObjectPermContainer = new VoPermContainer();
					objectPermission.setPermissionId(permissionId);
					objectPermission.setCreationType(CreationType.ADD);
					voObjectPermContainer.setObjectPermission(objectPermission);
					objectPermissions.setDeletePermission(voObjectPermContainer);
					this.objectPermissionViewer.getCachedObjPermissionMap().put(permissionId, voObjectPermContainer);
				}
				if (objectPermissions.isActivate()) {
					String permissionId = instance.getObjectPermissionMap().get(ObjectType.getObjectType(objectPermKey)).getActivatePermission().getPermissionId();
					ObjectPermission objectPermission = new ObjectPermission();
					VoPermContainer voObjectPermContainer = new VoPermContainer();
					objectPermission.setPermissionId(permissionId);
					objectPermission.setCreationType(CreationType.ADD);
					voObjectPermContainer.setObjectPermission(objectPermission);
					objectPermissions.setActivatePermission(voObjectPermContainer);
					this.objectPermissionViewer.getCachedObjPermissionMap().put(permissionId, voObjectPermContainer);
				}
			}
			Set<String> objectRelPermKeys = objectRelPermMap.keySet();
			for (final String objectRelPermKey : objectRelPermKeys) {
				ObjectRelationPermissions objectRelationPermissions = objectRelPermMap.get(objectRelPermKey);
				if (objectRelationPermissions.isAssign()) {
					String permissionId = instance.getObjectRelPermissionMap().get(ObjectRelationType.getObjectType(objectRelPermKey)).getAssignPermission().getPermissionId();
					ObjectPermission objectPermission = new ObjectPermission();
					VoPermContainer voObjectPermContainer = new VoPermContainer();
					objectPermission.setPermissionId(permissionId);
					objectPermission.setCreationType(CreationType.ADD);
					voObjectPermContainer.setObjectPermission(objectPermission);
					objectRelationPermissions.setAssignPermission(voObjectPermContainer);
					this.objRelationPermissionViewer.getCachedObjPermissionMap().put(permissionId, voObjectPermContainer);
				} 
				if (objectRelationPermissions.isRemove()) {
					String permissionId = instance.getObjectRelPermissionMap().get(ObjectRelationType.getObjectType(objectRelPermKey)).getRemovePermission().getPermissionId();
					ObjectPermission objectPermission = new ObjectPermission();
					VoPermContainer voObjectPermContainer = new VoPermContainer();
					objectPermission.setPermissionId(permissionId);
					objectPermission.setCreationType(CreationType.ADD);
					voObjectPermContainer.setObjectPermission(objectPermission);
					objectRelationPermissions.setRemovePermission(voObjectPermContainer);
					this.objRelationPermissionViewer.getCachedObjPermissionMap().put(permissionId, voObjectPermContainer);
				} 
				if (objectRelationPermissions.isActivate()) {
					String permissionId = instance.getObjectRelPermissionMap().get(ObjectRelationType.getObjectType(objectRelPermKey)).getActivatePermission().getPermissionId();
					ObjectPermission objectPermission = new ObjectPermission();
					VoPermContainer voObjectPermContainer = new VoPermContainer();
					objectPermission.setPermissionId(permissionId);
					objectPermission.setCreationType(CreationType.ADD);
					voObjectPermContainer.setObjectPermission(objectPermission);
					objectRelationPermissions.setActivatePermission(voObjectPermContainer);
					this.objRelationPermissionViewer.getCachedObjPermissionMap().put(permissionId, voObjectPermContainer);
				} 
				if (objectRelationPermissions.isInactiveAssignment()) {
					String permissionId = instance.getObjectRelPermissionMap().get(ObjectRelationType.getObjectType(objectRelPermKey)).getInactiveAssignmentPermission().getPermissionId();
					ObjectPermission objectPermission = new ObjectPermission();
					VoPermContainer voObjectPermContainer = new VoPermContainer();
					objectPermission.setPermissionId(permissionId);
					objectPermission.setCreationType(CreationType.ADD);
					voObjectPermContainer.setObjectPermission(objectPermission);
					objectRelationPermissions.setInactiveAssignmentPermission(voObjectPermContainer);
					this.objRelationPermissionViewer.getCachedObjPermissionMap().put(permissionId, voObjectPermContainer);
				}
			}
		}
	}

	/**
	 * Sets the dirty object.
	 *
	 * @param dirty
	 *            the new dirty object
	 */
	public void setDirtyObject(final MDirtyable dirty) {
		this.dirty = dirty;
	}
}
