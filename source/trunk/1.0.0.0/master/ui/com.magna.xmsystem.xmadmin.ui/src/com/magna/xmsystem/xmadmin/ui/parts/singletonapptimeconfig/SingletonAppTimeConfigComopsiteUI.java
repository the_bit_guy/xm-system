package com.magna.xmsystem.xmadmin.ui.parts.singletonapptimeconfig;

import org.eclipse.jface.layout.GridDataFactory;
import org.eclipse.jface.layout.GridLayoutFactory;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Spinner;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.singletonapptimeconfig.SingletonAppTimeConfig;

// TODO: Auto-generated Javadoc
/**
 * The Class SingletonAppTimeConfigComopsiteUI.
 * 
 * @author archita.patel
 */
public class SingletonAppTimeConfigComopsiteUI extends Composite {
	
	/** Logger instance. */
	private static final Logger LOGGER = LoggerFactory.getLogger(SingletonAppTimeConfigComopsiteUI.class);

	/** The grp singleton app time config. */
	protected Group grpSingletonAppTimeConfig;

	/** The lbl singleton app time. */
	protected Label lblSingletonAppTime;

	/** The spinner. */
	transient protected Spinner timeSpinner;

	/** The lbl max time. */
	transient protected Label lblsecTime;

	/** The btn save. */
	protected Button btnSave;

	/** The btn cancel. */
	protected Button btnCancel;

	/**
	 * Instantiates a new singleton app time config comopsite UI.
	 *
	 * @param parent
	 *            the parent
	 * @param style
	 *            the style
	 */
	public SingletonAppTimeConfigComopsiteUI(final Composite parent, final int style) {
		super(parent, style);
		this.initGUI();
	}

	/**
	 * Inits the GUI.
	 */
	private void initGUI() {
		try {
			GridLayoutFactory.fillDefaults().applyTo(this);
			this.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));

			this.grpSingletonAppTimeConfig = new Group(this, SWT.NONE);
			GridLayoutFactory.fillDefaults().numColumns(1).applyTo(this.grpSingletonAppTimeConfig);
			GridDataFactory.fillDefaults().grab(true, true).span(SWT.FILL, SWT.FILL)
					.applyTo(this.grpSingletonAppTimeConfig);

			final Composite widgetContainer = new Composite(this.grpSingletonAppTimeConfig, SWT.NONE);
			final GridLayout widgetContLayout = new GridLayout(3, false);
			widgetContLayout.horizontalSpacing = 10;

			widgetContainer.setLayout(widgetContLayout);
			widgetContainer.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));

			this.lblSingletonAppTime = new Label(widgetContainer, SWT.NONE);
			this.timeSpinner = new Spinner(widgetContainer, SWT.BORDER);
			this.timeSpinner.setMinimum(0);
			this.timeSpinner.setIncrement(1);
			this.timeSpinner.setPageIncrement(10);
			this.timeSpinner.setMaximum(SingletonAppTimeConfig.MAX_TIME);
			this.timeSpinner.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));

			this.lblsecTime = new Label(widgetContainer, SWT.NONE);
			final GridData gridDataCount = new GridData(SWT.LEFT, SWT.CENTER, false, false);
			gridDataCount.widthHint = 70;
			this.lblsecTime.setLayoutData(gridDataCount);

			final Composite buttonBarComp = new Composite(widgetContainer, SWT.NONE);
			final GridLayout btnBarCompLayout = new GridLayout(2, true);
			btnBarCompLayout.marginRight = 0;
			btnBarCompLayout.marginLeft = 0;
			btnBarCompLayout.marginTop = 0;
			btnBarCompLayout.marginBottom = 0;
			btnBarCompLayout.marginWidth = 0;
			buttonBarComp.setLayout(btnBarCompLayout);
			buttonBarComp.setLayoutData(new GridData(SWT.FILL, SWT.DEFAULT, true, false, 3, 1));
			createButtonBar(buttonBarComp);
		} catch (Exception e) {
			LOGGER.error("Unable to create Ui element", e);
		}

	}

	/**
	 * Creates the button bar.
	 *
	 * @param buttonBarComp
	 *            the button bar comp
	 */
	private void createButtonBar(final Composite buttonBarComp) {
		this.btnSave = new Button(buttonBarComp, SWT.NONE);
		this.btnSave.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, true, false));

		this.btnCancel = new Button(buttonBarComp, SWT.NONE);
		this.btnCancel.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, true, false));
	}

}
