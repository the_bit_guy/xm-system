package com.magna.xmsystem.xmadmin.ui.parts.objexpcontextmenu;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.inject.Inject;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.e4.core.services.nls.Translation;
import org.eclipse.e4.ui.model.application.MApplication;
import org.eclipse.e4.ui.model.application.ui.basic.MPart;
import org.eclipse.e4.ui.workbench.modeling.EModelService;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.ITreeSelection;
import org.eclipse.swt.widgets.Display;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.client.ResourceAccessException;

import com.magna.xmbackend.vo.adminArea.AdminAreaRequest;
import com.magna.xmbackend.vo.adminArea.AdminAreaResponse;
import com.magna.xmbackend.vo.baseApplication.BaseApplicationRequest;
import com.magna.xmbackend.vo.baseApplication.BaseApplicationResponse;
import com.magna.xmbackend.vo.jpa.site.SiteRequest;
import com.magna.xmbackend.vo.jpa.site.SiteResponse;
import com.magna.xmbackend.vo.project.ProjectRequest;
import com.magna.xmbackend.vo.project.ProjectResponse;
import com.magna.xmbackend.vo.projectApplication.ProjectApplicationRequest;
import com.magna.xmbackend.vo.projectApplication.ProjectApplicationResponse;
import com.magna.xmbackend.vo.startApplication.StartApplicationRequest;
import com.magna.xmbackend.vo.startApplication.StartApplicationResponse;
import com.magna.xmbackend.vo.user.UserRequest;
import com.magna.xmbackend.vo.user.UserResponse;
import com.magna.xmbackend.vo.userApplication.UserApplicationRequest;
import com.magna.xmbackend.vo.userApplication.UserApplicationResponse;
import com.magna.xmsystem.dependencies.customExceptions.UnauthorizedAccessException;
import com.magna.xmsystem.ui.controls.widgets.CustomMessageDialog;
import com.magna.xmsystem.xmadmin.message.Message;
import com.magna.xmsystem.xmadmin.restclient.adminArea.AdminAreaController;
import com.magna.xmsystem.xmadmin.restclient.application.BaseAppController;
import com.magna.xmsystem.xmadmin.restclient.application.ProjectAppController;
import com.magna.xmsystem.xmadmin.restclient.application.StartAppController;
import com.magna.xmsystem.xmadmin.restclient.application.UserAppController;
import com.magna.xmsystem.xmadmin.restclient.project.ProjectController;
import com.magna.xmsystem.xmadmin.restclient.site.SiteController;
import com.magna.xmsystem.xmadmin.restclient.users.UserController;
import com.magna.xmsystem.xmadmin.ui.parts.InformationPart;
import com.magna.xmsystem.xmadmin.ui.parts.objexp.ObjectExpPage;
import com.magna.xmsystem.xmadmin.ui.parts.objexp.ObjectExplorer;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.AdminTreeviewer;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.AdminTreeFactory;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.adminarea.AdministrationArea;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.adminarea.AdministrationAreas;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.baseapplicationmodel.BaseApplication;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.baseapplicationmodel.BaseApplications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.projectapplicationmodel.ProjectApplication;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.projectapplicationmodel.ProjectApplications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.startapplicationmodel.StartApplication;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.startapplicationmodel.StartApplications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.userapplicationmodel.UserApplication;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.userapplicationmodel.UserApplications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.projects.Project;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.projects.ProjectUserAdminAreas;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.projects.Projects;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.sitemodel.Site;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.sitemodel.Sites;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.users.User;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.users.UserAdminAreas;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.users.UserProjectAdminAreas;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.users.Users;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.users.UsersNameAlphabet;
import com.magna.xmsystem.xmadmin.util.CommonConstants;
import com.magna.xmsystem.xmadmin.util.JobSchedulingRule;
import com.magna.xmsystem.xmadmin.util.MessageType;
import com.magna.xmsystem.xmadmin.util.XMAdminUtil;

/**
 * The Class DeActivateAction.
 */
public class DeActivateAction extends Action {

	/** The Constant LOGGER. */
	private static final Logger LOGGER = LoggerFactory.getLogger(DeActivateAction.class);

	/** The messages. */
	@Inject
	@Translation
	private Message messages;

	/** Inject of {@link EModelService}. */
	@Inject
	private EModelService modelService;

	/** Inject of {@link MApplication}. */
	@Inject
	private MApplication application;

	/** The job scheduling rule. */
	private JobSchedulingRule jobSchedulingRule;

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jface.action.Action#run()
	 */
	@Override
	public void run() {
		final MPart mPart = (MPart) this.modelService.find(CommonConstants.PART_ID.INFORMATION_PART_ID, application);
		final InformationPart rightSidePart2 = (InformationPart) mPart.getObject();
		final ObjectExpPage objectExpPage = rightSidePart2.getObjectExpPartUI();
		final ObjectExplorer objExpTableViewer = objectExpPage.getObjExpTableViewer();
		final IStructuredSelection selections = (IStructuredSelection) objExpTableViewer.getSelection();
		final Object selectionObj = selections.getFirstElement();
		this.jobSchedulingRule = new JobSchedulingRule();
		try {
			if (XMAdminUtil.getInstance().isUpdateStatusAllowed(selections)) {
				if (selectionObj instanceof Site) {
					deactivateSite(selections);
				} else if (selectionObj instanceof AdministrationArea) {
					deactivateAdminArea(selections);
				} else if (selectionObj instanceof Project) {
					deactivateProject(selections);
				} else if (selectionObj instanceof User) {
					deactivateUser(selections);
				} else if (selectionObj instanceof UserApplication) {
					deactivateUserApplication(selections);
				} else if (selectionObj instanceof ProjectApplication) {
					deactivateProjectApplication(selections);
				} else if (selectionObj instanceof StartApplication) {
					deactivateStartApplication(selections);
				} else if (selectionObj instanceof BaseApplication) {
					deactivateBaseApplication(selections);
				}

				Job ruleJob = new Job("refresh..") {

					@Override
					protected IStatus run(IProgressMonitor monitor) {
						Display.getDefault().asyncExec(new Runnable() {
							@Override
							public void run() {
								objExpTableViewer.refresh(true);
							}
						});

						return Status.OK_STATUS;
					}
				};
				ruleJob.setRule(this.jobSchedulingRule);
				ruleJob.schedule();

			} else {
				CustomMessageDialog.openError(Display.getDefault().getActiveShell(), messages.objectPermissionDialogTitle,
						messages.objectPermissionDialogMsg);
			}
		} catch (UnauthorizedAccessException e) {
			LOGGER.error("Current user is Unauthorized " + e);
		} catch (Exception e) {
			if (e instanceof ResourceAccessException) {
				CustomMessageDialog.openError(Display.getCurrent().getActiveShell(), messages.errorDialogTitile,
						messages.serverNotReachable);
			}
			LOGGER.error("Exception occured in calling access allowed API " + e);
		}
	}

	/**
	 * Deactivate user.
	 *
	 * @param selections
	 *            the selections
	 */
	@SuppressWarnings("unchecked")
	private void deactivateUser(final IStructuredSelection selections) {
		Object selectionObj = selections.getFirstElement();
		AdminTreeviewer adminTree = XMAdminUtil.getInstance().getAdminTree();
		String name = ((User) selectionObj).getName();
		String confirmDialogTitle = messages.changeStatusConfirmDialogTitle;
		String confirmDialogMsg;
		if (selections.size() == 1) {
			confirmDialogMsg = messages.deActivatechangeStatusConfirmDialogMsg + " \'" + name + "\'" + " ?";
		} else {
			confirmDialogMsg = messages.deActivateMultichangeStatusConfirmDialogMsg + " ?";
		}
		boolean isConfirmed = CustomMessageDialog.openQuestion(adminTree.getControl().getShell(), confirmDialogTitle,
				confirmDialogMsg);
		if (isConfirmed) {
			final List<Object> selectionList = selections.toList();

			Job deactivateUserJob = new Job("De-Activating..") {

				@SuppressWarnings("rawtypes")
				@Override
				protected IStatus run(final IProgressMonitor monitor) {
					monitor.beginTask("De-Activating..", 100);
					monitor.worked(30);
					try {
						List<UserRequest> userRequestList = new ArrayList<>();
						Map<String, String> userIdMap = new HashMap<>();
						for (int i = 0; i < selectionList.size(); i++) {
							UserRequest userRequest = new UserRequest();
							User userModel = (User) selectionList.get(i);
							userRequest.setId(userModel.getUserId());
							userRequest.setStatus(com.magna.xmbackend.vo.enums.Status.INACTIVE);
							userRequest.setUserName(userModel.getName());
							userRequestList.add(userRequest);
							userIdMap.put(userModel.getUserId(), userModel.getName());
						}
						UserController userCntr = new UserController();
						UserResponse userResponse;
						userResponse = userCntr.multiUpdateUserStatus(userRequestList);
						if (userResponse != null) {
							List<String> statusUpdatationFailedList = userResponse.getStatusUpdatationFailedList();
							if (!statusUpdatationFailedList.isEmpty()) {
								for (String name : statusUpdatationFailedList) {
									Iterator<Map.Entry<String, String>> iter = userIdMap.entrySet().iterator();
									while (iter.hasNext()) {
										Map.Entry<String, String> entry = iter.next();
										if (name.equalsIgnoreCase(entry.getValue())) {
											iter.remove();
										}
									}
								}
								Display.getDefault().asyncExec(new Runnable() {
									public void run() {
										CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
												messages.changeStatusConfirmDialogTitle,
												messages.multiStatusUpadteObjectDilaogErrMsg);
									}
								});
							}
							Users users = AdminTreeFactory.getInstance().getUsers();
							Map<String, IAdminTreeChild> usersChildren = users.getUsersChildren();
							for (Entry<String, String> user : userIdMap.entrySet()) {
								String name = user.getValue();
								char startChar = (name.toString().toLowerCase().toCharArray()[0]);
								IAdminTreeChild iAdminTreeChild = usersChildren.get(String.valueOf(startChar));
								if (iAdminTreeChild instanceof UsersNameAlphabet) {
									UsersNameAlphabet usersNameAlphabet = (UsersNameAlphabet) iAdminTreeChild;
									IAdminTreeChild iAdminTreeChild2 = usersNameAlphabet.getUsersChildren()
											.get(user.getKey());
									((User) iAdminTreeChild2).setActive(false);
								}
							}
							Display.getDefault().asyncExec(new Runnable() {
								@Override
								public void run() {
									adminTree.refresh(true);
									monitor.worked(1);
								}
							});
							if (userIdMap.size() > 0) {
								Iterator it = userIdMap.entrySet().iterator();
								while (it.hasNext()) {
									Map.Entry map = (Map.Entry) it.next();
									XMAdminUtil.getInstance().updateLogFile(
											messages.objectStatusUpdate + " " + messages.userObject + " '"
													+ map.getValue() + "' " + messages.objectUpdate,
											MessageType.SUCCESS);
								}
							}
						}
					} catch (UnauthorizedAccessException e) {
						Display.getDefault().asyncExec(new Runnable() {
							@Override
							public void run() {
								CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
										messages.objectPermissionDialogTitle, messages.objectPermissionDialogMsg);
							}
						});
					} catch (Exception e) {
						LOGGER.warn("Unable to De-Activate user model! ", e);
					}
					monitor.worked(70);
					monitor.done();
					return Status.OK_STATUS;
				}
			};
			deactivateUserJob.setUser(true);
			deactivateUserJob.setRule(this.jobSchedulingRule);
			deactivateUserJob.schedule();
		}
		// adminTree.refresh(userModel);
	}

	/**
	 * Deactivate base application.
	 *
	 * @param selections
	 *            the selections
	 */
	@SuppressWarnings("unchecked")
	private void deactivateBaseApplication(final IStructuredSelection selections) {
		Object selectionObj = selections.getFirstElement();
		AdminTreeviewer adminTree = XMAdminUtil.getInstance().getAdminTree();
		ITreeSelection structuredSelection = adminTree.getStructuredSelection();
		String name = ((BaseApplication) selectionObj).getName();
		String confirmDialogTitle = messages.changeStatusConfirmDialogTitle;
		String confirmDialogMsg;
		if (selections.size() == 1) {
			confirmDialogMsg = messages.deActivatechangeStatusConfirmDialogMsg + " \'" + name + "\'" + " ?";
		} else {
			confirmDialogMsg = messages.deActivateMultichangeStatusConfirmDialogMsg + " ?";
		}
		boolean isConfirmed = CustomMessageDialog.openQuestion(adminTree.getControl().getShell(), confirmDialogTitle,
				confirmDialogMsg);
		if (isConfirmed) {
			final List<Object> selectionList = selections.toList();

			Job deactivateBaseAppJob = new Job("De-Activating..") {

				@SuppressWarnings("rawtypes")
				@Override
				protected IStatus run(final IProgressMonitor monitor) {
					monitor.beginTask("De-Activating..", 100);
					monitor.worked(30);
					try {
						List<BaseApplicationRequest> baseAppRequestList = new ArrayList<>();
						Map<String, String> baseAppIdMap = new HashMap<>();
						for (int i = 0; i < selectionList.size(); i++) {
							BaseApplicationRequest baseAppRequest = new BaseApplicationRequest();
							BaseApplication baseApplication = (BaseApplication) selectionList.get(i);
							baseAppRequest.setId(baseApplication.getBaseApplicationId());
							baseAppRequest.setStatus(com.magna.xmbackend.vo.enums.Status.INACTIVE.name());
							baseAppRequest.setName(baseApplication.getName());
							baseAppRequestList.add(baseAppRequest);
							baseAppIdMap.put(baseApplication.getBaseApplicationId(), baseApplication.getName());
						}
						BaseAppController baseAppController = new BaseAppController();
						BaseApplicationResponse BaseAppResponse = baseAppController
								.multiUpdateBaseAppStatus(baseAppRequestList);
						if (BaseAppResponse != null) {

							List<String> statusUpdatationFailedList = BaseAppResponse.getStatusUpdatationFailedList();
							if (!statusUpdatationFailedList.isEmpty()) {
								for (String name : statusUpdatationFailedList) {
									Iterator<Map.Entry<String, String>> iter = baseAppIdMap.entrySet().iterator();
									while (iter.hasNext()) {
										Map.Entry<String, String> entry = iter.next();
										if (name.equalsIgnoreCase(entry.getValue())) {
											iter.remove();
										}
									}
								}
								Display.getDefault().asyncExec(new Runnable() {
									public void run() {
										CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
												messages.changeStatusConfirmDialogTitle,
												messages.multiStatusUpadteObjectDilaogErrMsg);
									}
								});
							}

							Object firstElement = structuredSelection.getFirstElement();
							if (firstElement instanceof BaseApplications) {
								BaseApplications baseApps = (BaseApplications) firstElement;
								Map<String, IAdminTreeChild> baseAppsChildren = baseApps.getBaseApplications();
								for (String baseAppId : baseAppIdMap.keySet()) {
									((BaseApplication) baseAppsChildren.get(baseAppId)).setActive(false);
								}
							}
							Display.getDefault().asyncExec(new Runnable() {
								@Override
								public void run() {
									adminTree.refresh(true);
									monitor.worked(1);
								}
							});
							if (baseAppIdMap.size() > 0) {
								Iterator it = baseAppIdMap.entrySet().iterator();
								while (it.hasNext()) {
									Map.Entry map = (Map.Entry) it.next();
									XMAdminUtil.getInstance().updateLogFile(
											messages.objectStatusUpdate + " " + messages.baseApplicationObject + " '"
													+ map.getValue() + "' " + messages.objectUpdate,
											MessageType.SUCCESS);
								}
							}
						}
					} catch (UnauthorizedAccessException e) {
						Display.getDefault().asyncExec(new Runnable() {
							@Override
							public void run() {
								CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
										messages.objectPermissionDialogTitle, messages.objectPermissionDialogMsg);
							}
						});
					} catch (Exception e) {
						LOGGER.warn("Unable to De-Activate base application model! ", e);
					}
					monitor.worked(70);
					monitor.done();
					return Status.OK_STATUS;
				}
			};
			deactivateBaseAppJob.setUser(true);
			deactivateBaseAppJob.setRule(this.jobSchedulingRule);
			deactivateBaseAppJob.schedule();
		}
	}

	/**
	 * Deactivate start application.
	 *
	 * @param selections
	 *            the selections
	 */
	@SuppressWarnings("unchecked")
	private void deactivateStartApplication(final IStructuredSelection selections) {
		Object selectionObj = selections.getFirstElement();
		AdminTreeviewer adminTree = XMAdminUtil.getInstance().getAdminTree();
		ITreeSelection structuredSelection = adminTree.getStructuredSelection();
		String name = ((StartApplication) selectionObj).getName();
		String confirmDialogTitle = messages.changeStatusConfirmDialogTitle;
		String confirmDialogMsg;
		if (selections.size() == 1) {
			confirmDialogMsg = messages.deActivatechangeStatusConfirmDialogMsg + " \'" + name + "\'" + " ?";
		} else {
			confirmDialogMsg = messages.deActivateMultichangeStatusConfirmDialogMsg + " ?";
		}
		boolean isConfirmed = CustomMessageDialog.openQuestion(adminTree.getControl().getShell(), confirmDialogTitle,
				confirmDialogMsg);
		if (isConfirmed) {
			final List<Object> selectionList = selections.toList();

			Job deactivateStartAppJob = new Job("De-Activating..") {
				@SuppressWarnings("rawtypes")
				@Override
				protected IStatus run(final IProgressMonitor monitor) {
					monitor.beginTask("De-Activating..", 100);
					monitor.worked(30);
					try {
						List<StartApplicationRequest> startAppRequestList = new ArrayList<>();
						Map<String, String> startAppIdMap = new HashMap<>();
						for (int i = 0; i < selectionList.size(); i++) {
							StartApplicationRequest startAppRequest = new StartApplicationRequest();
							StartApplication startAppModel = (StartApplication) selectionList.get(i);
							startAppRequest.setId(startAppModel.getStartPrgmApplicationId());
							startAppRequest.setStatus(com.magna.xmbackend.vo.enums.Status.INACTIVE.name());
							startAppRequest.setName(startAppModel.getName());
							startAppRequestList.add(startAppRequest);
							startAppIdMap.put(startAppModel.getStartPrgmApplicationId(), startAppModel.getName());
						}
						StartAppController userAppCntr = new StartAppController();
						StartApplicationResponse startApplicationResponse = userAppCntr
								.multiUpdateStartAppStatus(startAppRequestList);
						if (startApplicationResponse != null) {

							List<String> statusUpdatationFailedList = startApplicationResponse
									.getStatusUpdatationFailedList();
							if (!statusUpdatationFailedList.isEmpty()) {
								for (String name : statusUpdatationFailedList) {
									Iterator<Map.Entry<String, String>> iter = startAppIdMap.entrySet().iterator();
									while (iter.hasNext()) {
										Map.Entry<String, String> entry = iter.next();
										if (name.equalsIgnoreCase(entry.getValue())) {
											iter.remove();
										}
									}
								}
								Display.getDefault().asyncExec(new Runnable() {
									public void run() {
										CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
												messages.changeStatusConfirmDialogTitle,
												messages.multiStatusUpadteObjectDilaogErrMsg);
									}
								});
							}

							Object firstElement = structuredSelection.getFirstElement();
							if (firstElement instanceof StartApplications) {
								StartApplications startApps = (StartApplications) firstElement;
								Map<String, IAdminTreeChild> startAppsChildren = startApps.getStartApplications();
								for (String startAppId : startAppIdMap.keySet()) {
									((StartApplication) startAppsChildren.get(startAppId)).setActive(false);
								}
							}
							Display.getDefault().asyncExec(new Runnable() {
								@Override
								public void run() {
									adminTree.refresh(true);
									monitor.worked(1);
								}
							});
							if (startAppIdMap.size() > 0) {
								Iterator it = startAppIdMap.entrySet().iterator();
								while (it.hasNext()) {
									Map.Entry map = (Map.Entry) it.next();
									XMAdminUtil.getInstance().updateLogFile(
											messages.objectStatusUpdate + " " + messages.startApplicationObject + " '"
													+ map.getValue() + "' " + messages.objectUpdate,
											MessageType.SUCCESS);
								}
							}
						}
					} catch (UnauthorizedAccessException e) {
						Display.getDefault().asyncExec(new Runnable() {

							@Override
							public void run() {
								CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
										messages.objectPermissionDialogTitle, messages.objectPermissionDialogMsg);
							}
						});
					} catch (Exception e) {
						LOGGER.warn("Unable to De-Activate Start Application model! ", e);
					}
					monitor.worked(70);
					monitor.done();
					return Status.OK_STATUS;
				}
			};
			deactivateStartAppJob.setUser(true);
			deactivateStartAppJob.setRule(this.jobSchedulingRule);
			deactivateStartAppJob.schedule();
		}
	}

	/**
	 * Deactivate project application.
	 *
	 * @param selections
	 *            the selections
	 */
	@SuppressWarnings("unchecked")
	private void deactivateProjectApplication(final IStructuredSelection selections) {
		Object selectionObj = selections.getFirstElement();
		AdminTreeviewer adminTree = XMAdminUtil.getInstance().getAdminTree();
		ITreeSelection structuredSelection = adminTree.getStructuredSelection();
		String name = ((ProjectApplication) selectionObj).getName();
		String confirmDialogTitle = messages.changeStatusConfirmDialogTitle;
		String confirmDialogMsg;
		if (selections.size() == 1) {
			confirmDialogMsg = messages.deActivatechangeStatusConfirmDialogMsg + " \'" + name + "\'" + " ?";
		} else {
			confirmDialogMsg = messages.deActivateMultichangeStatusConfirmDialogMsg + " ?";
		}
		boolean isConfirmed = CustomMessageDialog.openQuestion(adminTree.getControl().getShell(), confirmDialogTitle,
				confirmDialogMsg);
		if (isConfirmed) {
			final List<Object> selectionList = selections.toList();

			Job deactivateProjectAppJob = new Job("De-Activating..") {

				@SuppressWarnings("rawtypes")
				@Override
				protected IStatus run(final IProgressMonitor monitor) {
					monitor.beginTask("De-Activating..", 100);
					monitor.worked(30);
					try {
						List<ProjectApplicationRequest> projectAppRequestList = new ArrayList<>();
						Map<String, String> projectAppIdMap = new HashMap<>();
						for (int i = 0; i < selectionList.size(); i++) {
							ProjectApplicationRequest projectAppRequest = new ProjectApplicationRequest();
							ProjectApplication projectApplication = (ProjectApplication) selectionList.get(i);
							projectAppRequest.setId(projectApplication.getProjectApplicationId());
							projectAppRequest.setStatus(com.magna.xmbackend.vo.enums.Status.INACTIVE.name());
							projectAppRequest.setName(projectApplication.getName());
							projectAppRequestList.add(projectAppRequest);
							projectAppIdMap.put(projectApplication.getProjectApplicationId(),
									projectApplication.getName());
						}
						ProjectAppController projectAppController = new ProjectAppController();
						ProjectApplicationResponse projectAppResponse = projectAppController
								.multiUpdateProjectAppStatus(projectAppRequestList);
						if (projectAppResponse != null) {

							List<String> statusUpdatationFailedList = projectAppResponse
									.getStatusUpdatationFailedList();
							if (!statusUpdatationFailedList.isEmpty()) {
								for (String name : statusUpdatationFailedList) {
									Iterator<Map.Entry<String, String>> iter = projectAppIdMap.entrySet().iterator();
									while (iter.hasNext()) {
										Map.Entry<String, String> entry = iter.next();
										if (name.equalsIgnoreCase(entry.getValue())) {
											iter.remove();
										}
									}
								}
								Display.getDefault().asyncExec(new Runnable() {
									public void run() {
										CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
												messages.changeStatusConfirmDialogTitle,
												messages.multiStatusUpadteObjectDilaogErrMsg);
									}
								});
							}

							Object firstElement = structuredSelection.getFirstElement();
							if (firstElement instanceof ProjectApplications) {
								ProjectApplications projectApps = (ProjectApplications) firstElement;
								Map<String, IAdminTreeChild> projectAppsChildren = projectApps.getProjectApplications();
								for (String projectAppId : projectAppIdMap.keySet()) {
									((ProjectApplication) projectAppsChildren.get(projectAppId)).setActive(false);
								}
							}
							Display.getDefault().asyncExec(new Runnable() {
								@Override
								public void run() {
									adminTree.refresh(true);
									monitor.worked(1);
								}
							});
							if (projectAppIdMap.size() > 0) {
								Iterator it = projectAppIdMap.entrySet().iterator();
								while (it.hasNext()) {
									Map.Entry map = (Map.Entry) it.next();
									XMAdminUtil.getInstance().updateLogFile(
											messages.objectStatusUpdate + " " + messages.projectApplicationObject + " '"
													+ map.getValue() + "' " + messages.objectUpdate,
											MessageType.SUCCESS);
								}
							}
						}
					} catch (UnauthorizedAccessException e) {
						Display.getDefault().asyncExec(new Runnable() {

							@Override
							public void run() {
								CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
										messages.objectPermissionDialogTitle, messages.objectPermissionDialogMsg);
							}
						});

					} catch (Exception e) {
						LOGGER.warn("Unable to De-Activate project application model! ", e);
					}
					monitor.worked(70);
					monitor.done();
					return Status.OK_STATUS;
				}
			};
			deactivateProjectAppJob.setUser(true);
			deactivateProjectAppJob.setRule(this.jobSchedulingRule);
			deactivateProjectAppJob.schedule();
		}

	}

	/**
	 * Deactivate user application.
	 *
	 * @param selections
	 *            the selections
	 */
	@SuppressWarnings("unchecked")
	private void deactivateUserApplication(final IStructuredSelection selections) {
		Object selectionObj = selections.getFirstElement();
		AdminTreeviewer adminTree = XMAdminUtil.getInstance().getAdminTree();
		ITreeSelection structuredSelection = adminTree.getStructuredSelection();
		String name = ((UserApplication) selectionObj).getName();
		String confirmDialogTitle = messages.changeStatusConfirmDialogTitle;
		String confirmDialogMsg;
		if (selections.size() == 1) {
			confirmDialogMsg = messages.deActivatechangeStatusConfirmDialogMsg + " \'" + name + "\'" + " ?";
		} else {
			confirmDialogMsg = messages.deActivateMultichangeStatusConfirmDialogMsg + " ?";
		}
		boolean isConfirmed = CustomMessageDialog.openQuestion(adminTree.getControl().getShell(), confirmDialogTitle,
				confirmDialogMsg);
		if (isConfirmed) {
			final List<Object> selectionList = selections.toList();

			Job deactivateUserAppJob = new Job("De-Activating..") {

				@SuppressWarnings("rawtypes")
				@Override
				protected IStatus run(final IProgressMonitor monitor) {
					monitor.beginTask("De-Activating..", 100);
					monitor.worked(30);
					try {
						List<UserApplicationRequest> userAppRequestList = new ArrayList<>();
						Map<String, String> userAppIdMap = new HashMap<>();
						for (int i = 0; i < selectionList.size(); i++) {
							UserApplicationRequest userApplicationRequest = new UserApplicationRequest();
							UserApplication userAppModel = (UserApplication) selectionList.get(i);
							userApplicationRequest.setId(userAppModel.getUserApplicationId());
							userApplicationRequest.setStatus(com.magna.xmbackend.vo.enums.Status.INACTIVE.name());
							userApplicationRequest.setName(userAppModel.getName());
							userAppRequestList.add(userApplicationRequest);
							userAppIdMap.put(userAppModel.getUserApplicationId(), userAppModel.getName());
						}
						UserAppController userAppCntr = new UserAppController();
						UserApplicationResponse userApplicationResponse = userAppCntr
								.multiUpdateUserAppStatus(userAppRequestList);
						if (userApplicationResponse != null) {

							List<String> statusUpdatationFailedList = userApplicationResponse
									.getStatusUpdatationFailedList();
							if (!statusUpdatationFailedList.isEmpty()) {
								for (String name : statusUpdatationFailedList) {
									Iterator<Map.Entry<String, String>> iter = userAppIdMap.entrySet().iterator();
									while (iter.hasNext()) {
										Map.Entry<String, String> entry = iter.next();
										if (name.equalsIgnoreCase(entry.getValue())) {
											iter.remove();
										}
									}
								}
								Display.getDefault().asyncExec(new Runnable() {
									public void run() {
										CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
												messages.changeStatusConfirmDialogTitle,
												messages.multiStatusUpadteObjectDilaogErrMsg);
									}
								});
							}
							Object firstElement = structuredSelection.getFirstElement();
							if (firstElement instanceof UserApplications) {
								UserApplications userApplications = (UserApplications) firstElement;
								Map<String, IAdminTreeChild> userAppsChildren = userApplications.getUserApplications();
								for (String userAppId : userAppIdMap.keySet()) {
									((UserApplication) userAppsChildren.get(userAppId)).setActive(false);
								}
							}

							Display.getDefault().asyncExec(new Runnable() {
								@Override
								public void run() {
									adminTree.refresh(true);
								}
							});
							if (userAppIdMap.size() > 0) {
								Iterator it = userAppIdMap.entrySet().iterator();
								while (it.hasNext()) {
									Map.Entry map = (Map.Entry) it.next();
									XMAdminUtil.getInstance().updateLogFile(
											messages.objectStatusUpdate + " " + messages.userApplicationObject + " '"
													+ map.getValue() + "' " + messages.objectUpdate,
											MessageType.SUCCESS);
								}
							}
						}
					} catch (UnauthorizedAccessException e) {
						Display.getDefault().asyncExec(new Runnable() {

							@Override
							public void run() {
								CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
										messages.objectPermissionDialogTitle, messages.objectPermissionDialogMsg);
							}
						});
					} catch (Exception e) {
						LOGGER.warn("Unable to De-Activate user application model! ", e);
					}
					monitor.worked(70);
					monitor.done();
					return Status.OK_STATUS;
				}
			};
			deactivateUserAppJob.setUser(true);
			deactivateUserAppJob.setRule(this.jobSchedulingRule);
			deactivateUserAppJob.schedule();
		}
	}

	/**
	 * Deactivate project.
	 *
	 * @param selections
	 *            the selections
	 */
	@SuppressWarnings("unchecked")
	private void deactivateProject(final IStructuredSelection selections) {
		Object selectionObj = selections.getFirstElement();
		AdminTreeviewer adminTree = XMAdminUtil.getInstance().getAdminTree();
		ITreeSelection structuredSelection = adminTree.getStructuredSelection();
		String name = ((Project) selectionObj).getName();
		String confirmDialogTitle = messages.changeStatusConfirmDialogTitle;
		String confirmDialogMsg;
		if (selections.size() == 1) {
			confirmDialogMsg = messages.deActivatechangeStatusConfirmDialogMsg + " \'" + name + "\'" + " ?";
		} else {
			confirmDialogMsg = messages.deActivateMultichangeStatusConfirmDialogMsg + " ?";
		}
		boolean isConfirmed = CustomMessageDialog.openQuestion(adminTree.getControl().getShell(), confirmDialogTitle,
				confirmDialogMsg);
		if (isConfirmed) {
			final List<Object> selectionList = selections.toList();

			Job deactivateProjectJob = new Job("De-Activating..") {

				@SuppressWarnings("rawtypes")
				@Override
				protected IStatus run(final IProgressMonitor monitor) {
					monitor.beginTask("De-Activating..", 100);
					monitor.worked(30);
					try {
						List<ProjectRequest> projectRequestList = new ArrayList<>();
						Map<String, String> projectIdMap = new HashMap<>();
						for (int i = 0; i < selectionList.size(); i++) {
							ProjectRequest projectRequest = new ProjectRequest();
							Project projectModel = (Project) selectionList.get(i);
							projectRequest.setId(projectModel.getProjectId());
							projectRequest.setStatus(com.magna.xmbackend.vo.enums.Status.INACTIVE.name());
							projectRequest.setName(projectModel.getName());
							projectIdMap.put(projectModel.getProjectId(), projectModel.getName());
							projectRequestList.add(projectRequest);
						}
						ProjectController projectCntr = new ProjectController();
						ProjectResponse projectResponse = projectCntr.multiUpdateProjectStatus(projectRequestList);
						if (projectResponse != null) {

							List<String> statusUpdatationFailedList = projectResponse.getStatusUpdatationFailedList();
							if (!statusUpdatationFailedList.isEmpty()) {
								for (String name : statusUpdatationFailedList) {
									Iterator<Map.Entry<String, String>> iter = projectIdMap.entrySet().iterator();
									while (iter.hasNext()) {
										Map.Entry<String, String> entry = iter.next();
										if (name.equalsIgnoreCase(entry.getValue())) {
											iter.remove();
										}
									}
								}
								Display.getDefault().asyncExec(new Runnable() {
									public void run() {
										CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
												messages.changeStatusConfirmDialogTitle,
												messages.multiStatusUpadteObjectDilaogErrMsg);
									}
								});
							}

							Object firstElement = structuredSelection.getFirstElement();
							if (firstElement instanceof Projects) {
								Projects projects = (Projects) firstElement;
								Map<String, IAdminTreeChild> projectsChildren = projects.getProjectsChildren();

								for (String projectId : projectIdMap.keySet()) {
									((Project) projectsChildren.get(projectId)).setActive(false);
								}
							}

							Display.getDefault().asyncExec(new Runnable() {
								@Override
								public void run() {
									adminTree.refresh(true);
									monitor.worked(1);
								}
							});
							if (projectIdMap.size() > 0) {
								Iterator it = projectIdMap.entrySet().iterator();
								while (it.hasNext()) {
									Map.Entry map = (Map.Entry) it.next();
									XMAdminUtil.getInstance().updateLogFile(
											messages.objectStatusUpdate + " " + messages.projectObject + " '"
													+ map.getValue() + "' " + messages.objectUpdate,
											MessageType.SUCCESS);
								}
							}
						}
					} catch (UnauthorizedAccessException e) {
						Display.getDefault().asyncExec(new Runnable() {

							@Override
							public void run() {
								CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
										messages.objectPermissionDialogTitle, messages.objectPermissionDialogMsg);
							}
						});
					} catch (Exception e) {
						LOGGER.warn("Unable to De-Activate project model! ", e);
					}
					monitor.worked(70);
					monitor.done();
					return Status.OK_STATUS;
				}
			};
			deactivateProjectJob.setUser(true);
			deactivateProjectJob.setRule(this.jobSchedulingRule);
			deactivateProjectJob.schedule();
		}
	}

	/**
	 * Deactivate admin area.
	 *
	 * @param selections
	 *            the selections
	 */
	@SuppressWarnings("unchecked")
	private void deactivateAdminArea(final IStructuredSelection selections) {
		Object selectionObj = selections.getFirstElement();
		AdminTreeviewer adminTree = XMAdminUtil.getInstance().getAdminTree();
		ITreeSelection structuredSelection = adminTree.getStructuredSelection();
		String name = ((AdministrationArea) selectionObj).getName();
		String confirmDialogTitle = messages.changeStatusConfirmDialogTitle;
		String confirmDialogMsg;
		if (selections.size() == 1) {
			confirmDialogMsg = messages.deActivatechangeStatusConfirmDialogMsg + " \'" + name + "\'" + " ?";
		} else {
			confirmDialogMsg = messages.deActivateMultichangeStatusConfirmDialogMsg + " ?";
		}
		boolean isConfirmed = CustomMessageDialog.openQuestion(adminTree.getControl().getShell(), confirmDialogTitle,
				confirmDialogMsg);
		if (isConfirmed) {
			final List<Object> selectionList = selections.toList();

			Job deactivateAdminArea = new Job("De-Activating..") {

				@SuppressWarnings("rawtypes")
				@Override
				protected IStatus run(final IProgressMonitor monitor) {
					monitor.beginTask("De-Activating..", 100);
					monitor.worked(30);
					try {
						List<AdminAreaRequest> adminAreaRequestList = new ArrayList<>();
						Map<String, String> adminAreaIdMap = new HashMap<>();
						for (int i = 0; i < selectionList.size(); i++) {
							AdminAreaRequest adminAreaRequest = new AdminAreaRequest();
							AdministrationArea adminAreaModel = (AdministrationArea) selectionList.get(i);
							adminAreaRequest.setId(adminAreaModel.getAdministrationAreaId());
							adminAreaRequest.setStatus(com.magna.xmbackend.vo.enums.Status.INACTIVE.name());
							adminAreaRequest.setName(adminAreaModel.getName());
							adminAreaIdMap.put(adminAreaModel.getAdministrationAreaId(), adminAreaModel.getName());
							adminAreaRequestList.add(adminAreaRequest);
						}
						AdminAreaController adminAreaCntr = new AdminAreaController();
						AdminAreaResponse adminAreaResponse = adminAreaCntr
								.multiUpdateAdminAreaStatus(adminAreaRequestList);
						if (adminAreaResponse != null) {

							List<String> statusUpdatationFailedList = adminAreaResponse.getStatusUpdatationFailedList();
							if (!statusUpdatationFailedList.isEmpty()) {
								for (String name : statusUpdatationFailedList) {
									Iterator<Map.Entry<String, String>> iter = adminAreaIdMap.entrySet().iterator();
									while (iter.hasNext()) {
										Map.Entry<String, String> entry = iter.next();
										if (name.equalsIgnoreCase(entry.getValue())) {
											iter.remove();
										}
									}
								}
								Display.getDefault().asyncExec(new Runnable() {
									public void run() {
										CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
												messages.changeStatusConfirmDialogTitle,
												messages.multiStatusUpadteObjectDilaogErrMsg);
									}
								});
							}

							Object firstElement = structuredSelection.getFirstElement();
							if (firstElement instanceof AdministrationAreas) {
								AdministrationAreas adminAreas = (AdministrationAreas) firstElement;
								Map<String, IAdminTreeChild> adminAreasChildren = adminAreas
										.getAdminstrationAreasChildren();

								for (String adminAreaId : adminAreaIdMap.keySet()) {
									((AdministrationArea) adminAreasChildren.get(adminAreaId)).setActive(false);
								}
							}

							Display.getDefault().asyncExec(new Runnable() {
								@Override
								public void run() {
									adminTree.refresh(true);
									adminTree.refershBackReference(new Class[] { UserAdminAreas.class,
											UserProjectAdminAreas.class, ProjectUserAdminAreas.class });
									monitor.worked(1);
								}
							});
							if (adminAreaIdMap.size() > 0) {
								Iterator it = adminAreaIdMap.entrySet().iterator();
								while (it.hasNext()) {
									Map.Entry map = (Map.Entry) it.next();
									XMAdminUtil.getInstance().updateLogFile(
											messages.objectStatusUpdate + " " + messages.administrationAreaObject + " '"
													+ map.getValue() + "' " + messages.objectUpdate,
											MessageType.SUCCESS);
								}
							}
						}

					} catch (UnauthorizedAccessException e) {
						Display.getDefault().asyncExec(new Runnable() {

							@Override
							public void run() {
								CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
										messages.objectPermissionDialogTitle, messages.objectPermissionDialogMsg);
							}
						});
					} catch (Exception e) {
						LOGGER.warn("Unable to De-Activate admin area model! ", e);
					}
					monitor.worked(70);
					monitor.done();
					return Status.OK_STATUS;
				}
			};
			deactivateAdminArea.setUser(true);
			deactivateAdminArea.setRule(this.jobSchedulingRule);
			deactivateAdminArea.schedule();
		}
	}

	/**
	 * Deactivate site.
	 *
	 * @param selections
	 *            the selections
	 */
	@SuppressWarnings("unchecked")
	private void deactivateSite(final IStructuredSelection selections) {
		Object selectionObj = selections.getFirstElement();
		AdminTreeviewer adminTree = XMAdminUtil.getInstance().getAdminTree();
		ITreeSelection structuredSelection = adminTree.getStructuredSelection();
		String name = ((Site) selectionObj).getName();
		String confirmDialogTitle = messages.changeStatusConfirmDialogTitle;
		String confirmDialogMsg;
		if (selections.size() == 1) {
			confirmDialogMsg = messages.deActivatechangeStatusConfirmDialogMsg + " \'" + name + "\'" + " ?";
		} else {
			confirmDialogMsg = messages.deActivateMultichangeStatusConfirmDialogMsg + " ?";
		}
		boolean isConfirmed = CustomMessageDialog.openQuestion(adminTree.getControl().getShell(), confirmDialogTitle,
				confirmDialogMsg);
		if (isConfirmed) {
			final List<Object> selectionList = selections.toList();

			Job deactivateSiteJob = new Job("De-Activating..") {

				@SuppressWarnings("rawtypes")
				@Override
				protected IStatus run(final IProgressMonitor monitor) {
					monitor.beginTask("De-Activating..", 100);
					monitor.worked(30);
					try {
						List<SiteRequest> siteRequestList= new ArrayList<>();
						Map<String, String> siteIdMap = new HashMap<>();
						for (int i = 0; i < selectionList.size(); i++) {
							SiteRequest siteRequest = new SiteRequest();
							Site site = (Site) selectionList.get(i);
							siteRequest.setId(site.getSiteId());
							siteRequest.setStatus(com.magna.xmbackend.vo.enums.Status.INACTIVE);
							siteRequest.setName(site.getName());
							siteRequestList.add(siteRequest);
							siteIdMap.put(site.getSiteId(), site.getName());
						}
							SiteController siteCntr = new SiteController();
							SiteResponse siteResponse = siteCntr.multiUpdateSiteStatus(siteRequestList);
							if (siteResponse != null ) {
								List<String> statusUpdatationFailedList = siteResponse.getStatusUpdatationFailedList();
								if(!statusUpdatationFailedList.isEmpty()) {
									for (String name : statusUpdatationFailedList) {
										Iterator<Map.Entry<String, String>> iter = siteIdMap.entrySet().iterator();
										while (iter.hasNext()) {
											Map.Entry<String, String> entry = iter.next();
											if (name.equalsIgnoreCase(entry.getValue())) {
												iter.remove();
											}
										}
									}
									Display.getDefault().asyncExec(new Runnable() {
										public void run() {
											CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
													messages.changeStatusConfirmDialogTitle,
													messages.multiStatusUpadteObjectDilaogErrMsg);
										}
									});
								}
								Object firstElement = structuredSelection.getFirstElement();
								if(firstElement instanceof Sites) {
									Sites sites = (Sites) firstElement;
									Map<String, IAdminTreeChild> sitesChildren = sites.getSitesChildren();
									for(String siteId : siteIdMap.keySet()) {
										((Site)sitesChildren.get(siteId)).setActive(false);
									}
								}
								Display.getDefault().asyncExec(new Runnable() {
									@Override
									public void run() {
										adminTree.refresh(true);
										monitor.worked(1);
									}
							});
							if (siteIdMap.size() > 0) {
								Iterator it = siteIdMap.entrySet().iterator();
								while (it.hasNext()) {
									Map.Entry map = (Map.Entry) it.next();
									XMAdminUtil.getInstance().updateLogFile(
											messages.objectStatusUpdate + " " + messages.siteObject + " '"
													+ map.getValue() + "' " + messages.objectUpdate,
											MessageType.SUCCESS);
								}
							}
						}
					}
					catch (UnauthorizedAccessException e) {
						Display.getDefault().asyncExec(new Runnable() {

							@Override
							public void run() {
								CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
										messages.objectPermissionDialogTitle, messages.objectPermissionDialogMsg);
							}
						});

						return Status.OK_STATUS;
					} catch (Exception e) {
						LOGGER.warn("Unable to De-Activate site model! ", e);
					}

					monitor.worked(70);
					monitor.done();
					return Status.OK_STATUS;
				}
			};
			deactivateSiteJob.setUser(true);
			deactivateSiteJob.setRule(this.jobSchedulingRule);
			deactivateSiteJob.schedule();
		}
	}
}
