package com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.notification;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.stream.Collectors;

import com.magna.xmbackend.vo.enums.NotificationEventType;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild;

/**
 * The Class ProjectActivateEvt.
 */
public class ProjectActivateEvt extends INotificationEvent {
	
	/** The project activate evt child. */
	private Map<String, IAdminTreeChild> projectActivateEvtChild;
	
	/** The parent. */
	private IAdminTreeChild parent;
	
	
	public ProjectActivateEvt() {
		this.parent = null;
		this.eventType = NotificationEventType.PROJECT_ACTIVATE;
		this.projectActivateEvtChild = new LinkedHashMap<>();
	}
	
	
	/**
	 * Add.
	 *
	 * @param id the id
	 * @param child the child
	 * @return the i admin tree child
	 */
	public IAdminTreeChild add(final String id, final IAdminTreeChild child) {
		child.setParent(this);
		IAdminTreeChild returnVal = this.projectActivateEvtChild.put(id, child);
		sort();
		return returnVal;
	}
	
	/**
	 * Remove all.
	 */
	public void removeAll() {
		this.projectActivateEvtChild.clear();
	}

	/* (non-Javadoc)
	 * @see com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild#getParent()
	 */
	@Override
	public IAdminTreeChild getParent() {
		return this.parent;
	}

	/* (non-Javadoc)
	 * @see com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild#setParent(com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild)
	 */
	@Override
	public void setParent(IAdminTreeChild parent) {
		this.parent = parent;
	}


	/**
	 * Gets the project activate evt child.
	 *
	 * @return the project activate evt child
	 */
	public Map<String, IAdminTreeChild> getProjectActivateEvtChild() {
		return projectActivateEvtChild;
	}


	/**
	 * Gets the project activate evt collection.
	 *
	 * @return the project activate evt collection
	 */
	public Collection<IAdminTreeChild> getProjectActivateEvtCollection() {
		return this.projectActivateEvtChild.values();
	}
	
	/**
	 * Sort.
	 */
	public void sort() {
		final LinkedHashMap<String, IAdminTreeChild> collect = this.projectActivateEvtChild.entrySet().stream().sorted(
				(e1, e2) -> ((ProjectActivateEvtAction) e1.getValue()).getName().compareTo(((ProjectActivateEvtAction) e2.getValue()).getName()))
				.collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e1, LinkedHashMap::new));
		this.projectActivateEvtChild = collect;
	}
}
