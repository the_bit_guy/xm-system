package com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.notification;

import com.magna.xmbackend.vo.enums.NotificationEventType;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild;

/**
 * The Class ProAARelAssignEvt.
 * 
 * @author shashwat.anand
 */
public class ProAARelAssignEvt extends INotificationEvent {
	
	/**
	 * Instantiates a new pro AA rel assign evt.
	 */
	public ProAARelAssignEvt() {
		this.eventType = NotificationEventType.PROJECT_ADMIN_AREA_RELATION_ASSIGN;
	}

	@Override
	public IAdminTreeChild getParent() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setParent(IAdminTreeChild parent) {
		// TODO Auto-generated method stub
		
	}
}
