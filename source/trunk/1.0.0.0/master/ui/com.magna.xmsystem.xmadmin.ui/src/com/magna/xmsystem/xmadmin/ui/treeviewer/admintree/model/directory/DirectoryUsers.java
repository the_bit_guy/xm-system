package com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.directory;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.refs.RelationObj;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.users.User;

public class DirectoryUsers implements IAdminTreeChild {

	/** The parent. */
	private IAdminTreeChild parent;

	/** The directory users children. */
	private Map<String, IAdminTreeChild> directoryUsersChildren;

	/**
	 * Instantiates a new directory user model.
	 *
	 * @param parent
	 *            the parent
	 */
	public DirectoryUsers(final IAdminTreeChild parent) {
		this.parent = parent;
		this.directoryUsersChildren = new LinkedHashMap<>();
	}

	/**
	 * Adds the.
	 *
	 * @param directoryUsersChildrenId
	 *            the directory users children id
	 * @param child
	 *            the child
	 * @return the i admin tree child
	 */
	public IAdminTreeChild add(final String directoryUsersChildrenId, final IAdminTreeChild child) {
		IAdminTreeChild returnVal = this.directoryUsersChildren.put(directoryUsersChildrenId, child);
		if (child instanceof RelationObj && ((RelationObj) child).getRefObject() instanceof User) {
			sort();
		}
		return returnVal;
	}

	/**
	 * Removes the.
	 *
	 * @param directoryUsersChildrenId
	 *            the directory users children id
	 * @return the i admin tree child
	 */
	public IAdminTreeChild remove(final List<String> directoryUsersChildrenId) {
		return this.directoryUsersChildren.remove(directoryUsersChildrenId);
	}

	/**
	 * Gets the directory users children collection.
	 *
	 * @return the directory users children collection
	 */
	public Collection<IAdminTreeChild> getDirectoryUsersChildrenCollection() {
		return this.directoryUsersChildren.values();
	}

	/**
	 * Gets the directory users children children.
	 *
	 * @return the directory users children children
	 */
	public Map<String, IAdminTreeChild> getDirectoryUsersChildrenChildren() {
		return directoryUsersChildren;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild#
	 * getParent()
	 */
	@Override
	public IAdminTreeChild getParent() {
		return this.parent;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild#
	 * setParent(com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.
	 * IAdminTreeChild)
	 */
	@Override
	public void setParent(IAdminTreeChild parent) {
		this.parent = parent;

	}
	
	/**
	 * Sort.
	 */
	public void sort() {
		final LinkedHashMap<String, IAdminTreeChild> collect = this.directoryUsersChildren.entrySet().stream().sorted(
				(e1, e2) -> ((User) e1.getValue()).getName().compareTo(((User) e2.getValue()).getName()))
				.collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e1, LinkedHashMap::new));
		this.directoryUsersChildren = collect;
	}

}
