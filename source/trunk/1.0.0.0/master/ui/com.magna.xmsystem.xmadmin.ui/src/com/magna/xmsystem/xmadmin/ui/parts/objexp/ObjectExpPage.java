package com.magna.xmsystem.xmadmin.ui.parts.objexp;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.e4.core.contexts.ContextInjectionFactory;
import org.eclipse.e4.core.contexts.IEclipseContext;
import org.eclipse.e4.core.services.events.IEventBroker;
import org.eclipse.e4.core.services.nls.Translation;
import org.eclipse.jface.layout.GridDataFactory;
import org.eclipse.jface.viewers.IContentProvider;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.ITreeSelection;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.viewers.TreePath;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.widgets.Tree;
import org.eclipse.swt.widgets.TreeItem;

import com.magna.xmsystem.xmadmin.message.Message;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.AdminTreeviewer;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.AdminTreeDataLoad;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.AdminTreeFactory;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.adminarea.AdminAreaProjectAppFixed;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.adminarea.AdminAreaProjectAppNotFixed;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.adminarea.AdminAreaProjectAppProtected;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.adminarea.AdminAreaProjectApplications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.adminarea.AdminAreaProjectStartApplications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.adminarea.AdminAreaProjects;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.adminarea.AdminAreaStartApplications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.adminarea.AdminAreaUserAppFixed;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.adminarea.AdminAreaUserAppNotFixed;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.adminarea.AdminAreaUserAppProtected;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.adminarea.AdminAreaUserApplications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.adminarea.AdministrationArea;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.adminarea.AdministrationAreas;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.baseapplicationmodel.BaseAppProjectApplications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.baseapplicationmodel.BaseAppStartApplications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.baseapplicationmodel.BaseAppUserApplications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.baseapplicationmodel.BaseApplication;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.baseapplicationmodel.BaseApplications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.projectapplicationmodel.ProjectAppAdminAreaProjects;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.projectapplicationmodel.ProjectAppAdminAreas;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.projectapplicationmodel.ProjectAppUsers;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.projectapplicationmodel.ProjectApplication;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.projectapplicationmodel.ProjectApplications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.startapplicationmodel.StartAppAdminAreas;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.startapplicationmodel.StartAppProjects;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.startapplicationmodel.StartAppUsers;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.startapplicationmodel.StartApplication;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.startapplicationmodel.StartApplications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.userapplicationmodel.UserAppAdminAreas;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.userapplicationmodel.UserAppUsers;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.userapplicationmodel.UserApplication;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.userapplicationmodel.UserApplications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.adminconf.AdminProjectApps;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.adminconf.AdminUserApps;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.adminconf.AdminUsers;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.livemsgconfig.LiveMessage;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.livemsgconfig.LiveMessages;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.notification.NotificationTemplates;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.notification.ProjectActivateEvt;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.notification.ProjectActivateEvtAction;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.notification.ProjectCreateEvt;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.notification.ProjectCreateEvtAction;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.notification.ProjectDeactivateEvt;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.notification.ProjectDeactivateEvtAction;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.notification.ProjectDeleteEvt;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.notification.ProjectDeleteEvtAction;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.notification.UserProjectRelAssignEvt;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.notification.UserProjectRelRemoveEvt;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.roles.Role;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.roles.RoleScopeObjectUsers;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.roles.RoleScopeObjects;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.roles.RoleUsers;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.roles.Roles;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.directory.Directories;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.directory.Directory;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.directory.DirectoryProjectApplications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.directory.DirectoryProjects;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.directory.DirectoryUserApplications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.directory.DirectoryUsers;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.group.Groups;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.group.projectappgroup.ProjectAppGroupProjectApps;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.group.projectappgroup.ProjectApplicationGroup;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.group.projectappgroup.ProjectApplicationGroups;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.group.projectgroup.ProjectGroupModel;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.group.projectgroup.ProjectGroupProjects;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.group.projectgroup.ProjectGroupsModel;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.group.userappgroup.UserAppGroupUserApps;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.group.userappgroup.UserApplicationGroup;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.group.userappgroup.UserApplicationGroups;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.group.usergroup.UserGroupModel;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.group.usergroup.UserGroupUsers;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.group.usergroup.UserGroupsModel;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.projects.Project;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.projects.ProjectAdminAreaProjectAppFixed;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.projects.ProjectAdminAreaProjectAppNotFixed;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.projects.ProjectAdminAreaProjectAppProtected;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.projects.ProjectAdminAreaProjectApplications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.projects.ProjectAdminAreaStartApplications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.projects.ProjectAdminAreas;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.projects.ProjectProjectApplications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.projects.ProjectUserAAProjectAppAllowed;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.projects.ProjectUserAAProjectAppForbidden;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.projects.ProjectUserAAProjectApplications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.projects.ProjectUserAdminAreas;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.projects.ProjectUsers;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.projects.Projects;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.refs.RelationObj;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.sitemodel.Site;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.sitemodel.SiteAdminAreaProjectApplications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.sitemodel.SiteAdminAreaProjectStartApplications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.sitemodel.SiteAdminAreaProjects;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.sitemodel.SiteAdminAreaStartApplications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.sitemodel.SiteAdminAreaUserAppFixed;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.sitemodel.SiteAdminAreaUserAppNotFixed;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.sitemodel.SiteAdminAreaUserAppProtected;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.sitemodel.SiteAdminAreaUserApplications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.sitemodel.SiteAdminProjectAppFixed;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.sitemodel.SiteAdminProjectAppNotFixed;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.sitemodel.SiteAdminProjectAppProtected;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.sitemodel.SiteAdministrations;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.sitemodel.Sites;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.users.User;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.users.UserAAUserAppAllowed;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.users.UserAAUserAppForbidden;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.users.UserAAUserApplications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.users.UserAdminAreas;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.users.UserProjectAAProjectAppAllowed;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.users.UserProjectAAProjectAppForbidden;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.users.UserProjectAAProjectApplications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.users.UserProjectAdminAreas;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.users.UserProjects;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.users.UserStartApplications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.users.Users;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.users.UsersNameAlphabet;
import com.magna.xmsystem.xmadmin.util.CommonConstants;
import com.magna.xmsystem.xmadmin.util.XMAdminUtil;

/**
 * The Class ObjectExpPage.
 * 
 * @author shashwat.anand
 */
public class ObjectExpPage extends Composite {

	/** The eclipse context. */
	@Inject
	private IEclipseContext eclipseContext;
	
	/** The obj exp table viewer. */
	private ObjectExplorer objExpTableViewer;
	
	/** The event broker. */
	@Inject
	private IEventBroker eventBroker;

	/** The search txt. */
	private Text searchTxt;
	
	/** The messages. */
	@Inject
	@Translation
	private Message messages;
	
	/** The child obj count. */
	private int childObjCount;
	
	/**
	 * Instantiates a new object exp page.
	 *
	 * @param parent the parent
	 */
	@Inject
	public ObjectExpPage(final Composite parent) {
		super(parent, SWT.NONE);
		
	}

	/**
	 * Creates the treeviewer.
	 */
	@PostConstruct
	public void createUI(){
		this.setLayout(new GridLayout(1, false));
		GridDataFactory.fillDefaults().grab(true, true).span(SWT.FILL, SWT.FILL).applyTo(this);
		buildSearchBar();
		eclipseContext.set(Composite.class, this);
		objExpTableViewer = ContextInjectionFactory.make(ObjectExplorer.class, eclipseContext);
		GridDataFactory.fillDefaults().grab(true, true).span(SWT.FILL, SWT.FILL).applyTo(objExpTableViewer.getTree());
	}

	/**
	 * Builds the search bar.
	 */
	private void buildSearchBar() {
		this.searchTxt = new Text(this, SWT.BORDER);
		GridDataFactory.fillDefaults().grab(true, false).align(SWT.FILL, SWT.CENTER).applyTo(this.searchTxt);

		this.searchTxt.setMessage("type filter text");
		this.searchTxt.addListener(SWT.Modify, new Listener() {

			@Override
			public void handleEvent(final Event event) {
				final String searchTxt = ((Text) event.widget).getText().trim();
				refreshObjExpTree(searchTxt);
			}
		});
	}

	/**
	 * Select all.
	 */
	public void selectAll() {
		Job job = new Job("Selecting Objects") {
			
			@Override
			protected IStatus run(final IProgressMonitor monitor) {
				final Object input = objExpTableViewer.getInput();
				if (input instanceof List<?>) {
					final List<?> objs = (List<?>) input;
					monitor.beginTask("Selecting Objects", objs.size() + 5);
					Display.getDefault().asyncExec(new Runnable() {
						
						@Override
						public void run() {
							final Tree tree = objExpTableViewer.getTree();
							tree.setRedraw(false);
							final int itemCount = tree.getItemCount();
							final IContentProvider contentProvider = objExpTableViewer.getContentProvider();
							if (contentProvider instanceof ObjExpLazyContentProvider) {
								final ObjExpLazyContentProvider objExpLazyContentProvider = (ObjExpLazyContentProvider) contentProvider;
								for (int index = 0; index < itemCount; index++) {
									objExpLazyContentProvider.updateElement(objs, index);
									TreeItem item = tree.getItem(index);
									tree.showItem(item);
									monitor.worked(1);
								}
							}
							tree.showItem(tree.getItem(0));
							tree.setRedraw(true);
							tree.selectAll();
							eventBroker.send(CommonConstants.EVENT_BROKER.STATUSBAR, itemCount + " " + messages.objectsSelected);
							monitor.worked(5);
							monitor.done();
						}
					});
				}
				return Status.OK_STATUS;
			}
		};
		job.setUser(true);
		job.schedule();
	}
	
	
	/**
	 * Checks if is all enable.
	 *
	 * @return true, if is all enable
	 */
	public boolean isAllEnable(){
		if (this.objExpTableViewer.getStructuredSelection().size() == this.objExpTableViewer.getTree().getItemCount()) {
			return false;
		}
		return true;
	}
	
	/**
	 * Checks if is clear enable.
	 *
	 * @return true, if is clear enable
	 */
	public boolean isClearEnable() {
		if (this.objExpTableViewer.getStructuredSelection().isEmpty()) {
			return false;
		}
		return true;
	}
	
	
	/**
	 * Clear selections.
	 */
	public void clearSelections() {
		this.objExpTableViewer.getTree().deselectAll();
	}
	
	
	/**
	 * Checks if is invert enable.
	 *
	 * @return true, if is invert enable
	 */
	public boolean isInvertEnable() {
		if (this.objExpTableViewer.getStructuredSelection().isEmpty()) {
			return false;
		}
		return true;
	}
	
	/**
	 * Invert selection.
	 */
	public void invertSelection() {
		TreeItem[] items = this.objExpTableViewer.getTree().getItems();
		List<TreeItem> selectionList = Arrays.asList(this.objExpTableViewer.getTree().getSelection());
		for (TreeItem treeItem : items) {
			if (selectionList.contains(treeItem)) {
				this.objExpTableViewer.getTree().deselect(treeItem);
			} else {
				this.objExpTableViewer.getTree().select(treeItem);
			}
		}
	}
	
	/**
	 * Gets the obj exp table viewer.
	 *
	 * @return the obj exp table viewer
	 */
	public ObjectExplorer getObjExpTableViewer() {
		return objExpTableViewer;
	}

	/**
	 * Goto selection.
	 */
	public void gotoSelection() {
		 ITreeSelection selection = this.objExpTableViewer.getStructuredSelection();
		if(selection == null){
			return;
		}
		final AdminTreeviewer adminTree = XMAdminUtil.getInstance().getAdminTree();
		Object selectedObj = selection.getFirstElement();
		TreePath[] selectionPaths = adminTree.getStructuredSelection().getPaths();
		if (selectionPaths != null && selectionPaths.length > 0) {
			adminTree.setExpandedState(selectionPaths[0], true);

			if (selectedObj instanceof User) {
				Users users = AdminTreeFactory.getInstance().getUsers();
				Map<String, IAdminTreeChild> userChildren = users.getUsersChildren();
				char startChar = (((User) selectedObj).getName().toString().toLowerCase()
						.toCharArray()[0]);
				IAdminTreeChild iAdminTreeChild = userChildren.get(String.valueOf(startChar));
				adminTree.setExpandedState(iAdminTreeChild, true);
			}
		}
		adminTree.setSelection(new StructuredSelection(selectedObj), true);
	}

	/**
	 * Checks if is display enable.
	 *
	 * @return true, if is display enable
	 */
	public boolean isDisplayEnable() {
		 ITreeSelection selection = this.objExpTableViewer.getStructuredSelection();
		if (selection == null ||selection.isEmpty() || selection.size() > 1) {
			return false;
		}
		return true;
	}

	public void refreshExplorer() {
		this.searchTxt.setText(CommonConstants.EMPTY_STR);
		//refreshObjExpTree(CommonConstants.EMPTY_STR);
	}
	
	/**
	 * Refresh obj exp tree.
	 *
	 * @param searchTxt the search txt
	 */
	private void refreshObjExpTree(final String searchTxt) {
		this.objExpTableViewer.setSelection(null);
		Object selection = getSelection();
		if (selection != null && selection instanceof IStructuredSelection) {
			Object firstElement = ((IStructuredSelection) selection).getFirstElement();
			if (firstElement == null) {
				return;
			}
			List<IAdminTreeChild> childObjList = new ArrayList<>();

			if (firstElement instanceof Sites) {
				childObjList = new ArrayList<>(((Sites) firstElement).getSitesCollection());
			} else if (firstElement instanceof AdministrationAreas) {
				childObjList = new ArrayList<>(((AdministrationAreas) firstElement).getAdministrationAreasCollection());
			} else if (firstElement instanceof Users) {
				childObjList = new ArrayList<>();
				final Collection<IAdminTreeChild> userCollection = ((Users) firstElement).getUsersCollection();
				for (IAdminTreeChild userNameAlphabet : userCollection) {
					if (userNameAlphabet instanceof UsersNameAlphabet) {
						childObjList.addAll(((UsersNameAlphabet) userNameAlphabet).getUsersCollection());
					}
				}
			} else if (firstElement instanceof UsersNameAlphabet) {
				childObjList = new ArrayList<>(((UsersNameAlphabet) firstElement).getUsersCollection());
			} else if (firstElement instanceof Projects) {
				childObjList = new ArrayList<>(((Projects) firstElement).getProjectsCollection());
			} else if (firstElement instanceof UserApplications) {
				childObjList = new ArrayList<>(((UserApplications) firstElement).getUserAppCollection());
			} else if (firstElement instanceof ProjectApplications) {
				childObjList = new ArrayList<>(((ProjectApplications) firstElement).getProjectAppCollection());
			} else if (firstElement instanceof StartApplications) {
				childObjList = new ArrayList<>(((StartApplications) firstElement).getStartAppCollection());
			} else if (firstElement instanceof BaseApplications) {
				childObjList = new ArrayList<>(((BaseApplications) firstElement).getBaseAppCollection());
			} else if (firstElement instanceof Groups) {
				childObjList = new ArrayList<>(((Groups) firstElement).getGroupsCollection());
			} else if (firstElement instanceof UserGroupsModel) {
				childObjList = new ArrayList<>(((UserGroupsModel) firstElement).getUserGroupsCollection());
			} else if (firstElement instanceof ProjectGroupsModel) {
				childObjList = new ArrayList<>(((ProjectGroupsModel) firstElement).getProjectGroupsCollection());
			} else if (firstElement instanceof UserApplicationGroups) {
				childObjList = new ArrayList<>(((UserApplicationGroups) firstElement).getUserAppGroupsCollection());
			} else if (firstElement instanceof ProjectApplicationGroups) {
				childObjList = new ArrayList<>(
						((ProjectApplicationGroups) firstElement).getProjectAppGroupsCollection());
			} else if (firstElement instanceof Directories) {
				childObjList = new ArrayList<>(((Directories) firstElement).getDirectoriesCollection());
			} else if (firstElement instanceof ProjectAdminAreas) {
				childObjList = new ArrayList<>(((ProjectAdminAreas) firstElement).getProjectAdminAreaCollection());
			} else if (firstElement instanceof ProjectUsers) {
				childObjList = new ArrayList<>(((ProjectUsers) firstElement).getProjectUserCollection());
			} else if (firstElement instanceof ProjectUserAdminAreas) {
				childObjList = new ArrayList<>(
						((ProjectUserAdminAreas) firstElement).getProjectUserAdminAreaChildCollection());
			} else if (firstElement instanceof SiteAdministrations) {
				childObjList = new ArrayList<>(
						((SiteAdministrations) firstElement).getSiteAdministrationAreasCollection());
			} else if (firstElement instanceof SiteAdminAreaProjects) {
				childObjList = new ArrayList<>(((SiteAdminAreaProjects) firstElement).getSiteAdminProjectsCollection());
			} else if (firstElement instanceof AdminAreaProjects) {
				childObjList = new ArrayList<>(((AdminAreaProjects) firstElement).getAdminAreasProjectsCollection());
			} else if (firstElement instanceof AdminAreaProjects) {
				childObjList = new ArrayList<>(((AdminAreaProjects) firstElement).getAdminAreasProjectsCollection());
			} else if (firstElement instanceof Roles) {
				childObjList = new ArrayList<>(((Roles) firstElement).getRolesCollection());
			} else if (firstElement instanceof RoleScopeObjects) {
				childObjList = new ArrayList<>(
						((RoleScopeObjects) firstElement).getRoleScopeObjectsChildren().values());
			} else if (firstElement instanceof UserAdminAreas) {
				childObjList = new ArrayList<>(((UserAdminAreas) firstElement).getUserAdminAreaChildCollection());
			} else if (firstElement instanceof UserProjects) {
				childObjList = new ArrayList<>(((UserProjects) firstElement).getUserProjectsCollection());
			} else if (firstElement instanceof UserProjectAdminAreas) {
				childObjList = new ArrayList<>(
						((UserProjectAdminAreas) firstElement).getUserProjectAdminAreaChildCollection());
			} else if (firstElement instanceof ProjectAppAdminAreas) {
				childObjList = new ArrayList<>(
						((ProjectAppAdminAreas) firstElement).getProjectAppAdminAreaChildrenCollection());
			} else if (firstElement instanceof LiveMessages) {
				childObjList = new ArrayList<>(((LiveMessages) firstElement).getLiveMsgsCollection());
			} else {
				AdminTreeDataLoad adminTreeDataLoad = AdminTreeDataLoad.getInstance();
				if (firstElement instanceof SiteAdminAreaUserApplications
						|| firstElement instanceof SiteAdminAreaUserAppNotFixed
						|| firstElement instanceof SiteAdminAreaUserAppFixed
						|| firstElement instanceof SiteAdminAreaUserAppProtected
						|| firstElement instanceof AdminAreaUserApplications
						|| firstElement instanceof AdminAreaUserAppNotFixed
						|| firstElement instanceof AdminAreaUserAppFixed
						|| firstElement instanceof AdminAreaUserAppProtected) {
					childObjList = adminTreeDataLoad
							.loadSiteAdminAreaUserAppsFromService((IAdminTreeChild) firstElement);
				} else if (firstElement instanceof SiteAdminAreaProjectApplications
						|| firstElement instanceof SiteAdminProjectAppNotFixed
						|| firstElement instanceof SiteAdminProjectAppFixed
						|| firstElement instanceof SiteAdminProjectAppProtected
						|| firstElement instanceof AdminAreaProjectApplications
						|| firstElement instanceof AdminAreaProjectAppNotFixed
						|| firstElement instanceof AdminAreaProjectAppFixed
						|| firstElement instanceof AdminAreaProjectAppProtected
						|| firstElement instanceof ProjectAdminAreaProjectApplications
						|| firstElement instanceof ProjectAdminAreaProjectAppNotFixed
						|| firstElement instanceof ProjectAdminAreaProjectAppFixed
						|| firstElement instanceof ProjectAdminAreaProjectAppProtected) {
					childObjList = new ArrayList<>(
							adminTreeDataLoad.loadSiteAdminAreaProjAppsFromService((IAdminTreeChild) firstElement));
				} else if (firstElement instanceof SiteAdminAreaStartApplications
						|| firstElement instanceof AdminAreaStartApplications) {
					childObjList = new ArrayList<>(
							adminTreeDataLoad.loadSiteAdminAreaStartAppsFromService((IAdminTreeChild) firstElement));
				} else if (firstElement instanceof SiteAdminAreaProjectStartApplications
						|| firstElement instanceof AdminAreaProjectStartApplications
						|| firstElement instanceof ProjectAdminAreaStartApplications) {
					childObjList = new ArrayList<>(adminTreeDataLoad
							.loadSiteAdminAreaProjStartAppsFromService((IAdminTreeChild) firstElement));
				} else if (firstElement instanceof ProjectProjectApplications) {
					childObjList = adminTreeDataLoad
							.loadProjectProjectAppFromService((ProjectProjectApplications) firstElement);
				} else if (firstElement instanceof BaseAppProjectApplications) {
					childObjList = adminTreeDataLoad
							.loadBaseAppProjectAppFromService((BaseAppProjectApplications) firstElement);
				} else if (firstElement instanceof BaseAppUserApplications) {
					childObjList = adminTreeDataLoad
							.loadBaseAppUserAppFromService((BaseAppUserApplications) firstElement);
				} else if (firstElement instanceof BaseAppStartApplications) {
					childObjList = adminTreeDataLoad
							.loadBaseAppStartAppFromService((BaseAppStartApplications) firstElement);
				} else if (firstElement instanceof UserAAUserApplications
						|| firstElement instanceof UserAAUserAppAllowed
						|| firstElement instanceof UserAAUserAppForbidden) {
					childObjList = new ArrayList<>(
							adminTreeDataLoad.loadUserAAUserAppsFromService((IAdminTreeChild) firstElement));
				} else if (firstElement instanceof UserProjectAAProjectApplications
						|| firstElement instanceof UserProjectAAProjectAppAllowed
						|| firstElement instanceof UserProjectAAProjectAppForbidden
						|| firstElement instanceof ProjectUserAAProjectApplications
						|| firstElement instanceof ProjectUserAAProjectAppAllowed
						|| firstElement instanceof ProjectUserAAProjectAppForbidden) {
					childObjList = new ArrayList<>(
							adminTreeDataLoad.loadUserProjectAAProjectAppFromService((IAdminTreeChild) firstElement));
				} else if (firstElement instanceof UserStartApplications) {
					childObjList = new ArrayList<>(
							adminTreeDataLoad.loadUserStartAppsFromService((IAdminTreeChild) firstElement));
				} else if (firstElement instanceof UserAppAdminAreas) {
					childObjList = new ArrayList<>(
							adminTreeDataLoad.loadUserAppAdminAreasFromService((IAdminTreeChild) firstElement));
				} else if (firstElement instanceof UserAppUsers) {
					childObjList = new ArrayList<>(
							adminTreeDataLoad.loadUserAppUsersFromService((IAdminTreeChild) firstElement));
				} else if (firstElement instanceof ProjectAppAdminAreaProjects) {
					childObjList = new ArrayList<>(adminTreeDataLoad
							.loadProjectAppAdminAreaProjectsFromService((IAdminTreeChild) firstElement));
				} else if (firstElement instanceof ProjectAppUsers) {
					childObjList = new ArrayList<>(
							adminTreeDataLoad.loadProjectAppUsersFromService((IAdminTreeChild) firstElement));
				} else if (firstElement instanceof StartAppAdminAreas) {
					childObjList = new ArrayList<>(
							adminTreeDataLoad.loadStartAppAdminAreasFromService((IAdminTreeChild) firstElement));
				} else if (firstElement instanceof StartAppProjects) {
					childObjList = new ArrayList<>(
							adminTreeDataLoad.loadStartAppProjectsFromService((IAdminTreeChild) firstElement));
				} else if (firstElement instanceof StartAppUsers) {
					childObjList = new ArrayList<>(
							adminTreeDataLoad.loadStartAppUsersFromService((IAdminTreeChild) firstElement));
				} else if (firstElement instanceof DirectoryUsers) {
					childObjList = new ArrayList<>(
							adminTreeDataLoad.loadDirectoryUsersFromService((IAdminTreeChild) firstElement));
				} else if (firstElement instanceof DirectoryProjects) {
					childObjList = new ArrayList<>(
							adminTreeDataLoad.loadDirectoryProjectsFromService((IAdminTreeChild) firstElement));
				} else if (firstElement instanceof DirectoryUserApplications) {
					childObjList = new ArrayList<>(
							adminTreeDataLoad.loadDirectoryUserApplicationsFromService((IAdminTreeChild) firstElement));
				} else if (firstElement instanceof DirectoryProjectApplications) {
					childObjList = new ArrayList<>(adminTreeDataLoad
							.loadDirectoryProjectApplicationsFromService((IAdminTreeChild) firstElement));
				} else if (firstElement instanceof RoleUsers) {
					childObjList = new ArrayList<>(
							adminTreeDataLoad.loadRoleUsersFromService((IAdminTreeChild) firstElement));
				} else if (firstElement instanceof RoleScopeObjectUsers) {
					childObjList = new ArrayList<>(
							adminTreeDataLoad.loadRoleScopeObjectUsersFromService((IAdminTreeChild) firstElement));
				} else if (firstElement instanceof AdminUsers) {
					childObjList = new ArrayList<>(
							adminTreeDataLoad.loadAdminMenuUsersFromServices((IAdminTreeChild) firstElement));
				} else if (firstElement instanceof AdminUserApps) {
					childObjList = new ArrayList<>(
							adminTreeDataLoad.loadAdminMenuUserAppFromServices((IAdminTreeChild) firstElement));
				} else if (firstElement instanceof AdminProjectApps) {
					childObjList = new ArrayList<>(
							adminTreeDataLoad.loadAdminMenuProjectAppFromServices((IAdminTreeChild) firstElement));
				} else if (firstElement instanceof UserGroupUsers) {
					childObjList = new ArrayList<>(
							adminTreeDataLoad.loadUserGroupUsersFromService((IAdminTreeChild) firstElement));
				} else if (firstElement instanceof ProjectGroupProjects) {
					childObjList = new ArrayList<>(
							adminTreeDataLoad.loadProjectGroupProjectsFromService((IAdminTreeChild) firstElement));
				} else if (firstElement instanceof UserAppGroupUserApps) {
					childObjList = new ArrayList<>(
							adminTreeDataLoad.loadUserAppGroupUserAppsFromService((IAdminTreeChild) firstElement));
				} else if (firstElement instanceof ProjectAppGroupProjectApps) {
					childObjList = new ArrayList<>(adminTreeDataLoad
							.loadProjectAppGroupProjectAppsFromService((IAdminTreeChild) firstElement));
				} else if(firstElement instanceof ProjectCreateEvt) {
					childObjList = new ArrayList<>(((ProjectCreateEvt) firstElement).getProjectCreateEvtChild().values());
				} else if(firstElement instanceof ProjectDeleteEvt) {
					childObjList = new ArrayList<>(((ProjectDeleteEvt) firstElement).getProjectDeleteEvtCollection());
				} else if(firstElement instanceof ProjectDeactivateEvt) {
					childObjList = new ArrayList<>(((ProjectDeactivateEvt) firstElement).getProjectDeactivateEvtCollection());
				} else if(firstElement instanceof ProjectActivateEvt) {
					childObjList = new ArrayList<>(((ProjectActivateEvt) firstElement).getProjectActivateEvtCollection());
				} else if(firstElement instanceof UserProjectRelAssignEvt) {
					childObjList = new ArrayList<>(((UserProjectRelAssignEvt) firstElement).getUserProRelAssignEvtCollection());
				} else if(firstElement instanceof UserProjectRelRemoveEvt) {
					childObjList = new ArrayList<>(((UserProjectRelRemoveEvt) firstElement).getUserProRelRemoveEvtCollection());
				}else if(firstElement instanceof NotificationTemplates) {
					childObjList = new ArrayList<>(((NotificationTemplates) firstElement).getTemplatesCollection());
				} else {
					return;
				}
			}
			filterDataComposite(searchTxt, childObjList);
		}
	}
	
	/**
	 * Method for Filter data composite.
	 *
	 * @param filterText
	 *            {@link String}
	 */
	private void filterDataComposite(final String filterText, List<IAdminTreeChild> childObjList) {
		List<IAdminTreeChild> childObjTempList = childObjList;
		if (childObjList != null && !childObjList.isEmpty()) {
			IAdminTreeChild iAdminTreeChild = childObjList.get(0);
			if (iAdminTreeChild instanceof RelationObj) {
				iAdminTreeChild = ((RelationObj) iAdminTreeChild).getRefObject();
			}
			if (iAdminTreeChild instanceof Site) {
				childObjTempList = childObjList.stream()
						.filter(s -> ((Site) ((s instanceof RelationObj) ? ((RelationObj) s).getRefObject() : s))
								.getName().toLowerCase().contains(filterText.toLowerCase()))
						.collect(Collectors.toList());
			} else if (iAdminTreeChild instanceof AdministrationArea) {
				childObjTempList = childObjList.stream().filter(
						s -> ((AdministrationArea) ((s instanceof RelationObj) ? ((RelationObj) s).getRefObject() : s))
								.getName().toLowerCase().contains(filterText.toLowerCase()))
						.collect(Collectors.toList());
			} else if (iAdminTreeChild instanceof Project) {
				childObjTempList = childObjList.stream()
						.filter(s -> ((Project) ((s instanceof RelationObj) ? ((RelationObj) s).getRefObject() : s))
								.getName().toLowerCase().contains(filterText.toLowerCase()))
						.collect(Collectors.toList());
			} else if (iAdminTreeChild instanceof User) {
				childObjTempList = childObjList.stream()
						.filter(s -> ((User) ((s instanceof RelationObj) ? ((RelationObj) s).getRefObject() : s))
								.getName().toLowerCase().contains(filterText.toLowerCase()))
						.collect(Collectors.toList());
			} else if (iAdminTreeChild instanceof UserGroupModel) {
				childObjTempList = childObjList.stream()
						.filter(s -> ((UserGroupModel) s).getName().toLowerCase().contains(filterText.toLowerCase()))
						.collect(Collectors.toList());
			} else if (iAdminTreeChild instanceof ProjectGroupModel) {
				childObjTempList = childObjList.stream()
						.filter(s -> ((ProjectGroupModel) s).getName().toLowerCase().contains(filterText.toLowerCase()))
						.collect(Collectors.toList());
			} else if (iAdminTreeChild instanceof UserApplicationGroup) {
				childObjTempList = childObjList.stream().filter(
						s -> ((UserApplicationGroup) s).getName().toLowerCase().contains(filterText.toLowerCase()))
						.collect(Collectors.toList());
			} else if (iAdminTreeChild instanceof ProjectApplicationGroup) {
				childObjTempList = childObjList.stream().filter(
						s -> ((ProjectApplicationGroup) s).getName().toLowerCase().contains(filterText.toLowerCase()))
						.collect(Collectors.toList());
			} else if (iAdminTreeChild instanceof Directory) {
				childObjTempList = childObjList.stream()
						.filter(s -> ((Directory) s).getName().toLowerCase().contains(filterText.toLowerCase()))
						.collect(Collectors.toList());
			} else if (iAdminTreeChild instanceof UserApplication) {
				childObjTempList = childObjList.stream().filter(s -> {
					UserApplication userApplication = (UserApplication) ((s instanceof RelationObj)
							? ((RelationObj) s).getRefObject() : s);
					String name = userApplication.getName();
					return name.toLowerCase().contains(filterText.toLowerCase());
				}).collect(Collectors.toList());
			} else if (iAdminTreeChild instanceof ProjectApplication) {
				childObjTempList = childObjList.stream().filter(s -> {
					ProjectApplication projectApplication = (ProjectApplication) ((s instanceof RelationObj)
							? ((RelationObj) s).getRefObject() : s);
					String name = projectApplication.getName();
					return name.toLowerCase().contains(filterText.toLowerCase());
				}).collect(Collectors.toList());
			} else if (iAdminTreeChild instanceof StartApplication) {
				childObjTempList = childObjList.stream().filter(s -> {
					StartApplication startApplication = (StartApplication) ((s instanceof RelationObj)
							? ((RelationObj) s).getRefObject() : s);
					String name = startApplication.getName();
					return name.toLowerCase().contains(filterText.toLowerCase());
				}).collect(Collectors.toList());
			} else if (iAdminTreeChild instanceof BaseApplication) {
				childObjTempList = childObjList.stream().filter(s -> {
					BaseApplication baseApplication = (BaseApplication) ((s instanceof RelationObj)
							? ((RelationObj) s).getRefObject() : s);
					String name = baseApplication.getName();
					return name.toLowerCase().contains(filterText.toLowerCase());
				}).collect(Collectors.toList());
			} else if (iAdminTreeChild instanceof Role) {
				childObjTempList = childObjList.stream().filter(s -> {
					Role role = (Role) s;
					String name = role.getRoleName();
					return name.toLowerCase().contains(filterText.toLowerCase());
				}).collect(Collectors.toList());
			} else if (iAdminTreeChild instanceof LiveMessage) {
				childObjTempList = childObjList.stream().filter(s -> {
					LiveMessage liveMessage = (LiveMessage) s;
					String name = liveMessage.getName();
					return name.toLowerCase().contains(filterText.toLowerCase());
				}).collect(Collectors.toList());
			} else if(iAdminTreeChild instanceof ProjectCreateEvtAction){
				childObjTempList = childObjList.stream().filter(s -> {
					ProjectCreateEvtAction projectCreateEvtAction = (ProjectCreateEvtAction) s;
					String name = projectCreateEvtAction.getName();
					return name.toLowerCase().contains(filterText.toLowerCase());
				}).collect(Collectors.toList());
			} else if (iAdminTreeChild instanceof ProjectDeleteEvtAction) {
				childObjTempList = childObjList.stream().filter(s -> {
					ProjectDeleteEvtAction projectDeleteEvtAction = (ProjectDeleteEvtAction) s;
					String name = projectDeleteEvtAction.getName();
					return name.toLowerCase().contains(filterText.toLowerCase());
				}).collect(Collectors.toList());
			} else if (iAdminTreeChild instanceof ProjectDeactivateEvtAction) {
				childObjTempList = childObjList.stream().filter(s -> {
					ProjectDeactivateEvtAction projectDeactivateEvtAction = (ProjectDeactivateEvtAction) s;
					String name = projectDeactivateEvtAction.getName();
					return name.toLowerCase().contains(filterText.toLowerCase());
				}).collect(Collectors.toList());
			} else if (iAdminTreeChild instanceof ProjectActivateEvtAction) {
				childObjTempList = childObjList.stream().filter(s -> {
					ProjectActivateEvtAction projectActivateEvtAction = (ProjectActivateEvtAction) s;
					String name = projectActivateEvtAction.getName();
					return name.toLowerCase().contains(filterText.toLowerCase());
				}).collect(Collectors.toList());
			}
		}
		this.objExpTableViewer.getTree().setItemCount(childObjTempList.size());
		this.objExpTableViewer.setInput(childObjTempList);
		childObjCount = childObjTempList.size();
	}
	
	/**
	 * Gets the selection.
	 *
	 * @return the selection
	 */
	private IStructuredSelection getSelection() {
		AdminTreeviewer adminTree = XMAdminUtil.getInstance().getAdminTree();
		return (IStructuredSelection) adminTree.getSelection();
	}

	/**
	 * Gets the child obj count.
	 *
	 * @return the child obj count
	 */
	public int getChildObjCount() {
		return childObjCount;
	}

}
