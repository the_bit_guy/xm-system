package com.magna.xmsystem.xmadmin.ui.parts.dnd;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.e4.core.services.events.IEventBroker;
import org.eclipse.e4.core.services.nls.Translation;
import org.eclipse.jface.util.LocalSelectionTransfer;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.jface.viewers.ViewerDropAdapter;
import org.eclipse.swt.dnd.DropTarget;
import org.eclipse.swt.dnd.DropTargetEvent;
import org.eclipse.swt.dnd.Transfer;
import org.eclipse.swt.dnd.TransferData;
import org.eclipse.swt.widgets.Display;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.client.ResourceAccessException;

import com.magna.xmbackend.entities.AdminAreaProjAppRelTbl;
import com.magna.xmbackend.entities.AdminAreaProjectRelTbl;
import com.magna.xmbackend.entities.AdminAreaStartAppRelTbl;
import com.magna.xmbackend.entities.AdminAreaUserAppRelTbl;
import com.magna.xmbackend.entities.AdminMenuConfigTbl;
import com.magna.xmbackend.entities.DirectoryRefTbl;
import com.magna.xmbackend.entities.ProjectStartAppRelTbl;
import com.magna.xmbackend.entities.RoleAdminAreaRelTbl;
import com.magna.xmbackend.entities.RoleUserRelTbl;
import com.magna.xmbackend.entities.SiteAdminAreaRelTbl;
import com.magna.xmbackend.entities.UserProjAppRelTbl;
import com.magna.xmbackend.entities.UserProjectRelTbl;
import com.magna.xmbackend.entities.UserStartAppRelTbl;
import com.magna.xmbackend.entities.UserUserAppRelTbl;
import com.magna.xmbackend.exception.CannotCreateObjectException;
import com.magna.xmbackend.vo.adminMenu.AdminMenuConfigRequest;
import com.magna.xmbackend.vo.adminMenu.AdminMenuConfigResponse;
import com.magna.xmbackend.vo.directory.DirectoryRelBatchRequest;
import com.magna.xmbackend.vo.directory.DirectoryRelBatchResponse;
import com.magna.xmbackend.vo.directory.DirectoryRelRequest;
import com.magna.xmbackend.vo.enums.AdminMenuConfig;
import com.magna.xmbackend.vo.enums.ApplicationRelationType;
import com.magna.xmbackend.vo.enums.DirectoryObjectType;
import com.magna.xmbackend.vo.enums.NotificationEventType;
import com.magna.xmbackend.vo.enums.UserRelationType;
import com.magna.xmbackend.vo.group.GroupRelBatchRequest;
import com.magna.xmbackend.vo.group.GroupRelBatchResponse;
import com.magna.xmbackend.vo.group.GroupRelRequest;
import com.magna.xmbackend.vo.group.GroupRelResponse;
import com.magna.xmbackend.vo.notification.NotificationConfigResponse;
import com.magna.xmbackend.vo.notification.NotificationRequest;
import com.magna.xmbackend.vo.rel.AdminAreaProjectAppRelBatchRequest;
import com.magna.xmbackend.vo.rel.AdminAreaProjectAppRelBatchResponse;
import com.magna.xmbackend.vo.rel.AdminAreaProjectAppRelRequest;
import com.magna.xmbackend.vo.rel.AdminAreaProjectRelBatchRequest;
import com.magna.xmbackend.vo.rel.AdminAreaProjectRelBatchResponse;
import com.magna.xmbackend.vo.rel.AdminAreaProjectRelRequest;
import com.magna.xmbackend.vo.rel.AdminAreaStartAppRelBatchRequest;
import com.magna.xmbackend.vo.rel.AdminAreaStartAppRelBatchResponse;
import com.magna.xmbackend.vo.rel.AdminAreaStartAppRelRequest;
import com.magna.xmbackend.vo.rel.AdminAreaUserAppRelBatchRequest;
import com.magna.xmbackend.vo.rel.AdminAreaUserAppRelBatchResponse;
import com.magna.xmbackend.vo.rel.AdminAreaUserAppRelRequest;
import com.magna.xmbackend.vo.rel.ProjectStartAppRelBatchRequest;
import com.magna.xmbackend.vo.rel.ProjectStartAppRelBatchResponse;
import com.magna.xmbackend.vo.rel.ProjectStartAppRelRequest;
import com.magna.xmbackend.vo.rel.RoleAdminAreaRelRequest;
import com.magna.xmbackend.vo.rel.RoleAdminAreaRelResponse;
import com.magna.xmbackend.vo.rel.RoleUserRelRequest;
import com.magna.xmbackend.vo.rel.RoleUserRelResponse;
import com.magna.xmbackend.vo.rel.SiteAdminAreaRelBatchRequest;
import com.magna.xmbackend.vo.rel.SiteAdminAreaRelBatchResponse;
import com.magna.xmbackend.vo.rel.SiteAdminAreaRelRequest;
import com.magna.xmbackend.vo.rel.UserProjectAppRelBatchRequest;
import com.magna.xmbackend.vo.rel.UserProjectAppRelBatchResponse;
import com.magna.xmbackend.vo.rel.UserProjectAppRelRequest;
import com.magna.xmbackend.vo.rel.UserProjectRelBatchRequest;
import com.magna.xmbackend.vo.rel.UserProjectRelBatchResponse;
import com.magna.xmbackend.vo.rel.UserProjectRelRequest;
import com.magna.xmbackend.vo.rel.UserStartAppRelBatchRequest;
import com.magna.xmbackend.vo.rel.UserStartAppRelBatchResponse;
import com.magna.xmbackend.vo.rel.UserStartAppRelRequest;
import com.magna.xmbackend.vo.rel.UserUserAppRelBatchRequest;
import com.magna.xmbackend.vo.rel.UserUserAppRelBatchResponse;
import com.magna.xmbackend.vo.rel.UserUserAppRelRequest;
import com.magna.xmsystem.dependencies.customExceptions.UnauthorizedAccessException;
import com.magna.xmsystem.ui.controls.widgets.CustomMessageDialog;
import com.magna.xmsystem.xmadmin.message.Message;
import com.magna.xmsystem.xmadmin.restclient.notify.NotificationController;
import com.magna.xmsystem.xmadmin.restclient.relation.AdminAreaProjectAppRelController;
import com.magna.xmsystem.xmadmin.restclient.relation.AdminAreaProjectRelController;
import com.magna.xmsystem.xmadmin.restclient.relation.AdminAreaStartAppRelController;
import com.magna.xmsystem.xmadmin.restclient.relation.AdminAreaUserAppRelController;
import com.magna.xmsystem.xmadmin.restclient.relation.AdminMenuConfigRelController;
import com.magna.xmsystem.xmadmin.restclient.relation.DirectoryRelController;
import com.magna.xmsystem.xmadmin.restclient.relation.GroupRelController;
import com.magna.xmsystem.xmadmin.restclient.relation.ProjectStartAppRelController;
import com.magna.xmsystem.xmadmin.restclient.relation.RoleAdminAreaRelController;
import com.magna.xmsystem.xmadmin.restclient.relation.RoleUserRelController;
import com.magna.xmsystem.xmadmin.restclient.relation.SiteAdminAreaRelController;
import com.magna.xmsystem.xmadmin.restclient.relation.UserProjectAppRelController;
import com.magna.xmsystem.xmadmin.restclient.relation.UserProjectRelController;
import com.magna.xmsystem.xmadmin.restclient.relation.UserStartAppRelController;
import com.magna.xmsystem.xmadmin.restclient.relation.UserUserAppRelController;
import com.magna.xmsystem.xmadmin.ui.parts.dndperspective.model.AAWarpperObj;
import com.magna.xmsystem.xmadmin.ui.parts.dndperspective.model.ProjectWrapperObj;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.AdminTreeExpansion;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.AdminTreeviewer;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.AdminTreeDataLoad;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.AdminTreeFactory;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.adminarea.AdminAreaProjectAppFixed;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.adminarea.AdminAreaProjectAppNotFixed;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.adminarea.AdminAreaProjectAppProtected;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.adminarea.AdminAreaProjectApplications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.adminarea.AdminAreaProjectChild;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.adminarea.AdminAreaProjectStartApplications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.adminarea.AdminAreaProjects;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.adminarea.AdminAreaStartApplications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.adminarea.AdminAreaUserAppFixed;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.adminarea.AdminAreaUserAppNotFixed;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.adminarea.AdminAreaUserAppProtected;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.adminarea.AdminAreaUserApplications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.adminarea.AdministrationArea;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.projectapplicationmodel.ProjectAppAdminAreas;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.projectapplicationmodel.ProjectApplication;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.startapplicationmodel.StartApplication;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.userapplicationmodel.UserApplication;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.adminconf.AdminMenu;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.adminconf.AdminProjectApps;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.adminconf.AdminUserApps;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.adminconf.AdminUsers;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.notification.NotificationEvtActions;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.notification.NotificationTemplate;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.notification.ProjectActivateEvtAction;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.notification.ProjectCreateEvtAction;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.notification.ProjectDeactivateEvtAction;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.notification.ProjectDeleteEvtAction;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.notification.UserProjectRelAssignEvtAction;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.notification.UserProjectRelRemoveEvtAction;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.roles.Role;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.roles.RoleScopeObjectChild;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.roles.RoleScopeObjectUsers;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.roles.RoleScopeObjects;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.roles.RoleUsers;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.directory.Directory;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.directory.DirectoryApplications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.directory.DirectoryProjectApplications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.directory.DirectoryProjects;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.directory.DirectoryUserApplications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.directory.DirectoryUsers;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.group.projectappgroup.ProjectAppGroupProjectApps;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.group.projectappgroup.ProjectApplicationGroup;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.group.projectgroup.ProjectGroupModel;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.group.projectgroup.ProjectGroupProjects;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.group.userappgroup.UserAppGroupUserApps;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.group.userappgroup.UserApplicationGroup;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.group.usergroup.UserGroupModel;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.group.usergroup.UserGroupUsers;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.projects.Project;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.projects.ProjectAdminAreaChild;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.projects.ProjectAdminAreaProjectAppFixed;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.projects.ProjectAdminAreaProjectAppNotFixed;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.projects.ProjectAdminAreaProjectAppProtected;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.projects.ProjectAdminAreaProjectApplications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.projects.ProjectAdminAreas;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.projects.ProjectUserAAProjectAppAllowed;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.projects.ProjectUserAAProjectAppForbidden;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.projects.ProjectUserAAProjectApplications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.projects.ProjectUserAdminAreaChild;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.projects.ProjectUserChild;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.projects.ProjectUsers;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.refs.RelationObj;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.sitemodel.Site;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.sitemodel.SiteAdminAreaProjectApplications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.sitemodel.SiteAdminAreaProjectChild;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.sitemodel.SiteAdminAreaProjectStartApplications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.sitemodel.SiteAdminAreaProjects;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.sitemodel.SiteAdminAreaStartApplications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.sitemodel.SiteAdminAreaUserAppFixed;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.sitemodel.SiteAdminAreaUserAppNotFixed;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.sitemodel.SiteAdminAreaUserAppProtected;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.sitemodel.SiteAdminAreaUserApplications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.sitemodel.SiteAdminProjectAppFixed;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.sitemodel.SiteAdminProjectAppNotFixed;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.sitemodel.SiteAdminProjectAppProtected;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.sitemodel.SiteAdministrationChild;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.sitemodel.SiteAdministrations;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.users.User;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.users.UserAAUserAppAllowed;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.users.UserAAUserAppForbidden;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.users.UserAAUserApplications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.users.UserAdminAreaChild;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.users.UserAdminAreas;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.users.UserProjectAAProjectAppAllowed;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.users.UserProjectAAProjectAppForbidden;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.users.UserProjectAAProjectApplications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.users.UserProjectAdminAreaChild;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.users.UserProjectAdminAreas;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.users.UserProjectChild;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.users.UserProjects;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.users.UserStartApplications;
import com.magna.xmsystem.xmadmin.util.CommonConstants;
import com.magna.xmsystem.xmadmin.util.MessageType;
import com.magna.xmsystem.xmadmin.util.XMAdminUtil;

/**
 * The listener interface for receiving nodeDrop events. The class that is
 * interested in processing a nodeDrop event implements this interface, and the
 * object created with that class is registered with a component using the
 * component's <code>addNodeDropListener<code> method. When the nodeDrop event
 * occurs, that object's appropriate method is invoked.
 *
 * @see NodeDropEvent
 */
public class NodeDropListener extends ViewerDropAdapter {

	/** The Constant LOGGER. */
	private static final Logger LOGGER = LoggerFactory.getLogger(NodeDropListener.class);

	/** Member variable 'viewer' for {@link TreeViewer}. */
	private TreeViewer viewer;

	/** Member variable 'model new' for {@link AdminTreeFactory}. */
	public AdminTreeFactory modelNew;

	/** Member variable 'target object' for {@link Object}. */
	private Object targetObject;

	/** Member variable 'operation' for {@link Int}. */
	private int operation;

	/** Member variable 'messages' for {@link Message}. */
	@Inject
	@Translation
	transient private Message messages;

	/** The event broker. */
	@Inject
	private IEventBroker eventBroker;

	/**
	 * Constructor for NodeDropListener Class.
	 *
	 * @param treeViewer
	 *            {@link TreeViewer}
	 */
	@Inject
	public NodeDropListener(final TreeViewer treeViewer) {
		super(treeViewer);
		this.viewer = treeViewer;
	}

	/**
	 * Gets the operation.
	 *
	 * @return the operation
	 */
	public int getOperation() {
		return operation;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.jface.viewers.ViewerDropAdapter#drop(org.eclipse.swt.dnd.
	 * DropTargetEvent)
	 */
	@Override
	public void drop(DropTargetEvent event) {
		if (event.data instanceof IStructuredSelection) {
			StructuredSelection selections = (StructuredSelection) event.data;
			Object[] selectionArray = selections.toArray();
			List<Object> selectionElementList = new ArrayList<>();
			for (Object selection : selectionArray) {
				Object firstElement = ((StructuredSelection) selection).getFirstElement();
				// boolean flag = true;
				if (firstElement instanceof UserGroupModel) {
					UserGroupModel userGroupModel = (UserGroupModel) firstElement;
					Map<String, IAdminTreeChild> userGroupChild = userGroupModel.getUserGroupChildren();
					IAdminTreeChild iAdminTreeChild = userGroupChild.get(UserGroupUsers.class.getSimpleName());
					List<IAdminTreeChild> userGroupUsersChild = AdminTreeDataLoad.getInstance()
							.loadUserGroupUsersFromService(iAdminTreeChild);
					for (IAdminTreeChild iAdminTreeChild2 : userGroupUsersChild) {
						RelationObj relObj = (RelationObj) iAdminTreeChild2;
						User user = (User) relObj.getRefObject();
						selectionElementList.add(user);
					}
				} else if (firstElement instanceof ProjectGroupModel) {
					ProjectGroupModel projectGroupModel = (ProjectGroupModel) firstElement;
					Map<String, IAdminTreeChild> projectGroupChild = projectGroupModel.getProjectGroupChildren();
					IAdminTreeChild iAdminTreeChild = projectGroupChild.get(ProjectGroupProjects.class.getSimpleName());
					List<IAdminTreeChild> projectGroupProjectsChild = AdminTreeDataLoad.getInstance()
							.loadProjectGroupProjectsFromService(iAdminTreeChild);
					for (IAdminTreeChild iAdminTreeChild2 : projectGroupProjectsChild) {
						RelationObj relObj = (RelationObj) iAdminTreeChild2;
						Project project = (Project) relObj.getRefObject();
						selectionElementList.add(project);
					}
				} else if (firstElement instanceof UserApplicationGroup) {
					UserApplicationGroup userApplicationGroup = (UserApplicationGroup) firstElement;
					Map<String, IAdminTreeChild> userAppGroupChild = userApplicationGroup.getUserAppGroupChildren();
					IAdminTreeChild iAdminTreeChild = userAppGroupChild.get(UserAppGroupUserApps.class.getSimpleName());
					List<IAdminTreeChild> userAppGroupUserAppsChild = AdminTreeDataLoad.getInstance()
							.loadUserAppGroupUserAppsFromService(iAdminTreeChild);
					for (IAdminTreeChild iAdminTreeChild2 : userAppGroupUserAppsChild) {
						RelationObj relObj = (RelationObj) iAdminTreeChild2;
						UserApplication userApp = (UserApplication) relObj.getRefObject();
						selectionElementList.add(userApp);
					}
				} else if (firstElement instanceof ProjectApplicationGroup) {
					ProjectApplicationGroup projectApplicationGroup = (ProjectApplicationGroup) firstElement;
					Map<String, IAdminTreeChild> projectAppGroupChild = projectApplicationGroup
							.getProjectAppGroupChildren();
					IAdminTreeChild iAdminTreeChild = projectAppGroupChild
							.get(ProjectAppGroupProjectApps.class.getSimpleName());
					List<IAdminTreeChild> projecAppGroupProjectAppsChild = AdminTreeDataLoad.getInstance()
							.loadProjectAppGroupProjectAppsFromService(iAdminTreeChild);
					for (IAdminTreeChild iAdminTreeChild2 : projecAppGroupProjectAppsChild) {
						RelationObj relObj = (RelationObj) iAdminTreeChild2;
						ProjectApplication projectApp = (ProjectApplication) relObj.getRefObject();
						selectionElementList.add(projectApp);
					}
				} else if (firstElement instanceof AAWarpperObj) {
					if (((AAWarpperObj) firstElement).getRelationObj() == null) {
						AdministrationArea adminArea = ((AAWarpperObj) firstElement).getAdminArea();
						selectionElementList.add(adminArea);
					}
				} else if (firstElement instanceof ProjectWrapperObj) {
					if (((ProjectWrapperObj) firstElement).getRelationObj() == null) {
						Project project = ((ProjectWrapperObj) firstElement).getProject();
						selectionElementList.add(project);
					}
				} else if (firstElement instanceof RelationObj) {
					boolean flag = validateRelationObj(targetObject, firstElement);
					if (flag) {
						RelationObj relObj = (RelationObj) firstElement;
						firstElement = relObj.getRefObject();
					}
					selectionElementList.add(firstElement);
				} else {
					selectionElementList.add(firstElement);
				}
			}
			if (selectionElementList.isEmpty()) {
				CustomMessageDialog.openError(viewer.getControl().getShell(), messages.errorDialogTitile,
						messages.dndEmptySelectionMsg);
				XMAdminUtil.getInstance().updateLogFile(messages.dndEmptySelectionMsg, MessageType.FAILURE);
			}
			createRelations(selectionElementList.toArray(), targetObject, false);
		}
		/*
		 * else if (event.data instanceof IStructuredSelection) { Object
		 * firstElement = ((IStructuredSelection) event.data).getFirstElement();
		 * if (firstElement instanceof TreeSelection) { final Object droppedObj
		 * = ((TreeSelection) firstElement).getFirstElement();
		 * //dropHandler(droppedObj); } else if (firstElement instanceof
		 * IStructuredSelection) { final Object droppedObj =
		 * ((IStructuredSelection) firstElement).getFirstElement();
		 * //dropHandler(droppedObj); } }
		 */
	}

	/**
	 * Method for Creates the relations.
	 *
	 * @param draggedData
	 *            {@link Object[]}
	 * @param targetNode
	 *            {@link Object}
	 * @param isCopyPast
	 */
	public void createRelations(final Object[] draggedData, final Object targetNode, final boolean isCopyPast) {
		if (draggedData != null && draggedData.length > 0) {
			if (draggedData[0] instanceof AdministrationArea) {
				AdministrationArea[] adminAreaEventData = Arrays.copyOf(draggedData, draggedData.length,
						AdministrationArea[].class);
				if (targetNode instanceof SiteAdministrations || targetNode instanceof Site) {
					adminAreaToSiteAdminAreaDNDRel(adminAreaEventData, (IAdminTreeChild) targetNode, isCopyPast);
				} else if (targetNode instanceof Role || targetNode instanceof RoleScopeObjects) {
					adminAreaToRoleDNDRel(adminAreaEventData, (IAdminTreeChild) targetNode, isCopyPast);
				} else if (targetNode instanceof ProjectAdminAreas || targetNode instanceof Project) {
					adminAreaToProjectAdminAreaDNDRel(adminAreaEventData, (IAdminTreeChild) targetNode, isCopyPast);
				}
			} else if (draggedData[0] instanceof Project) {
				Project[] projectEventData = Arrays.copyOf(draggedData, draggedData.length, Project[].class);
				if (targetNode instanceof SiteAdminAreaProjects || targetNode instanceof SiteAdministrationChild
						|| targetNode instanceof AdminAreaProjects || targetNode instanceof AdministrationArea) {
					projectToSiteAdminAreaDNDRel(projectEventData, (IAdminTreeChild) targetNode, isCopyPast);
				} else if (targetNode instanceof User || targetNode instanceof UserProjects) {
					projectToUserDNDRel(projectEventData, (IAdminTreeChild) targetNode, isCopyPast);
				} else if (targetNode instanceof Directory || targetNode instanceof DirectoryProjects) {
					objectsToDirectoryDNDRel(projectEventData, (IAdminTreeChild) targetNode, isCopyPast);
				} else if (targetNode instanceof ProjectGroupModel || targetNode instanceof ProjectGroupProjects) {
					projectToProjectGroupDNDRel(projectEventData, (IAdminTreeChild) targetNode, isCopyPast);
				}
			} else if (draggedData[0] instanceof UserApplication
					&& UserApplication.class.getSimpleName().equals(draggedData[0].getClass().getSimpleName())) {
				UserApplication[] userAppEventData = Arrays.copyOf(draggedData, draggedData.length,
						UserApplication[].class);
				if (targetNode instanceof SiteAdminAreaUserApplications
						|| targetNode instanceof SiteAdminAreaUserAppNotFixed
						|| targetNode instanceof SiteAdminAreaUserAppFixed
						|| targetNode instanceof SiteAdminAreaUserAppProtected
						|| targetNode instanceof SiteAdministrationChild
						|| targetNode instanceof AdminAreaUserApplications
						|| targetNode instanceof AdminAreaUserAppFixed || targetNode instanceof AdminAreaUserAppNotFixed
						|| targetNode instanceof AdminAreaUserAppProtected
						|| targetNode instanceof AdministrationArea) {
					userAppToSiteAdminAreaDNDRel(userAppEventData, (IAdminTreeChild) targetNode, isCopyPast);
				} else if (targetNode instanceof UserAAUserApplications || targetNode instanceof UserAAUserAppAllowed
						|| targetNode instanceof UserAAUserAppForbidden || targetNode instanceof UserAdminAreaChild) {
					userAppToUserAdminAreaDNDRel(userAppEventData, (IAdminTreeChild) targetNode, isCopyPast);
				} else if (targetNode instanceof Directory || targetNode instanceof DirectoryApplications
						|| targetNode instanceof DirectoryUserApplications) {
					objectsToDirectoryDNDRel(userAppEventData, (IAdminTreeChild) targetNode, isCopyPast);
				} else if (targetNode instanceof AdminMenu || targetNode instanceof AdminUserApps) {
					objectsToAdminMenuDNDRel(userAppEventData, (IAdminTreeChild) targetNode, isCopyPast);
				} else if (targetNode instanceof UserApplicationGroup || targetNode instanceof UserAppGroupUserApps) {
					userAppToUserAppGroupDNDRel(userAppEventData, (IAdminTreeChild) targetNode, isCopyPast);
				}
			} else if (draggedData[0] instanceof StartApplication) {
				StartApplication[] startAppEventData = Arrays.copyOf(draggedData, draggedData.length,
						StartApplication[].class);
				if (targetNode instanceof SiteAdministrationChild
						|| targetNode instanceof SiteAdminAreaStartApplications
						|| targetNode instanceof AdministrationArea
						|| targetNode instanceof AdminAreaStartApplications) {
					startAppToSiteAdminAreaDNDRel(startAppEventData, (IAdminTreeChild) targetNode, isCopyPast);
				} else if (targetNode instanceof SiteAdminAreaProjectChild
						|| targetNode instanceof SiteAdminAreaProjectStartApplications
						|| targetNode instanceof AdminAreaProjectChild
						|| targetNode instanceof AdminAreaProjectStartApplications) {
					startAppToSiteAdminAreaProjectDNDRel(startAppEventData, (IAdminTreeChild) targetNode, isCopyPast);
				} else if (targetNode instanceof User || targetNode instanceof UserStartApplications) {
					startAppToUserDNDRel(startAppEventData, (IAdminTreeChild) targetNode, isCopyPast);
				}
			} else if (draggedData[0] instanceof ProjectApplication
					&& ProjectApplication.class.getSimpleName().equals(draggedData[0].getClass().getSimpleName())) {
				ProjectApplication[] projectAppEventData = Arrays.copyOf(draggedData, draggedData.length,
						ProjectApplication[].class);
				if (targetNode instanceof SiteAdminAreaProjectApplications
						|| targetNode instanceof SiteAdminProjectAppNotFixed
						|| targetNode instanceof SiteAdminProjectAppFixed
						|| targetNode instanceof SiteAdminProjectAppProtected
						|| targetNode instanceof SiteAdminAreaProjectChild
						|| targetNode instanceof AdminAreaProjectApplications
						|| targetNode instanceof AdminAreaProjectAppNotFixed
						|| targetNode instanceof AdminAreaProjectAppFixed
						|| targetNode instanceof AdminAreaProjectAppProtected
						|| targetNode instanceof AdminAreaProjectChild
						|| targetNode instanceof ProjectAdminAreaProjectApplications
						|| targetNode instanceof ProjectAdminAreaProjectAppFixed
						|| targetNode instanceof ProjectAdminAreaProjectAppNotFixed
						|| targetNode instanceof ProjectAdminAreaProjectAppProtected
						|| targetNode instanceof ProjectAdminAreaChild) {
					projectAppToSiteAdminAreaProjectDNDRel(projectAppEventData, (IAdminTreeChild) targetNode,
							isCopyPast);
				} else if (targetNode instanceof UserProjectAAProjectApplications
						|| targetNode instanceof UserProjectAAProjectAppAllowed
						|| targetNode instanceof UserProjectAAProjectAppForbidden
						|| targetNode instanceof UserProjectAdminAreaChild
						|| targetNode instanceof ProjectUserAAProjectApplications
						|| targetNode instanceof ProjectUserAAProjectAppAllowed
						|| targetNode instanceof ProjectUserAAProjectAppForbidden
						|| targetNode instanceof ProjectUserAdminAreaChild) {
					projectAppToUserProjectAdminAreaDNDRel(projectAppEventData, (IAdminTreeChild) targetNode,
							isCopyPast);
				} else if (targetNode instanceof Directory || targetNode instanceof DirectoryApplications
						|| targetNode instanceof DirectoryProjectApplications) {
					objectsToDirectoryDNDRel(projectAppEventData, (IAdminTreeChild) targetNode, isCopyPast);
				} else if (targetNode instanceof AdminMenu || targetNode instanceof AdminProjectApps) {
					objectsToAdminMenuDNDRel(projectAppEventData, (IAdminTreeChild) targetNode, isCopyPast);
				} else if (targetNode instanceof ProjectApplicationGroup
						|| targetNode instanceof ProjectAppGroupProjectApps) {
					projectAppToProjectAppGroupDNDRel(projectAppEventData, (IAdminTreeChild) targetNode, isCopyPast);
				}
			} else if (draggedData[0] instanceof User) {
				User[] userEventData = Arrays.copyOf(draggedData, draggedData.length, User[].class);
				if (targetNode instanceof Project || targetNode instanceof ProjectUsers) {
					userToProjectDNDRel(userEventData, (IAdminTreeChild) targetNode, isCopyPast);
				} else if (targetNode instanceof Directory || targetNode instanceof DirectoryUsers) {
					objectsToDirectoryDNDRel(userEventData, (IAdminTreeChild) targetNode, isCopyPast);
				} else if (targetNode instanceof Role || targetNode instanceof RoleUsers
						|| targetNode instanceof RoleScopeObjectUsers || targetNode instanceof RoleScopeObjectChild) {
					userToRoleDNDRel(userEventData, (IAdminTreeChild) targetNode, isCopyPast);
				} else if (targetNode instanceof AdminUsers || targetNode instanceof AdminMenu) {
					objectsToAdminMenuDNDRel(userEventData, (IAdminTreeChild) targetNode, isCopyPast);
				} else if (targetNode instanceof UserGroupModel || targetNode instanceof UserGroupUsers) {
					userToUserGroupDNDRel(userEventData, (IAdminTreeChild) targetNode, isCopyPast);
				}
			} else if(draggedData[0] instanceof NotificationTemplate){
				NotificationTemplate[] templateEventData = Arrays.copyOf(draggedData, draggedData.length, NotificationTemplate[].class);
				if (targetNode instanceof ProjectCreateEvtAction) {
					templateToProjectCreateEvtDNDRel(templateEventData, (IAdminTreeChild) targetNode, isCopyPast);
				} else if(targetNode instanceof ProjectDeleteEvtAction){
					templateToProjectDeleteEvtDNDRel(templateEventData, (IAdminTreeChild) targetNode, isCopyPast);
				} else if(targetNode instanceof ProjectDeactivateEvtAction){
					templateToProjectDeactivateEvtDNDRel(templateEventData, (IAdminTreeChild) targetNode, isCopyPast);
				} else if(targetNode instanceof ProjectActivateEvtAction){
					templateToProjectActivateEvtDNDRel(templateEventData, (IAdminTreeChild) targetNode, isCopyPast);
				} else if(targetNode instanceof UserProjectRelAssignEvtAction){
					templateToUserProRelAssignEvtDNDRel(templateEventData, (IAdminTreeChild) targetNode, isCopyPast);
				} else if(targetNode instanceof UserProjectRelRemoveEvtAction){
					templateToUserProRelRemoveEvtDNDRel(templateEventData, (IAdminTreeChild) targetNode, isCopyPast);
				}
			}
			else if (draggedData[0] instanceof RelationObj) {
				RelationObj[] relationObjEventData = Arrays.copyOf(draggedData, draggedData.length,
						RelationObj[].class);
				if (relationObjEventData != null && relationObjEventData.length > 0) {
					IAdminTreeChild containerObj = relationObjEventData[0].getContainerObj();
					IAdminTreeChild refObject = relationObjEventData[0].getRefObject();

					IAdminTreeChild containerParent = containerObj.getParent().getParent();
					IAdminTreeChild targetParent = ((IAdminTreeChild) targetNode).getParent();

					if ((targetNode instanceof SiteAdminAreaUserAppNotFixed
							|| targetNode instanceof SiteAdminAreaUserAppFixed
							|| targetNode instanceof SiteAdminAreaUserAppProtected
							|| targetNode instanceof AdminAreaUserAppFixed
							|| targetNode instanceof AdminAreaUserAppNotFixed
							|| targetNode instanceof AdminAreaUserAppProtected)
							&& !(containerObj instanceof SiteAdminAreaUserApplications
									|| containerObj instanceof AdminAreaUserApplications)
							&& refObject instanceof UserApplication
							&& !containerObj.getClass().equals(targetNode.getClass())) {
						if (containerParent.equals(targetParent)) {
							updateSiteAdminAreaUserAppStateDNDRel(relationObjEventData, (IAdminTreeChild) targetNode);
						}
					} else if ((targetNode instanceof SiteAdminProjectAppNotFixed
							|| targetNode instanceof SiteAdminProjectAppFixed
							|| targetNode instanceof SiteAdminProjectAppProtected
							|| targetNode instanceof AdminAreaProjectAppNotFixed
							|| targetNode instanceof AdminAreaProjectAppFixed
							|| targetNode instanceof AdminAreaProjectAppProtected
							|| targetNode instanceof ProjectAdminAreaProjectAppFixed
							|| targetNode instanceof ProjectAdminAreaProjectAppNotFixed
							|| targetNode instanceof ProjectAdminAreaProjectAppProtected)
							&& !(containerObj instanceof SiteAdminAreaProjectApplications
									|| containerObj instanceof AdminAreaProjectApplications
									|| containerObj instanceof ProjectAdminAreaProjectApplications)
							&& refObject instanceof ProjectApplication
							&& !containerObj.getClass().equals(targetNode.getClass())) {
						if (containerParent.equals(targetParent)) {
							updateSiteAdminAreaProjectAppStateDNDRel(relationObjEventData,
									(IAdminTreeChild) targetNode);
						}
					} else if ((targetNode instanceof UserAAUserAppAllowed
							|| targetNode instanceof UserAAUserAppForbidden)
							&& !(containerObj instanceof UserAAUserApplications) && refObject instanceof UserApplication
							&& !containerObj.getClass().equals(targetNode.getClass())) {
						if (containerParent.equals(targetParent)) {
							updateUserAAUserAppStateDNDRel(relationObjEventData, (IAdminTreeChild) targetNode);
						}
					} else if ((targetNode instanceof UserProjectAAProjectAppAllowed
							|| targetNode instanceof UserProjectAAProjectAppForbidden
							|| targetNode instanceof ProjectUserAAProjectAppAllowed
							|| targetNode instanceof ProjectUserAAProjectAppForbidden)
							&& !(containerObj instanceof UserProjectAAProjectApplications
									|| containerObj instanceof ProjectUserAAProjectApplications)
							&& refObject instanceof ProjectApplication
							&& !containerObj.getClass().equals(targetNode.getClass())) {
						if (containerParent.equals(targetParent)) {
							updateUserProjectAAProjectAppStateDNDRel(relationObjEventData,
									(IAdminTreeChild) targetNode);
						}
					}
				}
			}
		}
	}

	/**
	 * Template to user pro rel remove evt DND rel.
	 *
	 * @param templateEventData the template event data
	 * @param targetObj the target obj
	 * @param isCopyPast the is copy past
	 */
	private void templateToUserProRelRemoveEvtDNDRel(NotificationTemplate[] templateEventData,
			IAdminTreeChild targetObj, boolean isCopyPast) {

		if (targetObj != null && templateEventData != null && templateEventData.length > 0) {
			if (targetObj instanceof UserProjectRelRemoveEvtAction) {
				boolean isConfirmed = CustomMessageDialog.openConfirm(Display.getDefault().getActiveShell(),
						messages.dndTemplateDialogTitle, messages.dndTemplateConfirmMsg);
				if (isConfirmed && ValidateNotificationTemplate.validateTemplate(templateEventData[0], targetObj)) {
					Job job = new Job("Creating Relations...") {

						@Override
						protected IStatus run(IProgressMonitor monitor) {
							monitor.beginTask("Creating Relations..", 100);
							monitor.worked(30);
							try {
								UserProjectRelRemoveEvtAction userProRelRemoveEvtAction = (UserProjectRelRemoveEvtAction) targetObj;
								String configId = AdminTreeFactory.getInstance().getNotifications().getUserProjectRelRemoveEvt().getId();
								NotificationRequest request = getRequestObject(userProRelRemoveEvtAction,configId);
								request.setEmailTemplateId(templateEventData[0].getTemplateId());
								NotificationController notificationController = new NotificationController();
								NotificationConfigResponse response = notificationController
										.updateNotification(request);
								if (response != null) {
									userProRelRemoveEvtAction.setTemplate(templateEventData[0]);
									Display.getDefault().asyncExec(new Runnable() {
										@Override
										public void run() {
											XMAdminUtil.getInstance().getAdminTree().refresh(true);
											if (isCopyPast) {
												XMAdminUtil.getInstance().getAdminTree().setSelection(
														new StructuredSelection(userProRelRemoveEvtAction), true);
											}
											XMAdminUtil.getInstance().updateLogFile(messages.templateObject + " '"
													+ templateEventData[0].getName() + "' " + messages.assignedLbl + " "
													+ NotificationEventType.USER_PROJECT_RELATION_REMOVE.name() + " "
													+ messages.actionLbl + " '" + userProRelRemoveEvtAction.getName()
													+ "' " + messages.successfullyLbl, MessageType.SUCCESS);
										}
									});
								}

							} catch (UnauthorizedAccessException e) {
								Display.getDefault().asyncExec(new Runnable() {
									@Override
									public void run() {
										CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
												messages.unauthorizedDialogTitle, messages.unauthorizedDialogMsg);
									}
								});
							} catch (Exception e) {
								if (e instanceof ResourceAccessException) {
									Display.getDefault().asyncExec(new Runnable() {
										@Override
										public void run() {
											CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
													messages.errorDialogTitile, messages.serverNotReachable);
										}
									});
								}
								LOGGER.error(e.getMessage());
							}
							monitor.worked(70);
							return Status.OK_STATUS;
						}
					};
					job.setUser(true);
					job.schedule();
				} else {
					CustomMessageDialog.openError(Display.getDefault().getActiveShell(), messages.errorDialogTitile,
							messages.notiInvalidTemplateMsg);
				}
			}
		}
	}

	/**
	 * Template to user pro rel assign evt DND rel.
	 *
	 * @param templateEventData the template event data
	 * @param targetObj the target obj
	 * @param isCopyPast the is copy past
	 */
	private void templateToUserProRelAssignEvtDNDRel(NotificationTemplate[] templateEventData,
			IAdminTreeChild targetObj, boolean isCopyPast) {


		if (targetObj != null && templateEventData != null && templateEventData.length > 0) {
			if (targetObj instanceof UserProjectRelAssignEvtAction) {
				boolean isConfirmed = CustomMessageDialog.openConfirm(Display.getDefault().getActiveShell(),
						messages.dndTemplateDialogTitle, messages.dndTemplateConfirmMsg);
				if (isConfirmed && ValidateNotificationTemplate.validateTemplate(templateEventData[0], targetObj)) {
					Job job = new Job("Creating Relations...") {

						@Override
						protected IStatus run(IProgressMonitor monitor) {
							monitor.beginTask("Creating Relations..", 100);
							monitor.worked(30);
							try {
								UserProjectRelAssignEvtAction userProRelAssignEvtAction = (UserProjectRelAssignEvtAction) targetObj;
								String configId = AdminTreeFactory.getInstance().getNotifications().getUserProjectRelAssignEvt().getId();
								NotificationRequest request = getRequestObject(userProRelAssignEvtAction,configId);
								request.setEmailTemplateId(templateEventData[0].getTemplateId());
								NotificationController notificationController = new NotificationController();
								NotificationConfigResponse response = notificationController
										.updateNotification(request);
								if (response != null) {
									userProRelAssignEvtAction.setTemplate(templateEventData[0]);
									Display.getDefault().asyncExec(new Runnable() {
										@Override
										public void run() {
											XMAdminUtil.getInstance().getAdminTree().refresh(true);
											if (isCopyPast) {
												XMAdminUtil.getInstance().getAdminTree().setSelection(
														new StructuredSelection(userProRelAssignEvtAction), true);
											}
											XMAdminUtil.getInstance().updateLogFile(messages.templateObject + " '"
													+ templateEventData[0].getName() + "' " + messages.assignedLbl + " "
													+ NotificationEventType.USER_PROJECT_RELATION_ASSIGN.name() + " "
													+ messages.actionLbl + " '" + userProRelAssignEvtAction.getName()
													+ "' " + messages.successfullyLbl, MessageType.SUCCESS);
										}
									});
								}

							} catch (UnauthorizedAccessException e) {
								Display.getDefault().asyncExec(new Runnable() {
									@Override
									public void run() {
										CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
												messages.unauthorizedDialogTitle, messages.unauthorizedDialogMsg);
									}
								});
							} catch (Exception e) {
								if (e instanceof ResourceAccessException) {
									Display.getDefault().asyncExec(new Runnable() {
										@Override
										public void run() {
											CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
													messages.errorDialogTitile, messages.serverNotReachable);
										}
									});
								}
								LOGGER.error(e.getMessage());
							}
							monitor.worked(70);
							return Status.OK_STATUS;
						}
					};
					job.setUser(true);
					job.schedule();
				} else {
					CustomMessageDialog.openError(Display.getDefault().getActiveShell(), messages.errorDialogTitile,
							messages.notiInvalidTemplateMsg);
				}
			}
		}
	}

	private void templateToProjectActivateEvtDNDRel(NotificationTemplate[] templateEventData,
			IAdminTreeChild targetObj, boolean isCopyPast) {

		if (targetObj != null && templateEventData != null && templateEventData.length > 0) {
			if (targetObj instanceof ProjectActivateEvtAction) {
				boolean isConfirmed = CustomMessageDialog.openConfirm(Display.getDefault().getActiveShell(),
						messages.dndTemplateDialogTitle, messages.dndTemplateConfirmMsg);
				if (isConfirmed && ValidateNotificationTemplate.validateTemplate(templateEventData[0], targetObj)) {
					Job job = new Job("Creating Relations...") {

						@Override
						protected IStatus run(IProgressMonitor monitor) {
							monitor.beginTask("Creating Relations..", 100);
							monitor.worked(30);
							try {
								ProjectActivateEvtAction projectActivateEvtAction = (ProjectActivateEvtAction) targetObj;
								String configId = AdminTreeFactory.getInstance().getNotifications().getProjectActivateEvt().getId();
								NotificationRequest request = getRequestObject(projectActivateEvtAction,configId);
								request.setEmailTemplateId(templateEventData[0].getTemplateId());
								NotificationController notificationController = new NotificationController();
								NotificationConfigResponse response = notificationController
										.updateNotification(request);
								if (response != null) {
									projectActivateEvtAction.setTemplate(templateEventData[0]);
									Display.getDefault().asyncExec(new Runnable() {
										@Override
										public void run() {
											XMAdminUtil.getInstance().getAdminTree().refresh(true);
											if (isCopyPast) {
												XMAdminUtil.getInstance().getAdminTree().setSelection(
														new StructuredSelection(projectActivateEvtAction), true);
											}
											XMAdminUtil.getInstance().updateLogFile(messages.templateObject + " '"
													+ templateEventData[0].getName() + "' " + messages.assignedLbl + " "
													+ NotificationEventType.PROJECT_ACTIVATE.name() + " "
													+ messages.actionLbl + " '" + projectActivateEvtAction.getName()
													+ "' " + messages.successfullyLbl, MessageType.SUCCESS);
										}
									});
								}

							} catch (UnauthorizedAccessException e) {
								Display.getDefault().asyncExec(new Runnable() {
									@Override
									public void run() {
										CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
												messages.unauthorizedDialogTitle, messages.unauthorizedDialogMsg);
									}
								});
							} catch (Exception e) {
								if (e instanceof ResourceAccessException) {
									Display.getDefault().asyncExec(new Runnable() {
										@Override
										public void run() {
											CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
													messages.errorDialogTitile, messages.serverNotReachable);
										}
									});
								}
								LOGGER.error(e.getMessage());
							}
							monitor.worked(70);
							return Status.OK_STATUS;
						}
					};
					job.setUser(true);
					job.schedule();
				} else {
					CustomMessageDialog.openError(Display.getDefault().getActiveShell(), messages.errorDialogTitile,
							messages.notiInvalidTemplateMsg);
				}
			}
		}
	
		
	}

	private void templateToProjectDeactivateEvtDNDRel(NotificationTemplate[] templateEventData,
			IAdminTreeChild targetObj, boolean isCopyPast) {

		if (targetObj != null && templateEventData != null && templateEventData.length > 0) {
			if (targetObj instanceof ProjectDeactivateEvtAction) {
				boolean isConfirmed = CustomMessageDialog.openConfirm(Display.getDefault().getActiveShell(),
						messages.dndTemplateDialogTitle, messages.dndTemplateConfirmMsg);
				if (isConfirmed && ValidateNotificationTemplate.validateTemplate(templateEventData[0], targetObj)) {
					Job job = new Job("Creating Relations...") {

						@Override
						protected IStatus run(IProgressMonitor monitor) {
							monitor.beginTask("Creating Relations..", 100);
							monitor.worked(30);
							try {
								ProjectDeactivateEvtAction projectDeactivateEvtAction = (ProjectDeactivateEvtAction) targetObj;
								String configId = AdminTreeFactory.getInstance().getNotifications().getProjectDeactivateEvt().getId();
								NotificationRequest request = getRequestObject(projectDeactivateEvtAction,configId);
								request.setEmailTemplateId(templateEventData[0].getTemplateId());
								NotificationController notificationController = new NotificationController();
								NotificationConfigResponse response = notificationController
										.updateNotification(request);
								if (response != null) {
									projectDeactivateEvtAction.setTemplate(templateEventData[0]);
									Display.getDefault().asyncExec(new Runnable() {
										@Override
										public void run() {
											XMAdminUtil.getInstance().getAdminTree().refresh(true);
											if (isCopyPast) {
												XMAdminUtil.getInstance().getAdminTree().setSelection(
														new StructuredSelection(projectDeactivateEvtAction), true);
											}
											XMAdminUtil.getInstance().updateLogFile(messages.templateObject + " '"
													+ templateEventData[0].getName() + "' " + messages.assignedLbl + " "
													+ NotificationEventType.PROJECT_DEACTIVATE.name() + " "
													+ messages.actionLbl + " '" + projectDeactivateEvtAction.getName()
													+ "' " + messages.successfullyLbl, MessageType.SUCCESS);
										}
									});
								}

							} catch (UnauthorizedAccessException e) {
								Display.getDefault().asyncExec(new Runnable() {
									@Override
									public void run() {
										CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
												messages.unauthorizedDialogTitle, messages.unauthorizedDialogMsg);
									}
								});
							} catch (Exception e) {
								if (e instanceof ResourceAccessException) {
									Display.getDefault().asyncExec(new Runnable() {
										@Override
										public void run() {
											CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
													messages.errorDialogTitile, messages.serverNotReachable);
										}
									});
								}
								LOGGER.error(e.getMessage());
							}
							monitor.worked(70);
							return Status.OK_STATUS;
						}
					};
					job.setUser(true);
					job.schedule();
				} else {
					CustomMessageDialog.openError(Display.getDefault().getActiveShell(), messages.errorDialogTitile,
							messages.notiInvalidTemplateMsg);
				}
			}
		}
	
		
	}

	/**
	 * Gets the request object.
	 *
	 * @param actionModel the action model
	 * @return the request object
	 */
	protected NotificationRequest getRequestObject(NotificationEvtActions actionModel,String configId) {
		com.magna.xmbackend.vo.notification.NotificationRequest notificationRequest = new com.magna.xmbackend.vo.notification.NotificationRequest();
		notificationRequest.setNotificationConfigId(actionModel.getId());
		notificationRequest.setEmailTemplateId(actionModel.getTemplate().getTemplateId());
		notificationRequest.setNotificationConfigActionName(actionModel.getName());
		notificationRequest.setNotificationEventId(configId);
		notificationRequest.setNotificationActionStatus(actionModel.isActive() == true
				? com.magna.xmbackend.vo.enums.Status.ACTIVE : com.magna.xmbackend.vo.enums.Status.INACTIVE);
		notificationRequest.setNotificationEventStatus(
				AdminTreeFactory.getInstance().getNotifications().getProjectCreateEvt().isActive() == true
						? com.magna.xmbackend.vo.enums.Status.ACTIVE : com.magna.xmbackend.vo.enums.Status.INACTIVE);
		notificationRequest.setCcUsersToNotify(new ArrayList<>(actionModel.getCcUsers()));
		notificationRequest.setUsersToNotify(new ArrayList<>(actionModel.getToUsers()));
		return notificationRequest;
	}

	/**
	 * Template to project delete evt DND rel.
	 *
	 * @param templateEventData the template event data
	 * @param targetObj the target obj
	 * @param isCopyPast the is copy past
	 */
	private void templateToProjectDeleteEvtDNDRel(NotificationTemplate[] templateEventData, IAdminTreeChild targetObj,
			boolean isCopyPast) {

		if (targetObj != null && templateEventData != null && templateEventData.length > 0) {
			if (targetObj instanceof ProjectDeleteEvtAction) {
				boolean isConfirmed = CustomMessageDialog.openConfirm(Display.getDefault().getActiveShell(),
						messages.dndTemplateDialogTitle, messages.dndTemplateConfirmMsg);
				if (isConfirmed && ValidateNotificationTemplate.validateTemplate(templateEventData[0], targetObj)) {
					Job job = new Job("Creating Relations...") {

						@Override
						protected IStatus run(IProgressMonitor monitor) {
							monitor.beginTask("Creating Relations..", 100);
							monitor.worked(30);
							try {
								ProjectDeleteEvtAction projectDeleteEvtAction = (ProjectDeleteEvtAction) targetObj;
								String configId = AdminTreeFactory.getInstance().getNotifications().getProjectDeleteEvt().getId();
								NotificationRequest request = getRequestObject(projectDeleteEvtAction,configId);
								request.setEmailTemplateId(templateEventData[0].getTemplateId());
								NotificationController notificationController = new NotificationController();
								NotificationConfigResponse response = notificationController
										.updateNotification(request);
								if (response != null) {
									projectDeleteEvtAction.setTemplate(templateEventData[0]);
									Display.getDefault().asyncExec(new Runnable() {
										@Override
										public void run() {
											XMAdminUtil.getInstance().getAdminTree().refresh(true);
											if (isCopyPast) {
												XMAdminUtil.getInstance().getAdminTree().setSelection(
														new StructuredSelection(projectDeleteEvtAction), true);
											}
											XMAdminUtil.getInstance().updateLogFile(messages.templateObject + " '"
													+ templateEventData[0].getName() + "' " + messages.assignedLbl + " "
													+ NotificationEventType.PROJECT_DELETE.name() + " "
													+ messages.actionLbl + " '" + projectDeleteEvtAction.getName()
													+ "' " + messages.successfullyLbl, MessageType.SUCCESS);
										}
									});
								}

							} catch (UnauthorizedAccessException e) {
								Display.getDefault().asyncExec(new Runnable() {
									@Override
									public void run() {
										CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
												messages.unauthorizedDialogTitle, messages.unauthorizedDialogMsg);
									}
								});
							} catch (Exception e) {
								if (e instanceof ResourceAccessException) {
									Display.getDefault().asyncExec(new Runnable() {
										@Override
										public void run() {
											CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
													messages.errorDialogTitile, messages.serverNotReachable);
										}
									});
								}
								LOGGER.error(e.getMessage());
							}
							monitor.worked(70);
							return Status.OK_STATUS;
						}
					};
					job.setUser(true);
					job.schedule();
				} else {
					CustomMessageDialog.openError(Display.getDefault().getActiveShell(), messages.errorDialogTitile,
							messages.notiInvalidTemplateMsg);
				}
			}
		}
	
		
	}

	/**
	 * Template to project create evt DND rel.
	 *
	 * @param templateEventData the template event data
	 * @param targetObj the target obj
	 * @param isCopyPast the is copy past
	 */
	private void templateToProjectCreateEvtDNDRel(NotificationTemplate[] templateEventData, IAdminTreeChild targetObj,
			boolean isCopyPast) {
		if (targetObj != null && templateEventData != null && templateEventData.length > 0) {
			if (targetObj instanceof ProjectCreateEvtAction) {
				boolean isConfirmed = CustomMessageDialog.openConfirm(Display.getDefault().getActiveShell(),
						messages.dndTemplateDialogTitle, messages.dndTemplateConfirmMsg);
				if (isConfirmed && ValidateNotificationTemplate.validateTemplate(templateEventData[0], targetObj)) {
					Job job = new Job("Creating Relations...") {

						@Override
						protected IStatus run(IProgressMonitor monitor) {
							monitor.beginTask("Creating Relations..", 100);
							monitor.worked(30);
							try {
								ProjectCreateEvtAction projectCreateEvtAction = (ProjectCreateEvtAction) targetObj;
								String configId = AdminTreeFactory.getInstance().getNotifications().getProjectCreateEvt().getId();
								NotificationRequest request = getRequestObject(projectCreateEvtAction,configId);
								request.setEmailTemplateId(templateEventData[0].getTemplateId());
								NotificationController notificationController = new NotificationController();
								NotificationConfigResponse response = notificationController
										.updateNotification(request);
								if (response != null) {
									projectCreateEvtAction.setTemplate(templateEventData[0]);
									Display.getDefault().asyncExec(new Runnable() {
										@Override
										public void run() {
											XMAdminUtil.getInstance().getAdminTree().refresh(true);
											if (isCopyPast) {
												XMAdminUtil.getInstance().getAdminTree().setSelection(
														new StructuredSelection(projectCreateEvtAction), true);
											}
											XMAdminUtil.getInstance().updateLogFile(messages.templateObject + " '"
													+ templateEventData[0].getName() + "' " + messages.assignedLbl + " "
													+ NotificationEventType.PROJECT_CREATE.name() + " "
													+ messages.actionLbl + " '" + projectCreateEvtAction.getName()
													+ "' " + messages.successfullyLbl, MessageType.SUCCESS);
										}
									});
								}

							} catch (UnauthorizedAccessException e) {
								Display.getDefault().asyncExec(new Runnable() {
									@Override
									public void run() {
										CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
												messages.unauthorizedDialogTitle, messages.unauthorizedDialogMsg);
									}
								});
							} catch (Exception e) {
								if (e instanceof ResourceAccessException) {
									Display.getDefault().asyncExec(new Runnable() {
										@Override
										public void run() {
											CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
													messages.errorDialogTitile, messages.serverNotReachable);
										}
									});
								}
								LOGGER.error(e.getMessage());
							}
							monitor.worked(70);
							return Status.OK_STATUS;
						}
					};
					job.setUser(true);
					job.schedule();
				} else {
					CustomMessageDialog.openError(Display.getDefault().getActiveShell(), messages.errorDialogTitile,
							messages.notiInvalidTemplateMsg);
				}
			}
		}
	}

	/**
	 * Admin area to project admin area.
	 *
	 * @param adminAreaArray
	 *            the admin area array
	 * @param iAdminTreeChild
	 *            the i admin tree child
	 * @param isCopyPast
	 *            the is copy past
	 */
	private void adminAreaToProjectAdminAreaDNDRel(AdministrationArea[] adminAreaArray, IAdminTreeChild iAdminTreeChild,
			boolean isCopyPast) {
		if (iAdminTreeChild != null && adminAreaArray != null && adminAreaArray.length > 0) {
			IAdminTreeChild targetIAdminTreeChild = null;
			String ProjectId = null;
			if (iAdminTreeChild instanceof Project) {
				targetIAdminTreeChild = ((Project) iAdminTreeChild).getProjectChildren()
						.get(ProjectAdminAreas.class.getName());
				ProjectId = ((Project) iAdminTreeChild).getProjectId();
			} else if (iAdminTreeChild instanceof ProjectAdminAreas) {
				targetIAdminTreeChild = iAdminTreeChild;
				ProjectId = ((Project) iAdminTreeChild.getParent()).getProjectId();
			}

			if (targetIAdminTreeChild != null && ProjectId != null) {

				final IAdminTreeChild targetIAdminTreeChild2 = targetIAdminTreeChild;
				final String ProjectId2 = ProjectId;
				Job job = new Job("Creating Relations...") {

					@Override
					protected IStatus run(IProgressMonitor monitor) {
						monitor.beginTask("Creating Relations..", 100);
						monitor.worked(30);
						try {
							List<AdminAreaProjectRelRequest> adminAreaProjectRelRequestList = new ArrayList<>();
							List<String> adminAreaNamelist = new ArrayList<>();
							for (AdministrationArea adminArea : adminAreaArray) {
								if (adminArea.getRelationId() != null) {
									AdminAreaProjectRelRequest adminAreaProjectRelRequest = new AdminAreaProjectRelRequest();
									adminAreaProjectRelRequest.setProjectId(ProjectId2);
									adminAreaProjectRelRequest.setSiteAdminAreaRelId(adminArea.getRelationId());
									adminAreaProjectRelRequest
											.setStatus(com.magna.xmbackend.vo.enums.Status.ACTIVE.name());
									adminAreaProjectRelRequestList.add(adminAreaProjectRelRequest);
								} else {
									adminAreaNamelist.add(adminArea.getName());
								}
							}

							if (adminAreaNamelist.size() >= 1) {
								Display.getDefault().syncExec(new Runnable() {
									public void run() {
										String names = "";
										for (int i = 0; i < adminAreaNamelist.size(); i++) {
											names += messages.adminAreaDndErrorMsgPart1 + " \'"
													+ adminAreaNamelist.get(i) + "\' "
													+ messages.adminAreaDndErrorMsgPart2;
											names += "\n";
										}
										CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
												messages.unauthorizedDialogTitle, names);
									}
								});
							}
							
							AdminAreaProjectRelController adminAreaProjectRelController = new AdminAreaProjectRelController(
									XMAdminUtil.getInstance().getAAForHeaders(targetIAdminTreeChild2));
							AdminAreaProjectRelBatchResponse adminAreaProjectRelBatchResponse = adminAreaProjectRelController
									.createAdminAreaProjectRel(
											new AdminAreaProjectRelBatchRequest(adminAreaProjectRelRequestList));
							if (adminAreaProjectRelBatchResponse != null) {
								boolean isRelationCreated = false;
								Iterable<AdminAreaProjectRelTbl> adminAreaProjectRelTbls = adminAreaProjectRelBatchResponse
										.getAdminAreaProjectRelTbls();
								List<String> successObjList = new ArrayList<>();
								for (AdminAreaProjectRelTbl adminAreaProjectRelTbl : adminAreaProjectRelTbls) {
									String relId = adminAreaProjectRelTbl.getAdminAreaProjectRelId();
									if (relId != null) {
										for (AdministrationArea adminArea : adminAreaArray) {
											if (adminArea.getRelationId() != null
													&& adminArea.getRelationId().equals(adminAreaProjectRelTbl
															.getSiteAdminAreaRelId().getSiteAdminAreaRelId())) {
												successObjList.add(adminArea.getName());
												if (targetIAdminTreeChild2 instanceof ProjectAdminAreas) {
													RelationObj relObj = (RelationObj) adminArea.getAdapter(
															ProjectAdminAreaChild.class, targetIAdminTreeChild2, relId,
															true);
													((ProjectAdminAreaChild) relObj.getContainerObj())
															.addFixedChildren(relObj);
													((ProjectAdminAreas) targetIAdminTreeChild2).add(relObj.getRelId(),
															relObj);
												}
												isRelationCreated = true;
											}
										}
									}
								}
								if (isRelationCreated) {
									Display.getDefault().asyncExec(new Runnable() {
										@SuppressWarnings("unchecked")
										public void run() {
											viewer.refresh(targetIAdminTreeChild2);
											if (!viewer.getExpandedState(targetIAdminTreeChild2.getParent())) {
												(new AdminTreeExpansion(targetIAdminTreeChild2.getParent()))
														.loadObjects();
												viewer.setExpandedState(targetIAdminTreeChild2.getParent(), true);
											}
											viewer.setExpandedState(targetIAdminTreeChild2, true);
											final AdminTreeviewer adminTree = XMAdminUtil.getInstance().getAdminTree();

											adminTree.refershBackReference(new Class[] { UserProjectAdminAreas.class,
													SiteAdminAreaProjects.class, AdminAreaProjects.class });
											if (isCopyPast && (iAdminTreeChild instanceof ProjectAdminAreas)) {
												adminTree.setSelection(new StructuredSelection(iAdminTreeChild));
											}
										}
									});
								}
								List<Map<String, String>> statusMapList = adminAreaProjectRelBatchResponse
										.getStatusMap();
								int noOfObjRelCreated = adminAreaArray.length;
								final int noOfObjRelCreated2;
								if (statusMapList != null && !statusMapList.isEmpty()) {
									noOfObjRelCreated = adminAreaArray.length - statusMapList.size();
									Display.getDefault().asyncExec(new Runnable() {
										public void run() {
											String errorMessage = "";
											for (Map<String, String> statusMap : statusMapList) {
												errorMessage += statusMap.get(
														XMAdminUtil.getInstance().getCurrentLocaleEnum().getLangCode());
												errorMessage += "\n";
											}
											if (errorMessage.length() > 0) {
												CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
														messages.dndErrorTitle, errorMessage);
											}
										}
									});
								}

								noOfObjRelCreated2 = noOfObjRelCreated;
								if (noOfObjRelCreated2 > 0) {
									Project project = (Project) (targetIAdminTreeChild2.getParent());
									for (String objName : successObjList) {
										XMAdminUtil.getInstance()
												.updateLogFile(messages.relationLbl + " " + messages.betweenLbl + " "
														+ messages.administrationAreaObject + " '" + objName + "' "
														+ messages.andLbl + " " + messages.projectObject + " '"
														+ project.getName() + "' " + messages.objectCreate,
														MessageType.SUCCESS);
									}
									
									if (adminAreaNamelist.size() > 0) {
										for (int i = 0; i < adminAreaNamelist.size(); i++) {
											XMAdminUtil.getInstance().updateLogFile(
													messages.adminAreaDndErrorMsgPart1 + " '" + adminAreaNamelist.get(i)
															+ "' " + messages.adminAreaDndErrorMsgPart2,
													MessageType.FAILURE);
										}
									}
									Display.getDefault().asyncExec(new Runnable() {
										@Override
										public void run() {
											eventBroker.send(CommonConstants.EVENT_BROKER.STATUSBAR,
													noOfObjRelCreated2 + " " + messages.dndStatusBarMsg);
										}
									});
								}
								if (statusMapList != null && !statusMapList.isEmpty()) {

									for (Map<String, String> statusMap : statusMapList) {
										String errorMessage = statusMap
												.get(XMAdminUtil.getInstance().getCurrentLocaleEnum().getLangCode());
										XMAdminUtil.getInstance().updateLogFile(errorMessage, MessageType.FAILURE);
									}
								}
							}
						} catch (UnauthorizedAccessException e) {
							Display.getDefault().asyncExec(new Runnable() {
								@Override
								public void run() {
									CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
											messages.unauthorizedDialogTitle, messages.unauthorizedDialogMsg);
								}
							});
						} catch (Exception e) {
							if (e instanceof ResourceAccessException) {
								Display.getDefault().asyncExec(new Runnable() {
									@Override
									public void run() {
										CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
												messages.errorDialogTitile, messages.serverNotReachable);
									}
								});
							}
							LOGGER.error(e.getMessage());
						}

						monitor.worked(70);
						return Status.OK_STATUS;
					}
				};
				job.setUser(true);
				job.schedule();
			}
		}
	}

	/**
	 * Method for Admin area to site admin area DND rel.
	 *
	 * @param adminAreaArray
	 *            {@link AdministrationArea[]}
	 * @param iAdminTreeChild
	 *            {@link IAdminTreeChild}
	 * @param isCopyPast
	 */
	private void adminAreaToSiteAdminAreaDNDRel(final AdministrationArea[] adminAreaArray,
			final IAdminTreeChild iAdminTreeChild, boolean isCopyPast) {
		if (iAdminTreeChild != null && adminAreaArray != null && adminAreaArray.length > 0) {
			SiteAdministrations siteAdministrations = null;
			if (iAdminTreeChild instanceof SiteAdministrations) {
				siteAdministrations = (SiteAdministrations) iAdminTreeChild;
			} else if (iAdminTreeChild instanceof Site) {
				Map<String, IAdminTreeChild> siteChildren = ((Site) iAdminTreeChild).getSiteChildren();
				IAdminTreeChild iAdminTreeChild2 = siteChildren.get(SiteAdministrations.class.getName());
				if (iAdminTreeChild2 != null && iAdminTreeChild2 instanceof SiteAdministrations) {
					siteAdministrations = (SiteAdministrations) iAdminTreeChild2;
				}
			}
			if (siteAdministrations != null) {
				SiteAdministrations siteAdministrations2 = siteAdministrations;
				Job job = new Job("Creating Relations...") {

					@Override
					protected IStatus run(IProgressMonitor monitor) {
						monitor.beginTask("Creating Relations..", 100);
						monitor.worked(30);
						try {
							List<SiteAdminAreaRelRequest> siteAdminAreaRelRequestList = new ArrayList<>();
							for (AdministrationArea adminArea : adminAreaArray) {
								SiteAdminAreaRelRequest siteAdminAreaRelRequest = new SiteAdminAreaRelRequest();
								siteAdminAreaRelRequest
										.setSiteId(((Site) siteAdministrations2.getParent()).getSiteId());
								siteAdminAreaRelRequest.setAdminAreaId(adminArea.getAdministrationAreaId());
								siteAdminAreaRelRequest.setStatus(com.magna.xmbackend.vo.enums.Status.ACTIVE.name());
								siteAdminAreaRelRequestList.add(siteAdminAreaRelRequest);
							}
							SiteAdminAreaRelController siteAdminAreaRelController = new SiteAdminAreaRelController();
							SiteAdminAreaRelBatchResponse siteAdminAreaRelBatchResponse = siteAdminAreaRelController
									.createSiteAdminAreaRel(
											new SiteAdminAreaRelBatchRequest(siteAdminAreaRelRequestList));
							if (siteAdminAreaRelBatchResponse != null) {
								boolean isRelationCreated = false;
								Iterable<SiteAdminAreaRelTbl> siteAdminAreaRelTbls = siteAdminAreaRelBatchResponse
										.getSiteAdminAreaRelTbls();
								List<AdministrationArea> successObjList = new ArrayList<>();
								for (SiteAdminAreaRelTbl siteAdminAreaRelTbl : siteAdminAreaRelTbls) {
									String relId = siteAdminAreaRelTbl.getSiteAdminAreaRelId();
									if (relId != null) {
										for (AdministrationArea adminArea : adminAreaArray) {
											if (adminArea.getAdministrationAreaId()
													.equals(siteAdminAreaRelTbl.getAdminAreaId().getAdminAreaId())) {
												successObjList.add(adminArea);
												RelationObj siteAdministrationChildRel = (RelationObj) adminArea
														.getAdapter(SiteAdministrationChild.class, siteAdministrations2,
																relId, true);

												((SiteAdministrationChild) siteAdministrationChildRel.getContainerObj())
														.addFixedChildren(siteAdministrationChildRel);

												siteAdministrations2.add(relId, siteAdministrationChildRel);
												adminArea.setRelationId(relId);
												adminArea.setRelatedSiteName(
														((Site) siteAdministrations2.getParent()).getName());
												isRelationCreated = true;
											}
										}
									}
								}
								if (isRelationCreated) {
									Display.getDefault().syncExec(new Runnable() {
										@SuppressWarnings("unchecked")
										public void run() {
											viewer.refresh(siteAdministrations2);
											if (!viewer.getExpandedState(siteAdministrations2.getParent())) {
												(new AdminTreeExpansion(siteAdministrations2.getParent()))
														.loadObjects();
												viewer.setExpandedState(siteAdministrations2.getParent(), true);
											}
											viewer.setExpandedState(siteAdministrations2, true);
											viewer.refresh(true);
											AdminTreeviewer adminTree = XMAdminUtil.getInstance().getAdminTree();
											adminTree.refershBackReference(new Class[] { UserAdminAreas.class });
											if (isCopyPast && iAdminTreeChild instanceof SiteAdministrations) {
												adminTree.setSelection(new StructuredSelection(iAdminTreeChild));
											}
										}
									});
								}

								List<Map<String, String>> statusMapList = siteAdminAreaRelBatchResponse.getStatusMap();
								int noOfObjRelCreated = adminAreaArray.length;
								final int noOfObjRelCreated2;
								if (statusMapList != null && !statusMapList.isEmpty()) {
									noOfObjRelCreated = adminAreaArray.length - statusMapList.size();
									Display.getDefault().asyncExec(new Runnable() {
										public void run() {
											String errorMessage = "";
											for (Map<String, String> statusMap : statusMapList) {
												errorMessage += statusMap.get(
														XMAdminUtil.getInstance().getCurrentLocaleEnum().getLangCode());
												errorMessage += "\n";
											}
											if (errorMessage.length() > 0) {
												CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
														messages.dndErrorTitle, errorMessage);

											}
										}
									});
								}
								noOfObjRelCreated2 = noOfObjRelCreated;
								if (noOfObjRelCreated2 > 0) {
									for (AdministrationArea adminArea : successObjList) {
										XMAdminUtil.getInstance()
												.updateLogFile(messages.relationLbl + " " + messages.betweenLbl + " "
														+ messages.administrationAreaObject +" '"
														+ adminArea.getName() + "' " + messages.andLbl + " "
														+ messages.siteObject + " '" + adminArea.getRelatedSiteName()
														+ "' " + messages.objectCreate, MessageType.SUCCESS);
									}
									Display.getDefault().asyncExec(new Runnable() {
										@Override
										public void run() {
											eventBroker.send(CommonConstants.EVENT_BROKER.STATUSBAR,
													noOfObjRelCreated2 + " " + messages.dndStatusBarMsg);
										}
									});
								}
								if (statusMapList != null && !statusMapList.isEmpty()) {
									for (Map<String, String> statusMap : statusMapList) {
										String errorMessage = statusMap
												.get(XMAdminUtil.getInstance().getCurrentLocaleEnum().getLangCode());
										XMAdminUtil.getInstance().updateLogFile(errorMessage, MessageType.FAILURE);
									}
								}
							}
						} catch (UnauthorizedAccessException e) {
							Display.getDefault().asyncExec(new Runnable() {
								@Override
								public void run() {
									CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
											messages.unauthorizedDialogTitle, messages.unauthorizedDialogMsg);
								}
							});
						} catch (Exception e) {
							if (e instanceof ResourceAccessException) {
								Display.getDefault().asyncExec(new Runnable() {
									@Override
									public void run() {
										CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
												messages.errorDialogTitile, messages.serverNotReachable);
									}
								});
							}
							LOGGER.error(e.getMessage());
						}

						monitor.worked(70);
						return Status.OK_STATUS;
					}
				};
				job.setUser(true);
				job.schedule();
			}
		}
	}

	/**
	 * Method for Project to site admin area DND rel.
	 *
	 * @param projectArray
	 *            {@link Project[]}
	 * @param iAdminTreeChild
	 *            {@link IAdminTreeChild}
	 * @param isCopyPast
	 */
	private void projectToSiteAdminAreaDNDRel(final Project[] projectArray, final IAdminTreeChild iAdminTreeChild,
			boolean isCopyPast) {
		if (iAdminTreeChild != null && projectArray != null && projectArray.length > 0) {
			String siteAdminAreaRelId = null;
			IAdminTreeChild targetIAdminTreeChild = null;
			String adminAreaName = null;
			if (iAdminTreeChild instanceof SiteAdminAreaProjects) {
				targetIAdminTreeChild = iAdminTreeChild;
				RelationObj relationObj = (RelationObj) iAdminTreeChild.getParent();
				siteAdminAreaRelId = relationObj.getRelId();
				adminAreaName = ((AdministrationArea) ((RelationObj) iAdminTreeChild.getParent()).getRefObject())
						.getName();
			} else if (iAdminTreeChild instanceof SiteAdministrationChild) {
				targetIAdminTreeChild = ((SiteAdministrationChild) iAdminTreeChild).getSiteAdminAreaChildren()
						.get(SiteAdminAreaProjects.class.getName());
				RelationObj relationObj = (RelationObj) targetIAdminTreeChild.getParent();
				siteAdminAreaRelId = relationObj.getRelId();
				adminAreaName = ((AdministrationArea) ((RelationObj) targetIAdminTreeChild.getParent()).getRefObject()).getName();
			} else if (iAdminTreeChild instanceof AdminAreaProjects) {
				targetIAdminTreeChild = iAdminTreeChild;
				siteAdminAreaRelId = ((AdministrationArea) iAdminTreeChild.getParent()).getRelationId();
				adminAreaName = ((AdministrationArea) (iAdminTreeChild.getParent())).getName();
			} else if (iAdminTreeChild instanceof AdministrationArea) {
				targetIAdminTreeChild = ((AdministrationArea) iAdminTreeChild).getAdminAreaChildren()
						.get(AdminAreaProjects.class.getName());
				siteAdminAreaRelId = ((AdministrationArea) iAdminTreeChild).getRelationId();
				adminAreaName = ((AdministrationArea) (iAdminTreeChild)).getName();
			}
			if (targetIAdminTreeChild != null && siteAdminAreaRelId == null) {
				IAdminTreeChild parent = targetIAdminTreeChild.getParent();
				if (parent instanceof AdministrationArea) {
					AdministrationArea adminArea = (AdministrationArea) parent;
					String name = adminArea.getName();
					CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
							messages.unauthorizedDialogTitle, messages.adminAreaDndErrorMsgPart1 + " \'" + name + "\' "
									+ messages.adminAreaDndErrorMsgPart2);
					return;
				}
			}
			if (targetIAdminTreeChild != null && siteAdminAreaRelId != null) {
				final String siteAdminAreaRelId2 = siteAdminAreaRelId;
				final IAdminTreeChild targetIAdminTreeChild2 = targetIAdminTreeChild;
				final String adminAreaName2 = adminAreaName;
				Job job = new Job("Creating Relations...") {

					@Override
					protected IStatus run(IProgressMonitor monitor) {
						monitor.beginTask("Creating Relations..", 100);
						monitor.worked(30);
						try {
							List<AdminAreaProjectRelRequest> adminAreaProjectRelRequestList = new ArrayList<>();
							for (Project project : projectArray) {
								AdminAreaProjectRelRequest adminAreaProjectRelRequest = new AdminAreaProjectRelRequest();
								adminAreaProjectRelRequest.setProjectId(project.getProjectId());
								adminAreaProjectRelRequest.setSiteAdminAreaRelId(siteAdminAreaRelId2);
								adminAreaProjectRelRequest.setStatus(com.magna.xmbackend.vo.enums.Status.ACTIVE.name());
								adminAreaProjectRelRequestList.add(adminAreaProjectRelRequest);
							}
							AdminAreaProjectRelController adminAreaProjectRelController = new AdminAreaProjectRelController(
									XMAdminUtil.getInstance().getAAForHeaders(targetIAdminTreeChild2));
							AdminAreaProjectRelBatchResponse adminAreaProjectRelBatchResponse = adminAreaProjectRelController
									.createAdminAreaProjectRel(
											new AdminAreaProjectRelBatchRequest(adminAreaProjectRelRequestList));

							if (adminAreaProjectRelBatchResponse != null) {
								boolean isRelationCreated = false;
								Iterable<AdminAreaProjectRelTbl> adminAreaProjectRelTbls = adminAreaProjectRelBatchResponse
										.getAdminAreaProjectRelTbls();
								List<String> successObjList = new ArrayList<>();
								for (AdminAreaProjectRelTbl adminAreaProjectRelTbl : adminAreaProjectRelTbls) {
									String relId = adminAreaProjectRelTbl.getAdminAreaProjectRelId();
									if (relId != null) {
										for (Project project : projectArray) {
											if (project.getProjectId()
													.equals(adminAreaProjectRelTbl.getProjectId().getProjectId())) {
												successObjList.add(project.getName());
												if (targetIAdminTreeChild2 instanceof SiteAdminAreaProjects) {
													RelationObj relObj = (RelationObj) project.getAdapter(
															SiteAdminAreaProjectChild.class, targetIAdminTreeChild2,
															relId, true);
													((SiteAdminAreaProjectChild) relObj.getContainerObj())
															.addFixedChildren(relObj);
													((SiteAdminAreaProjects) targetIAdminTreeChild2)
															.add(relObj.getRelId(), relObj);
												} else if (targetIAdminTreeChild2 instanceof AdminAreaProjects) {
													RelationObj relObj = (RelationObj) project.getAdapter(
															AdminAreaProjectChild.class, targetIAdminTreeChild2, relId,
															true);
													((AdminAreaProjectChild) relObj.getContainerObj())
															.addFixedChildren(relObj);
													((AdminAreaProjects) targetIAdminTreeChild2).add(relObj.getRelId(),
															relObj);
												}
												isRelationCreated = true;
											}
										}
									}
								}
								if (isRelationCreated) {
									Display.getDefault().syncExec(new Runnable() {
										@SuppressWarnings("unchecked")
										public void run() {
											viewer.refresh(targetIAdminTreeChild2);
											if (!viewer.getExpandedState(targetIAdminTreeChild2.getParent())) {
												(new AdminTreeExpansion(targetIAdminTreeChild2.getParent()))
														.loadObjects();
												viewer.setExpandedState(targetIAdminTreeChild2.getParent(), true);
											}
											viewer.setExpandedState(targetIAdminTreeChild2, true);
											final AdminTreeviewer adminTree = XMAdminUtil.getInstance().getAdminTree();
											if (targetIAdminTreeChild2 instanceof SiteAdminAreaProjects) {
												adminTree.refershBackReference(new Class[] { AdminAreaProjects.class });
											} else if (targetIAdminTreeChild2 instanceof AdminAreaProjects) {
												adminTree.refershBackReference(
														new Class[] { SiteAdminAreaProjects.class });
											}
											adminTree.refershBackReference(new Class[] { ProjectAdminAreas.class,
													UserProjectAdminAreas.class });
											if (isCopyPast && (iAdminTreeChild instanceof SiteAdminAreaProjects
													|| iAdminTreeChild instanceof AdminAreaProjects)) {
												adminTree.setSelection(new StructuredSelection(iAdminTreeChild));
											}
										}
									});
								}
								List<Map<String, String>> statusMapList = adminAreaProjectRelBatchResponse
										.getStatusMap();
								int noOfObjRelCreated = projectArray.length;
								final int noOfObjRelCreated2;
								if (statusMapList != null && !statusMapList.isEmpty()) {
									noOfObjRelCreated = projectArray.length - statusMapList.size();
									Display.getDefault().asyncExec(new Runnable() {
										public void run() {
											String errorMessage = "";
											for (Map<String, String> statusMap : statusMapList) {
												errorMessage += statusMap.get(
														XMAdminUtil.getInstance().getCurrentLocaleEnum().getLangCode());
												errorMessage += "\n";
											}
											if (errorMessage.length() > 0) {
												CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
														messages.dndErrorTitle, errorMessage);
											}
										}
									});
								}
								noOfObjRelCreated2 = noOfObjRelCreated;
								if (noOfObjRelCreated2 > 0) {
									if (noOfObjRelCreated == 1) {
										for (String objName : successObjList) {
											XMAdminUtil.getInstance()
													.updateLogFile(messages.relationLbl + " " + messages.betweenLbl
															+ " " + messages.projectObject + " " + "'" + objName + "' "
															+ messages.andLbl + " " + messages.administrationAreaObject
															+ " '" + adminAreaName2 + "' " + messages.objectCreate,
															MessageType.SUCCESS);
										}
									}
									Display.getDefault().asyncExec(new Runnable() {
										@Override
										public void run() {
											eventBroker.send(CommonConstants.EVENT_BROKER.STATUSBAR,
													noOfObjRelCreated2 + " " + messages.dndStatusBarMsg);
										}
									});
								}
								if (statusMapList != null && !statusMapList.isEmpty()) {
									for (Map<String, String> statusMap : statusMapList) {
										String errorMessage = statusMap
												.get(XMAdminUtil.getInstance().getCurrentLocaleEnum().getLangCode());
										XMAdminUtil.getInstance().updateLogFile(errorMessage, MessageType.FAILURE);
									}
								}
							}
						} catch (UnauthorizedAccessException e) {
							Display.getDefault().asyncExec(new Runnable() {
								@Override
								public void run() {
									CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
											messages.unauthorizedDialogTitle, messages.unauthorizedDialogMsg);
								}
							});
						} catch (Exception e) {
							if (e instanceof ResourceAccessException) {
								Display.getDefault().asyncExec(new Runnable() {
									@Override
									public void run() {
										CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
												messages.errorDialogTitile, messages.serverNotReachable);
									}
								});
							}
							LOGGER.error(e.getMessage());
						}

						monitor.worked(70);
						return Status.OK_STATUS;
					}
				};
				job.setUser(true);
				job.schedule();
			}
		}
	}

	/**
	 * Method for User app to site admin area DND rel.
	 *
	 * @param userAppArray
	 *            {@link UserApplication[]}
	 * @param iAdminTreeChild
	 *            {@link IAdminTreeChild}
	 * @param isCopyPast
	 */
	private void userAppToSiteAdminAreaDNDRel(final UserApplication[] userAppArray,
			final IAdminTreeChild iAdminTreeChild, boolean isCopyPast) {
		if (iAdminTreeChild != null && userAppArray != null && userAppArray.length > 0) {
			String siteAdminAreaRelId = null;
			String relationType = null;
			IAdminTreeChild targetIAdminTreeChild = null;
			String adminAreaName = null;
			if (iAdminTreeChild instanceof SiteAdminAreaUserAppNotFixed) {
				RelationObj relationObj = (RelationObj) iAdminTreeChild.getParent().getParent();
				siteAdminAreaRelId = relationObj.getRelId();
				targetIAdminTreeChild = iAdminTreeChild;
				relationType = ApplicationRelationType.NOTFIXED.name();
				adminAreaName = ((AdministrationArea) relationObj.getRefObject()).getName();
			} else if (iAdminTreeChild instanceof SiteAdminAreaUserAppFixed) {
				RelationObj relationObj = (RelationObj) iAdminTreeChild.getParent().getParent();
				siteAdminAreaRelId = relationObj.getRelId();
				targetIAdminTreeChild = iAdminTreeChild;
				relationType = ApplicationRelationType.FIXED.name();
				adminAreaName = ((AdministrationArea) relationObj.getRefObject()).getName();
			} else if (iAdminTreeChild instanceof SiteAdminAreaUserAppProtected) {
				RelationObj relationObj = (RelationObj) iAdminTreeChild.getParent().getParent();
				siteAdminAreaRelId = relationObj.getRelId();
				targetIAdminTreeChild = iAdminTreeChild;
				relationType = ApplicationRelationType.PROTECTED.name();
				adminAreaName = ((AdministrationArea) relationObj.getRefObject()).getName();
			} else if (iAdminTreeChild instanceof SiteAdminAreaUserApplications) {
				RelationObj relationObj = (RelationObj) iAdminTreeChild.getParent();
				siteAdminAreaRelId = relationObj.getRelId();
				targetIAdminTreeChild = iAdminTreeChild;
				siteAdminAreaRelId = ((RelationObj) targetIAdminTreeChild.getParent()).getRelId();
				relationType = ApplicationRelationType.NOTFIXED.name();
				adminAreaName = ((AdministrationArea) relationObj.getRefObject()).getName();
			} else if (iAdminTreeChild instanceof SiteAdministrationChild) {
				Map<String, IAdminTreeChild> siteAdminAreaChildren = ((SiteAdministrationChild) iAdminTreeChild)
						.getSiteAdminAreaChildren();
				targetIAdminTreeChild = siteAdminAreaChildren.get(SiteAdminAreaUserApplications.class.getName());
				RelationObj relationObj = (RelationObj) targetIAdminTreeChild.getParent();
				siteAdminAreaRelId = relationObj.getRelId();
				relationType = ApplicationRelationType.NOTFIXED.name();
				adminAreaName = ((AdministrationArea) relationObj.getRefObject()).getName();
			} else if (iAdminTreeChild instanceof AdminAreaUserAppNotFixed) {
				AdministrationArea administrationArea = (AdministrationArea) iAdminTreeChild.getParent().getParent();
				siteAdminAreaRelId = administrationArea.getRelationId();
				targetIAdminTreeChild = iAdminTreeChild;
				relationType = ApplicationRelationType.NOTFIXED.name();
				adminAreaName = administrationArea.getName();
			} else if (iAdminTreeChild instanceof AdminAreaUserAppFixed) {
				AdministrationArea administrationArea = (AdministrationArea) iAdminTreeChild.getParent().getParent();
				siteAdminAreaRelId = administrationArea.getRelationId();
				targetIAdminTreeChild = iAdminTreeChild;
				relationType = ApplicationRelationType.FIXED.name();
				adminAreaName = administrationArea.getName();
			} else if (iAdminTreeChild instanceof AdminAreaUserAppProtected) {
				AdministrationArea administrationArea = (AdministrationArea) iAdminTreeChild.getParent().getParent();
				siteAdminAreaRelId = administrationArea.getRelationId();
				targetIAdminTreeChild = iAdminTreeChild;
				relationType = ApplicationRelationType.PROTECTED.name();
				adminAreaName = administrationArea.getName();
			} else if (iAdminTreeChild instanceof AdminAreaUserApplications) {
				AdministrationArea adminArea = (AdministrationArea) iAdminTreeChild.getParent();
				siteAdminAreaRelId = adminArea.getRelationId();
				targetIAdminTreeChild = iAdminTreeChild;
				relationType = ApplicationRelationType.NOTFIXED.name();
				adminAreaName = adminArea.getName();
			} else if (iAdminTreeChild instanceof AdministrationArea) {
				siteAdminAreaRelId = ((AdministrationArea) iAdminTreeChild).getRelationId();
				Map<String, IAdminTreeChild> adminAreaChildrenForUserApp = ((AdministrationArea) iAdminTreeChild)
						.getAdminAreaChildren();
				targetIAdminTreeChild = adminAreaChildrenForUserApp.get(AdminAreaUserApplications.class.getName());
				relationType = ApplicationRelationType.NOTFIXED.name();
				adminAreaName = ((AdministrationArea) iAdminTreeChild).getName();
			}

			if (targetIAdminTreeChild != null && siteAdminAreaRelId == null) {
				IAdminTreeChild parent;
				if (targetIAdminTreeChild instanceof AdminAreaUserAppProtected
						|| targetIAdminTreeChild instanceof AdminAreaUserAppFixed
						|| targetIAdminTreeChild instanceof AdminAreaUserAppNotFixed) {
					parent = targetIAdminTreeChild.getParent().getParent();
				} else {
					parent = targetIAdminTreeChild.getParent();
				}
				if (parent instanceof AdministrationArea) {
					AdministrationArea adminArea = (AdministrationArea) parent;
					String name = adminArea.getName();
					CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
							messages.unauthorizedDialogTitle, messages.adminAreaDndErrorMsgPart1 + " \'" + name + "\' "
									+ messages.adminAreaDndErrorMsgPart2);
					return;
				}

			}
			final String siteAdminAreaRelId2 = siteAdminAreaRelId;
			final String relationType2 = relationType;
			final IAdminTreeChild targetIAdminTreeChild2 = targetIAdminTreeChild;
			final String adminAreaName2 = adminAreaName;
			if (siteAdminAreaRelId2 != null && relationType2 != null && targetIAdminTreeChild2 != null) {
				Job job = new Job("Creating Relations...") {

					@Override
					protected IStatus run(IProgressMonitor monitor) {
						monitor.beginTask("Creating Relations..", 100);
						monitor.worked(30);
						try {
							List<AdminAreaUserAppRelRequest> adminAreaUserAppRelRequestList = new ArrayList<>();
							for (UserApplication userApplication : userAppArray) {
								AdminAreaUserAppRelRequest adminAreaUserAppRelRequest = new AdminAreaUserAppRelRequest();
								adminAreaUserAppRelRequest.setSiteAdminAreaRelId(siteAdminAreaRelId2);
								adminAreaUserAppRelRequest.setUserAppId(userApplication.getUserApplicationId());
								adminAreaUserAppRelRequest.setRelationType(relationType2);
								adminAreaUserAppRelRequest.setStatus(com.magna.xmbackend.vo.enums.Status.ACTIVE.name());
								adminAreaUserAppRelRequestList.add(adminAreaUserAppRelRequest);
							}

							AdminAreaUserAppRelController adminAreaUserAppRelController = new AdminAreaUserAppRelController(
									XMAdminUtil.getInstance().getAAForHeaders(targetIAdminTreeChild2));
							AdminAreaUserAppRelBatchResponse adminAreaUserAppRelBatchResponse = adminAreaUserAppRelController
									.createAdminAreaUserAppRel(
											new AdminAreaUserAppRelBatchRequest(adminAreaUserAppRelRequestList));

							if (adminAreaUserAppRelBatchResponse != null) {
								boolean isRelationCreated = false;
								Iterable<AdminAreaUserAppRelTbl> adminAreaUserAppRelTbls = adminAreaUserAppRelBatchResponse
										.getAdminAreaUserAppRelTbls();
								List<String> successObjList = new ArrayList<>();
								for (AdminAreaUserAppRelTbl adminAreaUserAppRelTbl : adminAreaUserAppRelTbls) {
									String relId = adminAreaUserAppRelTbl.getAdminAreaUserAppRelId();
									if (relId != null) {
										for (UserApplication userApplication : userAppArray) {
											if (userApplication.getUserApplicationId().equals(adminAreaUserAppRelTbl
													.getUserApplicationId().getUserApplicationId())) {
												successObjList.add(userApplication.getName());
												isRelationCreated = true;
											}
										}
									}
								}
								if (isRelationCreated) {
									Display.getDefault().asyncExec(new Runnable() {
										public void run() {
											viewer.refresh(targetIAdminTreeChild2);
											if (!viewer.getExpandedState(targetIAdminTreeChild2.getParent())) {
												(new AdminTreeExpansion(targetIAdminTreeChild2.getParent()))
														.loadObjects();
												viewer.setExpandedState(targetIAdminTreeChild2.getParent(), true);
											}
											viewer.setExpandedState(targetIAdminTreeChild2, true);
											if (isCopyPast && !(iAdminTreeChild instanceof SiteAdministrationChild
													|| iAdminTreeChild instanceof AdministrationArea)) {
												final AdminTreeviewer adminTree = XMAdminUtil.getInstance()
														.getAdminTree();
												adminTree.setSelection(new StructuredSelection(iAdminTreeChild));
											}
										}
									});
								}
								List<Map<String, String>> statusMapList = adminAreaUserAppRelBatchResponse
										.getStatusMap();
								int noOfObjRelCreated = userAppArray.length;
								final int noOfObjRelCreated2;
								if (statusMapList != null && !statusMapList.isEmpty()) {
									noOfObjRelCreated = userAppArray.length - statusMapList.size();
									Display.getDefault().asyncExec(new Runnable() {
										public void run() {
											String errorMessage = "";
											for (Map<String, String> statusMap : statusMapList) {
												errorMessage += statusMap.get(
														XMAdminUtil.getInstance().getCurrentLocaleEnum().getLangCode());
												errorMessage += "\n";
											}
											if (errorMessage.length() > 0) {
												CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
														messages.dndErrorTitle, errorMessage);
											}
										}
									});
								}
								noOfObjRelCreated2 = noOfObjRelCreated;
								if (noOfObjRelCreated2 > 0) {
									for (String objName : successObjList) {
										XMAdminUtil.getInstance()
												.updateLogFile(messages.relationLbl + " " + messages.betweenLbl + " "
														+ messages.userApplicationObject + " '" + objName + "' "
														+ messages.andLbl + " " + messages.administrationAreaObject
														+ " '" + adminAreaName2 + "' " + messages.objectCreate,
														MessageType.SUCCESS);
									}
									Display.getDefault().asyncExec(new Runnable() {
										@Override
										public void run() {
											eventBroker.send(CommonConstants.EVENT_BROKER.STATUSBAR,
													noOfObjRelCreated2 + " " + messages.dndStatusBarMsg);
										}
									});
								}
								if (statusMapList != null && !statusMapList.isEmpty()) {
									for (Map<String, String> statusMap : statusMapList) {
										String errorMessage = statusMap
												.get(XMAdminUtil.getInstance().getCurrentLocaleEnum().getLangCode());
										XMAdminUtil.getInstance().updateLogFile(errorMessage, MessageType.FAILURE);
									}
								}
							}
						} catch (UnauthorizedAccessException e) {
							Display.getDefault().asyncExec(new Runnable() {
								@Override
								public void run() {
									CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
											messages.unauthorizedDialogTitle, messages.unauthorizedDialogMsg);
								}
							});
						} catch (CannotCreateObjectException e) {
							Display.getDefault().asyncExec(new Runnable() {
								@Override
								public void run() {
									CustomMessageDialog.openError(Display.getDefault().getActiveShell(), "caxAdminMenu",
											"Same object exists");
								}
							});
						} catch (Exception e) {
							if (e instanceof ResourceAccessException) {
								Display.getDefault().asyncExec(new Runnable() {
									@Override
									public void run() {
										CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
												messages.errorDialogTitile, messages.serverNotReachable);
									}
								});
							}
							LOGGER.error("Exception occured " + e);
						}
						monitor.worked(70);
						return Status.OK_STATUS;
					}
				};
				job.setUser(true);
				job.schedule();
			}
		}
	}

	/**
	 * Method for Project app to site admin area project DND rel.
	 *
	 * @param projectAppArray
	 *            {@link ProjectApplication[]}
	 * @param iAdminTreeChild
	 *            {@link IAdminTreeChild}
	 * @param isCopyPast
	 */
	private void projectAppToSiteAdminAreaProjectDNDRel(final ProjectApplication[] projectAppArray,
			final IAdminTreeChild iAdminTreeChild, boolean isCopyPast) {
		if (iAdminTreeChild != null && projectAppArray != null && projectAppArray.length > 0) {
			String siteAdminAreaProjectRelId = null;
			String relationType = null;
			IAdminTreeChild targetIAdminTreeChild = null;
			String projectName = null;
			String adminAreaName = null;
			if (iAdminTreeChild instanceof SiteAdminProjectAppNotFixed
					|| iAdminTreeChild instanceof AdminAreaProjectAppNotFixed) {
				siteAdminAreaProjectRelId = ((RelationObj) iAdminTreeChild.getParent().getParent()).getRelId();
				targetIAdminTreeChild = iAdminTreeChild;
				relationType = ApplicationRelationType.NOTFIXED.name();
				projectName = ((Project) ((RelationObj) iAdminTreeChild.getParent().getParent()).getRefObject())
						.getName();
			} else if (iAdminTreeChild instanceof SiteAdminProjectAppFixed
					|| iAdminTreeChild instanceof AdminAreaProjectAppFixed) {
				siteAdminAreaProjectRelId = ((RelationObj) iAdminTreeChild.getParent().getParent()).getRelId();
				targetIAdminTreeChild = iAdminTreeChild;
				relationType = ApplicationRelationType.FIXED.name();
				projectName = ((Project) ((RelationObj) iAdminTreeChild.getParent().getParent()).getRefObject())
						.getName();
			} else if (iAdminTreeChild instanceof SiteAdminProjectAppProtected
					|| iAdminTreeChild instanceof AdminAreaProjectAppProtected) {
				siteAdminAreaProjectRelId = ((RelationObj) iAdminTreeChild.getParent().getParent()).getRelId();
				targetIAdminTreeChild = iAdminTreeChild;
				relationType = ApplicationRelationType.PROTECTED.name();
				projectName = ((Project) ((RelationObj) iAdminTreeChild.getParent().getParent()).getRefObject())
						.getName();
			} else if (iAdminTreeChild instanceof SiteAdminAreaProjectApplications
					|| iAdminTreeChild instanceof AdminAreaProjectApplications) {
				siteAdminAreaProjectRelId = ((RelationObj) iAdminTreeChild.getParent()).getRelId();
				targetIAdminTreeChild = iAdminTreeChild;
				relationType = ApplicationRelationType.NOTFIXED.name();
				projectName = ((Project) ((RelationObj) iAdminTreeChild.getParent()).getRefObject()).getName();
			} else if (iAdminTreeChild instanceof SiteAdminAreaProjectChild) {
				Map<String, IAdminTreeChild> siteAdminAreaProjectChildren = ((SiteAdminAreaProjectChild) iAdminTreeChild)
						.getSiteAdminAreaProjectChildren();
				targetIAdminTreeChild = siteAdminAreaProjectChildren
						.get(SiteAdminAreaProjectApplications.class.getName());
				siteAdminAreaProjectRelId = ((RelationObj) targetIAdminTreeChild.getParent()).getRelId();
				relationType = ApplicationRelationType.NOTFIXED.name();
				projectName = ((Project) ((RelationObj) targetIAdminTreeChild.getParent()).getRefObject()).getName();
				adminAreaName = ((AdministrationArea) ((RelationObj) targetIAdminTreeChild.getParent().getParent()
						.getParent()).getRefObject()).getName();
			} else if (iAdminTreeChild instanceof AdminAreaProjectChild) {
				Map<String, IAdminTreeChild> adminAreaProjectChildren = ((AdminAreaProjectChild) iAdminTreeChild)
						.getAdminAreaProChildren();
				targetIAdminTreeChild = adminAreaProjectChildren.get(AdminAreaProjectApplications.class.getName());
				siteAdminAreaProjectRelId = ((RelationObj) targetIAdminTreeChild.getParent()).getRelId();
				relationType = ApplicationRelationType.NOTFIXED.name();
				projectName = ((Project) ((RelationObj) targetIAdminTreeChild.getParent()).getRefObject()).getName();
				adminAreaName = ((AdministrationArea) (targetIAdminTreeChild.getParent().getParent().getParent()))
						.getName();
			} else if (iAdminTreeChild instanceof ProjectAdminAreaProjectAppFixed) {
				siteAdminAreaProjectRelId = ((RelationObj) iAdminTreeChild.getParent().getParent()).getRelId();
				targetIAdminTreeChild = iAdminTreeChild;
				relationType = ApplicationRelationType.FIXED.name();
				projectName = ((Project) (iAdminTreeChild.getParent().getParent().getParent().getParent())).getName();
				adminAreaName = ((AdministrationArea) ((RelationObj) iAdminTreeChild.getParent().getParent())
						.getRefObject()).getName();
			} else if (iAdminTreeChild instanceof ProjectAdminAreaProjectAppNotFixed) {
				siteAdminAreaProjectRelId = ((RelationObj) iAdminTreeChild.getParent().getParent()).getRelId();
				targetIAdminTreeChild = iAdminTreeChild;
				relationType = ApplicationRelationType.NOTFIXED.name();
				projectName = ((Project) (iAdminTreeChild.getParent().getParent().getParent().getParent())).getName();
				adminAreaName = ((AdministrationArea) ((RelationObj) iAdminTreeChild.getParent().getParent())
						.getRefObject()).getName();
			} else if (iAdminTreeChild instanceof ProjectAdminAreaProjectAppProtected) {
				siteAdminAreaProjectRelId = ((RelationObj) iAdminTreeChild.getParent().getParent()).getRelId();
				targetIAdminTreeChild = iAdminTreeChild;
				relationType = ApplicationRelationType.PROTECTED.name();
				projectName = ((Project) (iAdminTreeChild.getParent().getParent().getParent().getParent())).getName();
				adminAreaName = ((AdministrationArea) ((RelationObj) iAdminTreeChild.getParent().getParent())
						.getRefObject()).getName();
			} else if (iAdminTreeChild instanceof ProjectAdminAreaProjectApplications) {
				siteAdminAreaProjectRelId = ((RelationObj) iAdminTreeChild.getParent()).getRelId();
				targetIAdminTreeChild = iAdminTreeChild;
				relationType = ApplicationRelationType.NOTFIXED.name();
				projectName = ((Project) (iAdminTreeChild.getParent().getParent().getParent())).getName();
				adminAreaName = ((AdministrationArea) ((RelationObj) iAdminTreeChild.getParent()).getRefObject())
						.getName();
			} else if (iAdminTreeChild instanceof ProjectAdminAreaChild) {
				Map<String, IAdminTreeChild> ProjectAdminAreaChildren = ((ProjectAdminAreaChild) iAdminTreeChild)
						.getProjectAdminAreaChildrenChildren();
				targetIAdminTreeChild = ProjectAdminAreaChildren
						.get(ProjectAdminAreaProjectApplications.class.getName());
				siteAdminAreaProjectRelId = ((RelationObj) targetIAdminTreeChild.getParent()).getRelId();
				relationType = ApplicationRelationType.NOTFIXED.name();
				projectName = ((Project) (targetIAdminTreeChild.getParent().getParent().getParent())).getName();
				adminAreaName = ((AdministrationArea) ((RelationObj) targetIAdminTreeChild.getParent()).getRefObject())
						.getName();
			}

			if (iAdminTreeChild instanceof SiteAdminProjectAppNotFixed
					|| iAdminTreeChild instanceof SiteAdminProjectAppFixed
					|| iAdminTreeChild instanceof SiteAdminProjectAppProtected) {

				adminAreaName = ((AdministrationArea) ((RelationObj) iAdminTreeChild.getParent().getParent().getParent()
						.getParent()).getRefObject()).getName();
			} else if (iAdminTreeChild instanceof SiteAdminAreaProjectApplications) {
				adminAreaName = ((AdministrationArea) ((RelationObj) iAdminTreeChild.getParent().getParent()
						.getParent()).getRefObject()).getName();
			} else if (iAdminTreeChild instanceof AdminAreaProjectApplications) {
				adminAreaName = ((AdministrationArea) (iAdminTreeChild.getParent().getParent().getParent())).getName();
			} else if (iAdminTreeChild instanceof AdminAreaProjectAppNotFixed
					|| iAdminTreeChild instanceof AdminAreaProjectAppFixed
					|| iAdminTreeChild instanceof AdminAreaProjectAppProtected) {
				adminAreaName = ((AdministrationArea) (iAdminTreeChild.getParent().getParent().getParent().getParent()))
						.getName();
			}

			final String siteAdminAreaProjectRelId2 = siteAdminAreaProjectRelId;
			final String relationType2 = relationType;
			final IAdminTreeChild targetIAdminTreeChild2 = targetIAdminTreeChild;
			final String projectName2 = projectName;
			final String adminAreaName2 = adminAreaName;
			if (siteAdminAreaProjectRelId2 != null && relationType2 != null && targetIAdminTreeChild2 != null) {
				Job job = new Job("Creating Relations...") {

					@Override
					protected IStatus run(IProgressMonitor monitor) {
						monitor.beginTask("Creating Relations..", 100);
						monitor.worked(30);
						try {
							List<AdminAreaProjectAppRelRequest> adminAreaProjectAppRelRequestList = new ArrayList<>();
							for (ProjectApplication projectApplication : projectAppArray) {
								AdminAreaProjectAppRelRequest adminAreaProjectAppRelRequest = new AdminAreaProjectAppRelRequest();
								adminAreaProjectAppRelRequest.setAdminAreaProjectRelId(siteAdminAreaProjectRelId2);
								adminAreaProjectAppRelRequest
										.setProjectAppId(projectApplication.getProjectApplicationId());
								adminAreaProjectAppRelRequest.setRelationType(relationType2);
								adminAreaProjectAppRelRequest
										.setStatus(com.magna.xmbackend.vo.enums.Status.ACTIVE.name());
								adminAreaProjectAppRelRequestList.add(adminAreaProjectAppRelRequest);
							}

							AdminAreaProjectAppRelController adminAreaProjectAppRelController = new AdminAreaProjectAppRelController(
									XMAdminUtil.getInstance().getAAForHeaders(targetIAdminTreeChild2));
							AdminAreaProjectAppRelBatchResponse adminAreaProjectAppRelBatchResponse = adminAreaProjectAppRelController
									.createAdminAreaProjAppRel(
											new AdminAreaProjectAppRelBatchRequest(adminAreaProjectAppRelRequestList));

							if (adminAreaProjectAppRelBatchResponse != null) {
								boolean isRelationCreated = false;
								Iterable<AdminAreaProjAppRelTbl> adminAreaProjectAppRelTbls = adminAreaProjectAppRelBatchResponse
										.getAdminAreaProjectAppRelTbls();
								List<String> successObjList = new ArrayList<>();
								for (AdminAreaProjAppRelTbl adminAreaProjectAppRelTbl : adminAreaProjectAppRelTbls) {
									String relId = adminAreaProjectAppRelTbl.getAdminAreaProjAppRelId();
									if (relId != null) {
										for (ProjectApplication projectApplication : projectAppArray) {
											if (projectApplication.getProjectApplicationId()
													.equals(adminAreaProjectAppRelTbl.getProjectApplicationId()
															.getProjectApplicationId())) {
												successObjList.add(projectApplication.getName());
												isRelationCreated = true;
											}
										}
									}
								}
								if (isRelationCreated) {
									Display.getDefault().syncExec(new Runnable() {
										@SuppressWarnings("unchecked")
										public void run() {
											viewer.refresh(targetIAdminTreeChild2);
											viewer.setExpandedState(targetIAdminTreeChild2.getParent(), true);
											viewer.setExpandedState(targetIAdminTreeChild2, true);
											final AdminTreeviewer adminTree = XMAdminUtil.getInstance().getAdminTree();
											adminTree.refershBackReference(new Class[] { ProjectAppAdminAreas.class });
											if (isCopyPast && !(iAdminTreeChild instanceof SiteAdminAreaProjectChild
													|| iAdminTreeChild instanceof AdminAreaProjectChild)) {
												adminTree.setSelection(new StructuredSelection(iAdminTreeChild));
											}
										}
									});
								}
								List<Map<String, String>> statusMapList = adminAreaProjectAppRelBatchResponse
										.getStatusMap();
								int noOfObjRelCreated = projectAppArray.length;
								final int noOfObjRelCreated2;
								if (statusMapList != null && !statusMapList.isEmpty()) {
									noOfObjRelCreated = projectAppArray.length - statusMapList.size();
									Display.getDefault().asyncExec(new Runnable() {
										public void run() {
											String errorMessage = "";
											for (Map<String, String> statusMap : statusMapList) {
												errorMessage += statusMap.get(
														XMAdminUtil.getInstance().getCurrentLocaleEnum().getLangCode());
												errorMessage += "\n";
											}
											if (errorMessage.length() > 0) {
												CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
														messages.dndErrorTitle, errorMessage);
											}
										}
									});
								}
								noOfObjRelCreated2 = noOfObjRelCreated;
								if (noOfObjRelCreated2 > 0) {
									for (String objName : successObjList) {
										XMAdminUtil.getInstance().updateLogFile(messages.relationLbl + " "
												+ messages.betweenLbl + " " + messages.projectApplicationObject + " '"
												+ objName + "' " + messages.andLbl + " " + messages.projectObject + " '"
												+ projectName2 + "' " + messages.withLbl + " "
												+ messages.administrationAreaObject + " '" + adminAreaName2 + "' "
												+ messages.objectCreate, MessageType.SUCCESS);
									}
									Display.getDefault().asyncExec(new Runnable() {
										@Override
										public void run() {
											eventBroker.send(CommonConstants.EVENT_BROKER.STATUSBAR,
													noOfObjRelCreated2 + " " + messages.dndStatusBarMsg);
										}
									});
								}
								if (statusMapList != null && !statusMapList.isEmpty()) {
									for (Map<String, String> statusMap : statusMapList) {
										String errorMessage = statusMap
												.get(XMAdminUtil.getInstance().getCurrentLocaleEnum().getLangCode());
										XMAdminUtil.getInstance().updateLogFile(errorMessage, MessageType.FAILURE);
									}
								}
							}
						} catch (UnauthorizedAccessException e) {
							Display.getDefault().asyncExec(new Runnable() {
								@Override
								public void run() {
									CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
											messages.unauthorizedDialogTitle, messages.unauthorizedDialogMsg);
								}
							});
						} catch (Exception e) {
							if (e instanceof ResourceAccessException) {
								Display.getDefault().asyncExec(new Runnable() {
									@Override
									public void run() {
										CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
												messages.errorDialogTitile, messages.serverNotReachable);
									}
								});
							}
							LOGGER.error(e.getMessage());
						}

						monitor.worked(70);
						return Status.OK_STATUS;
					}
				};
				job.setUser(true);
				job.schedule();
			}
		}
	}

	/**
	 * Method for Start app to site admin area DND rel.
	 *
	 * @param startAppArray
	 *            {@link StartApplication[]}
	 * @param iAdminTreeChild
	 *            {@link IAdminTreeChild}
	 * @param isCopyPast
	 */
	private void startAppToSiteAdminAreaDNDRel(final StartApplication[] startAppArray,
			final IAdminTreeChild iAdminTreeChild, boolean isCopyPast) {
		String siteAdminAreaRelId = null;
		IAdminTreeChild targetIAdminTreeChild = null;
		String adminAreaName = null;
		if (iAdminTreeChild instanceof SiteAdminAreaStartApplications) {
			RelationObj relationObj = (RelationObj) iAdminTreeChild.getParent();
			targetIAdminTreeChild = iAdminTreeChild;
			siteAdminAreaRelId = relationObj.getRelId();
			adminAreaName = ((AdministrationArea) relationObj.getRefObject()).getName();
		} else if (iAdminTreeChild instanceof SiteAdministrationChild) {
			Map<String, IAdminTreeChild> siteAdminAreaChildren = ((SiteAdministrationChild) iAdminTreeChild)
					.getSiteAdminAreaChildren();
			targetIAdminTreeChild = siteAdminAreaChildren.get(SiteAdminAreaStartApplications.class.getName());
			RelationObj relationObj = (RelationObj) targetIAdminTreeChild.getParent();
			siteAdminAreaRelId = relationObj.getRelId();
			adminAreaName = ((AdministrationArea) relationObj.getRefObject()).getName();
		} else if (iAdminTreeChild instanceof AdminAreaStartApplications) {
			targetIAdminTreeChild = iAdminTreeChild;
			AdministrationArea administrationArea = (AdministrationArea) iAdminTreeChild.getParent();
			siteAdminAreaRelId = administrationArea.getRelationId();
			adminAreaName = administrationArea.getName();
		} else if (iAdminTreeChild instanceof AdministrationArea) {
			Map<String, IAdminTreeChild> adminAreaChildren = ((AdministrationArea) iAdminTreeChild)
					.getAdminAreaChildren();
			targetIAdminTreeChild = adminAreaChildren.get(AdminAreaStartApplications.class.getName());
			siteAdminAreaRelId = ((AdministrationArea) targetIAdminTreeChild.getParent()).getRelationId();
			adminAreaName = ((AdministrationArea) targetIAdminTreeChild.getParent()).getName();
		}
		if (targetIAdminTreeChild != null && siteAdminAreaRelId == null) {
			IAdminTreeChild parent = targetIAdminTreeChild.getParent();
			if (parent instanceof AdministrationArea) {
				AdministrationArea adminArea = (AdministrationArea) parent;
				String name = adminArea.getName();
				CustomMessageDialog.openError(Display.getDefault().getActiveShell(), messages.unauthorizedDialogTitle,
						messages.adminAreaDndErrorMsgPart1 + " \'" + name + "\' " + messages.adminAreaDndErrorMsgPart2);
				return;
			}
		}
		final String siteAdminAreaRelId2 = siteAdminAreaRelId;
		final IAdminTreeChild targetIAdminTreeChild2 = targetIAdminTreeChild;
		final String adminAreaName2 = adminAreaName;
		if (siteAdminAreaRelId2 != null && targetIAdminTreeChild2 != null) {
			Job job = new Job("Creating Relations...") {

				@Override
				protected IStatus run(IProgressMonitor monitor) {
					monitor.beginTask("Creating Relations..", 100);
					monitor.worked(30);
					try {
						List<AdminAreaStartAppRelRequest> adminAreaStartAppRelRequestList = new ArrayList<>();
						for (StartApplication startApp : startAppArray) {
							AdminAreaStartAppRelRequest adminAreaStartAppRelRequest = new AdminAreaStartAppRelRequest();
							adminAreaStartAppRelRequest.setSiteAdminAreaRelId(siteAdminAreaRelId2);
							adminAreaStartAppRelRequest.setStartAppId(startApp.getStartPrgmApplicationId());
							adminAreaStartAppRelRequest.setStatus(com.magna.xmbackend.vo.enums.Status.ACTIVE.name());
							adminAreaStartAppRelRequestList.add(adminAreaStartAppRelRequest);
						}
						AdminAreaStartAppRelController adminAreaStartAppRelController = new AdminAreaStartAppRelController(
								XMAdminUtil.getInstance().getAAForHeaders(targetIAdminTreeChild2));
						AdminAreaStartAppRelBatchResponse adminAreaStartAppRelBatchResponse = adminAreaStartAppRelController
								.createAdminAreaStartAppRel(
										new AdminAreaStartAppRelBatchRequest(adminAreaStartAppRelRequestList));

						if (adminAreaStartAppRelBatchResponse != null) {
							boolean isRelationCreated = false;
							Iterable<AdminAreaStartAppRelTbl> adminAreaStartAppRelTbls = adminAreaStartAppRelBatchResponse
									.getAdminAreaStartAppRelTbls();
							List<String> successObjList = new ArrayList<>();
							for (AdminAreaStartAppRelTbl adminAreaStartAppRelTbl : adminAreaStartAppRelTbls) {
								String relId = adminAreaStartAppRelTbl.getAdminAreaStartAppRelId();
								if (relId != null) {
									for (StartApplication startApp : startAppArray) {
										if (startApp.getStartPrgmApplicationId().equals(adminAreaStartAppRelTbl
												.getStartApplicationId().getStartApplicationId())) {
											successObjList.add(startApp.getName());
											isRelationCreated = true;
										}
									}
								}
							}
							if (isRelationCreated) {
								Display.getDefault().asyncExec(new Runnable() {
									public void run() {
										viewer.refresh(targetIAdminTreeChild2);
										if (!viewer.getExpandedState(targetIAdminTreeChild2.getParent())) {
											(new AdminTreeExpansion(targetIAdminTreeChild2.getParent())).loadObjects();
											viewer.setExpandedState(targetIAdminTreeChild2.getParent(), true);
										}
										viewer.setExpandedState(targetIAdminTreeChild2, true);
										if (isCopyPast && (iAdminTreeChild instanceof SiteAdminAreaStartApplications
												|| iAdminTreeChild instanceof AdminAreaStartApplications)) {
											final AdminTreeviewer adminTree = XMAdminUtil.getInstance().getAdminTree();
											adminTree.setSelection(new StructuredSelection(iAdminTreeChild));
										}
									}
								});
							}
							List<Map<String, String>> statusMapList = adminAreaStartAppRelBatchResponse.getStatusMap();
							int noOfObjRelCreated = startAppArray.length;
							final int noOfObjRelCreated2;
							if (statusMapList != null && !statusMapList.isEmpty()) {
								noOfObjRelCreated = startAppArray.length - statusMapList.size();
								Display.getDefault().asyncExec(new Runnable() {
									public void run() {
										String errorMessage = "";
										for (Map<String, String> statusMap : statusMapList) {
											errorMessage += statusMap.get(
													XMAdminUtil.getInstance().getCurrentLocaleEnum().getLangCode());
											errorMessage += "\n";
										}
										if (errorMessage.length() > 0) {
											CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
													messages.dndErrorTitle, errorMessage);
										}
									}
								});
							}
							noOfObjRelCreated2 = noOfObjRelCreated;
							if (noOfObjRelCreated2 > 0) {
								for (String objName : successObjList) {
									XMAdminUtil.getInstance().updateLogFile(
											messages.relationLbl + " " + messages.betweenLbl + " "
													+ messages.startApplicationObject + " '" + objName + "' "
													+ messages.andLbl + " " + messages.administrationAreaObject + " '"
													+ adminAreaName2 + "' " + messages.objectCreate,
											MessageType.SUCCESS);
								}
								Display.getDefault().asyncExec(new Runnable() {
									@Override
									public void run() {
										eventBroker.send(CommonConstants.EVENT_BROKER.STATUSBAR,
												noOfObjRelCreated2 + " " + messages.dndStatusBarMsg);
									}
								});
							}
							if (statusMapList != null && !statusMapList.isEmpty()) {
								for (Map<String, String> statusMap : statusMapList) {
									String errorMessage = statusMap
											.get(XMAdminUtil.getInstance().getCurrentLocaleEnum().getLangCode());
									XMAdminUtil.getInstance().updateLogFile(errorMessage, MessageType.FAILURE);
								}
							}
						}
					} catch (UnauthorizedAccessException e) {
						Display.getDefault().asyncExec(new Runnable() {
							@Override
							public void run() {
								CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
										messages.unauthorizedDialogTitle, messages.unauthorizedDialogMsg);
							}
						});
					} catch (Exception e) {
						if (e instanceof ResourceAccessException) {
							Display.getDefault().asyncExec(new Runnable() {
								@Override
								public void run() {
									CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
											messages.errorDialogTitile, messages.serverNotReachable);
								}
							});
						}
						LOGGER.error(e.getMessage());
					}

					monitor.worked(70);
					return Status.OK_STATUS;
				}
			};
			job.setUser(true);
			job.schedule();
		}
	}

	/**
	 * Method for Start app to site admin area project DND rel.
	 *
	 * @param startAppArray
	 *            {@link StartApplication[]}
	 * @param iAdminTreeChild
	 *            {@link IAdminTreeChild}
	 * @param isCopyPast
	 */
	private void startAppToSiteAdminAreaProjectDNDRel(final StartApplication[] startAppArray,
			final IAdminTreeChild iAdminTreeChild, boolean isCopyPast) {
		String siteAdminAreaProjectRelId = null;
		IAdminTreeChild targetIAdminTreeChild = null;
		String projectName = null;
		String adminAreaName = null;
		if (iAdminTreeChild instanceof SiteAdminAreaProjectStartApplications
				|| iAdminTreeChild instanceof AdminAreaProjectStartApplications) {
			targetIAdminTreeChild = iAdminTreeChild;
			siteAdminAreaProjectRelId = ((RelationObj) iAdminTreeChild.getParent()).getRelId();
			projectName = ((Project) ((RelationObj) targetIAdminTreeChild.getParent()).getRefObject()).getName();

			if (iAdminTreeChild instanceof SiteAdminAreaProjectStartApplications) {
				adminAreaName = ((AdministrationArea) ((RelationObj) iAdminTreeChild.getParent().getParent()
						.getParent()).getRefObject()).getName();
			} else {
				adminAreaName = ((AdministrationArea) (iAdminTreeChild.getParent().getParent().getParent())).getName();
			}
		} else if (iAdminTreeChild instanceof SiteAdminAreaProjectChild) {
			Map<String, IAdminTreeChild> siteAdminAreaProjectChildren = ((SiteAdminAreaProjectChild) iAdminTreeChild)
					.getSiteAdminAreaProjectChildren();
			targetIAdminTreeChild = siteAdminAreaProjectChildren
					.get(SiteAdminAreaProjectStartApplications.class.getName());
			siteAdminAreaProjectRelId = ((RelationObj) targetIAdminTreeChild.getParent()).getRelId();
			projectName = ((Project) ((RelationObj) targetIAdminTreeChild.getParent()).getRefObject()).getName();
			adminAreaName = ((AdministrationArea) ((RelationObj) iAdminTreeChild.getParent().getParent()).getRefObject())
					.getName();
		} else if (iAdminTreeChild instanceof AdminAreaProjectChild) {
			Map<String, IAdminTreeChild> adminAreaProjectChildren = ((AdminAreaProjectChild) iAdminTreeChild)
					.getAdminAreaProChildren();
			targetIAdminTreeChild = adminAreaProjectChildren.get(AdminAreaProjectStartApplications.class.getName());
			siteAdminAreaProjectRelId = ((RelationObj) targetIAdminTreeChild.getParent()).getRelId();
			projectName = ((Project) ((RelationObj) targetIAdminTreeChild.getParent()).getRefObject()).getName();
			adminAreaName = ((AdministrationArea)iAdminTreeChild.getParent().getParent()).getName();
		}

		final String siteAdminAreaProjectRelId2 = siteAdminAreaProjectRelId;
		final IAdminTreeChild targetIAdminTreeChild2 = targetIAdminTreeChild;
		final String projectName2 = projectName;
		final String adminAreaName2 = adminAreaName;
		if (siteAdminAreaProjectRelId2 != null && targetIAdminTreeChild2 != null) {
			Job job = new Job("Creating Relations...") {

				@Override
				protected IStatus run(IProgressMonitor monitor) {
					monitor.beginTask("Creating Relations..", 100);
					monitor.worked(30);
					try {
						List<ProjectStartAppRelRequest> projectStartAppRelRequestList = new ArrayList<>();
						for (StartApplication startApp : startAppArray) {
							ProjectStartAppRelRequest projectStartAppRelRequest = new ProjectStartAppRelRequest();
							projectStartAppRelRequest.setAdminAreaProjectRelId(siteAdminAreaProjectRelId2);
							projectStartAppRelRequest.setStartAppId(startApp.getStartPrgmApplicationId());
							projectStartAppRelRequest.setStatus(com.magna.xmbackend.vo.enums.Status.ACTIVE.name());
							projectStartAppRelRequestList.add(projectStartAppRelRequest);
						}

						ProjectStartAppRelController projectStartAppRelController = new ProjectStartAppRelController(
								XMAdminUtil.getInstance().getAAForHeaders(targetIAdminTreeChild2));
						ProjectStartAppRelBatchResponse projectStartAppRelBatchResponse = projectStartAppRelController
								.createProjectStartAppRel(
										new ProjectStartAppRelBatchRequest(projectStartAppRelRequestList));

						if (projectStartAppRelBatchResponse != null) {
							boolean isRelationCreated = false;
							Iterable<ProjectStartAppRelTbl> projectStartAppRelTbls = projectStartAppRelBatchResponse
									.getProjectStartAppRelTbls();
							List<String> successObjList = new ArrayList<>();
							for (ProjectStartAppRelTbl projectStartAppRelTbl : projectStartAppRelTbls) {
								String relId = projectStartAppRelTbl.getProjectStartAppRelId();
								if (relId != null) {
									for (StartApplication startApp : startAppArray) {
										if (startApp.getStartPrgmApplicationId().equals(projectStartAppRelTbl
												.getStartApplicationId().getStartApplicationId())) {
											successObjList.add(startApp.getName());
											isRelationCreated = true;
										}
									}
								}
							}
							if (isRelationCreated) {
								Display.getDefault().asyncExec(new Runnable() {
									public void run() {
										viewer.refresh(targetIAdminTreeChild2);
										viewer.setExpandedState(targetIAdminTreeChild2.getParent(), true);
										viewer.setExpandedState(targetIAdminTreeChild2, true);
										if (isCopyPast
												&& (iAdminTreeChild instanceof SiteAdminAreaProjectStartApplications
														|| iAdminTreeChild instanceof AdminAreaProjectStartApplications)) {
											final AdminTreeviewer adminTree = XMAdminUtil.getInstance().getAdminTree();
											adminTree.setSelection(new StructuredSelection(iAdminTreeChild));
										}
									}
								});
							}
							List<Map<String, String>> statusMapList = projectStartAppRelBatchResponse.getStatusMap();
							int noOfObjRelCreated = startAppArray.length;
							final int noOfObjRelCreated2;
							if (statusMapList != null && !statusMapList.isEmpty()) {
								noOfObjRelCreated = startAppArray.length - statusMapList.size();
								Display.getDefault().asyncExec(new Runnable() {
									public void run() {
										String errorMessage = "";
										for (Map<String, String> statusMap : statusMapList) {
											errorMessage += statusMap.get(
													XMAdminUtil.getInstance().getCurrentLocaleEnum().getLangCode());
											errorMessage += "\n";
										}
										if (errorMessage.length() > 0) {
											CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
													messages.dndErrorTitle, errorMessage);
										}
									}
								});
							}
							noOfObjRelCreated2 = noOfObjRelCreated;
							if (noOfObjRelCreated2 > 0) {
								for (String objName : successObjList) {
									XMAdminUtil.getInstance()
											.updateLogFile(messages.relationLbl + " " + messages.betweenLbl + " "
													+ messages.startApplicationObject + " '" + objName + "' "
													+ messages.andLbl + " " + messages.projectObject + " '"
													+ projectName2 + "' " + messages.withLbl + " "
													+ messages.administrationAreaObject + " '" + adminAreaName2 + "' "
													+ messages.objectCreate, MessageType.SUCCESS);
								}
								Display.getDefault().asyncExec(new Runnable() {
									@Override
									public void run() {
										eventBroker.send(CommonConstants.EVENT_BROKER.STATUSBAR,
												noOfObjRelCreated2 + " " + messages.dndStatusBarMsg);
									}
								});
							}
							if (statusMapList != null && !statusMapList.isEmpty()) {
								for (Map<String, String> statusMap : statusMapList) {
									String errorMessage = statusMap
											.get(XMAdminUtil.getInstance().getCurrentLocaleEnum().getLangCode());
									XMAdminUtil.getInstance().updateLogFile(errorMessage, MessageType.FAILURE);
								}
							}
							/*
							 * noOfObjRelCreated2 = noOfObjRelCreated;
							 * XMAdminUtil.getInstance().updateLogFile(
							 * noOfObjRelCreated+" "
							 * +"Site-AdministrationArea-Project-StartApplications"+
							 * " "+"relations created successfully!");
							 * Display.getDefault().asyncExec(new Runnable() {
							 * 
							 * @Override public void run() {
							 * eventBroker.send(CommonConstants.EVENT_BROKER.
							 * STATUSBAR, noOfObjRelCreated2 + " " +
							 * messages.dndStatusBarMsg); } });
							 */
						}
					} catch (UnauthorizedAccessException e) {
						Display.getDefault().asyncExec(new Runnable() {
							@Override
							public void run() {
								CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
										messages.unauthorizedDialogTitle, messages.unauthorizedDialogMsg);
							}
						});
					} catch (Exception e) {
						if (e instanceof ResourceAccessException) {
							Display.getDefault().asyncExec(new Runnable() {
								@Override
								public void run() {
									CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
											messages.errorDialogTitile, messages.serverNotReachable);
								}
							});
						}
						LOGGER.error(e.getMessage());
					}

					monitor.worked(70);
					return Status.OK_STATUS;
				}
			};
			job.setUser(true);
			job.schedule();
		}
	}

	/**
	 * Method for Start app to user DND rel.
	 *
	 * @param startAppArray
	 *            {@link StartApplication[]}
	 * @param iAdminTreeChild
	 *            {@link IAdminTreeChild}
	 * @param isCopyPast
	 */
	private void startAppToUserDNDRel(final StartApplication[] startAppArray, final IAdminTreeChild iAdminTreeChild,
			boolean isCopyPast) {
		String userId = null;
		IAdminTreeChild targetIAdminTreeChild = null;
		String userName = null;
		if (iAdminTreeChild instanceof UserStartApplications) {
			targetIAdminTreeChild = iAdminTreeChild;
			userId = ((User) iAdminTreeChild.getParent()).getUserId();
			userName = ((User) iAdminTreeChild.getParent()).getName();
		} else if (iAdminTreeChild instanceof User) {
			Map<String, IAdminTreeChild> userChildren = ((User) iAdminTreeChild).getUserChildren();
			targetIAdminTreeChild = userChildren.get(UserStartApplications.class.getName());
			userId = ((User) iAdminTreeChild).getUserId();
			userName = ((User) iAdminTreeChild).getName();
		}

		final String userId2 = userId;
		final IAdminTreeChild targetIAdminTreeChild2 = targetIAdminTreeChild;
		final String userName2 = userName;

		if (userId2 != null && targetIAdminTreeChild2 != null) {
			Job job = new Job("Creating Relations...") {

				@Override
				protected IStatus run(IProgressMonitor monitor) {
					monitor.beginTask("Creating Relations..", 100);
					monitor.worked(30);
					try {
						List<UserStartAppRelRequest> userStartAppRelRequestList = new ArrayList<>();
						for (StartApplication startApp : startAppArray) {
							UserStartAppRelRequest userStartAppRelRequest = new UserStartAppRelRequest();
							userStartAppRelRequest.setUserId(userId2);
							userStartAppRelRequest.setStartAppId(startApp.getStartPrgmApplicationId());
							userStartAppRelRequest.setStatus(com.magna.xmbackend.vo.enums.Status.ACTIVE.name());
							userStartAppRelRequestList.add(userStartAppRelRequest);
						}

						UserStartAppRelController userStartAppRelController = new UserStartAppRelController();
						UserStartAppRelBatchResponse userStartAppRelBatchResponse = userStartAppRelController
								.createUserStartAppRel(new UserStartAppRelBatchRequest(userStartAppRelRequestList));

						if (userStartAppRelBatchResponse != null) {
							boolean isRelationCreated = false;
							List<UserStartAppRelTbl> userStartectAppRelTbls = userStartAppRelBatchResponse
									.getUserStartectAppRelTbls();
							List<String> successObjList = new ArrayList<>();
							for (UserStartAppRelTbl userStartectAppRelTbl : userStartectAppRelTbls) {
								String relId = userStartectAppRelTbl.getUserStartAppRelId();
								if (relId != null) {
									for (StartApplication startApp : startAppArray) {
										if (startApp.getStartPrgmApplicationId().equals(userStartectAppRelTbl
												.getStartApplicationId().getStartApplicationId())) {
											successObjList.add(startApp.getName());
											isRelationCreated = true;
										}
									}
									/*
									 * isRelationCreated = true; break;
									 */
								}
							}
							if (isRelationCreated) {
								Display.getDefault().asyncExec(new Runnable() {
									public void run() {
										viewer.refresh(targetIAdminTreeChild2);
										viewer.setExpandedState(targetIAdminTreeChild2.getParent(), true);
										viewer.setExpandedState(targetIAdminTreeChild2, true);
										if (isCopyPast && iAdminTreeChild instanceof UserStartApplications) {
											final AdminTreeviewer adminTree = XMAdminUtil.getInstance().getAdminTree();
											adminTree.setSelection(new StructuredSelection(iAdminTreeChild));
										}
									}
								});
							}
							List<Map<String, String>> statusMapList = userStartAppRelBatchResponse.getStatusMap();
							int noOfObjRelCreated = startAppArray.length;
							final int noOfObjRelCreated2;
							if (statusMapList != null && !statusMapList.isEmpty()) {
								noOfObjRelCreated = startAppArray.length - statusMapList.size();
								Display.getDefault().asyncExec(new Runnable() {
									public void run() {
										String errorMessage = "";
										for (Map<String, String> statusMap : statusMapList) {
											errorMessage += statusMap.get(
													XMAdminUtil.getInstance().getCurrentLocaleEnum().getLangCode());
											errorMessage += "\n";
										}
										if (errorMessage.length() > 0) {
											CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
													messages.dndErrorTitle, errorMessage);
										}
									}
								});
							}
							noOfObjRelCreated2 = noOfObjRelCreated;
							if (noOfObjRelCreated2 > 0) {
								for (String objName : successObjList) {
									XMAdminUtil.getInstance().updateLogFile(messages.relationLbl + " "
											+ messages.betweenLbl + " " + messages.startApplicationObject + " '"
											+ objName + "' " + messages.andLbl + " " + messages.userObject + " '"
											+ userName2 + "' " + messages.objectCreate, MessageType.SUCCESS);
								}
								Display.getDefault().asyncExec(new Runnable() {
									@Override
									public void run() {
										eventBroker.send(CommonConstants.EVENT_BROKER.STATUSBAR,
												noOfObjRelCreated2 + " " + messages.dndStatusBarMsg);
									}
								});
							}
							if (statusMapList != null && !statusMapList.isEmpty()) {
								for (Map<String, String> statusMap : statusMapList) {
									String errorMessage = statusMap
											.get(XMAdminUtil.getInstance().getCurrentLocaleEnum().getLangCode());
									XMAdminUtil.getInstance().updateLogFile(errorMessage, MessageType.FAILURE);
								}
							}
						}
					} catch (UnauthorizedAccessException e) {
						Display.getDefault().asyncExec(new Runnable() {
							@Override
							public void run() {
								CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
										messages.unauthorizedDialogTitle, messages.unauthorizedDialogMsg);
							}
						});
					} catch (Exception e) {
						if (e instanceof ResourceAccessException) {
							Display.getDefault().asyncExec(new Runnable() {
								@Override
								public void run() {
									CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
											messages.errorDialogTitile, messages.serverNotReachable);
								}
							});
						}
						LOGGER.error(e.getMessage());
					}

					monitor.worked(70);
					return Status.OK_STATUS;
				}
			};
			job.setUser(true);
			job.schedule();
		}
	}

	/**
	 * Method for Update site admin area project app state DND rel.
	 *
	 * @param relationObjArray
	 *            {@link RelationObj[]}
	 * @param projectAppTypeNode
	 *            {@link IAdminTreeChild}
	 */
	private void updateSiteAdminAreaProjectAppStateDNDRel(RelationObj[] relationObjArray,
			IAdminTreeChild projectAppTypeNode) {
		if (projectAppTypeNode != null && relationObjArray != null && relationObjArray.length > 0) {
			IAdminTreeChild parent = projectAppTypeNode.getParent().getParent();
			String relationType = null;

			if (projectAppTypeNode instanceof SiteAdminProjectAppNotFixed
					|| projectAppTypeNode instanceof AdminAreaProjectAppNotFixed
					|| projectAppTypeNode instanceof ProjectAdminAreaProjectAppNotFixed) {
				relationType = ApplicationRelationType.NOTFIXED.name();
			} else if (projectAppTypeNode instanceof SiteAdminProjectAppFixed
					|| projectAppTypeNode instanceof AdminAreaProjectAppFixed
					|| projectAppTypeNode instanceof ProjectAdminAreaProjectAppFixed) {
				relationType = ApplicationRelationType.FIXED.name();
			} else if (projectAppTypeNode instanceof SiteAdminProjectAppProtected
					|| projectAppTypeNode instanceof AdminAreaProjectAppProtected
					|| projectAppTypeNode instanceof ProjectAdminAreaProjectAppProtected) {
				relationType = ApplicationRelationType.PROTECTED.name();
			}

			final String relationType2 = relationType;
			final String projectName;
			final String adminAreaName;
			if (projectAppTypeNode instanceof ProjectAdminAreaProjectAppNotFixed
					|| projectAppTypeNode instanceof ProjectAdminAreaProjectAppProtected
					|| projectAppTypeNode instanceof ProjectAdminAreaProjectAppFixed) {
				projectName = ((Project) (parent.getParent().getParent())).getName();
				adminAreaName = ((AdministrationArea) ((RelationObj) parent).getRefObject()).getName();
			} else if (projectAppTypeNode instanceof SiteAdminProjectAppNotFixed
					|| projectAppTypeNode instanceof SiteAdminProjectAppFixed
					|| projectAppTypeNode instanceof SiteAdminProjectAppProtected) {
				projectName = ((Project) ((RelationObj) parent).getRefObject()).getName();
				adminAreaName = ((AdministrationArea) ((RelationObj) parent.getParent().getParent()).getRefObject())
						.getName();
			} else {
				projectName = ((Project) ((RelationObj) parent).getRefObject()).getName();
				adminAreaName = ((AdministrationArea) (parent.getParent().getParent())).getName();
			}
			Job job = new Job("Creating Relations...") {

				@Override
				protected IStatus run(IProgressMonitor monitor) {
					monitor.beginTask("Creating Relations..", 100);
					monitor.worked(30);
					try {
						if (relationType2 != null) {
							List<AdminAreaProjectAppRelRequest> adminAreaProjectAppRelRequests = new ArrayList<AdminAreaProjectAppRelRequest>();

							for (RelationObj relationUserAppObj : relationObjArray) {
								String relId = relationUserAppObj.getRelId();
								AdminAreaProjectAppRelRequest adminAreaProjectAppRelRequest = new AdminAreaProjectAppRelRequest();
								adminAreaProjectAppRelRequest.setId(relId);
								adminAreaProjectAppRelRequest.setRelationType(relationType2);
								adminAreaProjectAppRelRequests.add(adminAreaProjectAppRelRequest);
							}

							AdminAreaProjectAppRelController adminAreaProjectAppRelController = new AdminAreaProjectAppRelController(
									XMAdminUtil.getInstance().getAAForHeaders(parent));
							AdminAreaProjectAppRelBatchResponse adminAreaProjectAppRelBatchResponse = adminAreaProjectAppRelController
									.updateRelTypesByIds(
											new AdminAreaProjectAppRelBatchRequest(adminAreaProjectAppRelRequests));

							if (adminAreaProjectAppRelBatchResponse != null) {
								List<AdminAreaProjAppRelTbl> adminAreaProjectAppRelTbls = adminAreaProjectAppRelBatchResponse
										.getAdminAreaProjectAppRelTbls();
								List<String> successObjList = new ArrayList<>();
								for (AdminAreaProjAppRelTbl adminAreaProjectAppRelTbl : adminAreaProjectAppRelTbls) {
									String relId = adminAreaProjectAppRelTbl.getAdminAreaProjAppRelId();
									if (relId != null) {
										for (RelationObj relObj : relationObjArray) {
											ProjectApplication projectApp = (ProjectApplication) relObj.getRefObject();
											if (projectApp.getProjectApplicationId().equals(adminAreaProjectAppRelTbl
													.getProjectApplicationId().getProjectApplicationId())) {
												successObjList.add(projectApp.getName());
											}
										}
									}
								}
								List<Map<String, String>> statusMapList = adminAreaProjectAppRelBatchResponse
										.getStatusMap();

								int noOfObjRelCreated = relationObjArray.length;
								final int noOfObjRelCreated2;
								if (statusMapList != null && !statusMapList.isEmpty()) {
									noOfObjRelCreated = relationObjArray.length - statusMapList.size();
									Display.getDefault().asyncExec(new Runnable() {
										public void run() {
											String errorMessage = "";
											for (Map<String, String> statusMap : statusMapList) {
												errorMessage += statusMap.get(
														XMAdminUtil.getInstance().getCurrentLocaleEnum().getLangCode());
												errorMessage += "\n";
											}
											if (errorMessage.length() > 0) {
												CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
														messages.dndErrorTitle, errorMessage);
											}
										}
									});
								}
								noOfObjRelCreated2 = noOfObjRelCreated;
								if (noOfObjRelCreated2 > 0) {
									for (String objName : successObjList) {
										XMAdminUtil.getInstance().updateLogFile(messages.relationLbl + " "
												+ messages.betweenLbl + " " + messages.projectApplicationObject + " '"
												+ objName + "' " + messages.andLbl + " " + messages.projectObject + " '"
												+ projectName + "' " + messages.withLbl + " "
												+ messages.administrationAreaObject + " '" + adminAreaName + "' "
												+ messages.objectUpdate, MessageType.SUCCESS);
									}
									Display.getDefault().asyncExec(new Runnable() {
										@Override
										public void run() {
											eventBroker.send(CommonConstants.EVENT_BROKER.STATUSBAR,
													noOfObjRelCreated2 + " " + messages.dndStatusBarMsg);
										}
									});
								}
								if (statusMapList != null && !statusMapList.isEmpty()) {
									for (Map<String, String> statusMap : statusMapList) {
										String errorMessage = statusMap
												.get(XMAdminUtil.getInstance().getCurrentLocaleEnum().getLangCode());
										XMAdminUtil.getInstance().updateLogFile(errorMessage, MessageType.FAILURE);
									}
								}
								Display.getDefault().asyncExec(new Runnable() {
									public void run() {
										viewer.refresh(projectAppTypeNode);
										viewer.setSelection(viewer.getSelection(), true);
										viewer.setExpandedState(projectAppTypeNode.getParent(), true);
										viewer.setExpandedState(projectAppTypeNode, true);
									}
								});
							}
						}
					} catch (UnauthorizedAccessException e) {
						Display.getDefault().asyncExec(new Runnable() {
							@Override
							public void run() {
								CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
										messages.unauthorizedDialogTitle, messages.unauthorizedDialogMsg);
							}
						});
					} catch (Exception e) {
						if (e instanceof ResourceAccessException) {
							Display.getDefault().asyncExec(new Runnable() {
								@Override
								public void run() {
									CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
											messages.errorDialogTitile, messages.serverNotReachable);
								}
							});
						}
						LOGGER.error(e.getMessage());
					}

					monitor.worked(70);
					return Status.OK_STATUS;
				}
			};
			job.setUser(true);
			job.schedule();
		}
	}

	/**
	 * Method for Update site admin area user app state DND rel.
	 *
	 * @param relationObjArray
	 *            {@link RelationObj[]}
	 * @param userAppTypeNode
	 *            {@link IAdminTreeChild}
	 */
	private void updateSiteAdminAreaUserAppStateDNDRel(RelationObj[] relationObjArray,
			IAdminTreeChild userAppTypeNode) {
		if (userAppTypeNode != null && relationObjArray != null && relationObjArray.length > 0) {
			IAdminTreeChild parent = userAppTypeNode.getParent();
			String relationType = null;
			if (userAppTypeNode instanceof SiteAdminAreaUserAppNotFixed
					|| userAppTypeNode instanceof AdminAreaUserAppNotFixed) {
				relationType = ApplicationRelationType.NOTFIXED.name();
			} else if (userAppTypeNode instanceof SiteAdminAreaUserAppFixed
					|| userAppTypeNode instanceof AdminAreaUserAppFixed) {
				relationType = ApplicationRelationType.FIXED.name();
			} else if (userAppTypeNode instanceof SiteAdminAreaUserAppProtected
					|| userAppTypeNode instanceof AdminAreaUserAppProtected) {
				relationType = ApplicationRelationType.PROTECTED.name();
			}

			final String relationType2 = relationType;
			final String adminAreaName;
			if (userAppTypeNode instanceof AdminAreaUserAppNotFixed || userAppTypeNode instanceof AdminAreaUserAppFixed
					|| userAppTypeNode instanceof AdminAreaUserAppProtected) {
				adminAreaName = (((AdministrationArea) parent.getParent())).getName();
			} else {
				adminAreaName = ((AdministrationArea) ((RelationObj) parent.getParent()).getRefObject()).getName();
			}
			Job job = new Job("Creating Relations...") {

				@Override
				protected IStatus run(IProgressMonitor monitor) {
					monitor.beginTask("Creating Relations..", 100);
					monitor.worked(30);
					try {
						if (relationType2 != null) {
							List<AdminAreaUserAppRelRequest> adminAreaUserAppRelRequests = new ArrayList<AdminAreaUserAppRelRequest>();

							for (RelationObj relationUserAppObj : relationObjArray) {
								String relId = relationUserAppObj.getRelId();
								AdminAreaUserAppRelRequest adminAreaUserAppRelRequest = new AdminAreaUserAppRelRequest();
								adminAreaUserAppRelRequest.setId(relId);
								adminAreaUserAppRelRequest.setRelationType(relationType2);
								adminAreaUserAppRelRequests.add(adminAreaUserAppRelRequest);
							}
							AdminAreaUserAppRelController adminAreaUserAppRelController = new AdminAreaUserAppRelController(
									XMAdminUtil.getInstance().getAAForHeaders(parent));
							AdminAreaUserAppRelBatchResponse adminAreaUserAppRelBatchResponse = adminAreaUserAppRelController
									.updateRelTypesByIds(
											new AdminAreaUserAppRelBatchRequest(adminAreaUserAppRelRequests));

							if (adminAreaUserAppRelBatchResponse != null) {
								List<AdminAreaUserAppRelTbl> adminAreaUserAppRelTbls = adminAreaUserAppRelBatchResponse
										.getAdminAreaUserAppRelTbls();
								List<String> successObjList = new ArrayList<>();
								for (AdminAreaUserAppRelTbl adminAreaUserAppRelTbl : adminAreaUserAppRelTbls) {
									String relId = adminAreaUserAppRelTbl.getAdminAreaUserAppRelId();
									if (relId != null) {
										for (RelationObj relObj : relationObjArray) {
											UserApplication userApp = (UserApplication) relObj.getRefObject();
											if (userApp.getUserApplicationId().equals(adminAreaUserAppRelTbl
													.getUserApplicationId().getUserApplicationId())) {
												successObjList.add(userApp.getName());
											}
										}
									}
								}
								List<Map<String, String>> statusMapList = adminAreaUserAppRelBatchResponse
										.getStatusMap();
								int noOfObjRelCreated = relationObjArray.length;
								final int noOfObjRelCreated2;
								if (statusMapList != null && !statusMapList.isEmpty()) {
									noOfObjRelCreated = relationObjArray.length - statusMapList.size();
									Display.getDefault().asyncExec(new Runnable() {
										public void run() {
											String errorMessage = "";
											for (Map<String, String> statusMap : statusMapList) {
												errorMessage += statusMap.get(
														XMAdminUtil.getInstance().getCurrentLocaleEnum().getLangCode());
												errorMessage += "\n";
											}
											if (errorMessage.length() > 0) {
												CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
														messages.dndErrorTitle, errorMessage);
											}
										}
									});
								}
								noOfObjRelCreated2 = noOfObjRelCreated;
								if (noOfObjRelCreated2 > 0) {
									for (String objName : successObjList) {
										XMAdminUtil.getInstance()
												.updateLogFile(messages.relationLbl + " " + messages.betweenLbl + " "
														+ messages.userApplicationObject +" '" + objName + "' "
														+ messages.andLbl + " " + messages.administrationAreaObject
														+ " '" + adminAreaName + "' " + messages.objectUpdate,
														MessageType.SUCCESS);
									}
									Display.getDefault().asyncExec(new Runnable() {
										@Override
										public void run() {
											eventBroker.send(CommonConstants.EVENT_BROKER.STATUSBAR,
													noOfObjRelCreated2 + " " + messages.dndStatusBarMsg);
										}
									});
								}
								if (statusMapList != null && !statusMapList.isEmpty()) {
									for (Map<String, String> statusMap : statusMapList) {
										String errorMessage = statusMap
												.get(XMAdminUtil.getInstance().getCurrentLocaleEnum().getLangCode());
										XMAdminUtil.getInstance().updateLogFile(errorMessage, MessageType.FAILURE);
									}
								}
								Display.getDefault().asyncExec(new Runnable() {
									public void run() {
										viewer.refresh(userAppTypeNode);
										viewer.setSelection(viewer.getSelection(), true);
										viewer.setExpandedState(parent, true);
										viewer.setExpandedState(userAppTypeNode, true);
									}
								});
							}
						}
					} catch (UnauthorizedAccessException e) {
						Display.getDefault().asyncExec(new Runnable() {
							@Override
							public void run() {
								CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
										messages.unauthorizedDialogTitle, messages.unauthorizedDialogMsg);
							}
						});
					} catch (Exception e) {
						if (e instanceof ResourceAccessException) {
							Display.getDefault().asyncExec(new Runnable() {
								@Override
								public void run() {
									CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
											messages.errorDialogTitile, messages.serverNotReachable);
								}
							});
						}
						LOGGER.error(e.getMessage());
					}

					monitor.worked(70);
					return Status.OK_STATUS;
				}
			};
			job.setUser(true);
			job.schedule();
		}

	}

	/**
	 * Method for Update user AA user app state DND rel.
	 *
	 * @param relationObjArray
	 *            {@link RelationObj[]}
	 * @param userAppTypeNode
	 *            {@link IAdminTreeChild}
	 */
	private void updateUserAAUserAppStateDNDRel(final RelationObj[] relationObjArray,
			final IAdminTreeChild userAppTypeNode) {
		if (userAppTypeNode != null && relationObjArray != null && relationObjArray.length > 0) {
			IAdminTreeChild parent = userAppTypeNode.getParent();
			String relationType = null;
			if (userAppTypeNode instanceof UserAAUserAppAllowed) {
				relationType = UserRelationType.ALLOWED.name();
			} else if (userAppTypeNode instanceof UserAAUserAppForbidden) {
				relationType = UserRelationType.FORBIDDEN.name();
			}

			final String relationType2 = relationType;
			final String adminAreaName = ((AdministrationArea) ((RelationObj) parent.getParent()).getRefObject())
					.getName();
			final String userName = ((User) parent.getParent().getParent().getParent()).getName();
			Job job = new Job("Creating Relations...") {

				@Override
				protected IStatus run(IProgressMonitor monitor) {
					monitor.beginTask("Creating Relations..", 100);
					monitor.worked(30);
					try {
						if (relationType2 != null) {
							List<UserUserAppRelRequest> userUserAppRelRequests = new ArrayList<UserUserAppRelRequest>();

							for (RelationObj relationUserAppObj : relationObjArray) {
								String relId = relationUserAppObj.getRelId();
								UserUserAppRelRequest userAppRelRequest = new UserUserAppRelRequest();
								userAppRelRequest.setId(relId);
								userAppRelRequest.setUserRelationType(relationType2);
								userUserAppRelRequests.add(userAppRelRequest);
							}

							if (!userUserAppRelRequests.isEmpty()) {
								UserUserAppRelController userAppRelController = new UserUserAppRelController(
										XMAdminUtil.getInstance().getAAForHeaders(parent));
								UserUserAppRelBatchResponse userUserAppRelBatchResponse = userAppRelController
										.updateUserRelTypesByIds(
												new UserUserAppRelBatchRequest(userUserAppRelRequests));

								if (userUserAppRelBatchResponse != null) {
									List<UserUserAppRelTbl> userUserAppRelTbls = userUserAppRelBatchResponse
											.getUserUserAppRelTbls();
									List<String> successObjList = new ArrayList<>();
									for (UserUserAppRelTbl userUserAppRelTbl : userUserAppRelTbls) {
										String relId = userUserAppRelTbl.getUserUserAppRelId();
										if (relId != null) {
											for (RelationObj relObj : relationObjArray) {
												UserApplication userApp = (UserApplication) relObj.getRefObject();
												if (userApp.getUserApplicationId().equals(userUserAppRelTbl
														.getUserApplicationId().getUserApplicationId())) {
													successObjList.add(userApp.getName());
												}
											}
										}
									}
									List<Map<String, String>> statusMapList = userUserAppRelBatchResponse
											.getStatusMap();
									int noOfObjRelCreated = relationObjArray.length;
									final int noOfObjRelCreated2;
									if (statusMapList != null && !statusMapList.isEmpty()) {
										noOfObjRelCreated = relationObjArray.length - statusMapList.size();
										Display.getDefault().asyncExec(new Runnable() {
											public void run() {
												String errorMessage = "";
												for (Map<String, String> statusMap : statusMapList) {
													errorMessage += statusMap.get(XMAdminUtil.getInstance()
															.getCurrentLocaleEnum().getLangCode());
													errorMessage += "\n";
												}
												if (errorMessage.length() > 0) {
													CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
															messages.dndErrorTitle, errorMessage);
												}
											}
										});
									}
									noOfObjRelCreated2 = noOfObjRelCreated;
									if (noOfObjRelCreated2 > 0) {
										for (String objName : successObjList) {
											XMAdminUtil.getInstance()
													.updateLogFile(messages.relationLbl + " " + messages.betweenLbl
															+ " " + messages.userApplicationObject + " '" + objName
															+ "' " + messages.andLbl + " "
															+ messages.administrationAreaObject + " '" + adminAreaName
															+ "' " + messages.withLbl + " " + messages.userObject + " '"
															+ userName + "' " + messages.objectUpdate,
															MessageType.SUCCESS);
										}
										Display.getDefault().asyncExec(new Runnable() {
											@Override
											public void run() {
												eventBroker.send(CommonConstants.EVENT_BROKER.STATUSBAR,
														noOfObjRelCreated2 + " " + messages.dndStatusBarMsg);
											}
										});
									}
									if (statusMapList != null && !statusMapList.isEmpty()) {
										for (Map<String, String> statusMap : statusMapList) {
											String errorMessage = statusMap.get(
													XMAdminUtil.getInstance().getCurrentLocaleEnum().getLangCode());
											XMAdminUtil.getInstance().updateLogFile(errorMessage, MessageType.FAILURE);
										}
									}
									Display.getDefault().asyncExec(new Runnable() {
										public void run() {
											viewer.refresh(userAppTypeNode);
											viewer.setSelection(viewer.getSelection(), true);
											viewer.setExpandedState(parent, true);
											viewer.setExpandedState(userAppTypeNode, true);
										}
									});
								}
							}
						}
					} catch (UnauthorizedAccessException e) {
						Display.getDefault().asyncExec(new Runnable() {
							@Override
							public void run() {
								CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
										messages.unauthorizedDialogTitle, messages.unauthorizedDialogMsg);
							}
						});
					} catch (Exception e) {
						if (e instanceof ResourceAccessException) {
							Display.getDefault().asyncExec(new Runnable() {
								@Override
								public void run() {
									CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
											messages.errorDialogTitile, messages.serverNotReachable);
								}
							});
						}
						LOGGER.error(e.getMessage());
					}

					monitor.worked(70);
					return Status.OK_STATUS;
				}
			};
			job.setUser(true);
			job.schedule();
		}
	}

	/**
	 * Method for Update user project AA project app state DND rel.
	 *
	 * @param relationObjArray
	 *            {@link RelationObj[]}
	 * @param projectAppTypeNode
	 *            {@link IAdminTreeChild}
	 */
	private void updateUserProjectAAProjectAppStateDNDRel(RelationObj[] relationObjArray,
			IAdminTreeChild projectAppTypeNode) {
		if (projectAppTypeNode != null && relationObjArray != null && relationObjArray.length > 0) {
			IAdminTreeChild parent = projectAppTypeNode.getParent();
			String relationType = null;
			if (projectAppTypeNode instanceof UserProjectAAProjectAppAllowed
					|| projectAppTypeNode instanceof ProjectUserAAProjectAppAllowed) {
				relationType = UserRelationType.ALLOWED.name();
			} else if (projectAppTypeNode instanceof UserProjectAAProjectAppForbidden
					|| projectAppTypeNode instanceof ProjectUserAAProjectAppForbidden) {
				relationType = UserRelationType.FORBIDDEN.name();
			}

			final String relationType2 = relationType;
			final String adminAreaName = ((AdministrationArea) (((RelationObj) parent.getParent())).getRefObject())
					.getName();
			final String userName;
			if (projectAppTypeNode instanceof UserProjectAAProjectAppAllowed
					|| projectAppTypeNode instanceof UserProjectAAProjectAppForbidden) {
				userName = ((User) parent.getParent().getParent().getParent().getParent().getParent()).getName();
			} else {
				userName = ((User) ((RelationObj) parent.getParent().getParent().getParent()).getRefObject()).getName();
			}
			Job job = new Job("Creating Relations...") {

				@Override
				protected IStatus run(IProgressMonitor monitor) {
					monitor.beginTask("Creating Relations..", 100);
					monitor.worked(30);
					try {
						if (relationType2 != null) {
							List<UserProjectAppRelRequest> userProjectAppRelRequests = new ArrayList<UserProjectAppRelRequest>();

							for (RelationObj relationUserAppObj : relationObjArray) {
								String relId = relationUserAppObj.getRelId();
								UserProjectAppRelRequest userProjectAppRelRequest = new UserProjectAppRelRequest();
								userProjectAppRelRequest.setId(relId);
								userProjectAppRelRequest.setUserRelationType(relationType2);
								userProjectAppRelRequests.add(userProjectAppRelRequest);
							}

							if (!userProjectAppRelRequests.isEmpty()) {
								UserProjectAppRelController userProjectAppRelController = new UserProjectAppRelController(
										XMAdminUtil.getInstance().getAAForHeaders(parent));
								UserProjectAppRelBatchResponse userProjectAppRelBatchResponse = userProjectAppRelController
										.updateUserRelTypesByIds(
												new UserProjectAppRelBatchRequest(userProjectAppRelRequests));

								if (userProjectAppRelBatchResponse != null) {
									List<UserProjAppRelTbl> userProjAppRelTbls = userProjectAppRelBatchResponse
											.getUserProjectAppRelTbls();
									List<String> successObjList = new ArrayList<>();
									for (UserProjAppRelTbl userProjAppRelTbl : userProjAppRelTbls) {
										String relId = userProjAppRelTbl.getUserProjAppRelId();
										if (relId != null) {
											for (RelationObj relObj : relationObjArray) {
												ProjectApplication projectApp = (ProjectApplication) relObj
														.getRefObject();
												if (projectApp.getProjectApplicationId().equals(userProjAppRelTbl
														.getProjectApplicationId().getProjectApplicationId())) {
													successObjList.add(projectApp.getName());
												}
											}
										}
									}
									List<Map<String, String>> statusMapList = userProjectAppRelBatchResponse
											.getStatusMap();
									int noOfObjRelCreated = relationObjArray.length;
									final int noOfObjRelCreated2;
									if (statusMapList != null && !statusMapList.isEmpty()) {
										noOfObjRelCreated = relationObjArray.length - statusMapList.size();
										Display.getDefault().asyncExec(new Runnable() {
											public void run() {
												String errorMessage = "";
												for (Map<String, String> statusMap : statusMapList) {
													errorMessage += statusMap.get(XMAdminUtil.getInstance()
															.getCurrentLocaleEnum().getLangCode());
													errorMessage += "\n";
												}
												if (errorMessage.length() > 0) {
													CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
															messages.dndErrorTitle, errorMessage);
												}
											}
										});
									}
									noOfObjRelCreated2 = noOfObjRelCreated;
									if (noOfObjRelCreated2 > 0) {
										for (String objName : successObjList) {
											XMAdminUtil.getInstance()
													.updateLogFile(messages.relationLbl + " " + messages.betweenLbl
															+ " " + messages.projectApplicationObject + " '" + objName
															+ "' " + messages.andLbl + " "
															+ messages.administrationAreaObject + " '" + adminAreaName
															+ "' " + messages.withLbl + " " + messages.userObject + " '"
															+ userName + "' " + messages.objectUpdate,
															MessageType.SUCCESS);
										}
										Display.getDefault().asyncExec(new Runnable() {
											@Override
											public void run() {
												eventBroker.send(CommonConstants.EVENT_BROKER.STATUSBAR,
														noOfObjRelCreated2 + " " + messages.dndStatusBarMsg);
											}
										});
									}
									if (statusMapList != null && !statusMapList.isEmpty()) {
										for (Map<String, String> statusMap : statusMapList) {
											String errorMessage = statusMap.get(
													XMAdminUtil.getInstance().getCurrentLocaleEnum().getLangCode());
											XMAdminUtil.getInstance().updateLogFile(errorMessage, MessageType.FAILURE);
										}
									}
									Display.getDefault().asyncExec(new Runnable() {
										public void run() {
											viewer.refresh(projectAppTypeNode);
											viewer.setSelection(viewer.getSelection(), true);
											viewer.setExpandedState(parent, true);
											viewer.setExpandedState(projectAppTypeNode, true);
										}
									});
								}
							}
						}
					} catch (UnauthorizedAccessException e) {
						Display.getDefault().asyncExec(new Runnable() {
							@Override
							public void run() {
								CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
										messages.unauthorizedDialogTitle, messages.unauthorizedDialogMsg);
							}
						});
					} catch (Exception e) {
						if (e instanceof ResourceAccessException) {
							Display.getDefault().asyncExec(new Runnable() {
								@Override
								public void run() {
									CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
											messages.errorDialogTitile, messages.serverNotReachable);
								}
							});
						}
						LOGGER.error(e.getMessage());
					}

					monitor.worked(70);
					return Status.OK_STATUS;
				}
			};
			job.setUser(true);
			job.schedule();
		}
	}

	/**
	 * Method for User to project DND rel.
	 *
	 * @param userArray
	 *            {@link User[]}
	 * @param iAdminTreeChild
	 *            {@link IAdminTreeChild}
	 * @param isCopyPast
	 */
	private void userToProjectDNDRel(final User[] userArray, final IAdminTreeChild iAdminTreeChild,
			boolean isCopyPast) {
		if (iAdminTreeChild != null && userArray != null && userArray.length > 0) {
			String projectId = null;
			IAdminTreeChild targetIAdminTreeChild = null;
			String projectName = null;
			if (iAdminTreeChild instanceof Project) {
				targetIAdminTreeChild = ((Project) iAdminTreeChild).getProjectChildren()
						.get(ProjectUsers.class.getName());
				projectId = ((Project) iAdminTreeChild).getProjectId();
				projectName = ((Project) iAdminTreeChild).getName();
			} else if (iAdminTreeChild instanceof ProjectUsers) {
				targetIAdminTreeChild = iAdminTreeChild;
				projectId = ((Project) iAdminTreeChild.getParent()).getProjectId();
				projectName = ((Project) iAdminTreeChild.getParent()).getName();
			}

			if (targetIAdminTreeChild != null && projectId != null) {
				final String projectId2 = projectId;
				final IAdminTreeChild targetIAdminTreeChild2 = targetIAdminTreeChild;
				final String projectName2 = projectName;
				Job job = new Job("Creating Relations...") {

					@Override
					protected IStatus run(IProgressMonitor monitor) {
						monitor.beginTask("Creating Relations..", 100);
						monitor.worked(30);
						try {
							List<UserProjectRelRequest> projectRelRequestList = new ArrayList<>();
							for (User user : userArray) {
								UserProjectRelRequest userProjectRelRequest = new UserProjectRelRequest();
								userProjectRelRequest.setUserId(user.getUserId());
								userProjectRelRequest.setProjectId(projectId2);
								userProjectRelRequest.setStatus(com.magna.xmbackend.vo.enums.Status.ACTIVE.name());
								projectRelRequestList.add(userProjectRelRequest);
							}

							UserProjectRelController adminAreaProjectRelController = new UserProjectRelController();
							UserProjectRelBatchResponse userProjectRelBatchResponse = adminAreaProjectRelController
									.createUserProjectRel(new UserProjectRelBatchRequest(projectRelRequestList));

							if (userProjectRelBatchResponse != null) {
								boolean isRelationCreated = false;
								List<String> successObjList = new ArrayList<>();
								Iterable<UserProjectRelTbl> userProjectRelTbls = userProjectRelBatchResponse
										.getUserProjectRelTbls();
								for (UserProjectRelTbl userProjectRelTbl : userProjectRelTbls) {
									String relId = userProjectRelTbl.getUserProjectRelId();
									if (relId != null) {
										for (User user : userArray) {
											if (user.getUserId().equals(userProjectRelTbl.getUserId().getUserId())) {
												successObjList.add(user.getName());
												RelationObj relObj = (RelationObj) user.getAdapter(
														ProjectUserChild.class, targetIAdminTreeChild2, relId, true);
												((ProjectUserChild) relObj.getContainerObj()).addFixedChildren(relObj);

												((ProjectUsers) targetIAdminTreeChild2).add(relId, relObj);

												isRelationCreated = true;
											}
										}
									}
								}
								if (isRelationCreated) {
									Display.getDefault().syncExec(new Runnable() {
										@SuppressWarnings("unchecked")
										public void run() {
											viewer.refresh(targetIAdminTreeChild2);
											if (!viewer.getExpandedState(targetIAdminTreeChild2.getParent())) {
												(new AdminTreeExpansion(targetIAdminTreeChild2.getParent()))
														.loadObjects();
												viewer.setExpandedState(targetIAdminTreeChild2.getParent(), true);
											}
											viewer.setExpandedState(targetIAdminTreeChild2, true);
											final AdminTreeviewer adminTree = XMAdminUtil.getInstance().getAdminTree();
											if (targetIAdminTreeChild2 instanceof ProjectUsers) {
												adminTree.refershBackReference(new Class[] { UserProjects.class });
											}
											if (isCopyPast && iAdminTreeChild instanceof ProjectUsers) {
												adminTree.setSelection(new StructuredSelection(iAdminTreeChild));
											}
										}
									});
								}
								List<Map<String, String>> statusMapList = userProjectRelBatchResponse.getStatusMap();
								int noOfObjRelCreated = userArray.length;
								final int noOfObjRelCreated2;
								if (statusMapList != null && !statusMapList.isEmpty()) {
									noOfObjRelCreated = userArray.length - statusMapList.size();
									Display.getDefault().asyncExec(new Runnable() {
										public void run() {
											String errorMessage = "";
											for (Map<String, String> statusMap : statusMapList) {
												errorMessage += statusMap.get(
														XMAdminUtil.getInstance().getCurrentLocaleEnum().getLangCode());
												errorMessage += "\n";
											}
											if (errorMessage.length() > 0) {
												CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
														messages.dndErrorTitle, errorMessage);
											}
										}
									});
								}
								noOfObjRelCreated2 = noOfObjRelCreated;
								if (noOfObjRelCreated2 > 0) {
									for (String objName : successObjList) {
										XMAdminUtil.getInstance().updateLogFile(messages.relationLbl + " "
												+ messages.betweenLbl + " " + messages.userObject + " '" + objName
												+ "' " + messages.andLbl + " " + messages.projectObject + " '"
												+ projectName2 + "' " + messages.objectCreate, MessageType.SUCCESS);
									}
									Display.getDefault().asyncExec(new Runnable() {
										@Override
										public void run() {
											eventBroker.send(CommonConstants.EVENT_BROKER.STATUSBAR,
													noOfObjRelCreated2 + " " + messages.dndStatusBarMsg);
										}
									});
								}
								if (statusMapList != null && !statusMapList.isEmpty()) {
									for (Map<String, String> statusMap : statusMapList) {
										String errorMessage = statusMap
												.get(XMAdminUtil.getInstance().getCurrentLocaleEnum().getLangCode());
										XMAdminUtil.getInstance().updateLogFile(errorMessage, MessageType.FAILURE);
									}
								}
							}
						} catch (UnauthorizedAccessException e) {
							Display.getDefault().asyncExec(new Runnable() {
								@Override
								public void run() {
									CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
											messages.unauthorizedDialogTitle, messages.unauthorizedDialogMsg);
								}
							});
						} catch (Exception e) {
							if (e instanceof ResourceAccessException) {
								Display.getDefault().asyncExec(new Runnable() {
									@Override
									public void run() {
										CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
												messages.errorDialogTitile, messages.serverNotReachable);
									}
								});
							}
							LOGGER.error(e.getMessage());
						}

						monitor.worked(70);
						return Status.OK_STATUS;
					}
				};
				job.setUser(true);
				job.schedule();
			}
		}
	}

	/**
	 * Method for Project to user DND rel.
	 *
	 * @param projectArray
	 *            {@link Project[]}
	 * @param iAdminTreeChild
	 *            {@link IAdminTreeChild}
	 * @param isCopyPast
	 */
	private void projectToUserDNDRel(final Project[] projectArray, final IAdminTreeChild iAdminTreeChild,
			boolean isCopyPast) {
		if (iAdminTreeChild != null && projectArray != null && projectArray.length > 0) {
			String userId = null;
			IAdminTreeChild targetIAdminTreeChild = null;
			String userName = null;
			if (iAdminTreeChild instanceof User) {
				targetIAdminTreeChild = ((User) iAdminTreeChild).getUserChildren().get(UserProjects.class.getName());
				userId = ((User) iAdminTreeChild).getUserId();
				userName = ((User) iAdminTreeChild).getName();
			} else if (iAdminTreeChild instanceof UserProjects) {
				targetIAdminTreeChild = iAdminTreeChild;
				userId = ((User) iAdminTreeChild.getParent()).getUserId();
				userName = ((User) iAdminTreeChild.getParent()).getName();
			}

			if (targetIAdminTreeChild != null && userId != null) {
				final String userId2 = userId;
				final IAdminTreeChild targetIAdminTreeChild2 = targetIAdminTreeChild;
				final String userName2 = userName;
				Job job = new Job("Creating Relations...") {

					@Override
					protected IStatus run(IProgressMonitor monitor) {
						monitor.beginTask("Creating Relations..", 100);
						monitor.worked(30);
						try {
							List<UserProjectRelRequest> projectRelRequestList = new ArrayList<>();
							for (Project project : projectArray) {
								UserProjectRelRequest userProjectRelRequest = new UserProjectRelRequest();
								userProjectRelRequest.setProjectId(project.getProjectId());
								userProjectRelRequest.setUserId(userId2);
								userProjectRelRequest.setStatus(com.magna.xmbackend.vo.enums.Status.ACTIVE.name());
								projectRelRequestList.add(userProjectRelRequest);
							}

							UserProjectRelController adminAreaProjectRelController = new UserProjectRelController();
							UserProjectRelBatchResponse userProjectRelBatchResponse = adminAreaProjectRelController
									.createUserProjectRel(new UserProjectRelBatchRequest(projectRelRequestList));

							if (userProjectRelBatchResponse != null) {
								boolean isRelationCreated = false;
								Iterable<UserProjectRelTbl> userProjectRelTbls = userProjectRelBatchResponse
										.getUserProjectRelTbls();
								List<String> successObjList = new ArrayList<>();
								for (UserProjectRelTbl userProjectRelTbl : userProjectRelTbls) {
									String relId = userProjectRelTbl.getUserProjectRelId();
									if (relId != null) {
										for (Project project : projectArray) {
											if (project.getProjectId()
													.equals(userProjectRelTbl.getProjectId().getProjectId())) {
												successObjList.add(project.getName());
												RelationObj relObj = (RelationObj) project.getAdapter(
														UserProjectChild.class, targetIAdminTreeChild2, relId, true);
												((UserProjectChild) relObj.getContainerObj()).addFixedChildren(relObj);

												((UserProjects) targetIAdminTreeChild2).add(relId, relObj);
												isRelationCreated = true;
											}
										}
									}
								}
								if (isRelationCreated) {
									Display.getDefault().syncExec(new Runnable() {
										@SuppressWarnings("unchecked")
										public void run() {
											viewer.refresh(targetIAdminTreeChild2);
											if (!viewer.getExpandedState(targetIAdminTreeChild2.getParent())) {
												(new AdminTreeExpansion(targetIAdminTreeChild2.getParent()))
														.loadObjects();
												viewer.setExpandedState(targetIAdminTreeChild2.getParent(), true);
											}
											viewer.setExpandedState(targetIAdminTreeChild2, true);
											final AdminTreeviewer adminTree = XMAdminUtil.getInstance().getAdminTree();
											if (targetIAdminTreeChild2 instanceof UserProjects) {
												adminTree.refershBackReference(new Class[] { ProjectUsers.class });
											}
											if (isCopyPast && iAdminTreeChild instanceof UserProjects) {
												adminTree.setSelection(new StructuredSelection(iAdminTreeChild));
											}
										}
									});
								}
								List<Map<String, String>> statusMapList = userProjectRelBatchResponse.getStatusMap();
								int noOfObjRelCreated = projectArray.length;
								final int noOfObjRelCreated2;
								if (statusMapList != null && !statusMapList.isEmpty()) {
									noOfObjRelCreated = projectArray.length - statusMapList.size();
									Display.getDefault().asyncExec(new Runnable() {
										public void run() {
											String errorMessage = "";
											for (Map<String, String> statusMap : statusMapList) {
												errorMessage += statusMap.get(
														XMAdminUtil.getInstance().getCurrentLocaleEnum().getLangCode());
												errorMessage += "\n";
											}
											if (errorMessage.length() > 0) {
												CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
														messages.dndErrorTitle, errorMessage);
											}
										}
									});
								}
								noOfObjRelCreated2 = noOfObjRelCreated;
								if (noOfObjRelCreated2 > 0) {
									for (String objName : successObjList) {
										XMAdminUtil.getInstance().updateLogFile(messages.relationLbl + " "
												+ messages.betweenLbl + " " + messages.projectObject +" '"
												+ objName + "' " + messages.andLbl + " " + messages.userObject + " '"
												+ userName2 + "' " + messages.objectCreate, MessageType.SUCCESS);
									}
									Display.getDefault().asyncExec(new Runnable() {
										@Override
										public void run() {
											eventBroker.send(CommonConstants.EVENT_BROKER.STATUSBAR,
													noOfObjRelCreated2 + " " + messages.dndStatusBarMsg);
										}
									});
								}
								if (statusMapList != null && !statusMapList.isEmpty()) {
									for (Map<String, String> statusMap : statusMapList) {
										String errorMessage = statusMap
												.get(XMAdminUtil.getInstance().getCurrentLocaleEnum().getLangCode());
										XMAdminUtil.getInstance().updateLogFile(errorMessage, MessageType.FAILURE);
									}
								}
							}
						} catch (UnauthorizedAccessException e) {
							Display.getDefault().asyncExec(new Runnable() {
								@Override
								public void run() {
									CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
											messages.unauthorizedDialogTitle, messages.unauthorizedDialogMsg);
								}
							});
						} catch (Exception e) {
							if (e instanceof ResourceAccessException) {
								Display.getDefault().asyncExec(new Runnable() {
									@Override
									public void run() {
										CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
												messages.errorDialogTitile, messages.serverNotReachable);
									}
								});
							}
							LOGGER.error(e.getMessage());
						}

						monitor.worked(70);
						return Status.OK_STATUS;
					}
				};
				job.setUser(true);
				job.schedule();
			}
		}
	}

	/**
	 * Method for User app to user admin area DND rel.
	 *
	 * @param userAppArray
	 *            {@link UserApplication[]}
	 * @param iAdminTreeChild
	 *            {@link IAdminTreeChild}
	 * @param isCopyPast
	 */
	private void userAppToUserAdminAreaDNDRel(final UserApplication[] userAppArray,
			final IAdminTreeChild iAdminTreeChild, boolean isCopyPast) {
		if (iAdminTreeChild != null && userAppArray != null && userAppArray.length > 0) {
			IAdminTreeChild parent = iAdminTreeChild.getParent();
			String userId = null;
			String siteAdminAreaRelId = null;
			String relationType = null;
			IAdminTreeChild targetIAdminTreeChild = null;
			String userName = null;
			String adminAreaName = null;
			if (iAdminTreeChild instanceof UserAAUserAppAllowed) {
				userId = ((User) parent.getParent().getParent().getParent()).getUserId();
				siteAdminAreaRelId = ((RelationObj) parent.getParent()).getRelId();
				targetIAdminTreeChild = iAdminTreeChild;
				relationType = UserRelationType.ALLOWED.name();
				userName = ((User) parent.getParent().getParent().getParent()).getName();
				adminAreaName = ((AdministrationArea) ((RelationObj) parent.getParent()).getRefObject()).getName();
			} else if (iAdminTreeChild instanceof UserAAUserAppForbidden) {
				userId = ((User) parent.getParent().getParent().getParent()).getUserId();
				siteAdminAreaRelId = ((RelationObj) parent.getParent()).getRelId();
				targetIAdminTreeChild = iAdminTreeChild;
				relationType = UserRelationType.FORBIDDEN.name();
				userName = ((User) parent.getParent().getParent().getParent()).getName();
				adminAreaName = ((AdministrationArea) ((RelationObj) parent.getParent()).getRefObject()).getName();
			} else if (iAdminTreeChild instanceof UserAdminAreaChild) {
				Map<String, IAdminTreeChild> userAdminAreaChildren = ((UserAdminAreaChild) iAdminTreeChild)
						.getUserAdminAreaChildren();
				targetIAdminTreeChild = userAdminAreaChildren.get(UserAAUserApplications.class.getName());
				siteAdminAreaRelId = ((RelationObj) targetIAdminTreeChild.getParent()).getRelId();
				userId = ((User) targetIAdminTreeChild.getParent().getParent().getParent()).getUserId();
				relationType = UserRelationType.ALLOWED.name();
				userName = ((User) targetIAdminTreeChild.getParent().getParent().getParent()).getName();
				adminAreaName = ((AdministrationArea) ((RelationObj) targetIAdminTreeChild.getParent()).getRefObject())
						.getName();
			} else if (iAdminTreeChild instanceof UserAAUserApplications) {
				userId = ((User) parent.getParent().getParent()).getUserId();
				siteAdminAreaRelId = ((RelationObj) parent).getRelId();
				targetIAdminTreeChild = iAdminTreeChild;
				relationType = UserRelationType.ALLOWED.name();
				userName = ((User) parent.getParent().getParent()).getName();
				adminAreaName = ((AdministrationArea) ((RelationObj) parent).getRefObject()).getName();
			}

			final String userId2 = userId;
			final String siteAdminAreaRelId2 = siteAdminAreaRelId;
			final String relationType2 = relationType;
			final IAdminTreeChild targetIAdminTreeChild2 = targetIAdminTreeChild;
			final String userName2 = userName;
			final String adminAreaName2 = adminAreaName;
			if (userId2 != null && siteAdminAreaRelId2 != null && relationType2 != null
					&& targetIAdminTreeChild2 != null) {
				Job job = new Job("Creating Relations...") {

					@Override
					protected IStatus run(IProgressMonitor monitor) {
						monitor.beginTask("Creating Relations..", 100);
						monitor.worked(30);
						try {
							List<UserUserAppRelRequest> userUserAppRelRequestList = new ArrayList<>();
							for (UserApplication userApplication : userAppArray) {
								UserUserAppRelRequest userUserAppRelRequest = new UserUserAppRelRequest();
								userUserAppRelRequest.setUserId(userId2);
								userUserAppRelRequest.setUserAppId(userApplication.getUserApplicationId());
								userUserAppRelRequest.setSiteAdminAreaRelId(siteAdminAreaRelId2);
								userUserAppRelRequest.setUserRelationType(relationType2);
								userUserAppRelRequestList.add(userUserAppRelRequest);
							}

							UserUserAppRelController userAppRelController = new UserUserAppRelController(
									XMAdminUtil.getInstance().getAAForHeaders(parent));
							UserUserAppRelBatchResponse userUserAppRelBatchResponse = userAppRelController
									.createUserUserAppRel(new UserUserAppRelBatchRequest(userUserAppRelRequestList));

							if (userUserAppRelBatchResponse != null) {
								boolean isRelationCreated = false;
								Iterable<UserUserAppRelTbl> userUserAppRelTbls = userUserAppRelBatchResponse
										.getUserUserAppRelTbls();
								List<String> successObjList = new ArrayList<>();
								for (UserUserAppRelTbl userUserAppRelTbl : userUserAppRelTbls) {
									String relId = userUserAppRelTbl.getUserUserAppRelId();
									if (relId != null) {
										for (UserApplication userApp : userAppArray) {
											if (userApp.getUserApplicationId().equals(
													userUserAppRelTbl.getUserApplicationId().getUserApplicationId())) {
												successObjList.add(userApp.getName());
												isRelationCreated = true;
											}
										}
										/*
										 * isRelationCreated = true; break;
										 */
									}
								}
								if (isRelationCreated) {
									Display.getDefault().asyncExec(new Runnable() {
										public void run() {
											viewer.refresh(targetIAdminTreeChild2);
											if (!viewer.getExpandedState(targetIAdminTreeChild2.getParent())) {
												(new AdminTreeExpansion(targetIAdminTreeChild2.getParent()))
														.loadObjects();
												viewer.setExpandedState(targetIAdminTreeChild2.getParent(), true);
											}
											viewer.setExpandedState(targetIAdminTreeChild2, true);
											if (isCopyPast && !(iAdminTreeChild instanceof UserAdminAreaChild)) {
												final AdminTreeviewer adminTree = XMAdminUtil.getInstance()
														.getAdminTree();
												adminTree.setSelection(new StructuredSelection(iAdminTreeChild));
											}
										}
									});
								}
								List<Map<String, String>> statusMapList = userUserAppRelBatchResponse.getStatusMap();
								int noOfObjRelCreated = userAppArray.length;
								final int noOfObjRelCreated2;
								if (statusMapList != null && !statusMapList.isEmpty()) {
									noOfObjRelCreated = userAppArray.length - statusMapList.size();
									Display.getDefault().asyncExec(new Runnable() {
										public void run() {
											String errorMessage = "";
											for (Map<String, String> statusMap : statusMapList) {
												errorMessage += statusMap.get(
														XMAdminUtil.getInstance().getCurrentLocaleEnum().getLangCode());
												errorMessage += "\n";
											}
											if (errorMessage.length() > 0) {
												CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
														messages.dndErrorTitle, errorMessage);
											}
										}
									});
								}
								noOfObjRelCreated2 = noOfObjRelCreated;
								if (noOfObjRelCreated2 > 0) {
									for (String objName : successObjList) {
										XMAdminUtil.getInstance()
												.updateLogFile(messages.relationLbl + " " + messages.betweenLbl + " "
														+ messages.userApplicationObject +" '" + objName + "' "
														+ messages.andLbl + " " + messages.administrationAreaObject
														+ " '" + adminAreaName2 + "' " + messages.withLbl + " "
														+ messages.userObject + " '" + userName2 + "' "
														+ messages.objectCreate, MessageType.SUCCESS);
									}
									Display.getDefault().asyncExec(new Runnable() {
										@Override
										public void run() {
											eventBroker.send(CommonConstants.EVENT_BROKER.STATUSBAR,
													noOfObjRelCreated2 + " " + messages.dndStatusBarMsg);
										}
									});
								}
								if (statusMapList != null && !statusMapList.isEmpty()) {
									for (Map<String, String> statusMap : statusMapList) {
										String errorMessage = statusMap
												.get(XMAdminUtil.getInstance().getCurrentLocaleEnum().getLangCode());
										XMAdminUtil.getInstance().updateLogFile(errorMessage, MessageType.FAILURE);
									}
								}
							}
						} catch (UnauthorizedAccessException e) {
							Display.getDefault().asyncExec(new Runnable() {
								@Override
								public void run() {
									CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
											messages.unauthorizedDialogTitle, messages.unauthorizedDialogMsg);
								}
							});
						} catch (Exception e) {
							if (e instanceof ResourceAccessException) {
								Display.getDefault().asyncExec(new Runnable() {
									@Override
									public void run() {
										CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
												messages.errorDialogTitile, messages.serverNotReachable);
									}
								});
							}
							LOGGER.error(e.getMessage());
						}

						monitor.worked(70);
						return Status.OK_STATUS;
					}
				};
				job.setUser(true);
				job.schedule();
			}
		}
	}

	/**
	 * Method for Project app to user project admin area DND rel.
	 *
	 * @param projectAppArray
	 *            {@link ProjectApplication[]}
	 * @param iAdminTreeChild
	 *            {@link IAdminTreeChild}
	 * @param isCopyPast
	 */
	private void projectAppToUserProjectAdminAreaDNDRel(final ProjectApplication[] projectAppArray,
			final IAdminTreeChild iAdminTreeChild, boolean isCopyPast) {
		if (iAdminTreeChild != null && projectAppArray != null && projectAppArray.length > 0) {
			IAdminTreeChild parent = iAdminTreeChild.getParent();
			String userProjectRelId = null;
			String adminAreaProjectRelId = null;
			String relationType = null;
			IAdminTreeChild targetIAdminTreeChild = null;
			String adminAreaName = null;
			String userName = null;
			String projectName = null;
			if (iAdminTreeChild instanceof UserProjectAAProjectAppAllowed
					|| iAdminTreeChild instanceof ProjectUserAAProjectAppAllowed) {
				userProjectRelId = ((RelationObj) parent.getParent().getParent().getParent()).getRelId();
				adminAreaProjectRelId = ((RelationObj) parent.getParent()).getRelId();
				targetIAdminTreeChild = iAdminTreeChild;
				relationType = UserRelationType.ALLOWED.name();
				adminAreaName = ((AdministrationArea) (((RelationObj) parent.getParent())).getRefObject()).getName();
				if (iAdminTreeChild instanceof UserProjectAAProjectAppAllowed) {
					userName = ((User) parent.getParent().getParent().getParent().getParent().getParent()).getName();
					projectName = ((Project) (((RelationObj) parent.getParent().getParent().getParent()))
							.getRefObject()).getName();
				} else {
					userName = ((User) ((RelationObj) parent.getParent().getParent().getParent()).getRefObject())
							.getName();
					projectName = ((Project) parent.getParent().getParent().getParent().getParent().getParent())
							.getName();
				}
			} else if (iAdminTreeChild instanceof UserProjectAAProjectAppForbidden
					|| iAdminTreeChild instanceof ProjectUserAAProjectAppForbidden) {
				userProjectRelId = ((RelationObj) parent.getParent().getParent().getParent()).getRelId();
				adminAreaProjectRelId = ((RelationObj) parent.getParent()).getRelId();
				targetIAdminTreeChild = iAdminTreeChild;
				relationType = UserRelationType.FORBIDDEN.name();
				adminAreaName = ((AdministrationArea) (((RelationObj) parent.getParent())).getRefObject()).getName();
				if (iAdminTreeChild instanceof UserProjectAAProjectAppForbidden) {
					userName = ((User) parent.getParent().getParent().getParent().getParent().getParent()).getName();
					projectName = ((Project) (((RelationObj) parent.getParent().getParent().getParent()))
							.getRefObject()).getName();
				} else {
					userName = ((User) ((RelationObj) parent.getParent().getParent().getParent()).getRefObject())
							.getName();
					projectName = ((Project) parent.getParent().getParent().getParent().getParent().getParent())
							.getName();
				}
			} else if (iAdminTreeChild instanceof UserProjectAAProjectApplications
					|| iAdminTreeChild instanceof ProjectUserAAProjectApplications) {
				userProjectRelId = ((RelationObj) parent.getParent().getParent()).getRelId();
				adminAreaProjectRelId = ((RelationObj) parent).getRelId();
				targetIAdminTreeChild = iAdminTreeChild;
				relationType = UserRelationType.ALLOWED.name();
				adminAreaName = ((AdministrationArea) (((RelationObj) parent)).getRefObject()).getName();
				if (iAdminTreeChild instanceof UserProjectAAProjectApplications) {
					userName = ((User) parent.getParent().getParent().getParent().getParent()).getName();
					projectName = ((Project) (((RelationObj) parent.getParent().getParent())).getRefObject()).getName();
				} else {
					userName = ((User) ((RelationObj) parent.getParent().getParent()).getRefObject()).getName();
					projectName = ((Project) parent.getParent().getParent().getParent().getParent()).getName();
				}
			} else if (iAdminTreeChild instanceof UserProjectAdminAreaChild) {
				Map<String, IAdminTreeChild> userProjectAdminAreaChildren = ((UserProjectAdminAreaChild) iAdminTreeChild)
						.getUserProjectAdminAreaChildren();
				targetIAdminTreeChild = userProjectAdminAreaChildren
						.get(UserProjectAAProjectApplications.class.getName());
				userProjectRelId = ((RelationObj) targetIAdminTreeChild.getParent().getParent().getParent()).getRelId();
				adminAreaProjectRelId = ((RelationObj) targetIAdminTreeChild.getParent()).getRelId();
				relationType = UserRelationType.ALLOWED.name();
				adminAreaName = ((AdministrationArea) (((RelationObj) targetIAdminTreeChild.getParent()))
						.getRefObject()).getName();
				userName = ((User) parent.getParent().getParent().getParent()).getName();
				projectName = ((Project) (((RelationObj) parent.getParent())).getRefObject()).getName();

			} else if (iAdminTreeChild instanceof ProjectUserAdminAreaChild) {
				Map<String, IAdminTreeChild> projectUserAdminAreaChildren = ((ProjectUserAdminAreaChild) iAdminTreeChild)
						.getProjectUserAdminAreaChildren();
				targetIAdminTreeChild = projectUserAdminAreaChildren
						.get(ProjectUserAAProjectApplications.class.getName());
				userProjectRelId = ((RelationObj) targetIAdminTreeChild.getParent().getParent().getParent()).getRelId();
				adminAreaProjectRelId = ((RelationObj) targetIAdminTreeChild.getParent()).getRelId();
				relationType = UserRelationType.ALLOWED.name();
				adminAreaName = ((AdministrationArea) (((RelationObj) targetIAdminTreeChild.getParent()))
						.getRefObject()).getName();
				userName = ((User) ((RelationObj) parent.getParent()).getRefObject()).getName();
				projectName = ((Project) parent.getParent().getParent().getParent()).getName();
			}

			final String userProjectRelId2 = userProjectRelId;
			final String adminAreaProjectRelId2 = adminAreaProjectRelId;
			final String relationType2 = relationType;
			final IAdminTreeChild targetIAdminTreeChild2 = targetIAdminTreeChild;
			final String adminAreaName2 = adminAreaName;
			final String userName2 = userName;
			final String projectName2 = projectName;
			if (userProjectRelId2 != null && adminAreaProjectRelId2 != null && relationType2 != null
					&& targetIAdminTreeChild2 != null) {
				Job job = new Job("Creating Relations...") {

					@Override
					protected IStatus run(IProgressMonitor monitor) {
						monitor.beginTask("Creating Relations..", 100);
						monitor.worked(30);
						try {
							List<UserProjectAppRelRequest> UserProjectAppRelRequestList = new ArrayList<>();
							for (ProjectApplication projectApplication : projectAppArray) {
								UserProjectAppRelRequest userProjectAppRelRequest = new UserProjectAppRelRequest();
								userProjectAppRelRequest.setUserProjectRelId(userProjectRelId2);
								userProjectAppRelRequest.setAdminAreaProjRelId(adminAreaProjectRelId2);
								userProjectAppRelRequest.setProjectAppId(projectApplication.getProjectApplicationId());
								userProjectAppRelRequest.setUserRelationType(relationType2);
								UserProjectAppRelRequestList.add(userProjectAppRelRequest);
							}

							UserProjectAppRelController userProjectAppRelController = new UserProjectAppRelController(
									XMAdminUtil.getInstance().getAAForHeaders(parent));
							UserProjectAppRelBatchResponse userProjectAppRelBatchResponse = userProjectAppRelController
									.createUserProjectAppRel(
											new UserProjectAppRelBatchRequest(UserProjectAppRelRequestList));

							if (userProjectAppRelBatchResponse != null) {
								boolean isRelationCreated = false;
								Iterable<UserProjAppRelTbl> userProjAppRelTbls = userProjectAppRelBatchResponse
										.getUserProjectAppRelTbls();
								List<String> successObjList = new ArrayList<>();
								for (UserProjAppRelTbl userProjAppRelTbl : userProjAppRelTbls) {
									String relId = userProjAppRelTbl.getUserProjAppRelId();
									if (relId != null) {
										for (ProjectApplication projectApp : projectAppArray) {
											if (projectApp.getProjectApplicationId().equals(userProjAppRelTbl
													.getProjectApplicationId().getProjectApplicationId())) {
												successObjList.add(projectApp.getName());
												isRelationCreated = true;
											}
										}
										/*
										 * isRelationCreated = true; break;
										 */
									}
								}
								if (isRelationCreated) {
									Display.getDefault().asyncExec(new Runnable() {
										public void run() {
											viewer.refresh(targetIAdminTreeChild2);
											if (!viewer.getExpandedState(targetIAdminTreeChild2.getParent())) {
												(new AdminTreeExpansion(targetIAdminTreeChild2.getParent()))
														.loadObjects();
												viewer.setExpandedState(targetIAdminTreeChild2.getParent(), true);
											}
											viewer.setExpandedState(targetIAdminTreeChild2, true);
											if (isCopyPast && !(iAdminTreeChild instanceof UserProjectAdminAreaChild
													|| iAdminTreeChild instanceof ProjectUserAdminAreaChild)) {
												final AdminTreeviewer adminTree = XMAdminUtil.getInstance()
														.getAdminTree();
												adminTree.setSelection(new StructuredSelection(iAdminTreeChild));
											}
										}
									});
								}
								List<Map<String, String>> statusMapList = userProjectAppRelBatchResponse.getStatusMap();
								int noOfObjRelCreated = projectAppArray.length;
								final int noOfObjRelCreated2;
								if (statusMapList != null && !statusMapList.isEmpty()) {
									noOfObjRelCreated = projectAppArray.length - statusMapList.size();
									Display.getDefault().asyncExec(new Runnable() {
										public void run() {
											String errorMessage = "";
											for (Map<String, String> statusMap : statusMapList) {
												errorMessage += statusMap.get(
														XMAdminUtil.getInstance().getCurrentLocaleEnum().getLangCode());
												errorMessage += "\n";
											}
											if (errorMessage.length() > 0) {
												CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
														messages.dndErrorTitle, errorMessage);
											}
										}
									});
								}
								noOfObjRelCreated2 = noOfObjRelCreated;
								if (noOfObjRelCreated2 > 0) {
									for (String objName : successObjList) {
										XMAdminUtil.getInstance()
												.updateLogFile(messages.relationLbl + " " + messages.betweenLbl + " "
														+ messages.projectApplicationObject +" '" + objName + "' "
														+ messages.andLbl + " " + messages.administrationAreaObject
														+ " '" + adminAreaName2 + "' " + messages.withLbl + " "
														+ messages.userObject + " '" + userName2 + "' "
														+ messages.andLbl + " " + messages.projectObject + " '"
														+ projectName2 + "' " + messages.objectCreate,
														MessageType.SUCCESS);
									}
									Display.getDefault().asyncExec(new Runnable() {
										@Override
										public void run() {
											eventBroker.send(CommonConstants.EVENT_BROKER.STATUSBAR,
													noOfObjRelCreated2 + " " + messages.dndStatusBarMsg);
										}
									});
								}
								if (statusMapList != null && !statusMapList.isEmpty()) {
									for (Map<String, String> statusMap : statusMapList) {
										String errorMessage = statusMap
												.get(XMAdminUtil.getInstance().getCurrentLocaleEnum().getLangCode());
										XMAdminUtil.getInstance().updateLogFile(errorMessage, MessageType.FAILURE);
									}
								}
							}
						} catch (UnauthorizedAccessException e) {
							Display.getDefault().asyncExec(new Runnable() {
								@Override
								public void run() {
									CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
											messages.unauthorizedDialogTitle, messages.unauthorizedDialogMsg);
								}
							});
						} catch (Exception e) {
							if (e instanceof ResourceAccessException) {
								Display.getDefault().asyncExec(new Runnable() {
									@Override
									public void run() {
										CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
												messages.errorDialogTitile, messages.serverNotReachable);
									}
								});
							}
							LOGGER.error(e.getMessage());
						}

						monitor.worked(70);
						return Status.OK_STATUS;
					}
				};
				job.setUser(true);
				job.schedule();
			}
		}
	}

	/**
	 * Method for Objects to directory DND rel.
	 *
	 * @param objectArray
	 *            {@link Object[]}
	 * @param iAdminTreeChild
	 *            {@link IAdminTreeChild}
	 * @param isCopyPast
	 */
	private void objectsToDirectoryDNDRel(final Object[] objectArray, final IAdminTreeChild iAdminTreeChild,
			boolean isCopyPast) {
		if (iAdminTreeChild != null && objectArray != null && objectArray.length > 0) {
			String directoryId = null;
			String directoryName = null;
			IAdminTreeChild targetIAdminTreeChild = null;
			if (iAdminTreeChild instanceof Directory) {
				if (objectArray[0] instanceof Project) {
					targetIAdminTreeChild = ((Directory) iAdminTreeChild).getDirectoryChildren()
							.get(DirectoryProjects.class.getName());
				} else if (objectArray[0] instanceof User) {
					targetIAdminTreeChild = ((Directory) iAdminTreeChild).getDirectoryChildren()
							.get(DirectoryUsers.class.getName());
				} else if (objectArray[0] instanceof ProjectApplication) {
					targetIAdminTreeChild = ((DirectoryApplications) ((Directory) iAdminTreeChild)
							.getDirectoryChildren().get(DirectoryApplications.class.getName()))
									.getDirectoryApplicationsChildrenChildren()
									.get(DirectoryProjectApplications.class.getName());
				} else if (objectArray[0] instanceof UserApplication) {
					targetIAdminTreeChild = ((DirectoryApplications) ((Directory) iAdminTreeChild)
							.getDirectoryChildren().get(DirectoryApplications.class.getName()))
									.getDirectoryApplicationsChildrenChildren()
									.get(DirectoryUserApplications.class.getName());
				}
				directoryId = ((Directory) iAdminTreeChild).getDirectoryId();
				directoryName = ((Directory) iAdminTreeChild).getName();
			} else if (iAdminTreeChild instanceof DirectoryApplications) {
				if (objectArray[0] instanceof ProjectApplication) {
					targetIAdminTreeChild = ((DirectoryApplications) iAdminTreeChild)
							.getDirectoryApplicationsChildrenChildren()
							.get(DirectoryProjectApplications.class.getName());
				} else if (objectArray[0] instanceof UserApplication) {
					targetIAdminTreeChild = ((DirectoryApplications) iAdminTreeChild)
							.getDirectoryApplicationsChildrenChildren().get(DirectoryUserApplications.class.getName());
				}
				directoryId = ((Directory) iAdminTreeChild.getParent()).getDirectoryId();
				directoryName = ((Directory) iAdminTreeChild.getParent()).getName();
			} else if (iAdminTreeChild instanceof DirectoryProjects || iAdminTreeChild instanceof DirectoryUsers) {
				targetIAdminTreeChild = iAdminTreeChild;
				directoryId = ((Directory) iAdminTreeChild.getParent()).getDirectoryId();
				directoryName = ((Directory) iAdminTreeChild.getParent()).getName();
			} else if (iAdminTreeChild instanceof DirectoryProjectApplications
					|| iAdminTreeChild instanceof DirectoryUserApplications) {
				targetIAdminTreeChild = iAdminTreeChild;
				directoryId = ((Directory) iAdminTreeChild.getParent().getParent()).getDirectoryId();
				directoryName = ((Directory) iAdminTreeChild.getParent().getParent()).getName();
			}

			final String directoryId2 = directoryId;
			final IAdminTreeChild targetIAdminTreeChild2 = targetIAdminTreeChild;
			final String directoryName2 = directoryName;
			if (directoryId2 != null && targetIAdminTreeChild2 != null) {
				Job job = new Job("Creating Relations...") {

					@Override
					protected IStatus run(IProgressMonitor monitor) {
						monitor.beginTask("Creating Relations..", 100);
						monitor.worked(30);
						try {
							List<DirectoryRelRequest> directoryRelRequestList = new ArrayList<>();
							for (Object object : objectArray) {
								DirectoryRelRequest directoryRelRequest = new DirectoryRelRequest();
								directoryRelRequest.setDirectoryId(directoryId2);
								if (object instanceof Project) {
									directoryRelRequest.setObjectId(((Project) object).getProjectId());
									directoryRelRequest.setObjectType(DirectoryObjectType.PROJECT);
									directoryRelRequestList.add(directoryRelRequest);
								} else if (object instanceof User) {
									directoryRelRequest.setObjectId(((User) object).getUserId());
									directoryRelRequest.setObjectType(DirectoryObjectType.USER);
									directoryRelRequestList.add(directoryRelRequest);
								} else if (object instanceof ProjectApplication) {
									directoryRelRequest
											.setObjectId(((ProjectApplication) object).getProjectApplicationId());
									directoryRelRequest.setObjectType(DirectoryObjectType.PROJECTAPPLICATION);
									directoryRelRequestList.add(directoryRelRequest);
								} else if (object instanceof UserApplication) {
									directoryRelRequest.setObjectId(((UserApplication) object).getUserApplicationId());
									directoryRelRequest.setObjectType(DirectoryObjectType.USERAPPLICATION);
									directoryRelRequestList.add(directoryRelRequest);
								}
							}

							DirectoryRelController directoryRelController = new DirectoryRelController();
							DirectoryRelBatchResponse directoryRelBatchResponse = directoryRelController
									.createDirectoryRelations(new DirectoryRelBatchRequest(directoryRelRequestList));

							if (directoryRelBatchResponse != null) {
								String objectType = null;
								boolean isRelationCreated = false;
								List<DirectoryRefTbl> directoryRefTbls = directoryRelBatchResponse
										.getDirectoryRefTbls();
								List<String> successObjList = new ArrayList<>();
								for (DirectoryRefTbl directoryRefTbl : directoryRefTbls) {
									String relId = directoryRefTbl.getDirectoryRefId();
									if (relId != null) {
										for (Object object : objectArray) {
											String objectId = null;
											String objectName = null;
											if (object instanceof User) {
												objectType = messages.userObject;
												objectId = ((User) object).getUserId();
												objectName = ((User) object).getName();
											} else if (object instanceof Project) {
												objectType = messages.projectObject;
												objectId = ((Project) object).getProjectId();
												objectName = ((Project) object).getName();
											} else if (object instanceof UserApplication) {
												objectType = messages.userApplicationObject;
												objectId = ((UserApplication) object).getUserApplicationId();
												objectName = ((UserApplication) object).getName();
											} else if (object instanceof ProjectApplication) {
												objectType = messages.projectApplicationObject;
												objectId = ((ProjectApplication) object).getProjectApplicationId();
												objectName = ((ProjectApplication) object).getName();
											}
											if (objectId != null && objectId.equals(directoryRefTbl.getObjectId())) {
												successObjList.add(objectName);
												isRelationCreated = true;
											}
										}
										/*
										 * isRelationCreated = true; break;
										 */
									}
								}
								if (isRelationCreated) {
									Display.getDefault().asyncExec(new Runnable() {
										public void run() {
											viewer.refresh(targetIAdminTreeChild2);
											IAdminTreeChild parent = targetIAdminTreeChild2.getParent().getParent();
											if (parent != null) {
												viewer.setExpandedState(parent, true);
											}
											viewer.setExpandedState(targetIAdminTreeChild2.getParent(), true);
											viewer.setExpandedState(targetIAdminTreeChild2, true);
											if (isCopyPast && (iAdminTreeChild instanceof DirectoryProjects
													|| iAdminTreeChild instanceof DirectoryUsers
													|| iAdminTreeChild instanceof DirectoryProjectApplications
													|| iAdminTreeChild instanceof DirectoryUserApplications)) {
												final AdminTreeviewer adminTree = XMAdminUtil.getInstance()
														.getAdminTree();
												adminTree.setSelection(new StructuredSelection(iAdminTreeChild));
											}
										}
									});
								}
								List<Map<String, String>> statusMapList = directoryRelBatchResponse.getStatusMaps();
								int noOfObjRelCreated = objectArray.length;
								final int noOfObjRelCreated2;
								if (statusMapList != null && !statusMapList.isEmpty()) {
									noOfObjRelCreated = objectArray.length - statusMapList.size();
									Display.getDefault().asyncExec(new Runnable() {
										public void run() {
											String errorMessage = "";
											for (Map<String, String> statusMap : statusMapList) {
												errorMessage += statusMap.get(
														XMAdminUtil.getInstance().getCurrentLocaleEnum().getLangCode());
												errorMessage += "\n";
											}
											if (errorMessage.length() > 0) {
												CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
														messages.dndErrorTitle, errorMessage);
											}
										}
									});
								}
								noOfObjRelCreated2 = noOfObjRelCreated;
								if (noOfObjRelCreated2 > 0) {
									for (String objName : successObjList) {
										XMAdminUtil.getInstance().updateLogFile(messages.relationLbl + " "
												+ messages.betweenLbl + " " + objectType +" '" + objName + "' "
												+ messages.andLbl + " " + messages.directoryObject + " '"
												+ directoryName2 + "' " + messages.objectCreate, MessageType.SUCCESS);
									}
									Display.getDefault().asyncExec(new Runnable() {
										@Override
										public void run() {
											eventBroker.send(CommonConstants.EVENT_BROKER.STATUSBAR,
													noOfObjRelCreated2 + " " + messages.dndStatusBarMsg);
										}
									});
								}
								if (statusMapList != null && !statusMapList.isEmpty()) {
									for (Map<String, String> statusMap : statusMapList) {
										String errorMessage = statusMap
												.get(XMAdminUtil.getInstance().getCurrentLocaleEnum().getLangCode());
										XMAdminUtil.getInstance().updateLogFile(errorMessage, MessageType.FAILURE);
									}
								}
							}
						} catch (UnauthorizedAccessException e) {
							Display.getDefault().asyncExec(new Runnable() {
								@Override
								public void run() {
									CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
											messages.unauthorizedDialogTitle, messages.unauthorizedDialogMsg);
								}
							});
						} catch (Exception e) {
							if (e instanceof ResourceAccessException) {
								Display.getDefault().asyncExec(new Runnable() {
									@Override
									public void run() {
										CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
												messages.errorDialogTitile, messages.serverNotReachable);
									}
								});
							}
							LOGGER.error(e.getMessage());
						}

						monitor.worked(70);
						return Status.OK_STATUS;
					}
				};
				job.setUser(true);
				job.schedule();
			}
		}
	}

	/**
	 * Method for User role user DND rel.
	 *
	 * @param userArray
	 *            {@link User[]}
	 * @param iAdminTreeChild
	 *            {@link IAdminTreeChild}
	 * @param isCopyPast
	 */
	private void userToRoleDNDRel(User[] userArray, final IAdminTreeChild iAdminTreeChild, boolean isCopyPast) {

		if (iAdminTreeChild != null && userArray != null && userArray.length > 0) {
			String roleId = null;
			String roleAdminAreaRelId = null;
			String roleName = null;
			IAdminTreeChild targetIAdminTreeChild = null;
			if (iAdminTreeChild instanceof Role) {
				targetIAdminTreeChild = ((Role) iAdminTreeChild).getRoleChildren().get(RoleUsers.class.getName());
				roleId = ((Role) iAdminTreeChild).getRoleId();
				roleName = ((Role) iAdminTreeChild).getRoleName();
			} else if (iAdminTreeChild instanceof RoleUsers) {
				targetIAdminTreeChild = iAdminTreeChild;
				roleId = ((Role) iAdminTreeChild.getParent()).getRoleId();
				roleName = ((Role) iAdminTreeChild.getParent()).getRoleName();
			} else if (iAdminTreeChild instanceof RoleScopeObjectUsers) {
				targetIAdminTreeChild = iAdminTreeChild;
				roleAdminAreaRelId = ((RelationObj) iAdminTreeChild.getParent()).getRelId();
				roleId = ((Role) iAdminTreeChild.getParent().getParent().getParent()).getRoleId();
				roleName = ((Role) iAdminTreeChild.getParent().getParent().getParent()).getRoleName();
			} else if (iAdminTreeChild instanceof RoleScopeObjectChild) {
				Map<String, IAdminTreeChild> roleScopeObjectChildren = ((RoleScopeObjectChild) iAdminTreeChild)
						.getRoleScopeObjectChildren();
				targetIAdminTreeChild = roleScopeObjectChildren.get(RoleScopeObjectUsers.class.getName());
				roleAdminAreaRelId = ((RelationObj) targetIAdminTreeChild.getParent()).getRelId();
				roleId = ((Role) iAdminTreeChild.getParent().getParent()).getRoleId();
				roleName = ((Role) iAdminTreeChild.getParent().getParent()).getRoleName();
			}

			if (targetIAdminTreeChild != null && roleId != null) {
				final IAdminTreeChild targetIAdminTreeChild2 = targetIAdminTreeChild;
				final String roleId2 = roleId;
				final String roleAdminAreaRelId2 = roleAdminAreaRelId;
				final String roleName2 = roleName;
				Job job = new Job("Creating Relations...") {

					@Override
					protected IStatus run(IProgressMonitor monitor) {
						monitor.beginTask("Creating Relations..", 100);
						monitor.worked(30);
						try {
							List<RoleUserRelRequest> roleUserRelRequestList = new ArrayList<>();
							for (User user : userArray) {
								RoleUserRelRequest roleUserRelRequest = new RoleUserRelRequest();
								roleUserRelRequest.setRoleId(roleId2);
								roleUserRelRequest.setUserId(user.getUserId());
								roleUserRelRequest.setRoleAdminAreaRelId(roleAdminAreaRelId2);
								roleUserRelRequestList.add(roleUserRelRequest);
							}

							RoleUserRelController roleUserRelController = new RoleUserRelController();
							RoleUserRelResponse roleUserRelResponse = roleUserRelController
									.createMultiRoleUserRel(roleUserRelRequestList);

							if (roleUserRelResponse != null) {
								boolean isRelationCreated = false;
								Iterable<RoleUserRelTbl> roleUserRelTbls = roleUserRelResponse.getRoleUserRelTbls();
								List<String> successObjList = new ArrayList<>();
								for (RoleUserRelTbl roleUserRelTbl : roleUserRelTbls) {
									String relId = roleUserRelTbl.getRoleUserRelId();
									if (relId != null) {
										for (User user : userArray) {
											if (user.getUserId().equals(roleUserRelTbl.getUserId().getUserId())) {
												successObjList.add(user.getName());
												isRelationCreated = true;
											}
										}
										/*
										 * isRelationCreated = true; break;
										 */
									}
								}
								if (isRelationCreated) {
									Display.getDefault().asyncExec(new Runnable() {
										public void run() {
											viewer.refresh(targetIAdminTreeChild2);
											if (!viewer.getExpandedState(targetIAdminTreeChild2.getParent())) {
												(new AdminTreeExpansion(targetIAdminTreeChild2.getParent()))
														.loadObjects();
												viewer.setExpandedState(targetIAdminTreeChild2.getParent(), true);
											}
											viewer.setExpandedState(targetIAdminTreeChild2, true);
											if (isCopyPast && (iAdminTreeChild instanceof RoleScopeObjectUsers
													|| iAdminTreeChild instanceof RoleUsers)) {
												final AdminTreeviewer adminTree = XMAdminUtil.getInstance()
														.getAdminTree();
												adminTree.setSelection(new StructuredSelection(iAdminTreeChild));
											}
										}
									});
								}
								List<Map<String, String>> statusMapList = roleUserRelResponse.getStatusMap();
								int noOfObjRelCreated = userArray.length;
								final int noOfObjRelCreated2;
								if (statusMapList != null && !statusMapList.isEmpty()) {
									noOfObjRelCreated = userArray.length - statusMapList.size();
									Display.getDefault().asyncExec(new Runnable() {
										public void run() {
											String errorMessage = "";
											for (Map<String, String> statusMap : statusMapList) {
												errorMessage += statusMap.get(
														XMAdminUtil.getInstance().getCurrentLocaleEnum().getLangCode());
												errorMessage += "\n";
											}
											if (errorMessage.length() > 0) {
												CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
														messages.dndErrorTitle, errorMessage);
											}
										}
									});
								}
								noOfObjRelCreated2 = noOfObjRelCreated;
								if (noOfObjRelCreated2 > 0) {
									for (String objName : successObjList) {
										XMAdminUtil.getInstance().updateLogFile(messages.relationLbl + " "
												+ messages.betweenLbl + " " + messages.userObject + " '" + objName
												+ "' " + messages.andLbl + " " + messages.roleObject + " '" + roleName2
												+ "' " + messages.objectCreate, MessageType.SUCCESS);
									}
									Display.getDefault().asyncExec(new Runnable() {
										@Override
										public void run() {
											eventBroker.send(CommonConstants.EVENT_BROKER.STATUSBAR,
													noOfObjRelCreated2 + " " + messages.dndStatusBarMsg);
										}
									});
								}
								if (statusMapList != null && !statusMapList.isEmpty()) {
									for (Map<String, String> statusMap : statusMapList) {
										String errorMessage = statusMap
												.get(XMAdminUtil.getInstance().getCurrentLocaleEnum().getLangCode());
										XMAdminUtil.getInstance().updateLogFile(errorMessage, MessageType.FAILURE);
									}
								}
							}
						} catch (UnauthorizedAccessException e) {
							Display.getDefault().asyncExec(new Runnable() {
								@Override
								public void run() {
									CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
											messages.unauthorizedDialogTitle, messages.unauthorizedDialogMsg);
								}
							});
						} catch (Exception e) {
							if (e instanceof ResourceAccessException) {
								Display.getDefault().asyncExec(new Runnable() {
									@Override
									public void run() {
										CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
												messages.errorDialogTitile, messages.serverNotReachable);
									}
								});
							}
							LOGGER.error(e.getMessage());
						}

						monitor.worked(70);
						return Status.OK_STATUS;
					}
				};
				job.setUser(true);
				job.schedule();
			}
		}
	}

	/**
	 * Method for Admin area role DND rel.
	 *
	 * @param adminAreaArray
	 *            {@link AdministrationArea[]}
	 * @param iAdminTreeChild
	 *            {@link IAdminTreeChild}
	 * @param isCopyPast
	 */
	private void adminAreaToRoleDNDRel(final AdministrationArea[] adminAreaArray, final IAdminTreeChild iAdminTreeChild,
			boolean isCopyPast) {
		if (iAdminTreeChild != null && adminAreaArray != null && adminAreaArray.length > 0) {
			String roleId = null;
			IAdminTreeChild targetIAdminTreeChild = null;
			String roleName = null;
			if (iAdminTreeChild instanceof Role) {
				if ((CommonConstants.SuperAdminRole.NAME.equals(((Role) iAdminTreeChild).getRoleName()))) {
					return;
				}
				targetIAdminTreeChild = ((Role) iAdminTreeChild).getRoleChildren()
						.get(RoleScopeObjects.class.getName());
				roleId = ((Role) iAdminTreeChild).getRoleId();
				roleName = ((Role) iAdminTreeChild).getRoleName();
			} else if (iAdminTreeChild instanceof RoleScopeObjects) {
				targetIAdminTreeChild = (RoleScopeObjects) iAdminTreeChild;
				roleId = ((Role) iAdminTreeChild.getParent()).getRoleId();
				roleName = ((Role) iAdminTreeChild.getParent()).getRoleName();
			}

			if (targetIAdminTreeChild != null && roleId != null) {
				final String roleId2 = roleId;
				final IAdminTreeChild targetIAdminTreeChild2 = targetIAdminTreeChild;
				final String roleName2 = roleName;
				Job job = new Job("Creating Relations...") {

					@Override
					protected IStatus run(IProgressMonitor monitor) {
						monitor.beginTask("Creating Relations..", 100);
						monitor.worked(30);

						try {
							List<RoleAdminAreaRelRequest> roleAARelRequestList = new ArrayList<>();
							for (AdministrationArea adminArea : adminAreaArray) {
								RoleAdminAreaRelRequest roleAdminAreaRelRequest = new RoleAdminAreaRelRequest();
								roleAdminAreaRelRequest.setRoleId(roleId2);
								roleAdminAreaRelRequest.setAdminAreaId(adminArea.getAdministrationAreaId());
								roleAARelRequestList.add(roleAdminAreaRelRequest);
							}

							RoleAdminAreaRelController roleUserRelController = new RoleAdminAreaRelController();
							RoleAdminAreaRelResponse roleAdminAreaRelResponse = roleUserRelController
									.createRoleAdminAreaRel(roleAARelRequestList);

							if (roleAdminAreaRelResponse != null) {
								boolean isRelationCreated = false;
								Iterable<RoleAdminAreaRelTbl> roleAdminAreaRelTbls = roleAdminAreaRelResponse
										.getRoleAdminAreaRelTbls();
								List<String> successObjList = new ArrayList<>();
								for (RoleAdminAreaRelTbl roleAdminAreaRelTbl : roleAdminAreaRelTbls) {
									String relId = roleAdminAreaRelTbl.getRoleAdminAreaRelId();
									if (relId != null) {
										for (AdministrationArea adminArea : adminAreaArray) {
											if (adminArea.getAdministrationAreaId()
													.equals(roleAdminAreaRelTbl.getAdminAreaId().getAdminAreaId())) {
												successObjList.add(adminArea.getName());
												RelationObj relObj = (RelationObj) adminArea.getAdapter(
														RoleScopeObjectChild.class, targetIAdminTreeChild2, relId,
														true);

												((RoleScopeObjectChild) relObj.getContainerObj())
														.addFixedChildren(relObj);

												((RoleScopeObjects) targetIAdminTreeChild2).add(relId, relObj);
												isRelationCreated = true;
											}
										}
									}
								}

								if (isRelationCreated) {
									Display.getDefault().asyncExec(new Runnable() {
										public void run() {
											viewer.refresh(targetIAdminTreeChild2);
											if (!viewer.getExpandedState(targetIAdminTreeChild2.getParent())) {
												(new AdminTreeExpansion(targetIAdminTreeChild2.getParent()))
														.loadObjects();
												viewer.setExpandedState(targetIAdminTreeChild2.getParent(), true);
											}
											viewer.setExpandedState(targetIAdminTreeChild2, true);
											if (isCopyPast && iAdminTreeChild instanceof RoleScopeObjects) {
												final AdminTreeviewer adminTree = XMAdminUtil.getInstance()
														.getAdminTree();
												adminTree.setSelection(new StructuredSelection(iAdminTreeChild));
											}
										}
									});
								}
								List<Map<String, String>> statusMapList = roleAdminAreaRelResponse.getStatusMap();
								int noOfObjRelCreated = adminAreaArray.length;
								final int noOfObjRelCreated2;
								if (statusMapList != null && !statusMapList.isEmpty()) {
									noOfObjRelCreated = adminAreaArray.length - statusMapList.size();
									Display.getDefault().asyncExec(new Runnable() {
										public void run() {
											String errorMessage = "";
											for (Map<String, String> statusMap : statusMapList) {
												errorMessage += statusMap.get(
														XMAdminUtil.getInstance().getCurrentLocaleEnum().getLangCode());
												errorMessage += "\n";
											}
											if (errorMessage.length() > 0) {
												CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
														messages.dndErrorTitle, errorMessage);
											}
										}
									});
								}
								noOfObjRelCreated2 = noOfObjRelCreated;
								if (noOfObjRelCreated2 > 0) {
									for (String objName : successObjList) {
										XMAdminUtil.getInstance().updateLogFile(messages.relationLbl + " "
												+ messages.betweenLbl + " " + messages.administrationAreaObject + " '"
												+ objName + "' " + messages.andLbl + " " + messages.roleObject + " '"
												+ roleName2 + "' " + messages.objectCreate, MessageType.SUCCESS);
									}
									Display.getDefault().asyncExec(new Runnable() {
										@Override
										public void run() {
											eventBroker.send(CommonConstants.EVENT_BROKER.STATUSBAR,
													noOfObjRelCreated2 + " " + messages.dndStatusBarMsg);
										}
									});
								}
								if (statusMapList != null && !statusMapList.isEmpty()) {
									for (Map<String, String> statusMap : statusMapList) {
										String errorMessage = statusMap
												.get(XMAdminUtil.getInstance().getCurrentLocaleEnum().getLangCode());
										XMAdminUtil.getInstance().updateLogFile(errorMessage, MessageType.FAILURE);
									}
								}
							}
						} catch (UnauthorizedAccessException e) {
							Display.getDefault().asyncExec(new Runnable() {
								@Override
								public void run() {
									CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
											messages.unauthorizedDialogTitle, messages.unauthorizedDialogMsg);
								}
							});
						} catch (Exception e) {
							if (e instanceof ResourceAccessException) {
								Display.getDefault().asyncExec(new Runnable() {
									@Override
									public void run() {
										CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
												messages.errorDialogTitile, messages.serverNotReachable);
									}
								});
							}
							LOGGER.error(e.getMessage());
						}

						monitor.worked(70);
						return Status.OK_STATUS;
					}
				};
				job.setUser(true);
				job.schedule();
			}
		}
	}

	/**
	 * Method for Objects to admin menu DND rel.
	 *
	 * @param objectArray
	 *            {@link Object[]}
	 * @param iAdminTreeChild
	 *            {@link IAdminTreeChild}
	 * @param isCopyPast
	 */
	private void objectsToAdminMenuDNDRel(final Object[] objectArray, final IAdminTreeChild iAdminTreeChild,
			boolean isCopyPast) {
		if (iAdminTreeChild != null && objectArray != null && objectArray.length > 0) {
			IAdminTreeChild targetIAdminTreeChild = null;

			if (iAdminTreeChild instanceof AdminMenu) {
				if (objectArray[0] instanceof User) {
					targetIAdminTreeChild = ((AdminMenu) iAdminTreeChild).getAdminMenuChildren()
							.get(AdminUsers.class.getSimpleName());
				} else if (objectArray[0] instanceof ProjectApplication) {
					targetIAdminTreeChild = ((AdminMenu) iAdminTreeChild).getAdminMenuChildren()
							.get(AdminProjectApps.class.getSimpleName());
				} else if (objectArray[0] instanceof UserApplication) {
					targetIAdminTreeChild = ((AdminMenu) iAdminTreeChild).getAdminMenuChildren()
							.get(AdminUserApps.class.getSimpleName());
				}
			}

			else if (iAdminTreeChild instanceof AdminUsers || iAdminTreeChild instanceof AdminProjectApps
					|| iAdminTreeChild instanceof AdminUserApps) {
				targetIAdminTreeChild = iAdminTreeChild;
			}

			final IAdminTreeChild targetIAdminTreeChild2 = targetIAdminTreeChild;

			if (targetIAdminTreeChild2 != null) {
				Job job = new Job("Creating Relations...") {

					@Override
					protected IStatus run(IProgressMonitor monitor) {
						monitor.beginTask("Creating Relations..", 100);
						monitor.worked(30);
						try {
							List<AdminMenuConfigRequest> adminMenuRelRequestList = new ArrayList<>();
							for (Object object : objectArray) {
								AdminMenuConfigRequest adminMenuRelRequest = new AdminMenuConfigRequest();
								if (object instanceof User) {
									adminMenuRelRequest.setObjectId(((User) object).getUserId());
									adminMenuRelRequest.setObjectType(AdminMenuConfig.USER);
									adminMenuRelRequestList.add(adminMenuRelRequest);
								} else if (object instanceof ProjectApplication) {
									adminMenuRelRequest
											.setObjectId(((ProjectApplication) object).getProjectApplicationId());
									adminMenuRelRequest.setObjectType(AdminMenuConfig.PROJECTAPPLICATION);
									adminMenuRelRequestList.add(adminMenuRelRequest);
								} else if (object instanceof UserApplication) {
									adminMenuRelRequest.setObjectId(((UserApplication) object).getUserApplicationId());
									adminMenuRelRequest.setObjectType(AdminMenuConfig.USERAPPLICATION);
									adminMenuRelRequestList.add(adminMenuRelRequest);
								}
							}

							AdminMenuConfigRelController adminMenuConfigController = new AdminMenuConfigRelController();
							AdminMenuConfigResponse adminMenuConfigResponse = adminMenuConfigController
									.createAdminMenuMultiRelation(adminMenuRelRequestList);

							if (adminMenuConfigResponse != null) {
								boolean isRelationCreated = false;
								String objectType = null;
								List<String> successObjList = new ArrayList<>();
								Iterable<AdminMenuConfigTbl> adminMenuConfigTbls = adminMenuConfigResponse
										.getAdminMenuConfigTbls();
								for (AdminMenuConfigTbl adminMenuConfigTbl : adminMenuConfigTbls) {
									String relId = adminMenuConfigTbl.getAdminMenuConfigId();
									if (relId != null) {
										for (Object object : objectArray) {
											String objectId = null;
											String objectName = null;
											if (object instanceof User) {
												objectType = messages.userObject;
												objectId = ((User) object).getUserId();
												objectName = ((User) object).getName();
											} else if (object instanceof UserApplication) {
												objectType = messages.userApplicationObject;
												objectId = ((UserApplication) object).getUserApplicationId();
												objectName = ((UserApplication) object).getName();
											} else if (object instanceof ProjectApplication) {
												objectType = messages.projectApplicationObject;
												objectId = ((ProjectApplication) object).getProjectApplicationId();
												objectName = ((ProjectApplication) object).getName();
											}
											if (objectId != null && objectId.equals(adminMenuConfigTbl.getObjectId())) {
												successObjList.add(objectName);
												isRelationCreated = true;
											}
										}
										/*
										 * isRelationCreated = true; break;
										 */
									}
								}

								if (isRelationCreated) {
									Display.getDefault().asyncExec(new Runnable() {
										public void run() {
											viewer.refresh(targetIAdminTreeChild2);
											IAdminTreeChild parent = targetIAdminTreeChild2.getParent();
											if (parent != null) {
												viewer.setExpandedState(parent, true);
											}
											viewer.setExpandedState(targetIAdminTreeChild2, true);
											if (isCopyPast && (iAdminTreeChild instanceof AdminUsers
													|| iAdminTreeChild instanceof AdminProjectApps
													|| iAdminTreeChild instanceof AdminUserApps)) {
												final AdminTreeviewer adminTree = XMAdminUtil.getInstance()
														.getAdminTree();
												adminTree.setSelection(new StructuredSelection(iAdminTreeChild));
											}
										}
									});
								}
								List<Map<String, String>> statusMapList = adminMenuConfigResponse.getStatusMaps();
								int noOfObjRelCreated = objectArray.length;
								final int noOfObjRelCreated2;
								if (statusMapList != null && !statusMapList.isEmpty()) {
									noOfObjRelCreated = objectArray.length - statusMapList.size();
									Display.getDefault().asyncExec(new Runnable() {
										public void run() {
											String errorMessage = "";
											for (Map<String, String> statusMap : statusMapList) {
												errorMessage += statusMap.get(
														XMAdminUtil.getInstance().getCurrentLocaleEnum().getLangCode());
												errorMessage += "\n";
											}
											if (errorMessage.length() > 0) {
												CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
														messages.dndErrorTitle, errorMessage);
											}
										}
									});
								}
								noOfObjRelCreated2 = noOfObjRelCreated;
								if (noOfObjRelCreated2 > 0) {
									for (String objName : successObjList) {
										XMAdminUtil.getInstance()
												.updateLogFile(messages.relationLbl + " " + messages.betweenLbl + " "
														+ objectType +" '" + objName + "' " + messages.andLbl + " "
														+ messages.adminMenuNode + " " + messages.objectCreate,
														MessageType.SUCCESS);
									}

									Display.getDefault().asyncExec(new Runnable() {
										@Override
										public void run() {
											eventBroker.send(CommonConstants.EVENT_BROKER.STATUSBAR,
													noOfObjRelCreated2 + " " + messages.dndStatusBarMsg);
										}
									});
								}
								if (statusMapList != null && !statusMapList.isEmpty()) {
									for (Map<String, String> statusMap : statusMapList) {
										String errorMessage = statusMap
												.get(XMAdminUtil.getInstance().getCurrentLocaleEnum().getLangCode());
										XMAdminUtil.getInstance().updateLogFile(errorMessage, MessageType.FAILURE);
									}
								}
							}
						} catch (UnauthorizedAccessException e) {
							Display.getDefault().asyncExec(new Runnable() {
								@Override
								public void run() {
									CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
											messages.unauthorizedDialogTitle, messages.unauthorizedDialogMsg);
								}
							});
						} catch (Exception e) {
							if (e instanceof ResourceAccessException) {
								Display.getDefault().asyncExec(new Runnable() {
									@Override
									public void run() {
										CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
												messages.errorDialogTitile, messages.serverNotReachable);
									}
								});
							}
							LOGGER.error(e.getMessage());
						}

						monitor.worked(70);
						return Status.OK_STATUS;
					}
				};
				job.setUser(true);
				job.schedule();
			}
		}

	}

	/**
	 * User to user group DND rel.
	 *
	 * @param userArray
	 *            the user array
	 * @param iAdminTreeChild
	 *            the i admin tree child
	 * @param isCopyPast
	 */
	private void userToUserGroupDNDRel(final User[] userArray, final IAdminTreeChild iAdminTreeChild,
			boolean isCopyPast) {
		if (iAdminTreeChild != null && userArray != null && userArray.length > 0) {
			String grouptId = null;
			IAdminTreeChild targetIAdminTreeChild = null;
			String groupName = null;
			if (iAdminTreeChild instanceof UserGroupModel) {
				targetIAdminTreeChild = ((UserGroupModel) iAdminTreeChild).getUserGroupChildren()
						.get(UserGroupUsers.class.getSimpleName());
				grouptId = ((UserGroupModel) iAdminTreeChild).getGroupId();
				groupName = ((UserGroupModel) iAdminTreeChild).getName();
			} else if (iAdminTreeChild instanceof UserGroupUsers) {
				targetIAdminTreeChild = iAdminTreeChild;
				grouptId = ((UserGroupModel) iAdminTreeChild.getParent()).getGroupId();
				groupName = ((UserGroupModel) iAdminTreeChild.getParent()).getName();
			}

			if (targetIAdminTreeChild != null && grouptId != null) {
				final String groupId2 = grouptId;
				final IAdminTreeChild targetIAdminTreeChild2 = targetIAdminTreeChild;
				final String groupName2 = groupName;
				Job job = new Job("Creating Relations...") {

					@Override
					protected IStatus run(IProgressMonitor monitor) {
						monitor.beginTask("Creating Relations..", 100);
						monitor.worked(30);
						try {
							List<GroupRelRequest> projectRelRequestList = new ArrayList<>();
							for (User user : userArray) {
								GroupRelRequest groupRelRequest = new GroupRelRequest();
								groupRelRequest.setGroupId(groupId2);
								groupRelRequest.setObjectId(user.getUserId());
								projectRelRequestList.add(groupRelRequest);
							}
							GroupRelController adminAreaProjectRelController = new GroupRelController();
							GroupRelBatchResponse groupRelResponse = adminAreaProjectRelController
									.createGroupRelations(new GroupRelBatchRequest(projectRelRequestList));

							if (groupRelResponse != null) {
								boolean isRelationCreated = false;
								List<String> successObjList = new ArrayList<>();
								Iterable<GroupRelResponse> groupRefTbls = groupRelResponse.getGroupRelResponses();
								for (GroupRelResponse groupRefTbl : groupRefTbls) {
									String relId = groupRefTbl.getGroupRefId();
									if (relId != null) {
										for (User user : userArray) {
											if (user.getUserId().equals(groupRefTbl.getUsersTbl().getUserId())) {
												successObjList.add(user.getName());
												RelationObj relObj = (RelationObj) user.getAdapter(UserGroupUsers.class,
														targetIAdminTreeChild2, relId, true);
												((UserGroupUsers) targetIAdminTreeChild2).add(relId, relObj);

												isRelationCreated = true;
											}
										}
									}
								}
								if (isRelationCreated) {
									Display.getDefault().asyncExec(new Runnable() {
										public void run() {
											viewer.refresh(targetIAdminTreeChild2);
											if (!viewer.getExpandedState(targetIAdminTreeChild2.getParent())) {
												(new AdminTreeExpansion(targetIAdminTreeChild2.getParent()))
														.loadObjects();
												viewer.setExpandedState(targetIAdminTreeChild2.getParent(), true);
											}
											viewer.setExpandedState(targetIAdminTreeChild2, true);
											if (isCopyPast && iAdminTreeChild instanceof UserGroupUsers) {
												final AdminTreeviewer adminTree = XMAdminUtil.getInstance()
														.getAdminTree();
												adminTree.setSelection(new StructuredSelection(iAdminTreeChild));
											}
										}
									});
								}
								List<Map<String, String>> statusMapList = groupRelResponse.getStatusMaps();
								int noOfObjRelCreated = userArray.length;
								final int noOfObjRelCreated2;
								if (statusMapList != null && !statusMapList.isEmpty()) {
									noOfObjRelCreated = userArray.length - statusMapList.size();
									Display.getDefault().asyncExec(new Runnable() {
										public void run() {
											String errorMessage = "";
											for (Map<String, String> statusMap : statusMapList) {
												errorMessage += statusMap.get(
														XMAdminUtil.getInstance().getCurrentLocaleEnum().getLangCode());
												errorMessage += "\n";
											}
											if (errorMessage.length() > 0) {
												CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
														messages.dndErrorTitle, errorMessage);
											}
										}
									});
								}
								noOfObjRelCreated2 = noOfObjRelCreated;
								if (noOfObjRelCreated2 > 0) {
									for (String objName : successObjList) {
										XMAdminUtil.getInstance().updateLogFile(
												messages.relationLbl + " " + messages.betweenLbl + " "
														+ messages.userObject +" '" + objName + "' "
														+ messages.andLbl + " " + messages.userGroupObject + " '"
														+ groupName2 + "' " + messages.objectCreate,
												MessageType.SUCCESS);
									}
									Display.getDefault().asyncExec(new Runnable() {
										@Override
										public void run() {
											eventBroker.send(CommonConstants.EVENT_BROKER.STATUSBAR,
													noOfObjRelCreated2 + " " + messages.dndStatusBarMsg);
										}
									});
								}
								if (statusMapList != null && !statusMapList.isEmpty()) {
									for (Map<String, String> statusMap : statusMapList) {
										String errorMessage = statusMap
												.get(XMAdminUtil.getInstance().getCurrentLocaleEnum().getLangCode());
										XMAdminUtil.getInstance().updateLogFile(errorMessage, MessageType.FAILURE);
									}
								}
							}
						} catch (UnauthorizedAccessException e) {
							Display.getDefault().asyncExec(new Runnable() {
								@Override
								public void run() {
									CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
											messages.unauthorizedDialogTitle, messages.unauthorizedDialogMsg);
								}
							});
						} catch (Exception e) {
							if (e instanceof ResourceAccessException) {
								Display.getDefault().asyncExec(new Runnable() {
									@Override
									public void run() {
										CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
												messages.errorDialogTitile, messages.serverNotReachable);
									}
								});
							}
							LOGGER.error(e.getMessage());
						}

						monitor.worked(70);
						return Status.OK_STATUS;
					}
				};
				job.setUser(true);
				job.schedule();
			}
		}
	}

	/**
	 * Project to project group DND rel.
	 *
	 * @param projectEventData
	 *            the project event data
	 * @param iAdminTreeChild
	 *            the i admin tree child
	 * @param isCopyPast
	 */
	private void projectToProjectGroupDNDRel(Project[] projectEventData, IAdminTreeChild iAdminTreeChild,
			boolean isCopyPast) {
		if (iAdminTreeChild != null && projectEventData != null && projectEventData.length > 0) {
			String grouptId = null;
			String groupName = null;
			IAdminTreeChild targetIAdminTreeChild = null;
			if (iAdminTreeChild instanceof ProjectGroupModel) {
				targetIAdminTreeChild = ((ProjectGroupModel) iAdminTreeChild).getProjectGroupChildren()
						.get(ProjectGroupProjects.class.getSimpleName());
				grouptId = ((ProjectGroupModel) iAdminTreeChild).getGroupId();
				groupName = ((ProjectGroupModel) iAdminTreeChild).getName();
			} else if (iAdminTreeChild instanceof ProjectGroupProjects) {
				targetIAdminTreeChild = iAdminTreeChild;
				grouptId = ((ProjectGroupModel) iAdminTreeChild.getParent()).getGroupId();
				groupName = ((ProjectGroupModel) iAdminTreeChild.getParent()).getName();
			}

			if (targetIAdminTreeChild != null && grouptId != null) {
				final String groupId2 = grouptId;
				final IAdminTreeChild targetIAdminTreeChild2 = targetIAdminTreeChild;
				final String groupName2 = groupName;
				Job job = new Job("Creating Relations...") {

					@Override
					protected IStatus run(IProgressMonitor monitor) {
						monitor.beginTask("Creating Relations..", 100);
						monitor.worked(30);
						try {
							List<GroupRelRequest> projectRelRequestList = new ArrayList<>();
							for (Project project : projectEventData) {
								GroupRelRequest groupRelRequest = new GroupRelRequest();
								groupRelRequest.setGroupId(groupId2);
								groupRelRequest.setObjectId(project.getProjectId());
								projectRelRequestList.add(groupRelRequest);
							}
							GroupRelController adminAreaProjectRelController = new GroupRelController();
							GroupRelBatchResponse groupRelResponse = adminAreaProjectRelController
									.createGroupRelations(new GroupRelBatchRequest(projectRelRequestList));

							if (groupRelResponse != null) {
								boolean isRelationCreated = false;
								Iterable<GroupRelResponse> groupRefTbls = groupRelResponse.getGroupRelResponses();
								List<String> successObjList = new ArrayList<>();
								for (GroupRelResponse groupRefTbl : groupRefTbls) {
									String relId = groupRefTbl.getGroupRefId();
									if (relId != null) {
										for (Project project : projectEventData) {
											if (project.getProjectId()
													.equals(groupRefTbl.getProjectsTbl().getProjectId())) {
												successObjList.add(project.getName());
												RelationObj relObj = (RelationObj) project.getAdapter(
														ProjectGroupProjects.class, targetIAdminTreeChild2, relId,
														true);
												((ProjectGroupProjects) targetIAdminTreeChild2).add(relId, relObj);

												isRelationCreated = true;
											}
										}
									}
								}
								if (isRelationCreated) {
									Display.getDefault().asyncExec(new Runnable() {
										public void run() {
											viewer.refresh(targetIAdminTreeChild2);
											if (!viewer.getExpandedState(targetIAdminTreeChild2.getParent())) {
												(new AdminTreeExpansion(targetIAdminTreeChild2.getParent()))
														.loadObjects();
												viewer.setExpandedState(targetIAdminTreeChild2.getParent(), true);
											}
											viewer.setExpandedState(targetIAdminTreeChild2, true);
											if (isCopyPast && iAdminTreeChild instanceof ProjectGroupProjects) {
												final AdminTreeviewer adminTree = XMAdminUtil.getInstance()
														.getAdminTree();
												adminTree.setSelection(new StructuredSelection(iAdminTreeChild));
											}
										}
									});
								}
								List<Map<String, String>> statusMapList = groupRelResponse.getStatusMaps();
								int noOfObjRelCreated = projectEventData.length;
								final int noOfObjRelCreated2;
								if (statusMapList != null && !statusMapList.isEmpty()) {
									noOfObjRelCreated = projectEventData.length - statusMapList.size();
									Display.getDefault().asyncExec(new Runnable() {
										public void run() {
											String errorMessage = "";
											for (Map<String, String> statusMap : statusMapList) {
												errorMessage += statusMap.get(
														XMAdminUtil.getInstance().getCurrentLocaleEnum().getLangCode());
												errorMessage += "\n";
											}
											if (errorMessage.length() > 0) {
												CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
														messages.dndErrorTitle, errorMessage);
											}
										}
									});
								}
								noOfObjRelCreated2 = noOfObjRelCreated;
								if (noOfObjRelCreated2 > 0) {
									for (String objName : successObjList) {
										XMAdminUtil.getInstance()
												.updateLogFile(messages.relationLbl + " " + messages.betweenLbl + " "
														+ messages.projectObject +" '" + objName + "' "
														+ messages.andLbl + " " + messages.projectGroupObject
														+ "  '" + groupName2 + "' " + messages.objectCreate,
														MessageType.SUCCESS);
									}
									Display.getDefault().asyncExec(new Runnable() {
										@Override
										public void run() {
											eventBroker.send(CommonConstants.EVENT_BROKER.STATUSBAR,
													noOfObjRelCreated2 + " " + messages.dndStatusBarMsg);
										}
									});
								}
								if (statusMapList != null && !statusMapList.isEmpty()) {
									for (Map<String, String> statusMap : statusMapList) {
										String errorMessage = statusMap
												.get(XMAdminUtil.getInstance().getCurrentLocaleEnum().getLangCode());
										XMAdminUtil.getInstance().updateLogFile(errorMessage, MessageType.FAILURE);
									}
								}
							}
						} catch (UnauthorizedAccessException e) {
							Display.getDefault().asyncExec(new Runnable() {
								@Override
								public void run() {
									CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
											messages.unauthorizedDialogTitle, messages.unauthorizedDialogMsg);
								}
							});
						} catch (Exception e) {
							if (e instanceof ResourceAccessException) {
								Display.getDefault().asyncExec(new Runnable() {
									@Override
									public void run() {
										CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
												messages.errorDialogTitile, messages.serverNotReachable);
									}
								});
							}
							LOGGER.error(e.getMessage());
						}

						monitor.worked(70);
						return Status.OK_STATUS;
					}
				};
				job.setUser(true);
				job.schedule();
			}
		}
	}

	/**
	 * User app to user app group DND rel.
	 *
	 * @param userAppEventData
	 *            the user app event data
	 * @param iAdminTreeChild
	 *            the i admin tree child
	 * @param isCopyPast
	 */
	private void userAppToUserAppGroupDNDRel(UserApplication[] userAppEventData, IAdminTreeChild iAdminTreeChild,
			boolean isCopyPast) {
		if (iAdminTreeChild != null && userAppEventData != null && userAppEventData.length > 0) {
			String grouptId = null;
			IAdminTreeChild targetIAdminTreeChild = null;
			String groupName = null;
			if (iAdminTreeChild instanceof UserApplicationGroup) {
				targetIAdminTreeChild = ((UserApplicationGroup) iAdminTreeChild).getUserAppGroupChildren()
						.get(UserAppGroupUserApps.class.getSimpleName());
				grouptId = ((UserApplicationGroup) iAdminTreeChild).getGroupId();
				groupName = ((UserApplicationGroup) iAdminTreeChild).getName();
			} else if (iAdminTreeChild instanceof UserAppGroupUserApps) {
				targetIAdminTreeChild = iAdminTreeChild;
				grouptId = ((UserApplicationGroup) iAdminTreeChild.getParent()).getGroupId();
				groupName = ((UserApplicationGroup) iAdminTreeChild.getParent()).getName();
			}

			if (targetIAdminTreeChild != null && grouptId != null) {
				final String groupId2 = grouptId;
				final IAdminTreeChild targetIAdminTreeChild2 = targetIAdminTreeChild;
				final String groupName2 = groupName;
				Job job = new Job("Creating Relations...") {

					@Override
					protected IStatus run(IProgressMonitor monitor) {
						monitor.beginTask("Creating Relations..", 100);
						monitor.worked(30);
						try {
							List<GroupRelRequest> projectRelRequestList = new ArrayList<>();
							for (UserApplication userApplication : userAppEventData) {
								GroupRelRequest groupRelRequest = new GroupRelRequest();
								groupRelRequest.setGroupId(groupId2);
								groupRelRequest.setObjectId(userApplication.getUserApplicationId());
								projectRelRequestList.add(groupRelRequest);
							}
							GroupRelController adminAreaProjectRelController = new GroupRelController();
							GroupRelBatchResponse groupRelResponse = adminAreaProjectRelController
									.createGroupRelations(new GroupRelBatchRequest(projectRelRequestList));

							if (groupRelResponse != null) {
								boolean isRelationCreated = false;
								Iterable<GroupRelResponse> groupRefTbls = groupRelResponse.getGroupRelResponses();
								List<String> successObjList = new ArrayList<>();
								for (GroupRelResponse groupRefTbl : groupRefTbls) {
									String relId = groupRefTbl.getGroupRefId();
									if (relId != null) {
										for (UserApplication userApplication : userAppEventData) {
											if (userApplication.getUserApplicationId().equals(
													groupRefTbl.getUserApplicationsTbl().getUserApplicationId())) {
												successObjList.add(userApplication.getName());
												RelationObj relObj = (RelationObj) userApplication.getAdapter(
														UserAppGroupUserApps.class, targetIAdminTreeChild2, relId,
														true);
												((UserAppGroupUserApps) targetIAdminTreeChild2).add(relId, relObj);
												isRelationCreated = true;
											}
										}
									}
								}
								if (isRelationCreated) {
									Display.getDefault().asyncExec(new Runnable() {
										public void run() {
											viewer.refresh(targetIAdminTreeChild2);
											if (!viewer.getExpandedState(targetIAdminTreeChild2.getParent())) {
												(new AdminTreeExpansion(targetIAdminTreeChild2.getParent()))
														.loadObjects();
												viewer.setExpandedState(targetIAdminTreeChild2.getParent(), true);
											}
											viewer.setExpandedState(targetIAdminTreeChild2, true);
											if (isCopyPast && iAdminTreeChild instanceof UserAppGroupUserApps) {
												final AdminTreeviewer adminTree = XMAdminUtil.getInstance()
														.getAdminTree();
												adminTree.setSelection(new StructuredSelection(iAdminTreeChild));
											}
										}
									});
								}
								List<Map<String, String>> statusMapList = groupRelResponse.getStatusMaps();
								int noOfObjRelCreated = userAppEventData.length;
								final int noOfObjRelCreated2;
								if (statusMapList != null && !statusMapList.isEmpty()) {
									noOfObjRelCreated = userAppEventData.length - statusMapList.size();
									Display.getDefault().asyncExec(new Runnable() {
										public void run() {
											String errorMessage = "";
											for (Map<String, String> statusMap : statusMapList) {
												errorMessage += statusMap.get(
														XMAdminUtil.getInstance().getCurrentLocaleEnum().getLangCode());
												errorMessage += "\n";
											}
											if (errorMessage.length() > 0) {
												CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
														messages.dndErrorTitle, errorMessage);
											}
										}
									});
								}
								noOfObjRelCreated2 = noOfObjRelCreated;
								if (noOfObjRelCreated2 > 0) {
									for (String objName : successObjList) {
										XMAdminUtil.getInstance()
												.updateLogFile(messages.relationLbl + " " + messages.betweenLbl + " "
														+ messages.userApplicationObject +" '" + objName + "' "
														+ messages.andLbl + " " + messages.userAppGroupObject
														+ " '" + groupName2 + "' "+ messages.objectCreate,
														MessageType.SUCCESS);
									}
									Display.getDefault().asyncExec(new Runnable() {
										@Override
										public void run() {
											eventBroker.send(CommonConstants.EVENT_BROKER.STATUSBAR,
													noOfObjRelCreated2 + " " + messages.dndStatusBarMsg);
										}
									});
								}
								if (statusMapList != null && !statusMapList.isEmpty()) {
									for (Map<String, String> statusMap : statusMapList) {
										String errorMessage = statusMap
												.get(XMAdminUtil.getInstance().getCurrentLocaleEnum().getLangCode());
										XMAdminUtil.getInstance().updateLogFile(errorMessage, MessageType.FAILURE);
									}
								}
							}
						} catch (UnauthorizedAccessException e) {
							Display.getDefault().asyncExec(new Runnable() {
								@Override
								public void run() {
									CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
											messages.unauthorizedDialogTitle, messages.unauthorizedDialogMsg);
								}
							});
						} catch (Exception e) {
							if (e instanceof ResourceAccessException) {
								Display.getDefault().asyncExec(new Runnable() {
									@Override
									public void run() {
										CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
												messages.errorDialogTitile, messages.serverNotReachable);
									}
								});
							}
							LOGGER.error(e.getMessage());
						}

						monitor.worked(70);
						return Status.OK_STATUS;
					}
				};
				job.setUser(true);
				job.schedule();
			}
		}
	}

	/**
	 * Project app to project app group DND rel.
	 *
	 * @param projectAppEventData
	 *            the project app event data
	 * @param isCopyPast
	 * @param targetNode
	 *            the target node
	 */
	private void projectAppToProjectAppGroupDNDRel(ProjectApplication[] projectAppEventData,
			IAdminTreeChild iAdminTreeChild, boolean isCopyPast) {
		if (iAdminTreeChild != null && projectAppEventData != null && projectAppEventData.length > 0) {
			String grouptId = null;
			IAdminTreeChild targetIAdminTreeChild = null;
			String groupName = null;
			if (iAdminTreeChild instanceof ProjectApplicationGroup) {
				targetIAdminTreeChild = ((ProjectApplicationGroup) iAdminTreeChild).getProjectAppGroupChildren()
						.get(ProjectAppGroupProjectApps.class.getSimpleName());
				grouptId = ((ProjectApplicationGroup) iAdminTreeChild).getGroupId();
				groupName = ((ProjectApplicationGroup) iAdminTreeChild).getName();
			} else if (iAdminTreeChild instanceof ProjectAppGroupProjectApps) {
				targetIAdminTreeChild = iAdminTreeChild;
				grouptId = ((ProjectApplicationGroup) iAdminTreeChild.getParent()).getGroupId();
				groupName = ((ProjectApplicationGroup) iAdminTreeChild.getParent()).getName();
			}

			if (targetIAdminTreeChild != null && grouptId != null) {
				final String groupId2 = grouptId;
				final IAdminTreeChild targetIAdminTreeChild2 = targetIAdminTreeChild;
				final String groupName2 = groupName;
				Job job = new Job("Creating Relations...") {

					@Override
					protected IStatus run(IProgressMonitor monitor) {
						monitor.beginTask("Creating Relations..", 100);
						monitor.worked(30);
						try {
							List<GroupRelRequest> projectRelRequestList = new ArrayList<>();
							for (ProjectApplication projectApplication : projectAppEventData) {
								GroupRelRequest groupRelRequest = new GroupRelRequest();
								groupRelRequest.setGroupId(groupId2);
								groupRelRequest.setObjectId(projectApplication.getProjectApplicationId());
								projectRelRequestList.add(groupRelRequest);
							}
							GroupRelController adminAreaProjectRelController = new GroupRelController();
							GroupRelBatchResponse groupRelResponse = adminAreaProjectRelController
									.createGroupRelations(new GroupRelBatchRequest(projectRelRequestList));

							if (groupRelResponse != null) {
								boolean isRelationCreated = false;
								Iterable<GroupRelResponse> groupRefTbls = groupRelResponse.getGroupRelResponses();
								List<String> successObjList = new ArrayList<>();
								for (GroupRelResponse groupRefTbl : groupRefTbls) {
									String relId = groupRefTbl.getGroupRefId();
									if (relId != null) {
										for (ProjectApplication projectApplication : projectAppEventData) {
											if (projectApplication.getProjectApplicationId().equals(groupRefTbl
													.getProjectApplicationsTbl().getProjectApplicationId())) {
												successObjList.add(projectApplication.getName());
												RelationObj relObj = (RelationObj) projectApplication.getAdapter(
														ProjectAppGroupProjectApps.class, targetIAdminTreeChild2, relId,
														true);
												((ProjectAppGroupProjectApps) targetIAdminTreeChild2).add(relId,
														relObj);
												isRelationCreated = true;
											}
										}
									}
								}
								if (isRelationCreated) {
									Display.getDefault().asyncExec(new Runnable() {
										public void run() {
											viewer.refresh(targetIAdminTreeChild2);
											if (!viewer.getExpandedState(targetIAdminTreeChild2.getParent())) {
												(new AdminTreeExpansion(targetIAdminTreeChild2.getParent()))
														.loadObjects();
												viewer.setExpandedState(targetIAdminTreeChild2.getParent(), true);
											}
											viewer.setExpandedState(targetIAdminTreeChild2, true);
											if (isCopyPast && iAdminTreeChild instanceof ProjectAppGroupProjectApps) {
												final AdminTreeviewer adminTree = XMAdminUtil.getInstance()
														.getAdminTree();
												adminTree.setSelection(new StructuredSelection(iAdminTreeChild));
											}
										}
									});
								}
								List<Map<String, String>> statusMapList = groupRelResponse.getStatusMaps();
								int noOfObjRelCreated = projectAppEventData.length;
								final int noOfObjRelCreated2;
								if (statusMapList != null && !statusMapList.isEmpty()) {
									noOfObjRelCreated = projectAppEventData.length - statusMapList.size();
									Display.getDefault().asyncExec(new Runnable() {
										public void run() {
											String errorMessage = "";
											for (Map<String, String> statusMap : statusMapList) {
												errorMessage += statusMap.get(
														XMAdminUtil.getInstance().getCurrentLocaleEnum().getLangCode());
												errorMessage += "\n";
											}
											if (errorMessage.length() > 0) {
												CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
														messages.dndErrorTitle, errorMessage);
											}
										}
									});
								}
								noOfObjRelCreated2 = noOfObjRelCreated;
								if (noOfObjRelCreated2 > 0) {
									for (String objName : successObjList) {
										XMAdminUtil.getInstance()
												.updateLogFile(messages.relationLbl + " " + messages.betweenLbl + " "
														+ messages.projectApplicationObject +" '" + objName + "' "
														+ messages.andLbl + " " + messages.projectAppGroupObject
														+ " '" + groupName2 + "' " + messages.objectCreate,
														MessageType.SUCCESS);
									}
									Display.getDefault().asyncExec(new Runnable() {
										@Override
										public void run() {
											eventBroker.send(CommonConstants.EVENT_BROKER.STATUSBAR,
													noOfObjRelCreated2 + " " + messages.dndStatusBarMsg);
										}
									});
								}
								if (statusMapList != null && !statusMapList.isEmpty()) {
									for (Map<String, String> statusMap : statusMapList) {
										String errorMessage = statusMap
												.get(XMAdminUtil.getInstance().getCurrentLocaleEnum().getLangCode());
										XMAdminUtil.getInstance().updateLogFile(errorMessage, MessageType.FAILURE);
									}
								}
							}
						} catch (UnauthorizedAccessException e) {
							Display.getDefault().asyncExec(new Runnable() {
								@Override
								public void run() {
									CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
											messages.unauthorizedDialogTitle, messages.unauthorizedDialogMsg);
								}
							});
						} catch (Exception e) {
							if (e instanceof ResourceAccessException) {
								Display.getDefault().asyncExec(new Runnable() {
									@Override
									public void run() {
										CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
												messages.errorDialogTitile, messages.serverNotReachable);
									}
								});
							}
							LOGGER.error(e.getMessage());
						}

						monitor.worked(70);
						return Status.OK_STATUS;
					}
				};
				job.setUser(true);
				job.schedule();
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.jface.viewers.ViewerDropAdapter#performDrop(java.lang.Object)
	 */
	@Override
	public boolean performDrop(Object data) {
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jface.viewers.ViewerDropAdapter#validateDrop(java.lang.
	 * Object, int, org.eclipse.swt.dnd.TransferData)
	 */
	@Override
	public boolean validateDrop(Object target, int operation, TransferData transferType) {
		List<Object> sourceElementList = new ArrayList<>();
		DropTargetEvent event = getCurrentEvent();
		if (event != null) {
			Object sourceObj = event.getSource();
			if (sourceObj != null) {
				Transfer[] transferArray = (((DropTarget) sourceObj).getTransfer());
				if (transferArray != null && transferArray.length > 0) {
					Transfer transfer = transferArray[0];
					if (transfer != null) {
						ISelection selection = ((LocalSelectionTransfer) transfer).getSelection();
						if (selection instanceof IStructuredSelection) {
							StructuredSelection selections = (StructuredSelection) selection;
							Object[] selectionArray = selections.toArray();
							for (Object selectionObj : selectionArray) {
								Object firstElement = ((StructuredSelection) selectionObj).getFirstElement();
								if (firstElement instanceof AAWarpperObj) {
									if (((AAWarpperObj) firstElement).getRelationObj() == null) {
										AdministrationArea adminArea = ((AAWarpperObj) firstElement).getAdminArea();
										sourceElementList.add(adminArea);
									}
								} else if (firstElement instanceof ProjectWrapperObj) {
									if (((ProjectWrapperObj) firstElement).getRelationObj() == null) {
										Project project = ((ProjectWrapperObj) firstElement).getProject();
										sourceElementList.add(project);
									}
								} else {
									sourceElementList.add(firstElement);
								}
							}

						}
					}
				}
			}
		}

		if (!sourceElementList.isEmpty() && target != null) {
			Object sourceObj = sourceElementList.get(0);
			if (sourceObj instanceof RelationObj) {
				boolean flag = validateRelationObj(target, sourceObj);
				if (flag) {
					RelationObj relObj = (RelationObj) sourceObj;
					sourceObj = relObj.getRefObject();
				}
			}
			if (sourceObj instanceof AdministrationArea) {
				if (target instanceof Site || target instanceof SiteAdministrations
						|| target instanceof RoleScopeObjects || target instanceof ProjectAdminAreas
						|| target instanceof Project) {
					this.targetObject = (IAdminTreeChild) target;
					this.operation = operation;
					return true;
				} else if (target instanceof Role) {
					Role role = (Role) target;
					if (!(CommonConstants.SuperAdminRole.NAME.equals(role.getRoleName()))) {
						this.targetObject = role;
						this.operation = operation;
						return true;
					}
				}
			} else if (sourceObj instanceof Project || sourceObj instanceof ProjectGroupModel) {
				if (target instanceof SiteAdminAreaProjects || target instanceof AdministrationArea
						|| target instanceof AdminAreaProjects || target instanceof Directory
						|| target instanceof DirectoryProjects || target instanceof User
						|| target instanceof UserProjects || target instanceof ProjectGroupModel
						|| target instanceof ProjectGroupProjects) {
					this.targetObject = (IAdminTreeChild) target;
					this.operation = operation;
					return true;
				} else if (target instanceof RelationObj) {
					IAdminTreeChild containerObj = ((RelationObj) target).getContainerObj();
					if (containerObj instanceof SiteAdministrationChild) {
						this.targetObject = (IAdminTreeChild) containerObj;
						this.operation = operation;
						return true;
					}
				}
			} else if (sourceObj instanceof StartApplication) {
				if (target instanceof SiteAdminAreaStartApplications
						|| target instanceof SiteAdminAreaProjectStartApplications
						|| target instanceof AdminAreaStartApplications || target instanceof AdministrationArea
						|| target instanceof AdminAreaProjectStartApplications || target instanceof User
						|| target instanceof UserStartApplications) {
					this.targetObject = (IAdminTreeChild) target;
					this.operation = operation;
					return true;
				} else if (target instanceof RelationObj) {
					IAdminTreeChild containerObj = ((RelationObj) target).getContainerObj();
					if (containerObj instanceof SiteAdministrationChild
							|| containerObj instanceof SiteAdminAreaProjectChild
							|| containerObj instanceof AdminAreaProjectChild) {
						this.targetObject = (IAdminTreeChild) containerObj;
						this.operation = operation;
						return true;
					}
				}
			} else if ((sourceObj instanceof UserApplication
					&& UserApplication.class.getSimpleName().equals(sourceObj.getClass().getSimpleName()))
					|| sourceObj instanceof UserApplicationGroup) {
				if (target instanceof SiteAdminAreaUserApplications || target instanceof SiteAdminAreaUserAppNotFixed
						|| target instanceof SiteAdminAreaUserAppFixed
						|| target instanceof SiteAdminAreaUserAppProtected
						|| target instanceof AdminAreaUserApplications || target instanceof AdminAreaUserAppFixed
						|| target instanceof AdminAreaUserAppNotFixed || target instanceof AdminAreaUserAppProtected
						|| target instanceof AdministrationArea || target instanceof UserAAUserApplications
						|| target instanceof UserAAUserAppAllowed || target instanceof UserAAUserAppForbidden
						|| target instanceof Directory || target instanceof DirectoryApplications
						|| target instanceof DirectoryUserApplications || target instanceof AdminMenu
						|| target instanceof AdminUserApps || target instanceof UserApplicationGroup
						|| target instanceof UserAppGroupUserApps) {
					this.targetObject = (IAdminTreeChild) target;
					this.operation = operation;
					return true;
				} else if (target instanceof RelationObj) {
					IAdminTreeChild containerObj = ((RelationObj) target).getContainerObj();
					if (containerObj instanceof SiteAdministrationChild || containerObj instanceof UserAdminAreaChild) {
						this.targetObject = (IAdminTreeChild) containerObj;
						this.operation = operation;
						return true;
					}
				}
			} else if ((sourceObj instanceof ProjectApplication
					&& ProjectApplication.class.getSimpleName().equals(sourceObj.getClass().getSimpleName()))
					|| sourceObj instanceof ProjectApplicationGroup) {
				if (target instanceof SiteAdminAreaProjectApplications || target instanceof SiteAdminProjectAppNotFixed
						|| target instanceof SiteAdminProjectAppFixed || target instanceof SiteAdminProjectAppProtected
						|| target instanceof AdminAreaProjectApplications
						|| target instanceof AdminAreaProjectAppNotFixed || target instanceof AdminAreaProjectAppFixed
						|| target instanceof AdminAreaProjectAppProtected || target instanceof Directory
						|| target instanceof DirectoryApplications || target instanceof DirectoryProjectApplications
						|| target instanceof UserProjectAAProjectApplications
						|| target instanceof UserProjectAAProjectAppAllowed
						|| target instanceof UserProjectAAProjectAppForbidden
						|| target instanceof ProjectUserAAProjectApplications
						|| target instanceof ProjectUserAAProjectAppAllowed
						|| target instanceof ProjectUserAAProjectAppForbidden || target instanceof AdminMenu
						|| target instanceof AdminProjectApps || target instanceof ProjectApplicationGroup
						|| target instanceof ProjectAppGroupProjectApps
						|| target instanceof ProjectAdminAreaProjectApplications
						|| target instanceof ProjectAdminAreaProjectAppFixed
						|| target instanceof ProjectAdminAreaProjectAppNotFixed
						|| target instanceof ProjectAdminAreaProjectAppProtected) {
					this.targetObject = (IAdminTreeChild) target;
					this.operation = operation;
					return true;
				} else if (target instanceof RelationObj) {
					IAdminTreeChild containerObj = ((RelationObj) target).getContainerObj();
					if (containerObj instanceof SiteAdminAreaProjectChild
							|| containerObj instanceof AdminAreaProjectChild
							|| containerObj instanceof UserProjectAdminAreaChild
							|| containerObj instanceof ProjectUserAdminAreaChild
							|| containerObj instanceof ProjectAdminAreaChild) {
						this.targetObject = (IAdminTreeChild) containerObj;
						this.operation = operation;
						return true;
					}
				}
			} else if (sourceObj instanceof User || sourceObj instanceof UserGroupModel) {
				if (target instanceof Project || target instanceof ProjectUsers || target instanceof Directory
						|| target instanceof DirectoryUsers || target instanceof Role || target instanceof RoleUsers
						|| target instanceof RoleScopeObjectUsers || target instanceof AdminMenu
						|| target instanceof AdminUsers || target instanceof UserGroupModel
						|| target instanceof UserGroupUsers) {
					this.targetObject = (IAdminTreeChild) target;
					this.operation = operation;
					return true;
				} else if (target instanceof RelationObj) {
					IAdminTreeChild containerObj = ((RelationObj) target).getContainerObj();
					if (containerObj instanceof RoleScopeObjectChild) {
						this.targetObject = (IAdminTreeChild) containerObj;
						this.operation = operation;
						return true;
					}
				}
			} else if (sourceObj instanceof RelationObj) {
				IAdminTreeChild containerObj = ((RelationObj) sourceObj).getContainerObj();
				IAdminTreeChild refObject = ((RelationObj) sourceObj).getRefObject();

				IAdminTreeChild containerParent = containerObj.getParent().getParent();
				IAdminTreeChild targetParent = ((IAdminTreeChild) target).getParent();

				if ((target instanceof SiteAdminAreaUserAppNotFixed || target instanceof SiteAdminAreaUserAppFixed
						|| target instanceof SiteAdminAreaUserAppProtected || target instanceof AdminAreaUserAppNotFixed
						|| target instanceof AdminAreaUserAppFixed || target instanceof AdminAreaUserAppProtected)
						&& !(containerObj instanceof SiteAdminAreaUserApplications
								|| containerObj instanceof AdminAreaUserApplications)
						&& refObject instanceof UserApplication && !containerObj.getClass().equals(target.getClass())) {
					if (containerParent.equals(targetParent)) {
						this.targetObject = (IAdminTreeChild) target;
						this.operation = operation;
						return true;
					}
				} else if ((target instanceof SiteAdminProjectAppNotFixed || target instanceof SiteAdminProjectAppFixed
						|| target instanceof SiteAdminProjectAppProtected
						|| target instanceof AdminAreaProjectAppNotFixed || target instanceof AdminAreaProjectAppFixed
						|| target instanceof AdminAreaProjectAppProtected
						|| target instanceof ProjectAdminAreaProjectAppFixed
						|| target instanceof ProjectAdminAreaProjectAppNotFixed
						|| target instanceof ProjectAdminAreaProjectAppProtected)
						&& !(containerObj instanceof SiteAdminAreaProjectApplications
								|| containerObj instanceof AdminAreaProjectApplications
								|| containerObj instanceof ProjectAdminAreaProjectApplications)
						&& refObject instanceof ProjectApplication
						&& !containerObj.getClass().equals(target.getClass())) {
					if (containerParent.equals(targetParent)) {
						this.targetObject = (IAdminTreeChild) target;
						this.operation = operation;
						return true;
					}
				} else if ((target instanceof UserAAUserAppAllowed || target instanceof UserAAUserAppForbidden)
						&& !(containerObj instanceof UserAAUserApplications) && refObject instanceof UserApplication
						&& !containerObj.getClass().equals(target.getClass())) {
					if (containerParent.equals(targetParent)) {
						this.targetObject = (IAdminTreeChild) target;
						this.operation = operation;
						return true;
					}
				} else if ((target instanceof UserProjectAAProjectAppAllowed
						|| target instanceof UserProjectAAProjectAppForbidden
						|| target instanceof ProjectUserAAProjectAppAllowed
						|| target instanceof ProjectUserAAProjectAppForbidden)
						&& !(containerObj instanceof UserProjectAAProjectApplications
								|| containerObj instanceof ProjectUserAAProjectApplications)
						&& refObject instanceof ProjectApplication
						&& !containerObj.getClass().equals(target.getClass())) {
					if (containerParent.equals(targetParent)) {
						this.targetObject = (IAdminTreeChild) target;
						this.operation = operation;
						return true;
					}
				}
			} else if (sourceObj instanceof NotificationTemplate && sourceElementList.size() == 1) {
				if (target instanceof NotificationEvtActions) {
					this.targetObject = (IAdminTreeChild) target;
					this.operation = operation;
					return true;
				}
			}
		}
		return false;
	}

	/**
	 * Validate relation obj.
	 *
	 * @param target
	 *            the target
	 * @param sourceObj
	 *            the source obj
	 * @return true, if successful
	 */
	private boolean validateRelationObj(Object target, Object sourceObj) {
		RelationObj relObj = (RelationObj) sourceObj;
		IAdminTreeChild containerObj = relObj.getContainerObj();
		boolean flag = true;
		IAdminTreeChild containerParent = containerObj.getParent().getParent();
		IAdminTreeChild targetParent = ((IAdminTreeChild) target).getParent();

		if (((containerObj instanceof SiteAdminAreaUserAppNotFixed || containerObj instanceof SiteAdminAreaUserAppFixed
				|| containerObj instanceof SiteAdminAreaUserAppProtected
				|| containerObj instanceof SiteAdminAreaUserApplications)
				&& (target instanceof SiteAdminAreaUserAppNotFixed || target instanceof SiteAdminAreaUserAppFixed
						|| target instanceof SiteAdminAreaUserAppProtected
						|| target instanceof SiteAdminAreaUserApplications))) {

			if ((containerObj instanceof SiteAdminAreaUserApplications)
					&& (target instanceof SiteAdminAreaUserAppNotFixed || target instanceof SiteAdminAreaUserAppFixed
							|| target instanceof SiteAdminAreaUserAppProtected)
					&& (containerObj.getParent().equals(targetParent))) {
				flag = false;
			} else if (target instanceof SiteAdminAreaUserApplications) {
				if (target.equals(containerParent) || (containerParent.equals(targetParent))) {
					flag = false;
				} else {
					flag = true;
				}
			} else if (containerParent.equals(targetParent)) {
				flag = false;
			} else {
				flag = true;
			}
		} else if (((containerObj instanceof SiteAdminProjectAppNotFixed
				|| containerObj instanceof SiteAdminProjectAppFixed
				|| containerObj instanceof SiteAdminProjectAppProtected
				|| containerObj instanceof SiteAdminAreaProjectApplications)
				&& (target instanceof SiteAdminProjectAppNotFixed || target instanceof SiteAdminProjectAppFixed
						|| target instanceof SiteAdminProjectAppProtected
						|| target instanceof SiteAdminAreaProjectApplications))) {
			if ((containerObj instanceof SiteAdminAreaProjectApplications)
					&& (target instanceof SiteAdminProjectAppNotFixed || target instanceof SiteAdminProjectAppFixed
							|| target instanceof SiteAdminProjectAppProtected)
					&& (containerObj.getParent().equals(targetParent))) {
				flag = false;
			} else if (target instanceof SiteAdminAreaProjectApplications) {
				if (target.equals(containerParent) || (containerParent.equals(targetParent))) {
					flag = false;
				} else {
					flag = true;
				}
			} else if (containerParent.equals(targetParent)) {
				flag = false;
			} else {
				flag = true;
			}
		} else if (((containerObj instanceof AdminAreaUserAppNotFixed || containerObj instanceof AdminAreaUserAppFixed
				|| containerObj instanceof AdminAreaUserAppProtected
				|| containerObj instanceof AdminAreaUserApplications)
				&& (target instanceof AdminAreaUserAppNotFixed || target instanceof AdminAreaUserAppFixed
						|| target instanceof AdminAreaUserAppProtected
						|| target instanceof AdminAreaUserApplications))) {
			if ((containerObj instanceof AdminAreaUserApplications)
					&& (target instanceof AdminAreaUserAppNotFixed || target instanceof AdminAreaUserAppFixed
							|| target instanceof AdminAreaUserAppProtected)
					&& (containerObj.getParent().equals(targetParent))) {
				flag = false;
			} else if (target instanceof AdminAreaUserApplications) {
				if (target.equals(containerParent) || (containerParent.equals(targetParent))) {
					flag = false;
				} else {
					flag = true;
				}
			} else if (containerParent.equals(targetParent)) {
				flag = false;
			} else {
				flag = true;
			}
		} else if (((containerObj instanceof AdminAreaProjectAppNotFixed
				|| containerObj instanceof AdminAreaProjectAppFixed
				|| containerObj instanceof AdminAreaProjectAppProtected
				|| containerObj instanceof AdminAreaProjectApplications)
				&& (target instanceof AdminAreaProjectAppNotFixed || target instanceof AdminAreaProjectAppFixed
						|| target instanceof AdminAreaProjectAppProtected
						|| target instanceof AdminAreaProjectApplications))) {
			if ((containerObj instanceof AdminAreaProjectApplications)
					&& (target instanceof AdminAreaProjectAppNotFixed || target instanceof AdminAreaProjectAppFixed
							|| target instanceof AdminAreaProjectAppProtected)
					&& (containerObj.getParent().equals(targetParent))) {
				flag = false;
			} else if (target instanceof AdminAreaProjectApplications) {
				if (target.equals(containerParent) || (containerParent.equals(targetParent))) {
					flag = false;
				} else {
					flag = true;
				}
			} else if (containerParent.equals(targetParent)) {
				flag = false;
			} else {
				flag = true;
			}
		} else if (((containerObj instanceof UserAAUserAppAllowed || containerObj instanceof UserAAUserAppForbidden
				|| containerObj instanceof UserAAUserApplications)
				&& (target instanceof UserAAUserAppAllowed || target instanceof UserAAUserAppForbidden
						|| target instanceof UserAAUserApplications))) {
			if ((containerObj instanceof UserAAUserApplications)
					&& (target instanceof UserAAUserAppForbidden || target instanceof UserAAUserAppAllowed)
					&& (containerObj.getParent().equals(targetParent))) {
				flag = false;
			} else if (target instanceof UserAAUserApplications) {
				if (target.equals(containerParent) || (containerParent.equals(targetParent))) {
					flag = false;
				} else {
					flag = true;
				}
			} else if (containerParent.equals(targetParent)) {
				flag = false;
			} else {
				flag = true;
			}
		} else if (((containerObj instanceof UserProjectAAProjectAppAllowed
				|| containerObj instanceof UserProjectAAProjectAppForbidden
				|| containerObj instanceof UserProjectAAProjectApplications)
				&& (target instanceof UserProjectAAProjectAppAllowed
						|| target instanceof UserProjectAAProjectAppForbidden
						|| target instanceof UserProjectAAProjectApplications))) {
			if ((containerObj instanceof UserProjectAAProjectApplications)
					&& (target instanceof UserProjectAAProjectAppAllowed
							|| target instanceof UserProjectAAProjectAppForbidden)
					&& (containerObj.getParent().equals(targetParent))) {
				flag = false;
			} else if (target instanceof UserProjectAAProjectApplications) {
				if (target.equals(containerParent) || (containerParent.equals(targetParent))) {
					flag = false;
				} else {
					flag = true;
				}
			} else if (containerParent.equals(targetParent)) {
				flag = false;
			} else {
				flag = true;
			}
		} else if (((containerObj instanceof ProjectUserAAProjectAppAllowed
				|| containerObj instanceof ProjectUserAAProjectAppForbidden
				|| containerObj instanceof ProjectUserAAProjectApplications)
				&& (target instanceof ProjectUserAAProjectAppAllowed
						|| target instanceof ProjectUserAAProjectAppForbidden
						|| target instanceof ProjectUserAAProjectApplications))) {
			if ((containerObj instanceof ProjectUserAAProjectApplications)
					&& (target instanceof AdminAreaProjectAppNotFixed || target instanceof AdminAreaProjectAppFixed
							|| target instanceof AdminAreaProjectAppProtected)
					&& (containerObj.getParent().equals(targetParent))) {
				flag = false;
			} else if (target instanceof ProjectUserAAProjectApplications) {
				if (target.equals(containerParent) || (containerParent.equals(targetParent))) {
					flag = false;
				} else {
					flag = true;
				}
			} else if (containerParent.equals(targetParent)) {
				flag = false;
			} else {
				flag = true;
			}
		} else if (((containerObj instanceof ProjectAdminAreaProjectAppNotFixed
				|| containerObj instanceof ProjectAdminAreaProjectAppFixed
				|| containerObj instanceof ProjectAdminAreaProjectAppProtected
				|| containerObj instanceof ProjectAdminAreaProjectApplications)
				&& (target instanceof ProjectAdminAreaProjectAppNotFixed
						|| target instanceof ProjectAdminAreaProjectAppFixed
						|| target instanceof ProjectAdminAreaProjectAppProtected
						|| target instanceof ProjectAdminAreaProjectApplications))) {
			if ((containerObj instanceof ProjectAdminAreaProjectApplications)
					&& (target instanceof ProjectAdminAreaProjectAppNotFixed
							|| target instanceof ProjectAdminAreaProjectAppFixed
							|| target instanceof ProjectAdminAreaProjectAppProtected)
					&& (containerObj.getParent().equals(targetParent))) {
				flag = false;
			} else if (target instanceof ProjectAdminAreaProjectApplications) {
				if (target.equals(containerParent) || (containerParent.equals(targetParent))) {
					flag = false;
				} else {
					flag = true;
				}
			} else if (containerParent.equals(targetParent)) {
				flag = false;
			} else {
				flag = true;
			}
		}
		return flag;
	}
}