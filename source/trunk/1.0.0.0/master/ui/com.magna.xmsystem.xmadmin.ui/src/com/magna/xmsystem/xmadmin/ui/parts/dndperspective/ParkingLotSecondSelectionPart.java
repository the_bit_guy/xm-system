package com.magna.xmsystem.xmadmin.ui.parts.dndperspective;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.eclipse.e4.core.di.annotations.Optional;
import org.eclipse.e4.ui.di.UIEventTopic;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.swt.widgets.Composite;

public class ParkingLotSecondSelectionPart extends ParkingLotSelectionBasePart {
	
	/**
	 * Method to create UI
	 * 
	 * @param parent
	 *            {@link Composite}
	 */
	@PostConstruct
	public void createUI(final Composite parentP) {
		super.createUI(parentP);
	}
	
	/**
	 * Listen for any event message
	 * 
	 * @param message
	 *            {@link String}
	 */
	@Inject
	@Optional
	public void getEvent(@UIEventTopic("UpdateSubAdminTreeSecond") final IStructuredSelection selection) {
		updateView(selection);
	}
}
