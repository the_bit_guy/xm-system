package com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.roles;

// TODO: Auto-generated Javadoc
/**
 * The Enum GlobalPermissionObjectType.
 * 
 * @author shashwat.anand
 */
public enum GlobalPermissionObjectType {
	
	/** The login cax start admin. */
	LOGIN_CAX_START_ADMIN,
	
	/** The login cax start hotline. */
	LOGIN_CAX_START_HOTLINE,
	
	/** The view inactive. */
	VIEW_INACTIVE;

	/**
	 * Gets the object types.
	 *
	 * @return the object types
	 */
	public static String[] getObjectTypes() {
		GlobalPermissionObjectType[] values = values();
		String[] returnArray = new String[values.length];
		for (int index = 0; index < values.length; index++) {
			returnArray[index] = values[index].name();
		}
		return returnArray;
	}
	

	/**
	 * Gets the object type.
	 *
	 * @param objectType the object type
	 * @return the object type
	 */
	public static GlobalPermissionObjectType getObjectType(String objectType) {
        for (GlobalPermissionObjectType value : values()) {
            if (objectType.equalsIgnoreCase(value.name())) {
                return value;
            }
        }
        return null;
    }
}
