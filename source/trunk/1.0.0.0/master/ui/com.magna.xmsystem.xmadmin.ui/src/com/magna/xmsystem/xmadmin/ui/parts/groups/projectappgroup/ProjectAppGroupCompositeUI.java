package com.magna.xmsystem.xmadmin.ui.parts.groups.projectappgroup;

import org.eclipse.jface.layout.GridDataFactory;
import org.eclipse.jface.layout.GridLayoutFactory;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.ScrolledComposite;
import org.eclipse.swt.events.ControlAdapter;
import org.eclipse.swt.events.ControlEvent;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Link;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.widgets.ToolBar;
import org.eclipse.swt.widgets.ToolItem;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.magna.xmsystem.dependencies.utils.XMSystemUtil;
import com.magna.xmsystem.xmadmin.ui.dialogs.lang.XMAdminLangTextAreaDialog;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.group.projectappgroup.ProjectApplicationGroup;
import com.magna.xmsystem.xmadmin.util.XMAdminUtil;

public class ProjectAppGroupCompositeUI extends Composite {

	/** Logger Instance. */
	private static final Logger LOGGER = LoggerFactory.getLogger(ProjectAppGroupCompositeUI.class);

	/** Member variable for Save/Cancel Button Name. */
	transient protected Button saveBtn, cancelBtn;

	/** Member variable for name label. */
	transient protected Label lblName;

	/** Member variable for symbol label. */
	transient protected Label lblSymbol;

	/** Member variable for description label. */
	transient protected Label lblDescrition;

	/** Member variable for text name. */
	transient protected Text txtName;

	/** Member variable for Text Symbol. */
	transient protected Text txtSymbol;

	/** Member variable for group. */
	transient protected Group grpProjectAppGroup;

	/** Member variable for parentShell. */
	transient protected Shell parentShell;

	/** Member variable for browse tool item. */
	transient protected ToolItem toolItem;

	/** Member variable for txtDesc. */
	protected Text txtDesc;

	/** Member variable for descLink. */
	protected Link descLink;

	/** Member variable for remarksLabel. */
	protected Label remarksLabel;

	/** Member variable for remarksTranslate. */
	protected Link remarksTranslate;

	/** Member variable for lblremarksCount. */
	protected Label lblremarksCount;

	/** Member variable for txtRemarks. */
	protected Text txtRemarks;

	/**
	 * Parameterized Constructor.
	 *
	 * @param parent
	 *            Composite
	 * @param style
	 *            int
	 */
	public ProjectAppGroupCompositeUI(final Composite parent, final int style) {
		super(parent, style);
		this.initGUI();
		setShowButtonBar(false);
	}

	/**
	 * Make the GUI for components of the Group Composite.
	 */
	private void initGUI() {
		try {
			GridLayoutFactory.fillDefaults().applyTo(this);
			this.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));

			this.grpProjectAppGroup = new Group(this, SWT.NONE);
			this.grpProjectAppGroup.setBackgroundMode(SWT.INHERIT_FORCE);
			GridLayoutFactory.fillDefaults().numColumns(1).applyTo(this.grpProjectAppGroup);
			GridDataFactory.fillDefaults().grab(true, true).align(SWT.FILL, SWT.FILL).applyTo(this.grpProjectAppGroup);
			final ScrolledComposite scrolledComposite = XMAdminUtil.getInstance().createScrolledComposite(this.grpProjectAppGroup);
			scrolledComposite.setBackgroundMode(SWT.INHERIT_FORCE);

			final Composite widgetContainer = new Composite(scrolledComposite, SWT.NONE);
			final GridLayout widgetContLayout = new GridLayout(3, false);
			widgetContainer.setLayout(widgetContLayout);
			widgetContainer.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));

			this.lblName = new Label(widgetContainer, SWT.NONE);
			this.txtName = new Text(widgetContainer, SWT.BORDER);
			this.txtName.setTextLimit(ProjectApplicationGroup.NAME_LIMIT);
			GridDataFactory.fillDefaults().grab(true, false).span(2, 1)
					/* .indent(2, 0) */.align(SWT.FILL, SWT.CENTER).applyTo(this.txtName);

			this.lblDescrition = new Label(widgetContainer, SWT.NONE);
			this.txtDesc = new Text(widgetContainer, SWT.BORDER);
			this.txtDesc.setTextLimit(ProjectApplicationGroup.DESC_LIMIT);
			this.txtDesc.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));
			this.descLink = new Link(widgetContainer, SWT.NONE);

			this.lblSymbol = new Label(widgetContainer, SWT.NONE);
			this.txtSymbol = new Text(widgetContainer, SWT.BORDER);
			this.txtSymbol.setEditable(false);
			GridDataFactory.fillDefaults().grab(true, false).span(1, 1).align(SWT.FILL, SWT.CENTER)
					.applyTo(this.txtSymbol);

			final ToolBar toolbar = new ToolBar(widgetContainer, SWT.NONE);
			this.toolItem = new ToolItem(toolbar, SWT.FLAT);
			this.toolItem.setImage(XMSystemUtil.getInstance().getImage(this.getClass(), "icons/16x16/browse.png"));
			toolbar.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false));

			this.remarksLabel = new Label(widgetContainer, SWT.NONE);
			this.remarksTranslate = new Link(widgetContainer, SWT.NONE);
			this.remarksTranslate.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, true, false, 1, 1));

			this.lblremarksCount = new Label(widgetContainer, SWT.BORDER | SWT.CENTER);
			final GridData gridDataHelpTextCount = new GridData(SWT.LEFT, SWT.CENTER, false, false);
			gridDataHelpTextCount.widthHint = 70;
			this.lblremarksCount.setLayoutData(gridDataHelpTextCount);

			this.txtRemarks = new Text(widgetContainer,
					SWT.BORDER | SWT.WRAP | SWT.H_SCROLL | SWT.V_SCROLL);
			final GridData gridData = new GridData(SWT.FILL, SWT.FILL, true, true);
			gridData.minimumHeight = 100;
			gridData.horizontalSpan = 3;
			this.txtRemarks.setLayoutData(gridData);
			this.txtRemarks.setTextLimit(ProjectApplicationGroup.REMARK_LIMIT);
			this.lblremarksCount.setText(XMAdminLangTextAreaDialog.LIMIT_COUNT_PADDING
					+ this.txtRemarks.getText().length() + XMAdminLangTextAreaDialog.SLASH
					+ ProjectApplicationGroup.REMARK_LIMIT + XMAdminLangTextAreaDialog.LIMIT_COUNT_PADDING);

			final Composite buttonBarComp = new Composite(this.grpProjectAppGroup, SWT.NONE);
			final GridLayout btnBarCompLayout = new GridLayout(2, true);
			btnBarCompLayout.marginRight = 0;
			btnBarCompLayout.marginLeft = 0;
			btnBarCompLayout.marginTop = 0;
			btnBarCompLayout.marginBottom = 0;
			btnBarCompLayout.marginWidth = 0;
			buttonBarComp.setLayout(btnBarCompLayout);
			buttonBarComp.setLayoutData(new GridData(SWT.FILL, SWT.DEFAULT, true, false, 3, 1));
			createButtonBar(buttonBarComp);
			
			scrolledComposite.setContent(widgetContainer);
			scrolledComposite.setSize(widgetContainer.getSize());
			scrolledComposite.setExpandVertical(true);
			scrolledComposite.setExpandHorizontal(true);
			scrolledComposite.update();

			scrolledComposite.addControlListener(new ControlAdapter() {
				public void controlResized(final ControlEvent e) {
					Rectangle rectangle = scrolledComposite.getClientArea();
					scrolledComposite.setMinSize(widgetContainer.computeSize(rectangle.width, SWT.DEFAULT));
				}
			});
		} catch (Exception e) {
			LOGGER.error("Unable to create UI elements", e);
		}
	}

	/**
	 * Method for dispose.
	 */
	@Override
	public void dispose() {
		super.dispose();
	}

	/**
	 * Method to create button bar.
	 *
	 * @param buttonBarComp
	 *            {@link Composite}
	 */
	private void createButtonBar(final Composite buttonBarComp) {
		this.saveBtn = new Button(buttonBarComp, SWT.NONE);
		this.saveBtn.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, true, false));

		this.cancelBtn = new Button(buttonBarComp, SWT.NONE);
		this.cancelBtn.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, true, false));
	}

	/**
	 * Sets the show button bar.
	 *
	 * @param showButtonBar
	 *            the new show button bar
	 */
	public void setShowButtonBar(final boolean showButtonBar) {
		if (this.saveBtn != null && !this.saveBtn.isDisposed() && this.cancelBtn != null
				&& !this.cancelBtn.isDisposed()) {
			GridData layoutData = (GridData) this.saveBtn.getParent().getLayoutData();
			layoutData.exclude = !showButtonBar;
			this.saveBtn.setVisible(showButtonBar);
			this.cancelBtn.setVisible(showButtonBar);
			this.saveBtn.getParent().setVisible(showButtonBar);
			this.saveBtn.getParent().requestLayout();
			this.saveBtn.getParent().redraw();
			this.saveBtn.getParent().getParent().update();
		}

	}

}
