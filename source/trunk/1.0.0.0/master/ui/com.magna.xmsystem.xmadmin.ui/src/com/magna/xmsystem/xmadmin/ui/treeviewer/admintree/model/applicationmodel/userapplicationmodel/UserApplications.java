package com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.userapplicationmodel;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild;

/**
 * Model class for UserApplications.
 *
 * @author Deepak upadhyay
 */
public class UserApplications implements IAdminTreeChild {
	/** The parent. */
	private IAdminTreeChild parent;
	/**
	 * List for storing {@link UserApplication}
	 */
	private Map<String, IAdminTreeChild> userAppChildren;

	/**
	 * Constructor
	 */
	public UserApplications() {
		this.userAppChildren = new LinkedHashMap<>();
	}

	/**
	 * Adds the child
	 * 
	 * @param child
	 *            {@link IAdminTreeChild}
	 * @return {@link boolean}
	 */
	public IAdminTreeChild add(final String userApplicatioId,final IAdminTreeChild child) {
		child.setParent(this);
		IAdminTreeChild returnVal = this.userAppChildren.put(userApplicatioId, child);
		sort();
		return returnVal;
	}

	/**
	 * Removes the child
	 * 
	 * @param child
	 *            {@link IAdminTreeChild}
	 * @return {@link boolean}
	 */
	public IAdminTreeChild remove(final String child) {
		return this.userAppChildren.remove(child);
	}
	
	/**
	 * Method for Removes the all.
	 */
	public void removeAll() {
		this.userAppChildren.clear();
	}
	
	/**
	 * @return {@link List} of {@link UserApplication} 
	 */
	public Collection<IAdminTreeChild> getUserAppCollection() {
		return this.userAppChildren.values();
	}

	/**
	 * @return {@link List} of {@link UserApplication}
	 */
	public Map<String, IAdminTreeChild> getUserApplications() {
		return this.userAppChildren;
	}
	
	/**
	 * Returns null as its parent
	 * @see com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild#getParent()
	 */
	@Override
	public IAdminTreeChild getParent() {
		return this.parent;
	}

	/**
	 * @see com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild#setParent(com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild)
	 */
	@Override
	public void setParent(final IAdminTreeChild parent) {
		this.parent = parent;
	}
	
	/**
	 * Sort.
	 */
	public void sort() {
		final LinkedHashMap<String, IAdminTreeChild> collect = this.userAppChildren.entrySet().stream().sorted(
				(e1, e2) -> ((UserApplication) e1.getValue()).getName().compareTo(((UserApplication) e2.getValue()).getName()))
				.collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e1, LinkedHashMap::new));
		this.userAppChildren = collect;
	}
}
