package com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.notification;

import java.util.LinkedList;
import java.util.List;

import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild;

/**
 * The Class Notifications.
 * 
 * @author shashwat.anand
 */
public class Notifications implements IAdminTreeChild {
	
	/** The parent. */
	private IAdminTreeChild parent;
	
	/** The notification children. */
	private final List<IAdminTreeChild> notificationChildren;

	/** The project create evt. */
	private ProjectCreateEvt projectCreateEvt;

	/** The project delete evt. */
	private ProjectDeleteEvt projectDeleteEvt;

	/** The project activate evt. */
	private ProjectActivateEvt projectActivateEvt;

	/** The project deactivate evt. */
	private ProjectDeactivateEvt projectDeactivateEvt;

	/** The user project rel assign evt. */
	private UserProjectRelAssignEvt userProjectRelAssignEvt;

	/** The user project rel remove evt. */
	private UserProjectRelRemoveEvt userProjectRelRemoveEvt;

	/** The pro AA rel assign evt. */
	private ProAARelAssignEvt proAARelAssignEvt;

	/** The pro AA rel remove evt. */
	private ProAARelRemoveEvt proAARelRemoveEvt;

	/** The user pro exp evt. */
	private UserProExpEvt userProExpEvt;

	/** The user pro exp grace evt. */
	private UserProExpGraceEvt userProExpGraceEvt;

	/** The notify project user evt. */
	private NotifyProjectUserEvt notifyProjectUserEvt;

	/** The notify AA project user evt. */
	private NotifyAAProjectUserEvt notifyAAProjectUserEvt;
	
	/** The notification templates. */
	private NotificationTemplates notificationTemplates;
	
	/**
	 * Instantiates a new live messages.
	 */
	public Notifications() {
		this.parent = null;
		this.notificationChildren = new LinkedList<>();
		this.projectCreateEvt = new ProjectCreateEvt();
		this.projectDeleteEvt = new ProjectDeleteEvt();
		this.projectActivateEvt = new ProjectActivateEvt();
		this.projectDeactivateEvt = new ProjectDeactivateEvt();
		this.userProjectRelAssignEvt = new UserProjectRelAssignEvt();
		this.userProjectRelRemoveEvt = new UserProjectRelRemoveEvt();
		this.proAARelAssignEvt = new ProAARelAssignEvt();
		this.proAARelRemoveEvt = new ProAARelRemoveEvt();
		this.userProExpEvt = new UserProExpEvt();
		this.userProExpGraceEvt = new UserProExpGraceEvt();
		this.notifyProjectUserEvt = new NotifyProjectUserEvt();
		this.notifyAAProjectUserEvt = new NotifyAAProjectUserEvt();
		this.notificationTemplates = new NotificationTemplates();
		
	}
	
	/**
	 * Inits the notification events.
	 */
	public void addNotificationEvents() {
		this.add(this.projectCreateEvt);
		this.add(this.projectDeleteEvt);
		this.add(this.projectActivateEvt);
		this.add(this.projectDeactivateEvt);
		this.add(this.userProjectRelAssignEvt);
		this.add(this.userProjectRelRemoveEvt);
		this.add(this.proAARelAssignEvt);
		this.add(this.proAARelRemoveEvt);
		this.add(this.userProExpEvt);
		this.add(this.userProExpGraceEvt);
		this.add(this.notifyProjectUserEvt);
		this.add(this.notifyAAProjectUserEvt);
		this.add(this.notificationTemplates);
	}
	
	/**
	 * Adds the.
	 *
	 * @param child the child
	 * @return true, if successful
	 */
	private boolean add(final IAdminTreeChild child) {
		child.setParent(this);
		return this.notificationChildren.add(child);
	}

	/* (non-Javadoc)
	 * @see com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild#getParent()
	 */
	@Override
	public IAdminTreeChild getParent() {
		return this.parent;
	}

	/* (non-Javadoc)
	 * @see com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild#setParent(com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild)
	 */
	@Override
	public void setParent(final IAdminTreeChild parent) {
		this.parent = parent;
	}
	
	/**
	 * Gets the all notifications.
	 *
	 * @return the all notifications
	 */
	public List<IAdminTreeChild> getAllNotifications() {
		return this.notificationChildren;
	}

	/**
	 * Gets the project create evt.
	 *
	 * @return the projectCreateEvt
	 */
	public ProjectCreateEvt getProjectCreateEvt() {
		return projectCreateEvt;
	}

	/**
	 * Sets the project create evt.
	 *
	 * @param projectCreateEvt the projectCreateEvt to set
	 */
	public void setProjectCreateEvt(ProjectCreateEvt projectCreateEvt) {
		this.projectCreateEvt = projectCreateEvt;
	}

	/**
	 * Gets the project delete evt.
	 *
	 * @return the projectDeleteEvt
	 */
	public ProjectDeleteEvt getProjectDeleteEvt() {
		return projectDeleteEvt;
	}

	/**
	 * Sets the project delete evt.
	 *
	 * @param projectDeleteEvt the projectDeleteEvt to set
	 */
	public void setProjectDeleteEvt(ProjectDeleteEvt projectDeleteEvt) {
		this.projectDeleteEvt = projectDeleteEvt;
	}

	/**
	 * Gets the project activate evt.
	 *
	 * @return the projectActivateEvt
	 */
	public ProjectActivateEvt getProjectActivateEvt() {
		return projectActivateEvt;
	}

	/**
	 * Sets the project activate evt.
	 *
	 * @param projectActivateEvt the projectActivateEvt to set
	 */
	public void setProjectActivateEvt(ProjectActivateEvt projectActivateEvt) {
		this.projectActivateEvt = projectActivateEvt;
	}

	/**
	 * Gets the project deactivate evt.
	 *
	 * @return the projectDeactivateEvt
	 */
	public ProjectDeactivateEvt getProjectDeactivateEvt() {
		return projectDeactivateEvt;
	}

	/**
	 * Sets the project deactivate evt.
	 *
	 * @param projectDeactivateEvt the projectDeactivateEvt to set
	 */
	public void setProjectDeactivateEvt(ProjectDeactivateEvt projectDeactivateEvt) {
		this.projectDeactivateEvt = projectDeactivateEvt;
	}

	/**
	 * Gets the user project rel assign evt.
	 *
	 * @return the userProjectRelAssignEvt
	 */
	public UserProjectRelAssignEvt getUserProjectRelAssignEvt() {
		return userProjectRelAssignEvt;
	}

	/**
	 * Sets the user project rel assign evt.
	 *
	 * @param userProjectRelAssignEvt the userProjectRelAssignEvt to set
	 */
	public void setUserProjectRelAssignEvt(UserProjectRelAssignEvt userProjectRelAssignEvt) {
		this.userProjectRelAssignEvt = userProjectRelAssignEvt;
	}

	/**
	 * Gets the user project rel remove evt.
	 *
	 * @return the userProjectRelRemoveEvt
	 */
	public UserProjectRelRemoveEvt getUserProjectRelRemoveEvt() {
		return userProjectRelRemoveEvt;
	}

	/**
	 * Sets the user project rel remove evt.
	 *
	 * @param userProjectRelRemoveEvt the userProjectRelRemoveEvt to set
	 */
	public void setUserProjectRelRemoveEvt(UserProjectRelRemoveEvt userProjectRelRemoveEvt) {
		this.userProjectRelRemoveEvt = userProjectRelRemoveEvt;
	}

	/**
	 * Gets the pro AA rel assign evt.
	 *
	 * @return the proAARelAssignEvt
	 */
	public ProAARelAssignEvt getProAARelAssignEvt() {
		return proAARelAssignEvt;
	}

	/**
	 * Sets the pro AA rel assign evt.
	 *
	 * @param proAARelAssignEvt the proAARelAssignEvt to set
	 */
	public void setProAARelAssignEvt(ProAARelAssignEvt proAARelAssignEvt) {
		this.proAARelAssignEvt = proAARelAssignEvt;
	}

	/**
	 * Gets the pro AA rel remove evt.
	 *
	 * @return the proAARelRemoveEvt
	 */
	public ProAARelRemoveEvt getProAARelRemoveEvt() {
		return proAARelRemoveEvt;
	}

	/**
	 * Sets the pro AA rel remove evt.
	 *
	 * @param proAARelRemoveEvt the proAARelRemoveEvt to set
	 */
	public void setProAARelRemoveEvt(ProAARelRemoveEvt proAARelRemoveEvt) {
		this.proAARelRemoveEvt = proAARelRemoveEvt;
	}

	/**
	 * Gets the user pro exp evt.
	 *
	 * @return the userProExpEvt
	 */
	public UserProExpEvt getUserProExpEvt() {
		return userProExpEvt;
	}

	/**
	 * Sets the user pro exp evt.
	 *
	 * @param userProExpEvt the userProExpEvt to set
	 */
	public void setUserProExpEvt(UserProExpEvt userProExpEvt) {
		this.userProExpEvt = userProExpEvt;
	}

	/**
	 * Gets the user pro exp grace evt.
	 *
	 * @return the userProExpGraceEvt
	 */
	public UserProExpGraceEvt getUserProExpGraceEvt() {
		return userProExpGraceEvt;
	}

	/**
	 * Sets the user pro exp grace evt.
	 *
	 * @param userProExpGraceEvt the userProExpGraceEvt to set
	 */
	public void setUserProExpGraceEvt(UserProExpGraceEvt userProExpGraceEvt) {
		this.userProExpGraceEvt = userProExpGraceEvt;
	}

	/**
	 * Gets the notify project user evt.
	 *
	 * @return the notifyProjectUserEvt
	 */
	public NotifyProjectUserEvt getNotifyProjectUserEvt() {
		return notifyProjectUserEvt;
	}

	/**
	 * Sets the notify project user evt.
	 *
	 * @param notifyProjectUserEvt the notifyProjectUserEvt to set
	 */
	public void setNotifyProjectUserEvt(NotifyProjectUserEvt notifyProjectUserEvt) {
		this.notifyProjectUserEvt = notifyProjectUserEvt;
	}

	/**
	 * Gets the notify AA project user evt.
	 *
	 * @return the notifyAAProjectUserEvt
	 */
	public NotifyAAProjectUserEvt getNotifyAAProjectUserEvt() {
		return notifyAAProjectUserEvt;
	}

	/**
	 * Sets the notify AA project user evt.
	 *
	 * @param notifyAAProjectUserEvt the notifyAAProjectUserEvt to set
	 */
	public void setNotifyAAProjectUserEvt(NotifyAAProjectUserEvt notifyAAProjectUserEvt) {
		this.notifyAAProjectUserEvt = notifyAAProjectUserEvt;
	}

	public NotificationTemplates getNotificationTemplates() {
		return notificationTemplates;
	}

	/**
	 * Gets the notification children.
	 *
	 * @return the notificationChildren
	 */
	public List<IAdminTreeChild> getNotificationChildren() {
		return notificationChildren;
	}
}
