//TODO: Who still needs that out-commented stuff?
/*package com.magna.xmsystem.xmadmin.ui.displayallobjectpage;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.eclipse.core.commands.ParameterizedCommand;
import org.eclipse.e4.core.commands.ECommandService;
import org.eclipse.e4.core.commands.EHandlerService;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.viewers.TreePath;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CLabel;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.magna.xmsystem.dependencies.customExceptions.UnauthorizedAccessException;
import com.magna.xmsystem.ui.controls.widgets.CustomMessageDialog;
import com.magna.xmsystem.xmadmin.message.Message;
import com.magna.xmsystem.xmadmin.restclient.adminArea.AdminAreaController;
import com.magna.xmsystem.xmadmin.restclient.application.BaseAppController;
import com.magna.xmsystem.xmadmin.restclient.application.ProjectAppController;
import com.magna.xmsystem.xmadmin.restclient.application.StartAppController;
import com.magna.xmsystem.xmadmin.restclient.application.UserAppController;
import com.magna.xmsystem.xmadmin.restclient.project.ProjectController;
import com.magna.xmsystem.xmadmin.restclient.relation.AdminAreaProjectAppRelController;
import com.magna.xmsystem.xmadmin.restclient.relation.AdminAreaProjectRelController;
import com.magna.xmsystem.xmadmin.restclient.relation.AdminAreaStartAppRelController;
import com.magna.xmsystem.xmadmin.restclient.relation.AdminAreaUserAppRelController;
import com.magna.xmsystem.xmadmin.restclient.relation.ProjectStartAppRelController;
import com.magna.xmsystem.xmadmin.restclient.relation.SiteAdminAreaRelController;
import com.magna.xmsystem.xmadmin.restclient.relation.UserProjectRelController;
import com.magna.xmsystem.xmadmin.restclient.relation.UserStartAppRelController;
import com.magna.xmsystem.xmadmin.restclient.site.SiteController;
import com.magna.xmsystem.xmadmin.restclient.users.UserController;
import com.magna.xmsystem.xmadmin.ui.dialogs.ProjectExpiryDialog;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.AdminTreeviewer;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.AdminTreeFactory;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.adminarea.AdminAreaProjectAppFixed;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.adminarea.AdminAreaProjectAppNotFixed;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.adminarea.AdminAreaProjectAppProtected;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.adminarea.AdminAreaProjectApplications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.adminarea.AdminAreaProjectChild;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.adminarea.AdminAreaProjectStartApplications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.adminarea.AdminAreaStartApplications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.adminarea.AdminAreaUserAppFixed;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.adminarea.AdminAreaUserAppNotFixed;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.adminarea.AdminAreaUserAppProtected;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.adminarea.AdminAreaUserApplications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.adminarea.AdministrationArea;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.baseapplicationmodel.BaseApplication;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.projectapplicationmodel.ProjectApplication;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.startapplicationmodel.StartApplication;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.userapplicationmodel.UserApplication;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.adminconf.AdminProjectApps;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.adminconf.AdminUserApps;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.adminconf.AdminUsers;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.livemsgconfig.LiveMessage;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.roles.Role;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.roles.RoleScopeObjectChild;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.roles.RoleScopeObjectUsers;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.roles.RoleUsers;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.directory.Directory;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.directory.DirectoryProjectApplications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.directory.DirectoryProjects;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.directory.DirectoryUserApplications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.directory.DirectoryUsers;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.group.projectappgroup.ProjectAppGroupProjectApps;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.group.projectappgroup.ProjectApplicationGroup;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.group.projectgroup.ProjectGroupModel;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.group.projectgroup.ProjectGroupProjects;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.group.userappgroup.UserAppGroupUserApps;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.group.userappgroup.UserApplicationGroup;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.group.usergroup.UserGroupModel;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.group.usergroup.UserGroupUsers;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.projects.Project;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.projects.ProjectAdminAreaProjectAppFixed;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.projects.ProjectAdminAreaProjectAppNotFixed;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.projects.ProjectAdminAreaProjectAppProtected;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.projects.ProjectAdminAreaStartApplications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.projects.ProjectUserAAProjectAppAllowed;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.projects.ProjectUserAAProjectAppForbidden;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.projects.ProjectUserAAProjectApplications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.projects.ProjectUserChild;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.refs.RelationObj;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.sitemodel.Site;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.sitemodel.SiteAdminAreaProjectApplications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.sitemodel.SiteAdminAreaProjectChild;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.sitemodel.SiteAdminAreaProjectStartApplications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.sitemodel.SiteAdminAreaStartApplications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.sitemodel.SiteAdminAreaUserAppFixed;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.sitemodel.SiteAdminAreaUserAppNotFixed;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.sitemodel.SiteAdminAreaUserAppProtected;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.sitemodel.SiteAdminAreaUserApplications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.sitemodel.SiteAdminProjectAppFixed;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.sitemodel.SiteAdminProjectAppNotFixed;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.sitemodel.SiteAdminProjectAppProtected;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.sitemodel.SiteAdministrationChild;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.users.User;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.users.UserAAUserAppAllowed;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.users.UserAAUserAppForbidden;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.users.UserAAUserApplications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.users.UserProjectAAProjectAppAllowed;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.users.UserProjectAAProjectAppForbidden;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.users.UserProjectAAProjectApplications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.users.UserProjectChild;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.users.UserStartApplications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.users.Users;
import com.magna.xmsystem.xmadmin.util.CommonConstants;
import com.magna.xmsystem.xmadmin.util.XMAdminUtil;

*//**
 * Class for Context menu.
 *
 * @author Chiranjeevi.Akula
 *//*
@SuppressWarnings("restriction")
public class ContextMenu {

	*//** Member variable 'handler service' for {@link EHandlerService}. *//*
	private EHandlerService handlerService;

	*//** Member variable 'command service' for {@link ECommandService}. *//*
	private ECommandService commandService;

	*//** Member variable 'messages' for {@link Message}. *//*
	private Message messages;

	*//**
	 * Member variable 'display all object page' for
	 * {@link DisplayAllObjectPage}.
	 *//*
	private DisplayAllObjectPage displayAllObjectPage;

	*//** Member variable 'c label' for {@link CLabel}. *//*
	private CLabel cLabel;

	*//** Member variable 'menu' for {@link Menu}. *//*
	private Menu menu;

	*//** Member variable 'admin tree' for {@link AdminTreeviewer}. *//*
	private AdminTreeviewer adminTree;

	*//** The Constant LOGGER. *//*
	private static final Logger LOGGER = LoggerFactory.getLogger(ContextMenu.class);

	*//**
	 * Constructor for ContextMenu Class.
	 *
	 * @param displayAllObjectPage
	 *            {@link DisplayAllObjectPage}
	 * @param cLabel
	 *            {@link CLabel}
	 * @param handlerService
	 *            {@link EHandlerService}
	 * @param commandService
	 *            {@link ECommandService}
	 * @param messages
	 *            {@link Message}
	 *//*
	public ContextMenu(DisplayAllObjectPage displayAllObjectPage, CLabel cLabel, EHandlerService handlerService,
			ECommandService commandService, Message messages) {
		this.displayAllObjectPage = displayAllObjectPage;
		this.adminTree = XMAdminUtil.getInstance().getAdminTree();
		this.cLabel = cLabel;
		this.handlerService = handlerService;
		this.commandService = commandService;
		this.messages = messages;
		this.menu = new Menu(displayAllObjectPage);
	}

	*//**
	 * Method for Creates the menu item for relation obj.
	 *//*
	public void createMenuItemForRelationObj() {
		Object selectedCLabelObjData = cLabel.getData();
		MenuItem activateMenuItem = new MenuItem(menu, SWT.PUSH);
		MenuItem deactivateMenuItem = new MenuItem(menu, SWT.PUSH);
		MenuItem removeRelationMenuItem = new MenuItem(menu, SWT.PUSH);
		activateMenuItem.setText(messages.popupmenulabelnodeActive);
		deactivateMenuItem.setText(messages.popupmenulabelnodeDeActive);
		removeRelationMenuItem.setText(messages.popupmenulabelnodeRemoveRelation);
		RelationObj relationObj = (RelationObj) selectedCLabelObjData;
		cLabel.setMenu(menu);

		boolean active = relationObj.isActive();

		IAdminTreeChild containerObj = relationObj.getContainerObj();
		if (containerObj instanceof SiteAdministrationChild) {
			if (active) {
				activateMenuItem.setEnabled(false);
				deActivateRelListener(deactivateMenuItem, relationObj);
			} else {
				deactivateMenuItem.setEnabled(false);
				activateRelListener(activateMenuItem, relationObj);
			}
		} else if (containerObj instanceof SiteAdminAreaProjectChild || containerObj instanceof AdminAreaProjectChild) {
			if (active) {
				activateMenuItem.setEnabled(false);
				deActivateRelListener(deactivateMenuItem, relationObj);
			} else {
				deactivateMenuItem.setEnabled(false);
				activateRelListener(activateMenuItem, relationObj);
			}
		} else if (containerObj instanceof SiteAdminAreaUserAppNotFixed
				|| containerObj instanceof SiteAdminAreaUserAppFixed
				|| containerObj instanceof SiteAdminAreaUserAppProtected
				|| containerObj instanceof AdminAreaUserAppProtected || containerObj instanceof AdminAreaUserAppNotFixed
				|| containerObj instanceof AdminAreaUserAppFixed) {
			if (active) {
				activateMenuItem.setEnabled(false);
				deActivateRelListener(deactivateMenuItem, relationObj);
			} else {
				deactivateMenuItem.setEnabled(false);
				activateRelListener(activateMenuItem, relationObj);
			}
		} else if (containerObj instanceof SiteAdminProjectAppNotFixed
				|| containerObj instanceof SiteAdminProjectAppFixed
				|| containerObj instanceof SiteAdminProjectAppProtected
				|| containerObj instanceof AdminAreaProjectAppNotFixed
				|| containerObj instanceof AdminAreaProjectAppFixed
				|| containerObj instanceof AdminAreaProjectAppProtected
				|| containerObj instanceof ProjectAdminAreaProjectAppNotFixed
				|| containerObj instanceof ProjectAdminAreaProjectAppFixed
				|| containerObj instanceof ProjectAdminAreaProjectAppProtected) {
			if (active) {
				activateMenuItem.setEnabled(false);
				deActivateRelListener(deactivateMenuItem, relationObj);
			} else {
				deactivateMenuItem.setEnabled(false);
				activateRelListener(activateMenuItem, relationObj);
			}
		} else if (containerObj instanceof SiteAdminAreaStartApplications
				|| containerObj instanceof AdminAreaStartApplications
				|| containerObj instanceof ProjectAdminAreaStartApplications) {
			if (active) {
				activateMenuItem.setEnabled(false);
				deActivateRelListener(deactivateMenuItem, relationObj);
			} else {
				deactivateMenuItem.setEnabled(false);
				activateRelListener(activateMenuItem, relationObj);
			}
		} else if (containerObj instanceof SiteAdminAreaProjectStartApplications
				|| containerObj instanceof AdminAreaProjectStartApplications) {
			if (active) {
				activateMenuItem.setEnabled(false);
				deActivateRelListener(deactivateMenuItem, relationObj);
			} else {
				deactivateMenuItem.setEnabled(false);
				activateRelListener(activateMenuItem, relationObj);
			}
		} else if (containerObj instanceof ProjectUserChild || containerObj instanceof UserProjectChild) {
			if (active) {
				activateMenuItem.setEnabled(false);
				deActivateRelListener(deactivateMenuItem, relationObj);
			} else {
				deactivateMenuItem.setEnabled(false);
				activateRelListener(activateMenuItem, relationObj);
			}
		} else if (containerObj instanceof UserProjectAAProjectAppAllowed
				|| containerObj instanceof UserProjectAAProjectAppForbidden
				|| containerObj instanceof UserAAUserAppAllowed || containerObj instanceof UserAAUserAppForbidden) {
			activateMenuItem.dispose();
			deactivateMenuItem.dispose();
		} else if (containerObj instanceof UserProjectChild) {
			if (active) {
				activateMenuItem.setEnabled(false);
				deActivateRelListener(deactivateMenuItem, relationObj);
			} else {
				deactivateMenuItem.setEnabled(false);
				activateRelListener(activateMenuItem, relationObj);
			}
		} else if (containerObj instanceof UserProjectChild) {
			if (active) {
				activateMenuItem.setEnabled(false);
				deActivateRelListener(deactivateMenuItem, relationObj);
			} else {
				deactivateMenuItem.setEnabled(false);
				activateRelListener(activateMenuItem, relationObj);
			}
		} else if (containerObj instanceof UserStartApplications) {
			if (active) {
				activateMenuItem.setEnabled(false);
				deActivateRelListener(deactivateMenuItem, relationObj);
			} else {
				deactivateMenuItem.setEnabled(false);
				activateRelListener(activateMenuItem, relationObj);
			}
		} else if (containerObj instanceof DirectoryUsers || containerObj instanceof DirectoryProjects
				|| containerObj instanceof DirectoryUserApplications
				|| containerObj instanceof DirectoryProjectApplications || containerObj instanceof RoleUsers
				|| containerObj instanceof RoleScopeObjectUsers || containerObj instanceof RoleScopeObjectChild
				|| containerObj instanceof AdminUsers || containerObj instanceof AdminUserApps
				|| containerObj instanceof AdminProjectApps || containerObj instanceof UserProjectAAProjectAppForbidden
				|| containerObj instanceof UserProjectAAProjectAppAllowed
				|| containerObj instanceof UserAAUserAppForbidden || containerObj instanceof UserAAUserAppAllowed
				|| containerObj instanceof UserGroupUsers || containerObj instanceof ProjectGroupProjects
				|| containerObj instanceof UserAppGroupUserApps || containerObj instanceof ProjectAppGroupProjectApps
				|| containerObj instanceof ProjectUserAAProjectAppForbidden || containerObj instanceof ProjectUserAAProjectAppAllowed) {
			MenuItem[] items = menu.getItems();
			List<MenuItem> removeMenuItems = new ArrayList<>(2);
			for (MenuItem menuItem : items) {
				if (messages.popupmenulabelnodeActive.equals(menuItem.getText())
						|| messages.popupmenulabelnodeDeActive.equals(menuItem.getText())) {
					removeMenuItems.add(menuItem);
				}
			}
			for (MenuItem menuItem : removeMenuItems) {
				menuItem.dispose();
			}
		} else if (containerObj instanceof SiteAdminAreaUserApplications
				|| containerObj instanceof SiteAdminAreaProjectApplications
				|| containerObj instanceof AdminAreaProjectApplications
				|| containerObj instanceof AdminAreaUserApplications
				|| containerObj instanceof ProjectUserAAProjectApplications
				|| containerObj instanceof UserProjectAAProjectApplications
				|| containerObj instanceof UserAAUserApplications) {
			MenuItem[] items = menu.getItems();
			List<MenuItem> removeMenuItems = new ArrayList<>(2);
			for (MenuItem menuItem : items) {
				if (messages.popupmenulabelnodeActive.equals(menuItem.getText())
						|| messages.popupmenulabelnodeDeActive.equals(menuItem.getText())
						|| messages.popupmenulabelnodeRemoveRelation.equals(menuItem.getText())) {
					removeMenuItems.add(menuItem);
				}
			}
			for (MenuItem menuItem : removeMenuItems) {
				menuItem.dispose();
			}
		} 

		if (containerObj instanceof ProjectUserChild) {
			MenuItem expiryDate = new MenuItem(menu, SWT.PUSH);
			expiryDate.setText(messages.popupmenuExpiryDays);
			expiryDate.addSelectionListener(new SelectionAdapter() {

				@Override
				public void widgetSelected(SelectionEvent arg0) {
					ProjectExpiryDialog project = new ProjectExpiryDialog(Display.getDefault().getActiveShell(),
							relationObj, messages);
					project.open();
					String expiryDays = project.getExpiryDays();
					relationObj.setExpiryDays(expiryDays);
				}
			});
		}

		if (!(containerObj instanceof SiteAdminAreaUserApplications)
				&& !(containerObj instanceof SiteAdminAreaProjectApplications)
				&& !(containerObj instanceof AdminAreaProjectApplications)
				&& !(containerObj instanceof AdminAreaUserApplications)
				&& !(containerObj instanceof ProjectUserAAProjectApplications)
				&& !(containerObj instanceof UserProjectAAProjectApplications)
				&& !(containerObj instanceof UserAAUserApplications)) {
			removeRelationMenuItem.addSelectionListener(new SelectionAdapter() {

				@Override
				public void widgetSelected(final SelectionEvent paramSelectionEvent) {
					ParameterizedCommand cmd = commandService.createCommand(CommonConstants.COMMAND_ID.REMOVE_RELATION,
							null);
					if (handlerService.canExecute(cmd)) {
						handlerService.executeHandler(cmd);
					}
				}
			});
		}
	}

	*//**
	 * Method for Activate rel listener.
	 *
	 * @param activateMenuItem
	 *            {@link MenuItem}
	 * @param relationObj
	 *            {@link RelationObj}
	 *//*
	private void activateRelListener(MenuItem activateMenuItem, RelationObj relationObj) {

		activateMenuItem.addSelectionListener(new SelectionAdapter() {

			@Override
			public void widgetSelected(SelectionEvent paramSelectionEvent) {
				IAdminTreeChild containerObj = relationObj.getContainerObj();
				if (containerObj instanceof SiteAdministrationChild) {
					activateSiteAARel();
				} else if (containerObj instanceof SiteAdminAreaProjectChild
						|| containerObj instanceof AdminAreaProjectChild) {
					activateSiteAAProjectRel();
				} else if (containerObj instanceof SiteAdminAreaUserAppNotFixed
						|| containerObj instanceof SiteAdminAreaUserAppFixed
						|| containerObj instanceof SiteAdminAreaUserAppProtected
						|| containerObj instanceof AdminAreaUserAppProtected
						|| containerObj instanceof AdminAreaUserAppNotFixed
						|| containerObj instanceof AdminAreaUserAppFixed) {
					activateSiteAAUserAppRel();
				} else if (containerObj instanceof SiteAdminProjectAppNotFixed
						|| containerObj instanceof SiteAdminProjectAppFixed
						|| containerObj instanceof SiteAdminProjectAppProtected
						|| containerObj instanceof AdminAreaProjectAppNotFixed
						|| containerObj instanceof AdminAreaProjectAppFixed
						|| containerObj instanceof AdminAreaProjectAppProtected
						|| containerObj instanceof ProjectAdminAreaProjectAppNotFixed
						|| containerObj instanceof ProjectAdminAreaProjectAppFixed
						|| containerObj instanceof ProjectAdminAreaProjectAppProtected) {
					activateSiteAAProjectAppRel();
				} else if (containerObj instanceof SiteAdminAreaStartApplications
						|| containerObj instanceof AdminAreaStartApplications) {
					activateSiteAAStartAppRel();
				} else if (containerObj instanceof SiteAdminAreaProjectStartApplications
						|| containerObj instanceof AdminAreaProjectStartApplications
						|| containerObj instanceof ProjectAdminAreaStartApplications) {
					activateSiteAAProStartAppRel();
				} else if (containerObj instanceof ProjectUserChild) {
					activateProjectUserRel();
				} else if (containerObj instanceof UserProjectChild) {
					activateUserProjectRel();
				} else if (containerObj instanceof UserStartApplications) {
					activateUserStartAppRel();
				}
				displayAllObjectPage.setLabelAndIcon(cLabel);
			}

			*//**
			 * Method for Activate user start app rel.
			 *//*
			private void activateUserStartAppRel() {
				try {
					StartApplication startApplication = (StartApplication) relationObj.getRefObject();
					String relId = relationObj.getRelId();
					String startAppRelname = startApplication.getName();
					String confirmDialogTitle = messages.changeStatusConfirmDialogTitle;
					String confirmDialogMsg = messages.activateRelchangeStatusConfirmDialogMsg + " \'" + startAppRelname
							+ " \'" + " ?";
					boolean isConfirmed = CustomMessageDialog.openQuestion(adminTree.getControl().getShell(),
							confirmDialogTitle, confirmDialogMsg);
					if (isConfirmed) {
						String status = com.magna.xmbackend.vo.enums.Status.ACTIVE.name();
						UserStartAppRelController userStartAppRelController = new UserStartAppRelController();
						boolean isUpdated = userStartAppRelController.updateUserStartAppRelStatus(status, relId);
						if (isUpdated) {
							relationObj.setActive(true);
							adminTree.refresh(relationObj);
						}
					}
				} catch (UnauthorizedAccessException e) {
					CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
							messages.objectPermissionDialogTitle , messages.objectPermissionDialogMsg );
					return;
				} catch (Exception e) {
					LOGGER.warn("Unable to activate user start application relation model! ", e);
				}
			}

			*//**
			 * Method for Activate user project rel.
			 *//*
			private void activateUserProjectRel() {
				try {
					Project project = (Project) relationObj.getRefObject();
					String relId = relationObj.getRelId();
					String projectUserRelName = project.getName();
					String confirmDialogTitle = messages.changeStatusConfirmDialogTitle;
					String confirmDialogMsg = messages.deActivateRelchangeStatusConfirmDialogMsg + " \'"
							+ projectUserRelName + " \'" + " ?";
					boolean isConfirmed = CustomMessageDialog.openQuestion(adminTree.getControl().getShell(),
							confirmDialogTitle, confirmDialogMsg);
					if (isConfirmed) {
						String status = com.magna.xmbackend.vo.enums.Status.ACTIVE.name();
						UserProjectRelController userProjectRelController = new UserProjectRelController();
						boolean isUpdated = userProjectRelController.updateUserProjectRelStatus(relId, status);
						if (isUpdated) {
							relationObj.setActive(true);
							adminTree.refresh(relationObj);
						}
					}
				} catch (UnauthorizedAccessException e) {
					CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
							messages.objectPermissionDialogTitle , messages.objectPermissionDialogMsg );
					return;
				} catch (Exception e) {
					LOGGER.warn("Unable to Activate user project relation model! ", e);
				}
			}

			*//**
			 * Method for Activate project user rel.
			 *//*
			private void activateProjectUserRel() {
				try {
					User user = (User) relationObj.getRefObject();
					String relId = relationObj.getRelId();
					String userRelname = user.getName();
					String confirmDialogTitle = messages.changeStatusConfirmDialogTitle;
					String confirmDialogMsg = messages.activateRelchangeStatusConfirmDialogMsg + " \'" + userRelname
							+ " \'" + " ?";
					boolean isConfirmed = CustomMessageDialog.openQuestion(adminTree.getControl().getShell(),
							confirmDialogTitle, confirmDialogMsg);
					if (isConfirmed) {
						String status = com.magna.xmbackend.vo.enums.Status.ACTIVE.name();
						UserProjectRelController userProjectRelController = new UserProjectRelController();
						boolean isUpdated = userProjectRelController.updateUserProjectRelStatus(relId, status);
						if (isUpdated) {
							relationObj.setActive(true);
							adminTree.refresh(relationObj);
						}
					}
				} catch (UnauthorizedAccessException e) {
					CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
							messages.objectPermissionDialogTitle , messages.objectPermissionDialogMsg );
					return;
				} catch (Exception e) {
					LOGGER.warn("Unable to Activate project user relation model! ", e);
				}
			}

			*//**
			 * Method for Activate site AA pro start app rel.
			 *//*
			private void activateSiteAAProStartAppRel() {
				try {
					StartApplication startApplication = (StartApplication) relationObj.getRefObject();
					String relId = relationObj.getRelId();
					String startAppRelname = startApplication.getName();
					String confirmDialogTitle = messages.changeStatusConfirmDialogTitle;
					String confirmDialogMsg = messages.activateRelchangeStatusConfirmDialogMsg + " \'" + startAppRelname
							+ " \'" + " ?";
					boolean isConfirmed = CustomMessageDialog.openQuestion(adminTree.getControl().getShell(),
							confirmDialogTitle, confirmDialogMsg);
					if (isConfirmed) {
						String status = com.magna.xmbackend.vo.enums.Status.ACTIVE.name();
						IAdminTreeChild parent = ((RelationObj)relationObj).getContainerObj().getParent();
						ProjectStartAppRelController projectStartAppRelController = new ProjectStartAppRelController(
								XMAdminUtil.getInstance().getAAForHeaders(parent));
						boolean isUpdated = projectStartAppRelController.updateProjectStartAppRelStatus(relId, status);
						if (isUpdated) {
							relationObj.setActive(true);
							adminTree.refresh(relationObj);
						}
					}
				} catch (UnauthorizedAccessException e) {
					CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
							messages.objectPermissionDialogTitle , messages.objectPermissionDialogMsg );
					return;
				} catch (Exception e) {
					LOGGER.warn("Unable to activate site admin area project start application relation model! ", e);
				}
			}

			*//**
			 * Method for Activate site AA start app rel.
			 *//*
			private void activateSiteAAStartAppRel() {
				try {
					StartApplication startApplication = (StartApplication) relationObj.getRefObject();
					String relId = relationObj.getRelId();
					String startAppRelname = startApplication.getName();
					String confirmDialogTitle = messages.changeStatusConfirmDialogTitle;
					String confirmDialogMsg = messages.activateRelchangeStatusConfirmDialogMsg + " \'" + startAppRelname
							+ " \'" + " ?";
					boolean isConfirmed = CustomMessageDialog.openQuestion(adminTree.getControl().getShell(),
							confirmDialogTitle, confirmDialogMsg);
					if (isConfirmed) {
						String status = com.magna.xmbackend.vo.enums.Status.ACTIVE.name();
						AdminAreaStartAppRelController adminAreaStartAppRelController = new AdminAreaStartAppRelController(
								XMAdminUtil.getInstance().getAAForHeaders(relationObj.getContainerObj()));
						boolean isUpdated = adminAreaStartAppRelController.updateAdminAreaStartAppRelStatus(relId,
								status);
						if (isUpdated) {
							relationObj.setActive(true);
							adminTree.refresh(relationObj);
						}
					}
				} catch (UnauthorizedAccessException e) {
					CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
							messages.objectPermissionDialogTitle , messages.objectPermissionDialogMsg );
					return;
				} catch (Exception e) {
					LOGGER.warn("Unable to activate site admin area start application relation model! ", e);
				}

			}

			*//**
			 * Method for Activate site AA project app rel.
			 *//*
			private void activateSiteAAProjectAppRel() {
				try {
					ProjectApplication projectApplication = (ProjectApplication) relationObj.getRefObject();
					String relId = relationObj.getRelId();
					String projectAppRelname = projectApplication.getName();
					String confirmDialogTitle = messages.changeStatusConfirmDialogTitle;
					String confirmDialogMsg = messages.activateRelchangeStatusConfirmDialogMsg + " \'"
							+ projectAppRelname + " \'" + " ?";
					boolean isConfirmed = CustomMessageDialog.openQuestion(adminTree.getControl().getShell(),
							confirmDialogTitle, confirmDialogMsg);
					if (isConfirmed) {
						String status = com.magna.xmbackend.vo.enums.Status.ACTIVE.name();
						IAdminTreeChild parent = ((RelationObj) relationObj).getContainerObj().getParent().getParent();
						//IAdminTreeChild containerObj = ((RelationObj) relationObj.getContainerObj().getParent()
						//		.getParent().getParent()).getContainerObj();
						AdminAreaProjectAppRelController adminAreaProjectAppRelController = new AdminAreaProjectAppRelController(
								XMAdminUtil.getInstance().getAAForHeaders(parent));
						boolean isUpdated = adminAreaProjectAppRelController.updateAdminAreaProjAppRelStatus(relId,
								status);
						if (isUpdated) {
							relationObj.setActive(true);
							adminTree.refresh(relationObj);
						}
					}
				} catch (UnauthorizedAccessException e) {
					CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
							messages.objectPermissionDialogTitle , messages.objectPermissionDialogMsg );
					return;
				} catch (Exception e) {
					LOGGER.warn("Unable to activate site admin area project application relation model! ", e);
				}

			}

			*//**
			 * Method for Activate site AA user app rel.
			 *//*
			private void activateSiteAAUserAppRel() {
				try {
					UserApplication userApplication = (UserApplication) relationObj.getRefObject();
					String relId = relationObj.getRelId();
					String userAppRelname = userApplication.getName();
					String confirmDialogTitle = messages.changeStatusConfirmDialogTitle;
					String confirmDialogMsg = messages.activateRelchangeStatusConfirmDialogMsg + " \'" + userAppRelname
							+ " \'" + " ?";
					boolean isConfirmed = CustomMessageDialog.openQuestion(adminTree.getControl().getShell(),
							confirmDialogTitle, confirmDialogMsg);
					if (isConfirmed) {
						String status = com.magna.xmbackend.vo.enums.Status.ACTIVE.name();
						AdminAreaUserAppRelController adminAreaUserAppRelController = new AdminAreaUserAppRelController(
								XMAdminUtil.getInstance().getAAForHeaders(relationObj.getContainerObj()));
						boolean isUpdated = adminAreaUserAppRelController.updateAdminAreaUserAppRelStatus(relId,
								status);
						if (isUpdated) {
							relationObj.setActive(true);
							adminTree.refresh(relationObj);
						}
					}
				} catch (UnauthorizedAccessException e) {
					CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
							messages.objectPermissionDialogTitle , messages.objectPermissionDialogMsg );
					return;
				} catch (Exception e) {
					LOGGER.warn("Unable to activate site admin area user application relation model! ", e);
				}

			}

			*//**
			 * Method for Activate site AA project rel.
			 *//*
			private void activateSiteAAProjectRel() {
				try {
					Project project = (Project) relationObj.getRefObject();
					String relId = relationObj.getRelId();
					String adminAreaProjectRelname = project.getName();
					String confirmDialogTitle = messages.changeStatusConfirmDialogTitle;
					String confirmDialogMsg = messages.activateRelchangeStatusConfirmDialogMsg + " \'"
							+ adminAreaProjectRelname + " \'" + " ?";
					boolean isConfirmed = CustomMessageDialog.openQuestion(adminTree.getControl().getShell(),
							confirmDialogTitle, confirmDialogMsg);

					if (isConfirmed) {
						String status = com.magna.xmbackend.vo.enums.Status.ACTIVE.name();
						AdminAreaProjectRelController adminAreaProjectRelController = new AdminAreaProjectRelController(
								XMAdminUtil.getInstance().getAAForHeaders(relationObj));
						boolean isUpdated = adminAreaProjectRelController.updateAdminAreaProjectRelStatus(relId,
								status);
						if (isUpdated) {
							relationObj.setActive(true);
							adminTree.refresh(relationObj);
						}
					}
				} catch (UnauthorizedAccessException e) {
					CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
							messages.objectPermissionDialogTitle , messages.objectPermissionDialogMsg );
					return;
				} catch (Exception e) {
					LOGGER.warn("Unable to activate site admin area project relation model! ", e);
				}
			}

			*//**
			 * Method for Activate site AA rel.
			 *//*
			private void activateSiteAARel() {
				try {
					AdministrationArea administrationArea = (AdministrationArea) relationObj.getRefObject();
					String relId = relationObj.getRelId();
					String adminAreaRelname = administrationArea.getName();
					String confirmDialogTitle = messages.changeStatusConfirmDialogTitle;
					String confirmDialogMsg = messages.activateRelchangeStatusConfirmDialogMsg + " \'"
							+ adminAreaRelname + " \'" + " ?";
					boolean isConfirmed = CustomMessageDialog.openQuestion(adminTree.getControl().getShell(),
							confirmDialogTitle, confirmDialogMsg);
					if (isConfirmed) {
						String status = com.magna.xmbackend.vo.enums.Status.ACTIVE.name();
						SiteAdminAreaRelController siteAdminAreaRelController = new SiteAdminAreaRelController();
						boolean isUpdated = siteAdminAreaRelController.updateSiteAdminAreaRelStatus(relId, status);
						if (isUpdated) {
							relationObj.setActive(true);
							adminTree.refresh(relationObj);
						}
					}
				} catch (UnauthorizedAccessException e) {
					CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
							messages.objectPermissionDialogTitle , messages.objectPermissionDialogMsg );
					return;
				} catch (Exception e) {
					LOGGER.warn("Unable to activate site admin area relation model! ", e);
				}
			}
		});
	}

	*//**
	 * Method for De activate rel listener.
	 *
	 * @param deactivateMenuItem
	 *            {@link MenuItem}
	 * @param relationObj
	 *            {@link RelationObj}
	 *//*
	private void deActivateRelListener(MenuItem deactivateMenuItem, RelationObj relationObj) {
		deactivateMenuItem.addSelectionListener(new SelectionListener() {
			@Override
			public void widgetSelected(SelectionEvent paramSelectionEvent) {
				IAdminTreeChild containerObj = relationObj.getContainerObj();
				if (containerObj instanceof SiteAdministrationChild) {
					deActivateSiteAARel();
				} else if (containerObj instanceof SiteAdminAreaProjectChild
						|| containerObj instanceof AdminAreaProjectChild) {
					deActivateSiteAAProjectRel();
				} else if (containerObj instanceof SiteAdminAreaUserAppNotFixed
						|| containerObj instanceof SiteAdminAreaUserAppFixed
						|| containerObj instanceof AdminAreaUserAppProtected
						|| containerObj instanceof AdminAreaUserAppNotFixed
						|| containerObj instanceof AdminAreaUserAppFixed
						|| containerObj instanceof SiteAdminAreaUserAppProtected) {
					deActivateSiteAAUserAppRel();
				} else if (containerObj instanceof SiteAdminProjectAppNotFixed
						|| containerObj instanceof SiteAdminProjectAppFixed
						|| containerObj instanceof SiteAdminProjectAppProtected
						|| containerObj instanceof AdminAreaProjectAppNotFixed
						|| containerObj instanceof AdminAreaProjectAppFixed
						|| containerObj instanceof AdminAreaProjectAppProtected
						|| containerObj instanceof ProjectAdminAreaProjectAppNotFixed
						|| containerObj instanceof ProjectAdminAreaProjectAppFixed
						|| containerObj instanceof ProjectAdminAreaProjectAppProtected) {
					deActivateSiteAAProjectAppRel();
				} else if (containerObj instanceof SiteAdminAreaStartApplications
						|| containerObj instanceof AdminAreaStartApplications) {
					deActivateSiteAAStartAppRel();
				} else if (containerObj instanceof SiteAdminAreaProjectStartApplications
						|| containerObj instanceof AdminAreaProjectStartApplications
						|| containerObj instanceof ProjectAdminAreaStartApplications) {
					deActivateSiteAAProStartAppRel();
				} else if (containerObj instanceof ProjectUserChild) {
					deActivateProjectUserRel();
				} else if (containerObj instanceof UserProjectChild) {
					deActivateUserProjectRel();
				} else if (containerObj instanceof UserStartApplications) {
					deActivateUserStartAppRel();
				}
				displayAllObjectPage.setLabelAndIcon(cLabel);
			}

			*//**
			 * Method for De activate user start app rel.
			 *//*
			private void deActivateUserStartAppRel() {
				try {
					StartApplication startApplication = (StartApplication) relationObj.getRefObject();
					String relId = relationObj.getRelId();
					String startAppRelname = startApplication.getName();
					String confirmDialogTitle = messages.changeStatusConfirmDialogTitle;
					String confirmDialogMsg = messages.activateRelchangeStatusConfirmDialogMsg + " \'" + startAppRelname
							+ " \'" + " ?";
					boolean isConfirmed = CustomMessageDialog.openQuestion(adminTree.getControl().getShell(),
							confirmDialogTitle, confirmDialogMsg);
					if (isConfirmed) {
						String status = com.magna.xmbackend.vo.enums.Status.INACTIVE.name();
						UserStartAppRelController userStartAppRelController = new UserStartAppRelController();
						boolean isUpdated = userStartAppRelController.updateUserStartAppRelStatus(status, relId);
						if (isUpdated) {
							relationObj.setActive(false);
							adminTree.refresh(relationObj);
						}
					}
				} catch (UnauthorizedAccessException e) {
					CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
							messages.objectPermissionDialogTitle , messages.objectPermissionDialogMsg );
					return;
				} catch (Exception e) {
					LOGGER.warn("Unable to De-Activate user start application relation model! ", e);
				}
			}

			*//**
			 * Method for De activate user project rel.
			 *//*
			private void deActivateUserProjectRel() {
				try {
					Project project = (Project) relationObj.getRefObject();
					String relId = relationObj.getRelId();
					String projectUserRelName = project.getName();
					String confirmDialogTitle = messages.changeStatusConfirmDialogTitle;
					String confirmDialogMsg = messages.deActivateRelchangeStatusConfirmDialogMsg + " \'"
							+ projectUserRelName + " \'" + " ?";
					boolean isConfirmed = CustomMessageDialog.openQuestion(adminTree.getControl().getShell(),
							confirmDialogTitle, confirmDialogMsg);
					if (isConfirmed) {
						String status = com.magna.xmbackend.vo.enums.Status.INACTIVE.name();
						UserProjectRelController userProjectRelController = new UserProjectRelController();
						boolean isUpdated = userProjectRelController.updateUserProjectRelStatus(relId, status);
						if (isUpdated) {
							relationObj.setActive(false);
							adminTree.refresh(relationObj);
						}
					}
				} catch (UnauthorizedAccessException e) {
					CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
							messages.objectPermissionDialogTitle , messages.objectPermissionDialogMsg );
					return;
				} catch (Exception e) {
					LOGGER.warn("Unable to De-Activate user project relation model! ", e);
				}
			}

			*//**
			 * Method for De activate project user rel.
			 *//*
			private void deActivateProjectUserRel() {
				try {
					User user = (User) relationObj.getRefObject();
					String relId = relationObj.getRelId();
					String userRelname = user.getName();
					String confirmDialogTitle = messages.changeStatusConfirmDialogTitle;
					String confirmDialogMsg = messages.deActivateRelchangeStatusConfirmDialogMsg + " \'" + userRelname
							+ " \'" + " ?";
					boolean isConfirmed = CustomMessageDialog.openQuestion(adminTree.getControl().getShell(),
							confirmDialogTitle, confirmDialogMsg);
					if (isConfirmed) {
						String status = com.magna.xmbackend.vo.enums.Status.INACTIVE.name();
						UserProjectRelController userProjectRelController = new UserProjectRelController();
						boolean isUpdated = userProjectRelController.updateUserProjectRelStatus(relId, status);
						if (isUpdated) {
							relationObj.setActive(false);
							adminTree.refresh(relationObj);
						}
					}
				} catch (UnauthorizedAccessException e) {
					CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
							messages.objectPermissionDialogTitle , messages.objectPermissionDialogMsg );
					return;
				} catch (Exception e) {
					LOGGER.warn("Unable to De-Activate project user relation model! ", e);
				}
			}

			*//**
			 * Method for De activate site AA pro start app rel.
			 *//*
			private void deActivateSiteAAProStartAppRel() {
				try {
					StartApplication startApplication = (StartApplication) relationObj.getRefObject();
					String relId = relationObj.getRelId();
					String startAppRelname = startApplication.getName();
					String confirmDialogTitle = messages.changeStatusConfirmDialogTitle;
					String confirmDialogMsg = messages.deActivateRelchangeStatusConfirmDialogMsg + " \'"
							+ startAppRelname + " \'" + " ?";
					boolean isConfirmed = CustomMessageDialog.openQuestion(adminTree.getControl().getShell(),
							confirmDialogTitle, confirmDialogMsg);
					if (isConfirmed) {
						String status = com.magna.xmbackend.vo.enums.Status.INACTIVE.name();
						IAdminTreeChild parent = ((RelationObj)relationObj).getContainerObj().getParent();
						//IAdminTreeChild containerObj = ((RelationObj) ((SiteAdminAreaProjectStartApplications) relationObj
								//.getContainerObj().getParent()).getParent()).getContainerObj();
						ProjectStartAppRelController projectStartAppRelController = new ProjectStartAppRelController(
								XMAdminUtil.getInstance().getAAForHeaders(parent));
						boolean isUpdated = projectStartAppRelController.updateProjectStartAppRelStatus(relId, status);
						if (isUpdated) {
							relationObj.setActive(false);
							adminTree.refresh(relationObj);
						}
					}
				} catch (UnauthorizedAccessException e) {
					CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
							messages.objectPermissionDialogTitle , messages.objectPermissionDialogMsg );
					return;
				} catch (Exception e) {
					LOGGER.warn("Unable to De-Activate site admin area project start application relation model! ", e);
				}
			}

			*//**
			 * Method for De activate site AA start app rel.
			 *//*
			private void deActivateSiteAAStartAppRel() {
				try {
					StartApplication startApplication = (StartApplication) relationObj.getRefObject();
					String relId = relationObj.getRelId();
					String startAppRelname = startApplication.getName();
					String confirmDialogTitle = messages.changeStatusConfirmDialogTitle;
					String confirmDialogMsg = messages.deActivateRelchangeStatusConfirmDialogMsg + " \'"
							+ startAppRelname + " \'" + " ?";
					boolean isConfirmed = CustomMessageDialog.openQuestion(adminTree.getControl().getShell(),
							confirmDialogTitle, confirmDialogMsg);
					if (isConfirmed) {
						String status = com.magna.xmbackend.vo.enums.Status.INACTIVE.name();
						AdminAreaStartAppRelController adminAreaStartAppRelController = new AdminAreaStartAppRelController(
								XMAdminUtil.getInstance().getAAForHeaders(relationObj.getContainerObj()));
						boolean isUpdated = adminAreaStartAppRelController.updateAdminAreaStartAppRelStatus(relId,
								status);
						if (isUpdated) {
							relationObj.setActive(false);
							adminTree.refresh(relationObj);
						}
					}
				} catch (UnauthorizedAccessException e) {
					CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
							messages.objectPermissionDialogTitle , messages.objectPermissionDialogMsg );
					return;
				} catch (Exception e) {
					LOGGER.warn("Unable to De-Activate site admin area start application relation model! ", e);
				}
			}

			*//**
			 * Method for De activate site AA project app rel.
			 *//*
			private void deActivateSiteAAProjectAppRel() {
				try {
					ProjectApplication projectApplication = (ProjectApplication) relationObj.getRefObject();
					String relId = relationObj.getRelId();
					String projectAppRelname = projectApplication.getName();
					String confirmDialogTitle = messages.changeStatusConfirmDialogTitle;
					String confirmDialogMsg = messages.deActivateRelchangeStatusConfirmDialogMsg + " \'"
							+ projectAppRelname + " \'" + " ?";
					boolean isConfirmed = CustomMessageDialog.openQuestion(adminTree.getControl().getShell(),
							confirmDialogTitle, confirmDialogMsg);
					if (isConfirmed) {
						String status = com.magna.xmbackend.vo.enums.Status.INACTIVE.name();
						IAdminTreeChild parent = ((RelationObj) relationObj).getContainerObj().getParent().getParent();
						//IAdminTreeChild containerObj = ((RelationObj) relationObj.getContainerObj().getParent()
								//.getParent().getParent()).getContainerObj();
						AdminAreaProjectAppRelController adminAreaProjectAppRelController = new AdminAreaProjectAppRelController(
								XMAdminUtil.getInstance().getAAForHeaders(parent));
						boolean isUpdated = adminAreaProjectAppRelController.updateAdminAreaProjAppRelStatus(relId,
								status);
						if (isUpdated) {
							relationObj.setActive(false);
							adminTree.refresh(relationObj);
						}
					}
				} catch (UnauthorizedAccessException e) {
					CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
							messages.objectPermissionDialogTitle , messages.objectPermissionDialogMsg );
					return;
				} catch (Exception e) {
					LOGGER.warn("Unable to De-Activate site admin area project application relation model! ", e);
				}
			}

			*//**
			 * Method for De activate site AA user app rel.
			 *//*
			private void deActivateSiteAAUserAppRel() {
				try {
					UserApplication userApplication = (UserApplication) relationObj.getRefObject();
					String relId = relationObj.getRelId();
					String userAppRelname = userApplication.getName();
					String confirmDialogTitle = messages.changeStatusConfirmDialogTitle;
					String confirmDialogMsg = messages.deActivateRelchangeStatusConfirmDialogMsg + " \'"
							+ userAppRelname + " \'" + " ?";
					boolean isConfirmed = CustomMessageDialog.openQuestion(adminTree.getControl().getShell(),
							confirmDialogTitle, confirmDialogMsg);
					if (isConfirmed) {
						String status = com.magna.xmbackend.vo.enums.Status.INACTIVE.name();
						AdminAreaUserAppRelController adminAreaUserAppRelController = new AdminAreaUserAppRelController(
								XMAdminUtil.getInstance().getAAForHeaders(relationObj.getContainerObj()));
						boolean isUpdated = adminAreaUserAppRelController.updateAdminAreaUserAppRelStatus(relId,
								status);
						if (isUpdated) {
							relationObj.setActive(false);
							adminTree.refresh(relationObj);
						}
					}
				} catch (UnauthorizedAccessException e) {
					CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
							messages.objectPermissionDialogTitle , messages.objectPermissionDialogMsg );
					return;
				} catch (Exception e) {
					LOGGER.warn("Unable to De-Activate site admin area user application relation model! ", e);
				}
			}

			*//**
			 * Method for De activate site AA project rel.
			 *//*
			private void deActivateSiteAAProjectRel() {
				try {
					Project project = (Project) relationObj.getRefObject();
					String relId = relationObj.getRelId();
					String adminAreaProjectRelname = project.getName();
					String confirmDialogTitle = messages.changeStatusConfirmDialogTitle;
					String confirmDialogMsg = messages.deActivateRelchangeStatusConfirmDialogMsg + " \'"
							+ adminAreaProjectRelname + " \'" + " ?";
					boolean isConfirmed = CustomMessageDialog.openQuestion(adminTree.getControl().getShell(),
							confirmDialogTitle, confirmDialogMsg);
					if (isConfirmed) {
						String status = com.magna.xmbackend.vo.enums.Status.INACTIVE.name();
						AdminAreaProjectRelController adminAreaProjectRelController = new AdminAreaProjectRelController(
								XMAdminUtil.getInstance().getAAForHeaders(relationObj));
						boolean isUpdated = adminAreaProjectRelController.updateAdminAreaProjectRelStatus(relId,
								status);
						if (isUpdated) {
							relationObj.setActive(false);
							adminTree.refresh(relationObj);
						}
					}
				} catch (UnauthorizedAccessException e) {
					CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
							messages.objectPermissionDialogTitle , messages.objectPermissionDialogMsg );
					return;
				} catch (Exception e) {
					LOGGER.warn("Unable to De-Activate site admin area project relation model! ", e);
				}
			}

			*//**
			 * Method for De activate site AA rel.
			 *//*
			private void deActivateSiteAARel() {
				try {
					AdministrationArea administrationArea = (AdministrationArea) relationObj.getRefObject();
					String relId = relationObj.getRelId();
					String adminAreaRelname = administrationArea.getName();
					String confirmDialogTitle = messages.changeStatusConfirmDialogTitle;
					String confirmDialogMsg = messages.deActivateRelchangeStatusConfirmDialogMsg + " \'"
							+ adminAreaRelname + " \'" + " ?";
					boolean isConfirmed = CustomMessageDialog.openQuestion(adminTree.getControl().getShell(),
							confirmDialogTitle, confirmDialogMsg);
					if (isConfirmed) {
						String status = com.magna.xmbackend.vo.enums.Status.INACTIVE.name();
						SiteAdminAreaRelController siteAdminAreaRelController = new SiteAdminAreaRelController();
						boolean isUpdated = siteAdminAreaRelController.updateSiteAdminAreaRelStatus(relId, status);
						if (isUpdated) {
							relationObj.setActive(false);
							adminTree.refresh(relationObj);
						}
					}
				} catch (UnauthorizedAccessException e) {
					CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
							messages.objectPermissionDialogTitle , messages.objectPermissionDialogMsg );
					return;
				} catch (Exception e) {
					LOGGER.warn("Unable to De-Activate site admin area relation model! ", e);
				}
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent paramSelectionEvent) {
			}
		});
	}

	*//**
	 * Method for Creates the menu item for I admin tree child.
	 *//*
	protected void createMenuItemForIAdminTreeChild() {
		Object selectedCLabelObjData = cLabel.getData();

		MenuItem activateDeactivateMenuItem = new MenuItem(menu, SWT.PUSH);
		MenuItem createAsMenuItem = new MenuItem(menu, SWT.PUSH);
		new MenuItem(menu, SWT.SEPARATOR);
		MenuItem changeMenuItem = new MenuItem(menu, SWT.PUSH);
		MenuItem deleteMenuItem = new MenuItem(menu, SWT.PUSH);
		new MenuItem(menu, SWT.SEPARATOR);
		MenuItem gotoMenuItem = new MenuItem(menu, SWT.PUSH);
		createAsMenuItem.setText(messages.rightSideCMlabelNodeCreateAs);
		changeMenuItem.setText(messages.rightSideCMlabelNodeChange);
		deleteMenuItem.setText(messages.rightSideCMlabelNodeDelete);
		gotoMenuItem.setText(messages.rightSideCMlabelNodeGoTo);
		cLabel.setMenu(menu);

		if (selectedCLabelObjData instanceof Site) {
			Site siteModel = (Site) selectedCLabelObjData;
			boolean active = siteModel.isActive();
			if (active) {
				activateDeactivateMenuItem.setText(messages.popupmenulabelnodeDeActive);
				addDeactivateListener(activateDeactivateMenuItem, selectedCLabelObjData, adminTree);
			} else {
				activateDeactivateMenuItem.setText(messages.popupmenulabelnodeActive);
				addActivateListener(activateDeactivateMenuItem, selectedCLabelObjData, adminTree);
			}
		} else if (selectedCLabelObjData instanceof AdministrationArea) {
			AdministrationArea administrationArea = (AdministrationArea) selectedCLabelObjData;
			boolean active = administrationArea.isActive();
			if (active) {
				activateDeactivateMenuItem.setText(messages.popupmenulabelnodeDeActive);
				addDeactivateListener(activateDeactivateMenuItem, selectedCLabelObjData, adminTree);
			} else {
				activateDeactivateMenuItem.setText(messages.popupmenulabelnodeActive);
				addActivateListener(activateDeactivateMenuItem, selectedCLabelObjData, adminTree);
			}
		} else if (selectedCLabelObjData instanceof Project) {
			Project project = (Project) selectedCLabelObjData;
			boolean active = project.isActive();
			if (active) {
				activateDeactivateMenuItem.setText(messages.popupmenulabelnodeDeActive);
				addDeactivateListener(activateDeactivateMenuItem, selectedCLabelObjData, adminTree);
			} else {
				activateDeactivateMenuItem.setText(messages.popupmenulabelnodeActive);
				addActivateListener(activateDeactivateMenuItem, selectedCLabelObjData, adminTree);
			}
		} else if (selectedCLabelObjData instanceof User) {
			User user = (User) selectedCLabelObjData;
			boolean active = user.isActive();
			if (active) {
				activateDeactivateMenuItem.setText(messages.popupmenulabelnodeDeActive);
				addDeactivateListener(activateDeactivateMenuItem, selectedCLabelObjData, adminTree);
			} else {
				activateDeactivateMenuItem.setText(messages.popupmenulabelnodeActive);
				addActivateListener(activateDeactivateMenuItem, selectedCLabelObjData, adminTree);
			}
			MenuItem[] items = menu.getItems();
			List<MenuItem> removeMenuItems = new ArrayList<>(2);
			for (MenuItem menuItem : items) {
				if (messages.rightSideCMlabelNodeCreateAs.equals(menuItem.getText())) {
					removeMenuItems.add(menuItem);
				}
			}
			for (MenuItem menuItem : removeMenuItems) {
				menuItem.dispose();
			}
		} else if (selectedCLabelObjData instanceof UserApplication) {
			UserApplication userApplication = (UserApplication) selectedCLabelObjData;
			boolean active = userApplication.isActive();
			if (active) {
				activateDeactivateMenuItem.setText(messages.popupmenulabelnodeDeActive);
				addDeactivateListener(activateDeactivateMenuItem, selectedCLabelObjData, adminTree);
			} else {
				activateDeactivateMenuItem.setText(messages.popupmenulabelnodeActive);
				addActivateListener(activateDeactivateMenuItem, selectedCLabelObjData, adminTree);
			}
		} else if (selectedCLabelObjData instanceof ProjectApplication) {
			ProjectApplication projectApplication = (ProjectApplication) selectedCLabelObjData;
			boolean active = projectApplication.isActive();
			if (active) {
				activateDeactivateMenuItem.setText(messages.popupmenulabelnodeDeActive);
				addDeactivateListener(activateDeactivateMenuItem, selectedCLabelObjData, adminTree);
			} else {
				activateDeactivateMenuItem.setText(messages.popupmenulabelnodeActive);
				addActivateListener(activateDeactivateMenuItem, selectedCLabelObjData, adminTree);
			}
		} else if (selectedCLabelObjData instanceof StartApplication) {
			StartApplication startApplication = (StartApplication) selectedCLabelObjData;
			boolean active = startApplication.isActive();
			if (active) {
				activateDeactivateMenuItem.setText(messages.popupmenulabelnodeDeActive);
				addDeactivateListener(activateDeactivateMenuItem, selectedCLabelObjData, adminTree);
			} else {
				activateDeactivateMenuItem.setText(messages.popupmenulabelnodeActive);
				addActivateListener(activateDeactivateMenuItem, selectedCLabelObjData, adminTree);
			}
		} else if (selectedCLabelObjData instanceof BaseApplication) {
			BaseApplication baseApplication = (BaseApplication) selectedCLabelObjData;
			boolean active = baseApplication.isActive();
			if (active) {
				activateDeactivateMenuItem.setText(messages.popupmenulabelnodeDeActive);
				addDeactivateListener(activateDeactivateMenuItem, selectedCLabelObjData, adminTree);
			} else {
				activateDeactivateMenuItem.setText(messages.popupmenulabelnodeActive);
				addActivateListener(activateDeactivateMenuItem, selectedCLabelObjData, adminTree);
			}
			
			 * if (!changeReturnVal) { changeMenuItem.setEnabled(false); } if
			 * (!deleteReturnValue) { deleteMenuItem.setEnabled(false); } if
			 * (!createAsReturnvalue) { createAsMenuItem.setEnabled(false); } if
			 * (!activateDeactivateReturnValue) {
			 * activateDeactivateMenuItem.setEnabled(false); }
			 
		} else if (selectedCLabelObjData instanceof Role) {
			activateDeactivateMenuItem.dispose();
			
			 * if (!changeReturnVal) { changeMenuItem.setEnabled(false); } if
			 * (!deleteReturnValue) { deleteMenuItem.setEnabled(false); } if
			 * (!createAsReturnvalue) { createAsMenuItem.setEnabled(false); }
			 
		} else if (selectedCLabelObjData instanceof UserGroupModel) {
			activateDeactivateMenuItem.dispose();
			
			 * if (!changeReturnVal) { changeMenuItem.setEnabled(false); } if
			 * (!deleteReturnValue) { deleteMenuItem.setEnabled(false); } if
			 * (!createAsReturnvalue) { createAsMenuItem.setEnabled(false); }
			 
		} else if (selectedCLabelObjData instanceof ProjectGroupModel) {
			activateDeactivateMenuItem.dispose();
		} else if (selectedCLabelObjData instanceof UserApplicationGroup) {
			activateDeactivateMenuItem.dispose();
		} else if (selectedCLabelObjData instanceof ProjectApplicationGroup) {
			activateDeactivateMenuItem.dispose();
		} else if (selectedCLabelObjData instanceof Directory) {
			activateDeactivateMenuItem.dispose();
		} else if (selectedCLabelObjData instanceof LiveMessage) {
			activateDeactivateMenuItem.dispose();
		}

		if (!createAsMenuItem.isDisposed()) {
			createAsMenuItem.addSelectionListener(new SelectionAdapter() {
				@Override
				public void widgetSelected(SelectionEvent event) {
					if (selectedCLabelObjData instanceof Site) {
						TreePath[] selectionPaths = adminTree.getStructuredSelection().getPaths();
						if (selectionPaths != null && selectionPaths.length > 0) {
							adminTree.setExpandedState(selectionPaths[0], true);
						}
						adminTree.setSelection(new StructuredSelection(selectedCLabelObjData));
						ParameterizedCommand cmd = commandService
								.createCommand(CommonConstants.COMMAND_ID.CREATEAS_SITE, null);
						if (handlerService.canExecute(cmd)) {
							handlerService.executeHandler(cmd);
						}
					} else if (selectedCLabelObjData instanceof AdministrationArea) {
						TreePath[] selectionPaths = adminTree.getStructuredSelection().getPaths();
						if (selectionPaths != null && selectionPaths.length > 0) {
							adminTree.setExpandedState(selectionPaths[0], true);
						}
						adminTree.setSelection(new StructuredSelection(selectedCLabelObjData));
						ParameterizedCommand cmd = commandService
								.createCommand(CommonConstants.COMMAND_ID.CREATEAS_ADMINAREA, null);
						if (handlerService.canExecute(cmd)) {
							handlerService.executeHandler(cmd);
						}
					} else if (selectedCLabelObjData instanceof Project) {
						TreePath[] selectionPaths = adminTree.getStructuredSelection().getPaths();
						if (selectionPaths != null && selectionPaths.length > 0) {
							adminTree.setExpandedState(selectionPaths[0], true);
						}
						adminTree.setSelection(new StructuredSelection(selectedCLabelObjData));
						ParameterizedCommand cmd = commandService
								.createCommand(CommonConstants.COMMAND_ID.CREATEAS_PROJECT, null);
						if (handlerService.canExecute(cmd)) {
							handlerService.executeHandler(cmd);
						}
					} else if (selectedCLabelObjData instanceof UserApplication) {
						TreePath[] selectionPaths = adminTree.getStructuredSelection().getPaths();
						if (selectionPaths != null && selectionPaths.length > 0) {
							adminTree.setExpandedState(selectionPaths[0], true);
						}
						adminTree.setSelection(new StructuredSelection(selectedCLabelObjData));
						ParameterizedCommand cmd = commandService
								.createCommand(CommonConstants.COMMAND_ID.CREATEAS_USERAPP, null);
						if (handlerService.canExecute(cmd)) {
							handlerService.executeHandler(cmd);
						}
					} else if (selectedCLabelObjData instanceof ProjectApplication) {
						TreePath[] selectionPaths = adminTree.getStructuredSelection().getPaths();
						if (selectionPaths != null && selectionPaths.length > 0) {
							adminTree.setExpandedState(selectionPaths[0], true);
						}
						adminTree.setSelection(new StructuredSelection(selectedCLabelObjData));
						ParameterizedCommand cmd = commandService
								.createCommand(CommonConstants.COMMAND_ID.CREATEAS_PROJECTAPP, null);
						if (handlerService.canExecute(cmd)) {
							handlerService.executeHandler(cmd);
						}
					} else if (selectedCLabelObjData instanceof StartApplication) {
						TreePath[] selectionPaths = adminTree.getStructuredSelection().getPaths();
						if (selectionPaths != null && selectionPaths.length > 0) {
							adminTree.setExpandedState(selectionPaths[0], true);
						}
						adminTree.setSelection(new StructuredSelection(selectedCLabelObjData));
						ParameterizedCommand cmd = commandService
								.createCommand(CommonConstants.COMMAND_ID.CREATEAS_STARTAPP, null);
						if (handlerService.canExecute(cmd)) {
							handlerService.executeHandler(cmd);
						}
					} else if (selectedCLabelObjData instanceof BaseApplication) {
						TreePath[] selectionPaths = adminTree.getStructuredSelection().getPaths();
						if (selectionPaths != null && selectionPaths.length > 0) {
							adminTree.setExpandedState(selectionPaths[0], true);
						}
						adminTree.setSelection(new StructuredSelection(selectedCLabelObjData));
						ParameterizedCommand cmd = commandService
								.createCommand(CommonConstants.COMMAND_ID.CREATEAS_BASEAPP, null);
						if (handlerService.canExecute(cmd)) {
							handlerService.executeHandler(cmd);
						}
					} else if (selectedCLabelObjData instanceof Role) {
						TreePath[] selectionPaths = adminTree.getStructuredSelection().getPaths();
						if (selectionPaths != null && selectionPaths.length > 0) {
							adminTree.setExpandedState(selectionPaths[0], true);
						}
						adminTree.setSelection(new StructuredSelection(selectedCLabelObjData));
						ParameterizedCommand cmd = commandService
								.createCommand(CommonConstants.COMMAND_ID.CREATEAS_ROLE, null);
						if (handlerService.canExecute(cmd)) {
							handlerService.executeHandler(cmd);
						}
					} else if (selectedCLabelObjData instanceof UserGroupModel) {
						TreePath[] selectionPaths = adminTree.getStructuredSelection().getPaths();
						if (selectionPaths != null && selectionPaths.length > 0) {
							adminTree.setExpandedState(selectionPaths[0], true);
						}
						adminTree.setSelection(new StructuredSelection(selectedCLabelObjData));
						ParameterizedCommand cmd = commandService
								.createCommand(CommonConstants.COMMAND_ID.CREATEAS_USER_GROUP, null);
						if (handlerService.canExecute(cmd)) {
							handlerService.executeHandler(cmd);
						}
					} else if (selectedCLabelObjData instanceof ProjectGroupModel) {
						TreePath[] selectionPaths = adminTree.getStructuredSelection().getPaths();
						if (selectionPaths != null && selectionPaths.length > 0) {
							adminTree.setExpandedState(selectionPaths[0], true);
						}
						adminTree.setSelection(new StructuredSelection(selectedCLabelObjData));
						ParameterizedCommand cmd = commandService
								.createCommand(CommonConstants.COMMAND_ID.CREATEAS_PROJECT_GROUP, null);
						if (handlerService.canExecute(cmd)) {
							handlerService.executeHandler(cmd);
						}
					} else if (selectedCLabelObjData instanceof UserApplicationGroup) {
						TreePath[] selectionPaths = adminTree.getStructuredSelection().getPaths();
						if (selectionPaths != null && selectionPaths.length > 0) {
							adminTree.setExpandedState(selectionPaths[0], true);
						}
						adminTree.setSelection(new StructuredSelection(selectedCLabelObjData));
						ParameterizedCommand cmd = commandService
								.createCommand(CommonConstants.COMMAND_ID.CREATEAS_USERAPP_GROUP, null);
						if (handlerService.canExecute(cmd)) {
							handlerService.executeHandler(cmd);
						}
					} else if (selectedCLabelObjData instanceof ProjectApplicationGroup) {
						TreePath[] selectionPaths = adminTree.getStructuredSelection().getPaths();
						if (selectionPaths != null && selectionPaths.length > 0) {
							adminTree.setExpandedState(selectionPaths[0], true);
						}
						adminTree.setSelection(new StructuredSelection(selectedCLabelObjData));
						ParameterizedCommand cmd = commandService
								.createCommand(CommonConstants.COMMAND_ID.CREATEAS_PROJECTAPP_GROUP, null);
						if (handlerService.canExecute(cmd)) {
							handlerService.executeHandler(cmd);
						}
					} else if (selectedCLabelObjData instanceof Directory) {
						TreePath[] selectionPaths = adminTree.getStructuredSelection().getPaths();
						if (selectionPaths != null && selectionPaths.length > 0) {
							adminTree.setExpandedState(selectionPaths[0], true);
						}
						adminTree.setSelection(new StructuredSelection(selectedCLabelObjData));
						ParameterizedCommand cmd = commandService
								.createCommand(CommonConstants.COMMAND_ID.CREATEAS_DIRECTORY, null);
						if (handlerService.canExecute(cmd)) {
							handlerService.executeHandler(cmd);
						}
					} else if (selectedCLabelObjData instanceof LiveMessage) {
						TreePath[] selectionPaths = adminTree.getStructuredSelection().getPaths();
						if (selectionPaths != null && selectionPaths.length > 0) {
							adminTree.setExpandedState(selectionPaths[0], true);
						}
						adminTree.setSelection(new StructuredSelection(selectedCLabelObjData));
						ParameterizedCommand cmd = commandService
								.createCommand(CommonConstants.COMMAND_ID.CREATEAS_LIVEMESSAGE, null);
						if (handlerService.canExecute(cmd)) {
							handlerService.executeHandler(cmd);
						}
					}
				}
			});
		}
		gotoMenuItem.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent event) {
				TreePath[] selectionPaths = adminTree.getStructuredSelection().getPaths();
				if (selectionPaths != null && selectionPaths.length > 0) {
					adminTree.setExpandedState(selectionPaths[0], true);

					if (selectedCLabelObjData instanceof User) {
						Users users = AdminTreeFactory.getInstance().getUsers();
						Map<String, IAdminTreeChild> userChildren = users.getUsersChildren();
						char startChar = (((User) selectedCLabelObjData).getName().toString().toLowerCase()
								.toCharArray()[0]);
						IAdminTreeChild iAdminTreeChild = userChildren.get(String.valueOf(startChar));
						adminTree.setExpandedState(iAdminTreeChild, true);
					}
				}
				adminTree.setSelection(new StructuredSelection(selectedCLabelObjData), true);
			}
		});

		deleteMenuItem.addSelectionListener(new SelectionAdapter() {

			@Override
			public void widgetSelected(SelectionEvent event) {
				if (selectedCLabelObjData instanceof Site) {
					ParameterizedCommand cmd = commandService.createCommand(CommonConstants.COMMAND_ID.DELETE_SITE,
							null);
					if (handlerService.canExecute(cmd)) {
						handlerService.executeHandler(cmd);
					}
				} else if (selectedCLabelObjData instanceof AdministrationArea) {
					ParameterizedCommand cmd = commandService.createCommand(CommonConstants.COMMAND_ID.DELETE_ADMINAREA,
							null);
					if (handlerService.canExecute(cmd)) {
						handlerService.executeHandler(cmd);
					}
				} else if (selectedCLabelObjData instanceof Project) {
					ParameterizedCommand cmd = commandService.createCommand(CommonConstants.COMMAND_ID.DELETE_PROJECT,
							null);
					if (handlerService.canExecute(cmd)) {
						handlerService.executeHandler(cmd);
					}
				} else if (selectedCLabelObjData instanceof User) {
					ParameterizedCommand cmd = commandService.createCommand(CommonConstants.COMMAND_ID.DELETE_USER,
							null);
					if (handlerService.canExecute(cmd)) {
						handlerService.executeHandler(cmd);
					}
				} else if (selectedCLabelObjData instanceof UserApplication) {
					ParameterizedCommand cmd = commandService.createCommand(CommonConstants.COMMAND_ID.DELETE_USERAPP,
							null);
					if (handlerService.canExecute(cmd)) {
						handlerService.executeHandler(cmd);
					}
				} else if (selectedCLabelObjData instanceof ProjectApplication) {
					ParameterizedCommand cmd = commandService
							.createCommand(CommonConstants.COMMAND_ID.DELETE_PROJECTAPP, null);
					if (handlerService.canExecute(cmd)) {
						handlerService.executeHandler(cmd);
					}
				} else if (selectedCLabelObjData instanceof StartApplication) {
					ParameterizedCommand cmd = commandService.createCommand(CommonConstants.COMMAND_ID.DELETE_STARTAPP,
							null);
					if (handlerService.canExecute(cmd)) {
						handlerService.executeHandler(cmd);
					}
				} else if (selectedCLabelObjData instanceof BaseApplication) {
					ParameterizedCommand cmd = commandService.createCommand(CommonConstants.COMMAND_ID.DELETE_BASEAPP,
							null);
					if (handlerService.canExecute(cmd)) {
						handlerService.executeHandler(cmd);
					}
				} else if (selectedCLabelObjData instanceof Role) {
					ParameterizedCommand cmd = commandService
							.createCommand("com.magna.xmsystem.xmadmin.ui.command.roledelete", null);
					if (handlerService.canExecute(cmd)) {
						handlerService.executeHandler(cmd);
					}
				} else if (selectedCLabelObjData instanceof UserGroupModel) {
					ParameterizedCommand cmd = commandService
							.createCommand(CommonConstants.COMMAND_ID.DELETE_USER_GROUP, null);
					if (handlerService.canExecute(cmd)) {
						handlerService.executeHandler(cmd);
					}
				} else if (selectedCLabelObjData instanceof ProjectGroupModel) {
					ParameterizedCommand cmd = commandService
							.createCommand(CommonConstants.COMMAND_ID.DELETE_PROJECT_GROUP, null);
					if (handlerService.canExecute(cmd)) {
						handlerService.executeHandler(cmd);
					}
				} else if (selectedCLabelObjData instanceof UserApplicationGroup) {
					ParameterizedCommand cmd = commandService
							.createCommand(CommonConstants.COMMAND_ID.DELETE_USERAPP_GROUP, null);
					if (handlerService.canExecute(cmd)) {
						handlerService.executeHandler(cmd);
					}
				} else if (selectedCLabelObjData instanceof ProjectApplicationGroup) {
					ParameterizedCommand cmd = commandService
							.createCommand(CommonConstants.COMMAND_ID.DELETE_PROJECTAPP_GROUP, null);
					if (handlerService.canExecute(cmd)) {
						handlerService.executeHandler(cmd);
					}
				} else if (selectedCLabelObjData instanceof Directory) {
					ParameterizedCommand cmd = commandService.createCommand(CommonConstants.COMMAND_ID.DELETE_DIRECTORY,
							null);
					if (handlerService.canExecute(cmd)) {
						handlerService.executeHandler(cmd);
					}
				} else if (selectedCLabelObjData instanceof LiveMessage) {
					ParameterizedCommand cmd = commandService
							.createCommand(CommonConstants.COMMAND_ID.DELETE_LIVEMESSAGE, null);
					if (handlerService.canExecute(cmd)) {
						handlerService.executeHandler(cmd);
					}
				}
			}
		});

		changeMenuItem.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent event) {
				if (selectedCLabelObjData instanceof Site) {
					TreePath[] selectionPaths = adminTree.getStructuredSelection().getPaths();
					if (selectionPaths != null && selectionPaths.length > 0) {
						adminTree.setExpandedState(selectionPaths[0], true);
					}
					adminTree.setSelection(new StructuredSelection(selectedCLabelObjData));
					ParameterizedCommand cmd = commandService.createCommand(CommonConstants.COMMAND_ID.CHANGE_SITE,
							null);
					if (handlerService.canExecute(cmd)) {
						handlerService.executeHandler(cmd);
					}
				} else if (selectedCLabelObjData instanceof AdministrationArea) {
					TreePath[] selectionPaths = adminTree.getStructuredSelection().getPaths();
					if (selectionPaths != null && selectionPaths.length > 0) {
						adminTree.setExpandedState(selectionPaths[0], true);
					}
					adminTree.setSelection(new StructuredSelection(selectedCLabelObjData));
					ParameterizedCommand cmd = commandService.createCommand(CommonConstants.COMMAND_ID.CHANGE_ADMINAREA,
							null);
					if (handlerService.canExecute(cmd)) {
						handlerService.executeHandler(cmd);
					}
				} else if (selectedCLabelObjData instanceof Project) {
					TreePath[] selectionPaths = adminTree.getStructuredSelection().getPaths();
					if (selectionPaths != null && selectionPaths.length > 0) {
						adminTree.setExpandedState(selectionPaths[0], true);
					}
					adminTree.setSelection(new StructuredSelection(selectedCLabelObjData));
					ParameterizedCommand cmd = commandService.createCommand(CommonConstants.COMMAND_ID.CHANGE_PROJECT,
							null);
					if (handlerService.canExecute(cmd)) {
						handlerService.executeHandler(cmd);
					}
				} else if (selectedCLabelObjData instanceof User) {
					TreePath[] selectionPaths = adminTree.getStructuredSelection().getPaths();
					if (selectionPaths != null && selectionPaths.length > 0) {
						adminTree.setExpandedState(selectionPaths[0], true);
						Users users = AdminTreeFactory.getInstance().getUsers();
						Map<String, IAdminTreeChild> userChildren = users.getUsersChildren();
						char startChar = (((User) selectedCLabelObjData).getName().toString().toLowerCase()
								.toCharArray()[0]);
						IAdminTreeChild iAdminTreeChild = userChildren.get(String.valueOf(startChar));
						adminTree.setExpandedState(iAdminTreeChild, true);
					}
					adminTree.setSelection(new StructuredSelection(selectedCLabelObjData));
					ParameterizedCommand cmd = commandService.createCommand(CommonConstants.COMMAND_ID.CHANGE_USER,
							null);
					if (handlerService.canExecute(cmd)) {
						handlerService.executeHandler(cmd);
					}
				} else if (selectedCLabelObjData instanceof UserApplication) {
					TreePath[] selectionPaths = adminTree.getStructuredSelection().getPaths();
					if (selectionPaths != null && selectionPaths.length > 0) {
						adminTree.setExpandedState(selectionPaths[0], true);
					}
					adminTree.setSelection(new StructuredSelection(selectedCLabelObjData));
					ParameterizedCommand cmd = commandService.createCommand(CommonConstants.COMMAND_ID.CHANGE_USERAPP,
							null);
					if (handlerService.canExecute(cmd)) {
						handlerService.executeHandler(cmd);
					}
				} else if (selectedCLabelObjData instanceof ProjectApplication) {
					TreePath[] selectionPaths = adminTree.getStructuredSelection().getPaths();
					if (selectionPaths != null && selectionPaths.length > 0) {
						adminTree.setExpandedState(selectionPaths[0], true);
					}
					adminTree.setSelection(new StructuredSelection(selectedCLabelObjData));
					ParameterizedCommand cmd = commandService
							.createCommand(CommonConstants.COMMAND_ID.CHANGE_PROJECTAPP, null);
					if (handlerService.canExecute(cmd)) {
						handlerService.executeHandler(cmd);
					}
				} else if (selectedCLabelObjData instanceof StartApplication) {
					TreePath[] selectionPaths = adminTree.getStructuredSelection().getPaths();
					if (selectionPaths != null && selectionPaths.length > 0) {
						adminTree.setExpandedState(selectionPaths[0], true);
					}
					adminTree.setSelection(new StructuredSelection(selectedCLabelObjData));
					ParameterizedCommand cmd = commandService.createCommand(CommonConstants.COMMAND_ID.CHANGE_STARTAPP,
							null);
					if (handlerService.canExecute(cmd)) {
						handlerService.executeHandler(cmd);
					}
				} else if (selectedCLabelObjData instanceof BaseApplication) {
					TreePath[] selectionPaths = adminTree.getStructuredSelection().getPaths();
					if (selectionPaths != null && selectionPaths.length > 0) {
						adminTree.setExpandedState(selectionPaths[0], true);
					}
					adminTree.setSelection(new StructuredSelection(selectedCLabelObjData));
					ParameterizedCommand cmd = commandService.createCommand(CommonConstants.COMMAND_ID.CHANGE_BASEAPP,
							null);
					if (handlerService.canExecute(cmd)) {
						handlerService.executeHandler(cmd);
					}
				} else if (selectedCLabelObjData instanceof Role) {
					TreePath[] selectionPaths = adminTree.getStructuredSelection().getPaths();
					if (selectionPaths != null && selectionPaths.length > 0) {
						adminTree.setExpandedState(selectionPaths[0], true);
					}
					adminTree.setSelection(new StructuredSelection(selectedCLabelObjData));
					ParameterizedCommand cmd = commandService.createCommand(CommonConstants.COMMAND_ID.CHANGE_ROLE,
							null);
					if (handlerService.canExecute(cmd)) {
						handlerService.executeHandler(cmd);
					}
				} else if (selectedCLabelObjData instanceof UserGroupModel) {
					TreePath[] selectionPaths = adminTree.getStructuredSelection().getPaths();
					if (selectionPaths != null && selectionPaths.length > 0) {
						adminTree.setExpandedState(selectionPaths[0], true);
					}
					adminTree.setSelection(new StructuredSelection(selectedCLabelObjData));
					ParameterizedCommand cmd = commandService
							.createCommand(CommonConstants.COMMAND_ID.CHANGE_USER_GROUP, null);
					if (handlerService.canExecute(cmd)) {
						handlerService.executeHandler(cmd);
					}
				} else if (selectedCLabelObjData instanceof ProjectGroupModel) {
					TreePath[] selectionPaths = adminTree.getStructuredSelection().getPaths();
					if (selectionPaths != null && selectionPaths.length > 0) {
						adminTree.setExpandedState(selectionPaths[0], true);
					}
					adminTree.setSelection(new StructuredSelection(selectedCLabelObjData));
					ParameterizedCommand cmd = commandService
							.createCommand(CommonConstants.COMMAND_ID.CHANGE_PROJECT_GROUP, null);
					if (handlerService.canExecute(cmd)) {
						handlerService.executeHandler(cmd);
					}
				} else if (selectedCLabelObjData instanceof UserApplicationGroup) {
					TreePath[] selectionPaths = adminTree.getStructuredSelection().getPaths();
					if (selectionPaths != null && selectionPaths.length > 0) {
						adminTree.setExpandedState(selectionPaths[0], true);
					}
					adminTree.setSelection(new StructuredSelection(selectedCLabelObjData));
					ParameterizedCommand cmd = commandService
							.createCommand(CommonConstants.COMMAND_ID.CHANGE_USERAPP_GROUP, null);
					if (handlerService.canExecute(cmd)) {
						handlerService.executeHandler(cmd);
					}
				} else if (selectedCLabelObjData instanceof ProjectApplicationGroup) {
					TreePath[] selectionPaths = adminTree.getStructuredSelection().getPaths();
					if (selectionPaths != null && selectionPaths.length > 0) {
						adminTree.setExpandedState(selectionPaths[0], true);
					}
					adminTree.setSelection(new StructuredSelection(selectedCLabelObjData));
					ParameterizedCommand cmd = commandService
							.createCommand(CommonConstants.COMMAND_ID.CHANGE_PROJECTAPP_GROUP, null);
					if (handlerService.canExecute(cmd)) {
						handlerService.executeHandler(cmd);
					}
				} else if (selectedCLabelObjData instanceof Directory) {
					TreePath[] selectionPaths = adminTree.getStructuredSelection().getPaths();
					if (selectionPaths != null && selectionPaths.length > 0) {
						adminTree.setExpandedState(selectionPaths[0], true);
					}
					adminTree.setSelection(new StructuredSelection(selectedCLabelObjData));
					ParameterizedCommand cmd = commandService.createCommand(CommonConstants.COMMAND_ID.CHANGE_DIRECTORY,
							null);
					if (handlerService.canExecute(cmd)) {
						handlerService.executeHandler(cmd);
					}
				} else if (selectedCLabelObjData instanceof LiveMessage) {
					TreePath[] selectionPaths = adminTree.getStructuredSelection().getPaths();
					if (selectionPaths != null && selectionPaths.length > 0) {
						adminTree.setExpandedState(selectionPaths[0], true);
					}
					adminTree.setSelection(new StructuredSelection(selectedCLabelObjData));
					ParameterizedCommand cmd = commandService
							.createCommand(CommonConstants.COMMAND_ID.CHANGE_LIVEMESSAGE, null);
					if (handlerService.canExecute(cmd)) {
						handlerService.executeHandler(cmd);
					}
				}
			}
		});
	}

	*//**
	 * Method for Adds the deactivate listener.
	 *
	 * @param deactivateMenuItem
	 *            {@link MenuItem}
	 * @param selectedCLabelObjData
	 *            {@link Object}
	 * @param adminTree
	 *            {@link AdminTreeviewer}
	 *//*
	private void addDeactivateListener(MenuItem deactivateMenuItem, Object selectedCLabelObjData,
			AdminTreeviewer adminTree) {
		deactivateMenuItem.addSelectionListener(new SelectionAdapter() {

			@Override
			public void widgetSelected(SelectionEvent event) {
				if (selectedCLabelObjData instanceof Site) {
					deactivateSite(selectedCLabelObjData);
				} else if (selectedCLabelObjData instanceof AdministrationArea) {
					deactivateAdminArea(selectedCLabelObjData);
				} else if (selectedCLabelObjData instanceof Project) {
					deactivateProject(selectedCLabelObjData);
				} else if (selectedCLabelObjData instanceof User) {
					deactivateUser(selectedCLabelObjData);
				} else if (selectedCLabelObjData instanceof UserApplication) {
					deactivateUserApplication(selectedCLabelObjData);
				} else if (selectedCLabelObjData instanceof ProjectApplication) {
					deactivateProjectApplication(selectedCLabelObjData);
				} else if (selectedCLabelObjData instanceof StartApplication) {
					deactivateStartApplication(selectedCLabelObjData);
				} else if (selectedCLabelObjData instanceof BaseApplication) {
					deactivateBaseApplication(selectedCLabelObjData);
				}
				displayAllObjectPage.setLabelAndIcon(cLabel);
			}

			*//**
			 * Deactivate user.
			 *
			 * @param selectedCLabelObjData
			 *            the selected C label obj data
			 *//*
			private void deactivateUser(Object selectedCLabelObjData) {
				try {
					User user = (User) selectedCLabelObjData;
					String name = user.getName();
					String confirmDialogTitle = messages.changeStatusConfirmDialogTitle;
					String confirmDialogMsg = messages.deActivatechangeStatusConfirmDialogMsg + " \'" + name + " \'" + " ?";
					boolean isConfirmed = CustomMessageDialog.openQuestion(adminTree.getControl().getShell(),
							confirmDialogTitle, confirmDialogMsg);
					User userModel = (User) selectedCLabelObjData;
					if (isConfirmed) {
						String id = userModel.getUserId();
						String status = com.magna.xmbackend.vo.enums.Status.INACTIVE.name();
						UserController userCntr = new UserController();
						boolean isUpdated = userCntr.updateUserStatus(id, status);
						if (isUpdated) {
							userModel.setActive(false);
							adminTree.refresh(userModel);
						}
					}
					adminTree.refresh(userModel);
				} catch (UnauthorizedAccessException e) {
					CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
							messages.objectPermissionDialogTitle , messages.objectPermissionDialogMsg );
					return;
				} catch (Exception e) {
					LOGGER.warn("Unable to De-Activate user model! ", e);
				}
			}

			*//**
			 * Deactivate base application.
			 *
			 * @param selectedCLabelObjData
			 *            the selected C label obj data
			 *//*
			private void deactivateBaseApplication(Object selectedCLabelObjData) {
				try {
					BaseApplication baseApplication = (BaseApplication) selectedCLabelObjData;
					String name = baseApplication.getName();
					String confirmDialogTitle = messages.changeStatusConfirmDialogTitle;
					String confirmDialogMsg = messages.deActivatechangeStatusConfirmDialogMsg + " \'" + name + " \'"
							+ " ?";
					boolean isConfirmed = CustomMessageDialog.openQuestion(adminTree.getControl().getShell(),
							confirmDialogTitle, confirmDialogMsg);
					if (isConfirmed) {
						String id = baseApplication.getBaseApplicationId();
						String status = com.magna.xmbackend.vo.enums.Status.INACTIVE.name();
						BaseAppController baseAppController = new BaseAppController();
						boolean isUpdated = baseAppController.updateBaseAppStatus(id, status);
						if (isUpdated) {
							baseApplication.setActive(false);
							adminTree.refresh(baseApplication);
						}
					}
				} catch (UnauthorizedAccessException e) {
					CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
							messages.objectPermissionDialogTitle , messages.objectPermissionDialogMsg );
					return;
				} catch (Exception e) {
					LOGGER.warn("Unable to De-Activate base application model! ", e);
				}
			}

			*//**
			 * Deactivate start application.
			 *
			 * @param selectedCLabelObjData
			 *            the selected C label obj data
			 *//*
			private void deactivateStartApplication(Object selectedCLabelObjData) {
				try {
					StartApplication startAppModel = (StartApplication) selectedCLabelObjData;
					String name = startAppModel.getName();
					String confirmDialogTitle = messages.changeStatusConfirmDialogTitle;
					String confirmDialogMsg = messages.deActivatechangeStatusConfirmDialogMsg + " \'" + name + " \'"
							+ " ?";
					boolean isConfirmed = CustomMessageDialog.openQuestion(adminTree.getControl().getShell(),
							confirmDialogTitle, confirmDialogMsg);
					if (isConfirmed) {
						String id = startAppModel.getStartPrgmApplicationId();
						String status = com.magna.xmbackend.vo.enums.Status.INACTIVE.name();
						StartAppController userAppCntr = new StartAppController();
						boolean isUpdated = userAppCntr.updateStartAppStatus(id, status);
						if (isUpdated) {
							startAppModel.setActive(false);
							adminTree.refresh(startAppModel);
						}
					}
				} catch (UnauthorizedAccessException e) {
					CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
							messages.objectPermissionDialogTitle , messages.objectPermissionDialogMsg );
					return;
				} catch (Exception e) {
					LOGGER.warn("Unable to De-Activate Start Application model! ", e);
				}
			}

			*//**
			 * Deactivate project application.
			 *
			 * @param selectedCLabelObjData
			 *            the selected C label obj data
			 *//*
			private void deactivateProjectApplication(Object selectedCLabelObjData) {
				try {
					ProjectApplication projectApplication = (ProjectApplication) selectedCLabelObjData;
					String name = projectApplication.getName();
					String confirmDialogTitle = messages.changeStatusConfirmDialogTitle;
					String confirmDialogMsg = messages.deActivatechangeStatusConfirmDialogMsg + " \'" + name + " \'"
							+ " ?";
					boolean isConfirmed = CustomMessageDialog.openQuestion(adminTree.getControl().getShell(),
							confirmDialogTitle, confirmDialogMsg);
					if (isConfirmed) {
						String id = projectApplication.getProjectApplicationId();
						String status = com.magna.xmbackend.vo.enums.Status.INACTIVE.name();
						ProjectAppController projectAppController = new ProjectAppController();
						boolean isUpdated = projectAppController.updateProjectAppStatus(id, status);
						if (isUpdated) {
							projectApplication.setActive(false);
							adminTree.refresh(projectApplication);
						}
					}
				} catch (UnauthorizedAccessException e) {
					CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
							messages.objectPermissionDialogTitle , messages.objectPermissionDialogMsg );
					return;
				} catch (Exception e) {
					LOGGER.warn("Unable to De-Activate project application model! ", e);
				}
			}

			*//**
			 * Deactivate user application.
			 *
			 * @param selectedCLabelObjData
			 *            the selected C label obj data
			 *//*
			private void deactivateUserApplication(Object selectedCLabelObjData) {
				try {
					UserApplication userAppModel = (UserApplication) selectedCLabelObjData;
					String name = userAppModel.getName();
					String confirmDialogTitle = messages.changeStatusConfirmDialogTitle;
					String confirmDialogMsg = messages.deActivatechangeStatusConfirmDialogMsg + " \'" + name + " \'"
							+ " ?";
					boolean isConfirmed = CustomMessageDialog.openQuestion(adminTree.getControl().getShell(),
							confirmDialogTitle, confirmDialogMsg);
					if (isConfirmed) {
						String id = userAppModel.getUserApplicationId();
						String status = com.magna.xmbackend.vo.enums.Status.INACTIVE.name();
						UserAppController userAppCntr = new UserAppController();
						boolean isUpdated = userAppCntr.updateUserAppStatus(id, status);
						if (isUpdated) {
							userAppModel.setActive(false);
							adminTree.refresh(userAppModel);
						}
					}
				} catch (UnauthorizedAccessException e) {
					CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
							messages.objectPermissionDialogTitle , messages.objectPermissionDialogMsg );
					return;
				} catch (Exception e) {
					LOGGER.warn("Unable to De-Activate user application model! ", e);
				}
			}

			*//**
			 * Deactivate project.
			 *
			 * @param selectedCLabelObjData
			 *            the selected C label obj data
			 *//*
			private void deactivateProject(Object selectedCLabelObjData) {
				try {
					Project projectModel = (Project) selectedCLabelObjData;
					String name = projectModel.getName();
					String confirmDialogTitle = messages.changeStatusConfirmDialogTitle;
					String confirmDialogMsg = messages.deActivatechangeStatusConfirmDialogMsg + " \'" + name + " \'"
							+ " ?";
					boolean isConfirmed = CustomMessageDialog.openQuestion(adminTree.getControl().getShell(),
							confirmDialogTitle, confirmDialogMsg);
					if (isConfirmed) {
						String id = projectModel.getProjectId();
						String status = com.magna.xmbackend.vo.enums.Status.INACTIVE.name();
						ProjectController projectCntr = new ProjectController();
						boolean isUpdated = projectCntr.updateProjectStatus(id, status);
						if (isUpdated) {
							projectModel.setActive(false);
							adminTree.refresh(projectModel);
						}
					}
				} catch (UnauthorizedAccessException e) {
					CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
							messages.objectPermissionDialogTitle , messages.objectPermissionDialogMsg );
					return;
				} catch (Exception e) {
					LOGGER.warn("Unable to De-Activate project model! ", e);
				}
			}

			*//**
			 * Deactivate admin area.
			 *
			 * @param selectedCLabelObjData
			 *            the selected C label obj data
			 *//*
			private void deactivateAdminArea(Object selectedCLabelObjData) {
				try {
					AdministrationArea adminAreaModel = (AdministrationArea) selectedCLabelObjData;
					String name = adminAreaModel.getName();
					String confirmDialogTitle = messages.changeStatusConfirmDialogTitle;
					String confirmDialogMsg = messages.deActivatechangeStatusConfirmDialogMsg + " \'" + name + " \'"
							+ " ?";
					boolean isConfirmed = CustomMessageDialog.openQuestion(adminTree.getControl().getShell(),
							confirmDialogTitle, confirmDialogMsg);
					if (isConfirmed) {
						String id = adminAreaModel.getAdministrationAreaId();
						String status = com.magna.xmbackend.vo.enums.Status.INACTIVE.name();
						AdminAreaController adminAreaCntr = new AdminAreaController();
						boolean isUpdated = adminAreaCntr.updateAdminAreaStatus(id, status);
						if (isUpdated) {
							adminAreaModel.setActive(false);
							adminTree.refresh(adminAreaModel);
						}
					}
				} catch (UnauthorizedAccessException e) {
					CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
							messages.objectPermissionDialogTitle , messages.objectPermissionDialogMsg );
					return;
				} catch (Exception e) {
					LOGGER.warn("Unable to De-Activate admin area model! ", e);
				}
			}

			*//**
			 * Deactivate site.
			 *
			 * @param selectedCLabelObjData
			 *            the selected C label obj data
			 *//*
			private void deactivateSite(Object selectedCLabelObjData) {
				try {
					Site siteModel = (Site) selectedCLabelObjData;
					String name = siteModel.getName();
					String confirmDialogTitle = messages.changeStatusConfirmDialogTitle;
					String confirmDialogMsg = messages.deActivatechangeStatusConfirmDialogMsg + " \'" + name + " \'"
							+ " ?";
					boolean isConfirmed = CustomMessageDialog.openQuestion(adminTree.getControl().getShell(),
							confirmDialogTitle, confirmDialogMsg);
					if (isConfirmed) {
						String id = siteModel.getSiteId();
						String status = com.magna.xmbackend.vo.enums.Status.INACTIVE.name();
						SiteController siteCntr = new SiteController();
						boolean isUpdated = siteCntr.updateSiteStatus(id, status);
						if (isUpdated) {
							siteModel.setActive(false);
							adminTree.refresh(siteModel);
						}
					}
				} catch (UnauthorizedAccessException e) {
					CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
							messages.objectPermissionDialogTitle , messages.objectPermissionDialogMsg );
					return;
				} catch (Exception e) {
					LOGGER.warn("Unable to De-Activate site model! ", e);
				}
			}
		});

	}

	*//**
	 * Method for Adds the activate listener.
	 *
	 * @param activateMenuItem
	 *            {@link MenuItem}
	 * @param selectedCLabelObjData
	 *            {@link Object}
	 * @param adminTree
	 *            {@link AdminTreeviewer}
	 *//*
	private void addActivateListener(MenuItem activateMenuItem, Object selectedCLabelObjData,
			AdminTreeviewer adminTree) {
		activateMenuItem.addSelectionListener(new SelectionAdapter() {

			@Override
			public void widgetSelected(SelectionEvent arg0) {
				if (selectedCLabelObjData instanceof Site) {
					activateSite(selectedCLabelObjData);
				} else if (selectedCLabelObjData instanceof AdministrationArea) {
					activateAdminArea(selectedCLabelObjData);
				} else if (selectedCLabelObjData instanceof Project) {
					activateProject(selectedCLabelObjData);
				} else if (selectedCLabelObjData instanceof User) {
					activateUser(selectedCLabelObjData);
				} else if (selectedCLabelObjData instanceof UserApplication) {
					activateUserApplication(selectedCLabelObjData);
				} else if (selectedCLabelObjData instanceof ProjectApplication) {
					activateProjectApplication(selectedCLabelObjData);
				} else if (selectedCLabelObjData instanceof StartApplication) {
					activateStartApplication(selectedCLabelObjData);
				} else if (selectedCLabelObjData instanceof BaseApplication) {
					activateBaseApplication(selectedCLabelObjData);
				}
				displayAllObjectPage.setLabelAndIcon(cLabel);
			}

			*//**
			 * Activate user.
			 *
			 * @param selectedCLabelObjData
			 *            the selected C label obj data
			 *//*
			private void activateUser(Object selectedCLabelObjData) {
				try {
					User user = (User) selectedCLabelObjData;
					String name = user.getName();
					String confirmDialogTitle = messages.changeStatusConfirmDialogTitle;
					String confirmDialogMsg = messages.activatechangeStatusConfirmDialogMsg + " \'" + name + " \'" + " ?";
					boolean isConfirmed = CustomMessageDialog.openQuestion(adminTree.getControl().getShell(),
							confirmDialogTitle, confirmDialogMsg);
					User userModel = (User) selectedCLabelObjData;
					if (isConfirmed) {
						String id = userModel.getUserId();
						String status = com.magna.xmbackend.vo.enums.Status.ACTIVE.name();
						UserController userCntr = new UserController();
						boolean isUpdated;
							isUpdated = userCntr.updateUserStatus(id, status);
							if (isUpdated) {
								userModel.setActive(true);
								adminTree.refresh(userModel);
							}
					}
					adminTree.refresh(userModel);
				} catch (UnauthorizedAccessException e) {
					CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
							messages.objectPermissionDialogTitle , messages.objectPermissionDialogMsg );
					return;
				} catch (Exception e) {
					LOGGER.warn("Unable to De-Activate user model! ", e);
				}
			}

			*//**
			 * Activate base application.
			 *
			 * @param selectedCLabelObjData
			 *            the selected C label obj data
			 *//*
			private void activateBaseApplication(Object selectedCLabelObjData) {
				try {
					BaseApplication baseApplication = (BaseApplication) selectedCLabelObjData;
					String name = baseApplication.getName();
					String confirmDialogTitle = messages.changeStatusConfirmDialogTitle;
					String confirmDialogMsg = messages.activatechangeStatusConfirmDialogMsg + " \'" + name + " \'"
							+ " ?";
					boolean isConfirmed = CustomMessageDialog.openQuestion(adminTree.getControl().getShell(),
							confirmDialogTitle, confirmDialogMsg);
					if (isConfirmed) {
						String id = baseApplication.getBaseApplicationId();
						String status = com.magna.xmbackend.vo.enums.Status.ACTIVE.name();
						BaseAppController baseAppController = new BaseAppController();
							boolean isUpdated = baseAppController.updateBaseAppStatus(id, status);
							if (isUpdated) {
								baseApplication.setActive(true);
								adminTree.refresh(baseApplication);
							}
					}
				} catch (UnauthorizedAccessException e) {
					CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
							messages.objectPermissionDialogTitle , messages.objectPermissionDialogMsg );
					return;
				} catch (Exception e) {
					LOGGER.warn("Unable to activate base application model! ", e);
				}
			}

			*//**
			 * Activate start application.
			 *
			 * @param selectedCLabelObjData
			 *            the selected C label obj data
			 *//*
			private void activateStartApplication(Object selectedCLabelObjData) {
				try {
					StartApplication startAppModel = (StartApplication) selectedCLabelObjData;
					String name = startAppModel.getName();
					String confirmDialogTitle = messages.changeStatusConfirmDialogTitle;
					String confirmDialogMsg = messages.activatechangeStatusConfirmDialogMsg + " \'" + name + " \'"
							+ " ?";
					boolean isConfirmed = CustomMessageDialog.openQuestion(adminTree.getControl().getShell(),
							confirmDialogTitle, confirmDialogMsg);
					if (isConfirmed) {
						String id = startAppModel.getStartPrgmApplicationId();
						String status = com.magna.xmbackend.vo.enums.Status.ACTIVE.name();
						StartAppController userAppCntr = new StartAppController();
						boolean isUpdated = userAppCntr.updateStartAppStatus(id, status);
						if (isUpdated) {
							startAppModel.setActive(true);
							adminTree.refresh(startAppModel);
						}
					}
				} catch (UnauthorizedAccessException e) {
					CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
							messages.objectPermissionDialogTitle , messages.objectPermissionDialogMsg );
					return;
				} catch (Exception e) {
					LOGGER.warn("Unable to activate Start Application model! ", e);
				}

			}

			*//**
			 * Activate project application.
			 *
			 * @param selectedCLabelObjData
			 *            the selected C label obj data
			 *//*
			private void activateProjectApplication(Object selectedCLabelObjData) {
				try {
					ProjectApplication projectApplication = (ProjectApplication) selectedCLabelObjData;
					String name = projectApplication.getName();
					String confirmDialogTitle = messages.changeStatusConfirmDialogTitle;
					String confirmDialogMsg = messages.activatechangeStatusConfirmDialogMsg + " \'" + name + " \'"
							+ " ?";
					boolean isConfirmed = CustomMessageDialog.openQuestion(adminTree.getControl().getShell(),
							confirmDialogTitle, confirmDialogMsg);
					if (isConfirmed) {
						String id = projectApplication.getProjectApplicationId();
						String status = com.magna.xmbackend.vo.enums.Status.ACTIVE.name();
						ProjectAppController projectAppController = new ProjectAppController();
						boolean isUpdated = projectAppController.updateProjectAppStatus(id, status);
						if (isUpdated) {
							projectApplication.setActive(true);
							adminTree.refresh(projectApplication);
						}
					}
				} catch (UnauthorizedAccessException e) {
					CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
							messages.objectPermissionDialogTitle , messages.objectPermissionDialogMsg );
					return;
				} catch (Exception e) {
					LOGGER.warn("Unable to activate project application model! ", e);
				}
			}

			*//**
			 * Activate user application.
			 *
			 * @param selectedCLabelObjData
			 *            the selected C label obj data
			 *//*
			private void activateUserApplication(Object selectedCLabelObjData) {
				try {
					UserApplication userAppModel = (UserApplication) selectedCLabelObjData;
					String name = userAppModel.getName();
					String confirmDialogTitle = messages.changeStatusConfirmDialogTitle;
					String confirmDialogMsg = messages.activatechangeStatusConfirmDialogMsg + " \'" + name + " \'"
							+ " ?";
					boolean isConfirmed = CustomMessageDialog.openQuestion(adminTree.getControl().getShell(),
							confirmDialogTitle, confirmDialogMsg);
					if (isConfirmed) {
						String id = userAppModel.getUserApplicationId();
						String status = com.magna.xmbackend.vo.enums.Status.ACTIVE.name();
						UserAppController userAppCntr = new UserAppController();
						boolean isUpdated = userAppCntr.updateUserAppStatus(id, status);
						if (isUpdated) {
							userAppModel.setActive(true);
							adminTree.refresh(userAppModel);
						}
					}
				} catch (UnauthorizedAccessException e) {
					CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
							messages.objectPermissionDialogTitle , messages.objectPermissionDialogMsg );
					return;
				} catch (Exception e) {
					LOGGER.warn("Unable to activate user application model! ", e);
				}
			}

			*//**
			 * Activate project.
			 *
			 * @param selectedCLabelObjData
			 *            the selected C label obj data
			 *//*
			private void activateProject(Object selectedCLabelObjData) {
				try {
					Project projectModel = (Project) selectedCLabelObjData;
					String name = projectModel.getName();
					String confirmDialogTitle = messages.changeStatusConfirmDialogTitle;
					String confirmDialogMsg = messages.activatechangeStatusConfirmDialogMsg + " \'" + name + " \'"
							+ " ?";
					boolean isConfirmed = CustomMessageDialog.openQuestion(adminTree.getControl().getShell(),
							confirmDialogTitle, confirmDialogMsg);
					if (isConfirmed) {
						String id = projectModel.getProjectId();
						String status = com.magna.xmbackend.vo.enums.Status.ACTIVE.name();
						ProjectController projectCntr = new ProjectController();
						boolean isUpdated = projectCntr.updateProjectStatus(id, status);
						if (isUpdated) {
							projectModel.setActive(true);
							adminTree.refresh(projectModel);
						}
					}
				} catch (UnauthorizedAccessException e) {
					CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
							messages.objectPermissionDialogTitle , messages.objectPermissionDialogMsg );
					return;
				} catch (Exception e) {
					LOGGER.warn("Unable to activate project model! ", e);
				}
			}

			*//**
			 * Activate admin area.
			 *
			 * @param selectedCLabelObjData
			 *            the selected C label obj data
			 *//*
			private void activateAdminArea(Object selectedCLabelObjData) {
				try {
					AdministrationArea adminAreaModel = (AdministrationArea) selectedCLabelObjData;
					String name = adminAreaModel.getName();
					String confirmDialogTitle = messages.changeStatusConfirmDialogTitle;
					String confirmDialogMsg = messages.activatechangeStatusConfirmDialogMsg + " \'" + name + " \'"
							+ " ?";
					boolean isConfirmed = CustomMessageDialog.openQuestion(adminTree.getControl().getShell(),
							confirmDialogTitle, confirmDialogMsg);
					if (isConfirmed) {
						String id = adminAreaModel.getAdministrationAreaId();
						String status = com.magna.xmbackend.vo.enums.Status.ACTIVE.name();
						AdminAreaController adminAreaCntr = new AdminAreaController();
						boolean isUpdated = adminAreaCntr.updateAdminAreaStatus(id, status);
						if (isUpdated) {
							adminAreaModel.setActive(true);
							adminTree.refresh(adminAreaModel);
						}
					}
				} catch (UnauthorizedAccessException e) {
					CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
							messages.objectPermissionDialogTitle , messages.objectPermissionDialogMsg );
					return;
				} catch (Exception e) {
					LOGGER.warn("Unable to activate admin area model! ", e);
				}
			}

			*//**
			 * Activate site.
			 *
			 * @param selectedCLabelObjData
			 *            the selected C label obj data
			 *//*
			private void activateSite(Object selectedCLabelObjData) {
				try {
					Site siteModel = (Site) selectedCLabelObjData;
					String name = siteModel.getName();
					String confirmDialogTitle = messages.changeStatusConfirmDialogTitle;
					String confirmDialogMsg = messages.activatechangeStatusConfirmDialogMsg + " \'" + name + " \'"
							+ " ?";
					boolean isConfirmed = CustomMessageDialog.openQuestion(adminTree.getControl().getShell(),
							confirmDialogTitle, confirmDialogMsg);
					if (isConfirmed) {
						String id = siteModel.getSiteId();
						String status = com.magna.xmbackend.vo.enums.Status.ACTIVE.name();
						SiteController siteCntr = new SiteController();
						boolean isUpdated = siteCntr.updateSiteStatus(id, status);
						if (isUpdated) {
							siteModel.setActive(true);
							adminTree.refresh(siteModel);
						}
					}
				} catch (UnauthorizedAccessException e) {
					CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
							messages.objectPermissionDialogTitle , messages.objectPermissionDialogMsg );
					return;
				} catch (Exception e) {
					LOGGER.warn("Unable to activate site model! ", e);
				}
			}

		});
	}

}
*/