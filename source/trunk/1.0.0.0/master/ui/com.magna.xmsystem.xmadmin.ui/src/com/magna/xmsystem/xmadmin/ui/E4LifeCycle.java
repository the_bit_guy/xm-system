package com.magna.xmsystem.xmadmin.ui;

import java.io.File;
import java.io.IOException;

import javax.inject.Inject;
import javax.inject.Named;

import org.eclipse.core.runtime.Platform;
import org.eclipse.e4.core.contexts.ContextInjectionFactory;
import org.eclipse.e4.core.contexts.IEclipseContext;
import org.eclipse.e4.core.di.annotations.Optional;
import org.eclipse.e4.core.services.events.IEventBroker;
import org.eclipse.e4.core.services.nls.Translation;
import org.eclipse.e4.ui.di.UIEventTopic;
import org.eclipse.e4.ui.model.application.MApplication;
import org.eclipse.e4.ui.model.application.ui.basic.MPart;
import org.eclipse.e4.ui.model.application.ui.basic.MWindow;
import org.eclipse.e4.ui.services.IServiceConstants;
import org.eclipse.e4.ui.workbench.UIEvents;
import org.eclipse.e4.ui.workbench.lifecycle.PostContextCreate;
import org.eclipse.e4.ui.workbench.lifecycle.PreSave;
import org.eclipse.e4.ui.workbench.lifecycle.ProcessAdditions;
import org.eclipse.e4.ui.workbench.lifecycle.ProcessRemovals;
import org.eclipse.e4.ui.workbench.modeling.EModelService;
import org.eclipse.e4.ui.workbench.modeling.IWindowCloseHandler;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Monitor;
import org.eclipse.swt.widgets.Shell;
import org.osgi.service.event.Event;
import org.osgi.service.event.EventHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.ResourceAccessException;

import com.magna.xmbackend.vo.enums.Application;
import com.magna.xmbackend.vo.user.AuthResponse;
import com.magna.xmsystem.dependencies.customExceptions.UnauthorizedAccessException;
import com.magna.xmsystem.dependencies.utils.APPLICATION;
import com.magna.xmsystem.dependencies.utils.OSValidator;
import com.magna.xmsystem.dependencies.utils.XMSystemUtil;
import com.magna.xmsystem.dependencies.utils.XmSystemEnvProcess;
import com.magna.xmsystem.ui.controls.widgets.CustomMessageDialog;
import com.magna.xmsystem.xmadmin.message.Message;
import com.magna.xmsystem.xmadmin.restclient.validation.AuthController;
import com.magna.xmsystem.xmadmin.ui.logger.HistoryTableUtil;
import com.magna.xmsystem.xmadmin.ui.parts.InformationPart;
import com.magna.xmsystem.xmadmin.util.CommonConstants;
import com.magna.xmsystem.xmadmin.util.XMAdminUtil;
import com.piterion.security.manager.windows.TicketManager;

/**
 * This is a stub implementation containing e4 LifeCycle annotated
 * methods.<br />
 * There is a corresponding entry in <em>plugin.xml</em> (under the
 * <em>org.eclipse.core.runtime.products' extension point</em>) that references
 * this class.
 * 
 * @author shashwat.anand
 **/
@SuppressWarnings("restriction")
public class E4LifeCycle {
	/**
	 * Logger instance
	 */
	private static final Logger LOGGER = LoggerFactory.getLogger(E4LifeCycle.class);

	/**
	 * Injecting event broker
	 */
	@Inject
	private IEventBroker eventBroker;

	/**
	 * Inject {@link IEclipseContext}
	 */
	@Inject
	private IEclipseContext eclipseContext;

	/**
	 * Member to store default location of XMAdmin shell
	 */
	private Point defaultLocation;
	
	/** Member variable 'message' for {@link Message}. */
	@Inject
	@Translation
	private Message message;
	
	/** Inject of {@link EModelService}. */
	@Inject
	private EModelService modelService;

	/**
	 * Login.
	 *
	 * @param shell
	 *            the shell
	 * @param context
	 *            the context
	 * @param preferences
	 *            the preferences
	 * @param userPreference
	 *            the user preference
	 * @param passwordPreference
	 *            the password preference
	 */
	@PostContextCreate
	void postContextCreate(final IEclipseContext context) {
		XmSystemEnvProcess.getInstance().start(APPLICATION.XMADMIN);
		
		try {
			boolean isWorkspaceLocked = Platform.getInstanceLocation().isLocked();
			if(isWorkspaceLocked) {
				CustomMessageDialog.openError(Display.getDefault().getActiveShell(), message.workspaceLockTitle,
						message.workspaceLockMsg);
				System.exit(0);
			}
			
			final File iconFolder = XMSystemUtil.getXMSystemServerIconsFolder();
			if (iconFolder == null) {
				CustomMessageDialog.openError(Display.getDefault().getActiveShell(), message.iconErrorTitle,
						message.iconErrorMsg);
				System.exit(0);
			}
		} catch (IOException e) {
			LOGGER.error("Execption ocuured while launching the application: ", e); //$NON-NLS-1$
		}	 
		 
		// final Shell shell = new Shell(SWT.INHERIT_NONE);
		
		/*final Shell shell = new Shell(SWT.INHERIT_NONE);
		if (!isEvnVariablesPresent(shell)) {
			// we don't have a workbench yet...
			System.exit(0);
		}
		Display display = context.get(Display.class);
		try {
			Field debug = display.getClass().getSuperclass().getDeclaredField("debug");
			Field tracking = display.getClass().getSuperclass().getDeclaredField("tracking");
			Field trackingLock = display.getClass().getSuperclass().getDeclaredField("trackingLock");
			Field errors = display.getClass().getSuperclass().getDeclaredField("errors");
			Field objects = display.getClass().getSuperclass().getDeclaredField("objects");
			debug.setAccessible(true);
			tracking.setAccessible(true);
			trackingLock.setAccessible(true);
			errors.setAccessible(true);
			objects.setAccessible(true);
			debug.set(display, true);
			tracking.set(display, true);
			trackingLock.set(display, new Object());
			errors.set(display, new Error[128]);
			objects.set(display, new Object[128]);
		} catch (NoSuchFieldException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Sleak sleak = new Sleak();
		sleak.open(display);  */
		
		initXMAdminUtil();
		initHistoryTableUtil();
		XMAdminUtil.getInstance().setDefaultLocale();

		/*final AuthenticationWindow authenticationWindow = new AuthenticationWindow(message.dialogAppName, Application.CAX_START_ADMIN.name(), message.loginDialogUserName,
				message.loginDialogPassword, message.loginDialogLoginBtn, message.loginDialogCancelBtn);
		authenticationWindow.setUsername(XMSystemUtil.getSystemUserName());*/
			
		/*authenticationDialog.create();
		authenticationDialog.getShell().setText(message.dialogAppName);*/
		
		/*loginDialog.getShell().setText(message.dialogAppName);
		String cssTheme = CommonConstants.THEMES.LOGIN_THEME;
		context.set(E4Application.THEME_ID, cssTheme);
		String cssURI = "css/login.css";
		context.set(E4Workbench.CSS_URI_ARG, cssURI);
		PartRenderingEngine.initializeStyling(shell.getDisplay(), context);*/
		
		/*if (authenticationWindow.open() != Window.OK) {
			System.exit(0);
		} else {*/
		String encryptTicket = null;
		long startTime = System.currentTimeMillis();
		if (OSValidator.isWindows()) {
			try {
				TicketManager ticketManager = new TicketManager();
				encryptTicket = ticketManager.generateTicket();
			} catch (Exception e) {
				LOGGER.error("Execption ocuured while launching the application: ", e); //$NON-NLS-1$
			}
		} else if (OSValidator.isUnixOrLinux()) {
			try {
				com.piterion.security.manager.linux.TicketManager ticketManager = new com.piterion.security.manager.linux.TicketManager();
				encryptTicket = ticketManager.generateTicket();
			} catch (Exception e) {
				LOGGER.error("Execption ocuured while launching the application: ", e); //$NON-NLS-1$
			}
		}
		long endTime = System.currentTimeMillis();
		LOGGER.info("Time taken to generate the ticket at caxstartadmin : " + (endTime - startTime));
		try {
			long startTimeMillis = System.currentTimeMillis();
			AuthController authController = new AuthController();
			AuthResponse authResponse = authController.authorizeLogin(encryptTicket, Application.CAX_START_ADMIN.name());
			long endTimeMillis = System.currentTimeMillis();
			if (authResponse != null) {
				boolean isValidUser = authResponse.isValidUser();
				long ldapResponseTime = authResponse.getLdapResponseTime();
				if (!isValidUser) {
					CustomMessageDialog.openError(Display.getCurrent().getActiveShell(),
							message.errorDialogTitile, authResponse.getMessage());
					System.exit(0);
				}
				XMSystemUtil.setSystemUserName(authResponse.getUsername());

				LOGGER.info("Ldap response time : " + ldapResponseTime + "ms");
				LOGGER.info("Webservice response time :" + (endTimeMillis - (startTimeMillis + ldapResponseTime) + "ms"));
				XMAdminUtil.getInstance().populatePermissionMaps();
			} else {
				System.exit(0);
			}
		} catch (RuntimeException e) {
			if (e instanceof ResourceAccessException) {
				CustomMessageDialog.openError(Display.getCurrent().getActiveShell(), message.errorDialogTitile, message.serverNotReachable);
				System.exit(0);
			} else if(e instanceof UnauthorizedAccessException) {
				CustomMessageDialog.openError(Display.getCurrent().getActiveShell(),
						message.errorDialogTitile, message.unauthorizedUserAccessMessage);
				System.exit(0);
			} else if(e instanceof IllegalArgumentException) {
				if (e.getMessage().equals("URI is not absolute")) {
					CustomMessageDialog.openError(Display.getCurrent().getActiveShell(), message.errorDialogTitile, message.inCorrectServerURI);
					System.exit(0);
				} else {
					LOGGER.error("Error while calling authorizeLogin REST Service!", e); //$NON-NLS-1$
				}
			} else if(e instanceof HttpClientErrorException) {
				if (e.getMessage().equals("404 Invalid resource access")) {
					CustomMessageDialog.openError(Display.getCurrent().getActiveShell(), message.errorDialogTitile, message.inCorrectServerURI);
					System.exit(0);
				} else {
					LOGGER.error("Error while calling authorizeLogin REST Service!", e); //$NON-NLS-1$
				}
			} else {
				LOGGER.error("Error while calling authorizeLogin REST Service!", e); //$NON-NLS-1$
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling authorizeLogin REST Service!", e); //$NON-NLS-1$
		}
	}

	/*private boolean isEvnVariablesPresent(final Shell shell) {
		if (Constants.DEBUG_MODE) {
			XmSystemEnvProcess instance1 = XmSystemEnvProcess.getInstance();
			instance1.start(APPLICATION.XMADMIN);
		}
		return true;
	}*/

	/**
	 * Method before save
	 * 
	 * @param workbenchContext
	 */
	@PreSave
	void preSave(final IEclipseContext workbenchContext) {
		// TODO
	}

	/**
	 * Method to process additions
	 * 
	 * @param workbenchContext
	 *            {@link IEclipseContext}
	 * @param eventBroker
	 *            {@link IEventBroker}
	 */
	@ProcessAdditions
	void processAdditions(final IEclipseContext workbenchContext, final IEventBroker eventBroker,
			final MApplication mApplication) {
		eventBroker.subscribe(UIEvents.UILifeCycle.APP_STARTUP_COMPLETE, new EventHandler() {
			/**
			 * Handles event
			 */
			@Override
			public void handleEvent(final Event event) {
				appStartupComplete(workbenchContext,mApplication);

				MWindow mainWindow = findMainWindow(mApplication);

				mainWindow.getContext().set(IWindowCloseHandler.class, new IWindowCloseHandler() {

					@Override
					public boolean close(MWindow window) {
						if (CustomMessageDialog.openConfirm(Display.getDefault().getActiveShell(), message.exitDialogTitle,
								message.exitDialogMessage)) {
							return true;
						}
						return false;
					}
				});
			}
		});
	}
	
	/**
	 * Find main window.
	 *
	 * @param application the application
	 * @return the m window
	 */
	private MWindow findMainWindow(final MApplication application) {
		return application.getChildren().get(0);
	}

	/**
	 * Method to process removal of additions
	 * 
	 * @param workbenchContext
	 *            {@link IEclipseContext}
	 */
	@ProcessRemovals
	void processRemovals(final IEclipseContext workbenchContext) {
		// TODO
	}

	/**
	 * Method for application startup complete
	 * 
	 * @param event
	 *            {@link Event}
	 */
	@Optional
	public void applicationStartupComplete(@UIEventTopic(UIEvents.UILifeCycle.APP_STARTUP_COMPLETE) final Event event) {
		// TODO
	}

	/**
	 * Change the location of main window
	 * 
	 * @param shell
	 *            {@link Shell}
	 */
	@Inject
	@Optional
	public void receiveActiveShell(@Named(IServiceConstants.ACTIVE_SHELL) final Shell shell) {
		try {
			if (shell != null) {
				if (this.defaultLocation == null) {
					final Display display = shell.getDisplay();
					final Monitor primary = display.getPrimaryMonitor();
					final Rectangle displayBounds = primary.getBounds();
					shell.setSize(displayBounds.width - 100, displayBounds.height - 100);
					final Point size = shell.getSize();
					this.defaultLocation = new Point((int) (displayBounds.width - size.x) / 2, (int) (displayBounds.height - size.y) / 2);
					shell.setLocation(this.defaultLocation);
				}
			}
		} catch (Exception e) {
			LOGGER.error("Execption ocuured in Active Shell", e); //$NON-NLS-1$
		}
	}

	/**
	 * Method call after startup is complete
	 * 
	 * @param workbenchContext
	 *            {@link IEclipseContext}
	 * @param mTrimmedWindow 
	 */
	private void appStartupComplete(final IEclipseContext workbenchContext,final MApplication mApplication) {
		XMSystemUtil.cleanupFilesOnExit();
		this.eventBroker.send(CommonConstants.EVENT_BROKER.STATUSBAR, "caxStartAdmin");

		MPart part;
		if ((part = (MPart) this.modelService.find(CommonConstants.PART_ID.INFORMATION_PART_ID, mApplication)) != null) {
			final Object partObj = part.getObject();
			if (partObj instanceof InformationPart) {
				InformationPart rightSidePart = (InformationPart) partObj;
				rightSidePart.updateUserReviewMenuItem();
			}
		}
	}

	/**
	 * Method which copy properties file to user root xmsystem folder
	 */
	/*
	 * private void checkAndCopyPropertiyFile(final File xmSystemRoot) { final
	 * File xmSystemPropFile = new File(xmSystemRoot.getAbsolutePath() +
	 * File.separator + Constants.XMSYSTEM_PORPS_FILE); if
	 * (!xmSystemPropFile.exists()) { try { URL url = new URL(
	 * "platform:/plugin/com.magna.xmsystem.dependencies/xmsystem.properties");
	 * InputStream inputStream = url.openConnection().getInputStream();
	 * FileUtils.copyInputStreamToFile(inputStream, xmSystemPropFile); } catch
	 * (IOException ex) {
	 * LOGGER.error("Execption ocuured in copying properties file", ex);
	 * //$NON-NLS-1$ } } }
	 */

	/**
	 * Method to init XMAdminUtil
	 */
	private void initXMAdminUtil() {
		XMAdminUtil util = ContextInjectionFactory.make(XMAdminUtil.class, eclipseContext);
		eclipseContext.set(XMAdminUtil.class, util);
	}

	
	/**
	 * Method to init HistoryTableUtil
	 */
	private void initHistoryTableUtil() {
		HistoryTableUtil util = ContextInjectionFactory.make(HistoryTableUtil.class, eclipseContext);
		eclipseContext.set(HistoryTableUtil.class, util);
	}
	/*
	 * private void copyIconsFiles(final File xmSystemRoot) { final File
	 * iconFolder = XMSystemUtil.getXMSystemIconFolder(true); if
	 * (iconFolder.exists() && iconFolder.isDirectory()) { String
	 * serverIconsLocation =
	 * XMSystemProperties.getInstance().getProperty("SERVER_ICON_FOLDER"); try {
	 * File serverIconFolder = new File(serverIconsLocation); if
	 * (serverIconFolder.exists() && serverIconFolder.isDirectory()) {
	 * IconController iconController = new IconController(); String allIcons =
	 * iconController.getAllIcons(); JSONArray iconArray = new
	 * JSONArray(allIcons); File[] icons = serverIconFolder.listFiles(); for
	 * (File serverIconFile : icons) { String absolutePath =
	 * serverIconFile.getAbsolutePath(); String iconFileName =
	 * XMSystemUtil.getFileNameWithExtension(absolutePath); // TODO create icon
	 * in DB with iconFileName and absolutePath
	 * FileUtils.copyFileToDirectory(serverIconFile, iconFolder); } }
	 * FileUtils.copyDirectory(new File(serverIconsLocation), iconFolder); }
	 * catch (IOException e) {
	 * LOGGER.error("Execption ocuured in copying icon files", e); //$NON-NLS-1$
	 * } } }
	 */

	/*
	 * private void copyScriptFiles(final File xmSystemRoot) { // TODO
	 * Auto-generated method stub }
	 */
}
