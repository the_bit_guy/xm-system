package com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.baseapplicationmodel;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.UUID;

import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild;

/**
 * The Class BaseAppInformationModel.
 */
public class BaseAppInformationModel implements IAdminTreeChild {

	/** The parent. */
	private IAdminTreeChild parent;
	
	/** The site admin information child. */
	final private Map<String, IAdminTreeChild> baseAppInformationChild;

	/**
	 * Instantiates a new site admin area informations.
	 */
	public BaseAppInformationModel(final IAdminTreeChild parent) {
		this.parent = parent;
		this.baseAppInformationChild = new LinkedHashMap<>();
		this.add(UUID.randomUUID().toString(), new BaseAppInformationStatus(this));
		this.add(UUID.randomUUID().toString(), new BaseAppInformationHistory(this));
	}

	/**
	 * Adds the.
	 *
	 * @param siteAdminInformationChildId the site admin information child id
	 * @param child the child
	 * @return the i admin tree child
	 */
	public IAdminTreeChild add(final String baseAppInformationChildId, final IAdminTreeChild child) {
		child.setParent(this);
		return this.baseAppInformationChild.put(baseAppInformationChildId, child);
	}

	/**
	 * Removes the.
	 *
	 * @param siteAdminInformationChildId the site admin information child id
	 * @return the i admin tree child
	 */
	public IAdminTreeChild remove(final String baseAppInformationChildId) {
		return this.baseAppInformationChild.remove(baseAppInformationChildId);
	}

	/**
	 * Gets the site admin information child collection.
	 *
	 * @return the site admin information child collection
	 */
	public Collection<IAdminTreeChild> getBaseAppInformationCollection() {
		return this.baseAppInformationChild.values();
	}

	/**
	 * Gets the site admin information child children.
	 *
	 * @return the site admin information child children
	 */
	public Map<String, IAdminTreeChild> getBaseAppInformationChild() {
		return baseAppInformationChild;
	}
	
	/*
	 * @see com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild#getParent()
	 */
	@Override
	public IAdminTreeChild getParent() {
		return this.parent;
	}

	/*
	 * @see com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild#
	 * setParent(com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild)
	 */
	@Override
	public void setParent(IAdminTreeChild parent) {
		this.parent = parent;
	}

}
