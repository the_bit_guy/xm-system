package com.magna.xmsystem.xmadmin.ui.dialogs;

import javax.inject.Inject;

import org.eclipse.e4.core.services.nls.Translation;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.layout.GridDataFactory;
import org.eclipse.jface.layout.GridLayoutFactory;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Spinner;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.magna.xmbackend.exception.CannotCreateObjectException;
import com.magna.xmsystem.dependencies.customExceptions.UnauthorizedAccessException;
import com.magna.xmsystem.ui.controls.widgets.CustomMessageDialog;
import com.magna.xmsystem.xmadmin.message.Message;
import com.magna.xmsystem.xmadmin.restclient.relation.UserProjectRelController;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.projects.Project;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.refs.RelationObj;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.users.User;

/**
 * The Class ProjectExpiryDialog.
 */
public class ProjectExpiryDialog extends Dialog {
	
	/** The Constant LOGGER. */
	private static final Logger LOGGER = LoggerFactory.getLogger(ProjectExpiryDialog.class);

	/** The grp project expiry config. */
	private Group grpProjectExpiryConfig;

	/** The lbl project expiry days. */
	private Label lblProjectExpiryDays;

	/** The day spinner. */
	private Spinner daySpinner;

	/** The lbldays. */
	private Label lbldays;

	/** The messages. */
	@Inject
	@Translation
	private Message message;

	/** The expiry days. */
	private String expiryDays;

	/** The relation obj. */
	private RelationObj relationObj;

	/**
	 * Instantiates a new project expiry dialog.
	 *
	 * @param parentShell
	 *            the parent shell
	 * @param relationObj
	 */
	public ProjectExpiryDialog(Shell parentShell, RelationObj relationObj, Message message) {
		super(parentShell);
		this.relationObj = relationObj;
		this.message = message;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.jface.dialogs.Dialog#createDialogArea(org.eclipse.swt.widgets
	 * .Composite)
	 */
	@Override
	protected Control createDialogArea(Composite parent) {

		GridLayoutFactory.fillDefaults().applyTo(parent);
		parent.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));

		this.grpProjectExpiryConfig = new Group(parent, SWT.NONE);
		GridLayoutFactory.fillDefaults().numColumns(1).applyTo(this.grpProjectExpiryConfig);
		GridDataFactory.fillDefaults().grab(true, true).span(SWT.FILL, SWT.FILL).applyTo(this.grpProjectExpiryConfig);

		final Composite widgetContainer = new Composite(this.grpProjectExpiryConfig, SWT.NONE);
		final GridLayout widgetContLayout = new GridLayout(3, false);

		widgetContainer.setLayout(widgetContLayout);
		widgetContainer.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));

		this.lblProjectExpiryDays = new Label(widgetContainer, SWT.NONE);
		this.lblProjectExpiryDays.setText(message.objectExpiryLabel);

		this.daySpinner = new Spinner(widgetContainer, SWT.BORDER);
		this.daySpinner.setIncrement(1);
		this.daySpinner.setPageIncrement(10);
		this.daySpinner.setMaximum(Integer.MAX_VALUE);
		GridDataFactory.fillDefaults().grab(true, false).span(1, 1).align(SWT.FILL, SWT.CENTER)
				.applyTo(this.daySpinner);

		this.lbldays = new Label(widgetContainer, SWT.NONE);
		this.lbldays.setText(message.projectExpiryDaysLabel);

		final Composite buttonBarComp = new Composite(widgetContainer, SWT.NONE);
		final GridLayout btnBarCompLayout = new GridLayout(2, true);
		btnBarCompLayout.marginRight = 0;
		btnBarCompLayout.marginLeft = 0;
		btnBarCompLayout.marginTop = 0;
		btnBarCompLayout.marginBottom = 0;
		btnBarCompLayout.marginWidth = 0;
		buttonBarComp.setLayout(btnBarCompLayout);
		buttonBarComp.setLayoutData(new GridData(SWT.FILL, SWT.DEFAULT, true, false, 3, 1));
		UserProjectRelController propertyConfigController = new UserProjectRelController();

		String expiryDay = this.relationObj.getExpiryDays();
		try {
			if (expiryDay == null) {
				String userId = ((User) ((RelationObj) relationObj).getRefObject()).getUserId();
				String projectId = ((Project) relationObj.getContainerObj().getParent().getParent()).getProjectId();
				String propVOList = propertyConfigController.getUserProjectExpiryDaysByUserProjectId(userId, projectId);
				String propertyValue = propVOList;
				int parseInt = Integer.parseInt(propertyValue);
				this.daySpinner.setSelection(parseInt);
			} else {
				String userId = relationObj.getRelId();
				boolean isUpdated = propertyConfigController.updateUserProjectExpiryDaysByUserProjectRelId(userId,
						expiryDay);
				if (isUpdated) {
					this.daySpinner.setSelection(Integer.parseInt(expiryDay));
				}
			}
		} catch (CannotCreateObjectException e) {
			Display.getDefault().asyncExec(new Runnable() {
				public void run() {
					CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
							message.objectPermissionDialogTitle, message.objectPermissionDialogMsg);
				}
			});
		} catch (UnauthorizedAccessException e) {
			Display.getDefault().asyncExec(new Runnable() {
				public void run() {
					CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
							message.objectPermissionDialogTitle, message.objectPermissionDialogMsg);
				}
			});
		} catch(Exception e){
			LOGGER.error("Error while getting expiry days REST Service", e); //$NON-NLS-1$
		}
		return buttonBarComp;
	}


	/* (non-Javadoc)
	 * @see org.eclipse.jface.window.Window#configureShell(org.eclipse.swt.widgets.Shell)
	 */
	@Override
	protected void configureShell(Shell newShell) {
		super.configureShell(newShell);
		newShell.setText(message.expirydaysDialogTitle);
	}

	/* (non-Javadoc)
	 * @see org.eclipse.jface.dialogs.Dialog#createButtonsForButtonBar(org.eclipse.swt.widgets.Composite)
	 */
	@Override
	protected void createButtonsForButtonBar(Composite parent) {
		createButton(parent, IDialogConstants.OK_ID, message.saveButtonText, true);
		createButton(parent, IDialogConstants.CANCEL_ID, message.cancelButtonText, false);
	}

	/* (non-Javadoc)
	 * @see org.eclipse.jface.dialogs.Dialog#okPressed()
	 */
	@Override
	protected void okPressed() {
		expiryDays = this.daySpinner.getText();
		super.okPressed();
	}

	/**
	 * Gets the expiry days.
	 *
	 * @return the expiry days
	 */
	public String getExpiryDays() {
		return expiryDays;
	}
}
