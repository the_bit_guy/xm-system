package com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.projectapplicationmodel;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.stream.Collectors;

import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.projects.Project;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.refs.RelationObj;

/**
 * Class for Project app admin area projects.
 *
 * @author Chiranjeevi.Akula
 */
public class ProjectAppAdminAreaProjects implements IAdminTreeChild {

	/** Member variable 'parent' for {@link IAdminTreeChild}. */
	private IAdminTreeChild parent;

	/** Member variable 'project app admin area project children' for {@link Map<String,IAdminTreeChild>}. */
	private Map<String, IAdminTreeChild> projectAppAdminAreaProjectsChildren;

	/**
	 * Constructor for ProjectAppAdminAreaProjects Class.
	 *
	 * @param parent {@link IAdminTreeChild}
	 */
	public ProjectAppAdminAreaProjects(IAdminTreeChild parent) {
		super();
		this.parent = parent;
		this.projectAppAdminAreaProjectsChildren = new LinkedHashMap<>();

	}

	/**
	 * Method for Adds the.
	 *
	 * @param projectAppAdminProjectId {@link String}
	 * @param child {@link IAdminTreeChild}
	 * @return the i admin tree child {@link IAdminTreeChild}
	 */
	public IAdminTreeChild add(final String projectAppAdminProjectId, final IAdminTreeChild child) {
		child.setParent(this);
		IAdminTreeChild returnVal = this.projectAppAdminAreaProjectsChildren.put(projectAppAdminProjectId, child);
		if (child instanceof RelationObj && ((RelationObj) child).getRefObject() instanceof Project) {
			sort();
		}
		return returnVal;
	}

	/**
	 * Method for Removes the.
	 *
	 * @param projectAppAdminProjectId {@link String}
	 * @return the i admin tree child {@link IAdminTreeChild}
	 */
	public IAdminTreeChild remove(final String projectAppAdminProjectId) {
		return this.projectAppAdminAreaProjectsChildren.remove(projectAppAdminProjectId);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild#
	 * getParent()
	 */
	@Override
	public IAdminTreeChild getParent() {
		return this.parent;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild#
	 * setParent(com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.
	 * IAdminTreeChild)
	 */
	@Override
	public void setParent(IAdminTreeChild parent) {
		this.parent = parent;
	}

	/**
	 * Sort.
	 */
	public void sort() {
		final LinkedHashMap<String, IAdminTreeChild> collect = this.projectAppAdminAreaProjectsChildren.entrySet().stream().sorted(
				(e1, e2) -> ((Project) e1.getValue()).getName().compareTo(((Project) e2.getValue()).getName()))
				.collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e1, LinkedHashMap::new));
		this.projectAppAdminAreaProjectsChildren = collect;
	}
}
