package com.magna.xmsystem.xmadmin.ui.parts.objexp;

import java.util.Map;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.eclipse.e4.core.contexts.ContextInjectionFactory;
import org.eclipse.e4.core.contexts.IEclipseContext;
import org.eclipse.e4.core.services.events.IEventBroker;
import org.eclipse.e4.core.services.nls.Translation;
import org.eclipse.e4.ui.model.application.MApplication;
import org.eclipse.e4.ui.model.application.ui.basic.MPart;
import org.eclipse.e4.ui.workbench.UIEvents;
import org.eclipse.e4.ui.workbench.modeling.EModelService;
import org.eclipse.jface.util.LocalSelectionTransfer;
import org.eclipse.jface.viewers.DoubleClickEvent;
import org.eclipse.jface.viewers.IDoubleClickListener;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.viewers.TreePath;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.dnd.DND;
import org.eclipse.swt.widgets.Composite;

import com.magna.xmsystem.xmadmin.message.Message;
import com.magna.xmsystem.xmadmin.ui.parts.InformationPart;
import com.magna.xmsystem.xmadmin.ui.parts.objexpcontextmenu.ObjExpContextMenu;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.AdminTreeLabelProvider;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.AdminTreeviewer;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.AdminTreeFactory;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.users.User;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.users.Users;
import com.magna.xmsystem.xmadmin.util.CommonConstants;
import com.magna.xmsystem.xmadmin.util.XMAdminUtil;

public class ObjectExplorer extends TreeViewer {

	//@Inject
	//private ESelectionService selectionService;

	/** The eclipse context. */
	@Inject
	private IEclipseContext eclipseContext;

	@Inject
	@Translation
	private Message messages;
	
	/** The event broker. */
	@Inject
	private IEventBroker eventBroker;

	/** Inject of {@link EModelService}. */
	@Inject
	private EModelService modelService;

	/** Inject of {@link MApplication}. */
	@Inject
	private MApplication application;


	@Inject
	public ObjectExplorer(Composite parent) {
		super(parent, SWT.MULTI | SWT.FULL_SELECTION | SWT.V_SCROLL | SWT.BORDER | SWT.VIRTUAL);
	}

	/**
	 * Creates the context menu.
	 */
	@PostConstruct
	private void createUI() {
		//final ObjectExpContentProvider objectExpContentProvider = ContextInjectionFactory.make(ObjectExpContentProvider.class, this.eclipseContext);
		this.eclipseContext.set(TreeViewer.class, this);
		final AdminTreeLabelProvider adminTreeLabelProvider = ContextInjectionFactory.make(AdminTreeLabelProvider.class, this.eclipseContext);
		ObjExpLazyContentProvider lazyContentProvider = new ObjExpLazyContentProvider(this);
		this.setContentProvider(lazyContentProvider);
		//this.setContentProvider(objectExpContentProvider);
		this.setLabelProvider(adminTreeLabelProvider);
		addDndToObjectExp();
		eclipseContext.set(ObjectExplorer.class, this);
		ContextInjectionFactory.make(ObjExpContextMenu.class, eclipseContext);
		// new ObjExpContextMenu(this);
		this.setUseHashlookup(true);
		initListener();
	}

	/**
	 * Inits the listener.
	 */
	private void initListener() {
		this.addSelectionChangedListener(new ISelectionChangedListener() {
			
			@Override
			public void selectionChanged(SelectionChangedEvent event) {
				eventBroker.send(UIEvents.REQUEST_ENABLEMENT_UPDATE_TOPIC, UIEvents.ALL_ELEMENT_ID);
				if (event.getSelection() instanceof IStructuredSelection) {
					int objectSelected = ((IStructuredSelection) event.getSelection()).size();
					eventBroker.send(CommonConstants.EVENT_BROKER.STATUSBAR, objectSelected + " " + messages.objectsSelected);
				}
			}
		});
		
		this.addDoubleClickListener(new IDoubleClickListener() {
			
			@Override
			public void doubleClick(DoubleClickEvent arg0) {
				MPart mPart = (MPart) modelService.find(CommonConstants.PART_ID.INFORMATION_PART_ID, application);
				InformationPart rightSidePart2 = (InformationPart) mPart.getObject();
				ObjectExpPage objectExpPage = rightSidePart2.getObjectExpPartUI();
				IStructuredSelection selections = (IStructuredSelection) objectExpPage.getObjExpTableViewer().getSelection();
				Object selectionObj = selections.getFirstElement();
				AdminTreeviewer adminTree = XMAdminUtil.getInstance().getAdminTree();
				TreePath[] selectionPaths = adminTree.getStructuredSelection().getPaths();
				if (selectionPaths != null && selectionPaths.length > 0) {
					adminTree.setExpandedState(selectionPaths[0], true);

					if (selectionObj instanceof User) {
						Users users = AdminTreeFactory.getInstance().getUsers();
						Map<String, IAdminTreeChild> userChildren = users.getUsersChildren();
						char startChar = (((User) selectionObj).getName().toString().toLowerCase().toCharArray()[0]);
						IAdminTreeChild iAdminTreeChild = userChildren.get(String.valueOf(startChar));
						adminTree.setExpandedState(iAdminTreeChild, true);
					}
				}
				adminTree.setSelection(new StructuredSelection(selectionObj), true);
			}
		});
	}

	/**
	 * Adds the dnd to object explorer.
	 */
	private void addDndToObjectExp() {
		final LocalSelectionTransfer transfer = LocalSelectionTransfer.getTransfer();
		LocalSelectionTransfer[] transferTypes = new LocalSelectionTransfer[] { LocalSelectionTransfer.getTransfer() };
		this.addDragSupport(DND.DROP_MOVE | DND.DROP_COPY, transferTypes, new ObjectExpDragListener(this, transfer));
	}

	
	/**
	 * Refresh obj exp tree.
	 */
	/*public void refreshObjExpTree() {
		Object selection = selectionService.getSelection();
		if (selection != null && selection instanceof IStructuredSelection) {
			Object firstElement = ((IStructuredSelection) selection).getFirstElement();
			if (firstElement == null) {
				return;
			}
			List<IAdminTreeChild> childObjList = new ArrayList<>();

			if (firstElement instanceof Sites) {
				childObjList = new ArrayList<>(((Sites) firstElement).getSitesCollection());
			} else if (firstElement instanceof AdministrationAreas) {
				childObjList = new ArrayList<>(((AdministrationAreas) firstElement).getAdministrationAreasCollection());
			} else if (firstElement instanceof Users) {
				childObjList = new ArrayList<>();
				final Collection<IAdminTreeChild> userCollection = ((Users) firstElement).getUsersCollection();
				for (IAdminTreeChild userNameAlphabet : userCollection) {
					if (userNameAlphabet instanceof UsersNameAlphabet) {
						childObjList.addAll(((UsersNameAlphabet) userNameAlphabet).getUsersCollection());
					}
				}
			} else if (firstElement instanceof UsersNameAlphabet) {
				childObjList = new ArrayList<>(((UsersNameAlphabet) firstElement).getUsersCollection());
			} else if (firstElement instanceof Projects) {
				childObjList = new ArrayList<>(((Projects) firstElement).getProjectsCollection());
			} else if (firstElement instanceof UserApplications) {
				childObjList = new ArrayList<>(((UserApplications) firstElement).getUserAppCollection());
			} else if (firstElement instanceof ProjectApplications) {
				childObjList = new ArrayList<>(((ProjectApplications) firstElement).getProjectAppCollection());
			} else if (firstElement instanceof StartApplications) {
				childObjList = new ArrayList<>(((StartApplications) firstElement).getStartAppCollection());
			} else if (firstElement instanceof BaseApplications) {
				childObjList = new ArrayList<>(((BaseApplications) firstElement).getBaseAppCollection());
			} else if (firstElement instanceof Groups) {
				childObjList = new ArrayList<>(((Groups) firstElement).getGroupsCollection());
			} else if (firstElement instanceof UserGroupsModel) {
				childObjList = new ArrayList<>(((UserGroupsModel) firstElement).getUserGroupsCollection());
			} else if (firstElement instanceof ProjectGroupsModel) {
				childObjList = new ArrayList<>(((ProjectGroupsModel) firstElement).getProjectGroupsCollection());
			} else if (firstElement instanceof UserApplicationGroups) {
				childObjList = new ArrayList<>(((UserApplicationGroups) firstElement).getUserAppGroupsCollection());
			} else if (firstElement instanceof ProjectApplicationGroups) {
				childObjList = new ArrayList<>(
						((ProjectApplicationGroups) firstElement).getProjectAppGroupsCollection());
			} else if (firstElement instanceof Directories) {
				childObjList = new ArrayList<>(((Directories) firstElement).getDirectoriesCollection());
			} else if (firstElement instanceof ProjectAdminAreas) {
				childObjList = new ArrayList<>(((ProjectAdminAreas) firstElement).getProjectAdminAreaCollection());
			} else if (firstElement instanceof ProjectUsers) {
				childObjList = new ArrayList<>(((ProjectUsers) firstElement).getProjectUserCollection());
			} else if (firstElement instanceof ProjectUserAdminAreas) {
				childObjList = new ArrayList<>(
						((ProjectUserAdminAreas) firstElement).getProjectUserAdminAreaChildCollection());
			} else if (firstElement instanceof SiteAdministrations) {
				childObjList = new ArrayList<>(
						((SiteAdministrations) firstElement).getSiteAdministrationAreasCollection());
			} else if (firstElement instanceof SiteAdminAreaProjects) {
				childObjList = new ArrayList<>(((SiteAdminAreaProjects) firstElement).getSiteAdminProjectsCollection());
			} else if (firstElement instanceof AdminAreaProjects) {
				childObjList = new ArrayList<>(((AdminAreaProjects) firstElement).getAdminAreasProjectsCollection());
			} else if (firstElement instanceof AdminAreaProjects) {
				childObjList = new ArrayList<>(((AdminAreaProjects) firstElement).getAdminAreasProjectsCollection());
			} else if (firstElement instanceof Roles) {
				childObjList = new ArrayList<>(((Roles) firstElement).getRolesCollection());
			} else if (firstElement instanceof RoleScopeObjects) {
				childObjList = new ArrayList<>(
						((RoleScopeObjects) firstElement).getRoleScopeObjectsChildren().values());
			} else if (firstElement instanceof UserAdminAreas) {
				childObjList = new ArrayList<>(((UserAdminAreas) firstElement).getUserAdminAreaChildCollection());
			} else if (firstElement instanceof UserProjects) {
				childObjList = new ArrayList<>(((UserProjects) firstElement).getUserProjectsCollection());
			} else if (firstElement instanceof UserProjectAdminAreas) {
				childObjList = new ArrayList<>(
						((UserProjectAdminAreas) firstElement).getUserProjectAdminAreaChildCollection());
			} else if (firstElement instanceof ProjectAppAdminAreas) {
				childObjList = new ArrayList<>(
						((ProjectAppAdminAreas) firstElement).getProjectAppAdminAreaChildrenCollection());
			} else if (firstElement instanceof LiveMessages) {
				childObjList = new ArrayList<>(((LiveMessages) firstElement).getLiveMsgsCollection());
			} else {
				AdminTreeDataLoad adminTreeDataLoad = AdminTreeDataLoad.getInstance();
				if (firstElement instanceof SiteAdminAreaUserApplications
						|| firstElement instanceof SiteAdminAreaUserAppNotFixed
						|| firstElement instanceof SiteAdminAreaUserAppFixed
						|| firstElement instanceof SiteAdminAreaUserAppProtected
						|| firstElement instanceof AdminAreaUserApplications
						|| firstElement instanceof AdminAreaUserAppNotFixed
						|| firstElement instanceof AdminAreaUserAppFixed
						|| firstElement instanceof AdminAreaUserAppProtected) {
					childObjList = adminTreeDataLoad
							.loadSiteAdminAreaUserAppsFromService((IAdminTreeChild) firstElement);
				} else if (firstElement instanceof SiteAdminAreaProjectApplications
						|| firstElement instanceof SiteAdminProjectAppNotFixed
						|| firstElement instanceof SiteAdminProjectAppFixed
						|| firstElement instanceof SiteAdminProjectAppProtected
						|| firstElement instanceof AdminAreaProjectApplications
						|| firstElement instanceof AdminAreaProjectAppNotFixed
						|| firstElement instanceof AdminAreaProjectAppFixed
						|| firstElement instanceof AdminAreaProjectAppProtected
						|| firstElement instanceof ProjectAdminAreaProjectApplications
						|| firstElement instanceof ProjectAdminAreaProjectAppNotFixed
						|| firstElement instanceof ProjectAdminAreaProjectAppFixed
						|| firstElement instanceof ProjectAdminAreaProjectAppProtected) {
					childObjList = new ArrayList<>(
							adminTreeDataLoad.loadSiteAdminAreaProjAppsFromService((IAdminTreeChild) firstElement));
				} else if (firstElement instanceof SiteAdminAreaStartApplications
						|| firstElement instanceof AdminAreaStartApplications) {
					childObjList = new ArrayList<>(
							adminTreeDataLoad.loadSiteAdminAreaStartAppsFromService((IAdminTreeChild) firstElement));
				} else if (firstElement instanceof SiteAdminAreaProjectStartApplications
						|| firstElement instanceof AdminAreaProjectStartApplications
						|| firstElement instanceof ProjectAdminAreaStartApplications) {
					childObjList = new ArrayList<>(adminTreeDataLoad
							.loadSiteAdminAreaProjStartAppsFromService((IAdminTreeChild) firstElement));
				} else if (firstElement instanceof ProjectProjectApplications) {
					childObjList = adminTreeDataLoad
							.loadProjectProjectAppFromService((ProjectProjectApplications) firstElement);
				} else if (firstElement instanceof BaseAppProjectApplications) {
					childObjList = adminTreeDataLoad
							.loadBaseAppProjectAppFromService((BaseAppProjectApplications) firstElement);
				} else if (firstElement instanceof BaseAppUserApplications) {
					childObjList = adminTreeDataLoad
							.loadBaseAppUserAppFromService((BaseAppUserApplications) firstElement);
				} else if (firstElement instanceof BaseAppStartApplications) {
					childObjList = adminTreeDataLoad
							.loadBaseAppStartAppFromService((BaseAppStartApplications) firstElement);
				} else if (firstElement instanceof UserAAUserApplications
						|| firstElement instanceof UserAAUserAppAllowed
						|| firstElement instanceof UserAAUserAppForbidden) {
					childObjList = new ArrayList<>(
							adminTreeDataLoad.loadUserAAUserAppsFromService((IAdminTreeChild) firstElement));
				} else if (firstElement instanceof UserProjectAAProjectApplications
						|| firstElement instanceof UserProjectAAProjectAppAllowed
						|| firstElement instanceof UserProjectAAProjectAppForbidden
						|| firstElement instanceof ProjectUserAAProjectApplications
						|| firstElement instanceof ProjectUserAAProjectAppAllowed
						|| firstElement instanceof ProjectUserAAProjectAppForbidden) {
					childObjList = new ArrayList<>(
							adminTreeDataLoad.loadUserProjectAAProjectAppFromService((IAdminTreeChild) firstElement));
				} else if (firstElement instanceof UserStartApplications) {
					childObjList = new ArrayList<>(
							adminTreeDataLoad.loadUserStartAppsFromService((IAdminTreeChild) firstElement));
				} else if (firstElement instanceof UserAppAdminAreas) {
					childObjList = new ArrayList<>(
							adminTreeDataLoad.loadUserAppAdminAreasFromService((IAdminTreeChild) firstElement));
				} else if (firstElement instanceof UserAppUsers) {
					childObjList = new ArrayList<>(
							adminTreeDataLoad.loadUserAppUsersFromService((IAdminTreeChild) firstElement));
				} else if (firstElement instanceof ProjectAppAdminAreaProjects) {
					childObjList = new ArrayList<>(adminTreeDataLoad
							.loadProjectAppAdminAreaProjectsFromService((IAdminTreeChild) firstElement));
				} else if (firstElement instanceof ProjectAppUsers) {
					childObjList = new ArrayList<>(
							adminTreeDataLoad.loadProjectAppUsersFromService((IAdminTreeChild) firstElement));
				} else if (firstElement instanceof StartAppAdminAreas) {
					childObjList = new ArrayList<>(
							adminTreeDataLoad.loadStartAppAdminAreasFromService((IAdminTreeChild) firstElement));
				} else if (firstElement instanceof StartAppProjects) {
					childObjList = new ArrayList<>(
							adminTreeDataLoad.loadStartAppProjectsFromService((IAdminTreeChild) firstElement));
				} else if (firstElement instanceof StartAppUsers) {
					childObjList = new ArrayList<>(
							adminTreeDataLoad.loadStartAppUsersFromService((IAdminTreeChild) firstElement));
				} else if (firstElement instanceof DirectoryUsers) {
					childObjList = new ArrayList<>(
							adminTreeDataLoad.loadDirectoryUsersFromService((IAdminTreeChild) firstElement));
				} else if (firstElement instanceof DirectoryProjects) {
					childObjList = new ArrayList<>(
							adminTreeDataLoad.loadDirectoryProjectsFromService((IAdminTreeChild) firstElement));
				} else if (firstElement instanceof DirectoryUserApplications) {
					childObjList = new ArrayList<>(
							adminTreeDataLoad.loadDirectoryUserApplicationsFromService((IAdminTreeChild) firstElement));
				} else if (firstElement instanceof DirectoryProjectApplications) {
					childObjList = new ArrayList<>(adminTreeDataLoad
							.loadDirectoryProjectApplicationsFromService((IAdminTreeChild) firstElement));
				} else if (firstElement instanceof RoleUsers) {
					childObjList = new ArrayList<>(
							adminTreeDataLoad.loadRoleUsersFromService((IAdminTreeChild) firstElement));
				} else if (firstElement instanceof RoleScopeObjectUsers) {
					childObjList = new ArrayList<>(
							adminTreeDataLoad.loadRoleScopeObjectUsersFromService((IAdminTreeChild) firstElement));
				} else if (firstElement instanceof AdminUsers) {
					childObjList = new ArrayList<>(
							adminTreeDataLoad.loadAdminMenuUsersFromServices((IAdminTreeChild) firstElement));
				} else if (firstElement instanceof AdminUserApps) {
					childObjList = new ArrayList<>(
							adminTreeDataLoad.loadAdminMenuUserAppFromServices((IAdminTreeChild) firstElement));
				} else if (firstElement instanceof AdminProjectApps) {
					childObjList = new ArrayList<>(
							adminTreeDataLoad.loadAdminMenuProjectAppFromServices((IAdminTreeChild) firstElement));
				} else if (firstElement instanceof UserGroupUsers) {
					childObjList = new ArrayList<>(
							adminTreeDataLoad.loadUserGroupUsersFromService((IAdminTreeChild) firstElement));
				} else if (firstElement instanceof ProjectGroupProjects) {
					childObjList = new ArrayList<>(
							adminTreeDataLoad.loadProjectGroupProjectsFromService((IAdminTreeChild) firstElement));
				} else if (firstElement instanceof UserAppGroupUserApps) {
					childObjList = new ArrayList<>(
							adminTreeDataLoad.loadUserAppGroupUserAppsFromService((IAdminTreeChild) firstElement));
				} else if (firstElement instanceof ProjectAppGroupProjectApps) {
					childObjList = new ArrayList<>(adminTreeDataLoad
							.loadProjectAppGroupProjectAppsFromService((IAdminTreeChild) firstElement));
				} else {
					return;
				}
			}
			this.setInput(childObjList);
		}
	}*/
}
