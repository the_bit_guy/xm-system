package com.magna.xmsystem.xmadmin.ui.dialogs.lang;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.layout.GridDataFactory;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CLabel;
import org.eclipse.swt.events.VerifyEvent;
import org.eclipse.swt.events.VerifyListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

import com.magna.xmsystem.dependencies.utils.LANG_ENUM;
import com.magna.xmsystem.dependencies.utils.XMSystemUtil;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.sitemodel.Site;
import com.magna.xmsystem.xmadmin.util.CommonConstants;

/**
 * The Class XMAdminLangTextAreaDialog.
 * 
 * @author shashwat.anand
 */
public class XMAdminLangTextAreaDialog extends Dialog {

	/** Instance for Max limit character. */
	static final int MAX_LIMIT = 1000;

	/** Instance for String slash. */
	public static final String SLASH = "/";

	/** Instance for Limit_count_padding. */
	public static final String LIMIT_COUNT_PADDING = "  ";

	/** The Constant TEXT_LANG_ENUM. */
	private static final String TEXT_LANG_ENUM = "TEXT_LANG_ENUM";

	/** The control model. */
	private ControlModel controlModel;

	/** The txt fields. */
	private Text[] txtFields;
	
	/** The Constant OK_LABEL_EN. */
	final static private String OK_LABEL_EN = "OK";
	
	/** The Constant OK_LABEL_DE. */
	final static private String OK_LABEL_DE = "OK";
	
	/** The Constant CANCEL_LABEL_EN. */
	final static private String CANCEL_LABEL_EN = "Cancel";
	
	/** The Constant CANCEL_LABEL_DE. */
	final static private String CANCEL_LABEL_DE = "Abbrechen";

	/**
	 * Instantiates a new XM admin lang text dialog.
	 *
	 * @param model
	 *            the model
	 */
	public XMAdminLangTextAreaDialog(final ControlModel model) {
		this(Display.getDefault().getActiveShell(), model);
	}

	/**
	 * Instantiates a new XM admin lang text dialog.
	 *
	 * @param parentShell
	 *            the parent shell
	 * @param model
	 *            the model
	 */
	public XMAdminLangTextAreaDialog(final Shell parentShell, final ControlModel model) {
		super(parentShell);
		this.controlModel = model;
	}

	/**
	 * @see org.eclipse.jface.window.Window#configureShell(org.eclipse.swt.widgets.Shell)
	 */
	@Override
	protected void configureShell(final Shell shell) {
		super.configureShell(shell);
		String title;
		if ((title = this.controlModel.getDialogTitle()) != null) {
			shell.setText(title);
		}
	}

	/**
	 * @see org.eclipse.jface.dialogs.Dialog#createDialogArea(org.eclipse.swt.widgets
	 *      .Composite)
	 */
	@Override
	protected Control createDialogArea(final Composite parent) {
		parent.setBackgroundMode(SWT.INHERIT_DEFAULT);
		final Composite control = createContentArea(parent);
		initWidgets();
		return control;
	}

	/**
	 * Inits the widgets.
	 */
	private void initWidgets() {
		LANG_ENUM lang;
		for (final Text text : txtFields) {
			if ((lang = (LANG_ENUM) text.getData(TEXT_LANG_ENUM)) != null
					&& this.controlModel.getObjectModel(lang) != null) {
				text.setText(this.controlModel.getObjectModel(lang));
			} else {
				text.setText(CommonConstants.EMPTY_STR);
			}
			text.setSelection(text.getText().length());
		}
	}

	/**
	 * Creates the content area.
	 *
	 * @param parent
	 *            the parent
	 * @return the composite
	 */
	private Composite createContentArea(final Composite parent) {
		final Composite comp = new Composite(parent, SWT.NONE);
		comp.setBackgroundMode(SWT.INHERIT_DEFAULT);
		final GridLayout gridLayout = new GridLayout();
		gridLayout.numColumns = 3;
		comp.setLayout(gridLayout);

		final LANG_ENUM[] values = LANG_ENUM.values();
		this.txtFields = new Text[values.length];

		for (int index = 0; index < values.length; index++) {
			Label label = new Label(comp, SWT.NONE);
			String langLabel;
			if ((langLabel = this.controlModel.getLangLabel()) != null) {
				label.setText(langLabel);
			}
			final CLabel imageLabel = new CLabel(comp, SWT.NONE);
			if (values[index] == LANG_ENUM.ENGLISH) {
				imageLabel.setImage(XMSystemUtil.getInstance().getImage(this.getClass(), "icons/16x16/english.gif")); //$NON-NLS-1$
			} else if (values[index] == LANG_ENUM.GERMAN) {
				imageLabel.setImage(XMSystemUtil.getInstance().getImage(this.getClass(), "icons/16x16/german.gif")); //$NON-NLS-1$
			}
			imageLabel.setText(this.controlModel.getLangLabelText(values[index]));
			GridDataFactory.fillDefaults().grab(true, false).align(SWT.FILL, SWT.CENTER).span(2, 1).applyTo(imageLabel);

			label = new Label(comp, SWT.NONE);
			String textLabel;
			if ((textLabel = this.controlModel.getWidgetLabel()) != null) {
				label.setText(textLabel);
			}

			label = new Label(comp, SWT.NONE);
			GridDataFactory.fillDefaults().grab(true, false).align(SWT.FILL, SWT.CENTER).applyTo(label);

			Label lblCount = new Label(comp, SWT.BORDER | SWT.CENTER);
			final GridData gridDatalblCount = new GridData(SWT.LEFT, SWT.CENTER, false, false);
			gridDatalblCount.widthHint = 70;
			lblCount.setLayoutData(gridDatalblCount);

			this.txtFields[index] = new Text(comp, SWT.BORDER | SWT.MULTI | SWT.WRAP | SWT.H_SCROLL | SWT.V_SCROLL);
			this.txtFields[index].setData(TEXT_LANG_ENUM, values[index]);
			final GridData gridData = new GridData(SWT.FILL, SWT.CENTER, true, false);
			gridData.widthHint = 400;
			gridData.heightHint = 150;
			gridData.horizontalSpan = 3;
			this.txtFields[index].setLayoutData(gridData);
			this.txtFields[index].setTextLimit(this.controlModel.getMaxTextLimit());
			this.txtFields[index].addVerifyListener(new VerifyListener() {

				@Override
				public void verifyText(VerifyEvent event) {
					String source = ((Text) event.widget).getText();
					final String remarkText = source.substring(0, event.start) + event.text
							+ source.substring(event.end);
					int length = remarkText.length();
					if (length > Site.REMARK_LIMIT) {
						event.doit = false;

					}

				}
			});
			this.txtFields[index].setEditable(this.controlModel.isEditable());
			lblCount.setText(this.txtFields[index].getText().length() + SLASH + this.controlModel.getMaxTextLimit());
			this.txtFields[index]
					.addModifyListener(new TextAreaModifyListener(lblCount, this.controlModel.getMaxTextLimit()));
		}
		return comp;
	}

	/**
	 * @see org.eclipse.jface.dialogs.Dialog#createButtonsForButtonBar(org.eclipse.
	 *      swt.widgets.Composite)
	 */
	@Override
	protected void createButtonsForButtonBar(final Composite parent) {
		// create OK and Cancel buttons by default
		parent.setBackgroundMode(SWT.INHERIT_DEFAULT);
		super.createButtonsForButtonBar(parent);
		
		Button cancelButton = getButton(IDialogConstants.CANCEL_ID);
		Button okButton = getButton(IDialogConstants.OK_ID);
		LANG_ENUM currentLang = LANG_ENUM.ENGLISH;
		final String localeString = XMSystemUtil.getLanguage();
		if(!XMSystemUtil.isEmpty(localeString)) {
			currentLang = LANG_ENUM.valueOf(localeString.toUpperCase());
		}
		if (currentLang == LANG_ENUM.GERMAN) {
			cancelButton.setText(CANCEL_LABEL_DE);
			okButton.setText(OK_LABEL_DE);
		}else{
			cancelButton.setText(CANCEL_LABEL_EN);
			okButton.setText(OK_LABEL_EN);
		}
	}

	/**
	 * @see org.eclipse.jface.dialogs.Dialog#okPressed()
	 */
	@Override
	protected void okPressed() {
		if (isValidData()) {
			for (final Text text : txtFields) {
				LANG_ENUM lang;
				if ((lang = (LANG_ENUM) text.getData(TEXT_LANG_ENUM)) != null) {
					this.controlModel.setObjectModelMap(lang, text.getText());

				}
			}
			super.okPressed();
		}
	}

	/**
	 * Checks if is valid data.
	 *
	 * @return true, if is valid data
	 */
	private boolean isValidData() {
		boolean returnVal = true;
		if (this.controlModel.isManditory()) {
			for (final Text text : txtFields) {
				returnVal &= XMSystemUtil.isEmpty(text.getText());
			}
		}
		return returnVal;
	}

	/**
	 * @return the txtFields
	 */
	public Text[] getTxtFields() {
		return this.txtFields;
	}

	/*
	 * private class TextAreaModifyListener implements ModifyListener { private
	 * Label countLabel;
	 * 
	 * public TextAreaModifyListener(final Label lblCount) { this.countLabel =
	 * lblCount; }
	 * 
	 * @Override public void modifyText(ModifyEvent event) { final Text text =
	 * (Text) event.widget; countCharactor(text, countLabel); } }
	 */
}
