package com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.adminconf;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;

import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild;

/**
 * The Class AdminProjectApps.
 * 
 * @author shashwat.anand
 */
public class AdminProjectApps implements IAdminTreeChild {
	/** The parent. */
	private IAdminTreeChild parent;
	
	/** The admin project app child. */
	final private Map<String, IAdminTreeChild> adminProjectAppChild;

	/**
	 * Instantiates a new admin user apps.
	 *
	 * @param parent the parent
	 */
	public AdminProjectApps(IAdminTreeChild parent) {
		this.parent = parent;
		this.adminProjectAppChild = new LinkedHashMap<>();
	}
	

	/**
	 * Adds the.
	 *
	 * @param adminProjectAppChildId the admin project app child id
	 * @param child the child
	 * @return the i admin tree child
	 */
	public IAdminTreeChild add(final String adminProjectAppChildId, final IAdminTreeChild child) {
		child.setParent(this);
		return this.adminProjectAppChild.put(adminProjectAppChildId, child);
	}
	

	/**
	 * Removes the.
	 *
	 * @param adminProjectAppChildId the admin project app child id
	 * @return the i admin tree child
	 */
	public IAdminTreeChild remove(final String adminProjectAppChildId) {
		return this.adminProjectAppChild.remove(adminProjectAppChildId);
	}
	
	/**
	 * Gets the role user children.
	 *
	 * @return the role user children
	 */
	public Map<String, IAdminTreeChild> getProjectAppChildren() {
		return adminProjectAppChild;
	}

	/**
	 * Gets the admin project app collection.
	 *
	 * @return the admin project app collection
	 */
	public Collection<IAdminTreeChild> getAdminProjectAppCollection() {
		return this.adminProjectAppChild.values();
	}

	/* (non-Javadoc)
	 * @see com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild#getParent()
	 */
	@Override
	public IAdminTreeChild getParent() {
		return this.parent;
	}

	/* (non-Javadoc)
	 * @see com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild#setParent(com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild)
	 */
	@Override
	public void setParent(IAdminTreeChild parent) {
		this.parent = parent;
	}
}
