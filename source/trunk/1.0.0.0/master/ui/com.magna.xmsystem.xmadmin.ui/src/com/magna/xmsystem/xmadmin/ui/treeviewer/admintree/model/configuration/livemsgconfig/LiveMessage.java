package com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.livemsgconfig;

import java.beans.PropertyChangeEvent;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import com.magna.xmsystem.dependencies.utils.LANG_ENUM;
import com.magna.xmsystem.dependencies.utils.XMSystemUtil;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.BeanModel;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild;
import com.magna.xmsystem.xmadmin.util.CommonConstants;

// TODO: Auto-generated Javadoc
/**
 * The Class LiveMessage.
 * @author archita.patel
 */
public class LiveMessage extends BeanModel implements IAdminTreeChild {

	/** The Constant SUB_LIMIT. */
	public static final int SUB_LIMIT = 100;

	/** The Constant MESSAGE_LIMIT. */
	public static final int MESSAGE_LIMIT = 1500;
	
	public static final int NAME_LIMIT = 30;

	/** The Constant PROPERTY_LIVEMSGNAME. */
	public static final String PROPERTY_LIVEMSGNAME = "name"; //$NON-NLS-1$

	/** The Constant PROPERTY_LIVEMSG_ID. */
	public static final String PROPERTY_LIVEMSG_ID = "liveMsgId"; //$NON-NLS-1$

	/** The Constant PROPERTY_MESSAGE. */
	public static final String PROPERTY_MESSAGE = "message"; //$NON-NLS-1$

	/** The Constant PROPERTY_SUBJECT. */
	public static final String PROPERTY_SUBJECT = "subject";
	
	/** The Constant PROPERTY_MESSAGE. */
	public static final String PROPERTY_MESSAGE_MAP = "messageMap"; //$NON-NLS-1$

	/** The Constant PROPERTY_SUBJECT. */
	public static final String PROPERTY_SUBJECT_MAP = "subjectMap"; //$NON-NLS-1$

	/** The Constant PROPERTY_LIVEMSG_ISPOPUP. */
	public static final String PROPERTY_LIVEMSG_ISPOPUP = "popup"; //$NON-NLS-1$

	/** The Constant PROPERTY_STARTDATE. */
	public static final String PROPERTY_STARTDATE = "startDateTime"; //$NON-NLS-1$

	/** The Constant PROPERTY_ENDDATE. */
	public static final String PROPERTY_ENDDATE = "endDateTime"; //$NON-NLS-1$

	/** The Constant PROPERTY_OPERATION_MODE. */
	public static final String PROPERTY_OPERATION_MODE = "operationMode"; //$NON-NLS-1$

	/** The Constant PROPERTY_USERS. */
	public static final String PROPERTY_LIVEMSG_To = "liveMsgToMap"; //$NON-NLS-1$

	/** The Constant PROPERTY_OBJID_MAP. */
	public static final String PROPERTY_OBJID_MAP = "objectIdMap"; //$NON-NLS-1$

	/** The live msg id. */
	private String liveMsgId;

	/** The live msg to. */
	private Map<String, List<Map<String, String>>> liveMsgToMap;

	/** The object id map. */
	private Map<String, String> objectIdMap;
	
	/** The subject. */
	private String subject;

	/** The message. */
	private String message;

	/** The name. */
	private String name;

	/** The subject map. */
	private Map<LANG_ENUM, String> subjectMap;

	/** The message map. */
	private Map<LANG_ENUM, String> messageMap;

	/** The is popup. */
	private boolean popup;

	/** The start date time. */
	private Date startDateTime;

	/** The end date time. */
	private Date expiryDateTime;

	/** Member variable for mode of operation. */
	private int operationMode;

	/**
	 * Instantiates a new live message.
	 *
	 * @param liveMsgId
	 *            the live msg id
	 * @param name
	 *            the name
	 * @param isPopup
	 *            the is popup
	 * @param operationMode
	 *            the operation mode
	 */
	public LiveMessage(String liveMsgId, String name, boolean isPopup, int operationMode) {
		this(liveMsgId, new HashMap<>(), new HashMap<>(), name,null,null,new HashMap<>(), new HashMap<>(), isPopup, null, null, operationMode);

	}

	
	/**
	 * Instantiates a new live message.
	 *
	 * @param liveMsgId
	 *            the live msg id
	 * @param liveMsgTo
	 *            the live msg to
	 * @param objectIdMap
	 *            the object id map
	 * @param name
	 *            the name
	 * @param message
	 *            the message
	 * @param subject
	 *            the subject
	 * @param messageMap
	 *            the message map
	 * @param subjectMap
	 *            the subject map
	 * @param isPopup
	 *            the is popup
	 * @param startDateTime
	 *            the start date time
	 * @param endDateTime
	 *            the end date time
	 * @param operationMode
	 *            the operation mode
	 */
	public LiveMessage(String liveMsgId, Map<String, List<Map<String, String>>> liveMsgTo,
			Map<String, String> objectIdMap, String name,String message, String subject, Map<LANG_ENUM, String> messageMap, Map<LANG_ENUM, String> subjectMap, boolean isPopup,
			Date startDateTime, Date endDateTime, int operationMode) {
		super();
		this.liveMsgId = liveMsgId;
		this.liveMsgToMap = liveMsgTo;
		this.name = name;
		this.message = message;
		this.subject = subject;
		this.messageMap = messageMap;
		this.subjectMap = subjectMap;
		this.popup = isPopup;
		this.startDateTime = startDateTime;
		this.expiryDateTime = endDateTime;
		this.operationMode = operationMode;
		this.objectIdMap = objectIdMap;
	}

	/**
	 * Gets the live msg id.
	 *
	 * @return the live msg id
	 */
	public String getLiveMsgId() {
		return liveMsgId;
	}

	/**
	 * Sets the live msg id.
	 *
	 * @param liveMsgId
	 *            the new live msg id
	 */
	public void setLiveMsgId(String liveMsgId) {
		this.propertyChangeSupport.firePropertyChange(PROPERTY_LIVEMSG_ID, this.liveMsgId, this.liveMsgId = liveMsgId);
		// this.liveMsgId = liveMsgId;
	}

	/**
	 * Gets the live msg to.
	 *
	 * @return the live msg to
	 */
	public Map<String, List<Map<String, String>>> getLiveMsgToMap() {
		return liveMsgToMap;
	}

	/**
	 * Sets the live msg to.
	 *
	 * @param liveMsgToMap
	 *            the live msg to
	 */
	public void setLiveMsgToMap(Map<String, List<Map<String, String>>> liveMsgToMap) {
		this.propertyChangeSupport.firePropertyChange(PROPERTY_LIVEMSG_To, this.liveMsgToMap, this.liveMsgToMap = liveMsgToMap);
		// this.liveMsgTo = liveMsgTo;
	}

	/**
	 * Gets the object id map.
	 *
	 * @return the object id map
	 */
	public Map<String, String> getObjectIdMap() {
		return objectIdMap;
	}

	/**
	 * Sets the object id map.
	 *
	 * @param objectIdMap
	 *            the object id map
	 */
	public void setObjectIdMap(Map<String, String> objectIdMap) {
		this.propertyChangeSupport.firePropertyChange(PROPERTY_OBJID_MAP, this.objectIdMap,
				this.objectIdMap = objectIdMap);
		// this.objectIdMap = objectIdMap;
	}

	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Sets the name.
	 *
	 * @param name
	 *            the new name
	 */
	public void setName(String name) {
		if (name == null)
			throw new IllegalArgumentException();
		this.propertyChangeSupport.firePropertyChange(PROPERTY_LIVEMSGNAME, this.name, this.name = name.trim());
		// this.name = name;
	}

	/**
	 * Gets the message.
	 *
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * Sets the message.
	 *
	 * @param message
	 *            the new message
	 */
	public void setMessage(String message) {
		if (message == null)
			throw new IllegalArgumentException();
		this.propertyChangeSupport.firePropertyChange(PROPERTY_MESSAGE, this.message, this.message = message);
		// this.message = message;
	}

	/**
	 * Gets the subject.
	 *
	 * @return the subject
	 */
	public String getSubject() {
		return subject;
	}

	/**
	 * Sets the subject.
	 *
	 * @param subject
	 *            the new subject
	 */
	public void setSubject(String subject) {
		if (subject == null)
			throw new IllegalArgumentException();
		this.propertyChangeSupport.firePropertyChange(PROPERTY_SUBJECT, this.subject, this.subject = subject);
		// this.subject = subject;
	}

	/**
	 * Gets the subject map.
	 *
	 * @return the subject map
	 */
	public Map<LANG_ENUM, String> getSubjectMap() {
		return subjectMap;
	}
	
	/**
	 * Gets the subject.
	 *
	 * @param lang the lang
	 * @return the subject
	 */
	public String getSubject(final LANG_ENUM lang) {
		return this.subjectMap.get(lang);
	}
	
	/**
	 * Set subject.
	 *
	 * @param lang the lang
	 * @param subject the subject
	 */
	public void setSubject(final LANG_ENUM lang, final String subject) {
		if (lang == null || subject == null)
			throw new IllegalArgumentException();
		this.propertyChangeSupport.firePropertyChange(PROPERTY_SUBJECT_MAP, this.subjectMap,
				this.subjectMap.put(lang, subject.trim()));
	}
	
	/**
	 * Set subject map.
	 *
	 * @param subjectMap the subject map
	 */
	public void setSubjectMap(Map<LANG_ENUM, String> subjectMap) {
		if (subjectMap == null)
			throw new IllegalArgumentException();
		this.propertyChangeSupport.firePropertyChange(PROPERTY_SUBJECT_MAP, this.subjectMap,
				this.subjectMap = subjectMap);
		//this.subjectMap = subjectMap;
	}

	/**
	 * Gets the message map.
	 *
	 * @return the message map
	 */
	public Map<LANG_ENUM, String> getMessageMap() {
		return messageMap;
	}
	
	/**
	 * Gets the message.
	 *
	 * @param lang the lang
	 * @return the message
	 */
	public String getMessage(final LANG_ENUM lang) {
		return this.messageMap.get(lang);
	}
	
	
	/**
	 * Set message.
	 *
	 * @param lang the lang
	 * @param message the message
	 */
	public void setMessage(final LANG_ENUM lang, final String message) {
		if (lang == null || message == null)
			throw new IllegalArgumentException();
		this.propertyChangeSupport.firePropertyChange(PROPERTY_MESSAGE_MAP, this.messageMap,
				this.messageMap.put(lang, message.trim()));
	}
	
	/**
	 * Set message map.
	 *
	 * @param messageMap the message map
	 */
	public void setMessageMap(Map<LANG_ENUM, String> messageMap) {
		if (messageMap == null)
			throw new IllegalArgumentException();
		this.propertyChangeSupport.firePropertyChange(PROPERTY_MESSAGE_MAP, this.messageMap,
				this.messageMap = messageMap);
		//this.messageMap = messageMap;
	}

	/**
	 * Checks if is popup.
	 *
	 * @return true, if is popup
	 */
	public boolean isPopup() {
		return popup;
	}

	/**
	 * Sets the popup.
	 *
	 * @param popup
	 *            the new popup
	 */
	public void setPopup(boolean popup) {
		this.propertyChangeSupport.firePropertyChange(PROPERTY_LIVEMSG_ISPOPUP, this.popup, this.popup = popup);
		// this.isPopup = isPopup;
	}

	/**
	 * Gets the start date time.
	 *
	 * @return the start date time
	 */
	public Date getStartDateTime() {
		return startDateTime;
	}

	/**
	 * Sets the start date time.
	 *
	 * @param startDateTime
	 *            the new start date time
	 */
	public void setStartDateTime(Date startDateTime) {
		this.propertyChangeSupport.firePropertyChange(PROPERTY_STARTDATE, this.startDateTime,
				this.startDateTime = startDateTime);
		// this.startDateTime = startDateTime;
	}

	/**
	 * Gets the expiry date time.
	 *
	 * @return the expiry date time
	 */
	public Date getExpiryDateTime() {
		return expiryDateTime;
	}

	/**
	 * Sets the end date time.
	 *
	 * @param expiryDateTime
	 *            the new end date time
	 */
	public void setExpiryDateTime(Date expiryDateTime) {

		this.propertyChangeSupport.firePropertyChange(PROPERTY_ENDDATE, this.expiryDateTime,
				this.expiryDateTime = expiryDateTime);
		// this.endDateTime = endDateTime;
	}

	/**
	 * Gets the operation mode.
	 *
	 * @return the operation mode
	 */
	public int getOperationMode() {
		return operationMode;
	}

	/**
	 * Sets the operation mode.
	 *
	 * @param operationMode
	 *            the new operation mode
	 */
	public void setOperationMode(int operationMode) {
		this.propertyChangeSupport.firePropertyChange(PROPERTY_OPERATION_MODE, this.operationMode,
				this.operationMode = operationMode);
		// this.operationMode = operationMode;
	}

	/**
	 * Deep copy live msg.
	 *
	 * @param update
	 *            the update
	 * @param updateThisObject
	 *            the update this object
	 * @return the live message
	 */
	public LiveMessage deepCopyLiveMsg(boolean update, LiveMessage updateThisObject) {
		LiveMessage clonedLiveMsg = null;
		String currentLiveMsgId = XMSystemUtil.isEmpty(this.getLiveMsgId()) ? CommonConstants.EMPTY_STR
				: this.getLiveMsgId();
		String currentName = XMSystemUtil.isEmpty(this.getName()) ? CommonConstants.EMPTY_STR : this.getName();
		String currentMessage = XMSystemUtil.isEmpty(this.getMessage()) ? CommonConstants.EMPTY_STR : this.getMessage();
		String currentSubject = XMSystemUtil.isEmpty(this.getSubject()) ? CommonConstants.EMPTY_STR : this.getSubject();
		
		
		Map<LANG_ENUM, String> currentTranslationIdMap = new HashMap<>();
		Map<LANG_ENUM, String> currentSubjectMap = new HashMap<>();
		Map<LANG_ENUM, String> currentMessageMap = new HashMap<>();

		for (LANG_ENUM langEnum : LANG_ENUM.values()) {
			String translationId = this.getTranslationId(langEnum);
			currentTranslationIdMap.put(langEnum,
					XMSystemUtil.isEmpty(translationId) ? CommonConstants.EMPTY_STR : translationId);

			String subject = this.getSubject(langEnum);
			currentSubjectMap.put(langEnum,
					XMSystemUtil.isEmpty(subject) ? CommonConstants.EMPTY_STR : subject);

			String message = this.getMessage(langEnum);
			currentMessageMap.put(langEnum, XMSystemUtil.isEmpty(message) ? CommonConstants.EMPTY_STR : message);
		}

		
		/*String currentMessage = XMSystemUtil.isEmpty(this.getMessage()) ? CommonConstants.EMPTY_STR : this.getMessage();
		String currentSubject = XMSystemUtil.isEmpty(this.getSubject()) ? CommonConstants.EMPTY_STR : this.getSubject();*/
		Date currentEndDateTime = this.getExpiryDateTime();
		Date currentStartDateTime = this.getStartDateTime();
		boolean currentIsPopup = this.isPopup();
		Map<String, String> currentObjectIdMap = this.getObjectIdMap();
		
		Map<String, List<Map<String, String>>> liveMsgToMap = this.getLiveMsgToMap();
		Map<String, List<Map<String, String>>> currentLiveMsgToMap = new HashMap<>();
		for (Entry<String, List<Map<String, String>>> entry : liveMsgToMap.entrySet()) {
			 List<Map<String, String>> currentlist= new ArrayList<>(entry.getValue());
			 currentLiveMsgToMap.put(entry.getKey(), currentlist);
		}
		
		if (update) {
			clonedLiveMsg = updateThisObject;
		} else {
			clonedLiveMsg = new LiveMessage(currentLiveMsgId, currentName, currentIsPopup,
					CommonConstants.OPERATIONMODE.VIEW);
		}
		clonedLiveMsg.setLiveMsgId(currentLiveMsgId);
		clonedLiveMsg.setName(currentName);
		clonedLiveMsg.setMessage(currentMessage);
		clonedLiveMsg.setSubject(currentSubject);
		clonedLiveMsg.setMessageMap(currentMessageMap);
		clonedLiveMsg.setSubjectMap(currentSubjectMap);
		clonedLiveMsg.setTranslationIdMap(currentTranslationIdMap);
		clonedLiveMsg.setExpiryDateTime(currentEndDateTime);
		clonedLiveMsg.setStartDateTime(currentStartDateTime);
		clonedLiveMsg.setPopup(currentIsPopup);
		clonedLiveMsg.setLiveMsgToMap(currentLiveMsgToMap);
		clonedLiveMsg.setObjectIdMap(currentObjectIdMap);
		return clonedLiveMsg;

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.BeanModel#
	 * propertyChange(java.beans.PropertyChangeEvent)
	 */
	@Override
	public void propertyChange(PropertyChangeEvent event) {
		this.propertyChangeSupport.firePropertyChange(event.getPropertyName(), event.getOldValue(),
				event.getNewValue());
	}
}
