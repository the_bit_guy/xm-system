 
package com.magna.xmsystem.xmadmin.ui.handlers;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.e4.core.contexts.ContextInjectionFactory;
import org.eclipse.e4.core.contexts.IEclipseContext;
import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.core.services.nls.Translation;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.client.ResourceAccessException;

import com.magna.xmbackend.entities.UserLdapAccountStatusTbl;
import com.magna.xmbackend.vo.userAccountStatus.UserAccountStatusResponse;
import com.magna.xmsystem.dependencies.customExceptions.UnauthorizedAccessException;
import com.magna.xmsystem.ui.controls.widgets.CustomMessageDialog;
import com.magna.xmsystem.xmadmin.message.Message;
import com.magna.xmsystem.xmadmin.restclient.users.UserAccountStatusController;
import com.magna.xmsystem.xmadmin.ui.userreviewdialogs.UserReviewDialog;
import com.magna.xmsystem.xmadmin.util.XMAdminUtil;

/**
 * The Class UserReviewHandler.
 */
public class UserReviewHandler {
	
	/** The Constant LOGGER. */
	private static final Logger LOGGER = LoggerFactory.getLogger(UserReviewHandler.class);
	
	/**
	 * Inject {@link IEclipseContext}
	 */
	@Inject
	private IEclipseContext eclipseContext;
	
	/** Member variable for {@link Message}. */
	@Inject
	@Translation
	private Message messages;
	
	@Execute
	public void execute() {

		Job userReviewJob = new Job("UserReview...") {

			@Override
			protected IStatus run(IProgressMonitor monitor) {
				monitor.beginTask("UserReview..", 100);
				monitor.worked(30);
				try {
					UserAccountStatusController userAccountStatusController = new UserAccountStatusController();
					UserAccountStatusResponse userAccountStatusResponse = userAccountStatusController
							.getAllUsersAccountStatus();
					Iterable<UserLdapAccountStatusTbl> userAccountStatusTbls = userAccountStatusResponse
							.getUserLdapAccountStatusTbls();
					List<UserLdapAccountStatusTbl> userAccountStatusTblList = new ArrayList<>();
					for (UserLdapAccountStatusTbl userAccountStatusTbl : userAccountStatusTbls) {
						userAccountStatusTblList.add(userAccountStatusTbl);
					}
					if (userAccountStatusTblList.size() > 0) {

						Display.getDefault().syncExec(new Runnable() {
							public void run() {
								eclipseContext.set(Shell.class,
										XMAdminUtil.getInstance().getAdminTree().getTree().getShell());
								UserReviewDialog dialog = ContextInjectionFactory.make(UserReviewDialog.class,
										eclipseContext);
								dialog.open();
							}
						});
					} else {
						Display.getDefault().syncExec(new Runnable() {
							public void run() {
								CustomMessageDialog.openError(Display.getCurrent().getActiveShell(),
										messages.userReviewErrorDialogTitle, messages.userReviewErrorDialogMsg);
							}
						});
					}
				} catch (UnauthorizedAccessException e) {
					LOGGER.error("Current user is Unauthorized " + e);
				} catch (Exception e) {
					if (e instanceof ResourceAccessException) {
						Display.getDefault().asyncExec(new Runnable() {
							@Override
							public void run() {
								CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
										messages.errorDialogTitile, messages.serverNotReachable);
							}
						});
					}
					LOGGER.error(e.getMessage());
				}
				monitor.worked(70);
				return Status.OK_STATUS;
			}
		};
		userReviewJob.setUser(true);
		userReviewJob.schedule();
	}
}