package com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import org.eclipse.swt.widgets.Display;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.client.ResourceAccessException;

import com.magna.xmbackend.entities.AdminAreaProjectRelTbl;
import com.magna.xmbackend.entities.AdminAreaTranslationTbl;
import com.magna.xmbackend.entities.AdminAreasTbl;
import com.magna.xmbackend.entities.BaseAppTranslationTbl;
import com.magna.xmbackend.entities.BaseApplicationsTbl;
import com.magna.xmbackend.entities.DirectoryTbl;
import com.magna.xmbackend.entities.DirectoryTranslationTbl;
import com.magna.xmbackend.entities.EmailTemplateTbl;
import com.magna.xmbackend.entities.GroupTranslationTbl;
import com.magna.xmbackend.entities.GroupsTbl;
import com.magna.xmbackend.entities.IconsTbl;
import com.magna.xmbackend.entities.ProjectAppTranslationTbl;
import com.magna.xmbackend.entities.ProjectApplicationsTbl;
import com.magna.xmbackend.entities.ProjectStartAppRelTbl;
import com.magna.xmbackend.entities.ProjectTranslationTbl;
import com.magna.xmbackend.entities.ProjectsTbl;
import com.magna.xmbackend.entities.RoleAdminAreaRelTbl;
import com.magna.xmbackend.entities.RoleUserRelTbl;
import com.magna.xmbackend.entities.RolesTbl;
import com.magna.xmbackend.entities.SiteAdminAreaRelTbl;
import com.magna.xmbackend.entities.SiteTranslationTbl;
import com.magna.xmbackend.entities.SitesTbl;
import com.magna.xmbackend.entities.StartAppTranslationTbl;
import com.magna.xmbackend.entities.StartApplicationsTbl;
import com.magna.xmbackend.entities.UserAppTranslationTbl;
import com.magna.xmbackend.entities.UserApplicationsTbl;
import com.magna.xmbackend.entities.UserProjAppRelTbl;
import com.magna.xmbackend.entities.UserProjectRelTbl;
import com.magna.xmbackend.entities.UserStartAppRelTbl;
import com.magna.xmbackend.entities.UserTranslationTbl;
import com.magna.xmbackend.entities.UserUserAppRelTbl;
import com.magna.xmbackend.entities.UsersTbl;
import com.magna.xmbackend.response.rel.adminareaproject.AdminAreaProjectRelationResponse;
import com.magna.xmbackend.response.rel.adminareaproject.AdminAreaProjectResponse;
import com.magna.xmbackend.response.rel.adminareaproject.AdminAreaProjectResponseWrapper;
import com.magna.xmbackend.response.rel.adminareaprojectapp.AdminAreaProjectAppResponse;
import com.magna.xmbackend.response.rel.adminareastartapp.AdminAreaStartAppRelation;
import com.magna.xmbackend.response.rel.adminareauserapp.AdminAreaUserAppRelation;
import com.magna.xmbackend.response.rel.userstartapp.UserStartAppRelation;
import com.magna.xmbackend.response.rel.useruserapp.UserUserAppRelResponseWrapper;
import com.magna.xmbackend.response.rel.useruserapp.UserUserAppRelation;
import com.magna.xmbackend.vo.adminMenu.AdminMenuConfigCustomeResponse;
import com.magna.xmbackend.vo.adminMenu.AdminMenuConfigCustomeResponseWrapper;
import com.magna.xmbackend.vo.directory.DirectoryRelResponse;
import com.magna.xmbackend.vo.directory.DirectoryRelResponseWrapper;
import com.magna.xmbackend.vo.enums.AdminMenuConfig;
import com.magna.xmbackend.vo.enums.DirectoryObjectType;
import com.magna.xmbackend.vo.enums.NotificationEventType;
import com.magna.xmbackend.vo.enums.UserRelationType;
import com.magna.xmbackend.vo.group.GroupRelBatchResponse;
import com.magna.xmbackend.vo.group.GroupRelResponse;
import com.magna.xmbackend.vo.group.GroupResponse;
import com.magna.xmbackend.vo.jpa.site.SiteAdminAreaRelIdWithAdminArea;
import com.magna.xmbackend.vo.jpa.site.SiteAdminAreaRelIdWithAdminAreaProjRel;
import com.magna.xmbackend.vo.jpa.site.SiteAdminAreaRelIdWithAdminAreaStartAppRel;
import com.magna.xmbackend.vo.jpa.site.SiteAdminAreaRelIdWithAdminAreaUsrAppRel;
import com.magna.xmbackend.vo.jpa.site.SiteResponse;
import com.magna.xmbackend.vo.liveMessage.LiveMessageConfigResponse;
import com.magna.xmbackend.vo.liveMessage.LiveMessageResponse;
import com.magna.xmbackend.vo.liveMessage.LiveMessageResponseWrapper;
import com.magna.xmbackend.vo.liveMessage.LiveMessageTranslation;
import com.magna.xmbackend.vo.notification.EmailTemplateResponse;
import com.magna.xmbackend.vo.notification.NotificationConfigResponse;
import com.magna.xmbackend.vo.rel.AdminAreaProjectRelIdWithProjectAppRel;
import com.magna.xmbackend.vo.rel.AdminAreaProjectRelIdWithProjectStartAppRel;
import com.magna.xmbackend.vo.rel.ProjectStartAppRelResponse;
import com.magna.xmsystem.dependencies.utils.LANG_ENUM;
import com.magna.xmsystem.dependencies.utils.XMSystemUtil;
import com.magna.xmsystem.ui.controls.widgets.CustomMessageDialog;
import com.magna.xmsystem.xmadmin.restclient.adminArea.AdminAreaController;
import com.magna.xmsystem.xmadmin.restclient.application.BaseAppController;
import com.magna.xmsystem.xmadmin.restclient.application.ProjectAppController;
import com.magna.xmsystem.xmadmin.restclient.application.StartAppController;
import com.magna.xmsystem.xmadmin.restclient.application.UserAppController;
import com.magna.xmsystem.xmadmin.restclient.directory.DirectoryController;
import com.magna.xmsystem.xmadmin.restclient.group.GroupController;
import com.magna.xmsystem.xmadmin.restclient.livemsg.LiveMessageController;
import com.magna.xmsystem.xmadmin.restclient.notify.NotificationController;
import com.magna.xmsystem.xmadmin.restclient.project.ProjectController;
import com.magna.xmsystem.xmadmin.restclient.relation.AdminAreaProjectAppRelController;
import com.magna.xmsystem.xmadmin.restclient.relation.AdminAreaProjectRelController;
import com.magna.xmsystem.xmadmin.restclient.relation.AdminAreaStartAppRelController;
import com.magna.xmsystem.xmadmin.restclient.relation.AdminAreaUserAppRelController;
import com.magna.xmsystem.xmadmin.restclient.relation.AdminMenuConfigRelController;
import com.magna.xmsystem.xmadmin.restclient.relation.DirectoryRelController;
import com.magna.xmsystem.xmadmin.restclient.relation.GroupRelController;
import com.magna.xmsystem.xmadmin.restclient.relation.ProjectStartAppRelController;
import com.magna.xmsystem.xmadmin.restclient.relation.RoleAdminAreaRelController;
import com.magna.xmsystem.xmadmin.restclient.relation.RoleUserRelController;
import com.magna.xmsystem.xmadmin.restclient.relation.SiteAdminAreaRelController;
import com.magna.xmsystem.xmadmin.restclient.relation.UserProjectAppRelController;
import com.magna.xmsystem.xmadmin.restclient.relation.UserProjectRelController;
import com.magna.xmsystem.xmadmin.restclient.relation.UserStartAppRelController;
import com.magna.xmsystem.xmadmin.restclient.relation.UserUserAppRelController;
import com.magna.xmsystem.xmadmin.restclient.role.RoleController;
import com.magna.xmsystem.xmadmin.restclient.site.SiteController;
import com.magna.xmsystem.xmadmin.restclient.users.UserController;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.adminarea.AdminAreaProjectAppFixed;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.adminarea.AdminAreaProjectAppNotFixed;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.adminarea.AdminAreaProjectAppProtected;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.adminarea.AdminAreaProjectApplications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.adminarea.AdminAreaProjectChild;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.adminarea.AdminAreaProjects;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.adminarea.AdminAreaUserAppFixed;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.adminarea.AdminAreaUserAppNotFixed;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.adminarea.AdminAreaUserAppProtected;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.adminarea.AdminAreaUserApplications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.adminarea.AdministrationArea;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.adminarea.AdministrationAreas;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.baseapplicationmodel.BaseAppProjectApplications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.baseapplicationmodel.BaseAppStartApplications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.baseapplicationmodel.BaseAppUserApplications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.baseapplicationmodel.BaseApplication;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.baseapplicationmodel.BaseApplications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.projectapplicationmodel.ProjectAppAdminAreaChild;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.projectapplicationmodel.ProjectAppAdminAreaProjects;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.projectapplicationmodel.ProjectAppAdminAreas;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.projectapplicationmodel.ProjectAppGroups;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.projectapplicationmodel.ProjectAppUsers;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.projectapplicationmodel.ProjectApplication;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.projectapplicationmodel.ProjectApplicationChild;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.projectapplicationmodel.ProjectApplications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.startapplicationmodel.StartAppProjects;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.startapplicationmodel.StartAppUsers;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.startapplicationmodel.StartApplication;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.startapplicationmodel.StartApplications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.userapplicationmodel.UserAppAdminAreas;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.userapplicationmodel.UserAppGroups;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.userapplicationmodel.UserAppUsers;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.userapplicationmodel.UserApplication;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.userapplicationmodel.UserApplicationChild;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.userapplicationmodel.UserApplications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.adminconf.AdminProjectApps;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.adminconf.AdminUserApps;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.adminconf.AdminUsers;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.icon.Icon;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.livemsgconfig.LiveMessage;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.livemsgconfig.LiveMessages;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.livemsgconfig.LiveMsgToPattern;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.notification.NotificationTemplate;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.notification.NotificationTemplates;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.notification.Notifications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.notification.ProjectActivateEvt;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.notification.ProjectActivateEvtAction;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.notification.ProjectCreateEvt;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.notification.ProjectCreateEvtAction;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.notification.ProjectDeactivateEvt;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.notification.ProjectDeactivateEvtAction;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.notification.ProjectDeleteEvt;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.notification.ProjectDeleteEvtAction;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.notification.UserProjectRelAssignEvt;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.notification.UserProjectRelAssignEvtAction;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.roles.Role;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.roles.RoleScopeObjectChild;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.roles.RoleScopeObjectUsers;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.roles.RoleScopeObjects;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.roles.RoleUsers;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.roles.Roles;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.directory.Directories;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.directory.Directory;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.directory.DirectoryApplications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.directory.DirectoryProjectApplications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.directory.DirectoryProjects;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.directory.DirectoryUserApplications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.directory.DirectoryUsers;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.group.projectappgroup.ProjectAppGroupProjectApps;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.group.projectappgroup.ProjectApplicationGroup;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.group.projectappgroup.ProjectApplicationGroups;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.group.projectgroup.ProjectGroupModel;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.group.projectgroup.ProjectGroupProjects;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.group.projectgroup.ProjectGroupsModel;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.group.userappgroup.UserAppGroupUserApps;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.group.userappgroup.UserApplicationGroup;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.group.userappgroup.UserApplicationGroups;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.group.usergroup.UserGroupModel;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.group.usergroup.UserGroupUsers;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.group.usergroup.UserGroupsModel;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.projects.Project;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.projects.ProjectAdminAreaChild;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.projects.ProjectAdminAreaProjectAppFixed;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.projects.ProjectAdminAreaProjectAppNotFixed;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.projects.ProjectAdminAreaProjectAppProtected;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.projects.ProjectAdminAreaProjectApplications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.projects.ProjectAdminAreas;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.projects.ProjectProjectApplications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.projects.ProjectUserAAProjectAppAllowed;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.projects.ProjectUserAAProjectAppForbidden;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.projects.ProjectUserAAProjectApplications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.projects.ProjectUserAdminAreaChild;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.projects.ProjectUserAdminAreas;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.projects.ProjectUserChild;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.projects.ProjectUsers;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.projects.Projects;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.refs.RelationObj;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.sitemodel.Site;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.sitemodel.SiteAdminAreaProjectApplications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.sitemodel.SiteAdminAreaProjectChild;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.sitemodel.SiteAdminAreaProjects;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.sitemodel.SiteAdminAreaUserAppFixed;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.sitemodel.SiteAdminAreaUserAppNotFixed;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.sitemodel.SiteAdminAreaUserAppProtected;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.sitemodel.SiteAdminAreaUserApplications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.sitemodel.SiteAdminProjectAppFixed;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.sitemodel.SiteAdminProjectAppNotFixed;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.sitemodel.SiteAdminProjectAppProtected;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.sitemodel.SiteAdministrationChild;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.sitemodel.SiteAdministrations;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.sitemodel.Sites;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.users.User;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.users.UserAAUserAppAllowed;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.users.UserAAUserAppForbidden;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.users.UserAAUserApplications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.users.UserAdminAreaChild;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.users.UserAdminAreas;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.users.UserProjectAAProjectAppAllowed;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.users.UserProjectAAProjectAppForbidden;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.users.UserProjectAAProjectApplications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.users.UserProjectAdminAreaChild;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.users.UserProjectAdminAreas;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.users.UserProjectChild;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.users.UserProjects;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.users.Users;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.users.UsersNameAlphabet;
import com.magna.xmsystem.xmadmin.util.CommonConstants;

/**
 * Class for Admin tree data load.
 *
 * @author Chiranjeevi.Akula
 */
public final class AdminTreeDataLoad {

	/** The Constant LOGGER. */
	private static final Logger LOGGER = LoggerFactory.getLogger(AdminTreeDataLoad.class);

	/** Member variable 'this ref' for {@link AdminTreeDataLoad}. */
	private static AdminTreeDataLoad thisRef;

	/** Member variable 'factory instance' for {@link AdminTreeFactory}. */
	private AdminTreeFactory factoryInstance;

	/**
	 * Constructor for AdminTreeDataLoad Class.
	 */
	private AdminTreeDataLoad() {
		this.factoryInstance = AdminTreeFactory.getInstance();
	}

	/**
	 * Gets the single instance of AdminTreeDataLoad.
	 *
	 * @return single instance of AdminTreeDataLoad
	 */
	public static synchronized AdminTreeDataLoad getInstance() {
		if (thisRef == null) {
			thisRef = new AdminTreeDataLoad();
		}
		return thisRef;
	}

	/**
	 * Method for Load sites from service.
	 */
	public void loadSitesFromService() {
		final File iconFolder = XMSystemUtil.getXMSystemServerIconsFolder();
		try {
			Sites sites = factoryInstance.getSites();

			List<String> dbIds = new ArrayList<>();
			List<String> deactiveObjs = new ArrayList<>();
			final SiteController siteController = new SiteController();
			List<SitesTbl> siteResponseList = siteController.getAllSites(true);

			Map<String, IAdminTreeChild> sitesChildren = sites.getSitesChildren();

			for (SitesTbl sitesTblVo : siteResponseList) {
				final String id = sitesTblVo.getSiteId();
				final String name = sitesTblVo.getName();
				boolean isActive = com.magna.xmbackend.vo.enums.Status.ACTIVE.name().equals(sitesTblVo.getStatus())
						? true : false;

				IconsTbl iconTbl = sitesTblVo.getIconId();
				Icon icon = new Icon(iconTbl.getIconId(), iconTbl.getIconName(),
						iconFolder + File.separator + iconTbl.getIconName(), iconTbl.getIconType());

				Map<LANG_ENUM, String> descriptionMap = new HashMap<>();
				Map<LANG_ENUM, String> remarksMap = new HashMap<>();
				Map<LANG_ENUM, String> translationMap = new HashMap<>();

				Collection<SiteTranslationTbl> siteTranslationTblList = sitesTblVo.getSiteTranslationTblCollection();
				for (SiteTranslationTbl siteTranslationTbl : siteTranslationTblList) {
					LANG_ENUM langEnum = LANG_ENUM.getLangEnum(siteTranslationTbl.getLanguageCode().getLanguageCode());
					String translationId = siteTranslationTbl.getSiteTranslationId();
					translationMap.put(langEnum, translationId);
					descriptionMap.put(langEnum, siteTranslationTbl.getDescription());
					remarksMap.put(langEnum, siteTranslationTbl.getRemarks());
				}

				if (!sitesChildren.containsKey(id)) {
					Site site = new Site(id, name, isActive, descriptionMap, remarksMap, icon,
							CommonConstants.OPERATIONMODE.VIEW);
					site.setTranslationIdMap(translationMap);
					site.getSiteChildren().put(SiteAdministrations.class.getName(), new SiteAdministrations(site));

					if (factoryInstance.isHideInActiveItems()) {
						if (isActive) {
							sites.add(id, site);
						}
					} else {
						sites.add(id, site);
					}
				} else {
					IAdminTreeChild iAdminTreeChild = sitesChildren.get(id);
					if (iAdminTreeChild instanceof Site) {
						Site site = (Site) iAdminTreeChild;
						site.setName(name);
						site.setActive(isActive);
						site.setDescriptionMap(descriptionMap);
						site.setRemarksMap(remarksMap);
						site.setIcon(icon);
					}
				}
				dbIds.add(id);
			}
			Set<String> keySet = sitesChildren.keySet();
			for (String key : keySet) {
				if (!dbIds.contains(key)) {
					deactiveObjs.add(key);
				}
			}
			for (String removeObj : deactiveObjs)
				sites.remove(removeObj);
		} catch (Exception e) {
			if (e instanceof ResourceAccessException) {
				Display.getDefault().asyncExec(new Runnable() {
					@Override
					public void run() {
						CustomMessageDialog.openError(Display.getDefault().getActiveShell(), "Error",
								"Server not reachable");
					}
				});
			}
			LOGGER.error("Exeception while getting site objects! " + e);
		}
	}

	/**
	 * Method for Load admin areas from service.
	 */
	public void loadAdminAreasFromService() {
		final File iconFolder = XMSystemUtil.getXMSystemServerIconsFolder();
		try {
			AdministrationAreas administrationAreas = factoryInstance.getAdministrationAreas();
			Map<String, IAdminTreeChild> adminAreasChildren = administrationAreas.getAdminstrationAreasChildren();

			List<String> dbIds = new ArrayList<>();
			List<String> deactiveObjs = new ArrayList<>();
			final AdminAreaController adminAreaController = new AdminAreaController();
			List<AdminAreasTbl> adminAreaResponseList = adminAreaController.getAllAdminAreas(true);

			for (AdminAreasTbl adminAreasTblVo : adminAreaResponseList) {
				final String id = adminAreasTblVo.getAdminAreaId();
				final String name = adminAreasTblVo.getName();
				final String contact = adminAreasTblVo.getHotlineContactNumber();
				final String email = adminAreasTblVo.getHotlineContactEmail();
				Long singletonAppTimeout = adminAreasTblVo.getSingletonAppTimeout();
				boolean isActive = com.magna.xmbackend.vo.enums.Status.ACTIVE.name().equals(adminAreasTblVo.getStatus())
						? true : false;

				IconsTbl iconTbl = adminAreasTblVo.getIconId();
				Icon icon = new Icon(iconTbl.getIconId(), iconTbl.getIconName(),
						iconFolder + File.separator + iconTbl.getIconName(), iconTbl.getIconType());

				Map<LANG_ENUM, String> descriptionMap = new HashMap<>();
				Map<LANG_ENUM, String> remarksMap = new HashMap<>();
				Map<LANG_ENUM, String> translationMap = new HashMap<>();

				Collection<AdminAreaTranslationTbl> adminAreaTranslationTblList = adminAreasTblVo
						.getAdminAreaTranslationTblCollection();
				for (AdminAreaTranslationTbl adminAreaTranslationTbl : adminAreaTranslationTblList) {
					LANG_ENUM langEnum = LANG_ENUM
							.getLangEnum(adminAreaTranslationTbl.getLanguageCode().getLanguageCode());
					String translationId = adminAreaTranslationTbl.getAdminAreaTranslationId();
					translationMap.put(langEnum, translationId);
					descriptionMap.put(langEnum, adminAreaTranslationTbl.getDescription());
					remarksMap.put(langEnum, adminAreaTranslationTbl.getRemarks());
				}
				if (!adminAreasChildren.containsKey(id)) {
					AdministrationArea adminArea = new AdministrationArea(id, name, isActive, descriptionMap, contact,
							email, singletonAppTimeout, remarksMap, icon, CommonConstants.OPERATIONMODE.VIEW);

					adminArea.setTranslationIdMap(translationMap);

					SiteController controller = new SiteController();
					SiteResponse siteResponse = controller.getSitesByAAId(id);
					Iterable<SitesTbl> sitesTbls = siteResponse.getSitesTbls();

					for (SitesTbl sitesTbl : sitesTbls) {
						List<SiteAdminAreaRelIdWithAdminArea> siteAdminRelLidt = controller
								.getAllAdminAreasBySiteId(sitesTbl.getSiteId());
						String siteName = sitesTbl.getName();
						for (SiteAdminAreaRelIdWithAdminArea siteAdminAreaRelTblVo : siteAdminRelLidt) {
							String siteAdminAreaRelId = siteAdminAreaRelTblVo.getSiteAdminAreaRelId();
							adminArea.setRelationId(siteAdminAreaRelId);
							adminArea.setRelatedSiteName(siteName);
						}
					}

					if (factoryInstance.isHideInActiveItems()) {
						if (isActive) {
							administrationAreas.add(id, adminArea);
						}
					} else {
						administrationAreas.add(id, adminArea);
					}
				} else {
					IAdminTreeChild iAdminTreeChild = adminAreasChildren.get(id);
					if (iAdminTreeChild instanceof AdministrationArea) {
						AdministrationArea adminAre = (AdministrationArea) iAdminTreeChild;
						adminAre.setName(name);
						adminAre.setActive(isActive);
						adminAre.setDescriptionMap(descriptionMap);
						adminAre.setHotlineContactNumber(contact == null ? CommonConstants.EMPTY_STR : contact);
						adminAre.setHotlineContactEmail(email == null ? CommonConstants.EMPTY_STR : email);
						adminAre.setSingletonAppTimeout(singletonAppTimeout);
						adminAre.setRemarksMap(remarksMap);
						adminAre.setIcon(icon);
					}
				}
				dbIds.add(id);
			}
			Set<String> keySet = adminAreasChildren.keySet();
			for (String key : keySet) {
				if (!dbIds.contains(key)) {
					deactiveObjs.add(key);

				}
			}
			for (String removeObj : deactiveObjs)
				administrationAreas.remove(removeObj);
		} catch (Exception e) {
			if (e instanceof ResourceAccessException) {
				Display.getDefault().asyncExec(new Runnable() {
					@Override
					public void run() {
						CustomMessageDialog.openError(Display.getDefault().getActiveShell(), "Error",
								"Server not reachable");
					}
				});
			}
			LOGGER.error("Exeception while getting Adminstration Areas objects! " + e);
		}
	}

	/**
	 * Method for Load projects from service.
	 */
	public void loadProjectsFromService() {
		final File iconFolder = XMSystemUtil.getXMSystemServerIconsFolder();
		try {
			Projects projects = factoryInstance.getProjects();
			Map<String, IAdminTreeChild> projectsChildren = projects.getProjectsChildren();

			List<String> dbIds = new ArrayList<>();
			List<String> deactiveObjs = new ArrayList<>();
			final ProjectController projectController = new ProjectController();
			List<ProjectsTbl> projectResponseList = projectController.getAllProjects(true);

			for (ProjectsTbl projectsTblVo : projectResponseList) {
				final String id = projectsTblVo.getProjectId();
				final String name = projectsTblVo.getName();
				boolean isActive = com.magna.xmbackend.vo.enums.Status.ACTIVE.name().equals(projectsTblVo.getStatus())
						? true : false;

				IconsTbl iconTbl = projectsTblVo.getIconId();
				Icon icon = new Icon(iconTbl.getIconId(), iconTbl.getIconName(),
						iconFolder + File.separator + iconTbl.getIconName(), iconTbl.getIconType());

				Map<LANG_ENUM, String> descriptionMap = new HashMap<>();
				Map<LANG_ENUM, String> remarksMap = new HashMap<>();
				Map<LANG_ENUM, String> translationMap = new HashMap<>();

				Collection<ProjectTranslationTbl> projectTranslationTblList = projectsTblVo
						.getProjectTranslationTblCollection();
				for (ProjectTranslationTbl projectTranslationTbl : projectTranslationTblList) {
					LANG_ENUM langEnum = LANG_ENUM
							.getLangEnum(projectTranslationTbl.getLanguageCode().getLanguageCode());
					String translationId = projectTranslationTbl.getProjectTranslationId();
					translationMap.put(langEnum, translationId);
					descriptionMap.put(langEnum, projectTranslationTbl.getDescription());
					remarksMap.put(langEnum, projectTranslationTbl.getRemarks());
				}

				if (!projectsChildren.containsKey(id)) {
					Project project = new Project(id, name, isActive, descriptionMap, remarksMap, icon,
							CommonConstants.OPERATIONMODE.VIEW);

					project.setTranslationIdMap(translationMap);

					if (factoryInstance.isHideInActiveItems()) {
						if (isActive) {
							projects.add(id, project);
						}
					} else {
						projects.add(id, project);
					}
				} else {
					IAdminTreeChild iAdminTreeChild = projectsChildren.get(id);
					if (iAdminTreeChild instanceof Project) {
						Project project = (Project) iAdminTreeChild;
						project.setName(name);
						project.setActive(isActive);
						project.setDescriptionMap(descriptionMap);
						project.setRemarksMap(remarksMap);
						project.setProjectIcon(icon);
					}
				}
				dbIds.add(id);
			}
			Set<String> keySet = projectsChildren.keySet();
			for (String key : keySet) {
				if (!dbIds.contains(key)) {
					deactiveObjs.add(key);
				}
			}
			for (String removeObj : deactiveObjs)
				projects.remove(removeObj);
		} catch (Exception e) {
			if (e instanceof ResourceAccessException) {
				Display.getDefault().asyncExec(new Runnable() {
					@Override
					public void run() {
						CustomMessageDialog.openError(Display.getDefault().getActiveShell(), "Error",
								"Server not reachable");
					}
				});
			}
			LOGGER.error("Exeception while getting  Project objects! " + e);
		}
	}

	/**
	 * Method for Load users from service.
	 */
	public void loadUsersFromService() {
		final File iconFolder = XMSystemUtil.getXMSystemServerIconsFolder();
		try {
			final UserController userController = new UserController();
			List<UsersTbl> userResponseList = userController.getAllUsers(true);

			if (userResponseList != null && !userResponseList.isEmpty()) {
				Users users = factoryInstance.getUsers();
				Map<String, IAdminTreeChild> usersChildren = users.getUsersChildren();
				Set<String> keySet = usersChildren.keySet();
				for (String key : keySet) {
					UsersNameAlphabet usersNameAlphabet = (UsersNameAlphabet) usersChildren.get(key);
					String firstChar = usersNameAlphabet.getFirstChar().replace("...", "");
					Map<String, IAdminTreeChild> userChildren = usersNameAlphabet.getUsersChildren();
					List<String> dbIds = new ArrayList<>();
					List<String> deactiveObjs = new ArrayList<>();

					for (UsersTbl usersTblVo : userResponseList) {
						String userName = usersTblVo.getUsername();
						if (firstChar.equalsIgnoreCase(userName.charAt(0) + "")) {
							String id = usersTblVo.getUserId();
							String fullName = usersTblVo.getFullName();
							String userManager = usersTblVo.getManager();
							String userEmailId = usersTblVo.getEmailId();
							String userTelephone = usersTblVo.getTelephoneNumber();
							String userDept = usersTblVo.getDepartment();
							boolean isActive = com.magna.xmbackend.vo.enums.Status.ACTIVE.name()
									.equals(usersTblVo.getStatus()) ? true : false;

							IconsTbl iconTbl = usersTblVo.getIconId();
							Icon icon = new Icon(iconTbl.getIconId(), iconTbl.getIconName(),
									iconFolder + File.separator + iconTbl.getIconName(), iconTbl.getIconType());

							Map<LANG_ENUM, String> descriptionMap = new HashMap<>();
							Map<LANG_ENUM, String> remarksMap = new HashMap<>();
							Map<LANG_ENUM, String> translationIdMap = new HashMap<>();

							Collection<UserTranslationTbl> userTranslationTblList = usersTblVo
									.getUserTranslationTblCollection();
							for (UserTranslationTbl userTranslationTbl : userTranslationTblList) {
								LANG_ENUM langEnum = LANG_ENUM
										.getLangEnum(userTranslationTbl.getLanguageCode().getLanguageCode());
								String translationId = userTranslationTbl.getUserTranslationId();
								translationIdMap.put(langEnum, translationId);
								descriptionMap.put(langEnum, userTranslationTbl.getDescription());
								remarksMap.put(langEnum, userTranslationTbl.getRemarks());
							}
							if (!userChildren.containsKey(id)) {
								User user = new User(id, userName, fullName, userManager, isActive, userEmailId, userTelephone,
										userDept, descriptionMap, remarksMap, icon, CommonConstants.OPERATIONMODE.VIEW);

								user.setTranslationIdMap(translationIdMap);

								if (factoryInstance.isHideInActiveItems()) {
									if (isActive) {
										usersNameAlphabet.add(id, user);
									}
								} else {
									usersNameAlphabet.add(id, user);
								}
							} else {
								IAdminTreeChild iAdminTreeChild = userChildren.get(id);
								if (iAdminTreeChild instanceof User) {
									User user = (User) iAdminTreeChild;
									user.setName(userName);
									user.setActive(isActive);
									user.setEmail(userEmailId == null ? CommonConstants.EMPTY_STR : userEmailId);
									user.setTelePhoneNum(userTelephone == null ? CommonConstants.EMPTY_STR : userTelephone);
									user.setDepartment(userDept == null ? CommonConstants.EMPTY_STR : userDept);
									user.setDescriptionMap(descriptionMap);
									user.setRemarksMap(remarksMap);
									user.setIcon(icon);
								}
							}
							dbIds.add(id);
						}
					}
					Set<String> userChildrenkeySet = userChildren.keySet();
					for (String userChildrenkey : userChildrenkeySet) {
						if (!dbIds.contains(userChildrenkey)) {
							deactiveObjs.add(userChildrenkey);
						}
					}
					for (String removeObj : deactiveObjs)
						userChildren.remove(removeObj);
				}
			}
		} catch (Exception e) {
			if (e instanceof ResourceAccessException) {
				Display.getDefault().asyncExec(new Runnable() {
					@Override
					public void run() {
						CustomMessageDialog.openError(Display.getDefault().getActiveShell(), "Error",
								"Server not reachable");
					}
				});
			}
			LOGGER.error("Exeception while getting User objects! " + e);
		}
	}

	/**
	 * Method for Load users based on first char.
	 *
	 * @param usersNameAlphabet
	 *            {@link UsersNameAlphabet}
	 */
	public void loadUsersBasedOnFirstChar(UsersNameAlphabet usersNameAlphabet) {
		final File iconFolder = XMSystemUtil.getXMSystemServerIconsFolder();
		try {
			String firstChar = usersNameAlphabet.getFirstChar().replace("...", "");
			Map<String, IAdminTreeChild> usersChildren = usersNameAlphabet.getUsersChildren();
			List<String> dbIds = new ArrayList<>();
			List<String> deactiveObjs = new ArrayList<>();

			final UserController userController = new UserController();
			List<UsersTbl> userResponseList = userController.getUsers(firstChar);

			for (UsersTbl usersTblVo : userResponseList) {
				String id = usersTblVo.getUserId();
				String userName = usersTblVo.getUsername();
				String fullName = usersTblVo.getFullName();
				String manager = usersTblVo.getManager();
				String userEmailId = usersTblVo.getEmailId();
				String userTelephone = usersTblVo.getTelephoneNumber();
				String userDept = usersTblVo.getDepartment();
				boolean isActive = com.magna.xmbackend.vo.enums.Status.ACTIVE.name().equals(usersTblVo.getStatus())
						? true : false;

				IconsTbl iconTbl = usersTblVo.getIconId();
				Icon icon = new Icon(iconTbl.getIconId(), iconTbl.getIconName(),
						iconFolder + File.separator + iconTbl.getIconName(), iconTbl.getIconType());

				Map<LANG_ENUM, String> descriptionMap = new HashMap<>();
				Map<LANG_ENUM, String> remarksMap = new HashMap<>();
				Map<LANG_ENUM, String> translationIdMap = new HashMap<>();

				Collection<UserTranslationTbl> userTranslationTblList = usersTblVo.getUserTranslationTblCollection();
				for (UserTranslationTbl userTranslationTbl : userTranslationTblList) {
					LANG_ENUM langEnum = LANG_ENUM.getLangEnum(userTranslationTbl.getLanguageCode().getLanguageCode());
					String translationId = userTranslationTbl.getUserTranslationId();
					translationIdMap.put(langEnum, translationId);
					descriptionMap.put(langEnum, userTranslationTbl.getDescription());
					remarksMap.put(langEnum, userTranslationTbl.getRemarks());
				}
				if (!usersChildren.containsKey(id)) {
					User user = new User(id, userName, fullName, manager, isActive, userEmailId, userTelephone, userDept,
							descriptionMap, remarksMap, icon, CommonConstants.OPERATIONMODE.VIEW);

					user.setTranslationIdMap(translationIdMap);

					if (factoryInstance.isHideInActiveItems()) {
						if (isActive) {
							usersNameAlphabet.add(id, user);
						}
					} else {
						usersNameAlphabet.add(id, user);
					}
				} else {
					IAdminTreeChild iAdminTreeChild = usersChildren.get(id);
					if (iAdminTreeChild instanceof User) {
						User user = (User) iAdminTreeChild;
						user.setName(userName);
						user.setActive(isActive);
						user.setEmail(userEmailId == null ? CommonConstants.EMPTY_STR : userEmailId);
						user.setTelePhoneNum(userTelephone == null ? CommonConstants.EMPTY_STR : userTelephone);
						user.setDepartment(userDept == null ? CommonConstants.EMPTY_STR : userDept);
						user.setDescriptionMap(descriptionMap);
						user.setRemarksMap(remarksMap);
						user.setIcon(icon);
					}
				}
				dbIds.add(id);
			}
			Set<String> keySet = usersChildren.keySet();
			for (String key : keySet) {
				if (!dbIds.contains(key)) {
					deactiveObjs.add(key);
				}
			}
			for (String removeObj : deactiveObjs){
				usersChildren.remove(removeObj);
			}
		} catch (Exception e) {
			if (e instanceof ResourceAccessException) {
				Display.getDefault().asyncExec(new Runnable() {
					@Override
					public void run() {
						CustomMessageDialog.openError(Display.getDefault().getActiveShell(), "Error",
								"Server not reachable");
					}
				});
			}
			LOGGER.error("Exeception while getting User objects! " + e);
		}
	}

	/**
	 * Method for Load all applications.
	 */
	public void loadAllApplications() {
		loadUserAppFromService();
		loadProjectAppFromService();
		loadStartAppFromService();
		loadBaseAppFromService();
	}

	/**
	 * Method for Load base app from service.
	 */
	public void loadBaseAppFromService() {
		final File iconFolder = XMSystemUtil.getXMSystemServerIconsFolder();
		try {
			BaseApplications baseApplications = factoryInstance.getBaseApplications();
			List<String> dbIds = new ArrayList<>();
			List<String> deactiveObjs = new ArrayList<>();

			final BaseAppController baseAppController = new BaseAppController();
			List<BaseApplicationsTbl> baseApplicationsTblList = baseAppController.getAllBaseApps(true);

			Map<String, IAdminTreeChild> baseAppChildren = baseApplications.getBaseApplications();

			for (BaseApplicationsTbl baseApplicationsTblVo : baseApplicationsTblList) {
				final String id = baseApplicationsTblVo.getBaseApplicationId();
				final String baseAppName = baseApplicationsTblVo.getName();
				boolean isActive = com.magna.xmbackend.vo.enums.Status.ACTIVE.name()
						.equals(baseApplicationsTblVo.getStatus()) ? true : false;
				String program = baseApplicationsTblVo.getProgram();

				Map<String, Boolean> platformMap = new HashMap<>();
				String platforms = baseApplicationsTblVo.getPlatforms();
				if (!XMSystemUtil.isEmpty(platforms)) {
					String[] platformArray = platforms.split(";");
					for (String paltform : platformArray) {
						if (BaseApplication.SUPPORTED_PLATFORMS.contains(paltform)) {
							platformMap.put(paltform, true);
						}
					}
				}

				IconsTbl iconTbl = baseApplicationsTblVo.getIconId();
				Icon icon = new Icon(iconTbl.getIconId(), iconTbl.getIconName(),
						iconFolder + File.separator + iconTbl.getIconName(), iconTbl.getIconType());

				Map<LANG_ENUM, String> descriptionMap = new HashMap<>();
				Map<LANG_ENUM, String> remarksMap = new HashMap<>();
				Map<LANG_ENUM, String> translationMap = new HashMap<>();

				Collection<BaseAppTranslationTbl> baseAppTranslationTblList = baseApplicationsTblVo
						.getBaseAppTranslationTblCollection();
				for (BaseAppTranslationTbl baseAppTranslationTbl : baseAppTranslationTblList) {
					LANG_ENUM langEnum = LANG_ENUM
							.getLangEnum(baseAppTranslationTbl.getLanguageCode().getLanguageCode());
					String translationId = baseAppTranslationTbl.getBaseAppTranslationId();
					translationMap.put(langEnum, translationId);
					descriptionMap.put(langEnum, baseAppTranslationTbl.getDescription());
					remarksMap.put(langEnum, baseAppTranslationTbl.getRemarks());
				}
				if (!baseAppChildren.containsKey(id)) {
					BaseApplication baseApplication = new BaseApplication(id, baseAppName, isActive, descriptionMap,
							remarksMap, platformMap, program, icon, CommonConstants.OPERATIONMODE.VIEW);
					baseApplication.setTranslationIdMap(translationMap);

					if (factoryInstance.isHideInActiveItems()) {
						if (isActive) {
							baseApplications.add(id, baseApplication);
						}
					} else {
						baseApplications.add(id, baseApplication);
					}
				} else {
					IAdminTreeChild iAdminTreeChild = baseAppChildren.get(id);
					if (iAdminTreeChild instanceof BaseApplication) {
						BaseApplication baseApplication = (BaseApplication) iAdminTreeChild;
						baseApplication.setName(baseAppName);
						baseApplication.setActive(isActive);
						baseApplication.setDescriptionMap(descriptionMap);
						baseApplication.setRemarksMap(remarksMap);
						baseApplication.setPlatformMap(platformMap);
						baseApplication.setProgram(program);
						baseApplication.setIcon(icon);
					}
				}
				dbIds.add(id);
			}
			Set<String> keySet = baseAppChildren.keySet();
			for (String key : keySet) {
				if (!dbIds.contains(key)) {
					deactiveObjs.add(key);
				}
			}

			for (String removeObj : deactiveObjs)
				baseApplications.remove(removeObj);
		} catch (Exception e) {
			/*if (e instanceof ResourceAccessException) {
				Display.getDefault().asyncExec(new Runnable() {
					@Override
					public void run() {
						CustomMessageDialog.openError(Display.getDefault().getActiveShell(), "Error",
								"Server not reachable");
					}
				});
			}*/
			LOGGER.error("Exeception while getting Base Application objects! " + e);
		}
	}

	/**
	 * Method for Load start app from service.
	 */
	public void loadStartAppFromService() {
		final File iconFolder = XMSystemUtil.getXMSystemServerIconsFolder();
		try {
			StartApplications startApplications = factoryInstance.getStartApplications();
			Map<String, IAdminTreeChild> startAppChildren = startApplications.getStartApplications();
			List<String> dbIds = new ArrayList<>();
			List<String> deactiveObjs = new ArrayList<>();

			final StartAppController startAppController = new StartAppController();
			List<StartApplicationsTbl> startApplicationsTblList = startAppController.getAllStartApps(true);

			for (StartApplicationsTbl startApplicationsTblVo : startApplicationsTblList) {
				final String id = startApplicationsTblVo.getStartApplicationId();
				final String startAppName = startApplicationsTblVo.getName();
				boolean isActive = com.magna.xmbackend.vo.enums.Status.ACTIVE.name()
						.equals(startApplicationsTblVo.getStatus()) ? true : false;

				IconsTbl iconTbl = startApplicationsTblVo.getIconId();
				Icon icon = new Icon(iconTbl.getIconId(), iconTbl.getIconName(),
						iconFolder + File.separator + iconTbl.getIconName(), iconTbl.getIconType());

				BaseApplicationsTbl baseApplicationsTbl = startApplicationsTblVo.getBaseApplicationId();
				String baseApplicationId = baseApplicationsTbl == null ? null
						: baseApplicationsTbl.getBaseApplicationId();

				Map<LANG_ENUM, String> descriptionMap = new HashMap<>();
				Map<LANG_ENUM, String> remarksMap = new HashMap<>();
				Map<LANG_ENUM, String> translationMap = new HashMap<>();

				Collection<StartAppTranslationTbl> starttAppTranslationTblList = startApplicationsTblVo
						.getStartAppTranslationTblCollection();
				for (StartAppTranslationTbl startAppTranslationTbl : starttAppTranslationTblList) {
					LANG_ENUM langEnum = LANG_ENUM
							.getLangEnum(startAppTranslationTbl.getLanguageCode().getLanguageCode());
					String translationId = startAppTranslationTbl.getStartAppTranslationId();
					translationMap.put(langEnum, translationId);
					descriptionMap.put(langEnum, startAppTranslationTbl.getDescription());
					remarksMap.put(langEnum, startAppTranslationTbl.getRemarks());
				}
				if (!startAppChildren.containsKey(id)) {
					StartApplication startApplication = new StartApplication(id, startAppName, isActive, descriptionMap,
							remarksMap, icon, CommonConstants.OPERATIONMODE.VIEW, baseApplicationId);
					startApplication.setTranslationIdMap(translationMap);

					if (factoryInstance.isHideInActiveItems()) {
						if (isActive) {
							startApplications.add(id, startApplication);
						}
					} else {
						startApplications.add(id, startApplication);
					}
				} else {
					IAdminTreeChild iAdminTreeChild = startAppChildren.get(id);
					if (iAdminTreeChild instanceof StartApplication) {
						StartApplication startApplication = (StartApplication) iAdminTreeChild;
						startApplication.setName(startAppName);
						startApplication.setActive(isActive);
						startApplication.setDescriptionMap(descriptionMap);
						startApplication.setRemarksMap(remarksMap);
						startApplication.setIcon(icon);
					}
				}
				dbIds.add(id);
			}
			Set<String> keySet = startAppChildren.keySet();
			for (String key : keySet) {
				if (!dbIds.contains(key)) {
					deactiveObjs.add(key);
				}
			}
			for (String removeObj : deactiveObjs) {
				startApplications.remove(removeObj);
			}
		} catch (Exception e) {
			/*if (e instanceof ResourceAccessException) {
				Display.getDefault().asyncExec(new Runnable() {
					@Override
					public void run() {
						CustomMessageDialog.openError(Display.getDefault().getActiveShell(), "Error",
								"Server not reachable");
					}
				});
			}*/
			LOGGER.error("Exeception while getting Start Application objects! " + e);
		}
	}

	/**
	 * Method for Load user app from service.
	 */
	public void loadUserAppFromService() {

		final File iconFolder = XMSystemUtil.getXMSystemServerIconsFolder();
		try {
			UserApplications userApplications = factoryInstance.getUserApplications();
			List<String> dbIds = new ArrayList<>();
			List<String> deactiveObjs = new ArrayList<>();

			final UserAppController userAppController = new UserAppController();
			List<UserApplicationsTbl> userApplicationsTblList = userAppController.getAllUserAppsByPositions();

			Map<String, IAdminTreeChild> userAppChildren = userApplications.getUserApplications();

			for (UserApplicationsTbl userApplicationsTblVo : userApplicationsTblList) {
				Boolean isParent = Boolean.valueOf(userApplicationsTblVo.getIsParent());

				final String id = userApplicationsTblVo.getUserApplicationId();
				final String name = userApplicationsTblVo.getName();
				final String description = userApplicationsTblVo.getDescription();
				boolean isActive = com.magna.xmbackend.vo.enums.Status.ACTIVE.name()
						.equals(userApplicationsTblVo.getStatus()) ? true : false;
				String position = userApplicationsTblVo.getPosition();
				boolean isSingleton = Boolean.parseBoolean(userApplicationsTblVo.getIsSingleton());
				BaseApplicationsTbl baseApplicationsTbl = userApplicationsTblVo.getBaseApplicationId();

				String baseApplicationId = baseApplicationsTbl == null ? null
						: baseApplicationsTbl.getBaseApplicationId();

				IconsTbl iconTbl = userApplicationsTblVo.getIconId();
				Icon icon = new Icon(iconTbl.getIconId(), iconTbl.getIconName(),
						iconFolder + File.separator + iconTbl.getIconName(), iconTbl.getIconType());

				Map<LANG_ENUM, String> nameMap = new HashMap<>();
				Map<LANG_ENUM, String> descriptionMap = new HashMap<>();
				Map<LANG_ENUM, String> remarksMap = new HashMap<>();
				Map<LANG_ENUM, String> translationMap = new HashMap<>();

				Collection<UserAppTranslationTbl> userAppTranslationTblList = userApplicationsTblVo
						.getUserAppTranslationTblCollection();
				for (UserAppTranslationTbl userAppTranslationTbl : userAppTranslationTblList) {
					LANG_ENUM langEnum = LANG_ENUM
							.getLangEnum(userAppTranslationTbl.getLanguageCode().getLanguageCode());
					String translationId = userAppTranslationTbl.getUserAppTranslationId();
					translationMap.put(langEnum, translationId);
					nameMap.put(langEnum, userAppTranslationTbl.getName());
					descriptionMap.put(langEnum, userAppTranslationTbl.getDescription());
					remarksMap.put(langEnum, userAppTranslationTbl.getRemarks());
				}

				if (!userAppChildren.containsKey(id)) {
					UserApplication userApplication = new UserApplication(id, name, description, nameMap, isActive, descriptionMap,
							remarksMap, icon, isParent, isSingleton, position, baseApplicationId,
							CommonConstants.OPERATIONMODE.VIEW);
					userApplication.setTranslationIdMap(translationMap);

					if (factoryInstance.isHideInActiveItems()) {
						if (isActive) {
							userApplications.add(id, userApplication);
						}
					} else {
						userApplications.add(id, userApplication);
					}
				} else {
					IAdminTreeChild iAdminTreeChild = userAppChildren.get(id);
					if (iAdminTreeChild instanceof UserApplication) {
						UserApplication userApplication = (UserApplication) iAdminTreeChild;
						userApplication.setName(name);
						userApplication.setDescription(description);
						userApplication.setNameMap(nameMap);
						userApplication.setActive(isActive);
						userApplication.setDescriptionMap(descriptionMap);
						userApplication.setRemarksMap(remarksMap);
						userApplication.setIcon(icon);
						userApplication.setParent(isParent);
						userApplication.setSingleton(isSingleton);
						userApplication.setPosition(position);
						userApplication.setBaseApplicationId(baseApplicationId);
					}
				}
				dbIds.add(id);
			}
			Set<String> keySet = userAppChildren.keySet();
			for (String key : keySet) {
				if (!dbIds.contains(key)) {
					deactiveObjs.add(key);
				}
			}
			for (String removeObj : deactiveObjs)
				userApplications.remove(removeObj);
		} catch (Exception e) {
			/*if (e instanceof ResourceAccessException) {
				Display.getDefault().asyncExec(new Runnable() {
					@Override
					public void run() {
						CustomMessageDialog.openError(Display.getDefault().getActiveShell(), "Error",
								"Server not reachable");
					}
				});
			}*/
			LOGGER.error("Exeception while getting User Application objects! " + e);
		}
	}

	/**
	 * Method for Load project app from service.
	 */
	public void loadProjectAppFromService() {
		final File iconFolder = XMSystemUtil.getXMSystemServerIconsFolder();
		try {
			ProjectApplications projectApplications = factoryInstance.getProjectApplications();

			List<String> dbIds = new ArrayList<>();
			List<String> deactiveObjs = new ArrayList<>();

			final ProjectAppController projectAppController = new ProjectAppController();
			List<ProjectApplicationsTbl> projectApplicationsTblList = projectAppController.getAllProjAppsByPositions();

			Map<String, IAdminTreeChild> projectAppChildren = projectApplications.getProjectApplications();

			for (ProjectApplicationsTbl projectApplicationsTblVo : projectApplicationsTblList) {
				final String id = projectApplicationsTblVo.getProjectApplicationId();
				final String name = projectApplicationsTblVo.getName();
				final String description = projectApplicationsTblVo.getDescription();
				boolean isActive = com.magna.xmbackend.vo.enums.Status.ACTIVE.name()
						.equals(projectApplicationsTblVo.getStatus()) ? true : false;

				IconsTbl iconTbl = projectApplicationsTblVo.getIconId();
				Icon icon = new Icon(iconTbl.getIconId(), iconTbl.getIconName(),
						iconFolder + File.separator + iconTbl.getIconName(), iconTbl.getIconType());
				boolean parent = Boolean.parseBoolean(projectApplicationsTblVo.getIsParent());
				String position = projectApplicationsTblVo.getPosition();
				boolean isSingleton = Boolean.parseBoolean(projectApplicationsTblVo.getIsSingleton());
				BaseApplicationsTbl baseApplicationsTbl = projectApplicationsTblVo.getBaseApplicationId();
				String baseApplicationId = baseApplicationsTbl == null ? null
						: baseApplicationsTbl.getBaseApplicationId();

				Map<LANG_ENUM, String> nameMap = new HashMap<>();
				Map<LANG_ENUM, String> descriptionMap = new HashMap<>();
				Map<LANG_ENUM, String> remarksMap = new HashMap<>();
				Map<LANG_ENUM, String> translationMap = new HashMap<>();

				Collection<ProjectAppTranslationTbl> projectAppTranslationTblList = projectApplicationsTblVo
						.getProjectAppTranslationTblCollection();
				for (ProjectAppTranslationTbl projectAppTranslationTbl : projectAppTranslationTblList) {
					LANG_ENUM langEnum = LANG_ENUM
							.getLangEnum(projectAppTranslationTbl.getLanguageCode().getLanguageCode());
					String translationId = projectAppTranslationTbl.getProjectAppTranslationId();
					translationMap.put(langEnum, translationId);
					nameMap.put(langEnum, projectAppTranslationTbl.getName());
					descriptionMap.put(langEnum, projectAppTranslationTbl.getDescription());
					remarksMap.put(langEnum, projectAppTranslationTbl.getRemarks());
				}
				if (!projectAppChildren.containsKey(id)) {
					ProjectApplication projectApplication = new ProjectApplication(id, name, description, nameMap, isActive,
							descriptionMap, remarksMap, icon, parent, isSingleton, position, baseApplicationId,
							CommonConstants.OPERATIONMODE.VIEW);
					projectApplication.setTranslationIdMap(translationMap);

					if (factoryInstance.isHideInActiveItems()) {
						if (isActive) {
							projectApplications.add(id, projectApplication);
						}
					} else {
						projectApplications.add(id, projectApplication);
					}
				} else {
					IAdminTreeChild iAdminTreeChild = projectAppChildren.get(id);
					if (iAdminTreeChild instanceof ProjectApplication) {
						ProjectApplication projectApplication = (ProjectApplication) iAdminTreeChild;
						projectApplication.setName(name);
						projectApplication.setDescription(description);
						projectApplication.setNameMap(nameMap);
						projectApplication.setActive(isActive);
						projectApplication.setDescriptionMap(descriptionMap);
						projectApplication.setRemarksMap(remarksMap);
						projectApplication.setIcon(icon);
						projectApplication.setParent(parent);
						projectApplication.setSingleton(isSingleton);
						projectApplication.setPosition(position);
						projectApplication.setBaseApplicationId(baseApplicationId);
					}
				}
				dbIds.add(id);
			}
			Set<String> keySet = projectAppChildren.keySet();
			for (String key : keySet) {
				if (!dbIds.contains(key)) {
					deactiveObjs.add(key);
				}
			}
			for (String removeObj : deactiveObjs)
				projectApplications.remove(removeObj);
		} catch (Exception e) {
			/*if (e instanceof ResourceAccessException) {
				Display.getDefault().asyncExec(new Runnable() {
					@Override
					public void run() {
						CustomMessageDialog.openError(Display.getDefault().getActiveShell(), "Error",
								"Server not reachable");
					}
				});
			}*/
			LOGGER.error("Exeception while getting project Application objects! " + e);
		}
	}

	/**
	 * Method for Load directories from service.
	 */
	public void loadDirectoriesFromService() {
		final File iconFolder = XMSystemUtil.getXMSystemServerIconsFolder();
		try {
			Directories directories = factoryInstance.getDirectories();
			List<String> deactiveObjs = new ArrayList<>();
			List<String> dbIds = new ArrayList<>();
			DirectoryController directoryController = new DirectoryController();
			List<DirectoryTbl> directoryResponseList = directoryController.getAllDirectory(true);

			Map<String, IAdminTreeChild> directoryChildren = directories.getDirectoriesChildren();

			for (DirectoryTbl directoryVo : directoryResponseList) {
				final String id = directoryVo.getDirectoryId();
				final String name = directoryVo.getName();
				IconsTbl iconTbl = directoryVo.getIconId();
				Icon icon = new Icon(iconTbl.getIconId(), iconTbl.getIconName(),
						iconFolder + File.separator + iconTbl.getIconName(), iconTbl.getIconType());

				Map<LANG_ENUM, String> descriptionMap = new HashMap<>();
				Map<LANG_ENUM, String> remarksMap = new HashMap<>();
				Map<LANG_ENUM, String> translationMap = new HashMap<>();
				Collection<DirectoryTranslationTbl> directoryTranslationTblList = directoryVo
						.getDirectoryTranslationTblCollection();
				for (DirectoryTranslationTbl directoryTranslationTbl : directoryTranslationTblList) {
					LANG_ENUM langEnum = LANG_ENUM
							.getLangEnum(directoryTranslationTbl.getLanguageCode().getLanguageCode());
					String translationId = directoryTranslationTbl.getDirectoryTranslationId();
					translationMap.put(langEnum, translationId);
					descriptionMap.put(langEnum, directoryTranslationTbl.getDescription());
					remarksMap.put(langEnum, directoryTranslationTbl.getRemarks());
				}
				if (!directoryChildren.containsKey(id)) {
					Directory directory = new Directory(id, name, descriptionMap, remarksMap, icon,
							CommonConstants.OPERATIONMODE.VIEW);
					directory.setTranslationIdMap(translationMap);
					directories.add(id, directory);

				} else {
					IAdminTreeChild iAdminTreeChild = directoryChildren.get(id);
					if (iAdminTreeChild instanceof Directory) {
						Directory site = (Directory) iAdminTreeChild;
						site.setName(name);
						site.setDescriptionMap(descriptionMap);
						site.setRemarksMap(remarksMap);
						site.setIcon(icon);
					}
				}
				dbIds.add(id);
			}
			Set<String> keySet = directoryChildren.keySet();
			for (String key : keySet) {
				if (!dbIds.contains(key)) {
					deactiveObjs.add(key);
				}
			}
			for (String removeObj : deactiveObjs)
				directories.remove(removeObj);

		} catch (Exception e) {
			if (e instanceof ResourceAccessException) {
				Display.getDefault().asyncExec(new Runnable() {
					@Override
					public void run() {
						CustomMessageDialog.openError(Display.getDefault().getActiveShell(), "Error",
								"Server not reachable");
					}
				});
			}
			LOGGER.error("Exeception while getting directories! " + e);
		}
	}

	/**
	 * Method for Load project project app from service.
	 *
	 * @param projectProjectApplications
	 *            {@link ProjectProjectApplications}
	 * @return the list {@link List<IAdminTreeChild>}
	 */
	public List<IAdminTreeChild> loadProjectProjectAppFromService(
			ProjectProjectApplications projectProjectApplications) {
		Project project = (Project) projectProjectApplications.getParent();
		String projectId = project.getProjectId();
		List<IAdminTreeChild> projectProjectAppsList = new ArrayList<IAdminTreeChild>();
		Map<String, IAdminTreeChild> projectProjectAppsMap = new LinkedHashMap<>();

		try {
			ProjectAppController projectAppController = new ProjectAppController();
			List<ProjectApplicationsTbl> projectAppResponse = projectAppController.getProjectAppByProjectId(projectId);
			for (ProjectApplicationsTbl projectApplicationsTbl : projectAppResponse) {
				ProjectApplication projectApplication = getUpdatedProjectApplication(projectApplicationsTbl);
				if (projectApplication != null) {
					RelationObj projectProjectApp = projectApplication.getAdapter(projectProjectApplications.getClass(),
							project, projectApplication.getProjectApplicationId(), projectApplication.isActive());

					projectProjectAppsMap.put(projectApplication.getProjectApplicationId(), projectProjectApp);
				}
			}

			if (!projectProjectAppsMap.isEmpty()) {
				projectProjectAppsList = new ArrayList<>(projectProjectAppsMap.values());
			}
		} catch (Exception e) {
			if (e instanceof ResourceAccessException) {
				Display.getDefault().asyncExec(new Runnable() {
					@Override
					public void run() {
						CustomMessageDialog.openError(Display.getDefault().getActiveShell(), "Error",
								"Server not reachable");
					}
				});
			}
			LOGGER.error("Exeception while getting project application based on project Relation! " + e);
		}

		return projectProjectAppsList;
	}

	/**
	 * Method for Load user children from service.
	 *
	 * @param user
	 *            {@link User}
	 */
	public void loadUserChildrenFromService(User user) {
		UserProjects userProjects = (UserProjects) user.getUserChildren().get(UserProjects.class.getName());
		loadUserProjectsFromService(user.getUserId(), userProjects);

		UserAdminAreas userAdminAreas = (UserAdminAreas) user.getUserChildren().get(UserAdminAreas.class.getName());
		loadUserAdminAreasFromService(userAdminAreas);
	}

	/**
	 * Method for Load user projects from service.
	 *
	 * @param userId
	 *            {@link String}
	 * @param userProjects
	 *            {@link UserProjects}
	 */
	public void loadUserProjectsFromService(String userId, UserProjects userProjects) {
		try {
			userProjects.removeAll();
			final UserProjectRelController userProjectRelController = new UserProjectRelController();
			List<UserProjectRelTbl> userProjectRelTbls = userProjectRelController.getUserProjectRelByUserId(userId);
			if (userProjectRelTbls != null) {
				for (UserProjectRelTbl userProjectRelTbl : userProjectRelTbls) {
					Project project = getUpdatedProject(userProjectRelTbl.getProjectId());

					if (project != null) {
						String userProjectRelId = userProjectRelTbl.getUserProjectRelId();
						boolean relationStatus = com.magna.xmbackend.vo.enums.Status.ACTIVE.name()
								.equals(userProjectRelTbl.getStatus());
						RelationObj userProjectRel = project.getAdapter(UserProjectChild.class, userProjects,
								userProjectRelId, relationStatus);
						((UserProjectChild) userProjectRel.getContainerObj()).addFixedChildren(userProjectRel);

						boolean active = userProjectRel.isActive();

						if (factoryInstance.isHideInActiveItems()) {
							if (active) {
								userProjects.add(userProjectRelId, userProjectRel);
							}
						} else {
							userProjects.add(userProjectRelId, userProjectRel);
						}

						// userProjects.add(userProjectRelId, userProjectRel);
					}
				}
			}
		} catch (Exception e) {
			/*if (e instanceof ResourceAccessException) {
				Display.getDefault().asyncExec(new Runnable() {
					@Override
					public void run() {
						CustomMessageDialog.openError(Display.getDefault().getActiveShell(), "Error",
								"Server not reachable");
					}
				});
			}*/
			LOGGER.error("Exeception while getting User Projects objects! " + e);
		}
	}

	/**
	 * Method for Load user admin areas from service.
	 *
	 * @param userId
	 *            {@link String}
	 * @param userAdminAreas
	 *            {@link UserAdminAreas}
	 */
	public void loadUserAdminAreasFromService(UserAdminAreas userAdminAreas) {
		try {
			userAdminAreas.removeAll();
			final SiteAdminAreaRelController siteAdminAreaRelController = new SiteAdminAreaRelController();
			List<SiteAdminAreaRelTbl> siteAdminAreaRels = siteAdminAreaRelController.getAllSiteAdminAreaRel();
			if (siteAdminAreaRels != null) {
				for (SiteAdminAreaRelTbl siteAdminAreaRel : siteAdminAreaRels) {
					AdministrationArea adminArea = getUpdatedAdminArea(siteAdminAreaRel.getAdminAreaId());
					if (adminArea != null) {
						String relId = siteAdminAreaRel.getSiteAdminAreaRelId();
						RelationObj relObj = adminArea.getAdapter(UserAdminAreaChild.class, userAdminAreas, relId,
								adminArea.isActive());
						((UserAdminAreaChild) relObj.getContainerObj()).addFixedChildren(relObj);

						boolean active = relObj.isActive();

						if (factoryInstance.isHideInActiveItems()) {
							if (active) {
								userAdminAreas.add(relId, relObj);
							}
						} else {
							userAdminAreas.add(relId, relObj);
						}
					}
				}
			}
		} catch (Exception e) {
			/*if (e instanceof ResourceAccessException) {
				Display.getDefault().asyncExec(new Runnable() {
					@Override
					public void run() {
						CustomMessageDialog.openError(Display.getDefault().getActiveShell(), "Error",
								"Server not reachable");
					}
				});
			}*/
			LOGGER.error("Exeception while getting Administration Area objects from user! " + e);
		}
	}

	/**
	 * Method for Load user project children from service.
	 *
	 * @param userProjectChild
	 *            {@link UserProjectChild}
	 */
	public void loadUserProjectChildrenFromService(UserProjectChild userProjectChild) {
		Map<String, IAdminTreeChild> userProjectChildren = userProjectChild.getUserProjectChildren();
		UserProjectAdminAreas userProjectAdminAreas = (UserProjectAdminAreas) userProjectChildren
				.get(UserProjectAdminAreas.class.getName());

		loadUserProjectAAsFromService(userProjectAdminAreas);
	}

	/**
	 * Method for Load project user children from service.
	 *
	 * @param projectUserChild
	 *            {@link ProjectUserChild}
	 */
	public void loadProjectUserChildrenFromService(ProjectUserChild projectUserChild) {
		Map<String, IAdminTreeChild> projectUserChildren = projectUserChild.getProjectUserChildren();
		ProjectUserAdminAreas projectUserAdminAreas = (ProjectUserAdminAreas) projectUserChildren
				.get(ProjectUserAdminAreas.class.getName());

		loadUserProjectAAsFromService(projectUserAdminAreas);
	}

	/**
	 * Method for Load user project A as from service.
	 *
	 * @param iAdminTreeChild
	 *            {@link IAdminTreeChild}
	 */
	public void loadUserProjectAAsFromService(IAdminTreeChild iAdminTreeChild) {
		try {
			String projectId = null;
			if (iAdminTreeChild instanceof UserProjectAdminAreas) {
				((UserProjectAdminAreas) iAdminTreeChild).removeAll();
				IAdminTreeChild refObject = ((RelationObj) iAdminTreeChild.getParent()).getRefObject();
				if (refObject instanceof Project) {
					projectId = ((Project) refObject).getProjectId();
				}
			} else if (iAdminTreeChild instanceof ProjectUserAdminAreas) {
				((ProjectUserAdminAreas) iAdminTreeChild).removeAll();
				IAdminTreeChild refObject = iAdminTreeChild.getParent().getParent().getParent();
				if (refObject instanceof Project) {
					projectId = ((Project) refObject).getProjectId();
				}
			}

			if (projectId != null) {

				final AdminAreaProjectRelController adminAreaProjectRelController = new AdminAreaProjectRelController();
				AdminAreaProjectResponseWrapper adminAreaProjectResponseWrapper = adminAreaProjectRelController
						.getAAProjectRelByProjectId(projectId);
				if (adminAreaProjectResponseWrapper != null) {
					List<AdminAreaProjectResponse> adminAreaProjectResponses = adminAreaProjectResponseWrapper
							.getAdminAreaProjectResponses();
					for (AdminAreaProjectResponse adminAreaProjectResponse : adminAreaProjectResponses) {
						AdministrationArea adminArea = getUpdatedAdminArea(
								adminAreaProjectResponse.getAdminAreasTbls());
						if (adminArea != null) {
							String relId = adminAreaProjectResponse.getAdminAreaProjectRelTbls()
									.getAdminAreaProjectRelId();

							if (iAdminTreeChild instanceof UserProjectAdminAreas) {
								RelationObj relObj = adminArea.getAdapter(UserProjectAdminAreaChild.class,
										iAdminTreeChild, relId, adminArea.isActive());
								((UserProjectAdminAreaChild) relObj.getContainerObj()).addFixedChildren(relObj);

								boolean active = relObj.isActive();

								if (factoryInstance.isHideInActiveItems()) {
									if (active) {
										((UserProjectAdminAreas) iAdminTreeChild).add(relId, relObj);
									}
								} else {
									((UserProjectAdminAreas) iAdminTreeChild).add(relId, relObj);
								}
							} else if (iAdminTreeChild instanceof ProjectUserAdminAreas) {
								RelationObj relObj = adminArea.getAdapter(ProjectUserAdminAreaChild.class,
										iAdminTreeChild, relId, adminArea.isActive());
								((ProjectUserAdminAreaChild) relObj.getContainerObj()).addFixedChildren(relObj);

								boolean active = relObj.isActive();
								if (factoryInstance.isHideInActiveItems()) {
									if (active) {
										((ProjectUserAdminAreas) iAdminTreeChild).add(relId, relObj);
									}
								} else {
									((ProjectUserAdminAreas) iAdminTreeChild).add(relId, relObj);
								}
							}
						}
					}
				}
			}
		} catch (Exception e) {
			/*if (e instanceof ResourceAccessException) {
				Display.getDefault().asyncExec(new Runnable() {
					@Override
					public void run() {
						CustomMessageDialog.openError(Display.getDefault().getActiveShell(), "Error",
								"Server not reachable");
					}
				});
			}*/
			LOGGER.error("Exeception while getting Administration Area objects from user/project! " + e);
		}
	}

	/**
	 * Method for Load start app admin areas from service.
	 *
	 * @param startAppAdminAreasNode
	 *            {@link IAdminTreeChild}
	 * @return the list {@link List<IAdminTreeChild>}
	 */
	public List<IAdminTreeChild> loadStartAppAdminAreasFromService(final IAdminTreeChild startAppAdminAreasNode) {
		List<IAdminTreeChild> userAdminAreas = new ArrayList<IAdminTreeChild>();
		try {
			IAdminTreeChild parent = startAppAdminAreasNode.getParent();

			if (parent != null && parent instanceof StartApplication) {
				String startApp = ((StartApplication) parent).getStartPrgmApplicationId();

				AdminAreaStartAppRelController adminAreaStartAppRelController = new AdminAreaStartAppRelController();
				List<AdminAreaStartAppRelation> adminAreaStartAppRelations = adminAreaStartAppRelController
						.getAAStartAppRelByStartAppId(startApp);

				if (adminAreaStartAppRelations != null) {
					for (AdminAreaStartAppRelation adminAreaStartAppRelation : adminAreaStartAppRelations) {
						AdminAreasTbl adminAreaTbl = adminAreaStartAppRelation.getSiteAdminAreaRelId().getAdminAreaId();
						AdministrationArea adminAreaa = getUpdatedAdminArea(adminAreaTbl);
						if (adminAreaa != null) {
							String relationId = adminAreaStartAppRelation.getAdminAreaStartAppRelId();
							boolean relationStatus = com.magna.xmbackend.vo.enums.Status.ACTIVE.name()
									.equals(adminAreaStartAppRelation.getStatus());
							RelationObj relObj = adminAreaa.getAdapter(startAppAdminAreasNode.getClass(),
									startAppAdminAreasNode, relationId, relationStatus);

							boolean active = relObj.isActive();
							if (factoryInstance.isHideInActiveItems()) {
								if (active) {
									userAdminAreas.add(relObj);
								}
							} else {
								userAdminAreas.add(relObj);
							}
						}
					}
				}
			}
		} catch (Exception e) {
			if (e instanceof ResourceAccessException) {
				Display.getDefault().asyncExec(new Runnable() {
					@Override
					public void run() {
						CustomMessageDialog.openError(Display.getDefault().getActiveShell(), "Error",
								"Server not reachable");
					}
				});
			}
			LOGGER.error("Exeception while getting Admin Areas based on Start Application! " + e);
		}
		return userAdminAreas;
	}

	/**
	 * Method for Load start app users from service.
	 *
	 * @param startAppUsersNode
	 *            {@link IAdminTreeChild}
	 * @return the list {@link List<IAdminTreeChild>}
	 */
	public List<IAdminTreeChild> loadStartAppUsersFromService(final IAdminTreeChild startAppUsersNode) {
		List<IAdminTreeChild> startAppUsers = new ArrayList<IAdminTreeChild>();
		try {
			IAdminTreeChild parent = startAppUsersNode.getParent();

			if (parent != null && parent instanceof StartApplication) {
				String startAppId = ((StartApplication) parent).getStartPrgmApplicationId();

				UserStartAppRelController userStartAppRelController = new UserStartAppRelController();
				List<UserStartAppRelation> userStartAppRelations = userStartAppRelController
						.getUserStartAppRelByStartAppId(startAppId);
				if (userStartAppRelations != null) {
					for (UserStartAppRelation userStartAppRelation : userStartAppRelations) {
						UsersTbl usersTbl = userStartAppRelation.getUserId();
						User user = getUpdatedUser(usersTbl);
						if (user != null) {
							String relationId = userStartAppRelation.getUserStartAppRelId();
							boolean relationStatus = com.magna.xmbackend.vo.enums.Status.ACTIVE.name()
									.equals(userStartAppRelation.getStatus());
							RelationObj relObj = user.getAdapter(StartAppUsers.class, startAppUsersNode, relationId,
									relationStatus);

							boolean active = relObj.isActive();
							if (factoryInstance.isHideInActiveItems()) {
								if (active) {
									startAppUsers.add(relObj);
								}
							} else {
								startAppUsers.add(relObj);
							}
						}
					}
				}
			}
		} catch (Exception e) {
			if (e instanceof ResourceAccessException) {
				Display.getDefault().asyncExec(new Runnable() {
					@Override
					public void run() {
						CustomMessageDialog.openError(Display.getDefault().getActiveShell(), "Error",
								"Server not reachable");
					}
				});
			}
			LOGGER.error("Exeception while getting Users based on Start Application ! " + e);
		}
		return startAppUsers;

	}

	/**
	 * Method for Load start app projects from service.
	 *
	 * @param startAppProjectsNode
	 *            {@link IAdminTreeChild}
	 * @return the list {@link List<IAdminTreeChild>}
	 */
	public List<IAdminTreeChild> loadStartAppProjectsFromService(final IAdminTreeChild startAppProjectsNode) {
		List<IAdminTreeChild> startAppProjects = new ArrayList<IAdminTreeChild>();
		Map<String, IAdminTreeChild> startAppProjectsMap = new LinkedHashMap<>();
		try {
			IAdminTreeChild parent = startAppProjectsNode.getParent();
			if (parent != null && parent instanceof StartApplication) {
				String startAppId = ((StartApplication) parent).getStartPrgmApplicationId();
				ProjectController projectController = new ProjectController();
				List<ProjectsTbl> projectsTbls = projectController.getProjectsByStartAppId(startAppId);
				for (ProjectsTbl projectsTbl : projectsTbls) {
					Project project = getUpdatedProject(projectsTbl);
					if (project != null) {
						RelationObj relObj = project.getAdapter(StartAppProjects.class, startAppProjectsNode,
								project.getProjectId(), project.isActive());

						boolean active = relObj.isActive();
						if (factoryInstance.isHideInActiveItems()) {
							if (active) {
								startAppProjectsMap.put(project.getProjectId(), relObj);
							}
						} else {
							startAppProjectsMap.put(project.getProjectId(), relObj);
						}
					}
				}
				if (!startAppProjectsMap.isEmpty()) {
					startAppProjects = new ArrayList<>(startAppProjectsMap.values());
				}
			}
		} catch (Exception e) {
			if (e instanceof ResourceAccessException) {
				Display.getDefault().asyncExec(new Runnable() {
					@Override
					public void run() {
						CustomMessageDialog.openError(Display.getDefault().getActiveShell(), "Error",
								"Server not reachable");
					}
				});
			}
			LOGGER.error("Exeception while getting Projects based on Start Application! " + e);
		}
		return startAppProjects;
	}

	/**
	 * Method for Load user app admin areas from service.
	 *
	 * @param userAppAdminAreasNode
	 *            {@link IAdminTreeChild}
	 * @return the list {@link List<IAdminTreeChild>}
	 */
	public List<IAdminTreeChild> loadUserAppAdminAreasFromService(final IAdminTreeChild userAppAdminAreasNode) {
		List<IAdminTreeChild> userAdminAreas = new ArrayList<IAdminTreeChild>();
		try {
			IAdminTreeChild parent = userAppAdminAreasNode.getParent();

			if (parent != null && parent instanceof UserApplication) {
				String userAppId = ((UserApplication) parent).getUserApplicationId();

				AdminAreaUserAppRelController adminAreaUserAppRelController = new AdminAreaUserAppRelController();
				List<AdminAreaUserAppRelation> adminAreaUserAppRelWrapper = adminAreaUserAppRelController
						.getAAUserAppByUserAppId(userAppId);
				if (adminAreaUserAppRelWrapper != null) {
					for (AdminAreaUserAppRelation adminAreaUserAppRelation : adminAreaUserAppRelWrapper) {
						AdminAreasTbl adminAreaTbl = adminAreaUserAppRelation.getSiteAdminAreaRelId().getAdminAreaId();
						AdministrationArea adminAreaa = getUpdatedAdminArea(adminAreaTbl);
						if (adminAreaa != null) {
							String relationId = adminAreaUserAppRelation.getAdminAreaUserAppRelId();
							boolean relationStatus = com.magna.xmbackend.vo.enums.Status.ACTIVE.name()
									.equals(adminAreaUserAppRelation.getStatus());
							RelationObj relObj = adminAreaa.getAdapter(userAppAdminAreasNode.getClass(),
									userAppAdminAreasNode, relationId, relationStatus);
							boolean active = relObj.isActive();
							if (factoryInstance.isHideInActiveItems()) {
								if (active) {
									userAdminAreas.add(relObj);
								}
							} else {
								userAdminAreas.add(relObj);
							}
						}
					}
				}
			}
		} catch (Exception e) {
			if (e instanceof ResourceAccessException) {
				Display.getDefault().asyncExec(new Runnable() {
					@Override
					public void run() {
						CustomMessageDialog.openError(Display.getDefault().getActiveShell(), "Error",
								"Server not reachable");
					}
				});
			}
			LOGGER.error("Exeception while getting start application based on user Relation! " + e);
		}
		return userAdminAreas;
	}

	/**
	 * Method for Load user app users from service.
	 *
	 * @param userAppUsersNode
	 *            {@link IAdminTreeChild}
	 * @return the list {@link List<IAdminTreeChild>}
	 */
	public List<IAdminTreeChild> loadUserAppUsersFromService(final IAdminTreeChild userAppUsersNode) {
		List<IAdminTreeChild> userAppUsers = new ArrayList<IAdminTreeChild>();
		try {
			IAdminTreeChild parent = userAppUsersNode.getParent();

			if (parent != null && parent instanceof UserApplication) {
				String userAppId = ((UserApplication) parent).getUserApplicationId();

				UserUserAppRelController userUserAppRelController = new UserUserAppRelController();
				UserUserAppRelResponseWrapper userUserAppRelResponseWrapper = userUserAppRelController
						.getUserUserAppRelsByUserAppId(userAppId);
				if (userUserAppRelResponseWrapper != null) {
					List<UserUserAppRelation> userUserAppRelationsList = userUserAppRelResponseWrapper
							.getUserUserAppRelations();
					List<String> userIds = new ArrayList<>();
					for (UserUserAppRelation UserUserAppRelation : userUserAppRelationsList) {
						UsersTbl usersTbl = UserUserAppRelation.getUserId();
						if (!userIds.contains(usersTbl.getUserId())) {
							userIds.add(usersTbl.getUserId());
							User user = getUpdatedUser(usersTbl);
							if (user != null) {
								String relationId = UserUserAppRelation.getUserUserAppRelId();
								RelationObj relObj = user.getAdapter(UserAppUsers.class, userAppUsersNode, relationId,
										user.isActive());
								boolean active = relObj.isActive();
								if (factoryInstance.isHideInActiveItems()) {
									if (active) {
										userAppUsers.add(relObj);
									}
								} else {
									userAppUsers.add(relObj);
								}
							}
						}
					}
				}
			}
		} catch (Exception e) {
			if (e instanceof ResourceAccessException) {
				Display.getDefault().asyncExec(new Runnable() {
					@Override
					public void run() {
						CustomMessageDialog.openError(Display.getDefault().getActiveShell(), "Error",
								"Server not reachable");
					}
				});
			}
			LOGGER.error("Exeception while getting User objects based on User Application Relation! " + e);
		}
		return userAppUsers;
	}

	/**
	 * Method for Load child user apps from service.
	 *
	 * @param parentUserApp
	 *            {@link UserApplication}
	 */
	public void loadChildUserAppsFromService(UserApplication parentUserApp) {
		final File iconFolder = XMSystemUtil.getXMSystemServerIconsFolder();
		try {
			String userApplicationId = parentUserApp.getUserApplicationId();
			Map<String, IAdminTreeChild> userAppChildren = parentUserApp.getUserApplicationChildren();
			List<String> dbIds = new ArrayList<>();
			UserAppController userAppCtrl = new UserAppController();
			List<UserApplicationsTbl> userApplicationsTblList = userAppCtrl
					.getAllChildUserAppByUserAppId(userApplicationId);

			for (UserApplicationsTbl userApplicationsTblVo : userApplicationsTblList) {
				final Boolean isParent = Boolean.valueOf(userApplicationsTblVo.getIsParent());
				final String id = userApplicationsTblVo.getUserApplicationId();
				final String name = userApplicationsTblVo.getName();
				final String description = userApplicationsTblVo.getDescription();
				boolean isActive = com.magna.xmbackend.vo.enums.Status.ACTIVE.name()
						.equals(userApplicationsTblVo.getStatus()) ? true : false;
				String position = userApplicationsTblVo.getPosition();
				boolean isSingleton = Boolean.parseBoolean(userApplicationsTblVo.getIsSingleton());
				BaseApplicationsTbl baseApplicationsTbl = userApplicationsTblVo.getBaseApplicationId();

				String baseApplicationId = baseApplicationsTbl == null ? null : baseApplicationsTbl.getBaseApplicationId();
				IconsTbl iconTbl = userApplicationsTblVo.getIconId();
				Icon icon = new Icon(iconTbl.getIconId(), iconTbl.getIconName(),
						iconFolder + File.separator + iconTbl.getIconName(), iconTbl.getIconType());

				Map<LANG_ENUM, String> nameMap = new HashMap<>();
				Map<LANG_ENUM, String> descriptionMap = new HashMap<>();
				Map<LANG_ENUM, String> remarksMap = new HashMap<>();
				Map<LANG_ENUM, String> translationMap = new HashMap<>();

				Collection<UserAppTranslationTbl> userAppTranslationTblList = userApplicationsTblVo
						.getUserAppTranslationTblCollection();
				for (UserAppTranslationTbl userAppTranslationTbl : userAppTranslationTblList) {
					LANG_ENUM langEnum = LANG_ENUM
							.getLangEnum(userAppTranslationTbl.getLanguageCode().getLanguageCode());
					String translationId = userAppTranslationTbl.getUserAppTranslationId();
					translationMap.put(langEnum, translationId);
					nameMap.put(langEnum, userAppTranslationTbl.getName());
					descriptionMap.put(langEnum, userAppTranslationTbl.getDescription());
					remarksMap.put(langEnum, userAppTranslationTbl.getRemarks());
				}

				if (!userAppChildren.containsKey(id)) {
					UserApplicationChild userApplicationChild = new UserApplicationChild(id, name, description, nameMap, isActive,
							descriptionMap, remarksMap, icon, isParent, isSingleton, position, baseApplicationId,
							CommonConstants.OPERATIONMODE.VIEW);
					userApplicationChild.setTranslationIdMap(translationMap);

					if (factoryInstance.isHideInActiveItems()) {
						if (isActive) {
							parentUserApp.add(id, userApplicationChild);
						}
					} else {
						parentUserApp.add(id, userApplicationChild);
					}
				} else {
					IAdminTreeChild iAdminTreeChild = userAppChildren.get(id);
					if (iAdminTreeChild instanceof UserApplicationChild) {
						UserApplicationChild userApplicationChild = (UserApplicationChild) iAdminTreeChild;
						userApplicationChild.setName(name);
						userApplicationChild.setDescription(description);
						userApplicationChild.setNameMap(nameMap);
						userApplicationChild.setActive(isActive);
						userApplicationChild.setDescriptionMap(descriptionMap);
						userApplicationChild.setRemarksMap(remarksMap);
						userApplicationChild.setIcon(icon);
						userApplicationChild.setParent(isParent);
						userApplicationChild.setSingleton(isSingleton);
						userApplicationChild.setPosition(position);
						userApplicationChild.setBaseApplicationId(baseApplicationId);
					}
				}
				dbIds.add(id);
			}
			Set<String> keySet = userAppChildren.keySet();
			List<String> deactiveObjs = new ArrayList<>();
			for (String key : keySet) {
				if (!dbIds.contains(key) && !UserAppAdminAreas.class.getName().equals(key)
						&& !UserAppUsers.class.getName().equals(key) && !UserAppGroups.class.getName().equals(key)) {
					deactiveObjs.add(key);
				}
			}
			
			for (String string : deactiveObjs) {
				parentUserApp.remove(string);
			}
		} catch (Exception e) {
			if (e instanceof ResourceAccessException) {
				Display.getDefault().asyncExec(new Runnable() {
					@Override
					public void run() {
						CustomMessageDialog.openError(Display.getDefault().getActiveShell(), "Error",
								"Server not reachable");
					}
				});
			}
			LOGGER.error("Exeception while getting Child User Application Relation objects! " + e);
		}
	}

	/**
	 * Method for Load project app children from service.
	 *
	 * @param parentProjectApp
	 *            {@link ProjectApplication}
	 */
	public void loadProjectAppChildrenFromService(ProjectApplication parentProjectApp) {
		String projectApplicationId = parentProjectApp.getProjectApplicationId();
		ProjectAppAdminAreas projectAppAdminAreas = (ProjectAppAdminAreas) parentProjectApp
				.getProjectApplicationChildren().get(ProjectAppAdminAreas.class.getName());
		loadProjectAppAdminAreasFromService(projectApplicationId, projectAppAdminAreas);
		loadChildProjectAppsFromService(parentProjectApp);
	}

	/**
	 * Method for Load project app admin areas from service.
	 *
	 * @param projectApplicationId
	 *            {@link String}
	 * @param projectAppAdminAreas
	 *            {@link ProjectAppAdminAreas}
	 */
	public void loadProjectAppAdminAreasFromService(String projectApplicationId,
			ProjectAppAdminAreas projectAppAdminAreas) {
		try {
			projectAppAdminAreas.removeAll();

			AdminAreaProjectAppRelController adminAreaProjectAppRelController = new AdminAreaProjectAppRelController();
			List<AdminAreaProjectAppResponse> adminAreaProjectAppResponses = adminAreaProjectAppRelController
					.getAdminAreaByProjAppId(projectApplicationId);
			for (AdminAreaProjectAppResponse adminAreaProjectAppResponse : adminAreaProjectAppResponses) {
				AdminAreasTbl adminAreaTbl = adminAreaProjectAppResponse.getAdminAreasTbl();
				AdministrationArea adminAreaa = getUpdatedAdminArea(adminAreaTbl);

				if (adminAreaa != null) {
					String relationId = adminAreaProjectAppResponse.getAdminAreaProjAppRelTbl()
							.getAdminAreaProjAppRelId();
					boolean relationStatus = com.magna.xmbackend.vo.enums.Status.ACTIVE.name()
							.equals(adminAreaProjectAppResponse.getAdminAreasTbl().getStatus());
					RelationObj relObj = adminAreaa.getAdapter(ProjectAppAdminAreaChild.class, projectAppAdminAreas,
							relationId, relationStatus);

					((ProjectAppAdminAreaChild) relObj.getContainerObj()).addFixedChildren(relObj);

					boolean active = relObj.isActive();
					if (factoryInstance.isHideInActiveItems()) {
						if (active) {
							projectAppAdminAreas.add(relationId, relObj);
						}
					} else {
						projectAppAdminAreas.add(relationId, relObj);
					}
				}
			}
		} catch (Exception e) {
			/*if (e instanceof ResourceAccessException) {
				Display.getDefault().asyncExec(new Runnable() {
					@Override
					public void run() {
						CustomMessageDialog.openError(Display.getDefault().getActiveShell(), "Error",
								"Server not reachable");
					}
				});
			}*/
			LOGGER.error("Exeception while getting site Administration Area Relation objects! " + e);
		}
	}

	/**
	 * Method for Load project app admin area projects from service.
	 *
	 * @param projectAppAAProjectsNode
	 *            {@link IAdminTreeChild}
	 * @return the list {@link List<IAdminTreeChild>}
	 */
	public List<IAdminTreeChild> loadProjectAppAdminAreaProjectsFromService(
			final IAdminTreeChild projectAppAAProjectsNode) {
		List<IAdminTreeChild> projectAppAdminAreaProjects = new ArrayList<IAdminTreeChild>();
		try {
			if (projectAppAAProjectsNode != null && projectAppAAProjectsNode instanceof ProjectAppAdminAreaProjects) {
				String siteAdminAreaId = ((AdministrationArea) ((RelationObj) projectAppAAProjectsNode.getParent())
						.getRefObject()).getRelationId();
				String projectAppId = ((ProjectApplication) projectAppAAProjectsNode.getParent().getParent()
						.getParent()).getProjectApplicationId();

				AdminAreaProjectRelController adminAreaProjectRelController = new AdminAreaProjectRelController();
				List<AdminAreaProjectRelationResponse> adminAreaProjRelResps = adminAreaProjectRelController
						.getAdminAreaProjectRelBySiteAAIdProjectAppId(siteAdminAreaId, projectAppId);
				if (adminAreaProjRelResps != null) {
					for (AdminAreaProjectRelationResponse adminAreaProjRelResp : adminAreaProjRelResps) {
						ProjectsTbl projectsTbl = adminAreaProjRelResp.getProjectId();
						Project project = getUpdatedProject(projectsTbl);
						if (project != null) {
							String relationId = adminAreaProjRelResp.getAdminAreaProjectRelId();
							boolean relationStatus = com.magna.xmbackend.vo.enums.Status.ACTIVE.name()
									.equals(adminAreaProjRelResp.getStatus());
							RelationObj relationObj = project.getAdapter(projectAppAAProjectsNode.getClass(),
									projectAppAAProjectsNode, relationId, relationStatus);

							boolean active = relationObj.isActive();
							if (factoryInstance.isHideInActiveItems()) {
								if (active) {
									projectAppAdminAreaProjects.add(relationObj);
								}
							} else {
								projectAppAdminAreaProjects.add(relationObj);
							}
						}
					}
				}
			}
		} catch (Exception e) {
			if (e instanceof ResourceAccessException) {
				Display.getDefault().asyncExec(new Runnable() {
					@Override
					public void run() {
						CustomMessageDialog.openError(Display.getDefault().getActiveShell(), "Error",
								"Server not reachable");
					}
				});
			}
			LOGGER.error("Exeception while getting Project Application Users! " + e);
		}
		return projectAppAdminAreaProjects;
	}

	/**
	 * Method for Load child project apps from service.
	 *
	 * @param parentProjectApp
	 *            {@link ProjectApplication}
	 */
	public void loadChildProjectAppsFromService(ProjectApplication parentProjectApp) {
		final File iconFolder = XMSystemUtil.getXMSystemServerIconsFolder();
		try {
			String projectApplicationId = parentProjectApp.getProjectApplicationId();
			Map<String, IAdminTreeChild> projectAppChildren = parentProjectApp.getProjectApplicationChildren();
			List<String> dbIds = new ArrayList<>();

			ProjectAppController projectAppCtrl = new ProjectAppController();
			List<ProjectApplicationsTbl> projectApplicationsTblList = projectAppCtrl
					.getAllChildProjAppByProjAppId(projectApplicationId);
			for (ProjectApplicationsTbl projectApplicationsTblVo : projectApplicationsTblList) {
				final String id = projectApplicationsTblVo.getProjectApplicationId();
				final String name = projectApplicationsTblVo.getName();
				final String description = projectApplicationsTblVo.getDescription();
				boolean isActive = com.magna.xmbackend.vo.enums.Status.ACTIVE.name()
						.equals(projectApplicationsTblVo.getStatus()) ? true : false;

				IconsTbl iconTbl = projectApplicationsTblVo.getIconId();
				Icon icon = new Icon(iconTbl.getIconId(), iconTbl.getIconName(),
						iconFolder + File.separator + iconTbl.getIconName(), iconTbl.getIconType());
				boolean parent = Boolean.parseBoolean(projectApplicationsTblVo.getIsParent());
				String position = projectApplicationsTblVo.getPosition();
				boolean isSingleton = Boolean.parseBoolean(projectApplicationsTblVo.getIsSingleton());
				BaseApplicationsTbl baseApplicationsTbl = projectApplicationsTblVo.getBaseApplicationId();
				String baseApplicationId = baseApplicationsTbl == null ? null
						: baseApplicationsTbl.getBaseApplicationId();

				Map<LANG_ENUM, String> nameMap = new HashMap<>();
				Map<LANG_ENUM, String> descriptionMap = new HashMap<>();
				Map<LANG_ENUM, String> remarksMap = new HashMap<>();
				Map<LANG_ENUM, String> translationMap = new HashMap<>();

				Collection<ProjectAppTranslationTbl> projectAppTranslationTblList = projectApplicationsTblVo
						.getProjectAppTranslationTblCollection();
				for (ProjectAppTranslationTbl projectAppTranslationTbl : projectAppTranslationTblList) {
					LANG_ENUM langEnum = LANG_ENUM
							.getLangEnum(projectAppTranslationTbl.getLanguageCode().getLanguageCode());
					String translationId = projectAppTranslationTbl.getProjectAppTranslationId();
					translationMap.put(langEnum, translationId);
					nameMap.put(langEnum, projectAppTranslationTbl.getName());
					descriptionMap.put(langEnum, projectAppTranslationTbl.getDescription());
					remarksMap.put(langEnum, projectAppTranslationTbl.getRemarks());
				}
				if (!projectAppChildren.containsKey(id)) {
					ProjectApplicationChild projectApplicationChild = new ProjectApplicationChild(id, name, description, nameMap, isActive,
							descriptionMap, remarksMap, icon, parent, isSingleton, position, baseApplicationId,
							CommonConstants.OPERATIONMODE.VIEW);
					projectApplicationChild.setTranslationIdMap(translationMap);

					if (factoryInstance.isHideInActiveItems()) {
						if (isActive) {
							parentProjectApp.add(id, projectApplicationChild);
						}
					} else {
						parentProjectApp.add(id, projectApplicationChild);
					}
				} else {
					IAdminTreeChild iAdminTreeChild = projectAppChildren.get(id);
					if (iAdminTreeChild instanceof ProjectApplicationChild) {
						ProjectApplicationChild projectApplicationChild = (ProjectApplicationChild) iAdminTreeChild;
						projectApplicationChild.setName(name);
						projectApplicationChild.setDescription(description);
						projectApplicationChild.setNameMap(nameMap);
						projectApplicationChild.setActive(isActive);
						projectApplicationChild.setDescriptionMap(descriptionMap);
						projectApplicationChild.setRemarksMap(remarksMap);
						projectApplicationChild.setIcon(icon);
						projectApplicationChild.setParent(parent);
						projectApplicationChild.setSingleton(isSingleton);
						projectApplicationChild.setPosition(position);
						projectApplicationChild.setBaseApplicationId(baseApplicationId);
					}
				}
				dbIds.add(id);
			}
			Set<String> keySet = projectAppChildren.keySet();
			List<String> deactiveObjs = new ArrayList<>();
			for (String key : keySet) {
				if (!dbIds.contains(key) && !ProjectAppAdminAreas.class.getName().equals(key)
						&& !ProjectAppUsers.class.getName().equals(key)
						&& !ProjectAppGroups.class.getName().equals(key)) {
					deactiveObjs.add(key);
				}
			}
			for (String string : deactiveObjs) {
				parentProjectApp.remove(string);
			}
		} catch (Exception e) {
			if (e instanceof ResourceAccessException) {
				Display.getDefault().asyncExec(new Runnable() {
					@Override
					public void run() {
						CustomMessageDialog.openError(Display.getDefault().getActiveShell(), "Error",
								"Server not reachable");
					}
				});
			}
			LOGGER.error("Exeception while getting Child Project Application Relation objects! " + e);
		}
	}

	/**
	 * Method for Load project app users from service.
	 *
	 * @param projectAppUsersNode
	 *            {@link IAdminTreeChild}
	 * @return the list {@link List<IAdminTreeChild>}
	 */
	public List<IAdminTreeChild> loadProjectAppUsersFromService(final IAdminTreeChild projectAppUsersNode) {
		List<IAdminTreeChild> projectAppUsers = new ArrayList<IAdminTreeChild>();
		Map<String, IAdminTreeChild> projectAppUserMap = new LinkedHashMap<>();
		try {
			IAdminTreeChild parent = projectAppUsersNode.getParent();

			if (parent != null && parent instanceof ProjectApplication) {
				String projectAppId = ((ProjectApplication) parent).getProjectApplicationId();

				UserController userController = new UserController();
				List<UsersTbl> usersTbls = userController.getAllUsersByProjectAppId(projectAppId);
				if (usersTbls != null) {
					for (UsersTbl usersTbl : usersTbls) {
						User user = getUpdatedUser(usersTbl);
						if (user != null) {
							RelationObj relationObj = user.getAdapter(projectAppUsersNode.getClass(),
									projectAppUsersNode, user.getUserId(), user.isActive());

							boolean active = relationObj.isActive();
							if (factoryInstance.isHideInActiveItems()) {
								if (active) {
									projectAppUserMap.put(user.getUserId(), relationObj);
								}
							} else {
								projectAppUserMap.put(user.getUserId(), relationObj);
							}
							// projectAppUserMap.put(user.getUserId(),
							// relationObj);
						}
					}

					if (!projectAppUserMap.isEmpty()) {
						projectAppUsers = new ArrayList<>(projectAppUserMap.values());
					}
				}
			}
		} catch (Exception e) {
			if (e instanceof ResourceAccessException) {
				Display.getDefault().asyncExec(new Runnable() {
					@Override
					public void run() {
						CustomMessageDialog.openError(Display.getDefault().getActiveShell(), "Error",
								"Server not reachable");
					}
				});
			}
			LOGGER.error("Exeception while getting Project Application Users! " + e);
		}
		return projectAppUsers;
	}

	/**
	 * Method for Load site children from service.
	 *
	 * @param site
	 *            {@link Site}
	 */
	public void loadSiteChildrenFromService(Site site) {
		SiteAdministrations siteAdministrations = (SiteAdministrations) site.getSiteChildren()
				.get(SiteAdministrations.class.getName());
		loadSiteAdministrationsFromService(site.getSiteId(), siteAdministrations);
	}

	/**
	 * Method for Load site administrations from service.
	 *
	 * @param siteId
	 *            {@link String}
	 * @param siteAdministrations
	 *            {@link SiteAdministrations}
	 */
	public void loadSiteAdministrationsFromService(String siteId, SiteAdministrations siteAdministrations) {
		try {
			siteAdministrations.removeAll();

			SiteAdminAreaRelController siteAdminAreaRelController = new SiteAdminAreaRelController();
			List<SiteAdminAreaRelTbl> siteAdminAreaRelResponse = siteAdminAreaRelController
					.getSiteAdminAreaRelBySiteId(siteId);
			// Iterable<SiteAdminAreaRelTbl> siteAdminAreaRelTbls =
			// siteAdminAreaRelResponse.getSiteAdminAreaRelTbls();
			for (SiteAdminAreaRelTbl siteAdminAreaRelTbl : siteAdminAreaRelResponse) {
				String siteAdminAreaRelId = siteAdminAreaRelTbl.getSiteAdminAreaRelId();
				AdminAreasTbl adminAreaTbl = siteAdminAreaRelTbl.getAdminAreaId();
				AdministrationArea adminAreaa = getUpdatedAdminArea(adminAreaTbl);
				if (adminAreaa != null) {
					boolean relationStatus = com.magna.xmbackend.vo.enums.Status.ACTIVE.name()
							.equals(siteAdminAreaRelTbl.getStatus());
					RelationObj siteAdmin = adminAreaa.getAdapter(SiteAdministrationChild.class, siteAdministrations,
							siteAdminAreaRelId, relationStatus);

					((SiteAdministrationChild) siteAdmin.getContainerObj()).addFixedChildren(siteAdmin);

					if (factoryInstance.isHideInActiveItems()) {
						if (relationStatus) {
							siteAdministrations.add(siteAdminAreaRelId, siteAdmin);
						}
					} else {
						siteAdministrations.add(siteAdminAreaRelId, siteAdmin);
					}

					// siteAdministrations.add(siteAdminAreaRelId, siteAdmin);
				}
			}
		} catch (Exception e) {
			/*if (e instanceof ResourceAccessException) {
				Display.getDefault().asyncExec(new Runnable() {
					@Override
					public void run() {
						CustomMessageDialog.openError(Display.getDefault().getActiveShell(), "Error",
								"Server not reachable");
					}
				});
			}*/
			LOGGER.error("Exeception while getting site Administration Area Relation objects! " + e);
		}
	}

	/**
	 * Method for Load site admin area children from service.
	 *
	 * @param siteAdminAreaRelId
	 *            {@link String}
	 * @param element
	 *            {@link RelationObj}
	 */
	public void loadSiteAdminAreaChildrenFromService(String siteAdminAreaRelId, RelationObj element) {
		SiteAdministrationChild containerObj = (SiteAdministrationChild) element.getContainerObj();
		SiteAdminAreaProjects siteAdminAreaProjects = (SiteAdminAreaProjects) containerObj.getSiteAdminAreaChildren()
				.get(SiteAdminAreaProjects.class.getName());
		loadSiteAdminAreaProjectsFromService(siteAdminAreaRelId, siteAdminAreaProjects);
	}

	/**
	 * Method for Load site admin area projects from service.
	 *
	 * @param siteAdminAreaRelId
	 *            {@link String}
	 * @param iAdminTreeFixedNode
	 *            {@link IAdminTreeChild}
	 */
	public void loadSiteAdminAreaProjectsFromService(String siteAdminAreaRelId, IAdminTreeChild iAdminTreeFixedNode) {
		try {
			if (iAdminTreeFixedNode instanceof SiteAdminAreaProjects) {
				((SiteAdminAreaProjects) iAdminTreeFixedNode).removeAll();
			} else if (iAdminTreeFixedNode instanceof AdminAreaProjects) {
				((AdminAreaProjects) iAdminTreeFixedNode).removeAll();
			}

			SiteAdminAreaRelController siteCtrl = new SiteAdminAreaRelController();
			List<SiteAdminAreaRelIdWithAdminAreaProjRel> adminAreaProjRelTblList = siteCtrl
					.getAllProjectsBySiteAdminRel(siteAdminAreaRelId);
			if (adminAreaProjRelTblList != null) {
				for (SiteAdminAreaRelIdWithAdminAreaProjRel adminAreaProjRelTbl : adminAreaProjRelTblList) {
					ProjectsTbl projectsTbl = adminAreaProjRelTbl.getProjectsTbl();
					Project project = getUpdatedProject(projectsTbl);
					if (project != null) {
						String siteAdminAreaProjRelId = adminAreaProjRelTbl.getAdminAreaProjRelId();
						boolean relationStatus = com.magna.xmbackend.vo.enums.Status.ACTIVE.name()
								.equals(adminAreaProjRelTbl.getStatus());
						if (iAdminTreeFixedNode instanceof SiteAdminAreaProjects) {
							RelationObj siteAdminProjChildRel = project.getAdapter(SiteAdminAreaProjectChild.class,
									iAdminTreeFixedNode, siteAdminAreaProjRelId, relationStatus);
							((SiteAdminAreaProjectChild) siteAdminProjChildRel.getContainerObj())
									.addFixedChildren(siteAdminProjChildRel);

							if (factoryInstance.isHideInActiveItems()) {
								if (relationStatus) {
									((SiteAdminAreaProjects) iAdminTreeFixedNode).add(siteAdminAreaProjRelId,
											siteAdminProjChildRel);
								}
							} else {
								((SiteAdminAreaProjects) iAdminTreeFixedNode).add(siteAdminAreaProjRelId,
										siteAdminProjChildRel);
							}
						} else if (iAdminTreeFixedNode instanceof AdminAreaProjects) {
							RelationObj AdminProjChildRel = project.getAdapter(AdminAreaProjectChild.class,
									iAdminTreeFixedNode, siteAdminAreaProjRelId, relationStatus);
							((AdminAreaProjectChild) AdminProjChildRel.getContainerObj())
									.addFixedChildren(AdminProjChildRel);

							if (factoryInstance.isHideInActiveItems()) {
								if (relationStatus) {
									((AdminAreaProjects) iAdminTreeFixedNode).add(siteAdminAreaProjRelId,
											AdminProjChildRel);
								}
							} else {
								((AdminAreaProjects) iAdminTreeFixedNode).add(siteAdminAreaProjRelId,
										AdminProjChildRel);
							}
						}
					}
				}
			}
		} catch (Exception e) {
			/*if (e instanceof ResourceAccessException) {
				Display.getDefault().asyncExec(new Runnable() {
					@Override
					public void run() {
						CustomMessageDialog.openError(Display.getDefault().getActiveShell(), "Error",
								"Server not reachable");
					}
				});
			}*/
			LOGGER.error("Exeception while getting site Administration Area Project Relation objects! " + e);
		}
	}

	/**
	 * Method for Load site admin area user apps from service.
	 *
	 * @param userApplicationsNode
	 *            {@link IAdminTreeChild}
	 * @return the list {@link List<IAdminTreeChild>}
	 */
	public List<IAdminTreeChild> loadSiteAdminAreaUserAppsFromService(IAdminTreeChild userApplicationsNode) {

		String relationType = null;
		List<IAdminTreeChild> siteAdminAreaUserApps = new ArrayList<IAdminTreeChild>();
		try {
			IAdminTreeChild parentNode = null;
			if (userApplicationsNode instanceof SiteAdminAreaUserApplications) {
				parentNode = ((SiteAdminAreaUserApplications) userApplicationsNode).getParent();
			} else if (userApplicationsNode instanceof SiteAdminAreaUserAppNotFixed) {
				parentNode = ((SiteAdminAreaUserAppNotFixed) userApplicationsNode).getParent().getParent();
				relationType = "NOTFIXED";
			} else if (userApplicationsNode instanceof SiteAdminAreaUserAppFixed) {
				parentNode = ((SiteAdminAreaUserAppFixed) userApplicationsNode).getParent().getParent();
				relationType = "FIXED";
			} else if (userApplicationsNode instanceof SiteAdminAreaUserAppProtected) {
				parentNode = ((SiteAdminAreaUserAppProtected) userApplicationsNode).getParent().getParent();
				relationType = "PROTECTED";
			} else if (userApplicationsNode instanceof AdminAreaUserApplications) {
				parentNode = ((AdminAreaUserApplications) userApplicationsNode).getParent();
			} else if (userApplicationsNode instanceof AdminAreaUserAppNotFixed) {
				parentNode = ((AdminAreaUserAppNotFixed) userApplicationsNode).getParent().getParent();
				relationType = "NOTFIXED";
			} else if (userApplicationsNode instanceof AdminAreaUserAppFixed) {
				parentNode = ((AdminAreaUserAppFixed) userApplicationsNode).getParent().getParent();
				relationType = "FIXED";
			} else if (userApplicationsNode instanceof AdminAreaUserAppProtected) {
				parentNode = ((AdminAreaUserAppProtected) userApplicationsNode).getParent().getParent();
				relationType = "PROTECTED";
			}

			if (parentNode != null) {
				String siteAdminAreaRelId = null;
				if (parentNode instanceof RelationObj) {
					siteAdminAreaRelId = ((RelationObj) parentNode).getRelId();
				} else if (parentNode instanceof AdministrationArea) {
					siteAdminAreaRelId = ((AdministrationArea) parentNode).getRelationId();
				}

				SiteAdminAreaRelController siteAdminCtrl = new SiteAdminAreaRelController();
				List<SiteAdminAreaRelIdWithAdminAreaUsrAppRel> adminAreaUserAppRelTblList = null;
				if (relationType != null && relationType.length() > 0) {
					adminAreaUserAppRelTblList = siteAdminCtrl.getAllUserAppsBySiteAdminRel(siteAdminAreaRelId,
							relationType);
				} else {
					adminAreaUserAppRelTblList = siteAdminCtrl.getAllUserAppsBySiteAdminRel(siteAdminAreaRelId);
				}
				if (adminAreaUserAppRelTblList != null) {
					for (SiteAdminAreaRelIdWithAdminAreaUsrAppRel adminAreaUserAppRelTbl : adminAreaUserAppRelTblList) {
						UserApplicationsTbl userApplicationsTbl = adminAreaUserAppRelTbl.getUserApplicationsTbl();
						UserApplication userApplication = getUpdatedUserApplication(userApplicationsTbl);
						if (userApplication != null) {
							String siteAdminAreaUserAppRelId = adminAreaUserAppRelTbl.getAdminAreaUsrAppRelId();
							boolean relationStatus = com.magna.xmbackend.vo.enums.Status.ACTIVE.name()
									.equals(adminAreaUserAppRelTbl.getStatus());
							RelationObj siteAdminUserApp = userApplication.getAdapter(userApplicationsNode.getClass(),
									userApplicationsNode, siteAdminAreaUserAppRelId, relationStatus);

							if (factoryInstance.isHideInActiveItems()) {
								if (relationStatus) {
									siteAdminAreaUserApps.add(siteAdminUserApp);
								}
							} else {
								siteAdminAreaUserApps.add(siteAdminUserApp);
							}
						}
					}
				}
			}
		} catch (Exception e) {
			if (e instanceof ResourceAccessException) {
				Display.getDefault().asyncExec(new Runnable() {
					@Override
					public void run() {
						CustomMessageDialog.openError(Display.getDefault().getActiveShell(), "Error",
								"Server not reachable");
					}
				});
			}
			LOGGER.error("Exeception while getting user application based on site Administration Area Relation! " + e);
		}
		return siteAdminAreaUserApps;
	}

	/**
	 * Method for Load site admin area proj apps from service.
	 *
	 * @param projectApplicationsNode
	 *            {@link IAdminTreeChild}
	 * @return the list {@link List<IAdminTreeChild>}
	 */
	public List<IAdminTreeChild> loadSiteAdminAreaProjAppsFromService(IAdminTreeChild projectApplicationsNode) {

		String relationType = null;
		String siteAdminAreaProjRelId = CommonConstants.EMPTY_STR;
		List<IAdminTreeChild> siteAdminAreaProjectApps = new ArrayList<IAdminTreeChild>();
		try {
			IAdminTreeChild parentNode = null;
			if (projectApplicationsNode instanceof SiteAdminAreaProjectApplications) {
				parentNode = ((SiteAdminAreaProjectApplications) projectApplicationsNode).getParent();
			} else if (projectApplicationsNode instanceof SiteAdminProjectAppNotFixed) {
				parentNode = ((SiteAdminProjectAppNotFixed) projectApplicationsNode).getParent().getParent();
				relationType = "NOTFIXED";
			} else if (projectApplicationsNode instanceof SiteAdminProjectAppFixed) {
				parentNode = ((SiteAdminProjectAppFixed) projectApplicationsNode).getParent().getParent();
				relationType = "FIXED";
			} else if (projectApplicationsNode instanceof SiteAdminProjectAppProtected) {
				parentNode = ((SiteAdminProjectAppProtected) projectApplicationsNode).getParent().getParent();
				relationType = "PROTECTED";
			} else if (projectApplicationsNode instanceof AdminAreaProjectApplications) {
				parentNode = ((AdminAreaProjectApplications) projectApplicationsNode).getParent();
			} else if (projectApplicationsNode instanceof AdminAreaProjectAppNotFixed) {
				parentNode = ((AdminAreaProjectAppNotFixed) projectApplicationsNode).getParent().getParent();
				relationType = "NOTFIXED";
			} else if (projectApplicationsNode instanceof AdminAreaProjectAppFixed) {
				parentNode = ((AdminAreaProjectAppFixed) projectApplicationsNode).getParent().getParent();
				relationType = "FIXED";
			} else if (projectApplicationsNode instanceof AdminAreaProjectAppProtected) {
				parentNode = ((AdminAreaProjectAppProtected) projectApplicationsNode).getParent().getParent();
				relationType = "PROTECTED";
			} else if (projectApplicationsNode instanceof ProjectAdminAreaProjectApplications) {
				parentNode = ((ProjectAdminAreaProjectApplications) projectApplicationsNode).getParent();
			} else if (projectApplicationsNode instanceof ProjectAdminAreaProjectAppNotFixed) {
				parentNode = ((ProjectAdminAreaProjectAppNotFixed) projectApplicationsNode).getParent().getParent();
				relationType = "NOTFIXED";
			} else if (projectApplicationsNode instanceof ProjectAdminAreaProjectAppFixed) {
				parentNode = ((ProjectAdminAreaProjectAppFixed) projectApplicationsNode).getParent().getParent();
				relationType = "FIXED";
			} else if (projectApplicationsNode instanceof ProjectAdminAreaProjectAppProtected) {
				parentNode = ((ProjectAdminAreaProjectAppProtected) projectApplicationsNode).getParent().getParent();
				relationType = "PROTECTED";
			}

			if (parentNode != null && parentNode instanceof RelationObj) {
				siteAdminAreaProjRelId = ((RelationObj) parentNode).getRelId();
			} else if (parentNode != null && parentNode instanceof AdministrationArea) {
				siteAdminAreaProjRelId = ((AdministrationArea) parentNode).getRelationId();
			}

			AdminAreaProjectRelController adminAreaProjCntrl = new AdminAreaProjectRelController();
			List<AdminAreaProjectRelIdWithProjectAppRel> adminAreaProjectAppRelTblList = null;
			if (relationType != null && relationType.length() > 0) {
				adminAreaProjectAppRelTblList = adminAreaProjCntrl
						.getAllProjectAppsBySiteAdminProjRel(siteAdminAreaProjRelId, relationType);
			} else {
				adminAreaProjectAppRelTblList = adminAreaProjCntrl
						.getAllProjectAppsBySiteAdminProjRel(siteAdminAreaProjRelId);
			}
			if (adminAreaProjectAppRelTblList != null) {
				for (AdminAreaProjectRelIdWithProjectAppRel adminAreaProjectAppRelTbl : adminAreaProjectAppRelTblList) {
					ProjectApplicationsTbl projectApplicationsTbl = adminAreaProjectAppRelTbl
							.getProjectApplicationsTbl();
					ProjectApplication projectApplication = getUpdatedProjectApplication(projectApplicationsTbl);
					if (projectApplication != null) {
						String siteAdminAreaProjectAppRelId = adminAreaProjectAppRelTbl.getAdminAreaProjectAppRelId();
						boolean relationStatus = com.magna.xmbackend.vo.enums.Status.ACTIVE.name()
								.equals(adminAreaProjectAppRelTbl.getStatus());
						RelationObj siteAdminUserApp = projectApplication.getAdapter(projectApplicationsNode.getClass(),
								projectApplicationsNode, siteAdminAreaProjectAppRelId, relationStatus);

						if (factoryInstance.isHideInActiveItems()) {
							if (relationStatus) {
								siteAdminAreaProjectApps.add(siteAdminUserApp);
							}
						} else {
							siteAdminAreaProjectApps.add(siteAdminUserApp);
						}
						// siteAdminAreaProjectApps.add(siteAdminUserApp);
					}
				}
			}
		} catch (Exception e) {
			if (e instanceof ResourceAccessException) {
				Display.getDefault().asyncExec(new Runnable() {
					@Override
					public void run() {
						CustomMessageDialog.openError(Display.getDefault().getActiveShell(), "Error",
								"Server not reachable");
					}
				});
			}
			LOGGER.error(
					"Exeception while getting project application based on site Administration Area Project Relation! "
							+ e);
		}
		return siteAdminAreaProjectApps;
	}

	/**
	 * Method for Load site admin area start apps from service.
	 *
	 * @param startApplicationsNode
	 *            {@link IAdminTreeChild}
	 * @return the list {@link List<IAdminTreeChild>}
	 */
	public List<IAdminTreeChild> loadSiteAdminAreaStartAppsFromService(IAdminTreeChild startApplicationsNode) {

		List<IAdminTreeChild> siteAdminAreaStartApps = new ArrayList<IAdminTreeChild>();
		try {
			IAdminTreeChild parent = startApplicationsNode.getParent();

			if (parent != null) {
				String siteAdminAreaRelId = null;
				if (parent instanceof RelationObj) {
					siteAdminAreaRelId = ((RelationObj) parent).getRelId();
				} else if (parent instanceof AdministrationArea) {
					siteAdminAreaRelId = ((AdministrationArea) parent).getRelationId();
				}

				SiteAdminAreaRelController siteAdminCtrl = new SiteAdminAreaRelController();
				List<SiteAdminAreaRelIdWithAdminAreaStartAppRel> adminAreaStartAppRelTblList = siteAdminCtrl
						.getAllStartAppsBySiteAdminRel(siteAdminAreaRelId);
				if (adminAreaStartAppRelTblList != null) {
					for (SiteAdminAreaRelIdWithAdminAreaStartAppRel adminAreaStartAppRelTbl : adminAreaStartAppRelTblList) {
						StartApplicationsTbl startApplicationsTbl = adminAreaStartAppRelTbl.getStartApplicationsTbl();
						StartApplication startApplication = getUpdatedStartApplication(startApplicationsTbl);
						if (startApplication != null) {
							String siteAdminAreaStartAppRelId = adminAreaStartAppRelTbl.getAdminAreaStartAppRelId();
							boolean relationStatus = com.magna.xmbackend.vo.enums.Status.ACTIVE.name()
									.equals(adminAreaStartAppRelTbl.getStatus());
							RelationObj siteAdminStartApp = startApplication.getAdapter(
									startApplicationsNode.getClass(), startApplicationsNode, siteAdminAreaStartAppRelId,
									relationStatus);

							if (factoryInstance.isHideInActiveItems()) {
								if (relationStatus) {
									siteAdminAreaStartApps.add(siteAdminStartApp);
								}
							} else {
								siteAdminAreaStartApps.add(siteAdminStartApp);
							}
						}
					}
				}
			}
		} catch (Exception e) {
			if (e instanceof ResourceAccessException) {
				Display.getDefault().asyncExec(new Runnable() {
					@Override
					public void run() {
						CustomMessageDialog.openError(Display.getDefault().getActiveShell(), "Error",
								"Server not reachable");
					}
				});
			}
			LOGGER.error("Exeception while getting start application based on site Administration Area Relation! " + e);
		}
		return siteAdminAreaStartApps;
	}

	/**
	 * Method for Load site admin area proj start apps from service.
	 *
	 * @param startApplicationsNode
	 *            {@link IAdminTreeChild}
	 * @return the list {@link List<IAdminTreeChild>}
	 */
	public List<IAdminTreeChild> loadSiteAdminAreaProjStartAppsFromService(IAdminTreeChild startApplicationsNode) {

		List<IAdminTreeChild> siteAdminAreaProjStartApps = new ArrayList<IAdminTreeChild>();
		try {
			IAdminTreeChild parent = startApplicationsNode.getParent();

			if (parent != null && parent instanceof RelationObj) {
				String siteAdminAreaProjRelId = ((RelationObj) parent).getRelId();

				AdminAreaProjectRelController adminAreaProjCntrl = new AdminAreaProjectRelController();
				List<AdminAreaProjectRelIdWithProjectStartAppRel> adminAreaProjAppRelTblList = adminAreaProjCntrl
						.getAllStartAppsBySiteAdminProjRel(siteAdminAreaProjRelId);

				if (adminAreaProjAppRelTblList != null) {
					for (AdminAreaProjectRelIdWithProjectStartAppRel adminAreaProjAppRelTbl : adminAreaProjAppRelTblList) {
						StartApplicationsTbl startApplicationsTbl = adminAreaProjAppRelTbl.getStartApplicationsTbl();
						StartApplication startApplication = getUpdatedStartApplication(startApplicationsTbl);
						if (startApplication != null) {
							String siteAdminAreaProjStartAppRelId = adminAreaProjAppRelTbl.getProjectStartAppRelId();
							boolean relationStatus = com.magna.xmbackend.vo.enums.Status.ACTIVE.name()
									.equals(adminAreaProjAppRelTbl.getStatus());
							RelationObj siteAdminStartApp = startApplication.getAdapter(
									startApplicationsNode.getClass(), startApplicationsNode,
									siteAdminAreaProjStartAppRelId, relationStatus);

							if (factoryInstance.isHideInActiveItems()) {
								if (relationStatus) {
									siteAdminAreaProjStartApps.add(siteAdminStartApp);
								}
							} else {
								siteAdminAreaProjStartApps.add(siteAdminStartApp);
							}
						}
					}
				}
			}
		} catch (Exception e) {
			if (e instanceof ResourceAccessException) {
				Display.getDefault().asyncExec(new Runnable() {
					@Override
					public void run() {
						CustomMessageDialog.openError(Display.getDefault().getActiveShell(), "Error",
								"Server not reachable");
					}
				});
			}
			LOGGER.error("Exeception while getting start application based on site Administration Area Project Relation! "
							+ e);
		}
		return siteAdminAreaProjStartApps;
	}

	/**
	 * Method for Load admin area children from service.
	 *
	 * @param administrationArea
	 *            {@link AdministrationArea}
	 */
	public void loadAdminAreaChildrenFromService(AdministrationArea administrationArea) {
		String adminAreaRelationId = administrationArea.getRelationId();
		AdminAreaProjects adminAreaProjects = (AdminAreaProjects) administrationArea.getAdminAreaChildren()
				.get(AdminAreaProjects.class.getName());
		loadSiteAdminAreaProjectsFromService(adminAreaRelationId, adminAreaProjects);
	}

	/**
	 * Method for Load project children from service.
	 *
	 * @param project
	 *            {@link Project}
	 */
	public void loadProjectChildrenFromService(Project project) {
		String projectId = project.getProjectId();
		ProjectAdminAreas projectAdminAreas = (ProjectAdminAreas) project.getProjectChildren()
				.get(ProjectAdminAreas.class.getName());
		loadProjectAdminAreasFromService(projectId, projectAdminAreas);
		ProjectUsers projectUsers = (ProjectUsers) project.getProjectChildren().get(ProjectUsers.class.getName());
		loadProjectUsersFromService(projectId, projectUsers);
	}

	/**
	 * Method for Load project admin areas from service.
	 *
	 * @param projectId
	 *            {@link String}
	 * @param projectAdminAreas
	 *            {@link ProjectAdminAreas}
	 */
	public void loadProjectAdminAreasFromService(String projectId, ProjectAdminAreas projectAdminAreas) {
		try {
			projectAdminAreas.removeAll();
			AdminAreaProjectRelController adminAreaProjRelCtrl = new AdminAreaProjectRelController();
			AdminAreaProjectResponseWrapper adminAreaProjectResponseWrapper = adminAreaProjRelCtrl
					.getAAProjectRelByProjectId(projectId);
			if (adminAreaProjectResponseWrapper != null) {
				List<AdminAreaProjectResponse> adminAreaProjectResponseList = adminAreaProjectResponseWrapper
						.getAdminAreaProjectResponses();
				for (AdminAreaProjectResponse adminAreaProjectResponse : adminAreaProjectResponseList) {
					AdminAreaProjectRelTbl adminAreaProjectRelTbl = adminAreaProjectResponse
							.getAdminAreaProjectRelTbls();
					AdminAreasTbl adminAreaTbl = adminAreaProjectResponse.getAdminAreasTbls();
					AdministrationArea adminAreaa = getUpdatedAdminArea(adminAreaTbl);
					if (adminAreaa != null) {
						String relationId = adminAreaProjectRelTbl.getAdminAreaProjectRelId();
						boolean relationStatus = com.magna.xmbackend.vo.enums.Status.ACTIVE.name()
								.equals(adminAreaProjectRelTbl.getStatus());
						RelationObj relationObj = adminAreaa.getAdapter(ProjectAdminAreaChild.class, projectAdminAreas,
								relationId, relationStatus);
						((ProjectAdminAreaChild) relationObj.getContainerObj()).addFixedChildren(relationObj);

						boolean active = relationObj.isActive();
						if (factoryInstance.isHideInActiveItems()) {
							if (active) {
								projectAdminAreas.add(relationId, relationObj);
							}
						} else {
							projectAdminAreas.add(relationId, relationObj);
						}
					}
				}
			}
		} catch (Exception e) {
			/*if (e instanceof ResourceAccessException) {
				Display.getDefault().asyncExec(new Runnable() {
					@Override
					public void run() {
						CustomMessageDialog.openError(Display.getDefault().getActiveShell(), "Error",
								"Server not reachable");
					}
				});
			}*/
			LOGGER.error("Exeception while getting site Administration Area Relation objects! " + e);
		}
	}

	/**
	 * Method for Load project users from service.
	 *
	 * @param projectId
	 *            {@link String}
	 * @param projectUsers
	 *            {@link ProjectUsers}
	 */
	public void loadProjectUsersFromService(String projectId, ProjectUsers projectUsers) {
		try {
			projectUsers.removeAll();
			final UserProjectRelController userProjectRelController = new UserProjectRelController();
			List<UserProjectRelTbl> userProjectRelResponse;
			if ((userProjectRelResponse = userProjectRelController.getUserProjectRelByProjectId(projectId)) != null) {
				for (UserProjectRelTbl userProjectRelTbl : userProjectRelResponse) {
					UsersTbl usersTbl = userProjectRelTbl.getUserId();
					User user = getUpdatedUser(usersTbl);
					if (user != null) {
						String userProjectRelId = userProjectRelTbl.getUserProjectRelId();
						boolean relationStatus = com.magna.xmbackend.vo.enums.Status.ACTIVE.name()
								.equals(userProjectRelTbl.getStatus());
						RelationObj userProjectRel = user.getAdapter(ProjectUserChild.class, projectUsers,
								userProjectRelId, relationStatus);
						((ProjectUserChild) userProjectRel.getContainerObj()).addFixedChildren(userProjectRel);

						boolean active = userProjectRel.isActive();
						if (factoryInstance.isHideInActiveItems()) {
							if (active) {
								projectUsers.add(userProjectRelId, userProjectRel);
							}
						} else {
							projectUsers.add(userProjectRelId, userProjectRel);
						}
					}
				}
			}
		} catch (Exception e) {
			/*if (e instanceof ResourceAccessException) {
				Display.getDefault().asyncExec(new Runnable() {
					@Override
					public void run() {
						CustomMessageDialog.openError(Display.getDefault().getActiveShell(), "Error",
								"Server not reachable");
					}
				});
			}*/
			LOGGER.error("Exeception while getting User Projects objects! " + e);
		}
	}

	/**
	 * Method for Load base app project app from service.
	 *
	 * @param baseAppProjectApplications
	 *            {@link BaseAppProjectApplications}
	 * @return the list {@link List<IAdminTreeChild>}
	 */
	public List<IAdminTreeChild> loadBaseAppProjectAppFromService(
			BaseAppProjectApplications baseAppProjectApplications) {
		BaseApplication baseApplication = (BaseApplication) baseAppProjectApplications.getParent();
		String baseAppId = baseApplication.getBaseApplicationId();
		List<IAdminTreeChild> baseAppProjectApps = new ArrayList<IAdminTreeChild>();
		final File iconFolder = XMSystemUtil.getXMSystemServerIconsFolder();
		if (iconFolder == null) {
			CustomMessageDialog.openError(Display.getDefault().getActiveShell(), "Error",
					"Icon folder is not reachable");
			return null;
		}

		try {
			ProjectAppController projectAppController = new ProjectAppController();
			List<ProjectApplicationsTbl> projectAppResponse = projectAppController.getProjectAppByBaseAppId(baseAppId);
			for (ProjectApplicationsTbl projectApplicationsTbl : projectAppResponse) {
				ProjectApplication projectApplication = getUpdatedProjectApplication(projectApplicationsTbl);
				if (projectApplication != null) {
					RelationObj baseAppProjectAppRelObj = projectApplication.getAdapter(
							BaseAppProjectApplications.class, baseApplication,
							projectApplication.getProjectApplicationId(), projectApplication.isActive());

					boolean active = baseAppProjectAppRelObj.isActive();
					if (factoryInstance.isHideInActiveItems()) {
						if (active) {
							baseAppProjectApps.add(baseAppProjectAppRelObj);
						}
					} else {
						baseAppProjectApps.add(baseAppProjectAppRelObj);
					}
				}
			}
		} catch (Exception e) {
			if (e instanceof ResourceAccessException) {
				Display.getDefault().asyncExec(new Runnable() {
					@Override
					public void run() {
						CustomMessageDialog.openError(Display.getDefault().getActiveShell(), "Error",
								"Server not reachable");
					}
				});
			}
			LOGGER.error("Exeception while getting project application based on base application Relation! " + e);
		}

		return baseAppProjectApps;
	}

	/**
	 * Method for Load base app user app from service.
	 *
	 * @param baseAppUserApplications
	 *            {@link BaseAppUserApplications}
	 * @return the list {@link List<IAdminTreeChild>}
	 */
	public List<IAdminTreeChild> loadBaseAppUserAppFromService(BaseAppUserApplications baseAppUserApplications) {
		BaseApplication baseApplication = (BaseApplication) baseAppUserApplications.getParent();
		String baseAppId = baseApplication.getBaseApplicationId();
		List<IAdminTreeChild> baseAppUserApps = new ArrayList<IAdminTreeChild>();
		final File iconFolder = XMSystemUtil.getXMSystemServerIconsFolder();
		if (iconFolder == null) {
			CustomMessageDialog.openError(Display.getDefault().getActiveShell(), "Error",
					"Icon folder is not reachable");
			return null;
		}

		try {
			UserAppController userAppController = new UserAppController();
			List<UserApplicationsTbl> allUserAppByBaseAppRes = userAppController.getAllUserAppByBaseAppId(baseAppId);
			for (UserApplicationsTbl userApplicationsTbl : allUserAppByBaseAppRes) {
				UserApplication userApplication = getUpdatedUserApplication(userApplicationsTbl);
				if (userApplication != null) {
					RelationObj baseAppUserAppRelObj = userApplication.getAdapter(BaseAppUserApplications.class,
							baseApplication, userApplication.getUserApplicationId(), userApplication.isActive());

					boolean active = baseAppUserAppRelObj.isActive();
					if (factoryInstance.isHideInActiveItems()) {
						if (active) {
							baseAppUserApps.add(baseAppUserAppRelObj);
						}
					} else {
						baseAppUserApps.add(baseAppUserAppRelObj);
					}
				}
			}
		} catch (Exception e) {
			if (e instanceof ResourceAccessException) {
				Display.getDefault().asyncExec(new Runnable() {
					@Override
					public void run() {
						CustomMessageDialog.openError(Display.getDefault().getActiveShell(), "Error",
								"Server not reachable");
					}
				});
			}
			LOGGER.error("Exeception while getting user application based on base application Relation! " + e);
		}
		return baseAppUserApps;
	}

	/**
	 * Method for Load base app start app from service.
	 *
	 * @param baseAppStartApplications
	 *            {@link BaseAppStartApplications}
	 * @return the list {@link List<IAdminTreeChild>}
	 */
	public List<IAdminTreeChild> loadBaseAppStartAppFromService(BaseAppStartApplications baseAppStartApplications) {
		BaseApplication baseApplication = (BaseApplication) baseAppStartApplications.getParent();
		String baseAppId = baseApplication.getBaseApplicationId();
		List<IAdminTreeChild> baseAppStartApps = new ArrayList<IAdminTreeChild>();
		final File iconFolder = XMSystemUtil.getXMSystemServerIconsFolder();
		if (iconFolder == null) {
			CustomMessageDialog.openError(Display.getDefault().getActiveShell(), "Error",
					"Icon folder is not reachable");
			return null;
		}

		try {
			StartAppController startAppController = new StartAppController();
			List<StartApplicationsTbl> startAppByBaseAppIdRes = startAppController.getStartAppByBaseAppId(baseAppId);
			for (StartApplicationsTbl startApplicationsTbl : startAppByBaseAppIdRes) {
				StartApplication startApplication = getUpdatedStartApplication(startApplicationsTbl);
				if (startApplication != null) {
					RelationObj baseAppStartAppRelObj = startApplication.getAdapter(BaseAppStartApplications.class,
							baseApplication, startApplication.getStartPrgmApplicationId(), startApplication.isActive());

					boolean active = baseAppStartAppRelObj.isActive();
					if (factoryInstance.isHideInActiveItems()) {
						if (active) {
							baseAppStartApps.add(baseAppStartAppRelObj);
						}
					} else {
						baseAppStartApps.add(baseAppStartAppRelObj);
					}
				}
			}
		} catch (Exception e) {
			if (e instanceof ResourceAccessException) {
				Display.getDefault().asyncExec(new Runnable() {
					@Override
					public void run() {
						CustomMessageDialog.openError(Display.getDefault().getActiveShell(), "Error",
								"Server not reachable");
					}
				});
			}
			LOGGER.error("Exeception while getting user application based on base application Relation! " + e);
		}
		return baseAppStartApps;
	}

	/**
	 * Method for Load project site AA start app from service.
	 *
	 * @param projectSiteAAChildStartApp
	 *            {@link IAdminTreeChild}
	 * @return the list {@link List<IAdminTreeChild>}
	 */
	public List<IAdminTreeChild> loadProjectSiteAAStartAppFromService(IAdminTreeChild projectSiteAAChildStartApp) {
		List<IAdminTreeChild> projectSiteAdminAreaStartApps = new ArrayList<IAdminTreeChild>();
		try {
			String administrationAreaId = ((AdministrationArea) ((RelationObj) projectSiteAAChildStartApp.getParent())
					.getRefObject()).getAdministrationAreaId();
			String siteId = ((Site) ((RelationObj) ((RelationObj) projectSiteAAChildStartApp.getParent()).getParent()
					.getParent()).getRefObject()).getSiteId();
			String projectId = ((Project) ((RelationObj) ((RelationObj) projectSiteAAChildStartApp.getParent())
					.getParent().getParent()).getContainerObj().getParent().getParent()).getProjectId();

			ProjectStartAppRelController projectStartAppRelController = new ProjectStartAppRelController();
			ProjectStartAppRelResponse projectStartAppRelResponse = projectStartAppRelController
					.getProjectStartAppByProjectSiteAAId(siteId, administrationAreaId, projectId);
			Iterable<ProjectStartAppRelTbl> projectStartAppRelTbls = projectStartAppRelResponse
					.getProjectStartAppRelTbls();
			for (ProjectStartAppRelTbl projectStartAppRelTbl : projectStartAppRelTbls) {
				StartApplicationsTbl startApplicationsTbl = projectStartAppRelTbl.getStartApplicationId();
				StartApplication startApplication = getUpdatedStartApplication(startApplicationsTbl);
				if (startApplication != null) {
					RelationObj projectSiteAdminStartApp = startApplication.getAdapter(
							projectSiteAAChildStartApp.getClass(), projectSiteAAChildStartApp.getParent(),
							startApplication.getStartPrgmApplicationId(), startApplication.isActive());

					boolean active = projectSiteAdminStartApp.isActive();
					if (factoryInstance.isHideInActiveItems()) {
						if (active) {
							projectSiteAdminAreaStartApps.add(projectSiteAdminStartApp);
						}
					} else {
						projectSiteAdminAreaStartApps.add(projectSiteAdminStartApp);
					}

					// projectSiteAdminAreaStartApps.add(projectSiteAdminStartApp);
				}
			}
		} catch (Exception e) {
			if (e instanceof ResourceAccessException) {
				Display.getDefault().asyncExec(new Runnable() {
					@Override
					public void run() {
						CustomMessageDialog.openError(Display.getDefault().getActiveShell(), "Error",
								"Server not reachable");
					}
				});
			}
			LOGGER.error(
					"Exeception while getting start application based on project site Administration Area Relation! "
							+ e);
		}
		return projectSiteAdminAreaStartApps;
	}

	/**
	 * Method for Load user AA user apps from service.
	 *
	 * @param userApplicationsNode
	 *            {@link IAdminTreeChild}
	 * @return the list {@link List<IAdminTreeChild>}
	 */
	public List<IAdminTreeChild> loadUserAAUserAppsFromService(IAdminTreeChild userApplicationsNode) {

		String relationType = null;
		List<IAdminTreeChild> userUserApps = new ArrayList<IAdminTreeChild>();
		try {
			String userId = null;
			String siteAARelId = null;
			if (userApplicationsNode instanceof UserAAUserApplications) {
				userId = ((User) userApplicationsNode.getParent().getParent().getParent()).getUserId();
				siteAARelId = ((RelationObj) userApplicationsNode.getParent()).getRelId();
			} else if (userApplicationsNode instanceof UserAAUserAppAllowed) {
				userId = ((User) userApplicationsNode.getParent().getParent().getParent().getParent()).getUserId();
				siteAARelId = ((RelationObj) userApplicationsNode.getParent().getParent()).getRelId();
				relationType = UserRelationType.ALLOWED.name();
			} else if (userApplicationsNode instanceof UserAAUserAppForbidden) {
				userId = ((User) userApplicationsNode.getParent().getParent().getParent().getParent()).getUserId();
				siteAARelId = ((RelationObj) userApplicationsNode.getParent().getParent()).getRelId();
				relationType = UserRelationType.FORBIDDEN.name();
			}

			if (userId != null && siteAARelId != null) {

				UserUserAppRelController userUserAppRelController = new UserUserAppRelController();
				List<UserUserAppRelTbl> userUserAppRelTbls= null;
				if (relationType != null && relationType.length() > 0) {
					userUserAppRelTbls  = userUserAppRelController.getUserUserAppRelsByUserIdSiteAARelIdRelType(userId,
							siteAARelId, relationType);
				} else {
					userUserAppRelTbls = userUserAppRelController.getUserUserAppRelsByUserIdSiteAARelId(userId,
							siteAARelId);
				}
				if (userUserAppRelTbls != null) {
					for (UserUserAppRelTbl userUserAppRelTbl : userUserAppRelTbls) {
						UserApplicationsTbl userApplicationsTbl = userUserAppRelTbl.getUserApplicationId();
						UserApplication userApplication = getUpdatedUserApplication(userApplicationsTbl);
						if (userApplication != null) {
							String userUserAppRelId = userUserAppRelTbl.getUserUserAppRelId();
							RelationObj userUserAppRelObj = userApplication.getAdapter(userApplicationsNode.getClass(),
									userApplicationsNode, userUserAppRelId, userApplication.isActive());

							boolean active = userUserAppRelObj.isActive();
							if (factoryInstance.isHideInActiveItems()) {
								if (active) {
									userUserApps.add(userUserAppRelObj);
								}
							} else {
								userUserApps.add(userUserAppRelObj);
							}
						}
					}
				}
			}
		} catch (Exception e) {
			if (e instanceof ResourceAccessException) {
				Display.getDefault().asyncExec(new Runnable() {
					@Override
					public void run() {
						CustomMessageDialog.openError(Display.getDefault().getActiveShell(), "Error",
								"Server not reachable");
					}
				});
			}
			LOGGER.error("Exeception while getting user user applications based on user! " + e);
		}
		return userUserApps;
	}

	/**
	 * Method for Load user to pro app from service.
	 *
	 * @param projectApplicationsNode
	 *            {@link IAdminTreeChild}
	 * @return the list {@link List<IAdminTreeChild>}
	 */
	public List<IAdminTreeChild> loadUserProjectAAProjectAppFromService(final IAdminTreeChild projectApplicationsNode) {
		String relationType = null;
		List<IAdminTreeChild> userProjectApps = new ArrayList<IAdminTreeChild>();
		try {
			String userProjectRelId = null;
			String aaProjectRelId = null;
			if (projectApplicationsNode instanceof UserProjectAAProjectApplications
					|| projectApplicationsNode instanceof ProjectUserAAProjectApplications) {
				aaProjectRelId = ((RelationObj) projectApplicationsNode.getParent()).getRelId();
				userProjectRelId = ((RelationObj) projectApplicationsNode.getParent().getParent().getParent())
						.getRelId();
			} else if (projectApplicationsNode instanceof UserProjectAAProjectAppAllowed
					|| projectApplicationsNode instanceof ProjectUserAAProjectAppAllowed) {
				aaProjectRelId = ((RelationObj) projectApplicationsNode.getParent().getParent()).getRelId();
				userProjectRelId = ((RelationObj) projectApplicationsNode.getParent().getParent().getParent()
						.getParent()).getRelId();
				relationType = UserRelationType.ALLOWED.name();
			} else if (projectApplicationsNode instanceof UserProjectAAProjectAppForbidden
					|| projectApplicationsNode instanceof ProjectUserAAProjectAppForbidden) {
				aaProjectRelId = ((RelationObj) projectApplicationsNode.getParent().getParent()).getRelId();
				userProjectRelId = ((RelationObj) projectApplicationsNode.getParent().getParent().getParent()
						.getParent()).getRelId();
				relationType = UserRelationType.FORBIDDEN.name();
			}

			UserProjectAppRelController userProjectAppRelController = new UserProjectAppRelController();
			List<UserProjAppRelTbl> userProjectAppRelResponse = null;
			if (relationType != null && relationType.length() > 0) {
				userProjectAppRelResponse = userProjectAppRelController
						.getUserProjAppRelsByUserProjRelIdAAProjRelIdRelType(userProjectRelId, aaProjectRelId,
								relationType);
			} else {
				userProjectAppRelResponse = userProjectAppRelController
						.getUserProjAppRelsByUserProjRelIdAAProjRelId(userProjectRelId, aaProjectRelId);
			}
			if (userProjectAppRelResponse != null) {
				for (UserProjAppRelTbl userProjAppRelTbl : userProjectAppRelResponse) {
					ProjectApplicationsTbl projectApplicationsTbl = userProjAppRelTbl.getProjectApplicationId();
					ProjectApplication projectApplication = getUpdatedProjectApplication(projectApplicationsTbl);
					if (projectApplication != null) {
						String userProjectAppRelId = userProjAppRelTbl.getUserProjAppRelId();
						RelationObj userProjectAppRelObj = projectApplication.getAdapter(
								projectApplicationsNode.getClass(), projectApplicationsNode, userProjectAppRelId,
								projectApplication.isActive());

						boolean active = userProjectAppRelObj.isActive();
						if (factoryInstance.isHideInActiveItems()) {
							if (active) {
								userProjectApps.add(userProjectAppRelObj);
							}
						} else {
							userProjectApps.add(userProjectAppRelObj);
						}
					}
				}
			}
		} catch (Exception e) {
			if (e instanceof ResourceAccessException) {
				Display.getDefault().asyncExec(new Runnable() {
					@Override
					public void run() {
						CustomMessageDialog.openError(Display.getDefault().getActiveShell(), "Error",
								"Server not reachable");
					}
				});
			}
			LOGGER.error("Exeception while getting project application based on User Project Relation! " + e);
		}
		return userProjectApps;
	}

	/**
	 * Method for Load user start apps from service.
	 *
	 * @param startApplicationsNode
	 *            {@link IAdminTreeChild}
	 * @return the list {@link List<IAdminTreeChild>}
	 */
	public List<IAdminTreeChild> loadUserStartAppsFromService(IAdminTreeChild startApplicationsNode) {

		List<IAdminTreeChild> userStartApps = new ArrayList<IAdminTreeChild>();
		try {
			IAdminTreeChild parent = startApplicationsNode.getParent();

			if (parent != null && parent instanceof User) {
				String userId = ((User) parent).getUserId();

				UserStartAppRelController userStartAppRelController = new UserStartAppRelController();
				List<UserStartAppRelTbl> UserStartAppRelTblList = userStartAppRelController.getUserStartAppRelByUserId(userId);
				if (UserStartAppRelTblList != null) {
					for (UserStartAppRelTbl userStartAppRelTbl : UserStartAppRelTblList) {
						StartApplicationsTbl startApplicationsTbl = userStartAppRelTbl.getStartApplicationId();
						StartApplication startApplication = getUpdatedStartApplication(startApplicationsTbl);

						if (startApplication != null) {
							String relationId = userStartAppRelTbl.getUserStartAppRelId();
							boolean relationStatus = com.magna.xmbackend.vo.enums.Status.ACTIVE.name()
									.equals(userStartAppRelTbl.getStatus());
							RelationObj relObj = startApplication.getAdapter(startApplicationsNode.getClass(),
									startApplicationsNode, relationId, relationStatus);

							boolean active = relObj.isActive();
							if (factoryInstance.isHideInActiveItems()) {
								if (active) {
									userStartApps.add(relObj);
								}
							} else {
								userStartApps.add(relObj);
							}
						}
					}
				}
			}
		} catch (Exception e) {
			if (e instanceof ResourceAccessException) {
				Display.getDefault().asyncExec(new Runnable() {
					@Override
					public void run() {
						CustomMessageDialog.openError(Display.getDefault().getActiveShell(), "Error",
								"Server not reachable");
					}
				});
			}
			LOGGER.error("Exeception while getting start application based on user Relation! " + e);
		}
		return userStartApps;
	}

	/**
	 * Method for Load directory users from service.
	 *
	 * @param directoryUsersNode
	 *            {@link IAdminTreeChild}
	 * @return the list {@link List<IAdminTreeChild>}
	 */
	public List<IAdminTreeChild> loadDirectoryUsersFromService(IAdminTreeChild directoryUsersNode) {
		List<IAdminTreeChild> directoryUsers = new ArrayList<IAdminTreeChild>();
		try {
			String directoryId = null;
			if (directoryUsersNode instanceof DirectoryUsers) {
				directoryId = ((Directory) directoryUsersNode.getParent()).getDirectoryId();
				if (directoryId != null) {
					DirectoryRelController directoryRelController = new DirectoryRelController();
					DirectoryRelResponseWrapper directoryRelResponseWrapper = directoryRelController
							.getDirectoryRelByDirIdAndObjType(directoryId, DirectoryObjectType.USER.name());

					List<DirectoryRelResponse> directoryRelResponses = directoryRelResponseWrapper
							.getDirectoryRelResponses();
					for (DirectoryRelResponse directoryRelResponse : directoryRelResponses) {
						UsersTbl usersTbl = directoryRelResponse.getUsersTbl();
						User user = getUpdatedUser(usersTbl);
						if (user != null) {
							String relationId = directoryRelResponse.getDirectoryRelId();
							RelationObj relationObj = user.getAdapter(directoryUsersNode.getClass(), directoryUsersNode,
									relationId, true);
							directoryUsers.add(relationObj);
							/*boolean active = relationObj.isActive();
							if (factoryInstance.isHideInActiveItems()) {
								if (active) {
									directoryUsers.add(relationObj);
								}
							} else {
								directoryUsers.add(relationObj);
							}*/
						}
					}
				}
			}
		} catch (Exception e) {
			if (e instanceof ResourceAccessException) {
				Display.getDefault().asyncExec(new Runnable() {
					@Override
					public void run() {
						CustomMessageDialog.openError(Display.getDefault().getActiveShell(), "Error",
								"Server not reachable");
					}
				});
			}
			LOGGER.error("Exeception while getting the directory users! " + e);
		}
		return directoryUsers;
	}

	/**
	 * Method for Load directory projects from service.
	 *
	 * @param directoryProjectsNode
	 *            {@link IAdminTreeChild}
	 * @return the list {@link List<IAdminTreeChild>}
	 */
	public List<IAdminTreeChild> loadDirectoryProjectsFromService(IAdminTreeChild directoryProjectsNode) {

		List<IAdminTreeChild> directoryProjects = new ArrayList<IAdminTreeChild>();
		try {
			String directoryId = null;
			if (directoryProjectsNode instanceof DirectoryProjects) {
				directoryId = ((Directory) directoryProjectsNode.getParent()).getDirectoryId();
				if (directoryId != null) {
					DirectoryRelController directoryRelController = new DirectoryRelController();
					DirectoryRelResponseWrapper directoryRelResponseWrapper = directoryRelController
							.getDirectoryRelByDirIdAndObjType(directoryId, DirectoryObjectType.PROJECT.name());

					List<DirectoryRelResponse> directoryRelResponses = directoryRelResponseWrapper
							.getDirectoryRelResponses();
					for (DirectoryRelResponse directoryRelResponse : directoryRelResponses) {
						ProjectsTbl projectsTbl = directoryRelResponse.getProjectsTbl();
						Project project = getUpdatedProject(projectsTbl);

						if (project != null) {
							String relationId = directoryRelResponse.getDirectoryRelId();
							RelationObj relationObj = project.getAdapter(directoryProjectsNode.getClass(),
									directoryProjectsNode, relationId, true);
							directoryProjects.add(relationObj);
							/*boolean active = relationObj.isActive();
							if (factoryInstance.isHideInActiveItems()) {
								if (active) {
									directoryProjects.add(relationObj);
								}
							} else {
								directoryProjects.add(relationObj);
							}*/
						}
					}
				}
			}
		} catch (Exception e) {
			if (e instanceof ResourceAccessException) {
				Display.getDefault().asyncExec(new Runnable() {
					@Override
					public void run() {
						CustomMessageDialog.openError(Display.getDefault().getActiveShell(), "Error",
								"Server not reachable");
					}
				});
			}
			LOGGER.error("Exeception while getting the directory projects! " + e);
		}
		return directoryProjects;
	}

	/**
	 * Method for Load directory applications from service.
	 *
	 * @param directoryApplicationsNode
	 *            {@link IAdminTreeChild}
	 * @return the list {@link List<IAdminTreeChild>}
	 */
	public List<IAdminTreeChild> loadDirectoryApplicationsFromService(IAdminTreeChild directoryApplicationsNode) {
		List<IAdminTreeChild> directoryApplications = new ArrayList<IAdminTreeChild>();
		if (directoryApplicationsNode instanceof DirectoryApplications) {
			IAdminTreeChild directoryUserApplications = ((DirectoryApplications) directoryApplicationsNode)
					.getDirectoryApplicationsChildrenChildren().get(DirectoryUserApplications.class.getName());
			directoryApplications.addAll(loadDirectoryUserApplicationsFromService(directoryUserApplications));

			IAdminTreeChild directoryProjectApplications = ((DirectoryApplications) directoryApplicationsNode)
					.getDirectoryApplicationsChildrenChildren().get(DirectoryProjectApplications.class.getName());
			directoryApplications.addAll(loadDirectoryProjectApplicationsFromService(directoryProjectApplications));
		}
		return directoryApplications;
	}

	/**
	 * Method for Load directory user applications from service.
	 *
	 * @param directoryUserApplicationsNode
	 *            {@link IAdminTreeChild}
	 * @return the list {@link List<IAdminTreeChild>}
	 */
	public List<IAdminTreeChild> loadDirectoryUserApplicationsFromService(
			IAdminTreeChild directoryUserApplicationsNode) {

		List<IAdminTreeChild> directoryUserApps = new ArrayList<IAdminTreeChild>();
		try {
			String directoryId = null;
			if (directoryUserApplicationsNode instanceof DirectoryUserApplications) {
				directoryId = ((Directory) directoryUserApplicationsNode.getParent().getParent()).getDirectoryId();
				if (directoryId != null) {
					DirectoryRelController directoryRelController = new DirectoryRelController();
					DirectoryRelResponseWrapper directoryRelResponseWrapper = directoryRelController
							.getDirectoryRelByDirIdAndObjType(directoryId, DirectoryObjectType.USERAPPLICATION.name());

					List<DirectoryRelResponse> directoryRelResponses = directoryRelResponseWrapper
							.getDirectoryRelResponses();
					for (DirectoryRelResponse directoryRelResponse : directoryRelResponses) {
						UserApplicationsTbl userApplicationsTbl = directoryRelResponse.getUserApplicationsTbl();
						if (userApplicationsTbl != null) {
							UserApplication userApplication = getUpdatedUserApplication(userApplicationsTbl);

							if (userApplication != null) {
								String relationId = directoryRelResponse.getDirectoryRelId();
								RelationObj relationObj = userApplication.getAdapter(
										directoryUserApplicationsNode.getClass(), directoryUserApplicationsNode,
										relationId, true);
								directoryUserApps.add(relationObj);
								/*boolean active = relationObj.isActive();
								if (factoryInstance.isHideInActiveItems()) {
									if (active) {
										directoryUserApps.add(relationObj);
									}
								} else {
									directoryUserApps.add(relationObj);
								}*/
							}
						}
					}
				}
			}
		} catch (Exception e) {
			/*if (e instanceof ResourceAccessException) {
				Display.getDefault().asyncExec(new Runnable() {
					@Override
					public void run() {
						CustomMessageDialog.openError(Display.getDefault().getActiveShell(), "Error",
								"Server not reachable");
					}
				});
			}*/
			LOGGER.error("Exeception while getting the directory user applications! " + e);
		}
		return directoryUserApps;
	}

	/**
	 * Method for Load directory project applications from service.
	 *
	 * @param directoryProjectApplicationsNode
	 *            {@link IAdminTreeChild}
	 * @return the list {@link List<IAdminTreeChild>}
	 */
	public List<IAdminTreeChild> loadDirectoryProjectApplicationsFromService(
			IAdminTreeChild directoryProjectApplicationsNode) {

		List<IAdminTreeChild> directoryProjectApps = new ArrayList<IAdminTreeChild>();
		try {
			String directoryId = null;
			if (directoryProjectApplicationsNode instanceof DirectoryProjectApplications) {
				directoryId = ((Directory) directoryProjectApplicationsNode.getParent().getParent()).getDirectoryId();
				if (directoryId != null) {
					DirectoryRelController directoryRelController = new DirectoryRelController();
					DirectoryRelResponseWrapper directoryRelResponseWrapper = directoryRelController
							.getDirectoryRelByDirIdAndObjType(directoryId, DirectoryObjectType.PROJECTAPPLICATION.name());
					List<DirectoryRelResponse> directoryRelResponses = directoryRelResponseWrapper.getDirectoryRelResponses();
					for (DirectoryRelResponse directoryRelResponse : directoryRelResponses) {
						ProjectApplicationsTbl projectApplicationsTbl = directoryRelResponse
								.getProjectApplicationsTbl();
						ProjectApplication projectApplication = getUpdatedProjectApplication(projectApplicationsTbl);

						if (projectApplication != null) {
							String relationId = directoryRelResponse.getDirectoryRelId();
							RelationObj relationObj = projectApplication.getAdapter(
									directoryProjectApplicationsNode.getClass(), directoryProjectApplicationsNode,
									relationId, true);
							directoryProjectApps.add(relationObj);
							/*boolean active = relationObj.isActive();
							if (factoryInstance.isHideInActiveItems()) {
								if (active) {
									directoryProjectApps.add(relationObj);
								}
							} else {
								directoryProjectApps.add(relationObj);
							}*/
						}
					}
				}
			}

		} catch (Exception e) {
			/*if (e instanceof ResourceAccessException) {
				Display.getDefault().asyncExec(new Runnable() {
					@Override
					public void run() {
						CustomMessageDialog.openError(Display.getDefault().getActiveShell(), "Error",
								"Server not reachable");
					}
				});
			}*/
			LOGGER.error("Exeception while getting the directory project applications! " + e);
		}
		return directoryProjectApps;
	}

	/**
	 * Gets the updated admin area.
	 *
	 * @param adminAreaTbl
	 *            {@link AdminAreasTbl}
	 * @return the updated admin area
	 */
	private AdministrationArea getUpdatedAdminArea(final AdminAreasTbl adminAreaTbl) {
		final File iconFolder = XMSystemUtil.getXMSystemServerIconsFolder();
		AdministrationArea adminArea = null;
		try {
			AdministrationAreas administrationAreas = factoryInstance.getAdministrationAreas();
			Map<String, IAdminTreeChild> adminAreasChildren = administrationAreas.getAdminstrationAreasChildren();
			final String adminAreaId = adminAreaTbl.getAdminAreaId();
			final String adminAreaName = adminAreaTbl.getName();
			final boolean isActive = com.magna.xmbackend.vo.enums.Status.ACTIVE.name().equals(adminAreaTbl.getStatus()) ? true : false;
			final String contact = adminAreaTbl.getHotlineContactNumber();
			final String email = adminAreaTbl.getHotlineContactEmail();
			Long singletonAppTimeout = adminAreaTbl.getSingletonAppTimeout();
			IconsTbl iconTbl = adminAreaTbl.getIconId();
			Icon icon = new Icon(iconTbl.getIconId(), iconTbl.getIconName(),
					iconFolder + File.separator + iconTbl.getIconName(), iconTbl.getIconType());

			Map<LANG_ENUM, String> descriptionMap = new HashMap<>();
			Map<LANG_ENUM, String> remarksMap = new HashMap<>();
			Map<LANG_ENUM, String> translationIdMap = new HashMap<>();

			Collection<AdminAreaTranslationTbl> adminAreaTranslationTblList = adminAreaTbl
					.getAdminAreaTranslationTblCollection();
			for (AdminAreaTranslationTbl adminAreaTranslationTbl : adminAreaTranslationTblList) {
				LANG_ENUM langEnum = LANG_ENUM.getLangEnum(adminAreaTranslationTbl.getLanguageCode().getLanguageCode());
				String translationId = adminAreaTranslationTbl.getAdminAreaTranslationId();
				translationIdMap.put(langEnum, translationId);
				descriptionMap.put(langEnum, adminAreaTranslationTbl.getDescription());
				remarksMap.put(langEnum, adminAreaTranslationTbl.getRemarks());
			}

			if (!adminAreasChildren.containsKey(adminAreaId)) {
				adminArea = new AdministrationArea(adminAreaId, adminAreaName, isActive, icon, contact, email,
						singletonAppTimeout, CommonConstants.OPERATIONMODE.VIEW);
				adminArea.setTranslationIdMap(translationIdMap);

				if (factoryInstance.isHideInActiveItems()) {
					if (isActive) {
						administrationAreas.add(adminAreaId, adminArea);
					}
				} else {
					administrationAreas.add(adminAreaId, adminArea);
				}
			} else {
				IAdminTreeChild iAdminTreeChild = adminAreasChildren.get(adminAreaId);
				if (iAdminTreeChild instanceof AdministrationArea) {
					adminArea = (AdministrationArea) iAdminTreeChild;
					adminArea.setName(adminAreaName);
					adminArea.setActive(isActive);
					adminArea.setDescriptionMap(descriptionMap);
					adminArea.setHotlineContactNumber(contact == null ? CommonConstants.EMPTY_STR : contact);
					adminArea.setHotlineContactEmail(email == null ? CommonConstants.EMPTY_STR : email);
					adminArea.setSingletonAppTimeout(singletonAppTimeout);
					adminArea.setRemarksMap(remarksMap);
					adminArea.setIcon(icon);
				}
			}
		} catch (Exception e) {
			LOGGER.error("Exeception while updating Admin Area! " + e);
		}
		return adminArea;
	}

	/**
	 * Gets the updated project.
	 *
	 * @param projectsTbl
	 *            {@link ProjectsTbl}
	 * @return the updated project
	 */
	private Project getUpdatedProject(final ProjectsTbl projectsTbl) {
		final File iconFolder = XMSystemUtil.getXMSystemServerIconsFolder();
		Project project = null;
		try {
			final Projects projects = factoryInstance.getProjects();
			final Map<String, IAdminTreeChild> projectsChildren = projects.getProjectsChildren();

			final String id = projectsTbl.getProjectId();
			final String name = projectsTbl.getName();
			final boolean isActive = com.magna.xmbackend.vo.enums.Status.ACTIVE.name().equals(projectsTbl.getStatus()) ? true : false;

			IconsTbl iconTbl = projectsTbl.getIconId();
			Icon icon = new Icon(iconTbl.getIconId(), iconTbl.getIconName(),
					iconFolder + File.separator + iconTbl.getIconName(), iconTbl.getIconType());

			Map<LANG_ENUM, String> descriptionMap = new HashMap<>();
			Map<LANG_ENUM, String> remarksMap = new HashMap<>();
			Map<LANG_ENUM, String> translationMap = new HashMap<>();

			Collection<ProjectTranslationTbl> projectTranslationTblList = projectsTbl
					.getProjectTranslationTblCollection();
			for (ProjectTranslationTbl projectTranslationTbl : projectTranslationTblList) {
				LANG_ENUM langEnum = LANG_ENUM.getLangEnum(projectTranslationTbl.getLanguageCode().getLanguageCode());
				String translationId = projectTranslationTbl.getProjectTranslationId();
				translationMap.put(langEnum, translationId);
				descriptionMap.put(langEnum, projectTranslationTbl.getDescription());
				remarksMap.put(langEnum, projectTranslationTbl.getRemarks());
			}

			if (!projectsChildren.containsKey(id)) {
				project = new Project(id, name, isActive, descriptionMap, remarksMap, icon, CommonConstants.OPERATIONMODE.VIEW);
				project.setTranslationIdMap(translationMap);

				if (factoryInstance.isHideInActiveItems()) {
					if (isActive) {
						projects.add(id, project);
					}
				} else {
					projects.add(id, project);
				}
			} else {
				IAdminTreeChild iAdminTreeChild = projectsChildren.get(id);
				if (iAdminTreeChild instanceof Project) {
					project = (Project) iAdminTreeChild;
					project.setName(name);
					project.setActive(isActive);
					project.setDescriptionMap(descriptionMap);
					project.setRemarksMap(remarksMap);
					project.setProjectIcon(icon);
				}
			}
		} catch (Exception e) {
			LOGGER.error("Exeception while updating project! " + e);
		}
		return project;
	}

	/**
	 * Gets the updated user.
	 *
	 * @param usersTbl
	 *            {@link UsersTbl}
	 * @return the updated user
	 */
	private User getUpdatedUser(final UsersTbl usersTbl) {
		final File iconFolder = XMSystemUtil.getXMSystemServerIconsFolder();
		User user = null;
		try {
			Map<String, IAdminTreeChild> usersAlphabetChildren = new HashMap<>();
			Users users = factoryInstance.getUsers();
			Map<String, IAdminTreeChild> usersChildren = users.getUsersChildren();
			Set<String> keySet = usersChildren.keySet();
			for (String key : keySet) {
				usersAlphabetChildren.putAll(((UsersNameAlphabet) usersChildren.get(key)).getUsersChildren());
			}

			String userId = usersTbl.getUserId();
			String userName = usersTbl.getUsername();
			String userFullName = usersTbl.getFullName();
			String userManager = usersTbl.getManager();
			String userEmailId = usersTbl.getEmailId();
			String userTelephone = usersTbl.getTelephoneNumber();
			String userDept = usersTbl.getDepartment();
			boolean isActive = com.magna.xmbackend.vo.enums.Status.ACTIVE.name().equals(usersTbl.getStatus()) ? true
					: false;

			IconsTbl iconTbl = usersTbl.getIconId();
			Icon icon = new Icon(iconTbl.getIconId(), iconTbl.getIconName(),
					iconFolder + File.separator + iconTbl.getIconName(), iconTbl.getIconType());

			Map<LANG_ENUM, String> descriptionMap = new HashMap<>();
			Map<LANG_ENUM, String> remarksMap = new HashMap<>();
			Map<LANG_ENUM, String> translationIdMap = new HashMap<>();

			Collection<UserTranslationTbl> userTranslationTblList = usersTbl.getUserTranslationTblCollection();
			for (UserTranslationTbl userTranslationTbl : userTranslationTblList) {
				LANG_ENUM langEnum = LANG_ENUM.getLangEnum(userTranslationTbl.getLanguageCode().getLanguageCode());
				String translationId = userTranslationTbl.getUserTranslationId();
				translationIdMap.put(langEnum, translationId);
				descriptionMap.put(langEnum, userTranslationTbl.getDescription());
				remarksMap.put(langEnum, userTranslationTbl.getRemarks());
			}

			if (!usersAlphabetChildren.containsKey(userId)) {
				user = new User(userId, userName, userFullName, userManager, isActive, userEmailId, userTelephone, userDept,
						descriptionMap, remarksMap, icon, CommonConstants.OPERATIONMODE.VIEW);

				user.setTranslationIdMap(translationIdMap);

				if (factoryInstance.isHideInActiveItems()) {
					if (isActive) {
						UsersNameAlphabet usersNameAlphabet = (UsersNameAlphabet) usersChildren.get(userName.charAt(0));
						usersNameAlphabet.add(userId, user);
					}
				} else {
					UsersNameAlphabet usersNameAlphabet = (UsersNameAlphabet) usersChildren.get(userName.charAt(0));
					usersNameAlphabet.add(userId, user);
				}
			} else {
				IAdminTreeChild iAdminTreeChild = usersAlphabetChildren.get(userId);
				if (iAdminTreeChild instanceof User) {
					user = (User) iAdminTreeChild;
					user.setName(userName);
					user.setActive(isActive);
					user.setEmail(userEmailId == null ? CommonConstants.EMPTY_STR : userEmailId);
					user.setTelePhoneNum(userTelephone == null ? CommonConstants.EMPTY_STR : userTelephone);
					user.setDepartment(userDept == null ? CommonConstants.EMPTY_STR : userDept);
					user.setDescriptionMap(descriptionMap);
					user.setRemarksMap(remarksMap);
					user.setIcon(icon);
				}
			}
		} catch (Exception e) {
			LOGGER.error("Exeception while updating User! " + e);
		}
		return user;
	}

	/**
	 * Gets the updated user application.
	 *
	 * @param userApplicationsTbl
	 *            {@link UserApplicationsTbl}
	 * @return the updated user application
	 */
	private UserApplication getUpdatedUserApplication(final UserApplicationsTbl userApplicationsTbl) {
		final File iconFolder = XMSystemUtil.getXMSystemServerIconsFolder();
		UserApplication userApplication = null;
		try {
			final UserApplications userApplications = factoryInstance.getUserApplications();
			final Map<String, IAdminTreeChild> userApplicationsChildren = userApplications.getUserApplications();
			final String userAppId = userApplicationsTbl.getUserApplicationId();
			final String name = userApplicationsTbl.getName();
			final String description = userApplicationsTbl.getDescription();
			boolean isActive = com.magna.xmbackend.vo.enums.Status.ACTIVE.name().equals(userApplicationsTbl.getStatus()) ? true : false;
			String position = userApplicationsTbl.getPosition();
			boolean isParent = Boolean.valueOf(userApplicationsTbl.getIsParent());
			boolean isSingleton = Boolean.parseBoolean(userApplicationsTbl.getIsSingleton());
			BaseApplicationsTbl baseApplicationsTbl = userApplicationsTbl.getBaseApplicationId();

			String baseApplicationId = baseApplicationsTbl == null ? null : baseApplicationsTbl.getBaseApplicationId();
			IconsTbl iconTbl = userApplicationsTbl.getIconId();
			Icon icon = new Icon(iconTbl.getIconId(), iconTbl.getIconName(),
					iconFolder + File.separator + iconTbl.getIconName(), iconTbl.getIconType());

			Map<LANG_ENUM, String> nameMap = new HashMap<>();
			Map<LANG_ENUM, String> descriptionMap = new HashMap<>();
			Map<LANG_ENUM, String> remarksMap = new HashMap<>();
			Map<LANG_ENUM, String> translationMap = new HashMap<>();

			Collection<UserAppTranslationTbl> userAppTranslationTblList = userApplicationsTbl
					.getUserAppTranslationTblCollection();
			for (UserAppTranslationTbl userAppTranslationTbl : userAppTranslationTblList) {
				LANG_ENUM langEnum = LANG_ENUM.getLangEnum(userAppTranslationTbl.getLanguageCode().getLanguageCode());
				String translationId = userAppTranslationTbl.getUserAppTranslationId();
				translationMap.put(langEnum, translationId);
				nameMap.put(langEnum, userAppTranslationTbl.getName());
				descriptionMap.put(langEnum, userAppTranslationTbl.getDescription());
				remarksMap.put(langEnum, userAppTranslationTbl.getRemarks());
			}

			if (!userApplicationsChildren.containsKey(userAppId)) {
				userApplication = new UserApplication(userAppId, name, description, nameMap, isActive, descriptionMap, remarksMap, icon,
						isParent, isSingleton, position, baseApplicationId, CommonConstants.OPERATIONMODE.VIEW);
				userApplication.setTranslationIdMap(translationMap);

				if (factoryInstance.isHideInActiveItems()) {
					if (isActive) {
						userApplications.add(userAppId, userApplication);
					}
				} else {
					userApplications.add(userAppId, userApplication);
				}
				// userApplications.add(userAppId, userApplication);
			} else {
				IAdminTreeChild iAdminTreeChild = userApplicationsChildren.get(userAppId);
				if (iAdminTreeChild instanceof UserApplication) {
					userApplication = (UserApplication) iAdminTreeChild;
					userApplication.setName(name);
					userApplication.setDescription(description);
					userApplication.setNameMap(nameMap);
					userApplication.setActive(isActive);
					userApplication.setDescriptionMap(descriptionMap);
					userApplication.setRemarksMap(remarksMap);
					userApplication.setIcon(icon);
					userApplication.setParent(isParent);
					userApplication.setSingleton(isSingleton);
					userApplication.setPosition(position);
					userApplication.setBaseApplicationId(baseApplicationId);
				}
			}
		} catch (Exception e) {
			LOGGER.error("Exeception while updating User Application! " + e);
		}
		return userApplication;
	}

	/**
	 * Gets the updated project application.
	 *
	 * @param projectApplicationsTbl
	 *            {@link ProjectApplicationsTbl}
	 * @return the updated project application
	 */
	private ProjectApplication getUpdatedProjectApplication(final ProjectApplicationsTbl projectApplicationsTbl) {
		final File iconFolder = XMSystemUtil.getXMSystemServerIconsFolder();
		ProjectApplication projectApplication = null;
		try {
			final ProjectApplications projectApplications = factoryInstance.getProjectApplications();
			final Map<String, IAdminTreeChild> projectApplicationsChildren = projectApplications.getProjectApplications();
			final String projectApplicationId = projectApplicationsTbl.getProjectApplicationId();
			final String name = projectApplicationsTbl.getName();
			final String description = projectApplicationsTbl.getDescription();
			boolean isActive = com.magna.xmbackend.vo.enums.Status.ACTIVE.name().equals(projectApplicationsTbl.getStatus()) ? true : false;
			IconsTbl iconTbl = projectApplicationsTbl.getIconId();
			Icon icon = new Icon(iconTbl.getIconId(), iconTbl.getIconName(),
					iconFolder + File.separator + iconTbl.getIconName(), iconTbl.getIconType());
			boolean isParent = Boolean.parseBoolean(projectApplicationsTbl.getIsParent());
			String position = projectApplicationsTbl.getPosition();
			boolean isSingleton = Boolean.parseBoolean(projectApplicationsTbl.getIsSingleton());
			BaseApplicationsTbl baseApplicationsTbl = projectApplicationsTbl.getBaseApplicationId();
			String baseApplicationId = baseApplicationsTbl == null ? null : baseApplicationsTbl.getBaseApplicationId();

			Map<LANG_ENUM, String> nameMap = new HashMap<>();
			Map<LANG_ENUM, String> descriptionMap = new HashMap<>();
			Map<LANG_ENUM, String> remarksMap = new HashMap<>();
			Map<LANG_ENUM, String> translationMap = new HashMap<>();

			Collection<ProjectAppTranslationTbl> projectAppTranslationTblList = projectApplicationsTbl
					.getProjectAppTranslationTblCollection();
			for (ProjectAppTranslationTbl projectAppTranslationTbl : projectAppTranslationTblList) {
				LANG_ENUM langEnum = LANG_ENUM
						.getLangEnum(projectAppTranslationTbl.getLanguageCode().getLanguageCode());
				String translationId = projectAppTranslationTbl.getProjectAppTranslationId();
				translationMap.put(langEnum, translationId);
				nameMap.put(langEnum, projectAppTranslationTbl.getName());
				descriptionMap.put(langEnum, projectAppTranslationTbl.getDescription());
				remarksMap.put(langEnum, projectAppTranslationTbl.getRemarks());
			}

			if (!projectApplicationsChildren.containsKey(projectApplicationId)) {
				projectApplication = new ProjectApplication(projectApplicationId, name, description, nameMap, isActive, descriptionMap,
						remarksMap, icon, isParent, isSingleton, position, baseApplicationId,
						CommonConstants.OPERATIONMODE.VIEW);
				projectApplication.setTranslationIdMap(translationMap);

				if (factoryInstance.isHideInActiveItems()) {
					if (isActive) {
						projectApplications.add(projectApplicationId, projectApplication);
					}
				} else {
					projectApplications.add(projectApplicationId, projectApplication);
				}
			} else {
				IAdminTreeChild iAdminTreeChild = projectApplicationsChildren.get(projectApplicationId);
				if (iAdminTreeChild instanceof ProjectApplication) {
					projectApplication = (ProjectApplication) iAdminTreeChild;
					projectApplication.setName(name);
					projectApplication.setDescription(description);
					projectApplication.setNameMap(nameMap);
					projectApplication.setActive(isActive);
					projectApplication.setDescriptionMap(descriptionMap);
					projectApplication.setRemarksMap(remarksMap);
					projectApplication.setIcon(icon);
					projectApplication.setParent(isParent);
					projectApplication.setSingleton(isSingleton);
					projectApplication.setPosition(position);
					projectApplication.setBaseApplicationId(baseApplicationId);
				}
			}
		} catch (Exception e) {
			LOGGER.error("Exeception while updating project application! " + e);
		}
		return projectApplication;
	}

	/**
	 * Gets the updated start application.
	 *
	 * @param startApplicationsTbl
	 *            {@link StartApplicationsTbl}
	 * @return the updated start application
	 */
	private StartApplication getUpdatedStartApplication(final StartApplicationsTbl startApplicationsTbl) {
		final File iconFolder = XMSystemUtil.getXMSystemServerIconsFolder();
		StartApplication startApplication = null;
		try {
			final StartApplications startApplications = factoryInstance.getStartApplications();
			final Map<String, IAdminTreeChild> startAppChildren = startApplications.getStartApplications();
			final String startAppId = startApplicationsTbl.getStartApplicationId();
			final String name = startApplicationsTbl.getName();
			boolean isActive = com.magna.xmbackend.vo.enums.Status.ACTIVE.name()
					.equals(startApplicationsTbl.getStatus()) ? true : false;

			IconsTbl iconTbl = startApplicationsTbl.getIconId();
			Icon icon = new Icon(iconTbl.getIconId(), iconTbl.getIconName(),
					iconFolder + File.separator + iconTbl.getIconName(), iconTbl.getIconType());

			BaseApplicationsTbl baseApplicationsTbl = startApplicationsTbl.getBaseApplicationId();
			String baseApplicationId = baseApplicationsTbl == null ? null : baseApplicationsTbl.getBaseApplicationId();

			Map<LANG_ENUM, String> descriptionMap = new HashMap<>();
			Map<LANG_ENUM, String> remarksMap = new HashMap<>();
			Map<LANG_ENUM, String> translationMap = new HashMap<>();

			Collection<StartAppTranslationTbl> starttAppTranslationTblList = startApplicationsTbl
					.getStartAppTranslationTblCollection();
			for (StartAppTranslationTbl startAppTranslationTbl : starttAppTranslationTblList) {
				LANG_ENUM langEnum = LANG_ENUM.getLangEnum(startAppTranslationTbl.getLanguageCode().getLanguageCode());
				String translationId = startAppTranslationTbl.getStartAppTranslationId();
				translationMap.put(langEnum, translationId);
				descriptionMap.put(langEnum, startAppTranslationTbl.getDescription());
				remarksMap.put(langEnum, startAppTranslationTbl.getRemarks());
			}

			if (!startAppChildren.containsKey(startAppId)) {
				startApplication = new StartApplication(startAppId, name, isActive, descriptionMap, remarksMap, icon,
						CommonConstants.OPERATIONMODE.VIEW, baseApplicationId);
				startApplication.setTranslationIdMap(translationMap);

				if (factoryInstance.isHideInActiveItems()) {
					if (isActive) {
						startApplications.add(startAppId, startApplication);
					}
				} else {
					startApplications.add(startAppId, startApplication);
				}
				// startApplications.add(startAppId, startApplication);
			} else {
				IAdminTreeChild iAdminTreeChild = startAppChildren.get(startAppId);
				if (iAdminTreeChild instanceof StartApplication) {
					startApplication = (StartApplication) iAdminTreeChild;
					startApplication.setName(name);
					startApplication.setActive(isActive);
					startApplication.setDescriptionMap(descriptionMap);
					startApplication.setRemarksMap(remarksMap);
					startApplication.setIcon(icon);
				}
			}
		} catch (Exception e) {
			LOGGER.error("Exeception while updating Start Application! " + e);
		}
		return startApplication;
	}

	/**
	 * Method for Load role children from service.
	 *
	 * @param role
	 *            {@link Role}
	 */
	public void loadRoleChildrenFromService(Role role) {
		RoleScopeObjects scopeObjectParent = (RoleScopeObjects) role.getRoleChildren()
				.get(RoleScopeObjects.class.getName());
		loadRoleAdminAreasFromService(role, scopeObjectParent);
	}

	/**
	 * Method for Load role admin areas from service.
	 *
	 * @param role
	 *            {@link Role}
	 * @param roleScopeObjects
	 *            {@link RoleScopeObjects}
	 */
	public void loadRoleAdminAreasFromService(Role role, RoleScopeObjects roleScopeObjects) {
		try {
			if (!CommonConstants.SuperAdminRole.NAME.equals(role.getRoleName())) {
				roleScopeObjects.getRoleScopeObjectsChildren().clear();
				final RoleAdminAreaRelController roleAdminAreaRelController = new RoleAdminAreaRelController();
				Iterable<RoleAdminAreaRelTbl> roleAdminAreaRelTbls;
				if ((roleAdminAreaRelTbls = roleAdminAreaRelController
						.getAllRoleAARelByRoleId(role.getRoleId())) != null) {
					for (RoleAdminAreaRelTbl roleAdminAreaRelTbl : roleAdminAreaRelTbls) {
						AdminAreasTbl adminArea = roleAdminAreaRelTbl.getAdminAreaId();
						AdministrationArea adminAreaModel = (AdministrationArea) AdminTreeFactory.getInstance()
								.getAdministrationAreas().getAdminstrationAreasChildren()
								.get(adminArea.getAdminAreaId());
						String relId = roleAdminAreaRelTbl.getRoleAdminAreaRelId();
						if (relId != null) {
							if (adminAreaModel.getAdministrationAreaId().equals(adminArea.getAdminAreaId())) {
								RelationObj relObj = (RelationObj) adminAreaModel.getAdapter(RoleScopeObjectChild.class,
										roleScopeObjects, relId, true);

								((RoleScopeObjectChild) relObj.getContainerObj()).addFixedChildren(relObj);

								boolean active = relObj.isActive();
								if (factoryInstance.isHideInActiveItems()) {
									if (active) {
										roleScopeObjects.add(relId, relObj);
									}
								} else {
									roleScopeObjects.add(relId, relObj);
								}

								// roleScopeObjects.add(relId, relObj);
							}
						}
					}
				}
			}
		} catch (Exception e) {
			/*if (e instanceof ResourceAccessException) {
				Display.getDefault().asyncExec(new Runnable() {
					@Override
					public void run() {
						CustomMessageDialog.openError(Display.getDefault().getActiveShell(), "Error",
								"Server not reachable");
					}
				});
			}*/
			LOGGER.error("Exeception while getting role admin areas objects! " + e);
		}
	}

	/**
	 * Method for Load role users from service.
	 *
	 * @param iAdminTreeChild
	 *            {@link IAdminTreeChild}
	 * @return the list {@link List<IAdminTreeChild>}
	 */
	public List<IAdminTreeChild> loadRoleUsersFromService(IAdminTreeChild iAdminTreeChild) {
		List<IAdminTreeChild> roleUsers = new ArrayList<IAdminTreeChild>();
		try {
			String roleId = null;
			if (iAdminTreeChild instanceof RoleUsers) {
				roleId = ((Role) iAdminTreeChild.getParent()).getRoleId();
			}
			if (!XMSystemUtil.isEmpty(roleId)) {
				final RoleUserRelController roleUserRelController = new RoleUserRelController();
				final List<RoleUserRelTbl> roleUserRelResponse = roleUserRelController.getRoleUserRelByRoleId(roleId);
				for (RoleUserRelTbl roleUserRelTbl : roleUserRelResponse) {
					UsersTbl usersTbl = roleUserRelTbl.getUserId();
					User user = getUpdatedUser(usersTbl);
					if (user != null) {
						String roleUserRelId = roleUserRelTbl.getRoleUserRelId();

						RelationObj relObj = user.getAdapter(RoleUsers.class, iAdminTreeChild, roleUserRelId, true);

						boolean active = relObj.isActive();
						if (factoryInstance.isHideInActiveItems()) {
							if (active) {
								roleUsers.add(relObj);
							}
						} else {
							roleUsers.add(relObj);
						}
						// roleUsers.add(relObj);
					}
				}
			}
		} catch (Exception e) {
			if (e instanceof ResourceAccessException) {
				Display.getDefault().asyncExec(new Runnable() {
					@Override
					public void run() {
						CustomMessageDialog.openError(Display.getDefault().getActiveShell(), "Error",
								"Server not reachable");
					}
				});
			}
			LOGGER.error("Exeception while getting Role User Relations based on RoleId ! " + e);
		}
		return roleUsers;
	}

	/**
	 * Method for Load role scope object users from service.
	 *
	 * @param iAdminTreeChild
	 *            {@link IAdminTreeChild}
	 * @return the list {@link List<IAdminTreeChild>}
	 */
	public List<IAdminTreeChild> loadRoleScopeObjectUsersFromService(IAdminTreeChild iAdminTreeChild) {
		List<IAdminTreeChild> roleUsers = new ArrayList<IAdminTreeChild>();
		try {
			String roleId = null;
			String roleAdminAreaRelId = null;
			if (iAdminTreeChild instanceof RoleScopeObjectUsers) {
				roleAdminAreaRelId = ((RelationObj) iAdminTreeChild.getParent()).getRelId();
				roleId = ((Role) iAdminTreeChild.getParent().getParent().getParent()).getRoleId();
			}
			if (!XMSystemUtil.isEmpty(roleAdminAreaRelId) && !XMSystemUtil.isEmpty(roleId)) {
				final RoleUserRelController roleUserRelController = new RoleUserRelController();
				final List<RoleUserRelTbl> RoleUserRelTbls = roleUserRelController
						.getRoleUserRelByRoleIdAndRoleAARelId(roleId, roleAdminAreaRelId);

				for (RoleUserRelTbl roleUserRelTbl : RoleUserRelTbls) {
					UsersTbl usersTbl = roleUserRelTbl.getUserId();
					User user = getUpdatedUser(usersTbl);
					if (user != null) {
						String roleUserRelId = roleUserRelTbl.getRoleUserRelId();

						RelationObj relObj = user.getAdapter(RoleScopeObjectUsers.class, iAdminTreeChild, roleUserRelId,
								true);
						boolean active = relObj.isActive();
						if (factoryInstance.isHideInActiveItems()) {
							if (active) {
								roleUsers.add(relObj);
							}
						} else {
							roleUsers.add(relObj);
						}

						// roleUsers.add(relObj);
					}
				}
			}
		} catch (Exception e) {
			if (e instanceof ResourceAccessException) {
				Display.getDefault().asyncExec(new Runnable() {
					@Override
					public void run() {
						CustomMessageDialog.openError(Display.getDefault().getActiveShell(), "Error",
								"Server not reachable");
					}
				});
			}
			LOGGER.error("Exeception while getting Role User Relations based on RoleId and Role AA Rel ID ! " + e);
		}
		return roleUsers;
	}

	/**
	 * Load admin menu users from services.
	 *
	 * @param adminMenuUsersNode
	 *            the admin menu users node
	 * @return the list
	 */
	public List<IAdminTreeChild> loadAdminMenuUsersFromServices(IAdminTreeChild adminMenuUsersNode) {
		List<IAdminTreeChild> adminMenuUsers = new ArrayList<IAdminTreeChild>();
		try {
			if (adminMenuUsersNode instanceof AdminUsers) {
				AdminMenuConfigRelController adminMenuConfigRelController = new AdminMenuConfigRelController();
				AdminMenuConfigCustomeResponseWrapper adminMenuConfigCustomeResponseWrapper = adminMenuConfigRelController.getAdminMenuRelByObjType(AdminMenuConfig.USER);
				if (adminMenuConfigCustomeResponseWrapper != null) {
					final List<AdminMenuConfigCustomeResponse> adminMenuConfigCustomeResponses = adminMenuConfigCustomeResponseWrapper
							.getAdminMenuConfigCustomeResponses();
					for (final AdminMenuConfigCustomeResponse adminMenuConfigCustomeResponse : adminMenuConfigCustomeResponses) {
						final String relationId = adminMenuConfigCustomeResponse.getAdminMenuConfigId();
						final UsersTbl usersTbl = adminMenuConfigCustomeResponse.getUsersTbl();
						User user = getUpdatedUser(usersTbl);
						if (user != null) {
							RelationObj relationObj = user.getAdapter(adminMenuUsersNode.getClass(), adminMenuUsersNode,
									relationId, user.isActive());
							boolean active = relationObj.isActive();
							if (factoryInstance.isHideInActiveItems()) {
								if (active) {
									adminMenuUsers.add(relationObj);
								}
							} else {
								adminMenuUsers.add(relationObj);
							}
						}
					}
				}
			}

		} catch (Exception e) {
			if (e instanceof ResourceAccessException) {
				Display.getDefault().asyncExec(new Runnable() {
					@Override
					public void run() {
						CustomMessageDialog.openError(Display.getDefault().getActiveShell(), "Error",
								"Server not reachable");
					}
				});
			}
			LOGGER.error("Exeception while getting the AdminMenu users! " + e);
		}
		return adminMenuUsers;
	}

	/**
	 * Load admin menu user app from services.
	 *
	 * @param adminMenuUserAppNode
	 *            the admin menu user app node
	 * @return the list
	 */
	public List<IAdminTreeChild> loadAdminMenuUserAppFromServices(IAdminTreeChild adminMenuUserAppNode) {
		List<IAdminTreeChild> adminMenuUserApp = new ArrayList<IAdminTreeChild>();
		try {
			if (adminMenuUserAppNode instanceof AdminUserApps) {
				AdminMenuConfigRelController adminMenuConfigRelController = new AdminMenuConfigRelController();
				AdminMenuConfigCustomeResponseWrapper adminMenuConfigCustomeResponseWrapper = adminMenuConfigRelController
						.getAdminMenuRelByObjType(AdminMenuConfig.USERAPPLICATION);
				if (adminMenuConfigCustomeResponseWrapper != null) {
					final List<AdminMenuConfigCustomeResponse> adminMenuConfigCustomeResponses = adminMenuConfigCustomeResponseWrapper
							.getAdminMenuConfigCustomeResponses();
					for (final AdminMenuConfigCustomeResponse adminMenuConfigCustomeResponse : adminMenuConfigCustomeResponses) {
						final String relationId = adminMenuConfigCustomeResponse.getAdminMenuConfigId();
						final UserApplicationsTbl userApplicationsTbl = adminMenuConfigCustomeResponse.getUserApplicationsTbl();
						UserApplication userApplication = getUpdatedUserApplication(userApplicationsTbl);
						if (userApplication != null) {
							RelationObj relationObj = userApplication.getAdapter(adminMenuUserAppNode.getClass(),
									adminMenuUserAppNode, relationId, userApplication.isActive());
							boolean active = relationObj.isActive();

							if (factoryInstance.isHideInActiveItems()) {
								if (active) {
									adminMenuUserApp.add(relationObj);
								}
							} else {
								adminMenuUserApp.add(relationObj);
							}
						}
					}
				}
			}
		} catch (Exception e) {
			if (e instanceof ResourceAccessException) {
				Display.getDefault().asyncExec(new Runnable() {
					@Override
					public void run() {
						CustomMessageDialog.openError(Display.getDefault().getActiveShell(), "Error",
								"Server not reachable");
					}
				});
			}
			LOGGER.error("Exeception while getting the AdminMenu userApps! " + e);
		}
		return adminMenuUserApp;
	}

	/**
	 * Load admin menu project app from services.
	 *
	 * @param adminMenuProjectAppNode
	 *            the admin menu project app node
	 * @return the list
	 */
	public List<IAdminTreeChild> loadAdminMenuProjectAppFromServices(IAdminTreeChild adminMenuProjectAppNode) {
		List<IAdminTreeChild> adminMenuProjectApp = new ArrayList<IAdminTreeChild>();
		try {
			if (adminMenuProjectAppNode instanceof AdminProjectApps) {
				final AdminMenuConfigRelController adminMenuConfigRelController = new AdminMenuConfigRelController();
				final AdminMenuConfigCustomeResponseWrapper adminMenuConfigCustomeResponseWrapper = adminMenuConfigRelController
						.getAdminMenuRelByObjType(AdminMenuConfig.PROJECTAPPLICATION);
				if (adminMenuConfigCustomeResponseWrapper != null) {
					final List<AdminMenuConfigCustomeResponse> adminMenuConfigCustomeResponses = adminMenuConfigCustomeResponseWrapper.getAdminMenuConfigCustomeResponses();
					for (final AdminMenuConfigCustomeResponse adminMenuConfigCustomeResponse : adminMenuConfigCustomeResponses) {
						
						ProjectApplicationsTbl projectApplicationsTbl = adminMenuConfigCustomeResponse.getProjectApplicationsTbl();
						ProjectApplication projectApplication = getUpdatedProjectApplication(projectApplicationsTbl);
						String relationId = adminMenuConfigCustomeResponse.getAdminMenuConfigId();
						if (projectApplication != null) {
							RelationObj relationObj = projectApplication.getAdapter(adminMenuProjectAppNode.getClass(),
									adminMenuProjectAppNode, relationId, projectApplication.isActive());
							boolean active = relationObj.isActive();
							if (factoryInstance.isHideInActiveItems()) {
								if (active) {
									adminMenuProjectApp.add(relationObj);
								}
							} else {
								adminMenuProjectApp.add(relationObj);
							}
						}
					}
				}
			}
		} catch (Exception e) {
			if (e instanceof ResourceAccessException) {
				Display.getDefault().asyncExec(new Runnable() {
					@Override
					public void run() {
						CustomMessageDialog.openError(Display.getDefault().getActiveShell(), "Error",
								"Server not reachable");
					}
				});
			}
			LOGGER.error("Exeception while getting the AdminMenu projectApp! " + e);
		}
		return adminMenuProjectApp;
	}

	/**
	 * Load user group users from service.
	 *
	 * @param userGroupUsersNode
	 *            the user group users node
	 * @return the list
	 */
	public List<IAdminTreeChild> loadUserGroupUsersFromService(IAdminTreeChild userGroupUsersNode) {

		List<IAdminTreeChild> userGroupUsers = new ArrayList<IAdminTreeChild>();
		try {
			String groupId = null;
			if (userGroupUsersNode instanceof UserGroupUsers) {
				groupId = ((UserGroupModel) userGroupUsersNode.getParent()).getGroupId();
				if (groupId != null) {
					GroupRelController groupRelController = new GroupRelController();
					GroupRelBatchResponse groupRelBatchResponse = groupRelController.getGroupRelByGroupId(groupId);

					List<GroupRelResponse> groupRelResponses = groupRelBatchResponse.getGroupRelResponses();

					for (GroupRelResponse groupRelResponse : groupRelResponses) {
						UsersTbl usersTbl = groupRelResponse.getUsersTbl();
						User user = getUpdatedUser(usersTbl);
						if (user != null) {
							String relationId = groupRelResponse.getGroupRefId();
							RelationObj relationObj = user.getAdapter(userGroupUsersNode.getClass(), userGroupUsersNode,
									relationId, true);
							((UserGroupUsers) userGroupUsersNode).getUserGroupUsersChildren().put(relationId,
									relationObj);
							userGroupUsers.add(relationObj);
						}
					}
				}
			}

		} catch (Exception e) {
			if (e instanceof ResourceAccessException) {
				Display.getDefault().asyncExec(new Runnable() {
					@Override
					public void run() {
						CustomMessageDialog.openError(Display.getDefault().getActiveShell(), "Error",
								"Server not reachable");
					}
				});
			}
			LOGGER.error("Exeception while getting the UserGroup users! " + e);
		}
		return userGroupUsers;
	}

	/**
	 * Load project group projects from service.
	 *
	 * @param projectGroupProjectsNode
	 *            the project group projects node
	 * @return the list
	 */
	public List<IAdminTreeChild> loadProjectGroupProjectsFromService(IAdminTreeChild projectGroupProjectsNode) {

		List<IAdminTreeChild> projectGroupProjects = new ArrayList<IAdminTreeChild>();
		try {
			String groupId = null;
			if (projectGroupProjectsNode instanceof ProjectGroupProjects) {
				groupId = ((ProjectGroupModel) projectGroupProjectsNode.getParent()).getGroupId();
				if (groupId != null) {
					GroupRelController groupRelController = new GroupRelController();
					GroupRelBatchResponse groupRelBatchResponse = groupRelController.getGroupRelByGroupId(groupId);

					List<GroupRelResponse> groupRelResponses = groupRelBatchResponse.getGroupRelResponses();

					for (GroupRelResponse groupRelResponse : groupRelResponses) {
						ProjectsTbl projectsTbl = groupRelResponse.getProjectsTbl();
						Project project = getUpdatedProject(projectsTbl);
						if (project != null) {
							String relationId = groupRelResponse.getGroupRefId();
							RelationObj relationObj = project.getAdapter(projectGroupProjectsNode.getClass(),
									projectGroupProjectsNode, relationId, true);
							((ProjectGroupProjects) projectGroupProjectsNode).getProjectGroupChildren().put(relationId,
									relationObj);
							projectGroupProjects.add(relationObj);
							/*boolean active = relationObj.isActive();
							if (factoryInstance.isHideInActiveItems()) {
								if (active) {
									projectGroupProjects.add(relationObj);
								}
							} else {
								projectGroupProjects.add(relationObj);
							}*/
						}
					}
				}
			}

		} catch (Exception e) {
			if (e instanceof ResourceAccessException) {
				Display.getDefault().asyncExec(new Runnable() {
					@Override
					public void run() {
						CustomMessageDialog.openError(Display.getDefault().getActiveShell(), "Error",
								"Server not reachable");
					}
				});
			}
			LOGGER.error("Exeception while getting the ProjectGroup Projects! " + e);
		}
		return projectGroupProjects;
	}

	/**
	 * Load user app group user apps from service.
	 *
	 * @param userAppGroupUserAppsNode
	 *            the user app group user apps node
	 * @return the list
	 */
	public List<IAdminTreeChild> loadUserAppGroupUserAppsFromService(IAdminTreeChild userAppGroupUserAppsNode) {

		List<IAdminTreeChild> userAppGroupUserApps = new ArrayList<IAdminTreeChild>();
		try {
			String groupId = null;
			if (userAppGroupUserAppsNode instanceof UserAppGroupUserApps) {
				groupId = ((UserApplicationGroup) userAppGroupUserAppsNode.getParent()).getGroupId();
				if (groupId != null) {
					GroupRelController groupRelController = new GroupRelController();
					GroupRelBatchResponse groupRelBatchResponse = groupRelController.getGroupRelByGroupId(groupId);

					List<GroupRelResponse> groupRelResponses = groupRelBatchResponse.getGroupRelResponses();

					for (GroupRelResponse groupRelResponse : groupRelResponses) {
						UserApplicationsTbl userApplicationsTbl = groupRelResponse.getUserApplicationsTbl();
						UserApplication userApp = getUpdatedUserApplication(userApplicationsTbl);
						if (userApp != null) {
							String relationId = groupRelResponse.getGroupRefId();
							RelationObj relationObj = userApp.getAdapter(userAppGroupUserAppsNode.getClass(),
									userAppGroupUserAppsNode, relationId, true);
							((UserAppGroupUserApps) userAppGroupUserAppsNode).getUserAppGroupChildren().put(relationId,
									relationObj);
							userAppGroupUserApps.add(relationObj);
							/*boolean active = relationObj.isActive();
							if (factoryInstance.isHideInActiveItems()) {
								if (active) {
									userAppGroupUserApps.add(relationObj);
								}
							} else {
								userAppGroupUserApps.add(relationObj);
							}*/
						}
					}
				}
			}

		} catch (Exception e) {
			if (e instanceof ResourceAccessException) {
				Display.getDefault().asyncExec(new Runnable() {
					@Override
					public void run() {
						CustomMessageDialog.openError(Display.getDefault().getActiveShell(), "Error",
								"Server not reachable");
					}
				});
			}
			LOGGER.error("Exeception while getting the UserAppGroup UserApps! " + e);
		}
		return userAppGroupUserApps;
	}

	/**
	 * Load project app group project apps from service.
	 *
	 * @param projectAppGroupProjectAppsNode
	 *            the project app group project apps node
	 * @return the list
	 */
	public List<IAdminTreeChild> loadProjectAppGroupProjectAppsFromService(
			IAdminTreeChild projectAppGroupProjectAppsNode) {

		List<IAdminTreeChild> projectAppGroupProjectApps = new ArrayList<IAdminTreeChild>();
		try {
			String groupId = null;
			if (projectAppGroupProjectAppsNode instanceof ProjectAppGroupProjectApps) {
				groupId = ((ProjectApplicationGroup) projectAppGroupProjectAppsNode.getParent()).getGroupId();
				if (groupId != null) {
					GroupRelController groupRelController = new GroupRelController();
					GroupRelBatchResponse groupRelBatchResponse = groupRelController.getGroupRelByGroupId(groupId);

					List<GroupRelResponse> groupRelResponses = groupRelBatchResponse.getGroupRelResponses();

					for (GroupRelResponse groupRelResponse : groupRelResponses) {
						ProjectApplicationsTbl projectAppsTbl = groupRelResponse.getProjectApplicationsTbl();
						ProjectApplication projectApp = getUpdatedProjectApplication(projectAppsTbl);
						if (projectApp != null) {
							String relationId = groupRelResponse.getGroupRefId();
							RelationObj relationObj = projectApp.getAdapter(projectAppGroupProjectAppsNode.getClass(),
									projectAppGroupProjectAppsNode, relationId, true);
							((ProjectAppGroupProjectApps) projectAppGroupProjectAppsNode).getProjectAppGroupChildren()
									.put(relationId, relationObj);
							projectAppGroupProjectApps.add(relationObj);
							/*boolean active = relationObj.isActive();
							if (factoryInstance.isHideInActiveItems()) {
								if (active) {
									projectAppGroupProjectApps.add(relationObj);
								}
							} else {
								projectAppGroupProjectApps.add(relationObj);
							}*/
						}
					}
				}
			}

		} catch (Exception e) {
			if (e instanceof ResourceAccessException) {
				Display.getDefault().asyncExec(new Runnable() {
					@Override
					public void run() {
						CustomMessageDialog.openError(Display.getDefault().getActiveShell(), "Error",
								"Server not reachable");
					}
				});
			}
			LOGGER.error("Exeception while getting the ProjectAppGroup ProjectApps! " + e);
		}
		return projectAppGroupProjectApps;
	}

	/**
	 * Load all groups.
	 */
	public void loadAllGroups() {
		loadUserGroupsFromService();
		loadProjectGroupsFromService();
		loadUserAppGroupsFromService();
		loadProjectAppGroupsFromService();
	}
	
	/**
	 * Load project app groups from service.
	 */
	public void loadProjectAppGroupsFromService() {
		final File iconFolder = XMSystemUtil.getXMSystemServerIconsFolder();
		try {
			final List<String> removeList = new ArrayList<>();
			final List<String> dbIds = new ArrayList<>();
			final ProjectApplicationGroups projectAppGroups = factoryInstance.getProjectAppGroups();
			final Map<String, IAdminTreeChild> projectAppGroupsChildren = projectAppGroups.getProjectAppGroupsChildren();
			final GroupController groupController = new GroupController();
			final GroupResponse groupResponse = groupController.getGroupByGroupType(com.magna.xmbackend.vo.enums.Groups.PROJECTAPPLICATION.name());
			List<GroupsTbl> groupTbls = groupResponse.getGroupTbls();
			for (GroupsTbl groupTblVo : groupTbls) {
				String id = groupTblVo.getGroupId();

				IconsTbl iconTbl = groupTblVo.getIconId();
				Icon icon = new Icon(iconTbl.getIconId(), iconTbl.getIconName(),
						iconFolder + File.separator + iconTbl.getIconName(), iconTbl.getIconType());

				Map<LANG_ENUM, String> descriptionMap = new HashMap<>();
				Map<LANG_ENUM, String> remarksMap = new HashMap<>();
				Map<LANG_ENUM, String> translationMap = new HashMap<>();
				String name = new String();
				name = groupTblVo.getName();
				Collection<GroupTranslationTbl> groupTranslationTblList = groupTblVo.getGroupTranslationTblCollection();
				for (GroupTranslationTbl groupTranslationTbl : groupTranslationTblList) {
					LANG_ENUM langEnum = LANG_ENUM.getLangEnum(groupTranslationTbl.getLanguageCode().getLanguageCode());
					String translationId = groupTranslationTbl.getGroupTranslationId();
					translationMap.put(langEnum, translationId);
					descriptionMap.put(langEnum, groupTranslationTbl.getDescription());
					remarksMap.put(langEnum, groupTranslationTbl.getRemarks());
				}
				
				if (!projectAppGroupsChildren.containsKey(id)) {
					ProjectApplicationGroup projectApplicationGroup = new ProjectApplicationGroup(id, name, descriptionMap, remarksMap, icon,
							CommonConstants.OPERATIONMODE.VIEW);
					projectApplicationGroup.setTranslationIdMap(translationMap);
					projectAppGroups.add(id, projectApplicationGroup);
				} else {
					IAdminTreeChild iAdminTreeChild = projectAppGroupsChildren.get(id);
					if (iAdminTreeChild instanceof ProjectApplicationGroup) {
						ProjectApplicationGroup projectApplicationGroup = (ProjectApplicationGroup) iAdminTreeChild;
						projectApplicationGroup.setName(name);
						projectApplicationGroup.setDescriptionMap(descriptionMap);
						projectApplicationGroup.setRemarksMap(remarksMap);
						projectApplicationGroup.setIcon(icon);
					}
				}
				dbIds.add(id);
			}
			Set<String> keySet = projectAppGroupsChildren.keySet();
			for (String key : keySet) {
				if (!dbIds.contains(key)) {
					removeList.add(key);
				}
			}
			for (String id : removeList) {
				projectAppGroups.remove(id);
			}
		} catch (Exception e) {
			/*if (e instanceof ResourceAccessException) {
				Display.getDefault().asyncExec(new Runnable() {
					@Override
					public void run() {
						CustomMessageDialog.openError(Display.getDefault().getActiveShell(), "Error",
								"Server not reachable");
					}
				});
			}*/
			LOGGER.error("Exeception while getting user Group objects! " + e);
		}
	}

	/**
	 * Load user app groups from service.
	 */
	public void loadUserAppGroupsFromService() {
		final File iconFolder = XMSystemUtil.getXMSystemServerIconsFolder();
		try {
			final List<String> removeList = new ArrayList<>();
			final List<String> dbIds = new ArrayList<>();
			final UserApplicationGroups userAppGroups = factoryInstance.getUserAppGroups();
			final Map<String, IAdminTreeChild> userAppGroupsChildren = userAppGroups.getUserAppGroupsChildren();
			final GroupController groupController = new GroupController();
			GroupResponse groupResponse = groupController.getGroupByGroupType(com.magna.xmbackend.vo.enums.Groups.USERAPPLICATION.name());
			List<GroupsTbl> groupTbls = groupResponse.getGroupTbls();
			for (GroupsTbl groupTblVo : groupTbls) {
				String id = groupTblVo.getGroupId();

				IconsTbl iconTbl = groupTblVo.getIconId();
				Icon icon = new Icon(iconTbl.getIconId(), iconTbl.getIconName(),
						iconFolder + File.separator + iconTbl.getIconName(), iconTbl.getIconType());

				Map<LANG_ENUM, String> descriptionMap = new HashMap<>();
				Map<LANG_ENUM, String> remarksMap = new HashMap<>();
				Map<LANG_ENUM, String> translationMap = new HashMap<>();
				String name = new String();
				name = groupTblVo.getName();
				Collection<GroupTranslationTbl> groupTranslationTblList = groupTblVo.getGroupTranslationTblCollection();
				for (GroupTranslationTbl groupTranslationTbl : groupTranslationTblList) {
					LANG_ENUM langEnum = LANG_ENUM.getLangEnum(groupTranslationTbl.getLanguageCode().getLanguageCode());
					String translationId = groupTranslationTbl.getGroupTranslationId();
					translationMap.put(langEnum, translationId);
					descriptionMap.put(langEnum, groupTranslationTbl.getDescription());
					remarksMap.put(langEnum, groupTranslationTbl.getRemarks());
				}
				
				if (!userAppGroupsChildren.containsKey(id)) {
					UserApplicationGroup userApplicationGroup = new UserApplicationGroup(id, name, descriptionMap, remarksMap, icon,
							CommonConstants.OPERATIONMODE.VIEW);
					userApplicationGroup.setTranslationIdMap(translationMap);
					userAppGroups.add(id, userApplicationGroup);
				} else {
					IAdminTreeChild iAdminTreeChild = userAppGroupsChildren.get(id);
					if (iAdminTreeChild instanceof UserApplicationGroup) {
						UserApplicationGroup userApplicationGroup = (UserApplicationGroup) iAdminTreeChild;
						userApplicationGroup.setName(name);
						userApplicationGroup.setDescriptionMap(descriptionMap);
						userApplicationGroup.setRemarksMap(remarksMap);
						userApplicationGroup.setIcon(icon);
					}
				}
				dbIds.add(id);
			}
			Set<String> keySet = userAppGroupsChildren.keySet();
			for (String key : keySet) {
				if (!dbIds.contains(key)) {
					removeList.add(key);
				}
			}
			for (String id : removeList) {
				userAppGroups.remove(id);
			}
		} catch (Exception e) {
			/*if (e instanceof ResourceAccessException) {
				Display.getDefault().asyncExec(new Runnable() {
					@Override
					public void run() {
						CustomMessageDialog.openError(Display.getDefault().getActiveShell(), "Error",
								"Server not reachable");
					}
				});
			}*/
			LOGGER.error("Exeception while getting user Group objects! " + e);
		}
	}

	/**
	 * Load project groups from service.
	 */
	public void loadProjectGroupsFromService() {
		final File iconFolder = XMSystemUtil.getXMSystemServerIconsFolder();
		try {
			final List<String> removeList = new ArrayList<>();
			final List<String> dbIds = new ArrayList<>();
			final ProjectGroupsModel projectGroups = factoryInstance.getProjectGroups();
			final Map<String, IAdminTreeChild> projectGroupsChildren = projectGroups.getProjectGroupsChildren();
			final GroupController groupController = new GroupController();
			GroupResponse groupResponse = groupController.getGroupByGroupType(com.magna.xmbackend.vo.enums.Groups.PROJECT.name());
			List<GroupsTbl> groupTbls = groupResponse.getGroupTbls();
			for (GroupsTbl groupTblVo : groupTbls) {
				String id = groupTblVo.getGroupId();

				IconsTbl iconTbl = groupTblVo.getIconId();
				Icon icon = new Icon(iconTbl.getIconId(), iconTbl.getIconName(),
						iconFolder + File.separator + iconTbl.getIconName(), iconTbl.getIconType());

				Map<LANG_ENUM, String> descriptionMap = new HashMap<>();
				Map<LANG_ENUM, String> remarksMap = new HashMap<>();
				Map<LANG_ENUM, String> translationMap = new HashMap<>();
				String name = new String();
				name = groupTblVo.getName();
				Collection<GroupTranslationTbl> groupTranslationTblList = groupTblVo.getGroupTranslationTblCollection();
				for (GroupTranslationTbl groupTranslationTbl : groupTranslationTblList) {
					LANG_ENUM langEnum = LANG_ENUM.getLangEnum(groupTranslationTbl.getLanguageCode().getLanguageCode());
					String translationId = groupTranslationTbl.getGroupTranslationId();
					translationMap.put(langEnum, translationId);
					descriptionMap.put(langEnum, groupTranslationTbl.getDescription());
					remarksMap.put(langEnum, groupTranslationTbl.getRemarks());
				}
				
				if (!projectGroupsChildren.containsKey(id)) {
					ProjectGroupModel projectGroup = new ProjectGroupModel(id, name, descriptionMap, remarksMap, icon,
							CommonConstants.OPERATIONMODE.VIEW);
					projectGroup.setTranslationIdMap(translationMap);
					projectGroups.add(id, projectGroup);
				} else {
					IAdminTreeChild iAdminTreeChild = projectGroupsChildren.get(id);
					if (iAdminTreeChild instanceof ProjectGroupModel) {
						ProjectGroupModel projectGroup = (ProjectGroupModel) iAdminTreeChild;
						projectGroup.setName(name);
						projectGroup.setDescriptionMap(descriptionMap);
						projectGroup.setRemarksMap(remarksMap);
						projectGroup.setIcon(icon);
					}
				}
				dbIds.add(id);
			}
			Set<String> keySet = projectGroupsChildren.keySet();
			for (String key : keySet) {
				if (!dbIds.contains(key)) {
					removeList.add(key);
				}
			}
			for (String id : removeList) {
				projectGroups.remove(id);
			}
		} catch (Exception e) {
			/*if (e instanceof ResourceAccessException) {
				Display.getDefault().asyncExec(new Runnable() {
					@Override
					public void run() {
						CustomMessageDialog.openError(Display.getDefault().getActiveShell(), "Error",
								"Server not reachable");
					}
				});
			}*/
			LOGGER.error("Exeception while getting user Group objects! " + e);
		}
	}

	/**
	 * Load user groups from service.
	 */
	public void loadUserGroupsFromService() {
		final File iconFolder = XMSystemUtil.getXMSystemServerIconsFolder();
		try {
			final List<String> removeList = new ArrayList<>();
			final List<String> dbIds = new ArrayList<>();
			final UserGroupsModel usergroups = factoryInstance.getUsergroups();
			final Map<String, IAdminTreeChild> userGroupsChildren = usergroups.getUserGroupsChildren();
			final GroupController groupController = new GroupController();
			GroupResponse groupResponse = groupController.getGroupByGroupType(com.magna.xmbackend.vo.enums.Groups.USER.name());
			List<GroupsTbl> groupTbls = groupResponse.getGroupTbls();
			for (GroupsTbl groupTblVo : groupTbls) {
				String id = groupTblVo.getGroupId();

				IconsTbl iconTbl = groupTblVo.getIconId();
				Icon icon = new Icon(iconTbl.getIconId(), iconTbl.getIconName(),
						iconFolder + File.separator + iconTbl.getIconName(), iconTbl.getIconType());

				Map<LANG_ENUM, String> descriptionMap = new HashMap<>();
				Map<LANG_ENUM, String> remarksMap = new HashMap<>();
				Map<LANG_ENUM, String> translationMap = new HashMap<>();
				String name = new String();
				name = groupTblVo.getName();
				Collection<GroupTranslationTbl> groupTranslationTblList = groupTblVo.getGroupTranslationTblCollection();
				for (GroupTranslationTbl groupTranslationTbl : groupTranslationTblList) {
					LANG_ENUM langEnum = LANG_ENUM.getLangEnum(groupTranslationTbl.getLanguageCode().getLanguageCode());
					String translationId = groupTranslationTbl.getGroupTranslationId();
					translationMap.put(langEnum, translationId);
					descriptionMap.put(langEnum, groupTranslationTbl.getDescription());
					remarksMap.put(langEnum, groupTranslationTbl.getRemarks());
				}
				
				if (!userGroupsChildren.containsKey(id)) {
					UserGroupModel userGroup = new UserGroupModel(id, name, descriptionMap, remarksMap, icon,
							CommonConstants.OPERATIONMODE.VIEW);
					userGroup.setTranslationIdMap(translationMap);
					usergroups.add(id, userGroup);
				} else {
					IAdminTreeChild iAdminTreeChild = userGroupsChildren.get(id);
					if (iAdminTreeChild instanceof UserGroupModel) {
						UserGroupModel userGroupModel = (UserGroupModel) iAdminTreeChild;
						userGroupModel.setName(name);
						userGroupModel.setDescriptionMap(descriptionMap);
						userGroupModel.setRemarksMap(remarksMap);
						userGroupModel.setIcon(icon);
					}
				}
				dbIds.add(id);
			}
			Set<String> keySet = userGroupsChildren.keySet();
			for (String key : keySet) {
				if (!dbIds.contains(key)) {
					removeList.add(key);
				}
			}
			for (String id : removeList) {
				usergroups.remove(id);
			}
		} catch (Exception e) {
			/*if (e instanceof ResourceAccessException) {
				Display.getDefault().asyncExec(new Runnable() {
					@Override
					public void run() {
						CustomMessageDialog.openError(Display.getDefault().getActiveShell(), "Error",
								"Server not reachable");
					}
				});
			}*/
			LOGGER.error("Exeception while getting user Group objects! " + e);
		}
	}

	/**
	 * Load roles from service.
	 */
	public void loadRolesFromService() {
		try {
			final List<String> removeList = new ArrayList<>();
			final List<String> dbIds = new ArrayList<>();
			final Roles roles = factoryInstance.getRoles();
			final Map<String, IAdminTreeChild> rolesChildren = roles.getRolesChildren();
			RoleController roleController = new RoleController();
			List<RolesTbl> rolesTbls = roleController.getAllRoles(true);
			for (RolesTbl roleTbl : rolesTbls) {
				String roleId = roleTbl.getRoleId();
				String roleName = roleTbl.getName();
				String description = roleTbl.getDescription();
				if (!rolesChildren.containsKey(roleId)) {
					Role role = new Role(roleId, roleName, description, CommonConstants.OPERATIONMODE.VIEW);
					roles.add(roleId, role);
				} else {
					IAdminTreeChild iAdminTreeChild = rolesChildren.get(roleId);
					if (iAdminTreeChild instanceof Role) {
						final Role role = (Role) iAdminTreeChild;
						role.setRoleName(roleName);
						role.setRoleDesc(description);
					}
				}
				dbIds.add(roleId);
			}
			Set<String> keySet = rolesChildren.keySet();
			for (String key : keySet) {
				if (!dbIds.contains(key)) {
					removeList.add(key);
				}
			}
			for (String id : removeList) {
				roles.remove(id);
			}
		} catch (Exception e) {
			if (e instanceof ResourceAccessException) {
				Display.getDefault().asyncExec(new Runnable() {
					@Override
					public void run() {
						CustomMessageDialog.openError(Display.getDefault().getActiveShell(), "Error",
								"Server not reachable");
					}
				});
			}
			LOGGER.error("Exeception while getting roles ! " + e);
		}
	}
	
	/**
	 * Load live messages from service.
	 */
	public void loadLiveMessagesFromService() {
		try {
			LiveMessageController liveMessageController = new LiveMessageController();
			LiveMessageResponseWrapper liveMsgResponseWrapper = liveMessageController.getAllLiveMessage();
			List<LiveMessageResponse> liveMsgResponseList = liveMsgResponseWrapper.getLiveMessageResponseWrapper();
			Map<String, String> liveMsgConfigIdMap;
			List<String> dbIds = new ArrayList<>();
			List<String> deactiveObjs = new ArrayList<>();
			LiveMessages liveMessages = factoryInstance.getLiveMessages();
			Map<String, IAdminTreeChild> liveMsgsChildren = liveMessages.getLiveMsgsChildren();
			for (LiveMessageResponse liveMsgResponse : liveMsgResponseList) {
				Map<String, List<Map<String, String>>> liveMsgTo = new HashMap<>();
				Map<String, String> objectIdMap = new HashMap<>();
				String liveMsgId = liveMsgResponse.getLiveMessageId();
				String liveMsgName = liveMsgResponse.getLiveMessageName();
				String subject = liveMsgResponse.getSubject();
				String message = liveMsgResponse.getMessage();

				Map<LANG_ENUM, String> subjectMap = new HashMap<>();
				Map<LANG_ENUM, String> messageMap = new HashMap<>();
				Map<LANG_ENUM, String> translationMap = new HashMap<>();

				Collection<LiveMessageTranslation> liveMsgTranslations = liveMsgResponse.getLiveMessageTranslations();
				for (LiveMessageTranslation liveMsgTranslation : liveMsgTranslations) {
					LANG_ENUM langEnum = LANG_ENUM.getLangEnum(liveMsgTranslation.getLanguageCode());
					String translationId = liveMsgTranslation.getLiveMsgTransId();
					translationMap.put(langEnum, translationId);
					subjectMap.put(langEnum, liveMsgTranslation.getSubject());
					messageMap.put(langEnum, liveMsgTranslation.getMessage());
				}

				
				/* String subject = liveMsgResponse.getSubject(); String message
				  liveMsgResponse.getMessage();*/
				
				boolean ispopup = liveMsgResponse.getIspopup().equals(CommonConstants.TRUE) ? true : false;
				Date startDatetime = liveMsgResponse.getStartDatetime();
				Date endDatetime = liveMsgResponse.getEndDatetime();
				List<LiveMessageConfigResponse> liveMessageConfigResponses = liveMsgResponse
						.getLiveMessageConfigResponses();

				liveMsgConfigIdMap = new HashMap<>();
				for (LiveMessageConfigResponse liveMsgConfigResponse : liveMessageConfigResponses) {
					AdminAreasTbl adminAreasTbl = liveMsgConfigResponse.getAdminAreasTbl();
					SitesTbl sitesTbl = liveMsgConfigResponse.getSitesTbl();
					ProjectsTbl projectsTbl = liveMsgConfigResponse.getProjectsTbl();
					UsersTbl usersTbl = liveMsgConfigResponse.getUsersTbl();

					if (usersTbl != null && projectsTbl == null && adminAreasTbl == null && sitesTbl == null) {
						objectIdMap.put(usersTbl.getUsername(), usersTbl.getUserId());
						List<Map<String, String>> listU = liveMsgTo.get(LiveMsgToPattern.U.name());
						Map<String, String> map = new HashMap<>();
						if (listU == null) {
							listU = new ArrayList<>();
						}
						map.put(LiveMsgToPattern.U.name(), usersTbl.getUsername());
						listU.add(map);
						liveMsgTo.put(LiveMsgToPattern.U.name(), listU);
						liveMsgConfigIdMap.put(LiveMsgToPattern.U.name(),
								liveMsgConfigResponse.getLiveMessageConfigId());
					} else if (usersTbl == null && projectsTbl != null && adminAreasTbl == null && sitesTbl == null) {
						final String projectName = projectsTbl.getName();
						objectIdMap.put(projectName, projectsTbl.getProjectId());
						List<Map<String, String>> listP = liveMsgTo.get(LiveMsgToPattern.P.name());
						Map<String, String> map = new HashMap<>();
						if (listP == null) {
							listP = new ArrayList<>();
						}
						map.put(LiveMsgToPattern.P.name(), projectName);
						listP.add(map);
						liveMsgTo.put(LiveMsgToPattern.P.name(), listP);
					} else if (usersTbl == null && projectsTbl == null && adminAreasTbl == null && sitesTbl != null) {
						String siteName = sitesTbl.getName();
						objectIdMap.put(siteName, sitesTbl.getSiteId());
						List<Map<String, String>> listS = liveMsgTo.get(LiveMsgToPattern.S.name());
						Map<String, String> map = new HashMap<>();
						if (listS == null) {
							listS = new ArrayList<>();
						}
						map.put(LiveMsgToPattern.S.name(), siteName);
						listS.add(map);
						liveMsgTo.put(LiveMsgToPattern.S.name(), listS);
					} else if (usersTbl == null && projectsTbl == null && adminAreasTbl != null && sitesTbl != null) {
						String siteName = sitesTbl.getName();
						String adminAreaName = adminAreasTbl.getName();
						objectIdMap.put(adminAreaName, adminAreasTbl.getAdminAreaId());
						objectIdMap.put(siteName, sitesTbl.getSiteId());
						List<Map<String, String>> listSA = liveMsgTo.get(LiveMsgToPattern.S_A.name());
						Map<String, String> map = new HashMap<>();
						if (listSA == null) {
							listSA = new ArrayList<>();
						}
						map.put(LiveMsgToPattern.S.name(), siteName);
						map.put(LiveMsgToPattern.A.name(), adminAreaName);
						listSA.add(map);
						liveMsgTo.put(LiveMsgToPattern.S_A.name(), listSA);
					} else if (usersTbl == null && projectsTbl != null && adminAreasTbl != null && sitesTbl != null) {
						String siteName = sitesTbl.getName();
						String projectName = projectsTbl.getName();
						String adminAreaName = adminAreasTbl.getName();
						objectIdMap.put(projectName, projectsTbl.getProjectId());
						objectIdMap.put(siteName, sitesTbl.getSiteId());
						objectIdMap.put(adminAreaName, adminAreasTbl.getAdminAreaId());
						List<Map<String, String>> listSAP = liveMsgTo.get(LiveMsgToPattern.S_A_P.name());
						Map<String, String> map = new HashMap<>();
						if (listSAP == null) {
							listSAP = new ArrayList<>();
						}
						map.put(LiveMsgToPattern.S.name(), siteName);
						map.put(LiveMsgToPattern.A.name(), adminAreaName);
						map.put(LiveMsgToPattern.P.name(), projectName);
						listSAP.add(map);
						liveMsgTo.put(LiveMsgToPattern.S_A_P.name(), listSAP);
					}
				}
				if (!liveMsgsChildren.containsKey(liveMsgId)) {
					LiveMessage liveMessage = new LiveMessage(liveMsgId, liveMsgTo, objectIdMap, liveMsgName, message,
							subject, messageMap, subjectMap, ispopup, startDatetime, endDatetime,
							CommonConstants.OPERATIONMODE.VIEW);
					liveMessage.setTranslationIdMap(translationMap);
					liveMessages.add(liveMsgId, liveMessage);
				} else {
					IAdminTreeChild iAdminTreeChild = liveMsgsChildren.get(liveMsgId);
					if (iAdminTreeChild instanceof LiveMessage) {
						LiveMessage liveMessage = (LiveMessage) iAdminTreeChild;
						liveMessage.setLiveMsgToMap(liveMsgTo);
						liveMessage.setObjectIdMap(objectIdMap);
						liveMessage.setName(liveMsgName);
						liveMessage.setSubject(subject);
						liveMessage.setMessage(message);
						liveMessage.setMessageMap(messageMap);
						liveMessage.setSubjectMap(subjectMap);
						liveMessage.setPopup(ispopup);
						liveMessage.setStartDateTime(startDatetime);
						liveMessage.setExpiryDateTime(endDatetime);
					}
				}
				dbIds.add(liveMsgId);
			}

			Set<String> keySet = liveMsgsChildren.keySet();
			for (String key : keySet) {
				if (!dbIds.contains(key)) {
					deactiveObjs.add(key);
				}
			}
			for (String removeObj : deactiveObjs)
				liveMessages.remove(removeObj);

		} catch (Exception e) {
			if (e instanceof ResourceAccessException) {
				Display.getDefault().asyncExec(new Runnable() {
					@Override
					public void run() {
						CustomMessageDialog.openError(Display.getDefault().getActiveShell(), "Error",
								"Server not reachable");
					}
				});
			}
			LOGGER.error("Exeception while getting live message! " + e);
		}
	}
	
	/**
	 * Load all notifications.
	 */
	public void loadAllNotifications() {
		loadProjectCreateEvt();
		loadProjectDeleteEvt();
		loadNotificationTemplate();
		loadProjectDeactivateEvt();
		loadProjectActivateEvt();
		loadUserProRelAssignEvt();
	}

	/**
	 * Load project activate evt.
	 */
	private void loadProjectActivateEvt() {
		try {
			final NotificationController notificationController = new NotificationController();
			final List<NotificationConfigResponse> response = notificationController.findNotificationByEvent(NotificationEventType.PROJECT_ACTIVATE.name());
			if (response != null) {
				final List<String> dbIds = new ArrayList<>();
				final Notifications notifications = factoryInstance.getNotifications();
				for (final NotificationConfigResponse configResponse : response) {
					final String id = configResponse.getNotifConfigId();
					final String name = configResponse.getNotifConfigName();
					final boolean isActive = com.magna.xmbackend.vo.enums.Status.ACTIVE.name().equals(configResponse.getNotifConfigStatus())
							? true : false;
					Set<String> toUsers = configResponse.getNotifConfigToUsers();
					Set<String> ccUsers = configResponse.getNotifConfigCcUsers();
					NotificationTemplate notificationTemplate = new NotificationTemplate();
					notificationTemplate.setMessage(configResponse.getNotifConfigEmailTemplateMsg());
					notificationTemplate.setSubject(configResponse.getNotifConfigEmailTemplateSubject());
					notificationTemplate.setTemplateId(configResponse.getNotifConfigEmailTemplateId());
					notificationTemplate.setName(configResponse.getNotifConfigEmailTemplateName());
					if (configResponse.getNotifConfigEmailTemplateVariables() != null) {
						notificationTemplate
								.setVariables(new TreeSet<>((configResponse.getNotifConfigEmailTemplateVariables())));
					}
					final ProjectActivateEvt projectActivateEvt = notifications.getProjectActivateEvt();
					final Map<String, IAdminTreeChild> projectActivateEvtChildren = projectActivateEvt.getProjectActivateEvtChild();
					if (!projectActivateEvtChildren.containsKey(id)) {
						ProjectActivateEvtAction projectActivateEvtAction = new ProjectActivateEvtAction(id, name, isActive, CommonConstants.OPERATIONMODE.VIEW);
						projectActivateEvtAction.setTemplate(notificationTemplate);
						if (toUsers != null && !toUsers.isEmpty()) {
							projectActivateEvtAction.setToUsers(toUsers);
						}
						if (ccUsers != null && !ccUsers.isEmpty()) {
							projectActivateEvtAction.setCcUsers(ccUsers);
						}
						projectActivateEvt.add(id, projectActivateEvtAction);
					} else {
						IAdminTreeChild iAdminTreeChild = projectActivateEvtChildren.get(id);
						if (iAdminTreeChild instanceof ProjectActivateEvtAction) {
							ProjectActivateEvtAction projectActivateEvtAction = (ProjectActivateEvtAction) iAdminTreeChild;
							projectActivateEvtAction.setActive(com.magna.xmbackend.vo.enums.Status.ACTIVE.name().equals(configResponse.getNotifConfigStatus())
							? true : false);
							projectActivateEvtAction.setId(id);
							projectActivateEvtAction.setName(name);
							projectActivateEvtAction.setTemplate(notificationTemplate);
							if (toUsers != null && !toUsers.isEmpty()) {
								projectActivateEvtAction.setToUsers(toUsers);
							}
							if (ccUsers != null && !ccUsers.isEmpty()) {
								projectActivateEvtAction.setCcUsers(ccUsers);
							}
						}
					}
					dbIds.add(id);
				}
			}
		} catch (Exception e) {
			if (e instanceof ResourceAccessException) {
				Display.getDefault().asyncExec(new Runnable() {
					@Override
					public void run() {
						CustomMessageDialog.openError(Display.getDefault().getActiveShell(), "Error",
								"Server not reachable");
					}
				});
			}
			LOGGER.error("Exeception while getting project deactivate event actions! " + e);
		}
	}

	/**
	 * Load notification template.
	 */
	private void loadNotificationTemplate() {
		try{
			final NotificationController notificationController = new NotificationController();
			EmailTemplateResponse response = notificationController.findAllNotificationTemplate();
			if(response != null){
				final List<String> dbIds = new ArrayList<>();
				Iterable<EmailTemplateTbl> emailTemplateTbls = response.getEmailTemplateTbls();
				final Notifications notifications = factoryInstance.getNotifications();
				for (EmailTemplateTbl emailTemplateTbl : emailTemplateTbls) {
					String emailTemplateId = emailTemplateTbl.getEmailTemplateId();
					String name = emailTemplateTbl.getName();
					String subject = emailTemplateTbl.getSubject();
					String message = emailTemplateTbl.getMessage();
					String templateVeriables = emailTemplateTbl.getTemplateVeriables();
					TreeSet<String> variablesSet;
					if (templateVeriables != null) {
						String[] varablesArray = templateVeriables.split(",");
						variablesSet = new TreeSet<>(Arrays.asList(varablesArray));
					}else{
						variablesSet = new TreeSet<>();
					}
					NotificationTemplates notificationTemplates = notifications.getNotificationTemplates();
					Map<String, IAdminTreeChild> templatesChildren = notificationTemplates.getTemplatesChildren();
				    if(!templatesChildren.containsKey(emailTemplateId)){
				    	NotificationTemplate notificationTemplate = new NotificationTemplate(emailTemplateId, name, subject, message,variablesSet, CommonConstants.OPERATIONMODE.VIEW);
				    	notificationTemplates.add(emailTemplateId, notificationTemplate);
				    } else {
						IAdminTreeChild iAdminTreeChild = templatesChildren.get(emailTemplateId);
						if (iAdminTreeChild instanceof NotificationTemplate) {
							NotificationTemplate template = (NotificationTemplate) iAdminTreeChild;
							template.setTemplateId(emailTemplateId);
							template.setName(name);
							template.setSubject(subject);
							template.setMessage(message);
							template.setVariables(variablesSet);
						}
					}
					dbIds.add(emailTemplateId);
				}
				
			}
		}catch (Exception e) {
			if (e instanceof ResourceAccessException) {
				Display.getDefault().asyncExec(new Runnable() {
					@Override
					public void run() {
						CustomMessageDialog.openError(Display.getDefault().getActiveShell(), "Error",
								"Server not reachable");
					}
				});
			}
			LOGGER.error("Exeception while getting notification templates! " + e);
		}
	}

	public void loadProjectCreateEvt() {
		try {
			final NotificationController notificationController = new NotificationController();
			final List<NotificationConfigResponse> response = notificationController.findNotificationByEvent(NotificationEventType.PROJECT_CREATE.name());
			if (response != null) {
				final List<String> dbIds = new ArrayList<>();
				final Notifications notifications = factoryInstance.getNotifications();
				for (final NotificationConfigResponse configResponse : response) {
					final String id = configResponse.getNotifConfigId();
					final String name = configResponse.getNotifConfigName();
					final boolean isActive = com.magna.xmbackend.vo.enums.Status.ACTIVE.name().equals(configResponse.getNotifConfigStatus())
							? true : false;
					Set<String> toUsers = configResponse.getNotifConfigToUsers();
					Set<String> ccUsers = configResponse.getNotifConfigCcUsers();
					NotificationTemplate notificationTemplate = new NotificationTemplate();
					notificationTemplate.setMessage(configResponse.getNotifConfigEmailTemplateMsg());
					notificationTemplate.setSubject(configResponse.getNotifConfigEmailTemplateSubject());
					notificationTemplate.setTemplateId(configResponse.getNotifConfigEmailTemplateId());
					notificationTemplate.setName(configResponse.getNotifConfigEmailTemplateName());
					if (configResponse.getNotifConfigEmailTemplateVariables() != null) {
						notificationTemplate
								.setVariables(new TreeSet<>((configResponse.getNotifConfigEmailTemplateVariables())));
					}
					final ProjectCreateEvt projectCreateEvt = notifications.getProjectCreateEvt();
					final Map<String, IAdminTreeChild> projectCreateEvtChildren = projectCreateEvt.getProjectCreateEvtChild();
					if (!projectCreateEvtChildren.containsKey(id)) {
						ProjectCreateEvtAction projectCreateEvtAction = new ProjectCreateEvtAction(id, name, isActive, CommonConstants.OPERATIONMODE.VIEW);
						projectCreateEvtAction.setTemplate(notificationTemplate);
						if (toUsers != null && !toUsers.isEmpty()) {
							projectCreateEvtAction.setToUsers(toUsers);
						}
						if (ccUsers != null && !ccUsers.isEmpty()) {
							projectCreateEvtAction.setCcUsers(ccUsers);
						}
						projectCreateEvt.add(id, projectCreateEvtAction);
					} else {
						IAdminTreeChild iAdminTreeChild = projectCreateEvtChildren.get(id);
						if (iAdminTreeChild instanceof ProjectCreateEvtAction) {
							ProjectCreateEvtAction projectCreateEvtAction = (ProjectCreateEvtAction) iAdminTreeChild;
							projectCreateEvtAction.setActive(com.magna.xmbackend.vo.enums.Status.ACTIVE.name().equals(configResponse.getNotifConfigStatus())
							? true : false);
							projectCreateEvtAction.setId(id);
							projectCreateEvtAction.setName(name);
							projectCreateEvtAction.setTemplate(notificationTemplate);
							if (toUsers != null && !toUsers.isEmpty()) {
								projectCreateEvtAction.setToUsers(toUsers);
							}
							if (ccUsers != null && !ccUsers.isEmpty()) {
								projectCreateEvtAction.setCcUsers(ccUsers);
							}
						}
					}
					dbIds.add(id);
				}
			}
		} catch (Exception e) {
			if (e instanceof ResourceAccessException) {
				Display.getDefault().asyncExec(new Runnable() {
					@Override
					public void run() {
						CustomMessageDialog.openError(Display.getDefault().getActiveShell(), "Error",
								"Server not reachable");
					}
				});
			}
			LOGGER.error("Exeception while getting project create event actions! " + e);
		}
	}

	/**
	 * Load project delete evt.
	 */
	public void loadProjectDeleteEvt() {
		try {
			final NotificationController notificationController = new NotificationController();
			final List<NotificationConfigResponse> response = notificationController.findNotificationByEvent(NotificationEventType.PROJECT_DELETE.name());
			if (response != null) {
				final List<String> dbIds = new ArrayList<>();
				final Notifications notifications = factoryInstance.getNotifications();
				for (final NotificationConfigResponse configResponse : response) {
					final String id = configResponse.getNotifConfigId();
					final String name = configResponse.getNotifConfigName();
					final boolean isActive = com.magna.xmbackend.vo.enums.Status.ACTIVE.name().equals(configResponse.getNotifConfigStatus())
							? true : false;
					Set<String> toUsers = configResponse.getNotifConfigToUsers();
					Set<String> ccUsers = configResponse.getNotifConfigCcUsers();
					NotificationTemplate notificationTemplate = new NotificationTemplate();
					notificationTemplate.setMessage(configResponse.getNotifConfigEmailTemplateMsg());
					notificationTemplate.setSubject(configResponse.getNotifConfigEmailTemplateSubject());
					notificationTemplate.setTemplateId(configResponse.getNotifConfigEmailTemplateId());
					notificationTemplate.setName(configResponse.getNotifConfigEmailTemplateName());
					if (configResponse.getNotifConfigEmailTemplateVariables() != null) {
						notificationTemplate
								.setVariables(new TreeSet<>((configResponse.getNotifConfigEmailTemplateVariables())));
					}
					final ProjectDeleteEvt projectDeleteEvt = notifications.getProjectDeleteEvt();
					final Map<String, IAdminTreeChild> projectDeleteEvtChildren = projectDeleteEvt.getProjectDeleteEvtChild();
					if (!projectDeleteEvtChildren.containsKey(id)) {
						ProjectDeleteEvtAction projectDeleteEvtAction = new ProjectDeleteEvtAction(id, name, isActive, CommonConstants.OPERATIONMODE.VIEW);
						projectDeleteEvtAction.setTemplate(notificationTemplate);
						if (toUsers != null && !toUsers.isEmpty()) {
							projectDeleteEvtAction.setToUsers(toUsers);
						}
						if (ccUsers != null && !ccUsers.isEmpty()) {
							projectDeleteEvtAction.setCcUsers(ccUsers);
						}
						projectDeleteEvt.add(id, projectDeleteEvtAction);
					} else {
						IAdminTreeChild iAdminTreeChild = projectDeleteEvtChildren.get(id);
						if (iAdminTreeChild instanceof ProjectDeleteEvtAction) {
							ProjectDeleteEvtAction projectDeleteEvtAction = (ProjectDeleteEvtAction) iAdminTreeChild;
							projectDeleteEvtAction.setActive(com.magna.xmbackend.vo.enums.Status.ACTIVE.name().equals(configResponse.getNotifConfigStatus())
							? true : false);
							projectDeleteEvtAction.setId(id);
							projectDeleteEvtAction.setName(name);
							projectDeleteEvtAction.setTemplate(notificationTemplate);
							if (toUsers != null && !toUsers.isEmpty()) {
								projectDeleteEvtAction.setToUsers(toUsers);
							}
							if (ccUsers != null && !ccUsers.isEmpty()) {
								projectDeleteEvtAction.setCcUsers(ccUsers);
							}
						}
					}
					dbIds.add(id);
				}
			}
		} catch (Exception e) {
			if (e instanceof ResourceAccessException) {
				Display.getDefault().asyncExec(new Runnable() {
					@Override
					public void run() {
						CustomMessageDialog.openError(Display.getDefault().getActiveShell(), "Error",
								"Server not reachable");
					}
				});
			}
			LOGGER.error("Exeception while getting project delete event actions! " + e);
		}
	}
	
	/**
	 * Load project deactivate evt.
	 */
	public void loadProjectDeactivateEvt() {
		try {
			final NotificationController notificationController = new NotificationController();
			final List<NotificationConfigResponse> response = notificationController.findNotificationByEvent(NotificationEventType.PROJECT_DEACTIVATE.name());
			if (response != null) {
				final List<String> dbIds = new ArrayList<>();
				final Notifications notifications = factoryInstance.getNotifications();
				for (final NotificationConfigResponse configResponse : response) {
					final String id = configResponse.getNotifConfigId();
					final String name = configResponse.getNotifConfigName();
					final boolean isActive = com.magna.xmbackend.vo.enums.Status.ACTIVE.name().equals(configResponse.getNotifConfigStatus())
							? true : false;
					Set<String> toUsers = configResponse.getNotifConfigToUsers();
					Set<String> ccUsers = configResponse.getNotifConfigCcUsers();
					NotificationTemplate notificationTemplate = new NotificationTemplate();
					notificationTemplate.setMessage(configResponse.getNotifConfigEmailTemplateMsg());
					notificationTemplate.setSubject(configResponse.getNotifConfigEmailTemplateSubject());
					notificationTemplate.setTemplateId(configResponse.getNotifConfigEmailTemplateId());
					notificationTemplate.setName(configResponse.getNotifConfigEmailTemplateName());
					if (configResponse.getNotifConfigEmailTemplateVariables() != null) {
						notificationTemplate
								.setVariables(new TreeSet<>((configResponse.getNotifConfigEmailTemplateVariables())));
					}
					final ProjectDeactivateEvt projectDeactivateEvt = notifications.getProjectDeactivateEvt();
					final Map<String, IAdminTreeChild> projectDeactivateEvtChildren = projectDeactivateEvt.getProjectDeactivateEvtChild();
					if (!projectDeactivateEvtChildren.containsKey(id)) {
						ProjectDeactivateEvtAction projectDeactivateEvtAction = new ProjectDeactivateEvtAction(id, name, isActive, CommonConstants.OPERATIONMODE.VIEW);
						projectDeactivateEvtAction.setTemplate(notificationTemplate);
						if (toUsers != null && !toUsers.isEmpty()) {
							projectDeactivateEvtAction.setToUsers(toUsers);
						}
						if (ccUsers != null && !ccUsers.isEmpty()) {
							projectDeactivateEvtAction.setCcUsers(ccUsers);
						}
						projectDeactivateEvt.add(id, projectDeactivateEvtAction);
					} else {
						IAdminTreeChild iAdminTreeChild = projectDeactivateEvtChildren.get(id);
						if (iAdminTreeChild instanceof ProjectDeactivateEvtAction) {
							ProjectDeactivateEvtAction projectDeactivateEvtAction = (ProjectDeactivateEvtAction) iAdminTreeChild;
							projectDeactivateEvtAction.setActive(com.magna.xmbackend.vo.enums.Status.ACTIVE.name().equals(configResponse.getNotifConfigStatus())
							? true : false);
							projectDeactivateEvtAction.setId(id);
							projectDeactivateEvtAction.setName(name);
							projectDeactivateEvtAction.setTemplate(notificationTemplate);
							if (toUsers != null && !toUsers.isEmpty()) {
								projectDeactivateEvtAction.setToUsers(toUsers);
							}
							if (ccUsers != null && !ccUsers.isEmpty()) {
								projectDeactivateEvtAction.setCcUsers(ccUsers);
							}
						}
					}
					dbIds.add(id);
				}
			}
		} catch (Exception e) {
			if (e instanceof ResourceAccessException) {
				Display.getDefault().asyncExec(new Runnable() {
					@Override
					public void run() {
						CustomMessageDialog.openError(Display.getDefault().getActiveShell(), "Error",
								"Server not reachable");
					}
				});
			}
			LOGGER.error("Exeception while getting project deactivate event actions! " + e);
		}
	}
	
	/**
	 * Load user pro rel assign evt.
	 */
	public void loadUserProRelAssignEvt() {
		try {
			final NotificationController notificationController = new NotificationController();
			final List<NotificationConfigResponse> response = notificationController.findNotificationByEvent(NotificationEventType.USER_PROJECT_RELATION_ASSIGN.name());
			if (response != null) {
				final List<String> dbIds = new ArrayList<>();
				final Notifications notifications = factoryInstance.getNotifications();
				for (final NotificationConfigResponse configResponse : response) {
					final String id = configResponse.getNotifConfigId();
					final String name = configResponse.getNotifConfigName();
					final boolean isActive = com.magna.xmbackend.vo.enums.Status.ACTIVE.name().equals(configResponse.getNotifConfigStatus())
							? true : false;
					Set<String> toUsers = configResponse.getNotifConfigToUsers();
					Set<String> ccUsers = configResponse.getNotifConfigCcUsers();
					NotificationTemplate notificationTemplate = new NotificationTemplate();
					notificationTemplate.setMessage(configResponse.getNotifConfigEmailTemplateMsg());
					notificationTemplate.setSubject(configResponse.getNotifConfigEmailTemplateSubject());
					notificationTemplate.setTemplateId(configResponse.getNotifConfigEmailTemplateId());
					notificationTemplate.setName(configResponse.getNotifConfigEmailTemplateName());
					if (configResponse.getNotifConfigEmailTemplateVariables() != null) {
						notificationTemplate
								.setVariables(new TreeSet<>((configResponse.getNotifConfigEmailTemplateVariables())));
					}
					final UserProjectRelAssignEvt userProRelAssignEvt = notifications.getUserProjectRelAssignEvt();
					final Map<String, IAdminTreeChild> userProRelAssignEvtChild = userProRelAssignEvt.getUserProRelAssignEvtChild();
					if (!userProRelAssignEvtChild.containsKey(id)) {
						UserProjectRelAssignEvtAction userProjectRelAssignEvtAction = new UserProjectRelAssignEvtAction(id, name, isActive, CommonConstants.OPERATIONMODE.VIEW);
						userProjectRelAssignEvtAction.setTemplate(notificationTemplate);
						if (toUsers != null && !toUsers.isEmpty()) {
							userProjectRelAssignEvtAction.setToUsers(toUsers);
						}
						if (ccUsers != null && !ccUsers.isEmpty()) {
							userProjectRelAssignEvtAction.setCcUsers(ccUsers);
						}
						userProRelAssignEvt.add(id, userProjectRelAssignEvtAction);
					} else {
						IAdminTreeChild iAdminTreeChild = userProRelAssignEvtChild.get(id);
						if (iAdminTreeChild instanceof UserProjectRelAssignEvtAction) {
							UserProjectRelAssignEvtAction userProjectRelAssignEvtAction = (UserProjectRelAssignEvtAction) iAdminTreeChild;
							userProjectRelAssignEvtAction.setActive(com.magna.xmbackend.vo.enums.Status.ACTIVE.name().equals(configResponse.getNotifConfigStatus())
							? true : false);
							userProjectRelAssignEvtAction.setId(id);
							userProjectRelAssignEvtAction.setName(name);
							userProjectRelAssignEvtAction.setTemplate(notificationTemplate);
							if (toUsers != null && !toUsers.isEmpty()) {
								userProjectRelAssignEvtAction.setToUsers(toUsers);
							}
							if (ccUsers != null && !ccUsers.isEmpty()) {
								userProjectRelAssignEvtAction.setCcUsers(ccUsers);
							}
						}
					}
					dbIds.add(id);
				}
			}
		} catch (Exception e) {
			if (e instanceof ResourceAccessException) {
				Display.getDefault().asyncExec(new Runnable() {
					@Override
					public void run() {
						CustomMessageDialog.openError(Display.getDefault().getActiveShell(), "Error",
								"Server not reachable");
					}
				});
			}
			LOGGER.error("Exeception while getting user project relation assign event actions! " + e);
		}
	}
}