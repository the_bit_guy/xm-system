package com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.roles.objectpermissions;

import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.roles.VoPermContainer;

/**
 * The Class BasePermission.
 * 
 * @author archita.patel
 */
public class ObjectPermissions {
	
	/** Member variable 'permission name' for {@link String}. */
	private String permissionName;

	/** The create. */
	private boolean create;

	/** The change. */
	private boolean change;

	/** The delete. */
	private boolean delete;

	/** The activate. */
	private boolean activate;
	
	/** Member variable 'create permission' for {@link VoPermContainer}. */
	private VoPermContainer createPermission;
	
	/** Member variable 'change permission' for {@link VoPermContainer}. */
	private VoPermContainer changePermission;
	
	/** Member variable 'delete permission' for {@link VoPermContainer}. */
	private VoPermContainer deletePermission;
	
	/** Member variable 'activate permission' for {@link VoPermContainer}. */
	private VoPermContainer activatePermission;
	
	/**
	 * Constructor for ObjectPermissions Class.
	 * @param role 
	 *
	 * @param permissionName {@link String}
	 * @param create {@link boolean}
	 * @param change {@link boolean}
	 * @param delete {@link boolean}
	 * @param activate {@link boolean}
	 */
	public ObjectPermissions(String permissionName, boolean create, boolean change, boolean delete, boolean activate) {
		super();
		this.permissionName = permissionName;
		this.create = create;
		this.change = change;
		this.delete = delete;
		this.activate = activate;
	}

	/**
	 * Gets the permission name.
	 *
	 * @return the permissionName
	 */
	public String getPermissionName() {
		return permissionName;
	}

	/**
	 * Sets the permission name.
	 *
	 * @param permissionName the permissionName to set
	 */
	public void setPermissionName(String permissionName) {
		this.permissionName = permissionName;
	}


	/**
	 * Checks if is create.
	 *
	 * @return true, if is create
	 */
	public boolean isCreate() {
		return create;
	}

	
	/**
	 * Checks if is delete.
	 *
	 * @return true, if is delete
	 */
	public boolean isDelete() {
		return delete;
	}

	/**
	 * Sets the create.
	 *
	 * @param create
	 *            the new create
	 */
	public void setCreate(boolean create) {
		this.create = create;
	}

	
	/**
	 * Sets the delete.
	 *
	 * @param delete
	 *            the new delete
	 */
	public void setDelete(boolean delete) {
		this.delete = delete;
	}


	/**
	 * Checks if is change.
	 *
	 * @return true, if is change
	 */
	public boolean isChange() {
		return change;
	}


	/**
	 * Sets the change.
	 *
	 * @param change the new change
	 */
	public void setChange(boolean change) {
		this.change = change;
	}


	/**
	 * Checks if is activate.
	 *
	 * @return true, if is activate
	 */
	public boolean isActivate() {
		return activate;
	}


	/**
	 * Sets the activate.
	 *
	 * @param activate the new activate
	 */
	public void setActivate(boolean activate) {
		this.activate = activate;
	}

	/**
	 * Gets the creates the permission.
	 *
	 * @return the createPermission
	 */
	public VoPermContainer getCreatePermission() {
		return createPermission;
	}

	/**
	 * Sets the creates the permission.
	 *
	 * @param createPermission the createPermission to set
	 */
	public void setCreatePermission(final VoPermContainer createPermission) {
		this.createPermission = createPermission;
	}

	/**
	 * Gets the change permission.
	 *
	 * @return the changePermission
	 */
	public VoPermContainer getChangePermission() {
		return changePermission;
	}

	/**
	 * Sets the change permission.
	 *
	 * @param changePermission the changePermission to set
	 */
	public void setChangePermission(final VoPermContainer changePermission) {
		this.changePermission = changePermission;
	}

	/**
	 * Gets the delete permission.
	 *
	 * @return the deletePermission
	 */
	public VoPermContainer getDeletePermission() {
		return deletePermission;
	}

	/**
	 * Sets the delete permission.
	 *
	 * @param deletePermission the deletePermission to set
	 */
	public void setDeletePermission(final VoPermContainer deletePermission) {
		this.deletePermission = deletePermission;
	}

	/**
	 * Gets the activate permission.
	 *
	 * @return the activatePermission
	 */
	public VoPermContainer getActivatePermission() {
		return activatePermission;
	}

	/**
	 * Sets the activate permission.
	 *
	 * @param activatePermission the activatePermission to set
	 */
	public void setActivatePermission(final VoPermContainer activatePermission) {
		this.activatePermission = activatePermission;
	}
}
