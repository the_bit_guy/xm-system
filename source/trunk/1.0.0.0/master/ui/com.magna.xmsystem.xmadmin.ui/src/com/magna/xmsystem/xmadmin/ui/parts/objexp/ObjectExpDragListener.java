package com.magna.xmsystem.xmadmin.ui.parts.objexp;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.jface.util.LocalSelectionTransfer;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.swt.dnd.DragSourceAdapter;
import org.eclipse.swt.dnd.DragSourceEvent;

// TODO: Auto-generated Javadoc
/**
 * The listener interface for receiving objectExpDrag events.
 * The class that is interested in processing a objectExpDrag
 * event implements this interface, and the object created
 * with that class is registered with a component using the
 * component's <code>addObjectExpDragListener<code> method. When
 * the objectExpDrag event occurs, that object's appropriate
 * method is invoked.
 *
 * @see ObjectExpDragEvent
 */
public class ObjectExpDragListener extends DragSourceAdapter {
	
	/** The object explorer. */
	private ObjectExplorer objectExplorer;
	
	/** The transfer. */
	private LocalSelectionTransfer transfer;


	/**
	 * Instantiates a new object exp drag listener.
	 *
	 * @param objectExplorer the object explorer
	 * @param transfer the transfer
	 */
	public ObjectExpDragListener(final ObjectExplorer objectExplorer, final LocalSelectionTransfer transfer) {
		this.objectExplorer = objectExplorer;
		this.transfer = transfer;
	}
	
	
	/* (non-Javadoc)
	 * @see org.eclipse.swt.dnd.DragSourceAdapter#dragStart(org.eclipse.swt.dnd.DragSourceEvent)
	 */
	@Override
	public void dragStart(DragSourceEvent event) {
		IStructuredSelection structuredSelection = objectExplorer.getStructuredSelection();
		Object[] array = structuredSelection.toArray();
		List<IStructuredSelection> elements = new ArrayList<>();
		for (Object object : array) {
			elements.add(new StructuredSelection(object));
		}
		if (elements != null) {
			transfer.setSelection(new StructuredSelection(elements));
		}
	}
}
