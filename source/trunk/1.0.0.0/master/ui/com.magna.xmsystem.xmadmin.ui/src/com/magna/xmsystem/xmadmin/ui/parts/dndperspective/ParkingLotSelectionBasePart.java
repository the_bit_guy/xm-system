package com.magna.xmsystem.xmadmin.ui.parts.dndperspective;

import javax.inject.Inject;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.e4.core.contexts.ContextInjectionFactory;
import org.eclipse.e4.core.contexts.IEclipseContext;
import org.eclipse.jface.util.LocalSelectionTransfer;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.ITreeViewerListener;
import org.eclipse.jface.viewers.TreeExpansionEvent;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.dnd.DND;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Tree;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.magna.xmsystem.xmadmin.ui.parts.dnd.NodeDropListener;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.AdminTreeExpansion;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.AdminTreeLabelProvider;

public abstract class ParkingLotSelectionBasePart {
	/**
	 * Logger instance
	 */
	private static final Logger LOGGER = LoggerFactory.getLogger(ParkingLotBase.class);
	
	/** The tree viewer. */
	protected TreeViewer treeViewer;
	
	/**
	 * Member variable for {@link IEclipseContext}
	 */
	@Inject
	private IEclipseContext eclipseContext;

	/** The group. */
	private Group group;
	
	/**
	 * Method to create UI
	 * 
	 * @param parent
	 *            {@link Composite}
	 */
	public void createUI(final Composite parentP) {
		try {
			//this.resourceManager = new LocalResourceManager(JFaceResources.getResources());
			FillLayout fillLayout = new FillLayout();
			fillLayout.type = SWT.VERTICAL;
			fillLayout.marginHeight = 5;
			fillLayout.marginWidth = 5;
			parentP.setLayout(fillLayout);
			buildComponents(parentP);
		} catch (Exception e) {
			LOGGER.error("Exception occured at creating UI compoents!", e); //$NON-NLS-1$
		}
	}

	private void buildComponents(final Composite parent) {
		this.group = new Group(parent, SWT.NONE);
		this.group.setLayout(new FillLayout());
		this.group.setText("Admin Sub-Tree");
		
		treeViewer = new TreeViewer(this.group);
		this.eclipseContext.set(TreeViewer.class, treeViewer);
		final AdminTreeLabelProvider adminTreeLabelProvider = ContextInjectionFactory
				.make(AdminTreeLabelProvider.class, this.eclipseContext);
		treeViewer.setContentProvider(new SubAdminTreeContentProvider());
		treeViewer.setLabelProvider(adminTreeLabelProvider);
		expandAndCollapseTree();
		
		LocalSelectionTransfer[] transferTypes = new LocalSelectionTransfer[] { LocalSelectionTransfer.getTransfer() };
		treeViewer.addDropSupport(DND.DROP_MOVE | DND.DROP_COPY, transferTypes, new NodeDropListener(treeViewer));
	}

	/**
	 * Expand and collapse tree.
	 */
	private void expandAndCollapseTree() {
		this.treeViewer.addTreeListener(new ITreeViewerListener() {
			@Override
			public void treeExpanded(TreeExpansionEvent event) {
				Object element = event.getElement();

				eclipseContext.set(Object.class, element);
				AdminTreeExpansion adminTreeExpansion = ContextInjectionFactory.make(AdminTreeExpansion.class,
						eclipseContext);
				Job job = new Job("Loading...") {

					@Override
					protected IStatus run(IProgressMonitor monitor) {
						Tree tree = treeViewer.getTree();
						monitor.beginTask("Loading Tree Nodes..", 100);
						monitor.worked(30);
						Display.getDefault().syncExec(new Runnable() {
							@Override
							public void run() {
								tree.setRedraw(false);
							}
						});
						adminTreeExpansion.loadObjects();
						monitor.worked(70);
						Display.getDefault().syncExec(new Runnable() {
							@Override
							public void run() {
								tree.setRedraw(true);
								treeViewer.refresh(element);
							}
						});
						return Status.OK_STATUS;
					}
				};
				job.setUser(true);
				job.schedule();
			}

			@Override
			public void treeCollapsed(TreeExpansionEvent arg0) {
				// TODO Auto-generated method stub
			}
		});
	}

	protected void updateView(IStructuredSelection selection) {
		Object selObj;
		if ((selObj = selection.getFirstElement()) != null) {
			this.group.setText("Admin Sub-Tree " + getName(selObj));
			treeViewer.setInput(new Object[] {selObj});
			treeViewer.refresh();
		}
	}

	private String getName(final Object selObj) {
		// TODO Auto-generated method stub
		return null;
	}
}
