
package com.magna.xmsystem.xmadmin.ui.logger.admin.handlers;

import org.eclipse.core.runtime.preferences.IEclipsePreferences;
import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.core.di.extensions.Preference;

import com.magna.xmsystem.xmadmin.util.CommonConstants;
import com.magna.xmsystem.xmadmin.util.XMAdminUtil;

/**
 * The Class AdminHistoryHandler.
 * 
 * @author shashwat.anand
 */
@SuppressWarnings("restriction")
public class AdminHistoryHandler {
	/**
	 * Execute.
	 *
	 * @param preferences
	 *            the preferences
	 */
	@Execute
	public void execute(
			@Preference(nodePath = CommonConstants.ADMIN_HISTROY_PREF) final IEclipsePreferences preferences) {
		XMAdminUtil.getInstance().openAdminHistory(preferences);
	}
}