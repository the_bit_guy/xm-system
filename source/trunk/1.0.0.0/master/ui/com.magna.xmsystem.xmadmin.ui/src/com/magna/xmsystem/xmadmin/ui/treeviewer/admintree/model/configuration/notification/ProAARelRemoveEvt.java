package com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.notification;

import com.magna.xmbackend.vo.enums.NotificationEventType;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild;

/**
 * The Class ProAARelRemoveEvt.
 * 
 * @author shashwat.anand
 */
public class ProAARelRemoveEvt extends INotificationEvent  {
	
	/**
	 * Instantiates a new pro AA rel remove evt.
	 */
	public ProAARelRemoveEvt() {
		this.eventType = NotificationEventType.PROJECT_ADMIN_AREA_RELATION_REMOVE;
	}

	@Override
	public IAdminTreeChild getParent() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setParent(IAdminTreeChild parent) {
		// TODO Auto-generated method stub
		
	}
}
