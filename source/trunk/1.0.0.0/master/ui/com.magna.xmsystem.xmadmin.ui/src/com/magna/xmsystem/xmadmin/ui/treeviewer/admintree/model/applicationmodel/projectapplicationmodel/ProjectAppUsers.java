package com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.projectapplicationmodel;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.stream.Collectors;

import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.refs.RelationObj;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.users.User;

/**
 * Class for Project app users.
 *
 * @author Chiranjeevi.Akula
 */
public class ProjectAppUsers implements IAdminTreeChild {

	/** Member variable 'parent' for {@link IAdminTreeChild}. */
	private IAdminTreeChild parent;

	/** Member variable 'project app users children' for {@link Map<String,IAdminTreeChild>}. */
	private Map<String, IAdminTreeChild> projectAppUsersChildren;

	/**
	 * Constructor for ProjectAppUsers Class.
	 *
	 * @param parent {@link IAdminTreeChild}
	 */
	public ProjectAppUsers(final IAdminTreeChild parent) {
		this.parent = parent;
		this.projectAppUsersChildren = new LinkedHashMap<>();
	}

	/**
	 * Method for Adds the.
	 *
	 * @param projectAppUserChildId {@link String}
	 * @param child {@link IAdminTreeChild}
	 * @return the i admin tree child {@link IAdminTreeChild}
	 */
	public IAdminTreeChild add(final String projectAppUserChildId, final IAdminTreeChild child) {
		child.setParent(this);
		IAdminTreeChild returnVal = this.projectAppUsersChildren.put(projectAppUserChildId, child);
		if (child instanceof RelationObj && ((RelationObj) child).getRefObject() instanceof User) {
			sort();
		}
		return returnVal;
	}

	/**
	 * Method for Removes the.
	 *
	 * @param projectAppUserChildId {@link String}
	 * @return the i admin tree child {@link IAdminTreeChild}
	 */
	public IAdminTreeChild remove(final String projectAppUserChildId) {
		return this.projectAppUsersChildren.remove(projectAppUserChildId);
	}
	
	/**
	 * Method for Removes the all.
	 */
	public void removeAll() {
		this.projectAppUsersChildren.clear();
	}
	
	/**
	 * Gets the project app users children collection.
	 *
	 * @return the project app users children collection
	 */
	public Collection<IAdminTreeChild> getProjectAppUsersChildrenCollection() {
		return this.projectAppUsersChildren.values();
	}

	/**
	 * Gets the project app users children.
	 *
	 * @return the project app users children
	 */
	public Map<String, IAdminTreeChild> getProjectAppUsersChildren() {
		return projectAppUsersChildren;
	}
	
	/* (non-Javadoc)
	 * @see com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild#getParent()
	 */
	@Override
	public IAdminTreeChild getParent() {
		return this.parent;
	}

	/* (non-Javadoc)
	 * @see com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild#setParent(com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild)
	 */
	@Override
	public void setParent(final IAdminTreeChild parent) {
		this.parent = parent;
	}
	
	/**
	 * Sort.
	 */
	public void sort() {
		final LinkedHashMap<String, IAdminTreeChild> collect = this.projectAppUsersChildren.entrySet().stream().sorted(
				(e1, e2) -> ((User) e1.getValue()).getName().compareTo(((User) e2.getValue()).getName()))
				.collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e1, LinkedHashMap::new));
		this.projectAppUsersChildren = collect;
	}

}
