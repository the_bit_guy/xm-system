package com.magna.xmsystem.xmadmin.ui.parts.icons;

import org.eclipse.jface.viewers.ITreeContentProvider;

import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.icon.Icons;

public class IconTreeContentProvider implements ITreeContentProvider {

	@Override
	public Object[] getElements(Object inputElement) {
		if (inputElement instanceof Icons) {
			return ((Icons) inputElement).getAllIcons().toArray();
		}
		return new Object[] {};
	}

	@Override
	public Object[] getChildren(Object parentElement) {
		return new Object[] {};
	}

	@Override
	public Object getParent(Object element) {
		return null;
	}

	@Override
	public boolean hasChildren(Object element) {
		return getChildren(element).length > 0;
	}

}
