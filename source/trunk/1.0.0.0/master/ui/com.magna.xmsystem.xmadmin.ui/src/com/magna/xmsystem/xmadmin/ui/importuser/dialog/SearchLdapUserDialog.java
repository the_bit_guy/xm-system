package com.magna.xmsystem.xmadmin.ui.importuser.dialog;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.layout.GridDataFactory;
import org.eclipse.jface.layout.GridLayoutFactory;
import org.eclipse.jface.viewers.CheckStateChangedEvent;
import org.eclipse.jface.viewers.CheckboxTreeViewer;
import org.eclipse.jface.viewers.ICheckStateListener;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.SashForm;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.magna.xmbackend.entities.LanguagesTbl;
import com.magna.xmbackend.entities.PropertyConfigTbl;
import com.magna.xmbackend.entities.UserTranslationTbl;
import com.magna.xmbackend.entities.UsersTbl;
import com.magna.xmbackend.exception.CannotCreateObjectException;
import com.magna.xmbackend.vo.enums.LdapAttribute;
import com.magna.xmbackend.vo.enums.LdapKey;
import com.magna.xmbackend.vo.ldap.LdapUser;
import com.magna.xmbackend.vo.ldap.LdapUsersResponse;
import com.magna.xmbackend.vo.user.UserRequest;
import com.magna.xmbackend.vo.user.UserResponse;
import com.magna.xmsystem.dependencies.customExceptions.UnauthorizedAccessException;
import com.magna.xmsystem.dependencies.utils.LANG_ENUM;
import com.magna.xmsystem.dependencies.utils.XMSystemUtil;
import com.magna.xmsystem.ui.controls.widgets.CustomMessageDialog;
import com.magna.xmsystem.xmadmin.message.Message;
import com.magna.xmsystem.xmadmin.restclient.ldap.LdapController;
import com.magna.xmsystem.xmadmin.restclient.propertyConfig.PropertyConfigController;
import com.magna.xmsystem.xmadmin.restclient.users.UserController;
import com.magna.xmsystem.xmadmin.ui.importuser.wizard.model.LdapUserProperties;
import com.magna.xmsystem.xmadmin.ui.importuser.wizard.model.LdapUserProperty;
import com.magna.xmsystem.xmadmin.ui.importuser.wizard.model.LdapUsers;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.AdminTreeviewer;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.AdminTreeFactory;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.icon.Icon;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.users.User;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.users.Users;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.users.UsersNameAlphabet;
import com.magna.xmsystem.xmadmin.util.CommonConstants;
import com.magna.xmsystem.xmadmin.util.MessageType;
import com.magna.xmsystem.xmadmin.util.XMAdminUtil;

/**
 * The Class UserImportDialog.
 * 
 * @author archita.patel
 */
public class SearchLdapUserDialog extends Dialog {

	/** The Constant LOGGER. */
	private static final Logger LOGGER = LoggerFactory.getLogger(SearchLdapUserDialog.class);
	
	/** The parent shell. */
	private Shell parentShell;

	/** The messages. */
	private Message messages;

	/** The grp user search. */
	private Group grpUserSearch;

	/** The lbl ldap url. */
	protected Label lblLdapUrl;

	/** The lbl user search. */
	protected Label lblUserName;

	/** The txt user search. */
	protected Text txtUserSearch;

	/** The search btn. */
	protected Button searchBtn;

	/** The grp search result. */
	private Group grpSearchResult;

	/** The Search result viewer. */
	private CheckboxTreeViewer ldapSearchResultViewer;

	/** The user info tree viewer. */
	private LdapUserInfoTableViewer ldapUserInfoViewer;
	
	/** The ldap users. */
	private LdapUsers ldapUsers;

	/** The import btn. */
	private Button importBtn;
	
	/**
	 * Instantiates a new user import dialog.
	 *
	 * @param parent
	 *            the parent
	 * @param messages
	 *            the messages
	 */
	public SearchLdapUserDialog(final Shell parent, final Message messages) {
		super(parent);
		this.parentShell = parent;
		this.messages = messages;
		ldapUsers=new LdapUsers();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.jface.window.Window#configureShell(org.eclipse.swt.widgets.
	 * Shell)
	 */
	@Override
	protected void configureShell(final Shell newShell) {
		try {
			super.configureShell(newShell);
			newShell.setText(messages.searchUserDialogTitle);
			newShell.setParent(this.parentShell);
			final Rectangle shellBounds = parentShell.getBounds();
			final int width = 650;
			final int height = 500;
			newShell.setSize(width, height);
			final Point pos = new Point((shellBounds.width - width) / 2, (shellBounds.height - height) / 2);
			newShell.setLocation(pos.x, pos.y);
		} catch (Exception e) {
			LOGGER.error("Execution occurred while configuring shell!", e); //$NON-NLS-1$
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.jface.dialogs.Dialog#createButtonsForButtonBar(org.eclipse.
	 * swt.widgets.Composite)
	 */
	@Override
	protected void createButtonsForButtonBar(final Composite parent) {
		super.createButtonsForButtonBar(parent);
		
		Button cancelButton = getButton(IDialogConstants.CANCEL_ID);
		cancelButton.setText(messages.cancelButtonText);
		importBtn = getButton(IDialogConstants.OK_ID);
		importBtn.setText(messages.searchUserImportBtnLbl);
		setButtonLayoutData(importBtn);
		this.getShell().setDefaultButton(this.searchBtn);
		importBtn.setEnabled(false);
	}

	
	/* (non-Javadoc)
	 * @see org.eclipse.jface.dialogs.Dialog#okPressed()
	 */
	@Override
	protected void okPressed() {
		try {
			UserController userController = new UserController();
			List<UserRequest> userRequestList = new ArrayList<UserRequest>();
			for (int i = 0; i < this.ldapSearchResultViewer.getCheckedElements().length; i++) {
				userRequestList.add(mapVOObjectWithModel((LdapUser) this.ldapSearchResultViewer.getCheckedElements()[i]));
			}
			UserResponse userResponse = null;
			userResponse = userController.saveUser(userRequestList);
			if (userResponse == null) {
				super.okPressed();
				return;
			}
			List<Map<String, String>> statusMapList = userResponse.getStatusMaps();
			if (statusMapList != null && !statusMapList.isEmpty()) {
				Display.getDefault().asyncExec(new Runnable() {
					public void run() {
						String errorMessage = "";
						for (Map<String, String> statusMap : statusMapList) {
							errorMessage += statusMap.get(XMAdminUtil.getInstance().getCurrentLocaleEnum().getLangCode());
							errorMessage += "\n";
						}
						if (errorMessage.length() > 0) {
							CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
									messages.searchUserAlradyExistDialogTitle, errorMessage);
						}
					}
				});
			}
			AdminTreeFactory instance = AdminTreeFactory.getInstance();
			Iterable<UsersTbl> usersVo = userResponse.getUserTbls();
			List<String> userNames = new ArrayList<>();
			for (UsersTbl userVo : usersVo) {
				String userId = userVo.getUserId();
				if (!XMSystemUtil.isEmpty(userId)) {
					User user = new User(userId, userVo.getUsername(), userVo.getFullName(), userVo.getManager(), true, userVo.getEmailId(),
							userVo.getTelephoneNumber(), userVo.getDepartment(),
							XMAdminUtil.getInstance().getIconByName("users.png"), CommonConstants.OPERATIONMODE.VIEW);
					Collection<UserTranslationTbl> userTranslationTblCollection = userVo.getUserTranslationTblCollection();
					userNames.add(userVo.getUsername());
					for (UserTranslationTbl userTranslationTbl : userTranslationTblCollection) {
						String userTranslationId = userTranslationTbl.getUserTranslationId();
						LanguagesTbl languageCode = userTranslationTbl.getLanguageCode();
						LANG_ENUM langEnum = LANG_ENUM.getLangEnum(languageCode.getLanguageCode());
						user.setTranslationId(langEnum, userTranslationId);
					}
					Users users = instance.getUsers();
					Map<String, IAdminTreeChild> usersChildren = users.getUsersChildren();
					char startChar = (user.getName().toString().toLowerCase().toCharArray()[0]);
					IAdminTreeChild iAdminTreeChild = usersChildren.get(String.valueOf(startChar));
					if (iAdminTreeChild instanceof UsersNameAlphabet) {
						UsersNameAlphabet usersNameAlphabet = (UsersNameAlphabet) iAdminTreeChild;
						usersNameAlphabet.add(userId, user);
					}
				}
			}
			AdminTreeviewer adminTree = XMAdminUtil.getInstance().getAdminTree();
			adminTree.refresh(true);
			adminTree.setSelection(adminTree.getSelection());
			if (userNames.size() > 0) {
				for (String name : userNames) {
					XMAdminUtil.getInstance().updateLogFile(
							messages.userObject + " '" + name + "' " + messages.userImport, MessageType.SUCCESS);
				}
			}
			if (statusMapList != null && !statusMapList.isEmpty()) {
				for (Map<String, String> statusMap : statusMapList) {
					String errorMessage = statusMap.get(XMAdminUtil.getInstance().getCurrentLocaleEnum().getLangCode());
					XMAdminUtil.getInstance().updateLogFile(errorMessage, MessageType.FAILURE);
				}
			}

			super.okPressed();
		} catch (CannotCreateObjectException e) {
			LOGGER.warn("Unable to Import User ! " + e);
		} catch (UnauthorizedAccessException e) {
			CustomMessageDialog.openError(this.getShell(), messages.unauthorizedDialogTitle,
					messages.unauthorizedDialogMsg);
		}
	}

	private com.magna.xmbackend.vo.user.UserRequest mapVOObjectWithModel(LdapUser userModel) {
		com.magna.xmbackend.vo.user.UserRequest userRequest = new com.magna.xmbackend.vo.user.UserRequest();

		userRequest.setId(CommonConstants.EMPTY_STR);
		userRequest.setIconId(((Icon)XMAdminUtil.getInstance().getIconByName("users.png")).getIconId());
		userRequest.setStatus(com.magna.xmbackend.vo.enums.Status.ACTIVE);
		userRequest.setUserName(userModel.getsAMAccountName());
		userRequest.setDepartment(CommonConstants.EMPTY_STR);
		userRequest.setEmail(userModel.getEmail());
		userRequest.setTelephoneNumber(userModel.getTelephoneNumber());
		userRequest.setManager(userModel.getManager());
		
		List<com.magna.xmbackend.vo.user.UserTranslation> userTranslationList = new ArrayList<>();
		LANG_ENUM[] lang_values = LANG_ENUM.values();
		for (int index = 0; index < lang_values.length; index++) {
			com.magna.xmbackend.vo.user.UserTranslation userTranslation = new com.magna.xmbackend.vo.user.UserTranslation();
			userTranslation.setLanguageCode(lang_values[index].getLangCode());
			userTranslation.setRemarks(CommonConstants.EMPTY_STR);
			userTranslation.setDescription(CommonConstants.EMPTY_STR);
			userTranslationList.add(userTranslation);
		}
		userRequest.setUserTranslation(userTranslationList);
		return userRequest;
	}
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.jface.dialogs.Dialog#createDialogArea(org.eclipse.swt.widgets
	 * .Composite)
	 */
	@Override
	protected Control createDialogArea(final Composite parent) {
		try {
			final Composite widgetContainer = new Composite(parent, SWT.NONE);
			final GridLayout widgetContainerLayout = new GridLayout(1, false);
			widgetContainer.setLayout(widgetContainerLayout);
			widgetContainer.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
			initGUI(widgetContainer);
			intiListener();
			updateLdapUrl();
		} catch (Exception e) {
			LOGGER.error("Unable to create UI elements", e);
		}
		return parent;

	}

	/**
	 * Update ldap url.
	 */
	private void updateLdapUrl() {
		PropertyConfigController propertyConfigController = new PropertyConfigController();
		List<PropertyConfigTbl> ldapDetails = propertyConfigController.getAllLDAPDetails(false);
		String url = CommonConstants.EMPTY_STR;
		String port = CommonConstants.EMPTY_STR;
		for (PropertyConfigTbl propertyConfigTbl : ldapDetails) {
			if (propertyConfigTbl.getProperty().equals(LdapKey.LDAP_URL.toString())) {
				url = propertyConfigTbl.getValue();
			} else if (propertyConfigTbl.getProperty().equals(LdapKey.LDAP_PORT_NUMBER.toString())) {
				port = propertyConfigTbl.getValue();
			}
		}
		this.lblLdapUrl.setText(messages.searchUserLdapUrllabel + url + ":" + port);
	}

	
	private void intiListener() {
		//searchBtn.setFocus();
		if (this.searchBtn != null) {
			this.searchBtn.addSelectionListener(new SelectionAdapter() {
				@Override
				public void widgetSelected(final SelectionEvent event) {
					final String userName = txtUserSearch.getText().trim();
					if (!userName.trim().isEmpty()) {
						final Display display = Display.getDefault();
						Job job = new Job("Searching User..") {
							@Override
							protected IStatus run(IProgressMonitor monitor) {
								monitor.beginTask("Searching User..", 100);
								monitor.worked(30);
								try {
									final LdapController ldapController = new LdapController();
									  LdapUsersResponse LdapUsersResponse = ldapController.searchAllMatchingUsers(userName);
									 
									final List<LdapUser> matchedUserList = LdapUsersResponse.getLdapUsers();
									Iterator<LdapUser> iterator = matchedUserList.iterator();
									while (iterator.hasNext()) {
										LdapUser user = iterator.next();
										if (user.getsAMAccountName() == null || user.getsAMAccountName().length() > 30) {
											iterator.remove();
										}
									}
									List<Map<String, String>> statusMapList = LdapUsersResponse.getStatusMaps();
									if (statusMapList != null && statusMapList.isEmpty() ) {
										ldapUsers.setLdapUsers(matchedUserList);
										if (matchedUserList.isEmpty()) {

											display.syncExec(new Runnable() {
												@Override
												public void run() {
													ldapSearchResultViewer.refresh();
													ldapUserInfoViewer.getTable().removeAll();
													CustomMessageDialog.openError(getShell(),
															messages.searchUserErrorDialogTitle,
															messages.searchUserErrorDialogMsg);
												}
											});
											return Status.CANCEL_STATUS;
										}
										display.asyncExec(new Runnable() {
											@Override
											public void run() {
												ldapSearchResultViewer.setInput(ldapUsers);
												ldapUserInfoViewer.getTable().removeAll();
											}
										});
									}else{
											Display.getDefault().asyncExec(new Runnable() {
												public void run() {
													String errorMessage = "";
													for (Map<String, String> statusMap : statusMapList) {
														errorMessage += statusMap.get(XMAdminUtil.getInstance().getCurrentLocaleEnum().getLangCode());
														errorMessage += "\n";
													}
													if (errorMessage.length() > 0) {
														CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
																messages.searchUserErrorDialogTitle, errorMessage);
													}
												}
											});
									}
								} catch (Exception e) {
									e.printStackTrace();
								}
								monitor.worked(70);
								return Status.OK_STATUS;
							}
						};
						job.schedule();
					}
				}
			});
		}
		
		this.ldapSearchResultViewer.addCheckStateListener(new ICheckStateListener() {
			
			@Override
			public void checkStateChanged(CheckStateChangedEvent event) {
				ldapSearchResultViewer.setSelection(new StructuredSelection(event.getElement()), true);
			}
		});
		
		this.ldapSearchResultViewer.addSelectionChangedListener(new ISelectionChangedListener() {

			@Override
			public void selectionChanged(final SelectionChangedEvent event) {

				final IStructuredSelection selection = (IStructuredSelection) event.getSelection();
				final LdapUser user = (LdapUser) selection.getFirstElement();

				if (user != null) {
					LdapUserProperties ldapUserProperties = new LdapUserProperties();
					LdapUserProperty ldapUserProperty = new LdapUserProperty();
					ldapUserProperty.setPropertName(LdapAttribute.LDAP_SAMACCOUNTNAME.toString());
					ldapUserProperty.setValue(user.getsAMAccountName());
					ldapUserProperties.getLdapUserProperties().add(ldapUserProperty);
					ldapUserProperty = new LdapUserProperty();
					ldapUserProperty.setPropertName(LdapAttribute.LDAP_CN.toString());
					ldapUserProperty.setValue(user.getCn());
					ldapUserProperties.getLdapUserProperties().add(ldapUserProperty);
					ldapUserProperty = new LdapUserProperty();
					ldapUserProperty.setPropertName(LdapAttribute.LDAP_SN.toString());
					ldapUserProperty.setValue(user.getSn());
					ldapUserProperties.getLdapUserProperties().add(ldapUserProperty);
					ldapUserProperty = new LdapUserProperty();
					ldapUserProperty.setPropertName(LdapAttribute.LDAP_TELEPHONENUMBER.toString());
					ldapUserProperty.setValue(user.getTelephoneNumber());
					ldapUserProperties.getLdapUserProperties().add(ldapUserProperty);
					ldapUserProperty = new LdapUserProperty();
					ldapUserProperty.setPropertName(LdapAttribute.LDAP_MAIL.toString());
					ldapUserProperty.setValue(user.getEmail());
					ldapUserProperties.getLdapUserProperties().add(ldapUserProperty);
					ldapUserProperty = new LdapUserProperty();
					ldapUserProperty.setPropertName(LdapAttribute.LDAP_USER_MANAGER.toString());
					ldapUserProperty.setValue(user.getManager());
					ldapUserProperties.getLdapUserProperties().add(ldapUserProperty);
					ldapUserInfoViewer.setInput(ldapUserProperties);
				}
			}
		});
		
		this.ldapSearchResultViewer.addSelectionChangedListener(new ISelectionChangedListener() {
			
			@Override
			public void selectionChanged(SelectionChangedEvent arg0) {
				if(ldapSearchResultViewer.getCheckedElements().length > 0){
					importBtn.setEnabled(true);
				}else{
					importBtn.setEnabled(false);
				}
			}
		});
	}

	/**
	 * Inits the GUI.
	 *
	 * @param parent
	 *            the parent
	 */
	private void initGUI(final Composite parent) {
		GridLayoutFactory.fillDefaults().applyTo(parent);
		parent.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));

		this.grpUserSearch = new Group(parent, SWT.NONE);
		GridLayoutFactory.fillDefaults().numColumns(1).applyTo(this.grpUserSearch);
		GridDataFactory.fillDefaults().grab(true, false).span(SWT.FILL, SWT.FILL).applyTo(this.grpUserSearch);

		final Composite widgetContainer = new Composite(this.grpUserSearch, SWT.NONE);
		final GridLayout widgetContainerLayout = new GridLayout(2, false);
		widgetContainer.setLayout(widgetContainerLayout);
		widgetContainer.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));

		this.lblLdapUrl = new Label(widgetContainer, SWT.NONE);
		GridDataFactory.fillDefaults().grab(false, false).align(SWT.FILL, SWT.CENTER).span(1, 1)
				.applyTo(this.lblLdapUrl);


		final Composite searchContainer = new Composite(widgetContainer, SWT.NONE);
		final GridLayout searchContainerLayout = new GridLayout(3, false);
		searchContainerLayout.marginRight = 0;
		searchContainerLayout.marginLeft = 0;
		searchContainerLayout.marginTop = 0;
		searchContainerLayout.marginBottom = 0;
		searchContainerLayout.marginWidth = 0;
		searchContainer.setLayout(searchContainerLayout);
		GridDataFactory.fillDefaults().grab(true, false).span(2, 1).align(SWT.FILL, SWT.CENTER)
				.applyTo(searchContainer);

		this.lblUserName = new Label(searchContainer, SWT.NONE);
		GridDataFactory.fillDefaults().grab(false, false).align(SWT.FILL, SWT.CENTER).span(1, 1)
				.applyTo(this.lblUserName);
		this.lblUserName.setText(messages.searchUserUserNamelabel);

		this.txtUserSearch = new Text(searchContainer, SWT.BORDER);
		GridDataFactory.fillDefaults().grab(true, false).align(SWT.FILL, SWT.CENTER).span(1, 1)
				.applyTo(this.txtUserSearch);


		this.searchBtn = new Button(searchContainer, SWT.NONE);
		this.searchBtn.setText(messages.searchUserSearchBtn);

		this.grpSearchResult = new Group(parent, SWT.NONE);
		GridLayoutFactory.fillDefaults().numColumns(1).applyTo(this.grpSearchResult);
		GridDataFactory.fillDefaults().grab(true, true).span(SWT.FILL, SWT.FILL).applyTo(this.grpSearchResult);
		this.grpSearchResult.setText(messages.searchUserGroupSerachResult);

		final Composite searchResultContainer = new Composite(this.grpSearchResult, SWT.NONE);
		final GridLayout searchResultContainerLayout = new GridLayout(1, false);
		searchResultContainer.setLayout(searchResultContainerLayout);
		searchResultContainer.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));

		final SashForm mainHorizantalSashForm = new SashForm(searchResultContainer, SWT.HORIZONTAL);
		mainHorizantalSashForm.setLayout(new GridLayout());
		final GridData gridData1 = new GridData(SWT.FILL, SWT.FILL, true, true);
		mainHorizantalSashForm.setLayoutData(gridData1);

		createSearchResulTree(mainHorizantalSashForm);
		createUserInfoTable(mainHorizantalSashForm);

		mainHorizantalSashForm.setWeights(new int[] { 1, 2 });

	}

	/**
	 * Creates the search resul tree.
	 *
	 * @param mainHorizantalSashForm
	 *            the main horizantal sash form
	 */
	private void createSearchResulTree(final SashForm mainHorizantalSashForm) {
		this.ldapSearchResultViewer = new CheckboxTreeViewer(mainHorizantalSashForm,SWT.FULL_SELECTION | SWT.V_SCROLL | SWT.H_SCROLL | SWT.BORDER);
		this.ldapSearchResultViewer.setLabelProvider(new LdapUserTreeLableProvider());
		this.ldapSearchResultViewer.setContentProvider(new LdapUserViewerContentProvider());
	}
	
	/**
	 * Creates the user info table.
	 *
	 * @param mainHorizantalSashForm
	 *            the main horizantal sash form
	 */
	private void createUserInfoTable(final SashForm mainHorizantalSashForm) {
		final Composite userInfoContainer = new Composite(mainHorizantalSashForm, SWT.NONE);
		final GridLayout userInfoContainerLayout = new GridLayout(1, false);
		userInfoContainer.setLayout(userInfoContainerLayout);
		userInfoContainer.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));

		this.ldapUserInfoViewer = new LdapUserInfoTableViewer(userInfoContainer);
		this.ldapUserInfoViewer.getTable().getColumn(0).setText(messages.searchUserPropertyNameCol);
		this.ldapUserInfoViewer.getTable().getColumn(1).setText(messages.searchUserPropertyValueCol);
		this.ldapUserInfoViewer.setLabelProvider(new LdapUserTreeLableProvider());
		this.ldapUserInfoViewer.setContentProvider(new LdapUserViewerContentProvider());
		// this.userInfoTreeViewer.setInput(list);
	}

}
