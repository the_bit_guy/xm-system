package com.magna.xmsystem.xmadmin.util;

import com.magna.xmbackend.entities.PermissionTbl;

/**
 * The Class ObjectGlobalPerm.
 * 
 * @author shashwat.anand
 */
public class ObjectGlobalPerm {
	
	/** The login cax start admin permission. */
	private PermissionTbl loginCaxStartAdminPermission;
	
	/** The login cax start hotline permission. */
	private PermissionTbl loginCaxStartHotlinePermission;
	
	/** The view inactive permission. */
	private PermissionTbl viewInactivePermission;
	
	/**
	 * Instantiates a new object global perm.
	 */
	public ObjectGlobalPerm() {
	}
	
	/**
	 * Gets the login cax start admin permission.
	 *
	 * @return the loginCaxStartAdminPermission
	 */
	public PermissionTbl getLoginCaxStartAdminPermission() {
		return loginCaxStartAdminPermission;
	}
	
	/**
	 * Sets the login cax start admin permission.
	 *
	 * @param loginCaxStartAdminPermission the loginCaxStartAdminPermission to set
	 */
	public void setLoginCaxStartAdminPermission(PermissionTbl loginCaxStartAdminPermission) {
		this.loginCaxStartAdminPermission = loginCaxStartAdminPermission;
	}
	
	/**
	 * Gets the login cax start hotline permission.
	 *
	 * @return the loginCaxStartHotlinePermission
	 */
	public PermissionTbl getLoginCaxStartHotlinePermission() {
		return loginCaxStartHotlinePermission;
	}
	
	/**
	 * Sets the login cax start hotline permission.
	 *
	 * @param loginCaxStartHotlinePermission the loginCaxStartHotlinePermission to set
	 */
	public void setLoginCaxStartHotlinePermission(PermissionTbl loginCaxStartHotlinePermission) {
		this.loginCaxStartHotlinePermission = loginCaxStartHotlinePermission;
	}
	
	/**
	 * Gets the view inactive permission.
	 *
	 * @return the viewInactivePermission
	 */
	public PermissionTbl getViewInactivePermission() {
		return viewInactivePermission;
	}
	
	/**
	 * Sets the view inactive permission.
	 *
	 * @param viewInactivePermission the viewInactivePermission to set
	 */
	public void setViewInactivePermission(PermissionTbl viewInactivePermission) {
		this.viewInactivePermission = viewInactivePermission;
	}
}
