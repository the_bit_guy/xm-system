package com.magna.xmsystem.xmadmin.ui.parts.groups.projectgroup;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.inject.Inject;

import org.eclipse.core.databinding.Binding;
import org.eclipse.core.databinding.DataBindingContext;
import org.eclipse.core.databinding.UpdateValueStrategy;
import org.eclipse.core.databinding.beans.BeanProperties;
import org.eclipse.core.databinding.observable.value.IObservableValue;
import org.eclipse.core.databinding.observable.value.IValueChangeListener;
import org.eclipse.core.databinding.observable.value.ValueChangeEvent;
import org.eclipse.e4.core.services.nls.Translation;
import org.eclipse.e4.ui.di.Persist;
import org.eclipse.e4.ui.model.application.ui.MDirtyable;
import org.eclipse.jface.databinding.fieldassist.ControlDecorationSupport;
import org.eclipse.jface.databinding.swt.WidgetProperties;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.viewers.TreePath;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.VerifyEvent;
import org.eclipse.swt.events.VerifyListener;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Link;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.widgets.ToolItem;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.client.ResourceAccessException;

import com.magna.xmbackend.entities.GroupTranslationTbl;
import com.magna.xmbackend.entities.GroupsTbl;
import com.magna.xmbackend.entities.LanguagesTbl;
import com.magna.xmsystem.dependencies.customExceptions.UnauthorizedAccessException;
import com.magna.xmsystem.dependencies.utils.LANG_ENUM;
import com.magna.xmsystem.dependencies.utils.XMSystemUtil;
import com.magna.xmsystem.ui.controls.widgets.CustomMessageDialog;
import com.magna.xmsystem.xmadmin.message.Message;
import com.magna.xmsystem.xmadmin.message.MessageRegistry;
import com.magna.xmsystem.xmadmin.restclient.group.GroupController;
import com.magna.xmsystem.xmadmin.ui.dialogs.lang.ControlModel;
import com.magna.xmsystem.xmadmin.ui.dialogs.lang.TextAreaModifyListener;
import com.magna.xmsystem.xmadmin.ui.dialogs.lang.XMAdminLangTextAreaDialog;
import com.magna.xmsystem.xmadmin.ui.dialogs.lang.XMAdminLangTextDialog;
import com.magna.xmsystem.xmadmin.ui.parts.icons.IconDialog;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.AdminTreeviewer;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.AdminTreeFactory;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.icon.Icon;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.group.Groups;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.group.projectgroup.ProjectGroupModel;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.group.projectgroup.ProjectGroupsModel;
import com.magna.xmsystem.xmadmin.ui.validation.NameValidation;
import com.magna.xmsystem.xmadmin.ui.validation.StatusValidation;
import com.magna.xmsystem.xmadmin.ui.validation.SymbolValidation;
import com.magna.xmsystem.xmadmin.util.CommonConstants;
import com.magna.xmsystem.xmadmin.util.MessageType;
import com.magna.xmsystem.xmadmin.util.XMAdminUtil;

public class ProjectGroupCompositeAction extends ProjectGroupCompositeUI {

	/** Logger instance. */
	private static final Logger LOGGER = LoggerFactory.getLogger(ProjectGroupCompositeAction.class);

	/** Member variable to store old model. */
	private ProjectGroupModel oldModel;

	/** Member variable for userGroup model. */
	private ProjectGroupModel projectGroupModel;

	/** The control model. */
	transient private ControlModel controlModel;

	/** Member variable for widgetValue. */
	transient private IObservableValue<?> widgetValue;

	/** Member variable for modelValue. */
	transient private IObservableValue<?> modelValue;

	/** Member variable for binding. */
	transient private Binding bindValue;

	/**
	 * Member variable for data binding context the DataBindingContext object
	 * will manage the databindings.
	 */
	final transient DataBindingContext dataBindContext = new DataBindingContext();

	/** Member variable for {@link MessageRegistry}. */
	@Inject
	private MessageRegistry registry;

	/** Member variable for message. */
	@Inject
	@Translation
	transient private Message messages;

	/**
	 * MDirtyable flag
	 */
	private MDirtyable dirty;

	@Inject
	public ProjectGroupCompositeAction(Composite parent) {
		super(parent, SWT.NONE);
		initListeners();
	}

	/**
	 * Method for Binding model to widget.
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public void bindValues() {
		try {
			// Name field binding
			widgetValue = WidgetProperties.text(SWT.Modify).observe(this.txtName);
			modelValue = BeanProperties.value(ProjectGroupModel.class, ProjectGroupModel.PROPERTY_GROUPNAME)
					.observe(this.projectGroupModel);
			widgetValue.addValueChangeListener(new IValueChangeListener() {

				@Override
				public void handleValueChange(ValueChangeEvent event) {

					updateButtonStatus(event);

				}
			});

			// define the UpdateValueStrategy
			final UpdateValueStrategy update = new UpdateValueStrategy();
			update.setAfterGetValidator(new NameValidation(messages, StatusValidation.COMMON_NODE_VALIDATE));
			bindValue = dataBindContext.bindValue(widgetValue, modelValue, update, null);
			ControlDecorationSupport.create(bindValue, SWT.TOP | SWT.LEFT);

			// Symbol toolItem setup
			Icon icon;
			String iconPath;
			if ((icon = this.projectGroupModel.getIcon()) != null
					&& !XMSystemUtil.isEmpty(iconPath = icon.getIconPath())) {
				if (iconPath.contains("null")) { //$NON-NLS-1$
					toolItem.setImage(XMSystemUtil.getInstance().getImage(this.getClass(), "icons/16x16/browse.png")); //$NON-NLS-1$
				} else {
					toolItem.setImage(XMSystemUtil.getInstance().getImage(this.getClass(), iconPath, true, true));

					// Symbol toolItem binding
					widgetValue = WidgetProperties.tooltipText().observe(this.toolItem);
					modelValue = BeanProperties
							.value(ProjectGroupModel.class,
									ProjectGroupModel.PROPERTY_ICON + "." + Icon.PROPERTY_ICONNAME, Icon.class)
							.observe(this.projectGroupModel);
					bindValue = dataBindContext.bindValue(widgetValue, modelValue);

					// Symbol Field binding
					widgetValue = WidgetProperties.text(SWT.Modify).observe(this.txtSymbol);
					modelValue = BeanProperties
							.value(ProjectGroupModel.class,
									ProjectGroupModel.PROPERTY_ICON + "." + Icon.PROPERTY_ICONNAME, Icon.class)
							.observe(this.projectGroupModel);
					final UpdateValueStrategy symbolUpdate = new UpdateValueStrategy();
					symbolUpdate.setAfterGetValidator(new SymbolValidation());
					bindValue = dataBindContext.bindValue(widgetValue, modelValue, symbolUpdate, null);
					ControlDecorationSupport.create(bindValue, SWT.TOP | SWT.LEFT);
				}
			}

		} catch (Exception e) {
			LOGGER.error("Exception occured while databinding", e);
		}
	}

	/**
	 * Method to open dialog
	 * 
	 * @param shell
	 */
	private void openDescDialog(Shell shell) {
		if (projectGroupModel == null) {
			return;
		}
		if (projectGroupModel.getOperationMode() != CommonConstants.OPERATIONMODE.VIEW) {
			String text = txtDesc.getText();
			LANG_ENUM currentLocaleEnum = XMAdminUtil.getInstance().getCurrentLocaleEnum();
			projectGroupModel.setDescription(currentLocaleEnum, text);
		}
		final Map<LANG_ENUM, String> obModelMap = new HashMap<>();
		obModelMap.put(LANG_ENUM.ENGLISH, this.projectGroupModel.getDescription(LANG_ENUM.ENGLISH));
		obModelMap.put(LANG_ENUM.GERMAN, this.projectGroupModel.getDescription(LANG_ENUM.GERMAN));
		boolean isEditable = txtDesc.getEditable();
		this.controlModel = new ControlModel(this.messages.objectDescriptionLabel, obModelMap,
				ProjectGroupModel.DESC_LIMIT, false, isEditable);
		this.controlModel.initDefaultLabels(this.messages);
		final XMAdminLangTextDialog dialogArea = new XMAdminLangTextDialog(shell, controlModel);
		final int retVal = dialogArea.open();
		if (retVal == IDialogConstants.OK_ID) {
			final Map<LANG_ENUM, String> descriptionMap = this.projectGroupModel.getDescriptionMap();
			descriptionMap.put(LANG_ENUM.ENGLISH, controlModel.getObjectModel(LANG_ENUM.ENGLISH));
			descriptionMap.put(LANG_ENUM.GERMAN, controlModel.getObjectModel(LANG_ENUM.GERMAN));
			updateDescWidget();
		}
	}

	/**
	 * Method to open dialog
	 * 
	 * @param shell
	 */
	private void openRemarkDialog(final Shell shell) {
		if (projectGroupModel == null) {
			return;
		}
		if (projectGroupModel.getOperationMode() != CommonConstants.OPERATIONMODE.VIEW) {
			String text = txtRemarks.getText();
			LANG_ENUM currentLocaleEnum = XMAdminUtil.getInstance().getCurrentLocaleEnum();
			projectGroupModel.setRemarks(currentLocaleEnum, text);
		}
		final Map<LANG_ENUM, String> obModelMap = new HashMap<>();
		obModelMap.put(LANG_ENUM.ENGLISH, this.projectGroupModel.getRemarks(LANG_ENUM.ENGLISH));
		obModelMap.put(LANG_ENUM.GERMAN, this.projectGroupModel.getRemarks(LANG_ENUM.GERMAN));
		boolean isEditable = txtRemarks.getEditable();
		this.controlModel = new ControlModel(this.messages.objectRemarkLabel, obModelMap,
				ProjectGroupModel.REMARK_LIMIT, false, isEditable);
		this.controlModel.initDefaultLabels(this.messages);
		final XMAdminLangTextAreaDialog dialogArea = new XMAdminLangTextAreaDialog(shell, controlModel);
		final int retVal = dialogArea.open();
		if (retVal == IDialogConstants.OK_ID) {
			final Map<LANG_ENUM, String> notesMap = this.projectGroupModel.getRemarksMap();
			notesMap.put(LANG_ENUM.ENGLISH, controlModel.getObjectModel(LANG_ENUM.ENGLISH));
			notesMap.put(LANG_ENUM.GERMAN, controlModel.getObjectModel(LANG_ENUM.GERMAN));
			updateRemarkWidget();
		}
	}

	/**
	 * Inits the listeners.
	 */
	private void initListeners() {

		// Event handling when users click on desc lang links.
		this.descLink.addSelectionListener(new SelectionAdapter() {
			/**
			 * description link handler
			 */
			@Override
			public void widgetSelected(final SelectionEvent event) {
				final Link linkWidget = (Link) event.widget;
				openDescDialog(linkWidget.getShell());
			}
		});

		// Event handling when users click on helptext lang links.
		this.remarksTranslate.addSelectionListener(new SelectionAdapter() {
			/**
			 * help text link handler
			 */
			@Override
			public void widgetSelected(final SelectionEvent event) {
				final Link linkWidget = (Link) event.widget;
				openRemarkDialog(linkWidget.getShell());
			}
		});
		if (this.saveBtn != null) {
			this.saveBtn.addSelectionListener(new SelectionAdapter() {

				/**
				 * Save button handler
				 */
				@Override
				public void widgetSelected(final SelectionEvent event) {
					saveGroupHandler();
				}

			});
		}
		if (this.cancelBtn != null) {
			this.cancelBtn.addSelectionListener(new SelectionAdapter() {

				/**
				 * Cancel button handler
				 */
				@Override
				public void widgetSelected(final SelectionEvent event) {
					cancelGroupHandler();
				}
			});
		}

		this.toolItem.addSelectionListener(new SelectionAdapter() {

			/**
			 * Symbol button handler
			 */
			@Override
			public void widgetSelected(final SelectionEvent event) {
				if ((boolean) toolItem.getData("editable")) {
					final ToolItem widget = (ToolItem) event.widget;
					final IconDialog dialog = new IconDialog(widget.getParent().getShell(),
							messages.browseIconDialogTitle, messages.icontableviewerSecondColumnLabel);

					final int returnVal = dialog.open();
					if (IDialogConstants.OK_ID == returnVal) {
						final Icon checkedIcon = dialog.getCheckedIcon();
						toolItem.setImage(XMSystemUtil.getInstance().getImage(this.getClass(), checkedIcon.getIconPath(), true, true));
						projectGroupModel.setIcon(checkedIcon);
					}
				}
			}
		});
		this.txtRemarks
				.addModifyListener(new TextAreaModifyListener(this.lblremarksCount, ProjectGroupModel.REMARK_LIMIT));
		
		this.txtRemarks.addVerifyListener(new VerifyListener() {

			@Override
			public void verifyText(VerifyEvent event) {
				String source = ((Text) event.widget).getText();
				final String remarkText = source.substring(0, event.start) + event.text + source.substring(event.end);
				int length = remarkText.length();
				if (length > ProjectGroupModel.REMARK_LIMIT) {
					event.doit = false;

				}
			}
		});
	}

	/**
	 * Save Handler
	 */
	public void saveGroupHandler() {
		saveDescAndRemarks();
		// validate the model
		if (validate()) {
			if (!projectGroupModel.getName().isEmpty() && projectGroupModel.getIcon().getIconId() != null) {
				if (projectGroupModel.getOperationMode() == CommonConstants.OPERATIONMODE.CREATE) {
					createGroupOperation();
				} else if (projectGroupModel.getOperationMode() == CommonConstants.OPERATIONMODE.CHANGE) {
					changeGroupOperation();
				}
			}
		}
	}

	/**
	 * Save desc and remarks.
	 */
	private void saveDescAndRemarks() {
		LANG_ENUM currentLocaleEnum = XMAdminUtil.getInstance().getCurrentLocaleEnum();
		String desc = txtDesc.getText();
		projectGroupModel.setDescription(currentLocaleEnum, desc);

		String remarks = txtRemarks.getText();
		projectGroupModel.setRemarks(currentLocaleEnum, remarks);

	}

	/**
	 * Validates the model before submit
	 * 
	 * @return boolean
	 */
	protected boolean validate() {
		String groupName = this.projectGroupModel.getName();
		Icon icon;
		if ((XMSystemUtil.isEmpty(groupName) && (icon = this.projectGroupModel.getIcon()) != null
				&& XMSystemUtil.isEmpty(icon.getIconName()))) {
			CustomMessageDialog.openError(this.getShell(), messages.nameSymbolErrorTitle, messages.nameSymbolError);
			return false;
		}
		if (XMSystemUtil.isEmpty(groupName)) {
			CustomMessageDialog.openError(this.getShell(), messages.nameErrorTitle, messages.nameError);
			return false;
		}
		if ((icon = this.projectGroupModel.getIcon()) != null && XMSystemUtil.isEmpty(icon.getIconName())) {
			CustomMessageDialog.openError(this.getShell(), messages.symbolErrorTitle, messages.symbolError);
			return false;
		}
		ProjectGroupsModel projectgroups = AdminTreeFactory.getInstance().getProjectGroups();
		Collection<IAdminTreeChild> groupsCollection = projectgroups.getProjectGroupsCollection();
		if (projectGroupModel.getOperationMode() == CommonConstants.OPERATIONMODE.CHANGE) {
			if (!groupName.equalsIgnoreCase(this.oldModel.getName())) {
				Map<String, Long> result = groupsCollection.parallelStream().collect(
						Collectors.groupingBy(group -> ((ProjectGroupModel) group).getName(), Collectors.counting()));
				if (result.containsKey(groupName)) {
					CustomMessageDialog.openError(this.getShell(), messages.existingProjectGroupNameTitle,
							messages.existingProjectGroupNameError);
					return false;
				}
			}
		} else if (this.projectGroupModel.getOperationMode() == CommonConstants.OPERATIONMODE.CREATE) {
			for (IAdminTreeChild group : groupsCollection) {
				if (groupName.equalsIgnoreCase(((ProjectGroupModel) group).getName())) {
					CustomMessageDialog.openError(this.getShell(), messages.existingProjectGroupNameTitle,
							messages.existingProjectGroupNameError);
					return false;
				}
			}
		}

		return true;
	}

	/**
	 * Gets the project old model.
	 *
	 * @return the project old model
	 */
	public ProjectGroupModel getProjectOldModel() {
		return oldModel;
	}

	/**
	 * Sets the project old model.
	 *
	 * @param projectOldModel the new project old model
	 */
	public void setProjectOldModel(ProjectGroupModel projectOldModel) {
		this.oldModel = projectOldModel;
	}

	/**
	 * Gets the project group model.
	 *
	 * @return the project group model
	 */
	public ProjectGroupModel getProjectGroupModel() {
		return projectGroupModel;
	}

	/**
	 * Sets the project group model.
	 *
	 * @param projectGroupModel the new project group model
	 */
	public void setProjectGroupModel(ProjectGroupModel projectGroupModel) {
		this.projectGroupModel = projectGroupModel;
	}

	/**
	 * Method for Creates the Group operation.
	 */
	private void createGroupOperation() {
		try {
			GroupController groupController = new GroupController();
			GroupsTbl groupVo = groupController.createGroup(mapVOObjectWithModel());
			String groupId = groupVo.getGroupId();
			if (!XMSystemUtil.isEmpty(groupId)) {
				this.projectGroupModel.setGroupId(groupId);
				Collection<GroupTranslationTbl> groupTranslationTblCollection = groupVo.getGroupTranslationTblCollection();
				for (GroupTranslationTbl groupTranslationTbl : groupTranslationTblCollection) {
					String groupTranslationId = groupTranslationTbl.getGroupTranslationId();
					LanguagesTbl languageCode = groupTranslationTbl.getLanguageCode();
					LANG_ENUM langEnum = LANG_ENUM.getLangEnum(languageCode.getLanguageCode());
					this.projectGroupModel.setTranslationId(langEnum, groupTranslationId);
				}
				
				AdminTreeFactory instance = AdminTreeFactory.getInstance();
				if (oldModel == null) {
					setProjectOldModel(projectGroupModel.deepCopyProjectGroup(false, null));
					AdminTreeFactory.getInstance().getProjectGroups().add(groupId, getProjectOldModel());
				}
				this.dirty.setDirty(false);
				AdminTreeviewer adminTree = XMAdminUtil.getInstance().getAdminTree();
				adminTree.refresh(true);

				adminTree.setSelection(new StructuredSelection(instance.getProjectGroups()), true);
				TreePath[] selectionPaths = adminTree.getStructuredSelection().getPaths();
				if (selectionPaths != null && selectionPaths.length > 0) {
					adminTree.setExpandedState(selectionPaths[0], true);
				}

				adminTree.setSelection(new StructuredSelection(getProjectOldModel()), true);
				setShowButtonBar(false);
				XMAdminUtil.getInstance().updateLogFile(messages.projectGroupObject + " " + "'"
						+ this.projectGroupModel.getName() + "'" + " " + messages.objectCreate, MessageType.SUCCESS);
			}

		} catch (UnauthorizedAccessException e) {
			CustomMessageDialog.openError(this.getShell(), messages.objectPermissionDialogTitle,
					messages.objectPermissionDialogMsg);
		} catch (Exception e) {
			if (e instanceof ResourceAccessException) {
				CustomMessageDialog.openError(Display.getCurrent().getActiveShell(), messages.errorDialogTitile,
						messages.serverNotReachable);
			}
			LOGGER.warn("Unable to Save Group data ! " + e);
		}
	}

	/**
	 * Map VO object with model.
	 *
	 * @return the com.magna.xmbackend.vo.group. group create request
	 */
	private com.magna.xmbackend.vo.group.GroupCreateRequest mapVOObjectWithModel() {
		com.magna.xmbackend.vo.group.GroupCreateRequest groupCreateRequest = new com.magna.xmbackend.vo.group.GroupCreateRequest();
		groupCreateRequest.setId(this.projectGroupModel.getGroupId());
		groupCreateRequest.setGroupName(this.projectGroupModel.getName());
		groupCreateRequest.setIconId(this.projectGroupModel.getIcon().getIconId());
		groupCreateRequest.setGroupType(com.magna.xmbackend.vo.enums.Groups.PROJECT.name());

		List<com.magna.xmbackend.vo.group.GroupTranslationRequest> groupTranslationList = new ArrayList<>();
		LANG_ENUM[] lang_values = LANG_ENUM.values();
		for (int index = 0; index < lang_values.length; index++) {
			com.magna.xmbackend.vo.group.GroupTranslationRequest groupTranslation = new com.magna.xmbackend.vo.group.GroupTranslationRequest();
			groupTranslation.setLanguageCode(lang_values[index].getLangCode());
			groupTranslation.setDescription(this.projectGroupModel.getDescription(lang_values[index]));
			groupTranslation.setRemarks(this.projectGroupModel.getRemarks(lang_values[index]));
			if (this.projectGroupModel.getOperationMode() == CommonConstants.OPERATIONMODE.CHANGE) {

				String translationId = this.projectGroupModel.getTranslationId(lang_values[index]);
				groupTranslation.setId(XMSystemUtil.isEmpty(translationId) ? CommonConstants.EMPTY_STR : translationId);
			}
			groupTranslationList.add(groupTranslation);
		}
		groupCreateRequest.setGroupTranslationReqs(groupTranslationList);

		return groupCreateRequest;
	}
	/**
	 * Method register method function for translation.
	 *
	 * @param registry
	 *            {@link MessageRegistry}
	 */
	public void registerMessages(final MessageRegistry registry) {
		registry.register((text) -> {
			if (grpGroup != null && !grpGroup.isDisposed()) {
				grpGroup.setText(text);
			}
		}, (message) -> {
			if (projectGroupModel != null) {
				if (projectGroupModel.getOperationMode() == CommonConstants.OPERATIONMODE.VIEW) {
					return message.objectGroupDisaplyLabel + " \'" + this.projectGroupModel.getName() + "\'";
				} else if (projectGroupModel.getOperationMode() == CommonConstants.OPERATIONMODE.CHANGE) {
					return message.objectGroupChangeLabel + " \'" + this.projectGroupModel.getName() + "\'";
				} else if (projectGroupModel.getOperationMode() == CommonConstants.OPERATIONMODE.CREATE) {
					return message.groupProjectModelCreateLabel;
				}
			}
			return CommonConstants.EMPTY_STR;
		});
		registry.register((text) -> {
			if (lblName != null && !lblName.isDisposed()) {
				lblName.setText(text);
			}
		}, (message) -> {
			if (lblName != null && !lblName.isDisposed()) {
				return getUpdatedWidgetText(message.objectNameLabel, lblName);
			}
			return CommonConstants.EMPTY_STR;
		});
		registry.register((text) -> {
			if (lblDescrition != null && !lblDescrition.isDisposed()) {
				lblDescrition.setText(text);
			}
		}, (message) -> {
			if (lblDescrition != null && !lblDescrition.isDisposed()) {
				return getUpdatedWidgetText(message.objectDescriptionLabel, lblDescrition);
			}
			return CommonConstants.EMPTY_STR;
		});

		registry.register((text) -> {
			if (lblSymbol != null && !lblSymbol.isDisposed()) {
				lblSymbol.setText(text);
			}
		}, (message) -> {
			if (lblSymbol != null && !lblSymbol.isDisposed()) {
				return getUpdatedWidgetText(message.objectSymbolLabel, lblSymbol);
			}
			return CommonConstants.EMPTY_STR;
		});
		if (saveBtn != null) {
			registry.register((text) -> {
				if (saveBtn != null && !saveBtn.isDisposed()) {
					saveBtn.setText(text);
				}
			}, (message) -> {
				if (saveBtn != null && !saveBtn.isDisposed()) {
					return getUpdatedWidgetText(message.saveButtonText, saveBtn);
				}
				return CommonConstants.EMPTY_STR;
			});
		}
		if (cancelBtn != null) {
			registry.register((text) -> {
				if (cancelBtn != null && !cancelBtn.isDisposed()) {
					cancelBtn.setText(text);
				}
			}, (message) -> {
				if (cancelBtn != null && !cancelBtn.isDisposed()) {
					return getUpdatedWidgetText(message.cancelButtonText, cancelBtn);
				}
				return CommonConstants.EMPTY_STR;
			});
		}
		registry.register((text) -> {
			if (remarksTranslate != null && !remarksTranslate.isDisposed()) {
				remarksTranslate.setText(text);
			}
		}, (message) -> {
			if (remarksTranslate != null && !remarksTranslate.isDisposed()) {
				return getUpdatedWidgetText("<a>" + message.objectTranslationLinkText + "</a>", remarksTranslate);
			}
			return CommonConstants.EMPTY_STR;
		});

		registry.register((text) -> {
			if (remarksLabel != null && !remarksLabel.isDisposed()) {
				remarksLabel.setText(text);
			}
		}, (message) -> {
			if (remarksLabel != null && !remarksLabel.isDisposed()) {
				return getUpdatedWidgetText(message.objectRemarkLabel, remarksLabel);
			}
			return CommonConstants.EMPTY_STR;
		});

		registry.register((text) -> {
			if (descLink != null && !descLink.isDisposed()) {
				descLink.setText(text);
			}
		}, (message) -> {
			if (descLink != null && !descLink.isDisposed()) {
				return getUpdatedWidgetText("<a>" + message.objectTranslationLinkText + "</a>", descLink);
			}
			return CommonConstants.EMPTY_STR;
		});

		registry.register((text) -> {
			if (txtDesc != null && !txtDesc.isDisposed()) {
				txtDesc.setText(text);
				updateDescWidget();
			}
		}, (message) -> {
			if (projectGroupModel != null && txtDesc != null && !txtDesc.isDisposed()) {
				final LANG_ENUM langEnum = XMAdminUtil.getInstance().getCurrentLocaleEnum();
				return this.projectGroupModel.getDescription(langEnum) == null ? CommonConstants.EMPTY_STR
						: this.projectGroupModel.getDescription(langEnum);
			}
			return CommonConstants.EMPTY_STR;
		});

		registry.register((text) -> {
			if (txtRemarks != null && !txtRemarks.isDisposed()) {
				txtRemarks.setText(text);
				updateRemarkWidget();
			}
		}, (message) -> {
			if (projectGroupModel != null && txtRemarks != null && !txtRemarks.isDisposed()) {
				final LANG_ENUM langEnum = XMAdminUtil.getInstance().getCurrentLocaleEnum();
				return this.projectGroupModel.getRemarks(langEnum) == null ? CommonConstants.EMPTY_STR
						: this.projectGroupModel.getRemarks(langEnum);
			}
			return CommonConstants.EMPTY_STR;
		});
	}

	/**
	 * Gets the updated widget text.
	 *
	 * @param message
	 *            the message
	 * @param control
	 *            the control
	 * @return the updated widget text
	 */
	private String getUpdatedWidgetText(String message, Control control) {
		control.requestLayout();
		control.getParent().redraw();
		control.getParent().getParent().update();
		control.getParent().getParent().getParent().update();
		return message;
	}

	/**
	 * Method to update the button status
	 * 
	 * @param event
	 */
	@SuppressWarnings("rawtypes")
	private void updateButtonStatus(final ValueChangeEvent event) {
		String name = (String) event.getObservableValue().getValue();
		if (this.saveBtn != null) {
			if (XMSystemUtil.isEmpty(name) || name.trim().length() == 0) {
				this.saveBtn.setEnabled(false);
			} else {
				this.saveBtn.setEnabled(true);
			}
		}
	}

	/**
	 * Method for Change group operation.
	 */
	private void changeGroupOperation() {
		try {
			GroupController groupController = new GroupController();
			boolean isUpdated = groupController.updateGroup(mapVOObjectWithModel());
			if (isUpdated) {
				setProjectOldModel(projectGroupModel.deepCopyProjectGroup(true, getProjectOldModel()));
				this.projectGroupModel.setOperationMode(CommonConstants.OPERATIONMODE.VIEW);
				setOperationMode();
				this.dirty.setDirty(false);
				final ProjectGroupsModel projectGroups = AdminTreeFactory.getInstance().getProjectGroups();
				projectGroups.sort();
				XMAdminUtil.getInstance().getAdminTree().refresh(true);
				XMAdminUtil.getInstance().getAdminTree().setSelection(new StructuredSelection(getProjectOldModel()),
						true);
				setShowButtonBar(false);
				XMAdminUtil.getInstance().updateLogFile(messages.projectGroupObject + " " + "'"
						+ this.projectGroupModel.getName() + "'" + " " + messages.objectUpdate, MessageType.SUCCESS);
			}
		} catch (UnauthorizedAccessException e) {
			CustomMessageDialog.openError(this.getShell(), messages.objectPermissionDialogTitle,
					messages.objectPermissionDialogMsg);
		} catch (Exception e) {
			if (e instanceof ResourceAccessException) {
				CustomMessageDialog.openError(Display.getCurrent().getActiveShell(), messages.errorDialogTitile,
						messages.serverNotReachable);
			}
			LOGGER.warn("Unable to Save Project Group data ! " + e);
		}
	}

	/**
	 * Method to set OperationMode.
	 */
	public void setOperationMode() {
		if (this.projectGroupModel != null) {
			if (projectGroupModel.getOperationMode() == CommonConstants.OPERATIONMODE.VIEW) {
				this.txtName.setEditable(false);
				this.txtDesc.setEditable(false);
				this.txtRemarks.setEditable(false);
				this.toolItem.setData("editable", false);
				setShowButtonBar(false);
			} else if (projectGroupModel.getOperationMode() == CommonConstants.OPERATIONMODE.CREATE) {
				this.txtName.setEditable(true);
				this.saveBtn.setEnabled(false);
				this.txtDesc.setEditable(true);
				this.txtRemarks.setEditable(true);
				this.toolItem.setData("editable", true);
				setShowButtonBar(true);
				this.dirty.setDirty(true);
			} else if (projectGroupModel.getOperationMode() == CommonConstants.OPERATIONMODE.CHANGE) {
				this.txtName.setEditable(true);
				this.toolItem.setData("editable", true);
				this.txtDesc.setEditable(true);
				this.txtRemarks.setEditable(true);
				setShowButtonBar(true);
				this.dirty.setDirty(true);
			} else {
				this.txtName.setEditable(false);
				this.txtDesc.setEditable(false);
				this.txtRemarks.setEditable(false);
				this.toolItem.setData("editable", false);
				setShowButtonBar(false);
			}
		}
	}

	public void setDirtyObject(final MDirtyable dirty) {
		this.dirty = dirty;
	}

	@Persist
	public void save() {
		dirty.setDirty(false);
	}

	/**
	 * Method for updating help text
	 */
	public void updateRemarkWidget() {
		if (this.projectGroupModel == null) {
			return;
		}
		int operationMode = this.projectGroupModel.getOperationMode();
		final LANG_ENUM currentLocaleEnum = XMAdminUtil.getInstance().getCurrentLocaleEnum();
		final String remarkForCurLocale = this.projectGroupModel.getRemarks(currentLocaleEnum) == null
				? CommonConstants.EMPTY_STR : this.projectGroupModel.getRemarks(currentLocaleEnum);
		if (operationMode != CommonConstants.OPERATIONMODE.VIEW) {
			this.txtRemarks.setText(remarkForCurLocale);
			return;
		}
		if (operationMode == CommonConstants.OPERATIONMODE.VIEW) {
			if (!XMSystemUtil.isEmpty(remarkForCurLocale)) {
				this.txtRemarks.setText(remarkForCurLocale);
				return;
			}
			final String remarkEN = this.projectGroupModel.getRemarks(LANG_ENUM.ENGLISH);
			final String remarkDE = this.projectGroupModel.getRemarks(LANG_ENUM.GERMAN);
			if (!XMSystemUtil.isEmpty(remarkEN)) {
				this.txtRemarks.setText(remarkEN);
				return;
			}

			if (!XMSystemUtil.isEmpty(remarkDE)) {
				this.txtRemarks.setText(remarkDE);
				return;
			}
		}
	}

	/**
	 * Update desc widget.
	 */
	public void updateDescWidget() {
		if (this.projectGroupModel == null) {
			return;
		}
		int operationMode = this.projectGroupModel.getOperationMode();
		final LANG_ENUM currentLocaleEnum = XMAdminUtil.getInstance().getCurrentLocaleEnum();
		final String descForCurLocale = this.projectGroupModel.getDescription(currentLocaleEnum) == null
				? CommonConstants.EMPTY_STR : this.projectGroupModel.getDescription(currentLocaleEnum);
		if (operationMode != CommonConstants.OPERATIONMODE.VIEW) {
			this.txtDesc.setText(descForCurLocale);
			return;
		}
		if (operationMode == CommonConstants.OPERATIONMODE.VIEW) {
			if (!XMSystemUtil.isEmpty(descForCurLocale)) {
				this.txtDesc.setText(descForCurLocale);
				return;
			}
			final String descriptionEN = this.projectGroupModel.getDescription(LANG_ENUM.ENGLISH);
			final String descriptionDE = this.projectGroupModel.getDescription(LANG_ENUM.GERMAN);
			if (!XMSystemUtil.isEmpty(descriptionEN)) {
				this.txtDesc.setText(descriptionEN);
				return;
			}

			if (!XMSystemUtil.isEmpty(descriptionDE)) {
				this.txtDesc.setText(descriptionDE);
				return;
			}
		}
	}

	/**
	 * Cancel site handler.
	 */
	public void cancelGroupHandler() {
		if (projectGroupModel == null) {
			dirty.setDirty(false);
			return;
		}
		String groupId = CommonConstants.EMPTY_STR;
		int operationMode = this.projectGroupModel.getOperationMode();
		ProjectGroupModel oldModel = getProjectOldModel();
		if (oldModel != null) {
			groupId = oldModel.getGroupId();
		}
		setProjectGroupModel(null);
		setProjectOldModel(null);
		this.saveBtn.setEnabled(true);
		dirty.setDirty(false);
		final AdminTreeviewer adminTree = XMAdminUtil.getInstance().getAdminTree();
		final Groups groups = AdminTreeFactory.getInstance().getGroups();
		if (operationMode == CommonConstants.OPERATIONMODE.CHANGE) {
			final IStructuredSelection selection = (IStructuredSelection) adminTree.getSelection();
			final Object firstElement = selection.getFirstElement();
			IAdminTreeChild iAdminTreeChild = groups.getGroupsChildren().get(ProjectGroupsModel.class.getSimpleName());
			if (firstElement != null && iAdminTreeChild instanceof  ProjectGroupsModel && firstElement.equals(((ProjectGroupsModel)iAdminTreeChild).getProjectGroupsChildren().get(groupId))) {
				adminTree.setSelection(new StructuredSelection(((ProjectGroupsModel)iAdminTreeChild).getProjectGroupsChildren().get(groupId)), true);
			}
		} else {
			adminTree.setSelection(new StructuredSelection(groups), true);
		}
	}

	/**
	 * set the group model from selection.
	 */
	public void setGroup() {
		try {
			final AdminTreeviewer adminTree = XMAdminUtil.getInstance().getAdminTree();
			final Object selectionObj = adminTree.getSelection();
			if (selectionObj instanceof IStructuredSelection) {
				Object firstElement = ((IStructuredSelection) selectionObj).getFirstElement();
				if (firstElement instanceof ProjectGroupModel) {
					this.setProjectOldModel((ProjectGroupModel) firstElement);
					ProjectGroupModel rightHandObject = this.getProjectOldModel().deepCopyProjectGroup(false, null);
					this.setProjectGroupModel(rightHandObject);
					this.registerMessages(this.registry);
					this.bindValues();
					this.setOperationMode();
				}
			}
		} catch (Exception e) {
			LOGGER.warn("Unable to set group model selection ! " + e);
		}

	}

	/**
	 * Sets the model.
	 *
	 * @param group
	 *            the new model
	 */
	public void setModel(ProjectGroupModel group) {
		try {
			setProjectOldModel(null);
			setProjectGroupModel(group);
			registerMessages(this.registry);
			bindValues();
			setOperationMode();
			updateDescWidget();
			updateRemarkWidget();
		} catch (Exception e) {
			LOGGER.warn("Unable to set group model ! " + e);
		}
	}
}
