package com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.directory;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.stream.Collectors;

import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.projectapplicationmodel.ProjectApplication;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.refs.RelationObj;

public class DirectoryProjectApplications implements IAdminTreeChild {
	
	/** The parent. */
	private IAdminTreeChild parent;
	
	/** The directory application children. */
	private Map<String, IAdminTreeChild> directoryProjectAppChildren;
	
	
	/**
	 * Instantiates a new directory project app model.
	 *
	 * @param parent the parent
	 */
	public DirectoryProjectApplications(final IAdminTreeChild parent) {
		this.parent = parent;
		this.directoryProjectAppChildren = new LinkedHashMap<>();
	}
	
	/**
	 * Adds the.
	 *
	 * @param directoryProjectAppsChildrenId the directory project apps children id
	 * @param child the child
	 * @return the i admin tree child
	 */
	public IAdminTreeChild add(final String directoryProjectAppsChildrenId, final IAdminTreeChild child) {
		IAdminTreeChild returnVal = this.directoryProjectAppChildren.put(directoryProjectAppsChildrenId, child);
		if (child instanceof RelationObj && ((RelationObj) child).getRefObject() instanceof ProjectApplication) {
			sort();
		}
		return returnVal;
	}
	
	/**
	 * Removes the.
	 *
	 * @param directoryProjectAppsChildrenId the directory project apps children id
	 * @return the i admin tree child
	 */
	public IAdminTreeChild remove(final String directoryProjectAppsChildrenId) {
		return this.directoryProjectAppChildren.remove(directoryProjectAppsChildrenId);

	}
	
	/**
	 * Gets the directory project apps children collection.
	 *
	 * @return the directory project apps children collection
	 */
	public Collection<IAdminTreeChild> getDirectoryProjectAppsChildrenCollection() {
		return this.directoryProjectAppChildren.values();
	}
	
	/**
	 * Gets the directory project apps children children.
	 *
	 * @return the directory project apps children children
	 */
	public Map<String, IAdminTreeChild> getDirectoryProjectAppsChildrenChildren() {
		return directoryProjectAppChildren;
	}

	/* (non-Javadoc)
	 * @see com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild#getParent()
	 */
	@Override
	public IAdminTreeChild getParent() {
		// TODO Auto-generated method stub
		return this.parent;
	}

	/* (non-Javadoc)
	 * @see com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild#setParent(com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild)
	 */
	@Override
	public void setParent(IAdminTreeChild parent) {
		this.parent = parent;

		
	}
	
	/**
	 * Sort.
	 */
	public void sort() {
		final LinkedHashMap<String, IAdminTreeChild> collect = this.directoryProjectAppChildren.entrySet().stream().sorted(
				(e1, e2) -> ((ProjectApplication) e1.getValue()).getName().compareTo(((ProjectApplication) e2.getValue()).getName()))
				.collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e1, LinkedHashMap::new));
		this.directoryProjectAppChildren = collect;
	}


}
