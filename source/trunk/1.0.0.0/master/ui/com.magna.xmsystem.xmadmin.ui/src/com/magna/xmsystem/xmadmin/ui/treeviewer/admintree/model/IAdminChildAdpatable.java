package com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model;

import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.refs.RelationObj;

/**
 * The Interface IAdminChildAdpatable.
 * 
 * @author shashwat.anand
 */
public interface IAdminChildAdpatable {
	
	/**
	 * Returns an object which is an instance of the given class
	 * associated with this object. Returns <code>null</code> if
	 * no such object can be found.
	 * @param <T>
	 *
	 * @param parent object from tree model adapter the adapter class to look up
	 * @param relationId {@link String}
	 * @param relationStatus {@link boolean}
	 * @return a object of the given class, 
	 *    or <code>null</code> if this object does not
	 *    have an adapter for the given class
	 */
	public <T> RelationObj getAdapter(Class<T> adapterType, IAdminTreeChild parent, String relationId, boolean relationStatus);
}
