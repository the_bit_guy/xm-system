package com.magna.xmsystem.xmadmin.ui.parts.projectdeleteevt;

import org.eclipse.jface.layout.GridDataFactory;
import org.eclipse.jface.layout.GridLayoutFactory;
import org.eclipse.nebula.widgets.pgroup.PGroup;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.ScrolledComposite;
import org.eclipse.swt.custom.StackLayout;
import org.eclipse.swt.custom.StyledText;
import org.eclipse.swt.events.ControlAdapter;
import org.eclipse.swt.events.ControlEvent;
import org.eclipse.swt.events.ExpandEvent;
import org.eclipse.swt.events.ExpandListener;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.widgets.ToolBar;
import org.eclipse.swt.widgets.ToolItem;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.magna.xmsystem.ui.controls.widgets.MagnaCustomCombo;
import com.magna.xmsystem.xmadmin.ui.parts.notification.FilterPanel;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.notification.NotificationEvtActions;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.notification.ProjectDeleteEvtAction;
import com.magna.xmsystem.xmadmin.util.XMAdminUtil;

// TODO: Auto-generated Javadoc
/**
 * The Class ProjectDeleteCompositeUI.
 */
public class ProjectDeleteEvtCompositeUI extends Composite{

	/** The Constant LOGGER. */
	private static final Logger LOGGER = LoggerFactory.getLogger(ProjectDeleteEvtCompositeUI.class);

	/** The grp pro delete evt. */
	protected Group grpProDeleteEvt;

	/** The lbl name. */
	protected Label lblName;

	/** The txt name. */
	protected Text txtName;

	/** The lbl active. */
	protected Label lblActive;

	/** The active btn. */
	protected Button activeBtn;

	/** The lbl description. */
	protected Label lblDescription;

	/** The txt description. */
	protected StyledText txtDescription;

	/** The base filter container. */
	private Composite baseFilterContainer;

	/** The filter label user. */
	protected Label filterLabelUser;

	/** The user filter button. */
	protected Button userFilterButton;

	/** The user filter text. */
	protected Text userFilterText;

	/** The user filter combo. */
	protected MagnaCustomCombo userFilterCombo;

	/** The filter label user group. */
	protected Label filterLabelUserGroup;

	/** The user group filter text. */
	protected Text userGroupFilterText;

	/** The user group filter combo. */
	protected MagnaCustomCombo userGroupFilterCombo;

	/** The user group filter button. */
	protected Button userGroupFilterButton;

	/** The addto to btn. */
	protected Button addtoToBtn;

	/** The addto CC btn. */
	protected Button addtoCCBtn;

	/** The lbl to users. */
	protected Label lblToUsers;

	/** The txt to users. */
	protected StyledText txtToUsers;

	/** The lbl CC users. */
	protected Label lblCCUsers;

	/** The txt CC users. */
	protected StyledText txtCCUsers;

	/** The user group P group. */
	protected PGroup userGroupPGroup;

	/** The user filer P group. */
	protected PGroup userFilerPGroup;

	/** The radio btn container. */
	protected Composite radioBtnContainer;

	/** The radio btn user. */
	protected Button radioBtnUser;

	/** The radio btn user group. */
	protected Button radioBtnUserGroup;

	/** The stack container. */
	protected Composite stackContainer;

	/** The stack layout. */
	protected StackLayout stackLayout;

	/** The filter panel. */
	protected FilterPanel filterPanel;

	/** The filter container. */
	protected Composite filterContainer;

	/** The base scrolled composite. */
	protected ScrolledComposite baseScrolledComposite;

	/** The save btn. */
	protected Button saveBtn;

	/** The cancel btn. */
	protected Button cancelBtn;
	
	/** The lbl tmplate. */
	protected Label lblTemplate;
	
	/** The txt tmplate. */
	protected Text txtTemplate;
	
	/** The tool item template. */
	protected ToolItem toolItemTemplate;

	/**
	 * Instantiates a new project create evt composite UI.
	 *
	 * @param parent
	 *            the parent
	 * @param style
	 *            the style
	 */
	public ProjectDeleteEvtCompositeUI(final Composite parent, final int style) {
		super(parent, style);
		this.initGUI();
	}

	/**
	 * Init GUI.
	 */
	private void initGUI() {
		try {
			GridLayoutFactory.fillDefaults().applyTo(this);
			this.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));

			this.grpProDeleteEvt = new Group(this, SWT.NONE);
			this.grpProDeleteEvt.setBackgroundMode(SWT.INHERIT_FORCE);
			GridLayoutFactory.fillDefaults().numColumns(1).applyTo(this.grpProDeleteEvt);
			GridDataFactory.fillDefaults().grab(true, true).span(SWT.FILL, SWT.FILL).applyTo(this.grpProDeleteEvt);
			baseScrolledComposite = XMAdminUtil.getInstance().createScrolledComposite(this.grpProDeleteEvt);
			baseScrolledComposite.setBackgroundMode(SWT.INHERIT_FORCE);

			final Composite widgetContainer = new Composite(baseScrolledComposite, SWT.NONE);
			final GridLayout widgetContLayout = new GridLayout(3, false);

			widgetContainer.setLayout(widgetContLayout);
			widgetContainer.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));

			this.lblName = new Label(widgetContainer, SWT.NONE);
			this.txtName = new Text(widgetContainer, SWT.BORDER);
			this.txtName.setTextLimit(ProjectDeleteEvtAction.NAME_LIMIT);
			GridDataFactory.fillDefaults().grab(true, false).span(2, 1).align(SWT.FILL, SWT.CENTER)
					.applyTo(this.txtName);
			this.txtName.setTextLimit(NotificationEvtActions.NAME_LIMIT);

			this.lblActive = new Label(widgetContainer, SWT.NONE);
			this.activeBtn = new Button(widgetContainer, SWT.CHECK);
			GridDataFactory.fillDefaults().grab(false, false).align(SWT.LEFT, SWT.CENTER).applyTo(this.lblActive);
			GridDataFactory.fillDefaults().grab(false, false).align(SWT.LEFT, SWT.CENTER ).span(2, 1)
					.applyTo(this.activeBtn);

			this.lblDescription = new Label(widgetContainer, SWT.NONE);
			this.lblDescription.setLayoutData(new GridData(SWT.LEFT, SWT.TOP, false, false));

			this.txtDescription = new StyledText(widgetContainer,
					SWT.BORDER | SWT.WRAP | SWT.MULTI | SWT.H_SCROLL | SWT.V_SCROLL);
			GridData gridData = new GridData(SWT.FILL, SWT.FILL, true, true);
			gridData.minimumHeight = 10;
			gridData.horizontalSpan = 2;
			this.txtDescription.setLayoutData(gridData);
			this.txtDescription.setEditable(false);
			this.txtDescription.setTextLimit(NotificationEvtActions.DESC_LIMIT);

			Label emptyLbl_radionBtn = new Label(widgetContainer, SWT.NONE);
			gridData = new GridData();
			emptyLbl_radionBtn.setLayoutData(gridData);

			this.radioBtnContainer = new Composite(widgetContainer, SWT.NONE);
			GridLayoutFactory.fillDefaults().numColumns(3).equalWidth(false).extendedMargins(0, 0, 0, 0)
					.applyTo(this.radioBtnContainer);
			GridDataFactory.fillDefaults().grab(true, false).span(2, 1).align(SWT.FILL, SWT.CENTER)
					.applyTo(this.radioBtnContainer);

			this.radioBtnUser = new Button(this.radioBtnContainer, SWT.RADIO);
			GridDataFactory.fillDefaults().grab(true, false).span(1, 1).align(SWT.FILL, SWT.CENTER)
					.applyTo(this.radioBtnUser);
			this.radioBtnUser.setSelection(true);

			this.radioBtnUserGroup = new Button(this.radioBtnContainer, SWT.RADIO);
			GridDataFactory.fillDefaults().grab(true, false).span(1, 1).align(SWT.FILL, SWT.CENTER)
					.applyTo(this.radioBtnUserGroup);

			Label emptyLbl_radionBtn2 = new Label(widgetContainer, SWT.NONE);
			gridData = new GridData();
			emptyLbl_radionBtn2.setLayoutData(gridData);

			this.baseFilterContainer = new Composite(widgetContainer, SWT.NONE);
			final GridLayout baseFilterCompLayout = new GridLayout(3, false);
			this.baseFilterContainer.setLayout(baseFilterCompLayout);
			GridDataFactory.fillDefaults().grab(true, false).span(2, 1).align(SWT.FILL, SWT.FILL)
					.applyTo(this.baseFilterContainer);

			this.stackContainer = new Composite(baseFilterContainer, SWT.NONE);
			this.stackLayout = new StackLayout();
			this.stackContainer.setLayout(this.stackLayout);
			GridDataFactory.fillDefaults().grab(true, false).span(2, 1).align(SWT.FILL, SWT.CENTER)
					.applyTo(this.stackContainer);

			this.userFilerPGroup = new PGroup(stackContainer, SWT.NONE);
			userFilerPGroup.setLayout(new GridLayout());
			GridDataFactory.fillDefaults().grab(true, false).span(1, 1).align(SWT.FILL, SWT.CENTER)
					.applyTo(this.userFilerPGroup);
			userFilerPGroup.addExpandListener(new ExpandListener() {

				@Override
				public void itemExpanded(final ExpandEvent event) {
					pGroupRepaint((PGroup) event.widget);
				}

				@Override
				public void itemCollapsed(final ExpandEvent event) {
					pGroupRepaint((PGroup) event.widget);
				}
			});

			userGroupPGroup = new PGroup(stackContainer, SWT.SMOOTH);
			userGroupPGroup.setLayout(new GridLayout());
			GridDataFactory.fillDefaults().grab(true, false).span(1, 1).align(SWT.FILL, SWT.FILL)
					.applyTo(this.userGroupPGroup);
			userGroupPGroup.addExpandListener(new ExpandListener() {

				@Override
				public void itemExpanded(final ExpandEvent event) {
					pGroupRepaint((PGroup) event.widget);
				}

				@Override
				public void itemCollapsed(final ExpandEvent event) {
					pGroupRepaint((PGroup) event.widget);
				}
			});

			ScrolledComposite userScrolledComposite = XMAdminUtil.getInstance()
					.createScrolledComposite(userFilerPGroup);
			final Composite userFilerPanelContainer = new Composite(userScrolledComposite, SWT.NONE);
			userFilerPanelContainer.setLayout(new GridLayout(4, false));
			userFilerPanelContainer.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));

			final GridData filterButtonsGridData = new GridData();
			filterButtonsGridData.widthHint = 30;
			filterButtonsGridData.heightHint = 30;

			this.userFilterText = new Text(userFilerPanelContainer, SWT.BORDER);
			GridDataFactory.fillDefaults().grab(true, false).span(1, 1).align(SWT.FILL, SWT.CENTER)
					.applyTo(this.userFilterText);
			this.userFilterButton = new Button(userFilerPanelContainer, SWT.PUSH);
			this.userFilterButton.setLayoutData(filterButtonsGridData);
			this.userFilterCombo = new MagnaCustomCombo(userFilerPanelContainer, SWT.BORDER | SWT.READ_ONLY);
			GridDataFactory.fillDefaults().grab(true, false).span(2, 1).minSize(120, SWT.DEFAULT)
					.align(SWT.FILL, SWT.CENTER).applyTo(this.userFilterCombo);

			ScrolledComposite userGrpScrolledComposite = XMAdminUtil.getInstance()
					.createScrolledComposite(userGroupPGroup);
			final Composite userGroupFilerPanelContainer = new Composite(userGrpScrolledComposite, SWT.NONE);
			userGroupFilerPanelContainer.setLayout(new GridLayout(4, false));
			userGroupFilerPanelContainer.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));

			this.userGroupFilterText = new Text(userGroupFilerPanelContainer, SWT.BORDER);
			GridDataFactory.fillDefaults().grab(true, false).span(1, 1).align(SWT.FILL, SWT.CENTER)
					.minSize(100, SWT.DEFAULT).applyTo(this.userGroupFilterText);
			this.userGroupFilterButton = new Button(userGroupFilerPanelContainer, SWT.PUSH);
			this.userGroupFilterButton.setLayoutData(filterButtonsGridData);
			this.userGroupFilterCombo = new MagnaCustomCombo(userGroupFilerPanelContainer, SWT.BORDER | SWT.READ_ONLY);
			GridDataFactory.fillDefaults().grab(true, false).span(2, 1).align(SWT.FILL, SWT.CENTER)
					.minSize(150, SWT.DEFAULT).applyTo(this.userGroupFilterCombo);

			this.stackLayout.topControl = this.userFilerPGroup;
			final Composite addBtnContainer = new Composite(baseFilterContainer, SWT.NONE);
			final GridLayout addBtnCompLayout = new GridLayout(1, false);
			addBtnCompLayout.marginRight = 0;
			addBtnCompLayout.marginLeft = 0;
			addBtnCompLayout.marginTop = 0;
			addBtnCompLayout.marginBottom = 0;
			addBtnCompLayout.marginWidth = 0;
			addBtnContainer.setLayout(addBtnCompLayout);
			addBtnContainer.setLayoutData(new GridData(SWT.FILL, SWT.DEFAULT, false, false, 1, 1));

			this.addtoToBtn = new Button(addBtnContainer, SWT.NONE);
			this.addtoToBtn.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));
			this.addtoCCBtn = new Button(addBtnContainer, SWT.NONE);
			this.addtoCCBtn.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));

			this.lblToUsers = new Label(widgetContainer, SWT.NONE);
			GridDataFactory.fillDefaults().grab(false, false).span(1, 1).align(SWT.LEFT, SWT.CENTER)
					.applyTo(this.lblToUsers);

			this.txtToUsers = new StyledText(widgetContainer,
					SWT.BORDER | SWT.WRAP | SWT.MULTI | SWT.H_SCROLL | SWT.V_SCROLL);
			gridData = new GridData(SWT.FILL, SWT.FILL, true, true);
			gridData.minimumHeight = 30;
			gridData.horizontalSpan = 2;
			this.txtToUsers.setLayoutData(gridData);

			this.lblCCUsers = new Label(widgetContainer, SWT.NONE);
			GridDataFactory.fillDefaults().grab(false, false).span(1, 1).align(SWT.LEFT, SWT.CENTER)
					.applyTo(this.lblCCUsers);

			this.txtCCUsers = new StyledText(widgetContainer,
					SWT.BORDER | SWT.WRAP | SWT.MULTI | SWT.H_SCROLL | SWT.V_SCROLL);
			gridData = new GridData(SWT.FILL, SWT.FILL, true, true);
			gridData.minimumHeight = 30;
			gridData.horizontalSpan = 2;
			this.txtCCUsers.setLayoutData(gridData);
			
			this.lblTemplate = new Label(widgetContainer, SWT.NONE);
			
			this.txtTemplate = new Text(widgetContainer, SWT.BORDER);
			GridDataFactory.fillDefaults().grab(true, false).span(1, 1).align(SWT.FILL, SWT.CENTER)
			.applyTo(this.txtTemplate);
			this.txtTemplate.setEditable(false);
			
			final ToolBar toolbar = new ToolBar(widgetContainer, SWT.NONE);
			this.toolItemTemplate = new ToolItem(toolbar, SWT.FLAT);
			this.toolItemTemplate.setText(" ... ");
			toolbar.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false));

			userGrpScrolledComposite.setContent(userGroupFilerPanelContainer);
			userGrpScrolledComposite.setSize(userGroupFilerPanelContainer.getSize());
			userGrpScrolledComposite.setExpandVertical(true);
			userGrpScrolledComposite.setExpandHorizontal(true);
			userGrpScrolledComposite.update();

			userGrpScrolledComposite.addControlListener(new ControlAdapter() {
				public void controlResized(final ControlEvent e) {
					userGrpScrolledComposite
							.setMinSize(userGroupFilerPanelContainer.computeSize(SWT.DEFAULT, SWT.DEFAULT));
				}
			});

			userScrolledComposite.setContent(userFilerPanelContainer);
			userScrolledComposite.setSize(userFilerPanelContainer.getSize());
			userScrolledComposite.setExpandVertical(true);
			userScrolledComposite.setExpandHorizontal(true);
			userScrolledComposite.update();

			userGrpScrolledComposite.addControlListener(new ControlAdapter() {
				public void controlResized(final ControlEvent e) {
					userScrolledComposite.setMinSize(userFilerPanelContainer.computeSize(SWT.DEFAULT, SWT.DEFAULT));
				}
			});

			baseScrolledComposite.setContent(widgetContainer);
			baseScrolledComposite.setSize(widgetContainer.getSize());
			baseScrolledComposite.setExpandVertical(true);
			baseScrolledComposite.setExpandHorizontal(true);
			baseScrolledComposite.update();

			baseScrolledComposite.addControlListener(new ControlAdapter() {
				public void controlResized(final ControlEvent e) {
					Rectangle rectangle = baseScrolledComposite.getClientArea();
					baseScrolledComposite.setMinSize(widgetContainer.computeSize(rectangle.width, SWT.DEFAULT));
				}
			});

			final Composite buttonBarComp = new Composite(this.grpProDeleteEvt, SWT.NONE);
			final GridLayout btnBarCompLayout = new GridLayout(2, true);
			btnBarCompLayout.marginRight = 0;
			btnBarCompLayout.marginLeft = 0;
			btnBarCompLayout.marginTop = 0;
			btnBarCompLayout.marginBottom = 0;
			btnBarCompLayout.marginWidth = 0;
			buttonBarComp.setLayout(btnBarCompLayout);
			buttonBarComp.setLayoutData(new GridData(SWT.FILL, SWT.DEFAULT, true, false, 3, 1));
			createButtonBar(buttonBarComp);

		} catch (Exception e) {
			LOGGER.error("Unable to crete UI elements", e); //$NON-NLS-1$
		}

	}

	/**
	 * Create button bar.
	 *
	 * @param buttonBarComp
	 *            the button bar comp
	 */
	private void createButtonBar(final Composite buttonBarComp) {
		this.saveBtn = new Button(buttonBarComp, SWT.NONE);
		this.saveBtn.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, true, false));

		this.cancelBtn = new Button(buttonBarComp, SWT.NONE);
		this.cancelBtn.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, true, false));
	}

	/**
	 * P group repaint.
	 *
	 * @param pGroup
	 *            the group
	 */
	protected void pGroupRepaint(final PGroup pGroup) {
		pGroup.requestLayout();
		pGroup.redraw();
		pGroup.getParent().update();
		baseScrolledComposite.notifyListeners(SWT.Resize, new Event());
	}

	/**
	 * Sets the show button bar.
	 *
	 * @param showButtonBar
	 *            the new show button bar
	 */
	protected void setShowButtonBar(final boolean showButtonBar) {
		if (this.saveBtn != null && !this.saveBtn.isDisposed() && this.cancelBtn != null
				&& !this.cancelBtn.isDisposed()) {
			final GridData layoutData = (GridData) this.saveBtn.getParent().getLayoutData();
			layoutData.exclude = !showButtonBar;
			this.saveBtn.setVisible(showButtonBar);
			this.cancelBtn.setVisible(showButtonBar);
			this.saveBtn.getParent().setVisible(showButtonBar);
			this.saveBtn.getParent().requestLayout();
			this.saveBtn.getParent().redraw();
			this.saveBtn.getParent().getParent().update();
		}
	}
}
