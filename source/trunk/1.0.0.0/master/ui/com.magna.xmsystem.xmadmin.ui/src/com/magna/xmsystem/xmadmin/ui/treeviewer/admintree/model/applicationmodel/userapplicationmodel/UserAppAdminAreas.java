package com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.userapplicationmodel;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.stream.Collectors;

import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.adminarea.AdministrationArea;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.refs.RelationObj;

/**
 * Class for User app admin areas.
 *
 * @author Chiranjeevi.Akula
 */
public class UserAppAdminAreas implements IAdminTreeChild {

	/** Member variable 'parent' for {@link IAdminTreeChild}. */
	private IAdminTreeChild parent;

	/** Member variable 'user app admin area children' for {@link Map<String,IAdminTreeChild>}. */
	private Map<String, IAdminTreeChild> userAppAdminAreaChildren;

	/**
	 * Constructor for UserAppAdminAreas Class.
	 *
	 * @param parent {@link IAdminTreeChild}
	 */
	public UserAppAdminAreas(IAdminTreeChild parent) {
		super();
		this.parent = parent;
		this.userAppAdminAreaChildren = new LinkedHashMap<>();
	}

	
	/**
	 * Method for Adds the.
	 *
	 * @param userAppAdminAreasId {@link String}
	 * @param child {@link IAdminTreeChild}
	 * @return the i admin tree child {@link IAdminTreeChild}
	 */
	public IAdminTreeChild add(final String userAppAdminAreasId, final IAdminTreeChild child) {
		child.setParent(this);
		IAdminTreeChild returnVal = this.userAppAdminAreaChildren.put(userAppAdminAreasId, child);
		if (child instanceof RelationObj && ((RelationObj) child).getRefObject() instanceof AdministrationArea) {
			sort();
		}
		return returnVal;
	}

	/**
	 * Method for Removes the.
	 *
	 * @param userAppAdminAreasId {@link String}
	 * @return the i admin tree child {@link IAdminTreeChild}
	 */
	public IAdminTreeChild remove(final String userAppAdminAreasId) {
		return this.userAppAdminAreaChildren.remove(userAppAdminAreasId);
	}
	
	/**
	 * Method for Removes the all.
	 */
	public void removeAll() {
		this.userAppAdminAreaChildren.clear();
	}
	
	/**
	 * Gets the user app admin area collection.
	 *
	 * @return the user app admin area collection
	 */
	public Collection<IAdminTreeChild> getUserAppAdminAreaCollection() {
		return this.userAppAdminAreaChildren.values();
	}

	/**
	 * Gets the user app admin area children.
	 *
	 * @return the user app admin area children
	 */
	public Map<String, IAdminTreeChild> getUserAppAdminAreaChildren() {
		return userAppAdminAreaChildren;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild#
	 * getParent()
	 */
	@Override
	public IAdminTreeChild getParent() {
		return this.parent;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild#
	 * setParent(com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.
	 * IAdminTreeChild)
	 */
	@Override
	public void setParent(IAdminTreeChild parent) {
		this.parent = parent;

	}
	
	/**
	 * Sort.
	 */
	public void sort() {
		final LinkedHashMap<String, IAdminTreeChild> collect = this.userAppAdminAreaChildren.entrySet().stream().sorted(
				(e1, e2) -> ((AdministrationArea) e1.getValue()).getName().compareTo(((AdministrationArea) e2.getValue()).getName()))
				.collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e1, LinkedHashMap::new));
		this.userAppAdminAreaChildren = collect;
	}

}
