package com.magna.xmsystem.xmadmin.ui.parts.dndperspective.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.nebula.widgets.pgroup.PGroup;
import org.eclipse.swt.dnd.DropTargetAdapter;
import org.eclipse.swt.dnd.DropTargetEvent;
import org.eclipse.swt.widgets.Composite;

import com.magna.xmsystem.dependencies.utils.LANG_ENUM;
import com.magna.xmsystem.dependencies.utils.XMSystemUtil;
import com.magna.xmsystem.ui.controls.widgets.CustomMessageDialog;
import com.magna.xmsystem.xmadmin.ui.parts.dndperspective.ParkingLotBase;
import com.magna.xmsystem.xmadmin.ui.parts.dndperspective.model.AAWarpperObj;
import com.magna.xmsystem.xmadmin.ui.parts.dndperspective.model.ProjectWrapperObj;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.adminarea.AdministrationArea;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.projectapplicationmodel.ProjectApplication;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.startapplicationmodel.StartApplication;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.userapplicationmodel.UserApplication;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.projects.Project;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.refs.RelationObj;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.sitemodel.Site;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.sitemodel.SiteAdminAreaProjectChild;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.sitemodel.SiteAdministrationChild;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.users.User;
import com.magna.xmsystem.xmadmin.util.XMAdminUtil;

public class ParkingLotDropListener extends DropTargetAdapter {
	
	private ParkingLotBase parkinglot;

	public ParkingLotDropListener(ParkingLotBase parkingLotBase) {
		this.parkinglot = parkingLotBase;
	}
	
	@Override
	public void drop(final DropTargetEvent event) {
		if (event.data == null && !(event.data instanceof IStructuredSelection)) {
			return;
		}
		final List<Object> selectionElementList = new ArrayList<>();
		if (event.data instanceof IStructuredSelection) {
			StructuredSelection selections = (StructuredSelection) event.data;
			Object[] selectionArray = selections.toArray();
			for (Object selection : selectionArray) {
				selectionElementList.add(((StructuredSelection) selection).getFirstElement());
			}
			if (selectionElementList.isEmpty()) {
				CustomMessageDialog.openError(this.parkinglot.getDataComposite().getShell(), "Error", "No object is selected");
			}
		}
		for (final Object droppedObj : selectionElementList) {
			dropHandler(droppedObj);
		}
		/*if (event.data instanceof IStructuredSelection) {
			Object firstElement = ((IStructuredSelection) event.data).getFirstElement();

			if (firstElement instanceof TreeSelection) {
				final Object droppedObj = ((TreeSelection) firstElement).getFirstElement();
				dropHandler(droppedObj);
			} else if (firstElement instanceof IStructuredSelection) {
				final Object droppedObj = ((StructuredSelection) firstElement).getFirstElement();
				dropHandler(droppedObj);
			}
		}*/
		// Step 1: Get first element from the StructuredSelection
		// final Object droppedObj =
		// ((StructuredSelection)event.data).getFirstElement();
		// final Object droppedObj = ((StructuredSelection)
		// transfer.getSelection()).getFirstElement();
		/*
		 * if (droppedObj instanceof TreeSelection) { Object firstElement =
		 * ((TreeSelection) droppedObj).getFirstElement();
		 * dropHandler(firstElement); } else if (droppedObj instanceof Control)
		 * {
		 * 
		 * }
		 */

		// Step 2: Get that control's parent from which it's being
		// dragged
		// final IAdminTreeChild oldParent = droppedObj.getParent();

		// If we drag and drop on the same parent, do nothing
		/*
		 * if (oldParent == parent) return;
		 */

		// Step 3: Figure out what are we dropping
		// This may be done in the dropAccept implementation
		/*
		 * if (droppedObj instanceof Label) { final Label droppedLabel = (Label)
		 * droppedObj; droppedLabel.setParent(parent); // Change parent }
		 * 
		 * if (droppedObj instanceof Button) { final Button droppedButton =
		 * (Button) droppedObj; droppedButton.setParent(parent); // Change
		 * parent }
		 */

		// Step 4: Tell all parent that the layout has changed
		// This is not necessary, but it looks nicer.
		// oldParent.layout();
		// parent.layout();
		Composite dataComposite = this.parkinglot.getDataComposite(); 
		dataComposite.layout();
		dataComposite.getParent().layout();
	}
	
	/**
	 * @param droppedObj
	 */
	public void dropHandler(Object droppedObj) {
		if (droppedObj instanceof IAdminTreeChild) {
			final XMAdminUtil instance = XMAdminUtil.getInstance();
			final LANG_ENUM currentLocale = instance.getCurrentLocaleEnum();
			final LANG_ENUM defaultLocale = instance.getDefaultLocaleEnum();
			IAdminTreeChild element = (IAdminTreeChild) droppedObj;
			if (element.getClass() == Site.class) {
				Site siteObj = (Site) element;
				String typeName = Site.class.getSimpleName();
				addSiteObject(siteObj, typeName);
			} else if (element.getClass() == AdministrationArea.class) {
				AdministrationArea adminAreaObj = (AdministrationArea) element;
				String typeName = AdministrationArea.class.getSimpleName();
				addAdminAreaObject(adminAreaObj, typeName);
			} else if (element.getClass() == Project.class) {
				Project projectObj = (Project) element;
				String typeName = Project.class.getSimpleName();
				addProjectObj(projectObj, typeName);
			} else if (element.getClass() == User.class) {
				User userObj = (User) element;
				String typeName = User.class.getSimpleName();
				addUserObj(userObj, typeName);
			} else if (element.getClass() == ProjectApplication.class) {
				ProjectApplication projectAppObj = (ProjectApplication) element;
				String typeName = ProjectApplication.class.getSimpleName();
				addProjectApplicationObj(projectAppObj, typeName, currentLocale, defaultLocale);
			} else if (element.getClass() == UserApplication.class) {
				UserApplication userAppObj = (UserApplication) element;
				String typeName = UserApplication.class.getSimpleName();
				addUserApplicationObj(userAppObj, typeName, currentLocale, defaultLocale);
			} else if (element.getClass() == StartApplication.class) {
				StartApplication startAppObj = (StartApplication) element;
				String typeName = StartApplication.class.getSimpleName();
				addStartAppObj(startAppObj, typeName, currentLocale, defaultLocale);
			}/* else if (element.getClass() == BaseApplication.class) {
				BaseApplication baseApplicationObj = (BaseApplication) element;
				String typeName = BaseApplication.class.getSimpleName();
				addBaseApplicationObj(baseApplicationObj, typeName);
			}*/ else if (element.getClass() == RelationObj.class) {
				RelationObj relObj = (RelationObj) element;
				IAdminTreeChild refObject = relObj.getRefObject();
				IAdminTreeChild containerObj = relObj.getContainerObj();
				if (containerObj instanceof SiteAdministrationChild) {
					SiteAdministrationChild siteAdministrationChildObj = (SiteAdministrationChild) containerObj;
					String typeName = AdministrationArea.class.getSimpleName();
					addSiteAdminAreaObject((AdministrationArea) refObject, siteAdministrationChildObj, relObj, typeName);
				} else if (containerObj instanceof SiteAdminAreaProjectChild) {
					SiteAdminAreaProjectChild siteAdminAreaProjectChildObj = (SiteAdminAreaProjectChild) containerObj;
					String typeName = Project.class.getSimpleName();
					addSiteAdminAreaProjectChild((Project) refObject, siteAdminAreaProjectChildObj, relObj, typeName);
				}
			}
		} else if (droppedObj instanceof AAWarpperObj) {
			if (((AAWarpperObj) droppedObj).getRelationObj() != null) {
				dropHandler(((AAWarpperObj) droppedObj).getRelationObj());
			} else {
				dropHandler(((AAWarpperObj) droppedObj).getAdminArea());
			}
		} else if (droppedObj instanceof ProjectWrapperObj) {
			if (((ProjectWrapperObj) droppedObj).getRelationObj() != null) {
				dropHandler(((ProjectWrapperObj) droppedObj).getRelationObj());
			} else {
				dropHandler(((ProjectWrapperObj) droppedObj).getProject());
			}
		}
	}
	
	/**
	 * Adds the site object.
	 *
	 * @param siteObj the site obj
	 * @param typeName the type name
	 */
	private void addSiteObject(Site siteObj, String typeName) {
		boolean isFound = false; 
		PGroup pGroup = this.parkinglot.getGroupForObject(typeName);
		TableViewer groupTableViewer = this.parkinglot.getGroupTreeViewer(pGroup);
		List<Site> siteList = new ArrayList<>();
		Object input;
		if ((input = groupTableViewer.getInput()) != null) {
			if (input instanceof Site[]) {
				Site[] siteArray = (Site[]) input;
				siteList.addAll(Arrays.asList(siteArray));
				for (Site site : siteArray) {
					String siteId = site.getSiteId();
					if (siteId.equals(siteObj.getSiteId())) {
						isFound = true;
						break;
					}
				}
			}
		}
		if (!isFound) {
			siteList.add(siteObj);
		}
		Collections.sort(siteList, (Site s1, Site s2) -> s1.getName().compareTo(s2.getName()));
		groupTableViewer.setInput(siteList.toArray(new Site[siteList.size()]));
	}
	
	/**
	 * Adds the admin area object.
	 *
	 * @param adminArea the admin area
	 * @param typeName the type name
	 */
	private void addAdminAreaObject(final AdministrationArea adminArea, final String typeName) {
		boolean isFound = false; 
		final PGroup pGroup = this.parkinglot.getGroupForObject(typeName);
		final TableViewer groupTableViewer = this.parkinglot.getGroupTreeViewer(pGroup);
		final List<AAWarpperObj> aaList = new ArrayList<>();
		Object input;
		if ((input = groupTableViewer.getInput()) != null) {
			if (input instanceof AAWarpperObj[]) {
				AAWarpperObj[] aaWarpperObjArray = (AAWarpperObj[]) input;
				aaList.addAll(Arrays.asList(aaWarpperObjArray));
				for (AAWarpperObj aaWarpperObj : aaWarpperObjArray) {
					final String id = aaWarpperObj.getId();
					if (id.equals(adminArea.getAdministrationAreaId())) {
						isFound = true;
						break;
					}
				}
			}
		}
		if (!isFound) {
			aaList.add(new AAWarpperObj(adminArea));
		}
		Collections.sort(aaList, (AAWarpperObj aa1, AAWarpperObj aa2) -> aa1.getName().compareTo(aa2.getName()));
		groupTableViewer.setInput(aaList.toArray(new AAWarpperObj[aaList.size()]));
	}
	

	/**
	 * Adds the project obj.
	 *
	 * @param projectObj the project obj
	 * @param typeName the type name
	 */
	private void addProjectObj(Project projectObj, String typeName) {
		boolean isFound = false; 
		final PGroup pGroup = this.parkinglot.getGroupForObject(typeName);
		final TableViewer groupTableViewer = this.parkinglot.getGroupTreeViewer(pGroup);
		final List<ProjectWrapperObj> projectList = new ArrayList<>();
		Object input;
		if ((input = groupTableViewer.getInput()) != null) {
			if (input instanceof ProjectWrapperObj[]) {
				ProjectWrapperObj[] projectWrapperObjArray = (ProjectWrapperObj[]) input;
				projectList.addAll(Arrays.asList(projectWrapperObjArray));
				for (ProjectWrapperObj projectWrapperObj : projectWrapperObjArray) {
					final String id = projectWrapperObj.getId();
					if (id.equals(projectObj.getProjectId())) {
						isFound = true;
						break;
					}
				}
			}
		}
		if (!isFound) {
			projectList.add(new ProjectWrapperObj(projectObj));
		}
		Collections.sort(projectList, (ProjectWrapperObj p1, ProjectWrapperObj p2) -> p1.getName().compareTo(p2.getName()));
		groupTableViewer.setInput(projectList.toArray(new ProjectWrapperObj[projectList.size()]));
	}
	
	/**
	 * Adds the project application obj.
	 *
	 * @param projectAppObj the project app obj
	 * @param typeName the type name
	 * @param currentLocale the current locale
	 * @param defaultLocale the default locale
	 */
	private void addProjectApplicationObj(final ProjectApplication projectAppObj, final String typeName, final LANG_ENUM currentLocale, final LANG_ENUM defaultLocale) {
		boolean isFound = false; 
		PGroup pGroup = this.parkinglot.getGroupForObject(typeName);
		TableViewer groupTableViewer = this.parkinglot.getGroupTreeViewer(pGroup);
		List<ProjectApplication> projectApplicationList = new ArrayList<>();
		Object input;
		if ((input = groupTableViewer.getInput()) != null) {
			if (input instanceof ProjectApplication[]) {
				ProjectApplication[] projectAppArray = (ProjectApplication[]) input;
				projectApplicationList.addAll(Arrays.asList(projectAppArray));
				for (ProjectApplication projectApp : projectAppArray) {
					String projectAppId = projectApp.getProjectApplicationId();
					if (projectAppId.equals(projectAppObj.getProjectApplicationId())) {
						isFound = true;
						break;
					}
				}
			}
		}
		if (!isFound) {
			projectApplicationList.add(projectAppObj);
		}
		Collections.sort(projectApplicationList, (ProjectApplication proApp1, ProjectApplication proApp2) -> {
			String name1 = proApp1.getName(currentLocale);
			if (XMSystemUtil.isEmpty(name1)) {
				name1 = proApp1.getName(defaultLocale);
			}
			String name2 = proApp1.getName(currentLocale);
			if (XMSystemUtil.isEmpty(name2)) {
				name2 = proApp1.getName(defaultLocale);
			}
			if (!XMSystemUtil.isEmpty(name1) && !XMSystemUtil.isEmpty(name2)) {
				return name1.compareTo(name2);
			}
			return 0;
		});
		
		groupTableViewer.setInput(projectApplicationList.toArray(new ProjectApplication[projectApplicationList.size()]));
	}
	
	/**
	 * Adds the user application obj.
	 *
	 * @param userAppObj the user app obj
	 * @param typeName the type name
	 * @param currentLocale the current locale
	 * @param defaultLocale the default locale
	 */
	private void addUserApplicationObj(final UserApplication userAppObj, final String typeName, final LANG_ENUM currentLocale, final LANG_ENUM defaultLocale) {
		boolean isFound = false; 
		PGroup pGroup = this.parkinglot.getGroupForObject(typeName);
		TableViewer groupTableViewer = this.parkinglot.getGroupTreeViewer(pGroup);
		List<UserApplication> userApplicationList = new ArrayList<>();
		Object input;
		if ((input = groupTableViewer.getInput()) != null) {
			if (input instanceof UserApplication[]) {
				UserApplication[] userAppArray = (UserApplication[]) input;
				userApplicationList.addAll(Arrays.asList(userAppArray));
				for (UserApplication userApp : userAppArray) {
					String userAppId = userApp.getUserApplicationId();
					if (userAppId.equals(userAppObj.getUserApplicationId())) {
						isFound = true;
						break;
					}
				}
			}
		}
		if (!isFound) {
			userApplicationList.add(userAppObj);
		}
		Collections.sort(userApplicationList, (UserApplication userApp1, UserApplication userApp2) -> {
			String name1 = userApp1.getName(currentLocale);
			if (XMSystemUtil.isEmpty(name1)) {
				name1 = userApp1.getName(defaultLocale);
			}
			String name2 = userApp2.getName(currentLocale);
			if (XMSystemUtil.isEmpty(name2)) {
				name2 = userApp2.getName(defaultLocale);
			}
			if (!XMSystemUtil.isEmpty(name1) && !XMSystemUtil.isEmpty(name2)) {
				return name1.compareTo(name2);
			}
			return 0;
		});
		groupTableViewer.setInput(userApplicationList.toArray(new UserApplication[userApplicationList.size()]));
	}
	
	/*private void addBaseApplicationObj(BaseApplication baseApplicationObj, String typeName) {
		boolean addNewObject = true; 
		PGroup pGroup = this.parkinglot.getGroupForObject(typeName);
		TableViewer groupTableViewer = this.parkinglot.getGroupTreeViewer(pGroup);
		List<BaseApplication> baseApplicationList = new ArrayList<>();
		Object input;
		if ((input = groupTableViewer.getInput()) != null) {
			if (input instanceof BaseApplication[]) {
				BaseApplication[] baseAppArray = (BaseApplication[]) input;
				for (BaseApplication baseApp : baseAppArray) {
					baseApplicationList.add(baseApp);
					String baseAppId = baseApp.getBaseApplicationId();
					if (baseAppId.equals(baseApplicationObj.getBaseApplicationId())) {
						addNewObject = false;
					}
				}
			}
		}
		if (addNewObject) {
			baseApplicationList.add(baseApplicationObj);
		}
		groupTableViewer.setInput(baseApplicationList.toArray(new BaseApplication[baseApplicationList.size()]));
	}*/
	
	/**
	 * Adds the start app obj.
	 *
	 * @param startAppObj the start app obj
	 * @param typeName the type name
	 * @param currentLocale the current locale
	 * @param defaultLocale the default locale
	 */
	private void addStartAppObj(final StartApplication startAppObj, final String typeName, final LANG_ENUM currentLocale, final LANG_ENUM defaultLocale) {
		boolean isFound = false; 
		PGroup pGroup = this.parkinglot.getGroupForObject(typeName);
		TableViewer groupTableViewer = this.parkinglot.getGroupTreeViewer(pGroup);
		List<StartApplication> startApplicationList = new ArrayList<>();
		Object input;
		if ((input = groupTableViewer.getInput()) != null) {
			if (input instanceof StartApplication[]) {
				StartApplication[] startAppArray = (StartApplication[]) input;
				startApplicationList.addAll(Arrays.asList(startAppArray));
				for (StartApplication startApp : startAppArray) {
					String baseAppId = startApp.getStartPrgmApplicationId();
					if (baseAppId.equals(startAppObj.getStartPrgmApplicationId())) {
						isFound = true;
						break;
					}
				}
			}
		}
		if (!isFound) {
			startApplicationList.add(startAppObj);
		}
		Collections.sort(startApplicationList, (StartApplication startApp1, StartApplication startApp2) -> {
			final String name1 = startApp1.getName();
			final String name2 = startApp2.getName();
			if (!XMSystemUtil.isEmpty(name1) && !XMSystemUtil.isEmpty(name2)) {
				return name1.compareTo(name2);
			}
			return 0;
		});
		groupTableViewer.setInput(startApplicationList.toArray(new StartApplication[startApplicationList.size()]));
	}
	
	/**
	 * Adds the user obj.
	 *
	 * @param userObj the user obj
	 * @param typeName the type name
	 */
	private void addUserObj(User userObj, String typeName) {
		boolean addNewObject = true; 
		PGroup pGroup = this.parkinglot.getGroupForObject(typeName);
		TableViewer groupTableViewer = this.parkinglot.getGroupTreeViewer(pGroup);
		List<User> userList = new ArrayList<>();
		Object input;
		if ((input = groupTableViewer.getInput()) != null) {
			if (input instanceof User[]) {
				User[] userArray = (User[]) input;
				for (User user : userArray) {
					userList.add(user);
					String userId = user.getUserId();
					if (userId.equals(userObj.getUserId())) {
						addNewObject = false;
					}
				}
			}
		}
		if (addNewObject) {
			userList.add(userObj);
		}
		Collections.sort(userList, (User u1, User u2) -> u1.getName().compareTo(u2.getName()));
		groupTableViewer.setInput(userList.toArray(new User[userList.size()]));
	}
	
	/**
	 * Adds the site admin area object.
	 *
	 * @param refObject the ref object
	 * @param iAdminTreeChild the i admin tree child
	 * @param relObj the rel obj
	 * @param typeName the type name
	 */
	private void addSiteAdminAreaObject(final AdministrationArea refObject, final IAdminTreeChild iAdminTreeChild, 
			final RelationObj relObj, final String typeName) {
		boolean isFound = false; 
		final PGroup pGroup = this.parkinglot.getGroupForObject(typeName);
		final TableViewer groupTableViewer = this.parkinglot.getGroupTreeViewer(pGroup);
		final List<AAWarpperObj> aaList = new ArrayList<>();
		Object input;
		if ((input = groupTableViewer.getInput()) != null) {
			if (input instanceof AAWarpperObj[]) {
				final AAWarpperObj[] aaWarpperObjArray = (AAWarpperObj[]) input;
				for (AAWarpperObj aaWarpperObj : aaWarpperObjArray) {
					aaList.add(aaWarpperObj);
					final String id = aaWarpperObj.getId();
					if (id.equals(relObj.getRelId())) {
						isFound = true;
						break;
					}
				}
			}
		}
		if (!isFound) {
			final AAWarpperObj aaWarpperObj = new AAWarpperObj(refObject, iAdminTreeChild);
			aaWarpperObj.setRelationObj(relObj);
			aaList.add(aaWarpperObj);
		}
		Collections.sort(aaList, (AAWarpperObj aa1, AAWarpperObj aa2) -> aa1.getName().compareTo(aa2.getName()));
		groupTableViewer.setInput(aaList.toArray(new AAWarpperObj[aaList.size()]));
	}
	
	/**
	 * Adds the site admin area project child.
	 *
	 * @param refObject the ref object
	 * @param iAdminTreeChild the i admin tree child
	 * @param relObj the rel obj
	 * @param typeName the type name
	 */
	private void addSiteAdminAreaProjectChild(final Project refObject, final IAdminTreeChild iAdminTreeChild,
			final RelationObj relObj, final String typeName) {
		boolean isFound = false; 
		final PGroup pGroup = this.parkinglot.getGroupForObject(typeName);
		final TableViewer groupTableViewer = this.parkinglot.getGroupTreeViewer(pGroup);
		final List<ProjectWrapperObj> projectList = new ArrayList<>();
		Object input;
		if ((input = groupTableViewer.getInput()) != null) {
			if (input instanceof ProjectWrapperObj[]) {
				ProjectWrapperObj[] projectWrapperObjArray = (ProjectWrapperObj[]) input;
				for (ProjectWrapperObj projectWrapperObj : projectWrapperObjArray) {
					projectList.add(projectWrapperObj);
					String id = projectWrapperObj.getId();
					if (id.equals(relObj.getRelId())) {
						isFound = true;
						break;
					}
				}
			}
		}
		if (!isFound) {
			final ProjectWrapperObj projectWarpperObj = new ProjectWrapperObj(refObject, iAdminTreeChild);
			projectWarpperObj.setRelationObj(relObj);
			projectList.add(projectWarpperObj);
		}
		Collections.sort(projectList, (ProjectWrapperObj pw1, ProjectWrapperObj pw2) -> pw1.getName().compareTo(pw2.getName()));
		groupTableViewer.setInput(projectList.toArray(new ProjectWrapperObj[projectList.size()]));
	}
}
