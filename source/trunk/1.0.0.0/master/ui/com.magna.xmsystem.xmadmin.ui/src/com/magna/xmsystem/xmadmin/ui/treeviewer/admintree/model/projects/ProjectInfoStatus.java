package com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.projects;

import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild;

/**
 * The Class ProjectInfoStatus.
 */
public class ProjectInfoStatus implements IAdminTreeChild {

	/** The parent. */
	private IAdminTreeChild parent;

	/**
	 * Instantiates a new project info status.
	 *
	 * @param parent
	 *            the parent
	 */
	public ProjectInfoStatus(final IAdminTreeChild parent) {
		this.parent = parent;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild#
	 * getParent()
	 */
	@Override
	public IAdminTreeChild getParent() {
		// TODO Auto-generated method stub
		return this.parent;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild#
	 * setParent(com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.
	 * IAdminTreeChild)
	 */
	@Override
	public void setParent(IAdminTreeChild parent) {
		this.parent = parent;

	}

}
