package com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.adminarea;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;

import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild;

public class AdminAreaInfo implements IAdminTreeChild {

	/** The parent. */
	private IAdminTreeChild parent;
	
	/** The site admin information child. */
	final private Map<String, IAdminTreeChild> AdminAreaInfoChild;

	/**
	 * Instantiates a new site admin area informations.
	 */
	public AdminAreaInfo(final IAdminTreeChild parent) {
		this.parent = parent;
		this.AdminAreaInfoChild = new LinkedHashMap<>();
		this.AdminAreaInfoChild.put(AdminAreaInfoStatus.class.getName(), new AdminAreaInfoStatus(this));
		this.AdminAreaInfoChild.put(AdminAreaInfoHistory.class.getName(), new AdminAreaInfoHistory(this));
	}


	/**
	 * Gets the site admin information child collection.
	 *
	 * @return the site admin information child collection
	 */
	public Collection<IAdminTreeChild> getAdminAreaInfoCollection() {
		return this.AdminAreaInfoChild.values();
	}

	/**
	 * Gets the site admin information child children.
	 *
	 * @return the site admin information child children
	 */
	public Map<String, IAdminTreeChild> getAdminAreaInfoChild() {
		return AdminAreaInfoChild;
	}
	
	/*
	 * @see com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild#getParent()
	 */
	@Override
	public IAdminTreeChild getParent() {
		return this.parent;
	}

	/*
	 * @see com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild#
	 * setParent(com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild)
	 */
	@Override
	public void setParent(IAdminTreeChild parent) {
		this.parent = parent;
	}


}
