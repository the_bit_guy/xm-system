package com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.group.usergroup;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.stream.Collectors;

import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.refs.RelationObj;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.users.User;

/**
 * The Class UserGroupUsers.
 */
public class UserGroupUsers implements IAdminTreeChild {

	/** The parent. */
	private IAdminTreeChild parent;

	/** The Usergroup children. */
	private Map<String, IAdminTreeChild> userGroupChildren;

	/**
	 * Instantiates a new user group users.
	 *
	 * @param parent
	 *            the parent
	 */
	public UserGroupUsers(final IAdminTreeChild parent) {
		this.parent = parent;
		this.userGroupChildren = new LinkedHashMap<>();
	}

	/**
	 * Adds the.
	 *
	 * @param userGroupUsersChildrenId
	 *            the user group users children id
	 * @param child
	 *            the child
	 * @return the i admin tree child
	 */
	public IAdminTreeChild add(final String userGroupUsersChildrenId, final IAdminTreeChild child) {
		IAdminTreeChild returnVal = this.userGroupChildren.put(userGroupUsersChildrenId, child);
		if (child instanceof RelationObj && ((RelationObj) child).getRefObject() instanceof User) {
			sort();
		}
		return returnVal;
	}

	/**
	 * Removes the.
	 *
	 * @param userGroupUsersChildrenId
	 *            the user group users children id
	 * @return the i admin tree child
	 */
	public IAdminTreeChild remove(final String userGroupUsersChildrenId) {
		return this.userGroupChildren.remove(userGroupUsersChildrenId);

	}

	/**
	 * Gets the directory project apps children collection.
	 *
	 * @return the directory project apps children collection
	 */
	public Collection<IAdminTreeChild> getUserGroupuserAppsChildrenCollection() {
		return this.userGroupChildren.values();
	}

	/**
	 * Gets the directory project apps children children.
	 *
	 * @return the directory project apps children children
	 */
	public Map<String, IAdminTreeChild> getUserGroupUsersChildren() {
		return userGroupChildren;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild#
	 * getParent()
	 */
	@Override
	public IAdminTreeChild getParent() {
		return this.parent;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild#
	 * setParent(com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.
	 * IAdminTreeChild)
	 */
	@Override
	public void setParent(IAdminTreeChild parent) {
		this.parent = parent;

	}
	
	/**
	 * Sort.
	 */
	public void sort() {
		final LinkedHashMap<String, IAdminTreeChild> collect = this.userGroupChildren.entrySet().stream().sorted(
				(e1, e2) -> ((User) ((RelationObj) e1.getValue()).getRefObject()).getName().compareTo(((User) ((RelationObj) e2.getValue()).getRefObject()).getName()))
				.collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e1, LinkedHashMap::new));
		this.userGroupChildren = collect;
	}

}
