package com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.sitemodel;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.stream.Collectors;

import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.userapplicationmodel.UserApplication;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.refs.RelationObj;

/**
 * The Class SiteAdminAreaUserApplications.
 * 
 * @author subash.janarthanan
 * 
 */
public class SiteAdminAreaUserApplications implements IAdminTreeChild {
	
	/** The parent. */
	private IAdminTreeChild parent;
	
	/** The site admin area user applications child. */
	private Map<String, IAdminTreeChild> siteAdminAreaUserAppChild;

	/**
	 * Instantiates a new site admin area user applications.
	 * @param parent 
	 */
	public SiteAdminAreaUserApplications(IAdminTreeChild parent) {
		this.parent = parent;
		this.siteAdminAreaUserAppChild = new LinkedHashMap<>();
		addFixedChildren();
	}

	/**
	 * Add.
	 *
	 * @param siteAdminAreaUserApplicationsChildId
	 *            the site admin area user applications child id
	 * @param child
	 *            the child
	 * @return the i admin tree child
	 */
	public IAdminTreeChild add(final String siteAdminAreaUserAppChildId, final IAdminTreeChild child) {
		child.setParent(this);
		IAdminTreeChild returnVal = this.siteAdminAreaUserAppChild.put(siteAdminAreaUserAppChildId, child);
		if (child instanceof RelationObj && ((RelationObj) child).getRefObject() instanceof UserApplication) {
			sort();
		}
		return returnVal;
	}

	/**
	 * Remove.
	 *
	 * @param siteAdminAreaUserApplicationsChildId
	 *            the site admin area user applications child id
	 * @return the i admin tree child
	 */
	public IAdminTreeChild remove(final String siteAdminAreaUserAppChildId) {
		return this.siteAdminAreaUserAppChild.remove(siteAdminAreaUserAppChildId);
	}

	/**
	 * Method for Adds the fixed children.
	 */
	public void addFixedChildren() {
		siteAdminAreaUserAppChild.put(SiteAdminAreaUserAppNotFixed.class.getName(), new SiteAdminAreaUserAppNotFixed(
				this));
		siteAdminAreaUserAppChild.put(SiteAdminAreaUserAppFixed.class.getName(), new SiteAdminAreaUserAppFixed(
				this));
		siteAdminAreaUserAppChild.put(SiteAdminAreaUserAppProtected.class.getName(), new SiteAdminAreaUserAppProtected(
				this));
	}
	
	/**
	 * Gets the site admin area user app child collection.
	 *
	 * @return the site admin area user app child collection
	 */
	public Collection<IAdminTreeChild> getSiteAdminAreaUserAppCollection() {
		return this.siteAdminAreaUserAppChild.values();
	}

	/**
	 * Gets the site admin area user app children.
	 *
	 * @return the site admin area user app children
	 */
	public Map<String, IAdminTreeChild> getSiteAdminAreaUserAppChild() {
		return siteAdminAreaUserAppChild;
	}
	
	/* (non-Javadoc)
	 * @see com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild#getParent()
	 */
	@Override
	public IAdminTreeChild getParent() {
		return this.parent;
	}

	/* (non-Javadoc)
	 * @see com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild#setParent(com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild)
	 */
	@Override
	public void setParent(final IAdminTreeChild parent) {
		this.parent = parent;
	}
	
	/**
	 * Sort.
	 */
	public void sort() {
		final LinkedHashMap<String, IAdminTreeChild> collect = this.siteAdminAreaUserAppChild.entrySet().stream().sorted(
				(e1, e2) -> ((UserApplication) (((RelationObj) e1.getValue()).getRefObject())).getName()
				.compareTo(((UserApplication) (((RelationObj) e2.getValue()).getRefObject())).getName()))
				.collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e1, LinkedHashMap::new));
		this.siteAdminAreaUserAppChild = collect;
	}

}
