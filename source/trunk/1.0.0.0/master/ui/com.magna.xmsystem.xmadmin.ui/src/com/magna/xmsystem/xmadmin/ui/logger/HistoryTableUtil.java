package com.magna.xmsystem.xmadmin.ui.logger;

import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;

import org.eclipse.e4.core.services.nls.Translation;
import org.eclipse.nebula.widgets.nattable.NatTable;
import org.eclipse.nebula.widgets.nattable.config.CellConfigAttributes;
import org.eclipse.nebula.widgets.nattable.config.DefaultNatTableStyleConfiguration;
import org.eclipse.nebula.widgets.nattable.config.IConfigRegistry;
import org.eclipse.nebula.widgets.nattable.data.ListDataProvider;
import org.eclipse.nebula.widgets.nattable.grid.layer.config.DefaultRowStyleConfiguration;
import org.eclipse.nebula.widgets.nattable.painter.cell.TextPainter;
import org.eclipse.nebula.widgets.nattable.painter.cell.decorator.PaddingDecorator;
import org.eclipse.nebula.widgets.nattable.selection.config.DefaultSelectionStyleConfiguration;
import org.eclipse.nebula.widgets.nattable.style.CellStyleAttributes;
import org.eclipse.nebula.widgets.nattable.style.DisplayMode;
import org.eclipse.nebula.widgets.nattable.style.HorizontalAlignmentEnum;
import org.eclipse.nebula.widgets.nattable.style.Style;
import org.eclipse.nebula.widgets.nattable.style.VerticalAlignmentEnum;
import org.eclipse.nebula.widgets.nattable.ui.menu.HeaderMenuConfiguration;
import org.eclipse.nebula.widgets.nattable.ui.menu.PopupMenuBuilder;
import org.eclipse.nebula.widgets.nattable.util.GUIHelper;

import com.magna.xmsystem.xmadmin.message.Message;
import com.magna.xmsystem.xmadmin.ui.logger.conf.HistoryNatTableToolTip;
import com.magna.xmsystem.xmadmin.ui.logger.conf.StyledColumnHeaderConfiguration;
import com.magna.xmsystem.xmadmin.ui.logger.conf.StyledRowHeaderConfiguration;

/**
 * The Class HistoryTableUtil.
 */
public class HistoryTableUtil {
	
	/** Member variable 'this ref' for {@link HistoryTableUtil}. */
	private static HistoryTableUtil thisRef;

	/** The user history prop to label map. */
	private  Map<String, String> userHistoryPropToLabelMap;

	/** The admin history base object prop to label map. */
	private  Map<String, String> adminHistoryBaseObjectPropToLabelMap;

	/** The admin history relations prop to label map. */
	private  Map<String, String> adminHistoryRelationsPropToLabelMap;
	
	/** Member variable 'messages' for {@link Message}. */
	@Inject
	@Translation
	private Message message;
	
	/**
	 * Constructor for HistoryTableUtil Class.
	 */
	public HistoryTableUtil() {
		setInstance(this);
	}
	
	/**
	 * Gets the single instance of HistoryTableUtil.
	 *
	 * @return single instance of HistoryTableUtil
	 */
	public static HistoryTableUtil getInstance() {
		return thisRef;
	}

	/**
	 * Sets the instance.
	 *
	 * @param thisRef
	 *            the new instance
	 */
	public static void setInstance(HistoryTableUtil thisRef) {
		HistoryTableUtil.thisRef = thisRef;
	}
	/**
	 * Register an attribute to be applied to all cells with the highlight
	 * label. A similar approach can be used to bind styling to an arbitrary
	 * group of cells
	 */
	public void addColumnHighlight(IConfigRegistry configRegistry) {
		Style style = new Style();
		style.setAttributeValue(CellStyleAttributes.FOREGROUND_COLOR, GUIHelper.COLOR_BLUE);
		style.setAttributeValue(CellStyleAttributes.HORIZONTAL_ALIGNMENT, HorizontalAlignmentEnum.RIGHT);

		configRegistry.registerConfigAttribute(CellConfigAttributes.CELL_STYLE, // attribute
																				// to
																				// apply
				style, // value of the attribute
				DisplayMode.NORMAL, // apply during normal rendering i.e not
									// during selection or edit
				"BodyLabel_1"); // apply the above for all cells with this label
	}

	/**
	 * Add custom styling.
	 *
	 * @param natTable the nat table
	 * @param dataProvider the data provider
	 */
	@SuppressWarnings({ "unused", "rawtypes" })
	public void addCustomStyling(NatTable natTable, ListDataProvider dataProvider) {
		// Setup NatTable default styling
		HistoryNatTableToolTip tooltip = new HistoryNatTableToolTip(natTable, dataProvider);

		// NOTE: Getting the colors and fonts from the GUIHelper ensures that
		// they are disposed properly (required by SWT)
		DefaultNatTableStyleConfiguration natTableConfiguration = new DefaultNatTableStyleConfiguration();
		natTableConfiguration.bgColor = GUIHelper.getColor(249, 172, 7);
		natTableConfiguration.fgColor = GUIHelper.getColor(30, 76, 19);
		natTableConfiguration.hAlign = HorizontalAlignmentEnum.LEFT;
		natTableConfiguration.vAlign = VerticalAlignmentEnum.TOP;

		// A custom painter can be plugged in to paint the cells differently
		natTableConfiguration.cellPainter = new PaddingDecorator(new TextPainter(), 1);

		// Setup even odd row colors - row colors override the NatTable default
		// colors
		DefaultRowStyleConfiguration rowStyleConfiguration = new DefaultRowStyleConfiguration();
		rowStyleConfiguration.oddRowBgColor = GUIHelper.getColor(254, 251, 243);
		rowStyleConfiguration.evenRowBgColor = GUIHelper.COLOR_WHITE;

		// Setup selection styling
		DefaultSelectionStyleConfiguration selectionStyle = new DefaultSelectionStyleConfiguration();
		//selectionStyle.selectionFont = GUIHelper.getFont(new FontData("Verdana", 8, SWT.NORMAL));
		//selectionStyle.selectionBgColor = GUIHelper.getColor(217, 232, 251);
		//selectionStyle.selectionFgColor = GUIHelper.COLOR_BLACK;
		//selectionStyle.anchorBorderStyle = new BorderStyle(1, GUIHelper.COLOR_DARK_GRAY, LineStyleEnum.SOLID);
		selectionStyle.anchorBgColor = GUIHelper.getColor(107, 139, 171);
		//selectionStyle.selectedHeaderBgColor = GUIHelper.getColor(156, 209, 103);

		// Add all style configurations to NatTable
		natTable.addConfiguration(natTableConfiguration);
		natTable.addConfiguration(rowStyleConfiguration);
		natTable.addConfiguration(selectionStyle);

		// Column/Row header style and custom painters
		natTable.addConfiguration(new StyledRowHeaderConfiguration());
		natTable.addConfiguration(new StyledColumnHeaderConfiguration());

		// Add popup menu - build your own popup menu using the PopupMenuBuilder
		natTable.addConfiguration(new HeaderMenuConfiguration(natTable) {
			@Override
			protected PopupMenuBuilder createColumnHeaderMenu(NatTable natTable) {
				return new PopupMenuBuilder(natTable).withHideColumnMenuItem().withShowAllColumnsMenuItem()
						.withAutoResizeSelectedColumnsMenuItem().withClearAllFilters();
			}
		});
	}

	/**
	 * Table Property Names
	 */
	private  String[] userHistoryPropNames = new String[] { "id", "logTime", "userName", "host", "site",
			"adminArea", "project", "application", "pid", "result", "args" };

	private  String[] adminHistoryRelationsPropName = new String[] { "id", "logTime", "adminName",
			"apiRequestPath", "operation", "adminArea", "project", "projectApplication", "userName", "startApplication",
			"relationType", "status", "site", "groupName", "userApplication", "directory", "role", "errorMessage",
			"result" };

	private  String[] adminHistoryBaseObjectPropNames = new String[] { "id", "logTime", "adminName",
			"apiRequestPath", "operation", "objectName", "changes", "errorMessage", "result" };

	public  String[] getUserHistoryPropNames() {
		return userHistoryPropNames;
	}

	public  String[] getAdminHistoryRelationsPropName() {
		return adminHistoryRelationsPropName;
	}

	public  String[] getAdminHistoryBaseObjectPropNames() {
		return adminHistoryBaseObjectPropNames;
	}

	/**
	 * End : Table Property Names
	 */
	public  Map<String, String> getUserHistoryPropToLabelMap() {
		if (userHistoryPropToLabelMap == null) {
			userHistoryPropToLabelMap = new HashMap<>();
		}
		userHistoryPropToLabelMap.put("id", message.historyIdLabel.toUpperCase());
		userHistoryPropToLabelMap.put("logTime", message.historyLogTimeLabel.toUpperCase());
		userHistoryPropToLabelMap.put("userName",  message.userHistoryUserNameLbl.toUpperCase());
		userHistoryPropToLabelMap.put("host",  message.userHistoryHostLbl.toUpperCase());
		userHistoryPropToLabelMap.put("site",  message.userHistorySiteLbl.toUpperCase());
		userHistoryPropToLabelMap.put("adminArea",  message.userHistoryAdminAreaLbl.toUpperCase());
		userHistoryPropToLabelMap.put("project",  message.userHistoryProjectLbl.toUpperCase());
		userHistoryPropToLabelMap.put("application",  message.userHistoryApplicationLbl.toUpperCase());
		userHistoryPropToLabelMap.put("pid",  message.userHistoryPIDLbl.toUpperCase());
		userHistoryPropToLabelMap.put("result",  message.userHistoryResultLbl.toUpperCase());
		userHistoryPropToLabelMap.put("args", message.userHistoryAgrsLbl.toUpperCase());
		return userHistoryPropToLabelMap;
	}

	public  Map<String, String> getAdminHistoryBaseObjectPropToLabelMap() {
		if (adminHistoryBaseObjectPropToLabelMap == null) {
			adminHistoryBaseObjectPropToLabelMap = new HashMap<>();
		}
		adminHistoryBaseObjectPropToLabelMap.put("id", message.historyIdLabel.toUpperCase());
		adminHistoryBaseObjectPropToLabelMap.put("logTime",message.historyLogTimeLabel.toUpperCase());
		adminHistoryBaseObjectPropToLabelMap.put("adminName",message.adminHistoryAdminName.toUpperCase());
		adminHistoryBaseObjectPropToLabelMap.put("apiRequestPath", message.adminHistoryApiPathLbl.toUpperCase());
		adminHistoryBaseObjectPropToLabelMap.put("operation", message.adminHistoryOperation.toUpperCase());
		adminHistoryBaseObjectPropToLabelMap.put("objectName", message.adminHistoryObjectName.toUpperCase());
		adminHistoryBaseObjectPropToLabelMap.put("changes", message.adminHistoryChanges.toUpperCase());
		adminHistoryBaseObjectPropToLabelMap.put("errorMessage", message.adminHistoryErrorMessage.toUpperCase());
		adminHistoryBaseObjectPropToLabelMap.put("result", message.adminHistoryResult.toUpperCase());
		return adminHistoryBaseObjectPropToLabelMap;
	}

	public  Map<String, String> getAdminHistoryRelationsPropToLabelMap() {
		if (adminHistoryRelationsPropToLabelMap == null) {
			adminHistoryRelationsPropToLabelMap = new HashMap<>();
		}
		adminHistoryRelationsPropToLabelMap.put("id", message.historyIdLabel.toUpperCase());
		adminHistoryRelationsPropToLabelMap.put("logTime", message.historyLogTimeLabel.toUpperCase());
		adminHistoryRelationsPropToLabelMap.put("adminName",message.adminHistoryAdminName.toUpperCase());
		adminHistoryRelationsPropToLabelMap.put("apiRequestPath", message.adminHistoryApiPathLbl.toUpperCase());
		adminHistoryRelationsPropToLabelMap.put("operation",  message.adminHistoryOperation.toUpperCase());
		adminHistoryRelationsPropToLabelMap.put("adminArea", message.adminHistoryAdminArea.toUpperCase());
		adminHistoryRelationsPropToLabelMap.put("project", message.adminHistoryProject.toUpperCase());
		adminHistoryRelationsPropToLabelMap.put("projectApplication", message.adminHistoryProjectApp.toUpperCase());
		adminHistoryRelationsPropToLabelMap.put("userName", message.adminHistoryUserName.toUpperCase());
		adminHistoryRelationsPropToLabelMap.put("startApplication",message.adminHistoryStartApplication.toUpperCase());
		adminHistoryRelationsPropToLabelMap.put("relationType",message.adminHistoryRelationType.toUpperCase());
		adminHistoryRelationsPropToLabelMap.put("status", message.adminHistoryStatus.toUpperCase());
		adminHistoryRelationsPropToLabelMap.put("site",message.adminHistorySite.toUpperCase());
		adminHistoryRelationsPropToLabelMap.put("groupName", message.adminHistoryGroupName.toUpperCase());
		adminHistoryRelationsPropToLabelMap.put("userApplication", message.adminHistoryUserApplication.toUpperCase());
		adminHistoryRelationsPropToLabelMap.put("directory", message.adminHistoryDirectory.toUpperCase());
		adminHistoryRelationsPropToLabelMap.put("role",message.adminHistoryRole.toUpperCase());
		adminHistoryRelationsPropToLabelMap.put("errorMessage", message.adminHistoryErrorMessage.toUpperCase());
		adminHistoryRelationsPropToLabelMap.put("result", message.adminHistoryResult.toUpperCase());
		return adminHistoryRelationsPropToLabelMap;
	}

}
