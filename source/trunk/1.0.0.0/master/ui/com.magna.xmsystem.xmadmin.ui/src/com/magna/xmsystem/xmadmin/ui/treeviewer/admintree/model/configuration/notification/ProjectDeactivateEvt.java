package com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.notification;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.stream.Collectors;

import com.magna.xmbackend.vo.enums.NotificationEventType;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild;

// TODO: Auto-generated Javadoc
/**
 * The Class ProjectDeactivateEvt.
 */
public class ProjectDeactivateEvt extends INotificationEvent {
	
	/** The project deactivate evt child. */
	private Map<String, IAdminTreeChild> projectDeactivateEvtChild;
	
	/** The parent. */
	private IAdminTreeChild parent;
	
	
	/**
	 * Instantiates a new project deactivate evt.
	 */
	public ProjectDeactivateEvt() {
		this.parent = null;
		this.eventType = NotificationEventType.PROJECT_DEACTIVATE;
		this.projectDeactivateEvtChild = new LinkedHashMap<>();
	}
	
	
	/**
	 * Add.
	 *
	 * @param id the id
	 * @param child the child
	 * @return the i admin tree child
	 */
	public IAdminTreeChild add(final String id, final IAdminTreeChild child) {
		child.setParent(this);
		IAdminTreeChild returnVal = this.projectDeactivateEvtChild.put(id, child);
		sort();
		return returnVal;
	}
	
	/**
	 * Remove all.
	 */
	public void removeAll() {
		this.projectDeactivateEvtChild.clear();
	}

	/* (non-Javadoc)
	 * @see com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild#getParent()
	 */
	@Override
	public IAdminTreeChild getParent() {
		return this.parent;
	}

	/* (non-Javadoc)
	 * @see com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild#setParent(com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild)
	 */
	@Override
	public void setParent(IAdminTreeChild parent) {
		this.parent = parent;
	}
	
	
	
	/**
	 * Gets the project deactivate evt child.
	 *
	 * @return the project deactivate evt child
	 */
	public Map<String, IAdminTreeChild> getProjectDeactivateEvtChild() {
		return projectDeactivateEvtChild;
	}


	/**
	 * Gets the project deactivate evt collection.
	 *
	 * @return the project deactivate evt collection
	 */
	public Collection<IAdminTreeChild> getProjectDeactivateEvtCollection() {
		return this.projectDeactivateEvtChild.values();
	}
	
	/**
	 * Sort.
	 */
	public void sort() {
		final LinkedHashMap<String, IAdminTreeChild> collect = this.projectDeactivateEvtChild.entrySet().stream().sorted(
				(e1, e2) -> ((ProjectDeactivateEvtAction) e1.getValue()).getName().compareTo(((ProjectDeactivateEvtAction) e2.getValue()).getName()))
				.collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e1, LinkedHashMap::new));
		this.projectDeactivateEvtChild = collect;
	}
}
