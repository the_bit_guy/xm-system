package com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.notification;

import java.beans.PropertyChangeEvent;
import java.util.Set;
import java.util.TreeSet;

import com.magna.xmsystem.dependencies.utils.XMSystemUtil;
import com.magna.xmsystem.xmadmin.util.CommonConstants;

/**
 * The Class ProjectCreateEvtAction.
 */
public class ProjectCreateEvtAction extends NotificationEvtActions {
	
	/**
	 * Instantiates a new project create evt action.
	 *
	 * @param id
	 *            the id
	 * @param name
	 *            the name
	 * @param isActive
	 *            the is active
	 * @param operationMode
	 *            the operation mode
	 */
	public ProjectCreateEvtAction(final String id, final String name, final boolean isActive, final int operationMode) {
		super(id, name, isActive, operationMode);
	}
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.BeanModel#
	 * propertyChange(java.beans.PropertyChangeEvent)
	 */
	@Override
	public void propertyChange(PropertyChangeEvent event) {
		this.propertyChangeSupport.firePropertyChange(event.getPropertyName(), event.getOldValue(),
				event.getNewValue());

	}

	/**
	 * Deep copy pro create evt action.
	 *
	 * @param update
	 *            the update
	 * @param updateThisObject
	 *            the update this object
	 * @return the project create evt action
	 */
	public ProjectCreateEvtAction deepCopyProCreateEvtAction(boolean update, ProjectCreateEvtAction updateThisObject) {
		ProjectCreateEvtAction clonedProjectCreateEvtAction = null;

		String currentId = XMSystemUtil.isEmpty(this.getId()) ? CommonConstants.EMPTY_STR : this.getId();
		String currentName = XMSystemUtil.isEmpty(this.getName()) ? CommonConstants.EMPTY_STR : this.getName();
		boolean currentIsActive = this.isActive();
		String currentDescription = XMSystemUtil.isEmpty(this.getDescription()) ? CommonConstants.EMPTY_STR
				: this.getDescription();
		Set<String> currentToUserList = this.getToUsers();
		Set<String> currentCCUserList = this.getCcUsers();
		
		NotificationTemplate currentTemplate = new NotificationTemplate();
		currentTemplate.setTemplateId(this.getTemplate().getTemplateId());
		currentTemplate.setName(this.getTemplate().getName());
		currentTemplate.setSubject(this.getTemplate().getSubject());
		currentTemplate.setMessage(this.getTemplate().getMessage());
		TreeSet<String> currentVariables;
		if (this.getTemplate().getVariables() != null) {
			currentVariables = new TreeSet<>(this.getTemplate().getVariables());
		}else{
			currentVariables = new TreeSet<>();
		}
		currentTemplate.setVariables(currentVariables);

		if (update) {
			clonedProjectCreateEvtAction = updateThisObject;
		} else {
			clonedProjectCreateEvtAction = new ProjectCreateEvtAction(currentId, currentName, currentIsActive,
					CommonConstants.OPERATIONMODE.VIEW);
		}
		clonedProjectCreateEvtAction.setId(currentId);
		clonedProjectCreateEvtAction.setName(currentName);
		clonedProjectCreateEvtAction.setActive(currentIsActive);
		clonedProjectCreateEvtAction.setDescription(currentDescription);
		clonedProjectCreateEvtAction.setToUsers(currentToUserList);
		clonedProjectCreateEvtAction.setCcUsers(currentCCUserList);
		clonedProjectCreateEvtAction.setTemplate(currentTemplate);

		return clonedProjectCreateEvtAction;
	}

}
