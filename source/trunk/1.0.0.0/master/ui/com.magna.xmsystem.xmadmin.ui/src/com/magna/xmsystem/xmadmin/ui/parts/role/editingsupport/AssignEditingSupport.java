package com.magna.xmsystem.xmadmin.ui.parts.role.editingsupport;

import com.magna.xmbackend.vo.enums.CreationType;
import com.magna.xmbackend.vo.roles.ObjectPermission;
import com.magna.xmsystem.xmadmin.ui.parts.role.ObjRelationPermissionsTable;
import com.magna.xmsystem.xmadmin.ui.parts.role.ObjectPermissionsTable;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.roles.ObjectRelationType;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.roles.VoPermContainer;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.roles.objRelationPermissions.ObjectRelationPermissions;
import com.magna.xmsystem.xmadmin.util.XMAdminUtil;

public class AssignEditingSupport extends CheckBoxEditingSupport {
	
	/** Member variable 'instance' for {@link XMAdminUtil}. */
	private XMAdminUtil instance;
	
	/** Member variable 'viewer' for {@link ObjectPermissionsTable}. */
	private ObjRelationPermissionsTable viewer;

	public AssignEditingSupport(final ObjRelationPermissionsTable viewer) {
		super(viewer);
		this.viewer = viewer;
		this.instance = XMAdminUtil.getInstance();
	}

	@Override
	protected Object getValue(Object element) {
		if (element instanceof ObjectRelationPermissions) {
			return ((ObjectRelationPermissions) element).isAssign();
		}
		return "N";
	}

	@Override
	protected void setValue(Object element, Object value) {
		if (element instanceof ObjectRelationPermissions) {
			ObjectRelationPermissions elem = (ObjectRelationPermissions) element;
			ObjectRelationType objectRelationType = ObjectRelationType.getObjectType(elem.getPermissionName());
			String permissionId = this.instance.getObjectRelPermissionMap().get(objectRelationType).getAssignPermission().getPermissionId();
			VoPermContainer voPermContainer = new VoPermContainer();
			ObjectPermission objectPermission = new ObjectPermission();
			objectPermission.setPermissionId(permissionId);
			voPermContainer.setObjectPermission(objectPermission);
			if (elem.isAssign() && ! (boolean) value) {
				objectPermission.setCreationType(CreationType.DELETE);
				elem.setAssignPermission(voPermContainer);
			} else if (!elem.isAssign() && (boolean) value) {
				objectPermission.setCreationType(CreationType.ADD);
				elem.setAssignPermission(voPermContainer);
			} else {
				return;
			}
			elem.setAssign((boolean) value);
			this.viewer.getCachedObjPermissionMap().put(permissionId, voPermContainer);
			getViewer().refresh(element);
		}
	}
	
	/* (non-Javadoc)
	 * @see com.magna.xmsystem.xmadmin.ui.parts.role.editingsupport.CheckBoxEditingSupport#canEdit(java.lang.Object)
	 */
	@Override
	protected boolean canEdit(Object element) {
		if (element instanceof ObjectRelationPermissions) {
			ObjectRelationPermissions objectRelationPermissions = (ObjectRelationPermissions) element;
			if (objectRelationPermissions.isAssign() && objectRelationPermissions.isInactiveAssignment()) {
				return false;
			}
		}
		return true;
	}
}
