package com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.roles;

import java.util.LinkedHashMap;
import java.util.Map;

import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.refs.RelationObj;

/**
 * Class for Role scope object child.
 *
 * @author Rohsan.Ekka
 */
public class RoleScopeObjectChild implements IAdminTreeChild {

	/** The parent. */
	private IAdminTreeChild parent;
	/** The role child. */
	private Map<String, IAdminTreeChild> roleScopeObjectChildren;
	
	/** The name. */
	private String name;

	/**
	 * Constructor for RoleScopeObjectChild Class.
	 *
	 * @param id
	 *            {@link String}
	 * @param refObject
	 *            {@link IAdminTreeChild}
	 * @param parent
	 *            {@link IAdminTreeChild}
	 */
	public RoleScopeObjectChild(IAdminTreeChild parent) {
		this.parent = parent;
		this.roleScopeObjectChildren = new LinkedHashMap<>();
	}

	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Sets the name.
	 *
	 * @param name
	 *            the new name
	 */
	public void setName(String name) {
		this.name = name;
	}
	
	
	/**
	 * Method for Adds the.
	 *
	 * @param roleScopeObjectChildrenId {@link String}
	 * @param child {@link IAdminTreeChild}
	 * @return the i admin tree child {@link IAdminTreeChild}
	 */
	public IAdminTreeChild add(final String roleScopeObjectChildrenId, final IAdminTreeChild child) {
		child.setParent(this);
		return this.roleScopeObjectChildren.put(roleScopeObjectChildrenId, child);
	}
	
	/**
	 * Method for Adds the fixed children.
	 */
	public void addFixedChildren(RelationObj roleScopeObjectChildRel) {
		roleScopeObjectChildren.put(RoleScopeObjectUsers.class.getName(), new RoleScopeObjectUsers(roleScopeObjectChildRel));
	}
	/**
	 * Method for Removes the.
	 *
	 * @param siteAdminAreaChildId {@link String}
	 * @return the i admin tree child {@link IAdminTreeChild}
	 */
	public IAdminTreeChild remove(final String siteAdminAreaChildId) {
		return this.roleScopeObjectChildren.remove(siteAdminAreaChildId);
	}
	
	/**
	 * Method for Removes the all.
	 */
	public void removeAll() {
		this.roleScopeObjectChildren.clear();
	}
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild#
	 * getParent()
	 */
	@Override
	public IAdminTreeChild getParent() {
		return this.parent;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild#
	 * setParent(com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.
	 * IAdminTreeChild)
	 */
	@Override
	public void setParent(IAdminTreeChild parent) {
		this.parent = parent;
	}
	
	/**
	 * Gets the role scope object children.
	 *
	 * @return the role scope object children
	 */
	public Map<String, IAdminTreeChild> getRoleScopeObjectChildren() {
		return roleScopeObjectChildren;
	}

	/**
	 * Method for Sets the role scope object children.
	 *
	 * @param scopeObjectUsers
	 *            {@link Map<String,IAdminTreeChild>}
	 */
	public void setRoleScopeObjectChildren(Map<String, IAdminTreeChild> scopeObjectUsers) {
		this.roleScopeObjectChildren = scopeObjectUsers;
	}
}
