package com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.projects;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;

import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.refs.RelationObj;

/**
 * Class for User admin area child.
 *
 * @author Chiranjeevi.Akula
 */
public class ProjectUserAdminAreaChild implements IAdminTreeChild {

	/** Member variable 'parent' for {@link IAdminTreeChild}. */
	private IAdminTreeChild parent;

	/**
	 * Member variable 'user admin area children' for
	 * {@link Map<String,IAdminTreeChild>}.
	 */
	private Map<String, IAdminTreeChild> projectUserAdminAreaChildren;

	/**
	 * Constructor for UserAdminAreaChild Class.
	 *
	 * @param parent
	 *            {@link IAdminTreeChild}
	 */
	public ProjectUserAdminAreaChild(final IAdminTreeChild parent) {
		this.parent = parent;
		this.projectUserAdminAreaChildren = new LinkedHashMap<>();
	}

	/**
	 * Method for Adds the fixed children.
	 *
	 * @param relationObj
	 *            {@link RelationObj}
	 */
	public void addFixedChildren(RelationObj relationObj) {
		this.projectUserAdminAreaChildren.put(ProjectUserAAProjectApplications.class.getName(),
				new ProjectUserAAProjectApplications(relationObj));
	}

	/**
	 * Gets the user admin area children collection.
	 *
	 * @return the user admin area children collection
	 */
	public Collection<IAdminTreeChild> getProjectUserAdminAreaChildrenCollection() {
		return this.projectUserAdminAreaChildren.values();
	}

	/**
	 * Gets the user admin area children children.
	 *
	 * @return the user admin area children children
	 */
	public Map<String, IAdminTreeChild> getProjectUserAdminAreaChildren() {
		return projectUserAdminAreaChildren;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild#
	 * getParent()
	 */
	@Override
	public IAdminTreeChild getParent() {
		return this.parent;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild#
	 * setParent(com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.
	 * IAdminTreeChild)
	 */
	@Override
	public void setParent(final IAdminTreeChild parent) {
		this.parent = parent;
	}

}
