package com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.group.userappgroup;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.stream.Collectors;

import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.userapplicationmodel.UserApplication;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.refs.RelationObj;

/**
 * The Class UserAppGroupUserApps.
 * 
 * @author archita.patel
 */
public class UserAppGroupUserApps implements IAdminTreeChild {

	/** The parent. */
	private IAdminTreeChild parent;

	/** The user app group children. */
	private Map<String, IAdminTreeChild> userAppGroupChildren;

	/**
	 * Instantiates a new user app group user apps.
	 *
	 * @param parent
	 *            the parent
	 */
	public UserAppGroupUserApps(final IAdminTreeChild parent) {
		this.parent = parent;
		this.userAppGroupChildren = new LinkedHashMap<>();
	}

	/**
	 * Adds the.
	 *
	 * @param userAppGroupChildId
	 *            the user app group child id
	 * @param child
	 *            the child
	 * @return the i admin tree child
	 */
	public IAdminTreeChild add(final String userAppGroupChildId, final IAdminTreeChild child) {
		IAdminTreeChild returnVal = this.userAppGroupChildren.put(userAppGroupChildId, child);
		if (child instanceof RelationObj && ((RelationObj) child).getRefObject() instanceof UserApplication) {
			sort();
		}
		return returnVal;
	}

	/**
	 * Removes the.
	 *
	 * @param userAppGroupChildId
	 *            the user app group child id
	 * @return the i admin tree child
	 */
	public IAdminTreeChild remove(final String userAppGroupChildId) {
		return this.userAppGroupChildren.remove(userAppGroupChildId);

	}

	/**
	 * Gets the user app group children collection.
	 *
	 * @return the user app group children collection
	 */
	public Collection<IAdminTreeChild> getUserAppGroupChildrenCollection() {
		return this.userAppGroupChildren.values();
	}

	/**
	 * Gets the user app group children.
	 *
	 * @return the user app group children
	 */
	public Map<String, IAdminTreeChild> getUserAppGroupChildren() {
		return userAppGroupChildren;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild#
	 * getParent()
	 */
	@Override
	public IAdminTreeChild getParent() {
		return this.parent;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild#
	 * setParent(com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.
	 * IAdminTreeChild)
	 */
	@Override
	public void setParent(IAdminTreeChild parent) {
		this.parent = parent;

	}
	
	/**
	 * Sort.
	 */
	public void sort() {
		final LinkedHashMap<String, IAdminTreeChild> collect = this.userAppGroupChildren.entrySet().stream().sorted(
				(e1, e2) -> ((UserApplication) ((RelationObj) e1.getValue()).getRefObject()).getName().compareTo(((UserApplication) ((RelationObj) e1.getValue()).getRefObject()).getName()))
				.collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e1, LinkedHashMap::new));
		this.userAppGroupChildren = collect;
	}

}
