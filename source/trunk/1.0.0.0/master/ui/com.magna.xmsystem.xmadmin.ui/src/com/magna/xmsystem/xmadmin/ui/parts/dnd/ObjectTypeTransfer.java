package com.magna.xmsystem.xmadmin.ui.parts.dnd;

import org.eclipse.swt.dnd.ByteArrayTransfer;
import org.eclipse.swt.dnd.TransferData;

/**
 * The Class ObjectTypeTransfer.
 * 
 * @author shashwat.anand
 */
public class ObjectTypeTransfer extends ByteArrayTransfer {

	/** The Constant INSTANCE. */
	private static final ObjectTypeTransfer INSTANCE = new ObjectTypeTransfer();
	
	/** The Constant TYPE_NAME. */
	private static final String TYPE_NAME = "Object Type Transfer" + System.currentTimeMillis() + ":" + INSTANCE.hashCode(); //$NON-NLS-2$
	
	/** The Constant TYPEID. */
	private static final int TYPEID = registerType(TYPE_NAME);

	/** The object. */
	private Object object;
	
	/** The start time. */
	private long startTime;

	/**
	 * Gets the single instance of ObjectTypeTransfer.
	 *
	 * @return single instance of ObjectTypeTransfer
	 */
	public static ObjectTypeTransfer getInstance() {
		return INSTANCE;
	}

	/**
	 * @see org.eclipse.swt.dnd.ByteArrayTransfer#javaToNative(java.lang.Object, org.eclipse.swt.dnd.TransferData)
	 */
	public void javaToNative(Object object, TransferData transferData) {
		setObject(object);
		this.startTime = System.currentTimeMillis();
		if (transferData != null) {
			super.javaToNative(String.valueOf(this.startTime).getBytes(), transferData);
		}
	}

	/**
	 * @see org.eclipse.swt.dnd.ByteArrayTransfer#nativeToJava(org.eclipse.swt.dnd.TransferData)
	 */
	public Object nativeToJava(TransferData transferData) {
		byte[] bytes = (byte[]) super.nativeToJava(transferData);
		if (bytes == null) {
			return null;
		}
		long startTime = Long.parseLong(new String(bytes));
		return this.startTime == startTime ? getObject() : null;
	}

	/**
	 * Gets the object.
	 *
	 * @return the object
	 */
	public Object getObject() {
		return object;
	}

	/**
	 * Sets the object.
	 *
	 * @param object the object to set
	 */
	public void setObject(final Object object) {
		this.object = object;
	}

	/**
	 * @see org.eclipse.swt.dnd.Transfer#getTypeIds()
	 */
	@Override
	protected int[] getTypeIds() {
		return new int[] { TYPEID };
	}

	/**
	 * @see org.eclipse.swt.dnd.Transfer#getTypeNames()
	 */
	@Override
	protected String[] getTypeNames() {
		return new String[] { TYPE_NAME };
	}
}
