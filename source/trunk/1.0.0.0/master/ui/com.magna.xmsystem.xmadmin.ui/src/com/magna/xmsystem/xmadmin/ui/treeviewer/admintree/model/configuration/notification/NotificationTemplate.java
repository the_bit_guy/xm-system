package com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.notification;

import java.beans.PropertyChangeEvent;
import java.util.TreeSet;

import com.magna.xmsystem.dependencies.utils.XMSystemUtil;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.BeanModel;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild;
import com.magna.xmsystem.xmadmin.util.CommonConstants;

// TODO: Auto-generated Javadoc
/**
 * The Class NotificationTemplate.
 */
public class NotificationTemplate extends BeanModel implements IAdminTreeChild {

	/** The Constant SUB_LIMIT. */
	public static final int SUB_LIMIT = 100;

	/** The Constant MESSAGE_LIMIT. */
	public static final int MESSAGE_LIMIT = 1500;

	/** The Constant PROPERTY_SUBJECT. */
	public static final String PROPERTY_SUBJECT = "subject"; //$NON-NLS-1$

	/** The Constant PROPERTY_MESSAGE. */
	public static final String PROPERTY_MESSAGE = "message"; //$NON-NLS-1$

	/** The Constant PROPERTY_NAME. */
	public static final String PROPERTY_NAME = "name"; //$NON-NLS-1$

	/** The Constant PROPERTY_TEMPLATE_ID. */
	public static final String PROPERTY_TEMPLATE_ID = "templateId"; //$NON-NLS-1$

	/** The Constant PROPERTY_VARIABLES. */
	public static final String PROPERTY_VARIABLES = "variables"; //$NON-NLS-1$

	/** The Constant PROPERTY_OPERATION_MODE. */
	public static final String PROPERTY_OPERATION_MODE = "operationMode"; //$NON-NLS-1$

	/** The template id. */
	private String templateId;

	/** The name. */
	private String name;

	/** The subject. */
	private String subject;

	/** The message. */
	private String message;

	/** The variables. */
	private TreeSet<String> variables;

	/** The operation mode. */
	private int operationMode;
	
	/**
	 * Instantiates a new notification template.
	 */
	public NotificationTemplate() {
		super();
	}

	/**
	 * Instantiates a new notification template.
	 *
	 * @param templateId
	 *            the template id
	 * @param name
	 *            the name
	 * @param subject
	 *            the subject
	 * @param message
	 *            the message
	 * @param variables
	 *            the variables
	 * @param operationMode
	 *            the operation mode
	 */
	public NotificationTemplate(String templateId, String name, String subject, String message, TreeSet<String> variables,
			int operationMode) {
		super();
		this.templateId = templateId;
		this.name = name;
		this.subject = subject;
		this.message = message;
		this.variables = variables;
		this.operationMode = operationMode;
	}

	/**
	 * Instantiates a new notification template.
	 *
	 * @param templateId
	 *            the template id
	 * @param name
	 *            the name
	 * @param subject
	 *            the subject
	 * @param message
	 *            the message
	 * @param operationMode
	 *            the operation mode
	 */
	public NotificationTemplate(String templateId, String name, String subject, String message, int operationMode) {
		this(templateId, name, subject, message, new TreeSet<>(), operationMode);
	}

	

	/**
	 * Gets the template id.
	 *
	 * @return the template id
	 */
	public String getTemplateId() {
		return templateId;
	}

	/**
	 * Sets the template id.
	 *
	 * @param templateId
	 *            the new template id
	 */
	public void setTemplateId(String templateId) {
		this.propertyChangeSupport.firePropertyChange(PROPERTY_TEMPLATE_ID, this.templateId,
				this.templateId = templateId);
	}

	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Sets the name.
	 *
	 * @param name
	 *            the new name
	 */
	public void setName(String name) {
		if (name == null)
			throw new IllegalArgumentException();
		this.propertyChangeSupport.firePropertyChange(PROPERTY_NAME, this.name, this.name = name.trim());
	}

	/**
	 * Gets the subject.
	 *
	 * @return the subject
	 */
	public String getSubject() {
		return subject;
	}

	/**
	 * Sets the subject.
	 *
	 * @param subject
	 *            the new subject
	 */
	public void setSubject(String subject) {
		if (subject == null)
			throw new IllegalArgumentException();
		this.propertyChangeSupport.firePropertyChange(PROPERTY_SUBJECT, this.subject, this.subject = subject.trim());
	}

	/**
	 * Gets the message.
	 *
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * Sets the message.
	 *
	 * @param message
	 *            the new message
	 */
	public void setMessage(String message) {
		if (message == null)
			throw new IllegalArgumentException();
		this.propertyChangeSupport.firePropertyChange(PROPERTY_MESSAGE, this.message, this.message = message.trim());
	}

	/**
	 * Gets the variables.
	 *
	 * @return the variables
	 */
	public TreeSet<String> getVariables() {
		return variables;
	}

	/**
	 * Sets the variables.
	 *
	 * @param variables
	 *            the new variables
	 */
	public void setVariables(TreeSet<String> variables) {
		if (variables == null)
			throw new IllegalArgumentException();
		this.propertyChangeSupport.firePropertyChange(PROPERTY_VARIABLES, this.variables, this.variables = variables);
	}

	/**
	 * Gets the operation mode.
	 *
	 * @return the operation mode
	 */
	public int getOperationMode() {
		return operationMode;
	}

	/**
	 * Sets the operation mode.
	 *
	 * @param operationMode
	 *            the new operation mode
	 */
	public void setOperationMode(int operationMode) {
		this.propertyChangeSupport.firePropertyChange(PROPERTY_OPERATION_MODE, this.operationMode,
				this.operationMode = operationMode);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.BeanModel#
	 * propertyChange(java.beans.PropertyChangeEvent)
	 */
	@Override
	public void propertyChange(PropertyChangeEvent event) {
		this.propertyChangeSupport.firePropertyChange(event.getPropertyName(), event.getOldValue(),
				event.getNewValue());
	}

	/**
	 * Deep copy template.
	 *
	 * @param b
	 *            the b
	 * @param object
	 *            the object
	 * @return the notification template
	 */
	public NotificationTemplate deepCopyTemplate(boolean update, NotificationTemplate updateThisObject) {
		NotificationTemplate clonedTemplate = null;
		String currentTemplateId = XMSystemUtil.isEmpty(this.getTemplateId()) ? CommonConstants.EMPTY_STR : this.getTemplateId();
		String currentName = XMSystemUtil.isEmpty(this.getName()) ? CommonConstants.EMPTY_STR : this.getName();
		String currentSubject = XMSystemUtil.isEmpty(this.getSubject()) ? CommonConstants.EMPTY_STR : this.getSubject();
		String currentMessage = XMSystemUtil.isEmpty(this.getMessage()) ? CommonConstants.EMPTY_STR : this.getMessage();
		TreeSet<String> currentVariables = new TreeSet<>(this.getVariables());
		
		if (update) {
			clonedTemplate = updateThisObject;
		} else {
			clonedTemplate = new NotificationTemplate(currentTemplateId, currentName,currentSubject,currentMessage,currentVariables, CommonConstants.OPERATIONMODE.VIEW);
		}
		clonedTemplate.setTemplateId(currentTemplateId);
		clonedTemplate.setName(currentName);
		clonedTemplate.setSubject(currentSubject);
		clonedTemplate.setMessage(currentMessage);
		clonedTemplate.setVariables(currentVariables);
		
		return clonedTemplate;

	}

}
