package com.magna.xmsystem.xmadmin.ui.handlers;

import javax.inject.Inject;

import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.core.services.nls.Translation;
import org.eclipse.swt.widgets.Shell;

import com.magna.xmsystem.dependencies.utils.XMSystemUtil;
import com.magna.xmsystem.dependencies.utils.XmSystemEnvProcess;
import com.magna.xmsystem.ui.controls.widgets.CustomMessageDialog;
import com.magna.xmsystem.xmadmin.message.Message;

/**
 * Class for About handler to determine the product version
 *
 * @author Chiranjeevi.Akula
 */
public class AboutHandler {
	
	/** Member variable for messages. */
	@Inject
	@Translation
	private Message messages;

	/**
	 * Method for Execute.
	 *
	 * @param shell
	 *            {@link Shell}
	 */
	@Execute
	public void execute(final Shell shell) {
		CustomMessageDialog.openInformation(shell, messages.aboutDialogTitle,
				"caxStartAdmin Version " + XmSystemEnvProcess.getInstance().getEnvironmentMap().get("XM_VERSION")
				+ "\nBuild Number " + XMSystemUtil.getBuildNumber());
	}
}
