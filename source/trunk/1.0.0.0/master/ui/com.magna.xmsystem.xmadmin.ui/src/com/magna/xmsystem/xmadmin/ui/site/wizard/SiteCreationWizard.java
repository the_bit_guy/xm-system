package com.magna.xmsystem.xmadmin.ui.site.wizard;

import org.eclipse.jface.wizard.Wizard;

import com.magna.xmsystem.ui.controls.widgets.CustomMessageDialog;
import com.magna.xmsystem.xmadmin.message.Message;
import com.magna.xmsystem.xmadmin.message.MessageRegistry;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.sitemodel.Site;

/**
 * Handler class for Site Creation
 * 
 * @author Roshan Ekka
 * 
 */
@Deprecated
public class SiteCreationWizard extends Wizard {
	private Message messages;
	private MessageRegistry registry;
	private SiteCreationPage siteCreationPage;

	public SiteCreationWizard(Message messages, MessageRegistry registry) {
		this.messages = messages;
		this.registry = registry;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jface.wizard.IWizard#addPages()
	 */
	public void addPages() {
		this.siteCreationPage = new SiteCreationPage("SiteCreationPage", this.messages, this.registry);
		addPage(this.siteCreationPage);
	}
	
	@Override
	public boolean canFinish() {
		return this.siteCreationPage.isPageComplete();
	}

	public boolean performFinish() {
		Site siteModel = this.siteCreationPage.getSiteModel();
		System.out.println(siteModel);
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jface.wizard.IWizard#performCancel()
	 */
	public boolean performCancel() {
		boolean ans = CustomMessageDialog.openConfirm(getShell(), "Confirmation", "Are you sure to cancel the task?");
		if (ans)
			return true;
		else
			return false;
	}

}