package com.magna.xmsystem.xmadmin.ui.parts.dndperspective.group;

import org.eclipse.nebula.widgets.pgroup.PGroup;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ExpandEvent;
import org.eclipse.swt.events.ExpandListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;

/**
 * The Class ParkingLotGroup.
 * 
 * @author shashwat.anand
 */
public class ParkingLotGroup extends PGroup {

	/**
	 * Instantiates a new parking lot group.
	 *
	 * @param parent the parent
	 * @param style the style
	 */
	public ParkingLotGroup(Composite parent, int style) {
		super(parent, style);
		initUi();
		this.addExpandListener(new ExpandListener() {

			@Override
			public void itemExpanded(final ExpandEvent event) {
				pGroupRepaint((PGroup) event.widget);
			}

			@Override
			public void itemCollapsed(final ExpandEvent event) {
				pGroupRepaint((PGroup) event.widget);
			}
		});
	}

	/**
	 * Inits the ui.
	 */
	private void initUi() {
		this.setTogglePosition(SWT.LEFT);
		ParkingLotGroupItem closeToolItem = new ParkingLotGroupItem(this, SWT.PUSH);
		closeToolItem.setText("x"); // $NON-NLS-1$
		closeToolItem.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent event) {
				closeHandler(event);
			}
		});
		Event event = new Event();
		event.gc = new GC(this);
		this.notifyListeners(SWT.Paint, event);
	}
	
	private void pGroupRepaint(final PGroup pGroup) {
		pGroup.requestLayout();
		pGroup.redraw();
		pGroup.getParent().update();
	}

	/**
	 * Close handler.
	 *
	 * @param event the event
	 */
	protected void closeHandler(SelectionEvent event) {
		// PGroupToolItem closeToolItem = (PGroupToolItem) e.widget;
		// System.out.println("Close item clicked : " + closeToolItem);
	}
}
