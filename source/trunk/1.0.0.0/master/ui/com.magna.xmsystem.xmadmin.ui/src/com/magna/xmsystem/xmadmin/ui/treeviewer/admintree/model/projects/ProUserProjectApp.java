package com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.projects;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;

import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.users.UserProjectAAProjectAppAllowed;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.users.UserProjectAAProjectAppForbidden;

/**
 * The Class ProUserProjectApp.
 * 
 * @author subash.janarthanan
 * 
 */
public class ProUserProjectApp implements IAdminTreeChild {

	/** The parent. */
	private IAdminTreeChild parent;

	/** The project app child. */
	final private Map<String, IAdminTreeChild> projectAppChild;

	/**
	 * Instantiates a new pro user project app.
	 *
	 * @param parent
	 *            the parent
	 */
	public ProUserProjectApp(IAdminTreeChild parent) {
		this.parent = parent;
		this.projectAppChild = new LinkedHashMap<>();
		this.projectAppChild.put(UserProjectAAProjectAppAllowed.class.getName(), new UserProjectAAProjectAppAllowed(this));
		this.projectAppChild.put(UserProjectAAProjectAppForbidden.class.getName(), new UserProjectAAProjectAppForbidden(this));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild#
	 * getParent()
	 */
	@Override
	public IAdminTreeChild getParent() {
		return this.parent;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild#
	 * setParent(com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.
	 * IAdminTreeChild)
	 */
	@Override
	public void setParent(IAdminTreeChild parent) {
		this.parent = parent;

	}

	/**
	 * Gets the project app child collection.
	 *
	 * @return the project app child collection
	 */
	public Collection<IAdminTreeChild> getProjectAppChildCollection() {
		return this.projectAppChild.values();
	}

	/**
	 * Gets the project app child.
	 *
	 * @return the project app child
	 */
	public Map<String, IAdminTreeChild> getProjectAppChild() {
		return projectAppChild;
	}

}