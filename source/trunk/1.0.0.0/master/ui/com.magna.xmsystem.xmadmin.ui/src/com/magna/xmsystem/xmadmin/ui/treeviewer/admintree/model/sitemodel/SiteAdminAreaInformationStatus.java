package com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.sitemodel;

import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild;

/**
 * The Class SiteAdminAreaInformationStatus.
 * 
 * @author shashwat.anand
 */
public class SiteAdminAreaInformationStatus implements IAdminTreeChild {
	/** The parent. */
	private IAdminTreeChild parent;
	
	/**
	 * Instantiates a new site admin area information status.
	 *
	 * @param parent the parent
	 */
	public SiteAdminAreaInformationStatus(final IAdminTreeChild parent) {
		this.parent = parent;
	}
	
	/*
	 * @see com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild#getParent()
	 */
	@Override
	public IAdminTreeChild getParent() {
		return this.parent;
	}

	/*
	 * @see com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild#
	 * setParent(com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild)
	 */
	@Override
	public void setParent(IAdminTreeChild parent) {
		this.parent = parent;
	}

}
