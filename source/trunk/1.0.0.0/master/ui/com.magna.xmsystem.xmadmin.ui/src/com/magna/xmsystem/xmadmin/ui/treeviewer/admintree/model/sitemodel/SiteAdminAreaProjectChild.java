package com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.sitemodel;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;

import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.refs.RelationObj;

/**
 * The Class SiteAdministrationProjectChild.
 * 
 * @author subash.janarthanan
 * 
 */
public class SiteAdminAreaProjectChild implements IAdminTreeChild {

	/** The parent. */
	private IAdminTreeChild parent;


	/** The site admin area project children. */
	final private Map<String, IAdminTreeChild> siteAdminAreaProjectChildren;
	
	/**
	 * Instantiates a new site administration project child.
	 *
	 * @param parent the parent
	 */
	public SiteAdminAreaProjectChild(final IAdminTreeChild parent) {
		this.parent = parent;
		this.siteAdminAreaProjectChildren = new LinkedHashMap<>();
		/*this.siteAdminAreaProjectChildren.put(SiteAdministrationAreaProjectChildren.PROJECTAPPLICATIONS,
				new SiteAdminAreaChildProjectApp(this));
		this.siteAdminAreaProjectChildren.put(SiteAdministrationAreaProjectChildren.STARTAPPLICATIONS,
				new SiteAdminAreaChildStartApplications(this));*/
	}


	/**
	 * Gets the site admin areas project collection.
	 *
	 * @return the site admin areas project collection
	 */
	public Collection<IAdminTreeChild> getSiteAdminAreasProjectCollection() {
		return this.siteAdminAreaProjectChildren.values();
	}

	/**
	 * Gets the site admin area project children.
	 *
	 * @return the site admin area project children
	 */
	public Map<String, IAdminTreeChild> getSiteAdminAreaProjectChildren() {
		return siteAdminAreaProjectChildren;
	}

	/**
	 * Method for Adds the.
	 *
	 * @param siteAdminAreaProjChildId {@link String}
	 * @param child {@link IAdminTreeChild}
	 * @return the i admin tree child {@link IAdminTreeChild}
	 */
	public IAdminTreeChild add(final String siteAdminAreaProjChildId, final IAdminTreeChild child) {
		child.setParent(this);
		return this.siteAdminAreaProjectChildren.put(siteAdminAreaProjChildId, child);
	}
	
	/**
	 * Method for Removes the.
	 *
	 * @param siteAdminAreaChildId {@link String}
	 * @return the i admin tree child {@link IAdminTreeChild}
	 */
	public IAdminTreeChild remove(final String siteAdminAreaProjChildId) {
		return this.siteAdminAreaProjectChildren.remove(siteAdminAreaProjChildId);
	}
	
	/**
	 * Method for Removes the all.
	 */
	public void removeAll() {
		this.siteAdminAreaProjectChildren.clear();
	}
	
	/**
	 * Method for Adds the fixed children.
	 *
	 * @param siteAdministrationChildRel {@link RelationObj}
	 */
	public void addFixedChildren(RelationObj siteAdminProjChildRel) {
		siteAdminAreaProjectChildren.put(SiteAdminAreaProjectApplications.class.getName(), new SiteAdminAreaProjectApplications(siteAdminProjChildRel));
		siteAdminAreaProjectChildren.put(SiteAdminAreaProjectStartApplications.class.getName(), new SiteAdminAreaProjectStartApplications(siteAdminProjChildRel));
	}
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild#
	 * getParent()
	 */
	@Override
	public IAdminTreeChild getParent() {
		return this.parent;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild#
	 * setParent(com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.
	 * IAdminTreeChild)
	 */
	@Override
	public void setParent(final IAdminTreeChild parent) {
		this.parent = parent;
	}

}
