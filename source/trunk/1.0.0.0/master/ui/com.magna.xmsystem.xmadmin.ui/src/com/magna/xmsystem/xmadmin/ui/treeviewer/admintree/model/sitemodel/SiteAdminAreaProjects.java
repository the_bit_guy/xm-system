package com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.sitemodel;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.stream.Collectors;

import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.projects.Project;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.refs.RelationObj;

/**
 * The Class SiteAdminAreaProjects.
 * 
 * @author subash.janarthanan
 * 
 */
public class SiteAdminAreaProjects implements IAdminTreeChild {
	
	/** The parent. */
	private IAdminTreeChild parent;

	/** The site admin projects children. */
	private Map<String, IAdminTreeChild> siteAdminProjectsChildren;

	/**
	 * Instantiates a new site admin area projects.
	 *
	 * @param parent the parent
	 */
	public SiteAdminAreaProjects(IAdminTreeChild parent) {
		this.parent = parent;
		this.siteAdminProjectsChildren = new LinkedHashMap<>();
	}

	/**
	 * Adds the.
	 *
	 * @param siteAdminAreaProId the site admin area pro id
	 * @param child the child
	 * @return the i admin tree child
	 */
	public IAdminTreeChild add(final String siteAdminAreaProId, final IAdminTreeChild child) {
		child.setParent(this);
		IAdminTreeChild returnVal = this.siteAdminProjectsChildren.put(siteAdminAreaProId, child);
		if (child instanceof RelationObj && ((RelationObj) child).getRefObject() instanceof Project) {
			sort();
		}
		return returnVal;
	}

	/**
	 * Method for Removes the all.
	 */
	public void removeAll() {
		this.siteAdminProjectsChildren.clear();
	}
	
	/**
	 * Removes the.
	 *
	 * @param siteAdminAreaProId the site admin area pro id
	 * @return the i admin tree child
	 */
	public IAdminTreeChild remove(final String siteAdminAreaProId) {
		return this.siteAdminProjectsChildren.remove(siteAdminAreaProId);
	}

	/**
	 * Gets the site admin projects collection.
	 *
	 * @return the site admin projects collection
	 */
	public Collection<IAdminTreeChild> getSiteAdminProjectsCollection() {
		return this.siteAdminProjectsChildren.values();
	}

	/**
	 * Gets the site admin projects children.
	 *
	 * @return the site admin projects children
	 */
	public Map<String, IAdminTreeChild> getSiteAdminProjectsChildren() {
		return siteAdminProjectsChildren;
	}

	/* (non-Javadoc)
	 * @see com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild#getParent()
	 */
	@Override
	public IAdminTreeChild getParent() {
		return this.parent;
	}

	/* (non-Javadoc)
	 * @see com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild#setParent(com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild)
	 */
	@Override
	public void setParent(IAdminTreeChild parent) {
		this.parent = parent;
	}
	
	/**
	 * Sort.
	 */
	public void sort() {
		final LinkedHashMap<String, IAdminTreeChild> collect = this.siteAdminProjectsChildren.entrySet().stream().sorted(
				(e1, e2) -> ((Project) (((RelationObj) e1.getValue()).getRefObject())).getName()
				.compareTo(((Project) (((RelationObj) e2.getValue()).getRefObject())).getName()))
				.collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e1, LinkedHashMap::new));
		this.siteAdminProjectsChildren = collect;
	}
	

}
