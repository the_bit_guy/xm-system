package com.magna.xmsystem.xmadmin.ui.parts.projectexpiryconfig;

import org.eclipse.jface.layout.GridDataFactory;
import org.eclipse.jface.layout.GridLayoutFactory;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Spinner;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

// TODO: Auto-generated Javadoc
/**
 * The Class ProjectExpiryConfigUI.
 * 
 * @author archita.patel
 */
public class ProjectExpiryConfigUI extends Composite {

	/** The Constant LOGGER. */
	private static final Logger LOGGER = LoggerFactory.getLogger(ProjectExpiryConfigUI.class);

	/** The grp singleton app time config. */
	protected Group grpProjectExpiryConfig;

	/** The lbl project expiry days. */
	protected Label lblProjectExpiryDays;

	/** The day spinner. */
	protected Spinner projectExpirydaySpinner;

	/** The lbldays. */
	protected Label lbldays;
	
	/** The lbl project grace days. */
	protected Label lblProjectGraceDays;

	/** The project grace day spinner. */
	protected Spinner projectGraceDaySpinner;

	/** The lbl gracedays. */
	protected Label lblGracedays;

	/** The btn save. */
	protected Button btnSave;

	/** The btn cancel. */
	protected Button btnCancel;

	/**
	 * Instantiates a new project expiry config UI.
	 *
	 * @param parent
	 *            the parent
	 * @param style
	 *            the style
	 */
	public ProjectExpiryConfigUI(final Composite parent, final int style) {
		super(parent, style);
		this.initGUI();
	}

	/**
	 * Inits the GUI.
	 */
	private void initGUI() {
		try {
			GridLayoutFactory.fillDefaults().applyTo(this);
			this.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));

			this.grpProjectExpiryConfig = new Group(this, SWT.NONE);
			GridLayoutFactory.fillDefaults().numColumns(1).applyTo(this.grpProjectExpiryConfig);
			GridDataFactory.fillDefaults().grab(true, true).span(SWT.FILL, SWT.FILL)
					.applyTo(this.grpProjectExpiryConfig);

			final Composite widgetContainer = new Composite(this.grpProjectExpiryConfig, SWT.NONE);
			final GridLayout widgetContLayout = new GridLayout(3, false);

			widgetContainer.setLayout(widgetContLayout);
			widgetContainer.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));

			this.lblProjectExpiryDays = new Label(widgetContainer, SWT.NONE);

			this.projectExpirydaySpinner = new Spinner(widgetContainer, SWT.BORDER);
			this.projectExpirydaySpinner.setMinimum(0);
			this.projectExpirydaySpinner.setIncrement(1);
			this.projectExpirydaySpinner.setPageIncrement(10);
			this.projectExpirydaySpinner.setMaximum(Integer.MAX_VALUE);
			GridDataFactory.fillDefaults().grab(true, false).span(1, 1).align(SWT.FILL, SWT.CENTER)
					.applyTo(this.projectExpirydaySpinner);
			this.lbldays = new Label(widgetContainer, SWT.NONE);
			
			
			this.lblProjectGraceDays = new Label(widgetContainer, SWT.NONE);

			this.projectGraceDaySpinner = new Spinner(widgetContainer, SWT.BORDER);
			this.projectGraceDaySpinner.setMinimum(0);
			this.projectGraceDaySpinner.setIncrement(1);
			this.projectGraceDaySpinner.setPageIncrement(10);
			this.projectGraceDaySpinner.setMaximum(Integer.MAX_VALUE);
			GridDataFactory.fillDefaults().grab(true, false).span(1, 1).align(SWT.FILL, SWT.CENTER)
					.applyTo(this.projectGraceDaySpinner);
			this.lblGracedays = new Label(widgetContainer, SWT.NONE);

			

			final Composite buttonBarComp = new Composite(widgetContainer, SWT.NONE);
			final GridLayout btnBarCompLayout = new GridLayout(2, true);
			btnBarCompLayout.marginRight = 0;
			btnBarCompLayout.marginLeft = 0;
			btnBarCompLayout.marginTop = 0;
			btnBarCompLayout.marginBottom = 0;
			btnBarCompLayout.marginWidth = 0;
			buttonBarComp.setLayout(btnBarCompLayout);
			buttonBarComp.setLayoutData(new GridData(SWT.FILL, SWT.DEFAULT, true, false, 3, 1));
			createButtonBar(buttonBarComp);
		} catch (Exception e) {
			LOGGER.error("Unable to create Ui element", e);
		}

	}

	/**
	 * Creates the button bar.
	 *
	 * @param buttonBarComp
	 *            the button bar comp
	 */
	private void createButtonBar(final Composite buttonBarComp) {
		this.btnSave = new Button(buttonBarComp, SWT.NONE);
		this.btnSave.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, true, false));

		this.btnCancel = new Button(buttonBarComp, SWT.NONE);
		this.btnCancel.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, true, false));
	}

}
