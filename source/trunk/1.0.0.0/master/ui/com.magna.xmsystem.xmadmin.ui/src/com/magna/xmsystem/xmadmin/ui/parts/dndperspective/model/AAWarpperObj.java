package com.magna.xmsystem.xmadmin.ui.parts.dndperspective.model;

import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.adminarea.AdministrationArea;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.refs.RelationObj;

/**
 * The Class AAWarpperObj.
 * 
 * @author shashwat.anand
 */
public class AAWarpperObj {
	
	/** The admin area. */
	private AdministrationArea adminArea;
	
	/** The related obj. */
	private IAdminTreeChild relatedObj;
	
	/** The relation obj. */
	private RelationObj relationObj;

	/**
	 * Instantiates a new AA warpper obj.
	 *
	 * @param adminArea the admin area
	 */
	public AAWarpperObj(final AdministrationArea adminArea) {
		this(adminArea, null);
	}

	/**
	 * Instantiates a new AA warpper obj.
	 *
	 * @param adminArea the admin area
	 * @param relatedObj the related obj
	 */
	public AAWarpperObj(final AdministrationArea adminArea, final IAdminTreeChild relatedObj) {
		this.adminArea = adminArea;
		this.relatedObj = relatedObj;
	}
	
	/**
	 * Gets the type name.
	 *
	 * @return the type name
	 */
	public String getTypeName() {
		return AdministrationArea.class.getSimpleName();
	}

	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	public String getName() {
		return this.adminArea.getName();
	}
	
	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public String getId() {
		if (this.relationObj != null) {
			return this.relationObj.getRelId();
		}
		return this.adminArea.getAdministrationAreaId();
	}

	/**
	 * @return the adminArea
	 */
	public AdministrationArea getAdminArea() {
		return adminArea;
	}

	/**
	 * @param adminArea the adminArea to set
	 */
	public void setAdminArea(AdministrationArea adminArea) {
		this.adminArea = adminArea;
	}

	/**
	 * @return the relatedObj
	 */
	public IAdminTreeChild getRelatedObj() {
		return relatedObj;
	}

	/**
	 * @param relatedObj the relatedObj to set
	 */
	public void setRelatedObj(IAdminTreeChild relatedObj) {
		this.relatedObj = relatedObj;
	}
	
	/**
	 * @return the relationObj
	 */
	public RelationObj getRelationObj() {
		return relationObj;
	}

	/**
	 * @param relationObj the relationObj to set
	 */
	public void setRelationObj(RelationObj relationObj) {
		this.relationObj = relationObj;
	}

}
