package com.magna.xmsystem.xmadmin.ui.parts.project;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.inject.Inject;

import org.eclipse.core.databinding.Binding;
import org.eclipse.core.databinding.DataBindingContext;
import org.eclipse.core.databinding.UpdateValueStrategy;
import org.eclipse.core.databinding.beans.BeanProperties;
import org.eclipse.core.databinding.observable.value.IObservableValue;
import org.eclipse.core.databinding.observable.value.IValueChangeListener;
import org.eclipse.core.databinding.observable.value.ValueChangeEvent;
import org.eclipse.e4.core.services.nls.Translation;
import org.eclipse.e4.ui.di.Persist;
import org.eclipse.e4.ui.model.application.ui.MDirtyable;
import org.eclipse.jface.databinding.fieldassist.ControlDecorationSupport;
import org.eclipse.jface.databinding.swt.WidgetProperties;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.viewers.TreePath;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.VerifyEvent;
import org.eclipse.swt.events.VerifyListener;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Link;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.widgets.ToolItem;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.client.ResourceAccessException;

import com.magna.xmbackend.entities.LanguagesTbl;
import com.magna.xmbackend.entities.ProjectTranslationTbl;
import com.magna.xmbackend.entities.ProjectsTbl;
import com.magna.xmbackend.vo.project.ProjectRequest;
import com.magna.xmsystem.dependencies.customExceptions.UnauthorizedAccessException;
import com.magna.xmsystem.dependencies.utils.LANG_ENUM;
import com.magna.xmsystem.dependencies.utils.XMSystemUtil;
import com.magna.xmsystem.ui.controls.widgets.CustomMessageDialog;
import com.magna.xmsystem.xmadmin.message.Message;
import com.magna.xmsystem.xmadmin.message.MessageRegistry;
import com.magna.xmsystem.xmadmin.restclient.project.ProjectController;
import com.magna.xmsystem.xmadmin.ui.dialogs.lang.ControlModel;
import com.magna.xmsystem.xmadmin.ui.dialogs.lang.TextAreaModifyListener;
import com.magna.xmsystem.xmadmin.ui.dialogs.lang.XMAdminLangTextAreaDialog;
import com.magna.xmsystem.xmadmin.ui.dialogs.lang.XMAdminLangTextDialog;
import com.magna.xmsystem.xmadmin.ui.parts.icons.IconDialog;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.AdminTreeviewer;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.AdminTreeFactory;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.icon.Icon;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.projects.Project;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.projects.Projects;
import com.magna.xmsystem.xmadmin.ui.validation.NameValidation;
import com.magna.xmsystem.xmadmin.ui.validation.StatusValidation;
import com.magna.xmsystem.xmadmin.ui.validation.SymbolValidation;
import com.magna.xmsystem.xmadmin.util.CommonConstants;
import com.magna.xmsystem.xmadmin.util.MessageType;
import com.magna.xmsystem.xmadmin.util.XMAdminUtil;

/**
 * The Class ProjectCompositeAction.
 * 
 * @author subash.janarthanan
 * 
 */
public class ProjectCompositeAction extends ProjectCompositeUI {

	/** The Constant LOGGER. */
	private static final Logger LOGGER = LoggerFactory.getLogger(ProjectCompositeAction.class);

	/** The old model. */
	private Project oldModel;

	/** The project model. */
	private Project projectModel;

	/** The widget value. */
	transient private IObservableValue<?> widgetValue;

	/** The model value. */
	transient private IObservableValue<?> modelValue;

	/** The bind value. */
	transient private Binding bindValue;

	/** The data bind context. */
	final transient DataBindingContext dataBindContext = new DataBindingContext();

	/** Member variable for {@link MessageRegistry}. */
	@Inject
	private MessageRegistry registry;

	/** The messages. */
	@Inject
	@Translation
	transient private Message messages;

	/** The dirty. */
	private MDirtyable dirty;

	/** The control model. */
	transient private ControlModel controlModel;

	/**
	 * Instantiates a new project composite action.
	 *
	 * @param parent
	 *            the parent
	 */
	@Inject
	public ProjectCompositeAction(final Composite parent) {
		super(parent, SWT.NONE);
		initListeners();
	}

	/**
	 * Init listeners.
	 */
	private void initListeners() {
		// Event handling when users click on desc lang links.
		this.descLink.addSelectionListener(new SelectionAdapter() {

			@Override
			public void widgetSelected(SelectionEvent event) {
				final Link linkWidget = (Link) event.widget;
				openDescDialog(linkWidget.getShell());
			}
		});

		// Event handling when users click on helptext lang links.
		this.remarksTranslate.addSelectionListener(new SelectionAdapter() {

			@Override
			public void widgetSelected(SelectionEvent event) {
				final Link linkWidget = (Link) event.widget;
				openRemarkDialog(linkWidget.getShell());
			}
		});

		if (this.saveBtn != null) {
			this.saveBtn.addSelectionListener(new SelectionAdapter() {

				/**
				 * Save button handler
				 */
				@Override
				public void widgetSelected(final SelectionEvent event) {
					saveProjectHandler();
				}

			});
		}
		if (this.cancelBtn != null) {
			this.cancelBtn.addSelectionListener(new SelectionAdapter() {

				/**
				 * Cancel button handler
				 */
				@Override
				public void widgetSelected(final SelectionEvent event) {
					cancelProjectHandler();
				}
			});
		}

		this.toolItem.addSelectionListener(new SelectionAdapter() {

			/**
			 * Symbol button handler
			 */
			@Override
			public void widgetSelected(final SelectionEvent event) {
				if ((boolean) toolItem.getData("editable")) {
					final ToolItem widget = (ToolItem) event.widget;
					final IconDialog dialog = new IconDialog(widget.getParent().getShell(),
							messages.browseIconDialogTitle, messages.icontableviewerSecondColumnLabel);

					final int returnVal = dialog.open();
					if (IDialogConstants.OK_ID == returnVal) {
						final Icon checkedIcon = dialog.getCheckedIcon();
						toolItem.setImage(XMSystemUtil.getInstance().getImage(this.getClass(), checkedIcon.getIconPath(), true, true));
						projectModel.setProjectIcon(checkedIcon);
					}
				}
			}
		});

		this.txtRemarks.addModifyListener(new TextAreaModifyListener(this.lblRemarksCount, Project.REMARK_LIMIT));
		
		this.txtRemarks.addVerifyListener(new VerifyListener() {

			@Override
			public void verifyText(VerifyEvent event) {
				String source = ((Text) event.widget).getText();
				final String remarkText = source.substring(0, event.start) + event.text + source.substring(event.end);
				int length = remarkText.length();
				if (length > Project.REMARK_LIMIT) {
					event.doit = false;

				}
			}
		});
		
	}

	/**
	 * Save project handler.
	 */
	public void saveProjectHandler() {
		// validate the model
		saveDescAndRemarks();
		if (validate()) {
			if (!projectModel.getName().isEmpty() && projectModel.getIcon().getIconId() != null) {
				if (projectModel.getOperationMode() == CommonConstants.OPERATIONMODE.CREATE) {
					createProjectOperation();
				} else if (projectModel.getOperationMode() == CommonConstants.OPERATIONMODE.CHANGE) {
					changeProjectOperation();
				}
			}
		}
	}

	/**
	 * Cancel project handler.
	 */
	public void cancelProjectHandler() {
		if (projectModel == null) {
			dirty.setDirty(false);
			return;
		}
		String projectId = CommonConstants.EMPTY_STR;
		int operationMode = this.projectModel.getOperationMode();
		Project oldModel = getOldModel();
		if (oldModel != null) {
			projectId = oldModel.getProjectId();
		}
		setProjectModel(null);
		setOldModel(null);
		this.saveBtn.setEnabled(true);
		final AdminTreeviewer adminTree = XMAdminUtil.getInstance().getAdminTree();
		final Projects projects = AdminTreeFactory.getInstance().getProjects();
		dirty.setDirty(false);
		if (operationMode == CommonConstants.OPERATIONMODE.CHANGE) {
			final IStructuredSelection selection = (IStructuredSelection) adminTree.getSelection();
			final Object firstElement = selection.getFirstElement();
			if (firstElement != null && firstElement.equals(projects.getProjectsChildren().get(projectId))) {
				adminTree.setSelection(new StructuredSelection(projects.getProjectsChildren().get(projectId)), true);
			}
		} else {
			adminTree.setSelection(new StructuredSelection(projects), true);
		}
		dirty.setDirty(false);
	}

	/**
	 * Save desc and remarks.
	 */
	private void saveDescAndRemarks() {
		LANG_ENUM currentLocaleEnum = XMAdminUtil.getInstance().getCurrentLocaleEnum();
		String desc = txtDesc.getText();
		projectModel.setDescription(currentLocaleEnum, desc);
		String remarks = txtRemarks.getText();
		projectModel.setRemarks(currentLocaleEnum, remarks);
	}

	/**
	 * Validate.
	 *
	 * @return true, if successful
	 */
	protected boolean validate() {
		if (!saveBtn.isEnabled()) {
			return false;
		}
		String projectName = this.projectModel.getName();
		Icon icon;
		if ((XMSystemUtil.isEmpty(projectName) && (icon = this.projectModel.getIcon()) != null
				&& XMSystemUtil.isEmpty(icon.getIconName()))) {
			CustomMessageDialog.openError(this.getShell(), messages.nameSymbolErrorTitle, messages.nameSymbolError);
			return false;
		}
		if (XMSystemUtil.isEmpty(projectName)) {
			CustomMessageDialog.openError(this.getShell(), messages.nameErrorTitle, messages.nameError);
			return false;
		}
		if ((icon = this.projectModel.getIcon()) != null && XMSystemUtil.isEmpty(icon.getIconName())) {
			CustomMessageDialog.openError(this.getShell(), messages.symbolErrorTitle, messages.symbolError);
			return false;
		}
		Projects projects = AdminTreeFactory.getInstance().getProjects();
		Collection<IAdminTreeChild> projectsCollection = projects.getProjectsCollection();
		if (projectModel.getOperationMode() == CommonConstants.OPERATIONMODE.CHANGE) {
			if (!projectName.equalsIgnoreCase(this.oldModel.getName())) {
				Map<String, Long> result = projectsCollection.parallelStream().collect(
						Collectors.groupingBy(project -> ((Project) project).getName().toUpperCase(), Collectors.counting()));
				if (result.containsKey(projectName.toUpperCase())) {
					CustomMessageDialog.openError(this.getShell(), messages.existingProjectNameTitle,
							messages.existingProjectNameError);
					return false;
				}
			}
		} else if (this.projectModel.getOperationMode() == CommonConstants.OPERATIONMODE.CREATE) {
			for (IAdminTreeChild project : projectsCollection) {
				if (projectName.equalsIgnoreCase(((Project) project).getName())) {
					CustomMessageDialog.openError(this.getShell(), messages.existingProjectNameTitle,
							messages.existingProjectNameError);
					return false;
				}
			}
		}
		return true;
	}

	/**
	 * Create project operation.
	 */
	private void createProjectOperation() {
		try {
			ProjectController projectController = new ProjectController();
			ProjectsTbl projectsVo = projectController.createProject(mapVOObjectWithModel());
			String projectId = projectsVo.getProjectId();
			if (!XMSystemUtil.isEmpty(projectId)) {
				this.projectModel.setProjectId(projectId);
				Collection<ProjectTranslationTbl> projectTranslationTblCollection = projectsVo
						.getProjectTranslationTblCollection();
				for (ProjectTranslationTbl projectTranslationTbl : projectTranslationTblCollection) {
					String projectTranslationId = projectTranslationTbl.getProjectTranslationId();
					LanguagesTbl languageCode = projectTranslationTbl.getLanguageCode();
					LANG_ENUM langEnum = LANG_ENUM.getLangEnum(languageCode.getLanguageCode());
					this.projectModel.setTranslationId(langEnum, projectTranslationId);
				}

				AdminTreeFactory instance = AdminTreeFactory.getInstance();
				// Attach model to old model
				if (oldModel == null) { // Attach this to tree
					setOldModel(projectModel.deepCopyProject(false, null));
					instance.getProjects().add(projectId, getOldModel());
				}
				this.dirty.setDirty(false);
				AdminTreeviewer adminTree = XMAdminUtil.getInstance().getAdminTree();
				adminTree.refresh(true);

				adminTree.setSelection(new StructuredSelection(instance.getProjects()), true);
				TreePath[] selectionPaths = adminTree.getStructuredSelection().getPaths();
				if (selectionPaths != null && selectionPaths.length > 0) {
					adminTree.setExpandedState(selectionPaths[0], true);
				}

				adminTree.setSelection(new StructuredSelection(getOldModel()), true);
				XMAdminUtil.getInstance().updateLogFile(messages.projectObject + " " + "'" + this.projectModel.getName()
						+ "'" + " " + messages.objectCreate, MessageType.SUCCESS);
			}
		} catch (UnauthorizedAccessException e) {
			CustomMessageDialog.openError(this.getShell(), messages.objectPermissionDialogTitle,
					messages.objectPermissionDialogMsg);
		}catch (Exception e) {
			if (e instanceof ResourceAccessException) {
				CustomMessageDialog.openError(Display.getCurrent().getActiveShell(), messages.errorDialogTitile,
						messages.serverNotReachable);
			}
			LOGGER.warn("Unable to Save data ! " + e);
		}
	}

	/**
	 * Method for Change Project operation.
	 */
	private void changeProjectOperation() {
		try {
			ProjectController projectController = new ProjectController();
			boolean isUpdated = projectController.updateProject(mapVOObjectWithModel());
			if (isUpdated) {
				setOldModel(projectModel.deepCopyProject(true, getOldModel()));
				this.projectModel.setOperationMode(CommonConstants.OPERATIONMODE.VIEW);
				setOperationMode();
				this.dirty.setDirty(false);
				final Projects projects = AdminTreeFactory.getInstance().getProjects();
				projects.sort();
				XMAdminUtil.getInstance().getAdminTree().refresh(true);
				XMAdminUtil.getInstance().getAdminTree().setSelection(new StructuredSelection(getOldModel()), true);
				XMAdminUtil.getInstance().updateLogFile(messages.projectObject + " " + "'" + this.projectModel.getName()
						+ "'" + " " + messages.objectUpdate, MessageType.SUCCESS);
			}
		} catch (UnauthorizedAccessException e) {
			CustomMessageDialog.openError(this.getShell(), messages.objectPermissionDialogTitle,
					messages.objectPermissionDialogMsg);
		}catch (Exception e) {
			if (e instanceof ResourceAccessException) {
				CustomMessageDialog.openError(Display.getCurrent().getActiveShell(), messages.errorDialogTitile,
						messages.serverNotReachable);
			}
			LOGGER.warn("Unable to Update Project data ! " + e);
		}
	}

	/**
	 * Method for Map VO object with model.
	 *
	 * @return the com.magna.xmbackend.vo.project. project request
	 *         {@link ProjectRequest}
	 */
	private com.magna.xmbackend.vo.project.ProjectRequest mapVOObjectWithModel() {
		com.magna.xmbackend.vo.project.ProjectRequest projectRequest = new com.magna.xmbackend.vo.project.ProjectRequest();

		projectRequest.setId(this.projectModel.getProjectId());
		projectRequest.setName(this.projectModel.getName());
		projectRequest.setIconId(this.projectModel.getIcon().getIconId());
		projectRequest.setStatus(this.projectModel.isActive() == true ? com.magna.xmbackend.vo.enums.Status.ACTIVE.name()
				: com.magna.xmbackend.vo.enums.Status.INACTIVE.name());

		List<com.magna.xmbackend.vo.project.ProjectTranslation> projectTranslationList = new ArrayList<>();
		LANG_ENUM[] lang_values = LANG_ENUM.values();
		for (int index = 0; index < lang_values.length; index++) {
			com.magna.xmbackend.vo.project.ProjectTranslation projectTranslation = new com.magna.xmbackend.vo.project.ProjectTranslation();
			projectTranslation.setLanguageCode(lang_values[index].getLangCode());
			projectTranslation.setRemarks(this.projectModel.getRemarks(lang_values[index]));
			projectTranslation.setDescription(this.projectModel.getDescription(lang_values[index]));
			if (this.projectModel.getOperationMode() == CommonConstants.OPERATIONMODE.CHANGE) {
				String translationId = projectModel.getTranslationId(lang_values[index]);
				projectTranslation
						.setId(XMSystemUtil.isEmpty(translationId) ? CommonConstants.EMPTY_STR : translationId);
			}
			projectTranslationList.add(projectTranslation);
		}

		projectRequest.setProjectTranslations(projectTranslationList);

		return projectRequest;
	}

	/**
	 * Gets the old model.
	 *
	 * @return the old model
	 */
	public Project getOldModel() {
		return oldModel;
	}

	/**
	 * Sets the old model.
	 *
	 * @param oldModel
	 *            the new old model
	 */
	public void setOldModel(Project oldModel) {
		this.oldModel = oldModel;
	}

	/**
	 * Gets the project model.
	 *
	 * @return the project model
	 */
	public Project getProjectModel() {
		return projectModel;
	}

	/**
	 * Sets the project model.
	 *
	 * @param projectModel
	 *            the new project model
	 */
	public void setProjectModel(final Project projectModel) {
		this.projectModel = projectModel;
	}

	/**
	 * Open desc dialog.
	 *
	 * @param shell
	 *            the shell
	 */
	private void openDescDialog(final Shell shell) {
		if (projectModel == null) {
			return;
		}
		if (projectModel.getOperationMode() != CommonConstants.OPERATIONMODE.VIEW) {
			String text = txtDesc.getText();
			LANG_ENUM currentLocaleEnum = XMAdminUtil.getInstance().getCurrentLocaleEnum();
			projectModel.setDescription(currentLocaleEnum, text);
		}
		final Map<LANG_ENUM, String> obModelMap = new HashMap<>();
		obModelMap.put(LANG_ENUM.ENGLISH, this.projectModel.getDescription(LANG_ENUM.ENGLISH));
		obModelMap.put(LANG_ENUM.GERMAN, this.projectModel.getDescription(LANG_ENUM.GERMAN));
		boolean isEditable = txtDesc.getEditable();
		this.controlModel = new ControlModel(this.messages.objectDescriptionLabel, obModelMap, Project.DESC_LIMIT,
				false, isEditable);
		this.controlModel.initDefaultLabels(this.messages);
		final XMAdminLangTextDialog dialogArea = new XMAdminLangTextDialog(shell, controlModel);
		int retVal = dialogArea.open();
		if (retVal == IDialogConstants.OK_ID) {
			final Map<LANG_ENUM, String> descriptionMap = this.projectModel.getDescriptionMap();
			descriptionMap.put(LANG_ENUM.ENGLISH, controlModel.getObjectModel(LANG_ENUM.ENGLISH));
			descriptionMap.put(LANG_ENUM.GERMAN, controlModel.getObjectModel(LANG_ENUM.GERMAN));
			updateDescWidget();
		}
	}

	/**
	 * Open remark dialog.
	 *
	 * @param shell
	 *            the shell
	 */
	private void openRemarkDialog(final Shell shell) {
		if (projectModel == null) {
			return;
		}
		if (projectModel.getOperationMode() != CommonConstants.OPERATIONMODE.VIEW) {
			String text = txtRemarks.getText();
			LANG_ENUM currentLocaleEnum = XMAdminUtil.getInstance().getCurrentLocaleEnum();
			projectModel.setRemarks(currentLocaleEnum, text);
		}
		final Map<LANG_ENUM, String> obModelMap = new HashMap<>();
		obModelMap.put(LANG_ENUM.ENGLISH, this.projectModel.getRemarks(LANG_ENUM.ENGLISH));
		obModelMap.put(LANG_ENUM.GERMAN, this.projectModel.getRemarks(LANG_ENUM.GERMAN));
		boolean isEditable = txtRemarks.getEditable();
		this.controlModel = new ControlModel(this.messages.objectRemarkLabel, obModelMap, Project.REMARK_LIMIT, false,
				isEditable);
		this.controlModel.initDefaultLabels(this.messages);
		final XMAdminLangTextAreaDialog dialogArea = new XMAdminLangTextAreaDialog(shell, controlModel);
		final int retVal = dialogArea.open();
		if (retVal == IDialogConstants.OK_ID) {
			Map<LANG_ENUM, String> notesMap = this.projectModel.getRemarksMap();
			notesMap.put(LANG_ENUM.ENGLISH, controlModel.getObjectModel(LANG_ENUM.ENGLISH));
			notesMap.put(LANG_ENUM.GERMAN, controlModel.getObjectModel(LANG_ENUM.GERMAN));
			updateRemarkWidget();
		}
	}

	/**
	 * Register messages.
	 *
	 * @param registry
	 *            the registry
	 */
	public void registerMessages(final MessageRegistry registry) {
		registry.register((text) -> {
			if (grpProject != null && !grpProject.isDisposed()) {
				grpProject.setText(text);
			}
		}, (message) -> {
			if (projectModel != null) {
				if (projectModel.getOperationMode() == CommonConstants.OPERATIONMODE.VIEW) {
					return message.objectGroupDisaplyLabel + " \'" + this.projectModel.getName() + "\'";
				} else if (projectModel.getOperationMode() == CommonConstants.OPERATIONMODE.CHANGE) {
					return message.objectGroupChangeLabel + " \'" + this.projectModel.getName() + "\'";
				} else if (projectModel.getOperationMode() == CommonConstants.OPERATIONMODE.CREATE) {
					return message.projectGroupCreateLabel;
				}
			}
			return CommonConstants.EMPTY_STR;
		});
		registry.register((text) -> {
			if (lblName != null && !lblName.isDisposed()) {
				lblName.setText(text);
			}
		}, (message) -> {
			if (lblName != null && !lblName.isDisposed()) {
				return getUpdatedWidgetText(message.objectNameLabel, lblName);
			}
			return CommonConstants.EMPTY_STR;
		});
		registry.register((text) -> {
			if (lblDescrition != null && !lblDescrition.isDisposed()) {
				lblDescrition.setText(text);
			}
		}, (message) -> {
			if (lblDescrition != null && !lblDescrition.isDisposed()) {
				return getUpdatedWidgetText(message.objectDescriptionLabel, lblDescrition);
			}
			return CommonConstants.EMPTY_STR;
		});
		registry.register((text) -> {
			if (lblActive != null && !lblActive.isDisposed()) {
				lblActive.setText(text);
			}
		}, (message) -> {
			if (lblActive != null && !lblActive.isDisposed()) {
				return getUpdatedWidgetText(message.objectActiveLabel, lblActive);
			}
			return CommonConstants.EMPTY_STR;
		});
		registry.register((text) -> {
			if (lblSymbol != null && !lblSymbol.isDisposed()) {
				lblSymbol.setText(text);
			}
		}, (message) -> {
			if (lblSymbol != null && !lblSymbol.isDisposed()) {
				return getUpdatedWidgetText(message.objectSymbolLabel, lblSymbol);
			}
			return CommonConstants.EMPTY_STR;
		});
		if (saveBtn != null) {
			registry.register((text) -> {
				if (saveBtn != null && !saveBtn.isDisposed()) {
					saveBtn.setText(text);
				}
			}, (message) -> {
				if (saveBtn != null && !saveBtn.isDisposed()) {
					return getUpdatedWidgetText(message.saveButtonText, saveBtn);
				}
				return CommonConstants.EMPTY_STR;
			});
		}
		if (cancelBtn != null) {
			registry.register((text) -> {
				if (cancelBtn != null && !cancelBtn.isDisposed()) {
					cancelBtn.setText(text);
				}
			}, (message) -> {
				if (cancelBtn != null && !cancelBtn.isDisposed()) {
					return getUpdatedWidgetText(message.cancelButtonText, cancelBtn);
				}
				return CommonConstants.EMPTY_STR;
			});
		}
		registry.register((text) -> {
			if (remarksTranslate != null && !remarksTranslate.isDisposed()) {
				remarksTranslate.setText(text);
			}
		}, (message) -> {
			if (remarksTranslate != null && !remarksTranslate.isDisposed()) {
				return getUpdatedWidgetText("<a>" + message.objectTranslationLinkText + "</a>", remarksTranslate);
			}
			return CommonConstants.EMPTY_STR;
		});

		registry.register((text) -> {
			if (remarksLabel != null && !remarksLabel.isDisposed()) {
				remarksLabel.setText(text);
			}
		}, (message) -> {
			if (remarksLabel != null && !remarksLabel.isDisposed()) {
				return getUpdatedWidgetText(message.objectRemarkLabel, remarksLabel);
			}
			return CommonConstants.EMPTY_STR;
		});

		registry.register((text) -> {
			if (descLink != null && !descLink.isDisposed()) {
				descLink.setText(text);
			}
		}, (message) -> {
			if (descLink != null && !descLink.isDisposed()) {
				return getUpdatedWidgetText("<a>" + message.objectTranslationLinkText + "</a>", descLink);
			}
			return CommonConstants.EMPTY_STR;
		});

		registry.register((text) -> {
			if (txtDesc != null && !txtDesc.isDisposed()) {
				txtDesc.setText(text);
				updateDescWidget();
			}
		}, (message) -> {
			if (this.projectModel != null && txtDesc != null && !txtDesc.isDisposed()) {
				LANG_ENUM langEnum = XMAdminUtil.getInstance().getCurrentLocaleEnum();
				return this.projectModel.getDescription(langEnum) == null ? CommonConstants.EMPTY_STR
						: this.projectModel.getDescription(langEnum);
			}
			return CommonConstants.EMPTY_STR;
		});

		registry.register((text) -> {
			if (txtRemarks != null && !txtRemarks.isDisposed()) {
				txtRemarks.setText(text);
				updateRemarkWidget();
			}
		}, (message) -> {
			if (this.projectModel != null && txtRemarks != null && !txtRemarks.isDisposed()) {
				LANG_ENUM langEnum = XMAdminUtil.getInstance().getCurrentLocaleEnum();
				return this.projectModel.getRemarks(langEnum) == null ? CommonConstants.EMPTY_STR
						: this.projectModel.getRemarks(langEnum);
			}
			return CommonConstants.EMPTY_STR;
		});
	}

	/**
	 * Gets the updated widget text.
	 *
	 * @param message
	 *            the message
	 * @param control
	 *            the control
	 * @return the updated widget text
	 */
	private String getUpdatedWidgetText(String message, Control control) {
		control.requestLayout();
		control.getParent().redraw();
		control.getParent().getParent().update();
		control.getParent().getParent().getParent().update();
		return message;
	}

	/**
	 * Bind values.
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public void bindValues() {
		try {
			// Name field binding
			widgetValue = WidgetProperties.text(SWT.Modify).observe(this.txtName);
			modelValue = BeanProperties.value(Project.class, Project.PROPERTY_PROJECTNAME).observe(this.projectModel);
			widgetValue.addValueChangeListener(new IValueChangeListener() {

				@Override
				public void handleValueChange(ValueChangeEvent event) {
					updateButtonStatus(event);
				}
			});

			// define the UpdateValueStrategy
			final UpdateValueStrategy update = new UpdateValueStrategy();
			update.setAfterGetValidator(new NameValidation(messages, StatusValidation.PROJECT));
			bindValue = dataBindContext.bindValue(widgetValue, modelValue, update, null);
			ControlDecorationSupport.create(bindValue, SWT.TOP | SWT.LEFT);

			// Action check box binding
			widgetValue = WidgetProperties.selection().observe(this.activeBtn);
			modelValue = BeanProperties.value(Project.class, Project.PROPERTY_ACTIVE).observe(this.projectModel);
			bindValue = dataBindContext.bindValue(widgetValue, modelValue);

			// Symbol toolItem setup
			Icon icon;
			String iconPath;
			if ((icon = this.projectModel.getIcon()) != null && !XMSystemUtil.isEmpty(iconPath = icon.getIconPath())) {
				if (iconPath.contains("null")) { //$NON-NLS-1$
					toolItem.setImage(XMSystemUtil.getInstance().getImage(this.getClass(), "icons/16x16/browse.png")); //$NON-NLS-1$
				} else {
					toolItem.setImage(XMSystemUtil.getInstance().getImage(this.getClass(), iconPath, true, true));

					// Symbol toolItem binding
					widgetValue = WidgetProperties.tooltipText().observe(this.toolItem);
					modelValue = BeanProperties
							.value(Project.class, Project.PROPERTY_ICON + "." + Icon.PROPERTY_ICONNAME, Icon.class)
							.observe(this.projectModel);
					bindValue = dataBindContext.bindValue(widgetValue, modelValue);

					// Symbol Field binding
					widgetValue = WidgetProperties.text(SWT.Modify).observe(this.txtSymbol);
					modelValue = BeanProperties
							.value(Project.class, Project.PROPERTY_ICON + "." + Icon.PROPERTY_ICONNAME, Icon.class)
							.observe(this.projectModel);
					final UpdateValueStrategy symbolUpdate = new UpdateValueStrategy();
					symbolUpdate.setAfterGetValidator(new SymbolValidation());
					bindValue = dataBindContext.bindValue(widgetValue, modelValue, symbolUpdate, null);
					ControlDecorationSupport.create(bindValue, SWT.TOP | SWT.LEFT);
				}
			}
			ControlDecorationSupport.create(bindValue, SWT.TOP | SWT.LEFT);
		}

		catch (Exception e) {
			LOGGER.error("Exception occured while databinding", e);
		}
	}

	/**
	 * Update button status.
	 *
	 * @param event
	 *            the event
	 */
	@SuppressWarnings("rawtypes")
	private void updateButtonStatus(final ValueChangeEvent event) {
		String name = (String) event.getObservableValue().getValue();
		if (this.saveBtn != null) {
			if (XMSystemUtil.isEmpty(name) || name.trim().length() == 0
					|| (!name.matches(CommonConstants.RegularExpressions.ALLOWED_PROJECT_NAME_REGEX))) {
				this.saveBtn.setEnabled(false);
			} else {
				this.saveBtn.setEnabled(true);
			}
		}
	}

	/**
	 * Set operation mode.
	 */
	public void setOperationMode() {
		if (this.projectModel != null) {
			if (projectModel.getOperationMode() == CommonConstants.OPERATIONMODE.VIEW) {
				this.txtName.setEditable(false);
				this.activeBtn.setEnabled(false);
				this.txtDesc.setEditable(false);
				this.txtRemarks.setEditable(false);
				this.toolItem.setData("editable", false);
				setShowButtonBar(false);
			} else if (projectModel.getOperationMode() == CommonConstants.OPERATIONMODE.CREATE) {
				this.txtName.setEditable(true);
				this.activeBtn.setEnabled(true);
				this.saveBtn.setEnabled(false);
				this.txtDesc.setEditable(true);
				this.txtRemarks.setEditable(true);
				this.toolItem.setData("editable", true);
				setShowButtonBar(true);
				this.dirty.setDirty(true);
			} else if (projectModel.getOperationMode() == CommonConstants.OPERATIONMODE.CHANGE) {
				this.txtName.setEditable(true);
				this.activeBtn.setEnabled(true);
				this.toolItem.setData("editable", true);
				this.txtDesc.setEditable(true);
				this.txtRemarks.setEditable(true);
				setShowButtonBar(true);
				this.dirty.setDirty(true);
			} else {
				this.txtName.setEditable(false);
				this.activeBtn.setEnabled(false);
				this.txtDesc.setEditable(false);
				this.txtRemarks.setEditable(false);
				this.toolItem.setData("editable", false);
				setShowButtonBar(false);
			}
		}
	}

	/**
	 * Update desc widget.
	 */
	public void updateDescWidget() {
		if (this.projectModel == null) {
			return;
		}
		int operationMode = this.projectModel.getOperationMode();
		final LANG_ENUM currentLocaleEnum = XMAdminUtil.getInstance().getCurrentLocaleEnum();
		final String descForCurLocale = this.projectModel.getDescription(currentLocaleEnum) == null
				? CommonConstants.EMPTY_STR : this.projectModel.getDescription(currentLocaleEnum);
		if (operationMode != CommonConstants.OPERATIONMODE.VIEW) {
			this.txtDesc.setText(descForCurLocale);
			return;
		}
		if (operationMode == CommonConstants.OPERATIONMODE.VIEW) {
			if (!XMSystemUtil.isEmpty(descForCurLocale)) {
				this.txtDesc.setText(descForCurLocale);
				return;
			}
			final String descriptionEN = this.projectModel.getDescription(LANG_ENUM.ENGLISH);
			final String descriptionDE = this.projectModel.getDescription(LANG_ENUM.GERMAN);
			if (!XMSystemUtil.isEmpty(descriptionEN)) {
				this.txtDesc.setText(descriptionEN);
				return;
			}

			if (!XMSystemUtil.isEmpty(descriptionDE)) {
				this.txtDesc.setText(descriptionDE);
				return;
			}
		}
	}

	/**
	 * Update remark widget.
	 */
	public void updateRemarkWidget() {
		if (this.projectModel == null) {
			return;
		}

		int operationMode = this.projectModel.getOperationMode();
		final LANG_ENUM currentLocaleEnum = XMAdminUtil.getInstance().getCurrentLocaleEnum();
		final String remarkForCurLocale = this.projectModel.getRemarks(currentLocaleEnum) == null
				? CommonConstants.EMPTY_STR : this.projectModel.getRemarks(currentLocaleEnum);
		if (operationMode != CommonConstants.OPERATIONMODE.VIEW) {
			this.txtRemarks.setText(remarkForCurLocale);
			return;
		}
		if (operationMode == CommonConstants.OPERATIONMODE.VIEW) {
			if (!XMSystemUtil.isEmpty(remarkForCurLocale)) {
				this.txtRemarks.setText(remarkForCurLocale);
				return;
			}
			final String remarkEN = this.projectModel.getRemarks(LANG_ENUM.ENGLISH);
			final String remarkDE = this.projectModel.getRemarks(LANG_ENUM.GERMAN);
			if (!XMSystemUtil.isEmpty(remarkEN)) {
				this.txtRemarks.setText(remarkEN);
				return;
			}

			if (!XMSystemUtil.isEmpty(remarkDE)) {
				this.txtRemarks.setText(remarkDE);
				return;
			}
		}
	}

	/**
	 * set the project model from selection.
	 */
	public void setProject() {
		try {
			final AdminTreeviewer adminTree = XMAdminUtil.getInstance().getAdminTree();
			final Object selectionObj = adminTree.getSelection();
			if (selectionObj instanceof IStructuredSelection) {
				Object firstElement = ((IStructuredSelection) selectionObj).getFirstElement();
				if (firstElement instanceof Project) {
					setOldModel((Project) firstElement);
					Project rightHandObject = this.getOldModel().deepCopyProject(false, null);
					setProjectModel(rightHandObject);
					registerMessages(this.registry);
					bindValues();
					setOperationMode();
				}
			}
		} catch (Exception e) {
			LOGGER.warn("Unable to set project model selection ! " + e);
		}
	}

	public void setModel(Project project) {
		try {
			setOldModel(null);
			setProjectModel(project);
			registerMessages(this.registry);
			bindValues();
			setOperationMode();
			updateDescWidget();
			updateRemarkWidget();
		} catch (Exception e) {
			LOGGER.warn("Unable to set project model ! " + e);
		}
	}

	/**
	 * Sets the dirty object.
	 *
	 * @param dirty
	 *            the new dirty object
	 */
	@Persist
	public void setDirtyObject(final MDirtyable dirty) {
		this.dirty = dirty;
	}
}
