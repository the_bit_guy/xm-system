package com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.userapplicationmodel;

import java.util.HashMap;
import java.util.Map;

import com.magna.xmsystem.dependencies.utils.LANG_ENUM;
import com.magna.xmsystem.dependencies.utils.XMSystemUtil;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.icon.Icon;
import com.magna.xmsystem.xmadmin.util.CommonConstants;

/**
 * Class for User application.
 *
 * @author archita.patel
 */
public class UserApplicationChild extends UserApplication {

	/**
	 * Instantiates a new user application child.
	 *
	 * @param userApplicationId the user application id
	 * @param name the name
	 * @param description the description
	 * @param nameMap the name map
	 * @param isActive the is active
	 * @param icon the icon
	 * @param operationMode the operation mode
	 */
	public UserApplicationChild(final String userApplicationId, final String name, final String description, final Map<LANG_ENUM, String> nameMap,
			final boolean isActive, final Icon icon, final int operationMode) {
		this(userApplicationId, name, description, nameMap, isActive, new HashMap<>(), new HashMap<>(), icon, false, false, null, null,
				operationMode);
	}
	public UserApplicationChild(final String userApplicationId, final String name, final String description, final Map<LANG_ENUM, String> nameMap,
			final boolean active, final Map<LANG_ENUM, String> descriptionMap, final Map<LANG_ENUM, String> remarksMap,
			final Icon icon, final boolean parent, final boolean singleton, final String position,
			final String baseApplicationId, final int operationMode) {
		super(userApplicationId, name, description, nameMap, active, descriptionMap, remarksMap, icon, parent, singleton, position,
				baseApplicationId, operationMode);
		getUserApplicationChildren().clear();

	}

	/**
	 * Method for Deep copy user application.
	 *
	 * @param update
	 *            {@link boolean}
	 * @param updateThisObject
	 *            {@link UserApplication}
	 * @return the user application {@link UserApplication}
	 */
	public UserApplicationChild deepCopyUserApplicationChild(boolean update, UserApplicationChild updateThisObject) {
		UserApplicationChild clonedUserApplicationChild = null;
		String currentUserApplicationId = XMSystemUtil.isEmpty(this.getUserApplicationId()) ? CommonConstants.EMPTY_STR
				: this.getUserApplicationId();
		String currentInternalName = this.getName();
		String currentInternalDesc = this.getDescription();
		boolean currentIsActive = this.isActive();
		boolean currentIsParent = this.isParent();
		boolean currentIsSingleton = this.isSingleton();
		String currentBaseAppId = XMSystemUtil.isEmpty(this.getBaseApplicationId()) ? null
				: this.getBaseApplicationId();
		String currentPosition = XMSystemUtil.isEmpty(this.getPosition()) ? CommonConstants.EMPTY_STR
				: this.getPosition();
		Icon currentIcon = this.getIcon() == null ? null : this.getIcon();

		Map<LANG_ENUM, String> currentTranslationIdMap = new HashMap<>();
		Map<LANG_ENUM, String> currentNameMap = new HashMap<>();
		Map<LANG_ENUM, String> currentDescriptionMap = new HashMap<>();
		Map<LANG_ENUM, String> currentRemarksMap = new HashMap<>();

		for (LANG_ENUM langEnum : LANG_ENUM.values()) {
			String translationId = this.getTranslationId(langEnum);
			currentTranslationIdMap.put(langEnum,
					XMSystemUtil.isEmpty(translationId) ? CommonConstants.EMPTY_STR : translationId);
			String name = this.getName(langEnum);
			currentNameMap.put(langEnum, XMSystemUtil.isEmpty(name) ? CommonConstants.EMPTY_STR : name);
			String description = this.getDescription(langEnum);
			currentDescriptionMap.put(langEnum,
					XMSystemUtil.isEmpty(description) ? CommonConstants.EMPTY_STR : description);
			String remarks = this.getRemarks(langEnum);
			currentRemarksMap.put(langEnum, XMSystemUtil.isEmpty(remarks) ? CommonConstants.EMPTY_STR : remarks);
		}

		if (update) {
			clonedUserApplicationChild = updateThisObject;
		} else {
			clonedUserApplicationChild = new UserApplicationChild(currentUserApplicationId, currentInternalName, currentInternalDesc, currentNameMap,
					currentIsActive, currentIcon, CommonConstants.OPERATIONMODE.VIEW);
		}

		clonedUserApplicationChild.setUserApplicationId(currentUserApplicationId);
		clonedUserApplicationChild.setActive(currentIsActive);
		clonedUserApplicationChild.setIcon(currentIcon);
		clonedUserApplicationChild.setNameMap(currentNameMap);
		clonedUserApplicationChild.setTranslationIdMap(currentTranslationIdMap);
		clonedUserApplicationChild.setDescriptionMap(currentDescriptionMap);
		clonedUserApplicationChild.setRemarksMap(currentRemarksMap);
		clonedUserApplicationChild.setParent(currentIsParent);
		clonedUserApplicationChild.setSingleton(currentIsSingleton);
		clonedUserApplicationChild.setBaseApplicationId(currentBaseAppId);
		clonedUserApplicationChild.setPosition(currentPosition);

		return clonedUserApplicationChild;

	}

}
