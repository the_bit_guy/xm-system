package com.magna.xmsystem.xmadmin.ui.handlers.toolbar;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.e4.core.di.annotations.CanExecute;
import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.TreePath;
import org.eclipse.jface.viewers.TreeSelection;

import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.AdminTreeviewer;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.adminarea.AdministrationAreas;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.Applications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.Configurations;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.directory.Directories;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.group.Groups;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.projects.Projects;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.sitemodel.Sites;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.users.Users;
import com.magna.xmsystem.xmadmin.util.XMAdminUtil;

/**
 * The Class RootSelectionToolBarHandler.
 * 
 * @author subash.janarthanan
 * 
 */
public class RootSelectionToolBarHandler {

	/**
	 * Execute.
	 */
	@Execute
	public void execute() {
		final AdminTreeviewer adminTree = XMAdminUtil.getInstance().getAdminTree();
		IStructuredSelection selection = adminTree.getStructuredSelection();
		Object selectionObj = selection.getFirstElement();
		List<IAdminTreeChild> path = new ArrayList<IAdminTreeChild>();

		if (selectionObj instanceof IAdminTreeChild) {
			IAdminTreeChild projectAppSiteAdminModel = (IAdminTreeChild) selectionObj;
			path.add(projectAppSiteAdminModel.getParent());
		} else {
			return;
		}
		adminTree.setSelection(new TreeSelection(new TreePath(path.toArray())), true);
	}

	/**
	 * Can execute.
	 *
	 * @return true, if successful
	 */
	@CanExecute
	public boolean canExecute() {
		XMAdminUtil instance = XMAdminUtil.getInstance();
		if(instance != null){
			final AdminTreeviewer adminTree = instance.getAdminTree();
			IStructuredSelection selection = adminTree.getStructuredSelection();
			Object selectionObj = selection.getFirstElement();
			if (selectionObj instanceof Sites || selectionObj instanceof Projects
					|| selectionObj instanceof AdministrationAreas || selectionObj instanceof Users
					|| selectionObj instanceof Applications || selectionObj instanceof Groups
					|| selectionObj instanceof Configurations || selectionObj == null || selectionObj instanceof Directories) {
				return false;
			}
		} else if (instance == null){
			return false;
		}
		return true;
	}
}
