package com.magna.xmsystem.xmadmin.ui.parts.application.userapplication;

import org.eclipse.jface.fieldassist.ControlDecoration;
import org.eclipse.jface.layout.GridDataFactory;
import org.eclipse.jface.layout.GridLayoutFactory;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.ScrolledComposite;
import org.eclipse.swt.events.ControlAdapter;
import org.eclipse.swt.events.ControlEvent;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Link;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.widgets.ToolBar;
import org.eclipse.swt.widgets.ToolItem;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.magna.xmsystem.dependencies.utils.XMSystemUtil;
import com.magna.xmsystem.xmadmin.ui.dialogs.lang.XMAdminLangTextAreaDialog;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.userapplicationmodel.UserApplication;
import com.magna.xmsystem.xmadmin.util.XMAdminUtil;

/**
 * Class for User application composite UI.
 *
 * @author Chiranjeevi.Akula
 */
public abstract class UserApplicationCompositeUI extends Composite {

	/** The Constant LOGGER. */
	private static final Logger LOGGER = LoggerFactory.getLogger(UserApplicationCompositeUI.class);

	/** Member variable 'cancel btn' for {@link Button}. */
	protected Button saveBtn, cancelBtn;

	/** Member variable 'grp user application' for {@link Group}. */
	protected Group grpUserApplication;

	/** Member variable 'lbl name' for {@link Label}. */
	protected Label lblName;

	/** Member variable 'lbl descrition' for {@link Label}. */
	protected Label lblDescrition;

	/** Member variable 'lbl active' for {@link Label}. */
	protected Label lblActive;

	/** Member variable 'lbl position' for {@link Label}. */
	protected Label lblPosition;

	/** Member variable 'lbl base application' for {@link Label}. */
	protected Label lblBaseApplication;

	/** Member variable 'lbl symbol' for {@link Label}. */
	protected Label lblSymbol;

	/** Member variable 'active btn' for {@link Button}. */
	protected Button activeBtn;

	/** Member variable 'lbl parent' for {@link Label}. */
	protected Label lblParent;

	/** Member variable 'parent btn' for {@link Button}. */
	protected Button parentBtn;

	/** Member variable 'txt symbol' for {@link Text}. */
	protected Text txtSymbol;

	/** Member variable 'parent shell' for {@link Shell}. */
	protected Shell parentShell;

	/** Member variable 'tool item' for {@link ToolItem}. */
	protected ToolItem toolItem;

	/** Member variable 'txt position' for {@link Text}. */
	protected Text txtPosition;

	/** Member variable 'txt base application' for {@link Text}. */
	protected Text txtBaseApplication;

	/** Member variable 'txt desc' for {@link Text}. */
	protected Text txtDesc;

	/** Member variable 'txt name' for {@link Text}. */
	protected Text txtName;

	/** Member variable 'desc translate link' for {@link Link}. */
	protected Link descTranslateLink;

	/** Member variable 'name translate link' for {@link Link}. */
	protected Link nameTranslateLink;

	/** Member variable 'remarks label' for {@link Label}. */
	protected Label remarksLabel;

	/** Member variable 'txt remarks' for {@link Text}. */
	protected Text txtRemarks;

	/** Member variable 'remarks translate link' for {@link Link}. */
	protected Link remarksTranslateLink;

	/** Member variable 'lbl remarks count' for {@link Label}. */
	protected Label lblRemarksCount;

	/**
	 * Member variable 'position content prop adapter' for
	 * {@link XMContentProposalAdapter}.
	 *//*
	protected XMContentProposalAdapter positionContentPropAdapter;

	*//**
	 * Member variable 'base app content prop adapter' for
	 * {@link XMContentProposalAdapter}.
	 *//*
	protected XMContentProposalAdapter baseAppContentPropAdapter;*/

	/** Member variable 'txt position deco' for {@link ControlDecoration}. */
	protected ControlDecoration txtPositionDeco;

	/**
	 * Member variable 'txt base application deco' for
	 * {@link ControlDecoration}.
	 */
	protected ControlDecoration txtBaseApplicationDeco;

	/** The lbl nameG. */
	protected Label lblNameG;

	/** The txt nameG. */
	protected Text txtNameG;

	/** The lbl singleton. */
	protected Label lblSingleton;

	/** The btn singleton. */
	protected Button btnSingleton;

	/** The program tool item. */
	protected ToolItem programToolItem;

	/** The program app tool item. */
	protected ToolItem programAppToolItem;

	/**
	 * Constructor for UserApplicationCompositeUI Class.
	 *
	 * @param parent
	 *            {@link Composite}
	 * @param style
	 *            {@link int}
	 */
	public UserApplicationCompositeUI(final Composite parent, final int style) {
		super(parent, style);
		this.initGUI();
		setShowButtonBar(false);
	}

	/**
	 * Method for Inits the GUI.
	 */
	private void initGUI() {
		try {
			GridLayoutFactory.fillDefaults().applyTo(this);
			this.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));

			this.grpUserApplication = new Group(this, SWT.NONE);
			this.grpUserApplication.setBackgroundMode(SWT.INHERIT_FORCE);
			GridLayoutFactory.fillDefaults().numColumns(1).applyTo(this.grpUserApplication);
			GridDataFactory.fillDefaults().grab(true, true).align(SWT.FILL, SWT.FILL).applyTo(this.grpUserApplication);
			final ScrolledComposite scrolledComposite = XMAdminUtil.getInstance().createScrolledComposite(this.grpUserApplication);
			scrolledComposite.setBackgroundMode(SWT.INHERIT_FORCE);
			
			final Composite widgetContainer = new Composite(scrolledComposite, SWT.NONE);
			final GridLayout widgetContLayout = new GridLayout(3, false);

			widgetContainer.setLayout(widgetContLayout);
			widgetContainer.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));

			this.lblName = new Label(widgetContainer, SWT.NONE);
			this.txtName = new Text(widgetContainer, SWT.BORDER);
			this.txtName.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));
			this.txtName.setTextLimit(UserApplication.NAME_LIMIT);
			this.nameTranslateLink = new Link(widgetContainer, SWT.NONE);

			this.lblActive = new Label(widgetContainer, SWT.NONE);
			this.activeBtn = new Button(widgetContainer, SWT.CHECK);

			GridDataFactory.fillDefaults().grab(false, false).align(SWT.LEFT, SWT.CENTER).applyTo(this.lblActive);
			GridDataFactory.fillDefaults().grab(false, false).align(SWT.LEFT, SWT.CENTER).span(2, 1).applyTo(this.activeBtn);

			this.lblDescrition = new Label(widgetContainer, SWT.NONE);
			this.txtDesc = new Text(widgetContainer, SWT.BORDER);
			this.txtDesc.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));
			this.txtDesc.setTextLimit(UserApplication.DESCRIPTION_LIMIT);
			this.descTranslateLink = new Link(widgetContainer, SWT.NONE);

			this.lblSymbol = new Label(widgetContainer, SWT.NONE);
			this.txtSymbol = new Text(widgetContainer, SWT.BORDER);
			this.txtSymbol.setEditable(false);
			GridDataFactory.fillDefaults().grab(true, false).span(1, 1)./*indent(2, 0).*/align(SWT.FILL, SWT.CENTER)
					.applyTo(this.txtSymbol);
			final ToolBar toolbar = new ToolBar(widgetContainer, SWT.NONE);
			this.toolItem = new ToolItem(toolbar, SWT.FLAT);
			this.toolItem
					.setImage(XMSystemUtil.getInstance().getImage(this.getClass(), "icons/16x16/browse.png"));
			toolbar.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false));

			this.remarksLabel = new Label(widgetContainer, SWT.NONE);
			this.remarksTranslateLink = new Link(widgetContainer, SWT.NONE);
			this.remarksTranslateLink.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, true, false, 1, 1));

			this.lblRemarksCount = new Label(widgetContainer, SWT.BORDER | SWT.CENTER);
			final GridData gridDataRemarksCount = new GridData(SWT.LEFT, SWT.CENTER, false, false);
			gridDataRemarksCount.widthHint = 70;
			this.lblRemarksCount.setLayoutData(gridDataRemarksCount);

			this.txtRemarks = new Text(widgetContainer,
					SWT.BORDER | SWT.MULTI | SWT.WRAP | SWT.H_SCROLL | SWT.V_SCROLL);
			final GridData gridData = new GridData(SWT.FILL, SWT.FILL, true, true);
			gridData.minimumHeight = 100;
			gridData.horizontalSpan = 3;
			this.txtRemarks.setLayoutData(gridData);
			this.txtRemarks.setTextLimit(UserApplication.REMARK_LIMIT);
			this.lblRemarksCount.setText(XMAdminLangTextAreaDialog.LIMIT_COUNT_PADDING
					+ this.txtRemarks.getText().length() + XMAdminLangTextAreaDialog.SLASH
					+ UserApplication.REMARK_LIMIT + XMAdminLangTextAreaDialog.LIMIT_COUNT_PADDING);


			this.lblParent = new Label(widgetContainer, SWT.NONE);
			this.parentBtn = new Button(widgetContainer, SWT.CHECK);

			GridDataFactory.fillDefaults().grab(false, false).align(SWT.LEFT, SWT.CENTER).indent(0, 3)
					.applyTo(this.lblParent);
			GridDataFactory.fillDefaults().grab(false, false).align(SWT.LEFT, SWT.CENTER).indent(2, 3).span(2, 1)
					.applyTo(this.parentBtn);

			this.lblSingleton = new Label(widgetContainer, SWT.NONE);
			this.btnSingleton = new Button(widgetContainer, SWT.CHECK);
			GridDataFactory.fillDefaults().grab(false, false).align(SWT.LEFT, SWT.CENTER).indent(0, 3)
					.applyTo(this.lblSingleton);
			GridDataFactory.fillDefaults().grab(false, false).align(SWT.LEFT, SWT.CENTER).indent(2, 3).span(2, 1)
					.applyTo(this.btnSingleton);
			
			this.lblPosition = new Label(widgetContainer, SWT.NONE);
			this.txtPosition = new Text(widgetContainer, SWT.BORDER);
			this.txtPosition.setEditable(false);
		
			GridDataFactory.fillDefaults().grab(true, false).span(1, 1).indent(2, 0).align(SWT.FILL, SWT.CENTER)
					.applyTo(this.txtPosition);
			this.programToolItem = new ToolItem(new ToolBar(widgetContainer, SWT.NONE), SWT.FLAT);
			this.programToolItem.setText(" ... ");
			toolbar.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false));

			this.lblBaseApplication = new Label(widgetContainer, SWT.NONE);
			this.txtBaseApplication = new Text(widgetContainer, SWT.BORDER);
			this.txtBaseApplication.setEditable(false);
	
			GridDataFactory.fillDefaults().grab(true, false).span(1, 1).indent(2, 0).align(SWT.FILL, SWT.CENTER)
					.applyTo(this.txtBaseApplication);

			this.programAppToolItem = new ToolItem(new ToolBar(widgetContainer, SWT.NONE), SWT.FLAT);
			this.programAppToolItem.setText(" ... ");
			toolbar.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false));

			final Composite buttonBarComp = new Composite(this.grpUserApplication, SWT.NONE);
			final GridLayout btnBarCompLayout = new GridLayout(2, true);
			btnBarCompLayout.marginRight = 0;
			btnBarCompLayout.marginLeft = 0;
			btnBarCompLayout.marginTop = 0;
			btnBarCompLayout.marginBottom = 0;
			btnBarCompLayout.marginWidth = 0;
			buttonBarComp.setLayout(btnBarCompLayout);
			buttonBarComp.setLayoutData(new GridData(SWT.FILL, SWT.DEFAULT, true, false, 3, 1));
			createButtonBar(buttonBarComp);
			
			scrolledComposite.setContent(widgetContainer);
			scrolledComposite.setSize(widgetContainer.getSize());
			scrolledComposite.setExpandVertical(true);
			scrolledComposite.setExpandHorizontal(true);
			scrolledComposite.update();

			scrolledComposite.addControlListener(new ControlAdapter() {
				public void controlResized(final ControlEvent e) {
					Rectangle rectangle = scrolledComposite.getClientArea();
					scrolledComposite.setMinSize(widgetContainer.computeSize(rectangle.width, SWT.DEFAULT));
				}
			});
		} catch (Exception ex) {
			LOGGER.error("Unable to crete UI elements", ex); //$NON-NLS-1$
		}
	}

	/**
	 * Method for Creates the button bar.
	 *
	 * @param buttonBarComp
	 *            {@link Composite}
	 */
	private void createButtonBar(final Composite buttonBarComp) {
		this.saveBtn = new Button(buttonBarComp, SWT.NONE);
		this.saveBtn.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, true, false));

		this.cancelBtn = new Button(buttonBarComp, SWT.NONE);
		this.cancelBtn.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, true, false));
	}

	/**
	 * Gets the grp user.
	 *
	 * @return the grp user
	 */
	public Group getGrpUser() {
		return grpUserApplication;
	}

	/**
	 * Gets the lbl name.
	 *
	 * @return the lbl name
	 */
	public Label getLblName() {
		return lblName;
	}

	/**
	 * Gets the parent shell.
	 *
	 * @return the parent shell
	 */
	public Shell getParentShell() {
		return parentShell;
	}

	/**
	 * Gets the lbl descrition.
	 *
	 * @return the lbl descrition
	 */
	public Label getLblDescrition() {
		return lblDescrition;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.swt.widgets.Widget#dispose()
	 */
	@Override
	public void dispose() {
		super.dispose();
	}

	/**
	 * Sets the show button bar.
	 *
	 * @param showButtonBar
	 *            the new show button bar
	 */
	public void setShowButtonBar(final boolean showButtonBar) {
		if (this.saveBtn != null && !this.saveBtn.isDisposed() && this.cancelBtn != null
				&& !this.cancelBtn.isDisposed()) {
			GridData layoutData = (GridData) this.saveBtn.getParent().getLayoutData();
			layoutData.exclude = !showButtonBar;
			this.saveBtn.setVisible(showButtonBar);
			this.saveBtn.getParent().setVisible(showButtonBar);
			this.cancelBtn.setVisible(showButtonBar);
			this.saveBtn.getParent().requestLayout();
			this.saveBtn.getParent().redraw();
			this.saveBtn.getParent().getParent().update();
		}
	}
}
