package com.magna.xmsystem.xmadmin.ui.handlers.userapplication;

import java.util.HashMap;

import javax.inject.Inject;

import org.eclipse.e4.core.di.annotations.CanExecute;
import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.core.services.nls.Translation;
import org.eclipse.e4.ui.model.application.MApplication;
import org.eclipse.e4.ui.model.application.ui.basic.MPart;
import org.eclipse.e4.ui.workbench.modeling.EModelService;
import org.eclipse.swt.custom.StackLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.client.ResourceAccessException;

import com.magna.xmsystem.dependencies.customExceptions.UnauthorizedAccessException;
import com.magna.xmsystem.dependencies.utils.LANG_ENUM;
import com.magna.xmsystem.dependencies.utils.XMSystemUtil;
import com.magna.xmsystem.ui.controls.widgets.CustomMessageDialog;
import com.magna.xmsystem.xmadmin.message.Message;
import com.magna.xmsystem.xmadmin.ui.parts.IEditablePart;
import com.magna.xmsystem.xmadmin.ui.parts.InformationPart;
import com.magna.xmsystem.xmadmin.ui.parts.application.userapplication.UserApplicationCompositeAction;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.AdminTreeviewer;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.userapplicationmodel.UserApplication;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.icon.Icon;
import com.magna.xmsystem.xmadmin.util.CommonConstants;
import com.magna.xmsystem.xmadmin.util.XMAdminUtil;

/**
 * Class for Create user app handler.
 *
 * @author Chiranjeevi.Akula
 */
public class CreateUserAppHandler {
	
	/** The Constant LOGGER. */
	private static final Logger LOGGER = LoggerFactory.getLogger(CreateUserAppHandler.class);

	/** Member variable 'model service' for {@link EModelService}. */
	@Inject
	private EModelService modelService;

	/** Member variable 'application' for {@link MApplication}. */
	@Inject
	private MApplication application;

	/** Member variable for messages. */
	@Inject
	@Translation
	transient private Message messages;
	
	/**
	 * Method for Execute.
	 *
	 * @param shell
	 *            {@link Shell}
	 */
	@Execute
	public void execute(final Shell shell) {
		try {
			if (XMAdminUtil.getInstance().isAcessAllowed("USER_APPLICATION-CREATE")) {
				MPart part = (MPart) this.modelService.find(CommonConstants.PART_ID.INFORMATION_PART_ID, application);
				InformationPart rightSidePart = (InformationPart) part.getObject();
				Composite rightcomposite = rightSidePart.getComposite();
				StackLayout rightCompLayout = rightSidePart.getCompLayout();
				UserApplicationCompositeAction userCompositeUI = rightSidePart.getUserAppCompositeUI();
				rightCompLayout.topControl = userCompositeUI;
				final AdminTreeviewer adminTree = XMAdminUtil.getInstance().getAdminTree();
				Icon icon = XMAdminUtil.getInstance().getIconByName("ausertasks.png");
				UserApplication userApplication = new UserApplication(null, CommonConstants.EMPTY_STR,
						CommonConstants.EMPTY_STR, new HashMap<>(), false, icon, CommonConstants.OPERATIONMODE.CREATE);
				String timeStampe = messages.remarkCreatedbyLabel + ": " + XMSystemUtil.getSystemUserName() + "\n"
						+ messages.remarkDateLabel + ": " + XMSystemUtil.getTimeStamp();
				for (LANG_ENUM langEnum : LANG_ENUM.values()) {
					userApplication.getRemarksMap().put(langEnum,
							XMSystemUtil.isEmpty(timeStampe) ? CommonConstants.EMPTY_STR : timeStampe);
				}
				userCompositeUI.setModel(userApplication);
				adminTree.setSelection(null);
				adminTree.setPreviousSelection(null);
				rightcomposite.layout();
			} else {
				CustomMessageDialog.openError(Display.getCurrent().getActiveShell(),
						messages.objectPermissionDialogTitle, messages.objectPermissionDialogMsg);
			}
		} catch (UnauthorizedAccessException e) {
			LOGGER.error("Current user is Unauthorized " + e);
		} catch (Exception e) {
			if (e instanceof ResourceAccessException) {
				CustomMessageDialog.openError(Display.getCurrent().getActiveShell(), messages.errorDialogTitile,
						messages.serverNotReachable);
			}
			LOGGER.error("Exception occured in calling access allowed API " + e);
		}
	}

	/**
	 * Checks if is acess allowed.
	 *
	 * @return true, if is acess allowed
	 */
	/*public boolean isAcessAllowed(){
		boolean returnVal = true;
		try {
			ValidationController controller = new ValidationController();
			ValidationRequest request = new ValidationRequest();
			request.setUserName(RestClientUtil.getInstance().getUserName());
			request.setPermissionType(PermissionType.OBJECT_PERMISSION.name());
			request.setPermissionName("USER_APPLICATION-CREATE"); //$NON-NLS-1$
			returnVal = controller.getAccessAllowed(request);
		} catch (UnauthorizedAccessException e) {
			LOGGER.error("Current user is Unauthorized " + e);
			returnVal = false;
		} catch (Exception e) {
			LOGGER.error("Exception occured in calling access allowed API " + e);
			returnVal = false;
		}
		return returnVal;
	}*/
	
	/**
	 * Method for Can execute.
	 *
	 * @return true, if successful
	 */
	@CanExecute
	public boolean canExecute() {
		boolean returnVal = true;
		final XMAdminUtil instance = XMAdminUtil.getInstance();
		MPart rightPart;
		Object rightView;
		if ((rightPart = instance.getInformationPart()) != null
				&& (rightView = rightPart.getObject()) != null && rightView instanceof IEditablePart
				&& ((IEditablePart) rightView).isDirty()) {
			returnVal = false;
		}
		return returnVal;
	}

}