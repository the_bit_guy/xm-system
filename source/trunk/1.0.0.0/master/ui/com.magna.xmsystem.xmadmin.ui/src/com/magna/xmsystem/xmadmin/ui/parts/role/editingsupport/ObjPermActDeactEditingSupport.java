package com.magna.xmsystem.xmadmin.ui.parts.role.editingsupport;

import com.magna.xmbackend.vo.enums.CreationType;
import com.magna.xmbackend.vo.roles.ObjectPermission;
import com.magna.xmsystem.xmadmin.ui.parts.role.ObjectPermissionsTable;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.roles.ObjectType;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.roles.VoPermContainer;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.roles.objRelationPermissions.ObjectRelationPermissions;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.roles.objectpermissions.ObjectPermissions;
import com.magna.xmsystem.xmadmin.util.XMAdminUtil;

/**
 * The Class ActivateDeactivateEditingSupport.
 * 
 * @author shashwat.anand
 */
public class ObjPermActDeactEditingSupport extends CheckBoxEditingSupport {
	/** Member variable 'instance' for {@link XMAdminUtil}. */
	private XMAdminUtil instance;
	
	/** Member variable 'viewer' for {@link ObjectPermissionsTable}. */
	private ObjectPermissionsTable viewer;
	
	/**
	 * Instantiates a new activate deactivate editing support.
	 *
	 * @param viewer the viewer
	 */
	public ObjPermActDeactEditingSupport(final ObjectPermissionsTable viewer) {
		super(viewer);
		this.viewer = viewer;
		this.instance = XMAdminUtil.getInstance();
	}

	/* (non-Javadoc)
	 * @see org.eclipse.jface.viewers.EditingSupport#getValue(java.lang.Object)
	 */
	@Override
	protected Object getValue(Object element) {
		if (element instanceof ObjectPermissions) {
			return ((ObjectPermissions) element).isActivate();
		} else if (element instanceof ObjectRelationPermissions) {
			return ((ObjectRelationPermissions) element).isActivate();
		}
		return "N";
	}

	/* (non-Javadoc)
	 * @see org.eclipse.jface.viewers.EditingSupport#setValue(java.lang.Object, java.lang.Object)
	 */
	@Override
	protected void setValue(Object element, Object value) {
		if (element instanceof ObjectPermissions) {
			ObjectPermissions elem = (ObjectPermissions) element;
			ObjectType objectType = ObjectType.getObjectType(elem.getPermissionName());
			String permissionId = this.instance.getObjectPermissionMap().get(objectType).getActivatePermission().getPermissionId();
			VoPermContainer voObjectPermContainer = new VoPermContainer();
			ObjectPermission objectPermission = new ObjectPermission();
			objectPermission.setPermissionId(permissionId);
			voObjectPermContainer.setObjectPermission(objectPermission);
			if (elem.isActivate() && ! (boolean) value) {
				objectPermission.setCreationType(CreationType.DELETE);
				elem.setActivatePermission(voObjectPermContainer);
			} else if (!elem.isActivate() && (boolean) value){
				objectPermission.setCreationType(CreationType.ADD);
				elem.setActivatePermission(voObjectPermContainer);
			} else {
				return;
			}
			elem.setActivate((boolean) value);
			this.viewer.getCachedObjPermissionMap().put(permissionId, voObjectPermContainer);
			getViewer().refresh(element);
		}
	}
	
	/* (non-Javadoc)
	 * @see com.magna.xmsystem.xmadmin.ui.parts.role.editingsupport.CheckBoxEditingSupport#canEdit(java.lang.Object)
	 */
	@Override
	protected boolean canEdit(final Object element) {
		if (element instanceof ObjectPermissions) {
			ObjectPermissions objectPermissions = (ObjectPermissions) element;
			String permissionName = objectPermissions.getPermissionName();
			ObjectType objectType = ObjectType.getObjectType(permissionName);
			if (objectType == ObjectType.ICON || objectType == ObjectType.DIRECTORY || objectType == ObjectType.GROUP
					|| objectType == ObjectType.ROLE || objectType == ObjectType.PROPERTY_CONFIG
					|| objectType == ObjectType.LIVE_MESSAGE) {
				return false;
			}
			if (objectPermissions.isChange() && objectPermissions.isActivate()) {
				return false;
			}
		}
		return true;
	}
}
