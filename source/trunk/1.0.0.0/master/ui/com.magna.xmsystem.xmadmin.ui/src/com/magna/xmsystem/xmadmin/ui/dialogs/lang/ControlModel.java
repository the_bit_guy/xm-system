package com.magna.xmsystem.xmadmin.ui.dialogs.lang;

import java.util.HashMap;
import java.util.Map;

import com.magna.xmsystem.dependencies.utils.LANG_ENUM;
import com.magna.xmsystem.xmadmin.message.Message;

/**
 * The Class ControlModel.
 * 
 * @author shashwat.anand
 */
public class ControlModel {

	/** The dialog title. */
	private String dialogTitle;
	
	/** The widget label. */
	private String widgetLabel;
	
	/** The object model map. */
	private Map<LANG_ENUM, String> objectModelMap;
	
	/** The is manditory. */
	private boolean manditory;
	
	/** The max text limit. */
	private int maxTextLimit;

	/** The translate label. */
	private String langLabel;

	/** The grp label map. */
	private Map<LANG_ENUM, String> langLabelTextMap;

	/** The is editable. */
	private boolean editable;
	

	/**
	 * Instantiates a new control model.
	 *
	 * @param textLimit the text limit
	 * @param isManditory the is manditory
	 * @param isEditable the is editable
	 */
	public ControlModel(final int textLimit, final boolean isManditory, final boolean isEditable) {
		this ("", "", new HashMap<>(),
				"", new HashMap<>(), textLimit, isManditory, isEditable);
	}
	
	
	/**
	 * Instantiates a new control model.
	 *
	 * @param widgetLabelMap the widget label map
	 * @param objectModelMap the object model map
	 * @param textLimit the text limit
	 * @param isManditory the is manditory
	 * @param isEditable the is editable
	 */
	public ControlModel(final String widgetLabel, 
			final Map<LANG_ENUM, String> objectModelMap, final int textLimit, final boolean isManditory, final boolean isEditable) {
		this ("", "", new HashMap<>(),
				widgetLabel, objectModelMap, textLimit, isManditory, isEditable);
	}

	/**
	 * Instantiates a new control model.
	 *
	 * @param dialogTitle {@link String}
	 * @param langLabel {@link String}
	 * @param langLabelTextMap the lang label text map
	 * @param widgetLabelMap the widget label map
	 * @param objectModelMap the object model map
	 * @param textLimit the text limit
	 * @param isManditory the is manditory
	 * @param isEditable the is editable
	 */
	public ControlModel(final String dialogTitle, final String langLabel,
			final Map<LANG_ENUM, String> langLabelTextMap, final String widgetLabel,
			final Map<LANG_ENUM, String> objectModelMap, final int textLimit, final boolean isManditory, final boolean isEditable) {
		this.dialogTitle = dialogTitle;
		this.widgetLabel = widgetLabel;
		this.langLabel = langLabel;
		this.langLabelTextMap = langLabelTextMap;
		this.objectModelMap = objectModelMap;
		this.maxTextLimit = textLimit;
		this.manditory = isManditory;
		this.editable = isEditable;
		
	}
	
	/**
	 * Inits the with default labels.
	 *
	 * @param message the message
	 */
	public void initDefaultLabels(final Message message) {
		setDialogTitle(message.translationDialogTitle);
		setLangLabel(message.translationLangLabel);
		this.langLabelTextMap.put(LANG_ENUM.ENGLISH, message.translationLangLabelTxtEN);
		this.langLabelTextMap.put(LANG_ENUM.GERMAN, message.translationLangLabelTxtDE);
	}
	
	/**
	 * Gets the max text limit.
	 *
	 * @return the maxTextLimit
	 */
	public int getMaxTextLimit() {
		return maxTextLimit;
	}

	/**
	 * Sets the max text limit.
	 *
	 * @param maxTextLimit the maxTextLimit to set
	 */
	public void setMaxTextLimit(final int maxTextLimit) {
		this.maxTextLimit = maxTextLimit;
	}

	/**
	 * Checks if is manditory.
	 *
	 * @return the isManditory
	 */
	public boolean isManditory() {
		return manditory;
	}

	/**
	 * Sets the manditory.
	 *
	 * @param isManditory the isManditory to set
	 */
	public void setManditory(final boolean isManditory) {
		this.manditory = isManditory;
	}
	
	/**
	 * Checks if is editable.
	 *
	 * @return true, if is editable
	 */
	public boolean isEditable() {
		return editable;
	}

	/**
	 * Sets the editable.
	 *
	 * @param isEditable the new enabled
	 */
	public void setEditable(final boolean isEditable) {
		this.editable = isEditable;
	}


	/**
	 * Gets the object model map.
	 *
	 * @return the objectModelMap
	 */
	public Map<LANG_ENUM, String> getObjectModelMap() {
		return objectModelMap;
	}
	
	/**
	 * Gets the object model.
	 *
	 * @param lang the lang
	 * @return the object model
	 */
	public String getObjectModel(final LANG_ENUM lang) {
		if (lang == null || !this.objectModelMap.containsKey(lang)) {
			throw new IllegalArgumentException();
		}
		return this.objectModelMap.get(lang);
	}
	
	/**
	 * Sets the object model map.
	 *
	 * @param lang the lang
	 * @param value the value
	 */
	public void setObjectModelMap(final LANG_ENUM lang, final String value) {
		if (lang == null || value == null) {
			throw new IllegalArgumentException();
		}
		this.objectModelMap.put(lang, value);
	}

	/**
	 * Sets the object model map.
	 *
	 * @param objectModelMap the objectModelMap to set
	 */
	public void setObjectModelMap(final Map<LANG_ENUM, String> objectModelMap) {
		this.objectModelMap = objectModelMap;
	}

	
	/**
	 * Gets the dialog title.
	 *
	 * @return the dialog title
	 */
	public String getDialogTitle() {
		return dialogTitle;
	}
	
	/**
	 * Sets the dialog title.
	 *
	 * @param dialogTitle the new dialog title
	 */
	public void setDialogTitle(final String dialogTitle) {
		this.dialogTitle = dialogTitle;
	}
	
	/**
	 * Gets the widget label.
	 *
	 * @return the widget label
	 */
	public String getWidgetLabel() {
		return widgetLabel;
	}
	
	/**
	 * Sets the widget label.
	 *
	 * @param widgetLabel the new widget label
	 */
	public void setWidgetLabel(final String widgetLabel) {
		this.widgetLabel = widgetLabel;
	}

	/**
	 * Gets the lang label.
	 *
	 * @return the lang label
	 */
	public String getLangLabel() {
		return langLabel;
	}
	
	public void setLangLabel(final String langLabel) {
		this.langLabel = langLabel;
	}

	/**
	 * Gets the lang label text map.
	 *
	 * @return the langLabelTextMap
	 */
	public Map<LANG_ENUM, String> getLangLabelTextMap() {
		return langLabelTextMap;
	}
	
	/**
	 * Gets the lang label text.
	 *
	 * @param lang the lang
	 * @return the lang label text
	 */
	public String getLangLabelText(final LANG_ENUM lang) {
		if (lang == null || !this.langLabelTextMap.containsKey(lang)) {
			throw new IllegalArgumentException();
		}
		return this.langLabelTextMap.get(lang);
	}
	
	/**
	 * Sets the lang label text map.
	 *
	 * @param langLabelTextMap the langLabelTextMap to set
	 */
	public void setLangLabelTextMap(final Map<LANG_ENUM, String> langLabelTextMap) {
		this.langLabelTextMap = langLabelTextMap;
	}
}
