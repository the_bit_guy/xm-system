package com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.group.projectappgroup;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.stream.Collectors;

import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.projectapplicationmodel.ProjectApplication;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.refs.RelationObj;

/**
 * The Class ProjectAppGroupProjectApps.
 * 
 * @author archita.patel
 */
public class ProjectAppGroupProjectApps implements IAdminTreeChild {

	/** The parent. */
	private IAdminTreeChild parent;

	/** The project app group children. */
	private Map<String, IAdminTreeChild> projectAppGroupChildren;

	/**
	 * Instantiates a new project app group project apps.
	 *
	 * @param parent
	 *            the parent
	 */
	public ProjectAppGroupProjectApps(final IAdminTreeChild parent) {
		this.parent = parent;
		this.projectAppGroupChildren = new LinkedHashMap<>();
	}

	/**
	 * Adds the.
	 *
	 * @param projectAppGroupChildId
	 *            the project app group child id
	 * @param child
	 *            the child
	 * @return the i admin tree child
	 */
	public IAdminTreeChild add(final String projectAppGroupChildId, final IAdminTreeChild child) {
		IAdminTreeChild returnVal = this.projectAppGroupChildren.put(projectAppGroupChildId, child);
		if (child instanceof RelationObj && ((RelationObj) child).getRefObject() instanceof ProjectApplication) {
			sort();
		}
		return returnVal;
	}

	/**
	 * Removes the.
	 *
	 * @param projectAppGroupChildId
	 *            the project app group child id
	 * @return the i admin tree child
	 */
	public IAdminTreeChild remove(final String projectAppGroupChildId) {
		return this.projectAppGroupChildren.remove(projectAppGroupChildId);

	}

	/**
	 * Gets the project app group children collection.
	 *
	 * @return the project app group children collection
	 */
	public Collection<IAdminTreeChild> getProjectAppGroupChildrenCollection() {
		return this.projectAppGroupChildren.values();
	}

	/**
	 * Gets the project app group children.
	 *
	 * @return the project app group children
	 */
	public Map<String, IAdminTreeChild> getProjectAppGroupChildren() {
		return projectAppGroupChildren;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild#
	 * getParent()
	 */
	@Override
	public IAdminTreeChild getParent() {
		return this.parent;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild#
	 * setParent(com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.
	 * IAdminTreeChild)
	 */
	@Override
	public void setParent(IAdminTreeChild parent) {
		this.parent = parent;

	}
	
	/**
	 * Sort.
	 */
	public void sort() {
		final LinkedHashMap<String, IAdminTreeChild> collect = this.projectAppGroupChildren.entrySet().stream().sorted(
				(e1, e2) -> ((ProjectApplication) ((RelationObj) e1.getValue()).getRefObject()).getName().compareTo(((ProjectApplication) ((RelationObj) e1.getValue()).getRefObject()).getName()))
				.collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e1, LinkedHashMap::new));
		this.projectAppGroupChildren = collect;
	}

}
