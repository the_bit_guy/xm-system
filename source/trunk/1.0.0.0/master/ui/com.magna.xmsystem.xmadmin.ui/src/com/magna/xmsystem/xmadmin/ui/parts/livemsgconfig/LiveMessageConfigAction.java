package com.magna.xmsystem.xmadmin.ui.parts.livemsgconfig;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import javax.inject.Inject;

import org.eclipse.core.databinding.Binding;
import org.eclipse.core.databinding.DataBindingContext;
import org.eclipse.core.databinding.UpdateValueStrategy;
import org.eclipse.core.databinding.beans.BeanProperties;
import org.eclipse.core.databinding.observable.value.IObservableValue;
import org.eclipse.core.databinding.observable.value.IValueChangeListener;
import org.eclipse.core.databinding.observable.value.ValueChangeEvent;
import org.eclipse.e4.core.services.nls.Translation;
import org.eclipse.e4.ui.model.application.ui.MDirtyable;
import org.eclipse.jface.databinding.fieldassist.ControlDecorationSupport;
import org.eclipse.jface.databinding.swt.WidgetProperties;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.fieldassist.ControlDecoration;
import org.eclipse.jface.fieldassist.FieldDecorationRegistry;
import org.eclipse.jface.resource.JFaceResources;
import org.eclipse.jface.resource.LocalResourceManager;
import org.eclipse.jface.resource.ResourceManager;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.viewers.TreePath;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.VerifyEvent;
import org.eclipse.swt.events.VerifyListener;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Link;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.swt.widgets.Text;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.client.ResourceAccessException;

import com.magna.xmbackend.entities.AdminAreaTranslationTbl;
import com.magna.xmbackend.entities.AdminAreasTbl;
import com.magna.xmbackend.entities.IconsTbl;
import com.magna.xmbackend.entities.LanguagesTbl;
import com.magna.xmbackend.entities.LiveMessageTbl;
import com.magna.xmbackend.entities.LiveMessageTranslationTbl;
import com.magna.xmbackend.entities.ProjectTranslationTbl;
import com.magna.xmbackend.entities.ProjectsTbl;
import com.magna.xmbackend.entities.UserTranslationTbl;
import com.magna.xmbackend.entities.UsersTbl;
import com.magna.xmbackend.vo.jpa.site.SiteAdminAreaRelIdWithAdminArea;
import com.magna.xmbackend.vo.liveMessage.LiveMessageConfigCreateRequest;
import com.magna.xmbackend.vo.liveMessage.LiveMessageCreateRequest;
import com.magna.xmbackend.vo.project.ProjectResponse;
import com.magna.xmbackend.vo.user.UserResponse;
import com.magna.xmsystem.dependencies.customExceptions.UnauthorizedAccessException;
import com.magna.xmsystem.dependencies.utils.LANG_ENUM;
import com.magna.xmsystem.dependencies.utils.XMSystemUtil;
import com.magna.xmsystem.ui.controls.widgets.CustomMessageDialog;
import com.magna.xmsystem.ui.controls.widgets.MagnaCustomCombo;
import com.magna.xmsystem.xmadmin.message.Message;
import com.magna.xmsystem.xmadmin.message.MessageRegistry;
import com.magna.xmsystem.xmadmin.restclient.livemsg.LiveMessageController;
import com.magna.xmsystem.xmadmin.restclient.project.ProjectController;
import com.magna.xmsystem.xmadmin.restclient.site.SiteController;
import com.magna.xmsystem.xmadmin.restclient.users.UserController;
import com.magna.xmsystem.xmadmin.ui.dialogs.lang.ControlModel;
import com.magna.xmsystem.xmadmin.ui.dialogs.lang.XMAdminLangTextAreaDialog;
import com.magna.xmsystem.xmadmin.ui.dialogs.lang.XMAdminLangTextDialog;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.AdminTreeviewer;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.AdminTreeDataLoad;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.AdminTreeFactory;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.adminarea.AdministrationArea;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.adminarea.AdministrationAreas;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.icon.Icon;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.livemsgconfig.LiveMessage;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.livemsgconfig.LiveMessages;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.livemsgconfig.LiveMsgToPattern;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.group.projectgroup.ProjectGroupModel;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.group.projectgroup.ProjectGroupProjects;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.group.projectgroup.ProjectGroupsModel;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.group.usergroup.UserGroupModel;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.group.usergroup.UserGroupUsers;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.group.usergroup.UserGroupsModel;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.projects.Project;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.projects.Projects;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.refs.RelationObj;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.sitemodel.Site;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.sitemodel.Sites;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.users.User;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.users.Users;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.users.UsersNameAlphabet;
import com.magna.xmsystem.xmadmin.ui.validation.NameValidation;
import com.magna.xmsystem.xmadmin.ui.validation.StatusValidation;
import com.magna.xmsystem.xmadmin.util.CommonConstants;
import com.magna.xmsystem.xmadmin.util.MessageType;
import com.magna.xmsystem.xmadmin.util.XMAdminUtil;

/**
 * The Class LiveMessageConfigAction.
 * 
 * @author archita.patel
 */
public class LiveMessageConfigAction extends LiveMessageConfigUI {

	/** The Constant LOGGER. */
	private static final Logger LOGGER = LoggerFactory.getLogger(LiveMessageConfigAction.class);

	/** Member variable for widgetValue. */
	private IObservableValue<?> widgetValue;

	/** Member variable for modelValue. */
	private IObservableValue<?> modelValue;

	/** The bind value. */
	private Binding bindValue;

	/** The live messages config model. */
	private LiveMessage liveMessageModel;

	/** The old model. */
	private LiveMessage oldModel;

	/** The dirty. */
	private MDirtyable dirty;

	/** The registry. */
	@Inject
	private MessageRegistry registry;

	/** The messages. */
	@Inject
	@Translation
	transient private Message messages;

	/** The site combo items. */
	protected transient List<Map<String, Object>> siteComboItems;

	/** The admin area combo items. */
	private List<Map<String, Object>> adminAreaComboItems;

	/** The site controller. */
	private SiteController siteController;

	/** The project controller. */
	private ProjectController projectController;

	/** The user combo items. */
	protected transient ArrayList<Map<String, Object>> userComboItems;

	/** The project combo items. */
	private List<Map<String, Object>> projectComboItems;

	/** The user controller. */
	private UserController userController;

	/** The resource manager. */
	protected final ResourceManager resourceManager;

	/** The users. */
	protected StringBuilder objects;

	/** The data bind context. */
	final transient DataBindingContext dataBindContext = new DataBindingContext();

	/** The user group combo items. */
	private ArrayList<Map<String, Object>> userGroupComboItems;
	
	/** The project group combo items. */
	private ArrayList<Map<String, Object>> projectGroupComboItems;

	/** The object id map. */
	private Map<String, String> objectIdMap;

	/**
	 * Instantiates a new live message config action.
	 *
	 * @param parent
	 *            the parent
	 */
	@Inject
	public LiveMessageConfigAction(final Composite parent) {
		super(parent, SWT.NONE);
		this.siteController = new SiteController();
		this.projectController = new ProjectController();
		this.userController = new UserController();
		this.resourceManager = new LocalResourceManager(JFaceResources.getResources());
		initListener();
	}

	/**
	 * Inits the listener.
	 */
	private void initListener() {
		MagnaCustomCombo userGroupFilterCombo = groupsFilterPanel.getUserGroupFilterCombo();
		Button userGroupFilterButton = this.groupsFilterPanel.getUserGroupFilterButton();
		Text userGroupFilterText = this.groupsFilterPanel.getUserGroupFilterText();
		
		MagnaCustomCombo projectGroupFilterCombo = groupsFilterPanel.getProjectGroupFilterCombo();
		Button projectGroupFilterButton = this.groupsFilterPanel.getProjectGroupFilterButton();
		Text projectGroupFilterText = this.groupsFilterPanel.getProjectGroupFilterText();

		MagnaCustomCombo userFilterCombo = this.filterPanel.getUserFilterCombo();
		Button userFilterButton = this.filterPanel.getUserFilterButton();
		Text userFilterText = this.filterPanel.getUserFilterText();

		MagnaCustomCombo projectFilterCombo = this.filterPanel.getProjectFilterCombo();
		Button projectFilterButton = this.filterPanel.getProjectFilterButton();
		Text projectFilterText = this.filterPanel.getProjectFilterText();

		MagnaCustomCombo adminAreaFilterCombo = this.filterPanel.getAdminAreaFilterCombo();
		Button adminAreaFilterButton = this.filterPanel.getAdminAreaFilterButton();
		Text adminAreaFilterText = this.filterPanel.getAdminAreaFilterText();

		MagnaCustomCombo siteFilterCombo = this.filterPanel.getSiteFilterCombo();
		Button siteFilterButton = this.filterPanel.getSiteFilterButton();
		Text siteFilterText = this.filterPanel.getSiteFilterText();

		this.subjectLink.addSelectionListener(new SelectionAdapter() {
			/**
			 * description link handler
			 */
			@Override
			public void widgetSelected(final SelectionEvent event) {
				final Link linkWidget = (Link) event.widget;
				openSubjectDialog(linkWidget.getShell());
			}
		});

		// Event handling when users click on remarks lang links.
		this.messageLink.addSelectionListener(new SelectionAdapter() {
			/**
			 * remarks text link handler
			 */
			@Override
			public void widgetSelected(final SelectionEvent event) {
				final Link linkWidget = (Link) event.widget;
				openMessageDialog(linkWidget.getShell());
			}
		});
		
		if (this.radioBtnGroups != null && !this.radioBtnGroups.isDisposed()) {
			this.radioBtnGroups.addSelectionListener(new SelectionAdapter() {

				@Override
				public void widgetSelected(SelectionEvent event) {
					if (radioBtnGroups.getSelection()) {
						filterContainerLayout.topControl = groupsFilterPanel;
						filterContainer.layout();
						siteFilterCombo.select(-1);
						adminAreaFilterCombo.select(-1);
						projectFilterCombo.select(-1);
						userFilterCombo.select(-1);
						initlizeFilterCombos();
					} else {
						filterContainerLayout.topControl = filterPanel;
						filterContainer.layout();
						userGroupFilterCombo.select(-1);
						projectGroupFilterCombo.select(-1);
					}
				}
			});
		}
		if (this.saveBtn != null) {
			this.saveBtn.addSelectionListener(new SelectionAdapter() {

				/**
				 * Save button handler
				 */
				@Override
				public void widgetSelected(final SelectionEvent event) {
					saveLiveMessageHandler();
				}
			});
		}

		if (this.cancelBtn != null) {
			this.cancelBtn.addSelectionListener(new SelectionAdapter() {

				/**
				 * Cancel button handler
				 */
				@Override
				public void widgetSelected(final SelectionEvent event) {
					cancelLiveMessageHandler();
				}
			});
		}

		if (toolitemAddBtn != null) {
			toolitemAddBtn.addSelectionListener(new SelectionAdapter() {

				@Override
				public void widgetSelected(final SelectionEvent paramSelectionEvent) {

					addObjectOperation();
				}
			});
		}
		siteFilterText.addListener(SWT.FocusOut, new Listener() {

			@Override
			public void handleEvent(final Event event) {
				final String filterText = ((Text) event.widget).getText();
				//siteFilterCombo.select(-1);
				filterCombo(filterText, siteComboItems, siteFilterCombo);
			}
		});
		siteFilterCombo.addSelectionListener(new SelectionAdapter() {

			@Override
			public void widgetSelected(final SelectionEvent paramSelectionEvent) {
				Object object = siteFilterCombo.getTable().getItem(siteFilterCombo.getSelectionIndex()).getData();
				String siteName = null;
				String siteId = null;
				if (object instanceof Site) {
					Site site = (Site) object;
					siteName = site.getName();
					siteId = site.getSiteId();
				}
				adminAreaFilterText.setText(CommonConstants.EMPTY_STR);
				projectFilterText.setText(CommonConstants.EMPTY_STR);
				userFilterText.setText(CommonConstants.EMPTY_STR);
				updateAdminAreaComboBySite(adminAreaFilterCombo, siteName);
				updateProjectComboBySite(projectFilterCombo, siteName);
				updateUserComboBySite(userFilterCombo, siteId);
				adminAreaFilterCombo.select(-1);
				projectFilterCombo.select(-1);
				userFilterCombo.select(-1);
				toolitemAddBtn.getParent().forceFocus();
			}
		});
		adminAreaFilterText.addListener(SWT.FocusOut, new Listener() {

			@Override
			public void handleEvent(final Event event) {
				final String filterText = ((Text) event.widget).getText();
				//adminAreaFilterCombo.select(-1);
				filterCombo(filterText, adminAreaComboItems, adminAreaFilterCombo);
			}
		});
		adminAreaFilterCombo.addSelectionListener(new SelectionAdapter() {

			@Override
			public void widgetSelected(final SelectionEvent paramSelectionEvent) {
				Object object = adminAreaFilterCombo.getTable().getItem(adminAreaFilterCombo.getSelectionIndex())
						.getData();
				String adminAreasId = null;
				if (object instanceof AdministrationArea) {
					AdministrationArea adminArea = (AdministrationArea) object;
					adminAreasId = adminArea.getAdministrationAreaId();
				}
				projectFilterText.setText(CommonConstants.EMPTY_STR);
				userFilterText.setText(CommonConstants.EMPTY_STR);
				updateProjectComboByAdminArea(projectFilterCombo, adminAreasId);
				updateUserComboByAdminArea(userFilterCombo, adminAreasId);
				projectFilterCombo.select(-1);
				userFilterCombo.select(-1);
				toolitemAddBtn.getParent().forceFocus();
			}

		});
		userFilterText.addListener(SWT.FocusOut, new Listener() {

			@Override
			public void handleEvent(final Event event) {
				final String filterText = ((Text) event.widget).getText();
				//userFilterCombo.select(-1);
				filterCombo(filterText, userComboItems, userFilterCombo);
			}
		});
		projectFilterText.addListener(SWT.FocusOut, new Listener() {

			@Override
			public void handleEvent(final Event event) {
				final String filterText = ((Text) event.widget).getText();
				//projectFilterCombo.select(-1);
				filterCombo(filterText, projectComboItems, projectFilterCombo);
			}
		});

		projectFilterCombo.addSelectionListener(new SelectionAdapter() {

			@Override
			public void widgetSelected(SelectionEvent paramSelectionEvent) {
				final Object object = projectFilterCombo.getTable().getItem(projectFilterCombo.getSelectionIndex())
						.getData();
				String projectsId = null;
				if (object instanceof Project) {
					Project project = (Project) object;
					projectsId = project.getProjectId();
				}
				userFilterText.setText(CommonConstants.EMPTY_STR);
				updateUserComboByProject(userFilterCombo, projectsId);
				userFilterCombo.select(-1);
				toolitemAddBtn.getParent().forceFocus();
			}
		});

		userFilterCombo.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent paramSelectionEvent) {
				toolitemAddBtn.getParent().forceFocus();
			}
		});
		userGroupFilterText.addListener(SWT.FocusOut, new Listener() {

			@Override
			public void handleEvent(final Event event) {
				final String filterText = ((Text) event.widget).getText();
				userGroupFilterCombo.select(-1);
				filterCombo(filterText, userGroupComboItems, userGroupFilterCombo);
				
			}
		});
		
		projectGroupFilterText.addListener(SWT.FocusOut, new Listener() {

			@Override
			public void handleEvent(final Event event) {
				final String filterText = ((Text) event.widget).getText();
				projectGroupFilterCombo.select(-1);
				filterCombo(filterText, projectGroupComboItems, projectGroupFilterCombo);
			}
		});

		userGroupFilterCombo.addSelectionListener(new SelectionAdapter() {
			
			@Override
			public void widgetSelected(SelectionEvent paramSelectionEvent) {
				int selectionIndex = projectGroupFilterCombo.getSelectionIndex();
				if(selectionIndex >= 0){
					projectGroupFilterCombo.select(-1);
				}
				toolitemAddBtn.getParent().forceFocus();
			}
		
		});
		
		projectGroupFilterCombo.addSelectionListener(new SelectionAdapter() {
			
			@Override
			public void widgetSelected(SelectionEvent paramSelectionEvent) {
				int selectionIndex = userGroupFilterCombo.getSelectionIndex();
				if(selectionIndex >= 0){
					userGroupFilterCombo.select(-1);
				}
				toolitemAddBtn.getParent().forceFocus();
			}
		
		});
		siteFilterButton.addListener(SWT.Selection, new Listener() {
			/**
			 * Overrides handleEvent method for show All sites button selection
			 */
			@Override
			public void handleEvent(final Event event) {
				siteFilterText.setText(CommonConstants.EMPTY_STR);
				initlizeAdminAreaCombo();
				initlizeProjectCombo();
				initlizeUsersCombo();
				removeAllComboSelection();
				setItemsWithImages(siteComboItems, siteFilterCombo);
			}
		});

		adminAreaFilterButton.addListener(SWT.Selection, new Listener() {
			/**
			 * Overrides handleEvent method for show All admin areas button
			 * selection
			 */
			@Override
			public void handleEvent(final Event event) {
				adminAreaFilterText.setText(CommonConstants.EMPTY_STR);
				setItemsWithImages(adminAreaComboItems, adminAreaFilterCombo);
				adminAreaFilterCombo.select(-1);
				userFilterCombo.select(-1);
				projectFilterCombo.select(-1);
				if (siteFilterCombo.getSelectionIndex() < 0) {
					initlizeProjectCombo();
					initlizeUsersCombo();
				}else{
					String siteName = ((Site)siteFilterCombo.getTable().getItem(siteFilterCombo.getSelectionIndex()).getData()).getName();
					String siteId = ((Site)siteFilterCombo.getTable().getItem(siteFilterCombo.getSelectionIndex()).getData()).getSiteId();
					updateProjectComboBySite(projectFilterCombo, siteName);
					updateUserComboBySite(userFilterCombo, siteId);
				}
			}
		});

		userFilterButton.addListener(SWT.Selection, new Listener() {
			/**
			 * Overrides handleEvent method for show All users button selection
			 */
			@Override
			public void handleEvent(final Event event) {
				userFilterText.setText(CommonConstants.EMPTY_STR);
				setItemsWithImages(userComboItems, userFilterCombo);
				userFilterCombo.select(-1);
			}
		});

		projectFilterButton.addListener(SWT.Selection, new Listener() {
			/**
			 * Overrides handleEvent method for show All projects button
			 * selection
			 */
			@Override
			public void handleEvent(final Event event) {
				projectFilterText.setText(CommonConstants.EMPTY_STR);
				setItemsWithImages(projectComboItems, projectFilterCombo);
				projectFilterCombo.select(-1);
				userFilterCombo.select(-1);
				if (siteFilterCombo.getSelectionIndex() < 0 && adminAreaFilterCombo.getSelectionIndex() < 0) {
					initlizeUsersCombo();
				}else if(adminAreaFilterCombo.getSelectionIndex() < 0 && siteFilterCombo.getSelectionIndex() >= 0){
					String siteId = ((Site)siteFilterCombo.getTable().getItem(siteFilterCombo.getSelectionIndex()).getData()).getSiteId();
					updateUserComboBySite(userFilterCombo, siteId);
				}else if(siteFilterCombo.getSelectionIndex() < 0 && adminAreaFilterCombo.getSelectionIndex() >= 0){
					String adminAreasId = ((AdministrationArea)adminAreaFilterCombo.getTable().getItem(adminAreaFilterCombo.getSelectionIndex())
							.getData()).getAdministrationAreaId();
					updateUserComboByAdminArea(userFilterCombo, adminAreasId);
				}else{
					String adminAreasId = ((AdministrationArea)adminAreaFilterCombo.getTable().getItem(adminAreaFilterCombo.getSelectionIndex())
							.getData()).getAdministrationAreaId();
					updateUserComboByAdminArea(userFilterCombo, adminAreasId);
				}
			}
		});
		
		userGroupFilterButton.addListener(SWT.Selection, new Listener() {
			/**
			 * Overrides handleEvent method for show All users button selection
			 */
			@Override
			public void handleEvent(final Event event) {
				userGroupFilterText.setText(CommonConstants.EMPTY_STR);
				setItemsWithImages(userGroupComboItems, userGroupFilterCombo);
				userGroupFilterCombo.select(-1);
				projectGroupFilterCombo.select(-1);
			}
		});
		
		projectGroupFilterButton.addListener(SWT.Selection, new Listener() {
			/**
			 * Overrides handleEvent method for show All users button selection
			 */
			@Override
			public void handleEvent(final Event event) {
				projectGroupFilterText.setText(CommonConstants.EMPTY_STR);
				setItemsWithImages(projectGroupComboItems, projectGroupFilterCombo);
				projectGroupFilterCombo.select(-1);
				userGroupFilterCombo.select(-1);
			}
		});
		if (this.txtMessage != null) {
			this.txtMessage.addVerifyListener(new VerifyListener() {
				ControlDecoration txtDecoratorDecroator = new ControlDecoration(txtMessage, SWT.TOP);

				@Override
				public void verifyText(VerifyEvent event) {

					event.doit = false;
					Text source = (Text) event.getSource();
					final String existingText = source.getText();
					final String updatedText = existingText.substring(0, event.start) + event.text
							+ existingText.substring(event.end);
					final Image nameDecoratorImage = FieldDecorationRegistry.getDefault()
							.getFieldDecoration(FieldDecorationRegistry.DEC_ERROR).getImage();
					txtDecoratorDecroator.setImage(nameDecoratorImage);
					if (updatedText.trim().length() <= 0) {
						txtDecoratorDecroator.setDescriptionText(messages.emptyNotificationMessageErr);
						event.doit = true;
						txtDecoratorDecroator.show();
					} else if (updatedText.length() > LiveMessage.MESSAGE_LIMIT) {
						event.doit = false;
 					} else {
						event.doit = true;
						txtDecoratorDecroator.hide();
					}
				}
			});
		}

		if (this.txtSubject != null) {
			this.txtSubject.addVerifyListener(new VerifyListener() {
				ControlDecoration txtDecoratorDecroator = new ControlDecoration(txtSubject, SWT.TOP);

				@Override
				public void verifyText(VerifyEvent event) {
					event.doit = false;
					Text source = (Text) event.getSource();
					final String existingText = source.getText();
					final String updatedText = existingText.substring(0, event.start) + event.text
							+ existingText.substring(event.end);
					final Image nameDecoratorImage = FieldDecorationRegistry.getDefault()
							.getFieldDecoration(FieldDecorationRegistry.DEC_ERROR).getImage();
					txtDecoratorDecroator.setImage(nameDecoratorImage);
					if (updatedText.trim().length() <= 0) {
						txtDecoratorDecroator.setDescriptionText(messages.emptyNotificationSubjectErr);
						event.doit = true;
						txtDecoratorDecroator.show();
					} else {
						event.doit = true;
						txtDecoratorDecroator.hide();
					}
				}
			});
		}

		if (this.txtTo != null) {
			this.txtTo.addModifyListener(new ModifyListener() {
				@Override
				public void modifyText(ModifyEvent event) {
					saveToObjects();
				}
			});
		}
	}

	private void openSubjectDialog(final Shell shell) {
		if (liveMessageModel == null) {
			return;
		}
		/*if (liveMessageModel.getOperationMode() != CommonConstants.OPERATIONMODE.VIEW) {
			String text = txtSubject.getText();
			LANG_ENUM currentLocaleEnum = XMAdminUtil.getInstance().getCurrentLocaleEnum();
			liveMessageModel.setSubject(currentLocaleEnum, text);
		}*/
		final Map<LANG_ENUM, String> obModelMap = new HashMap<>();
		obModelMap.put(LANG_ENUM.ENGLISH, this.liveMessageModel.getSubject(LANG_ENUM.ENGLISH));
		obModelMap.put(LANG_ENUM.GERMAN, this.liveMessageModel.getSubject(LANG_ENUM.GERMAN));
		boolean isEditable = txtSubject.getEditable();
		ControlModel controlModel = new ControlModel(this.messages.liveMsgConfigSubjectLbl, obModelMap, LiveMessage.SUB_LIMIT, false,
				isEditable);
		controlModel.initDefaultLabels(this.messages);
		final XMAdminLangTextDialog dialogArea = new XMAdminLangTextDialog(shell, controlModel);
		final int retVal = dialogArea.open();
		if (retVal == IDialogConstants.OK_ID) {
			final Map<LANG_ENUM, String> subjectMap = this.liveMessageModel.getSubjectMap();
			subjectMap.put(LANG_ENUM.ENGLISH, controlModel.getObjectModel(LANG_ENUM.ENGLISH));
			subjectMap.put(LANG_ENUM.GERMAN, controlModel.getObjectModel(LANG_ENUM.GERMAN));
			//updateSubjectWidget();
		}
	}
	
	/*public void updateSubjectWidget() {
		if (this.liveMessageModel == null) {
			return;
		}
		int operationMode = this.liveMessageModel.getOperationMode();
		final LANG_ENUM currentLocaleEnum = XMAdminUtil.getInstance().getCurrentLocaleEnum();
		final String descForCurLocale = this.liveMessageModel.getSubject(currentLocaleEnum) == null
				? CommonConstants.EMPTY_STR : this.liveMessageModel.getSubject(currentLocaleEnum);
		if (operationMode != CommonConstants.OPERATIONMODE.VIEW) {
			this.txtSubject.setText(descForCurLocale);
			return;
		}
		if (operationMode == CommonConstants.OPERATIONMODE.VIEW) {
			if (!XMSystemUtil.isEmpty(descForCurLocale)) {
				this.txtSubject.setText(descForCurLocale);
				return;
			}
			final String descriptionEN = this.liveMessageModel.getSubject(LANG_ENUM.ENGLISH);
			final String descriptionDE = this.liveMessageModel.getSubject(LANG_ENUM.GERMAN);
			if (!XMSystemUtil.isEmpty(descriptionEN)) {
				this.txtSubject.setText(descriptionEN);
				return;
			}

			if (!XMSystemUtil.isEmpty(descriptionDE)) {
				this.txtSubject.setText(descriptionDE);
				return;
			}
		}
	}*/
	
	private void openMessageDialog(final Shell shell) {
		if (liveMessageModel == null) {
			return;
		}
		/*if (liveMessageModel.getOperationMode() != CommonConstants.OPERATIONMODE.VIEW) {
			String text = txtMessage.getText();
			LANG_ENUM currentLocaleEnum = XMAdminUtil.getInstance().getCurrentLocaleEnum();
			liveMessageModel.setMessage(currentLocaleEnum, text);
		}*/
		final Map<LANG_ENUM, String> obModelMap = new HashMap<>();
		obModelMap.put(LANG_ENUM.ENGLISH, this.liveMessageModel.getMessage(LANG_ENUM.ENGLISH));
		obModelMap.put(LANG_ENUM.GERMAN, this.liveMessageModel.getMessage(LANG_ENUM.GERMAN));
		boolean isEditable = txtMessage.getEditable();
		ControlModel controlModel = new ControlModel(this.messages.liveMsgConfigMsgLabel, obModelMap, LiveMessage.MESSAGE_LIMIT, false,
				isEditable);
		controlModel.initDefaultLabels(this.messages);
		final XMAdminLangTextAreaDialog dialogArea = new XMAdminLangTextAreaDialog(shell, controlModel);
		final int retVal = dialogArea.open();
		if (retVal == IDialogConstants.OK_ID) {
			final Map<LANG_ENUM, String> remarksMap = this.liveMessageModel.getMessageMap();
			remarksMap.put(LANG_ENUM.ENGLISH, controlModel.getObjectModel(LANG_ENUM.ENGLISH));
			remarksMap.put(LANG_ENUM.GERMAN, controlModel.getObjectModel(LANG_ENUM.GERMAN));
			//updateMessageWidget();
		}
	}
	
	/*public void updateMessageWidget() {
		if (this.liveMessageModel == null) {
			return;
		}
		int operationMode = this.liveMessageModel.getOperationMode();
		final LANG_ENUM currentLocaleEnum = XMAdminUtil.getInstance().getCurrentLocaleEnum();
		final String remarkForCurLocale = this.liveMessageModel.getMessage(currentLocaleEnum) == null
				? CommonConstants.EMPTY_STR : this.liveMessageModel.getMessage(currentLocaleEnum);
		if (operationMode != CommonConstants.OPERATIONMODE.VIEW) {
			this.txtMessage.setText(remarkForCurLocale);
			return;
		}
		if (operationMode == CommonConstants.OPERATIONMODE.VIEW) {
			if (!XMSystemUtil.isEmpty(remarkForCurLocale)) {
				this.txtMessage.setText(remarkForCurLocale);
				return;
			}
			final String remarkEN = this.liveMessageModel.getMessage(LANG_ENUM.ENGLISH);
			final String remarkDE = this.liveMessageModel.getMessage(LANG_ENUM.GERMAN);
			if (!XMSystemUtil.isEmpty(remarkEN)) {
				this.txtMessage.setText(remarkEN);
				return;
			}

			if (!XMSystemUtil.isEmpty(remarkDE)) {
				this.txtMessage.setText(remarkDE);
				return;
			}
		}
	}*/
	/**
	 * Update user combo by project.
	 *
	 * @param userFilterCombo the user filter combo
	 * @param projectsId the projects id
	 */
	private void updateUserComboByProject(final MagnaCustomCombo userFilterCombo, final String projectsId) {
		List<UsersTbl> userResponse;
		if (projectsId != null) {
			userResponse = userController.getAllUsersByProjectId(projectsId);
			setUsersToCombo(getUsers(userResponse));
		} else {
			userFilterCombo.getTable().removeAll();
		}
	}
	
	/**
	 * Update user combo by admin area.
	 *
	 * @param userFilterCombo the user filter combo
	 * @param adminAreasId the admin areas id
	 */
	private void updateUserComboByAdminArea(final MagnaCustomCombo userFilterCombo, final String adminAreasId) {
		List<UsersTbl> userTbl = new ArrayList<>();
		if (adminAreasId != null) {
			userTbl = userController.getUsersByAdminArea(adminAreasId);
			setUsersToCombo(getUsers(userTbl));
		} else {
			userFilterCombo.getTable().removeAll();
		}
	}

	/**
	 * Update project combo by admin area.
	 *
	 * @param projectFilterCombo the project filter combo
	 * @param adminAreasId the admin areas id
	 */
	private void updateProjectComboByAdminArea(final MagnaCustomCombo projectFilterCombo, final String adminAreasId) {
		List<ProjectsTbl> projectResponse;
		if (adminAreasId != null) {
			projectResponse = projectController.getProjectsByAdminAreaId(adminAreasId);
			setProjectToCombo(getProjects(projectResponse));
		} else {
			projectFilterCombo.getTable().removeAll();
		}
	}

	/**
	 * Update user combo by site.
	 *
	 * @param userFilterCombo the user filter combo
	 * @param siteId the site id
	 */
	private void updateUserComboBySite(final MagnaCustomCombo userFilterCombo, final String siteId) {
		UserResponse allUsersBySiteId;
		final List<UsersTbl> userTbl = new ArrayList<>();
		if (siteId != null) {
			allUsersBySiteId = userController.getUsersBySiteId(siteId);
			if (allUsersBySiteId != null) {
				for (UsersTbl userTblVo : allUsersBySiteId.getUserTbls()) {
					userTbl.add(userTblVo);
				}
			}
			setUsersToCombo(getUsers(userTbl));
		} else {
			userFilterCombo.getTable().removeAll();
		}
	}

	/**
	 * Update project combo by site.
	 *
	 * @param projectFilterCombo the project filter combo
	 * @param siteName the site name
	 */
	private void updateProjectComboBySite(final MagnaCustomCombo projectFilterCombo, final String siteName) {
		ProjectResponse allProjectsBySiteId;
		final List<ProjectsTbl> projectsTbl = new ArrayList<>();
		if (siteName != null && (allProjectsBySiteId = siteController.getAllProjectBySiteName(siteName)) != null) {
			for (ProjectsTbl siteProjectRelIdWithAdminAreaVo : allProjectsBySiteId.getProjectsTbls()) {
				projectsTbl.add(siteProjectRelIdWithAdminAreaVo);
			}
			setProjectToCombo(getProjects(projectsTbl));
		} else {
			projectFilterCombo.getTable().removeAll();
		}
	}

	/**
	 * Update admin area combo by site.
	 *
	 * @param adminAreaFilterCombo the admin area filter combo
	 * @param siteName the site name
	 */
	private void updateAdminAreaComboBySite(final MagnaCustomCombo adminAreaFilterCombo, final String siteName) {
		List<SiteAdminAreaRelIdWithAdminArea> allAdminAreasBySiteId;
		final List<AdminAreasTbl> adminAreasTbl = new ArrayList<>();
		if (siteName != null && (allAdminAreasBySiteId = siteController.getAllAdminAreasBySiteName(siteName)) != null) {
			for (SiteAdminAreaRelIdWithAdminArea siteAdminAreaRelIdWithAdminAreaVo : allAdminAreasBySiteId) {
				SiteAdminAreaRelIdWithAdminArea siteAdminAreaRelIdWithAdminArea = siteAdminAreaRelIdWithAdminAreaVo;
				adminAreasTbl.add(siteAdminAreaRelIdWithAdminArea.getAdminAreasTbl());
			}
			setAdminAreaToCombo(getAdminArea(adminAreasTbl));
		} else {
			adminAreaFilterCombo.getTable().removeAll();
		}
	}
	
	/**
	 * Cancel live message handler.
	 */
	public void cancelLiveMessageHandler() {
		if (liveMessageModel == null) {
			dirty.setDirty(false);
			return;
		}
		String liveMsgId = CommonConstants.EMPTY_STR;
		int operationMode = this.liveMessageModel.getOperationMode();
		LiveMessage oldModel = getOldModel();
		if (oldModel != null) {
			liveMsgId = oldModel.getLiveMsgId();
		}
		setLiveMessageModel(null);
		setOldModel(null);
		this.saveBtn.setEnabled(true);
		dirty.setDirty(false);
		final AdminTreeviewer adminTree = XMAdminUtil.getInstance().getAdminTree();
		final LiveMessages liveMsgs = AdminTreeFactory.getInstance().getLiveMessages();
		if (operationMode == CommonConstants.OPERATIONMODE.CHANGE) {
			final IStructuredSelection selection = (IStructuredSelection) adminTree.getSelection();
			final Object firstElement = selection.getFirstElement();
			if (firstElement != null && firstElement.equals(liveMsgs.getLiveMsgsChildren().get(liveMsgId))) {
				adminTree.setSelection(new StructuredSelection(liveMsgs.getLiveMsgsChildren().get(liveMsgId)), true);
			}
		} else {
			if (!adminTree.getExpandedState(liveMsgs)) {
				adminTree.setExpandedState(liveMsgs.getParent(), true);
			}
			adminTree.setSelection(new StructuredSelection(liveMsgs), true);
		}
		removeAllComboSelection();
		removeAllFilterText();
	}

	/**
	 * Removes the all filter text.
	 */
	private void removeAllFilterText() {
		this.filterPanel.getSiteFilterText().setText(CommonConstants.EMPTY_STR);
		this.filterPanel.getAdminAreaFilterText().setText(CommonConstants.EMPTY_STR);
		this.filterPanel.getProjectFilterText().setText(CommonConstants.EMPTY_STR);
		this.filterPanel.getUserFilterText().setText(CommonConstants.EMPTY_STR);
		this.groupsFilterPanel.getUserGroupFilterText().setText(CommonConstants.EMPTY_STR);
	}

	/**
	 * Save live message handler.
	 */
	public void saveLiveMessageHandler() {
		saveToObjects();
		saveStartAndExpiryDate();
		if (validate()) {
			if (!liveMessageModel.getName().isEmpty()) {
				if (liveMessageModel.getOperationMode() == CommonConstants.OPERATIONMODE.CREATE) {
					createLiveMsgOperation();
				} else if (liveMessageModel.getOperationMode() == CommonConstants.OPERATIONMODE.CHANGE) {
					changeLiveMsgOperation();
				}
			}
			removeAllComboSelection();
			removeAllFilterText();
		}
	}

	/**
	 * Removes the all combo selection.
	 */
	private void removeAllComboSelection() {
		this.filterPanel.getSiteFilterCombo().select(-1);
		this.filterPanel.getAdminAreaFilterCombo().select(-1);
		this.filterPanel.getProjectFilterCombo().select(-1);
		this.filterPanel.getUserFilterCombo().select(-1);
		this.groupsFilterPanel.getUserGroupFilterCombo().select(-1);
	}

	/**
	 * Save start and expiry date.
	 */
	private void saveStartAndExpiryDate() {
		Date expiryDate;
		Date startDate;
		if ((expiryDate = this.cdtExpiryDate.getDate()) != null) {
			liveMessageModel.setExpiryDateTime(expiryDate);
		}
		if ((startDate = this.cdtStartDate.getDate()) != null) {
			liveMessageModel.setStartDateTime(startDate);
		}
	}
	
	/**
	 * Save to objects.
	 */
	private void saveToObjects() {
		if (!this.txtTo.getText().trim().isEmpty()) {
			final List<String> objList = Arrays.asList(txtTo.getText().trim().toString().split(";"));
			final LiveMessageSaveObjHelper saveLiveMessageToObjects = new LiveMessageSaveObjHelper(messages);
			Map<String, List<Map<String, String>>> toObjectsMap = saveLiveMessageToObjects.getToObjectsMap(objList);
			if (toObjectsMap != null) {
				this.liveMessageModel.setLiveMsgToMap(toObjectsMap);
			} else {
				CustomMessageDialog.openError(this.getShell(), messages.liveMsgInvalidRemoveErrorTilte,
						messages.liveMsgObjRemoveErrorMsg);
				updateToTextField();
			}
		} else {
			Map<String, List<Map<String, String>>> toObjectsMap = new HashMap<>();
			this.liveMessageModel.setLiveMsgToMap(toObjectsMap);
		}
	}

	/**
	 * Change live msg operation.
	 */
	private void changeLiveMsgOperation() {
		try {
			LiveMessageController liveMessageController = new LiveMessageController();
			LiveMessageCreateRequest liveMessageCreateRequest = mapVOObjectWithModel();
			if (liveMessageCreateRequest != null) {
				boolean isUpdated = liveMessageController.updateLiveMessage(liveMessageCreateRequest);
				if (isUpdated) {
					setOldModel(liveMessageModel.deepCopyLiveMsg(true, getOldModel()));
					this.liveMessageModel.setOperationMode(CommonConstants.OPERATIONMODE.VIEW);
					setOperationMode();
					this.dirty.setDirty(false);
					final LiveMessages liveMessages = AdminTreeFactory.getInstance().getLiveMessages();
					liveMessages.sort();
					XMAdminUtil.getInstance().getAdminTree().refresh(true);
					XMAdminUtil.getInstance().getAdminTree().setSelection(new StructuredSelection(getOldModel()), true);
					XMAdminUtil.getInstance().updateLogFile(messages.liveMsgConfigObject + " " + "'"
							+ this.liveMessageModel.getName() + "'" + " " + messages.objectUpdate, MessageType.SUCCESS);
				}
			}
		} catch (UnauthorizedAccessException e) {
			CustomMessageDialog.openError(this.getShell(), messages.objectPermissionDialogTitle,
					messages.objectPermissionDialogMsg);
		} catch (Exception e) {
			if (e instanceof ResourceAccessException) {
				CustomMessageDialog.openError(Display.getCurrent().getActiveShell(), messages.errorDialogTitile,
						messages.serverNotReachable);
			}
			LOGGER.warn("Unable to update liveMessage data ! " + e);  // $NON-NLS-1$
		}
	}

	/**
	 * Creates the live msg operation.
	 */
	private void createLiveMsgOperation() {
		try {
			LiveMessageController liveMessageController = new LiveMessageController();
			LiveMessageCreateRequest liveMessageCreateRequest = mapVOObjectWithModel();
			if (liveMessageCreateRequest != null) {
				LiveMessageTbl liveMsgVo = liveMessageController.saveLiveMessage(liveMessageCreateRequest);
				final String liveMsgId = liveMsgVo.getLiveMessageId();
				if (!XMSystemUtil.isEmpty(liveMsgId)) {
					this.liveMessageModel.setLiveMsgId(liveMsgId);
					
					Collection<LiveMessageTranslationTbl> liveMsgTranslationTblCollection = liveMsgVo.getLiveMessageTranslationTbls();
					for (LiveMessageTranslationTbl liveMsgTranslationTbl : liveMsgTranslationTblCollection) {
						String siteTranslationId = liveMsgTranslationTbl.getId();
						LanguagesTbl languageCode = liveMsgTranslationTbl.getLanguageCode();
						LANG_ENUM langEnum = LANG_ENUM.getLangEnum(languageCode.getLanguageCode());
						this.liveMessageModel.setTranslationId(langEnum, siteTranslationId);
					}
					AdminTreeFactory instance = AdminTreeFactory.getInstance();
					if (oldModel == null) { // Attach this to tree
						setOldModel(liveMessageModel.deepCopyLiveMsg(false, null));
						instance.getLiveMessages().add(liveMsgId, getOldModel());
					}
					this.dirty.setDirty(false);
					final AdminTreeviewer adminTree = XMAdminUtil.getInstance().getAdminTree();
					adminTree.refresh(true);
					if (!adminTree.getExpandedState(instance.getConfigurations())) {
						adminTree.setExpandedState(instance.getConfigurations(), true);
					}
					adminTree.setSelection(new StructuredSelection(instance.getLiveMessages()), true);
					final TreePath[] selectionPaths = adminTree.getStructuredSelection().getPaths();
					if (selectionPaths != null && selectionPaths.length > 0) {
						adminTree.setExpandedState(selectionPaths[0], true);
					}
					adminTree.setSelection(new StructuredSelection(getOldModel()), true);
					XMAdminUtil.getInstance().updateLogFile(messages.liveMsgConfigObject + " " + "'"
							+ this.liveMessageModel.getName() + "'" + " " + messages.objectCreate, MessageType.SUCCESS);
				}
			}
		} catch (UnauthorizedAccessException e) {
			CustomMessageDialog.openError(this.getShell(), messages.objectPermissionDialogTitle,
					messages.objectPermissionDialogMsg);
		} catch (Exception e) {
			if (e instanceof ResourceAccessException) {
				CustomMessageDialog.openError(Display.getCurrent().getActiveShell(), messages.errorDialogTitile,
						messages.serverNotReachable);
			}
			LOGGER.warn("Unable to save data ! " + e);  // $NON-NLS-1$
		}
	}

	/**
	 * Map VO object with model.
	 *
	 * @return the com.magna.xmbackend.vo.live message. live message create
	 *         request
	 */
	private com.magna.xmbackend.vo.liveMessage.LiveMessageCreateRequest mapVOObjectWithModel() {
		com.magna.xmbackend.vo.liveMessage.LiveMessageCreateRequest liveMsgRequest = new com.magna.xmbackend.vo.liveMessage.LiveMessageCreateRequest();

		liveMsgRequest.setLiveMessageId(this.liveMessageModel.getLiveMsgId());
		liveMsgRequest.setLiveMessageName(this.liveMessageModel.getName());
		liveMsgRequest.setSubject(this.liveMessageModel.getSubject());
		liveMsgRequest.setMessage(this.liveMessageModel.getMessage());
		
		List<com.magna.xmbackend.vo.liveMessage.LiveMessageTranslation> liveMsgTranslationList= new ArrayList<>();
		LANG_ENUM[] lang_values = LANG_ENUM.values();
		for (int index = 0; index < lang_values.length; index++) {
			com.magna.xmbackend.vo.liveMessage.LiveMessageTranslation liveMsgTranslation = new com.magna.xmbackend.vo.liveMessage.LiveMessageTranslation();
			liveMsgTranslation.setLanguageCode(lang_values[index].getLangCode());
			liveMsgTranslation.setSubject(this.liveMessageModel.getSubject(lang_values[index]));
			liveMsgTranslation.setMessage(this.liveMessageModel.getMessage(lang_values[index]));
			if (this.liveMessageModel.getOperationMode() == CommonConstants.OPERATIONMODE.CHANGE) {
				String translationId = liveMessageModel.getTranslationId(lang_values[index]);
				liveMsgTranslation.setLiveMsgTransId(XMSystemUtil.isEmpty(translationId) ? CommonConstants.EMPTY_STR : translationId);
			}
			liveMsgTranslationList.add(liveMsgTranslation);
		}

		liveMsgRequest.setLiveMsgTranslations(liveMsgTranslationList);
		liveMsgRequest
				.setIspopup(this.liveMessageModel.isPopup() == true ? CommonConstants.TRUE : CommonConstants.FALSE);
		liveMsgRequest.setStartDatetime(this.liveMessageModel.getStartDateTime());
		liveMsgRequest.setEndDatetime(this.liveMessageModel.getExpiryDateTime());

		objectIdMap = this.liveMessageModel.getObjectIdMap();
		LiveMessageSaveObjHelper saveLiveMessageToObjects = new LiveMessageSaveObjHelper(messages);
		Map<String, List<Map<String, String>>> toObjectsMap = this.liveMessageModel.getLiveMsgToMap();
		Set<LiveMessageConfigCreateRequest> mapLiveMsgConfigCreateReq = saveLiveMessageToObjects
				.mapLiveMsgConfigCreateReq(objectIdMap, toObjectsMap);
		if (mapLiveMsgConfigCreateReq != null) {
			liveMsgRequest.setLiveMessageConfigCreateRequest(mapLiveMsgConfigCreateReq);
		} else {
			return null;
		}
		return liveMsgRequest;
	}

	/**
	 * Bind values.
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public void bindValues() {
		try {
			widgetValue = WidgetProperties.text(SWT.Modify).observe(this.txtName);
			modelValue = BeanProperties.value(LiveMessage.class, LiveMessage.PROPERTY_LIVEMSGNAME)
					.observe(this.liveMessageModel);
			widgetValue.addValueChangeListener(new IValueChangeListener() {
				/**
				 * handler to update button status
				 */
				@Override
				public void handleValueChange(final ValueChangeEvent event) {
					updateButtonStatus(event);
				}
			});
			final UpdateValueStrategy update = new UpdateValueStrategy();

			/*
			 * ContextInjectionFactory .make(NameValidation.class,
			 * this.eclipseContext);
			 */
			update.setAfterGetValidator(new NameValidation(messages, StatusValidation.LIVE_MESSAGE));
			bindValue = dataBindContext.bindValue(widgetValue, modelValue, update, null);
			ControlDecorationSupport.create(bindValue, SWT.TOP | SWT.LEFT);

			widgetValue = WidgetProperties.text(SWT.Modify).observe(this.txtMessage);
			modelValue = BeanProperties.value(LiveMessage.class, LiveMessage.PROPERTY_MESSAGE)
					.observe(this.liveMessageModel);
			dataBindContext.bindValue(widgetValue, modelValue);

			widgetValue = WidgetProperties.text(SWT.Modify).observe(this.txtSubject);
			modelValue = BeanProperties.value(LiveMessage.class, LiveMessage.PROPERTY_SUBJECT)
					.observe(this.liveMessageModel);
			dataBindContext.bindValue(widgetValue, modelValue);

			widgetValue = WidgetProperties.selection().observe(this.popupBtn);
			modelValue = BeanProperties.value(LiveMessage.class, LiveMessage.PROPERTY_LIVEMSG_ISPOPUP)
					.observe(this.liveMessageModel);
			dataBindContext.bindValue(widgetValue, modelValue);

		} catch (Exception e) {
			LOGGER.error("Exception occured while databinding", e);  // $NON-NLS-1$
		}
	}

	/**
	 * Update button status.
	 *
	 * @param event
	 *            the event
	 */
	@SuppressWarnings("rawtypes")
	private void updateButtonStatus(final ValueChangeEvent event) {
		final String name = this.txtName.getText();
		if (this.saveBtn != null) {
			if ((XMSystemUtil.isEmpty(name) || name.trim().length() == 0)) {
				this.saveBtn.setEnabled(false);
			} else {
				this.saveBtn.setEnabled(true);
			}
		}
	}

	/**
	 * Register messages.
	 *
	 * @param registry
	 *            the registry
	 */
	public void registerMessages(final MessageRegistry registry) {
		registry.register((text) -> {
			if (grpliveMsg != null && !grpliveMsg.isDisposed()) {
				grpliveMsg.setText(text);
			}
		}, (message) -> {
			if (liveMessageModel != null) {
				if (liveMessageModel.getOperationMode() == CommonConstants.OPERATIONMODE.CREATE) {
					return message.liveMsgConfigGroupLabel;
				} else if (liveMessageModel.getOperationMode() == CommonConstants.OPERATIONMODE.CHANGE) {
					return message.objectGroupChangeLabel + " \'" + this.liveMessageModel.getName() + "\'";
				} else if (liveMessageModel.getOperationMode() == CommonConstants.OPERATIONMODE.VIEW) {
					return message.objectGroupDisaplyLabel + " \'" + this.liveMessageModel.getName() + "\'";
				}
			}
			return CommonConstants.EMPTY_STR;
		});
		

		registry.register((text) -> {
			if (lblName != null && !lblName.isDisposed()) {
				lblName.setText(text);
			}
		}, (message) -> {
			if (lblName != null && !lblName.isDisposed()) {
				return getUpdatedWidgetText(message.liveMsgConfigNameLbl, lblName);
			}
			return CommonConstants.EMPTY_STR;
		});

		registry.register((text) -> {
			if (lblTo != null && !lblTo.isDisposed()) {
				lblTo.setText(text);
			}
		}, (message) -> {
			if (lblTo != null && !lblTo.isDisposed()) {
				return getUpdatedWidgetText(message.liveMsgConfigToLabel, lblTo);
			}
			return CommonConstants.EMPTY_STR;
		});

		registry.register((text) -> {
			if (lblSubject != null && !lblSubject.isDisposed()) {
				lblSubject.setText(text);
			}
		}, (message) -> {
			if (lblSubject != null && !lblSubject.isDisposed()) {
				return getUpdatedWidgetText(message.liveMsgConfigSubjectLbl, lblSubject);
			}
			return CommonConstants.EMPTY_STR;
		});
		registry.register((text) -> {
			if (this.liveMessageModel != null && txtSubject != null && !txtSubject.isDisposed()) {
				txtSubject.setText(text);
			}
		}, (message) -> {
			if (this.liveMessageModel != null && txtSubject != null && !txtSubject.isDisposed()) {
				return XMSystemUtil.isEmpty(this.liveMessageModel.getSubject()) ? CommonConstants.EMPTY_STR : this.liveMessageModel.getSubject();
			}
			return CommonConstants.EMPTY_STR;
		});
		/*registry.register((text) -> {
			if (txtSubject != null && !txtSubject.isDisposed()) {
				txtSubject.setText(text);
				updateSubjectWidget();
			}
		}, (message) -> {
			if (this.liveMessageModel != null && txtSubject != null && !txtSubject.isDisposed()) {
				final LANG_ENUM langEnum = XMAdminUtil.getInstance().getCurrentLocaleEnum();
				return this.liveMessageModel.getSubject(langEnum) == null ? CommonConstants.EMPTY_STR
						: this.liveMessageModel.getSubject(langEnum);
			}
			return CommonConstants.EMPTY_STR;
		});*/

		registry.register((text) -> {
			if (subjectLink != null && !subjectLink.isDisposed()) {
				subjectLink.setText(text);
			}
		}, (message) -> {
			if (subjectLink != null && !subjectLink.isDisposed()) {
				return getUpdatedWidgetText("<a>" + message.objectTranslationLinkText + "</a>", subjectLink);
			}
			return CommonConstants.EMPTY_STR;
		});
		
		registry.register((text) -> {
			if (lblmessages != null && !lblmessages.isDisposed()) {
				lblmessages.setText(text);
			}
		}, (message) -> {
			if (lblmessages != null && !lblmessages.isDisposed()) {
				return getUpdatedWidgetText(message.liveMsgConfigMsgLabel, lblmessages);
			}
			return CommonConstants.EMPTY_STR;
		});
		registry.register((text) -> {
			if (this.liveMessageModel != null && txtMessage != null && !txtMessage.isDisposed()) {
				txtMessage.setText(text);
			}
		}, (message) -> {
			if (this.liveMessageModel != null && txtMessage != null && !txtMessage.isDisposed()) {
				return XMSystemUtil.isEmpty(this.liveMessageModel.getMessage()) ? CommonConstants.EMPTY_STR : this.liveMessageModel.getMessage();
			}
			return CommonConstants.EMPTY_STR;
		});
		/*registry.register((text) -> {
			if (txtMessage != null && !txtMessage.isDisposed()) {
				txtMessage.setText(text);
				updateMessageWidget();
			}
		}, (message) -> {
			if (this.liveMessageModel != null && txtMessage != null && !txtMessage.isDisposed()) {
				final LANG_ENUM langEnum = XMAdminUtil.getInstance().getCurrentLocaleEnum();
				return this.liveMessageModel.getMessage(langEnum) == null ? CommonConstants.EMPTY_STR
						: this.liveMessageModel.getMessage(langEnum);
			}
			return CommonConstants.EMPTY_STR;
		});*/

		
		registry.register((text) -> {
			if (messageLink != null && !messageLink.isDisposed()) {
				messageLink.setText(text);
			}
		}, (message) -> {
			if (messageLink != null && !messageLink.isDisposed()) {
				return getUpdatedWidgetText("<a>" + message.objectTranslationLinkText + "</a>", messageLink);
			}
			return CommonConstants.EMPTY_STR;
		});
		
		registry.register((text) -> {
			if (popupBtn != null && !popupBtn.isDisposed()) {
				popupBtn.setText(text);
			}
		}, (message) -> {
			if (popupBtn != null && !popupBtn.isDisposed()) {
				return getUpdatedWidgetText(message.liveMsgConfigPopupLabel, popupBtn);
			}
			return CommonConstants.EMPTY_STR;
		});
		registry.register((text) -> {
			if (lblStartDateTime != null && !lblStartDateTime.isDisposed()) {
				lblStartDateTime.setText(text);
			}
		}, (message) -> {
			if (lblStartDateTime != null && !lblStartDateTime.isDisposed()) {
				return getUpdatedWidgetText(message.liveMsgConfigStartDateTimeLabel, lblStartDateTime);
			}
			return CommonConstants.EMPTY_STR;
		});
		registry.register((text) -> {
			if (lblExpiryDateTime != null && !lblExpiryDateTime.isDisposed()) {
				lblExpiryDateTime.setText(text);
			}
		}, (message) -> {
			if (lblExpiryDateTime != null && !lblExpiryDateTime.isDisposed()) {
				return getUpdatedWidgetText(message.liveMsgConfigExpiryDateTimeLabel, lblExpiryDateTime);
			}
			return CommonConstants.EMPTY_STR;
		});
		registry.register((text) -> {
			if (radioBtnObject != null && !radioBtnObject.isDisposed()) {
				radioBtnObject.setText(text);
			}
		}, (message) -> {
			if (radioBtnObject != null && !radioBtnObject.isDisposed()) {
				return getUpdatedWidgetText(message.liveMsgAddObjRadioBtnLbl, radioBtnObject);
			}
			return CommonConstants.EMPTY_STR;
		});
		registry.register((text) -> {
			if (radioBtnGroups != null && !radioBtnGroups.isDisposed()) {
				radioBtnGroups.setText(text);
			}
		}, (message) -> {
			if (radioBtnGroups != null && !radioBtnGroups.isDisposed()) {
				return getUpdatedWidgetText(message.liveMsgGroupRadioBtnLbl, radioBtnGroups);
			}
			return CommonConstants.EMPTY_STR;
		});
		
		registry.register((text) -> {
		}, (message) -> {
			if(cdtStartDate != null && !cdtStartDate.isDisposed()){
			LANG_ENUM currentLocaleEnum = XMAdminUtil.getInstance().getCurrentLocaleEnum();
			cdtStartDate.updateTooltipText(currentLocaleEnum.toString());
			}
			return CommonConstants.EMPTY_STR;
		});
		
		registry.register((text) -> {
		}, (message) -> {
			if(cdtExpiryDate != null && !cdtExpiryDate.isDisposed()){
			LANG_ENUM currentLocaleEnum = XMAdminUtil.getInstance().getCurrentLocaleEnum();
			cdtExpiryDate.updateTooltipText(currentLocaleEnum.toString());
			}
			return CommonConstants.EMPTY_STR;
		});
		/*registry.register((text) -> {
		}, (message) -> {
			if (cdtStartDate != null && !cdtStartDate.isDisposed()) {
				cdtStartDate.updateText();
			}
			return CommonConstants.EMPTY_STR;
		});
		
		registry.register((text) -> {
		}, (message) -> {
			if (cdtExpiryDate != null && !cdtExpiryDate.isDisposed()) {
				cdtExpiryDate.updateText();
			}
			return CommonConstants.EMPTY_STR;
		});*/
		registry.register((text) -> {
			if (saveBtn != null && !saveBtn.isDisposed()) {
				saveBtn.setText(text);
			}
		}, (message) -> {
			if (saveBtn != null && !saveBtn.isDisposed()) {
				return getUpdatedWidgetText(message.saveButtonText, saveBtn);
			}
			return CommonConstants.EMPTY_STR;
		});
		registry.register((text) -> {
			if (cancelBtn != null && !cancelBtn.isDisposed()) {
				cancelBtn.setText(text);
			}
		}, (message) -> {
			if (cancelBtn != null && !cancelBtn.isDisposed()) {
				return getUpdatedWidgetText(message.cancelButtonText, cancelBtn);
			}
			return CommonConstants.EMPTY_STR;
		});

		this.filterPanel.registerMessages(registry);
		this.groupsFilterPanel.registerMessages(registry);
		initlizeFilterCombos();
	}

	/**
	 * Gets the updated widget text.
	 *
	 * @param message
	 *            the message
	 * @param control
	 *            the control
	 * @return the updated widget text
	 */
	private String getUpdatedWidgetText(final String message, final Control control) {
		control.requestLayout();
		control.getParent().redraw();
		control.getParent().getParent().update();
		control.getParent().getParent().getParent().update();
		return message;
	}

	/**
	 * Initlize filter combos.
	 */
	private void initlizeFilterCombos() {
		initlizeSiteCombo();
		initlizeAdminAreaCombo();
		initlizeProjectCombo();
		initlizeUsersCombo();
		initlizeUserGroupCombo();
		initlizeProjectGroupCombo();
	}

	/**
	 * Initlize user group combo.
	 */
	@SuppressWarnings("unchecked")
	private void initlizeUserGroupCombo() {
		UserGroupsModel userGroupsModel = (UserGroupsModel) AdminTreeFactory.getInstance().getGroups()
				.getGroupsChildren().get(UserGroupsModel.class.getSimpleName());
		Collection<IAdminTreeChild> userGroupsChild = userGroupsModel.getUserGroupsChildren().values();
		List<UserGroupModel> userGroupModelList = new ArrayList<>();
		userGroupModelList.addAll((Collection<? extends UserGroupModel>) userGroupsChild);
		setUserGroupToCombo(userGroupModelList);
	}

	/**
	 * Initlize project group combo.
	 */
	@SuppressWarnings("unchecked")
	private void initlizeProjectGroupCombo() {
		ProjectGroupsModel projectGroupModel = (ProjectGroupsModel) AdminTreeFactory.getInstance().getGroups()
				.getGroupsChildren().get(ProjectGroupsModel.class.getSimpleName());
		Collection<IAdminTreeChild> projectGroupsChild = projectGroupModel.getProjectGroupsChildren().values();
		 List<ProjectGroupModel>  projectGroupModelList = new ArrayList<>();
		projectGroupModelList.addAll( (Collection<? extends ProjectGroupModel>) projectGroupsChild);
		setProjectGroupToCombo(projectGroupModelList);
	}

	/**
	 * Initlize site combo.
	 */
	@SuppressWarnings("unchecked")
	private void initlizeSiteCombo() {
		final Sites sites = AdminTreeFactory.getInstance().getSites();
		final Collection<IAdminTreeChild> values = sites.getSitesChildren().values();
		final List<Site> siteList = new ArrayList<Site>();
		siteList.addAll((Collection<? extends Site>) values);
		setSitesToCombo(siteList);
	}

	/**
	 * Initlize admin area combo.
	 */
	@SuppressWarnings("unchecked")
	private void initlizeAdminAreaCombo() {
		final AdministrationAreas administrationAreas = AdminTreeFactory.getInstance().getAdministrationAreas();
		final Collection<IAdminTreeChild> values = administrationAreas.getAdminstrationAreasChildren().values();
		final List<AdministrationArea> adminAreaList = new ArrayList<AdministrationArea>();
		adminAreaList.addAll((Collection<? extends AdministrationArea>) values);
		setAdminAreaToCombo(adminAreaList);
	}

	/**
	 * Initlize project combo.
	 */
	@SuppressWarnings("unchecked")
	private void initlizeProjectCombo() {
		final Projects projects = AdminTreeFactory.getInstance().getProjects();
		final Collection<IAdminTreeChild> projectsChild = projects.getProjectsCollection();
		final List<Project> projectList = new ArrayList<Project>();
		projectList.addAll((Collection<? extends Project>) projectsChild);
		setProjectToCombo(projectList);
	}

	/**
	 * Initlize users combo.
	 */
	private void initlizeUsersCombo() {
		final Users users = AdminTreeFactory.getInstance().getUsers();
		final Map<String, IAdminTreeChild> usersChildren = users.getUsersChildren();
		final List<User> userList = new ArrayList<>();
		for (Entry<String, IAdminTreeChild> entry : usersChildren.entrySet()) {
			IAdminTreeChild iAdminTreeChild = entry.getValue();
			if (iAdminTreeChild instanceof UsersNameAlphabet) {
				UsersNameAlphabet usersNameAlphabet = (UsersNameAlphabet) iAdminTreeChild;
				final Map<String, IAdminTreeChild> userNameAlphaChild = usersNameAlphabet.getUsersChildren();
				for (Entry<String, IAdminTreeChild> alphaUserEntry : userNameAlphaChild.entrySet()) {
					IAdminTreeChild alphaUser = alphaUserEntry.getValue();
					if (alphaUser instanceof User) {
						userList.add((User) alphaUser);
					}
				}
			}
		}
		setUsersToCombo(userList);
	}

	/**
	 * Sets the users to combo.
	 *
	 * @param userList
	 *            the new users to combo
	 */
	private void setUsersToCombo(final List<User> userList) {
		userComboItems = new ArrayList<Map<String, Object>>();
		for (User user : userList) {
			final HashMap<String, Object> item = new HashMap<String, Object>();
			item.put("itemText", user.getName());
			item.put("itemImage", getImage(user));
			item.put("object", user);
			userComboItems.add(item);
		}
		setItemsWithImages(userComboItems, this.filterPanel.getUserFilterCombo());
	}

	/**
	 * Sets the project to combo.
	 *
	 * @param projectList
	 *            the new project to combo
	 */
	private void setProjectToCombo(final List<Project> projectList) {
		projectComboItems = new ArrayList<Map<String, Object>>();
		for (Project project : projectList) {
			final HashMap<String, Object> item = new HashMap<String, Object>();
			item.put("itemText", project.getName());
			item.put("itemImage", getImage(project));
			item.put("object", project);
			projectComboItems.add(item);
		}
		setItemsWithImages(projectComboItems, this.filterPanel.getProjectFilterCombo());
	}

	/**
	 * Sets the admin area to combo.
	 *
	 * @param adminAreaList
	 *            the new admin area to combo
	 */
	private void setAdminAreaToCombo(final List<AdministrationArea> adminAreaList) {
		adminAreaComboItems = new ArrayList<Map<String, Object>>();
		for (AdministrationArea adminArea : adminAreaList) {
			final HashMap<String, Object> item = new HashMap<String, Object>();
			item.put("itemText", adminArea.getName());
			item.put("itemImage", getImage(adminArea));
			item.put("object", adminArea);
			adminAreaComboItems.add(item);
		}
		setItemsWithImages(adminAreaComboItems, this.filterPanel.getAdminAreaFilterCombo());
	}

	/**
	 * Sets the sites to combo.
	 *
	 * @param siteList
	 *            the new sites to combo
	 */
	private void setSitesToCombo(final List<Site> siteList) {
		siteComboItems = new ArrayList<Map<String, Object>>();
		for (Site site : siteList) {
			final HashMap<String, Object> item = new HashMap<String, Object>();
			item.put("itemText", site.getName());
			item.put("itemImage", getImage(site));
			item.put("object", site);
			siteComboItems.add(item);
		}
		setItemsWithImages(siteComboItems, this.filterPanel.getSiteFilterCombo());
	}

	/**
	 * Sets the user group to combo.
	 *
	 * @param userGroupModelList
	 *            the new user group to combo
	 */
	private void setUserGroupToCombo(final List<UserGroupModel> userGroupModelList) {
		userGroupComboItems = new ArrayList<Map<String, Object>>();
		for (UserGroupModel userGroupModel : userGroupModelList) {
			final HashMap<String, Object> item = new HashMap<String, Object>();
			item.put("itemText", userGroupModel.getName());
			item.put("itemImage", getImage(userGroupModel));
			item.put("object", userGroupModel);
			userGroupComboItems.add(item);
		}
		setItemsWithImages(userGroupComboItems, this.groupsFilterPanel.getUserGroupFilterCombo());
	}

	/**
	 * Sets the project group to combo.
	 *
	 * @param projectGroupModelList the new project group to combo
	 */
	private void setProjectGroupToCombo(final List<ProjectGroupModel> projectGroupModelList) {
		projectGroupComboItems = new ArrayList<Map<String, Object>>();
		for (ProjectGroupModel projectGroupModel : projectGroupModelList) {
			final HashMap<String, Object> item = new HashMap<String, Object>();
			item.put("itemText", projectGroupModel.getName());
			item.put("itemImage", getImage(projectGroupModel));
			item.put("object", projectGroupModel);
			projectGroupComboItems.add(item);
		}
		setItemsWithImages(projectGroupComboItems, this.groupsFilterPanel.getProjectGroupFilterCombo());

	}

	/**
	 * Sets the items with images.
	 *
	 * @param items
	 *            the items
	 * @param combo
	 *            the combo
	 */
	public void setItemsWithImages(final List<Map<String, Object>> items, final MagnaCustomCombo combo) {
		try {
			String itemText = null;
			Object itemImage = null;
			final Table table = combo.getTable();
			table.removeAll();
			if (table.getColumnCount() <= 0) {
				new TableColumn(table, SWT.NONE);
			}
			if (items != null) {
				for (int i = 0; i < items.size(); i++) {
					itemText = (String) items.get(i).get("itemText");
					itemImage = items.get(i).get("itemImage");
					final TableItem item = new TableItem(table, SWT.NONE);
					if (itemText != null) {
						item.setText(itemText);
						if (itemImage instanceof Image) {
							item.setImage((Image) itemImage);
						}
					}
					item.setData(items.get(i).get("object"));
				}
			}
		} catch (Exception e) {
			LOGGER.error("Unable to create table combo widget", e); //$NON-NLS-1$
		}
	}

	/**
	 * Gets the image.
	 *
	 * @param object
	 *            the object
	 * @return the image
	 */
	private Image getImage(final Object object) {
		Image image = null;
		if (object != null) {
			if (object instanceof Site) {
				final Site site = (Site) object;
				image = XMSystemUtil.getInstance().getImage(this.getClass(), site.getIcon().getIconPath(), site.isActive(), true);
			} else if (object instanceof User) {
				final User user = (User) object;
				image = XMSystemUtil.getInstance().getImage(this.getClass(), user.getIcon().getIconPath(), user.isActive(), true);
			} else if (object instanceof Project) {
				final Project project = (Project) object;
				image = XMSystemUtil.getInstance().getImage(this.getClass(), project.getIcon().getIconPath(), project.isActive(), true);
			} else if (object instanceof AdministrationArea) {
				final AdministrationArea adminsArea = (AdministrationArea) object;
				image = XMSystemUtil.getInstance().getImage(this.getClass(), adminsArea.getIcon().getIconPath(), adminsArea.isActive(), true);
			} else if (object instanceof UserGroupModel) {
				final UserGroupModel userGroupModel = (UserGroupModel) object;
				image = XMSystemUtil.getInstance().getImage(this.getClass(), userGroupModel.getIcon().getIconPath(), true, true);
			} else if (object instanceof ProjectGroupModel) {
				final ProjectGroupModel projectGroupModel = (ProjectGroupModel) object;
				image = XMSystemUtil.getInstance().getImage(this.getClass(), projectGroupModel.getIcon().getIconPath(), true, true);
			}
		}
		return image;
	}

	/**
	 * Filter combo.
	 *
	 * @param filterText
	 *            the filter text
	 * @param items
	 *            the items
	 * @param FilterCombo
	 *            the filter combo
	 */
	private void filterCombo(final String filterText, final List<Map<String, Object>> items,
			final MagnaCustomCombo FilterCombo) {
		try {
			if (filterText != null && filterText.length() >= 0 && items != null && !items.isEmpty()) {
				final List<Map<String, Object>> tempItems = new ArrayList<Map<String, Object>>();
				for (final Map<String, Object> item : items) {
					final Object itemText = item.get("itemText");
					if (itemText != null && itemText instanceof String
							&& Pattern.compile(Pattern.quote(filterText), Pattern.CASE_INSENSITIVE)
									.matcher((String) itemText).find()) {
						tempItems.add(item);
					}
				}

				setItemsWithImages(tempItems, FilterCombo);
			}
		} catch (Exception e) {
			LOGGER.error("Unable to filter the Combo", e); //$NON-NLS-1$
		}
	}

	/**
	 * Validate.
	 *
	 * @return true, if successful
	 */
	private boolean validate() {
		final String liveMsgName = this.liveMessageModel.getName();
		final LiveMessages liveMessages = AdminTreeFactory.getInstance().getLiveMessages();
		final Collection<IAdminTreeChild> livMsgsCollection = liveMessages.getLiveMsgsCollection();
		if (liveMessageModel.getOperationMode() == CommonConstants.OPERATIONMODE.CHANGE) {
			if (!liveMsgName.equalsIgnoreCase(this.oldModel.getName())) {
				final Map<String, Long> result = livMsgsCollection.parallelStream().collect(Collectors
						.groupingBy(liveMsg -> ((LiveMessage) liveMsg).getName().toUpperCase(), Collectors.counting()));
				if (result.containsKey(liveMsgName.toUpperCase())) {
					CustomMessageDialog.openError(this.getShell(), messages.existingLiveMsgNameTitle,
							messages.existingLiveMsgNameError);
					return false;
				}
			}
		} else if (this.liveMessageModel.getOperationMode() == CommonConstants.OPERATIONMODE.CREATE) {
			for (final IAdminTreeChild liveMsg : livMsgsCollection) {
				if (liveMsgName.equalsIgnoreCase(((LiveMessage) liveMsg).getName())) {
					CustomMessageDialog.openError(this.getShell(), messages.existingLiveMsgNameTitle,
							messages.existingLiveMsgNameError);
					return false;
				}
			}
		}

		if (XMSystemUtil.isEmpty(liveMsgName)) {
			CustomMessageDialog.openError(this.getShell(), messages.nameErrorTitle, messages.nameError);
			return false;
		}

		if (this.txtSubject.getText().trim().isEmpty()) {
			CustomMessageDialog.openError(this.getShell(), messages.notificationSubjectErrTitle,
					messages.notificationSubjectErrMsg);
			return false;
		}
		if (this.txtMessage.getText().trim().isEmpty()) {
			CustomMessageDialog.openError(this.getShell(), messages.notificationMessageErrTitle,
					messages.notificationMessageErrMsg);
			return false;
		}

		/*Date todaysDate = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.YYYY HH:mm:ss.SSS");*/
		Date startDate = this.liveMessageModel.getStartDateTime();
		Date endDate = this.liveMessageModel.getExpiryDateTime();
		if (this.cdtStartDate.getDate() == null) {
			CustomMessageDialog.openError(this.getShell(), messages.liveMsgStartDateErrorTitle,
					messages.liveMsgStartDateErrorMsg);
			return false;
		}
		/*if (!(todaysDate.before(startDate) || (sdf.format(todaysDate).equals(sdf.format(startDate))))) {

			CustomMessageDialog.openError(this.getShell(), messages.liveMsgStartDateErrorTitle,
					messages.liveMsgInvalidStartDateErrMsg);
			return false;
		}*/

		if (this.cdtExpiryDate.getDate() == null) {
			CustomMessageDialog.openError(this.getShell(), messages.liveMsgExpiryDateErrorTitle,
					messages.liveMsgExpiryDateErrorMsg);
			return false;
		}
		
		if (startDate.after(endDate)) {
			CustomMessageDialog.openError(this.getShell(), messages.liveMsgExpiryDateErrorTitle,
					messages.liveMsgInvalidEndDateErrMsg);
			return false;
		}
		return true;
	}

	/**
	 * Sets the operation mode.
	 */
	public void setOperationMode() {
		if (this.liveMessageModel != null) {
			if (liveMessageModel.getOperationMode() == CommonConstants.OPERATIONMODE.VIEW) {
				this.txtTo.setEditable(false);
				this.txtName.setEditable(false);
				this.txtMessage.setEditable(false);
				this.txtSubject.setEditable(false);
				this.cdtStartDate.setEnabled(false);
				this.cdtExpiryDate.setEnabled(false);
				this.toolitemAddBtn.setEnabled(false);
				this.radioBtnObject.setEnabled(false);
				this.radioBtnGroups.setEnabled(false);
				this.popupBtn.setEnabled(false);
				enableFilterCombos(false);
				setShowButtonBar(false);
			} else if (liveMessageModel.getOperationMode() == CommonConstants.OPERATIONMODE.CREATE) {
				this.txtTo.setEditable(true);
				this.txtName.setEditable(true);
				this.txtSubject.setEditable(true);
				this.txtMessage.setEditable(true);
				this.cdtStartDate.setEnabled(true);
				this.cdtExpiryDate.setEnabled(true);
				this.toolitemAddBtn.setEnabled(true);
				this.popupBtn.setEnabled(true);
				this.radioBtnObject.setEnabled(true);
				this.radioBtnGroups.setEnabled(true);
				this.saveBtn.setEnabled(false);
				enableFilterCombos(true);
				setShowButtonBar(true);
				this.dirty.setDirty(true);
			} else if (liveMessageModel.getOperationMode() == CommonConstants.OPERATIONMODE.CHANGE) {
				this.txtTo.setEditable(true);
				this.txtName.setEditable(true);
				this.txtSubject.setEditable(true);
				this.txtMessage.setEditable(true);
				this.cdtStartDate.setEnabled(true);
				this.cdtExpiryDate.setEnabled(true);
				this.popupBtn.setEnabled(true);
				this.toolitemAddBtn.setEnabled(true);
				this.radioBtnObject.setEnabled(true);
				this.radioBtnGroups.setEnabled(true);
				setShowButtonBar(true);
				enableFilterCombos(true);
				this.dirty.setDirty(true);
			} else {
				this.txtTo.setEditable(false);
				this.txtName.setEditable(false);
				this.txtSubject.setEditable(false);
				this.txtMessage.setEditable(false);
				this.cdtStartDate.setEnabled(false);
				this.cdtExpiryDate.setEnabled(false);
				this.toolitemAddBtn.setEnabled(false);
				this.radioBtnObject.setEnabled(false);
				this.popupBtn.setEnabled(false);
				this.radioBtnGroups.setEnabled(false);
				enableFilterCombos(false);
				setShowButtonBar(false);
			}
		}
	}

	/**
	 * Enable filter combos.
	 *
	 * @param flag
	 *            the flag
	 */
	private void enableFilterCombos(boolean flag) {
		this.filterPanel.enableAdminAreaFilter(flag);
		this.filterPanel.enableProjectFilter(flag);
		this.filterPanel.enableSiteFilter(flag);
		this.filterPanel.enableUserFilter(flag);
		this.groupsFilterPanel.enableUserGroupFilter(flag);
	}

	/**
	 * 7 * Sets the dirty object.
	 *
	 * @param dirty
	 *            the new dirty object
	 */
	public void setDirtyObject(final MDirtyable dirty) {
		this.dirty = dirty;
	}

	/**
	 * Sets the live msg config.
	 */
	public void setLiveMessage() {
		try {
			final AdminTreeviewer adminTree = XMAdminUtil.getInstance().getAdminTree();
			final Object selectionObj = adminTree.getSelection();
			if (selectionObj instanceof IStructuredSelection) {
				final Object firstElement = ((IStructuredSelection) selectionObj).getFirstElement();
				if (firstElement instanceof LiveMessage) {
					setOldModel((LiveMessage) firstElement);
					LiveMessage rightHandObject = this.getOldModel().deepCopyLiveMsg(false, null);
					setLiveMessageModel(rightHandObject);
					registerMessages(this.registry);
					bindValues();
					setOperationMode();
					updateToTextField();
					updateStartAndExpiryDate();
				}
			}
		} catch (Exception e) {
			LOGGER.warn("Unable to set liveMsg model selection ! " + e);
		}
	}

	/**
	 * Sets the model.
	 *
	 * @param liveMessage
	 *            the new model
	 */
	public void setModel(final LiveMessage liveMessage) {
		try {
			setOldModel(null);
			setLiveMessageModel(liveMessage);
			registerMessages(this.registry);
			bindValues();
			setOperationMode();
			updateToTextField();
			updateStartAndExpiryDate();
		} catch (Exception e) {
			LOGGER.warn("Unable to set livemsg model ! " + e);
		}
	}

	/**
	 * Gets the live messages config model.
	 *
	 * @return the liveMessagesConfigModel
	 */
	public LiveMessage getLiveMessagesConfigModel() {
		return liveMessageModel;
	}

	/**
	 * Sets the live messages config model.
	 *
	 * @param liveMessagesConfigModel
	 *            the liveMessagesConfigModel to set
	 */
	public void setLiveMessageModel(LiveMessage liveMessagesConfigModel) {
		this.liveMessageModel = liveMessagesConfigModel;
	}

	/**
	 * Gets the old model.
	 *
	 * @return the oldModel
	 */
	public LiveMessage getOldModel() {
		return oldModel;
	}

	/**
	 * Sets the old model.
	 *
	 * @param oldModel
	 *            the oldModel to set
	 */
	public void setOldModel(LiveMessage oldModel) {
		this.oldModel = oldModel;
	}

	/**
	 * Gets the projects.
	 *
	 * @param projectsTbl
	 *            the projects tbl
	 * @return the projects
	 */
	protected List<Project> getProjects(final Iterable<ProjectsTbl> projectsTbl) {
		List<Project> projectList = new ArrayList<>();
		final File iconFolder = XMSystemUtil.getXMSystemServerIconsFolder();
		if(projectsTbl != null){
			try {
				for (ProjectsTbl projectsTblVo : projectsTbl) {
					final String id = projectsTblVo.getProjectId();
					String name = projectsTblVo.getName();
					boolean isActive = com.magna.xmbackend.vo.enums.Status.ACTIVE.name().equals(projectsTblVo.getStatus())
							? true : false;
					
					IconsTbl iconTbl = projectsTblVo.getIconId();
					Icon icon = new Icon(iconTbl.getIconId(), iconTbl.getIconName(),
							iconFolder + File.separator + iconTbl.getIconName(), iconTbl.getIconType());
					
					Map<LANG_ENUM, String> descriptionMap = new HashMap<>();
					Map<LANG_ENUM, String> remarksMap = new HashMap<>();
					Map<LANG_ENUM, String> translationMap = new HashMap<>();
					
					Collection<ProjectTranslationTbl> projectTranslationTblList = projectsTblVo
							.getProjectTranslationTblCollection();
					for (ProjectTranslationTbl projectTranslationTbl : projectTranslationTblList) {
						LANG_ENUM langEnum = LANG_ENUM
								.getLangEnum(projectTranslationTbl.getLanguageCode().getLanguageCode());
						final String translationId = projectTranslationTbl.getProjectTranslationId();
						translationMap.put(langEnum, translationId);
						descriptionMap.put(langEnum, projectTranslationTbl.getDescription());
						remarksMap.put(langEnum, projectTranslationTbl.getRemarks());
					}
					
					Project projectModel = new Project(id, name, isActive, descriptionMap, remarksMap, icon,
							CommonConstants.OPERATIONMODE.VIEW);
					
					projectModel.setTranslationIdMap(translationMap);
					projectList.add(projectModel);
				}
			} catch (Exception e) {
				LOGGER.error("Exeception while getting  Project objects! " + e);
			}
		}
		return projectList;
	}

	/**
	 * Gets the admin area.
	 *
	 * @param adminAreasTbl
	 *            the admin areas tbl
	 * @return the admin area
	 */
	private List<AdministrationArea> getAdminArea(final List<AdminAreasTbl> adminAreasTbl) {
		List<AdministrationArea> adminAreaList = new ArrayList<>();
		final File iconFolder = XMSystemUtil.getXMSystemServerIconsFolder();
		try {
			for (AdminAreasTbl adminAreasTblVo : adminAreasTbl) {
				final String id = adminAreasTblVo.getAdminAreaId();
				final String name = adminAreasTblVo.getName();
				final String contact = adminAreasTblVo.getHotlineContactNumber();
				final String email = adminAreasTblVo.getHotlineContactEmail();
				final Long singletonAppTimeout = adminAreasTblVo.getSingletonAppTimeout();
				final boolean isActive = com.magna.xmbackend.vo.enums.Status.ACTIVE.name()
						.equals(adminAreasTblVo.getStatus()) ? true : false;

				IconsTbl iconTbl = adminAreasTblVo.getIconId();
				Icon icon = new Icon(iconTbl.getIconId(), iconTbl.getIconName(),
						iconFolder + File.separator + iconTbl.getIconName(), iconTbl.getIconType());

				Map<LANG_ENUM, String> descriptionMap = new HashMap<>();
				Map<LANG_ENUM, String> remarksMap = new HashMap<>();
				Map<LANG_ENUM, String> translationMap = new HashMap<>();

				Collection<AdminAreaTranslationTbl> adminAreaTranslationTblList = adminAreasTblVo
						.getAdminAreaTranslationTblCollection();
				for (AdminAreaTranslationTbl adminAreaTranslationTbl : adminAreaTranslationTblList) {
					LANG_ENUM langEnum = LANG_ENUM
							.getLangEnum(adminAreaTranslationTbl.getLanguageCode().getLanguageCode());
					String translationId = adminAreaTranslationTbl.getAdminAreaTranslationId();
					translationMap.put(langEnum, translationId);
					descriptionMap.put(langEnum, adminAreaTranslationTbl.getDescription());
					remarksMap.put(langEnum, adminAreaTranslationTbl.getRemarks());
				}

				AdministrationArea adminArea = new AdministrationArea(id, name, isActive, descriptionMap, contact,
						email, singletonAppTimeout, remarksMap, icon, CommonConstants.OPERATIONMODE.VIEW);

				adminArea.setTranslationIdMap(translationMap);
				adminAreaList.add(adminArea);
			}
		} catch (Exception e) {
			LOGGER.error("Exeception while getting  AdminArea objects! " + e);
		}
		return adminAreaList;
	}

	/**
	 * Gets the users.
	 *
	 * @param usersTbls
	 *            the users tbls
	 * @return the users
	 */
	protected List<User> getUsers(final Iterable<UsersTbl> usersTbls) {
		final List<User> userList = new ArrayList<>();
		if(usersTbls != null){
			for (UsersTbl usersTblVo : usersTbls) {
				final File iconFolder = XMSystemUtil.getXMSystemServerIconsFolder();
				try {
					final String id = usersTblVo.getUserId();
					final String userName = usersTblVo.getUsername();
					final String fullName = usersTblVo.getFullName();
					final String manager = usersTblVo.getManager();
					final String userEmailId = usersTblVo.getEmailId();
					final String userTelephone = usersTblVo.getTelephoneNumber();
					final String userDept = usersTblVo.getDepartment();
					final boolean isActive = com.magna.xmbackend.vo.enums.Status.ACTIVE.name()
							.equals(usersTblVo.getStatus()) ? true : false;
					
					IconsTbl iconTbl = usersTblVo.getIconId();
					Icon icon = new Icon(iconTbl.getIconId(), iconTbl.getIconName(),
							iconFolder + File.separator + iconTbl.getIconName(), iconTbl.getIconType());
					
					final Map<LANG_ENUM, String> descriptionMap = new HashMap<>();
					final Map<LANG_ENUM, String> remarksMap = new HashMap<>();
					final Map<LANG_ENUM, String> translationIdMap = new HashMap<>();
					
					Collection<UserTranslationTbl> userTranslationTblList = usersTblVo.getUserTranslationTblCollection();
					for (UserTranslationTbl userTranslationTbl : userTranslationTblList) {
						LANG_ENUM langEnum = LANG_ENUM.getLangEnum(userTranslationTbl.getLanguageCode().getLanguageCode());
						final String translationId = userTranslationTbl.getUserTranslationId();
						translationIdMap.put(langEnum, translationId);
						descriptionMap.put(langEnum, userTranslationTbl.getDescription());
						remarksMap.put(langEnum, userTranslationTbl.getRemarks());
					}
					
					User user = new User(id, userName, fullName, manager, isActive, userEmailId, userTelephone, userDept,
							descriptionMap, remarksMap, icon, CommonConstants.OPERATIONMODE.VIEW);
					user.setTranslationIdMap(translationIdMap);
					userList.add(user);
				} catch (Exception e) {
					LOGGER.error("Exeception while getting  user objects! " + e);
				}
			}
		}
		return userList;
	}

	/**
	 * Adds the object operation.
	 */
	private void addObjectOperation() {
		objectIdMap = this.liveMessageModel.getObjectIdMap();
		final Map<String, List<Map<String, String>>> toObjectsMap = this.liveMessageModel.getLiveMsgToMap();
		final int userCmbSeleIndex = filterPanel.getUserFilterCombo().getSelectionIndex();
		final int projectCmbSeleIndex = filterPanel.getProjectFilterCombo().getSelectionIndex();
		final int adminAreaCmbSeleIndex = filterPanel.getAdminAreaFilterCombo().getSelectionIndex();
		final int siteCmbSeleIndex = filterPanel.getSiteFilterCombo().getSelectionIndex();
		final int userGrpCmbSeleIndex = groupsFilterPanel.getUserGroupFilterCombo().getSelectionIndex();
		final int projectGrpCmbSeleIndex = groupsFilterPanel.getProjectGroupFilterCombo().getSelectionIndex();
		if (userCmbSeleIndex >= 0 && projectCmbSeleIndex < 0 && adminAreaCmbSeleIndex < 0 && siteCmbSeleIndex < 0
				&& userGrpCmbSeleIndex < 0) {
			MagnaCustomCombo userFilterCombo = filterPanel.getUserFilterCombo();
			final int selectionIndex = userFilterCombo.getSelectionIndex();
			Object objectU = userFilterCombo.getTable().getItem(selectionIndex).getData();
			User user = (User) objectU;
			List<Map<String, String>> listU = toObjectsMap.get(LiveMsgToPattern.U.name());
			boolean flag = false;
			if (listU == null) {
				listU = new ArrayList<>();
			}
			for (Map<String, String> mapU : listU) {
				if (mapU.containsValue(user.getName())) {
					flag = true;
					break;
				}
			}
			if (!flag) {
				final Map<String, String> map = new HashMap<>();
				map.put(LiveMsgToPattern.U.name(), user.getName());
				listU.add(map);
				objectIdMap.put(user.getName(), user.getUserId());
				toObjectsMap.put(LiveMsgToPattern.U.name(), listU);
			}
			userFilterCombo.select(-1);
		} else if (userCmbSeleIndex < 0 && projectCmbSeleIndex < 0 && adminAreaCmbSeleIndex < 0 && siteCmbSeleIndex < 0
				&& userGrpCmbSeleIndex >= 0) {
			final MagnaCustomCombo userGrpFilterCombo = groupsFilterPanel.getUserGroupFilterCombo();
			final Object objectUG = userGrpFilterCombo.getTable().getItem(userGrpCmbSeleIndex).getData();
			final UserGroupModel userGroupModel = (UserGroupModel) objectUG;
			/*final UserGroupUsers userGroupUsers = (UserGroupUsers) userGroupModel.getUserGroupChildren()
					.get(UserGroupUsers.class.getSimpleName());
			final Collection<IAdminTreeChild> values = userGroupUsers.getUserGroupUsersChildren().values();
			 */
			Map<String, IAdminTreeChild> userGroupChild = userGroupModel.getUserGroupChildren();
			IAdminTreeChild userGroupUsers = userGroupChild.get(UserGroupUsers.class.getSimpleName());
			List<IAdminTreeChild> values = AdminTreeDataLoad.getInstance().loadUserGroupUsersFromService(userGroupUsers);
			List<Map<String, String>> listUG = toObjectsMap.get(LiveMsgToPattern.U.name());
			for (IAdminTreeChild iAdminTreeChild : values) {
				boolean flag = false;
				if (iAdminTreeChild instanceof RelationObj) {
					RelationObj relObj = (RelationObj) iAdminTreeChild;
					IAdminTreeChild refObject = relObj.getRefObject();
					if (refObject instanceof User) {
						User user = (User) refObject;

						if (listUG == null) {
							listUG = new ArrayList<>();
						}
						for (Map<String, String> mapUG : listUG) {
							if (mapUG.containsValue(user.getName())) {
								flag = true;
							}
						}
						if (!flag) {
							final Map<String, String> map = new HashMap<>();
							map.put(LiveMsgToPattern.U.name(), user.getName());
							listUG.add(map);
							objectIdMap.put(user.getName(), user.getUserId());
						}
					}
				}
			}
			toObjectsMap.put(LiveMsgToPattern.U.name(), listUG);
			userGrpFilterCombo.select(-1);
		} else if (userCmbSeleIndex < 0 && projectCmbSeleIndex < 0 && adminAreaCmbSeleIndex < 0 && siteCmbSeleIndex < 0
				&& userGrpCmbSeleIndex < 0 && projectGrpCmbSeleIndex >= 0 ) {
			final MagnaCustomCombo projectGrpFilterCombo = groupsFilterPanel.getProjectGroupFilterCombo();
			final Object objectPG = projectGrpFilterCombo.getTable().getItem(projectGrpCmbSeleIndex).getData();
			final ProjectGroupModel projectGroupModel = (ProjectGroupModel) objectPG;
			/*final ProjectGroupProjects projectGroupProjects = (ProjectGroupProjects) userGroupModel.getProjectGroupChildren()
					.get(ProjectGroupProjects.class.getSimpleName());
			final Collection<IAdminTreeChild> values = projectGroupProjects.getProjectGroupChildren().values();*/
			Map<String, IAdminTreeChild> projectGroupChild = projectGroupModel.getProjectGroupChildren();
			IAdminTreeChild projectGroupProjects = projectGroupChild.get(ProjectGroupProjects.class.getSimpleName());
			List<IAdminTreeChild> values = AdminTreeDataLoad.getInstance()
					.loadProjectGroupProjectsFromService(projectGroupProjects);
			List<Map<String, String>> listPG = toObjectsMap.get(LiveMsgToPattern.P.name());
			for (IAdminTreeChild iAdminTreeChild : values) {
				boolean flag = false;
				if (iAdminTreeChild instanceof RelationObj) {
					RelationObj relObj = (RelationObj) iAdminTreeChild;
					IAdminTreeChild refObject = relObj.getRefObject();
					if (refObject instanceof Project) {
						Project project = (Project) refObject;

						if (listPG == null) {
							listPG = new ArrayList<>();
						}
						for (Map<String, String> mapPG : listPG) {
							if (mapPG.containsValue(project.getName())) {
								flag = true;
							}
						}
						if (!flag) {
							final Map<String, String> map = new HashMap<>();
							map.put(LiveMsgToPattern.P.name(), project.getName());
							listPG.add(map);
							objectIdMap.put(project.getName(), project.getProjectId());
						}
					}
				}
			}
			toObjectsMap.put(LiveMsgToPattern.P.name(), listPG);
			projectGrpFilterCombo.select(-1);
		} else if (userCmbSeleIndex < 0 && projectCmbSeleIndex >= 0 && adminAreaCmbSeleIndex < 0 && siteCmbSeleIndex < 0
				&& userGrpCmbSeleIndex < 0) {
			final MagnaCustomCombo projectFilterCombo = filterPanel.getProjectFilterCombo();
			final int selectionIndex = projectFilterCombo.getSelectionIndex();
			final Object objectP = projectFilterCombo.getTable().getItem(selectionIndex).getData();
			final Project project = (Project) objectP;
			List<Map<String, String>> listP = toObjectsMap.get(LiveMsgToPattern.P.name());
			boolean flag = false;
			if (listP == null) {
				listP = new ArrayList<>();
			}
			for (Map<String, String> mapP : listP) {
				if (mapP.containsValue(project.getName())) {
					flag = true;
					break;
				}
			}
			if (!flag) {
				final Map<String, String> map = new HashMap<>();
				map.put(LiveMsgToPattern.P.name(), project.getName());
				listP.add(map);
				objectIdMap.put(project.getName(), project.getProjectId());
				toObjectsMap.put(LiveMsgToPattern.P.name(), listP);
			}
			projectFilterCombo.select(-1);
			initlizeUsersCombo();
		} else if (userCmbSeleIndex < 0 && projectCmbSeleIndex < 0 && adminAreaCmbSeleIndex < 0 && siteCmbSeleIndex >= 0
				&& userGrpCmbSeleIndex < 0) {
			final MagnaCustomCombo siteFilterCombo = filterPanel.getSiteFilterCombo();
			final int selectionIndex = siteFilterCombo.getSelectionIndex();
			final Object objectS = siteFilterCombo.getTable().getItem(selectionIndex).getData();
			final Site site = (Site) objectS;
			List<Map<String, String>> listS = toObjectsMap.get(LiveMsgToPattern.S.name());
			boolean flag = false;
			if (listS == null) {
				listS = new ArrayList<>();
			}
			for (Map<String, String> mapS : listS) {
				if (mapS.containsValue(site.getName())) {
					flag = true;
					break;
				}
			}
			if (!flag) {
				final Map<String, String> map = new HashMap<>();
				map.put(LiveMsgToPattern.S.name(), site.getName());
				listS.add(map);
				objectIdMap.put(site.getName(), site.getSiteId());
				toObjectsMap.put(LiveMsgToPattern.S.name(), listS);
			}
			siteFilterCombo.select(-1);
			initlizeAdminAreaCombo();
		} else if (userCmbSeleIndex < 0 && projectCmbSeleIndex < 0 && adminAreaCmbSeleIndex >= 0
				&& siteCmbSeleIndex >= 0 && userGrpCmbSeleIndex < 0) {
			final MagnaCustomCombo siteFilterCombo = filterPanel.getSiteFilterCombo();
			final MagnaCustomCombo adminAreaFilterCombo = filterPanel.getAdminAreaFilterCombo();
			final int siteSeleIndex = siteFilterCombo.getSelectionIndex();
			final int adminAreaSeleIndex = adminAreaFilterCombo.getSelectionIndex();
			final Object objectS = siteFilterCombo.getTable().getItem(siteSeleIndex).getData();
			final Object objectA = adminAreaFilterCombo.getTable().getItem(adminAreaSeleIndex).getData();
			final AdministrationArea adminArea = (AdministrationArea) objectA;
			final Site site = (Site) objectS;
			List<Map<String, String>> listSA = toObjectsMap.get(LiveMsgToPattern.S_A.name());
			boolean flag = false;
			if (listSA == null) {
				listSA = new ArrayList<>();
			}
			for (Map<String, String> mapSA : listSA) {
				if (mapSA.containsValue(site.getName()) && mapSA.containsValue(adminArea.getName())) {
					flag = true;
					break;
				}
			}
			if (!flag) {
				final Map<String, String> map = new HashMap<>();
				map.put(LiveMsgToPattern.A.name(), adminArea.getName());
				map.put(LiveMsgToPattern.S.name(), site.getName());
				objectIdMap.put(adminArea.getName(), adminArea.getAdministrationAreaId());
				objectIdMap.put(site.getName(), site.getSiteId());
				listSA.add(map);
				toObjectsMap.put(LiveMsgToPattern.S_A.name(), listSA);
			}
			siteFilterCombo.select(-1);
			adminAreaFilterCombo.select(-1);
			initlizeAdminAreaCombo();
			initlizeProjectCombo();
		} else if (userCmbSeleIndex < 0 && projectCmbSeleIndex >= 0 && adminAreaCmbSeleIndex >= 0
				&& siteCmbSeleIndex >= 0 && userGrpCmbSeleIndex < 0) {
			final MagnaCustomCombo siteFilterCombo = filterPanel.getSiteFilterCombo();
			final MagnaCustomCombo adminAreaFilterCombo = filterPanel.getAdminAreaFilterCombo();
			final MagnaCustomCombo projectFilterCombo = filterPanel.getProjectFilterCombo();
			final int siteSeleIndex = siteFilterCombo.getSelectionIndex();
			final int adminAreaSeleIndex = adminAreaFilterCombo.getSelectionIndex();
			final int projectSeleIndex = projectFilterCombo.getSelectionIndex();
			final Object objectS = siteFilterCombo.getTable().getItem(siteSeleIndex).getData();
			final Object objectA = adminAreaFilterCombo.getTable().getItem(adminAreaSeleIndex).getData();
			final Object objectP = projectFilterCombo.getTable().getItem(projectSeleIndex).getData();
			final AdministrationArea adminArea = (AdministrationArea) objectA;
			final Site site = (Site) objectS;
			final Project project = (Project) objectP;
			List<Map<String, String>> listSAP = toObjectsMap.get(LiveMsgToPattern.S_A_P.name());
			boolean flag = false;
			if (listSAP == null) {
				listSAP = new ArrayList<>();
			}
			for (Map<String, String> mapSAP : listSAP) {
				if (mapSAP.containsValue(site.getName()) && mapSAP.containsValue(adminArea.getName())
						&& mapSAP.containsValue(project.getName())) {
					flag = true;
					break;
				}
			}
			if (!flag) {
				final Map<String, String> map = new HashMap<>();
				map.put(LiveMsgToPattern.A.name(), adminArea.getName());
				map.put(LiveMsgToPattern.S.name(), site.getName());
				map.put(LiveMsgToPattern.P.name(), project.getName());
				objectIdMap.put(adminArea.getName(), adminArea.getAdministrationAreaId());
				objectIdMap.put(site.getName(), site.getSiteId());
				objectIdMap.put(project.getName(), project.getProjectId());
				listSAP.add(map);
				toObjectsMap.put(LiveMsgToPattern.S_A_P.name(), listSAP);
			}
			siteFilterCombo.select(-1);
			adminAreaFilterCombo.select(-1);
			projectFilterCombo.select(-1);
			initlizeFilterCombos();
		} else {
			final AdminTreeviewer adminTree = XMAdminUtil.getInstance().getAdminTree();
			CustomMessageDialog.openError(adminTree.getControl().getShell(), messages.liveMsgInvalidObjCombinationTitle,
					messages.liveMsgInvalidObjCombinationMsgPart1+ "\n"+ messages.liveMsgInvalidObjCombinationMsgPart2);
			return;
		}
		updateToTextField();
		this.liveMessageModel.setLiveMsgToMap(toObjectsMap);
		this.liveMessageModel.setObjectIdMap(objectIdMap);
	}

	/**
	 * Update start and expiry date.
	 */
	private void updateStartAndExpiryDate() {
		if (this.liveMessageModel == null) {
			return;
		}
		Date expiryDate;
		Date startDate;
		if ((expiryDate = this.liveMessageModel.getExpiryDateTime()) != null
				&& (startDate = this.liveMessageModel.getStartDateTime()) != null) {
			this.cdtExpiryDate.setDate(expiryDate);
			this.cdtStartDate.setDate(startDate);
			this.cdtExpiryDate.getDate();
			return;
		}
		this.cdtExpiryDate.setDate(null);
		this.cdtStartDate.setDate(null);
	}

	/**
	 * Update to text field.
	 */
	private void updateToTextField() {
		if (this.liveMessageModel == null) {
			return;
		}

		Map<String, List<Map<String, String>>> toObjectsMap = this.liveMessageModel.getLiveMsgToMap();
		final StringBuilder sb = new StringBuilder();
		StringBuilder sbObj;
		for (Entry<String, List<Map<String, String>>> entry : toObjectsMap.entrySet()) {
			if (entry.getKey().equals(LiveMsgToPattern.U.name())) {
				List<Map<String, String>> list = entry.getValue();
				if (list != null) {
					sbObj = new StringBuilder();
					String prefix = "";
					for (Map<String, String> map : list) {
						sbObj.append(prefix);
						prefix = ",";
						sbObj.append(map.get(LiveMsgToPattern.U.name()));
					}
					sb.append(LiveMsgToPattern.U.name()).append('[').append(sbObj).append(']').append(';');
				}
			} else if (entry.getKey().equals(LiveMsgToPattern.P.name())) {
				List<Map<String, String>> list = entry.getValue();
				if (list != null) {
					sbObj = new StringBuilder();
					String prefix = "";
					for (Map<String, String> map : list) {
						sbObj.append(prefix);
						prefix = ",";
						sbObj.append(map.get(LiveMsgToPattern.P.name()));
					}
					sb.append(LiveMsgToPattern.P.name()).append('[').append(sbObj).append(']').append(';');
				}
			} else if (entry.getKey().equals(LiveMsgToPattern.S.name())) {
				final List<Map<String, String>> list = entry.getValue();
				if (list != null) {
					sbObj = new StringBuilder();
					String prefix = "";
					for (Map<String, String> map : list) {
						sbObj.append(prefix);
						prefix = ",";
						sbObj.append(map.get(LiveMsgToPattern.S.name()));
					}
					sb.append(LiveMsgToPattern.S.name()).append('[').append(sbObj).append(']').append(';');
				}
			} else if (entry.getKey().equals(LiveMsgToPattern.S_A.name())) {
				List<Map<String, String>> list = entry.getValue();
				if (list != null) {
					sbObj = new StringBuilder();
					String prefix = "";
					for (Map<String, String> map : list) {
						sbObj.append(prefix);
						prefix = ",";
						sbObj.append(map.get(LiveMsgToPattern.S.name())).append('/')
								.append(map.get(LiveMsgToPattern.A.name()));
					}
					sb.append(LiveMsgToPattern.S_A.name()).append('[').append(sbObj).append(']').append(';');
				}
			} else if (entry.getKey().equals(LiveMsgToPattern.S_A_P.name())) {
				List<Map<String, String>> list = entry.getValue();
				if (list != null) {
					sbObj = new StringBuilder();
					String prefix = "";
					for (Map<String, String> map : list) {
						sbObj.append(prefix);
						prefix = ",";
						sbObj.append(map.get(LiveMsgToPattern.S.name())).append('/')
								.append(map.get(LiveMsgToPattern.A.name())).append('/')
								.append(map.get(LiveMsgToPattern.P.name()));
					}
					sb.append(LiveMsgToPattern.S_A_P.name()).append('[').append(sbObj).append(']').append(';');
				}
			} 
		}
		this.txtTo.setText(sb.toString());

	}
}
