package com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.notification;

import java.beans.PropertyChangeEvent;
import java.util.HashSet;
import java.util.Set;

import com.magna.xmsystem.dependencies.utils.XMSystemUtil;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.BeanModel;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild;
import com.magna.xmsystem.xmadmin.util.CommonConstants;

/**
 * The Class Notification.
 * 
 * @author archita.patel
 */
public class Notification extends BeanModel implements IAdminTreeChild, Cloneable {

	/** The Constant PROPERTY_USERS. */
	public static final String PROPERTY_USERS = "users";
	
	/** The Constant PROPERTY_SUBJECT. */
	public static final String PROPERTY_SUBJECT = "subject"; //$NON-NLS-1$

	/** The Constant PROPERTY_MESSAGE. */
	public static final String PROPERTY_MESSAGE = "message"; //$NON-NLS-1$

	/** The Constant PROPERTY_CRIETERIA. */
	public static final String PROPERTY_EVENTTYPE = "eventType"; //$NON-NLS-1$

	/** The Constant PROPERTY_OBJNOTIFI. */
	public static final String PROPERTY_INCLUDEREMARKS = "includeRemarks"; //$NON-NLS-1$

	/** The Constant PROPERTY_DESCRIPTION. */
	public static final String PROPERTY_DESCRIPTION = "description";

	/** The Constant PROPERTY_SENDTO_ASSIGNUSER. */
	public static final String PROPERTY_SENDTO_ASSIGNUSER = "sendToAssignUser";

	/** The Constant PROPERTY_ACTIVE. */
	public static final String PROPERTY_ACTIVE = "active"; //$NON-NLS-1$

	/** The description. */
	private String description;

	/** The users. */
	private Set<String> users;
	
	/** The active. */
	private boolean active;

	/** The subject. */
	private String subject;

	/** The message. */
	private String message;

	/** The event type. */
	private String eventType;

	/** The include remarks. */
	private boolean includeRemarks;

	/** The send to assign user. */
	private boolean sendToAssignUser;
/*
	*//** The project expiry notice period. *//*
	private boolean projectExpiryNoticePeriod;*/
	
	/** The Constant SUB_LIMIT. */
	public static final int SUB_LIMIT = 100;
	
	/** The Constant MESSAGE_LIMIT. */
	public static final int MESSAGE_LIMIT = 1500;

	/**
	 * Instantiates a new notification.
	 */
	public Notification() {
		this(false,null, new HashSet<>(), null, null, null, false, false, false);
	}

	
	/**
	 * Instantiates a new notification.
	 *
	 * @param isActive the is active
	 * 
	 * @param description the description
	 * 
	 * @param users the users
	 * 
	 * @param subject the subject
	 * 
	 * @param message the message
	 * 
	 * @param eventType the event type
	 * 
	 * @param includeRemarks the include remarks
	 * 
	 * @param sendToAssignUser the send to assign user
	 * 
	 * @param projectExpiryNoticePeriod the project expiry notice period
	 */
	public Notification(final boolean isActive,final String description, final Set<String> users,
			final String subject, final String message, final String eventType, final boolean includeRemarks,
			final boolean sendToAssignUser, final boolean projectExpiryNoticePeriod) {
		super();
		this.active = isActive;
		this.description = description;
		this.users = users;
		this.subject = subject;
		this.message = message;
		this.eventType = eventType;
		this.includeRemarks = includeRemarks;
		this.sendToAssignUser = sendToAssignUser;
		//this.projectExpiryNoticePeriod = projectExpiryNoticePeriod;
	}

	/**
	 * Instantiates a new notification.
	 *
	 * @param currentNotificationId
	 *            the current notification id
	 */
	public Notification(String currentNotificationId) {
		this(false,null, new HashSet<>(), null, null, null, false, false, false);
	}

	
	/**
	 * Checks if is active.
	 *
	 * @return the isActive
	 */
	public boolean isActive() {
		return active;
	}

	/**
	 * Sets the active.
	 *
	 * @param isActive
	 *            the isActive to set
	 */
	public void setActive(final boolean isActive) {
		this.propertyChangeSupport.firePropertyChange(PROPERTY_ACTIVE, this.active, this.active = isActive);
	}
	

	/**
	 * Gets the description.
	 *
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Sets the description.
	 *
	 * @param name
	 *            the new description
	 */
	public void setDescription(String name) {
		if (name == null)
			throw new IllegalArgumentException();
		this.propertyChangeSupport.firePropertyChange(PROPERTY_DESCRIPTION, this.description,
				this.description = name.trim());
	}

	/**
	 * Gets the users.
	 *
	 * @return the users
	 */
	public Set<String> getUsers() {
		return users;
	}

	/**
	 * Sets the users.
	 *
	 * @param users
	 *            the new users
	 */
	public void setUsers(Set<String> users) {
		if (users == null)
			throw new IllegalArgumentException();
		this.propertyChangeSupport.firePropertyChange(PROPERTY_USERS, this.users, this.users = users);
		//this.users = users;
	}

	/**
	 * Gets the subject.
	 *
	 * @return the subject
	 */
	public String getSubject() {
		return subject;
	}

	/**
	 * Sets the subject.
	 *
	 * @param subject
	 *            the new subject
	 */
	public void setSubject(String subject) {
		if (subject == null)
			throw new IllegalArgumentException();
		this.propertyChangeSupport.firePropertyChange(PROPERTY_SUBJECT, this.subject, this.subject = subject.trim());
		// this.subject = subject;
	}

	/**
	 * Gets the message.
	 *
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * Sets the message.
	 *
	 * @param message
	 *            the new message
	 */
	public void setMessage(String message) {
		// this.message = message;
		if (message == null)
			throw new IllegalArgumentException();
		this.propertyChangeSupport.firePropertyChange(PROPERTY_MESSAGE, this.message, this.message = message.trim());
	}

	/**
	 * Gets the criteria.
	 *
	 * @return the criteria
	 */
	public String getEventType() {
		return eventType;
	}

	/**
	 * Sets the criteria.
	 *
	 * @param criteria
	 *            the new criteria
	 */
	public void setEventType(String criteria) {
		if (criteria == null)
			throw new IllegalArgumentException();
		this.propertyChangeSupport.firePropertyChange(PROPERTY_EVENTTYPE, this.eventType, this.eventType = criteria);
		// this.criteria = criteria;
	}

	/**
	 * Checks if is include remarks.
	 *
	 * @return true, if is include remarks
	 */
	public boolean isIncludeRemarks() {
		return includeRemarks;
	}

	/**
	 * Sets the include remarks.
	 *
	 * @param includeRemarks
	 *            the new include remarks
	 */
	public void setIncludeRemarks(boolean includeRemarks) {
		this.propertyChangeSupport.firePropertyChange(PROPERTY_INCLUDEREMARKS, this.includeRemarks,
				this.includeRemarks = includeRemarks);
		// this.isObjNotification = isObjNotification;
	}

	/**
	 * Checks if is send to assign user.
	 *
	 * @return true, if is send to assign user
	 */
	public boolean isSendToAssignUser() {
		return sendToAssignUser;
	}

	/**
	 * Sets the send to assign user.
	 *
	 * @param sendToAssignUser
	 *            the new send to assign user
	 */
	public void setSendToAssignUser(boolean sendToAssignUser) {
		this.propertyChangeSupport.firePropertyChange(PROPERTY_SENDTO_ASSIGNUSER, this.sendToAssignUser,
				this.sendToAssignUser = sendToAssignUser);
		// this.sendToAssignUser = sendToAssignUser;
	}

	/**
	 * Checks if is project expiry notice period.
	 *
	 * @return true, if is project expiry notice period
	 *//*
	public boolean isProjectExpiryNoticePeriod() {
		return projectExpiryNoticePeriod;
	}

	*//**
	 * Sets the project expiry notice period.
	 *
	 * @param projectExpiryNoticePeriod
	 *            the new project expiry notice period
	 *//*
	public void setProjectExpiryNoticePeriod(boolean projectExpiryNoticePeriod) {
		this.propertyChangeSupport.firePropertyChange(PROPERTY_PROJEXPNOTIPERIOD, this.projectExpiryNoticePeriod,
				this.projectExpiryNoticePeriod = projectExpiryNoticePeriod);
		// this.projectExpiryNoticePeriod = projectExpiryNoticePeriod;
	}*/

	/**
	 * Deep copy notification.
	 *
	 * @param update
	 *            the update
	 * @param updateThisObject
	 *            the update this object
	 * @return the notification
	 */
	public Notification deepCopyNotification(boolean update, Notification updateThisObject) {
		Notification clonedNotification = null;
		
		boolean currentIsActive = this.isActive();
		String currentDescription = XMSystemUtil.isEmpty(this.getDescription()) ? CommonConstants.EMPTY_STR
				: this.getDescription();
		Set<String> currentUserList = this.getUsers();
		String currentEvent = XMSystemUtil.isEmpty(this.getEventType()) ? CommonConstants.EMPTY_STR
				: this.getEventType();
		String currentSubject = XMSystemUtil.isEmpty(this.getSubject()) ? CommonConstants.EMPTY_STR : this.getSubject();
		String currentMessage = XMSystemUtil.isEmpty(this.getMessage()) ? CommonConstants.EMPTY_STR : this.getMessage();
		boolean currentIsObjNoti = this.isIncludeRemarks();
		boolean currentSendToAssignUser = this.isSendToAssignUser();
		//boolean currentProjExpiry = this.isProjectExpiryNoticePeriod();

		if (update) {
			clonedNotification = updateThisObject;
		} else {
			clonedNotification = new Notification();
		}
		clonedNotification.setActive(currentIsActive);
		clonedNotification.setDescription(currentDescription);
		clonedNotification.setUsers(currentUserList);
		clonedNotification.setEventType(currentEvent);
		clonedNotification.setSubject(currentSubject);
		clonedNotification.setMessage(currentMessage);
		clonedNotification.setIncludeRemarks(currentIsObjNoti);
		clonedNotification.setSendToAssignUser(currentSendToAssignUser);
		//clonedNotification.setProjectExpiryNoticePeriod(currentProjExpiry);

		return clonedNotification;

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.BeanModel#
	 * propertyChange(java.beans.PropertyChangeEvent)
	 */
	@Override
	public void propertyChange(PropertyChangeEvent event) {
		this.propertyChangeSupport.firePropertyChange(event.getPropertyName(), event.getOldValue(),
				event.getNewValue());
	}

}
