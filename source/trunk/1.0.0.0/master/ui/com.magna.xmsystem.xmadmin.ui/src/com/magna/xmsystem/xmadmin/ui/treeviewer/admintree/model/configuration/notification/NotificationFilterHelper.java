package com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.notification;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.regex.Pattern;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.magna.xmsystem.dependencies.utils.XMSystemUtil;
import com.magna.xmsystem.ui.controls.widgets.MagnaCustomCombo;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.AdminTreeFactory;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.adminarea.AdministrationArea;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.adminarea.AdministrationAreas;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.group.projectgroup.ProjectGroupModel;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.group.projectgroup.ProjectGroupsModel;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.group.usergroup.UserGroupModel;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.group.usergroup.UserGroupsModel;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.projects.Project;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.projects.Projects;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.sitemodel.Site;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.sitemodel.Sites;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.users.User;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.users.Users;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.users.UsersNameAlphabet;

// TODO: Auto-generated Javadoc
/**
 * The Class NotificationFilterHelper.
 */
public class NotificationFilterHelper {

	/** The Constant LOGGER. */
	private static final Logger LOGGER = LoggerFactory.getLogger(NotificationFilterHelper.class);

	/**
	 * Gets the site objects.
	 *
	 * @return the site objects
	 */
	@SuppressWarnings("unchecked")
	public List<Site> getSiteObjects() {
		final Sites sites = AdminTreeFactory.getInstance().getSites();
		final Collection<IAdminTreeChild> values = sites.getSitesChildren().values();
		final List<Site> siteList = new ArrayList<Site>();
		siteList.addAll((Collection<? extends Site>) values);
		return siteList;
	}

	/**
	 * Gets the admin area objects.
	 *
	 * @return the admin area objects
	 */
	@SuppressWarnings("unchecked")
	public List<AdministrationArea> getAdminAreaObjects() {
		final AdministrationAreas administrationAreas = AdminTreeFactory.getInstance().getAdministrationAreas();
		final Collection<IAdminTreeChild> values = administrationAreas.getAdminstrationAreasChildren().values();
		final List<AdministrationArea> adminAreaList = new ArrayList<AdministrationArea>();
		adminAreaList.addAll((Collection<? extends AdministrationArea>) values);
		return adminAreaList;
	}

	/**
	 * Gets the projectobjects.
	 *
	 * @return the projectobjects
	 */
	@SuppressWarnings("unchecked")
	public List<Project> getProjectObjects() {
		final Projects projects = AdminTreeFactory.getInstance().getProjects();
		final Collection<IAdminTreeChild> projectsChild = projects.getProjectsCollection();
		final List<Project> projectList = new ArrayList<Project>();
		projectList.addAll((Collection<? extends Project>) projectsChild);
		return projectList;
	}

	/**
	 * Gets the users objets.
	 *
	 * @return the users objets
	 */
	public List<User> getUsersObjets() {
		final Users users = AdminTreeFactory.getInstance().getUsers();
		final Map<String, IAdminTreeChild> usersChildren = users.getUsersChildren();
		final List<User> userList = new ArrayList<>();
		for (Entry<String, IAdminTreeChild> entry : usersChildren.entrySet()) {
			IAdminTreeChild iAdminTreeChild = entry.getValue();
			if (iAdminTreeChild instanceof UsersNameAlphabet) {
				UsersNameAlphabet usersNameAlphabet = (UsersNameAlphabet) iAdminTreeChild;
				final Map<String, IAdminTreeChild> userNameAlphaChild = usersNameAlphabet.getUsersChildren();
				for (Entry<String, IAdminTreeChild> alphaUserEntry : userNameAlphaChild.entrySet()) {
					IAdminTreeChild alphaUser = alphaUserEntry.getValue();
					if (alphaUser instanceof User) {
						userList.add((User) alphaUser);
					}
				}
			}
		}
		return userList;
	}

	/**
	 * Gets the user group objects.
	 *
	 * @return the user group objects
	 */
	@SuppressWarnings("unchecked")
	public List<UserGroupModel> getUserGroupObjects() {
		UserGroupsModel userGroupsModel = (UserGroupsModel) AdminTreeFactory.getInstance().getGroups()
				.getGroupsChildren().get(UserGroupsModel.class.getSimpleName());
		Collection<IAdminTreeChild> userGroupsChild = userGroupsModel.getUserGroupsChildren().values();
		List<UserGroupModel> userGroupModelList = new ArrayList<>();
		userGroupModelList.addAll((Collection<? extends UserGroupModel>) userGroupsChild);
		return userGroupModelList;
	}

	/**
	 * Gets the project group objects.
	 *
	 * @return the project group objects
	 */
	@SuppressWarnings("unchecked")
	public List<ProjectGroupModel> getProjectGroupObjects() {
		ProjectGroupsModel projectGroupModel = (ProjectGroupsModel) AdminTreeFactory.getInstance().getGroups()
				.getGroupsChildren().get(ProjectGroupsModel.class.getSimpleName());
		Collection<IAdminTreeChild> projectGroupsChild = projectGroupModel.getProjectGroupsChildren().values();
		List<ProjectGroupModel> projectGroupModelList = new ArrayList<>();
		projectGroupModelList.addAll((Collection<? extends ProjectGroupModel>) projectGroupsChild);
		return projectGroupModelList;
	}

	/**
	 * Gets the site combo items.
	 *
	 * @param siteList
	 *            the site list
	 * @return the site combo items
	 */
	public ArrayList<Map<String, Object>> getSiteComboItems(List<Site> siteList) {
		ArrayList<Map<String, Object>> siteComboItems = new ArrayList<Map<String, Object>>();
		for (Site site : siteList) {
			final HashMap<String, Object> item = new HashMap<String, Object>();
			item.put("itemText", site.getName());
			item.put("itemImage", getImage(site));
			item.put("object", site);
			siteComboItems.add(item);
		}
		return siteComboItems;

	}

	/**
	 * Gets the admin area combo items.
	 *
	 * @param adminAreaList
	 *            the admin area list
	 * @return the admin area combo items
	 */
	public ArrayList<Map<String, Object>> getAdminAreaComboItems(final List<AdministrationArea> adminAreaList) {
		ArrayList<Map<String, Object>> adminAreaComboItems = new ArrayList<Map<String, Object>>();
		for (AdministrationArea adminArea : adminAreaList) {
			final HashMap<String, Object> item = new HashMap<String, Object>();
			item.put("itemText", adminArea.getName());
			item.put("itemImage", getImage(adminArea));
			item.put("object", adminArea);
			adminAreaComboItems.add(item);
		}
		return adminAreaComboItems;
	}

	/**
	 * Gets the project comboitems.
	 *
	 * @param projectList
	 *            the project list
	 * @return the project comboitems
	 */
	public ArrayList<Map<String, Object>> getProjectComboItems(final List<Project> projectList) {
		ArrayList<Map<String, Object>> projectComboItems = new ArrayList<Map<String, Object>>();
		for (Project project : projectList) {
			final HashMap<String, Object> item = new HashMap<String, Object>();
			item.put("itemText", project.getName());
			item.put("itemImage", getImage(project));
			item.put("object", project);
			projectComboItems.add(item);
		}
		return projectComboItems;
	}

	/**
	 * Gets the users combo items.
	 *
	 * @param userList
	 *            the user list
	 * @return the users combo items
	 */
	public ArrayList<Map<String, Object>> getUsersComboItems(final List<User> userList) {
		ArrayList<Map<String, Object>> userComboItems = new ArrayList<Map<String, Object>>();
		for (User user : userList) {
			final HashMap<String, Object> item = new HashMap<String, Object>();
			item.put("itemText", user.getName());
			item.put("itemImage", getImage(user));
			item.put("object", user);
			userComboItems.add(item);
		}
		return userComboItems;
	}

	/**
	 * Gets the user group combo items.
	 *
	 * @param userGroupModelList
	 *            the user group model list
	 * @return the user group combo items
	 */
	public ArrayList<Map<String, Object>> getUserGroupComboItems(final List<UserGroupModel> userGroupModelList) {
		ArrayList<Map<String, Object>> userGroupComboItems = new ArrayList<Map<String, Object>>();
		for (UserGroupModel userGroupModel : userGroupModelList) {
			final HashMap<String, Object> item = new HashMap<String, Object>();
			item.put("itemText", userGroupModel.getName());
			item.put("itemImage", getImage(userGroupModel));
			item.put("object", userGroupModel);
			userGroupComboItems.add(item);
		}
		return userGroupComboItems;
	}

	/**
	 * Set project group combo.
	 *
	 * @param projectGroupModelList
	 *            the project group model list
	 * @return the array list
	 */
	public ArrayList<Map<String, Object>> setProjectGroupCombo(final List<ProjectGroupModel> projectGroupModelList) {
		ArrayList<Map<String, Object>> projectGroupComboItems = new ArrayList<Map<String, Object>>();
		for (ProjectGroupModel projectGroupModel : projectGroupModelList) {
			final HashMap<String, Object> item = new HashMap<String, Object>();
			item.put("itemText", projectGroupModel.getName());
			item.put("itemImage", getImage(projectGroupModel));
			item.put("object", projectGroupModel);
			projectGroupComboItems.add(item);
		}
		return projectGroupComboItems;

	}

	/**
	 * Filter combo.
	 *
	 * @param filterText
	 *            the filter text
	 * @param items
	 *            the items
	 * @param FilterCombo
	 *            the filter combo
	 */
	public void filterCombo(final String filterText, final List<Map<String, Object>> items,
			final MagnaCustomCombo FilterCombo) {
		try {
			if (filterText != null && filterText.length() >= 0 && items != null && !items.isEmpty()) {
				final List<Map<String, Object>> tempItems = new ArrayList<Map<String, Object>>();
				for (final Map<String, Object> item : items) {
					final Object itemText = item.get("itemText");
					if (itemText != null && itemText instanceof String
							&& Pattern.compile(Pattern.quote(filterText), Pattern.CASE_INSENSITIVE)
									.matcher((String) itemText).find()) {
						tempItems.add(item);
					}
				}

				setItemsWithImages(tempItems, FilterCombo);
			}
		} catch (Exception e) {
			LOGGER.error("Unable to filter the Combo", e); //$NON-NLS-1$
		}
	}

	/**
	 * Set items with images.
	 *
	 * @param items
	 *            the items
	 * @param combo
	 *            the combo
	 */
	public void setItemsWithImages(final List<Map<String, Object>> items, final MagnaCustomCombo combo) {
		try {
			String itemText = null;
			Object itemImage = null;
			final Table table = combo.getTable();
			table.removeAll();
			if (table.getColumnCount() <= 0) {
				new TableColumn(table, SWT.NONE);
			}
			if (items != null) {
				for (int i = 0; i < items.size(); i++) {
					itemText = (String) items.get(i).get("itemText");
					itemImage = items.get(i).get("itemImage");
					final TableItem item = new TableItem(table, SWT.NONE);
					if (itemText != null) {
						item.setText(itemText);
						if (itemImage instanceof Image) {
							item.setImage((Image) itemImage);
						}
					}
					item.setData(items.get(i).get("object"));
				}
			}
		} catch (Exception e) {
			// LOGGER.error("Unable to create table combo widget", e);
			// //$NON-NLS-1$
		}
	}

	/**
	 * Gets the image.
	 *
	 * @param object
	 *            the object
	 * @return the image
	 */
	public Image getImage(final Object object) {
		Image image = null;
		if (object != null) {
			if (object instanceof Site) {
				final Site site = (Site) object;
				image = XMSystemUtil.getInstance().getImage(this.getClass(), site.getIcon().getIconPath(),
						site.isActive(), true);
			} else if (object instanceof User) {
				final User user = (User) object;
				image = XMSystemUtil.getInstance().getImage(this.getClass(), user.getIcon().getIconPath(),
						user.isActive(), true);
			} else if (object instanceof Project) {
				final Project project = (Project) object;
				image = XMSystemUtil.getInstance().getImage(this.getClass(), project.getIcon().getIconPath(),
						project.isActive(), true);
			} else if (object instanceof AdministrationArea) {
				final AdministrationArea adminsArea = (AdministrationArea) object;
				image = XMSystemUtil.getInstance().getImage(this.getClass(), adminsArea.getIcon().getIconPath(),
						adminsArea.isActive(), true);
			} else if (object instanceof UserGroupModel) {
				final UserGroupModel userGroupModel = (UserGroupModel) object;
				image = XMSystemUtil.getInstance().getImage(this.getClass(), userGroupModel.getIcon().getIconPath(),
						true, true);
			} else if (object instanceof ProjectGroupModel) {
				final ProjectGroupModel projectGroupModel = (ProjectGroupModel) object;
				image = XMSystemUtil.getInstance().getImage(this.getClass(), projectGroupModel.getIcon().getIconPath(),
						true, true);
			}
		}
		return image;
	}
}
