
package com.magna.xmsystem.xmadmin.ui.handlers.baseapplication;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.inject.Inject;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.e4.core.di.annotations.CanExecute;
import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.core.services.nls.Translation;
import org.eclipse.e4.ui.model.application.MApplication;
import org.eclipse.e4.ui.model.application.ui.basic.MPart;
import org.eclipse.e4.ui.workbench.modeling.EModelService;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.swt.widgets.Display;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.client.ResourceAccessException;

import com.magna.xmbackend.vo.baseApplication.BaseApplicationResponse;
import com.magna.xmsystem.dependencies.customExceptions.UnauthorizedAccessException;
import com.magna.xmsystem.ui.controls.widgets.CustomMessageDialog;
import com.magna.xmsystem.xmadmin.message.Message;
import com.magna.xmsystem.xmadmin.restclient.application.BaseAppController;
import com.magna.xmsystem.xmadmin.ui.parts.IEditablePart;
import com.magna.xmsystem.xmadmin.ui.parts.InformationPart;
import com.magna.xmsystem.xmadmin.ui.parts.objexp.ObjectExpPage;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.AdminTreeviewer;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.AdminTreeFactory;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.baseapplicationmodel.BaseApplication;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.baseapplicationmodel.BaseApplications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.projectapplicationmodel.ProjectApplication;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.startapplicationmodel.StartApplication;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.userapplicationmodel.UserApplication;
import com.magna.xmsystem.xmadmin.util.CommonConstants;
import com.magna.xmsystem.xmadmin.util.MessageType;
import com.magna.xmsystem.xmadmin.util.XMAdminUtil;

/**
 * Class for Delete Base Application handler.
 *
 * @author Chiranjeevi.Akula
 */
public class DeleteBaseAppHandler {

	/** The Constant LOGGER. */
	private static final Logger LOGGER = LoggerFactory.getLogger(DeleteBaseAppHandler.class);

	/** Member variable for {@link Message}. */
	@Inject
	@Translation
	private Message messages;

	/** Member variable 'application' for {@link MApplication}. */
	@Inject
	private MApplication application;

	/** Member variable 'model service' for {@link EModelService}. */
	@Inject
	private EModelService modelService;

	/**
	 * Method for Execute.
	 */
	@Execute
	public void execute() {
		try {
			if (XMAdminUtil.getInstance().isAcessAllowed("BASE_APPLICATION-DELETE")) {
				final AdminTreeviewer adminTree = XMAdminUtil.getInstance().getAdminTree();
				IStructuredSelection selection;
				Object selectionObj;
				MPart mPart = (MPart) this.modelService.find(CommonConstants.PART_ID.INFORMATION_PART_ID, application);
				InformationPart rightSidePart2 = (InformationPart) mPart.getObject();
				ObjectExpPage objectExpPage = rightSidePart2.getObjectExpPartUI();
				if (objectExpPage != null
						&& (selection = objectExpPage.getObjExpTableViewer().getStructuredSelection()) != null
						&& (selectionObj = selection.getFirstElement()) != null
						&& selectionObj instanceof BaseApplication) {
					deleteBaseApplication(selection);
					objectExpPage.refreshExplorer();
				} else if (adminTree != null && (selection = adminTree.getStructuredSelection()) != null
						&& (selectionObj = selection.getFirstElement()) != null
						&& selectionObj instanceof BaseApplication) {
					deleteBaseApplication(selection);
				}
				XMAdminUtil.getInstance().clearClipBoardContents();
			} else {
				CustomMessageDialog.openError(Display.getCurrent().getActiveShell(),
						messages.objectPermissionDialogTitle, messages.objectPermissionDialogMsg);
			}
		} catch (ClassCastException e) {
			LOGGER.error("Exception occured while deleting baseApplication " + e);
		} catch (UnauthorizedAccessException e) {
			LOGGER.error("Current user is Unauthorized " + e);
		} catch (Exception e) {
			if (e instanceof ResourceAccessException) {
				CustomMessageDialog.openError(Display.getCurrent().getActiveShell(), messages.errorDialogTitile,
						messages.serverNotReachable);
			}
			LOGGER.error("Exception occured in calling access allowed API " + e);
		}
	}

	/**
	 * Delete base application.
	 *
	 * @param selectionObj
	 *            the selection obj
	 */
	@SuppressWarnings("rawtypes")
	private void deleteBaseApplication(IStructuredSelection selection) {
		Object selectionObj = selection.getFirstElement();
		String baseAppId;
		List<String> baseAppIdList = new ArrayList<>();
		final AdminTreeviewer adminTree = XMAdminUtil.getInstance().getAdminTree();
		final BaseApplications baseApplications = AdminTreeFactory.getInstance().getBaseApplications();
		Map<String, IAdminTreeChild> getUserapplication = AdminTreeFactory.getInstance().getUserApplications()
				.getUserApplications();
		List<IAdminTreeChild> userApplist = new ArrayList<>(getUserapplication.values());
		Map<String,String> successBaseAppNames = new HashMap<>();
		Map<String,String> failBaseAppNames = new HashMap<>();
		for (IAdminTreeChild iAdminTreeChild : userApplist) {
			UserApplication userApp = (UserApplication) iAdminTreeChild;
			if (userApp.getBaseApplicationId() != null) {
				baseAppId = userApp.getBaseApplicationId();
				baseAppIdList.add(baseAppId);
			}
		}
		Map<String, IAdminTreeChild> getProjectapplication = AdminTreeFactory.getInstance().getProjectApplications()
				.getProjectApplications();
		List<IAdminTreeChild> projectApplist = new ArrayList<>(getProjectapplication.values());
		for (IAdminTreeChild iAdminTreeChild : projectApplist) {
			ProjectApplication projectApp = (ProjectApplication) iAdminTreeChild;
			if (projectApp.getBaseApplicationId() != null) {
				baseAppId = projectApp.getBaseApplicationId();
				baseAppIdList.add(baseAppId);
			}
		}
		Map<String, IAdminTreeChild> getStartapplication = AdminTreeFactory.getInstance().getStartApplications()
				.getStartApplications();
		List<IAdminTreeChild> startApplist = new ArrayList<>(getStartapplication.values());
		for (IAdminTreeChild iAdminTreeChild : startApplist) {
			StartApplication startApp = (StartApplication) iAdminTreeChild;
			if (startApp.getBaseApplicationId() != null) {
				baseAppId = startApp.getBaseApplicationId();
				baseAppIdList.add(baseAppId);
			}
		}
		List selectionList = selection.toList();
		List<String> deleteBaseAppIds = new ArrayList<>();
		String confirmDialogMsg = CommonConstants.EMPTY_STR;
		boolean isSucess = true;
		for (int i = 0; i < selectionList.size(); i++) {
			selectionObj = selectionList.get(i);
			if (selectionObj instanceof BaseApplication) {
				BaseApplication baseApp = (BaseApplication) selectionObj;
				baseAppId = baseApp.getBaseApplicationId();
				if (baseAppIdList.contains(baseAppId)) {
					isSucess = false;
					failBaseAppNames.put(baseAppId,baseApp.getName());
				} else {
					deleteBaseAppIds.add(baseApp.getBaseApplicationId());
					successBaseAppNames.put(baseAppId,baseApp.getName());
				}
			}
		}
		String confirmDialogTitle = messages.deleteConfirmDialogTitle;
		if (selectionList.size() == 1 && !isSucess) {
			confirmDialogMsg = messages.deleteBaseAppRelationDialogMsgPart1 + "\'"
					+ ((BaseApplication) selectionObj).getName() + "\' " + messages.deleteBaseAppRelationDialogMsgPart2;
			//XMAdminUtil.getInstance().updateLogFile(confirmDialogMsg, MessageType.FAILURE);
		} else if (selectionList.size() == 1 && isSucess) {
			confirmDialogMsg =  messages.deleteBaseAppConfirmDialogMsg + " \'" + ((BaseApplication) selectionObj).getName() +"\' " + messages.deleteStartAppConfirmDialogMsgPart2 + " ?";
		} else if (selectionList.size() > 1 && isSucess) {
			confirmDialogMsg = messages.deleteMultiObjectsConfirmDialogMsg + " "
					+ messages.deleteBaseAppRelationDialogMsgPart2 + " ?";
		} else if (selectionList.size() > 1 && !isSucess) {
			confirmDialogMsg = messages.deleteBaseAppRelationDialogMsgPart1 + " " + messages.deleteBaseAppRelationDialogMsgPart2;
			//XMAdminUtil.getInstance().updateLogFile(confirmDialogMsg, MessageType.FAILURE);
		}
		boolean isConfirmed = CustomMessageDialog.openConfirm(adminTree.getControl().getShell(), confirmDialogTitle,
				confirmDialogMsg);
		if (isConfirmed) {

			Job job = new Job("Deleting Objects...") {

				@Override
				protected IStatus run(IProgressMonitor monitor) {
					monitor.beginTask("Deleting Objects..", 100);
					monitor.worked(30);
					try {
						Set<String> baseAppIds = new HashSet<>();
						for (int i = 0; i < deleteBaseAppIds.size(); i++) {
							baseAppIds.add(deleteBaseAppIds.get(i));
						}
						final BaseAppController baseAppController = new BaseAppController();
						final BaseApplicationResponse baseAppResponse = baseAppController
								.deleteMultiBaseApp(baseAppIds);
						if (baseAppResponse != null) {
							if (!baseAppResponse.getStatusMaps().isEmpty()) {
								List<Map<String, String>> statusMapList = baseAppResponse.getStatusMaps();
								for (Map<String, String> statusMap : statusMapList) {
									String string = statusMap.get("en");
									String[] split = string.split("id");
									baseAppIds.remove(split[1].trim());
								}
								Display.getDefault().asyncExec(new Runnable() {
									public void run() {
										CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
												messages.deleteConfirmDialogTitle,
												messages.deleteMultiObjectsDialogErrMsg);
									}
								});
							}
							for (String baseAppId : baseAppIds) {
								baseApplications.getBaseApplications().remove(baseAppId);
							}
							Display.getDefault().asyncExec(new Runnable() {
								@Override
								public void run() {
									adminTree.refresh();
									adminTree.setSelection(new StructuredSelection(baseApplications), true);
								}
							});
							
							if (successBaseAppNames.size() > 0) {
								Iterator it = successBaseAppNames.entrySet().iterator();
								while (it.hasNext()) {
									Map.Entry map = (Map.Entry) it.next();
									XMAdminUtil.getInstance().updateLogFile(messages.baseApplicationObject +" '"
											+ map.getValue() + "' "+ messages.objectDelete, MessageType.SUCCESS);
								}
							}

							if (failBaseAppNames.size() > 0) {
								Iterator it = failBaseAppNames.entrySet().iterator();
								while (it.hasNext()) {
									Map.Entry map = (Map.Entry) it.next();
									XMAdminUtil.getInstance().updateLogFile(
											messages.deleteBaseAppRelationDialogMsgPart1 + "\'" + map.getValue() + "\' "
													+ messages.deleteBaseAppRelationDialogMsgPart2,
											MessageType.FAILURE);
								}
							}
						}

					} catch (Exception e) {
						LOGGER.error(e.getMessage());
					}

					monitor.worked(70);
					return Status.OK_STATUS;
				}
			};
			job.setUser(true);
			job.schedule();

		}
	}

	/**
	 * Method for Can execute.
	 *
	 * @return true, if successful
	 */
	@CanExecute
	public boolean canExecute() {
		boolean returnVal = true;
		final XMAdminUtil instance = XMAdminUtil.getInstance();
		MPart rightPart;
		Object rightView;
		if ((rightPart = instance.getInformationPart()) != null
				&& (rightView = rightPart.getObject()) != null && rightView instanceof IEditablePart
				&& ((IEditablePart) rightView).isDirty()) {
			returnVal = false;
		}
		return returnVal;
	}
}
