package com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.roles;

import java.beans.PropertyChangeEvent;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.TreeMap;
import java.util.function.BiConsumer;

import com.magna.xmsystem.dependencies.utils.XMSystemUtil;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.BeanModel;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.adminarea.AdministrationAreas;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.roles.objRelationPermissions.ObjectRelationPermissions;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.roles.objectpermissions.ObjectPermissions;
import com.magna.xmsystem.xmadmin.util.CommonConstants;

/**
 * The Class Role.
 *
 * @author Rohsan.Ekka
 */
public class Role extends BeanModel implements IAdminTreeChild {

	/** Constant variable for desc text limit. */
	public static final int DESC_LIMIT = 240;
	
	/** The Constant NAME_LIMIT. */
	public static final int NAME_LIMIT = 30;

	/** The Constant PROPERTY_ROLEID. */
	public static final String PROPERTY_ROLEID = "roleId"; //$NON-NLS-1$

	/** The Constant PROPERTY_ROLE_NAME. */
	public static final String PROPERTY_ROLE_NAME = "roleName"; //$NON-NLS-1$

	/** The Constant PROPERTY_ROLE_DESCRIPTION. */
	public static final String PROPERTY_ROLE_DESCRIPTION = "roleDesc"; //$NON-NLS-1$

	/** The Constant PROPERTY_ROLE_OBJPERMISSIONMAP. */
	public static final String PROPERTY_ROLE_OBJPERMISSIONMAP = "objPermissonMap"; //$NON-NLS-1$

	/** The Constant PROPERTY_ROLE_OBJRELATION_PERMISSIONMAP. */
	public static final String PROPERTY_ROLE_OBJRELATION_PERMISSIONMAP = "objRelationPermissonMap"; //$NON-NLS-1$

	/** The Constant PROPERTY_ROLE_OPERATIONMODE. */
	public static final String PROPERTY_ROLE_OPERATIONMODE = "operationMode"; //$NON-NLS-1$

	/** The Constant PROPERTY_ROLE_OPERATIONMODE. */
	public static final String PROPERTY_ROLE_VIEW_INACTIVE = "isViewInactive"; //$NON-NLS-1$
	
	/** The Constant PROPERTY_ROLE_XM_ADMIN. */
	public static final String PROPERTY_ROLE_XM_ADMIN = "isXmAdmin"; //$NON-NLS-1$
	
	/** The Constant PROPERTY_ROLE_XM_HOTLINE. */
	public static final String PROPERTY_ROLE_XM_HOTLINE = "isXmHotline"; //$NON-NLS-1$
	
	/** The role id. */
	private String roleId;

	/** The role name. */
	private String roleName;

	/** The role desc. */
	private String roleDesc;
	
	/** The is view inactive. */
	private boolean isViewInactive;
	
	/** The is xm admin. */
	private boolean isXmAdmin;
	
	/** The is xm hotline. */
	private boolean isXmHotline;
	
	/** The permisson map. */
	private Map<String, ObjectPermissions> objPermissonMap;

	/** The obj relation permisson map. */
	private Map<String, ObjectRelationPermissions> objRelationPermissonMap;

	/** The operation mode. */
	private int operationMode;

	/** The role child. */
	private Map<String, IAdminTreeChild> roleChildren;

	/**
	 * Instantiates a new role.
	 *
	 * @param roleId
	 *            the role id
	 * @param roleName
	 *            the role name
	 * @param roleDesc
	 *            the role desc
	 * @param operationMode
	 *            the operation mode
	 */
	public Role(final String roleId, final String roleName, final String roleDesc, final int operationMode) {
		this(roleId, roleName, operationMode, roleDesc, new TreeMap<>(), new TreeMap<>());
	}

	/**
	 * Instantiates a new role.
	 *
	 * @param roleId
	 *            the role id
	 * @param roleName
	 *            the role name
	 * @param operationMode
	 *            the operation mode
	 * @param roleDesc
	 *            the role desc
	 * @param objPermissonMap
	 *            the obj permisson map
	 * @param objRelationPermissonMap
	 *            the obj relation permisson map
	 */
	public Role(final String roleId, final String roleName, final int operationMode, final String roleDesc,
			final Map<String, ObjectPermissions> objPermissonMap,
			final Map<String, ObjectRelationPermissions> objRelationPermissonMap) {
		super();
		this.roleId = roleId;
		this.roleName = roleName;
		this.operationMode = operationMode;
		this.roleDesc = roleDesc;
		this.objPermissonMap = objPermissonMap;
		this.objRelationPermissonMap = objRelationPermissonMap;
		this.roleChildren = new LinkedHashMap<>();
		addFixedChildren();
		initRolePermission();
	}

	/**
	 * Constructor for superAdmin Role Instantiates a new role.
	 */
	/*
	 * public Role() { this.roleId = UUID.randomUUID().toString(); this.roleName
	 * = CommonConstants.SuperAdminRole.NAME; this.roleDesc =
	 * CommonConstants.SuperAdminRole.DESCRIPATION; this.roleChild = new
	 * LinkedHashMap<>(); this.roleChild.put(RoleUsers.class.getName(), new
	 * RoleUsers(this)); this.objPermissonMap = new LinkedHashMap<>();
	 * this.objRelationPermissonMap = new LinkedHashMap<>();
	 * initSuperAdminPermission(); }
	 */
	/**
	 * Method for Adds the fixed children.
	 */
	public void addFixedChildren() {
		if (!CommonConstants.SuperAdminRole.NAME.equals(this.roleName)) {
			roleChildren.put(RoleScopeObjects.class.getName(), new RoleScopeObjects(this, AdministrationAreas.class.getName()));
		}
		roleChildren.put(RoleUsers.class.getName(), new RoleUsers(this));
	}
	/**
	 * Inits the super admin permission.
	 */
	/*
	 * private void initSuperAdminPermission() { final ObjectType[]
	 * objectTypeValues = ObjectType.values(); for (ObjectType objectType :
	 * objectTypeValues) { this.objPermissonMap.put(objectType.name(), new
	 * ObjectPermissions(objectType.name(), true, true, true, true, true)); }
	 * final ObjectRelationType[] objectRelationTypes =
	 * ObjectRelationType.values(); for (ObjectRelationType objectRelationType :
	 * objectRelationTypes) {
	 * this.objRelationPermissonMap.put(objectRelationType.name(), new
	 * ObjectRelationPermissions(objectRelationType.name(), true, true, true,
	 * true)); } }
	 */

	/**
	 * Inits the role permission.
	 */
	public void initRolePermission() {
		final ObjectType[] objectTypeValues = ObjectType.values();
		for (ObjectType objectType : objectTypeValues) {
			this.objPermissonMap.put(objectType.name(),
					new ObjectPermissions(objectType.name(), false, false, false, false));
		}
		final ObjectRelationType[] objectRelationTypes = ObjectRelationType.values();
		for (ObjectRelationType objectRelationType : objectRelationTypes) {
			this.objRelationPermissonMap.put(objectRelationType.name(),
					new ObjectRelationPermissions(objectRelationType.name(), false, false, false, false));
		}
	}

	/**
	 * Gets the role id.
	 *
	 * @return the role id
	 */
	public String getRoleId() {
		return roleId;
	}

	/**
	 * Gets the role name.
	 *
	 * @return the role name
	 */
	public String getRoleName() {
		return roleName;
	}

	/**
	 * Gets the role desc.
	 *
	 * @return the role desc
	 */
	public String getRoleDesc() {
		return roleDesc;
	}

	/**
	 * Sets the role id.
	 *
	 * @param roleId
	 *            the new role id
	 */
	public void setRoleId(String roleId) {
		// this.roleId = roleId;
		/*
		 * if (roleId == null) { throw new IllegalArgumentException(); }
		 */
		this.propertyChangeSupport.firePropertyChange(PROPERTY_ROLEID, this.roleId, this.roleId = roleId);
	}

	/**
	 * Sets the role name.
	 *
	 * @param roleName
	 *            the new role name
	 */
	public void setRoleName(String roleName) {
		// this.roleName = roleName;
		if (roleName == null) {
			throw new IllegalArgumentException();
		}
		this.propertyChangeSupport.firePropertyChange(PROPERTY_ROLE_NAME, this.roleName,
				this.roleName = roleName.trim());
	}

	/**
	 * Sets the role desc.
	 *
	 * @param roleDesc
	 *            the new role desc
	 */
	public void setRoleDesc(final String roleDesc) {
		if (!XMSystemUtil.isEmpty(roleDesc)) {
			this.propertyChangeSupport.firePropertyChange(PROPERTY_ROLE_DESCRIPTION, this.roleDesc, this.roleDesc = roleDesc);
		}
	}

	/**
	 * @return the isViewInactive
	 */
	public boolean getIsViewInactive() {
		return isViewInactive;
	}

	/**
	 * @param isViewInactive the isViewInactive to set
	 */
	public void setIsViewInactive(final boolean isViewInactive) {
		this.propertyChangeSupport.firePropertyChange(PROPERTY_ROLE_VIEW_INACTIVE, this.isViewInactive,
				this.isViewInactive = isViewInactive);
	}

	/**
	 * @return the isXmAdmin
	 */
	public boolean getIsXmAdmin() {
		return isXmAdmin;
	}

	/**
	 * @param isXmAdmin the isXmAdmin to set
	 */
	public void setIsXmAdmin(final boolean isXmAdmin) {
		this.propertyChangeSupport.firePropertyChange(PROPERTY_ROLE_XM_ADMIN, this.isXmAdmin,
				this.isXmAdmin = isXmAdmin);
	}

	/**
	 * @return the isXmHotline
	 */
	public boolean getIsXmHotline() {
		return isXmHotline;
	}

	/**
	 * @param isXmHotline the isXmHotline to set
	 */
	public void setIsXmHotline(final boolean isXmHotline) {
		this.propertyChangeSupport.firePropertyChange(PROPERTY_ROLE_XM_HOTLINE, this.isXmHotline,
				this.isXmHotline = isXmHotline);
		//this.isXmHotline = isXmHotline;
	}

	/**
	 * Gets the obj permisson map.
	 *
	 * @return the obj permisson map
	 */
	public Map<String, ObjectPermissions> getObjPermissonMap() {
		return objPermissonMap;
	}

	/**
	 * Sets the permisson map.
	 *
	 * @param objPermissonMap
	 *            the permisson map
	 */
	public void setObjPermissonMap(Map<String, ObjectPermissions> objPermissonMap) {
		if (objPermissonMap == null) {
			throw new IllegalArgumentException();
		}
		this.propertyChangeSupport.firePropertyChange(PROPERTY_ROLE_OBJPERMISSIONMAP, this.objPermissonMap,
				this.objPermissonMap = objPermissonMap);
		// this.objPermissonMap = permissonMap;
	}

	/**
	 * Gets the obj relation permisson map.
	 *
	 * @return the obj relation permisson map
	 */
	public Map<String, ObjectRelationPermissions> getObjRelationPermissonMap() {
		return objRelationPermissonMap;
	}

	/**
	 * Sets the obj relation permisson map.
	 *
	 * @param objRelationPermissonMap
	 *            the obj relation permisson map
	 */
	public void setObjRelationPermissonMap(Map<String, ObjectRelationPermissions> objRelationPermissonMap) {
		if (objRelationPermissonMap == null) {
			throw new IllegalArgumentException();
		}
		this.propertyChangeSupport.firePropertyChange(PROPERTY_ROLE_OBJRELATION_PERMISSIONMAP,
				this.objRelationPermissonMap, this.objRelationPermissonMap = objRelationPermissonMap);
		// this.objRelationPermissonMap = objRelationPermissonMap;
	}

	/**
	 * Gets the operation mode.
	 *
	 * @return the operation mode
	 */
	public int getOperationMode() {
		return operationMode;
	}

	/**
	 * Sets the operation mode.
	 *
	 * @param operationMode
	 *            the new operation mode
	 */
	public void setOperationMode(int operationMode) {

		this.propertyChangeSupport.firePropertyChange(PROPERTY_ROLE_OPERATIONMODE, this.operationMode,
				this.operationMode = operationMode);
		// this.operationMode = operationMode;
	}

	/**
	 * Gets the role child.
	 *
	 * @return the role child
	 */
	public Map<String, IAdminTreeChild> getRoleChildren() {
		return roleChildren;
	}

	/**
	 * Sets the role child.
	 *
	 * @param roleChild
	 *            the role child
	 */
	public void setRoleChildren(Map<String, IAdminTreeChild> roleChild) {
		this.roleChildren = roleChild;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.BeanModel#
	 * propertyChange(java.beans.PropertyChangeEvent)
	 */
	@Override
	public void propertyChange(PropertyChangeEvent event) {
		// TODO Auto-generated method stub

	}

	/**
	 * Deep copy role.
	 *
	 * @param update
	 *            the update
	 * @param updateThisObject
	 *            the update this object
	 * @return the role
	 */
	public Role deepCopyRole(boolean update, Role updateThisObject) {
		Role clonedRole = null;
		try {
			String currentRoleId = XMSystemUtil.isEmpty(this.getRoleId()) ? CommonConstants.EMPTY_STR
					: this.getRoleId();
			String currentName = XMSystemUtil.isEmpty(this.getRoleName()) ? CommonConstants.EMPTY_STR
					: this.getRoleName();

			String currentDes = XMSystemUtil.isEmpty(this.getRoleDesc()) ? CommonConstants.EMPTY_STR
					: this.getRoleDesc();

			Map<String, ObjectPermissions> currentObjPermissionMap = getObjPermissonMap();
			Map<String, ObjectRelationPermissions> currentObjRelPermissionMap = getObjRelationPermissonMap();
			
			Map<String, ObjectPermissions> newObjPermissionMap = new TreeMap<>();
			Map<String, ObjectRelationPermissions> newObjRelPermissionMap = new TreeMap<>();
			boolean currentRoleViewInactive = this.isViewInactive;
			boolean currentRoleXmAdmin = this.isXmAdmin;
			boolean currentRoleXmHotline = this.isXmHotline;
			currentObjPermissionMap.forEach(new BiConsumer<String, ObjectPermissions>() {
				@Override
				public void accept(String t, ObjectPermissions u) {
					newObjPermissionMap.put(t, u);
				}
			});
			
			currentObjRelPermissionMap.forEach(new BiConsumer<String, ObjectRelationPermissions>() {
				@Override
				public void accept(String t, ObjectRelationPermissions u) {
					newObjRelPermissionMap.put(t, u);
				}
			});

			if (update) {
				clonedRole = updateThisObject;
			} else {
				clonedRole = new Role(currentRoleId, currentName, currentDes, CommonConstants.OPERATIONMODE.VIEW);
			}
			clonedRole.setRoleId(currentRoleId);
			clonedRole.setRoleName(currentName);
			clonedRole.setRoleDesc(currentDes);
			clonedRole.setObjPermissonMap(newObjPermissionMap);
			clonedRole.setObjRelationPermissonMap(newObjRelPermissionMap);
			clonedRole.setIsViewInactive(currentRoleViewInactive);
			clonedRole.setIsXmAdmin(currentRoleXmAdmin);
			clonedRole.setIsXmHotline(currentRoleXmHotline);
			
			return clonedRole;
		} catch (Exception e) {
			return null;
		}

	}

}
