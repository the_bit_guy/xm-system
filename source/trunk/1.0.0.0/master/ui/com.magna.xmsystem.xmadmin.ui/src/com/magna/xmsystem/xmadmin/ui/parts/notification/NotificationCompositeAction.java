package com.magna.xmsystem.xmadmin.ui.parts.notification;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.inject.Inject;

import org.eclipse.core.databinding.DataBindingContext;
import org.eclipse.core.databinding.beans.BeanProperties;
import org.eclipse.core.databinding.observable.value.IObservableValue;
import org.eclipse.e4.core.services.nls.Translation;
import org.eclipse.e4.ui.model.application.ui.MDirtyable;
import org.eclipse.jface.databinding.swt.WidgetProperties;
import org.eclipse.jface.fieldassist.ControlDecoration;
import org.eclipse.jface.fieldassist.FieldDecorationRegistry;
import org.eclipse.jface.resource.JFaceResources;
import org.eclipse.jface.resource.LocalResourceManager;
import org.eclipse.jface.resource.ResourceManager;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.StyleRange;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.events.VerifyEvent;
import org.eclipse.swt.events.VerifyListener;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.swt.widgets.Text;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.magna.xmbackend.entities.AdminAreaTranslationTbl;
import com.magna.xmbackend.entities.AdminAreasTbl;
import com.magna.xmbackend.entities.IconsTbl;
import com.magna.xmbackend.entities.ProjectTranslationTbl;
import com.magna.xmbackend.entities.ProjectsTbl;
import com.magna.xmbackend.entities.UserTranslationTbl;
import com.magna.xmbackend.entities.UsersTbl;
import com.magna.xmbackend.vo.enums.NotificationEventType;
import com.magna.xmbackend.vo.enums.PermissionType;
import com.magna.xmbackend.vo.jpa.site.SiteAdminAreaRelIdWithAdminArea;
import com.magna.xmbackend.vo.notification.NotificationResponse;
import com.magna.xmbackend.vo.permission.ValidationRequest;
import com.magna.xmsystem.dependencies.customExceptions.UnauthorizedAccessException;
import com.magna.xmsystem.dependencies.utils.LANG_ENUM;
import com.magna.xmsystem.dependencies.utils.XMSystemUtil;
import com.magna.xmsystem.ui.controls.widgets.CustomMessageDialog;
import com.magna.xmsystem.ui.controls.widgets.MagnaCustomCombo;
import com.magna.xmsystem.xmadmin.message.Message;
import com.magna.xmsystem.xmadmin.message.MessageRegistry;
import com.magna.xmsystem.xmadmin.restclient.notify.NotificationController;
import com.magna.xmsystem.xmadmin.restclient.project.ProjectController;
import com.magna.xmsystem.xmadmin.restclient.site.SiteController;
import com.magna.xmsystem.xmadmin.restclient.users.UserController;
import com.magna.xmsystem.xmadmin.restclient.util.RestClientUtil;
import com.magna.xmsystem.xmadmin.restclient.validation.ValidationController;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.AdminTreeviewer;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.AdminTreeDataLoad;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.AdminTreeFactory;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.adminarea.AdministrationArea;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.adminarea.AdministrationAreas;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.icon.Icon;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.notification.Notification;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.group.usergroup.UserGroupModel;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.group.usergroup.UserGroupUsers;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.group.usergroup.UserGroupsModel;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.projects.Project;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.projects.Projects;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.refs.RelationObj;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.sitemodel.Site;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.sitemodel.Sites;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.users.User;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.users.Users;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.users.UsersNameAlphabet;
import com.magna.xmsystem.xmadmin.util.CommonConstants;
import com.magna.xmsystem.xmadmin.util.XMAdminUtil;

/**
 * The Class NotificationCompositeAction.
 * 
 * @author archita.patel
 */
public class NotificationCompositeAction extends NotificationCompositeUI {

	/** The Constant LOGGER. */
	private static final Logger LOGGER = LoggerFactory.getLogger(NotificationCompositeAction.class);

	/** The notification model. */
	private Notification notificationModel;

	/** Member variable for widgetValue. */
	private IObservableValue<?> widgetValue;

	/** Member variable for modelValue. */
	private IObservableValue<?> modelValue;

	/** The registry. */
	@Inject
	private MessageRegistry registry;

	/** The messages. */
	@Inject
	@Translation
	private Message messages;

	/** The data bind context. */
	final transient DataBindingContext dataBindContext = new DataBindingContext();

	/** The old model. */
	private Notification oldModel;

	/** The dirty. */
	private MDirtyable dirty;

	/** The site controller. */
	private SiteController siteController;

	/** The project controller. */
	private ProjectController projectController;

	/** The user controller. */
	private UserController userController;

	/** The user combo items. */
	protected transient ArrayList<Map<String, Object>> userComboItems;

	/** The user group model list. */
	protected List<UserGroupModel> userGroupModelList;

	/** The site combo items. */
	protected transient List<Map<String, Object>> siteComboItems;

	/** The admin area combo items. */
	private List<Map<String, Object>> adminAreaComboItems;

	/** The project combo items. */
	private List<Map<String, Object>> projectComboItems;

	/** The resource manager. */
	protected final ResourceManager resourceManager;

	/** The to users. */
	private StringBuilder users;

	/** The user group combo items. */
	private ArrayList<Map<String, Object>> userGroupComboItems;

	/** The message info decroator. */
	private ControlDecoration messageInfoDecroator;

	/**
	 * Instantiates a new notification composite action.
	 *
	 * @param parent
	 *            the parent
	 */
	@Inject
	public NotificationCompositeAction(final Composite parent) {
		super(parent, SWT.NONE);
		this.siteController = new SiteController();
		this.projectController = new ProjectController();
		this.userController = new UserController();
		this.resourceManager = new LocalResourceManager(JFaceResources.getResources());
		initListeners();
	}

	/**
	 * Register messages.
	 *
	 * @param registry
	 *            the registry
	 */
	public void registerMessages(final MessageRegistry registry) {

		registry.register((text) -> {
			if (lblActive != null && !lblActive.isDisposed()) {
				lblActive.setText(text);
			}
		}, (message) -> {
			if (lblActive != null && !lblActive.isDisposed()) {
				return getUpdatedWidgetText(message.objectActiveLabel, lblActive);
			}
			return CommonConstants.EMPTY_STR;
		});

		registry.register((text) -> {
			if (editButton != null && !editButton.isDisposed()) {
				editButton.setText(text);
			}
		}, (message) -> {
			if (editButton != null && !editButton.isDisposed()) {
				return getUpdatedWidgetText(message.notificationEditBtn, editButton);
			}
			return CommonConstants.EMPTY_STR;
		});

		registry.register((text) -> {
			if (lblDescription != null && !lblDescription.isDisposed()) {
				lblDescription.setText(text);
			}
		}, (message) -> {
			if (lblDescription != null && !lblDescription.isDisposed()) {
				return getUpdatedWidgetText(message.notificationDescriptionLbl, lblDescription);
			}
			return CommonConstants.EMPTY_STR;
		});

		registry.register((text) -> {
			if (lblToUsers != null && !lblToUsers.isDisposed()) {
				lblToUsers.setText(text);
			}
		}, (message) -> {
			if (lblToUsers != null && !lblToUsers.isDisposed()) {
				return getUpdatedWidgetText(message.notificationToUserLbl, lblToUsers);
			}
			return CommonConstants.EMPTY_STR;
		});

		registry.register((text) -> {
			if (addToolItem != null && !addToolItem.isDisposed()) {
				addToolItem.setToolTipText(text);
				;
			}
		}, (message) -> {
			if (addToolItem != null && !addToolItem.isDisposed()) {
				return message.notificationAddBtn;
			}
			return CommonConstants.EMPTY_STR;
		});

		registry.register((text) -> {
			if (lblEventType != null && !lblEventType.isDisposed()) {
				lblEventType.setText(text);
			}
		}, (message) -> {
			if (lblEventType != null && !lblEventType.isDisposed()) {
				return getUpdatedWidgetText(message.notificationEventTypeLbl, lblEventType);
			}
			return CommonConstants.EMPTY_STR;
		});

		registry.register((text) -> {
			if (radioBtnUser != null && !radioBtnUser.isDisposed()) {
				radioBtnUser.setText(text);
			}
		}, (message) -> {
			if (radioBtnUser != null && !radioBtnUser.isDisposed()) {
				return getUpdatedWidgetText(message.notificationAddUserRadioBtnLbl, radioBtnUser);
			}
			return CommonConstants.EMPTY_STR;
		});

		registry.register((text) -> {
			if (radioBtnUserGroup != null && !radioBtnUserGroup.isDisposed()) {
				radioBtnUserGroup.setText(text);
			}
		}, (message) -> {
			if (radioBtnUserGroup != null && !radioBtnUserGroup.isDisposed()) {
				return getUpdatedWidgetText(message.notificationUserGroupRadioBtnLbl, radioBtnUserGroup);
			}
			return CommonConstants.EMPTY_STR;
		});

		registry.register((text) -> {
			if (lblMessage != null && !lblMessage.isDisposed()) {
				lblMessage.setText(text);
			}
		}, (message) -> {
			if (lblMessage != null && !lblMessage.isDisposed()) {
				return getUpdatedWidgetText(message.notificationMessageLbl, lblMessage);
			}
			return CommonConstants.EMPTY_STR;
		});

		registry.register((text) -> {
			if (lblSubject != null && !lblSubject.isDisposed()) {
				lblSubject.setText(text);
			}
		}, (message) -> {
			if (lblSubject != null && !lblSubject.isDisposed()) {
				return getUpdatedWidgetText(message.notificationSubjectLbl, lblSubject);
			}
			return CommonConstants.EMPTY_STR;
		});

		if (saveBtn != null) {
			registry.register((text) -> {
				if (saveBtn != null && !saveBtn.isDisposed()) {
					saveBtn.setText(text);
				}
			}, (message) -> {
				if (saveBtn != null && !saveBtn.isDisposed()) {
					return getUpdatedWidgetText(message.saveButtonText, saveBtn);
				}
				return CommonConstants.EMPTY_STR;
			});
		}

		if (cancelBtn != null) {
			registry.register((text) -> {
				if (cancelBtn != null && !cancelBtn.isDisposed()) {
					cancelBtn.setText(text);
				}
			}, (message) -> {
				if (cancelBtn != null && !cancelBtn.isDisposed()) {
					return getUpdatedWidgetText(message.cancelButtonText, cancelBtn);
				}
				return CommonConstants.EMPTY_STR;
			});
		}

		registry.register((text) -> {
			if (includeRemarksBtn != null && !includeRemarksBtn.isDisposed()) {
				includeRemarksBtn.setText(text);
			}
		}, (message) -> {
			if (includeRemarksBtn != null && !includeRemarksBtn.isDisposed()) {
				return getUpdatedWidgetText(message.notificationIncludeRemarkBtn, includeRemarksBtn);
			}
			return CommonConstants.EMPTY_STR;
		});

		registry.register((text) -> {
			if (sendToAssignUserBtn != null && !sendToAssignUserBtn.isDisposed()) {
				sendToAssignUserBtn.setText(text);
			}
		}, (message) -> {
			if (sendToAssignUserBtn != null && !sendToAssignUserBtn.isDisposed()) {
				return getUpdatedWidgetText(message.notificationSendToAssUserBtn, sendToAssignUserBtn);
			}
			return CommonConstants.EMPTY_STR;
		});

		registry.register((text) -> {
		}, (message) -> {
			if (this.txtMessage != null && !this.txtMessage.isDisposed() && this.cmbEventType != null
					&& !this.cmbEventType.isDisposed() && this.txtMessage.isFocusControl()) {
				this.cmbEventType.setFocus();
			}
			/*
			 * if (messageInfoDecroator != null &&
			 * messageInfoDecroator.isVisible()) { messageInfoDecroator.hide();
			 * this.cmbEventType.setFocus();
			 * if(this.txtMessage.isFocusControl()){ messageInfoDecroator =
			 * null; updateMessageDecoInfo(); this.cmbEventType.setFocus(); } }
			 */
			return CommonConstants.EMPTY_STR;
		});

		this.filterPanel.registerMessages(registry);
		this.userGroupFilterPanel.registerMessages(registry);
		initlizeEventType();
		initlizeFilterCombos();
	}

	/**
	 * Bind values.
	 */
	@SuppressWarnings("unchecked")
	public void bindValues() {
		try {

			widgetValue = WidgetProperties.selection().observe(this.activeBtn);
			modelValue = BeanProperties.value(Notification.class, Notification.PROPERTY_ACTIVE)
					.observe(this.notificationModel);
			dataBindContext.bindValue(widgetValue, modelValue);

			widgetValue = WidgetProperties.text(SWT.Modify).observe(this.txtDescription);
			modelValue = BeanProperties.value(Notification.class, Notification.PROPERTY_DESCRIPTION)
					.observe(this.notificationModel);
			dataBindContext.bindValue(widgetValue, modelValue);

			widgetValue = WidgetProperties.text(SWT.Modify).observe(this.txtSubject);
			modelValue = BeanProperties.value(Notification.class, Notification.PROPERTY_SUBJECT)
					.observe(this.notificationModel);
			dataBindContext.bindValue(widgetValue, modelValue);

			widgetValue = WidgetProperties.text(SWT.Modify).observe(this.txtMessage);
			modelValue = BeanProperties.value(Notification.class, Notification.PROPERTY_MESSAGE)
					.observe(this.notificationModel);
			dataBindContext.bindValue(widgetValue, modelValue);

			/*
			 * ControlDecoration messageInfoDecroator = new
			 * ControlDecoration(this.txtMessage, SWT.TOP); final Image
			 * messageInfoDecoratorImage = FieldDecorationRegistry.getDefault()
			 * .getFieldDecoration(FieldDecorationRegistry.DEC_INFORMATION).
			 * getImage();
			 * messageInfoDecroator.setImage(messageInfoDecoratorImage);
			 */
			// updateMessageInfo();
			// messageInfoDecroator.setShowOnlyOnFocus(true);

			if (messageInfoDecroator == null) {
				messageInfoDecroator = new ControlDecoration(this.txtMessage, SWT.TOP);
				final Image messageInfoDecoratorImage = FieldDecorationRegistry.getDefault()
						.getFieldDecoration(FieldDecorationRegistry.DEC_INFORMATION).getImage();
				messageInfoDecroator.setImage(messageInfoDecoratorImage);
				messageInfoDecroator.setShowOnlyOnFocus(true);
			}
			messageInfoDecroator.hide();

			widgetValue = WidgetProperties.selection().observe(this.includeRemarksBtn);
			modelValue = BeanProperties.value(Notification.class, Notification.PROPERTY_INCLUDEREMARKS)
					.observe(this.notificationModel);
			dataBindContext.bindValue(widgetValue, modelValue);

			widgetValue = WidgetProperties.selection().observe(this.sendToAssignUserBtn);
			modelValue = BeanProperties.value(Notification.class, Notification.PROPERTY_SENDTO_ASSIGNUSER)
					.observe(this.notificationModel);
			dataBindContext.bindValue(widgetValue, modelValue);

			StyleRange style1 = new StyleRange();
			style1.start = 0;
			style1.length = txtDescription.getText().length();
			style1.fontStyle = SWT.ITALIC;
			txtDescription.setStyleRange(style1);

		} catch (Exception e) {
			LOGGER.error("Exception occured while databinding", e);
		}
	}

	/**
	 * Update message deco info.
	 */
	private void updateMessageDecoInfo() {/*
		StringBuilder message = new StringBuilder();
		String eventType = this.cmbEventType.getText();
		messageInfoDecroator.show();
		if (eventType.trim().isEmpty()) {
			messageInfoDecroator.setDescriptionText(message.toString());
			messageInfoDecroator.hide();
			return;
		}
		message.append(messages.notifiMessageDecoTitle);
		message.append("\n").append("\n");
		message.append(NotifMessage.TO_USERNAME).append(" ").append(":").append(" ")
				.append(messages.notifiMessageDecoUserName).append("\n");

		if (eventType.equals(NotificationEventType.USER_PROJECT_RELATION_ASSIGN.toString())
				|| eventType.equals(NotificationEventType.USER_PROJECT_RELATION_REMOVE.toString())) {

			message.append(NotifMessage.PROJECT_NAME).append(" ").append(":").append(" ")
					.append(messages.notifiMessageDecoAssignProjectName).append("\n");

			message.append(NotifMessage.ASSIGNED_USERNAME).append(" ").append(":").append(" ")
					.append(messages.notifiMessageDecoAssignedUserName).append("\n");

		} else if (eventType.equals(NotificationEventType.USER_PROJECT_RELATION_EXPIRY.toString())) {

			message.append(NotifMessage.PROJECT_NAME).append(" ").append(":").append(" ")
					.append(messages.notifiMessageDecoExpireProjectName).append("\n");
			message.append(NotifMessage.PROJECT_EXPIRY_DAYS).append(" ").append(":").append(" ")
					.append(messages.notifiMessageDecoProjectExpiryDays).append("\n");
			message.append(NotifMessage.GRACE_PERIOD).append(" ").append(":").append(" ")
					.append(messages.notifiMessageDecoProjectGraceDays).append("\n");

		} else if (eventType.equals(NotificationEventType.PROJECT_ADMIN_AREA_RELATION_ASSIGN.toString())
				|| eventType.equals(NotificationEventType.PROJECT_ADMIN_AREA_RELATION_REMOVE.toString())) {

			message.append(NotifMessage.PROJECT_NAME).append(" ").append(":").append(" ")
					.append(messages.notifiMessageDecoAssignProjectName).append("\n");

			message.append(NotifMessage.ADMIN_AREA_NAME).append(" ").append(":").append(" ")
					.append(messages.notifiMessageDecoAdminAreaName).append("\n");

		} else if (eventType.equals(NotificationEventType.PROJECT_CREATE.toString())) {

			message.append(NotifMessage.PROJECT_NAME).append(" ").append(":").append(" ")
					.append(messages.notifiMessageDecoProjectName).append("\n");

			message.append(NotifMessage.PROJECT_CREATE_DATE).append(" ").append(":").append(" ")
					.append(messages.notifiMessageDecoProjectCreateDate).append("\n");
		} else if (eventType.equals(NotificationEventType.USER_PROJECT_RELATION_GRACE_EXPIRY.toString())) {

			message.append(NotifMessage.PROJECT_NAME).append(" ").append(":").append(" ")
					.append(messages.notifiMessageDecoExpireProjectName).append("\n");
		} else {
			if (!(eventType.equals(NotificationEventType.EMAIL_NOTIFY_TO_AA_PROJECT_USERS.toString())
					|| eventType.equals(NotificationEventType.EMAIL_NOTIFY_TO_PROJECT_USERS.toString()))) {

				message.append(NotifMessage.PROJECT_NAME).append(" ").append(":").append(" ")
						.append(messages.notifiMessageDecoProjectName).append("\n");
			}

		}

		messageInfoDecroator.setDescriptionText(message.toString());
	*/}

	/**
	 * Initlize filter combos.
	 */
	private void initlizeFilterCombos() {
		initlizeSiteCombo();
		initlizeAdminAreaCombo();
		initlizeProjectCombo();
		initlizeUsersCombo();
		initlizeUserGroupCombo();
	}

	/**
	 * Initlize user group combo.
	 */
	@SuppressWarnings("unchecked")
	private void initlizeUserGroupCombo() {
		UserGroupsModel userGroupsModel = (UserGroupsModel) AdminTreeFactory.getInstance().getGroups()
				.getGroupsChildren().get(UserGroupsModel.class.getSimpleName());
		Collection<IAdminTreeChild> userGroupsChild = userGroupsModel.getUserGroupsChildren().values();
		userGroupModelList = new ArrayList<>();
		userGroupModelList.addAll((Collection<? extends UserGroupModel>) userGroupsChild);
		setUserGroupToCombo(userGroupModelList);
	}

	/**
	 * Initlize site combo.
	 */
	@SuppressWarnings("unchecked")
	private void initlizeSiteCombo() {
		Sites sites = AdminTreeFactory.getInstance().getSites();
		Collection<IAdminTreeChild> values = sites.getSitesChildren().values();
		List<Site> siteList = new ArrayList<Site>();
		siteList.addAll((Collection<? extends Site>) values);
		setSitesToCombo(siteList);
	}

	/**
	 * Initlize admin area combo.
	 */
	@SuppressWarnings("unchecked")
	private void initlizeAdminAreaCombo() {
		final AdministrationAreas administrationAreas = AdminTreeFactory.getInstance().getAdministrationAreas();
		Collection<IAdminTreeChild> values = administrationAreas.getAdminstrationAreasChildren().values();
		List<AdministrationArea> adminAreaList = new ArrayList<AdministrationArea>();
		adminAreaList.addAll((Collection<? extends AdministrationArea>) values);
		setAdminAreaToCombo(adminAreaList);
	}

	/**
	 * Initlize project combo.
	 */
	@SuppressWarnings("unchecked")
	private void initlizeProjectCombo() {
		Projects projects = AdminTreeFactory.getInstance().getProjects();
		Collection<IAdminTreeChild> projectsChild = projects.getProjectsCollection();
		List<Project> projectList = new ArrayList<Project>();
		projectList.addAll((Collection<? extends Project>) projectsChild);
		setProjectToCombo(projectList);
	}

	/**
	 * Initlize users combo.
	 */
	private void initlizeUsersCombo() {
		Users users = AdminTreeFactory.getInstance().getUsers();
		Map<String, IAdminTreeChild> usersChildren = users.getUsersChildren();
		List<User> userList = new ArrayList<>();
		for (Entry<String, IAdminTreeChild> entry : usersChildren.entrySet()) {
			IAdminTreeChild iAdminTreeChild = entry.getValue();
			if (iAdminTreeChild instanceof UsersNameAlphabet) {
				UsersNameAlphabet usersNameAlphabet = (UsersNameAlphabet) iAdminTreeChild;
				Map<String, IAdminTreeChild> userNameAlphaChild = usersNameAlphabet.getUsersChildren();
				for (Entry<String, IAdminTreeChild> alphaUserEntry : userNameAlphaChild.entrySet()) {
					IAdminTreeChild alphaUser = alphaUserEntry.getValue();
					if (alphaUser instanceof User) {
						userList.add((User) alphaUser);
					}
				}
			}
		}
		setUsersToCombo(userList);
	}

	/**
	 * Sets the users to combo.
	 *
	 * @param userList
	 *            the new users to combo
	 */
	private void setUsersToCombo(final List<User> userList) {
		userComboItems = new ArrayList<Map<String, Object>>();
		for (User user : userList) {
			final HashMap<String, Object> item = new HashMap<String, Object>();
			item.put("itemText", user.getName());
			item.put("itemImage", getImage(user));
			item.put("object", user);
			userComboItems.add(item);
		}
		setItemsWithImages(userComboItems, this.filterPanel.getUserFilterCombo());
	}

	/**
	 * Sets the project to combo.
	 *
	 * @param projectList
	 *            the new project to combo
	 */
	private void setProjectToCombo(List<Project> projectList) {
		projectComboItems = new ArrayList<Map<String, Object>>();

		for (Project project : projectList) {
			final HashMap<String, Object> item = new HashMap<String, Object>();
			item.put("itemText", project.getName());
			item.put("itemImage", getImage(project));
			item.put("object", project);
			projectComboItems.add(item);
		}
		setItemsWithImages(projectComboItems, this.filterPanel.getProjectFilterCombo());
	}

	/**
	 * Sets the admin area to combo.
	 *
	 * @param adminAreaList
	 *            the new admin area to combo
	 */
	private void setAdminAreaToCombo(List<AdministrationArea> adminAreaList) {
		adminAreaComboItems = new ArrayList<Map<String, Object>>();
		for (AdministrationArea adminArea : adminAreaList) {
			final HashMap<String, Object> item = new HashMap<String, Object>();
			item.put("itemText", adminArea.getName());
			item.put("itemImage", getImage(adminArea));
			item.put("object", adminArea);
			adminAreaComboItems.add(item);
		}
		setItemsWithImages(adminAreaComboItems, this.filterPanel.getAdminAreaFilterCombo());
	}

	/**
	 * Sets the sites to combo.
	 *
	 * @param siteList
	 *            the new sites to combo
	 */
	private void setSitesToCombo(final List<Site> siteList) {
		siteComboItems = new ArrayList<Map<String, Object>>();
		for (Site site : siteList) {
			final HashMap<String, Object> item = new HashMap<String, Object>();
			item.put("itemText", site.getName());
			item.put("itemImage", getImage(site));
			item.put("object", site);
			siteComboItems.add(item);
		}
		setItemsWithImages(siteComboItems, this.filterPanel.getSiteFilterCombo());
	}

	/**
	 * Sets the user group to combo.
	 *
	 * @param userGroupModelList
	 *            the new user group to combo
	 */
	private void setUserGroupToCombo(List<UserGroupModel> userGroupModelList) {
		userGroupComboItems = new ArrayList<Map<String, Object>>();
		for (UserGroupModel userGroupModel : userGroupModelList) {
			final HashMap<String, Object> item = new HashMap<String, Object>();
			item.put("itemText", userGroupModel.getName());
			item.put("itemImage", getImage(userGroupModel));
			item.put("object", userGroupModel);
			userGroupComboItems.add(item);
		}
		setItemsWithImages(userGroupComboItems, this.userGroupFilterPanel.getUserGroupFilterCombo());
	}

	/**
	 * Sets the items with images.
	 *
	 * @param items
	 *            the items
	 * @param combo
	 *            the combo
	 */
	public void setItemsWithImages(final List<Map<String, Object>> items, MagnaCustomCombo combo) {

		try {
			String itemText = null;
			Object itemImage = null;
			final Table table = combo.getTable();
			table.removeAll();
			if (table.getColumnCount() <= 0) {
				new TableColumn(table, SWT.NONE);
			}
			if (items != null) {
				for (int i = 0; i < items.size(); i++) {
					itemText = (String) items.get(i).get("itemText");
					itemImage = items.get(i).get("itemImage");
					final TableItem item = new TableItem(table, SWT.NONE);
					if (itemText != null) {
						item.setText(itemText);
						if (itemImage instanceof Image) {
							item.setImage((Image) itemImage);
						}
					}
					item.setData(items.get(i).get("object"));
				}
			}
		} catch (Exception e) {
			LOGGER.error("Unable to create table combo widget", e); //$NON-NLS-1$
		}
	}

	/**
	 * Gets the image.
	 *
	 * @param object
	 *            the object
	 * @return the image
	 */
	private Image getImage(final Object object) {
		Image image = null;
		if (object != null) {
			if (object instanceof Site) {
				final Site site = (Site) object;
				image = XMSystemUtil.getInstance().getImage(this.getClass(), site.getIcon().getIconPath(), site.isActive(), true);
			} else if (object instanceof User) {
				final User user = (User) object;
				image = XMSystemUtil.getInstance().getImage(this.getClass(), user.getIcon().getIconPath(), user.isActive(), true);
			} else if (object instanceof Project) {
				final Project project = (Project) object;
				image = XMSystemUtil.getInstance().getImage(this.getClass(), project.getIcon().getIconPath(), project.isActive(), true);
			} else if (object instanceof AdministrationArea) {
				final AdministrationArea adminsArea = (AdministrationArea) object;
				image = XMSystemUtil.getInstance().getImage(this.getClass(), adminsArea.getIcon().getIconPath(), adminsArea.isActive(), true);
			} else if (object instanceof UserGroupModel) {
				final UserGroupModel userGroupModel = (UserGroupModel) object;
				image = XMSystemUtil.getInstance().getImage(this.getClass(), userGroupModel.getIcon().getIconPath(), true, true);
			}
		}
		return image;
	}

	/**
	 * Filter combo.
	 *
	 * @param filterText
	 *            the filter text
	 * @param items
	 *            the items
	 * @param FilterCombo
	 *            the filter combo
	 */
	private void filterCombo(final String filterText, final List<Map<String, Object>> items,
			final MagnaCustomCombo FilterCombo) {
		try {
			if (filterText != null && filterText.length() >= 0 && items != null && !items.isEmpty()) {
				final List<Map<String, Object>> tempItems = new ArrayList<Map<String, Object>>();
				for (final Map<String, Object> item : items) {
					final Object itemText = item.get("itemText");
					if (itemText != null && itemText instanceof String
							&& Pattern.compile(Pattern.quote(filterText), Pattern.CASE_INSENSITIVE)
									.matcher((String) itemText).find()) {
						tempItems.add(item);
					}
				}

				setItemsWithImages(tempItems, FilterCombo);
			}
		} catch (Exception e) {
			LOGGER.error("Unable to filter the Combo", e); //$NON-NLS-1$
		}
	}

	/**
	 * Initlize event type.
	 */
	private void initlizeEventType() {
		final String criteria[] = NotificationEventType.getNotificationCriteria();
		this.cmbEventType.setItems(criteria);
		// updateCheckboxVisibilty();
	}

	/**
	 * Gets the updated widget text.
	 *
	 * @param message
	 *            the message
	 * @param control
	 *            the control
	 * @return the updated widget text
	 */
	private String getUpdatedWidgetText(final String message, final Control control) {
		control.requestLayout();
		control.getParent().redraw();
		control.getParent().getParent().update();
		control.getParent().getParent().getParent().update();
		return message;
	}

	/**
	 * Inits the listeners.
	 */
	private void initListeners() {

		MagnaCustomCombo userGroupFilterCombo = userGroupFilterPanel.getUserGroupFilterCombo();
		Button userGroupFilterButton = this.userGroupFilterPanel.getUserGroupFilterButton();
		Text userGroupFilterText = this.userGroupFilterPanel.getUserGroupFilterText();

		MagnaCustomCombo userFilterCombo = this.filterPanel.getUserFilterCombo();
		Button userFilterButton = this.filterPanel.getUserFilterButton();
		Text userFilterText = this.filterPanel.getUserFilterText();

		MagnaCustomCombo projectFilterCombo = this.filterPanel.getProjectFilterCombo();
		Button projectFilterButton = this.filterPanel.getProjectFilterButton();
		Text projectFilterText = this.filterPanel.getProjectFilterText();

		MagnaCustomCombo adminAreaFilterCombo = this.filterPanel.getAdminAreaFilterCombo();
		Button adminAreaFilterButton = this.filterPanel.getAdminAreaFilterButton();
		Text adminAreaFilterText = this.filterPanel.getAdminAreaFilterText();

		MagnaCustomCombo siteFilterCombo = this.filterPanel.getSiteFilterCombo();
		Button siteFilterButton = this.filterPanel.getSiteFilterButton();
		Text siteFilterText = this.filterPanel.getSiteFilterText();

		if (this.radioBtnUserGroup != null && !this.radioBtnUserGroup.isDisposed()) {
			this.radioBtnUserGroup.addSelectionListener(new SelectionAdapter() {
				@Override
				public void widgetSelected(SelectionEvent event) {
					if (radioBtnUserGroup.getSelection()) {
						filterContainerLayout.topControl = userGroupFilterPanel;
						filterContainer.layout();
					} else {
						filterContainerLayout.topControl = filterPanel;
						filterContainer.layout();
					}
				}
			});
		}
		if (this.activeBtn != null && !this.activeBtn.isDisposed()) {
			this.activeBtn.addSelectionListener(new SelectionListener() {

				@Override
				public void widgetSelected(SelectionEvent arg0) {
					String eventType = cmbEventType.getText();
					if (eventType.equals(NotificationEventType.EMAIL_NOTIFY_TO_PROJECT_USERS.toString())
							|| eventType.equals(NotificationEventType.EMAIL_NOTIFY_TO_AA_PROJECT_USERS.toString())) {
						if (!activeBtn.getSelection()) {
							saveBtn.setEnabled(false);
						} else {
							saveBtn.setEnabled(true);
						}
					}
				}

				@Override
				public void widgetDefaultSelected(SelectionEvent arg0) {
					// TODO Auto-generated method stub

				}
			});
		}
		if (this.txtToUsers != null) {
			this.txtToUsers.addModifyListener(new ModifyListener() {

				@Override
				public void modifyText(ModifyEvent paramModifyEvent) {

					List<String> list = new ArrayList<String>();
					final String strValue = txtToUsers.getText();
					list = Arrays.asList(strValue.split(";"));

					final List<StyleRange> styleRangeList = new ArrayList<>();
					final List<Integer> rangeList = new ArrayList<>();
					int fromIndex = 0;
					for (String link : list) {
						final int indexOf = strValue.indexOf(link, fromIndex);
						rangeList.add(indexOf);
						final int length = indexOf + link.length();
						fromIndex = length;
						rangeList.add(length);

						final StyleRange style = new StyleRange();
						style.underline = true;
						style.underlineStyle = SWT.UNDERLINE_LINK;
						style.start = indexOf;
						style.length = link.length();
						styleRangeList.add(style);
					}
					txtToUsers.setStyleRanges(styleRangeList.toArray(new StyleRange[styleRangeList.size()]));
				}
			});
		}
		if (this.saveBtn != null) {
			this.saveBtn.addSelectionListener(new SelectionAdapter() {

				/**
				 * Save button handler
				 */
				@Override
				public void widgetSelected(final SelectionEvent event) {
					saveNotificationHandler();
				}
			});
		}

		if (this.cancelBtn != null) {
			this.cancelBtn.addSelectionListener(new SelectionAdapter() {

				/**
				 * Cancel button handler
				 */
				@Override
				public void widgetSelected(final SelectionEvent event) {
					cancelNotificationHandler();
				}
			});
		}
		if (this.editButton != null) {
			this.editButton.addSelectionListener(new SelectionAdapter() {

				@Override
				public void widgetSelected(SelectionEvent paramSelectionEvent) {
					boolean returnVal = true;
					try {
						ValidationController controller = new ValidationController();
						ValidationRequest request = new ValidationRequest();
						request.setUserName(RestClientUtil.getInstance().getUserName());
						request.setPermissionType(PermissionType.OBJECT_PERMISSION.name());
						request.setPermissionName("NOTIFICATION-CHANGE"); //$NON-NLS-1$
						returnVal = controller.getAccessAllowed(request);
					} catch (UnauthorizedAccessException e) {
						LOGGER.error("Current user is Unauthorized " + e);
						returnVal = false;
					} catch (Exception e) {
						LOGGER.error("Exception occured in calling access allowed API " + e);
						returnVal = false;
					}
					if (returnVal) {
						updateOperationMode(true);
						dirty.setDirty(true);
						updateCheckboxVisibilty(true);
						setOldModel(getNotificationModel().deepCopyNotification(false, null));
					} else {
						CustomMessageDialog.openError(Display.getCurrent().getActiveShell(),
								messages.objectPermissionDialogTitle, messages.objectPermissionDialogMsg);
					}
				}
			});
		}
		if (this.cmbEventType != null) {
			this.cmbEventType.addSelectionListener(new SelectionAdapter() {

				@Override
				public void widgetSelected(final SelectionEvent event) {

					if (dirty.isDirty()) {
						boolean response = CustomMessageDialog.openSaveDiscardDialog(
								Display.getDefault().getActiveShell(), messages.dirtyDialogTitle,
								messages.dirtyDialogMessage);
						if (response) {
							saveNotificationHandler();
						} else {
							cancelNotificationHandler();
						}
					}
					if (cmbEventType.getSelectionIndex() >= 0) {
						editButton.setEnabled(true);
						updateCheckboxVisibilty(false);
						loadNotification();
						String eventType = cmbEventType.getText();
						if (eventType.equals(NotificationEventType.EMAIL_NOTIFY_TO_PROJECT_USERS.toString())
								|| eventType
										.equals(NotificationEventType.EMAIL_NOTIFY_TO_AA_PROJECT_USERS.toString())) {
							saveBtn.setText(messages.notificationSendBtn);
							if (!activeBtn.getSelection()) {
								saveBtn.setEnabled(false);
							} else {
								saveBtn.setEnabled(true);
							}
						} else {
							saveBtn.setText(messages.saveButtonText);
							saveBtn.setEnabled(true);
						}
					}
					removeAllComboSelection();
					removeAllFilterText();
					updateMessageDecoInfo();
				}
			});
		}

		if (this.addToolItem != null) {
			this.addToolItem.addSelectionListener(new SelectionAdapter() {

				@Override
				public void widgetSelected(final SelectionEvent paramSelectionEvent) {

					users = new StringBuilder();
					users.append(txtToUsers.getText());
					Set<String> userNameList = new HashSet<>(Arrays.asList(users.toString().split(";")));
					if (userFilterCombo.getSelectionIndex() >= 0) {
						String userName = userFilterCombo.getItem(userFilterCombo.getSelectionIndex());
						if (!userNameList.contains(userName)) {
							userNameList.add(userName);
						} else {
							CustomMessageDialog.openError(getShell(), messages.notificationUserErrorTitle,
									messages.notificationUserErrorMsg);
						}
						userFilterCombo.select(-1);
					} else if (projectFilterCombo.getSelectionIndex() >= 0) {
						userNameList.addAll(Arrays.asList(userFilterCombo.getItems()));
						projectFilterCombo.select(-1);
						initlizeUsersCombo();
					} else if (adminAreaFilterCombo.getSelectionIndex() >= 0) {
						for (int i = 0; i < projectFilterCombo.getItemCount(); i++) {
							Object object = projectFilterCombo.getTable().getItem(i).getData();
							List<UsersTbl> userResponse;
							Project project = (Project) object;
							String projectId = project.getProjectId();
							if (projectId != null
									&& (userResponse = userController.getAllUsersByProjectId(projectId)) != null) {
								for (UsersTbl user : userResponse) {
									userNameList.add(user.getUsername());
								}
							}
						}
						adminAreaFilterCombo.select(-1);
						initlizeProjectCombo();
					} else if (userGroupFilterCombo.getSelectionIndex() >= 0) {
						Object data = userGroupFilterCombo.getTable().getItem(userGroupFilterCombo.getSelectionIndex())
								.getData();
						if (data instanceof UserGroupModel) {
							UserGroupModel userGroupModel = (UserGroupModel) data;
							UserGroupUsers userGroupUsers = (UserGroupUsers) userGroupModel.getUserGroupChildren()
									.get(UserGroupUsers.class.getSimpleName());
							List<IAdminTreeChild> values = AdminTreeDataLoad.getInstance()
									.loadUserGroupUsersFromService(userGroupUsers);
							for (IAdminTreeChild iAdminTreeChild : values) {
								if (iAdminTreeChild instanceof RelationObj) {
									RelationObj relObj = (RelationObj) iAdminTreeChild;
									IAdminTreeChild refObject = relObj.getRefObject();
									if (refObject instanceof User) {
										User user = (User) refObject;
										userNameList.add(user.getName());
									}
								}
							}
						}
						userGroupFilterCombo.select(-1);
					}
					updateToUsersWidget(new TreeSet<>(userNameList));
				}
			});
		}
		siteFilterText.addListener(SWT.FocusOut, new Listener() {

			@Override
			public void handleEvent(final Event event) {
				final String filterText = ((Text) event.widget).getText();
				siteFilterCombo.select(-1);
				filterCombo(filterText, siteComboItems, siteFilterCombo);
			}
		});
		siteFilterCombo.addSelectionListener(new SelectionAdapter() {

			@Override
			public void widgetSelected(final SelectionEvent paramSelectionEvent) {
				Object object = siteFilterCombo.getTable().getItem(siteFilterCombo.getSelectionIndex()).getData();
				String siteId = null;
				if (object instanceof Site) {
					Site site = (Site) object;
					siteId = site.getSiteId();
				}
				List<SiteAdminAreaRelIdWithAdminArea> allAdminAreasBySiteId;
				final List<AdminAreasTbl> adminAreasTbl = new ArrayList<>();
				if (siteId != null
						&& (allAdminAreasBySiteId = siteController.getAllAdminAreasBySiteId(siteId)) != null) {
					for (SiteAdminAreaRelIdWithAdminArea siteAdminAreaRelIdWithAdminAreaVo : allAdminAreasBySiteId) {
						SiteAdminAreaRelIdWithAdminArea siteAdminAreaRelIdWithAdminArea = siteAdminAreaRelIdWithAdminAreaVo;
						adminAreasTbl.add(siteAdminAreaRelIdWithAdminArea.getAdminAreasTbl());
					}
					setAdminAreaToCombo(getAdminArea(adminAreasTbl));
				} else {

					adminAreaFilterCombo.getTable().removeAll();
				}
				initlizeProjectCombo();
				initlizeUsersCombo();
				adminAreaFilterCombo.select(-1);
				projectFilterCombo.select(-1);
				userFilterCombo.select(-1);
			}
		});
		adminAreaFilterText.addListener(SWT.FocusOut, new Listener() {

			@Override
			public void handleEvent(final Event event) {
				final String filterText = ((Text) event.widget).getText();
				adminAreaFilterCombo.select(-1);
				filterCombo(filterText, adminAreaComboItems, adminAreaFilterCombo);
			}
		});
		adminAreaFilterCombo.addSelectionListener(new SelectionAdapter() {

			@Override
			public void widgetSelected(final SelectionEvent paramSelectionEvent) {

				Object object = adminAreaFilterCombo.getTable().getItem(adminAreaFilterCombo.getSelectionIndex())
						.getData();
				String adminAreasId = null;
				if (object instanceof AdministrationArea) {
					AdministrationArea adminArea = (AdministrationArea) object;
					adminAreasId = adminArea.getAdministrationAreaId();
				}
				List<ProjectsTbl> projectResponse;
				if (adminAreasId != null) {
					projectResponse = projectController.getProjectsByAdminAreaId(adminAreasId);
					setProjectToCombo(getProjects(projectResponse));
				} else {

					projectFilterCombo.getTable().removeAll();
				}
				initlizeUsersCombo();
				projectFilterCombo.select(-1);
				userFilterCombo.select(-1);
			}

		});
		userFilterText.addListener(SWT.FocusOut, new Listener() {

			@Override
			public void handleEvent(final Event event) {
				final String filterText = ((Text) event.widget).getText();
				userFilterCombo.select(-1);
				filterCombo(filterText, userComboItems, userFilterCombo);
			}
		});
		projectFilterText.addListener(SWT.FocusOut, new Listener() {

			@Override
			public void handleEvent(final Event event) {
				final String filterText = ((Text) event.widget).getText();
				projectFilterCombo.select(-1);
				filterCombo(filterText, projectComboItems, projectFilterCombo);
			}
		});

		projectFilterCombo.addSelectionListener(new SelectionAdapter() {

			@Override
			public void widgetSelected(SelectionEvent paramSelectionEvent) {
				Object object = projectFilterCombo.getTable().getItem(projectFilterCombo.getSelectionIndex()).getData();
				String projectsId = null;
				if (object instanceof Project) {
					Project project = (Project) object;
					projectsId = project.getProjectId();
				}
				List<UsersTbl> userResponse;
				if (projectsId != null) {
					userResponse = userController.getAllUsersByProjectId(projectsId);
					setUsersToCombo(getUsers(userResponse));
				} else {
					userFilterCombo.getTable().removeAll();
				}
				userFilterCombo.select(-1);
			}
		});

		userGroupFilterText.addListener(SWT.FocusOut, new Listener() {

			@Override
			public void handleEvent(final Event event) {
				final String filterText = ((Text) event.widget).getText();
				userGroupFilterCombo.select(-1);
				filterCombo(filterText, userGroupComboItems, userGroupFilterCombo);
			}
		});

		siteFilterButton.addListener(SWT.Selection, new Listener() {
			/**
			 * Overrides handleEvent method for show All sites button selection
			 */
			@Override
			public void handleEvent(final Event event) {
				siteFilterText.setText(CommonConstants.EMPTY_STR);
				setItemsWithImages(siteComboItems, siteFilterCombo);
				siteFilterCombo.select(-1);
			}
		});

		adminAreaFilterButton.addListener(SWT.Selection, new Listener() {
			/**
			 * Overrides handleEvent method for show All admin areas button
			 * selection
			 */
			@Override
			public void handleEvent(final Event event) {
				adminAreaFilterText.setText(CommonConstants.EMPTY_STR);
				setItemsWithImages(adminAreaComboItems, adminAreaFilterCombo);
				adminAreaFilterCombo.select(-1);
			}
		});

		userFilterButton.addListener(SWT.Selection, new Listener() {
			/**
			 * Overrides handleEvent method for show All users button selection
			 */
			@Override
			public void handleEvent(final Event event) {
				userFilterText.setText(CommonConstants.EMPTY_STR);
				setItemsWithImages(userComboItems, userFilterCombo);
				userFilterCombo.select(-1);
			}
		});

		projectFilterButton.addListener(SWT.Selection, new Listener() {
			/**
			 * Overrides handleEvent method for show All projects button
			 * selection
			 */
			@Override
			public void handleEvent(final Event event) {
				projectFilterText.setText(CommonConstants.EMPTY_STR);
				setItemsWithImages(projectComboItems, projectFilterCombo);
				projectFilterCombo.select(-1);
			}
		});

		userGroupFilterButton.addListener(SWT.Selection, new Listener() {
			/**
			 * Overrides handleEvent method for show All users button selection
			 */
			@Override
			public void handleEvent(final Event event) {
				userGroupFilterText.setText(CommonConstants.EMPTY_STR);
				setItemsWithImages(userGroupComboItems, userGroupFilterCombo);
				userGroupFilterCombo.select(-1);
			}
		});

		if (this.txtMessage != null) {
			this.txtMessage.addVerifyListener(new VerifyListener() {
				ControlDecoration txtDecoratorDecroator = new ControlDecoration(txtMessage, SWT.TOP);

				@Override
				public void verifyText(VerifyEvent event) {

					if (cmbEventType.getSelectionIndex() >= 0) {
						event.doit = false;
						Text source = (Text) event.getSource();
						final String existingText = source.getText();
						final String updatedText = existingText.substring(0, event.start) + event.text
								+ existingText.substring(event.end);
						final Image nameDecoratorImage = FieldDecorationRegistry.getDefault()
								.getFieldDecoration(FieldDecorationRegistry.DEC_ERROR).getImage();
						txtDecoratorDecroator.setImage(nameDecoratorImage);
						if (updatedText.trim().length() <= 0) {
							txtDecoratorDecroator.setDescriptionText(messages.emptyNotificationMessageErr);
							event.doit = true;
							txtDecoratorDecroator.show();
						} else {
							event.doit = true;
							txtDecoratorDecroator.hide();
						}
					}
				}
			});
		}

		if (this.txtSubject != null) {
			this.txtSubject.addVerifyListener(new VerifyListener() {
				ControlDecoration txtDecoratorDecroator = new ControlDecoration(txtSubject, SWT.TOP);

				@Override
				public void verifyText(VerifyEvent event) {

					if (cmbEventType.getSelectionIndex() >= 0) {
						event.doit = false;
						Text source = (Text) event.getSource();
						final String existingText = source.getText();
						final String updatedText = existingText.substring(0, event.start) + event.text
								+ existingText.substring(event.end);
						final Image nameDecoratorImage = FieldDecorationRegistry.getDefault()
								.getFieldDecoration(FieldDecorationRegistry.DEC_ERROR).getImage();
						txtDecoratorDecroator.setImage(nameDecoratorImage);
						if (updatedText.trim().length() <= 0) {
							txtDecoratorDecroator.setDescriptionText(messages.emptyNotificationSubjectErr);
							event.doit = true;
							txtDecoratorDecroator.show();
						} else {
							event.doit = true;
							txtDecoratorDecroator.hide();
						}
					}
				}
			});
		}

	}

	/**
	 * Load notification.
	 */
	protected void loadNotification() {/*
		try {
			NotificationController notificationController = new NotificationController();
			String event = this.cmbEventType.getItem(this.cmbEventType.getSelectionIndex());
			// FIXME
			NotificationResponse notificationResponse = notificationController.findNotificationByEvent(event);
			if (notificationResponse != null) {
				
				 * EmailNotificationConfigTbl emailNotificationConfigTbl =
				 * notificationResponse.getEmailNotificationConfigTbl(); boolean
				 * isActive =
				 * com.magna.xmbackend.vo.enums.Status.ACTIVE.name().equals(
				 * emailNotificationConfigTbl.getStatus()) ? true : false;
				 * this.notificationModel.setActive(isActive); // FIXME temp fix
				 * to resolve jenkins fail issue
				 * //this.notificationModel.setDescription(
				 * emailNotificationConfigTbl.getDescription());
				 * this.notificationModel.setMessage(emailNotificationConfigTbl.
				 * getMessage());
				 * this.notificationModel.setSubject(emailNotificationConfigTbl.
				 * getSubject()); if
				 * (emailNotificationConfigTbl.getIncludeRemarks() != null) {
				 * this.notificationModel.setIncludeRemarks(
				 * emailNotificationConfigTbl.getIncludeRemarks().equals(
				 * CommonConstants.Notification.TRUE) ? true : false);
				 * 
				 * } if (emailNotificationConfigTbl.getSendToAssignedUser() !=
				 * null) { this.notificationModel.setSendToAssignUser(
				 * emailNotificationConfigTbl.getSendToAssignedUser().equals(
				 * CommonConstants.Notification.TRUE) ? true : false);
				 * 
				 * }
				 
				
				 * if (emailNotificationConfigTbl.getProjectExpiryNoticePeriod()
				 * != null) {
				 * this.notificationModel.setProjectExpiryNoticePeriod(
				 * emailNotificationConfigTbl
				 * .getProjectExpiryNoticePeriod().equals(CommonConstants.
				 * Notification.TRUE) ? true : false); }
				 
				
				 * Iterable<EmailNotifyToUserRelTbl> emailNotifyToUserRelTbls =
				 * notificationResponse .getEmailNotifyToUserRelTbls(); if
				 * (emailNotifyToUserRelTbls != null) { Set<String> userNameList
				 * = new HashSet<>(); for (EmailNotifyToUserRelTbl
				 * emailNotifyToUserRelTbl : emailNotifyToUserRelTbls) {
				 * UsersTbl userTbl = emailNotifyToUserRelTbl.getUserId();
				 * userNameList.add(userTbl.getUsername()); }
				 * this.notificationModel.setUsers(userNameList);
				 * updateToUsersWidget(this.notificationModel.getUsers()); }
				 
			}
			// saveUsers();

		} catch (UnauthorizedAccessException e) {
			CustomMessageDialog.openError(this.getShell(), messages.objectPermissionDialogTitle,
					messages.objectPermissionDialogMsg);
		} catch (Exception e) {
			LOGGER.error("Exeception while getting  loading Notification! " + e);
		}
		StyleRange style1 = new StyleRange();
		style1.start = 0;
		style1.length = txtDescription.getText().length();
		style1.fontStyle = SWT.ITALIC;
		txtDescription.setStyleRange(style1);
	*/}

	/**
	 * Update field visibility.
	 *
	 * @param flag
	 *            the flag
	 */
	protected void updateOperationMode(final boolean flag) {
		this.activeBtn.setEnabled(flag);
		this.addToolItem.setEnabled(flag);
		this.txtToUsers.setEditable(flag);
		this.txtSubject.setEditable(flag);
		this.txtMessage.setEditable(flag);
		this.includeRemarksBtn.setEnabled(flag);
		this.sendToAssignUserBtn.setEnabled(flag);
		// this.projectExpiryPeriodBtn.setEnabled(flag);
		this.filterPanel.enableAdminAreaFilter(flag);
		this.filterPanel.enableProjectFilter(flag);
		this.filterPanel.enableSiteFilter(flag);
		this.filterPanel.enableUserFilter(flag);
		this.userGroupFilterPanel.enableUserGroupFilter(flag);
		this.radioBtnUser.setEnabled(flag);
		this.radioBtnUserGroup.setEnabled(flag);
		setShowButtonBar(flag);
	}

	/**
	 * Cancel notification handler.
	 */
	public void cancelNotificationHandler() {
		dirty.setDirty(false);
		this.setNotificationModel(this.getOldModel());
		updateOperationMode(false);
		updateToUsersWidget(this.notificationModel.getUsers());
		bindValues();
		removeAllComboSelection();
		removeAllFilterText();
	}

	/**
	 * Save notification handler.
	 */
	public void saveNotificationHandler() {/*
		saveUsers();
		this.notificationModel.setEventType(this.cmbEventType.getItem(this.cmbEventType.getSelectionIndex()));
		if (validate()) {
			try {
				NotificationController notificationController = new NotificationController();
				List<Map<String, String>> statusMaps;
				if (messages.notificationSendBtn.equals(saveBtn.getText())) {

					if (!txtToUsers.getText().trim().toString().isEmpty()) {
						SendMailResponse sendDirectMailResponse = notificationController
								.sendDirectMail(mapSendMailRequestVoObjectWithModel());
						statusMaps = sendDirectMailResponse.getStatusMaps();
					} else {
						CustomMessageDialog.openError(this.getShell(), messages.notificationSendErrTiltle,
								messages.notificationSendErrMsg);
						return;
					}

				} else {
					NotificationResponse notificationResponse = notificationController
							.updateNotification(mapVOObjectWithModel());
					statusMaps = notificationResponse.getStatusMaps();
				}
				if (statusMaps != null && !statusMaps.isEmpty()) {
					String errorMessage = "";
					for (Map<String, String> statusMap : statusMaps) {
						errorMessage += statusMap.get(XMAdminUtil.getInstance().getCurrentLocaleEnum().getLangCode());
						errorMessage += "\n";
					}
					if (errorMessage.length() > 0) {
						CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
								messages.notificationMessageErrTitle, errorMessage);
					}
					return;
				}

				updateOperationMode(false);
				dirty.setDirty(false);
				removeAllComboSelection();
				removeAllFilterText();
				filterCombo(CommonConstants.EMPTY_STR, siteComboItems, this.filterPanel.getSiteFilterCombo());
				filterCombo(CommonConstants.EMPTY_STR, adminAreaComboItems, this.filterPanel.getAdminAreaFilterCombo());
				filterCombo(CommonConstants.EMPTY_STR, projectComboItems, this.filterPanel.getProjectFilterCombo());
				filterCombo(CommonConstants.EMPTY_STR, userComboItems, this.filterPanel.getUserFilterCombo());
				filterCombo(CommonConstants.EMPTY_STR, userGroupComboItems,
						this.userGroupFilterPanel.getUserGroupFilterCombo());
				// XMAdminUtil.getInstance().updateLogFile("Notification
				// EventType:"+ " "+ this.notificationModel.getEventType()+"
				// "+"updated successfully!");
			} catch (Exception e) {
				if (e instanceof ResourceAccessException) {
					CustomMessageDialog.openError(Display.getCurrent().getActiveShell(), messages.errorDialogTitile,
							messages.serverNotReachable);
				}
				LOGGER.warn("Unable to Save data ! " + e);
			}
		}
	*/}

	/**
	 * Removes the all combo selection.
	 */
	private void removeAllComboSelection() {
		this.filterPanel.getSiteFilterCombo().select(-1);
		this.filterPanel.getAdminAreaFilterCombo().select(-1);
		this.filterPanel.getProjectFilterCombo().select(-1);
		this.filterPanel.getUserFilterCombo().select(-1);
		this.userGroupFilterPanel.getUserGroupFilterCombo().select(-1);
	}

	/**
	 * Removes the all filter text.
	 */
	private void removeAllFilterText() {
		this.filterPanel.getSiteFilterText().setText(CommonConstants.EMPTY_STR);
		this.filterPanel.getAdminAreaFilterText().setText(CommonConstants.EMPTY_STR);
		this.filterPanel.getProjectFilterText().setText(CommonConstants.EMPTY_STR);
		this.filterPanel.getUserFilterText().setText(CommonConstants.EMPTY_STR);
		this.userGroupFilterPanel.getUserGroupFilterText().setText(CommonConstants.EMPTY_STR);
	}

	/**
	 * Validate.
	 *
	 * @return true, if successful
	 */
	private boolean validate() {
		if (this.txtSubject.getText().trim().isEmpty()) {
			CustomMessageDialog.openError(this.getShell(), messages.notificationSubjectErrTitle,
					messages.notificationSubjectErrMsg);
			return false;
		}
		if (this.txtMessage.getText().trim().isEmpty()) {
			CustomMessageDialog.openError(this.getShell(), messages.notificationMessageErrTitle,
					messages.notificationMessageErrMsg);
			return false;
		}

		if (!validateMessageText()) {
			CustomMessageDialog.openError(this.getShell(), messages.notificationMessageErrTitle,
					messages.notificationInvalidMsgDialogMsg);
			return false;
		}
		return true;
	}

	/**
	 * Validate message text.
	 *
	 * @return true, if successful
	 */
	private boolean validateMessageText() {
		String message = this.txtMessage.getText();

		String endParanthPattern = "\\}";
		String startParanthPattern = "\\{";

		Pattern startPattern = Pattern.compile(startParanthPattern);
		Pattern endPattern = Pattern.compile(endParanthPattern);
		Matcher matcher = startPattern.matcher(message);
		int startPatternCount = 0;
		int endPatternCount = 0;
		while (matcher.find()) {
			startPatternCount++;
		}

		matcher = endPattern.matcher(message);
		while (matcher.find()) {
			endPatternCount++;
		}
		if (startPatternCount != endPatternCount) {
			return false;
		}
		List<String> varList = new ArrayList<>();
		if (startPatternCount == endPatternCount) {
			String varPatternStr = "\\$\\{.*?\\}";
			Pattern varPattern = Pattern.compile(varPatternStr);
			matcher = varPattern.matcher(message);
			while (matcher.find()) {
				varList.add(message.substring(matcher.start(), matcher.end()));
			}
			int count = 0;
			for (String var : varList) {
				if (var.matches("\\$\\{userName}") || var.matches("\\$\\{projectName}")
						|| var.matches("\\$\\{assignedUserName}") || var.matches("\\$\\{adminAreaName}")
						|| var.matches("\\$\\{projExpDays}") || var.matches("\\$\\{gracePeriod}")
						|| var.matches("\\$\\{projectCreatedDate}")) {
					count++;
				}
			}
			if (count != varList.size()) {
				return false;
			}
		}
		return true;
	}

	/**
	 * Save users.
	 */
	private void saveUsers() {
		Set<String> usersName = new HashSet<String>();
		if (!this.txtToUsers.getText().trim().isEmpty()) {
			List<String> list = Arrays.asList(txtToUsers.getText().trim().toString().split(";"));
			usersName = new HashSet<String>(list);
		}
		this.notificationModel.setUsers(usersName);
		updateToUsersWidget(usersName);
	}

	/**
	 * Update to users widget.
	 *
	 * @param userNameList
	 *            the user name list
	 */
	private void updateToUsersWidget(final Set<String> userNameList) {
		// List<String> userNameList = this.notificationModel.getUsers();
		this.users = new StringBuilder();
		for (String userName : userNameList) {
			if (this.users.length() > 0) {
				this.users.append(';').append(userName);
			} else {
				this.users.append(userName);
			}
		}
		this.txtToUsers.setText(this.users.toString());
	}

	/**
	 * Map VO object with model.
	 *
	 * @return the com.magna.xmbackend.vo.notification. notification request
	 */
	private com.magna.xmbackend.vo.notification.NotificationRequest mapVOObjectWithModel() {
		com.magna.xmbackend.vo.notification.NotificationRequest notificationRequest = new com.magna.xmbackend.vo.notification.NotificationRequest();

		/*
		 * notificationRequest.setNotificationEvent(this.notificationModel.
		 * getEventType());
		 * notificationRequest.setStatus(this.notificationModel.isActive() ==
		 * true ? com.magna.xmbackend.vo.enums.Status.ACTIVE :
		 * com.magna.xmbackend.vo.enums.Status.INACTIVE);
		 * notificationRequest.setUsersToNotify(new
		 * ArrayList<>(this.notificationModel.getUsers()));
		 * notificationRequest.setMessage(this.notificationModel.getMessage());
		 * notificationRequest.setSubject(this.notificationModel.getSubject());
		 * notificationRequest.setIncludeRemarks(this.notificationModel.
		 * isIncludeRemarks() == true ? CommonConstants.Notification.TRUE :
		 * CommonConstants.Notification.FALSE);
		 * notificationRequest.setSendToAssignedUser(this.notificationModel.
		 * isSendToAssignUser() == true ? CommonConstants.Notification.TRUE :
		 * CommonConstants.Notification.FALSE);
		 */
		/*
		 * notificationRequest.setProjectExpNoticePeriod(this.notificationModel.
		 * isProjectExpiryNoticePeriod() == true ?
		 * CommonConstants.Notification.TRUE :
		 * CommonConstants.Notification.FALSE);
		 */

		return notificationRequest;
	}

	/**
	 * Map send mail request vo object with model.
	 *
	 * @return the com.magna.xmbackend.vo.notification. send mail request
	 */
	private com.magna.xmbackend.vo.notification.SendMailRequest mapSendMailRequestVoObjectWithModel() {
		com.magna.xmbackend.vo.notification.SendMailRequest sendMailRequest = new com.magna.xmbackend.vo.notification.SendMailRequest();
		sendMailRequest.setStatus(this.notificationModel.isActive() == true ? com.magna.xmbackend.vo.enums.Status.ACTIVE
				: com.magna.xmbackend.vo.enums.Status.INACTIVE);
		sendMailRequest.setSubject(this.notificationModel.getSubject());
		sendMailRequest.setMessage(this.notificationModel.getMessage());
		sendMailRequest.setUsersToNotify(new ArrayList<>(this.notificationModel.getUsers()));
		return sendMailRequest;
	}

	/**
	 * Sets the notification.
	 */
	public void setNotification() {
		try {
			final AdminTreeviewer adminTree = XMAdminUtil.getInstance().getAdminTree();
			final Object selectionObj = adminTree.getSelection();
			if (selectionObj instanceof IStructuredSelection) {
				Object firstElement = ((IStructuredSelection) selectionObj).getFirstElement();
				if (firstElement instanceof Notification) {
					setOldModel((Notification) firstElement);
					Notification rightHandObject = this.getOldModel().deepCopyNotification(false, null);
					setNotificationModel(rightHandObject);
					updateOperationMode(false);
					registerMessages(this.registry);
					bindValues();
					updateToUsersWidget(this.notificationModel.getUsers());
					if (this.cmbEventType.getSelectionIndex() < 0) {
						this.editButton.setEnabled(false);
					}
					this.filterPanel.getUserFilterCombo().select(-1);
					this.filterPanel.getProjectFilterCombo().select(-1);
					this.filterPanel.getAdminAreaFilterCombo().select(-1);
					this.filterPanel.getSiteFilterCombo().select(-1);
					this.userGroupFilterPanel.getUserGroupFilterCombo().select(-1);
				}
			}
		} catch (Exception e) {
			LOGGER.warn("Unable to set Notification model selection ! " + e);
		}
	}

	/**
	 * Sets the model.
	 *
	 * @param notificationModel
	 *            the new model
	 */
	public void setModel(final Notification notificationModel) {
		try {
			setOldModel(null);
			setNotificationModel(notificationModel);
			registerMessages(this.registry);
			bindValues();
		} catch (Exception e) {
			LOGGER.warn("Unable to set Notification model ! " + e);
		}
	}

	/**
	 * Gets the notification model.
	 *
	 * @return the notification model
	 */
	public Notification getNotificationModel() {
		return notificationModel;
	}

	/**
	 * Sets the notification model.
	 *
	 * @param notificationModel
	 *            the new notification model
	 */
	public void setNotificationModel(final Notification notificationModel) {
		this.notificationModel = notificationModel;
	}

	/**
	 * Gets the old model.
	 *
	 * @return the old model
	 */
	public Notification getOldModel() {
		return oldModel;
	}

	/**
	 * Sets the old model.
	 *
	 * @param oldModel
	 *            the new old model
	 */
	public void setOldModel(Notification oldModel) {
		this.oldModel = oldModel;
	}

	/**
	 * Sets the dirty object.
	 *
	 * @param dirty
	 *            the new dirty object
	 */
	public void setDirtyObject(final MDirtyable dirty) {
		this.dirty = dirty;
	}

	/**
	 * Gets the projects.
	 *
	 * @param projectsTbl
	 *            the projects tbl
	 * @return the projects
	 */
	protected List<Project> getProjects(final Iterable<ProjectsTbl> projectsTbl) {
		List<Project> projectList = new ArrayList<>();
		final File iconFolder = XMSystemUtil.getXMSystemServerIconsFolder();
		if (projectsTbl != null) {
			try {
				for (ProjectsTbl projectsTblVo : projectsTbl) {
					final String id = projectsTblVo.getProjectId();
					final String name = projectsTblVo.getName();
					boolean isActive = com.magna.xmbackend.vo.enums.Status.ACTIVE.name()
							.equals(projectsTblVo.getStatus()) ? true : false;

					IconsTbl iconTbl = projectsTblVo.getIconId();
					Icon icon = new Icon(iconTbl.getIconId(), iconTbl.getIconName(),
							iconFolder + File.separator + iconTbl.getIconName(), iconTbl.getIconType());

					Map<LANG_ENUM, String> descriptionMap = new HashMap<>();
					Map<LANG_ENUM, String> remarksMap = new HashMap<>();
					Map<LANG_ENUM, String> translationMap = new HashMap<>();

					Collection<ProjectTranslationTbl> projectTranslationTblList = projectsTblVo
							.getProjectTranslationTblCollection();
					for (ProjectTranslationTbl projectTranslationTbl : projectTranslationTblList) {
						LANG_ENUM langEnum = LANG_ENUM
								.getLangEnum(projectTranslationTbl.getLanguageCode().getLanguageCode());
						String translationId = projectTranslationTbl.getProjectTranslationId();
						translationMap.put(langEnum, translationId);
						descriptionMap.put(langEnum, projectTranslationTbl.getDescription());
						remarksMap.put(langEnum, projectTranslationTbl.getRemarks());
					}

					Project projectModel = new Project(id, name, isActive, descriptionMap, remarksMap, icon,
							CommonConstants.OPERATIONMODE.VIEW);

					projectModel.setTranslationIdMap(translationMap);
					projectList.add(projectModel);
				}
			} catch (Exception e) {
				LOGGER.error("Exeception while getting  Project objects! " + e);
			}
		}
		return projectList;
	}

	/**
	 * Gets the admin area.
	 *
	 * @param adminAreasTbl
	 *            the admin areas tbl
	 * @return the admin area
	 */
	private List<AdministrationArea> getAdminArea(final List<AdminAreasTbl> adminAreasTbl) {
		List<AdministrationArea> adminAreaList = new ArrayList<>();
		final File iconFolder = XMSystemUtil.getXMSystemServerIconsFolder();
		try {
			for (AdminAreasTbl adminAreasTblVo : adminAreasTbl) {
				final String id = adminAreasTblVo.getAdminAreaId();
				final String contact = adminAreasTblVo.getHotlineContactNumber();
				final String email = adminAreasTblVo.getHotlineContactEmail();
				final String name = adminAreasTblVo.getName();
				Long singletonAppTimeout = adminAreasTblVo.getSingletonAppTimeout();
				final boolean isActive = com.magna.xmbackend.vo.enums.Status.ACTIVE.name()
						.equals(adminAreasTblVo.getStatus()) ? true : false;

				IconsTbl iconTbl = adminAreasTblVo.getIconId();
				Icon icon = new Icon(iconTbl.getIconId(), iconTbl.getIconName(),
						iconFolder + File.separator + iconTbl.getIconName(), iconTbl.getIconType());

				Map<LANG_ENUM, String> descriptionMap = new HashMap<>();
				Map<LANG_ENUM, String> remarksMap = new HashMap<>();
				Map<LANG_ENUM, String> translationMap = new HashMap<>();
				Collection<AdminAreaTranslationTbl> adminAreaTranslationTblList = adminAreasTblVo
						.getAdminAreaTranslationTblCollection();
				for (AdminAreaTranslationTbl adminAreaTranslationTbl : adminAreaTranslationTblList) {
					LANG_ENUM langEnum = LANG_ENUM
							.getLangEnum(adminAreaTranslationTbl.getLanguageCode().getLanguageCode());
					String translationId = adminAreaTranslationTbl.getAdminAreaTranslationId();
					translationMap.put(langEnum, translationId);
					descriptionMap.put(langEnum, adminAreaTranslationTbl.getDescription());
					remarksMap.put(langEnum, adminAreaTranslationTbl.getRemarks());
				}

				AdministrationArea adminArea = new AdministrationArea(id, name, isActive, descriptionMap, contact,
						email, singletonAppTimeout, remarksMap, icon, CommonConstants.OPERATIONMODE.VIEW);

				adminArea.setTranslationIdMap(translationMap);
				adminAreaList.add(adminArea);
			}
		} catch (Exception e) {
			LOGGER.error("Exeception while getting  AdminArea objects! " + e);
		}
		return adminAreaList;
	}

	/**
	 * Gets the users.
	 *
	 * @param usersTbls
	 *            the users tbls
	 * @return the users
	 */
	protected List<User> getUsers(final Iterable<UsersTbl> usersTbls) {
		List<User> userList = new ArrayList<>();
		if (usersTbls != null) {
			for (UsersTbl usersTblVo : usersTbls) {
				final File iconFolder = XMSystemUtil.getXMSystemServerIconsFolder();
				try {
					final String id = usersTblVo.getUserId();
					final String userName = usersTblVo.getUsername();
					final String fullName = usersTblVo.getFullName();
					final String manager = usersTblVo.getManager();
					final String userEmailId = usersTblVo.getEmailId();
					final String userTelephone = usersTblVo.getTelephoneNumber();
					final String userDept = usersTblVo.getDepartment();
					final boolean isActive = com.magna.xmbackend.vo.enums.Status.ACTIVE.name()
							.equals(usersTblVo.getStatus()) ? true : false;

					IconsTbl iconTbl = usersTblVo.getIconId();
					Icon icon = new Icon(iconTbl.getIconId(), iconTbl.getIconName(),
							iconFolder + File.separator + iconTbl.getIconName(), iconTbl.getIconType());

					final Map<LANG_ENUM, String> descriptionMap = new HashMap<>();
					final Map<LANG_ENUM, String> remarksMap = new HashMap<>();
					final Map<LANG_ENUM, String> translationIdMap = new HashMap<>();

					Collection<UserTranslationTbl> userTranslationTblList = usersTblVo
							.getUserTranslationTblCollection();
					for (UserTranslationTbl userTranslationTbl : userTranslationTblList) {
						LANG_ENUM langEnum = LANG_ENUM
								.getLangEnum(userTranslationTbl.getLanguageCode().getLanguageCode());
						final String translationId = userTranslationTbl.getUserTranslationId();
						translationIdMap.put(langEnum, translationId);
						descriptionMap.put(langEnum, userTranslationTbl.getDescription());
						remarksMap.put(langEnum, userTranslationTbl.getRemarks());
					}

					User user = new User(id, userName, fullName, manager, isActive, userEmailId, userTelephone,
							userDept, descriptionMap, remarksMap, icon, CommonConstants.OPERATIONMODE.VIEW);
					user.setTranslationIdMap(translationIdMap);
					userList.add(user);
				} catch (Exception e) {
					LOGGER.error("Exeception while getting  user objects! " + e);
				}
			}
		}
		return userList;
	}

}
