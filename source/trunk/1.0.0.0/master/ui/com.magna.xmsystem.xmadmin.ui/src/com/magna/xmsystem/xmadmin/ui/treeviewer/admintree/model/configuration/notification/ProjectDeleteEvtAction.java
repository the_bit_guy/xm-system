package com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.notification;

import java.beans.PropertyChangeEvent;
import java.util.Set;
import java.util.TreeSet;

import com.magna.xmsystem.dependencies.utils.XMSystemUtil;
import com.magna.xmsystem.xmadmin.util.CommonConstants;

// TODO: Auto-generated Javadoc
/**
 * The Class ProjectDeleteEvtAction.
 */
public class ProjectDeleteEvtAction extends NotificationEvtActions {

	/**
	 * Instantiates a new project delete evt action.
	 *
	 * @param id
	 *            the id
	 * @param name
	 *            the name
	 * @param isActive
	 *            the is active
	 * @param operationMode
	 *            the operation mode
	 */
	public ProjectDeleteEvtAction(final String id, final String name, final boolean isActive, final int operationMode) {
		super(id, name, isActive, operationMode);
	}

	/**
	 * Deep copy pro delete evt action.
	 *
	 * @param update
	 *            the update
	 * @param updateThisObject
	 *            the update this object
	 * @return the project delete evt action
	 */
	public ProjectDeleteEvtAction deepCopyProDeleteEvtAction(boolean update, ProjectDeleteEvtAction updateThisObject) {
		ProjectDeleteEvtAction clonedProjectDeleteEvtAction = null;

		String currentId = XMSystemUtil.isEmpty(this.getId()) ? CommonConstants.EMPTY_STR : this.getId();
		String currentName = XMSystemUtil.isEmpty(this.getName()) ? CommonConstants.EMPTY_STR : this.getName();
		boolean currentIsActive = this.isActive();
		String currentDescription = XMSystemUtil.isEmpty(this.getDescription()) ? CommonConstants.EMPTY_STR
				: this.getDescription();
		Set<String> currentToUserList = this.getToUsers();
		Set<String> currentCCUserList = this.getCcUsers();

		NotificationTemplate currentTemplate = new NotificationTemplate();
		currentTemplate.setTemplateId(this.getTemplate().getTemplateId());
		currentTemplate.setName(this.getTemplate().getName());
		currentTemplate.setSubject(this.getTemplate().getSubject());
		currentTemplate.setMessage(this.getTemplate().getMessage());
		TreeSet<String> currentVariables;
		if (this.getTemplate().getVariables() != null) {
			currentVariables = new TreeSet<>(this.getTemplate().getVariables());
		}else{
			currentVariables = new TreeSet<>();
		}
		currentTemplate.setVariables(currentVariables);


		if (update) {
			clonedProjectDeleteEvtAction = updateThisObject;
		} else {
			clonedProjectDeleteEvtAction = new ProjectDeleteEvtAction(currentId, currentName, currentIsActive,
					CommonConstants.OPERATIONMODE.VIEW);
		}
		clonedProjectDeleteEvtAction.setId(currentId);
		clonedProjectDeleteEvtAction.setName(currentName);
		clonedProjectDeleteEvtAction.setActive(currentIsActive);
		clonedProjectDeleteEvtAction.setDescription(currentDescription);
		clonedProjectDeleteEvtAction.setToUsers(currentToUserList);
		clonedProjectDeleteEvtAction.setCcUsers(currentCCUserList);
		clonedProjectDeleteEvtAction.setTemplate(currentTemplate);

		return clonedProjectDeleteEvtAction;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.BeanModel#
	 * propertyChange(java.beans.PropertyChangeEvent)
	 */
	@Override
	public void propertyChange(PropertyChangeEvent event) {
		this.propertyChangeSupport.firePropertyChange(event.getPropertyName(), event.getOldValue(),
				event.getNewValue());

	}
}
