package com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.users;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.stream.Collectors;

import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.adminarea.AdministrationArea;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.refs.RelationObj;

/**
 * Class for User project admin areas.
 *
 * @author Chiranjeevi.Akula
 */
public class UserProjectAdminAreas implements IAdminTreeChild {

	/** Member variable 'parent' for {@link IAdminTreeChild}. */
	private IAdminTreeChild parent;

	/**
	 * Member variable 'user project admin areas children' for
	 * {@link Map<String,IAdminTreeChild>}.
	 */
	private Map<String, IAdminTreeChild> userProjectAdminAreasChildren;

	/**
	 * Constructor for UserProjectAdminAreas Class.
	 *
	 * @param parent
	 *            {@link IAdminTreeChild}
	 */
	public UserProjectAdminAreas(final IAdminTreeChild parent) {
		this.parent = parent;
		this.userProjectAdminAreasChildren = new LinkedHashMap<>();
	}

	/**
	 * Method for Adds the.
	 *
	 * @param userAdminAreaChildId
	 *            {@link String}
	 * @param child
	 *            {@link IAdminTreeChild}
	 * @return the i admin tree child {@link IAdminTreeChild}
	 */
	public IAdminTreeChild add(final String userAdminAreaChildId, final IAdminTreeChild child) {
		child.setParent(this);
		IAdminTreeChild returnVal = this.userProjectAdminAreasChildren.put(userAdminAreaChildId, child);
		if (child instanceof RelationObj && ((RelationObj) child).getRefObject() instanceof AdministrationArea) {
			sort();
		}
		return returnVal;
	}

	/**
	 * Method for Removes the.
	 *
	 * @param userAdminAreaChildId
	 *            {@link String}
	 * @return the i admin tree child {@link IAdminTreeChild}
	 */
	public IAdminTreeChild remove(final String userAdminAreaChildId) {
		return this.userProjectAdminAreasChildren.remove(userAdminAreaChildId);
	}

	/**
	 * Method for Removes the all.
	 */
	public void removeAll() {
		this.userProjectAdminAreasChildren.clear();
	}

	/**
	 * Gets the user project admin area child collection.
	 *
	 * @return the user project admin area child collection
	 */
	public Collection<IAdminTreeChild> getUserProjectAdminAreaChildCollection() {
		return this.userProjectAdminAreasChildren.values();
	}

	/**
	 * Gets the user project admin area child children.
	 *
	 * @return the user project admin area child children
	 */
	public Map<String, IAdminTreeChild> getUserProjectAdminAreaChildChildren() {
		return userProjectAdminAreasChildren;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild#
	 * getParent()
	 */
	@Override
	public IAdminTreeChild getParent() {
		return this.parent;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild#
	 * setParent(com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.
	 * IAdminTreeChild)
	 */
	@Override
	public void setParent(final IAdminTreeChild parent) {
		this.parent = parent;
	}
	
	/**
	 * Sort.
	 */
	public void sort() {
		final LinkedHashMap<String, IAdminTreeChild> collect = this.userProjectAdminAreasChildren.entrySet().stream().sorted(
				(e1, e2) -> ((AdministrationArea) (((RelationObj) e1.getValue()).getRefObject())).getName()
				.compareTo(((AdministrationArea) (((RelationObj) e2.getValue()).getRefObject())).getName()))
				.collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e1, LinkedHashMap::new));
		this.userProjectAdminAreasChildren = collect;
	}

}
