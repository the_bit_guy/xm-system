package com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.startapplicationmodel;

import java.beans.PropertyChangeEvent;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.core.runtime.Assert;

import com.magna.xmsystem.dependencies.utils.LANG_ENUM;
import com.magna.xmsystem.dependencies.utils.XMSystemUtil;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.BeanModel;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.adminarea.AdminAreaProjectStartApplications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.adminarea.AdminAreaStartApplications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.baseapplicationmodel.BaseAppStartApplications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.icon.Icon;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.projects.ProjectAdminAreaStartApplications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.refs.RelationObj;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.sitemodel.SiteAdminAreaProjectStartApplications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.sitemodel.SiteAdminAreaStartApplications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.users.UserStartApplications;
import com.magna.xmsystem.xmadmin.util.CommonConstants;

/**
 * Model class for StartApplication.
 *
 * @author Deepak upadhyay
 */
public class StartApplication extends BeanModel implements IAdminTreeChild, Cloneable {
	/**
	 * PROPERTY_STARTID constant
	 */
	public static final String PROPERTY_STARTID = "startApplicationId"; //$NON-NLS-1$

	/**
	 * PROPERTY_NAME constant
	 */
	public static final String PROPERTY_NAME = "name"; //$NON-NLS-1$

	/**
	 * PROPERTY_ACTIVE constant
	 */
	public static final String PROPERTY_ACTIVE = "active"; //$NON-NLS-1$
	
	/**
	 * PROPERTY_ISMESSAGE constant
	 */
	public static final String PROPERTY_MESSAGE = "message"; //$NON-NLS-1$
	
	/**
	 * PROPERTY_EXPIRYDATE constant
	 */
	public static final String PROPERTY_EXPIRYDATE = "expiryDate"; //$NON-NLS-1$

	/**
	 * PROPERTY_DESC_MAP constant
	 */
	public static final String PROPERTY_DESC_MAP = "descriptionMap"; //$NON-NLS-1$

	/**
	 * PROPERTY_REMARKS_MAP constant
	 */
	public static final String PROPERTY_REMARKS_MAP = "remarksMap"; //$NON-NLS-1$

	/**
	 * PROPERTY_APPLICATION_MAP constant
	 */
	public static final String PROPERTY_BASE_APPLICATION = "baseApplicationId"; //$NON-NLS-1$

	/**
	 * PROPERTY_OPERATION_MODE constant
	 */
	public static final String PROPERTY_OPERATION_MODE = "operationMode"; //$NON-NLS-1$

	/**
	 * PROPERTY_ICON constant
	 */
	public static final String PROPERTY_ICON = "icon"; //$NON-NLS-1$
	/**
	 * START_APPLICATION_NAME_LIMIT constant
	 */
	public static final int NAME_LIMIT = 30; // $NON-NLS-1$

	/**
	 * START_APPLICATION_DESC_LIMIT constant
	 */
	public static final int DESC_LIMIT = 240; // $NON-NLS-1$

	/**
	 * HELPTEXT_LIMIT constant
	 */
	public static final int REMARK_LIMIT = 1500; // $NON-NLS-1$

	/** Member variable for startApplication Id. */
	private String startApplicationId;

	/** The name. */
	private String name;

	/** Member variable to store is startPrgmApplication is active. */
	private boolean active;
	
	/** Member variable to store is startPrgmApplication is start message. */
	private boolean message;
	
	/** The expiry date. */
	private Date expiryDate;

	/** Member variable for startPrgmApplication description. */
	private Map<LANG_ENUM, String> descriptionMap;

	/** Member variable for startPrgmApplication remarks. */
	private Map<LANG_ENUM, String> remarksMap;

	/** Member variable for icon. */
	private Icon icon;

	/** Member variable for baseApplicationId. */
	private String baseApplicationId;

	/** Member variable for mode of operation. */
	private int operationMode;

	/** The project children. */
	private Map<String, IAdminTreeChild> startApplicationChildren;

	/** Member variable 'rel ids' for {@link List<String>}. */
	private List<String> relIds;
	
	/**
	 * Instantiates a new start application.
	 *
	 * @param startPrgmApplicationId the start prgm application id
	 * @param name the name
	 * @param isActive the is active
	 * @param icon the icon
	 * @param operationMode the operation mode
	 */
	public StartApplication(final String startPrgmApplicationId, final String name,
			final boolean isActive, final Icon icon, final int operationMode) {
		this(startPrgmApplicationId, name, isActive, new HashMap<>(), new HashMap<>(), icon, operationMode, null);
	}
	
	/**
	 * Instantiates a new start application.
	 *
	 * @param startPrgmApplicationId the start prgm application id
	 * @param name the name
	 * @param isActive the is active
	 * @param descriptionMap the description map
	 * @param remarksMap the remarks map
	 * @param icon the icon
	 * @param operationMode the operation mode
	 * @param baseApplicationId the base application id
	 */
	public StartApplication(final String startPrgmApplicationId, final String name,
			final boolean isActive, final Map<LANG_ENUM, String> descriptionMap,
			final Map<LANG_ENUM, String> remarksMap, final Icon icon, final int operationMode,
			final String baseApplicationId) {
		super();
		this.startApplicationId = startPrgmApplicationId;
		this.name = name;
		this.active = isActive;
		this.message = false;
		this.descriptionMap = descriptionMap;
		this.remarksMap = remarksMap;
		this.icon = icon;
		this.operationMode = operationMode;
		this.baseApplicationId = baseApplicationId;
		this.startApplicationChildren = new LinkedHashMap<>();
		
		this.relIds = new ArrayList<>();
		addFixedChildren();
	}

	/**
	 * Method for Adds the fixed children.
	 */
	public void addFixedChildren() {
		this.startApplicationChildren.put(StartAppAdminAreas.class.getName(), new StartAppAdminAreas(this));
		this.startApplicationChildren.put(StartAppProjects.class.getName(), new StartAppProjects(this));
		this.startApplicationChildren.put(StartAppUsers.class.getName(), new StartAppUsers(this));
	}
	
	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	public String getName() {
		return this.name;
	}

	/**
	 * Sets the name.
	 *
	 * @param name the new name
	 */
	public void setName(final String name) {
		if (name == null)
			throw new IllegalArgumentException();
		this.propertyChangeSupport.firePropertyChange(PROPERTY_NAME, this.name, this.name = name.trim());
	}

	/**
	 * Checks if is active.
	 *
	 * @return the isActive
	 */
	public boolean isActive() {
		return active;
	}

	/**
	 * Sets the active.
	 *
	 * @param isActive
	 *            the isActive to set
	 */
	public void setActive(final boolean isActive) {
		this.propertyChangeSupport.firePropertyChange(PROPERTY_ACTIVE, this.active, this.active = isActive);
	}
	
	/**
	 * @return the message
	 */
	public boolean isMessage() {
		return message;
	}

	/**
	 * @param message the message to set
	 */
	public void setMessage(boolean message) {
		this.propertyChangeSupport.firePropertyChange(PROPERTY_MESSAGE, this.message, this.message = message);
	}

	/**
	 * @return the expiryDate
	 */
	public Date getExpiryDate() {
		return expiryDate;
	}

	/**
	 * @param expiryDate the expiryDate to set
	 */
	public void setExpiryDate(final Date expiryDate) {
		this.propertyChangeSupport.firePropertyChange(PROPERTY_EXPIRYDATE, this.expiryDate, this.expiryDate = expiryDate);
	}

	/**
	 * Gets the description map.
	 *
	 * @return the descriptionMap
	 */
	public Map<LANG_ENUM, String> getDescriptionMap() {
		return descriptionMap;
	}

	/**
	 * Gets the description for given LANG_ENUM.
	 *
	 * @param lang
	 *            {@link LANG_ENUM}
	 * @return {@link String}
	 */
	public String getDescription(final LANG_ENUM lang) {
		return this.descriptionMap.get(lang);
	}

	/**
	 * Sets the description for given LANG_ENUM.
	 *
	 * @param lang
	 *            {@link LANG_ENUM}
	 * @param description
	 *            {@link String}
	 */
	public void setDescription(final LANG_ENUM lang, final String description) {
		if (lang == null || description == null)
			throw new IllegalArgumentException();
		this.propertyChangeSupport.firePropertyChange(PROPERTY_DESC_MAP, this.descriptionMap,
				this.descriptionMap.put(lang, description.trim()));
	}

	/**
	 * Method for Sets the description map.
	 *
	 * @param descriptionMap
	 *            the descriptionMap to set
	 */
	public void setDescriptionMap(final Map<LANG_ENUM, String> descriptionMap) {
		if (descriptionMap == null)
			throw new IllegalArgumentException();
		this.propertyChangeSupport.firePropertyChange(PROPERTY_DESC_MAP, this.descriptionMap,
				this.descriptionMap = descriptionMap);
	}

	/**
	 * Gets the remarks map.
	 *
	 * @return the remarksMap
	 */
	public Map<LANG_ENUM, String> getRemarksMap() {
		return remarksMap;
	}

	/**
	 * Gets the remarks for given LANG_ENUM.
	 *
	 * @param lang
	 *            {@link LANG_ENUM}
	 * @return {@link String}
	 */
	public String getRemarks(final LANG_ENUM lang) {
		return this.remarksMap.get(lang);
	}

	/**
	 * Method for Sets the remarks map.
	 *
	 * @param remarksMap
	 *            the remarksMap to set
	 */
	public void setRemarksMap(final Map<LANG_ENUM, String> remarksMap) {
		if (remarksMap == null)
			throw new IllegalArgumentException();
		this.propertyChangeSupport.firePropertyChange(PROPERTY_REMARKS_MAP, this.remarksMap,
				this.remarksMap = remarksMap);
	}

	/**
	 * Sets the remarks for given LANG_ENUM.
	 *
	 * @param lang
	 *            {@link LANG_ENUM}
	 * @param remarks
	 *            {@link String}
	 */
	public void setRemarks(final LANG_ENUM lang, final String remarks) {
		if (lang == null || remarks == null)
			throw new IllegalArgumentException();
		this.propertyChangeSupport.firePropertyChange(PROPERTY_REMARKS_MAP, this.remarksMap,
				this.remarksMap.put(lang, remarks));
	}

	/**
	 * Gets the startApplicationId id.
	 * 
	 * @return the startApplicationId
	 */
	public String getStartPrgmApplicationId() {
		return startApplicationId;
	}

	/**
	 * Sets the startApplication Id.
	 *
	 * @param startApplicationId
	 *            the startApplicationId to set
	 */
	public void setStartPrgmApplicationId(String startApplicationId) {
		this.propertyChangeSupport.firePropertyChange(PROPERTY_STARTID, this.startApplicationId,
				this.startApplicationId = startApplicationId);
	}

	/**
	 * Gets the icon.
	 *
	 * @return the icon
	 */
	public Icon getIcon() {
		return icon;
	}

	/**
	 * Sets the icon.
	 *
	 * @param icon
	 *            the icon to set
	 */
	public void setIcon(Icon icon) {
		if (icon == null)
			throw new IllegalArgumentException();
		this.propertyChangeSupport.firePropertyChange(PROPERTY_ICON, this.icon, this.icon = icon);
	}

	public Map<String, IAdminTreeChild> getStartApplicationChildren() {
		return startApplicationChildren;
	}

	/**
	 * Gets the operation mode.
	 *
	 * @return the operation mode
	 */
	public int getOperationMode() {
		return operationMode;
	}

	/**
	 * Sets the operation mode.
	 *
	 * @param operationMode
	 *            the new operation mode
	 */
	public void setOperationMode(int operationMode) {
		this.propertyChangeSupport.firePropertyChange(PROPERTY_OPERATION_MODE, this.operationMode,
				this.operationMode = operationMode);
	}

	/**
	 * @return the baseApplicationId
	 */
	public String getBaseApplicationId() {
		return baseApplicationId;
	}

	/**
	 * @param baseApplicationId
	 *            the baseApplicationId to set
	 */
	public void setBaseApplicationId(String baseApplicationId) {
		this.propertyChangeSupport.firePropertyChange(PROPERTY_BASE_APPLICATION, this.baseApplicationId,
				this.baseApplicationId = baseApplicationId);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.BeanModel#
	 * propertyChange(java.beans.PropertyChangeEvent)
	 */
	@Override
	public void propertyChange(PropertyChangeEvent event) {
		this.propertyChangeSupport.firePropertyChange(event.getPropertyName(), event.getOldValue(),
				event.getNewValue());
	}

	/**
	 * Method for Deep copy Start Application.
	 *
	 * @param update
	 *            {@link boolean}
	 * @return the StartApplication {@link StartApplication}
	 */
	public StartApplication deepCopyStartApplication(boolean update, StartApplication updateThisObject) {
		StartApplication clonedStartApplication = null;
		String currentStartApplicationId = XMSystemUtil.isEmpty(this.getStartPrgmApplicationId())
				? CommonConstants.EMPTY_STR : this.getStartPrgmApplicationId();

		//Icon currentIcon = this.getIcon() == null ? null : this.getIcon();
		Icon currentIcon = new Icon(this.getIcon().getIconId(), this.getIcon().getIconName(),
				this.getIcon().getIconPath(), this.getIcon().getIconType());
		boolean currentIsActive = this.isActive();
		boolean currentIsMessage = this.isMessage();
		Date currentExpiryDate = this.getExpiryDate();
		String name = this.getName();

		Map<LANG_ENUM, String> currentTranslationIdMap = new HashMap<>();
		Map<LANG_ENUM, String> currentDescriptionMap = new HashMap<>();
		Map<LANG_ENUM, String> currentRemarksMap = new HashMap<>();

		for (LANG_ENUM langEnum : LANG_ENUM.values()) {
			String translationId = this.getTranslationId(langEnum);
			currentTranslationIdMap.put(langEnum,
					XMSystemUtil.isEmpty(translationId) ? CommonConstants.EMPTY_STR : translationId);

			String description = this.getDescription(langEnum);
			currentDescriptionMap.put(langEnum,
					XMSystemUtil.isEmpty(description) ? CommonConstants.EMPTY_STR : description);

			String remarks = this.getRemarks(langEnum);
			currentRemarksMap.put(langEnum, XMSystemUtil.isEmpty(remarks) ? CommonConstants.EMPTY_STR : remarks);
		}

		String currentBaseApplicationId = XMSystemUtil.isEmpty(this.getBaseApplicationId()) ? null
				: this.getBaseApplicationId();

		if (update) {
			clonedStartApplication = updateThisObject;
		} else {
			clonedStartApplication = new StartApplication(currentStartApplicationId, name, currentIsActive,
					currentIcon, CommonConstants.OPERATIONMODE.VIEW);
		}
		clonedStartApplication.setName(name);
		clonedStartApplication.setBaseApplicationId(currentStartApplicationId);
		clonedStartApplication.setActive(currentIsActive);
		clonedStartApplication.setMessage(currentIsMessage);
		clonedStartApplication.setExpiryDate(currentExpiryDate);
		clonedStartApplication.setIcon(currentIcon);
		clonedStartApplication.setTranslationIdMap(currentTranslationIdMap);
		clonedStartApplication.setDescriptionMap(currentDescriptionMap);
		clonedStartApplication.setRemarksMap(currentRemarksMap);
		clonedStartApplication.setBaseApplicationId(currentBaseApplicationId);
		return clonedStartApplication;
	}
	
	/* (non-Javadoc)
	 * @see com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.BeanModel#getAdapter(java.lang.Class, com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild)
	 */
	@Override
	public <T> RelationObj getAdapter(Class<T> adapterType, IAdminTreeChild parent, String relationId, boolean relationStatus) {
		Assert.isNotNull(adapterType);
		Assert.isNotNull(this);
		this.relIds.add(relationId);
		if (adapterType == SiteAdminAreaStartApplications.class) {
			SiteAdminAreaStartApplications siteAdminAreaStartApplications = new SiteAdminAreaStartApplications(parent);
			RelationObj relObj = new RelationObj(relationId, this, siteAdminAreaStartApplications, relationStatus, null);
			return relObj;
		} else if (adapterType == SiteAdminAreaProjectStartApplications.class) {
			SiteAdminAreaProjectStartApplications siteAdminAreaProjectStartApplications = new SiteAdminAreaProjectStartApplications(parent);
			RelationObj relObj = new RelationObj(relationId, this, siteAdminAreaProjectStartApplications, relationStatus, null);
			return relObj;
		} else if (adapterType == AdminAreaStartApplications.class) {
			AdminAreaStartApplications adminAreaStartApplications = new AdminAreaStartApplications(parent);
			RelationObj relObj = new RelationObj(relationId, this, adminAreaStartApplications, relationStatus, null);
			return relObj;
		} else if (adapterType == AdminAreaProjectStartApplications.class) {
			AdminAreaProjectStartApplications siteAdminAreaProjectStartApplications = new AdminAreaProjectStartApplications(parent);
			RelationObj relObj = new RelationObj(relationId, this, siteAdminAreaProjectStartApplications, relationStatus, null);
			return relObj;
		} else if (adapterType == ProjectAdminAreaStartApplications.class) {
			ProjectAdminAreaStartApplications projectAdminAreaStartApplications = new ProjectAdminAreaStartApplications(parent);
			RelationObj relObj = new RelationObj(relationId, this, projectAdminAreaStartApplications, relationStatus, null);
			return relObj;
		} else if (adapterType == BaseAppStartApplications.class) {
			BaseAppStartApplications baseAppStartApplications = new BaseAppStartApplications(parent);
			RelationObj relObj = new RelationObj(relationId, this, baseAppStartApplications, relationStatus, null);
			return relObj;
		} else if (adapterType == UserStartApplications.class) {
			UserStartApplications userStartApplications = new UserStartApplications(parent);
			RelationObj relObj = new RelationObj(relationId, this, userStartApplications, relationStatus, null);
			return relObj;
		}
		
		return super.getAdapter(adapterType, parent, relationId, relationStatus);
	}
}
