package com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.roles;

/**
 * The Enum ObjectType.
 * 
 * @author archita.patel
 */
public enum ObjectType {

	/** The administration area. */
	ADMINISTRATION_AREA,

	/** The base application. */
	BASE_APPLICATION,
	
	/** The directory. */
	DIRECTORY,
	
	/** The group. */
	GROUP,
	
	/** The icon. */
	ICON,
	
	/** The live message. */
	LIVE_MESSAGE,
	
	/** The notification. */
	NOTIFICATION,
	
	/** The projects. */
	PROJECT,
	
	/** The project application. */
	PROJECT_APPLICATION,
	
	/** The property config. */
	PROPERTY_CONFIG,
	
	/** The role. */
	ROLE,
	
	/** The site. */
	SITE,
	
	/** The start application. */
	START_APPLICATION,
	
	/** The user. */
	USER,
	
	/** The user application. */
	USER_APPLICATION;
	
	/**
	 * Gets the object types.
	 *
	 * @return the object types
	 */
	public static String[] getObjectTypes() {
		ObjectType[] values = values();
		String[] returnArray = new String[values.length];
		for (int index = 0; index < values.length; index++) {
			returnArray[index] = values[index].name();
		}
		return returnArray;
	}
	

	/**
	 * Gets the object type.
	 *
	 * @param objectType the object type
	 * @return the object type
	 */
	public static ObjectType getObjectType(String objectType) {
        for (ObjectType value : values()) {
            if (objectType.equalsIgnoreCase(value.name())) {
                return value;
            }
        }
        return null;
    }
}
