
package com.magna.xmsystem.xmadmin.ui.handlers;

import java.util.List;

import javax.inject.Inject;

import org.eclipse.e4.core.di.annotations.CanExecute;
import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.ui.model.application.MApplication;
import org.eclipse.e4.ui.model.application.ui.basic.MPartStack;
import org.eclipse.e4.ui.model.application.ui.basic.MStackElement;
import org.eclipse.e4.ui.model.application.ui.menu.MMenuItem;
import org.eclipse.e4.ui.workbench.modeling.EModelService;

// TODO: Auto-generated Javadoc
/**
 * The Class MessageConsoleHandler.
 */
public class MessageConsoleHandler {

	/** Member variable 'model service' for {@link EModelService}. */
	@Inject
	private EModelService modelService;

	/** Member variable 'application' for {@link MApplication}. */
	@Inject
	private MApplication application;

	/**
	 * Execute.
	 *
	 * @param menuItem
	 *            the menu item
	 */
	@Execute
	public void execute(final MMenuItem menuItem) {

		if (menuItem != null) {
			boolean isMenuItemSelected = menuItem.isSelected();
			MPartStack mPartStack = (MPartStack) this.modelService
					.find("com.magna.xmsystem.xmadmin.ui.partstack.messageconsole", application);
			List<MStackElement> children = mPartStack.getChildren();
			if (!isMenuItemSelected) {
				for (MStackElement mStackElement : children) {
					mStackElement.setVisible(true);
					mStackElement.setToBeRendered(true);
					mStackElement.setOnTop(true);
				}
				mPartStack.setVisible(true);
				mPartStack.setToBeRendered(true);
				mPartStack.setOnTop(true);
			} else {
				mPartStack.setVisible(false);
				mPartStack.setToBeRendered(false);
			}
		}

	}

	/**
	 * Can execute.
	 *
	 * @param menuItem
	 *            the menu item
	 * @return true, if successful
	 */
	@CanExecute
	public boolean canExecute(final MMenuItem menuItem) {
		MPartStack mPartStack = (MPartStack) this.modelService
				.find("com.magna.xmsystem.xmadmin.ui.partstack.messageconsole", application);
		if (menuItem != null && mPartStack != null) {
			menuItem.setSelected(mPartStack.isToBeRendered());
			return true;
		}
		return false;
	}

}