package com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.adminarea;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.stream.Collectors;

import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.startapplicationmodel.StartApplication;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.refs.RelationObj;

/**
 * The Class AdminAreaStartApplications.
 * 
 * @author subash.janarthanan
 * 
 */
public class AdminAreaStartApplications implements IAdminTreeChild {

	/** The parent. */
	private IAdminTreeChild parent;
	
	/** The admin area start app child. */
	private Map<String, IAdminTreeChild> adminAreaStartAppChild;


	/**
	 * Instantiates a new admin area child start app.
	 *
	 * @param parent the parent
	 */
	public AdminAreaStartApplications(final IAdminTreeChild parent) {
		this.parent = parent;
		this.adminAreaStartAppChild = new LinkedHashMap<>();
	}
	
	/**
	 * @return the adminAreaStartAppChild
	 */
	public Map<String, IAdminTreeChild> getAdminAreaStartAppChild() {
		return adminAreaStartAppChild;
	}
	
	/**
	 * Gets the admin area start app collection.
	 *
	 * @return the admin area start app collection
	 */
	public Collection<IAdminTreeChild> getAdminAreaStartAppCollection() {
		return this.adminAreaStartAppChild.values();
	}
	
	/**
	 * Add.
	 *
	 * @param adminAreaStartAppChildId the admin area start app child id
	 * @param child the child
	 * @return the i admin tree child
	 */
	public IAdminTreeChild add(final String adminAreaStartAppChildId, final IAdminTreeChild child) {
		child.setParent(this);
		IAdminTreeChild returnVal = this.adminAreaStartAppChild.put(adminAreaStartAppChildId, child);
		if (child instanceof RelationObj && ((RelationObj) child).getRefObject() instanceof StartApplication) {
			sort();
		}
		return returnVal;
	}
	
	/**
	 * Remove.
	 *
	 * @param adminAreaStartAppChildId the admin area start app child id
	 * @return the i admin tree child
	 */
	public IAdminTreeChild remove(final String adminAreaStartAppChildId) {
		return this.adminAreaStartAppChild.remove(adminAreaStartAppChildId);
	}

	/* (non-Javadoc)
	 * @see com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild#getParent()
	 */
	@Override
	public IAdminTreeChild getParent() {
		return this.parent;
	}

	/* (non-Javadoc)
	 * @see com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild#setParent(com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild)
	 */
	@Override
	public void setParent(final IAdminTreeChild parent) {
		this.parent = parent;
	}
	
	/**
	 * Sort.
	 */
	public void sort() {
		final LinkedHashMap<String, IAdminTreeChild> collect = this.adminAreaStartAppChild.entrySet().stream().sorted(
				(e1, e2) -> ((StartApplication) (((RelationObj) e1.getValue()).getRefObject())).getName()
				.compareTo(((StartApplication) (((RelationObj) e2.getValue()).getRefObject())).getName()))
				.collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e1, LinkedHashMap::new));
		this.adminAreaStartAppChild = collect;
	}

}
