package com.magna.xmsystem.xmadmin.ui.handlers.tester;

import org.eclipse.core.expressions.PropertyTester;

import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.roles.Role;
import com.magna.xmsystem.xmadmin.util.CommonConstants;

/**
 * The Class RolePropertyTester.
 * 
 * @author archita.patel
 */
public class RolePropertyTester extends PropertyTester {
	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.core.expressions.IPropertyTester#test(java.lang.Object,
	 * java.lang.String, java.lang.Object[], java.lang.Object)
	 */
	@Override
	public boolean test(Object receiver, String arg1, Object[] arg2, Object arg3) {
		if (receiver instanceof Role) {
			Role role = (Role) receiver;
			if (CommonConstants.SuperAdminRole.NAME.equals(role.getRoleName())) { //$NON-NLS-1$
				return false;
			} else
				return true;
		}
		return false;
	}

}
