package com.magna.xmsystem.xmadmin.ui.site.wizard;

import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.widgets.Composite;

import com.magna.xmsystem.dependencies.utils.LANG_ENUM;
import com.magna.xmsystem.dependencies.utils.XMSystemUtil;
import com.magna.xmsystem.xmadmin.message.Message;
import com.magna.xmsystem.xmadmin.message.MessageRegistry;
import com.magna.xmsystem.xmadmin.ui.parts.site.SiteCompositeAction;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.icon.Icon;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.sitemodel.Site;
import com.magna.xmsystem.xmadmin.util.CommonConstants;

/**
 * SiteCreationPage class to create site page.
 *
 * @author Roshan Ekka
 */
@Deprecated
public class SiteCreationPage extends WizardPage {

	/** Member variable 'messages' for {@link Message}. */
	private Message messages;

	/** Member variable 'registry' for {@link MessageRegistry}. */
	private MessageRegistry registry;

	/** Member variable 'site model' for {@link Site}. */
	private Site siteModel;

	/**
	 * Constructor for SiteCreationPage Class.
	 *
	 * @param pageName
	 *            {@link String}
	 * @param messages
	 *            {@link Message}
	 * @param registry
	 *            {@link MessageRegistry}
	 */
	protected SiteCreationPage(String pageName, Message messages, MessageRegistry registry) {
		super(pageName);
		this.messages = messages;
		this.registry = registry;
		setTitle("Site Creation");
		setDescription("Display the master data of <site>");
		this.siteModel = new Site(null, "Name Name", false, null, CommonConstants.OPERATIONMODE.CREATE);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jface.dialogs.IDialogPage#createControl(org.eclipse.swt.
	 * widgets.Composite)
	 */
	public void createControl(Composite composite) {
		SiteCompositeAction comp = new SiteCompositeAction(composite);
		//comp.setShowButtonBar(false);
		/*comp.getNotesComponent().getTextArea().addFocusListener(new FocusListener() {

			@Override
			public void focusLost(FocusEvent e) {
				// TODO Auto-generated method stub
				setPageComplete(false);

			}

			@Override
			public void focusGained(FocusEvent e) {
				// TODO Auto-generated method stub

			}
		});*/
		comp.setSiteModel(this.siteModel);
		comp.registerMessages(this.registry);

		comp.bindValues();
		setControl(composite);
		setPageComplete(false);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jface.wizard.WizardPage#setPageComplete(boolean)
	 */
	@Override
	public void setPageComplete(boolean complete) {
		super.setPageComplete(validateModel());
	}

	/**
	 * Method for Validate model.
	 *
	 * @return true, if successful
	 */
	private boolean validateModel() {

		Icon icon;
		return ((!XMSystemUtil.isEmpty(this.siteModel.getName()) && ((icon = this.siteModel.getIcon()) != null)
				&& !XMSystemUtil.isEmpty(icon.getIconName()))
				&& (!XMSystemUtil.isEmpty(this.siteModel.getDescription(LANG_ENUM.ENGLISH))
						&& !XMSystemUtil.isEmpty(this.siteModel.getDescription(LANG_ENUM.GERMAN))));
	}

	/**
	 * Gets the site model.
	 *
	 * @return the siteModel
	 */
	public Site getSiteModel() {
		return siteModel;
	}

	/**
	 * Sets the site model.
	 *
	 * @param siteModel
	 *            the siteModel to set
	 */
	public void setSiteModel(Site siteModel) {
		this.siteModel = siteModel;
	}

}