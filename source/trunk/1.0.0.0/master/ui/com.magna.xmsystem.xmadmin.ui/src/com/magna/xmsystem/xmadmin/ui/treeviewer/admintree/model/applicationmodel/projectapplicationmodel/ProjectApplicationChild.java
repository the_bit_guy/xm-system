package com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.projectapplicationmodel;

import java.util.HashMap;
import java.util.Map;

import com.magna.xmsystem.dependencies.utils.LANG_ENUM;
import com.magna.xmsystem.dependencies.utils.XMSystemUtil;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.icon.Icon;
import com.magna.xmsystem.xmadmin.util.CommonConstants;

// TODO: Auto-generated Javadoc
/**
 * The Class ProjectApplicationChild.
 * 
 * @author shashwat.anand
 */
public class ProjectApplicationChild extends ProjectApplication {

	/**
	 * Instantiates a new project application child.
	 *
	 * @param userApplicationId the user application id
	 * @param name the name
	 * @param description the description
	 * @param nameMap the name map
	 * @param isActive the is active
	 * @param icon the icon
	 * @param operationMode the operation mode
	 */
	public ProjectApplicationChild(final String userApplicationId, final String name, final  String description, final Map<LANG_ENUM, String> nameMap,
			final boolean isActive, final Icon icon, final int operationMode) {
		this(userApplicationId, name, description, nameMap, isActive, new HashMap<>(), new HashMap<>(), icon, false, false, null, null,
				operationMode);
	}

	/**
	 * Instantiates a new project application child.
	 *
	 * @param userApplicationId the user application id
	 * @param name the name
	 * @param description the description
	 * @param nameMap the name map
	 * @param active the active
	 * @param descriptionMap the description map
	 * @param remarksMap the remarks map
	 * @param icon the icon
	 * @param parent the parent
	 * @param singleton the singleton
	 * @param position the position
	 * @param baseApplicationId the base application id
	 * @param operationMode the operation mode
	 */
	public ProjectApplicationChild(final String userApplicationId, final String name, final  String description, final Map<LANG_ENUM, String> nameMap,
			final boolean active, final Map<LANG_ENUM, String> descriptionMap, final Map<LANG_ENUM, String> remarksMap,
			final Icon icon, final boolean parent, final boolean singleton, final String position,
			final String baseApplicationId, final int operationMode) {
		super(userApplicationId, name, description, nameMap, active, descriptionMap, remarksMap, icon, parent, singleton, position,
				baseApplicationId, operationMode);
		getProjectApplicationChildren().clear();

	}

	/**
	 * Deep copy project application child.
	 *
	 * @param update
	 *            the update
	 * @param updateThisObject
	 *            the update this object
	 * @return the project application child
	 */
	public ProjectApplicationChild deepCopyProjectApplicationChild(boolean update,
			ProjectApplicationChild updateThisObject) {
		ProjectApplicationChild clonedProjectApplicationChild = null;
		String currentUserApplicationId = XMSystemUtil.isEmpty(this.getProjectApplicationId())
				? CommonConstants.EMPTY_STR : this.getProjectApplicationId();
		String currentInternalName = this.getName();
		String currentInternalDesc = this.getDescription();
		boolean currentIsActive = this.isActive();
		boolean currentIsParent = this.isParent();
		boolean currentIsSingleton = this.isSingleton();
		String currentBaseAppId = XMSystemUtil.isEmpty(this.getBaseApplicationId()) ? null
				: this.getBaseApplicationId();
		String currentPosition = XMSystemUtil.isEmpty(this.getPosition()) ? CommonConstants.EMPTY_STR
				: this.getPosition();
		Icon currentIcon = this.getIcon() == null ? null : this.getIcon();

		Map<LANG_ENUM, String> currentTranslationIdMap = new HashMap<>();
		Map<LANG_ENUM, String> currentNameMap = new HashMap<>();
		Map<LANG_ENUM, String> currentDescriptionMap = new HashMap<>();
		Map<LANG_ENUM, String> currentRemarksMap = new HashMap<>();

		for (LANG_ENUM langEnum : LANG_ENUM.values()) {
			String translationId = this.getTranslationId(langEnum);
			currentTranslationIdMap.put(langEnum,
					XMSystemUtil.isEmpty(translationId) ? CommonConstants.EMPTY_STR : translationId);
			String name = this.getName(langEnum);
			currentNameMap.put(langEnum, XMSystemUtil.isEmpty(name) ? CommonConstants.EMPTY_STR : name);
			String description = this.getDescription(langEnum);
			currentDescriptionMap.put(langEnum,
					XMSystemUtil.isEmpty(description) ? CommonConstants.EMPTY_STR : description);
			String remarks = this.getRemarks(langEnum);
			currentRemarksMap.put(langEnum, XMSystemUtil.isEmpty(remarks) ? CommonConstants.EMPTY_STR : remarks);
		}

		if (update) {
			clonedProjectApplicationChild = updateThisObject;
		} else {
			clonedProjectApplicationChild = new ProjectApplicationChild(currentUserApplicationId, currentInternalName, currentInternalDesc, currentNameMap,
					currentIsActive, currentIcon, CommonConstants.OPERATIONMODE.VIEW);
		}

		clonedProjectApplicationChild.setProjectApplicationId(currentUserApplicationId);
		clonedProjectApplicationChild.setActive(currentIsActive);
		clonedProjectApplicationChild.setIcon(currentIcon);
		clonedProjectApplicationChild.setNameMap(currentNameMap);
		clonedProjectApplicationChild.setTranslationIdMap(currentTranslationIdMap);
		clonedProjectApplicationChild.setDescriptionMap(currentDescriptionMap);
		clonedProjectApplicationChild.setRemarksMap(currentRemarksMap);
		clonedProjectApplicationChild.setParent(currentIsParent);
		clonedProjectApplicationChild.setSingleton(currentIsSingleton);
		clonedProjectApplicationChild.setBaseApplicationId(currentBaseAppId);
		clonedProjectApplicationChild.setPosition(currentPosition);

		return clonedProjectApplicationChild;

	}

}
