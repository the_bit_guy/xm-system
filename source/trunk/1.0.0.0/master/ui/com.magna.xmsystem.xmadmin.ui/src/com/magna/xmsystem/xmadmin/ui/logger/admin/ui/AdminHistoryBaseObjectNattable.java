package com.magna.xmsystem.xmadmin.ui.logger.admin.ui;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.nebula.widgets.nattable.NatTable;
import org.eclipse.nebula.widgets.nattable.config.AbstractRegistryConfiguration;
import org.eclipse.nebula.widgets.nattable.config.ConfigRegistry;
import org.eclipse.nebula.widgets.nattable.config.DefaultNatTableStyleConfiguration;
import org.eclipse.nebula.widgets.nattable.config.IConfigRegistry;
import org.eclipse.nebula.widgets.nattable.data.ListDataProvider;
import org.eclipse.nebula.widgets.nattable.layer.DataLayer;
import org.eclipse.nebula.widgets.nattable.layer.cell.ColumnOverrideLabelAccumulator;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.client.ResourceAccessException;

import com.magna.xmbackend.entities.AdminHistoryBaseObjectsTbl;
import com.magna.xmbackend.vo.adminHistory.AdminHistoryRequest;
import com.magna.xmbackend.vo.adminHistory.AdminHistoryResponse;
import com.magna.xmsystem.dependencies.customExceptions.UnauthorizedAccessException;
import com.magna.xmsystem.ui.controls.widgets.CustomMessageDialog;
import com.magna.xmsystem.xmadmin.restclient.adminHistory.AdminHistoryController;
import com.magna.xmsystem.xmadmin.ui.logger.HistoryTableUtil;
import com.magna.xmsystem.xmadmin.ui.logger.admin.ui.enums.AdminHistoryBaseColumnEnum;
import com.magna.xmsystem.xmadmin.util.CommonConstants;

/**
 * The Class AdminHistoryBaseObjectNattable.
 */
public class AdminHistoryBaseObjectNattable {
	
	/** The Constant LOGGER. */
	private static final Logger LOGGER = LoggerFactory.getLogger(AdminHistoryBaseObjectNattable.class);
	
	/** The nat table. */
	private NatTable natTable;

	/** The nattable layer. */
	private AdminHistoryBaseObjectGridLayer nattableLayer;
	
	/** The admin history job. */
	private Job adminHistoryJob;

	/**
	 * Create admin history base object nat table.
	 *
	 * @param parent the parent
	 * @param limit the limit
	 * @return the nat table
	 */
	@SuppressWarnings("rawtypes")
	public NatTable createAdminHistoryBaseObjectNatTable(final Composite parent, final AdminHistoryRequest adminHistoryRequest) {
		IConfigRegistry configRegistry = new ConfigRegistry();
		ArrayList<AdminHistoryBaseObjectsTbl> userHistoryTableList = retrieveAdminHistoryTableList(adminHistoryRequest);
		nattableLayer = new AdminHistoryBaseObjectGridLayer(configRegistry, userHistoryTableList);

		DataLayer bodyDataLayer = nattableLayer.getBodyDataLayer();
		ListDataProvider dataProvider = nattableLayer.getBodyDataProvider();

		// NOTE: Register the accumulator on the body data layer.
		// This ensures that the labels are bound to the column index and are
		// unaffected by column order.
		final ColumnOverrideLabelAccumulator columnLabelAccumulator = new ColumnOverrideLabelAccumulator(bodyDataLayer);
		bodyDataLayer.setConfigLabelAccumulator(columnLabelAccumulator);

		natTable = new NatTable(parent, nattableLayer, false);
		
		GridData gd_table = new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1);
		gd_table.widthHint = 600;
		gd_table.minimumHeight = 250;
		gd_table.heightHint = 250;
		natTable.setLayoutData(gd_table);
		
		HistoryTableUtil.getInstance().addCustomStyling(natTable, dataProvider);
		natTable.addConfiguration(new DefaultNatTableStyleConfiguration());
		HistoryTableUtil.getInstance().addColumnHighlight(configRegistry);

		// natTable.addConfiguration(new DebugMenuConfiguration(natTable));
		natTable.addConfiguration(new FilterRowCustomConfiguration());

		natTable.setConfigRegistry(configRegistry);
		natTable.configure();

		return natTable;
	}

	/**
	 * Retrieve user history table list.
	 *
	 * @param adminHistoryRequest the admin history request
	 * @return the array list
	 */
	private ArrayList<AdminHistoryBaseObjectsTbl> retrieveAdminHistoryTableList(final AdminHistoryRequest adminHistoryRequest) {
		ArrayList<AdminHistoryBaseObjectsTbl> adminHistoryBaseObjTableList = new ArrayList<>();
		try {
			if(adminHistoryRequest != null){
				AdminHistoryResponse adminHistoryResponse;
				AdminHistoryController adminHistoryController = new AdminHistoryController();
				adminHistoryResponse = adminHistoryController.getAllAdminBaseHistory(adminHistoryRequest);
				
				if (adminHistoryResponse != null) {
					List<Map<String, Object>> queryResultSet = adminHistoryResponse.getQueryResultSet();
					for (Map<String, Object> map : queryResultSet) {
						AdminHistoryBaseObjectsTbl adminHistoryBaseObjectsTbl = new AdminHistoryBaseObjectsTbl();
						adminHistoryBaseObjectsTbl.setId(Long.valueOf(String.valueOf(map.get(AdminHistoryBaseColumnEnum.ID.name()))));
						Long logTime = (Long) map.get(AdminHistoryBaseColumnEnum.LOG_TIME.name());
						adminHistoryBaseObjectsTbl.setLogTime(new Date(logTime));
						adminHistoryBaseObjectsTbl.setAdminName((String) map.get(AdminHistoryBaseColumnEnum.ADMIN_NAME.name()));
						adminHistoryBaseObjectsTbl.setApiRequestPath((String) map.get(AdminHistoryBaseColumnEnum.API_REQUEST_PATH.name()));
						adminHistoryBaseObjectsTbl.setOperation((String) map.get(AdminHistoryBaseColumnEnum.OPERATION.name()));
						adminHistoryBaseObjectsTbl.setObjectName((String) map.get(AdminHistoryBaseColumnEnum.OBJECT_NAME.name()));
						adminHistoryBaseObjectsTbl.setChanges((String) map.get(AdminHistoryBaseColumnEnum.CHANGES.name()));
						adminHistoryBaseObjectsTbl.setErrorMessage((String) map.get(AdminHistoryBaseColumnEnum.ERROR_MESSAGE.name()));
						adminHistoryBaseObjectsTbl.setResult((String) map.get(AdminHistoryBaseColumnEnum.RESULT.name()));
						adminHistoryBaseObjTableList.add(adminHistoryBaseObjectsTbl);
					}
				}
			}
			
			return adminHistoryBaseObjTableList;
		} catch (NumberFormatException e) {
			LOGGER.error("Exception occured while loading adminHistory data " + e);
		} catch (UnauthorizedAccessException e) {
			LOGGER.error("Current user is Unauthorized " + e);
		}catch (Exception e) {
			if (e instanceof ResourceAccessException) {
				/*Display.getDefault().asyncExec(new Runnable() {
					
					@Override
					public void run() {
						CustomMessageDialog.openError(Display.getDefault().getActiveShell(),"Error",
								"Server not reachable");
					}
				});*/
				throw e;
			
			}
			LOGGER.error("Exception occured while loading adminHistory data " + e);
		}
		return adminHistoryBaseObjTableList;
	}

	/**
	 * The Class FilterRowCustomConfiguration.
	 */
	static class FilterRowCustomConfiguration extends AbstractRegistryConfiguration {

		/* (non-Javadoc)
		 * @see org.eclipse.nebula.widgets.nattable.config.IConfiguration#configureRegistry(org.eclipse.nebula.widgets.nattable.config.IConfigRegistry)
		 */
		@Override
		public void configureRegistry(IConfigRegistry configRegistry) {

		}
	}

	/**
	 * Update nat table.
	 *
	 * @param adminHistoryRequest the admin history request
	 */
	public void updateNatTable(final AdminHistoryRequest adminHistoryRequest) {
		
		if (adminHistoryJob != null && adminHistoryJob.getState() == Job.RUNNING) {
			//adminHistoryJob.done(Status.OK_STATUS);
			return;
		}

		adminHistoryJob = new Job(CommonConstants.History.ADMIN_HISTORY_JOB_NAME) {
			@Override
			protected IStatus run(IProgressMonitor monitor) {
				monitor.beginTask(CommonConstants.History.ADMIN_HISTORY_JOB_NAME, 100);
				monitor.worked(30);
				try {
					Display.getDefault().asyncExec(new Runnable() {
						@Override
						public void run() {
							// if (!adminHistoryJob.cancel()) {
							if (nattableLayer != null) {
								nattableLayer.getBodyDataProvider().getList().clear();
							}
							// }
						}
					});
					ArrayList<AdminHistoryBaseObjectsTbl> retrieveAdminHistoryTableList = retrieveAdminHistoryTableList(adminHistoryRequest);
					Display.getDefault().asyncExec(new Runnable() {
						@Override
						public void run() {
							// if (!adminHistoryJob.cancel()) {
							if (nattableLayer != null && natTable != null && !natTable.isDisposed()) {
								nattableLayer.getBodyDataProvider().getList().addAll(retrieveAdminHistoryTableList);
								natTable.update();
								natTable.refresh();
							}
							// }
						}
					});

				} catch (Exception e) {
					if (e instanceof ResourceAccessException) {
						Display.getDefault().asyncExec(new Runnable() {
							@Override
							public void run() {
								CustomMessageDialog.openError(Display.getDefault().getActiveShell(), "Error",
										"Server not reachable");
							}
						});
					}
					LOGGER.error(e.getMessage());
				}

				monitor.worked(70);
				return Status.OK_STATUS;
			}
		};
		adminHistoryJob.setUser(true);
		adminHistoryJob.schedule();
	};
}

