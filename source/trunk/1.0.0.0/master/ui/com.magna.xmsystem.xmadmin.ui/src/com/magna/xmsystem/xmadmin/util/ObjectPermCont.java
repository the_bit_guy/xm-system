package com.magna.xmsystem.xmadmin.util;

import com.magna.xmbackend.entities.PermissionTbl;

public class ObjectPermCont {
	private PermissionTbl createPermission;
	private PermissionTbl changePermission;
	private PermissionTbl deletePermission;
	private PermissionTbl activatePermission;
	private PermissionTbl viewInactivePermission;
	
	public ObjectPermCont() {
	}

	/**
	 * @return the createPermission
	 */
	public PermissionTbl getCreatePermission() {
		return createPermission;
	}

	/**
	 * @param createPermission
	 *            the createPermission to set
	 */
	public void setCreatePermission(PermissionTbl createPermission) {
		this.createPermission = createPermission;
	}

	/**
	 * @return the changePermission
	 */
	public PermissionTbl getChangePermission() {
		return changePermission;
	}

	/**
	 * @param changePermission
	 *            the changePermission to set
	 */
	public void setChangePermission(PermissionTbl changePermission) {
		this.changePermission = changePermission;
	}

	/**
	 * @return the deletePermission
	 */
	public PermissionTbl getDeletePermission() {
		return deletePermission;
	}

	/**
	 * @param deletePermission
	 *            the deletePermission to set
	 */
	public void setDeletePermission(PermissionTbl deletePermission) {
		this.deletePermission = deletePermission;
	}

	/**
	 * @return the activatePermission
	 */
	public PermissionTbl getActivatePermission() {
		return activatePermission;
	}

	/**
	 * @param activatePermission
	 *            the activatePermission to set
	 */
	public void setActivatePermission(PermissionTbl activatePermission) {
		this.activatePermission = activatePermission;
	}

	/**
	 * @return the viewInactivePermission
	 */
	public PermissionTbl getViewInactivePermission() {
		return viewInactivePermission;
	}

	/**
	 * @param viewInactivePermission
	 *            the viewInactivePermission to set
	 */
	public void setViewInactivePermission(PermissionTbl viewInactivePermission) {
		this.viewInactivePermission = viewInactivePermission;
	}
}
