package com.magna.xmsystem.xmadmin.ui.parts.livemsgconfig;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import com.magna.xmbackend.vo.liveMessage.LiveMessageConfigCreateRequest;
import com.magna.xmsystem.ui.controls.widgets.CustomMessageDialog;
import com.magna.xmsystem.xmadmin.message.Message;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.AdminTreeviewer;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.livemsgconfig.LiveMsgToPattern;
import com.magna.xmsystem.xmadmin.util.XMAdminUtil;

/**
 * The Class LiveMessageSaveObjHelper.
 * 
 * @author archita.patel
 */
public class LiveMessageSaveObjHelper {

	/** The messages. */
	private Message messages;

	public LiveMessageSaveObjHelper(Message messages) {
		this.messages = messages;
	}

	/**
	 * Gets the to objects map.
	 *
	 * @param objList
	 *            the obj list
	 * @return the to objects map
	 */
	protected Map<String, List<Map<String, String>>> getToObjectsMap(List<String> objList) {
		final AdminTreeviewer adminTree = XMAdminUtil.getInstance().getAdminTree();
		Map<String, List<Map<String, String>>> liveMsgToObjsMap = new HashMap<String, List<Map<String, String>>>();
		try {
			for (String string : objList) {
				int endIndex = string.indexOf('[');
				String pattern = string.substring(0, endIndex);
				if (pattern.equals(LiveMsgToPattern.U.name())) {
					List<Map<String, String>> objectList = new ArrayList<>();
					String wholeString = string;
					int index = wholeString.indexOf(LiveMsgToPattern.U.name() + "[");
					int startIndex = wholeString.indexOf('[', index);
					endIndex = wholeString.indexOf(']', startIndex);
					String centreString = wholeString.substring(startIndex + 1, endIndex);
					List<String> nameList = Arrays.asList(centreString.split(","));
					if (!centreString.isEmpty() && !(LiveMsgToPattern.U.getSize() > nameList.size())) {
						Map<String, String> map = new HashMap<>();
						for (String name : nameList) {
							if (!map.containsValue(name)) {
								map = new HashMap<>();
								map.put(LiveMsgToPattern.U.name(), name);
								objectList.add(map);
								liveMsgToObjsMap.put(LiveMsgToPattern.U.name(), objectList);
							}
						}
					} else {
						return null;
					}

				} else if (pattern.equals(LiveMsgToPattern.S.name())) {
					List<Map<String, String>> objectList = new ArrayList<>();
					String wholeString = string;
					int index = wholeString.indexOf(LiveMsgToPattern.S.name() + "[");
					int startIndex = wholeString.indexOf('[', index);
					endIndex = wholeString.indexOf(']', startIndex);
					String centreString = wholeString.substring(startIndex + 1, endIndex);
					List<String> nameList = Arrays.asList(centreString.split(","));
					if (!centreString.isEmpty() && !(LiveMsgToPattern.S.getSize() > nameList.size())) {
						Map<String, String> map = new HashMap<>();
						for (String name : nameList) {
							if (!map.containsValue(name)) {
								map = new HashMap<>();
								map.put(LiveMsgToPattern.S.name(), name);
								objectList.add(map);
								liveMsgToObjsMap.put(LiveMsgToPattern.S.name(), objectList);
							}
						}
					} else {
						return null;
					}

				}  else if (pattern.equals(LiveMsgToPattern.P.name())) {
					List<Map<String, String>> objectList = new ArrayList<>();
					String wholeString = string;
					int index = wholeString.indexOf(LiveMsgToPattern.P.name() + "[");
					int startIndex = wholeString.indexOf('[', index);
					endIndex = wholeString.indexOf(']', startIndex);
					String centreString = wholeString.substring(startIndex + 1, endIndex);
					List<String> nameList = Arrays.asList(centreString.split(","));
					if (!centreString.isEmpty() && !(LiveMsgToPattern.P.getSize() > nameList.size())) {
						Map<String, String> map = new HashMap<>();
						for (String name : nameList) {
							if (!map.containsValue(name)) {
								map = new HashMap<>();
								map.put(LiveMsgToPattern.P.name(), name);
								objectList.add(map);
								liveMsgToObjsMap.put(LiveMsgToPattern.P.name(), objectList);
							}
						}
					} else {
						return null;
					}
				} else if (pattern.equals(LiveMsgToPattern.S_A.name())) {
					List<Map<String, String>> objectList = new ArrayList<>();
					String wholeString = string;
					int index = wholeString.indexOf(LiveMsgToPattern.S_A.name() + "[");
					int startIndex = wholeString.indexOf('[', index);
					endIndex = wholeString.indexOf(']', startIndex);
					String centreString = wholeString.substring(startIndex + 1, endIndex);
					List<String> siteAdminAreaNameList = Arrays.asList(centreString.split(","));
					Map<String, String> map = new HashMap<>();
					for (String projectUserName : siteAdminAreaNameList) {
						List<String> nameList = Arrays.asList(projectUserName.split("/"));
						if (!(LiveMsgToPattern.S_A.getSize() < nameList.size())) {
							String siteName = nameList.get(0);
							String adminAreaName = nameList.get(1);
							if (!(map.containsValue(siteName) && map.containsValue(adminAreaName))) {
								map = new HashMap<>();
								map.put(LiveMsgToPattern.S.name(), siteName);
								map.put(LiveMsgToPattern.A.name(), adminAreaName);
								objectList.add(map);
								liveMsgToObjsMap.put(LiveMsgToPattern.S_A.name(), objectList);
							}
						} else {
							CustomMessageDialog.openError(adminTree.getControl().getShell(),
									messages.liveMsgInvalidRemoveErrorTilte,
									messages.liveMsgInvalidRemoveErrorMsg + " \'" + nameList.toString() + "\'");
						}
					}

				} else if (pattern.equals(LiveMsgToPattern.S_A_P.name())) {
					List<Map<String, String>> objectList = new ArrayList<>();
					String wholeString = string;
					int index = wholeString.indexOf(LiveMsgToPattern.S_A_P.name() + "[");
					int startIndex = wholeString.indexOf('[', index);
					endIndex = wholeString.indexOf(']', startIndex);
					String centreString = wholeString.substring(startIndex + 1, endIndex);
					List<String> projectUserNameList = Arrays.asList(centreString.split(","));
					Map<String, String> map = new HashMap<>();
					for (String projectUserName : projectUserNameList) {
						List<String> nameList = Arrays.asList(projectUserName.split("/"));
						if (!(LiveMsgToPattern.S_A_P.getSize() < nameList.size())) {
							String siteName = nameList.get(0);
							String adminAreaName = nameList.get(1);
							String projectName = nameList.get(2);
							if (!(map.containsValue(siteName) && map.containsValue(adminAreaName)
									&& map.containsValue(projectName))) {
								map = new HashMap<>();
								map.put(LiveMsgToPattern.S.name(), siteName);
								map.put(LiveMsgToPattern.A.name(), adminAreaName);
								map.put(LiveMsgToPattern.P.name(), projectName);
								objectList.add(map);
								liveMsgToObjsMap.put(LiveMsgToPattern.S_A_P.name(), objectList);
							}
						} else {
							CustomMessageDialog.openError(adminTree.getControl().getShell(),
									messages.liveMsgInvalidRemoveErrorTilte,
									messages.liveMsgInvalidRemoveErrorMsg + " \'" + nameList.toString() + "\'");
						}
					}
				}else{
					CustomMessageDialog.openError(adminTree.getControl().getShell(),
							messages.liveMsgInvalidRemoveErrorTilte,
							messages.liveMsgInvalidRemoveErrorMsg);
				}
			}
		} catch (Exception e) {
			return null;
		}

		return liveMsgToObjsMap;
	}

	/**
	 * Map live msg config create req.
	 *
	 * @param objectIdMap
	 *            the object id map
	 * @param toObjectsMap
	 *            the to objects map
	 * @param messages
	 * @return the Set
	 */
	protected Set<LiveMessageConfigCreateRequest> mapLiveMsgConfigCreateReq(Map<String, String> objectIdMap,
			Map<String, List<Map<String, String>>> toObjectsMap) {

		Set<LiveMessageConfigCreateRequest> configCreateReqList = new HashSet<>();
		LiveMessageConfigCreateRequest liveMsgConfigCreateReq;

		for (Entry<String, List<Map<String, String>>> entry : toObjectsMap.entrySet()) {
			if (entry.getKey().equals(LiveMsgToPattern.U.name())) {
				List<Map<String, String>> userList = entry.getValue();
				for (Map<String, String> map : userList) {
					liveMsgConfigCreateReq = new LiveMessageConfigCreateRequest();
					String name = map.get(LiveMsgToPattern.U.name());
					String id = objectIdMap.get(name);
					if (validateObjectName(name, id)) {
						liveMsgConfigCreateReq.setUserId(id);
					} else {
						return null;
					}
					configCreateReqList.add(liveMsgConfigCreateReq);
				}
			} else if (entry.getKey().equals(LiveMsgToPattern.S.name())) {
				List<Map<String, String>> siteList = entry.getValue();
				for (Map<String, String> map : siteList) {
					liveMsgConfigCreateReq = new LiveMessageConfigCreateRequest();
					String name = map.get(LiveMsgToPattern.S.name());
					String id = objectIdMap.get(name);
					if (validateObjectName(name, id)) {
						liveMsgConfigCreateReq.setSiteId(id);
					} else {
						return null;
					}
					configCreateReqList.add(liveMsgConfigCreateReq);
				}
			}  else if (entry.getKey().equals(LiveMsgToPattern.P.name())) {
				List<Map<String, String>> projectList = entry.getValue();
				for (Map<String, String> map : projectList) {
					liveMsgConfigCreateReq = new LiveMessageConfigCreateRequest();
					String name = map.get(LiveMsgToPattern.P.name());
					String id = objectIdMap.get(name);
					if (validateObjectName(name, id)) {
						liveMsgConfigCreateReq.setProjectId(id);
					} else {
						return null;
					}
					configCreateReqList.add(liveMsgConfigCreateReq);
				}
			} else if (entry.getKey().equals(LiveMsgToPattern.S_A.name())) {
				List<Map<String, String>> projectUserList = entry.getValue();
				for (Map<String, String> map : projectUserList) {
					liveMsgConfigCreateReq = new LiveMessageConfigCreateRequest();
					String name = map.get(LiveMsgToPattern.S.name());
					String id = objectIdMap.get(name);
					if (validateObjectName(name, id)) {
						liveMsgConfigCreateReq.setSiteId(id);
					} else {
						return null;
					}
					name = map.get(LiveMsgToPattern.A.name());
					id = objectIdMap.get(name);
					if (validateObjectName(name, id)) {
						liveMsgConfigCreateReq.setAdminAreaId(id);
					} else {
						return null;
					}
					configCreateReqList.add(liveMsgConfigCreateReq);
				}
			} else if (entry.getKey().equals(LiveMsgToPattern.S_A_P.name())) {
				List<Map<String, String>> projectUserList = entry.getValue();
				for (Map<String, String> map : projectUserList) {
					liveMsgConfigCreateReq = new LiveMessageConfigCreateRequest();
					String name = map.get(LiveMsgToPattern.S.name());
					String id = objectIdMap.get(name);
					if (validateObjectName(name, id)) {
						liveMsgConfigCreateReq.setSiteId(id);
					} else {
						return null;
					}
					name = map.get(LiveMsgToPattern.A.name());
					id = objectIdMap.get(name);
					if (validateObjectName(name, id)) {
						liveMsgConfigCreateReq.setAdminAreaId(id);
					} else {
						return null;
					}
					name = map.get(LiveMsgToPattern.P.name());
					id = objectIdMap.get(name);
					if (validateObjectName(name, id)) {
						liveMsgConfigCreateReq.setProjectId(id);
					} else {
						return null;
					}
					configCreateReqList.add(liveMsgConfigCreateReq);
				}
			}

		}
		return configCreateReqList;

	}

	private boolean validateObjectName(String name, String id) {
		final AdminTreeviewer adminTree = XMAdminUtil.getInstance().getAdminTree();
		if (id == null) {
			CustomMessageDialog.openError(adminTree.getControl().getShell(), messages.liveMsgInvalidObjNameErrorTitle,
					messages.liveMsgInvalidObjNameErrorMsg + " \'" + name + "\'");
			return false;
		} else {
			return true;
		}
	}
}
