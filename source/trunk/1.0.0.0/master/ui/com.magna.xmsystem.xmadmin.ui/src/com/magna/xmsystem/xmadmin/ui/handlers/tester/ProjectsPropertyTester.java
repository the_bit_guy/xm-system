package com.magna.xmsystem.xmadmin.ui.handlers.tester;

import org.eclipse.core.expressions.PropertyTester;

/**
 * Class for Projects property tester
 * 
 * @author subash.janarthanan
 *
 */
public class ProjectsPropertyTester extends PropertyTester{
	
	public ProjectsPropertyTester() {
	}
	

	/* (non-Javadoc)
	 * @see org.eclipse.core.expressions.IPropertyTester#test(java.lang.Object, java.lang.String, java.lang.Object[], java.lang.Object)
	 */
	@Override
	public boolean test(Object receiver, String property, Object[] arg, Object expectedValue) {
		return true;
	}

}
