package com.magna.xmsystem.xmadmin.ui.Application.dialogs;

import org.eclipse.jface.viewers.ColumnLabelProvider;

import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.baseapplicationmodel.BaseApplication;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.projectapplicationmodel.ProjectApplication;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.userapplicationmodel.UserApplication;


/**
 * The Class BrowserApplicationLabelProvider.
 */
public class BrowserApplicationLabelProvider extends ColumnLabelProvider {
	
	/* (non-Javadoc)
	 * @see org.eclipse.jface.viewers.ColumnLabelProvider#getText(java.lang.Object)
	 */
	@Override
	public String getText(Object element) {
		if (element instanceof UserApplication) {
			return ((UserApplication) element).getName();
		} else if (element instanceof BaseApplication) {
			return ((BaseApplication) element).getName();
		} else if (element instanceof ProjectApplication) {
			return ((ProjectApplication) element).getName();
		} 
		return element == null ? "" : element.toString();//$NON-NLS-1$
	}

}
