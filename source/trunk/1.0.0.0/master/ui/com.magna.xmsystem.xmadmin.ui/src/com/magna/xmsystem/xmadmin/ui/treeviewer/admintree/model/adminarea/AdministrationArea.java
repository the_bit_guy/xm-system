package com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.adminarea;

import java.beans.PropertyChangeEvent;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import org.eclipse.core.runtime.Assert;

import com.magna.xmsystem.dependencies.utils.LANG_ENUM;
import com.magna.xmsystem.dependencies.utils.XMSystemUtil;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.BeanModel;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.projectapplicationmodel.ProjectAppAdminAreaChild;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.startapplicationmodel.StartAppAdminAreas;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.userapplicationmodel.UserAppAdminAreas;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.icon.Icon;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.roles.RoleScopeObjectChild;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.projects.ProjectAdminAreaChild;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.projects.ProjectUserAdminAreaChild;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.refs.RelationObj;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.sitemodel.SiteAdministrationChild;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.users.UserAdminAreaChild;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.users.UserProjectAdminAreaChild;
import com.magna.xmsystem.xmadmin.util.CommonConstants;

/**
 * The Class AdministrationArea.
 * 
 * @author shashwat.anand
 * 
 */
public class AdministrationArea extends BeanModel implements IAdminTreeChild, Cloneable {

	/** The Constant REMARK_LIMIT. */
	public static final int REMARK_LIMIT = 1500;

	/** The Constant DESC_LIMIT. */
	public static final int DESC_LIMIT = 240;

	/** The Constant NAME_LIMIT. */
	public static final int NAME_LIMIT = 30;

	/** The Constant MAX_TIME. */
	public static final int MAX_TIME = 7200;
	
	/** The Constant SPINNER_LIMIT_TIME. */
	public static final int SPINNER_LIMIT_TIME = 4;

	/** The Constant PROPERTY_ADMINAREAID. */
	public static final String PROPERTY_ADMINAREAID = "adminAreaId"; //$NON-NLS-1$

	/** The Constant PROPERTY_ADMINAREANAME. */
	public static final String PROPERTY_ADMINAREANAME = "name"; //$NON-NLS-1$

	/** The Constant PROPERTY_HOTLINE_CONTACT_NUMBER. */
	public static final String PROPERTY_HOTLINE_CONTACT_NUMBER = "hotlineContactNumber"; //$NON-NLS-1$

	/** The Constant PROPERTY_HOTLINE_CONTACT_EMAIL. */
	public static final String PROPERTY_HOTLINE_CONTACT_EMAIL = "hotlineContactEmail"; //$NON-NLS-1$

	/** The Constant PROPERTY_SINGLETON_APP_TIMEOUT. */
	public static final String PROPERTY_SINGLETON_APP_TIMEOUT = "singletonAppTimeout"; //$NON-NLS-1$

	/** The Constant PROPERTY_ACTIVE. */
	public static final String PROPERTY_ACTIVE = "active"; //$NON-NLS-1$

	/** The Constant PROPERTY_DESC_MAP. */
	public static final String PROPERTY_DESC_MAP = "descriptionMap"; //$NON-NLS-1$

	/** The Constant PROPERTY_REMARKS_MAP. */
	public static final String PROPERTY_REMARKS_MAP = "remarksMap"; //$NON-NLS-1$

	/** The Constant PROPERTY_OPERATION_MODE. */
	public static final String PROPERTY_OPERATION_MODE = "operationMode"; //$NON-NLS-1$

	/** The Constant PROPERTY_ICON. */
	public static final String PROPERTY_ICON = "icon"; //$NON-NLS-1$

	/** The admin area id. */
	private String adminAreaId;

	/** The name. */
	private String name;

	/** The active. */
	private boolean active;

	/** The description map. */
	private Map<LANG_ENUM, String> descriptionMap;

	/** The contact. */
	private String hotlineContactNumber;

	/** The email. */
	private String hotlineContactEmail;

	/** The spinner. */
	private Long singletonAppTimeout;

	/** The remarks map. */
	private Map<LANG_ENUM, String> remarksMap;

	/** The operation mode. */
	private int operationMode;

	/** The icon. */
	private Icon icon;

	/** The admin area children. */
	final private Map<String, IAdminTreeChild> adminAreaChildren;

	/** The rel id. */
	private String relationId;

	/** The set related site name. */
	private String setRelatedSiteName;

	/**
	 * Constructor for AdministrationArea Class.
	 *
	 * @param AdminAreaId
	 *            {@link String}
	 * @param name
	 *            {@link String}
	 * @param isActive
	 *            {@link boolean}
	 * @param icon
	 *            {@link Icon}
	 * @param hotlineContactNumber
	 *            {@link String}
	 * @param hotlineContactEmail
	 *            {@link String}
	 * @param singletonAppTimeout
	 *            {@link Long}
	 * @param operationMode
	 *            {@link int}
	 */
	public AdministrationArea(final String AdminAreaId, final String name, final boolean isActive, final Icon icon,
			final String hotlineContactNumber, final String hotlineContactEmail, final Long singletonAppTimeout,
			final int operationMode) {
		this(AdminAreaId, name, isActive, new HashMap<>(), hotlineContactNumber, hotlineContactEmail,
				singletonAppTimeout, new HashMap<>(), icon, operationMode);

	}

	/**
	 * Constructor for AdministrationArea Class.
	 *
	 * @param adminAreaId
	 *            {@link String}
	 * @param name
	 *            {@link String}
	 * @param isActive
	 *            {@link boolean}
	 * @param descriptionMap
	 *            {@link Map<LANG_ENUM,String>}
	 * @param hotlineContactNumber
	 *            {@link String}
	 * @param hotlineContactEmail
	 *            {@link String}
	 * @param singletonAppTimeout
	 *            {@link Long}
	 * @param remarksMap
	 *            {@link Map<LANG_ENUM,String>}
	 * @param icon
	 *            {@link Icon}
	 * @param operationMode
	 *            {@link int}
	 */
	public AdministrationArea(final String adminAreaId, final String name, final boolean isActive,
			final Map<LANG_ENUM, String> descriptionMap, final String hotlineContactNumber,
			final String hotlineContactEmail, final Long singletonAppTimeout, final Map<LANG_ENUM, String> remarksMap,
			final Icon icon, final int operationMode) {
		super();
		this.adminAreaId = adminAreaId;
		this.name = name;
		this.active = isActive;
		this.descriptionMap = descriptionMap;
		this.hotlineContactNumber = hotlineContactNumber;
		this.hotlineContactEmail = hotlineContactEmail;
		this.singletonAppTimeout = singletonAppTimeout;
		this.remarksMap = remarksMap;
		this.icon = icon;
		this.operationMode = operationMode;
		this.adminAreaChildren = new LinkedHashMap<>();
		addFixedChildren();
	}

	/**
	 * Method for Adds the fixed children.
	 */
	public void addFixedChildren() {
		adminAreaChildren.put(AdminAreaProjects.class.getName(), new AdminAreaProjects(this));
		adminAreaChildren.put(AdminAreaUserApplications.class.getName(), new AdminAreaUserApplications(this));
		adminAreaChildren.put(AdminAreaStartApplications.class.getName(), new AdminAreaStartApplications(this));
	}

	/**
	 * Gets the administration area id.
	 *
	 * @return the administration area id
	 */
	public String getAdministrationAreaId() {
		return adminAreaId;
	}

	/**
	 * Sets the administration area id.
	 *
	 * @param adminAreaID
	 *            the new administration area id
	 */
	public void setAdministrationAreaId(final String adminAreaID) {
		this.propertyChangeSupport.firePropertyChange(PROPERTY_ADMINAREAID, this.adminAreaId,
				this.adminAreaId = adminAreaID);
	}

	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Sets the name.
	 *
	 * @param name
	 *            the new name
	 */
	public void setName(final String name) {
		if (name == null) {
			throw new IllegalArgumentException();
		}
		this.propertyChangeSupport.firePropertyChange(PROPERTY_ADMINAREANAME, this.name, this.name = name.trim());
	}

	/**
	 * Gets the site admin areas collection.
	 *
	 * @return the site admin areas collection
	 */
	public Collection<IAdminTreeChild> getAdminAreasCollection() {
		return this.adminAreaChildren.values();
	}

	/**
	 * Gets the admin area children.
	 *
	 * @return the admin area children
	 */
	public Map<String, IAdminTreeChild> getAdminAreaChildren() {
		return adminAreaChildren;
	}

	/**
	 * Checks if is active.
	 *
	 * @return true, if is active
	 */
	public boolean isActive() {
		return active;
	}

	/**
	 * Sets the active.
	 *
	 * @param isActive
	 *            the new active
	 */
	public void setActive(final boolean isActive) {
		this.propertyChangeSupport.firePropertyChange(PROPERTY_ACTIVE, this.active, this.active = isActive);
	}

	/**
	 * Gets the hotline contact number.
	 *
	 * @return the hotline contact number
	 */
	public String getHotlineContactNumber() {
		return hotlineContactNumber;
	}

	/**
	 * Sets the hotline contact number.
	 *
	 * @param hotlineContactNumber
	 *            the new hotline contact number
	 */
	public void setHotlineContactNumber(String hotlineContactNumber) {
		if (hotlineContactNumber == null) {
			throw new IllegalArgumentException();
		}
		this.propertyChangeSupport.firePropertyChange(PROPERTY_HOTLINE_CONTACT_NUMBER, this.hotlineContactNumber,
				this.hotlineContactNumber = hotlineContactNumber.trim());
	}

	/**
	 * Gets the hotline contact email.
	 *
	 * @return the hotline contact email
	 */
	public String getHotlineContactEmail() {
		return hotlineContactEmail;
	}

	/**
	 * Sets the hotline contact email.
	 *
	 * @param hotlineContactEmail
	 *            the new hotline contact email
	 */
	public void setHotlineContactEmail(String hotlineContactEmail) {
		if (hotlineContactEmail == null) {
			throw new IllegalArgumentException();
		}
		this.propertyChangeSupport.firePropertyChange(PROPERTY_HOTLINE_CONTACT_EMAIL, this.hotlineContactEmail,
				this.hotlineContactEmail = hotlineContactEmail.trim());
	}

	/**
	 * Gets the singleton app timeout.
	 *
	 * @return the singleton app timeout
	 */
	public Long getSingletonAppTimeout() {
		return singletonAppTimeout;
	}

	/**
	 * Sets the singleton app timeout.
	 *
	 * @param singletonAppTimeout
	 *            the new singleton app timeout
	 */
	public void setSingletonAppTimeout(Long singletonAppTimeout) {
		if (singletonAppTimeout == null) {
			throw new IllegalArgumentException();
		}
		this.propertyChangeSupport.firePropertyChange(PROPERTY_SINGLETON_APP_TIMEOUT, this.singletonAppTimeout,
				this.singletonAppTimeout = singletonAppTimeout);
	}

	/**
	 * Gets the description map.
	 *
	 * @return the description map
	 */
	public Map<LANG_ENUM, String> getDescriptionMap() {
		return descriptionMap;
	}

	/**
	 * Sets the description map.
	 *
	 * @param descriptionMap
	 *            the description map
	 */
	public void setDescriptionMap(final Map<LANG_ENUM, String> descriptionMap) {
		if (descriptionMap == null) {
			throw new IllegalArgumentException();
		}
		this.propertyChangeSupport.firePropertyChange(PROPERTY_DESC_MAP, this.descriptionMap,
				this.descriptionMap = descriptionMap);
	}

	/**
	 * Gets the description.
	 *
	 * @param lang
	 *            the lang
	 * @return the description
	 */
	public String getDescription(final LANG_ENUM lang) {
		return this.descriptionMap.get(lang);
	}

	/**
	 * Sets the description.
	 *
	 * @param lang
	 *            the lang
	 * @param description
	 *            the description
	 */
	public void setDescription(final LANG_ENUM lang, final String description) {
		if (lang == null || description == null) {
			throw new IllegalArgumentException();
		}
		this.propertyChangeSupport.firePropertyChange(PROPERTY_DESC_MAP, this.descriptionMap,
				this.descriptionMap.put(lang, description.trim()));
	}

	/**
	 * Gets the remarks map.
	 *
	 * @return the remarks map
	 */
	public Map<LANG_ENUM, String> getRemarksMap() {
		return remarksMap;
	}

	/**
	 * Sets the remarks map.
	 *
	 * @param remarksMap
	 *            the remarks map
	 */
	public void setRemarksMap(final Map<LANG_ENUM, String> remarksMap) {
		if (remarksMap == null) {
			throw new IllegalArgumentException();
		}
		this.propertyChangeSupport.firePropertyChange(PROPERTY_REMARKS_MAP, this.remarksMap,
				this.remarksMap = remarksMap);
	}

	/**
	 * Gets the remarks.
	 *
	 * @param lang
	 *            the lang
	 * @return the remarks
	 */
	public String getRemarks(final LANG_ENUM lang) {
		return this.remarksMap.get(lang);
	}

	/**
	 * Sets the remarks.
	 *
	 * @param lang
	 *            the lang
	 * @param remarks
	 *            the remarks
	 */
	public void setRemarks(final LANG_ENUM lang, final String remarks) {
		if (lang == null || remarks == null) {
			throw new IllegalArgumentException();
		}
		this.propertyChangeSupport.firePropertyChange(PROPERTY_REMARKS_MAP, this.remarksMap,
				this.remarksMap.put(lang, remarks));
	}

	/**
	 * Gets the operation mode.
	 *
	 * @return the operation mode
	 */
	public int getOperationMode() {
		return operationMode;
	}

	/**
	 * Sets the operation mode.
	 *
	 * @param operationMode
	 *            the new operation mode
	 */
	public void setOperationMode(final int operationMode) {
		this.propertyChangeSupport.firePropertyChange(PROPERTY_OPERATION_MODE, this.operationMode,
				this.operationMode = operationMode);
	}

	/**
	 * Gets the icon.
	 *
	 * @return the icon
	 */
	public Icon getIcon() {
		return icon;
	}

	/**
	 * Sets the icon.
	 *
	 * @param icon
	 *            the new icon
	 */
	public void setIcon(final Icon icon) {
		if (icon == null) {
			throw new IllegalArgumentException();
		}
		this.propertyChangeSupport.firePropertyChange(PROPERTY_ICON, this.icon, this.icon = icon);
	}

	/**
	 * Gets the relation id.
	 *
	 * @return the relationId
	 */
	public String getRelationId() {
		return relationId;
	}

	/**
	 * Sets the relation id.
	 *
	 * @param relationId            the relationId to set
	 */
	public void setRelationId(String relationId) {
		this.relationId = relationId;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.BeanModel#
	 * propertyChange(java.beans.PropertyChangeEvent)
	 */
	@Override
	public void propertyChange(final PropertyChangeEvent event) {
		this.propertyChangeSupport.firePropertyChange(event.getPropertyName(), event.getOldValue(),
				event.getNewValue());
	}

	/**
	 * Add.
	 *
	 * @param id
	 *            the id
	 * @param child
	 *            the child
	 * @return the i admin tree child
	 */
	public IAdminTreeChild add(final String id, final IAdminTreeChild child) {
		child.setParent(this);
		return this.adminAreaChildren.put(id, child);
	}

	/**
	 * Remove.
	 *
	 * @param id
	 *            the id
	 * @return the i admin tree child
	 */
	public IAdminTreeChild remove(final String id) {
		return this.adminAreaChildren.remove(id);
	}

	/**
	 * Remove all.
	 */
	public void removeAll() {
		this.adminAreaChildren.clear();
	}
	
	/**
	 * Sets the related site name.
	 *
	 * @param siteName the new related site name
	 */
	public void setRelatedSiteName(final String siteName) {
		this.setRelatedSiteName= siteName;
	}
	
	/**
	 * Gets the related site name.
	 *
	 * @return the related site name
	 */
	public String getRelatedSiteName() {
		return this.setRelatedSiteName;
	}

	/**
	 * Deep copy admin area.
	 *
	 * @param update
	 *            the update
	 * @param updateThisObject
	 *            the update this object
	 * @return the administration area
	 */
	public AdministrationArea deepCopyAdminArea(boolean update, AdministrationArea updateThisObject) {

		AdministrationArea clonedAdminArea = null;
		try {
			String currentAdminAreaId = XMSystemUtil.isEmpty(this.getAdministrationAreaId()) ? CommonConstants.EMPTY_STR
					: this.getAdministrationAreaId();
			String currentName = XMSystemUtil.isEmpty(this.getName()) ? CommonConstants.EMPTY_STR : this.getName();
			String currentContact = XMSystemUtil.isEmpty(this.getHotlineContactNumber()) ? CommonConstants.EMPTY_STR
					: this.getHotlineContactNumber();
			String currentEmail = XMSystemUtil.isEmpty(this.getHotlineContactEmail()) ? CommonConstants.EMPTY_STR
					: this.getHotlineContactEmail();

			// String currentSpinner = XMSystemUtil.isEmpty(this.getTime()) ?
			// null : this.getTime();
			Long currentSpinner = this.getSingletonAppTimeout() == null ? 0 : this.getSingletonAppTimeout();
			boolean currentIsActive = this.isActive();
			Map<LANG_ENUM, String> currentTranslationIdMap = new HashMap<>();
			Map<LANG_ENUM, String> currentDescriptionMap = new HashMap<>();
			Map<LANG_ENUM, String> currentRemarksMap = new HashMap<>();

			for (LANG_ENUM langEnum : LANG_ENUM.values()) {
				String translationId = this.getTranslationId(langEnum);
				currentTranslationIdMap.put(langEnum,
						XMSystemUtil.isEmpty(translationId) ? CommonConstants.EMPTY_STR : translationId);

				String description = this.getDescription(langEnum);
				currentDescriptionMap.put(langEnum,
						XMSystemUtil.isEmpty(description) ? CommonConstants.EMPTY_STR : description);

				String remarks = this.getRemarks(langEnum);
				currentRemarksMap.put(langEnum, XMSystemUtil.isEmpty(remarks) ? CommonConstants.EMPTY_STR : remarks);
			}

			//Icon currentIcon = this.getIcon() == null ? null : this.getIcon();
			Icon currentIcon = new Icon(this.getIcon().getIconId(), this.getIcon().getIconName(),
					this.getIcon().getIconPath(), this.getIcon().getIconType());
			if (update) {
				clonedAdminArea = updateThisObject;
			} else {
				clonedAdminArea = new AdministrationArea(currentAdminAreaId, currentName, currentIsActive, currentIcon,
						currentEmail, currentContact, currentSpinner, CommonConstants.OPERATIONMODE.VIEW);
			}
			clonedAdminArea.setAdministrationAreaId(currentAdminAreaId);
			clonedAdminArea.setName(currentName);
			clonedAdminArea.setActive(currentIsActive);
			clonedAdminArea.setHotlineContactNumber(currentContact);
			clonedAdminArea.setHotlineContactEmail(currentEmail);
			clonedAdminArea.setSingletonAppTimeout(currentSpinner);
			clonedAdminArea.setIcon(currentIcon);
			clonedAdminArea.setTranslationIdMap(currentTranslationIdMap);
			clonedAdminArea.setDescriptionMap(currentDescriptionMap);
			clonedAdminArea.setRemarksMap(currentRemarksMap);
			return clonedAdminArea;
		} catch (Exception e) {
			return null;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.BeanModel#
	 * getAdapterRef(java.lang.Class,
	 * com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild)
	 */
	@Override
	public <T> RelationObj getAdapter(Class<T> adapterType, IAdminTreeChild parent, String relationId,
			boolean relationStatus) {
		Assert.isNotNull(adapterType);
		Assert.isNotNull(this);
		if (adapterType == SiteAdministrationChild.class) {
			SiteAdministrationChild siteAdminChild = new SiteAdministrationChild(parent);
			RelationObj relationObj = new RelationObj(relationId, this, siteAdminChild, relationStatus, null);
			return relationObj;
		} else if (adapterType == ProjectAdminAreaChild.class) {
			ProjectAdminAreaChild projectAdminAreaChild = new ProjectAdminAreaChild(parent);
			RelationObj relationObj = new RelationObj(relationId, this, projectAdminAreaChild, relationStatus, null);
			return relationObj;
		} else if (adapterType == ProjectUserAdminAreaChild.class) {
			ProjectUserAdminAreaChild projectUserAdminAreaChild = new ProjectUserAdminAreaChild(parent);
			RelationObj relationObj = new RelationObj(relationId, this, projectUserAdminAreaChild, relationStatus, null);
			return relationObj;
		} else if (adapterType == ProjectAppAdminAreaChild.class) {
			ProjectAppAdminAreaChild projectAppAdminAreaChild = new ProjectAppAdminAreaChild(parent);
			RelationObj relationObj = new RelationObj(relationId, this, projectAppAdminAreaChild, relationStatus, null);
			return relationObj;
		} else if (adapterType == UserAdminAreaChild.class) {
			UserAdminAreaChild userAdminAreaChild = new UserAdminAreaChild(parent);
			RelationObj relationObj = new RelationObj(relationId, this, userAdminAreaChild, relationStatus, null);
			return relationObj;
		} else if (adapterType == UserProjectAdminAreaChild.class) {
			UserProjectAdminAreaChild userProjectAdminAreaChild = new UserProjectAdminAreaChild(parent);
			RelationObj relationObj = new RelationObj(relationId, this, userProjectAdminAreaChild, relationStatus, null);
			return relationObj;
		} else if (adapterType == UserAppAdminAreas.class) {
			UserAppAdminAreas userAppAdminAreas = new UserAppAdminAreas(parent);
			RelationObj relationObj = new RelationObj(relationId, this, userAppAdminAreas, relationStatus, null);
			return relationObj;
		} else if (adapterType == StartAppAdminAreas.class) {
			StartAppAdminAreas startAppAdminAreas = new StartAppAdminAreas(parent);
			RelationObj relationObj = new RelationObj(relationId, this, startAppAdminAreas, relationStatus, null);
			return relationObj;
		} else if (adapterType == RoleScopeObjectChild.class) {
			RoleScopeObjectChild roleScopeObjectChild = new RoleScopeObjectChild(parent);
			RelationObj scopeObjRel = new RelationObj(relationId, this, roleScopeObjectChild, relationStatus, null);
			return scopeObjRel;
		}

		return super.getAdapter(adapterType, parent, relationId, relationStatus);
	}
}
