package com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.baseapplicationmodel;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.stream.Collectors;

import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.projectapplicationmodel.ProjectApplication;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.refs.RelationObj;

/**
 * The Class BaseAppProjectAppModel.
 */
public class BaseAppProjectApplications implements IAdminTreeChild {

	/** The parent. */
	private IAdminTreeChild parent;

	/** The base app project app children. */
	private Map<String, IAdminTreeChild> baseAppProjectAppChildren;

	/**
	 * Instantiates a new base app project app model.
	 *
	 * @param parent the parent
	 */
	public BaseAppProjectApplications(IAdminTreeChild parent) {
		super();
		this.parent = parent;
		this.baseAppProjectAppChildren = new LinkedHashMap<>();
	}
	
	/**
	 * Adds the.
	 *
	 * @param baseAppProjectAppId the base app project app id
	 * @param child the child
	 * @return the i admin tree child
	 */
	public IAdminTreeChild add(final String baseAppProjectAppId, final IAdminTreeChild child) {
		child.setParent(this);
		IAdminTreeChild returnVal = this.baseAppProjectAppChildren.put(baseAppProjectAppId, child);
		if (child instanceof RelationObj && ((RelationObj) child).getRefObject() instanceof ProjectApplication) {
			sort();
		}
		return returnVal;
	}

	/**
	 * Removes the.
	 *
	 * @param baseAppProjectAppId the base app project app id
	 * @return the i admin tree child
	 */
	public IAdminTreeChild remove(final String baseAppProjectAppId) {
		return this.baseAppProjectAppChildren.remove(baseAppProjectAppId);
	}

	/**
	 * Gets the base app project app collection.
	 *
	 * @return the base app project app collection
	 */
	public Collection<IAdminTreeChild> getBaseAppProjectAppCollection() {
		return this.baseAppProjectAppChildren.values();
	}

	/**
	 * Gets the base app project app children.
	 *
	 * @return the base app project app children
	 */
	public Map<String, IAdminTreeChild> getBaseAppProjectAppChildren() {
		return baseAppProjectAppChildren;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild#
	 * getParent()
	 */
	@Override
	public IAdminTreeChild getParent() {
		return this.parent;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild#
	 * setParent(com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.
	 * IAdminTreeChild)
	 */
	@Override
	public void setParent(IAdminTreeChild parent) {
		this.parent = parent;

	}
	
	/**
	 * Sort.
	 */
	public void sort() {
		final LinkedHashMap<String, IAdminTreeChild> collect = this.baseAppProjectAppChildren.entrySet().stream().sorted(
				(e1, e2) -> ((ProjectApplication) e1.getValue()).getName().compareTo(((ProjectApplication) e2.getValue()).getName()))
				.collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e1, LinkedHashMap::new));
		this.baseAppProjectAppChildren = collect;
	}

}
