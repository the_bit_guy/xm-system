package com.magna.xmsystem.xmadmin.ui.parts.smtpconfig;

import org.eclipse.jface.layout.GridDataFactory;
import org.eclipse.jface.layout.GridLayoutFactory;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.smtpconfig.SmtpConfiguration;

// TODO: Auto-generated Javadoc
/**
 * The Class SmtpConfigCompositeUI.
 * 
 * @author archita.patel
 */
public class SmtpConfigCompositeUI extends Composite {

	/** Logger instance. */
	private static final Logger LOGGER = LoggerFactory.getLogger(SmtpConfigCompositeAction.class);

	/** The grp SMTP config. */
	protected Group grpSmtpConfig;

	/** The lbl smtp email. */
	protected Label lblSmtpEmail;
	
	/** The lbl password. */
	protected Label lblPassword;

	/** The lbl smtp port. */
	protected Label lblSmtpPort;

	/** The lbl smtp server name. */
	protected Label lblSmtpServerName;

	/** The txt smtp email. */
	protected Text txtSmtpEmail;
	
	/** The txt password. */
	protected Text txtPassword;

	/** The txt smtp port. */
	protected Text txtSmtpPort;

	/** The txt smtp server name. */
	protected Text txtSmtpServerName;

	/** The btn save. */
	protected Button btnSave;

	/** The btn cancel. */
	protected Button btnCancel;

	/** The lbl msg config. */
	protected Label lblMsgConfig;

	/**
	 * Instantiates a new smtp config composite UI.
	 *
	 * @param parent
	 *            the parent
	 * @param style
	 *            the style
	 */
	public SmtpConfigCompositeUI(Composite parent, int style) {
		super(parent, style);
		this.initGUI();
	}

	/**
	 * Inits the GUI.
	 */
	private void initGUI() {

		try {
			GridLayoutFactory.fillDefaults().applyTo(this);
			this.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));

			this.grpSmtpConfig = new Group(this, SWT.NONE);
			GridLayoutFactory.fillDefaults().numColumns(1).applyTo(this.grpSmtpConfig);
			GridDataFactory.fillDefaults().grab(true, true).span(SWT.FILL, SWT.FILL).applyTo(this.grpSmtpConfig);

			final Composite widgetContainer = new Composite(this.grpSmtpConfig, SWT.NONE);
			final GridLayout widgetContLayout = new GridLayout(2, false);
			widgetContLayout.horizontalSpacing = 10;

			widgetContainer.setLayout(widgetContLayout);
			widgetContainer.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));

			this.lblSmtpEmail = new Label(widgetContainer, SWT.NONE);
			this.txtSmtpEmail = new Text(widgetContainer, SWT.BORDER);

			GridDataFactory.fillDefaults().grab(true, false).span(1, 1).align(SWT.FILL, SWT.CENTER)
					.applyTo(this.txtSmtpEmail);
			
			this.lblPassword = new Label(widgetContainer, SWT.NONE);
			this.txtPassword = new Text(widgetContainer, SWT.PASSWORD|SWT.BORDER);

			GridDataFactory.fillDefaults().grab(true, false).span(1, 1).align(SWT.FILL, SWT.CENTER)
					.applyTo(this.txtPassword);

			this.lblSmtpPort = new Label(widgetContainer, SWT.NONE);
			this.txtSmtpPort = new Text(widgetContainer, SWT.BORDER);
			this.txtSmtpPort.setTextLimit(SmtpConfiguration.PORT_LIMIT);
			GridDataFactory.fillDefaults().grab(true, false).span(1, 1).align(SWT.FILL, SWT.CENTER)
					.applyTo(this.txtSmtpPort);

			this.lblSmtpServerName = new Label(widgetContainer, SWT.NONE);
			this.txtSmtpServerName = new Text(widgetContainer, SWT.BORDER);

			GridDataFactory.fillDefaults().grab(true, false).span(1, 1).align(SWT.FILL, SWT.CENTER)
					.applyTo(this.txtSmtpServerName);
			
			this.lblMsgConfig = new Label(widgetContainer, SWT.NONE);
			GridDataFactory.fillDefaults().grab(true, false).span(2, 1).align(SWT.FILL, SWT.CENTER)
			.applyTo(this.lblMsgConfig);

			final Composite buttonBarComp = new Composite(widgetContainer, SWT.NONE);
			final GridLayout btnBarCompLayout = new GridLayout(2, true);
			btnBarCompLayout.marginRight = 0;
			btnBarCompLayout.marginLeft = 0;
			btnBarCompLayout.marginTop = 0;
			btnBarCompLayout.marginBottom = 0;
			btnBarCompLayout.marginWidth = 0;
			buttonBarComp.setLayout(btnBarCompLayout);
			buttonBarComp.setLayoutData(new GridData(SWT.FILL, SWT.DEFAULT, true, false, 3, 1));
			createButtonBar(buttonBarComp);
		} catch (Exception e) {
			LOGGER.error("Unable to crete UI elements", e);
		}

	}

	/**
	 * Creates the button bar.
	 *
	 * @param buttonBarComp
	 *            the button bar comp
	 */
	private void createButtonBar(Composite buttonBarComp) {
		this.btnSave = new Button(buttonBarComp, SWT.NONE);
		this.btnSave.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, true, false));

		this.btnCancel = new Button(buttonBarComp, SWT.NONE);
		this.btnCancel.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, true, false));
	}

}
