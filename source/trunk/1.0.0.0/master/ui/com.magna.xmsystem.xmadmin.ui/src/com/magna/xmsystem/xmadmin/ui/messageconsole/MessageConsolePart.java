package com.magna.xmsystem.xmadmin.ui.messageconsole;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.eclipse.e4.core.contexts.ContextInjectionFactory;
import org.eclipse.e4.core.contexts.IEclipseContext;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

// TODO: Auto-generated Javadoc
/**
 * The Class MessageConsolePart.
 */
public class MessageConsolePart {

	/** The Constant LOGGER. */
	private static final Logger LOGGER = LoggerFactory.getLogger(MessageConsolePart.class);

	/** Member variable 'eclipse context' for {@link IEclipseContext}. */
	@Inject
	private IEclipseContext eclipseContext;

	/**
	 * Method for Post construct.
	 *
	 * @param parent
	 *            {@link Composite}
	 */
	@PostConstruct
	public void postConstruct(final Composite parent) {
		try {
			final GridLayout gridLayout = new GridLayout(1, false);
			gridLayout.marginHeight = 0;
			gridLayout.marginBottom = 0;
			gridLayout.marginLeft = 0;
			gridLayout.marginRight = 0;
			gridLayout.marginTop = 0;
			gridLayout.horizontalSpacing = 0;
			gridLayout.verticalSpacing = 0;
			gridLayout.marginWidth = 0;

			parent.setLayout(gridLayout);
			parent.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
			buildComponents(parent);

		} catch (Exception e) {
			LOGGER.error("Exception occured at creating UI compoents!", e); //$NON-NLS-1$
		}
	}

	/**
	 * Method for Builds the components.
	 *
	 * @param parent
	 *            {@link Composite}
	 */
	private void buildComponents(final Composite parent) {
		eclipseContext.set(Composite.class, parent);
		ContextInjectionFactory.make(MessageConsoleUI.class, eclipseContext);
	}
}
