package com.magna.xmsystem.xmadmin.ui.handlers.tester;

import org.eclipse.core.expressions.PropertyTester;

import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.projectapplicationmodel.ProjectApplication;

// TODO: Auto-generated Javadoc
/**
 * The Class ProjectAppPropertyTester.
 * 
 * @author archita.patel
 */
public class ProjectAppPropertyTester extends PropertyTester {

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.core.expressions.IPropertyTester#test(java.lang.Object,
	 * java.lang.String, java.lang.Object[], java.lang.Object)
	 */
	@Override
	public boolean test(Object receiver, String arg1, Object[] arg2, Object arg3) {
		if (ProjectApplication.class.getSimpleName().equals(receiver.getClass().getSimpleName())) {
			return true;
		} else {
			return false;
		}
	}
}
