package com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.roles.objRelationPermissions;

import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.roles.VoPermContainer;

/**
 * The Class ObjectRelationPermissions.
 */
public class ObjectRelationPermissions {

	/** The assign. */
	private boolean assign;
	
	/** The remove. */
	private boolean remove;
	
	/** The activate. */
	private boolean activate;
	
	/** The inactive assignment. */
	private boolean inactiveAssignment;

	/** Member variable 'permission name' for {@link String}. */
	private String permissionName;
	
	/** Member variable 'assign permission' for {@link VoPermContainer}. */
	private VoPermContainer assignPermission;
	
	/** Member variable 'remove permission' for {@link VoPermContainer}. */
	private VoPermContainer removePermission;
	
	
	/** Member variable 'activate permission' for {@link VoPermContainer}. */
	private VoPermContainer activatePermission;
	
	/** Member variable 'Inactive Assignment Permission' for {@link VoPermContainer}. */
	private VoPermContainer inactiveAssignmentPermission;

	/**
	 * @param assign
	 * @param remove
	 * @param activate
	 * @param inactiveAssignment
	 */
	public ObjectRelationPermissions(final String permissionName, final boolean assign, boolean remove, boolean activate, boolean inactiveAssignment) {
		super();
		this.permissionName = permissionName;
		this.assign = assign;
		this.remove = remove;
		this.activate = activate;
		this.inactiveAssignment = inactiveAssignment;
	}

	/**
	 * @return the permissionName
	 */
	public String getPermissionName() {
		return permissionName;
	}

	/**
	 * @param permissionName the permissionName to set
	 */
	public void setPermissionName(String permissionName) {
		this.permissionName = permissionName;
	}

	/**
	 * Checks if is assign.
	 *
	 * @return true, if is assign
	 */
	public boolean isAssign() {
		return assign;
	}

	/**
	 * Sets the assign.
	 *
	 * @param assign the new assign
	 */
	public void setAssign(boolean assign) {
		this.assign = assign;
	}

	/**
	 * Checks if is removes the.
	 *
	 * @return true, if is removes the
	 */
	public boolean isRemove() {
		return remove;
	}

	/**
	 * Sets the removes the.
	 *
	 * @param remove the new removes the
	 */
	public void setRemove(boolean remove) {
		this.remove = remove;
	}

	/**
	 * Checks if is activate.
	 *
	 * @return true, if is activate
	 */
	public boolean isActivate() {
		return activate;
	}

	/**
	 * Sets the activate.
	 *
	 * @param activate the new activate
	 */
	public void setActivate(boolean activate) {
		this.activate = activate;
	}

	/**
	 * Checks if is inactive assignment.
	 *
	 * @return true, if is inactive assignment
	 */
	public boolean isInactiveAssignment() {
		return inactiveAssignment;
	}

	/**
	 * Sets the inactive assignment.
	 *
	 * @param inactiveAssignment the new inactive assignment
	 */
	public void setInactiveAssignment(boolean inactiveAssignment) {
		this.inactiveAssignment = inactiveAssignment;
	}

	/**
	 * @return the assignPermission
	 */
	public VoPermContainer getAssignPermission() {
		return assignPermission;
	}

	/**
	 * @param assignPermission the assignPermission to set
	 */
	public void setAssignPermission(VoPermContainer assignPermission) {
		this.assignPermission = assignPermission;
	}

	/**
	 * @return the removePermission
	 */
	public VoPermContainer getRemovePermission() {
		return removePermission;
	}

	/**
	 * @param removePermission the removePermission to set
	 */
	public void setRemovePermission(VoPermContainer removePermission) {
		this.removePermission = removePermission;
	}

	/**
	 * @return the activatePermission
	 */
	public VoPermContainer getActivatePermission() {
		return activatePermission;
	}

	/**
	 * @param activatePermission the activatePermission to set
	 */
	public void setActivatePermission(VoPermContainer activatePermission) {
		this.activatePermission = activatePermission;
	}

	/**
	 * @return the inactiveAssignmentPermission
	 */
	public VoPermContainer getInactiveAssignmentPermission() {
		return inactiveAssignmentPermission;
	}

	/**
	 * @param inactiveAssignmentPermission the inactiveAssignmentPermission to set
	 */
	public void setInactiveAssignmentPermission(VoPermContainer inactiveAssignmentPermission) {
		this.inactiveAssignmentPermission = inactiveAssignmentPermission;
	}
}
