package com.magna.xmsystem.xmadmin.ui.treeviewer.admintree;

import java.io.File;

import javax.inject.Inject;

import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.swt.widgets.Display;

import com.magna.xmsystem.dependencies.utils.XMSystemUtil;
import com.magna.xmsystem.ui.controls.widgets.CustomMessageDialog;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.AdminTreeDataLoad;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.AdminTreeFactory;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.adminarea.AdminAreaProjects;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.adminarea.AdministrationArea;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.adminarea.AdministrationAreas;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.Applications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.baseapplicationmodel.BaseApplications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.projectapplicationmodel.ProjectAppAdminAreas;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.projectapplicationmodel.ProjectApplication;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.projectapplicationmodel.ProjectApplications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.startapplicationmodel.StartApplications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.userapplicationmodel.UserApplication;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.userapplicationmodel.UserApplications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.livemsgconfig.LiveMessages;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.notification.Notifications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.notification.ProjectCreateEvt;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.notification.ProjectDeleteEvt;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.roles.Role;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.roles.RoleScopeObjects;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.roles.Roles;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.directory.Directories;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.group.Groups;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.group.projectappgroup.ProjectApplicationGroups;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.group.projectgroup.ProjectGroupsModel;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.group.userappgroup.UserApplicationGroups;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.group.usergroup.UserGroupsModel;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.projects.Project;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.projects.ProjectAdminAreas;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.projects.ProjectUserAdminAreas;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.projects.ProjectUserChild;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.projects.ProjectUsers;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.projects.Projects;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.refs.RelationObj;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.sitemodel.Site;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.sitemodel.SiteAdminAreaProjects;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.sitemodel.SiteAdministrationChild;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.sitemodel.SiteAdministrations;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.sitemodel.Sites;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.users.User;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.users.UserAdminAreas;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.users.UserProjectAdminAreas;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.users.UserProjectChild;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.users.UserProjects;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.users.Users;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.users.UsersNameAlphabet;
import com.magna.xmsystem.xmadmin.util.XMAdminUtil;

/**
 * Class for Admin tree expansion.
 *
 * @author Chiranjeevi.Akula
 */
public class AdminTreeExpansion {

	/** Member variable 'element' for {@link Object}. */
	private Object element;
	
	/** Member variable 'instance' for {@link AdminTreeFactory}. */
	private AdminTreeDataLoad dataLoadInstance;
	
	/** Member variable 'admin tree' for {@link AdminTreeviewer}. */
	private AdminTreeviewer adminTree;

	/**
	 * Constructor for AdminTreeExpansion Class.
	 *
	 * @param element {@link Object}
	 */
	@Inject
	public AdminTreeExpansion(Object element) {
		this.element = element;
		this.dataLoadInstance = AdminTreeDataLoad.getInstance();
		this.adminTree = XMAdminUtil.getInstance().getAdminTree();
	}

	/**
	 * Method for Load objects.
	 */
	public void loadObjects() {
		if (element != null) {
			// FIXME 2017-10-11 SCP: Too many nested if-else-statements,
			// extensively usage of instanceof
			final File iconFolder = XMSystemUtil.getXMSystemServerIconsFolder();
			if (iconFolder == null) {
				Display.getDefault().asyncExec(new Runnable() {
					@Override
					public void run() {
						CustomMessageDialog.openError(Display.getDefault().getActiveShell(), "Error",
								"Icon folder is not reachable");
					}
				});
			}
			if (element instanceof Sites) {
				dataLoadInstance.loadSitesFromService();
			} else if (element instanceof AdministrationAreas) {
				dataLoadInstance.loadAdminAreasFromService();
			} else if (element instanceof Projects) {
				dataLoadInstance.loadProjectsFromService();
			} else if (element instanceof Applications) {
				dataLoadInstance.loadAllApplications();
			} else if (element instanceof UserApplications) {
				dataLoadInstance.loadUserAppFromService();
			} else if (element instanceof UserApplication) {
				dataLoadInstance.loadChildUserAppsFromService((UserApplication) element);
			} else if (element instanceof ProjectApplications) {
				dataLoadInstance.loadProjectAppFromService();
			} else if (element instanceof ProjectApplication) {
				dataLoadInstance.loadProjectAppChildrenFromService((ProjectApplication) element);
			} else if (element instanceof ProjectAppAdminAreas) {
				String projectApplicationId = ((ProjectApplication) ((ProjectAppAdminAreas) element).getParent())
						.getProjectApplicationId();
				dataLoadInstance.loadProjectAppAdminAreasFromService(projectApplicationId,
						(ProjectAppAdminAreas) element);
			} else if (element instanceof StartApplications) {
				dataLoadInstance.loadStartAppFromService();
			} else if (element instanceof BaseApplications) {
				dataLoadInstance.loadBaseAppFromService();
			} else if (element instanceof Users) {
				dataLoadInstance.loadUsersFromService();
			} else if (element instanceof User) {
				dataLoadInstance.loadUserChildrenFromService((User) element);
			} else if (element instanceof UserProjects) {
				String userId = ((User) ((UserProjects) element).getParent()).getUserId();
				dataLoadInstance.loadUserProjectsFromService(userId, (UserProjects) element);
			} else if (element instanceof UserProjectAdminAreas) {
				dataLoadInstance.loadUserProjectAAsFromService((UserProjectAdminAreas) element);
			} else if (element instanceof UserAdminAreas) {
				dataLoadInstance.loadUserAdminAreasFromService((UserAdminAreas) element);
			} else if (element instanceof UsersNameAlphabet) {
				dataLoadInstance.loadUsersBasedOnFirstChar((UsersNameAlphabet) element);
			} else if (element instanceof Directories) {
				dataLoadInstance.loadDirectoriesFromService();
			} else if (element instanceof Site) {
				dataLoadInstance.loadSiteChildrenFromService((Site) element);
			} else if (element instanceof SiteAdministrations) {
				String siteId = ((Site) ((SiteAdministrations) element).getParent()).getSiteId();
				dataLoadInstance.loadSiteAdministrationsFromService(siteId, (SiteAdministrations) element);
			} else if (element instanceof ProjectAdminAreas) {
				String projectId = ((Project) ((ProjectAdminAreas) element).getParent()).getProjectId();
				dataLoadInstance.loadProjectAdminAreasFromService(projectId, (ProjectAdminAreas) element);
			} else if (element instanceof SiteAdminAreaProjects) {
				String siteAdminAreaRelId = ((RelationObj) ((SiteAdminAreaProjects) element).getParent()).getRelId();
				dataLoadInstance.loadSiteAdminAreaProjectsFromService(siteAdminAreaRelId,
						(SiteAdminAreaProjects) element);
			} else if (element instanceof AdministrationArea) {
				dataLoadInstance.loadAdminAreaChildrenFromService((AdministrationArea) element);
			} else if (element instanceof AdminAreaProjects) {
				String siteAdminAreaRelId = ((AdministrationArea) ((AdminAreaProjects) element).getParent())
						.getRelationId();
				dataLoadInstance.loadSiteAdminAreaProjectsFromService(siteAdminAreaRelId, (AdminAreaProjects) element);
			} else if (element instanceof Project) {
				dataLoadInstance.loadProjectChildrenFromService((Project) element);
			} else if (element instanceof ProjectUsers) {
				Project project = (Project) ((ProjectUsers) element).getParent();
				dataLoadInstance.loadProjectUsersFromService(project.getProjectId(), (ProjectUsers) element);
			} else if (element instanceof ProjectUserAdminAreas) {
				dataLoadInstance.loadUserProjectAAsFromService((ProjectUserAdminAreas) element);
			} else if (element instanceof Role) {
				dataLoadInstance.loadRoleChildrenFromService((Role) element);
			} else if (element instanceof RoleScopeObjects) {
				dataLoadInstance.loadRoleAdminAreasFromService(((Role) ((RoleScopeObjects) element).getParent()),
						(RoleScopeObjects) element);
			} else if (element instanceof Groups) {
				dataLoadInstance.loadAllGroups();
			} else if (element instanceof UserGroupsModel) {
				dataLoadInstance.loadUserGroupsFromService();
			} else if (element instanceof ProjectGroupsModel) {
				dataLoadInstance.loadProjectGroupsFromService();
			} else if (element instanceof UserApplicationGroups) {
				dataLoadInstance.loadUserAppGroupsFromService();
			} else if (element instanceof ProjectApplicationGroups) {
				dataLoadInstance.loadProjectAppGroupsFromService();
			} else if (element instanceof Roles) {
				dataLoadInstance.loadRolesFromService();
			} else if (element instanceof LiveMessages) {
				dataLoadInstance.loadLiveMessagesFromService();
			} else if(element instanceof Notifications) {
				dataLoadInstance.loadAllNotifications();
			} else if (element instanceof ProjectCreateEvt) {
				dataLoadInstance.loadProjectCreateEvt();
			} else if (element instanceof ProjectDeleteEvt) {
				dataLoadInstance.loadProjectDeleteEvt();
			} else if (element instanceof RelationObj) {
				IAdminTreeChild containerObj = ((RelationObj) element).getContainerObj();
				if (containerObj instanceof SiteAdministrationChild) {
					String siteAdminAreaRelId = ((RelationObj) element).getRelId();
					dataLoadInstance.loadSiteAdminAreaChildrenFromService(siteAdminAreaRelId, (RelationObj) element);
				} else if (containerObj instanceof UserProjectChild) {
					dataLoadInstance.loadUserProjectChildrenFromService((UserProjectChild) containerObj);
				} else if (containerObj instanceof ProjectUserChild) {
					dataLoadInstance.loadProjectUserChildrenFromService((ProjectUserChild) containerObj);
				}
			}
			refreshElement(true);
		}
	}

	/**
	 * Method for Refresh element.
	 *
	 * @param loadSlection {@link boolean}
	 */
	private void refreshElement(final boolean loadSlection) {
		Display.getDefault().asyncExec(new Runnable() {
			public void run() {
				adminTree.refresh(element);
				//adminTree.setExpandedState(element, true);
				if (loadSlection) {
					Object firstElement = adminTree.getStructuredSelection().getFirstElement();
					if (element.equals(firstElement)) {
						StructuredSelection selection = new StructuredSelection(element);
						//if (!(selection.equals(adminTree.getPerviousSelection()))) { 
							adminTree.setSelection(selection, true);
						//}
					}
				}
			}
		});
	}
}
