package com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.users;

import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild;

/**
 * The Class UserProjectAppAllowed.
 */
public class UserProjectAAProjectAppAllowed implements IAdminTreeChild {
	/** The parent. */
	private IAdminTreeChild parent;

	/**
	 * Instantiates a new user app allowed.
	 *
	 * @param parent the parent
	 */
	public UserProjectAAProjectAppAllowed(final IAdminTreeChild parent) {
		this.parent = parent;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild#
	 * getParent()
	 */
	@Override
	public IAdminTreeChild getParent() {
		return this.parent;
	}

	/*
	 * com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild#
	 * setParent(com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.
	 * IAdminTreeChild)
	 */
	@Override
	public void setParent(final IAdminTreeChild parent) {
		this.parent = parent;
	}
}
