package com.magna.xmsystem.xmadmin.ui.parts.notification;

import org.eclipse.jface.layout.GridDataFactory;
import org.eclipse.jface.layout.GridLayoutFactory;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.ScrolledComposite;
import org.eclipse.swt.custom.StackLayout;
import org.eclipse.swt.custom.StyledText;
import org.eclipse.swt.events.ControlAdapter;
import org.eclipse.swt.events.ControlEvent;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.widgets.ToolBar;
import org.eclipse.swt.widgets.ToolItem;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.magna.xmbackend.vo.enums.NotificationEventType;
import com.magna.xmsystem.dependencies.utils.XMSystemUtil;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.notification.Notification;
import com.magna.xmsystem.xmadmin.util.XMAdminUtil;

/**
 * The Class NotificationCompositeUI.
 * 
 * @author archita.patel
 */
public class NotificationCompositeUI extends Composite {

	/** The Constant LOGGER. */
	private static final Logger LOGGER = LoggerFactory.getLogger(NotificationCompositeUI.class);

	/** The save btn. */
	protected Button saveBtn;

	/** The cancel btn. */
	protected Button cancelBtn;

	/** The lbl to users. */
	protected Label lblToUsers;

	/** The lbl description. */
	protected Label lblDescription;

	/** The txt description. */
	protected StyledText txtDescription;

	/** The txt to users. */
	protected StyledText txtToUsers;

	/** The lbl notification crie. */
	protected Label lblEventType;

	/** The cmb notification crie. */
	protected Combo cmbEventType;

	/** The edit btn. */
	protected Button editButton;

	/** The lbl subject. */
	protected Label lblSubject;

	/** The txt subject. */
	protected Text txtSubject;

	/** The lbl message. */
	protected Label lblMessage;

	/** The txt message. */
	protected Text txtMessage;

	/** The btnis obj notification. */
	protected Button includeRemarksBtn;

	/** The grp notification. */
	protected Group grpNotification;

	/** The send to assign user. */
	protected Button sendToAssignUserBtn;

	/** The filter panel. */
	protected FilterPanel filterPanel;

	/** The radio btn user. */
	protected Button radioBtnUser;

	/** The radio btn group user. */
	protected Button radioBtnUserGroup;

	/** The user stack layout. */
	protected StackLayout filterContainerLayout;

	/** The filter container. */
	protected Composite filterContainer;

	/** The user group filter panel. */
	protected UserGroupFilterPanel userGroupFilterPanel;

	/** The add tool item. */
	protected ToolItem addToolItem;

	/** The empty label 1. */
	private Label emptyLabel1;

	/** The empty label 2. */
	private Label emptyLabel2;

	/** The empty label 3. */
	private Label emptyLabel3;

	/** The empty label 4. */
	private Label emptyLabel4;

	/** The empty lbl radion btn. */
	private Label emptyLbl_radionBtn;

	/** The radio btn container. */
	private Composite radioBtnContainer;

	/** The base filter cont. */
	private Composite baseFilterCont;

	/** The add btn container. */
	private Composite addBtnContainer;

	/** The add toolbar. */
	private ToolBar addToolbar;

	/** The empty lbl filter panel. */
	private Label emptyLbl_FilterPanel;
	
	/** The lbl active. */
	protected Label lblActive;
	
	/** The active btn. */
	protected Button activeBtn;

	/**
	 * Instantiates a new notification composite UI.
	 *
	 * @param parent
	 *            the parent
	 * @param style
	 *            the style
	 */
	public NotificationCompositeUI(Composite parent, int style) {
		super(parent, SWT.NONE);
		this.initGUI();
	}

	/**
	 * Inits the GUI.
	 */
	private void initGUI() {
		try {
			GridLayoutFactory.fillDefaults().applyTo(this);
			this.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));

			this.grpNotification = new Group(this, SWT.NONE);
			this.grpNotification.setBackgroundMode(SWT.INHERIT_FORCE);
			GridLayoutFactory.fillDefaults().numColumns(1).applyTo(this.grpNotification);
			GridDataFactory.fillDefaults().grab(true, true).span(SWT.FILL, SWT.FILL).applyTo(this.grpNotification);
			ScrolledComposite scrolledComposite = XMAdminUtil.getInstance().createScrolledComposite(this.grpNotification);
			scrolledComposite.setBackgroundMode(SWT.INHERIT_FORCE);

			final Composite widgetContainer = new Composite(scrolledComposite, SWT.NONE);
			final GridLayout widgetContLayout = new GridLayout(3, false);

			widgetContainer.setLayout(widgetContLayout);
			widgetContainer.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));

			this.lblEventType = new Label(widgetContainer, SWT.NONE);

			final Composite comboBoxComp = new Composite(widgetContainer, SWT.NONE);
			final GridLayout comboBoxCompLayout = new GridLayout(2, false);
			comboBoxCompLayout.marginRight = 0;
			comboBoxCompLayout.marginTop = 0;
			comboBoxCompLayout.marginBottom = 0;
			comboBoxCompLayout.marginWidth = 0;
			comboBoxComp.setLayout(comboBoxCompLayout);
			GridDataFactory.fillDefaults().grab(true, false).span(2, 1).align(SWT.FILL, SWT.CENTER)
					.applyTo(comboBoxComp);

			this.cmbEventType = new Combo(comboBoxComp, SWT.BORDER | SWT.READ_ONLY);
			GridDataFactory.fillDefaults().grab(true, false).span(1, 1).align(SWT.FILL, SWT.CENTER)
					.applyTo(this.cmbEventType);

			//final ToolBar toolbar = new ToolBar(comboBoxComp, SWT.NONE);
			this.editButton = new Button(comboBoxComp, SWT.NONE);

			/*this.editToolItem.setImage(XMSystemUtil.getInstance().getImage(this.getClass(),
					"icons/16x16/editnotification.png", this.resourceManager, true));*/
			this.editButton.setEnabled(false);
			this.editButton.setBackground(Display.getDefault().getSystemColor(SWT.COLOR_DARK_GRAY));
			
			this.lblActive = new Label(widgetContainer, SWT.NONE);
			this.activeBtn = new Button(widgetContainer, SWT.CHECK);
			GridDataFactory.fillDefaults().grab(true, false).span(2, 1).align(SWT.FILL, SWT.CENTER)
			.applyTo(this.activeBtn);

			this.lblDescription = new Label(widgetContainer, SWT.NONE);
			this.lblDescription.setLayoutData(new GridData(SWT.LEFT, SWT.TOP, false, false));

			this.txtDescription = new StyledText(widgetContainer,
					SWT.BORDER | SWT.WRAP | SWT.MULTI | SWT.H_SCROLL | SWT.V_SCROLL);
			GridData gridData = new GridData(SWT.FILL, SWT.FILL, true, true);
			gridData.minimumHeight = 10;
			gridData.horizontalSpan = 2;
			this.txtDescription.setLayoutData(gridData);
			this.txtDescription.setEditable(false);
			

			this.emptyLbl_radionBtn = new Label(widgetContainer, SWT.NONE);
			gridData = new GridData();
			this.emptyLbl_radionBtn.setLayoutData(gridData);
			
			this.radioBtnContainer = new Composite(widgetContainer, SWT.NONE);
			GridLayoutFactory.fillDefaults().numColumns(3).equalWidth(false).extendedMargins(0, 0, 0, 0)
					.applyTo(this.radioBtnContainer);
			GridDataFactory.fillDefaults().grab(true, false).span(2, 1).align(SWT.FILL, SWT.CENTER)
					.applyTo(this.radioBtnContainer);

			this.radioBtnUser = new Button(this.radioBtnContainer, SWT.RADIO);
			GridDataFactory.fillDefaults().grab(true, false).span(1, 1).align(SWT.FILL, SWT.CENTER)
					.applyTo(this.radioBtnUser);
			this.radioBtnUser.setSelection(true);

			this.radioBtnUserGroup = new Button(this.radioBtnContainer, SWT.RADIO);
			GridDataFactory.fillDefaults().grab(true, false).span(1, 1).align(SWT.FILL, SWT.CENTER)
					.applyTo(this.radioBtnUserGroup);

			this.baseFilterCont = new Composite(widgetContainer, SWT.NONE);
			final GridLayout containerLayout = new GridLayout(2, false);
			this.baseFilterCont.setLayout(containerLayout);
			GridDataFactory.fillDefaults().grab(true, false).span(2, 1).align(SWT.FILL, SWT.FILL).applyTo(this.baseFilterCont);

			this.filterContainer = new Composite(baseFilterCont, SWT.NONE);
			this.filterContainerLayout = new StackLayout();
			this.filterContainer.setLayout(this.filterContainerLayout);
			GridDataFactory.fillDefaults().grab(true, false).span(1, 1).align(SWT.FILL, SWT.CENTER)
					.applyTo(this.filterContainer);

			this.filterPanel = new FilterPanel(this.filterContainer);
			final GridLayout filterCompLayout = new GridLayout(1, true);
			filterCompLayout.marginRight = 0;
			filterCompLayout.marginLeft = 0;
			filterCompLayout.marginTop = 0;
			filterCompLayout.marginBottom = 0;
			filterCompLayout.marginWidth = 0;
			this.filterPanel.setLayout(filterCompLayout);
			this.filterPanel.setLayoutData(new GridData(SWT.FILL, SWT.DEFAULT, true, false, 1, 1));

			this.userGroupFilterPanel = new UserGroupFilterPanel(this.filterContainer);
			final GridLayout userGroupfilterCompLayout = new GridLayout(1, true);
			filterCompLayout.marginRight = 0;
			filterCompLayout.marginLeft = 0;
			filterCompLayout.marginTop = 0;
			filterCompLayout.marginBottom = 0;
			filterCompLayout.marginWidth = 0;
			this.userGroupFilterPanel.setLayout(userGroupfilterCompLayout);
			this.userGroupFilterPanel.setLayoutData(new GridData(SWT.FILL, SWT.DEFAULT, true, false, 1, 1));

			this.filterContainerLayout.topControl = this.filterPanel;

			this.addBtnContainer = new Composite(this.baseFilterCont, SWT.NONE);
			final GridLayout addcontainerLayout = new GridLayout(1, false);
			this.addBtnContainer.setLayout(addcontainerLayout);
			GridData layoutData = new GridData(SWT.CENTER, SWT.CENTER, false, false);
			layoutData.verticalIndent = -50;
			this.addBtnContainer.setLayoutData(layoutData);

			this.addToolbar = new ToolBar(this.addBtnContainer, SWT.NONE);
			this.addToolItem = new ToolItem(this.addToolbar, SWT.FLAT);
			this.addToolItem.setImage(XMSystemUtil.getInstance().getImage(this.getClass(), "icons/32x32/addButton_32.png"));
			this.addToolbar.setLayoutData(new GridData(SWT.CENTER, SWT.CENTER, false, false));
			//this.addToolItem.setToolTipText("Add");

			this.emptyLbl_FilterPanel = new Label(widgetContainer, SWT.NONE);
			gridData = new GridData();
			this.emptyLbl_FilterPanel.setLayoutData(gridData);

			this.lblToUsers = new Label(widgetContainer, SWT.NONE);
			GridDataFactory.fillDefaults().grab(false, false).span(1, 1).align(SWT.LEFT, SWT.CENTER)
					.applyTo(this.lblToUsers);

			this.txtToUsers = new StyledText(widgetContainer,
					SWT.BORDER | SWT.WRAP | SWT.MULTI | SWT.H_SCROLL | SWT.V_SCROLL);
			gridData = new GridData(SWT.FILL, SWT.FILL, true, true);
			gridData.minimumHeight = 30;
			gridData.horizontalSpan = 2;
			this.txtToUsers.setLayoutData(gridData);
			
			this.emptyLabel1 = new Label(widgetContainer, SWT.NONE);
			GridData stackGridData = new GridData(SWT.FILL, SWT.FILL,false,false);
			stackGridData.horizontalSpan = 1;
			stackGridData.heightHint = 30;
			this.emptyLabel1.setLayoutData(stackGridData);

			this.sendToAssignUserBtn = new Button(widgetContainer, SWT.CHECK);
			this.sendToAssignUserBtn.setLayoutData(stackGridData);

			this.emptyLabel2 = new Label(widgetContainer, SWT.NONE);
			this.emptyLabel2.setLayoutData(stackGridData);
			
			this.lblSubject = new Label(widgetContainer, SWT.NONE);
			GridDataFactory.fillDefaults().grab(false, false).span(1, 1).align(SWT.LEFT, SWT.CENTER)
					.applyTo(this.lblSubject);

			this.txtSubject = new Text(widgetContainer, SWT.BORDER);
			this.txtSubject.setTextLimit(Notification.SUB_LIMIT);
			GridDataFactory.fillDefaults().grab(true, false).span(2, 1).align(SWT.FILL, SWT.CENTER)
			.applyTo(this.txtSubject);

			this.lblMessage = new Label(widgetContainer, SWT.NONE);
			GridDataFactory.fillDefaults().grab(false, false).span(1, 1).align(SWT.LEFT, SWT.TOP)
					.applyTo(this.lblMessage);

			this.txtMessage = new Text(widgetContainer,
					SWT.BORDER | SWT.WRAP | SWT.MULTI | SWT.H_SCROLL | SWT.V_SCROLL);
			gridData = new GridData(SWT.FILL, SWT.FILL, true, true);
			gridData.minimumHeight = 150;
			gridData.horizontalSpan = 2;
			this.txtMessage.setLayoutData(gridData);
			this.txtMessage.setTextLimit(Notification.MESSAGE_LIMIT);

			final Composite buttonBarComp = new Composite(this.grpNotification, SWT.NONE);
			final GridLayout btnBarCompLayout = new GridLayout(2, true);
			btnBarCompLayout.marginRight = 0;
			btnBarCompLayout.marginLeft = 0;
			btnBarCompLayout.marginTop = 0;
			btnBarCompLayout.marginBottom = 0;
			btnBarCompLayout.marginWidth = 0;
			buttonBarComp.setLayout(btnBarCompLayout);
			buttonBarComp.setLayoutData(new GridData(SWT.FILL, SWT.DEFAULT, true, false, 3, 1));
			createButtonBar(buttonBarComp);
			
			this.emptyLabel3 = new Label(widgetContainer, SWT.NONE);
			GridData stackGridData1 = new GridData(SWT.FILL, SWT.FILL,false,false);
			stackGridData1.horizontalSpan = 1;
			stackGridData1.heightHint = 30;
			this.emptyLabel3.setLayoutData(stackGridData1);
			
			this.includeRemarksBtn = new Button(widgetContainer, SWT.CHECK);
			this.includeRemarksBtn.setLayoutData(stackGridData1);
			
			this.emptyLabel4 = new Label(widgetContainer, SWT.NONE);
			this.emptyLabel4.setLayoutData(stackGridData1);

			excludeSendToAssignUserBtn(false);
			excludeRemarkIncludeBtn(false);
			scrolledComposite.setContent(widgetContainer);
			scrolledComposite.setSize(widgetContainer.getSize());
			scrolledComposite.setExpandVertical(true);
			scrolledComposite.setExpandHorizontal(true);
			scrolledComposite.update();

			scrolledComposite.addControlListener(new ControlAdapter() {
				public void controlResized(final ControlEvent e) {
					Rectangle rectangle = scrolledComposite.getClientArea();
					scrolledComposite.setMinSize(widgetContainer.computeSize(rectangle.width, SWT.DEFAULT));
				}
			});
		} catch (Exception e) {
			LOGGER.error("Unable to crete UI elements", e); //$NON-NLS-1$
		}
	}

	/**
	 * Creates the button bar.
	 *
	 * @param buttonBarComp
	 *            the button bar comp
	 */
	private void createButtonBar(final Composite buttonBarComp) {
		this.saveBtn = new Button(buttonBarComp, SWT.NONE);
		this.saveBtn.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, true, false));

		this.cancelBtn = new Button(buttonBarComp, SWT.NONE);
		this.cancelBtn.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, true, false));
	}

	/**
	 * Update checkbox visibilty.
	 */
	protected void updateCheckboxVisibilty(boolean isEdit) {
		if (this.includeRemarksBtn != null && !this.includeRemarksBtn.isDisposed() && this.sendToAssignUserBtn != null
				&& !this.sendToAssignUserBtn.isDisposed()) {

			String eventType = this.cmbEventType.getText();
			excludeActiveBtn(true);
			if (eventType.equals(NotificationEventType.PROJECT_ACTIVATE.toString())
					|| eventType.equals(NotificationEventType.PROJECT_CREATE.toString())
					|| eventType.equals(NotificationEventType.PROJECT_DELETE.toString())
					|| eventType.equals(NotificationEventType.PROJECT_DEACTIVATE.toString())) {
				excludeFilterPanel(true);
				excludeToUsers(true);
				excludeRadioBtn(true);
				excludeSendToAssignUserBtn(false);
				excludeRemarkIncludeBtn(true);
				if (isEdit) {
					this.filterPanel.enableSiteFilter(false);
					this.filterPanel.enableAdminAreaFilter(false);
					this.filterPanel.enableProjectFilter(false);
					this.filterPanel.enableUserFilter(true);
				}

			} else if (eventType.equals(NotificationEventType.USER_PROJECT_RELATION_ASSIGN.toString())
					|| eventType.equals(NotificationEventType.USER_PROJECT_RELATION_REMOVE.toString())) {
				excludeFilterPanel(true);
				excludeToUsers(true);
				excludeRadioBtn(true);
				excludeSendToAssignUserBtn(true);
				excludeRemarkIncludeBtn(false);
				if (isEdit) {
					this.filterPanel.enableSiteFilter(false);
					this.filterPanel.enableAdminAreaFilter(false);
					this.filterPanel.enableProjectFilter(true);
					this.filterPanel.enableUserFilter(true);
				}
			} else if (eventType.equals(NotificationEventType.USER_PROJECT_RELATION_EXPIRY.toString())
					|| eventType.equals(NotificationEventType.USER_PROJECT_RELATION_GRACE_EXPIRY.toString())) {
				excludeToUsers(false);
				excludeFilterPanel(false);
				excludeRadioBtn(false);
				excludeSendToAssignUserBtn(false);
				excludeRemarkIncludeBtn(false);
				if (eventType.equals(NotificationEventType.USER_PROJECT_RELATION_GRACE_EXPIRY.toString())) {
					excludeActiveBtn(false);
				}
			} else {
				excludeToUsers(true);
				excludeFilterPanel(true);
				excludeRadioBtn(true);
				excludeSendToAssignUserBtn(false);
				excludeRemarkIncludeBtn(false);
				if (isEdit) {
					this.filterPanel.enableSiteFilter(true);
					this.filterPanel.enableAdminAreaFilter(true);
					this.filterPanel.enableProjectFilter(true);
					this.filterPanel.enableUserFilter(true);
				}
			}
		}
	}

	/**
	 * Exclude active btn.
	 *
	 * @param showLabel the show label
	 */
	private void excludeActiveBtn(boolean showLabel) {
		if (this.activeBtn != null && !this.activeBtn.isDisposed()) {
			GridData layoutData = (GridData) this.activeBtn.getLayoutData();
			layoutData.exclude = !showLabel;
			layoutData = (GridData) this.lblActive.getLayoutData();
			layoutData.exclude = !showLabel;
			this.activeBtn.setVisible(showLabel);
			this.lblActive.setVisible(showLabel);
			this.activeBtn.setVisible(showLabel);
			this.activeBtn.requestLayout();
			this.activeBtn.redraw();
			this.activeBtn.getParent().update();
		}
	}

	/**
	 * Exclude send to assign user btn.
	 *
	 * @param showLabel the show label
	 */
	private void excludeSendToAssignUserBtn(final boolean showLabel) {
		if (this.sendToAssignUserBtn != null && !this.sendToAssignUserBtn.isDisposed()) {
			final GridData layoutData = (GridData) this.sendToAssignUserBtn.getLayoutData();
			layoutData.exclude = !showLabel;
			this.emptyLabel1.setVisible(showLabel);
			this.emptyLabel2.setVisible(showLabel);
			this.sendToAssignUserBtn.setVisible(showLabel);
			this.sendToAssignUserBtn.requestLayout();
			this.sendToAssignUserBtn.redraw();
			this.sendToAssignUserBtn.getParent().update();
		}
	}
	
	/**
	 * Exclude remark include btn.
	 *
	 * @param showLabel the show label
	 */
	private void excludeRemarkIncludeBtn(final boolean showLabel) {
		if (this.includeRemarksBtn != null && !this.includeRemarksBtn.isDisposed()) {
			final GridData layoutData = (GridData) this.includeRemarksBtn.getLayoutData();
			layoutData.exclude = !showLabel;
			this.emptyLabel3.setVisible(showLabel);
			this.emptyLabel4.setVisible(showLabel);
			this.includeRemarksBtn.setVisible(showLabel);
			this.includeRemarksBtn.requestLayout();
			this.includeRemarksBtn.redraw();
			this.includeRemarksBtn.getParent().update();
		}
	}
	
	/**
	 * Exclude to users.
	 *
	 * @param showLabel the show label
	 */
	private void excludeToUsers(final boolean showLabel) {
		if (this.txtToUsers != null && !this.txtToUsers.isDisposed()) {
			GridData layoutData = (GridData) this.txtToUsers.getLayoutData();
			layoutData.exclude = !showLabel;
			layoutData = (GridData) this.lblToUsers.getLayoutData();
			layoutData.exclude = !showLabel;
			this.lblToUsers.setVisible(showLabel);
			this.txtToUsers.setVisible(showLabel);
			this.includeRemarksBtn.setVisible(showLabel);
			this.includeRemarksBtn.requestLayout();
			this.includeRemarksBtn.redraw();
			this.includeRemarksBtn.getParent().update();
		}
	}
	
	/**
	 * Exclude filter panel.
	 *
	 * @param showLabel the show label
	 */
	private void excludeFilterPanel(final boolean showLabel) {
		if (this.baseFilterCont != null && !this.baseFilterCont.isDisposed()) {
			GridData layoutData = (GridData) this.baseFilterCont.getLayoutData();
			layoutData.exclude = !showLabel;
			layoutData = (GridData) this.emptyLbl_FilterPanel.getLayoutData();
			layoutData.exclude = !showLabel;
			this.baseFilterCont.setVisible(showLabel);
			this.emptyLbl_FilterPanel.setVisible(showLabel);
			this.baseFilterCont.requestLayout();
			this.baseFilterCont.redraw();
			this.baseFilterCont.getParent().update();
		}
	}
	
	/**
	 * Exclude radio btn.
	 *
	 * @param showLabel the show label
	 */
	private void excludeRadioBtn(final boolean showLabel) {
		if (this.radioBtnContainer != null && !this.radioBtnContainer.isDisposed()) {
			GridData layoutData = (GridData) this.radioBtnContainer.getLayoutData();
			layoutData.exclude = !showLabel;
			layoutData = (GridData) this.emptyLbl_radionBtn.getLayoutData();
			layoutData.exclude = !showLabel;
			this.radioBtnContainer.setVisible(showLabel);
			this.emptyLbl_radionBtn.setVisible(showLabel);
			this.radioBtnContainer.requestLayout();
			this.radioBtnContainer.redraw();
			this.radioBtnContainer.getParent().update();
		}
	}

	
	/**
	 * Sets the show button bar.
	 *
	 * @param showButtonBar
	 *            the new show button bar
	 */
	protected void setShowButtonBar(final boolean showButtonBar) {
		if (this.saveBtn != null && !this.saveBtn.isDisposed() && this.cancelBtn != null
				&& !this.cancelBtn.isDisposed()) {
			final GridData layoutData = (GridData) this.saveBtn.getParent().getLayoutData();
			layoutData.exclude = !showButtonBar;
			this.saveBtn.setVisible(showButtonBar);
			this.cancelBtn.setVisible(showButtonBar);
			this.saveBtn.getParent().setVisible(showButtonBar);
			this.saveBtn.getParent().requestLayout();
			this.saveBtn.getParent().redraw();
			this.saveBtn.getParent().getParent().update();
		}
	}
}
