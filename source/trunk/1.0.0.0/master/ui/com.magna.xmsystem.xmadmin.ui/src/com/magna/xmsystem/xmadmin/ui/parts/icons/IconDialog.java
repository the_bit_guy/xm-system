package com.magna.xmsystem.xmadmin.ui.parts.icons;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.ITreeSelection;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Shell;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.magna.xmsystem.dependencies.utils.LANG_ENUM;
import com.magna.xmsystem.dependencies.utils.XMSystemUtil;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.icon.Icon;

public class IconDialog extends Dialog {
	/**
	 * Logger Instance class
	 */
	private static final Logger LOGGER = LoggerFactory.getLogger(IconDialog.class);
	private Shell parentShell;
	private String title;
	private Composite area;
	private IconsCompositeAction iconComp;
	private Icon selectedObject;
	private String iconColoumLabel;
	
	/** The Constant OK_LABEL_EN. */
	final static private String OK_LABEL_EN = "OK";
	
	/** The Constant OK_LABEL_DE. */
	final static private String OK_LABEL_DE = "OK";
	
	/** The Constant CANCEL_LABEL_EN. */
	final static private String CANCEL_LABEL_EN = "Cancel";
	
	/** The Constant CANCEL_LABEL_DE. */
	final static private String CANCEL_LABEL_DE = "Abbrechen";
	/**
	 * Parameterized constructor
	 * @param icontableviewerFirstColumnLabel 
	 * 
	 * @param parentShell
	 * @param strMainLabel
	 * @param maxLimit
	 */
	public IconDialog(final Shell shell, final String title, String iconColoumLabel) {
		super(shell);
		this.parentShell = shell;
		this.title = title;
		this.iconColoumLabel = iconColoumLabel;
	}
	
	/**
	 * Configures the Shell
	 * 
	 * @see org.eclipse.jface.window.Window#configureShell(org.eclipse.swt.widgets.Shell)
	 * @param newShell
	 */
	@Override
	protected void configureShell(final Shell newShell) {
		try {
			super.configureShell(newShell);
			newShell.setText(this.title);
			newShell.setParent(this.parentShell);
			final Rectangle shellBounds = parentShell.getBounds();
			int width = 450;
			int height = 300;
			newShell.setSize(width, height);
			final Point pos = new Point((shellBounds.width - width) / 2, (shellBounds.height - height) / 2);
			newShell.setLocation(pos.x, pos.y);
		} catch (Exception e) {
			LOGGER.error("Execution occurred while configuring shell!", e); //$NON-NLS-1$
		}
	}
	
	
	/**
	 * method createDialogArea for create control dialog
	 */
	@Override
	protected Control createDialogArea(final Composite parent) {
		try {
			parent.setBackgroundMode(SWT.INHERIT_FORCE);
			this.area = (Composite) super.createDialogArea(parent);
			final GridLayout areaLayout = new GridLayout(1, false);
			this.area.setLayout(areaLayout);
			final Composite widgetContainer = new Composite(area, SWT.NONE);
			final GridLayout widgetContainerLayout = new GridLayout(1, false);
			widgetContainer.setLayout(widgetContainerLayout);
			widgetContainer.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
			this.iconComp = new IconsCompositeAction(widgetContainer, SWT.NONE, true);
			final IconTreeviewer iconTreeviewer = iconComp.getIconTreeviewer();
			iconTreeviewer.getFirstColoum().getColumn().setText(iconColoumLabel);
			
			iconTreeviewer.addSelectionChangedListener(new ISelectionChangedListener() {
				
				@Override
				public void selectionChanged(SelectionChangedEvent event) {
					updateOKStatus(iconTreeviewer);
				}
			});
		} catch (Exception e) {
			LOGGER.error("Unable to create UI elements", e);
		}
		return this.area;
	}
	
	protected void updateOKStatus(IconTreeviewer iconTreeviewer) {
		ITreeSelection selection;
		if ((selection = iconTreeviewer.getStructuredSelection()) != null && selection.getFirstElement() != null) {
			getButton(IDialogConstants.OK_ID).setEnabled(true);
		} else {
			getButton(IDialogConstants.OK_ID).setEnabled(false);
		}
	}

	public Icon getCheckedIcon() {
		return this.selectedObject;
	}
	
	@Override
	protected Control createButtonBar(Composite parent) {
		final Control buttonBar = super.createButtonBar(parent);
		
		Button cancelButton = getButton(IDialogConstants.CANCEL_ID);
		Button okButton = getButton(IDialogConstants.OK_ID);
		LANG_ENUM currentLang = LANG_ENUM.ENGLISH;
		final String localeString = XMSystemUtil.getLanguage();
		if(!XMSystemUtil.isEmpty(localeString)) {
			currentLang = LANG_ENUM.valueOf(localeString.toUpperCase());
		}
		if (currentLang == LANG_ENUM.GERMAN) {
			cancelButton.setText(CANCEL_LABEL_DE);
			okButton.setText(OK_LABEL_DE);
		}else{
			cancelButton.setText(CANCEL_LABEL_EN);
			okButton.setText(OK_LABEL_EN);
		}
		
		okButton.setEnabled(false);
		return buttonBar;
	}
	
	@Override
	protected void okPressed() {
		IconTreeviewer iconTreeviewer = this.iconComp.getIconTreeviewer();
		ITreeSelection selection;
		Object element;
		if ((selection = iconTreeviewer.getStructuredSelection()) != null && (element = selection.getFirstElement()) != null) {
			if (element instanceof Icon) {
				this.selectedObject = (Icon) element;
			}
		}
		super.okPressed();
	}
}
