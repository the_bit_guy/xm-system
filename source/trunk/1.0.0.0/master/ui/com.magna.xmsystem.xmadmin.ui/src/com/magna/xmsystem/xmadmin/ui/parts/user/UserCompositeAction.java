package com.magna.xmsystem.xmadmin.ui.parts.user;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.inject.Inject;

import org.eclipse.core.databinding.Binding;
import org.eclipse.core.databinding.DataBindingContext;
import org.eclipse.core.databinding.UpdateValueStrategy;
import org.eclipse.core.databinding.beans.BeanProperties;
import org.eclipse.core.databinding.observable.value.IObservableValue;
import org.eclipse.core.databinding.observable.value.IValueChangeListener;
import org.eclipse.core.databinding.observable.value.ValueChangeEvent;
import org.eclipse.e4.core.services.nls.Translation;
import org.eclipse.e4.ui.model.application.ui.MDirtyable;
import org.eclipse.jface.databinding.fieldassist.ControlDecorationSupport;
import org.eclipse.jface.databinding.swt.WidgetProperties;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.VerifyEvent;
import org.eclipse.swt.events.VerifyListener;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Link;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.widgets.ToolItem;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.client.ResourceAccessException;

import com.magna.xmsystem.dependencies.customExceptions.UnauthorizedAccessException;
import com.magna.xmsystem.dependencies.utils.LANG_ENUM;
import com.magna.xmsystem.dependencies.utils.XMSystemUtil;
import com.magna.xmsystem.ui.controls.widgets.CustomMessageDialog;
import com.magna.xmsystem.xmadmin.message.Message;
import com.magna.xmsystem.xmadmin.message.MessageRegistry;
import com.magna.xmsystem.xmadmin.restclient.users.UserController;
import com.magna.xmsystem.xmadmin.ui.dialogs.lang.ControlModel;
import com.magna.xmsystem.xmadmin.ui.dialogs.lang.TextAreaModifyListener;
import com.magna.xmsystem.xmadmin.ui.dialogs.lang.XMAdminLangTextAreaDialog;
import com.magna.xmsystem.xmadmin.ui.dialogs.lang.XMAdminLangTextDialog;
import com.magna.xmsystem.xmadmin.ui.parts.icons.IconDialog;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.AdminTreeviewer;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.AdminTreeFactory;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.icon.Icon;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.users.User;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.users.Users;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.users.UsersNameAlphabet;
import com.magna.xmsystem.xmadmin.ui.validation.SymbolValidation;
import com.magna.xmsystem.xmadmin.util.CommonConstants;
import com.magna.xmsystem.xmadmin.util.MessageType;
import com.magna.xmsystem.xmadmin.util.XMAdminUtil;

/**
 * Class for UI of User Composite.
 *
 * @author archita.patel
 */
/**
 * @author archita.patel
 *
 */
public class UserCompositeAction extends UserCompositeUI {

	/** Logger instance. */
	private static final Logger LOGGER = LoggerFactory.getLogger(UserCompositeAction.class);

	/** Member variable for user model. */
	private User userModel;

	/** Member variable for widgetValue. */
	transient private IObservableValue<?> widgetValue;

	/** Member variable for modelValue. */
	transient private IObservableValue<?> modelValue;

	/** Member variable for binding. */
	transient private Binding bindValue;

	/** Member variable for {@link MessageRegistry}. */
	@Inject
	private MessageRegistry registry;

	/** Member variable for message. */
	@Inject
	@Translation
	transient private Message messages;

	/**
	 * Member variable for data binding context the DataBindingContext object
	 * will manage the databindings.
	 */
	final transient DataBindingContext dataBindContext = new DataBindingContext();

	/** Member variable to store old model. */
	private User oldModel;

	/** Member variable for dirty. */
	transient private MDirtyable dirty;

	/** The control model. */
	transient private ControlModel controlModel;

	/**
	 * Constructor.
	 *
	 * @param parent
	 *            the parent
	 */
	@Inject
	public UserCompositeAction(Composite parent) {
		super(parent, SWT.NONE);
		initListeners();
	}

	/**
	 * Method for Binding modal to widget.
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public void bindValues() {
		try {
			// Name field binding
			widgetValue = WidgetProperties.text(SWT.Modify).observe(this.txtName);
			modelValue = BeanProperties.value(User.class, User.PROPERTY_USERNAME).observe(this.userModel);
			widgetValue.addValueChangeListener(new IValueChangeListener() {
				@Override
				public void handleValueChange(ValueChangeEvent event) {
					updateButtonStatus(event);
				}

			});

			bindValue = dataBindContext.bindValue(widgetValue, modelValue);
			//full name binding
			widgetValue = WidgetProperties.text(SWT.Modify).observe(this.txtFullName);
			modelValue = BeanProperties.value(User.class, User.PROPERTY_FULLNAME).observe(this.userModel);
			bindValue = dataBindContext.bindValue(widgetValue, modelValue);
			
			//manager binding
			widgetValue = WidgetProperties.text(SWT.Modify).observe(this.txtManager);
			modelValue = BeanProperties.value(User.class, User.PROPERTY_MANAGER).observe(this.userModel);
			bindValue = dataBindContext.bindValue(widgetValue, modelValue);

			// Action check box binding
			widgetValue = WidgetProperties.selection().observe(this.activeBtn);
			modelValue = BeanProperties.value(User.class, User.PROPERTY_ACTIVE).observe(this.userModel);
			bindValue = dataBindContext.bindValue(widgetValue, modelValue);
			
            //email binding
			widgetValue = WidgetProperties.text(SWT.Modify).observe(this.txtEmail);
			modelValue = BeanProperties.value(User.class, User.PROPERTY_EMAIL).observe(this.userModel);
			bindValue = dataBindContext.bindValue(widgetValue, modelValue);

			//phone binding
			widgetValue = WidgetProperties.text(SWT.Modify).observe(this.txtTelephoneNum);
			modelValue = BeanProperties.value(User.class, User.PROPERTY_TELEPHONENUM).observe(this.userModel);
			bindValue = dataBindContext.bindValue(widgetValue, modelValue);
            //department binding
			widgetValue = WidgetProperties.text(SWT.Modify).observe(this.txtDepartment);
			modelValue = BeanProperties.value(User.class, User.PROPERTY_DEPARTMENT).observe(this.userModel);
			bindValue = dataBindContext.bindValue(widgetValue, modelValue);

			// Symbol toolItem setup
			Icon icon;
			String iconPath;
			if ((icon = this.userModel.getIcon()) != null && !XMSystemUtil.isEmpty(iconPath = icon.getIconPath())) {
				if (iconPath.contains("null")) { //$NON-NLS-1$
					toolItem.setImage(XMSystemUtil.getInstance().getImage(this.getClass(), "icons/16x16/browse.png")); //$NON-NLS-1$
				} else {
					toolItem.setImage(XMSystemUtil.getInstance().getImage(this.getClass(), iconPath, true, true));
					// Symbol toolItem binding
					widgetValue = WidgetProperties.tooltipText().observe(this.toolItem);
					modelValue = BeanProperties
							.value(User.class, User.PROPERTY_ICON + "." + Icon.PROPERTY_ICONNAME, Icon.class)
							.observe(this.userModel);
					bindValue = dataBindContext.bindValue(widgetValue, modelValue);

					// Symbol Field binding
					widgetValue = WidgetProperties.text(SWT.Modify).observe(this.txtSymbol);
					modelValue = BeanProperties
							.value(User.class, User.PROPERTY_ICON + "." + Icon.PROPERTY_ICONNAME, Icon.class)
							.observe(this.userModel);
					final UpdateValueStrategy symbolUpdate = new UpdateValueStrategy();
					symbolUpdate.setAfterGetValidator(new SymbolValidation());
					bindValue = dataBindContext.bindValue(widgetValue, modelValue, symbolUpdate, null);
					ControlDecorationSupport.create(bindValue, SWT.TOP | SWT.LEFT);
				}
			}
			// add some decorations
			ControlDecorationSupport.create(bindValue, SWT.TOP | SWT.LEFT);
		} catch (Exception e) {
			LOGGER.error("Exception occured while databinding", e);
		}
	}

	/**
	 * Method to update the button status
	 * 
	 * @param event
	 */
	@SuppressWarnings("rawtypes")
	private void updateButtonStatus(ValueChangeEvent event) {
		String name = (String) event.getObservableValue().getValue();
		if (this.saveBtn != null) {
			if (XMSystemUtil.isEmpty(name) || name.trim().length() == 0) {
				this.saveBtn.setEnabled(false);
			} else {
				this.saveBtn.setEnabled(true);
			}
		}

	}

	/**
	 * Method to open dialog.
	 *
	 * @param shell
	 *            the shell
	 */
	private void openDescDialog(final Shell shell) {
		if (userModel == null) {
			return;
		}
		if (userModel.getOperationMode() != CommonConstants.OPERATIONMODE.VIEW) {
			String text = txtDesc.getText();
			LANG_ENUM currentLocaleEnum = XMAdminUtil.getInstance().getCurrentLocaleEnum();
			userModel.setDescription(currentLocaleEnum, text);
		}
		final Map<LANG_ENUM, String> obModelMap = new HashMap<>();
		obModelMap.put(LANG_ENUM.ENGLISH, this.userModel.getDescription(LANG_ENUM.ENGLISH));
		obModelMap.put(LANG_ENUM.GERMAN, this.userModel.getDescription(LANG_ENUM.GERMAN));
		boolean isEditable = txtDesc.getEditable();
		this.controlModel = new ControlModel(this.messages.objectDescriptionLabel, obModelMap, User.DESC_LIMIT, false,
				isEditable);
		this.controlModel.initDefaultLabels(this.messages);
		final XMAdminLangTextDialog dialogArea = new XMAdminLangTextDialog(shell, controlModel);
		final int retVal = dialogArea.open();
		if (retVal == IDialogConstants.OK_ID) {
			final Map<LANG_ENUM, String> descriptionMap = this.userModel.getDescriptionMap();
			descriptionMap.put(LANG_ENUM.ENGLISH, controlModel.getObjectModel(LANG_ENUM.ENGLISH));
			descriptionMap.put(LANG_ENUM.GERMAN, controlModel.getObjectModel(LANG_ENUM.GERMAN));
			updateDescWidget();
		}
	}

	/**
	 * Method to open dialog.
	 *
	 * @param shell
	 *            the shell
	 */
	private void openRemarkDialog(final Shell shell) {
		if (userModel == null) {
			return;
		}
		if (userModel.getOperationMode() != CommonConstants.OPERATIONMODE.VIEW) {
			String text = txtRemarks.getText();
			LANG_ENUM currentLocaleEnum = XMAdminUtil.getInstance().getCurrentLocaleEnum();
			userModel.setRemarks(currentLocaleEnum, text);
		}
		final Map<LANG_ENUM, String> obModelMap = new HashMap<>();
		obModelMap.put(LANG_ENUM.ENGLISH, this.userModel.getRemarks(LANG_ENUM.ENGLISH));
		obModelMap.put(LANG_ENUM.GERMAN, this.userModel.getRemarks(LANG_ENUM.GERMAN));
		boolean isEditable = txtRemarks.getEditable();
		this.controlModel = new ControlModel(this.messages.objectRemarkLabel, obModelMap, User.REMARK_LIMIT, false,
				isEditable);
		this.controlModel.initDefaultLabels(this.messages);
		final XMAdminLangTextAreaDialog dialogArea = new XMAdminLangTextAreaDialog(shell, controlModel);
		final int retVal = dialogArea.open();
		if (retVal == IDialogConstants.OK_ID) {
			final Map<LANG_ENUM, String> remarksMap = this.userModel.getRemarksMap();
			remarksMap.put(LANG_ENUM.ENGLISH, controlModel.getObjectModel(LANG_ENUM.ENGLISH));
			remarksMap.put(LANG_ENUM.GERMAN, controlModel.getObjectModel(LANG_ENUM.GERMAN));
			updateRemarkWidget();
		}
	}

	private void initListeners() {
		// Add listener to widgets

		// Event handling when users click on desc lang links.
		this.descLink.addSelectionListener(new SelectionAdapter() {
			/**
			 * description link handler
			 */
			@Override
			public void widgetSelected(final SelectionEvent event) {
				final Link linkWidget = (Link) event.widget;
				openDescDialog(linkWidget.getShell());
			}
		});

		// Event handling when users click on remarks lang links.
		this.remarksTranslate.addSelectionListener(new SelectionAdapter() {
			/**
			 * remarks text link handler
			 */
			@Override
			public void widgetSelected(final SelectionEvent event) {
				final Link linkWidget = (Link) event.widget;
				openRemarkDialog(linkWidget.getShell());
			}
		});

		if (this.saveBtn != null) {
			this.saveBtn.addSelectionListener(new SelectionAdapter() {

				/**
				 * Save button handler
				 */
				@Override
				public void widgetSelected(final SelectionEvent event) {
					saveUserHandler();
				}

			});
		}

		if (this.cancelBtn != null) {
			this.cancelBtn.addSelectionListener(new SelectionAdapter() {

				/**
				 * Cancel button handler
				 */
				@Override
				public void widgetSelected(final SelectionEvent event) {
					cancelUserHandler();
				}
			});
		}
       /* this.txtEmail.addVerifyListener(new VerifyListener() {
        	ControlDecoration txtNameDecroator = new ControlDecoration(txtEmail, SWT.TOP);
			@Override
			public void verifyText(VerifyEvent event) {
				event.doit = false;
				final String existingText = ((Text)event.widget).getText();
				final String enteredText = event.text;
				final String updatedText = existingText + enteredText;
				final Image nameDecoratorImage = FieldDecorationRegistry.getDefault().getFieldDecoration(FieldDecorationRegistry.DEC_ERROR)
						.getImage();
				txtNameDecroator.setImage(nameDecoratorImage);
				 if (updatedText.trim().length() > 0
							&& updatedText.matches(CommonConstants.RegularExpressions.EMAIL_REGEX)) {
						event.doit = true;
						txtNameDecroator.hide();

					} else {
						txtNameDecroator.setDescriptionText("Invalid Email");
						txtNameDecroator.show();

					}
				
			}
		});*/
		this.toolItem.addSelectionListener(new SelectionAdapter() {

			/**
			 * Symbol button handler
			 */
			@Override
			public void widgetSelected(final SelectionEvent event) {
				if ((boolean) toolItem.getData("editable")) {
					final ToolItem widget = (ToolItem) event.widget;
					final IconDialog dialog = new IconDialog(widget.getParent().getShell(),
							messages.browseIconDialogTitle, messages.icontableviewerSecondColumnLabel);

					final int returnVal = dialog.open();
					if (IDialogConstants.OK_ID == returnVal) {
						final Icon checkedIcon = dialog.getCheckedIcon();
						toolItem.setImage(XMSystemUtil.getInstance().getImage(this.getClass(), checkedIcon.getIconPath(), true, true));
						userModel.setIcon(checkedIcon);
					}
				}
			}
		});
		this.txtRemarks.addModifyListener(new TextAreaModifyListener(this.lblRemarksCount, User.REMARK_LIMIT));
		
		this.txtRemarks.addVerifyListener(new VerifyListener() {

			@Override
			public void verifyText(VerifyEvent event) {
				String source = ((Text) event.widget).getText();
				final String remarkText = source.substring(0, event.start) + event.text + source.substring(event.end);
				int length = remarkText.length();
				if (length > User.REMARK_LIMIT) {
					event.doit = false;

				}
			}
		});
	}

	/**
	 * Validate Method
	 * @return Boolean
	 */
	private boolean validate() {
		if (!saveBtn.isEnabled()) {
			return false;
		}
		String userName = this.userModel.getName();
		Icon icon;
		if ((XMSystemUtil.isEmpty(userName) && (icon = this.userModel.getIcon()) != null
				&& XMSystemUtil.isEmpty(icon.getIconName()))) {
			CustomMessageDialog.openError(this.getShell(), messages.nameSymbolErrorTitle, messages.nameSymbolError);
			return false;
		}
		if (XMSystemUtil.isEmpty(userName)) {
			CustomMessageDialog.openError(this.getShell(), messages.nameErrorTitle, messages.nameError);
			return false;
		}
		if ((icon = this.userModel.getIcon()) != null && XMSystemUtil.isEmpty(icon.getIconName())) {
			CustomMessageDialog.openError(this.getShell(), messages.symbolErrorTitle, messages.symbolError);
			return false;
		}

		Users users = AdminTreeFactory.getInstance().getUsers();
		Collection<IAdminTreeChild> aaCollection = users.getUsersCollection();
		if (this.userModel.getOperationMode() == CommonConstants.OPERATIONMODE.CHANGE) {
			if (!userName.equalsIgnoreCase(this.oldModel.getName())) {
				Map<String, Long> result = aaCollection.parallelStream()
						.collect(Collectors.groupingBy(aaObj -> ((User) aaObj).getName().toUpperCase(), Collectors.counting()));
				if (result.containsKey(userName.toUpperCase())) {
					CustomMessageDialog.openError(this.getShell(), messages.existingUserNameTitle,
							messages.existingUserNameError);
					return false;
				}
			}
		} else if (this.userModel.getOperationMode() == CommonConstants.OPERATIONMODE.CREATE) {
			Map<String, Long> result = aaCollection.parallelStream()
					.collect(Collectors.groupingBy(aaObj -> ((User) aaObj).getName().toUpperCase(), Collectors.counting()));
			if (result.containsKey(userName.toUpperCase())) {
				CustomMessageDialog.openError(this.getShell(), messages.existingUserNameTitle,
						messages.existingUserNameError);
				return false;
			}

		}

		return true;
	}

	/**
	 * Method register method function for translation.
	 *
	 * @param registry
	 *            {@link MessageRegistry}
	 */
	public void registerMessages(MessageRegistry registry) {
		registry.register((text) -> {
			if (grpUser != null && !grpUser.isDisposed()) {
				grpUser.setText(text);
			}
		}, (message) -> {
			if (userModel != null) {
				if (userModel.getOperationMode() == CommonConstants.OPERATIONMODE.VIEW) {
					return message.objectGroupDisaplyLabel + " \'" + this.userModel.getName() + "\'";
				} else if (userModel.getOperationMode() == CommonConstants.OPERATIONMODE.CHANGE) {
					return message.objectGroupChangeLabel + " \'" + this.userModel.getName() + "\'";
				} /*
					 * else if (userModel.getOperationMode() ==
					 * CommonConstants.OPERATIONMODE.CREATE) { return
					 * message.userGroupCreateLabel; }
					 */
			}
			return CommonConstants.EMPTY_STR;
		});
		registry.register((text) -> {
			if (lblName != null && !lblName.isDisposed()) {
				lblName.setText(text);
			}
		}, (message) -> {
			if (lblName != null && !lblName.isDisposed()) {
				return getUpdatedWidgetText(message.objectUserNameLabel, lblName);
			}
			return CommonConstants.EMPTY_STR;
		});
		registry.register((text) -> {
			if (lblFullName != null && !lblFullName.isDisposed()) {
				lblFullName.setText(text);
			}
		}, (message) -> {
			if (lblFullName != null && !lblFullName.isDisposed()) {
				return getUpdatedWidgetText(message.objectFullNameLabel, lblFullName);
			}
			return CommonConstants.EMPTY_STR;
		});
		registry.register((text) -> {
			if (lblManager != null && !lblManager.isDisposed()) {
				lblManager.setText(text);
			}
		}, (message) -> {
			if (lblManager != null && !lblManager.isDisposed()) {
				return getUpdatedWidgetText(message.userManagerLabel, lblManager);
			}
			return CommonConstants.EMPTY_STR;
		});
		registry.register((text) -> {
			if (lblEmail != null && !lblEmail.isDisposed()) {
				lblEmail.setText(text);
			}
		}, (message) -> {
			if (lblEmail != null && !lblEmail.isDisposed()) {
				return getUpdatedWidgetText(message.userEmailLabel, lblEmail);
			}
			return CommonConstants.EMPTY_STR;
		});
		registry.register((text) -> {
			if (lblTelephoneNum != null && !lblTelephoneNum.isDisposed()) {
				lblTelephoneNum.setText(text);
			}
		}, (message) -> {
			if (lblTelephoneNum != null && !lblTelephoneNum.isDisposed()) {
				return getUpdatedWidgetText(message.userTelePhoneNumLabel, lblTelephoneNum);
			}
			return CommonConstants.EMPTY_STR;
		});
		registry.register((text) -> {
			if (lblDepartment != null && !lblDepartment.isDisposed()) {
				lblDepartment.setText(text);
			}
		}, (message) -> {
			if (lblDepartment != null && !lblDepartment.isDisposed()) {
				return getUpdatedWidgetText(message.userDepartmentLabel, lblDepartment);
			}
			return CommonConstants.EMPTY_STR;
		});
		registry.register((text) -> {
			if (lblDescrition != null && !lblDescrition.isDisposed()) {
				lblDescrition.setText(text);
			}
		}, (message) -> {
			if (lblDescrition != null && !lblDescrition.isDisposed()) {
				return getUpdatedWidgetText(message.objectDescriptionLabel, lblDescrition);
			}
			return CommonConstants.EMPTY_STR;
		});
		registry.register((text) -> {
			if (lblActive != null && !lblActive.isDisposed()) {
				lblActive.setText(text);
			}
		}, (message) -> {
			if (lblActive != null && !lblActive.isDisposed()) {
				return getUpdatedWidgetText(message.objectActiveLabel, lblActive);
			}
			return CommonConstants.EMPTY_STR;
		});
		registry.register((text) -> {
			if (lblSymbol != null && !lblSymbol.isDisposed()) {
				lblSymbol.setText(text);
			}
		}, (message) -> {
			if (lblSymbol != null && !lblSymbol.isDisposed()) {
				return getUpdatedWidgetText(message.objectSymbolLabel, lblSymbol);
			}
			return CommonConstants.EMPTY_STR;
		});
		if (saveBtn != null) {
			registry.register((text) -> {
				if (saveBtn != null && !saveBtn.isDisposed()) {
					saveBtn.setText(text);
				}
			}, (message) -> {
				if (saveBtn != null && !saveBtn.isDisposed()) {
					return getUpdatedWidgetText(message.saveButtonText, saveBtn);
				}
				return CommonConstants.EMPTY_STR;
			});
		}
		if (cancelBtn != null) {
			registry.register((text) -> {
				if (cancelBtn != null && !cancelBtn.isDisposed()) {
					cancelBtn.setText(text);
				}
			}, (message) -> {
				if (cancelBtn != null && !cancelBtn.isDisposed()) {
					return getUpdatedWidgetText(message.cancelButtonText, cancelBtn);
				}
				return CommonConstants.EMPTY_STR;
			});
		}
		registry.register((text) -> {
			if (remarksTranslate != null && !remarksTranslate.isDisposed()) {
				remarksTranslate.setText(text);
			}
		}, (message) -> {
			if (remarksTranslate != null && !remarksTranslate.isDisposed()) {
				return getUpdatedWidgetText("<a>" + message.objectTranslationLinkText + "</a>", remarksTranslate);
			}
			return CommonConstants.EMPTY_STR;
		});

		registry.register((text) -> {
			if (remarksLabel != null && !remarksLabel.isDisposed()) {
				remarksLabel.setText(text);
			}
		}, (message) -> {
			if (remarksLabel != null && !remarksLabel.isDisposed()) {
				return getUpdatedWidgetText(message.objectRemarkLabel, remarksLabel);
			}
			return CommonConstants.EMPTY_STR;
		});

		registry.register((text) -> {
			if (descLink != null && !descLink.isDisposed()) {
				descLink.setText(text);
			}
		}, (message) -> {
			if (descLink != null && !descLink.isDisposed()) {
				return getUpdatedWidgetText("<a>" + message.objectTranslationLinkText + "</a>", descLink);
			}
			return CommonConstants.EMPTY_STR;
		});

		registry.register((text) -> {
			if (txtDesc != null && !txtDesc.isDisposed()) {
				txtDesc.setText(text);
				updateDescWidget();
			}
		}, (message) -> {
			if (txtDesc != null && !txtDesc.isDisposed()) {
				final LANG_ENUM langEnum = XMAdminUtil.getInstance().getCurrentLocaleEnum();
				return this.userModel.getDescription(langEnum) == null ? CommonConstants.EMPTY_STR
						: this.userModel.getDescription(langEnum);
			}
			return CommonConstants.EMPTY_STR;
		});

		registry.register((text) -> {
			if (txtRemarks != null && !txtRemarks.isDisposed()) {
				txtRemarks.setText(text);
				updateRemarkWidget();
			}
		}, (message) -> {
			if (txtRemarks != null && !txtRemarks.isDisposed()) {
				final LANG_ENUM langEnum = XMAdminUtil.getInstance().getCurrentLocaleEnum();
				return this.userModel.getRemarks(langEnum) == null ? CommonConstants.EMPTY_STR
						: this.userModel.getRemarks(langEnum);
			}
			return CommonConstants.EMPTY_STR;
		});
	}

	/**
	 * Gets the new text based on new locale
	 * 
	 * @param message
	 *            {@link String}
	 * @param control
	 *            {@link Control}
	 * @return {@link String} new text
	 */
	private String getUpdatedWidgetText(final String message, final Control control) {
		control.requestLayout();
		control.getParent().redraw();
		control.getParent().getParent().update();
		control.getParent().getParent().getParent().update();
		return message;
	}

	
	/**
	 * ChangeUser Operation Method
	 */
	private void changeUserOperation() {
		try {
			UserController userController = new UserController();
			boolean isUpdated = userController.updateUser(mapVOObjectWithModel());
			if (isUpdated) {
				setOldModel(userModel.deepCopyUser(true, getOldModel()));
				this.userModel.setOperationMode(CommonConstants.OPERATIONMODE.VIEW);
				setOperationMode();
				this.dirty.setDirty(false);
				XMAdminUtil.getInstance().getAdminTree().refresh(true);
				XMAdminUtil.getInstance().getAdminTree().setSelection(new StructuredSelection(getOldModel()), true);
				setShowButtonBar(false);
				XMAdminUtil.getInstance().updateLogFile(
						messages.userObject + " " + "'" + this.userModel.getName() + "'" + " " + messages.objectUpdate,
						MessageType.SUCCESS);
			}
		} catch (UnauthorizedAccessException e) {
			CustomMessageDialog.openError(this.getShell(), messages.objectPermissionDialogTitle,
					messages.objectPermissionDialogMsg);
		}catch (Exception e) {
			if (e instanceof ResourceAccessException) {
				CustomMessageDialog.openError(Display.getCurrent().getActiveShell(), messages.errorDialogTitile,
						messages.serverNotReachable);
			}
			LOGGER.warn("Unable to Save User data ! " + e);
		}
	}

	private com.magna.xmbackend.vo.user.UserRequest mapVOObjectWithModel() {
		com.magna.xmbackend.vo.user.UserRequest userRequest = new com.magna.xmbackend.vo.user.UserRequest();

		userRequest.setId(this.userModel.getUserId());
		userRequest.setIconId(this.userModel.getIcon().getIconId());
		userRequest.setStatus(this.userModel.isActive() == true ? com.magna.xmbackend.vo.enums.Status.ACTIVE
				: com.magna.xmbackend.vo.enums.Status.INACTIVE);
		userRequest.setUserName(userModel.getName());
		userRequest.setFullName(userModel.getFullName());
		userRequest.setDepartment(CommonConstants.EMPTY_STR);
		userRequest.setEmail(userModel.getEmail());
		userRequest.setTelephoneNumber(userModel.getTelePhoneNum());
		
		List<com.magna.xmbackend.vo.user.UserTranslation> siteTranslationList = new ArrayList<>();
		LANG_ENUM[] lang_values = LANG_ENUM.values();
		for (int index = 0; index < lang_values.length; index++) {
			com.magna.xmbackend.vo.user.UserTranslation userTranslation = new com.magna.xmbackend.vo.user.UserTranslation();
			userTranslation.setLanguageCode(lang_values[index].getLangCode());
			userTranslation.setRemarks(this.userModel.getRemarks(lang_values[index]));
			userTranslation.setDescription(this.userModel.getDescription(lang_values[index]));
			if (this.userModel.getOperationMode() == CommonConstants.OPERATIONMODE.CHANGE) {
				String translationId = userModel.getTranslationId(lang_values[index]);
				userTranslation.setId(XMSystemUtil.isEmpty(translationId) ? CommonConstants.EMPTY_STR : translationId);
			}
			siteTranslationList.add(userTranslation);
		}

		userRequest.setUserTranslation(siteTranslationList);

		return userRequest;
	}

	
	/**
	 * Method for Sets the operation mode.
	 */
	public void setOperationMode() {
		if (this.userModel != null) {
			if (userModel.getOperationMode() == CommonConstants.OPERATIONMODE.VIEW) {
				this.txtName.setEditable(false);
				this.txtFullName.setEditable(false);
				this.txtManager.setEditable(false);
				this.activeBtn.setEnabled(false);
				this.txtEmail.setEditable(false);
				this.txtTelephoneNum.setEditable(false);
				this.txtDepartment.setEditable(false);
				this.txtDesc.setEditable(false);
				this.txtRemarks.setEditable(false);
				this.toolItem.setData("editable", false);
				setShowButtonBar(false);
			} else if (userModel.getOperationMode() == CommonConstants.OPERATIONMODE.CHANGE) {
				this.txtName.setEditable(false);
				this.txtFullName.setEditable(false);
				this.txtManager.setEditable(false);
				this.activeBtn.setEnabled(true);
				this.txtEmail.setEditable(false);
				this.txtTelephoneNum.setEditable(false);
				this.txtDepartment.setEditable(false);
				this.toolItem.setData("editable", true);
				this.txtDesc.setEditable(true);
				this.txtRemarks.setEditable(true);
				setShowButtonBar(true);
				this.dirty.setDirty(true);
			} else {
				this.txtName.setEditable(false);
				this.txtFullName.setEditable(false);
				this.txtManager.setEditable(false);
				this.activeBtn.setEnabled(false);
				this.txtEmail.setEditable(false);
				this.txtTelephoneNum.setEditable(false);
				this.txtDepartment.setEditable(false);
				this.txtDesc.setEditable(false);
				this.txtRemarks.setEditable(false);
				this.toolItem.setData("editable", false);
				setShowButtonBar(false);
			}
		}
	}

	/**
	 * Save Handler
	 */
	public void saveUserHandler() {
		saveDescAndRemarks();
		if (validate()) {
			if (!userModel.getName().isEmpty() && userModel.getIcon().getIconId() != null) {
				if (userModel.getOperationMode() == CommonConstants.OPERATIONMODE.CHANGE) {
					changeUserOperation();
				}
			}
		}

	}
	
	/**
	 * Save desc and remarks.
	 */
	private void saveDescAndRemarks() {
		LANG_ENUM currentLocaleEnum = XMAdminUtil.getInstance().getCurrentLocaleEnum();
		String desc = txtDesc.getText();
		userModel.setDescription(currentLocaleEnum, desc);

		String remarks = txtRemarks.getText();
		userModel.setRemarks(currentLocaleEnum, remarks);
	}
	
	/**
	 * 7 * Sets the dirty object.
	 *
	 * @param dirty
	 *            the new dirty object
	 */
	public void setDirtyObject(final MDirtyable dirty) {
		this.dirty = dirty;
	}

	/**
	 * Method for updating remarks.
	 */
	public void updateRemarkWidget() {
		if (this.userModel == null) {
			return;
		}
		int operationMode = this.userModel.getOperationMode();
		final LANG_ENUM currentLocaleEnum = XMAdminUtil.getInstance().getCurrentLocaleEnum();
		final String remarkForCurLocale = this.userModel.getRemarks(currentLocaleEnum) == null
				? CommonConstants.EMPTY_STR : this.userModel.getRemarks(currentLocaleEnum);
		if (operationMode != CommonConstants.OPERATIONMODE.VIEW) {
			this.txtRemarks.setText(remarkForCurLocale);
			return;
		}
		if (operationMode == CommonConstants.OPERATIONMODE.VIEW) {
			if (!XMSystemUtil.isEmpty(remarkForCurLocale)) {
				this.txtRemarks.setText(remarkForCurLocale);
				return;
			}
			final String remarkEN = this.userModel.getRemarks(LANG_ENUM.ENGLISH);
			final String remarkDE = this.userModel.getRemarks(LANG_ENUM.GERMAN);
			if (!XMSystemUtil.isEmpty(remarkEN)) {
				this.txtRemarks.setText(remarkEN);
				return;
			}

			if (!XMSystemUtil.isEmpty(remarkDE)) {
				this.txtRemarks.setText(remarkDE);
				return;
			}
		}

	}

	/**
	 * Inits the widgets.
	 */
	public void updateDescWidget() {
		if (this.userModel == null) {
			return;
		}
		int operationMode = this.userModel.getOperationMode();
		final LANG_ENUM currentLocaleEnum = XMAdminUtil.getInstance().getCurrentLocaleEnum();
		final String descForCurLocale = this.userModel.getDescription(currentLocaleEnum) == null
				? CommonConstants.EMPTY_STR : this.userModel.getDescription(currentLocaleEnum);
		if (operationMode != CommonConstants.OPERATIONMODE.VIEW) {
			this.txtDesc.setText(descForCurLocale);
			return;
		}
		if (operationMode == CommonConstants.OPERATIONMODE.VIEW) {
			if (!XMSystemUtil.isEmpty(descForCurLocale)) {
				this.txtDesc.setText(descForCurLocale);
				return;
			}
			final String descriptionEN = this.userModel.getDescription(LANG_ENUM.ENGLISH);
			final String descriptionDE = this.userModel.getDescription(LANG_ENUM.GERMAN);
			if (!XMSystemUtil.isEmpty(descriptionEN)) {
				this.txtDesc.setText(descriptionEN);
				return;
			}

			if (!XMSystemUtil.isEmpty(descriptionDE)) {
				this.txtDesc.setText(descriptionDE);
				return;
			}
		}
	}

	/**
	 * Cancel user handler.
	 */
	public void cancelUserHandler() {
		if (userModel == null) {
			dirty.setDirty(false);
			return;
		}
		String userId = CommonConstants.EMPTY_STR;
		int operationMode = this.userModel.getOperationMode();
		User oldModel = getOldModel();
		if (oldModel != null) {
			userId = oldModel.getUserId();
		}
		setUserModel(null);
		setOldModel(null);
		dirty.setDirty(false);
		final AdminTreeviewer adminTree = XMAdminUtil.getInstance().getAdminTree();
		final Users users = AdminTreeFactory.getInstance().getUsers();
		if (operationMode == CommonConstants.OPERATIONMODE.CHANGE) {
			final IStructuredSelection selection = (IStructuredSelection) adminTree.getSelection();
			final Object firstElement = selection.getFirstElement();

			if (firstElement != null && firstElement instanceof User) {
				Map<String, IAdminTreeChild> usersChildren = users.getUsersChildren();
				char startChar = (((User) firstElement).getName().toString().toLowerCase().toCharArray()[0]);
				IAdminTreeChild iAdminTreeChild = usersChildren.get(String.valueOf(startChar));
				if (iAdminTreeChild instanceof UsersNameAlphabet) {
					UsersNameAlphabet usersNameAlphabet = (UsersNameAlphabet) iAdminTreeChild;
					if (firstElement.equals(usersNameAlphabet.getUsersChildren().get(userId))) {
						adminTree.setSelection(
								new StructuredSelection(usersNameAlphabet.getUsersChildren().get(userId)), true);
					}
				}
			}
		} else {
			adminTree.setSelection(new StructuredSelection(users), true);
		}
	}

	/**
	 * set the user model from selection.
	 */
	public void setUser() {
		try {
			final AdminTreeviewer adminTree = XMAdminUtil.getInstance().getAdminTree();
			final Object selectionObj = adminTree.getSelection();
			if (selectionObj instanceof IStructuredSelection) {
				Object firstElement = ((IStructuredSelection) selectionObj).getFirstElement();
				if (firstElement instanceof User) {
					setOldModel((User) firstElement);
					User rightHandObject = getOldModel().deepCopyUser(false, null);
					setUserModel(rightHandObject);
					registerMessages(this.registry);
					bindValues();
					setOperationMode();
				}
			}
		} catch (Exception e) {
			LOGGER.warn("Unable to set user model selection ! " + e);//$NON-NLS-1$
		}

	}

	/**
	 * Sets the model.
	 *
	 * @param user
	 *            the new model
	 */
	public void setModel(final User user) {
		try {
			setOldModel(null);
			setUserModel(user);
			registerMessages(this.registry);
			bindValues();
			setOperationMode();
		} catch (Exception e) {
			LOGGER.warn("Unable to set user model ! " + e);//$NON-NLS-1$
		}

	}

	/**
	 * sets the old model.
	 */
	public void setOldModel(final User oldModel) {
		this.oldModel = oldModel;

	}

	/**
	 * Gets the old model.
	 *
	 * @return the oldModel
	 */
	public User getOldModel() {
		return oldModel;
	}

	/**
	 * sets the user model.
	 *
	 */
	public void setUserModel(final User userModel) {
		this.userModel = userModel;

	}

	/**
	 * method to get userModel
	 * 
	 * @return user model
	 */
	public User getUserModel() {
		// TODO Auto-generated method stub
		return userModel;
	}

}
