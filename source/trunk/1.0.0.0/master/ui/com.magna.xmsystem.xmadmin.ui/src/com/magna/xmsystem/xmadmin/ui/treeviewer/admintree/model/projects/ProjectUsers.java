package com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.projects;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.stream.Collectors;

import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.icon.Icon;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.refs.RelationObj;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.users.User;

/**
 * The Class ProjectUserModel.
 * 
 * @author subash.janarthanan
 * 
 */
public class ProjectUsers implements IAdminTreeChild {

	/** The project user children. */
	private Map<String, IAdminTreeChild> projectUserChildren;

	/** The icon. */
	private Icon icon;

	/** The active. */
	private boolean active;

	/** The name. */
	private String name;

	/** The parent. */
	private IAdminTreeChild parent;

	/**
	 * Instantiates a new project user model.
	 *
	 * @param parent
	 *            the parent
	 */
	public ProjectUsers(IAdminTreeChild parent) {
		this.parent = parent;
		this.projectUserChildren = new LinkedHashMap<>();
	}

	/**
	 * Add.
	 *
	 * @param projectUserChildrenId
	 *            the project user children id
	 * @param child
	 *            the child
	 * @return the i admin tree child
	 */
	public IAdminTreeChild add(final String projectUserChildrenId, final IAdminTreeChild child) {
		child.setParent(this);
		IAdminTreeChild returnVal = this.projectUserChildren.put(projectUserChildrenId, child);
		if (child instanceof RelationObj && ((RelationObj) child).getRefObject() instanceof User) {
			sort();
		}
		return returnVal;
	}

	/**
	 * Remove.
	 *
	 * @param projectUserChildrenId
	 *            the project user children id
	 * @return the i admin tree child
	 */
	public IAdminTreeChild remove(final String projectUserChildrenId) {
		return this.projectUserChildren.remove(projectUserChildrenId);
	}

	/**
	 * Method for Removes the all.
	 */
	public void removeAll() {
		this.projectUserChildren.clear();
	}
	
	/**
	 * Gets the project user children.
	 *
	 * @return the project user children
	 */
	public Map<String, IAdminTreeChild> getProjectUserChildren() {
		return projectUserChildren;
	}

	/**
	 * Gets the project user collection.
	 *
	 * @return the project user collection
	 */
	public Collection<IAdminTreeChild> getProjectUserCollection() {
		return this.projectUserChildren.values();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild#
	 * getParent()
	 */
	@Override
	public IAdminTreeChild getParent() {
		return this.parent;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild#
	 * setParent(com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.
	 * IAdminTreeChild)
	 */
	@Override
	public void setParent(IAdminTreeChild parent) {
		this.parent = parent;
	}

	/**
	 * Gets the icon.
	 *
	 * @return the icon
	 */
	public Icon getIcon() {
		return icon;
	}

	/**
	 * Sets the icon.
	 *
	 * @param icon
	 *            the new icon
	 */
	public void setIcon(Icon icon) {
		this.icon = icon;
	}

	/**
	 * Checks if is active.
	 *
	 * @return true, if is active
	 */
	public boolean isActive() {
		return active;
	}

	/**
	 * Sets the active.
	 *
	 * @param active
	 *            the new active
	 */
	public void setActive(boolean active) {
		this.active = active;
	}

	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Sets the name.
	 *
	 * @param name
	 *            the new name
	 */
	public void setName(String name) {
		this.name = name;
	}
	
	/**
	 * Sort.
	 */
	public void sort() {
		final LinkedHashMap<String, IAdminTreeChild> collect = this.projectUserChildren.entrySet().stream().sorted(
				(e1, e2) -> ((User) (((RelationObj) e1.getValue()).getRefObject())).getName()
				.compareTo(((User) (((RelationObj) e2.getValue()).getRefObject())).getName()))
				.collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e1, LinkedHashMap::new));
		this.projectUserChildren = collect;
	}

}
