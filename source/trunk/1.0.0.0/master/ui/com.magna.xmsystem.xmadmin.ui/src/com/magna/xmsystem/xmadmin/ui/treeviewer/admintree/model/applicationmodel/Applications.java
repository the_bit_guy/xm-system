package com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;

import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild;

/**
 * Class for Applications.
 *
 * @author Chiranjeevi.Akula
 */
public class Applications implements IAdminTreeChild {

	/** Member variable 'parent' for {@link IAdminTreeChild}. */
	private IAdminTreeChild parent;

	/**
	 * Member variable 'application children' for
	 * {@link Map<String,IAdminTreeChild>}.
	 */
	private final Map<String, IAdminTreeChild> applicationChildren;

	/**
	 * Constructor for Applications Class.
	 */
	public Applications() {
		this.parent = null;
		this.applicationChildren = new LinkedHashMap<>();
	}

	/**
	 * Method for Adds the.
	 *
	 * @param applicatioId
	 *            {@link String}
	 * @param child
	 *            {@link IAdminTreeChild}
	 * @return the i admin tree child {@link IAdminTreeChild}
	 */
	public IAdminTreeChild add(final String applicatioId, final IAdminTreeChild child) {
		child.setParent(this);
		return this.applicationChildren.put(applicatioId, child);
	}

	/**
	 * Method for Removes the.
	 *
	 * @param applicatioId
	 *            {@link String}
	 * @return the i admin tree child {@link IAdminTreeChild}
	 */
	public IAdminTreeChild remove(final String applicatioId) {
		return this.applicationChildren.remove(applicatioId);
	}

	/**
	 * Method for Removes the all.
	 */
	public void removeAll() {
		this.applicationChildren.clear();
	}

	/**
	 * Gets the app collection.
	 *
	 * @return the app collection
	 */
	public Collection<IAdminTreeChild> getAppCollection() {
		return this.applicationChildren.values();
	}

	/**
	 * Gets the applications.
	 *
	 * @return the applications
	 */
	public Map<String, IAdminTreeChild> getApplications() {
		return this.applicationChildren;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild#
	 * getParent()
	 */
	@Override
	public IAdminTreeChild getParent() {
		return this.parent;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild#
	 * setParent(com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.
	 * IAdminTreeChild)
	 */
	@Override
	public void setParent(final IAdminTreeChild parent) {
		this.parent = parent;
	}
}
