package com.magna.xmsystem.xmadmin.ui.importuser.dialog;

import org.eclipse.jface.viewers.ITreeContentProvider;

import com.magna.xmsystem.xmadmin.ui.importuser.wizard.model.LdapUserProperties;
import com.magna.xmsystem.xmadmin.ui.importuser.wizard.model.LdapUsers;



// TODO: Auto-generated Javadoc
/**
 * The Class UserInfoViewerContentProvider.
 * 
 * @author archita.patel
 */
public class LdapUserViewerContentProvider implements ITreeContentProvider {

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.jface.viewers.ITreeContentProvider#getChildren(java.lang.
	 * Object)
	 */
	@Override
	public Object[] getChildren(final Object parentElement) {
		return new Object[] {};
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.jface.viewers.ITreeContentProvider#getElements(java.lang.
	 * Object)
	 */
	@Override
	public Object[] getElements(final Object inputElement) {
		if (inputElement instanceof LdapUsers) {
			return ((LdapUsers) inputElement).getLdapUsers().toArray();
		}else if (inputElement instanceof LdapUserProperties) {
			return ((LdapUserProperties) inputElement).getLdapUserProperties().toArray();
		}
		return new Object[] {};
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jface.viewers.ITreeContentProvider#getParent(java.lang.
	 * Object)
	 */
	@Override
	public Object getParent(final Object arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.jface.viewers.ITreeContentProvider#hasChildren(java.lang.
	 * Object)
	 */
	@Override
	public boolean hasChildren(final Object element) {
		return getChildren(element).length > 0;
	}

}
