package com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.roles;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.stream.Collectors;

import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.refs.RelationObj;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.users.User;

/**
 * The Class RoleUsers.
 * 
 * @author subash.janarthanan
 * 
 */
public class RoleUsers implements IAdminTreeChild {

	/** The role user children. */
	private Map<String, IAdminTreeChild> roleUserChildren;

	/** The name. */
	private String name;

	/** The parent. */
	private IAdminTreeChild parent;

	/**
	 * Instantiates a new role users.
	 *
	 * @param parent
	 *            the parent
	 */
	public RoleUsers(IAdminTreeChild parent) {
		this.parent = parent;
		this.roleUserChildren = new LinkedHashMap<>();
	}

	/**
	 * Adds the.
	 *
	 * @param roleUserChildrenId
	 *            the role user children id
	 * @param child
	 *            the child
	 * @return the i admin tree child
	 */
	public IAdminTreeChild add(final String roleUserChildrenId, final IAdminTreeChild child) {
		child.setParent(this);
		IAdminTreeChild returnVal = this.roleUserChildren.put(roleUserChildrenId, child);
		if (child instanceof RelationObj && ((RelationObj) child).getRefObject() instanceof User) {
			sort();
		}
		return returnVal;
	}

	/**
	 * Method for Removes the all.
	 */
	public void removeAll() {
		this.roleUserChildren.clear();
	}

	/**
	 * Removes the.
	 *
	 * @param roleUserChildrenId
	 *            the role user children id
	 * @return the i admin tree child
	 */
	public IAdminTreeChild remove(final String roleUserChildrenId) {
		return this.roleUserChildren.remove(roleUserChildrenId);
	}

	/**
	 * Gets the role user children.
	 *
	 * @return the role user children
	 */
	public Map<String, IAdminTreeChild> getRoleUserChildren() {
		return roleUserChildren;
	}

	/**
	 * Gets the role user collection.
	 *
	 * @return the role user collection
	 */
	public Collection<IAdminTreeChild> getRoleUserCollection() {
		return this.roleUserChildren.values();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild#
	 * getParent()
	 */
	@Override
	public IAdminTreeChild getParent() {
		return this.parent;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild#
	 * setParent(com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.
	 * IAdminTreeChild)
	 */
	@Override
	public void setParent(IAdminTreeChild parent) {
		this.parent = parent;
	}

	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Sets the name.
	 *
	 * @param name
	 *            the new name
	 */
	public void setName(String name) {
		this.name = name;
	}
	
	/**
	 * Sort.
	 */
	public void sort() {
		final LinkedHashMap<String, IAdminTreeChild> collect = this.roleUserChildren.entrySet().stream().sorted(
				(e1, e2) -> ((User) (((RelationObj) e1.getValue()).getRefObject())).getName()
				.compareTo(((User) (((RelationObj) e2.getValue()).getRefObject())).getName()))
				.collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e1, LinkedHashMap::new));
		this.roleUserChildren = collect;
	}

}
