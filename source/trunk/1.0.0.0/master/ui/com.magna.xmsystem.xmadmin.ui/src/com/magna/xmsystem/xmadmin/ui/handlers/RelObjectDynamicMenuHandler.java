
package com.magna.xmsystem.xmadmin.ui.handlers;

import java.util.List;

import javax.inject.Inject;

import org.eclipse.e4.core.services.nls.Translation;
import org.eclipse.e4.ui.di.AboutToShow;
import org.eclipse.e4.ui.model.application.ui.menu.MDirectMenuItem;
import org.eclipse.e4.ui.model.application.ui.menu.MMenuElement;
import org.eclipse.e4.ui.model.application.ui.menu.MMenuFactory;
import org.eclipse.jface.viewers.IStructuredSelection;

import com.magna.xmsystem.xmadmin.message.Message;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.AdminTreeviewer;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.adminarea.AdminAreaProjectAppFixed;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.adminarea.AdminAreaProjectAppNotFixed;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.adminarea.AdminAreaProjectAppProtected;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.adminarea.AdminAreaProjectChild;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.adminarea.AdminAreaProjectStartApplications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.adminarea.AdminAreaStartApplications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.adminarea.AdminAreaUserAppFixed;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.adminarea.AdminAreaUserAppNotFixed;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.adminarea.AdminAreaUserAppProtected;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.projectapplicationmodel.ProjectAppAdminAreaChild;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.projects.ProjectAdminAreaChild;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.projects.ProjectAdminAreaProjectAppFixed;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.projects.ProjectAdminAreaProjectAppNotFixed;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.projects.ProjectAdminAreaProjectAppProtected;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.projects.ProjectAdminAreaStartApplications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.projects.ProjectUserAdminAreaChild;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.projects.ProjectUserChild;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.refs.RelationObj;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.sitemodel.SiteAdminAreaProjectChild;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.sitemodel.SiteAdminAreaProjectStartApplications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.sitemodel.SiteAdminAreaStartApplications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.sitemodel.SiteAdminAreaUserAppFixed;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.sitemodel.SiteAdminAreaUserAppNotFixed;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.sitemodel.SiteAdminAreaUserAppProtected;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.sitemodel.SiteAdminProjectAppFixed;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.sitemodel.SiteAdminProjectAppNotFixed;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.sitemodel.SiteAdminProjectAppProtected;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.sitemodel.SiteAdministrationChild;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.users.UserAdminAreaChild;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.users.UserProjectAdminAreaChild;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.users.UserProjectChild;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.users.UserStartApplications;
import com.magna.xmsystem.xmadmin.util.CommonConstants;
import com.magna.xmsystem.xmadmin.util.XMAdminUtil;

// TODO: Auto-generated Javadoc
/**
 * The Class RelObjectDynamicMenuHandler.
 */
public class RelObjectDynamicMenuHandler {

	/** Member variable 'messages' for {@link Message}. */
	@Inject
	@Translation
	private Message messages;

	/**
	 * About to show.
	 *
	 * @param items
	 *            the items
	 */
	@AboutToShow
	public void aboutToShow(List<MMenuElement> items) {
		AdminTreeviewer adminTree = XMAdminUtil.getInstance().getAdminTree();
		final IStructuredSelection selection = (IStructuredSelection) adminTree.getSelection();
		final Object firstElement = selection.getFirstElement();
		if (firstElement instanceof RelationObj) {
			setMenuIteam(items);
		}

	}

	/**
	 * Sets the menu iteam.
	 *
	 * @param items
	 *            the new menu iteam
	 */
	private void setMenuIteam(final List<MMenuElement> items) {
		AdminTreeviewer adminTree = XMAdminUtil.getInstance().getAdminTree();
		final IStructuredSelection selection = (IStructuredSelection) adminTree.getSelection();
		final Object firstElement = selection.getFirstElement();
		final RelationObj relationObj = (RelationObj) firstElement;
		if (relationObj.getContainerObj() instanceof ProjectAppAdminAreaChild
				|| relationObj.getContainerObj() instanceof UserProjectAdminAreaChild
				|| relationObj.getContainerObj() instanceof ProjectUserAdminAreaChild
				|| relationObj.getContainerObj() instanceof UserAdminAreaChild) {
			return;
		} else {
			createMenuItemForRelationObj(selection, items);
		}
	}

	/**
	 * Create menu item for relation obj.
	 *
	 * @param selections
	 *            the selections
	 * @param items
	 *            the items
	 */
	private void createMenuItemForRelationObj(IStructuredSelection selections, List<MMenuElement> items) {
		boolean isActiveState = false;
		boolean isDeActiveState = false;
		Object firstElement = selections.getFirstElement();
		final RelationObj relationObj = (RelationObj) firstElement;
		final IAdminTreeChild containerObj = relationObj.getContainerObj();
		final boolean active = relationObj.isActive();
		if (containerObj instanceof SiteAdministrationChild || containerObj instanceof SiteAdminAreaProjectChild
				|| containerObj instanceof AdminAreaProjectChild || containerObj instanceof SiteAdminAreaUserAppNotFixed
				|| containerObj instanceof SiteAdminAreaUserAppFixed
				|| containerObj instanceof SiteAdminAreaUserAppProtected
				|| containerObj instanceof AdminAreaUserAppProtected || containerObj instanceof AdminAreaUserAppNotFixed
				|| containerObj instanceof AdminAreaUserAppFixed || containerObj instanceof SiteAdminProjectAppNotFixed
				|| containerObj instanceof SiteAdminProjectAppFixed
				|| containerObj instanceof SiteAdminProjectAppProtected
				|| containerObj instanceof AdminAreaProjectAppNotFixed
				|| containerObj instanceof AdminAreaProjectAppFixed
				|| containerObj instanceof AdminAreaProjectAppProtected
				|| containerObj instanceof ProjectAdminAreaProjectAppNotFixed
				|| containerObj instanceof ProjectAdminAreaProjectAppFixed
				|| containerObj instanceof ProjectAdminAreaProjectAppProtected
				|| containerObj instanceof SiteAdminAreaStartApplications
				|| containerObj instanceof AdminAreaStartApplications
				|| containerObj instanceof ProjectAdminAreaStartApplications
				|| containerObj instanceof SiteAdminAreaProjectStartApplications
				|| containerObj instanceof AdminAreaProjectStartApplications || containerObj instanceof ProjectUserChild
				|| containerObj instanceof UserProjectChild || containerObj instanceof UserStartApplications
				|| containerObj instanceof ProjectAdminAreaChild) {
			if (active) {
				isActiveState = true;
			} else {
				isDeActiveState = true;
			}
		}
		MDirectMenuItem dynamicItem = MMenuFactory.INSTANCE.createDirectMenuItem();
		if (isActiveState) {
			dynamicItem.setLabel(messages.popupmenulabelnodeDeActive);
			dynamicItem.setContributionURI(
					CommonConstants.XMADMIN_UI_BUNDLE + "/" + DeActivateRelHandler.class.getCanonicalName());
			items.add(dynamicItem);
		} else if (isDeActiveState) {

			dynamicItem.setLabel(messages.popupmenulabelnodeActive);
			dynamicItem.setContributionURI(
					CommonConstants.XMADMIN_UI_BUNDLE + "/" + ActivateRelHandler.class.getCanonicalName());
			items.add(dynamicItem);
		}

		dynamicItem = MMenuFactory.INSTANCE.createDirectMenuItem();
		dynamicItem.setLabel(messages.popupmenulabelnodeRemoveRelation);
		dynamicItem.setContributionURI(
				CommonConstants.XMADMIN_UI_BUNDLE + "/" + RemoveRelationHandler.class.getCanonicalName());
		items.add(dynamicItem);

		if (containerObj instanceof ProjectUserChild) {
			dynamicItem = MMenuFactory.INSTANCE.createDirectMenuItem();
			dynamicItem.setLabel(messages.popupmenuExpiryDays);
			dynamicItem.setContributionURI(
					CommonConstants.XMADMIN_UI_BUNDLE + "/" + ExpiryDaysHandler.class.getCanonicalName());
			items.add(dynamicItem);
		}
	}
}