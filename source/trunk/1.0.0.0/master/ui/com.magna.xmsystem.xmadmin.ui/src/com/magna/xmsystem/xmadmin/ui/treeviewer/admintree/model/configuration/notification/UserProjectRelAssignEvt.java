package com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.notification;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.stream.Collectors;

import com.magna.xmbackend.vo.enums.NotificationEventType;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild;

/**
 * The Class UserProjectRelAssignEvt.
 * 
 * @author shashwat.anand
 */
public class UserProjectRelAssignEvt extends INotificationEvent {
	
	/** The user pro rel assign evt child. */
	private Map<String, IAdminTreeChild> userProRelAssignEvtChild;
	
	/** The parent. */
	private IAdminTreeChild parent;
	
	/**
	 * Instantiates a new user project rel assign evt.
	 */
	public UserProjectRelAssignEvt() {
		this.parent = null;
		this.eventType = NotificationEventType.USER_PROJECT_RELATION_ASSIGN;
		this.userProRelAssignEvtChild = new LinkedHashMap<>();
	}
	
	
	/**
	 * Add.
	 *
	 * @param id the id
	 * @param child the child
	 * @return the i admin tree child
	 */
	public IAdminTreeChild add(final String id, final IAdminTreeChild child) {
		child.setParent(this);
		IAdminTreeChild returnVal = this.userProRelAssignEvtChild.put(id, child);
		sort();
		return returnVal;
	}
	
	/**
	 * Remove all.
	 */
	public void removeAll() {
		this.userProRelAssignEvtChild.clear();
	}

	/* (non-Javadoc)
	 * @see com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild#getParent()
	 */
	@Override
	public IAdminTreeChild getParent() {
		return this.parent;
	}

	/* (non-Javadoc)
	 * @see com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild#setParent(com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild)
	 */
	@Override
	public void setParent(IAdminTreeChild parent) {
		this.parent = parent;
	}
	

	/**
	 * Gets the user pro rel assign evt child.
	 *
	 * @return the user pro rel assign evt child
	 */
	public Map<String, IAdminTreeChild> getUserProRelAssignEvtChild() {
		return userProRelAssignEvtChild;
	}
	
	/**
	 * Gets the user pro rel assign evt collection.
	 *
	 * @return the user pro rel assign evt collection
	 */
	public Collection<IAdminTreeChild> getUserProRelAssignEvtCollection() {
		return this.userProRelAssignEvtChild.values();
	}
	
	/**
	 * Sort.
	 */
	public void sort() {
		final LinkedHashMap<String, IAdminTreeChild> collect = this.userProRelAssignEvtChild.entrySet().stream().sorted(
				(e1, e2) -> ((UserProjectRelAssignEvtAction) e1.getValue()).getName().compareTo(((UserProjectRelAssignEvtAction) e2.getValue()).getName()))
				.collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e1, LinkedHashMap::new));
		this.userProRelAssignEvtChild = collect;
	}
}
