package com.magna.xmsystem.xmadmin.ui.handlers.toolbar;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import org.eclipse.e4.core.contexts.ContextInjectionFactory;
import org.eclipse.e4.core.contexts.IEclipseContext;
import org.eclipse.e4.core.di.annotations.CanExecute;
import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.swt.dnd.Clipboard;
import org.eclipse.swt.widgets.Display;

import com.magna.xmsystem.xmadmin.ui.parts.dnd.NodeDropListener;
import com.magna.xmsystem.xmadmin.ui.parts.dnd.ObjectTypeTransfer;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.AdminTreeviewer;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.adminarea.AdminAreaProjectAppFixed;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.adminarea.AdminAreaProjectAppNotFixed;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.adminarea.AdminAreaProjectAppProtected;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.adminarea.AdminAreaProjectApplications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.adminarea.AdminAreaProjectChild;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.adminarea.AdminAreaProjectStartApplications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.adminarea.AdminAreaProjects;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.adminarea.AdminAreaStartApplications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.adminarea.AdminAreaUserAppFixed;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.adminarea.AdminAreaUserAppNotFixed;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.adminarea.AdminAreaUserAppProtected;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.adminarea.AdminAreaUserApplications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.adminarea.AdministrationArea;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.projectapplicationmodel.ProjectApplication;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.startapplicationmodel.StartApplication;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.userapplicationmodel.UserApplication;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.adminconf.AdminMenu;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.adminconf.AdminProjectApps;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.adminconf.AdminUserApps;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.adminconf.AdminUsers;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.roles.Role;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.roles.RoleScopeObjectChild;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.roles.RoleScopeObjectUsers;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.roles.RoleScopeObjects;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.roles.RoleUsers;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.directory.Directory;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.directory.DirectoryApplications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.directory.DirectoryProjectApplications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.directory.DirectoryProjects;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.directory.DirectoryUserApplications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.directory.DirectoryUsers;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.group.projectappgroup.ProjectAppGroupProjectApps;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.group.projectappgroup.ProjectApplicationGroup;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.group.projectgroup.ProjectGroupModel;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.group.projectgroup.ProjectGroupProjects;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.group.userappgroup.UserAppGroupUserApps;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.group.userappgroup.UserApplicationGroup;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.group.usergroup.UserGroupModel;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.group.usergroup.UserGroupUsers;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.projects.Project;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.projects.ProjectAdminAreaChild;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.projects.ProjectAdminAreaProjectAppFixed;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.projects.ProjectAdminAreaProjectAppNotFixed;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.projects.ProjectAdminAreaProjectAppProtected;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.projects.ProjectAdminAreaProjectApplications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.projects.ProjectAdminAreas;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.projects.ProjectUserAAProjectAppAllowed;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.projects.ProjectUserAAProjectAppForbidden;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.projects.ProjectUserAAProjectApplications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.projects.ProjectUserAdminAreaChild;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.projects.ProjectUserChild;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.projects.ProjectUsers;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.refs.RelationObj;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.sitemodel.Site;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.sitemodel.SiteAdminAreaProjectApplications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.sitemodel.SiteAdminAreaProjectChild;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.sitemodel.SiteAdminAreaProjectStartApplications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.sitemodel.SiteAdminAreaProjects;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.sitemodel.SiteAdminAreaStartApplications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.sitemodel.SiteAdminAreaUserAppFixed;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.sitemodel.SiteAdminAreaUserAppNotFixed;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.sitemodel.SiteAdminAreaUserAppProtected;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.sitemodel.SiteAdminAreaUserApplications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.sitemodel.SiteAdminProjectAppFixed;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.sitemodel.SiteAdminProjectAppNotFixed;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.sitemodel.SiteAdminProjectAppProtected;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.sitemodel.SiteAdministrationChild;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.sitemodel.SiteAdministrations;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.users.User;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.users.UserAAUserAppAllowed;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.users.UserAAUserAppForbidden;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.users.UserAAUserApplications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.users.UserAdminAreaChild;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.users.UserProjectAAProjectAppAllowed;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.users.UserProjectAAProjectAppForbidden;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.users.UserProjectAAProjectApplications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.users.UserProjectAdminAreaChild;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.users.UserProjectChild;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.users.UserProjects;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.users.UserStartApplications;
import com.magna.xmsystem.xmadmin.util.XMAdminUtil;

/**
 * The Class PasteObjectHandler.
 * 
 * @author subash.janarthanan
 * 
 */
public class PasteObjectHandler {

	/** The node drop listener. */
	private NodeDropListener nodeDropListener;

	/** The eclipse context. */
	@Inject
	private IEclipseContext eclipseContext;

	/**
	 * Execute.
	 */
	@Execute
	public void execute() {
		final AdminTreeviewer adminTree = XMAdminUtil.getInstance().getAdminTree();
		IStructuredSelection selection = adminTree.getStructuredSelection();
		Object targetObject = selection.getFirstElement();
		Object targetObjFinal = null;
		if (targetObject instanceof RelationObj) {
			RelationObj relationObj = (RelationObj) targetObject;
			targetObjFinal = relationObj.getContainerObj();
		} else {
			targetObjFinal = targetObject;
		}
		eclipseContext.set(TreeViewer.class, adminTree);
		nodeDropListener = ContextInjectionFactory.make(NodeDropListener.class, eclipseContext);

		Clipboard clipboard = new Clipboard(Display.getCurrent());
		Object copiedObj = clipboard.getContents(ObjectTypeTransfer.getInstance());
		if (copiedObj instanceof java.util.Collection) {
			Object[] selectionArray = ((java.util.Collection<?>) copiedObj).toArray();

			List<Object> selectionElementList = new ArrayList<>();
			for (Object firstElement : selectionArray) {
				//Object firstElement = ((StructuredSelection) selectionObj).getFirstElement();
				if (firstElement instanceof RelationObj) {
					boolean flag = validateRelationObj(targetObject, firstElement);
					if (flag) {
						RelationObj relObj = (RelationObj) firstElement;
						firstElement = relObj.getRefObject();
					}
					selectionElementList.add(firstElement);
				} else {
					selectionElementList.add(firstElement);
				}
				//selectionElementList.add(((StructuredSelection) selectionObj).getFirstElement());
			}
			nodeDropListener.createRelations(selectionElementList.toArray(), targetObjFinal,true);
		}
	}

	/**
	 * Can execute.
	 *
	 * @return true, if successful
	 */
	@CanExecute
	public boolean canExecute() {
		XMAdminUtil instance = XMAdminUtil.getInstance();
		Object draggedObject = null;
		Clipboard clipboard = new Clipboard(Display.getCurrent());
		Object copiedObj = clipboard.getContents(ObjectTypeTransfer.getInstance());

		if (copiedObj instanceof java.util.Collection) {
			Object[] selectionArray = ((java.util.Collection<?>) copiedObj).toArray();
			List<Object> selectionElementList = new ArrayList<>();
			for (Object selectionObj : selectionArray) {
				selectionElementList.add(selectionObj);
			}

			draggedObject = selectionElementList.get(0);
		}
		if (instance != null && draggedObject != null) {
			final AdminTreeviewer adminTree = instance.getAdminTree();
			IStructuredSelection selection = adminTree.getStructuredSelection();
			Object selectionObject = selection.getFirstElement();
			Object targetObjFinal = null;
			if (selectionObject instanceof RelationObj) {
				RelationObj relationObj = (RelationObj) selectionObject;
				targetObjFinal = relationObj.getContainerObj();
			} else {
				targetObjFinal = selectionObject;
			}
			if (draggedObject instanceof RelationObj) {
				boolean flag = validateRelationObj(targetObjFinal, draggedObject);
				if (flag) {
					RelationObj relObj = (RelationObj) draggedObject;
					draggedObject = relObj.getRefObject();
				}
			}
			if (draggedObject instanceof AdministrationArea && (targetObjFinal instanceof SiteAdministrations
					|| targetObjFinal instanceof Site || targetObjFinal instanceof Role || targetObjFinal instanceof RoleScopeObjects
					|| targetObjFinal instanceof ProjectAdminAreas || targetObjFinal instanceof Project)) {
				return true;
			} else if (draggedObject instanceof Project && (targetObjFinal instanceof SiteAdminAreaProjects
					|| targetObjFinal instanceof SiteAdministrationChild || targetObjFinal instanceof AdminAreaProjects
					|| targetObjFinal instanceof AdministrationArea || targetObjFinal instanceof User
					|| targetObjFinal instanceof UserProjects || targetObjFinal instanceof Directory
					|| targetObjFinal instanceof DirectoryProjects || targetObjFinal instanceof ProjectGroupProjects
					|| targetObjFinal instanceof ProjectGroupModel)) {
				return true;
			} else if (draggedObject instanceof UserApplication
					&& (targetObjFinal instanceof SiteAdminAreaUserApplications
							|| targetObjFinal instanceof SiteAdminAreaUserAppNotFixed
							|| targetObjFinal instanceof SiteAdminAreaUserAppFixed
							|| targetObjFinal instanceof SiteAdminAreaUserAppProtected
							|| targetObjFinal instanceof SiteAdministrationChild
							|| targetObjFinal instanceof AdminAreaUserApplications
							|| targetObjFinal instanceof AdminAreaUserAppFixed
							|| targetObjFinal instanceof AdminAreaUserAppNotFixed
							|| targetObjFinal instanceof AdminAreaUserAppProtected
							|| targetObjFinal instanceof AdministrationArea 
							|| targetObjFinal instanceof UserAdminAreaChild
							|| targetObjFinal instanceof UserAAUserApplications
							|| targetObjFinal instanceof UserAAUserAppAllowed
							|| targetObjFinal instanceof UserAAUserAppForbidden
							|| targetObjFinal instanceof Directory
							|| targetObjFinal instanceof DirectoryApplications
							|| targetObjFinal instanceof DirectoryUserApplications
							|| targetObjFinal instanceof AdminMenu 
							|| targetObjFinal instanceof AdminUserApps
							|| targetObjFinal instanceof UserApplicationGroup
							|| targetObjFinal instanceof UserAppGroupUserApps)) {
				return true;
			} else if (draggedObject instanceof StartApplication && (targetObjFinal instanceof SiteAdministrationChild
					|| targetObjFinal instanceof SiteAdminAreaStartApplications
					|| targetObjFinal instanceof AdministrationArea
					|| targetObjFinal instanceof AdminAreaStartApplications
					|| targetObjFinal instanceof SiteAdminAreaProjectChild
					|| targetObjFinal instanceof SiteAdminAreaProjectStartApplications
					|| targetObjFinal instanceof AdminAreaProjectChild
					|| targetObjFinal instanceof AdminAreaProjectStartApplications || targetObjFinal instanceof User
					|| targetObjFinal instanceof UserStartApplications)) {
				return true;
			} else if (draggedObject instanceof ProjectApplication
					&& (targetObjFinal instanceof SiteAdminAreaProjectApplications
							|| targetObjFinal instanceof SiteAdminProjectAppNotFixed
							|| targetObjFinal instanceof SiteAdminProjectAppFixed
							|| targetObjFinal instanceof SiteAdminProjectAppProtected
							|| targetObjFinal instanceof SiteAdminAreaProjectChild
							|| targetObjFinal instanceof AdminAreaProjectApplications
							|| targetObjFinal instanceof AdminAreaProjectAppNotFixed
							|| targetObjFinal instanceof AdminAreaProjectAppFixed
							|| targetObjFinal instanceof AdminAreaProjectAppProtected
							|| targetObjFinal instanceof AdminAreaProjectChild
							|| targetObjFinal instanceof UserProjectAAProjectApplications
							|| targetObjFinal instanceof UserProjectAAProjectAppAllowed
							|| targetObjFinal instanceof UserProjectAAProjectAppForbidden
							|| targetObjFinal instanceof UserProjectChild 
							|| targetObjFinal instanceof UserProjectAdminAreaChild
							|| targetObjFinal instanceof ProjectUserChild || targetObjFinal instanceof Directory
							|| targetObjFinal instanceof DirectoryApplications
							|| targetObjFinal instanceof DirectoryProjectApplications
							|| targetObjFinal instanceof AdminMenu || targetObjFinal instanceof AdminProjectApps
							|| targetObjFinal instanceof ProjectAppGroupProjectApps
							|| targetObjFinal instanceof ProjectApplicationGroup
							|| targetObjFinal instanceof ProjectUserAdminAreaChild
							|| targetObjFinal instanceof ProjectUserAAProjectApplications
							|| targetObjFinal instanceof ProjectUserAAProjectAppAllowed
							|| targetObjFinal instanceof ProjectUserAAProjectAppForbidden
							|| targetObjFinal instanceof ProjectAdminAreaProjectApplications
							|| targetObjFinal instanceof ProjectAdminAreaProjectAppFixed
							|| targetObjFinal instanceof ProjectAdminAreaProjectAppNotFixed
							|| targetObjFinal instanceof ProjectAdminAreaProjectAppProtected
							|| targetObjFinal instanceof ProjectAdminAreaChild)) {
				return true;
			} else if (draggedObject instanceof User && (targetObjFinal instanceof Project
					|| targetObjFinal instanceof ProjectUsers || targetObjFinal instanceof Directory
					|| targetObjFinal instanceof DirectoryUsers || targetObjFinal instanceof RoleUsers
					|| targetObjFinal instanceof Role || targetObjFinal instanceof RoleScopeObjectChild
					|| targetObjFinal instanceof RoleScopeObjectUsers || targetObjFinal instanceof AdminMenu
					|| targetObjFinal instanceof AdminUsers || targetObjFinal instanceof UserGroupUsers
					|| targetObjFinal instanceof UserGroupModel)) {
				return true;
			}else if (draggedObject instanceof RelationObj) {
				IAdminTreeChild containerObj = ((RelationObj) draggedObject).getContainerObj();
				IAdminTreeChild refObject = ((RelationObj) draggedObject).getRefObject();

				IAdminTreeChild containerParent = containerObj.getParent().getParent();
				IAdminTreeChild targetParent = ((IAdminTreeChild) targetObjFinal).getParent();

				if ((targetObjFinal instanceof SiteAdminAreaUserAppNotFixed || targetObjFinal instanceof SiteAdminAreaUserAppFixed
						|| targetObjFinal instanceof SiteAdminAreaUserAppProtected || targetObjFinal instanceof AdminAreaUserAppNotFixed
						|| targetObjFinal instanceof AdminAreaUserAppFixed || targetObjFinal instanceof AdminAreaUserAppProtected)
						&& !(containerObj instanceof SiteAdminAreaUserApplications
								|| containerObj instanceof AdminAreaUserApplications)
						&& refObject instanceof UserApplication && !containerObj.getClass().equals(targetObjFinal.getClass())) {
					if (containerParent.equals(targetParent)) {
						return true;
					}
				} else if ((targetObjFinal instanceof SiteAdminProjectAppNotFixed || targetObjFinal instanceof SiteAdminProjectAppFixed
						|| targetObjFinal instanceof SiteAdminProjectAppProtected
						|| targetObjFinal instanceof AdminAreaProjectAppNotFixed || targetObjFinal instanceof AdminAreaProjectAppFixed
						|| targetObjFinal instanceof AdminAreaProjectAppProtected
						|| targetObjFinal instanceof ProjectAdminAreaProjectAppFixed
						|| targetObjFinal instanceof ProjectAdminAreaProjectAppNotFixed
						|| targetObjFinal instanceof ProjectAdminAreaProjectAppProtected)
						&& !(containerObj instanceof SiteAdminAreaProjectApplications
								|| containerObj instanceof AdminAreaProjectApplications
								|| containerObj instanceof ProjectAdminAreaProjectApplications)
						&& refObject instanceof ProjectApplication
						&& !containerObj.getClass().equals(targetObjFinal.getClass())) {
					if (containerParent.equals(targetParent)) {
						return true;
					}
				} else if ((targetObjFinal instanceof UserAAUserAppAllowed || targetObjFinal instanceof UserAAUserAppForbidden)
						&& !(containerObj instanceof UserAAUserApplications) && refObject instanceof UserApplication
						&& !containerObj.getClass().equals(targetObjFinal.getClass())) {
					if (containerParent.equals(targetParent)) {
						return true;
					}
				} else if ((targetObjFinal instanceof UserProjectAAProjectAppAllowed
						|| targetObjFinal instanceof UserProjectAAProjectAppForbidden
						|| targetObjFinal instanceof ProjectUserAAProjectAppAllowed
						|| targetObjFinal instanceof ProjectUserAAProjectAppForbidden)
						&& !(containerObj instanceof UserProjectAAProjectApplications
								|| containerObj instanceof ProjectUserAAProjectApplications)
						&& refObject instanceof ProjectApplication
						&& !containerObj.getClass().equals(targetObjFinal.getClass())) {
					if (containerParent.equals(targetParent)) {
						return true;
					}
				}
		}
		}
		return false;
	}

	/**
	 * Validate relation obj.
	 *
	 * @param target the target
	 * @param sourceObj the source obj
	 * @return true, if successful
	 */
	private boolean validateRelationObj(Object target, Object sourceObj) {
		RelationObj relObj = (RelationObj) sourceObj;
		IAdminTreeChild containerObj = relObj.getContainerObj();
		boolean flag = true;
		IAdminTreeChild containerParent = containerObj.getParent().getParent();
		IAdminTreeChild targetParent = ((IAdminTreeChild) target).getParent();

		if (((containerObj instanceof SiteAdminAreaUserAppNotFixed
				|| containerObj instanceof SiteAdminAreaUserAppFixed
				|| containerObj instanceof SiteAdminAreaUserAppProtected
				|| containerObj instanceof SiteAdminAreaUserApplications)
				&& (target instanceof SiteAdminAreaUserAppNotFixed
						|| target instanceof SiteAdminAreaUserAppFixed
						|| target instanceof SiteAdminAreaUserAppProtected
						|| target instanceof SiteAdminAreaUserApplications))) {

			if ((containerObj instanceof SiteAdminAreaUserApplications)
					&& (target instanceof SiteAdminAreaUserAppNotFixed
							|| target instanceof SiteAdminAreaUserAppFixed
							|| target instanceof SiteAdminAreaUserAppProtected)
					&& (containerObj.getParent().equals(targetParent))) {
				flag = false;
			} else if (target instanceof SiteAdminAreaUserApplications) {
				if (target.equals(containerParent) || (containerParent.equals(targetParent))) {
					flag = false;
				} else {
					flag = true;
				}
			} else if (containerParent.equals(targetParent)) {
				flag = false;
			} else {
				flag = true;
			}
		} else if (((containerObj instanceof SiteAdminProjectAppNotFixed
				|| containerObj instanceof SiteAdminProjectAppFixed
				|| containerObj instanceof SiteAdminProjectAppProtected
				|| containerObj instanceof SiteAdminAreaProjectApplications)
				&& (target instanceof SiteAdminProjectAppNotFixed || target instanceof SiteAdminProjectAppFixed
						|| target instanceof SiteAdminProjectAppProtected
						|| target instanceof SiteAdminAreaProjectApplications))) {
			if ((containerObj instanceof SiteAdminAreaProjectApplications)
					&& (target instanceof SiteAdminProjectAppNotFixed
							|| target instanceof SiteAdminProjectAppFixed
							|| target instanceof SiteAdminProjectAppProtected)
					&& (containerObj.getParent().equals(targetParent))) {
				flag = false;
			} else if (target instanceof SiteAdminAreaProjectApplications) {
				if (target.equals(containerParent) || (containerParent.equals(targetParent))) {
					flag = false;
				} else {
					flag = true;
				}
			} else if (containerParent.equals(targetParent)) {
				flag = false;
			} else {
				flag = true;
			}
		} else if (((containerObj instanceof AdminAreaUserAppNotFixed
				|| containerObj instanceof AdminAreaUserAppFixed
				|| containerObj instanceof AdminAreaUserAppProtected
				|| containerObj instanceof AdminAreaUserApplications)
				&& (target instanceof AdminAreaUserAppNotFixed || target instanceof AdminAreaUserAppFixed
						|| target instanceof AdminAreaUserAppProtected
						|| target instanceof AdminAreaUserApplications))) {
			if ((containerObj instanceof AdminAreaUserApplications)
					&& (target instanceof AdminAreaUserAppNotFixed || target instanceof AdminAreaUserAppFixed
							|| target instanceof AdminAreaUserAppProtected)
					&& (containerObj.getParent().equals(targetParent))) {
				flag = false;
			} else if (target instanceof AdminAreaUserApplications) {
				if (target.equals(containerParent) || (containerParent.equals(targetParent))) {
					flag = false;
				} else {
					flag = true;
				}
			} else if (containerParent.equals(targetParent)) {
				flag = false;
			} else {
				flag = true;
			}
		} else if (((containerObj instanceof AdminAreaProjectAppNotFixed
				|| containerObj instanceof AdminAreaProjectAppFixed
				|| containerObj instanceof AdminAreaProjectAppProtected
				|| containerObj instanceof AdminAreaProjectApplications)
				&& (target instanceof AdminAreaProjectAppNotFixed || target instanceof AdminAreaProjectAppFixed
						|| target instanceof AdminAreaProjectAppProtected
						|| target instanceof AdminAreaProjectApplications))) {
			if ((containerObj instanceof AdminAreaProjectApplications)
					&& (target instanceof AdminAreaProjectAppNotFixed
							|| target instanceof AdminAreaProjectAppFixed
							|| target instanceof AdminAreaProjectAppProtected)
					&& (containerObj.getParent().equals(targetParent))) {
				flag = false;
			} else if (target instanceof AdminAreaProjectApplications) {
				if (target.equals(containerParent) || (containerParent.equals(targetParent))) {
					flag = false;
				} else {
					flag = true;
				}
			} else if (containerParent.equals(targetParent)) {
				flag = false;
			} else {
				flag = true;
			}
		} else if (((containerObj instanceof UserAAUserAppAllowed
				|| containerObj instanceof UserAAUserAppForbidden
				|| containerObj instanceof UserAAUserApplications)
				&& (target instanceof UserAAUserAppAllowed || target instanceof UserAAUserAppForbidden
						|| target instanceof UserAAUserApplications))) {
			if ((containerObj instanceof UserAAUserApplications)
					&& (target instanceof UserAAUserAppForbidden || target instanceof UserAAUserAppAllowed)
					&& (containerObj.getParent().equals(targetParent))) {
				flag = false;
			} else if (target instanceof UserAAUserApplications) {
				if (target.equals(containerParent) || (containerParent.equals(targetParent))) {
					flag = false;
				} else {
					flag = true;
				}
			} else if (containerParent.equals(targetParent)) {
				flag = false;
			} else {
				flag = true;
			}
		} else if (((containerObj instanceof UserProjectAAProjectAppAllowed
				|| containerObj instanceof UserProjectAAProjectAppForbidden
				|| containerObj instanceof UserProjectAAProjectApplications)
				&& (target instanceof UserProjectAAProjectAppAllowed
						|| target instanceof UserProjectAAProjectAppForbidden
						|| target instanceof UserProjectAAProjectApplications))) {
			if ((containerObj instanceof UserProjectAAProjectApplications)
					&& (target instanceof UserProjectAAProjectAppAllowed
							|| target instanceof UserProjectAAProjectAppForbidden)
					&& (containerObj.getParent().equals(targetParent))) {
				flag = false;
			} else if (target instanceof UserProjectAAProjectApplications) {
				if (target.equals(containerParent) || (containerParent.equals(targetParent))) {
					flag = false;
				} else {
					flag = true;
				}
			} else if (containerParent.equals(targetParent)) {
				flag = false;
			} else {
				flag = true;
			}
		} else if (((containerObj instanceof ProjectUserAAProjectAppAllowed
				|| containerObj instanceof ProjectUserAAProjectAppForbidden
				|| containerObj instanceof ProjectUserAAProjectApplications)
				&& (target instanceof ProjectUserAAProjectAppAllowed
						|| target instanceof ProjectUserAAProjectAppForbidden
						|| target instanceof ProjectUserAAProjectApplications))) {
			if ((containerObj instanceof ProjectUserAAProjectApplications)
					&& (target instanceof AdminAreaProjectAppNotFixed
							|| target instanceof AdminAreaProjectAppFixed
							|| target instanceof AdminAreaProjectAppProtected)
					&& (containerObj.getParent().equals(targetParent))) {
				flag = false;
			} else if (target instanceof ProjectUserAAProjectApplications) {
				if (target.equals(containerParent) || (containerParent.equals(targetParent))) {
					flag = false;
				} else {
					flag = true;
				}
			} else if (containerParent.equals(targetParent)) {
				flag = false;
			} else {
				flag = true;
			}
		}else if (((containerObj instanceof ProjectAdminAreaProjectAppNotFixed || containerObj instanceof ProjectAdminAreaProjectAppFixed
				|| containerObj instanceof ProjectAdminAreaProjectAppProtected
				|| containerObj instanceof ProjectAdminAreaProjectApplications)
				&& (target instanceof ProjectAdminAreaProjectAppNotFixed || target instanceof ProjectAdminAreaProjectAppFixed
						|| target instanceof ProjectAdminAreaProjectAppProtected
						|| target instanceof ProjectAdminAreaProjectApplications))) {
			if ((containerObj instanceof ProjectAdminAreaProjectApplications)
					&& (target instanceof ProjectAdminAreaProjectAppNotFixed || target instanceof ProjectAdminAreaProjectAppFixed
							|| target instanceof ProjectAdminAreaProjectAppProtected)
					&& (containerObj.getParent().equals(targetParent))) {
				flag = false;
			} else if (target instanceof ProjectAdminAreaProjectApplications) {
				if (target.equals(containerParent) || (containerParent.equals(targetParent))) {
					flag = false;
				} else {
					flag = true;
				}
			} else if (containerParent.equals(targetParent)) {
				flag = false;
			} else {
				flag = true;
			}
		} 
		return flag;
	}
}
