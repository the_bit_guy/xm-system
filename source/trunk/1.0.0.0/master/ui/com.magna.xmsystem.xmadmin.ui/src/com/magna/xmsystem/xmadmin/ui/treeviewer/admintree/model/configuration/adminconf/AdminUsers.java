package com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.adminconf;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;

import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild;

/**
 * The Class AdminUsers.
 * 
 * @author shashwat.anand
 */
public class AdminUsers implements IAdminTreeChild {

	/** The parent. */
	private IAdminTreeChild parent;
	
	
	/** The admin user children. */
	final private Map<String, IAdminTreeChild> adminUserChildren;

	/**
	 * Instantiates a new admin users.
	 *
	 * @param parent the parent
	 */
	public AdminUsers(IAdminTreeChild parent) {
		this.parent = parent;
		this.adminUserChildren = new LinkedHashMap<>();
	}
	
	/**
	 * Adds the.
	 *
	 * @param adminUserChildrenId the admin user children id
	 * @param child the child
	 * @return the i admin tree child
	 */
	public IAdminTreeChild add(final String adminUserChildrenId, final IAdminTreeChild child) {
		child.setParent(this);
		return this.adminUserChildren.put(adminUserChildrenId, child);
	}
	
	/**
	 * Removes the.
	 *
	 * @param roleUserChildrenId the role user children id
	 * @return the i admin tree child
	 */
	public IAdminTreeChild remove(final String roleUserChildrenId) {
		return this.adminUserChildren.remove(roleUserChildrenId);
	}
	
	/**
	 * Gets the role user children.
	 *
	 * @return the role user children
	 */
	public Map<String, IAdminTreeChild> getAdminUserChildren() {
		return adminUserChildren;
	}
	
	/**
	 * Gets the role user collection.
	 *
	 * @return the role user collection
	 */
	public Collection<IAdminTreeChild> getAdminUserCollection() {
		return this.adminUserChildren.values();
	}

	/* (non-Javadoc)
	 * @see com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild#getParent()
	 */
	@Override
	public IAdminTreeChild getParent() {
		return this.parent;
	}

	/* (non-Javadoc)
	 * @see com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild#setParent(com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild)
	 */
	@Override
	public void setParent(IAdminTreeChild parent) {
		this.parent = parent;
	}
}
