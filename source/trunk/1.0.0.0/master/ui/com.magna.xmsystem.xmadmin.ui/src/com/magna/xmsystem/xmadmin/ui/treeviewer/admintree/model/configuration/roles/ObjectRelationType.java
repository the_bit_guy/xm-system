package com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.roles;

/**
 * The Enum ObjectRelationType.
 * 
 * @author shashwat.anand
 */
public enum ObjectRelationType {

	/** The administration area to site. */
	ADMINISTRATION_AREA_TO_SITE,
	
	/**  The administration area to role. */
	ADMINISTRATION_AREA_TO_ROLE,
	
	/** The admin menu config. */
	ADMIN_MENU_CONFIG,
	
	/** The directory relation. */
	DIRECTORY_RELATION,
	
	/** The group relation. */
	GROUP_RELATION,
	
	/** The project to administration area. */
	PROJECT_TO_ADMINISTRATION_AREA,
	
	/** The project application to project. */
	PROJECT_APPLICATION_TO_PROJECT,
	
	/** The project application to user project. */
	PROJECT_APPLICATION_TO_USER_PROJECT,
	
	/** The start application to administration area. */
	START_APPLICATION_TO_ADMINISTRATION_AREA,
	
	/** The start application to project. */
	START_APPLICATION_TO_PROJECT,
	
	/** The start application to user. */
	START_APPLICATION_TO_USER,
	
	/** The user application to administration area. */
	USER_APPLICATION_TO_ADMINISTRATION_AREA,
	
	/** The user application to user. */
	USER_APPLICATION_TO_USER,
	
	/** The user to project. */
	USER_TO_PROJECT,
	
	/** The user to role. */
	USER_TO_ROLE;
	
	
	/**
	 * Gets the object relation type.
	 *
	 * @return the object relation type
	 */
	public static String[] getObjectRelationType() {
		ObjectRelationType[] values = values();
		String[] returnArray = new String[values.length];
		for (int index = 0; index < values.length; index++) {
			returnArray[index] = values[index].name();
		}
		return returnArray;
	}
	
	/**
	 * Gets the object type.
	 *
	 * @param objectType the object type
	 * @return the object type
	 */
	public static ObjectRelationType getObjectType(String objectType) {
        for (ObjectRelationType value : values()) {
            if (objectType.equalsIgnoreCase(value.name())) {
                return value;
            }
        }
        return null;
    }
}
