package com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.adminarea;

import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild;

/**
 * The Class AdminAreaStartApp.
 */
public class AdminAreaStartApp implements IAdminTreeChild {
	
	/** The parent. */
	private IAdminTreeChild parent;

	/**
	 * Instantiates a new admin area start app.
	 *
	 * @param parent the parent
	 */
	public AdminAreaStartApp(final IAdminTreeChild parent) {
		this.parent = parent;
	}
	
	/**
	 * Gets the parent.
	 *
	 * @return the parent
	 */
	@Override
	public IAdminTreeChild getParent() {
		return this.parent;
	}

	/**
	 * Sets the parent.
	 *
	 * @param parent the new parent
	 */
	@Override
	public void setParent(final IAdminTreeChild parent) {
		this.parent = parent;
	}

}
