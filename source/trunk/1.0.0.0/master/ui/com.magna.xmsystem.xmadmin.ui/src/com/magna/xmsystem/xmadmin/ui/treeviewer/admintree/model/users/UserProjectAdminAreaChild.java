package com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.users;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;

import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.refs.RelationObj;

/**
 * Class for User admin area child.
 *
 * @author Chiranjeevi.Akula
 */
public class UserProjectAdminAreaChild implements IAdminTreeChild {

	/** Member variable 'parent' for {@link IAdminTreeChild}. */
	private IAdminTreeChild parent;

	/**
	 * Member variable 'user admin area children' for
	 * {@link Map<String,IAdminTreeChild>}.
	 */
	private Map<String, IAdminTreeChild> userProjectAdminAreaChildren;

	/**
	 * Constructor for UserAdminAreaChild Class.
	 *
	 * @param parent
	 *            {@link IAdminTreeChild}
	 */
	public UserProjectAdminAreaChild(final IAdminTreeChild parent) {
		this.parent = parent;
		this.userProjectAdminAreaChildren = new LinkedHashMap<>();
	}

	/**
	 * Method for Adds the fixed children.
	 *
	 * @param relationObj
	 *            {@link RelationObj}
	 */
	public void addFixedChildren(RelationObj relationObj) {
		this.userProjectAdminAreaChildren.put(UserProjectAAProjectApplications.class.getName(),
				new UserProjectAAProjectApplications(relationObj));
	}

	/**
	 * Gets the user admin area children collection.
	 *
	 * @return the user admin area children collection
	 */
	public Collection<IAdminTreeChild> getUserProjectAdminAreaChildrenCollection() {
		return this.userProjectAdminAreaChildren.values();
	}

	/**
	 * Gets the user admin area children children.
	 *
	 * @return the user admin area children children
	 */
	public Map<String, IAdminTreeChild> getUserProjectAdminAreaChildren() {
		return userProjectAdminAreaChildren;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild#
	 * getParent()
	 */
	@Override
	public IAdminTreeChild getParent() {
		return this.parent;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild#
	 * setParent(com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.
	 * IAdminTreeChild)
	 */
	@Override
	public void setParent(final IAdminTreeChild parent) {
		this.parent = parent;
	}

}
