package com.magna.xmsystem.xmadmin.ui.parts.icons;

import org.eclipse.jface.resource.ResourceManager;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.swt.graphics.Image;

import com.magna.xmsystem.dependencies.utils.XMSystemUtil;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.icon.Icon;

/**
 * Class for Label provider for Icon tree viewer
 * 
 * @author shashwat.anand
 *
 */
public class IconTreeLabelProvider extends LabelProvider {

	/**
	 * Constructor
	 */
	public IconTreeLabelProvider() {
		super();
	}

	@Override
	public String getText(Object element) {
		if (element instanceof Icon) {
			return ((Icon) element).getIconName();
		}
		return super.getText(element);
	}

	/**
	 * Get the image for object
	 * 
	 * @param element
	 *            {@link Object}
	 */
	@Override
	public Image getImage(final Object element) {
		if (element instanceof Icon) {
			return getImage(((Icon) element).getIconPath());
		}
		return null;
	}

	/**
	 * Get the image from {@link ResourceManager}
	 * 
	 * @param iconPath
	 *            {@link String} Path of Image
	 * @return {@link Image}
	 */
	private Image getImage(final String iconPath) {
		return XMSystemUtil.getInstance().getImage(this.getClass(), iconPath, true, true);
	}

	/**
	 * Method called before dispose
	 */
	@Override
	public void dispose() {
		super.dispose();
	}
}
