package com.magna.xmsystem.xmadmin.ui.parts.dndperspective;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import javax.inject.Inject;

import org.eclipse.e4.core.contexts.ContextInjectionFactory;
import org.eclipse.e4.core.contexts.IEclipseContext;
import org.eclipse.jface.util.LocalSelectionTransfer;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.DoubleClickEvent;
import org.eclipse.jface.viewers.IDoubleClickListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.nebula.widgets.pgroup.PGroup;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.ScrolledComposite;
import org.eclipse.swt.dnd.DND;
import org.eclipse.swt.dnd.DragSource;
import org.eclipse.swt.dnd.DragSourceAdapter;
import org.eclipse.swt.dnd.DragSourceEvent;
import org.eclipse.swt.dnd.DropTarget;
import org.eclipse.swt.dnd.Transfer;
import org.eclipse.swt.events.ControlAdapter;
import org.eclipse.swt.events.ControlEvent;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.layout.RowLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.magna.xmsystem.xmadmin.ui.parts.dndperspective.group.ParkingLotGroup;
import com.magna.xmsystem.xmadmin.ui.parts.dndperspective.group.ParkingLotGroupItem;
import com.magna.xmsystem.xmadmin.ui.parts.dndperspective.model.AAWarpperObj;
import com.magna.xmsystem.xmadmin.ui.parts.dndperspective.model.ProjectWrapperObj;
import com.magna.xmsystem.xmadmin.ui.parts.dndperspective.util.ParkingLotDropListener;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild;

public abstract class ParkingLotBase {
	/**
	 * Logger instance
	 */
	private static final Logger LOGGER = LoggerFactory.getLogger(ParkingLotBase.class);

	/** The eclipse context. */
	@Inject
	private IEclipseContext eclipseContext;

	protected Composite dataComposite;
	
	protected Map<String, PGroup> objectMap;
	
	protected IStructuredSelection partSelection;

	private ParkingLotObjectLabelProvider parkingLotObjectLabelProvider;
	
	/**
	 * Send event.
	 *
	 * @param selection the selection
	 */
	protected abstract void sendEvent(IStructuredSelection selection);

	/**
	 * @return the partSelection
	 */
	public IStructuredSelection getPartSelection() {
		return partSelection;
	}

	/**
	 * @param partSelection the partSelection to set
	 */
	public void setPartSelection(IStructuredSelection partSelection) {
		this.partSelection = partSelection;
	}

	/**
	 * Method to create UI
	 * 
	 * @param parent
	 *            {@link Composite}
	 */
	public void createUI(final Composite parentP) {
		try {
			this.partSelection = null;
			this.objectMap = new TreeMap<>();
			//this.resourceManager = new LocalResourceManager(JFaceResources.getResources());
			FillLayout fillLayout = new FillLayout();
			fillLayout.type = SWT.VERTICAL;
			fillLayout.marginHeight = 5;
			fillLayout.marginWidth = 5;
			parentP.setLayout(fillLayout);
			buildComponents(parentP);
			addDropListener(this.dataComposite);
			this.parkingLotObjectLabelProvider = ContextInjectionFactory.make(ParkingLotObjectLabelProvider.class, this.eclipseContext);
		} catch (Exception e) {
			LOGGER.error("Exception occured at creating UI compoents!", e); //$NON-NLS-1$
		}
	}
	
	/**
	 * Method for Builds the components.
	 *
	 * @param parent
	 *            {@link Composite}
	 */
	final private void buildComponents(final Composite parent) {
		try {
			final RowLayout rowLayout = new RowLayout();
			rowLayout.pack = false;
			rowLayout.justify = false;
			rowLayout.wrap = true;
			rowLayout.spacing = 1;

			Group group = new Group(parent, SWT.NONE);
			group.setLayout(new FillLayout());
			group.setText("Parking lot");

			ScrolledComposite scrollComposite = new ScrolledComposite(group, SWT.V_SCROLL | SWT.H_SCROLL);
			this.dataComposite = new Composite(scrollComposite, SWT.NONE);
			this.dataComposite.setData("org.eclipse.e4.ui.css.CssClassName", "ImageLabel");
			this.dataComposite.setLayout(rowLayout);

			scrollComposite.setContent(dataComposite);
			scrollComposite.setExpandVertical(true);
			scrollComposite.setExpandHorizontal(true);
			scrollComposite.addControlListener(new ControlAdapter() {
				public void controlResized(ControlEvent e) {
					Rectangle rectangle = scrollComposite.getClientArea();
					scrollComposite.setMinSize(dataComposite.computeSize(rectangle.width, SWT.DEFAULT));
				}
			});
		} catch (Exception e) {
			LOGGER.error("Execution occurred while dialog creation!", e);
		}
	}
	
	/**
	 * @return the dataComposite
	 */
	public Composite getDataComposite() {
		return dataComposite;
	}

	private void addDragListener(final TableViewer groupTableViewer) {
		// Step 1: Get JFace's LocalSelectionTransfer instance
		final LocalSelectionTransfer transfer = LocalSelectionTransfer.getTransfer();

		final DragSourceAdapter dragAdapter = new DragSourceAdapter() {
			@Override
			public void dragSetData(final DragSourceEvent event) {
				// Step 2: On drag events, create a new JFace
				// StructuredSelection
				// with the dragged control.
				IStructuredSelection structuredSelection = groupTableViewer.getStructuredSelection();
				Object[] array = structuredSelection.toArray();
				List<IStructuredSelection> elements = new ArrayList<>();
				for (Object object : array) {
					elements.add(new StructuredSelection(object));
				}
				transfer.setSelection(new StructuredSelection(elements));
			}
		};

		final DragSource dragSource = new DragSource(groupTableViewer.getControl(), DND.DROP_MOVE | DND.DROP_COPY);
		dragSource.setTransfer(new Transfer[] { transfer });
		dragSource.addDragListener(dragAdapter);
	}
	
	public PGroup getGroupForObject(final String typeName) {
		PGroup pGroup;
		if ((pGroup = objectMap.get(typeName)) == null) {
			pGroup = new ParkingLotGroup(dataComposite, SWT.SMOOTH) {
				@Override
				protected void closeHandler(SelectionEvent event) {
					ParkingLotGroupItem closeToolItem = (ParkingLotGroupItem) event.widget;
					Composite parent = closeToolItem.getParent().getParent();
					closeToolItem.getParent().dispose();
					objectMap.remove(typeName);
					parent.requestLayout();
					parent.redraw();
					parent.update();
				}
			};
			pGroup.setText(typeName);
			pGroup.setLayout(new GridLayout());
			objectMap.put(typeName, pGroup);
			TableViewer groupTableViewer = new TableViewer(pGroup);
			GridData gridData = new GridData();
			gridData.heightHint = 80;
			gridData.widthHint = 100;
			groupTableViewer.getTable().setLayoutData(gridData);
			pGroup.setData(groupTableViewer);
			groupTableViewer.setContentProvider(ArrayContentProvider.getInstance());
			groupTableViewer.setLabelProvider(this.parkingLotObjectLabelProvider);
			addDragListener(groupTableViewer);
			// Single selection was creating issue with multi selection
			groupTableViewer.addDoubleClickListener(new IDoubleClickListener() {
				
				@Override
				public void doubleClick(DoubleClickEvent event) {
					final IStructuredSelection selection = (IStructuredSelection) event.getSelection();
					// set the selection to the service
					Object firstElement = selection.getFirstElement();
					if (firstElement == null) {
						return;
					}
					if (firstElement instanceof AAWarpperObj) {
						IAdminTreeChild relObj;
						if ((relObj = ((AAWarpperObj) firstElement).getRelationObj()) != null) {
							firstElement = relObj;
						} else {
							firstElement = ((AAWarpperObj) firstElement).getAdminArea();
						}
					} else if (firstElement instanceof ProjectWrapperObj) {
						IAdminTreeChild relObj;
						if ((relObj = ((ProjectWrapperObj) firstElement).getRelationObj()) != null) {
							firstElement = relObj;
						} else {
							firstElement = ((ProjectWrapperObj) firstElement).getProject();
						}
					}
					Object source = event.getSource();
					if (source instanceof TableViewer) {
						Composite parent = ((TableViewer) source).getTable().getParent();
						if (parent instanceof PGroup) {
							String selectedGroupText = ((PGroup) parent).getText();
							Set<String> keySet = objectMap.keySet();
							for (String key : keySet) {
								PGroup pGroup = objectMap.get(key);
								TableViewer tableViewer;
								if ((tableViewer = getGroupTreeViewer(pGroup)) != null) {
									if (!(key.equals(selectedGroupText))) {
										tableViewer.setSelection(null);
									}
									if (key.equals(selectedGroupText)) {
										// send event
										setPartSelection(new StructuredSelection(firstElement));
										sendEvent(getPartSelection());
									}
								}
							}
						}
					}
				}
			});
		}
		return pGroup;
	}
	
	/**
	 * Gets the group tree viewer.
	 *
	 * @param group the group
	 * @return the group tree viewer
	 */
	public TableViewer getGroupTreeViewer(final PGroup group) {
		if (group == null) throw new IllegalArgumentException("Group is null");
		Object obj;
		if ((obj = group.getData()) != null && obj instanceof TableViewer) {
			return (TableViewer) obj;
		}
		return null;
	}
	
	/**
	 * Adds the drop listener.
	 *
	 * @param parent the parent
	 */
	private void addDropListener(final Composite parent) {
		final LocalSelectionTransfer transfer = LocalSelectionTransfer.getTransfer();
		final DropTarget dropTarget = new DropTarget(parent, DND.DROP_MOVE | DND.DROP_COPY);
		dropTarget.setTransfer(new Transfer[] { transfer });
		dropTarget.addDropListener(new ParkingLotDropListener(this));
	}
}
