package com.magna.xmsystem.xmadmin.ui.parts.adminarea;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.inject.Inject;

import org.eclipse.core.databinding.Binding;
import org.eclipse.core.databinding.DataBindingContext;
import org.eclipse.core.databinding.UpdateValueStrategy;
import org.eclipse.core.databinding.beans.BeanProperties;
import org.eclipse.core.databinding.observable.value.IObservableValue;
import org.eclipse.core.databinding.observable.value.IValueChangeListener;
import org.eclipse.core.databinding.observable.value.ValueChangeEvent;
import org.eclipse.e4.core.services.nls.Translation;
import org.eclipse.e4.ui.model.application.ui.MDirtyable;
import org.eclipse.jface.databinding.fieldassist.ControlDecorationSupport;
import org.eclipse.jface.databinding.swt.WidgetProperties;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.fieldassist.ControlDecoration;
import org.eclipse.jface.fieldassist.FieldDecorationRegistry;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.viewers.TreePath;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.VerifyEvent;
import org.eclipse.swt.events.VerifyListener;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Link;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.widgets.ToolItem;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.client.ResourceAccessException;

import com.magna.xmbackend.entities.AdminAreaTranslationTbl;
import com.magna.xmbackend.entities.AdminAreasTbl;
import com.magna.xmbackend.entities.LanguagesTbl;
import com.magna.xmbackend.vo.adminArea.AdminAreaRequest;
import com.magna.xmsystem.dependencies.customExceptions.UnauthorizedAccessException;
import com.magna.xmsystem.dependencies.utils.LANG_ENUM;
import com.magna.xmsystem.dependencies.utils.XMSystemUtil;
import com.magna.xmsystem.ui.controls.widgets.CustomMessageDialog;
import com.magna.xmsystem.xmadmin.message.Message;
import com.magna.xmsystem.xmadmin.message.MessageRegistry;
import com.magna.xmsystem.xmadmin.restclient.adminArea.AdminAreaController;
import com.magna.xmsystem.xmadmin.ui.dialogs.lang.ControlModel;
import com.magna.xmsystem.xmadmin.ui.dialogs.lang.TextAreaModifyListener;
import com.magna.xmsystem.xmadmin.ui.dialogs.lang.XMAdminLangTextAreaDialog;
import com.magna.xmsystem.xmadmin.ui.dialogs.lang.XMAdminLangTextDialog;
import com.magna.xmsystem.xmadmin.ui.parts.icons.IconDialog;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.AdminTreeviewer;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.AdminTreeFactory;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.adminarea.AdministrationArea;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.adminarea.AdministrationAreas;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.icon.Icon;
import com.magna.xmsystem.xmadmin.ui.validation.NameValidation;
import com.magna.xmsystem.xmadmin.ui.validation.StatusValidation;
import com.magna.xmsystem.xmadmin.ui.validation.SymbolValidation;
import com.magna.xmsystem.xmadmin.util.CommonConstants;
import com.magna.xmsystem.xmadmin.util.MessageType;
import com.magna.xmsystem.xmadmin.util.XMAdminUtil;

/**
 * class to AdministrationAreaCompositeAction
 * 
 * @author Archita.patel
 * 
 */

public class AdministrationAreaCompositeAction extends AdministrationAreaUI {

	/** Logger instance. */
	private static final Logger LOGGER = LoggerFactory.getLogger(AdministrationAreaCompositeAction.class);

	/** Member variable for administrartionArea model. */
	private AdministrationArea adminAreaModel;

	/** Member variable for widgetValue. */
	transient private IObservableValue<?> widgetValue;

	/** Member variable for modelValue. */
	transient private IObservableValue<?> modelValue;

	/** Member variable for message. */
	@Inject
	@Translation
	transient private Message messages;

	/**
	 * Member variable for data binding context the DataBindingContext object
	 * will manage the databindings.
	 */
	final transient DataBindingContext dataBindContext = new DataBindingContext();

	/** Member variable to store old model. */
	private AdministrationArea oldModel;

	/** Member variable for dirty. */
	transient private MDirtyable dirty;

	/** Member variable for {@link MessageRegistry}. */
	@Inject
	private MessageRegistry registry;

	/**
	 * Constructor.
	 *
	 * @param parent
	 *            the parent
	 */
	@Inject
	public AdministrationAreaCompositeAction(final Composite parent) {
		super(parent, SWT.NONE);
		initListeners();
	}

	/**
	 * Method for Binding modal to widget.
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public void bindValues() {
		try {
			// Name field binding
			widgetValue = WidgetProperties.text(SWT.Modify).observe(this.txtName);
			modelValue = BeanProperties.value(AdministrationArea.class, AdministrationArea.PROPERTY_ADMINAREANAME)
					.observe(this.adminAreaModel);
			widgetValue.addValueChangeListener(new IValueChangeListener() {
				@Override
				public void handleValueChange(ValueChangeEvent event) {
					updateButtonStatus(event);
				}
			});

			// define the UpdateValueStrategy
			UpdateValueStrategy update = new UpdateValueStrategy();
			update.setAfterGetValidator(new NameValidation(messages, StatusValidation.COMMON_NODE_VALIDATE));
			Binding bindValue = dataBindContext.bindValue(widgetValue, modelValue, update, null);
			ControlDecorationSupport.create(bindValue, SWT.TOP | SWT.LEFT);

			// Action check box binding
			widgetValue = WidgetProperties.selection().observe(this.activeBtn);
			modelValue = BeanProperties.value(AdministrationArea.class, AdministrationArea.PROPERTY_ACTIVE)
					.observe(this.adminAreaModel);
			bindValue = dataBindContext.bindValue(widgetValue, modelValue);

			// Action txtContact binding
			widgetValue = WidgetProperties.text(SWT.Modify).observe(this.txtContact);
			modelValue = BeanProperties.value(AdministrationArea.class, AdministrationArea.PROPERTY_HOTLINE_CONTACT_NUMBER)
					.observe(this.adminAreaModel);
			widgetValue.addValueChangeListener(new IValueChangeListener() {
				@Override
				public void handleValueChange(ValueChangeEvent event) {
					updateButtonStatus(event);
				}
			});

			// define the UpdateValueStrategy
			update = new UpdateValueStrategy();
			update.setAfterGetValidator(new NameValidation(messages, StatusValidation.CONTACT));
			bindValue = dataBindContext.bindValue(widgetValue, modelValue, update, null);
			ControlDecorationSupport.create(bindValue, SWT.TOP | SWT.LEFT);

			// Action txtEmail binding
			widgetValue = WidgetProperties.text(SWT.Modify).observe(this.txtEmail);
			modelValue = BeanProperties
					.value(AdministrationArea.class, AdministrationArea.PROPERTY_HOTLINE_CONTACT_EMAIL)
					.observe(this.adminAreaModel);
			widgetValue.addValueChangeListener(new IValueChangeListener() {
				@Override
				public void handleValueChange(ValueChangeEvent event) {
					updateButtonStatus(event);
				}
			});

			// txtEmail UpdateValueStrategy
			update = new UpdateValueStrategy();
			update.setAfterGetValidator(new NameValidation(messages, StatusValidation.EMAIL));
			bindValue = dataBindContext.bindValue(widgetValue, modelValue, update, null);
			ControlDecorationSupport.create(bindValue, SWT.TOP | SWT.LEFT);

			// Action timeSpinner binding
			widgetValue = WidgetProperties.selection().observe(this.timeSpinner);
			modelValue = BeanProperties
					.value(AdministrationArea.class, AdministrationArea.PROPERTY_SINGLETON_APP_TIMEOUT)
					.observe(this.adminAreaModel);
			bindValue = dataBindContext.bindValue(widgetValue, modelValue);
			ControlDecoration timeSpinnerInfoDecroator = new ControlDecoration(timeSpinner, SWT.TOP);
			final Image nameInfoDecoratorImage = FieldDecorationRegistry.getDefault()
					.getFieldDecoration(FieldDecorationRegistry.DEC_INFORMATION).getImage();
			timeSpinnerInfoDecroator.setImage(nameInfoDecoratorImage);
			timeSpinnerInfoDecroator.setDescriptionText(messages.infoTimeSpinnerValidate);
			timeSpinnerInfoDecroator.setShowOnlyOnFocus(true);

			// Symbol toolItem setup
			Icon icon;
			String iconPath;
			if ((icon = this.adminAreaModel.getIcon()) != null
					&& !XMSystemUtil.isEmpty(iconPath = icon.getIconPath())) {
				if (iconPath.contains("null")) { //$NON-NLS-1$
					toolItem.setImage(
							XMSystemUtil.getInstance().getImage(this.getClass(), "icons/16x16/browse.png")); //$NON-NLS-1$

				} else {
					toolItem.setImage(XMSystemUtil.getInstance().getImage(this.getClass(), iconPath, true, true));
					// Symbol toolItem binding
					widgetValue = WidgetProperties.tooltipText().observe(this.toolItem);
					modelValue = BeanProperties
							.value(AdministrationArea.class,
									AdministrationArea.PROPERTY_ICON + "." + Icon.PROPERTY_ICONNAME, Icon.class)
							.observe(this.adminAreaModel);
					bindValue = dataBindContext.bindValue(widgetValue, modelValue);

					// Symbol Field binding
					widgetValue = WidgetProperties.text(SWT.Modify).observe(this.txtSymbol);
					modelValue = BeanProperties
							.value(AdministrationArea.class,
									AdministrationArea.PROPERTY_ICON + "." + Icon.PROPERTY_ICONNAME, Icon.class)
							.observe(this.adminAreaModel);
					final UpdateValueStrategy symbolUpdate = new UpdateValueStrategy();
					symbolUpdate.setAfterGetValidator(new SymbolValidation());
					bindValue = dataBindContext.bindValue(widgetValue, modelValue, symbolUpdate, null);
					ControlDecorationSupport.create(bindValue, SWT.TOP | SWT.LEFT);
				}
			}
			ControlDecorationSupport.create(bindValue, SWT.TOP | SWT.LEFT);
		} catch (Exception e) {
			LOGGER.error("Exception occured while databinding", e);
		}

	}

	/**
	 * Method to update the button status
	 * 
	 * @param event
	 */
	@SuppressWarnings("rawtypes")
	private void updateButtonStatus(ValueChangeEvent event) {
		final String nameText = txtName.getText().trim();
		final String contactText = txtContact.getText().trim();
		final String emailText = txtEmail.getText().trim();
		if (this.saveBtn != null) {
			if ((XMSystemUtil.isEmpty(nameText) || nameText.trim().length() == 0)
					|| (XMSystemUtil.isEmpty(contactText) || contactText.trim().length() == 0)
					|| (XMSystemUtil.isEmpty(emailText) || emailText.trim().length() == 0)
					|| (!nameText.matches(CommonConstants.RegularExpressions.ALLOWED_NAME_REGEX))
					/*|| (!contactText.matches(CommonConstants.RegularExpressions.CONTACT_NUMBER_REGEX))*/
					|| (!emailText.matches(CommonConstants.RegularExpressions.EMAIL_REGEX))) {
				this.saveBtn.setEnabled(false);
			} else {
				this.saveBtn.setEnabled(true);
			}
		}
	}

	/**
	 * Method to open dialog
	 * 
	 * @param shell
	 *            the shell
	 */
	private void openDescDialog(final Shell shell) {

		if (adminAreaModel == null) {
			return;
		}

		if (adminAreaModel.getOperationMode() != CommonConstants.OPERATIONMODE.VIEW) {
			String text = txtDesc.getText();
			LANG_ENUM currentLocaleEnum = XMAdminUtil.getInstance().getCurrentLocaleEnum();
			adminAreaModel.setDescription(currentLocaleEnum, text);
		}

		final Map<LANG_ENUM, String> obModelMap = new HashMap<>();
		obModelMap.put(LANG_ENUM.ENGLISH, this.adminAreaModel.getDescription(LANG_ENUM.ENGLISH));
		obModelMap.put(LANG_ENUM.GERMAN, this.adminAreaModel.getDescription(LANG_ENUM.GERMAN));
		boolean isEditable = txtDesc.getEditable();
		ControlModel controlModel = new ControlModel(this.messages.objectDescriptionLabel, obModelMap,
				AdministrationArea.DESC_LIMIT, false, isEditable);
		controlModel.initDefaultLabels(this.messages);
		final XMAdminLangTextDialog dialogArea = new XMAdminLangTextDialog(shell, controlModel);
		final int retVal = dialogArea.open();
		if (retVal == IDialogConstants.OK_ID) {
			final Map<LANG_ENUM, String> descriptionMap = this.adminAreaModel.getDescriptionMap();
			descriptionMap.put(LANG_ENUM.ENGLISH, controlModel.getObjectModel(LANG_ENUM.ENGLISH));
			descriptionMap.put(LANG_ENUM.GERMAN, controlModel.getObjectModel(LANG_ENUM.GERMAN));
			updateDescWidget();
		}
	}

	/**
	 * Method to open dialog
	 * 
	 * @param shell
	 */
	private void openRemarksDialog(final Shell shell) {
		if (adminAreaModel == null) {
			return;
		}
		if (adminAreaModel.getOperationMode() != CommonConstants.OPERATIONMODE.VIEW) {
			String text = txtRemarks.getText();
			LANG_ENUM currentLocaleEnum = XMAdminUtil.getInstance().getCurrentLocaleEnum();
			adminAreaModel.setRemarks(currentLocaleEnum, text);
		}
		final Map<LANG_ENUM, String> obModelMap = new HashMap<>();
		obModelMap.put(LANG_ENUM.ENGLISH, this.adminAreaModel.getRemarks(LANG_ENUM.ENGLISH));
		obModelMap.put(LANG_ENUM.GERMAN, this.adminAreaModel.getRemarks(LANG_ENUM.GERMAN));
		boolean isEditable = txtRemarks.getEditable();

		ControlModel controlModel = new ControlModel(this.messages.objectRemarkLabel, obModelMap,
				AdministrationArea.REMARK_LIMIT, false, isEditable);
		controlModel.initDefaultLabels(this.messages);
		final XMAdminLangTextAreaDialog dialogArea = new XMAdminLangTextAreaDialog(shell, controlModel);
		final int retVal = dialogArea.open();
		if (retVal == IDialogConstants.OK_ID) {
			final Map<LANG_ENUM, String> notesMap = this.adminAreaModel.getRemarksMap();
			notesMap.put(LANG_ENUM.ENGLISH, controlModel.getObjectModel(LANG_ENUM.ENGLISH));
			notesMap.put(LANG_ENUM.GERMAN, controlModel.getObjectModel(LANG_ENUM.GERMAN));
			updateRemarksWidget();
		}
	}

	/**
	 * Add the listener to widgets.
	 */
	private void initListeners() {
		// Add listener to widgets

		// Event handling when users click on desc lang links.
		this.descLink.addSelectionListener(new SelectionAdapter() {
			/**
			 * description link handler
			 */
			@Override
			public void widgetSelected(final SelectionEvent event) {
				final Link linkWidget = (Link) event.widget;
				openDescDialog(linkWidget.getShell());
			}
		});

		// Event handling when users click on remarks lang links.
		this.remarksTranslate.addSelectionListener(new SelectionAdapter() {
			/**
			 * remarks link handler
			 */
			@Override
			public void widgetSelected(final SelectionEvent event) {
				final Link linkWidget = (Link) event.widget;
				openRemarksDialog(linkWidget.getShell());
			}
		});

		if (this.saveBtn != null) {
			this.saveBtn.addSelectionListener(new SelectionAdapter() {
				/**
				 * Save button handler
				 */
				@Override
				public void widgetSelected(final SelectionEvent event) {
					// CustomMessageDialog.openConfirm(getDisplay().getActiveShell(),
					// "API Error", "Work in Process");
					saveAdminAreaHandler();
				}

			});
		}
		if (this.cancelBtn != null) {
			this.cancelBtn.addSelectionListener(new SelectionAdapter() {

				/**
				 * Cancel button handler
				 */
				@Override
				public void widgetSelected(final SelectionEvent event) {
					cancelAdminAreaHandler();
				}
			});
		}

		this.toolItem.addSelectionListener(new SelectionAdapter() {

			/**
			 * Symbol button handler
			 */
			@Override
			public void widgetSelected(final SelectionEvent event) {

				if ((boolean) toolItem.getData("editable")) {
					final ToolItem widget = (ToolItem) event.widget;
					final IconDialog dialog = new IconDialog(widget.getParent().getShell(),
							messages.browseIconDialogTitle, messages.icontableviewerSecondColumnLabel);
					final int returnVal = dialog.open();
					if (IDialogConstants.OK_ID == returnVal) {
						final Icon checkedIcon = dialog.getCheckedIcon();
						toolItem.setImage(XMSystemUtil.getInstance().getImage(this.getClass(), checkedIcon.getIconPath(), true, true));
						adminAreaModel.setIcon(checkedIcon);
					}
				}
			}
		});
		this.txtRemarks
				.addModifyListener(new TextAreaModifyListener(this.lblRemarksCount, AdministrationArea.REMARK_LIMIT));
		
		this.txtRemarks.addVerifyListener(new VerifyListener() {

			@Override
			public void verifyText(VerifyEvent event) {
				String source = ((Text) event.widget).getText();
				final String remarkText = source.substring(0, event.start) + event.text + source.substring(event.end);
				int length = remarkText.length();
				if (length > AdministrationArea.REMARK_LIMIT) {
					event.doit = false;

				}
			}
		});
		
	}

	/**
	 * Validates the model before submit.
	 *
	 * @return boolean
	 */
	private boolean validate() {
		if (!saveBtn.isEnabled()) {
			return false;
		}
		String adminAreaName = this.adminAreaModel.getName();
		String hotlineContactNumber = this.adminAreaModel.getHotlineContactNumber();
		String hotlineContactEmail = this.adminAreaModel.getHotlineContactEmail();
		Icon icon;
		if ((XMSystemUtil.isEmpty(adminAreaName) && (icon = this.adminAreaModel.getIcon()) != null
				&& XMSystemUtil.isEmpty(icon.getIconName()))) {
			CustomMessageDialog.openError(this.getShell(), messages.nameSymbolErrorTitle, messages.nameSymbolError);
			return false;
		}
		if (XMSystemUtil.isEmpty(adminAreaName)) {
			CustomMessageDialog.openError(this.getShell(), messages.nameErrorTitle, messages.nameError);
			return false;
		}
		if ((icon = this.adminAreaModel.getIcon()) != null && XMSystemUtil.isEmpty(icon.getIconName())) {
			CustomMessageDialog.openError(this.getShell(), messages.symbolErrorTitle, messages.symbolError);
			return false;
		}
		if (XMSystemUtil.isEmpty(hotlineContactNumber)) {
			CustomMessageDialog.openError(this.getShell(), messages.hotlineContactErrorTitle, messages.hotlineContactError);
			return false;
		}
		if (XMSystemUtil.isEmpty(hotlineContactEmail)) {
			CustomMessageDialog.openError(this.getShell(), messages.hotlineEmailIDErrorTitle, messages.hotlineEmailIDError);
			return false;
		}

		AdministrationAreas administrationAreas = AdminTreeFactory.getInstance().getAdministrationAreas();
		Collection<IAdminTreeChild> aaCollection = administrationAreas.getAdministrationAreasCollection();
		if (this.adminAreaModel.getOperationMode() == CommonConstants.OPERATIONMODE.CHANGE) {
			if (!adminAreaName.equalsIgnoreCase(this.oldModel.getName())) {
				Map<String, Long> result = aaCollection.parallelStream().collect(
						Collectors.groupingBy(aaObj -> ((AdministrationArea) aaObj).getName().toUpperCase(), Collectors.counting()));
				if (result.containsKey(adminAreaName.toUpperCase())) {
					CustomMessageDialog.openError(this.getShell(), messages.existingAdminAreaNameTitle,
							messages.existingAdminAreaNameError);
					return false;
				}
			}
		} else if (this.adminAreaModel.getOperationMode() == CommonConstants.OPERATIONMODE.CREATE) {
			for (IAdminTreeChild aaObj : aaCollection) {
				if (adminAreaName.equalsIgnoreCase(((AdministrationArea) aaObj).getName())) {
					CustomMessageDialog.openError(this.getShell(), messages.existingAdminAreaNameTitle,
							messages.existingAdminAreaNameError);
					return false;
				}
			}
		}

		return true;
	}

	/**
	 * Method register method function for translation.
	 *
	 * @param registry
	 *            {@link MessageRegistry}
	 */
	public void registerMessages(final MessageRegistry registry) {
		registry.register((text) -> {
			if (grpAdminArea != null && !grpAdminArea.isDisposed()) {
				grpAdminArea.setText(text);
			}
		}, (message) -> {
			if (adminAreaModel != null) {
				if (adminAreaModel.getOperationMode() == CommonConstants.OPERATIONMODE.VIEW) {
					return message.objectGroupDisaplyLabel + " \'" + this.adminAreaModel.getName() + "\'";
				} else if (adminAreaModel.getOperationMode() == CommonConstants.OPERATIONMODE.CHANGE) {
					return message.objectGroupChangeLabel + " \'" + this.adminAreaModel.getName() + "\'";
				} else if (adminAreaModel.getOperationMode() == CommonConstants.OPERATIONMODE.CREATE) {
					return message.adminAreaGroupCreateLabel;
				}
			}
			return CommonConstants.EMPTY_STR;
		});

		registry.register((text) -> {
			if (lblName != null && !lblName.isDisposed()) {
				lblName.setText(text);
			}
		}, (message) -> {
			if (lblName != null && !lblName.isDisposed()) {
				return getUpdatedWidgetText(message.objectNameLabel, lblName);
			}
			return CommonConstants.EMPTY_STR;
		});
		registry.register((text) -> {
			if (lblDescrition != null && !lblDescrition.isDisposed()) {
				lblDescrition.setText(text);
			}
		}, (message) -> {
			if (lblDescrition != null && !lblDescrition.isDisposed()) {
				return getUpdatedWidgetText(message.objectDescriptionLabel, lblDescrition);
			}
			return CommonConstants.EMPTY_STR;
		});

		registry.register((text) -> {
			if (lblContact != null && !lblContact.isDisposed()) {
				lblContact.setText(text);
			}
		}, (message) -> {
			if (lblContact != null && !lblContact.isDisposed()) {
				return getUpdatedWidgetText(message.objectContactLabel, lblContact);
			}
			return CommonConstants.EMPTY_STR;
		});

		registry.register((text) -> {
			if (lblEmail != null && !lblEmail.isDisposed()) {
				lblEmail.setText(text);
			}
		}, (message) -> {
			if (lblEmail != null && !lblEmail.isDisposed()) {
				return getUpdatedWidgetText(message.objectEmailLabel, lblEmail);
			}
			return CommonConstants.EMPTY_STR;
		});
		registry.register((text) -> {
			if (lblTime != null && !lblTime.isDisposed()) {
				lblTime.setText(text);
			}
		}, (message) -> {
			if (lblTime != null && !lblTime.isDisposed()) {
				return getUpdatedWidgetText(message.objectTimeLabel, lblTime);
			}
			return CommonConstants.EMPTY_STR;
		});

		registry.register((text) -> {
			if (lblsecTime != null && !lblsecTime.isDisposed()) {
				lblsecTime.setText(text);
			}
		}, (message) -> {
			if (lblsecTime != null && !lblsecTime.isDisposed()) {
				return getUpdatedWidgetText(message.objectSecLabel, lblTime);
			}
			return CommonConstants.EMPTY_STR;
		});
		registry.register((text) -> {
			if (lblActive != null && !lblActive.isDisposed()) {
				lblActive.setText(text);
			}
		}, (message) -> {
			if (lblActive != null && !lblActive.isDisposed()) {
				return getUpdatedWidgetText(message.objectActiveLabel, lblActive);
			}
			return CommonConstants.EMPTY_STR;
		});
		registry.register((text) -> {
			if (lblSymbol != null && !lblSymbol.isDisposed()) {
				lblSymbol.setText(text);
			}
		}, (message) -> {
			if (lblSymbol != null && !lblSymbol.isDisposed()) {
				return getUpdatedWidgetText(message.objectSymbolLabel, lblSymbol);
			}
			return CommonConstants.EMPTY_STR;
		});
		if (saveBtn != null) {
			registry.register((text) -> {
				if (saveBtn != null && !saveBtn.isDisposed()) {
					saveBtn.setText(text);
				}
			}, (message) -> {
				if (saveBtn != null && !saveBtn.isDisposed()) {
					return getUpdatedWidgetText(message.saveButtonText, saveBtn);
				}
				return CommonConstants.EMPTY_STR;
			});
		}
		if (cancelBtn != null) {
			registry.register((text) -> {
				if (cancelBtn != null && !cancelBtn.isDisposed()) {
					cancelBtn.setText(text);
				}
			}, (message) -> {
				if (cancelBtn != null && !cancelBtn.isDisposed()) {
					return getUpdatedWidgetText(message.cancelButtonText, cancelBtn);
				}
				return CommonConstants.EMPTY_STR;
			});
		}
		registry.register((text) -> {
			if (remarksTranslate != null && !remarksTranslate.isDisposed()) {
				remarksTranslate.setText(text);
			}
		}, (message) -> {
			if (remarksTranslate != null && !remarksTranslate.isDisposed()) {
				return getUpdatedWidgetText("<a>" + message.objectTranslationLinkText + "</a>", remarksTranslate);
			}
			return CommonConstants.EMPTY_STR;
		});

		registry.register((text) -> {
			if (remarksLabel != null && !remarksLabel.isDisposed()) {
				remarksLabel.setText(text);
			}
		}, (message) -> {
			if (remarksLabel != null && !remarksLabel.isDisposed()) {
				return getUpdatedWidgetText(message.objectRemarkLabel, remarksLabel);
			}
			return CommonConstants.EMPTY_STR;
		});

		registry.register((text) -> {
			if (descLink != null && !descLink.isDisposed()) {
				descLink.setText(text);
			}
		}, (message) -> {
			if (descLink != null && !descLink.isDisposed()) {
				return getUpdatedWidgetText("<a>" + message.objectTranslationLinkText + "</a>", descLink);
			}
			return CommonConstants.EMPTY_STR;
		});

		registry.register((text) -> {
			if (txtDesc != null && !txtDesc.isDisposed()) {
				txtDesc.setText(text);
				updateDescWidget();
			}
		}, (message) -> {
			if (this.adminAreaModel != null && txtDesc != null && !txtDesc.isDisposed()) {
				final LANG_ENUM langEnum = XMAdminUtil.getInstance().getCurrentLocaleEnum();
				return this.adminAreaModel.getDescription(langEnum) == null ? CommonConstants.EMPTY_STR
						: this.adminAreaModel.getDescription(langEnum);
			}
			return CommonConstants.EMPTY_STR;
		});

		registry.register((text) -> {
			if (txtRemarks != null && !txtRemarks.isDisposed()) {
				txtRemarks.setText(text);
				updateRemarksWidget();
			}
		}, (message) -> {
			if (this.adminAreaModel != null && txtRemarks != null && !txtRemarks.isDisposed()) {
				final LANG_ENUM langEnum = XMAdminUtil.getInstance().getCurrentLocaleEnum();
				return this.adminAreaModel.getRemarks(langEnum) == null ? CommonConstants.EMPTY_STR
						: this.adminAreaModel.getRemarks(langEnum);
			}
			return CommonConstants.EMPTY_STR;
		});

	}

	/**
	 * Gets the new text based on new locale
	 * 
	 * @param message
	 *            {@link String}
	 * @param control
	 *            {@link Control}
	 * @return {@link String} new text
	 */
	private String getUpdatedWidgetText(final String message, final Control control) {
		control.requestLayout();
		control.getParent().redraw();
		control.getParent().getParent().update();
		control.getParent().getParent().getParent().update();
		return message;
	}

	/**
	 * Method for Create the AdministrartionArea operation.
	 */
	private void createAdministrartionAreaOperation() {

		try {
			AdminAreaController adminAreaController = new AdminAreaController();
			AdminAreasTbl adminAreaVo = adminAreaController.createAdminArea(mapVOObjectWithModel());
			String adminAreaId = adminAreaVo.getAdminAreaId();
			if (!XMSystemUtil.isEmpty(adminAreaId)) {
				this.adminAreaModel.setAdministrationAreaId(adminAreaId);
				Collection<AdminAreaTranslationTbl> adminAreaTranslationTblCollection = adminAreaVo
						.getAdminAreaTranslationTblCollection();
				for (AdminAreaTranslationTbl adminAreaTranslationTbl : adminAreaTranslationTblCollection) {
					String adminAreaTranslationId = adminAreaTranslationTbl.getAdminAreaTranslationId();
					LanguagesTbl languageCode = adminAreaTranslationTbl.getLanguageCode();
					LANG_ENUM langEnum = LANG_ENUM.getLangEnum(languageCode.getLanguageCode());
					this.adminAreaModel.setTranslationId(langEnum, adminAreaTranslationId);
				}

				AdminTreeFactory instance = AdminTreeFactory.getInstance();
				// Attach model to old model
				if (oldModel == null) { // Attach this to tree
					setOldModel(adminAreaModel.deepCopyAdminArea(false, null));	
					instance.getAdministrationAreas().add(adminAreaId, getOldModel());
				}
				this.dirty.setDirty(false);
				AdminTreeviewer adminTree = XMAdminUtil.getInstance().getAdminTree();
				adminTree.refresh(true);

				adminTree.setSelection(new StructuredSelection(instance.getAdministrationAreas()), true);
				TreePath[] selectionPaths = adminTree.getStructuredSelection().getPaths();
				if (selectionPaths != null && selectionPaths.length > 0) {
					adminTree.setExpandedState(selectionPaths[0], true);
				}

				adminTree.setSelection(new StructuredSelection(getOldModel()), true);
				XMAdminUtil.getInstance().updateLogFile(messages.administrationAreaObject + " " + "'"
						+ this.adminAreaModel.getName() + "'" + " " + messages.objectCreate,
						MessageType.SUCCESS);
			}
		} catch (UnauthorizedAccessException e) {
			CustomMessageDialog.openError(this.getShell(), messages.objectPermissionDialogTitle,
					messages.objectPermissionDialogMsg);
		}catch (Exception e) {
			if (e instanceof ResourceAccessException) {
				CustomMessageDialog.openError(Display.getCurrent().getActiveShell(), messages.errorDialogTitile,
						messages.serverNotReachable);
			}
			LOGGER.warn("Unable to Save data ! " + e);
		}
	}

	/**
	 * Method for Change AdministrationArea operation.
	 */
	private void changeAdministrationAreaOperation() {
		try {
			AdminAreaController adminAreaController = new AdminAreaController();
			boolean isUpdated = adminAreaController.updateAdminArea(mapVOObjectWithModel());
			if (isUpdated) {
				setOldModel(adminAreaModel.deepCopyAdminArea(true, getOldModel()));
				this.adminAreaModel.setOperationMode(CommonConstants.OPERATIONMODE.VIEW);
				setOperationMode();
				this.dirty.setDirty(false);
				final AdministrationAreas administrationAreas = AdminTreeFactory.getInstance().getAdministrationAreas();
				administrationAreas.sort();
				XMAdminUtil.getInstance().getAdminTree().refresh(true);
				XMAdminUtil.getInstance().getAdminTree().setSelection(new StructuredSelection(getOldModel()), true);
				XMAdminUtil.getInstance().updateLogFile(messages.administrationAreaObject + " " + "'"
						+ this.adminAreaModel.getName() + "'" + " " + messages.objectUpdate,
						MessageType.SUCCESS);
			}
		} catch (UnauthorizedAccessException e) {
			CustomMessageDialog.openError(this.getShell(), messages.objectPermissionDialogTitle,
					messages.objectPermissionDialogMsg);
		}catch (Exception e) {
			if (e instanceof ResourceAccessException) {
				CustomMessageDialog.openError(Display.getCurrent().getActiveShell(), messages.errorDialogTitile,
						messages.serverNotReachable);
			}
			LOGGER.warn("Unable to Update Administration Area data ! " + e);
		}
	}

	/**
	 * Method for Map VO object with model.
	 *
	 * @return the com.magna.xmbackend.vo.admin area. admin area request
	 *         {@link AdminAreaRequest}
	 */
	private com.magna.xmbackend.vo.adminArea.AdminAreaRequest mapVOObjectWithModel() {
		com.magna.xmbackend.vo.adminArea.AdminAreaRequest adminAreaRequest = new com.magna.xmbackend.vo.adminArea.AdminAreaRequest();

		adminAreaRequest.setId(this.adminAreaModel.getAdministrationAreaId());
		adminAreaRequest.setName(this.adminAreaModel.getName());
		adminAreaRequest.setIconId(this.adminAreaModel.getIcon().getIconId());
		adminAreaRequest.setHotlineContactEmail(this.adminAreaModel.getHotlineContactEmail());
		adminAreaRequest.setHotlineContactNo(this.adminAreaModel.getHotlineContactNumber());
		Long sigletonAppTimeout = this.adminAreaModel.getSingletonAppTimeout();

		if (sigletonAppTimeout != null) {
			adminAreaRequest.setSingleTonAppTimeOut(sigletonAppTimeout);
		}
		adminAreaRequest.setStatus(this.adminAreaModel.isActive() == true ? com.magna.xmbackend.vo.enums.Status.ACTIVE.name()
				: com.magna.xmbackend.vo.enums.Status.INACTIVE.name());

		List<com.magna.xmbackend.vo.adminArea.AdminAreaTranslation> adminAreaTranslationList = new ArrayList<>();
		LANG_ENUM[] lang_values = LANG_ENUM.values();
		for (int index = 0; index < lang_values.length; index++) {
			com.magna.xmbackend.vo.adminArea.AdminAreaTranslation adminAreaTranslation = new com.magna.xmbackend.vo.adminArea.AdminAreaTranslation();
			adminAreaTranslation.setLanguageCode(lang_values[index].getLangCode());
			adminAreaTranslation.setRemarks(this.adminAreaModel.getRemarks(lang_values[index]));
			adminAreaTranslation.setDescription(this.adminAreaModel.getDescription(lang_values[index]));
			if (this.adminAreaModel.getOperationMode() == CommonConstants.OPERATIONMODE.CHANGE) {
				String translationId = adminAreaModel.getTranslationId(lang_values[index]);
				adminAreaTranslation
						.setId(XMSystemUtil.isEmpty(translationId) ? CommonConstants.EMPTY_STR : translationId);
			}
			adminAreaTranslationList.add(adminAreaTranslation);
		}

		adminAreaRequest.setAdminAreaTranslations(adminAreaTranslationList);

		return adminAreaRequest;
	}

	/**
	 * Method to set OperationMode.
	 */
	public void setOperationMode() {
		if (this.adminAreaModel != null) {
			if (adminAreaModel.getOperationMode() == CommonConstants.OPERATIONMODE.VIEW) {
				this.txtName.setEditable(false);
				this.activeBtn.setEnabled(false);
				this.txtDesc.setEditable(false);
				this.txtContact.setEditable(false);
				this.txtEmail.setEditable(false);
				this.timeSpinner.setEnabled(false);
				this.txtRemarks.setEditable(false);
				this.toolItem.setData("editable", false);
				setShowButtonBar(false);
			} else if (adminAreaModel.getOperationMode() == CommonConstants.OPERATIONMODE.CREATE) {
				this.txtName.setEditable(true);
				this.activeBtn.setEnabled(true);
				this.saveBtn.setEnabled(false);
				this.txtDesc.setEditable(true);
				this.txtContact.setEditable(true);
				this.txtEmail.setEditable(true);
				this.timeSpinner.setEnabled(true);
				this.txtRemarks.setEditable(true);
				this.toolItem.setData("editable", true);
				setShowButtonBar(true);
				this.dirty.setDirty(true);
			} else if (adminAreaModel.getOperationMode() == CommonConstants.OPERATIONMODE.CHANGE) {
				this.txtName.setEditable(true);
				this.activeBtn.setEnabled(true);
				this.toolItem.setData("editable", true);
				this.txtDesc.setEditable(true);
				this.txtContact.setEditable(true);
				this.txtEmail.setEditable(true);
				this.timeSpinner.setEnabled(true);
				this.txtRemarks.setEditable(true);
				setShowButtonBar(true);
				this.dirty.setDirty(true);
			} else {
				this.txtName.setEditable(false);
				this.activeBtn.setEnabled(false);
				this.txtDesc.setEditable(false);
				this.txtContact.setEditable(false);
				this.txtEmail.setEditable(false);
				this.timeSpinner.setEnabled(false);
				this.txtRemarks.setEditable(false);
				this.toolItem.setData("editable", false);
				setShowButtonBar(false);
			}
		}
	}

	/**
	 * Save Handler
	 */
	public void saveAdminAreaHandler() {
		saveDescAndRemarks();
		if (validate()) {
			if (!adminAreaModel.getName().isEmpty() && adminAreaModel.getIcon().getIconId() != null) {
				if (adminAreaModel.getOperationMode() == CommonConstants.OPERATIONMODE.CREATE) {
					createAdministrartionAreaOperation();
				} else if (adminAreaModel.getOperationMode() == CommonConstants.OPERATIONMODE.CHANGE) {
					changeAdministrationAreaOperation();
				}
			}
		}
	}

	/**
	 * Save desc and remarks.
	 */
	private void saveDescAndRemarks() {
		LANG_ENUM currentLocaleEnum = XMAdminUtil.getInstance().getCurrentLocaleEnum();
		String desc = txtDesc.getText();
		adminAreaModel.setDescription(currentLocaleEnum, desc);
		String remarks = txtRemarks.getText();
		adminAreaModel.setRemarks(currentLocaleEnum, remarks);
	}

	/**
	 * Sets the dirty object.
	 *
	 * @param dirty
	 *            the new dirty object
	 */
	public void setDirtyObject(final MDirtyable dirty) {
		this.dirty = dirty;
	}

	/**
	 * Method for updating Remarks
	 */
	public void updateRemarksWidget() {
		if (this.adminAreaModel == null) {
			return;
		}
		int operationMode = this.adminAreaModel.getOperationMode();
		final LANG_ENUM currentLocaleEnum = XMAdminUtil.getInstance().getCurrentLocaleEnum();
		final String remarkForCurLocale = this.adminAreaModel.getRemarks(currentLocaleEnum) == null
				? CommonConstants.EMPTY_STR : this.adminAreaModel.getRemarks(currentLocaleEnum);
		if (operationMode != CommonConstants.OPERATIONMODE.VIEW) {
			this.txtRemarks.setText(remarkForCurLocale);
			return;
		}
		if (operationMode == CommonConstants.OPERATIONMODE.VIEW) {
			if (!XMSystemUtil.isEmpty(remarkForCurLocale)) {
				this.txtRemarks.setText(remarkForCurLocale);
				return;
			}
			final String remarkEN = this.adminAreaModel.getRemarks(LANG_ENUM.ENGLISH);
			final String remarkDE = this.adminAreaModel.getRemarks(LANG_ENUM.GERMAN);
			if (!XMSystemUtil.isEmpty(remarkEN)) {
				this.txtRemarks.setText(remarkEN);
				return;
			}

			if (!XMSystemUtil.isEmpty(remarkDE)) {
				this.txtRemarks.setText(remarkDE);
				return;
			}
		}
	}

	/**
	 * Inits the widgets.
	 */
	public void updateDescWidget() {
		if (this.adminAreaModel == null) {
			return;
		}
		int operationMode = this.adminAreaModel.getOperationMode();
		final LANG_ENUM currentLocaleEnum = XMAdminUtil.getInstance().getCurrentLocaleEnum();
		final String descForCurLocale = this.adminAreaModel.getDescription(currentLocaleEnum) == null
				? CommonConstants.EMPTY_STR : this.adminAreaModel.getDescription(currentLocaleEnum);
		if (operationMode != CommonConstants.OPERATIONMODE.VIEW) {
			this.txtDesc.setText(descForCurLocale);
			return;
		}
		if (operationMode == CommonConstants.OPERATIONMODE.VIEW) {
			if (!XMSystemUtil.isEmpty(descForCurLocale)) {
				this.txtDesc.setText(descForCurLocale);
				return;
			}
			final String descriptionEN = this.adminAreaModel.getDescription(LANG_ENUM.ENGLISH);
			final String descriptionDE = this.adminAreaModel.getDescription(LANG_ENUM.GERMAN);
			if (!XMSystemUtil.isEmpty(descriptionEN)) {
				this.txtDesc.setText(descriptionEN);
				return;
			}

			if (!XMSystemUtil.isEmpty(descriptionDE)) {
				this.txtDesc.setText(descriptionDE);
				return;
			}
		}
	}

	/**
	 * Cancel adminArea handler.
	 */
	public void cancelAdminAreaHandler() {
		if (this.adminAreaModel == null) {
			dirty.setDirty(false);
			return;
		}
		String adminAreaId = CommonConstants.EMPTY_STR;
		int operationMode = this.adminAreaModel.getOperationMode();
		AdministrationArea oldModel = getOldModel();
		if (oldModel != null) {
			adminAreaId = oldModel.getAdministrationAreaId();
		}
		setAdministrationAreaModel(null);
		setOldModel(null);
		this.saveBtn.setEnabled(true);
		dirty.setDirty(false);
		final AdminTreeviewer adminTree = XMAdminUtil.getInstance().getAdminTree();
		final AdministrationAreas adminAreas = AdminTreeFactory.getInstance().getAdministrationAreas();
		if (operationMode == CommonConstants.OPERATIONMODE.CHANGE) {
			final IStructuredSelection selection = (IStructuredSelection) adminTree.getSelection();
			final Object firstElement = selection.getFirstElement();
			if (firstElement != null
					&& firstElement.equals(adminAreas.getAdminstrationAreasChildren().get(adminAreaId))) {
				adminTree.setSelection(
						new StructuredSelection(adminAreas.getAdminstrationAreasChildren().get(adminAreaId)), true);
			}
		} else {
			adminTree.setSelection(new StructuredSelection(adminAreas), true);
		}

	}

	/**
	 * set the AdministrartionArea model from selection.
	 */
	public void setAdministrartionArea() {
		try {
			final AdminTreeviewer adminTree = XMAdminUtil.getInstance().getAdminTree();
			final Object selectionObj = adminTree.getSelection();
			if (selectionObj instanceof IStructuredSelection) {
				Object firstElement = ((IStructuredSelection) selectionObj).getFirstElement();
				if (firstElement instanceof AdministrationArea) {
					setOldModel((AdministrationArea) firstElement);
					AdministrationArea rightHandObject = this.getOldModel().deepCopyAdminArea(false, null);
					setAdministrationAreaModel(rightHandObject);
					registerMessages(this.registry);
					bindValues();
					setOperationMode();
				}
			}
		} catch (Exception e) {
			LOGGER.warn("Unable to set administration area model selection ! " + e);//$NON-NLS-1$
		}
	}

	/**
	 * method for set AdministartionArea Model
	 * 
	 * @param adminArea
	 *            the new model
	 */
	public void setModel(final AdministrationArea adminArea) {
		try {
			setOldModel(null);
			setAdministrationAreaModel(adminArea);
			registerMessages(this.registry);
			bindValues();
			setOperationMode();
			updateDescWidget();
			updateRemarksWidget();
		} catch (Exception e) {
			LOGGER.warn("Unable to set administration area model ! " + e);//$NON-NLS-1$
		}

	}

	/**
	 * Gets the old model.
	 *
	 * @return the oldModel
	 */
	public AdministrationArea getOldModel() {
		return oldModel;
	}

	/**
	 * Sets the old model.
	 *
	 * @param oldModel
	 *            the oldModel to set
	 */
	public void setOldModel(final AdministrationArea oldModel) {
		this.oldModel = oldModel;
	}

	/**
	 * Gets the AdministrationArea model.
	 *
	 * @return the AdministrationAreaModel
	 */
	public AdministrationArea getAdministrationAreaModel() {
		return adminAreaModel;
	}

	/**
	 * Sets the AdministrationArea Model.
	 *
	 * @param adminAreaModel
	 *            the AdministrationAreaModel to set
	 */
	public void setAdministrationAreaModel(final AdministrationArea adminAreaModel) {
		this.adminAreaModel = adminAreaModel;
	}

}
