package com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.adminarea;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.stream.Collectors;

import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.projects.Project;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.refs.RelationObj;

/**
 * The Class AdminAreaProjects.
 */
public class AdminAreaProjects implements IAdminTreeChild {

	/** The parent. */
	private IAdminTreeChild parent;

	/** The admin areas projects children. */
	private Map<String, IAdminTreeChild> adminAreasProjectsChildren;

	/**
	 * Instantiates a new site admin area projects.
	 *
	 * @param parent
	 *            the parent
	 */
	public AdminAreaProjects(IAdminTreeChild parent) {
		this.parent = parent;
		this.adminAreasProjectsChildren = new LinkedHashMap<>();
	}

	/**
	 * Add.
	 *
	 * @param adminAreaId
	 *            the admin area id
	 * @param child
	 *            the child
	 * @return the i admin tree child
	 */
	public IAdminTreeChild add(final String adminAreaProId, final IAdminTreeChild child) {
		child.setParent(this);
		IAdminTreeChild returnVal = this.adminAreasProjectsChildren.put(adminAreaProId, child);
		if (child instanceof RelationObj && ((RelationObj) child).getRefObject() instanceof Project) {
			sort();
		}
		return returnVal;
	}

	/**
	 * Remove.
	 *
	 * @param adminAreaId
	 *            the admin area id
	 * @return the i admin tree child
	 */
	public IAdminTreeChild remove(final String adminAreaProId) {
		return this.adminAreasProjectsChildren.remove(adminAreaProId);
	}

	/**
	 * Gets the administration areas collection.
	 *
	 * @return the administration areas collection
	 */
	public Collection<IAdminTreeChild> getAdminAreasProjectsCollection() {
		return this.adminAreasProjectsChildren.values();
	}

	/**
	 * Remove all.
	 */
	public void removeAll() {
		this.adminAreasProjectsChildren.clear();
	}

	/**
	 * Gets the adminstration areas children.
	 *
	 * @return the adminstration areas children
	 */
	public Map<String, IAdminTreeChild> getAdminAreasProjectsChildren() {
		return adminAreasProjectsChildren;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild#
	 * getParent()
	 */
	@Override
	public IAdminTreeChild getParent() {
		return this.parent;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild#
	 * setParent(com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.
	 * IAdminTreeChild)
	 */
	@Override
	public void setParent(IAdminTreeChild parent) {
		this.parent = parent;
	}
	
	/**
	 * Sort.
	 */
	public void sort() {
		final LinkedHashMap<String, IAdminTreeChild> collect = this.adminAreasProjectsChildren.entrySet().stream().sorted(
				(e1, e2) -> ((Project) (((RelationObj) e1.getValue()).getRefObject())).getName()
				.compareTo(((Project) (((RelationObj) e2.getValue()).getRefObject())).getName()))
				.collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e1, LinkedHashMap::new));
		this.adminAreasProjectsChildren = collect;
	}

}
