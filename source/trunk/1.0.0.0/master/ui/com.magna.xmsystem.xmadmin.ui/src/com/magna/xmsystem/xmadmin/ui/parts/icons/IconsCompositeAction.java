package com.magna.xmsystem.xmadmin.ui.parts.icons;

import java.io.File;
import java.util.List;

import javax.inject.Inject;

import org.apache.commons.io.FileUtils;
import org.eclipse.e4.core.services.nls.Translation;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.ImageData;
import org.eclipse.swt.graphics.ImageLoader;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.FileDialog;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.magna.xmbackend.vo.enums.PermissionType;
import com.magna.xmbackend.vo.permission.ValidationRequest;
import com.magna.xmsystem.dependencies.customExceptions.UnauthorizedAccessException;
import com.magna.xmsystem.dependencies.utils.Constants;
import com.magna.xmsystem.dependencies.utils.XMSystemUtil;
import com.magna.xmsystem.ui.controls.widgets.CustomMessageDialog;
import com.magna.xmsystem.xmadmin.message.Message;
import com.magna.xmsystem.xmadmin.message.MessageRegistry;
import com.magna.xmsystem.xmadmin.restclient.icons.IconController;
import com.magna.xmsystem.xmadmin.restclient.util.RestClientUtil;
import com.magna.xmsystem.xmadmin.restclient.validation.ValidationController;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.AdminTreeFactory;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.Configurations;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.icon.Icon;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.icon.Icons;
import com.magna.xmsystem.xmadmin.util.CommonConstants;
import com.magna.xmsystem.xmadmin.util.MessageType;
import com.magna.xmsystem.xmadmin.util.XMAdminUtil;

/**
 * Class for Icons Part UI
 * 
 * @author shashwat.anand
 *
 */
public class IconsCompositeAction extends IconsCompositeUI {
	/**
	 * Logger instance
	 */
	private static final Logger LOGGER = LoggerFactory.getLogger(IconsCompositeAction.class);
	
	
	/** Member variable for messages. */
	@Inject
	@Translation
	transient private Message messages;

	/**
	 * Instantiates a new icons composite action.
	 *
	 * @param parent the parent
	 */
	@Inject
	public IconsCompositeAction(final Composite parent) {
		this(parent, SWT.NONE, false);
	}
	
	/**
	 * Constructor
	 * 
	 * @param parent
	 *            {@link Composite}
	 * @param style
	 *            int
	 * @param isWizard
	 *            boolean
	 */
	public IconsCompositeAction(final Composite parent, final int style, final boolean isWizard) {
		super(parent, style, isWizard);
		init();
		initListeners();
	}

	/**
	 * method to init widget data
	 */
	private void init() {
		if (this.iconTreeviewer == null) {
			return;
		}
		this.iconTreeviewer.setContentProvider(new IconTreeContentProvider());
		this.iconTreeviewer.setLabelProvider(new IconTreeLabelProvider());
		updateIconsInTree();
	}

	/**
	 * Add the listener to widgets
	 */
	private void initListeners() {
		addAddIconButtonSelectionListerner();
		addRemoveButtonSelectionListener();
	}

	/**
	 * Selection listener for remove icon button
	 */
	private void addRemoveButtonSelectionListener() {
		if (this.btnRemoveIcon == null) {
			return;
		}
		this.btnRemoveIcon.addSelectionListener(new SelectionAdapter() {
			/**
			 * implementing widget selected
			 */
			@Override
			public void widgetSelected(final SelectionEvent event) {
				if (!isPermissionPermitted("ICON-DELETE")) {
					CustomMessageDialog.openWarning(Display.getCurrent().getActiveShell(),
							messages.objectPermissionDialogTitle, messages.objectPermissionDialogMsg);
					event.doit = false;
					return;
				}
				IStructuredSelection selection;
				Object element;
				if ((selection = iconTreeviewer.getStructuredSelection()) != null
						&& (element = selection.getFirstElement()) != null) {
					if (element instanceof Icon) {
						Configurations configurations;
						if ((configurations = AdminTreeFactory.getInstance().getConfigurations()) != null) {
							final Icons icons = configurations.getIconsNode();
							final Icon icoObj = (Icon) element;
							final IconController iconCtrlr = new IconController();
							boolean response = iconCtrlr.deleteIcon(icoObj.getIconId());
							if (response) {
								if (deleteFileFromIconFolder(icoObj)) {
									icons.remove(icoObj);
									XMAdminUtil.getInstance().updateLogFile(messages.iconObject + " " + "'"
											+ icoObj.getIconName() + "'" + " " + messages.objectDelete, MessageType.SUCCESS);
								}
							} else {
								CustomMessageDialog.openError(Display.getCurrent().getActiveShell(), messages.removeIconDialogTitle,
										messages.removeIconDialogMsg);
								XMAdminUtil.getInstance().updateLogFile(messages.removeIconDialogMsg, MessageType.FAILURE);
							}
							iconTreeviewer.refresh();
							
						}
					}
				} else {
					CustomMessageDialog.openWarning(Display.getCurrent().getActiveShell(), "Remove Icon",
							"Select icon to remove");
				}
			}
		});
	}

	/**
	 * Delete file from Icon folder
	 * 
	 * @param icon
	 *            {@link Icon}
	 * @return boolean
	 */
	private boolean deleteFileFromIconFolder(final Icon icon) {
		return new File(icon.getIconPath()).delete();
	}

	/**
	 * Selection listener for add icon button
	 */
	private void addAddIconButtonSelectionListerner() {
		if (this.btnAddIcon == null) {
			return;
		}
		this.btnAddIcon.addSelectionListener(new SelectionAdapter() {
			/**
			 * implementing widget selected
			 */
			@Override
			public void widgetSelected(final SelectionEvent event) {
				if (!isPermissionPermitted("ICON-CREATE")) {
					CustomMessageDialog.openWarning(Display.getCurrent().getActiveShell(),
							messages.objectPermissionDialogTitle, messages.objectPermissionDialogMsg);
					event.doit = false;
					return;
				}
				final Button widget = (Button) event.widget;
				final FileDialog dialog = new FileDialog(widget.getShell(), SWT.OPEN);
				dialog.setFilterExtensions(Constants.SUPPORTED_ICON_EXT);
				final String selectedFilePath = dialog.open();
				try {
					if (selectedFilePath != null) {
						Configurations configurations;
						if ((configurations = AdminTreeFactory.getInstance().getConfigurations()) != null) {
							final Icons icons = configurations.getIconsNode();
							final String fileName = XMSystemUtil.getFileNameWithExtension(selectedFilePath);
							if (!icons.containsKey(fileName)) {
								File iconFolder;

								if ((iconFolder = XMSystemUtil.getXMSystemServerIconsFolder()) != null) {
									if ((iconFolder + "\\" + fileName).equals(selectedFilePath)) {
										CustomMessageDialog.openWarning(Display.getCurrent().getActiveShell(),
												"Add Icon", "Source and Destination location is same");
										return;
									}
									final File file = new File(selectedFilePath);
									final Image img = new Image(Display.getDefault(), selectedFilePath);
									if (img != null && !img.isDisposed()) {
										final Rectangle boundsInPixels = img.getBoundsInPixels();
										if (boundsInPixels.width > 16 && boundsInPixels.height > 16) {
											final Image resizedImg = XMSystemUtil.resize(img, 16, 16);
											final ImageLoader saver = new ImageLoader();
											saver.data = new ImageData[] { resizedImg.getImageData() };
											final String extension = fileName.substring(fileName.lastIndexOf(".") + 1);
											if ("GIF".equalsIgnoreCase(extension)) {
												saver.save(iconFolder + File.separator + fileName, SWT.IMAGE_GIF);
											} else if ("PNG".equalsIgnoreCase(extension)) {
												saver.save(iconFolder + File.separator + fileName, SWT.IMAGE_PNG);
											}
											resizedImg.dispose();
										} else {
											FileUtils.copyFileToDirectory(file, iconFolder);
										}
										img.dispose();
									}
									final String copiedFile = iconFolder + File.separator + fileName;
									final IconController iconCntrl = new IconController();
									final String iconId = iconCntrl
											.createIcon(new com.magna.xmbackend.vo.icon.IkonRequest(CommonConstants.EMPTY_STR,fileName,CommonConstants.CUSTOM_ICON));
									if (!XMSystemUtil.isEmpty(iconId)) {
										icons.add(new Icon(iconId, fileName, copiedFile, CommonConstants.CUSTOM_ICON));
									}
									iconTreeviewer.refresh();
									XMAdminUtil.getInstance().updateLogFile(
											messages.iconObject + " " + "'" + fileName + "'" + " " + messages.addIcon, MessageType.SUCCESS);
								} else {
									CustomMessageDialog.openWarning(Display.getCurrent().getActiveShell(),messages.addIconDialogTitle,
											messages.addIconDialogMsg);
								}
							} else {
								CustomMessageDialog.openWarning(Display.getCurrent().getActiveShell(), messages.addIconButton,
										messages.iconAlreadyExistPart1  + " '" + fileName + "' " + messages.iconAlreadyExistPart2);
							}
						}
					}
				} catch (Exception e) {
					LOGGER.error("Execption ocuured at icon uploding", e); //$NON-NLS-1$
				}
			}
		});
	}



	/**
	 * Method register method function for translation
	 * 
	 * @param registry
	 *            {@link MessageRegistry}
	 */
	public void registerMessages(final MessageRegistry registry) {
		registry.register(this.grpIcons::setText, (message) -> message.iconsNode);
		registry.register(this.btnAddIcon::setText, (message) -> {
			if (btnAddIcon != null && !btnAddIcon.isDisposed()) {
				return getUpdatedWidgetText(message.addIconButton, btnAddIcon);
			}
			return CommonConstants.EMPTY_STR;
		});
		registry.register(this.btnRemoveIcon::setText, (message) -> {
			if (btnRemoveIcon != null && !btnRemoveIcon.isDisposed()) {
				return getUpdatedWidgetText(message.removeIconButton, btnRemoveIcon);
			}
			return CommonConstants.EMPTY_STR;
		});
		this.iconTreeviewer.registerMessages(registry);
	}
	
	/**
	 * Gets the new text based on new locale.
	 * @param message {@link String}
	 * @param control {@link Control}
	 * @return {@link String} new text
	 */
	private String getUpdatedWidgetText(final String message, final Control control) {
		control.requestLayout();
		control.getParent().redraw();
		control.getParent().getParent().update();
		control.getParent().getParent().getParent().update();
		return message;
	}
	
	protected boolean isPermissionPermitted(String permissionName) {
		boolean returnVal = true;
		try {
			ValidationController controller = new ValidationController();
			ValidationRequest request = new ValidationRequest();
			request.setUserName(RestClientUtil.getInstance().getUserName());
			request.setPermissionType(PermissionType.OBJECT_PERMISSION.name());
			request.setPermissionName(permissionName);
			returnVal = controller.getAccessAllowed(request);
		} catch (UnauthorizedAccessException e) {
			LOGGER.error("Current user is Unauthorized " + e);
			returnVal = false;
		} catch (Exception e) {
			LOGGER.error("Exception occured in calling access allowed API " + e);
			returnVal = false;
		}
		return returnVal;
	}
	
	/**
	 * Update icons in tree.
	 */
	public void updateIconsInTree() {
		final Icons iconsNode = AdminTreeFactory.getInstance().getConfigurations().getIconsNode();
		try {
			final IconController iconCtrlr = new IconController();
			final List<com.magna.xmbackend.entities.IconsTbl> iconList = iconCtrlr.getAllNonDefaultIcons(true);
			File iconFolder;
			if ((iconFolder = XMSystemUtil.getXMSystemServerIconsFolder()) != null) {
				for (com.magna.xmbackend.entities.IconsTbl icon : iconList) {
					final String iconId = icon.getIconId();
					final String iconName = icon.getIconName();
					final File iconFile = new File(iconFolder + File.separator + iconName);
					if (iconFile != null && iconFile.exists()) {
						iconsNode.add(
								new Icon(iconId, iconName, iconFile.getAbsolutePath(), CommonConstants.CUSTOM_ICON));
					}
				}
			} else {
				CustomMessageDialog.openError(getShell(), messages.iconErrorTitle, messages.iconErrorMsg);
			}
		} catch (Exception e) {
			LOGGER.error("Status returned for get all icons : ", e);
			CustomMessageDialog.openError(getShell(), "Error", "Application server not reachable");
			this.iconTreeviewer.setInput(iconsNode);
			return;
		}
		this.iconTreeviewer.setInput(iconsNode);
	}
}
