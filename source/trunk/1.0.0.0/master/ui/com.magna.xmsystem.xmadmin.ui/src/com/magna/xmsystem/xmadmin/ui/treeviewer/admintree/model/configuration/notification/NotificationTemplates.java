package com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.notification;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.stream.Collectors;

import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild;

// TODO: Auto-generated Javadoc
/**
 * The Class NotificationTemplates.
 */
public class NotificationTemplates implements IAdminTreeChild {

	/** The parent. */
	private IAdminTreeChild parent;

	/** The templates children. */
	private Map<String, IAdminTreeChild> templatesChildren;;

	/**
	 * Instantiates a new notification templates.
	 */
	public NotificationTemplates() {
		this.parent = null;
		this.templatesChildren = new LinkedHashMap<>();
	}

	/**
	 * Add.
	 *
	 * @param templateId
	 *            the template id
	 * @param child
	 *            the child
	 * @return the i admin tree child
	 */
	public IAdminTreeChild add(final String templateId, final IAdminTreeChild child) {
		child.setParent(this);
		IAdminTreeChild returnVal = this.templatesChildren.put(templateId, child);
		sort();
		return returnVal;
	}

	/**
	 * Remove.
	 *
	 * @param templateId
	 *            the template id
	 * @return the i admin tree child
	 */
	public IAdminTreeChild remove(final String templateId) {
		return this.templatesChildren.remove(templateId);
	}

	/**
	 * Remove all.
	 */
	public void removeAll() {
		this.templatesChildren.clear();
	}

	/**
	 * Gets the templates collection.
	 *
	 * @return the templates collection
	 */
	public Collection<IAdminTreeChild> getTemplatesCollection() {
		return this.templatesChildren.values();
	}

	/**
	 * Gets the templates children.
	 *
	 * @return the templates children
	 */
	public Map<String, IAdminTreeChild> getTemplatesChildren() {
		return templatesChildren;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild#
	 * getParent()
	 */
	@Override
	public IAdminTreeChild getParent() {
		return this.parent;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild#
	 * setParent(com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.
	 * IAdminTreeChild)
	 */
	@Override
	public void setParent(IAdminTreeChild parent) {
		this.parent = parent;

	}

	/**
	 * Sort.
	 */
	public void sort() {
		final LinkedHashMap<String, IAdminTreeChild> collect = this.templatesChildren.entrySet().stream()
				.sorted((e1, e2) -> ((NotificationTemplate) e1.getValue()).getName()
						.compareTo(((NotificationTemplate) e2.getValue()).getName()))
				.collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e1, LinkedHashMap::new));
		this.templatesChildren = collect;
	}

}
