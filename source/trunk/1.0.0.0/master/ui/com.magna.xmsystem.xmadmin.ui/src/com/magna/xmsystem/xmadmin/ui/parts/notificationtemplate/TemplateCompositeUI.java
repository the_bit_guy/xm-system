package com.magna.xmsystem.xmadmin.ui.parts.notificationtemplate;

import org.eclipse.jface.layout.GridDataFactory;
import org.eclipse.jface.layout.GridLayoutFactory;
import org.eclipse.jface.resource.JFaceResources;
import org.eclipse.jface.resource.LocalResourceManager;
import org.eclipse.jface.resource.ResourceManager;
import org.eclipse.nebula.widgets.pgroup.PGroup;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.ScrolledComposite;
import org.eclipse.swt.events.ControlAdapter;
import org.eclipse.swt.events.ControlEvent;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.List;
import org.eclipse.swt.widgets.Text;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.magna.xmbackend.vo.enums.NotificationEventType;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.notification.NotificationEvtActions;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.notification.NotificationTemplate;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.notification.ProjectCreateEvtAction;
import com.magna.xmsystem.xmadmin.util.XMAdminUtil;

// TODO: Auto-generated Javadoc
/**
 * The Class TemplateCompositeUI.
 */
public class TemplateCompositeUI extends Composite {

	/** The Constant LOGGER. */
	private static final Logger LOGGER = LoggerFactory.getLogger(TemplateCompositeUI.class);

	/** The resource manager. */
	protected ResourceManager resourceManager;

	/** The grp template. */
	protected Group grpTemplate;

	/** The lbl name. */
	protected Label lblName;

	/** The txt name. */
	protected Text txtName;

	/** The lbl subject. */
	protected Label lblSubject;

	/** The txt subject. */
	protected Text txtSubject;

	/** The lbl message. */
	protected Label lblMessage;

	/** The txt message. */
	protected Text txtMessage;

	/** The save btn. */
	protected Button saveBtn;

	/** The cancel btn. */
	protected Button cancelBtn;

	/** The txt variable. */
	protected Text txtVariable;

	/** The event list. */
	protected List eventList;

	/** The lbl variables. */
	protected Label lblVariables;

	/**
	 * Instantiates a new template composite UI.
	 *
	 * @param parent
	 *            the parent
	 * @param style
	 *            the style
	 */
	public TemplateCompositeUI(Composite parent, int style) {
		super(parent, style);
		this.resourceManager = new LocalResourceManager(JFaceResources.getResources(), this);
		this.initGUI();
	}

	/**
	 * Init GUI.
	 */
	private void initGUI() {
		try {
			GridLayoutFactory.fillDefaults().applyTo(this);
			this.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));

			this.grpTemplate = new Group(this, SWT.NONE);
			this.grpTemplate.setBackgroundMode(SWT.INHERIT_FORCE);
			GridLayoutFactory.fillDefaults().numColumns(1).applyTo(this.grpTemplate);
			GridDataFactory.fillDefaults().grab(true, true).span(SWT.FILL, SWT.FILL).applyTo(this.grpTemplate);
			ScrolledComposite baseScrolledComposite = XMAdminUtil.getInstance()
					.createScrolledComposite(this.grpTemplate);
			baseScrolledComposite.setBackgroundMode(SWT.INHERIT_FORCE);

			final Composite widgetContainer = new Composite(baseScrolledComposite, SWT.NONE);
			final GridLayout widgetContLayout = new GridLayout(3, false);

			widgetContainer.setLayout(widgetContLayout);
			widgetContainer.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));

			this.lblName = new Label(widgetContainer, SWT.NONE);
			this.txtName = new Text(widgetContainer, SWT.BORDER);
			this.txtName.setTextLimit(ProjectCreateEvtAction.NAME_LIMIT);
			GridDataFactory.fillDefaults().grab(true, false).span(2, 1).align(SWT.FILL, SWT.CENTER)
					.applyTo(this.txtName);
			this.txtName.setTextLimit(NotificationEvtActions.NAME_LIMIT);

			this.lblVariables = new Label(widgetContainer, SWT.NONE);
			GridDataFactory.fillDefaults().grab(false, false).span(1, 1).align(SWT.LEFT, SWT.TOP)
					.applyTo(this.lblVariables);

			Composite listComposite = new Composite(widgetContainer, SWT.NONE);
			GridLayoutFactory.fillDefaults().numColumns(2).equalWidth(false).extendedMargins(0, 0, 0, 0)
					.applyTo(listComposite);
			listComposite.setLayoutData(new GridData(SWT.FILL, SWT.DEFAULT, true, false, 2, 1));
			this.eventList = new List(listComposite, SWT.BORDER | SWT.MULTI | SWT.V_SCROLL);
			this.eventList.add(NotificationEventType.PROJECT_CREATE.name());
			this.eventList.add(NotificationEventType.PROJECT_DELETE.name());
			this.eventList.add(NotificationEventType.PROJECT_DEACTIVATE.name());
			this.eventList.add(NotificationEventType.PROJECT_ACTIVATE.name());
			this.eventList.add(NotificationEventType.PROJECT_ADMIN_AREA_RELATION_ASSIGN.name());
			this.eventList.add(NotificationEventType.PROJECT_ADMIN_AREA_RELATION_REMOVE.name());
			this.eventList.add(NotificationEventType.USER_PROJECT_RELATION_ASSIGN.name());
			this.eventList.add(NotificationEventType.USER_PROJECT_RELATION_REMOVE.name());
			this.eventList.add(NotificationEventType.USER_PROJECT_RELATION_EXPIRY.name());
			this.eventList.add(NotificationEventType.USER_PROJECT_RELATION_GRACE_EXPIRY.name());
			this.eventList.add(NotificationEventType.EMAIL_NOTIFY_TO_AA_PROJECT_USERS.name());
			this.eventList.add(NotificationEventType.EMAIL_NOTIFY_TO_PROJECT_USERS.name());

			this.txtVariable = new Text(listComposite, SWT.BORDER | SWT.WRAP | SWT.MULTI | SWT.H_SCROLL | SWT.V_SCROLL);
			GridData gridData = new GridData(SWT.FILL, SWT.FILL, true, true);
			gridData.minimumHeight = 150;
			gridData.horizontalSpan = 1;
			this.txtVariable.setLayoutData(gridData);
			this.txtVariable.setEditable(false);

			this.lblSubject = new Label(widgetContainer, SWT.NONE);
			GridDataFactory.fillDefaults().grab(false, false).span(1, 1).align(SWT.LEFT, SWT.CENTER)
					.applyTo(this.lblSubject);

			this.txtSubject = new Text(widgetContainer, SWT.BORDER);
			this.txtSubject.setTextLimit(NotificationTemplate.SUB_LIMIT);
			GridDataFactory.fillDefaults().grab(true, false).span(2, 1).align(SWT.FILL, SWT.CENTER)
					.applyTo(this.txtSubject);

			this.lblMessage = new Label(widgetContainer, SWT.NONE);
			GridDataFactory.fillDefaults().grab(false, false).span(1, 1).align(SWT.LEFT, SWT.TOP)
					.applyTo(this.lblMessage);

			this.txtMessage = new Text(widgetContainer,
					SWT.BORDER | SWT.WRAP | SWT.MULTI | SWT.H_SCROLL | SWT.V_SCROLL);
			gridData = new GridData(SWT.FILL, SWT.FILL, true, true);
			gridData.minimumHeight = 150;
			gridData.horizontalSpan = 2;
			this.txtMessage.setLayoutData(gridData);
			this.txtMessage.setTextLimit(NotificationTemplate.MESSAGE_LIMIT);

			final Composite buttonBarComp = new Composite(this.grpTemplate, SWT.NONE);
			GridLayoutFactory.fillDefaults().numColumns(2).equalWidth(true).extendedMargins(0, 0, 0, 0)
					.applyTo(buttonBarComp);
			buttonBarComp.setLayoutData(new GridData(SWT.FILL, SWT.DEFAULT, true, false, 3, 1));
			createButtonBar(buttonBarComp);

			baseScrolledComposite.setContent(widgetContainer);
			baseScrolledComposite.setSize(widgetContainer.getSize());
			baseScrolledComposite.setExpandVertical(true);
			baseScrolledComposite.setExpandHorizontal(true);
			baseScrolledComposite.update();

			baseScrolledComposite.addControlListener(new ControlAdapter() {
				public void controlResized(final ControlEvent e) {
					Rectangle rectangle = baseScrolledComposite.getClientArea();
					baseScrolledComposite.setMinSize(widgetContainer.computeSize(rectangle.width, SWT.DEFAULT));
				}
			});
		} catch (Exception e) {
			LOGGER.error("Unable to crete UI elements", e); //$NON-NLS-1$
		}
	}

	/**
	 * P group repaint.
	 *
	 * @param pGroup
	 *            the group
	 */
	protected void pGroupRepaint(final PGroup pGroup) {
		pGroup.requestLayout();
		pGroup.redraw();
		pGroup.getParent().update();
		// baseScrolledComposite.notifyListeners(SWT.Resize, new Event());
	}

	/**
	 * Create button bar.
	 *
	 * @param buttonBarComp
	 *            the button bar comp
	 */
	private void createButtonBar(Composite buttonBarComp) {
		this.saveBtn = new Button(buttonBarComp, SWT.NONE);
		this.saveBtn.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, true, false));

		this.cancelBtn = new Button(buttonBarComp, SWT.NONE);
		this.cancelBtn.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, true, false));
	}

	/**
	 * Sets the show button bar.
	 *
	 * @param showButtonBar
	 *            the new show button bar
	 */
	protected void setShowButtonBar(final boolean showButtonBar) {
		if (this.saveBtn != null && !this.saveBtn.isDisposed() && this.cancelBtn != null
				&& !this.cancelBtn.isDisposed()) {
			final GridData layoutData = (GridData) this.saveBtn.getParent().getLayoutData();
			layoutData.exclude = !showButtonBar;
			this.saveBtn.setVisible(showButtonBar);
			this.cancelBtn.setVisible(showButtonBar);
			this.saveBtn.getParent().setVisible(showButtonBar);
			this.saveBtn.getParent().requestLayout();
			this.saveBtn.getParent().redraw();
			this.saveBtn.getParent().getParent().update();
		}
	}
}
