package com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.eclipse.swt.widgets.Display;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.client.ResourceAccessException;

import com.magna.xmbackend.entities.AdminAreaTranslationTbl;
import com.magna.xmbackend.entities.AdminAreasTbl;
import com.magna.xmbackend.entities.BaseAppTranslationTbl;
import com.magna.xmbackend.entities.BaseApplicationsTbl;
import com.magna.xmbackend.entities.DirectoryTbl;
import com.magna.xmbackend.entities.DirectoryTranslationTbl;
import com.magna.xmbackend.entities.EmailNotificationEventTbl;
import com.magna.xmbackend.entities.GroupTranslationTbl;
import com.magna.xmbackend.entities.GroupsTbl;
import com.magna.xmbackend.entities.IconsTbl;
import com.magna.xmbackend.entities.PermissionTbl;
import com.magna.xmbackend.entities.ProjectAppTranslationTbl;
import com.magna.xmbackend.entities.ProjectApplicationsTbl;
import com.magna.xmbackend.entities.ProjectTranslationTbl;
import com.magna.xmbackend.entities.ProjectsTbl;
import com.magna.xmbackend.entities.RolePermissionRelTbl;
import com.magna.xmbackend.entities.RolesTbl;
import com.magna.xmbackend.entities.SiteAdminAreaRelTbl;
import com.magna.xmbackend.entities.SiteTranslationTbl;
import com.magna.xmbackend.entities.SitesTbl;
import com.magna.xmbackend.entities.StartAppTranslationTbl;
import com.magna.xmbackend.entities.StartApplicationsTbl;
import com.magna.xmbackend.entities.UserAppTranslationTbl;
import com.magna.xmbackend.entities.UserApplicationsTbl;
import com.magna.xmbackend.entities.UserTranslationTbl;
import com.magna.xmbackend.entities.UsersTbl;
import com.magna.xmbackend.vo.enums.NotificationEventType;
import com.magna.xmbackend.vo.group.GroupResponse;
import com.magna.xmbackend.vo.liveMessage.LiveMessageConfigResponse;
import com.magna.xmbackend.vo.liveMessage.LiveMessageResponse;
import com.magna.xmbackend.vo.liveMessage.LiveMessageResponseWrapper;
import com.magna.xmbackend.vo.liveMessage.LiveMessageTranslation;
import com.magna.xmbackend.vo.notification.NotificationEventResponse;
import com.magna.xmsystem.dependencies.utils.LANG_ENUM;
import com.magna.xmsystem.dependencies.utils.XMSystemUtil;
import com.magna.xmsystem.ui.controls.widgets.CustomMessageDialog;
import com.magna.xmsystem.xmadmin.restclient.adminArea.AdminAreaController;
import com.magna.xmsystem.xmadmin.restclient.application.BaseAppController;
import com.magna.xmsystem.xmadmin.restclient.application.ProjectAppController;
import com.magna.xmsystem.xmadmin.restclient.application.StartAppController;
import com.magna.xmsystem.xmadmin.restclient.application.UserAppController;
import com.magna.xmsystem.xmadmin.restclient.directory.DirectoryController;
import com.magna.xmsystem.xmadmin.restclient.group.GroupController;
import com.magna.xmsystem.xmadmin.restclient.livemsg.LiveMessageController;
import com.magna.xmsystem.xmadmin.restclient.notify.NotificationController;
import com.magna.xmsystem.xmadmin.restclient.project.ProjectController;
import com.magna.xmsystem.xmadmin.restclient.relation.SiteAdminAreaRelController;
import com.magna.xmsystem.xmadmin.restclient.role.RoleController;
import com.magna.xmsystem.xmadmin.restclient.role.RolePermissionRelController;
import com.magna.xmsystem.xmadmin.restclient.site.SiteController;
import com.magna.xmsystem.xmadmin.restclient.users.UserController;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.adminarea.AdministrationArea;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.adminarea.AdministrationAreas;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.Applications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.baseapplicationmodel.BaseApplication;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.baseapplicationmodel.BaseApplications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.projectapplicationmodel.ProjectApplication;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.projectapplicationmodel.ProjectApplications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.startapplicationmodel.StartApplication;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.startapplicationmodel.StartApplications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.userapplicationmodel.UserApplication;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.userapplicationmodel.UserApplications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.Configurations;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.adminconf.AdminMenu;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.icon.Icon;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.icon.Icons;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.ldapconfig.LdapConfiguration;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.livemsgconfig.LiveMessage;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.livemsgconfig.LiveMessages;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.livemsgconfig.LiveMsgToPattern;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.noProjectMessage.NoProjectPopMessage;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.notification.Notification;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.notification.Notifications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.projectexpiryconfig.ProjectExpiryConfig;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.roles.GlobalPermissionObjectType;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.roles.Role;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.roles.Roles;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.singletonapptimeconfig.SingletonAppTimeConfig;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.smtpconfig.SmtpConfiguration;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.directory.Directories;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.directory.Directory;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.group.Groups;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.group.projectappgroup.ProjectApplicationGroup;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.group.projectappgroup.ProjectApplicationGroups;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.group.projectgroup.ProjectGroupModel;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.group.projectgroup.ProjectGroupsModel;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.group.userappgroup.UserApplicationGroup;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.group.userappgroup.UserApplicationGroups;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.group.usergroup.UserGroupModel;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.group.usergroup.UserGroupsModel;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.projects.Project;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.projects.Projects;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.sitemodel.Site;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.sitemodel.SiteAdministrations;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.sitemodel.Sites;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.users.User;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.users.Users;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.users.UsersNameAlphabet;
import com.magna.xmsystem.xmadmin.util.CommonConstants;

/**
 * A factory for creating AdminTree objects.
 */
public final class AdminTreeFactory {

	/** The Constant LOGGER. */
	private static final Logger LOGGER = LoggerFactory.getLogger(AdminTreeFactory.class);

	/** Member variable 'this ref' for {@link AdminTreeFactory}. */
	private static AdminTreeFactory thisRef;

	/** Member variable 'admin tree nodes' for {@link List<IAdminTreeChild>}. */
	private final List<IAdminTreeChild> adminTreeNodes;

	/** Member variable 'sites' for {@link Sites}. */
	private Sites sites;

	/**
	 * Member variable 'administration areas' for {@link AdministrationAreas}.
	 */
	private AdministrationAreas administrationAreas;

	/** Member variable 'users' for {@link Users}. */
	private Users users;

	/** Member variable 'configurations' for {@link Configurations}. */
	private Configurations configurations;

	/** Member variable 'Roles' for {@link Roles}. */
	private Roles roles;

	/** Member variable 'projects' for {@link Projects}. */
	private Projects projects;

	/** Member variable 'applications' for {@link Applications}. */
	private Applications applications;

	/** Member variable 'groups' for {@link Groups}. */
	private Groups groups;

	/** The usergroups. */
	private UserGroupsModel usergroups;
	
	/** The project groups. */
	private ProjectGroupsModel projectGroups;
	
	/** The user app groups. */
	private UserApplicationGroups userAppGroups;
	
	/** The project app groups. */
	private ProjectApplicationGroups projectAppGroups;

	/**
	 * Member variable 'project applications' for {@link ProjectApplications}.
	 */
	private ProjectApplications projectApplications;

	/** Member variable 'user applications' for {@link UserApplications}. */
	private UserApplications userApplications;

	/** Member variable 'base applications' for {@link BaseApplications}. */
	private BaseApplications baseApplications;

	/** Member variable 'start applications' for {@link StartApplications}. */
	private StartApplications startApplications;

	/** Member variable 'hide in active items' for {@link Boolean}. */
	private boolean hideInActiveItems;

	/** The directories. */
	private Directories directories;

	/** The notification. */
	private Notification notification;
	
	/** The notifications. */
	private Notifications notifications;

	/** The live messages. */
	private LiveMessages liveMessages;

	/**
	 * Constructor for AdminTreeFactory Class.
	 */
	private AdminTreeFactory() {
		this.adminTreeNodes = new LinkedList<>();
	}

	/**
	 * Gets the single instance of AdminTreeFactory.
	 *
	 * @return single instance of AdminTreeFactory
	 */
	public static synchronized AdminTreeFactory getInstance() {
		if (thisRef == null) {
			thisRef = new AdminTreeFactory();
		}
		return thisRef;
	}

	/**
	 * Method for Adds the.
	 *
	 * @param child
	 *            {@link IAdminTreeChild}
	 * @return true, if successful
	 */
	public boolean add(final IAdminTreeChild child) {
		return this.adminTreeNodes.add(child);
	}

	/**
	 * Method for Removes the.
	 *
	 * @param child
	 *            {@link IAdminTreeChild}
	 * @return true, if successful
	 */
	public boolean remove(final IAdminTreeChild child) {
		return this.adminTreeNodes.remove(child);
	}

	/**
	 * Creates a new AdminTree object.
	 *
	 * @param hideInActiveItems
	 *            {@link boolean}
	 * @return the list< I admin tree child>
	 */
	public List<IAdminTreeChild> createInitialModel(final boolean hideInActiveItems) {
		final File iconFolder = XMSystemUtil.getXMSystemServerIconsFolder();
		if (iconFolder == null) {
			Display.getDefault().asyncExec(new Runnable() {
				@Override
				public void run() {
					CustomMessageDialog.openError(Display.getDefault().getActiveShell(), "Error",
							"Icon folder is not reachable");
					return;
				}
			});
		}
		this.hideInActiveItems = hideInActiveItems;
		if (this.adminTreeNodes.isEmpty()) {
			this.sites = new Sites();
			this.getSitesFromService();
			add(this.sites);

			this.administrationAreas = new AdministrationAreas();
			this.getAdminAreasFromService();
			add(this.administrationAreas);

			this.projects = new Projects();
			this.getProjectsFromService();
			add(this.projects);

			this.users = new Users();
			this.getAllUsersFromService();
			add(this.users);

			this.userApplications = new UserApplications();
			this.projectApplications = new ProjectApplications();
			this.baseApplications = new BaseApplications();
			this.startApplications = new StartApplications();

			this.applications = new Applications();
			this.getAllApplications();
			add(this.applications);

			this.usergroups = new UserGroupsModel();
			this.projectGroups = new ProjectGroupsModel();
			this.userAppGroups = new UserApplicationGroups();
			this.projectAppGroups = new ProjectApplicationGroups();
			
			this.groups = new Groups();
			this.getAllGroups();
			add(this.groups);

			this.directories = new Directories();
			this.getDirectoryFromService();
			add(this.directories);

			this.configurations = new Configurations();
			this.configurations.add(new AdminMenu(this.configurations));
			this.configurations.add(new Icons());
			this.roles = new Roles();
			this.getRoleFromService();
			this.configurations.add(this.roles);
			this.configurations
					.add(new SmtpConfiguration(UUID.randomUUID().toString(), CommonConstants.OPERATIONMODE.VIEW));
			this.configurations
					.add(new LdapConfiguration(UUID.randomUUID().toString(), CommonConstants.OPERATIONMODE.VIEW));
			this.configurations
					.add(new SingletonAppTimeConfig(UUID.randomUUID().toString(), CommonConstants.OPERATIONMODE.VIEW));
			this.configurations
					.add(new NoProjectPopMessage(UUID.randomUUID().toString(), CommonConstants.OPERATIONMODE.VIEW));
			this.configurations
					.add(new ProjectExpiryConfig(UUID.randomUUID().toString(), CommonConstants.OPERATIONMODE.VIEW));
			this.liveMessages = new LiveMessages();
			this.getLiveMessagesFromService();
			this.configurations.add(this.liveMessages);
			this.notification = new Notification();
			this.configurations.add(this.notification);
			
			this.notifications = new Notifications();
			this.getNotificationEvents();
			this.notifications.addNotificationEvents();
			this.configurations.add(notifications);
			add(this.configurations);

		}
		return this.adminTreeNodes;
	}

	/**
	 * Gets the notification events.
	 *
	 * @return the notification events
	 */
	private void getNotificationEvents() {
		try {
			NotificationController notificationController = new NotificationController();
			NotificationEventResponse response = notificationController.findAllEvents();
			if (response != null) {
				List<EmailNotificationEventTbl> notificationEventTbls = response.getNotificationEventTbls();
				for (EmailNotificationEventTbl emailNotificationEventTbl : notificationEventTbls) {
					if (NotificationEventType.PROJECT_CREATE.name().equals(emailNotificationEventTbl.getEvent())) {
						this.notifications.getProjectCreateEvt()
								.setId(emailNotificationEventTbl.getEmailNotificationEventId());
						this.notifications.getProjectCreateEvt().setActive(com.magna.xmbackend.vo.enums.Status.ACTIVE
								.name().equals(emailNotificationEventTbl.getStatus()) ? true : false);
					} else if (NotificationEventType.PROJECT_DELETE.name()
							.equals(emailNotificationEventTbl.getEvent())) {
						this.notifications.getProjectDeleteEvt()
								.setId(emailNotificationEventTbl.getEmailNotificationEventId());
						this.notifications.getProjectDeleteEvt().setActive(com.magna.xmbackend.vo.enums.Status.ACTIVE
								.name().equals(emailNotificationEventTbl.getStatus()) ? true : false);
					} else if (NotificationEventType.PROJECT_ACTIVATE.name()
							.equals(emailNotificationEventTbl.getEvent())) {
						this.notifications.getProjectActivateEvt()
								.setId(emailNotificationEventTbl.getEmailNotificationEventId());
						this.notifications.getProjectActivateEvt().setActive(com.magna.xmbackend.vo.enums.Status.ACTIVE
								.name().equals(emailNotificationEventTbl.getStatus()) ? true : false);
					} else if (NotificationEventType.PROJECT_DEACTIVATE.name()
							.equals(emailNotificationEventTbl.getEvent())) {
						this.notifications.getProjectDeactivateEvt()
								.setId(emailNotificationEventTbl.getEmailNotificationEventId());
						this.notifications.getProjectDeactivateEvt().setActive(com.magna.xmbackend.vo.enums.Status.ACTIVE
								.name().equals(emailNotificationEventTbl.getStatus()) ? true : false);
					} else if (NotificationEventType.USER_PROJECT_RELATION_ASSIGN.name()
							.equals(emailNotificationEventTbl.getEvent())) {
						this.notifications.getUserProjectRelAssignEvt()
								.setId(emailNotificationEventTbl.getEmailNotificationEventId());
						this.notifications.getUserProjectRelAssignEvt().setActive(com.magna.xmbackend.vo.enums.Status.ACTIVE
								.name().equals(emailNotificationEventTbl.getStatus()) ? true : false);
					} else if (NotificationEventType.USER_PROJECT_RELATION_REMOVE.name()
							.equals(emailNotificationEventTbl.getEvent())) {
						this.notifications.getUserProjectRelRemoveEvt()
								.setId(emailNotificationEventTbl.getEmailNotificationEventId());
						this.notifications.getUserProjectRelRemoveEvt().setActive(com.magna.xmbackend.vo.enums.Status.ACTIVE
								.name().equals(emailNotificationEventTbl.getStatus()) ? true : false);
					} else if (NotificationEventType.PROJECT_ADMIN_AREA_RELATION_ASSIGN.name()
							.equals(emailNotificationEventTbl.getEvent())) {
						this.notifications.getProAARelAssignEvt()
								.setId(emailNotificationEventTbl.getEmailNotificationEventId());
						this.notifications.getProAARelAssignEvt().setActive(com.magna.xmbackend.vo.enums.Status.ACTIVE
								.name().equals(emailNotificationEventTbl.getStatus()) ? true : false);
					} else if (NotificationEventType.PROJECT_ADMIN_AREA_RELATION_REMOVE.name()
							.equals(emailNotificationEventTbl.getEvent())) {
						this.notifications.getProAARelRemoveEvt()
								.setId(emailNotificationEventTbl.getEmailNotificationEventId());
						this.notifications.getProAARelRemoveEvt().setActive(com.magna.xmbackend.vo.enums.Status.ACTIVE
								.name().equals(emailNotificationEventTbl.getStatus()) ? true : false);
					} else if (NotificationEventType.USER_PROJECT_RELATION_EXPIRY.name()
							.equals(emailNotificationEventTbl.getEvent())) {
						this.notifications.getUserProExpEvt()
								.setId(emailNotificationEventTbl.getEmailNotificationEventId());
						this.notifications.getUserProExpEvt().setActive(com.magna.xmbackend.vo.enums.Status.ACTIVE
								.name().equals(emailNotificationEventTbl.getStatus()) ? true : false);
					} else if (NotificationEventType.USER_PROJECT_RELATION_GRACE_EXPIRY.name()
							.equals(emailNotificationEventTbl.getEvent())) {
						this.notifications.getUserProExpGraceEvt()
								.setId(emailNotificationEventTbl.getEmailNotificationEventId());
					} else if (NotificationEventType.EMAIL_NOTIFY_TO_PROJECT_USERS.name()
							.equals(emailNotificationEventTbl.getEvent())) {
						this.notifications.getNotifyProjectUserEvt()
								.setId(emailNotificationEventTbl.getEmailNotificationEventId());
					} else if (NotificationEventType.EMAIL_NOTIFY_TO_AA_PROJECT_USERS.name()
							.equals(emailNotificationEventTbl.getEvent())) {
						this.notifications.getNotifyAAProjectUserEvt()
								.setId(emailNotificationEventTbl.getEmailNotificationEventId());
					}

				}
			}

		} catch (Exception e) {
			if (e instanceof ResourceAccessException) {
				Display.getDefault().asyncExec(new Runnable() {
					@Override
					public void run() {
						CustomMessageDialog.openError(Display.getDefault().getActiveShell(), "Error",
								"Server not reachable");
					}
				});
			}
			LOGGER.error("Exeception while getting notification event objects! " + e);
		}
	}

	/**
	 * Gets the admin tree nodes.
	 *
	 * @return the admin tree nodes
	 */
	public List<IAdminTreeChild> getAdminTreeNodes() {
		return adminTreeNodes;
	}

	/**
	 * Gets the sites.
	 *
	 * @return the sites
	 */
	public Sites getSites() {
		return sites;
	}

	/**
	 * Gets the administration areas.
	 *
	 * @return the administration areas
	 */
	public AdministrationAreas getAdministrationAreas() {
		return administrationAreas;
	}

	/**
	 * Gets the users.
	 *
	 * @return the users
	 */
	public Users getUsers() {
		return users;
	}

	/**
	 * Gets the projects.
	 *
	 * @return the projects
	 */
	public Projects getProjects() {
		return projects;
	}

	/**
	 * Gets the applications.
	 *
	 * @return the applications
	 */
	public Applications getApplications() {
		return applications;
	}

	/**
	 * Gets the base applications.
	 *
	 * @return the base applications
	 */
	public BaseApplications getBaseApplications() {
		return baseApplications;
	}

	/**
	 * Gets the user applications.
	 *
	 * @return the user applications
	 */
	public UserApplications getUserApplications() {
		return userApplications;
	}

	/**
	 * Gets the project applications.
	 *
	 * @return the project applications
	 */
	public ProjectApplications getProjectApplications() {
		return projectApplications;
	}

	/**
	 * Gets the start applications.
	 *
	 * @return the start applications
	 */
	public StartApplications getStartApplications() {
		return startApplications;
	}

	/**
	 * Gets the groups.
	 *
	 * @return the groups
	 */
	public Groups getGroups() {
		return groups;
	}

	/**
	 * Gets the usergroups.
	 *
	 * @return the usergroups
	 */
	public UserGroupsModel getUsergroups() {
		return usergroups;
	}

	/**
	 * Gets the project groups.
	 *
	 * @return the project groups
	 */
	public ProjectGroupsModel getProjectGroups() {
		return projectGroups;
	}

	/**
	 * Gets the user app groups.
	 *
	 * @return the user app groups
	 */
	public UserApplicationGroups getUserAppGroups() {
		return userAppGroups;
	}

	/**
	 * Gets the project app groups.
	 *
	 * @return the project app groups
	 */
	public ProjectApplicationGroups getProjectAppGroups() {
		return projectAppGroups;
	}

	/**
	 * Gets the directories.
	 *
	 * @return the directories
	 */
	public Directories getDirectories() {
		return directories;
	}

	/**
	 * Gets the configurations.
	 *
	 * @return the configurations
	 */
	public Configurations getConfigurations() {
		return configurations;
	}

	/**
	 * Gets the roles.
	 *
	 * @return the roles
	 */
	public Roles getRoles() {
		return roles;
	}

	/**
	 * Gets the live messages.
	 *
	 * @return the live messages
	 */
	public LiveMessages getLiveMessages() {
		return liveMessages;
	}

	/**
	 * Gets the notification.
	 *
	 * @return the notification
	 */
	public Notification getNotification() {
		return notification;
	}
	
	/**
	 * Gets the notifications.
	 *
	 * @return the notifications
	 */
	public Notifications getNotifications() {
		return this.notifications;
	}

	/**
	 * Checks if is hide in active items.
	 *
	 * @return true, if is hide in active items
	 */
	public boolean isHideInActiveItems() {
		return hideInActiveItems;
	}

	/**
	 * Sets the hide in active items.
	 *
	 * @param hideInActiveItems
	 *            the new hide in active items
	 */
	public void setHideInActiveItems(boolean hideInActiveItems) {
		this.hideInActiveItems = hideInActiveItems;
	}

	/**
	 * Gets the all applications.
	 *
	 * @return the all applications
	 */
	private void getAllApplications() {
		getUserAppFromService();
		this.applications.add(UserApplications.class.getName(), this.userApplications);
		getProjectAppFromService();
		this.applications.add(ProjectApplications.class.getName(), this.projectApplications);
		getStartAppFromService();
		this.applications.add(StartApplications.class.getName(), this.startApplications);
		getBaseAppFromService();
		this.applications.add(BaseApplications.class.getName(), this.baseApplications);
	}

	/**
	 * Gets the all groups.
	 *
	 * @return the all groups
	 */
	private void getAllGroups() {
		this.getUserGroupFromServices();
		this.groups.add(UserGroupsModel.class.getSimpleName(), usergroups);
		this.getProjectGroupFromServices();
		this.groups.add(ProjectGroupsModel.class.getSimpleName(), projectGroups);
		this.getUserAppGroupFromServices();
		this.groups.add(UserApplicationGroups.class.getSimpleName(), userAppGroups);
		this.getProjectAppGroupFromServices();
		this.groups.add(ProjectApplicationGroups.class.getSimpleName(), projectAppGroups);
		
	}

	/**
	 * Gets the sites from service.
	 *
	 * @return the sites from service
	 */
	private void getSitesFromService() {
		final File iconFolder = XMSystemUtil.getXMSystemServerIconsFolder();
		try {
			final SiteController siteController = new SiteController();
			List<SitesTbl> siteResponseList = siteController.getAllSites(true);

			for (SitesTbl sitesTblVo : siteResponseList) {
				final String id = sitesTblVo.getSiteId();
				final String name = sitesTblVo.getName();
				boolean isActive = com.magna.xmbackend.vo.enums.Status.ACTIVE.name().equals(sitesTblVo.getStatus())
						? true : false;

				IconsTbl iconTbl = sitesTblVo.getIconId();
				Icon icon = new Icon(iconTbl.getIconId(), iconTbl.getIconName(),
						iconFolder + File.separator + iconTbl.getIconName(), iconTbl.getIconType());

				Map<LANG_ENUM, String> descriptionMap = new HashMap<>();
				Map<LANG_ENUM, String> remarksMap = new HashMap<>();
				Map<LANG_ENUM, String> translationMap = new HashMap<>();

				Collection<SiteTranslationTbl> siteTranslationTblList = sitesTblVo.getSiteTranslationTblCollection();
				for (SiteTranslationTbl siteTranslationTbl : siteTranslationTblList) {
					LANG_ENUM langEnum = LANG_ENUM.getLangEnum(siteTranslationTbl.getLanguageCode().getLanguageCode());
					String translationId = siteTranslationTbl.getSiteTranslationId();
					translationMap.put(langEnum, translationId);
					descriptionMap.put(langEnum, siteTranslationTbl.getDescription());
					remarksMap.put(langEnum, siteTranslationTbl.getRemarks());
				}

				Site site = new Site(id, name, isActive, descriptionMap, remarksMap, icon,
						CommonConstants.OPERATIONMODE.VIEW);
				site.setTranslationIdMap(translationMap);
				site.getSiteChildren().put(SiteAdministrations.class.getName(), new SiteAdministrations(site));
				if (this.hideInActiveItems) {
					if (isActive) {
						this.sites.add(id, site);
					}
				} else {
					this.sites.add(id, site);
				}
			}
		} catch (Exception e) {
			if (e instanceof ResourceAccessException) {
				Display.getDefault().asyncExec(new Runnable() {
					@Override
					public void run() {
						CustomMessageDialog.openError(Display.getDefault().getActiveShell(), "Error",
								"Server not reachable");
					}
				});
			}
			LOGGER.error("Exeception while getting site objects! " + e);
		}
	}

	/**
	 * Gets the admin areas from service.
	 *
	 * @return the admin areas from service
	 */
	private void getAdminAreasFromService() {
		final File iconFolder = XMSystemUtil.getXMSystemServerIconsFolder();
		try {
			final AdminAreaController adminAreaController = new AdminAreaController();
			List<AdminAreasTbl> adminAreaResponseList = adminAreaController.getAllAdminAreas(true);

			List<SiteAdminAreaRelTbl> siteAdminAreaRelList = new ArrayList<>();
			if (!adminAreaResponseList.isEmpty()) {
				SiteAdminAreaRelController siteAdminAreaRelController = new SiteAdminAreaRelController();
				siteAdminAreaRelList = siteAdminAreaRelController.getAllSiteAdminAreaRel();
			}

			for (AdminAreasTbl adminAreasTblVo : adminAreaResponseList) {
				String id = adminAreasTblVo.getAdminAreaId();
				String name = adminAreasTblVo.getName();
				String contact = adminAreasTblVo.getHotlineContactNumber();
				String email = adminAreasTblVo.getHotlineContactEmail();
				Long singletonAppTimeout = adminAreasTblVo.getSingletonAppTimeout();
				boolean isActive = com.magna.xmbackend.vo.enums.Status.ACTIVE.name().equals(adminAreasTblVo.getStatus())
						? true : false;

				IconsTbl iconTbl = adminAreasTblVo.getIconId();
				Icon icon = new Icon(iconTbl.getIconId(), iconTbl.getIconName(),
						iconFolder + File.separator + iconTbl.getIconName(), iconTbl.getIconType());

				Map<LANG_ENUM, String> descriptionMap = new HashMap<>();
				Map<LANG_ENUM, String> remarksMap = new HashMap<>();
				Map<LANG_ENUM, String> translationMap = new HashMap<>();

				Collection<AdminAreaTranslationTbl> adminAreaTranslationTblList = adminAreasTblVo
						.getAdminAreaTranslationTblCollection();
				for (AdminAreaTranslationTbl adminAreaTranslationTbl : adminAreaTranslationTblList) {
					LANG_ENUM langEnum = LANG_ENUM
							.getLangEnum(adminAreaTranslationTbl.getLanguageCode().getLanguageCode());
					String translationId = adminAreaTranslationTbl.getAdminAreaTranslationId();
					translationMap.put(langEnum, translationId);
					descriptionMap.put(langEnum, adminAreaTranslationTbl.getDescription());
					remarksMap.put(langEnum, adminAreaTranslationTbl.getRemarks());
				}

				AdministrationArea adminArea = new AdministrationArea(id, name, isActive, descriptionMap, contact,
						email, singletonAppTimeout, remarksMap, icon, CommonConstants.OPERATIONMODE.VIEW);

				adminArea.setTranslationIdMap(translationMap);

				for (SiteAdminAreaRelTbl siteAdminAreaRel : siteAdminAreaRelList) {
					if (id.equals(siteAdminAreaRel.getAdminAreaId().getAdminAreaId())) {
						adminArea.setRelationId(siteAdminAreaRel.getSiteAdminAreaRelId());
						adminArea.setRelatedSiteName(siteAdminAreaRel.getSiteId().getName());
						break;
					}
				}
				if (this.hideInActiveItems) {
					if (isActive) {
						this.administrationAreas.add(id, adminArea);

					}
				} else {
					this.administrationAreas.add(id, adminArea);
				}
			}
		} catch (Exception e) {
			LOGGER.error("Exeception while getting Adminstration Areas objects! " + e);
		}
	}

	/**
	 * Gets the projects from service.
	 *
	 * @return the projects from service
	 */
	private void getProjectsFromService() {
		final File iconFolder = XMSystemUtil.getXMSystemServerIconsFolder();
		try {
			final ProjectController projectsController = new ProjectController();
			List<ProjectsTbl> projectResponseList = projectsController.getAllProjects(true);

			for (ProjectsTbl projectsTblVo : projectResponseList) {
				final String id = projectsTblVo.getProjectId();
				final String name = projectsTblVo.getName();
				boolean isActive = com.magna.xmbackend.vo.enums.Status.ACTIVE.name().equals(projectsTblVo.getStatus())
						? true : false;

				IconsTbl iconTbl = projectsTblVo.getIconId();
				Icon icon = new Icon(iconTbl.getIconId(), iconTbl.getIconName(),
						iconFolder + File.separator + iconTbl.getIconName(), iconTbl.getIconType());

				Map<LANG_ENUM, String> descriptionMap = new HashMap<>();
				Map<LANG_ENUM, String> remarksMap = new HashMap<>();
				Map<LANG_ENUM, String> translationMap = new HashMap<>();

				Collection<ProjectTranslationTbl> projectTranslationTblList = projectsTblVo
						.getProjectTranslationTblCollection();
				for (ProjectTranslationTbl projectTranslationTbl : projectTranslationTblList) {
					LANG_ENUM langEnum = LANG_ENUM
							.getLangEnum(projectTranslationTbl.getLanguageCode().getLanguageCode());
					String translationId = projectTranslationTbl.getProjectTranslationId();
					translationMap.put(langEnum, translationId);
					descriptionMap.put(langEnum, projectTranslationTbl.getDescription());
					remarksMap.put(langEnum, projectTranslationTbl.getRemarks());
				}

				Project projectModel = new Project(id, name, isActive, descriptionMap, remarksMap, icon,
						CommonConstants.OPERATIONMODE.VIEW);

				projectModel.setTranslationIdMap(translationMap);

				if (this.hideInActiveItems) {
					if (isActive) {
						this.projects.add(id, projectModel);
					}
				} else {
					this.projects.add(id, projectModel);
				}
			}
		} catch (Exception e) {
			LOGGER.error("Exeception while getting  Project objects! " + e);
		}
	}

	/**
	 * Gets the base app from service.
	 *
	 * @return the base app from service
	 */
	private void getBaseAppFromService() {
		final File iconFolder = XMSystemUtil.getXMSystemServerIconsFolder();
		try {
			final BaseAppController baseAppController = new BaseAppController();
			List<BaseApplicationsTbl> baseApplicationsTblList = baseAppController.getAllBaseApps(true);

			for (BaseApplicationsTbl baseApplicationsTblVo : baseApplicationsTblList) {
				final String id = baseApplicationsTblVo.getBaseApplicationId();
				final String name = baseApplicationsTblVo.getName();
				boolean isActive = com.magna.xmbackend.vo.enums.Status.ACTIVE.name()
						.equals(baseApplicationsTblVo.getStatus()) ? true : false;
				String program = baseApplicationsTblVo.getProgram();

				Map<String, Boolean> platformMap = new HashMap<>();
				String platforms = baseApplicationsTblVo.getPlatforms();
				if (!XMSystemUtil.isEmpty(platforms)) {
					String[] platformArray = platforms.split(";");
					for (String paltform : platformArray) {
						if (BaseApplication.SUPPORTED_PLATFORMS.contains(paltform)) {
							platformMap.put(paltform, true);
						}
					}
				}

				IconsTbl iconTbl = baseApplicationsTblVo.getIconId();
				Icon icon = new Icon(iconTbl.getIconId(), iconTbl.getIconName(),
						iconFolder + File.separator + iconTbl.getIconName(), iconTbl.getIconType());

				Map<LANG_ENUM, String> descriptionMap = new HashMap<>();
				Map<LANG_ENUM, String> remarksMap = new HashMap<>();
				Map<LANG_ENUM, String> translationMap = new HashMap<>();

				Collection<BaseAppTranslationTbl> baseAppTranslationTblList = baseApplicationsTblVo
						.getBaseAppTranslationTblCollection();
				for (BaseAppTranslationTbl baseAppTranslationTbl : baseAppTranslationTblList) {
					LANG_ENUM langEnum = LANG_ENUM
							.getLangEnum(baseAppTranslationTbl.getLanguageCode().getLanguageCode());
					String translationId = baseAppTranslationTbl.getBaseAppTranslationId();
					translationMap.put(langEnum, translationId);
					descriptionMap.put(langEnum, baseAppTranslationTbl.getDescription());
					remarksMap.put(langEnum, baseAppTranslationTbl.getRemarks());
				}

				BaseApplication baseApplication = new BaseApplication(id, name, isActive, descriptionMap, remarksMap,
						platformMap, program, icon, CommonConstants.OPERATIONMODE.VIEW);
				baseApplication.setTranslationIdMap(translationMap);

				if (this.hideInActiveItems) {
					if (isActive) {
						this.baseApplications.add(id, baseApplication);
					}
				} else {
					this.baseApplications.add(id, baseApplication);
				}
			}
		} catch (Exception e) {
			LOGGER.error("Exeception while getting Base Application objects! " + e);
		}
	}

	/**
	 * Gets the project app from service.
	 *
	 * @return the project app from service
	 */
	private void getProjectAppFromService() {
		final File iconFolder = XMSystemUtil.getXMSystemServerIconsFolder();
		try {
			final ProjectAppController projectAppController = new ProjectAppController();
			List<ProjectApplicationsTbl> projectApplicationsTblList = projectAppController.getAllProjAppsByPositions();

			for (ProjectApplicationsTbl projectApplicationsTblVo : projectApplicationsTblList) {
				final String id = projectApplicationsTblVo.getProjectApplicationId();
				final String name = projectApplicationsTblVo.getName();
				final String description = projectApplicationsTblVo.getDescription();
				boolean isActive = com.magna.xmbackend.vo.enums.Status.ACTIVE.name()
						.equals(projectApplicationsTblVo.getStatus()) ? true : false;

				IconsTbl iconTbl = projectApplicationsTblVo.getIconId();
				Icon icon = new Icon(iconTbl.getIconId(), iconTbl.getIconName(),
						iconFolder + File.separator + iconTbl.getIconName(), iconTbl.getIconType());
				boolean parent = Boolean.parseBoolean(projectApplicationsTblVo.getIsParent());
				String position = projectApplicationsTblVo.getPosition();
				boolean isSingleton = Boolean.parseBoolean(projectApplicationsTblVo.getIsSingleton());
				BaseApplicationsTbl baseApplicationsTbl = projectApplicationsTblVo.getBaseApplicationId();
				String baseApplicationId = baseApplicationsTbl == null ? null
						: baseApplicationsTbl.getBaseApplicationId();

				Map<LANG_ENUM, String> nameMap = new HashMap<>();
				Map<LANG_ENUM, String> descriptionMap = new HashMap<>();
				Map<LANG_ENUM, String> remarksMap = new HashMap<>();
				Map<LANG_ENUM, String> translationMap = new HashMap<>();

				Collection<ProjectAppTranslationTbl> projectAppTranslationTblList = projectApplicationsTblVo
						.getProjectAppTranslationTblCollection();
				for (ProjectAppTranslationTbl projectAppTranslationTbl : projectAppTranslationTblList) {
					LANG_ENUM langEnum = LANG_ENUM
							.getLangEnum(projectAppTranslationTbl.getLanguageCode().getLanguageCode());
					String translationId = projectAppTranslationTbl.getProjectAppTranslationId();
					translationMap.put(langEnum, translationId);
					nameMap.put(langEnum, projectAppTranslationTbl.getName());
					descriptionMap.put(langEnum, projectAppTranslationTbl.getDescription());
					remarksMap.put(langEnum, projectAppTranslationTbl.getRemarks());
				}

				ProjectApplication projectApplication = new ProjectApplication(id, name, description, nameMap, isActive, descriptionMap,
						remarksMap, icon, parent, isSingleton, position, baseApplicationId,
						CommonConstants.OPERATIONMODE.VIEW);
				projectApplication.setTranslationIdMap(translationMap);

				if (this.isHideInActiveItems()) {
					if (isActive) {
						projectApplications.add(id, projectApplication);
					}
				} else {
					projectApplications.add(id, projectApplication);
				}
			}
		} catch (Exception e) {
			LOGGER.error("Exeception while getting project Application objects! " + e);
		}
	}

	/**
	 * Gets the user app from service.
	 *
	 * @return the user app from service
	 */
	private void getUserAppFromService() {

		final File iconFolder = XMSystemUtil.getXMSystemServerIconsFolder();
		try {
			final UserAppController userAppController = new UserAppController();
			List<UserApplicationsTbl> userApplicationsTblList = userAppController.getAllUserAppsByPositions();

			for (UserApplicationsTbl userApplicationsTblVo : userApplicationsTblList) {
				final Boolean isParent = Boolean.valueOf(userApplicationsTblVo.getIsParent());
				final String id = userApplicationsTblVo.getUserApplicationId();
				final String name = userApplicationsTblVo.getName();
				final String description = userApplicationsTblVo.getDescription();
				boolean isActive = com.magna.xmbackend.vo.enums.Status.ACTIVE.name()
						.equals(userApplicationsTblVo.getStatus()) ? true : false;
				String position = userApplicationsTblVo.getPosition();
				boolean isSingleton = Boolean.parseBoolean(userApplicationsTblVo.getIsSingleton());
				BaseApplicationsTbl baseApplicationsTbl = userApplicationsTblVo.getBaseApplicationId();

				String baseApplicationId = baseApplicationsTbl == null ? null
						: baseApplicationsTbl.getBaseApplicationId();

				IconsTbl iconTbl = userApplicationsTblVo.getIconId();
				Icon icon = new Icon(iconTbl.getIconId(), iconTbl.getIconName(),
						iconFolder + File.separator + iconTbl.getIconName(), iconTbl.getIconType());

				Map<LANG_ENUM, String> nameMap = new HashMap<>();
				Map<LANG_ENUM, String> descriptionMap = new HashMap<>();
				Map<LANG_ENUM, String> remarksMap = new HashMap<>();
				Map<LANG_ENUM, String> translationMap = new HashMap<>();

				Collection<UserAppTranslationTbl> userAppTranslationTblList = userApplicationsTblVo
						.getUserAppTranslationTblCollection();
				for (UserAppTranslationTbl userAppTranslationTbl : userAppTranslationTblList) {
					LANG_ENUM langEnum = LANG_ENUM
							.getLangEnum(userAppTranslationTbl.getLanguageCode().getLanguageCode());
					String translationId = userAppTranslationTbl.getUserAppTranslationId();
					translationMap.put(langEnum, translationId);
					nameMap.put(langEnum, userAppTranslationTbl.getName());
					descriptionMap.put(langEnum, userAppTranslationTbl.getDescription());
					remarksMap.put(langEnum, userAppTranslationTbl.getRemarks());
				}

				UserApplication userApplication = new UserApplication(id, name, description, nameMap, isActive, descriptionMap, remarksMap,
						icon, isParent, isSingleton, position, baseApplicationId, CommonConstants.OPERATIONMODE.VIEW);
				userApplication.setTranslationIdMap(translationMap);

				if (this.isHideInActiveItems()) {
					if (isActive) {
						userApplications.add(id, userApplication);
					}
				} else {
					userApplications.add(id, userApplication);
				}
			}
		} catch (Exception e) {
			LOGGER.error("Exeception while getting User Application objects! " + e);
		}

	}

	/**
	 * Gets the start app from service.
	 *
	 * @return the start app from service
	 */
	private void getStartAppFromService() {
		final File iconFolder = XMSystemUtil.getXMSystemServerIconsFolder();
		try {
			final StartAppController startAppController = new StartAppController();
			List<StartApplicationsTbl> startApplicationsTblList = startAppController.getAllStartApps(true);

			for (StartApplicationsTbl startApplicationsTblVo : startApplicationsTblList) {
				final String id = startApplicationsTblVo.getStartApplicationId();
				final String name = startApplicationsTblVo.getName();
				boolean isActive = com.magna.xmbackend.vo.enums.Status.ACTIVE.name()
						.equals(startApplicationsTblVo.getStatus()) ? true : false;
				boolean isMessage = Boolean.valueOf(startApplicationsTblVo.getIsMessage());
				Date expiryDate = null;
				if (isMessage && (expiryDate = startApplicationsTblVo.getStartMessageExpiryDate()) != null) {
					expiryDate = startApplicationsTblVo.getStartMessageExpiryDate();
				}
				IconsTbl iconTbl = startApplicationsTblVo.getIconId();
				Icon icon = new Icon(iconTbl.getIconId(), iconTbl.getIconName(),
						iconFolder + File.separator + iconTbl.getIconName(), iconTbl.getIconType());

				BaseApplicationsTbl baseApplicationsTbl = startApplicationsTblVo.getBaseApplicationId();
				String baseApplicationId = baseApplicationsTbl == null ? null
						: baseApplicationsTbl.getBaseApplicationId();

				Map<LANG_ENUM, String> descriptionMap = new HashMap<>();
				Map<LANG_ENUM, String> remarksMap = new HashMap<>();
				Map<LANG_ENUM, String> translationMap = new HashMap<>();

				Collection<StartAppTranslationTbl> starttAppTranslationTblList = startApplicationsTblVo
						.getStartAppTranslationTblCollection();
				for (StartAppTranslationTbl startAppTranslationTbl : starttAppTranslationTblList) {
					LANG_ENUM langEnum = LANG_ENUM
							.getLangEnum(startAppTranslationTbl.getLanguageCode().getLanguageCode());
					String translationId = startAppTranslationTbl.getStartAppTranslationId();
					translationMap.put(langEnum, translationId);
					descriptionMap.put(langEnum, startAppTranslationTbl.getDescription());
					remarksMap.put(langEnum, startAppTranslationTbl.getRemarks());
				}

				StartApplication startApplication = new StartApplication(id, name, isActive, descriptionMap,
						remarksMap, icon, CommonConstants.OPERATIONMODE.VIEW, baseApplicationId);
				startApplication.setTranslationIdMap(translationMap);
				startApplication.setMessage(isMessage);
				startApplication.setExpiryDate(expiryDate);
				if (this.hideInActiveItems) {
					if (isActive) {
						this.startApplications.add(id, startApplication);
					}
				} else {
					this.startApplications.add(id, startApplication);
				}
			}
		} catch (Exception e) {
			LOGGER.error("Exeception while getting Start Application objects! " + e);
		}
	}

	private void getAllUsersFromService() {
		final File iconFolder = XMSystemUtil.getXMSystemServerIconsFolder();
		try {
			final UserController userController = new UserController();
			List<UsersTbl> userResponseList = userController.getAllUsers(true);
			
			if(userResponseList != null && !userResponseList.isEmpty()) {
				for (int i = 0; i < 26; i++) {
					String alphabet = ((char) (97 + i)) + "";
					UsersNameAlphabet usersNameAlphabet = new UsersNameAlphabet(alphabet + "...");
					
					for (UsersTbl usersTblVo : userResponseList) {
						String userName = usersTblVo.getUsername();
						if( alphabet.equalsIgnoreCase(userName.charAt(0) + "") ) {
							String id = usersTblVo.getUserId();						
							String fullName = usersTblVo.getFullName();
							String userManager = usersTblVo.getManager();
							String userEmailId = usersTblVo.getEmailId();
							String userTelephone = usersTblVo.getTelephoneNumber();
							String userDept = usersTblVo.getDepartment();
							boolean isActive = com.magna.xmbackend.vo.enums.Status.ACTIVE.name().equals(usersTblVo.getStatus())
									? true : false;

							IconsTbl iconTbl = usersTblVo.getIconId();
							Icon icon = new Icon(iconTbl.getIconId(), iconTbl.getIconName(),
									iconFolder + File.separator + iconTbl.getIconName(), iconTbl.getIconType());

							Map<LANG_ENUM, String> descriptionMap = new HashMap<>();
							Map<LANG_ENUM, String> remarksMap = new HashMap<>();
							Map<LANG_ENUM, String> translationIdMap = new HashMap<>();

							Collection<UserTranslationTbl> userTranslationTblList = usersTblVo.getUserTranslationTblCollection();
							for (UserTranslationTbl userTranslationTbl : userTranslationTblList) {
								LANG_ENUM langEnum = LANG_ENUM.getLangEnum(userTranslationTbl.getLanguageCode().getLanguageCode());
								String translationId = userTranslationTbl.getUserTranslationId();
								translationIdMap.put(langEnum, translationId);
								descriptionMap.put(langEnum, userTranslationTbl.getDescription());
								remarksMap.put(langEnum, userTranslationTbl.getRemarks());
							}
							
							User user = new User(id, userName, fullName, userManager, isActive, userEmailId, userTelephone, userDept,
									descriptionMap, remarksMap, icon, CommonConstants.OPERATIONMODE.VIEW);

							user.setTranslationIdMap(translationIdMap);

							if (this.hideInActiveItems) {
								if (isActive) {
									usersNameAlphabet.add(id, user);
								}
							} else {
								usersNameAlphabet.add(id, user);
							}
						}						
					}
					
					this.users.add(alphabet, usersNameAlphabet);
				}
			}	
		} catch (Exception e) {
			LOGGER.error("Exeception while getting User objects! " + e);
		}
	}

	/**
	 * Gets the directory from service.
	 *
	 * @return the directory from service
	 */
	private void getDirectoryFromService() {
		final File iconFolder = XMSystemUtil.getXMSystemServerIconsFolder();
		try {
			DirectoryController directoryController = new DirectoryController();
			List<DirectoryTbl> directoryResponseList = directoryController.getAllDirectory(true);
			for (DirectoryTbl directoryVo : directoryResponseList) {
				final String id = directoryVo.getDirectoryId();
				final String name = directoryVo.getName();
				IconsTbl iconTbl = directoryVo.getIconId();
				Icon icon = new Icon(iconTbl.getIconId(), iconTbl.getIconName(),
						iconFolder + File.separator + iconTbl.getIconName(), iconTbl.getIconType());

				Map<LANG_ENUM, String> descriptionMap = new HashMap<>();
				Map<LANG_ENUM, String> remarksMap = new HashMap<>();
				Map<LANG_ENUM, String> translationMap = new HashMap<>();
				Collection<DirectoryTranslationTbl> directoryTranslationTblList = directoryVo
						.getDirectoryTranslationTblCollection();
				for (DirectoryTranslationTbl directoryTranslationTbl : directoryTranslationTblList) {
					LANG_ENUM langEnum = LANG_ENUM
							.getLangEnum(directoryTranslationTbl.getLanguageCode().getLanguageCode());
					String translationId = directoryTranslationTbl.getDirectoryTranslationId();
					translationMap.put(langEnum, translationId);
					descriptionMap.put(langEnum, directoryTranslationTbl.getDescription());
					remarksMap.put(langEnum, directoryTranslationTbl.getRemarks());
				}
				Directory directory = new Directory(id, name, descriptionMap, remarksMap, icon,
						CommonConstants.OPERATIONMODE.VIEW);
				directory.setTranslationIdMap(translationMap);
				this.directories.add(id, directory);
			}

		} catch (Exception e) {
			LOGGER.error("Exeception while getting directory! " + e);
		}
	}

	/**
	 * Gets the role from service.
	 *
	 * @return the role from service
	 */
	private void getRoleFromService() {
		try {
			RoleController roleController = new RoleController();
			List<RolesTbl> rolesTbls = roleController.getAllRoles(true);
			for (RolesTbl roleTbl : rolesTbls) {
				String roleId = roleTbl.getRoleId();
				String roleName = roleTbl.getName();
				String description = roleTbl.getDescription();
				Role role = new Role(roleId, roleName, description, CommonConstants.OPERATIONMODE.VIEW);
				RolePermissionRelController controller = new RolePermissionRelController();
				Iterable<RolePermissionRelTbl> rolePermssionTbls;
				if ((rolePermssionTbls = controller.getPermissionByRoleId(roleId)) != null) {
					for (RolePermissionRelTbl rolePermissionRelTbl : rolePermssionTbls) {
						PermissionTbl permissionTbl = rolePermissionRelTbl.getPermissionId();
						final String name = permissionTbl.getName();
						if (GlobalPermissionObjectType.VIEW_INACTIVE.name().equals(name)) {
							role.setIsViewInactive(true);
						} else if (GlobalPermissionObjectType.LOGIN_CAX_START_ADMIN.name().equals(name)) {
							role.setIsXmAdmin(true);
						} else if (GlobalPermissionObjectType.LOGIN_CAX_START_HOTLINE.name().equals(name)) {
							role.setIsXmHotline(true);
						}
					}
				}
				this.roles.add(roleId, role);
			}
		} catch (Exception e) {
			LOGGER.error("Exeception while getting role! " + e);
		}
	}

	/**
	 * Gets the user group from services.
	 *
	 * @return the user group from services
	 */
	private void getUserGroupFromServices() {
		final File iconFolder = XMSystemUtil.getXMSystemServerIconsFolder();
		try {
			final GroupController groupController = new GroupController();
			GroupResponse groupResponse = groupController.getGroupByGroupType(com.magna.xmbackend.vo.enums.Groups.USER.name());
			List<GroupsTbl> groupTbls = groupResponse.getGroupTbls();
			for (GroupsTbl groupTblVo : groupTbls) {
				String id = groupTblVo.getGroupId();

				IconsTbl iconTbl = groupTblVo.getIconId();
				Icon icon = new Icon(iconTbl.getIconId(), iconTbl.getIconName(),
						iconFolder + File.separator + iconTbl.getIconName(), iconTbl.getIconType());

				Map<LANG_ENUM, String> descriptionMap = new HashMap<>();
				Map<LANG_ENUM, String> remarksMap = new HashMap<>();
				Map<LANG_ENUM, String> translationMap = new HashMap<>();
				String name = new String();
				name = groupTblVo.getName();
				Collection<GroupTranslationTbl> groupTranslationTblList = groupTblVo.getGroupTranslationTblCollection();
				for (GroupTranslationTbl groupTranslationTbl : groupTranslationTblList) {
					LANG_ENUM langEnum = LANG_ENUM.getLangEnum(groupTranslationTbl.getLanguageCode().getLanguageCode());
					String translationId = groupTranslationTbl.getGroupTranslationId();
					translationMap.put(langEnum, translationId);
					descriptionMap.put(langEnum, groupTranslationTbl.getDescription());
					remarksMap.put(langEnum, groupTranslationTbl.getRemarks());
				}

				UserGroupModel userGroup = new UserGroupModel(id, name, descriptionMap, remarksMap, icon,
						CommonConstants.OPERATIONMODE.VIEW);
				userGroup.setTranslationIdMap(translationMap);
				this.usergroups.add(id, userGroup);
			}
		} catch (Exception e) {
			LOGGER.error("Exeception while getting user Group objects! " + e);
		}
	}

	/**
	 * Gets the project app group from services.
	 *
	 * @return the project app group from services
	 */
	private void getProjectAppGroupFromServices() {
		final File iconFolder = XMSystemUtil.getXMSystemServerIconsFolder();
		try {
			final GroupController groupController = new GroupController();
			GroupResponse groupResponse = groupController
					.getGroupByGroupType(com.magna.xmbackend.vo.enums.Groups.PROJECTAPPLICATION.name());
			List<GroupsTbl> groupTbls = groupResponse.getGroupTbls();
			for (GroupsTbl groupTblVo : groupTbls) {
				String id = groupTblVo.getGroupId();

				IconsTbl iconTbl = groupTblVo.getIconId();
				Icon icon = new Icon(iconTbl.getIconId(), iconTbl.getIconName(),
						iconFolder + File.separator + iconTbl.getIconName(), iconTbl.getIconType());

				Map<LANG_ENUM, String> descriptionMap = new HashMap<>();
				Map<LANG_ENUM, String> remarksMap = new HashMap<>();
				Map<LANG_ENUM, String> translationMap = new HashMap<>();
				String name = new String();
				name = groupTblVo.getName();
				Collection<GroupTranslationTbl> groupTranslationTblList = groupTblVo.getGroupTranslationTblCollection();
				for (GroupTranslationTbl groupTranslationTbl : groupTranslationTblList) {
					LANG_ENUM langEnum = LANG_ENUM.getLangEnum(groupTranslationTbl.getLanguageCode().getLanguageCode());
					String translationId = groupTranslationTbl.getGroupTranslationId();
					translationMap.put(langEnum, translationId);
					descriptionMap.put(langEnum, groupTranslationTbl.getDescription());
					remarksMap.put(langEnum, groupTranslationTbl.getRemarks());
				}

				ProjectApplicationGroup projectApplicationGroup = new ProjectApplicationGroup(id, name, descriptionMap,
						remarksMap, icon, CommonConstants.OPERATIONMODE.VIEW);
				projectApplicationGroup.setTranslationIdMap(translationMap);
				this.projectAppGroups.add(id, projectApplicationGroup);
			}
		} catch (Exception e) {
			LOGGER.error("Exeception while getting ProjectApplication Group objects! " + e);
		}
	}

	/**
	 * Gets the user app group from services.
	 *
	 * @return the user app group from services
	 */
	private void getUserAppGroupFromServices() {
		final File iconFolder = XMSystemUtil.getXMSystemServerIconsFolder();
		try {
			final GroupController groupController = new GroupController();
			GroupResponse groupResponse = groupController
					.getGroupByGroupType(com.magna.xmbackend.vo.enums.Groups.USERAPPLICATION.name());
			List<GroupsTbl> groupTbls = groupResponse.getGroupTbls();
			for (GroupsTbl groupTblVo : groupTbls) {
				String id = groupTblVo.getGroupId();

				IconsTbl iconTbl = groupTblVo.getIconId();
				Icon icon = new Icon(iconTbl.getIconId(), iconTbl.getIconName(),
						iconFolder + File.separator + iconTbl.getIconName(), iconTbl.getIconType());

				Map<LANG_ENUM, String> descriptionMap = new HashMap<>();
				Map<LANG_ENUM, String> remarksMap = new HashMap<>();
				Map<LANG_ENUM, String> translationMap = new HashMap<>();
				String name = new String();
				name = groupTblVo.getName();
				Collection<GroupTranslationTbl> groupTranslationTblList = groupTblVo.getGroupTranslationTblCollection();
				for (GroupTranslationTbl groupTranslationTbl : groupTranslationTblList) {
					LANG_ENUM langEnum = LANG_ENUM.getLangEnum(groupTranslationTbl.getLanguageCode().getLanguageCode());
					String translationId = groupTranslationTbl.getGroupTranslationId();
					translationMap.put(langEnum, translationId);
					descriptionMap.put(langEnum, groupTranslationTbl.getDescription());
					remarksMap.put(langEnum, groupTranslationTbl.getRemarks());
				}

				UserApplicationGroup userApplicationGroup = new UserApplicationGroup(id, name, descriptionMap,
						remarksMap, icon, CommonConstants.OPERATIONMODE.VIEW);
				userApplicationGroup.setTranslationIdMap(translationMap);
				this.userAppGroups.add(id, userApplicationGroup);
			}
		} catch (Exception e) {
			LOGGER.error("Exeception while getting UserApplication Group objects! " + e);
		}
	}

	/**
	 * Gets the project group from services.
	 *
	 * @return the project group from services
	 */
	private void getProjectGroupFromServices() {
		final File iconFolder = XMSystemUtil.getXMSystemServerIconsFolder();
		try {
			final GroupController groupController = new GroupController();
			GroupResponse groupResponse = groupController
					.getGroupByGroupType(com.magna.xmbackend.vo.enums.Groups.PROJECT.name());
			List<GroupsTbl> groupTbls = groupResponse.getGroupTbls();
			for (GroupsTbl groupTblVo : groupTbls) {
				String id = groupTblVo.getGroupId();

				IconsTbl iconTbl = groupTblVo.getIconId();
				Icon icon = new Icon(iconTbl.getIconId(), iconTbl.getIconName(),
						iconFolder + File.separator + iconTbl.getIconName(), iconTbl.getIconType());

				Map<LANG_ENUM, String> descriptionMap = new HashMap<>();
				Map<LANG_ENUM, String> remarksMap = new HashMap<>();
				Map<LANG_ENUM, String> translationMap = new HashMap<>();
				String name = new String();
				name = groupTblVo.getName();
				Collection<GroupTranslationTbl> groupTranslationTblList = groupTblVo.getGroupTranslationTblCollection();
				for (GroupTranslationTbl groupTranslationTbl : groupTranslationTblList) {
					LANG_ENUM langEnum = LANG_ENUM.getLangEnum(groupTranslationTbl.getLanguageCode().getLanguageCode());
					String translationId = groupTranslationTbl.getGroupTranslationId();
					translationMap.put(langEnum, translationId);
					descriptionMap.put(langEnum, groupTranslationTbl.getDescription());
					remarksMap.put(langEnum, groupTranslationTbl.getRemarks());
				}

				ProjectGroupModel projectGroupModel = new ProjectGroupModel(id, name, descriptionMap, remarksMap, icon,
						CommonConstants.OPERATIONMODE.VIEW);
				projectGroupModel.setTranslationIdMap(translationMap);
				this.projectGroups.add(id, projectGroupModel);
			}
		} catch (Exception e) {
			LOGGER.error("Exeception while getting Project Group objects! " + e);
		}
	}
	/**
	 * Gets the live messages from service.
	 *
	 * @return the live messages from service
	 */
	private void getLiveMessagesFromService() {
		try {
			LiveMessageController liveMessageController = new LiveMessageController();
			LiveMessageResponseWrapper liveMsgResponseWrapper = liveMessageController.getAllLiveMessage();
			List<LiveMessageResponse> liveMsgResponseList = liveMsgResponseWrapper.getLiveMessageResponseWrapper();
			Map<String, String> liveMsgConfigIdMap;
			for (LiveMessageResponse liveMsgResponse : liveMsgResponseList) {
				Map<String, List<Map<String, String>>> liveMsgTo = new HashMap<>();
				Map<String, String> objectIdMap = new HashMap<>();
				String liveMsgId = liveMsgResponse.getLiveMessageId();
				String liveMsgName = liveMsgResponse.getLiveMessageName();

				Map<LANG_ENUM, String> subjectMap = new HashMap<>();
				Map<LANG_ENUM, String> messageMap = new HashMap<>();
				Map<LANG_ENUM, String> translationMap = new HashMap<>();

				Collection<LiveMessageTranslation> liveMsgTranslations = liveMsgResponse.getLiveMessageTranslations();
				for (LiveMessageTranslation liveMsgTranslation : liveMsgTranslations) {
					LANG_ENUM langEnum = LANG_ENUM.getLangEnum(liveMsgTranslation.getLanguageCode());
					String translationId = liveMsgTranslation.getLiveMsgTransId();
					translationMap.put(langEnum, translationId);
					subjectMap.put(langEnum, liveMsgTranslation.getSubject());
					messageMap.put(langEnum, liveMsgTranslation.getMessage());
				}

				String subject = liveMsgResponse.getSubject();
				String message = liveMsgResponse.getMessage();
				boolean ispopup = liveMsgResponse.getIspopup().equals(CommonConstants.TRUE) ? true : false;
				Date startDatetime = liveMsgResponse.getStartDatetime();
				Date endDatetime = liveMsgResponse.getEndDatetime();
				List<LiveMessageConfigResponse> liveMessageConfigResponses = liveMsgResponse
						.getLiveMessageConfigResponses();

				liveMsgConfigIdMap = new HashMap<>();
				for (LiveMessageConfigResponse liveMsgConfigResponse : liveMessageConfigResponses) {
					AdminAreasTbl adminAreasTbl = liveMsgConfigResponse.getAdminAreasTbl();
					SitesTbl sitesTbl = liveMsgConfigResponse.getSitesTbl();
					ProjectsTbl projectsTbl = liveMsgConfigResponse.getProjectsTbl();
					UsersTbl usersTbl = liveMsgConfigResponse.getUsersTbl();

					if (usersTbl != null && projectsTbl == null && adminAreasTbl == null && sitesTbl == null) {
						objectIdMap.put(usersTbl.getUsername(), usersTbl.getUserId());
						List<Map<String, String>> listU = liveMsgTo.get(LiveMsgToPattern.U.name());
						Map<String, String> map = new HashMap<>();
						if (listU == null) {
							listU = new ArrayList<>();
						}
						map.put(LiveMsgToPattern.U.name(), usersTbl.getUsername());
						listU.add(map);
						liveMsgTo.put(LiveMsgToPattern.U.name(), listU);
						liveMsgConfigIdMap.put(LiveMsgToPattern.U.name(),
								liveMsgConfigResponse.getLiveMessageConfigId());
					} else if (usersTbl == null && projectsTbl != null && adminAreasTbl == null && sitesTbl == null) {
						final String projectName = projectsTbl.getName();
						objectIdMap.put(projectName, projectsTbl.getProjectId());
						List<Map<String, String>> listP = liveMsgTo.get(LiveMsgToPattern.P.name());
						Map<String, String> map = new HashMap<>();
						if (listP == null) {
							listP = new ArrayList<>();
						}
						map.put(LiveMsgToPattern.P.name(), projectName);
						listP.add(map);
						liveMsgTo.put(LiveMsgToPattern.P.name(), listP);
					} else if (usersTbl == null && projectsTbl == null && adminAreasTbl == null && sitesTbl != null) {
						String siteName = sitesTbl.getName();
						objectIdMap.put(siteName, sitesTbl.getSiteId());
						List<Map<String, String>> listS = liveMsgTo.get(LiveMsgToPattern.S.name());
						Map<String, String> map = new HashMap<>();
						if (listS == null) {
							listS = new ArrayList<>();
						}
						map.put(LiveMsgToPattern.S.name(), siteName);
						listS.add(map);
						liveMsgTo.put(LiveMsgToPattern.S.name(), listS);
					} else if (usersTbl == null && projectsTbl == null && adminAreasTbl != null && sitesTbl != null) {
						String siteName = sitesTbl.getName();
						String adminAreaName = adminAreasTbl.getName();
						objectIdMap.put(adminAreaName, adminAreasTbl.getAdminAreaId());
						objectIdMap.put(siteName, sitesTbl.getSiteId());
						List<Map<String, String>> listSA = liveMsgTo.get(LiveMsgToPattern.S_A.name());
						Map<String, String> map = new HashMap<>();
						if (listSA == null) {
							listSA = new ArrayList<>();
						}
						map.put(LiveMsgToPattern.S.name(), siteName);
						map.put(LiveMsgToPattern.A.name(), adminAreaName);
						listSA.add(map);
						liveMsgTo.put(LiveMsgToPattern.S_A.name(), listSA);
					} else if (usersTbl == null && projectsTbl != null && adminAreasTbl != null && sitesTbl != null) {
						String siteName = sitesTbl.getName();
						String projectName = projectsTbl.getName();
						String adminAreaName = adminAreasTbl.getName();
						objectIdMap.put(projectName, projectsTbl.getProjectId());
						objectIdMap.put(siteName, sitesTbl.getSiteId());
						objectIdMap.put(adminAreaName, adminAreasTbl.getAdminAreaId());
						List<Map<String, String>> listSAP = liveMsgTo.get(LiveMsgToPattern.S_A_P.name());
						Map<String, String> map = new HashMap<>();
						if (listSAP == null) {
							listSAP = new ArrayList<>();
						}
						map.put(LiveMsgToPattern.S.name(), siteName);
						map.put(LiveMsgToPattern.A.name(), adminAreaName);
						map.put(LiveMsgToPattern.P.name(), projectName);
						listSAP.add(map);
						liveMsgTo.put(LiveMsgToPattern.S_A_P.name(), listSAP);
					}
				}
				LiveMessage liveMessage = new LiveMessage(liveMsgId, liveMsgTo, objectIdMap, liveMsgName, message,
						subject, messageMap, subjectMap, ispopup, startDatetime, endDatetime,
						CommonConstants.OPERATIONMODE.VIEW);
				liveMessage.setTranslationIdMap(translationMap);
				this.liveMessages.add(liveMsgId, liveMessage);
			}

		} catch (Exception e) {
			LOGGER.error("Exeception while getting live message! " + e);
		}
	}
}
