package com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.notification;

import java.beans.PropertyChangeEvent;
import java.util.Set;
import java.util.TreeSet;

import com.magna.xmsystem.dependencies.utils.XMSystemUtil;
import com.magna.xmsystem.xmadmin.util.CommonConstants;

/**
 * The Class UserProjectRelAssignEvtAction.
 */
public class UserProjectRelAssignEvtAction extends NotificationEvtActions {

	/**
	 * Instantiates a new user project rel assign evt action.
	 *
	 * @param id the id
	 * @param name the name
	 * @param isActive the is active
	 * @param operationMode the operation mode
	 */
	public UserProjectRelAssignEvtAction(final String id, final String name, final boolean isActive,
			final int operationMode) {
		super(id, name, isActive, operationMode);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.BeanModel#
	 * propertyChange(java.beans.PropertyChangeEvent)
	 */
	@Override
	public void propertyChange(PropertyChangeEvent event) {
		this.propertyChangeSupport.firePropertyChange(event.getPropertyName(), event.getOldValue(),
				event.getNewValue());
	}

	/**
	 * Deep copy pro rel assign evt action.
	 *
	 * @param update the update
	 * @param updateThisObject the update this object
	 * @return the user project rel assign evt action
	 */
	public UserProjectRelAssignEvtAction deepCopyProRelAssignEvtAction(boolean update,UserProjectRelAssignEvtAction updateThisObject) {
		UserProjectRelAssignEvtAction clonedUserProjectRelAssignEvtAction = null;

		String currentId = XMSystemUtil.isEmpty(this.getId()) ? CommonConstants.EMPTY_STR : this.getId();
		String currentName = XMSystemUtil.isEmpty(this.getName()) ? CommonConstants.EMPTY_STR : this.getName();
		boolean currentIsActive = this.isActive();
		String currentDescription = XMSystemUtil.isEmpty(this.getDescription()) ? CommonConstants.EMPTY_STR
				: this.getDescription();
		Set<String> currentToUserList = this.getToUsers();
		Set<String> currentCCUserList = this.getCcUsers();

		NotificationTemplate currentTemplate = new NotificationTemplate();
		currentTemplate.setTemplateId(this.getTemplate().getTemplateId());
		currentTemplate.setName(this.getTemplate().getName());
		currentTemplate.setSubject(this.getTemplate().getSubject());
		currentTemplate.setMessage(this.getTemplate().getMessage());
		TreeSet<String> currentVariables;
		if (this.getTemplate().getVariables() != null) {
			currentVariables = new TreeSet<>(this.getTemplate().getVariables());
		} else {
			currentVariables = new TreeSet<>();
		}
		currentTemplate.setVariables(currentVariables);

		if (update) {
			clonedUserProjectRelAssignEvtAction = updateThisObject;
		} else {
			clonedUserProjectRelAssignEvtAction = new UserProjectRelAssignEvtAction(currentId, currentName,
					currentIsActive, CommonConstants.OPERATIONMODE.VIEW);
		}
		clonedUserProjectRelAssignEvtAction.setId(currentId);
		clonedUserProjectRelAssignEvtAction.setName(currentName);
		clonedUserProjectRelAssignEvtAction.setActive(currentIsActive);
		clonedUserProjectRelAssignEvtAction.setDescription(currentDescription);
		clonedUserProjectRelAssignEvtAction.setToUsers(currentToUserList);
		clonedUserProjectRelAssignEvtAction.setCcUsers(currentCCUserList);
		clonedUserProjectRelAssignEvtAction.setTemplate(currentTemplate);

		return clonedUserProjectRelAssignEvtAction;
	}

}
