package com.magna.xmsystem.xmadmin.ui.parts.adminarea;


import org.eclipse.jface.layout.GridDataFactory;
import org.eclipse.jface.layout.GridLayoutFactory;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.ScrolledComposite;
import org.eclipse.swt.events.ControlAdapter;
import org.eclipse.swt.events.ControlEvent;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Link;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Spinner;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.widgets.ToolBar;
import org.eclipse.swt.widgets.ToolItem;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.magna.xmsystem.dependencies.utils.XMSystemUtil;
import com.magna.xmsystem.ui.controls.widgets.CustomTextAreaDialog;
import com.magna.xmsystem.xmadmin.ui.dialogs.lang.XMAdminLangTextAreaDialog;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.adminarea.AdministrationArea;
import com.magna.xmsystem.xmadmin.util.CommonConstants;
import com.magna.xmsystem.xmadmin.util.XMAdminUtil;

/**
 * class to create AdministrationArea UI
 * 
 * @author Archita.patel
 * 
 */

public abstract class AdministrationAreaUI extends Composite {

	/**
	 * Logger Instance
	 */
	private static final Logger LOGGER = LoggerFactory.getLogger(AdministrationAreaUI.class);
	/**
	 * Member variable for Save Button Name
	 */
	transient protected Button saveBtn;
	/**
	 * Member variable for Cancel Button Name
	 */
	transient protected Button cancelBtn;
	/**
	 * Member variable for Active Button
	 */
	transient protected Button activeBtn;
	/**
	 * Member variable for name label
	 */
	transient protected Label lblName;
	/**
	 * Member variable for active label
	 */
	transient protected Label lblActive;
	/**
	 * Member variable for symbol label
	 */
	transient protected Label lblSymbol;
	/**
	 * Member variable for description label
	 */
	transient protected Label lblDescrition;
	/**
	 * Member variable for text name
	 */
	transient protected Text txtName;
	/**
	 * Member variable for Text Symbol
	 */
	transient protected Text txtSymbol;
	/**
	 * Member variable for txtDesc
	 */
	transient protected Text txtDesc;
	/**
	 * Member variable for description link
	 */
	transient protected Link descLink;
	/**
	 * Member variable for help text label
	 */
	transient protected Label remarksLabel;
	/**
	 * Member variable for help text area
	 */
	transient protected Text txtRemarks;
	/**
	 * Member variable for help text link
	 */
	transient protected Link remarksTranslate;
	/**
	 * Member variable for help text count
	 */
	transient protected Label lblRemarksCount;
	/**
	 * Member variable for Group AdminArea
	 */
	transient protected Group grpAdminArea;
	/**
	 * Member variable for CustomXMAdminNewDialogArea
	 */
	transient protected CustomTextAreaDialog remarks;
	/**
	 * Member variable for parentShell
	 */
	protected Shell parentShell;
	/**
	 * Member variable for browse tool item
	 */
	protected ToolItem toolItem;

	/** The lbl contact. */
	transient protected Label lblContact;

	/** The txt contact. */
	transient protected Text txtContact;

	/** The lbl email. */
	transient protected Label lblEmail;

	/** The txt email. */
	transient protected Text txtEmail;

	/** The lbl spinner. */
	transient protected Label lblTime;

	/** The spinner. */
	transient protected Spinner timeSpinner;

	/** The lbl max time. */
	protected Label lblsecTime;

	/**
	 * Parameterized Constructor
	 * 
	 * @param parent
	 *            Composite
	 * @param style
	 *            int
	 */
	public AdministrationAreaUI(final Composite parent, int style) {
		super(parent, style);
		this.initGUI();
	}

	/**
	 * Make the GUI for components of the AdminArea Composite
	 */
	private void initGUI() {
		try {
			GridLayoutFactory.fillDefaults().applyTo(this);
			this.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));

			this.grpAdminArea = new Group(this, SWT.NONE);
			this.grpAdminArea.setBackgroundMode(SWT.INHERIT_FORCE);
			GridLayoutFactory.fillDefaults().numColumns(1).applyTo(this.grpAdminArea);
			GridDataFactory.fillDefaults().grab(true, true).align(SWT.FILL, SWT.FILL).applyTo(this.grpAdminArea);
			final ScrolledComposite scrolledComposite = XMAdminUtil.getInstance().createScrolledComposite(this.grpAdminArea);
			scrolledComposite.setBackgroundMode(SWT.INHERIT_FORCE);
			
			final Composite widgetContainer = new Composite(scrolledComposite, SWT.NONE);
			final GridLayout widgetContLayout = new GridLayout(3, false);
			widgetContLayout.horizontalSpacing = 10;

			widgetContainer.setLayout(widgetContLayout);
			widgetContainer.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));

			this.lblName = new Label(widgetContainer, SWT.NONE);
			this.txtName = new Text(widgetContainer, SWT.BORDER);
			this.txtName.setTextLimit(AdministrationArea.NAME_LIMIT);
			GridDataFactory.fillDefaults().grab(true, false).span(2, 1)
					.align(SWT.FILL, SWT.CENTER).applyTo(this.txtName);

			this.lblActive = new Label(widgetContainer, SWT.NONE);
			this.activeBtn = new Button(widgetContainer, SWT.CHECK);

			GridDataFactory.fillDefaults().grab(false, false).align(SWT.LEFT, SWT.CENTER).applyTo(this.lblActive);
			GridDataFactory.fillDefaults().grab(false, false).align(SWT.LEFT, SWT.CENTER).span(2, 1)
					.applyTo(this.activeBtn);

			this.lblContact = new Label(widgetContainer, SWT.NONE);
			this.txtContact = new Text(widgetContainer, SWT.BORDER | SWT.LEFT);
			this.txtContact.setTextLimit(AdministrationArea.NAME_LIMIT);
			GridDataFactory.fillDefaults().grab(true, false).span(2, 1).align(SWT.FILL, SWT.CENTER)
					.applyTo(this.txtContact);

			this.lblEmail = new Label(widgetContainer, SWT.NONE);
			this.txtEmail = new Text(widgetContainer, SWT.BORDER);
			this.txtEmail.setMessage(CommonConstants.Messages.EMAIL_MESSAGE);
			GridDataFactory.fillDefaults().grab(true, false).span(2, 1)
					./* indent(2, 0). */align(SWT.FILL, SWT.CENTER).applyTo(this.txtEmail);

			this.lblDescrition = new Label(widgetContainer, SWT.NONE);
			this.txtDesc = new Text(widgetContainer, SWT.BORDER);
			this.txtDesc.setTextLimit(AdministrationArea.DESC_LIMIT);

			this.txtDesc.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));
			this.descLink = new Link(widgetContainer, SWT.NONE);

			this.lblSymbol = new Label(widgetContainer, SWT.NONE);
			this.txtSymbol = new Text(widgetContainer, SWT.BORDER);
			this.txtSymbol.setEditable(false);
			GridDataFactory.fillDefaults().grab(true, false).span(1, 1).align(SWT.FILL, SWT.CENTER)
					.applyTo(this.txtSymbol);
			final ToolBar toolbar = new ToolBar(widgetContainer, SWT.NONE);
			this.toolItem = new ToolItem(toolbar, SWT.FLAT);
			this.toolItem.setImage(XMSystemUtil.getInstance().getImage(this.getClass(), "icons/16x16/browse.png"));
			toolbar.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false));

			this.lblTime = new Label(widgetContainer, SWT.NONE);
			this.timeSpinner = new Spinner(widgetContainer, SWT.BORDER);
			this.timeSpinner.setIncrement(1);
			this.timeSpinner.setPageIncrement(10);
			this.timeSpinner.setTextLimit(AdministrationArea.SPINNER_LIMIT_TIME);
			this.timeSpinner.setMaximum(AdministrationArea.MAX_TIME);
			this.timeSpinner.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));

			this.lblsecTime = new Label(widgetContainer, SWT.NONE);
			final GridData gridDataCount = new GridData(SWT.LEFT, SWT.CENTER, false, false);
			gridDataCount.widthHint = 70;
			this.lblsecTime.setLayoutData(gridDataCount);

			this.remarksLabel = new Label(widgetContainer, SWT.NONE);
			this.remarksTranslate = new Link(widgetContainer, SWT.NONE);
			this.remarksTranslate.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, true, false, 1, 1));

			this.lblRemarksCount = new Label(widgetContainer, SWT.BORDER | SWT.CENTER);
			final GridData gridDataRemarksCount = new GridData(SWT.LEFT, SWT.CENTER, false, false);
			gridDataRemarksCount.widthHint = 70;
			this.lblRemarksCount.setLayoutData(gridDataRemarksCount);

			this.txtRemarks = new Text(widgetContainer,
					SWT.BORDER | SWT.MULTI | SWT.WRAP | SWT.H_SCROLL | SWT.V_SCROLL);
			final GridData gridData = new GridData(SWT.FILL, SWT.FILL, true, true);
			gridData.minimumHeight = 100;
			gridData.horizontalSpan = 3;
			this.txtRemarks.setLayoutData(gridData);
			this.txtRemarks.setTextLimit(AdministrationArea.REMARK_LIMIT);
			this.lblRemarksCount.setText(XMAdminLangTextAreaDialog.LIMIT_COUNT_PADDING
					+ this.txtRemarks.getText().length() + XMAdminLangTextAreaDialog.SLASH
					+ AdministrationArea.REMARK_LIMIT + XMAdminLangTextAreaDialog.LIMIT_COUNT_PADDING);

			final Composite buttonBarComp = new Composite(this.grpAdminArea, SWT.NONE);
			final GridLayout btnBarCompLayout = new GridLayout(2, true);
			btnBarCompLayout.marginRight = 0;
			btnBarCompLayout.marginLeft = 0;
			btnBarCompLayout.marginTop = 0;
			btnBarCompLayout.marginBottom = 0;
			btnBarCompLayout.marginWidth = 0;
			buttonBarComp.setLayout(btnBarCompLayout);
			buttonBarComp.setLayoutData(new GridData(SWT.FILL, SWT.DEFAULT, true, false, 3, 1));
			createButtonBar(buttonBarComp);
			
			scrolledComposite.setContent(widgetContainer);
			scrolledComposite.setSize(widgetContainer.getSize());
			scrolledComposite.setExpandVertical(true);
			scrolledComposite.setExpandHorizontal(true);
			scrolledComposite.update();

			scrolledComposite.addControlListener(new ControlAdapter() {
				public void controlResized(final ControlEvent e) {
					Rectangle rectangle = scrolledComposite.getClientArea();
					scrolledComposite.setMinSize(widgetContainer.computeSize(rectangle.width, SWT.DEFAULT));
				}
			});
		} catch (Exception e) {
			LOGGER.error("Unable to crete UI elements", e);//$NON-NLS-1$
		}
	}

	/**
	 * Method to create button bar
	 * 
	 * @param buttonBarComp
	 *            {@link Composite}
	 */
	private void createButtonBar(final Composite buttonBarComp) {
		this.saveBtn = new Button(buttonBarComp, SWT.NONE);
		this.saveBtn.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, true, false));

		this.cancelBtn = new Button(buttonBarComp, SWT.NONE);
		this.cancelBtn.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, true, false));
	}

	/**
	 * method for set ButtonBar
	 */
	public void setShowButtonBar(final boolean showButtonBar) {

		if (this.saveBtn != null && !this.saveBtn.isDisposed() && this.cancelBtn != null
				&& !this.cancelBtn.isDisposed()) {
			GridData layoutData = (GridData) this.saveBtn.getParent().getLayoutData();
			layoutData.exclude = !showButtonBar;
			this.saveBtn.setVisible(showButtonBar);
			this.cancelBtn.setVisible(showButtonBar);
			this.saveBtn.getParent().setVisible(showButtonBar);
			this.saveBtn.getParent().requestLayout();
			this.saveBtn.getParent().redraw();
			this.saveBtn.getParent().getParent().update();
		}
	}

	/**
	 * Method for dispose
	 */
	@Override
	public void dispose() {
		super.dispose();
	}

	/**
	 * @return the lblName
	 */
	public Label getLblName() {
		return lblName;
	}

	/**
	 * @return the parentShel
	 */
	public Shell getParentShell() {
		return parentShell;
	}

	/**
	 * @return the lblDescrition
	 */
	public Label getLblDescrition() {
		return lblDescrition;
	}

	/**
	 * @return the grpAdminArea
	 */
	public Group getGrpAdminArea() {
		return grpAdminArea;
	}
}
