
package com.magna.xmsystem.xmadmin.ui.handlers.notitemplate;

import javax.inject.Inject;

import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.core.services.nls.Translation;
import org.eclipse.e4.ui.model.application.MApplication;
import org.eclipse.e4.ui.model.application.ui.basic.MPart;
import org.eclipse.e4.ui.workbench.modeling.EModelService;
import org.eclipse.swt.custom.StackLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.client.ResourceAccessException;

import com.magna.xmsystem.dependencies.customExceptions.UnauthorizedAccessException;
import com.magna.xmsystem.ui.controls.widgets.CustomMessageDialog;
import com.magna.xmsystem.xmadmin.message.Message;
import com.magna.xmsystem.xmadmin.ui.parts.InformationPart;
import com.magna.xmsystem.xmadmin.ui.parts.notificationtemplate.TemplateCompositeAction;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.AdminTreeviewer;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.notification.NotificationTemplate;
import com.magna.xmsystem.xmadmin.util.CommonConstants;
import com.magna.xmsystem.xmadmin.util.XMAdminUtil;

// TODO: Auto-generated Javadoc
/**
 * The Class CreateTemplate.
 */
public class CreateTemplate {

	/** The Constant LOGGER. */
	private static final Logger LOGGER = LoggerFactory.getLogger(CreateTemplate.class);

	/** The model service. */
	@Inject
	private EModelService modelService;

	/** The application. */
	@Inject
	private MApplication application;

	/** The messages. */
	@Inject
	@Translation
	transient private Message messages;

	/**
	 * Execute.
	 */
	@Execute
	public void execute() {
		try {
			MPart part = (MPart) this.modelService.find(CommonConstants.PART_ID.INFORMATION_PART_ID, application);
			InformationPart rightSidePart = (InformationPart) part.getObject();
			Composite rightcomposite = rightSidePart.getComposite();
			StackLayout rightCompLayout = rightSidePart.getCompLayout();
			TemplateCompositeAction proCreateEvtCompositeAction = rightSidePart.getTemplateCompositeUI();
			rightCompLayout.topControl = proCreateEvtCompositeAction;
			final AdminTreeviewer adminTree = XMAdminUtil.getInstance().getAdminTree();
			NotificationTemplate projectCreateEvtAction = new NotificationTemplate(null, CommonConstants.EMPTY_STR,
					null, null, CommonConstants.OPERATIONMODE.CREATE);

			proCreateEvtCompositeAction.setModel(projectCreateEvtAction);
			adminTree.setSelection(null);
			adminTree.setPreviousSelection(null);
			rightcomposite.layout();
		} catch (UnauthorizedAccessException e) {
			LOGGER.error("Current user is Unauthorized " + e);
		} catch (Exception e) {
			if (e instanceof ResourceAccessException) {
				CustomMessageDialog.openError(Display.getCurrent().getActiveShell(), messages.errorDialogTitile,
						messages.serverNotReachable);
			}
			LOGGER.error("Exception occured in calling access allowed API " + e);
		}
	}

}