package com.magna.xmsystem.xmadmin.ui.parts.objexpcontextmenu;

import javax.inject.Inject;

import org.eclipse.e4.core.services.nls.Translation;
import org.eclipse.e4.ui.model.application.MApplication;
import org.eclipse.e4.ui.model.application.ui.basic.MPart;
import org.eclipse.e4.ui.workbench.modeling.EModelService;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.swt.widgets.Display;

import com.magna.xmbackend.exception.CannotCreateObjectException;
import com.magna.xmsystem.dependencies.customExceptions.UnauthorizedAccessException;
import com.magna.xmsystem.ui.controls.widgets.CustomMessageDialog;
import com.magna.xmsystem.xmadmin.message.Message;
import com.magna.xmsystem.xmadmin.restclient.relation.UserProjectRelController;
import com.magna.xmsystem.xmadmin.ui.dialogs.ProjectExpiryDialog;
import com.magna.xmsystem.xmadmin.ui.parts.InformationPart;
import com.magna.xmsystem.xmadmin.ui.parts.objexp.ObjectExpPage;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.refs.RelationObj;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.users.User;
import com.magna.xmsystem.xmadmin.util.CommonConstants;
import com.magna.xmsystem.xmadmin.util.MessageType;
import com.magna.xmsystem.xmadmin.util.XMAdminUtil;

// TODO: Auto-generated Javadoc
/**
 * The Class ExpiryDaysAction.
 */
public class ExpiryDaysAction extends Action {

	/** The messages. */
	@Inject
	@Translation
	private Message messages;

	/** Inject of {@link EModelService}. */
	@Inject
	private EModelService modelService;

	/** Inject of {@link MApplication}. */
	@Inject
	private MApplication application;


	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jface.action.Action#run()
	 */
	@Override
	public void run() {

		MPart mPart = (MPart) this.modelService.find(CommonConstants.PART_ID.INFORMATION_PART_ID, application);
		InformationPart rightSidePart2 = (InformationPart) mPart.getObject();
		ObjectExpPage objectExpPage = rightSidePart2.getObjectExpPartUI();
		IStructuredSelection selections = (IStructuredSelection) objectExpPage.getObjExpTableViewer().getSelection();
		Object selectionObj = selections.getFirstElement();
		if (selectionObj == null) {
			selections = (IStructuredSelection) XMAdminUtil.getInstance().getAdminTree().getSelection();
			selectionObj = selections.getFirstElement();
		}
		RelationObj relationObj = (RelationObj) selectionObj;
		ProjectExpiryDialog projectExpiryDialog = new ProjectExpiryDialog(Display.getDefault().getActiveShell(),
				relationObj, messages);
		if (projectExpiryDialog.open() == IDialogConstants.OK_ID) {
			String expiryDays = projectExpiryDialog.getExpiryDays();
			UserProjectRelController propertyConfigController = new UserProjectRelController();
			boolean isUpdated;
			try {
				isUpdated = propertyConfigController
						.updateUserProjectExpiryDaysByUserProjectRelId(relationObj.getRelId(), expiryDays);
				if (isUpdated) {
					relationObj.setExpiryDays(expiryDays);
					XMAdminUtil.getInstance().updateLogFile(
							messages.projectExpiryNode + " " + messages.forLbl + " " + messages.userObject + " '"
									+ ((User) relationObj.getRefObject()).getName() + "' " + messages.objectUpdate,
							MessageType.SUCCESS);
				}
			} catch (CannotCreateObjectException e) {
				Display.getDefault().asyncExec(new Runnable() {
					public void run() {
						CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
								messages.objectPermissionDialogTitle, messages.objectPermissionDialogMsg);
					}
				});
			} catch (UnauthorizedAccessException e) {
				Display.getDefault().asyncExec(new Runnable() {
					public void run() {
						CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
								messages.objectPermissionDialogTitle, messages.objectPermissionDialogMsg);
					}
				});
			}

		}
	}

}
