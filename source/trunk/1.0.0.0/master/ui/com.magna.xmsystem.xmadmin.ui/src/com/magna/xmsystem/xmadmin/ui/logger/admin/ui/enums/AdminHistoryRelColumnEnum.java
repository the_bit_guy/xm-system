package com.magna.xmsystem.xmadmin.ui.logger.admin.ui.enums;

/**
 * The Enum AdminHistoryRelColumnEnum.
 */
public enum AdminHistoryRelColumnEnum {

	ID, LOG_TIME, ADMIN_NAME, API_REQUEST_PATH, OPERATION, SITE, ADMIN_AREA, USER_APPLICATION, PROJECT, PROJECT_APPLICATION, USER_NAME, START_APPLICATION, RELATION_TYPE, STATUS, GROUP_NAME, DIRECTORY, ROLE, ERROR_MESSAGE, RESULT

}
