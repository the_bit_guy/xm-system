package com.magna.xmsystem.xmadmin.ui.handlers.userprorelassignevt;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.inject.Inject;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.e4.core.di.annotations.CanExecute;
import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.core.services.nls.Translation;
import org.eclipse.e4.ui.model.application.MApplication;
import org.eclipse.e4.ui.model.application.ui.basic.MPart;
import org.eclipse.e4.ui.workbench.modeling.EModelService;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.swt.widgets.Display;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.client.ResourceAccessException;

import com.magna.xmbackend.vo.enums.NotificationEventType;
import com.magna.xmbackend.vo.notification.NotificationResponse;
import com.magna.xmsystem.dependencies.customExceptions.UnauthorizedAccessException;
import com.magna.xmsystem.ui.controls.widgets.CustomMessageDialog;
import com.magna.xmsystem.xmadmin.message.Message;
import com.magna.xmsystem.xmadmin.restclient.notify.NotificationController;
import com.magna.xmsystem.xmadmin.ui.parts.IEditablePart;
import com.magna.xmsystem.xmadmin.ui.parts.InformationPart;
import com.magna.xmsystem.xmadmin.ui.parts.objexp.ObjectExpPage;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.AdminTreeviewer;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.AdminTreeFactory;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.notification.UserProjectRelAssignEvt;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.notification.UserProjectRelAssignEvtAction;
import com.magna.xmsystem.xmadmin.util.CommonConstants;
import com.magna.xmsystem.xmadmin.util.MessageType;
import com.magna.xmsystem.xmadmin.util.XMAdminUtil;

public class DeleteUserProjectRelAssignEvtAction {

	/** The Constant LOGGER. */
	private static final Logger LOGGER = LoggerFactory.getLogger(DeleteUserProjectRelAssignEvtAction.class);

	/** The messages. */
	@Inject
	@Translation
	private Message messages;

	/** The model service. */
	@Inject
	private EModelService modelService;

	/** The application. */
	@Inject
	private MApplication application;

	/**
	 * Execute.
	 */
	@Execute
	public void execute() {
		try {
			final AdminTreeviewer adminTree = XMAdminUtil.getInstance().getAdminTree();
			IStructuredSelection selection;
			Object selectionObj;
			MPart mPart = (MPart) this.modelService.find(CommonConstants.PART_ID.INFORMATION_PART_ID, application);
			InformationPart rightSidePart = (InformationPart) mPart.getObject();
			ObjectExpPage objectExpPage = rightSidePart.getObjectExpPartUI();
			if (objectExpPage != null
					&& (selection = objectExpPage.getObjExpTableViewer().getStructuredSelection()) != null
					&& (selectionObj = selection.getFirstElement()) != null
					&& selectionObj instanceof UserProjectRelAssignEvtAction) {
				deleteAction(selection);
				objectExpPage.refreshExplorer();
			} else if (adminTree != null && (selection = adminTree.getStructuredSelection()) != null
					&& (selectionObj = selection.getFirstElement()) != null
					&& selectionObj instanceof UserProjectRelAssignEvtAction) {
				deleteAction(selection);
			}
			XMAdminUtil.getInstance().clearClipBoardContents();
		} catch (UnauthorizedAccessException e) {
			LOGGER.error("Current user is Unauthorized " + e);
		} catch (Exception e) {
			if (e instanceof ResourceAccessException) {
				CustomMessageDialog.openError(Display.getCurrent().getActiveShell(), messages.errorDialogTitile,
						messages.serverNotReachable);
			}
			LOGGER.error("Exception occured in calling access allowed API " + e);
		}
	}

	
	/**
	 * Delete action.
	 *
	 * @param selection the selection
	 */
	@SuppressWarnings("rawtypes")
	private void deleteAction(IStructuredSelection selection) {
		Object selectionObj = selection.getFirstElement();
		final AdminTreeviewer adminTree = XMAdminUtil.getInstance().getAdminTree();
		final UserProjectRelAssignEvt event = AdminTreeFactory.getInstance().getNotifications().getUserProjectRelAssignEvt();
		final String name = ((UserProjectRelAssignEvtAction) selectionObj).getName();
		String confirmDialogTitle = messages.deleteConfirmDialogTitle;
		String confirmDialogMsg;
		if (selection.size() == 1) {
			confirmDialogMsg = messages.deleteNotiActionConfirmDialogMsg + " \'" + name + "\' " + " ?";
		} else {
			confirmDialogMsg = messages.deleteMultiObjectsConfirmDialogMsg + " " + " ?";
		}
		boolean isConfirmed = CustomMessageDialog.openConfirm(adminTree.getControl().getShell(), confirmDialogTitle,
				confirmDialogMsg);
		if (isConfirmed) {
			List selectionList = selection.toList();
			Job job = new Job("Deleting Objects...") {

				@Override
				protected IStatus run(IProgressMonitor monitor) {
					monitor.beginTask("Deleting Objects..", 100);
					monitor.worked(30);
					try {
						Set<String> actionIds = new HashSet<>();
						Map<String, String> successObjectNames = new HashMap<>();
						Map<String, String> failObjectNames = new HashMap<>();
						for (int i = 0; i < selectionList.size(); i++) {
							String id = ((UserProjectRelAssignEvtAction) selectionList.get(i)).getId();
							String name = ((UserProjectRelAssignEvtAction) selectionList.get(i)).getName();
							actionIds.add(id);
							successObjectNames.put(id, name);
						}
						final NotificationController notificationController = new NotificationController();
						final NotificationResponse response = notificationController.deleteMultiAction(actionIds);
						if (response != null) {
							if (!response.getStatusMaps().isEmpty()) {
								List<Map<String, String>> statusMapList = response.getStatusMaps();
								for (Map<String, String> statusMap : statusMapList) {
									String string = statusMap.get("en");
									String[] split = string.split("id");
									actionIds.remove(split[1].trim());
									failObjectNames.put(split[1].trim(), successObjectNames.get(split[1].trim()));
									successObjectNames.remove(split[1].trim());
								}
								Display.getDefault().asyncExec(new Runnable() {
									public void run() {
										CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
												messages.deleteConfirmDialogTitle,
												messages.deleteMultiObjectsDialogErrMsg);
									}
								});
							}
							for (String actionId : actionIds) {
								event.getUserProRelAssignEvtChild().remove(actionId);
							}
							Display.getDefault().asyncExec(new Runnable() {
								public void run() {
									adminTree.refresh();
									adminTree.setSelection(new StructuredSelection(event), true);
								}
							});
							if (successObjectNames.size() > 0) {
								Iterator it = successObjectNames.entrySet().iterator();
								while (it.hasNext()) {
									Map.Entry map = (Map.Entry) it.next();
									XMAdminUtil.getInstance()
											.updateLogFile(
													messages.notificationNode+" "+NotificationEventType.USER_PROJECT_RELATION_ASSIGN + " " + messages.actionLbl + " '"
															+ map.getValue() + "' " + messages.objectDelete,
													MessageType.SUCCESS);
								}
							}
							if (failObjectNames.size() > 0) {
								Iterator it = failObjectNames.entrySet().iterator();
								while (it.hasNext()) {
									Map.Entry map = (Map.Entry) it.next();
									XMAdminUtil.getInstance().updateLogFile(
											messages.deleteErrorMsg + " " + messages.notificationNode +" "+NotificationEventType.USER_PROJECT_RELATION_REMOVE + " " 
													+ messages.actionLbl + " '" + map.getValue() + "'",
											MessageType.FAILURE);
								}
							}
						}

					} catch (Exception e) {
						LOGGER.error(e.getMessage());
					}

					monitor.worked(70);
					return Status.OK_STATUS;
				}
			};
			job.setUser(true);
			job.schedule();
		}
	}
	
	/**
	 * Can execute.
	 *
	 * @return true, if successful
	 */
	@CanExecute
	public boolean canExecute() {
		boolean returnVal = true;
		final XMAdminUtil instance = XMAdminUtil.getInstance();
		MPart rightPart;
		Object rightView;
		if ((rightPart = instance.getInformationPart()) != null
				&& (rightView = rightPart.getObject()) != null && rightView instanceof IEditablePart
				&& ((IEditablePart) rightView).isDirty()) {
			returnVal = false;
		}
		return returnVal;
	}
}
