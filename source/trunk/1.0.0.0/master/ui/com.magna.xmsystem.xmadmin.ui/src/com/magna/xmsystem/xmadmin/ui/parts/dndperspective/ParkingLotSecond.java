package com.magna.xmsystem.xmadmin.ui.parts.dndperspective;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.eclipse.e4.core.services.events.IEventBroker;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.swt.widgets.Composite;

public class ParkingLotSecond extends ParkingLotBase {
	/** The event broker. */
	@Inject
	private IEventBroker eventBroker;

	/**
	 * Method to create UI
	 * 
	 * @param parent
	 *            {@link Composite}
	 */
	@PostConstruct
	public void createUI(final Composite parentP) {
		super.createUI(parentP);
	}

	@Override
	protected void sendEvent(IStructuredSelection selection) {
		eventBroker.send("UpdateSubAdminTreeSecond", selection);
	}
}
