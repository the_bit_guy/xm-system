package com.magna.xmsystem.xmadmin.ui.handlers;

import javax.inject.Inject;

import org.eclipse.e4.core.contexts.ContextInjectionFactory;
import org.eclipse.e4.core.contexts.IEclipseContext;
import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.ui.workbench.modeling.EPartService;

import com.magna.xmsystem.xmadmin.ui.parts.objexpcontextmenu.DeActivateRelAction;

// TODO: Auto-generated Javadoc
/**
 * The Class DeActivateRelHandler.
 */
public class DeActivateRelHandler {

	/** The eclipse context. */
	@Inject
	private IEclipseContext eclipseContext;

	/**
	 * Execute.
	 *
	 * @param partService
	 *            the part service
	 */
	@Execute
	public void execute(EPartService partService) {
		DeActivateRelAction deActivateRel = ContextInjectionFactory.make(DeActivateRelAction.class, eclipseContext);
		deActivateRel.run();
	}

}
