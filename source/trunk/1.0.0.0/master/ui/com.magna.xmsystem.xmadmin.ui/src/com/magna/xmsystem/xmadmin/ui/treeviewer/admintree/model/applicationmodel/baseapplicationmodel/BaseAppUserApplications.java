package com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.baseapplicationmodel;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.stream.Collectors;

import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.userapplicationmodel.UserApplication;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.refs.RelationObj;

/**
 * The Class BaseAppUserAppModel.
 */
public class BaseAppUserApplications implements IAdminTreeChild {

	/** The parent. */
	private IAdminTreeChild parent;

	/** The base app user app children. */
	private Map<String, IAdminTreeChild> baseAppUserAppChildren;

	/**
	 * Instantiates a new base app user app model.
	 *
	 * @param parent the parent
	 */
	public BaseAppUserApplications(IAdminTreeChild parent) {
		super();
		this.parent = parent;
		this.baseAppUserAppChildren = new LinkedHashMap<>();
	}
	
	/**
	 * Adds the.
	 *
	 * @param baseAppUserAppChildrenId the base app user app children id
	 * @param child the child
	 * @return the i admin tree child
	 */
	public IAdminTreeChild add(final String baseAppUserAppChildrenId, final IAdminTreeChild child) {
		child.setParent(this);
		IAdminTreeChild returnVal = this.baseAppUserAppChildren.put(baseAppUserAppChildrenId, child);
		if (child instanceof RelationObj && ((RelationObj) child).getRefObject() instanceof UserApplication) {
			sort();
		}
		return returnVal;
	}

	/**
	 * Removes the.
	 *
	 * @param baseAppUserAppChildrenId the base app user app children id
	 * @return the i admin tree child
	 */
	public IAdminTreeChild remove(final String baseAppUserAppChildrenId) {
		return this.baseAppUserAppChildren.remove(baseAppUserAppChildrenId);
	}

	/**
	 * Gets the base app user app collection.
	 *
	 * @return the base app user app collection
	 */
	public Collection<IAdminTreeChild> getBaseAppUserAppCollection() {
		return this.baseAppUserAppChildren.values();
	}

	/**
	 * Gets the base app user app children.
	 *
	 * @return the base app user app children
	 */
	public Map<String, IAdminTreeChild> getBaseAppUserAppChildren() {
		return baseAppUserAppChildren;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild#
	 * getParent()
	 */
	@Override
	public IAdminTreeChild getParent() {
		return this.parent;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild#
	 * setParent(com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.
	 * IAdminTreeChild)
	 */
	@Override
	public void setParent(IAdminTreeChild parent) {
		this.parent = parent;

	}

	/**
	 * Sort.
	 */
	public void sort() {
		final LinkedHashMap<String, IAdminTreeChild> collect = this.baseAppUserAppChildren.entrySet().stream().sorted(
				(e1, e2) -> ((UserApplication) e1.getValue()).getName().compareTo(((UserApplication) e2.getValue()).getName()))
				.collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e1, LinkedHashMap::new));
		this.baseAppUserAppChildren = collect;
	}
}
