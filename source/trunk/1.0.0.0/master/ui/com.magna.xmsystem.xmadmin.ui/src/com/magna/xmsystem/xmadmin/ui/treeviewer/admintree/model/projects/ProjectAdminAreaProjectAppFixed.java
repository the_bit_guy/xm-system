package com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.projects;

import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild;

/**
 * Class for Project admin area project app fixed.
 *
 * @author Chiranjeevi.Akula
 */
public class ProjectAdminAreaProjectAppFixed implements IAdminTreeChild {

	/** Member variable 'parent' for {@link IAdminTreeChild}. */
	private IAdminTreeChild parent;

	/**
	 * Constructor for ProjectAdminAreaProjectAppFixed Class.
	 *
	 * @param parent {@link IAdminTreeChild}
	 */
	public ProjectAdminAreaProjectAppFixed(final IAdminTreeChild parent) {
		this.parent = parent;
	}
	
	/* (non-Javadoc)
	 * @see com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild#getParent()
	 */
	@Override
	public IAdminTreeChild getParent() {
		return this.parent;
	}

	/* (non-Javadoc)
	 * @see com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild#setParent(com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild)
	 */
	@Override
	public void setParent(IAdminTreeChild parent) {
		this.parent = parent;
	}

}
