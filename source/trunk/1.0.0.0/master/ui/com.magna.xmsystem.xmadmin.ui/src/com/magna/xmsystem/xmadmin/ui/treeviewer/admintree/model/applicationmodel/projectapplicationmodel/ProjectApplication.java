package com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.projectapplicationmodel;

import java.beans.PropertyChangeEvent;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.core.runtime.Assert;

import com.magna.xmsystem.dependencies.utils.LANG_ENUM;
import com.magna.xmsystem.dependencies.utils.XMSystemUtil;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.BeanModel;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.adminarea.AdminAreaProjectAppFixed;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.adminarea.AdminAreaProjectAppNotFixed;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.adminarea.AdminAreaProjectAppProtected;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.adminarea.AdminAreaProjectApplications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.baseapplicationmodel.BaseAppProjectApplications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.adminconf.AdminProjectApps;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.icon.Icon;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.directory.DirectoryProjectApplications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.group.projectappgroup.ProjectAppGroupProjectApps;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.projects.ProjectAdminAreaProjectAppFixed;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.projects.ProjectAdminAreaProjectAppNotFixed;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.projects.ProjectAdminAreaProjectAppProtected;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.projects.ProjectAdminAreaProjectApplications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.projects.ProjectProjectApplications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.projects.ProjectUserAAProjectAppAllowed;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.projects.ProjectUserAAProjectAppForbidden;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.projects.ProjectUserAAProjectApplications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.refs.RelationObj;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.sitemodel.SiteAdminAreaProjectApplications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.sitemodel.SiteAdminProjectAppFixed;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.sitemodel.SiteAdminProjectAppNotFixed;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.sitemodel.SiteAdminProjectAppProtected;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.users.UserProjectAAProjectAppAllowed;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.users.UserProjectAAProjectAppForbidden;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.users.UserProjectAAProjectApplications;
import com.magna.xmsystem.xmadmin.util.CommonConstants;

/**
 * Class for Project application.
 *
 * @author Chiranjeevi.Akula
 */
public class ProjectApplication extends BeanModel implements IAdminTreeChild, Cloneable {
	
	/** The parent. */
	private IAdminTreeChild parentNode;

	/** The Constant NAME_LIMIT. */
	public static final int NAME_LIMIT = 30; // $NON-NLS-1$

	/** The Constant DESCRIPTION_LIMIT. */
	public static final int DESCRIPTION_LIMIT = 240; // $NON-NLS-1$

	/** The Constant REMARK_LIMIT. */
	public static final int REMARK_LIMIT = 1500; // $NON-NLS-1$

	/** The Constant PROPERTY_APPLICATIONID. */
	public static final String PROPERTY_APPLICATIONID = "projectApplicationId"; //$NON-NLS-1$

	/** The Constant PROPERTY_NAME_MAP. */
	public static final String PROPERTY_NAME_MAP = "nameMap"; //$NON-NLS-1$

	/** The Constant PROPERTY_ACTIVE. */
	public static final String PROPERTY_ACTIVE = "active"; //$NON-NLS-1$

	/** The Constant PROPERTY_PARENT. */
	public static final String PROPERTY_PARENT = "parent"; //$NON-NLS-1$
	
	/** The Constant PROPERTY_SINGLETON. */
	public static final String PROPERTY_SINGLETON = "singleton"; //$NON-NLS-1$

	/** The Constant PROPERTY_DESC_MAP. */
	public static final String PROPERTY_DESC_MAP = "descriptionMap"; //$NON-NLS-1$

	/** The Constant PROPERTY_REMARKS_MAP. */
	public static final String PROPERTY_REMARKS_MAP = "remarksMap"; //$NON-NLS-1$

	/** The Constant PROPERTY_POSITION. */
	public static final String PROPERTY_POSITION = "position"; //$NON-NLS-1$

	/** The Constant PROPERTY_BASE_APPLICATION. */
	public static final String PROPERTY_BASE_APPLICATION = "baseApplicationId"; //$NON-NLS-1$

	/** The Constant PROPERTY_OPERATION_MODE. */
	public static final String PROPERTY_OPERATION_MODE = "operationMode"; //$NON-NLS-1$

	/** The Constant PROPERTY_ICON. */
	public static final String PROPERTY_ICON = "icon"; //$NON-NLS-1$
	
	/** The Constant PROPERTY_DESC. */
	public static final String PROPERTY_DESC = "description"; //$NON-NLS-1$
	
	/** The Constant PROPERTY_NAME. */
	public static final String PROPERTY_NAME = "name"; //$NON-NLS-1$

	/** Member variable 'project application id' for {@link String}. */
	private String projectApplicationId;

	/** Member variable 'name map' for {@link Map<LANG_ENUM,String>}. */
	private Map<LANG_ENUM, String> nameMap;

	/** Member variable 'active' for {@link Boolean}. */
	private boolean active;

	/** Member variable 'parent' for {@link Boolean}. */
	private boolean parent;
	
	/** Member variable 'singleton' for {@link Boolean}. */
	private boolean singleton;

	/** Member variable 'description map' for {@link Map<LANG_ENUM,String>}. */
	private Map<LANG_ENUM, String> descriptionMap;

	/** Member variable 'remarks map' for {@link Map<LANG_ENUM,String>}. */
	private Map<LANG_ENUM, String> remarksMap;

	/** Member variable 'icon' for {@link Icon}. */
	private Icon icon;

	/** Member variable 'position' for {@link String}. */
	private String position;

	/** Member variable 'base application id' for {@link String}. */
	private String baseApplicationId;

	/** Member variable 'operation mode' for {@link Int}. */
	private int operationMode;

	/** The project children. */
	private Map<String, IAdminTreeChild> projectApplicationChildren;

	/** Member variable 'rel ids' for {@link List<String>}. */
	private List<String> relIds;
	
	/** The name. */
	private String name;

	/** The description. */
	private String description;

	/**
	 * Instantiates a new project application.
	 *
	 * @param projectApplicationId the project application id
	 * @param name the name
	 * @param description the description
	 * @param nameMap the name map
	 * @param isActive the is active
	 * @param icon the icon
	 * @param operationMode the operation mode
	 */
	public ProjectApplication(final String projectApplicationId, final String name, final  String description, final Map<LANG_ENUM, String> nameMap,
			final boolean isActive, final Icon icon, final int operationMode) {
		this(projectApplicationId, name, description, nameMap, isActive, new HashMap<>(), new HashMap<>(), icon, false, false, null, null,
				operationMode);
	}
	
	/**
	 * Instantiates a new project application.
	 *
	 * @param projectApplicationId the project application id
	 * @param name the name
	 * @param description the description
	 * @param nameMap the name map
	 * @param isActive the is active
	 * @param descriptionMap the description map
	 * @param remarksMap the remarks map
	 * @param icon the icon
	 * @param parent the parent
	 * @param singleton the singleton
	 * @param position the position
	 * @param baseApplicationId the base application id
	 * @param operationMode the operation mode
	 */
	public ProjectApplication(final String projectApplicationId, final String name, final  String description, final Map<LANG_ENUM, String> nameMap,
			final boolean isActive, final Map<LANG_ENUM, String> descriptionMap,
			final Map<LANG_ENUM, String> remarksMap, final Icon icon, final boolean parent ,final boolean singleton, final String position,
			final String baseApplicationId, final int operationMode) {
		super();
		this.projectApplicationId = projectApplicationId;
		this.name = name;
		this.description = description;
		this.nameMap = nameMap;
		this.active = isActive;
		this.descriptionMap = descriptionMap;
		this.remarksMap = remarksMap;
		this.icon = icon;
		this.parent = parent;
		this.singleton=singleton;
		this.position = position;
		this.baseApplicationId = baseApplicationId;
		this.operationMode = operationMode;
		this.projectApplicationChildren = new LinkedHashMap<>();
		
		this.relIds = new ArrayList<>();
		addFixedChildren();
	}
	
	/**
	 * Method for Adds the fixed children.
	 */
	public void addFixedChildren() {
		this.projectApplicationChildren.put(ProjectAppAdminAreas.class.getName(), new ProjectAppAdminAreas(this));
		this.projectApplicationChildren.put(ProjectAppUsers.class.getName(), new ProjectAppUsers(this));
	}
	
	/**
	 * Adds the child
	 * 
	 * @param child
	 *            {@link IAdminTreeChild}
	 * @return {@link boolean}
	 */
	public IAdminTreeChild add(final String projectAppId,final IAdminTreeChild child) {
		child.setParent(this);
		return this.projectApplicationChildren.put(projectAppId, child);
	}
	
	/**
	 * Removes the child
	 * 
	 * @param child
	 *            {@link IAdminTreeChild}
	 * @return {@link boolean}
	 */
	public IAdminTreeChild remove(final String child) {
		return this.projectApplicationChildren.remove(child);
	}
	
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(final String name) {
		this.propertyChangeSupport.firePropertyChange(PROPERTY_NAME, this.name, this.name = name.trim());
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.propertyChangeSupport.firePropertyChange(PROPERTY_DESC, this.description, this.description = description);
	}

	/**
	 * Gets the name map.
	 *
	 * @return the name map
	 */
	public Map<LANG_ENUM, String> getNameMap() {
		return this.nameMap;

	}

	/**
	 * Gets the name.
	 *
	 * @param lang
	 *            {@link LANG_ENUM}
	 * @return the name
	 */
	public String getName(final LANG_ENUM lang) {
		return this.nameMap.get(lang);
	}

	/**
	 * Method for Sets the name.
	 *
	 * @param lang
	 *            {@link LANG_ENUM}
	 * @param name
	 *            {@link String}
	 */
	public void setName(final LANG_ENUM lang, final String name) {
		if (lang == null || name == null)
			throw new IllegalArgumentException();
		this.propertyChangeSupport.firePropertyChange(PROPERTY_NAME_MAP, this.nameMap,
				this.nameMap.put(lang, name.trim()));
	}

	/**
	 * Method for Sets the name map.
	 *
	 * @param nameMap
	 *            {@link Map<LANG_ENUM,String>}
	 */
	public void setNameMap(final Map<LANG_ENUM, String> nameMap) {
		if (nameMap == null)
			throw new IllegalArgumentException();
		this.propertyChangeSupport.firePropertyChange(PROPERTY_NAME_MAP, this.nameMap, this.nameMap = nameMap);
	}

	/**
	 * Checks if is active.
	 *
	 * @return true, if is active
	 */
	public boolean isActive() {
		return active;
	}

	/**
	 * Sets the active.
	 *
	 * @param isActive
	 *            the new active
	 */
	public void setActive(final boolean isActive) {
		this.propertyChangeSupport.firePropertyChange(PROPERTY_ACTIVE, this.active, this.active = isActive);
	}

	/**
	 * Checks if is parent.
	 *
	 * @return true, if is parent
	 */
	public boolean isParent() {
		return parent;
	}

	/**
	 * Sets the parent.
	 *
	 * @param parent
	 *            the new parent
	 */
	public void setParent(boolean parent) {
		this.propertyChangeSupport.firePropertyChange(PROPERTY_PARENT, this.parent, this.parent = parent);
	}
	
	/**
	 * Checks if is singleton.
	 *
	 * @return true, if is singleton
	 */
	public boolean isSingleton() {
		return singleton;
	}

	/**
	 * Sets the singleton.
	 *
	 * @param singleton
	 *            the new singleton
	 */
	public void setSingleton(final boolean singleton) {
		this.propertyChangeSupport.firePropertyChange(PROPERTY_SINGLETON, this.singleton, this.singleton = singleton);
	}

	/**
	 * Gets the description map.
	 *
	 * @return the description map
	 */
	public Map<LANG_ENUM, String> getDescriptionMap() {
		return descriptionMap;
	}

	/**
	 * Gets the description.
	 *
	 * @param lang
	 *            {@link LANG_ENUM}
	 * @return the description
	 */
	public String getDescription(final LANG_ENUM lang) {
		return this.descriptionMap.get(lang);
	}

	/**
	 * Method for Sets the description.
	 *
	 * @param lang
	 *            {@link LANG_ENUM}
	 * @param description
	 *            {@link String}
	 */
	public void setDescription(final LANG_ENUM lang, final String description) {
		if (lang == null || description == null)
			throw new IllegalArgumentException();
		this.propertyChangeSupport.firePropertyChange(PROPERTY_DESC_MAP, this.descriptionMap,
				this.descriptionMap.put(lang, description.trim()));
	}

	/**
	 * Method for Sets the description map.
	 *
	 * @param descriptionMap
	 *            {@link Map<LANG_ENUM,String>}
	 */
	public void setDescriptionMap(final Map<LANG_ENUM, String> descriptionMap) {
		if (descriptionMap == null)
			throw new IllegalArgumentException();
		this.propertyChangeSupport.firePropertyChange(PROPERTY_DESC_MAP, this.descriptionMap,
				this.descriptionMap = descriptionMap);
	}

	/**
	 * Gets the remarks map.
	 *
	 * @return the remarks map
	 */
	public Map<LANG_ENUM, String> getRemarksMap() {
		return remarksMap;
	}

	/**
	 * Gets the remarks.
	 *
	 * @param lang
	 *            {@link LANG_ENUM}
	 * @return the remarks
	 */
	public String getRemarks(final LANG_ENUM lang) {
		return this.remarksMap.get(lang);
	}

	/**
	 * Method for Sets the remarks map.
	 *
	 * @param remarksMap
	 *            {@link Map<LANG_ENUM,String>}
	 */
	public void setRemarksMap(final Map<LANG_ENUM, String> remarksMap) {
		if (remarksMap == null)
			throw new IllegalArgumentException();
		this.propertyChangeSupport.firePropertyChange(PROPERTY_REMARKS_MAP, this.remarksMap,
				this.remarksMap = remarksMap);
	}

	/**
	 * Method for Sets the remarks.
	 *
	 * @param lang
	 *            {@link LANG_ENUM}
	 * @param remarks
	 *            {@link String}
	 */
	public void setRemarks(final LANG_ENUM lang, final String remarks) {
		if (lang == null || remarks == null)
			throw new IllegalArgumentException();
		this.propertyChangeSupport.firePropertyChange(PROPERTY_REMARKS_MAP, this.remarksMap,
				this.remarksMap.put(lang, remarks));
	}

	/**
	 * Gets the project application id.
	 *
	 * @return the project application id
	 */
	public String getProjectApplicationId() {
		return projectApplicationId;
	}

	/**
	 * Sets the project application id.
	 *
	 * @param projectApplicationId
	 *            the new project application id
	 */
	public void setProjectApplicationId(String projectApplicationId) {
		this.propertyChangeSupport.firePropertyChange(PROPERTY_APPLICATIONID, this.projectApplicationId,
				this.projectApplicationId = projectApplicationId);
	}

	/**
	 * Gets the icon.
	 *
	 * @return the icon
	 */
	public Icon getIcon() {
		return icon;
	}

	/**
	 * Sets the icon.
	 *
	 * @param icon
	 *            the new icon
	 */
	public void setIcon(Icon icon) {
		if (icon == null)
			throw new IllegalArgumentException();
		this.propertyChangeSupport.firePropertyChange(PROPERTY_ICON, this.icon, this.icon = icon);
	}

	public Map<String, IAdminTreeChild> getProjectApplicationChildren() {
		return projectApplicationChildren;
	}

	/**
	 * Gets the operation mode.
	 *
	 * @return the operation mode
	 */
	public int getOperationMode() {
		return operationMode;
	}

	/**
	 * Sets the operation mode.
	 *
	 * @param operationMode
	 *            the new operation mode
	 */
	public void setOperationMode(int operationMode) {
		this.propertyChangeSupport.firePropertyChange(PROPERTY_OPERATION_MODE, this.operationMode,
				this.operationMode = operationMode);
	}

	/**
	 * Gets the position.
	 *
	 * @return the position
	 */
	public String getPosition() {
		return position;
	}

	/**
	 * Sets the position.
	 *
	 * @param position
	 *            the new position
	 */
	public void setPosition(String position) {
		this.propertyChangeSupport.firePropertyChange(PROPERTY_POSITION, this.position, this.position = position);
	}

	/**
	 * Gets the base application id.
	 *
	 * @return the base application id
	 */
	public String getBaseApplicationId() {
		return baseApplicationId;
	}

	/**
	 * Sets the base application id.
	 *
	 * @param baseApplicationId
	 *            the new base application id
	 */
	public void setBaseApplicationId(String baseApplicationId) {
		this.propertyChangeSupport.firePropertyChange(PROPERTY_BASE_APPLICATION, this.baseApplicationId,
				this.baseApplicationId = baseApplicationId);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.BeanModel#
	 * propertyChange(java.beans.PropertyChangeEvent)
	 */
	@Override
	public void propertyChange(PropertyChangeEvent event) {
		this.propertyChangeSupport.firePropertyChange(event.getPropertyName(), event.getOldValue(),
				event.getNewValue());
	}

	/**
	 * Method for Deep copy project application.
	 *
	 * @param update
	 *            {@link boolean}
	 * @param updateThisObject
	 *            {@link ProjectApplication}
	 * @return the project application {@link ProjectApplication}
	 */
	public ProjectApplication deepCopyProjectApplication(boolean update, ProjectApplication updateThisObject) {
		ProjectApplication clonedProjectApplication = null;
		String currentProjectApplicationId = XMSystemUtil.isEmpty(this.getProjectApplicationId())
				? CommonConstants.EMPTY_STR : this.getProjectApplicationId();
		String currentInternalName = this.getName();
		String currentInternalDesc = this.getDescription();
		//Icon currentIcon = this.getIcon() == null ? null : this.getIcon();
		Icon currentIcon = new Icon(this.getIcon().getIconId(), this.getIcon().getIconName(),
				this.getIcon().getIconPath(), this.getIcon().getIconType());
		boolean currentIsActive = this.isActive();
		boolean currentIsParent = this.isParent();
		boolean currentIsSingleton = this.isSingleton();
		String currentPosition = XMSystemUtil.isEmpty(this.getPosition()) ? CommonConstants.EMPTY_STR
				: this.getPosition();
		String currentBaseAppId = XMSystemUtil.isEmpty(this.getBaseApplicationId()) ? null
				: this.getBaseApplicationId();

		Map<LANG_ENUM, String> currentTranslationIdMap = new HashMap<>();
		Map<LANG_ENUM, String> currentNameMap = new HashMap<>();
		Map<LANG_ENUM, String> currentDescriptionMap = new HashMap<>();
		Map<LANG_ENUM, String> currentRemarksMap = new HashMap<>();

		for (LANG_ENUM langEnum : LANG_ENUM.values()) {
			String translationId = this.getTranslationId(langEnum);
			currentTranslationIdMap.put(langEnum,
					XMSystemUtil.isEmpty(translationId) ? CommonConstants.EMPTY_STR : translationId);

			String name = this.getName(langEnum);
			currentNameMap.put(langEnum, XMSystemUtil.isEmpty(name) ? CommonConstants.EMPTY_STR : name);

			String description = this.getDescription(langEnum);
			currentDescriptionMap.put(langEnum,
					XMSystemUtil.isEmpty(description) ? CommonConstants.EMPTY_STR : description);

			String remarks = this.getRemarks(langEnum);
			currentRemarksMap.put(langEnum, XMSystemUtil.isEmpty(remarks) ? CommonConstants.EMPTY_STR : remarks);
		}

		if (update) {
			clonedProjectApplication = updateThisObject;
		} else {
			clonedProjectApplication = new ProjectApplication(currentProjectApplicationId, currentInternalName, currentInternalDesc, currentNameMap,
					currentIsActive, currentIcon, CommonConstants.OPERATIONMODE.VIEW);
		}
		clonedProjectApplication.setName(currentInternalName);
		clonedProjectApplication.setDescription(currentInternalDesc);
		clonedProjectApplication.setProjectApplicationId(currentProjectApplicationId);
		clonedProjectApplication.setActive(currentIsActive);
		clonedProjectApplication.setParent(currentIsParent);
		clonedProjectApplication.setSingleton(currentIsSingleton);
		clonedProjectApplication.setNameMap(currentNameMap);
		clonedProjectApplication.setIcon(currentIcon);
		clonedProjectApplication.setTranslationIdMap(currentTranslationIdMap);
		clonedProjectApplication.setDescriptionMap(currentDescriptionMap);
		clonedProjectApplication.setRemarksMap(currentRemarksMap);
		clonedProjectApplication.setBaseApplicationId(currentBaseAppId);
		clonedProjectApplication.setPosition(currentPosition);

		return clonedProjectApplication;

	}
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild#
	 * getParent()
	 */
	@Override
	public IAdminTreeChild getParent() {
		return this.parentNode;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild#
	 * setParent(com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.
	 * IAdminTreeChild)
	 */
	@Override
	public void setParent(IAdminTreeChild parentNode) {
		this.parentNode = parentNode;

	}
	
	/* (non-Javadoc)
	 * @see com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.BeanModel#getAdapter(java.lang.Class, com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild)
	 */
	@Override
	public <T> RelationObj getAdapter(Class<T> adapterType, IAdminTreeChild parent, String relationId, boolean relationStatus) {
		Assert.isNotNull(adapterType);
		Assert.isNotNull(this);
		/*String relId = UUID.randomUUID().toString();*/
		this.relIds.add(relationId);
		if (adapterType == SiteAdminAreaProjectApplications.class) {
			SiteAdminAreaProjectApplications siteAdminAreaProjectApplications = new SiteAdminAreaProjectApplications(
					parent);
			RelationObj relObj = new RelationObj(relationId, this, siteAdminAreaProjectApplications, relationStatus,
					null);
			return relObj;
		} else if (adapterType == SiteAdminProjectAppNotFixed.class) {
			SiteAdminProjectAppNotFixed siteAdminProjectAppNotFixed = new SiteAdminProjectAppNotFixed(parent);
			RelationObj relObj = new RelationObj(relationId, this, siteAdminProjectAppNotFixed, relationStatus, null);
			return relObj;
		} else if (adapterType == SiteAdminProjectAppFixed.class) {
			SiteAdminProjectAppFixed siteAdminProjectAppFixed = new SiteAdminProjectAppFixed(parent);
			RelationObj relObj = new RelationObj(relationId, this, siteAdminProjectAppFixed, relationStatus, null);
			return relObj;
		} else if (adapterType == SiteAdminProjectAppProtected.class) {
			SiteAdminProjectAppProtected siteAdminProjectAppProtected = new SiteAdminProjectAppProtected(parent);
			RelationObj relObj = new RelationObj(relationId, this, siteAdminProjectAppProtected, relationStatus, null);
			return relObj;
		} else if (adapterType == AdminProjectApps.class) {
			AdminProjectApps adminProjectApps = new AdminProjectApps(parent);
			RelationObj relObj = new RelationObj(relationId, this, adminProjectApps, relationStatus, null);
			return relObj;
		} else if (adapterType == AdminAreaProjectApplications.class) {
			AdminAreaProjectApplications adminAreaProjectApplications = new AdminAreaProjectApplications(parent);
			RelationObj relObj = new RelationObj(relationId, this, adminAreaProjectApplications, relationStatus, null);
			return relObj;
		} else if (adapterType == AdminAreaProjectAppNotFixed.class) {
			AdminAreaProjectAppNotFixed adminProAppChildNotFixed = new AdminAreaProjectAppNotFixed(parent);
			RelationObj relObj = new RelationObj(relationId, this, adminProAppChildNotFixed, relationStatus, null);
			return relObj;
		} else if (adapterType == AdminAreaProjectAppFixed.class) {
			AdminAreaProjectAppFixed adminProAppChildFix = new AdminAreaProjectAppFixed(parent);
			RelationObj relObj = new RelationObj(relationId, this, adminProAppChildFix, relationStatus, null);
			return relObj;
		} else if (adapterType == AdminAreaProjectAppProtected.class) {
			AdminAreaProjectAppProtected adminProAppChildProtected = new AdminAreaProjectAppProtected(parent);
			RelationObj relObj = new RelationObj(relationId, this, adminProAppChildProtected, relationStatus, null);
			return relObj;
		} else if (adapterType == ProjectAdminAreaProjectApplications.class) {
			ProjectAdminAreaProjectApplications projectAdminAreaProjectApplications = new ProjectAdminAreaProjectApplications(
					parent);
			RelationObj relObj = new RelationObj(relationId, this, projectAdminAreaProjectApplications, relationStatus,
					null);
			return relObj;
		} else if (adapterType == ProjectAdminAreaProjectAppNotFixed.class) {
			ProjectAdminAreaProjectAppNotFixed projectAdminProAppChildNotFixed = new ProjectAdminAreaProjectAppNotFixed(
					parent);
			RelationObj relObj = new RelationObj(relationId, this, projectAdminProAppChildNotFixed, relationStatus,
					null);
			return relObj;
		} else if (adapterType == ProjectAdminAreaProjectAppFixed.class) {
			ProjectAdminAreaProjectAppFixed projectAdminProAppChildFix = new ProjectAdminAreaProjectAppFixed(parent);
			RelationObj relObj = new RelationObj(relationId, this, projectAdminProAppChildFix, relationStatus, null);
			return relObj;
		} else if (adapterType == ProjectAdminAreaProjectAppProtected.class) {
			ProjectAdminAreaProjectAppProtected projectAdminProAppChildProtected = new ProjectAdminAreaProjectAppProtected(
					parent);
			RelationObj relObj = new RelationObj(relationId, this, projectAdminProAppChildProtected, relationStatus,
					null);
			return relObj;
		} else if (adapterType == ProjectUserAAProjectApplications.class) {
			ProjectUserAAProjectApplications projectUserAAProjectApplications = new ProjectUserAAProjectApplications(
					parent);
			RelationObj relObj = new RelationObj(relationId, this, projectUserAAProjectApplications, relationStatus,
					null);
			return relObj;
		} else if (adapterType == ProjectUserAAProjectAppAllowed.class) {
			ProjectUserAAProjectAppAllowed projectUserAAProjectAppAllowed = new ProjectUserAAProjectAppAllowed(parent);
			RelationObj relObj = new RelationObj(relationId, this, projectUserAAProjectAppAllowed, relationStatus,
					null);
			return relObj;
		} else if (adapterType == ProjectUserAAProjectAppForbidden.class) {
			ProjectUserAAProjectAppForbidden projectUserAAProjectAppForbidden = new ProjectUserAAProjectAppForbidden(
					parent);
			RelationObj relObj = new RelationObj(relationId, this, projectUserAAProjectAppForbidden, relationStatus,
					null);
			return relObj;
		} else if (adapterType == ProjectProjectApplications.class) {
			ProjectProjectApplications projectProjectApplications = new ProjectProjectApplications(parent);
			RelationObj relObj = new RelationObj(relationId, this, projectProjectApplications, relationStatus, null);
			return relObj;
		} else if (adapterType == BaseAppProjectApplications.class) {
			BaseAppProjectApplications baseAppProjectApplications = new BaseAppProjectApplications(parent);
			RelationObj relObj = new RelationObj(relationId, this, baseAppProjectApplications, relationStatus, null);
			return relObj;
		} else if (adapterType == UserProjectAAProjectApplications.class) {
			UserProjectAAProjectApplications userProjectAAProjectApplications = new UserProjectAAProjectApplications(
					parent);
			RelationObj relObj = new RelationObj(relationId, this, userProjectAAProjectApplications, relationStatus,
					null);
			return relObj;
		} else if (adapterType == UserProjectAAProjectAppAllowed.class) {
			UserProjectAAProjectAppAllowed userProjectAAProjectAppAllowed = new UserProjectAAProjectAppAllowed(parent);
			RelationObj relObj = new RelationObj(relationId, this, userProjectAAProjectAppAllowed, relationStatus,
					null);
			return relObj;
		} else if (adapterType == UserProjectAAProjectAppForbidden.class) {
			UserProjectAAProjectAppForbidden userProjectAAProjectAppForbidden = new UserProjectAAProjectAppForbidden(
					parent);
			RelationObj relObj = new RelationObj(relationId, this, userProjectAAProjectAppForbidden, relationStatus,
					null);
			return relObj;
		} else if (adapterType == DirectoryProjectApplications.class) {
			DirectoryProjectApplications directoryProjectApplications = new DirectoryProjectApplications(parent);
			RelationObj relObj = new RelationObj(relationId, this, directoryProjectApplications, relationStatus, null);
			return relObj;
		} else if (adapterType == ProjectAppGroupProjectApps.class) {
			ProjectAppGroupProjectApps projectAppGroupProjectApps = new ProjectAppGroupProjectApps(parent);
			RelationObj relObj = new RelationObj(relationId, this, projectAppGroupProjectApps, relationStatus, null);
			return relObj;
		}
		
		return super.getAdapter(adapterType, parent, relationId, relationStatus);
	}

}
