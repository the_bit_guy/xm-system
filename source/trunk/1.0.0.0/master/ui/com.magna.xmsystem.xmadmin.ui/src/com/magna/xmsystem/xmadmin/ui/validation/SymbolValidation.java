package com.magna.xmsystem.xmadmin.ui.validation;

import org.eclipse.core.databinding.validation.IValidator;
import org.eclipse.core.databinding.validation.ValidationStatus;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;

/**
 * Class for string longer than 2 char Validation
 * 
 * @author Roshan
 *
 */
public class SymbolValidation implements IValidator {

	/**
	 * Method to validate
	 */
	@Override
	public IStatus validate(Object value) {
		if (value instanceof String) {
			String s = (String) value;

			if (s.length() > 0) {
				return Status.OK_STATUS;
			} else {
				return ValidationStatus.error("Please insert symbol");

			}
		} else {
			throw new RuntimeException("Not supposed to be called for non-strings.");
		}
	}
}
