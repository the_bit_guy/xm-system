package com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.adminarea;

import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild;

public class AdminAreaUserAppFixed implements IAdminTreeChild {

	/** The parent. */
	private IAdminTreeChild parent;

	/**
	 * Instantiates a new site admin user app child fix.
	 *
	 * @param parent the parent
	 */
	public AdminAreaUserAppFixed(final IAdminTreeChild parent) {
		this.parent = parent;
	}

	/*
	 * @see
	 * com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild#
	 * getParent()
	 */
	@Override
	public IAdminTreeChild getParent() {
		return this.parent;
	}

	/*
	 * com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild#
	 * setParent(com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.
	 * IAdminTreeChild)
	 */
	@Override
	public void setParent(final IAdminTreeChild parent) {
		this.parent = parent;
	}

}
