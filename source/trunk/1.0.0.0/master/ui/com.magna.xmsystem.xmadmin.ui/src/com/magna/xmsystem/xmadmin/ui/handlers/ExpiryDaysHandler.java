package com.magna.xmsystem.xmadmin.ui.handlers;

import javax.inject.Inject;

import org.eclipse.e4.core.contexts.ContextInjectionFactory;
import org.eclipse.e4.core.contexts.IEclipseContext;
import org.eclipse.e4.core.di.annotations.Execute;

import com.magna.xmsystem.xmadmin.ui.parts.objexpcontextmenu.ExpiryDaysAction;

// TODO: Auto-generated Javadoc
/**
 * The Class ExpiryDaysHandler.
 */
public class ExpiryDaysHandler {

	/** The eclipse context. */
	@Inject
	private IEclipseContext eclipseContext;

	/**
	 * Execute.
	 */
	@Execute
	public void execute() {

		ExpiryDaysAction expiryDaysAction = ContextInjectionFactory.make(ExpiryDaysAction.class, eclipseContext);
		expiryDaysAction.run();
	}
}
