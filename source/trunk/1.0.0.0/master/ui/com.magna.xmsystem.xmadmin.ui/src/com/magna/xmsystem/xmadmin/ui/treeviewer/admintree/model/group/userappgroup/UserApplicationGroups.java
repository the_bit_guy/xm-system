/*
 * 
 */
package com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.group.userappgroup;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.stream.Collectors;

import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild;

/**
 * The Class UserApplicationGroups.
 * 
 * @author archita.patel
 */
public class UserApplicationGroups implements IAdminTreeChild {
	/** The parent. */
	private IAdminTreeChild parent;
	
	/** The user app groups children. */
	private Map<String, IAdminTreeChild> userAppGroupsChildren;

	/**
	 * Constructor.
	 */
	public UserApplicationGroups() {
		this.parent = null;
		this.userAppGroupsChildren = new LinkedHashMap<>();
	}

	/**
	 * Adds the child.
	 *
	 * @param userAppGroupId {@link String}
	 * @param child {@link IAdminTreeChild}
	 * @return {@link IAdminTreeChild}
	 */
	public IAdminTreeChild add(final String userAppGroupId, final IAdminTreeChild child) {
		child.setParent(this);
		IAdminTreeChild returnVal = this.userAppGroupsChildren.put(userAppGroupId, child);
		sort();
		return returnVal;
	}

	/**
	 * Removes the child.
	 *
	 * @param userAppGroupId the group name
	 * @return {@link IAdminTreeChild}
	 */
	public IAdminTreeChild remove(final String userAppGroupId) {
		return this.userAppGroupsChildren.remove(userAppGroupId);
	}

	
	/**
	 * Gets the user app groups collection.
	 *
	 * @return the user app groups collection
	 */
	public Collection<IAdminTreeChild> getUserAppGroupsCollection() {
		return this.userAppGroupsChildren.values();
	}

	
	/**
	 * Gets the user app groups children.
	 *
	 * @return the user app groups children
	 */
	public Map<String, IAdminTreeChild> getUserAppGroupsChildren() {
		return userAppGroupsChildren;
	}
	
	/**
	 * Returns null as its parent.
	 *
	 * @return the parent
	 * @see com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild#getParent()
	 */
	@Override
	public IAdminTreeChild getParent() {
		return this.parent;
	}

	/**
	 * Sets the parent.
	 *
	 * @param parent the new parent
	 * @see com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild#setParent(com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild)
	 */
	@Override
	public void setParent(final IAdminTreeChild parent) {
		this.parent = parent;
	}
	
	/**
	 * Sort.
	 */
	public void sort() {
		final LinkedHashMap<String, IAdminTreeChild> collect = this.userAppGroupsChildren.entrySet().stream().sorted(
				(e1, e2) -> ((UserApplicationGroup) e1.getValue()).getName().compareTo(((UserApplicationGroup) e2.getValue()).getName()))
				.collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e1, LinkedHashMap::new));
		this.userAppGroupsChildren = collect;
	}
}

