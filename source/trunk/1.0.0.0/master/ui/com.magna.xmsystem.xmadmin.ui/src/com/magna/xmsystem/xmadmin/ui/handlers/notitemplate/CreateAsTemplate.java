
package com.magna.xmsystem.xmadmin.ui.handlers.notitemplate;

import javax.inject.Inject;

import org.eclipse.e4.core.di.annotations.CanExecute;
import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.core.services.nls.Translation;
import org.eclipse.e4.ui.model.application.MApplication;
import org.eclipse.e4.ui.model.application.ui.basic.MPart;
import org.eclipse.e4.ui.workbench.modeling.EModelService;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.swt.widgets.Display;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.client.ResourceAccessException;

import com.magna.xmsystem.dependencies.customExceptions.UnauthorizedAccessException;
import com.magna.xmsystem.ui.controls.widgets.CustomMessageDialog;
import com.magna.xmsystem.xmadmin.message.Message;
import com.magna.xmsystem.xmadmin.ui.parts.IEditablePart;
import com.magna.xmsystem.xmadmin.ui.parts.InformationPart;
import com.magna.xmsystem.xmadmin.ui.parts.notificationtemplate.TemplateCompositeAction;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.AdminTreeviewer;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.notification.NotificationTemplate;
import com.magna.xmsystem.xmadmin.util.CommonConstants;
import com.magna.xmsystem.xmadmin.util.XMAdminUtil;

// TODO: Auto-generated Javadoc
/**
 * The Class CreateAsTemplate.
 */
public class CreateAsTemplate {

	/** The Constant LOGGER. */
	private static final Logger LOGGER = LoggerFactory.getLogger(CreateAsTemplate.class);

	/** The model service. */
	@Inject
	private EModelService modelService;

	/** The application. */
	@Inject
	private MApplication application;

	/** The messages. */
	@Inject
	@Translation
	transient private Message messages;

	/**
	 * Execute.
	 */
	@Execute
	public void execute() {
		try {
			// if
			// (XMAdminUtil.getInstance().isAcessAllowed("NOTIFICATION-CREATE"))
			// {
			final AdminTreeviewer adminTree = XMAdminUtil.getInstance().getAdminTree();
			IStructuredSelection selection;
			Object selectionObj;
			if (adminTree != null && (selection = adminTree.getStructuredSelection()) != null
					&& (selectionObj = selection.getFirstElement()) != null
					&& selectionObj instanceof NotificationTemplate) {

				MPart part = (MPart) this.modelService.find(CommonConstants.PART_ID.INFORMATION_PART_ID, application);
				InformationPart rightSidePart = (InformationPart) part.getObject();
				TemplateCompositeAction templateCompositeUI = rightSidePart.getTemplateCompositeUI();
				NotificationTemplate template = ((NotificationTemplate) selectionObj).deepCopyTemplate(false, null);
				template.setTemplateId(null);
				template.setOperationMode(CommonConstants.OPERATIONMODE.CREATE);
				templateCompositeUI.setModel(template);
				adminTree.setSelection(null);
			}
			/*
			 * }else{
			 * CustomMessageDialog.openError(Display.getCurrent().getActiveShell
			 * (), messages.objectPermissionDialogTitle,
			 * messages.objectPermissionDialogMsg); }
			 */
		} catch (UnauthorizedAccessException e) {
			LOGGER.error("Current user is Unauthorized " + e);
		} catch (Exception e) {
			if (e instanceof ResourceAccessException) {
				CustomMessageDialog.openError(Display.getCurrent().getActiveShell(), messages.errorDialogTitile,
						messages.serverNotReachable);
			}
			LOGGER.error("Exception occured in calling access allowed API " + e);
		}
	}

	/**
	 * Can execute.
	 *
	 * @return true, if successful
	 */
	@CanExecute
	public boolean canExecute() {
		boolean returnVal = true;
		final XMAdminUtil instance = XMAdminUtil.getInstance();
		MPart rightPart;
		Object rightView;
		if ((rightPart = instance.getInformationPart()) != null && (rightView = rightPart.getObject()) != null
				&& rightView instanceof IEditablePart && ((IEditablePart) rightView).isDirty()) {
			returnVal = false;
		}
		return returnVal;
	}
}