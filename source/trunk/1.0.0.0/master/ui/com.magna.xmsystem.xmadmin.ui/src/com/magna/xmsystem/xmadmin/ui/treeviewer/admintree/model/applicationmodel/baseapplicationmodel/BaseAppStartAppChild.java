package com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.baseapplicationmodel;

import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild;

/**
 * The Class BaseAppStartAppChild.
 */
public class BaseAppStartAppChild implements IAdminTreeChild{
	
	/** The parent. */
	private IAdminTreeChild parent;

	/**
	 * Instantiates a new base app start app child.
	 *
	 * @param parent the parent
	 */
	public BaseAppStartAppChild(IAdminTreeChild parent) {
		this.parent = parent;
	}

	/* (non-Javadoc)
	 * @see com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild#getParent()
	 */
	@Override
	public IAdminTreeChild getParent() {
		return this.parent;
	}

	/* (non-Javadoc)
	 * @see com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild#setParent(com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild)
	 */
	@Override
	public void setParent(IAdminTreeChild parent) {
		this.parent = parent;
		
	}

}
