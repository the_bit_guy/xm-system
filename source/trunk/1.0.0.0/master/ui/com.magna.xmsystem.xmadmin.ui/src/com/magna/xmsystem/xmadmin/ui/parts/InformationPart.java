package com.magna.xmsystem.xmadmin.ui.parts;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.eclipse.e4.core.contexts.ContextInjectionFactory;
import org.eclipse.e4.core.contexts.IEclipseContext;
import org.eclipse.e4.core.services.nls.Translation;
import org.eclipse.e4.ui.di.Persist;
import org.eclipse.e4.ui.model.application.MApplication;
import org.eclipse.e4.ui.model.application.ui.MDirtyable;
import org.eclipse.e4.ui.model.application.ui.basic.MPart;
import org.eclipse.e4.ui.model.application.ui.basic.MTrimmedWindow;
import org.eclipse.e4.ui.model.application.ui.basic.MWindow;
import org.eclipse.e4.ui.model.application.ui.menu.MMenu;
import org.eclipse.e4.ui.model.application.ui.menu.MMenuElement;
import org.eclipse.e4.ui.workbench.modeling.EModelService;
import org.eclipse.e4.ui.workbench.modeling.EPartService;
import org.eclipse.e4.ui.workbench.modeling.IWindowCloseHandler;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.StackLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.magna.xmsystem.ui.controls.widgets.CustomMessageDialog;
import com.magna.xmsystem.xmadmin.message.Message;
import com.magna.xmsystem.xmadmin.restclient.users.UserController;
import com.magna.xmsystem.xmadmin.ui.parts.adminarea.AdministrationAreaCompositeAction;
import com.magna.xmsystem.xmadmin.ui.parts.application.baseapplication.BaseAppCompositeAction;
import com.magna.xmsystem.xmadmin.ui.parts.application.projectapplication.ProjectApplicationCompositeAction;
import com.magna.xmsystem.xmadmin.ui.parts.application.startapplication.StartApplicationCompositeAction;
import com.magna.xmsystem.xmadmin.ui.parts.application.userapplication.UserApplicationCompositeAction;
import com.magna.xmsystem.xmadmin.ui.parts.directory.DirectoryCompositeAction;
import com.magna.xmsystem.xmadmin.ui.parts.groups.projectappgroup.ProjectAppGroupCompositeAction;
import com.magna.xmsystem.xmadmin.ui.parts.groups.projectgroup.ProjectGroupCompositeAction;
import com.magna.xmsystem.xmadmin.ui.parts.groups.userappgroup.UserAppGroupCompositeAction;
import com.magna.xmsystem.xmadmin.ui.parts.groups.usergroup.UserGroupCompositeAction;
import com.magna.xmsystem.xmadmin.ui.parts.icons.IconsCompositeAction;
import com.magna.xmsystem.xmadmin.ui.parts.ldapconfig.LdapConfigCompositeAction;
import com.magna.xmsystem.xmadmin.ui.parts.livemsgconfig.LiveMessageConfigAction;
import com.magna.xmsystem.xmadmin.ui.parts.notification.NotificationCompositeAction;
import com.magna.xmsystem.xmadmin.ui.parts.notificationtemplate.TemplateCompositeAction;
import com.magna.xmsystem.xmadmin.ui.parts.notifyaaprojectuser.NotifyAAProjectUserCompositeAction;
import com.magna.xmsystem.xmadmin.ui.parts.objexp.ObjectExpPage;
import com.magna.xmsystem.xmadmin.ui.parts.proaarelassignevt.ProjectAARelAssignEvtCompositeAction;
import com.magna.xmsystem.xmadmin.ui.parts.project.ProjectCompositeAction;
import com.magna.xmsystem.xmadmin.ui.parts.projectactivateevt.ProjectActivateEvtCompositeAction;
import com.magna.xmsystem.xmadmin.ui.parts.projectcreateevt.ProjectCreateEvtCompositeAction;
import com.magna.xmsystem.xmadmin.ui.parts.projectdeactivateevt.ProjectDeactivateEvtCompositeAction;
import com.magna.xmsystem.xmadmin.ui.parts.projectdeleteevt.ProjectDeleteEvtCompositeAction;
import com.magna.xmsystem.xmadmin.ui.parts.projectexpiryconfig.ProjectExpiryConfigCompositeAction;
import com.magna.xmsystem.xmadmin.ui.parts.role.RoleCompositeAction;
import com.magna.xmsystem.xmadmin.ui.parts.singletonapptimeconfig.SingletonAppTimeConfigComopsiteAction;
import com.magna.xmsystem.xmadmin.ui.parts.site.SiteCompositeAction;
import com.magna.xmsystem.xmadmin.ui.parts.smtpconfig.SmtpConfigCompositeAction;
import com.magna.xmsystem.xmadmin.ui.parts.unassignedusermessage.NoProjectPopmessageCompositeAction;
import com.magna.xmsystem.xmadmin.ui.parts.user.UserCompositeAction;
import com.magna.xmsystem.xmadmin.ui.parts.userproexpgrace.UserProjectExpGraceCompositeAction;
import com.magna.xmsystem.xmadmin.ui.parts.userprojectrelexpiry.UserProjectExpCompositeAction;
import com.magna.xmsystem.xmadmin.ui.parts.userprorelassignevt.UserProRelAssignEvtCompositeAction;
import com.magna.xmsystem.xmadmin.ui.parts.userprorelremoveevt.UserProRelRemoveEvtCompositeAction;
import com.magna.xmsystem.xmadmin.ui.welcomepage.InfoDisplayPage;
import com.magna.xmsystem.xmadmin.util.CommonConstants;

/**
 * Class for Information part.
 *
 * @author archita.patel
 */
public class InformationPart implements IEditablePart {

	/** The Constant LOGGER. */
	private static final Logger LOGGER = LoggerFactory.getLogger(InformationPart.class);

	/** Member variable 'messages' for {@link Message}. */
	@Inject
	@Translation
	private Message messages;

	/** Member variable 'composite' for {@link Composite}. */
	private Composite composite;

	/** Member variable 'comp layout' for {@link StackLayout}. */
	private StackLayout compLayout;

	/** Member variable 'info display page' for {@link InfoDisplayPage}. */
	private InfoDisplayPage infoDisplayPage;

	/** Member variable 'site composite UI' for {@link SiteCompositeAction}. */
	private SiteCompositeAction siteCompositeUI;

	/** Member variable 'admin area compisite UI' for {@link AdministrationAreaCompositeAction}. */
	private AdministrationAreaCompositeAction adminAreaCompisiteUI;

	/** Member variable 'project compsite UI' for {@link ProjectCompositeAction}. */
	private ProjectCompositeAction projectCompsiteUI;

	/** Member variable 'group composite UI' for {@link UserGroupCompositeAction}. */
	//private GroupCompositeAction groupCompositeUI;
	
	/** The group composite UI. */
	private UserGroupCompositeAction groupCompositeUI;
	
	/** Member variable 'project group composite UI' for {@link ProjectGroupCompositeAction}. */
	private ProjectGroupCompositeAction projectGroupCompositeUI;
	
	/** Member variable 'user app group composite UI' for {@link UserAppGroupCompositeAction}. */
	private UserAppGroupCompositeAction userAppGroupCompositeUI;
	
	/** Member variable 'project app group composite UI' for {@link ProjectAppGroupCompositeAction}. */
	private ProjectAppGroupCompositeAction projectAppGroupCompositeUI;
	
	/** Member variable 'directory composite UI' for {@link DirectoryCompositeAction}. */
	private DirectoryCompositeAction directoryCompositeUI;

	/** Member variable 'icon compsite UI' for {@link IconsCompositeAction}. */
	private IconsCompositeAction iconCompsiteUI;

	/** Member variable 'user composite UI' for {@link UserCompositeAction}. */
	private UserCompositeAction userCompositeUI;

	/** Member variable 'role composite UI' for {@link RoleCompositeAction}. */
	private RoleCompositeAction roleCompositeUI;
	
	/** Member variable 'smtp config composite UI' for {@link SmtpConfigCompositeAction}. */
	private SmtpConfigCompositeAction smtpConfigCompositeUI;
	
	/** Member variable 'ldap config composite UI' for {@link LdapConfigCompositeAction}. */
	private LdapConfigCompositeAction ldapConfigCompositeUI;
	
	/** Member variable 'singleton app time config comopsite UI' for {@link SingletonAppTimeConfigComopsiteAction}. */
	private SingletonAppTimeConfigComopsiteAction singletonAppTimeConfigComopsiteUI;
	
	/** Member variable 'No project pop message composite UI' for {@link NoProjectPopmessageCompositeAction}. */
	private NoProjectPopmessageCompositeAction noProjectPopMessageCompositeUI;
	
	/** Member variable 'project expiry config composite UI' for {@link ProjectExpiryConfigCompositeAction}. */
	private ProjectExpiryConfigCompositeAction projectExpiryConfigCompositeUI;
	
	/** Member variable 'live message config UI' for {@link LiveMessageConfigAction}. */
	private LiveMessageConfigAction liveMessageConfigUI;
	
	/** Member variable 'notification compo UI' for {@link NotificationCompositeAction}. */
	private NotificationCompositeAction notificationCompoUI;

	/** Member variable 'base app composite UI' for {@link BaseAppCompositeAction}. */
	private BaseAppCompositeAction baseAppCompositeUI;

	/** Member variable 'user app composite UI' for {@link UserApplicationCompositeAction}. */
	private UserApplicationCompositeAction userAppCompositeUI;
	
	/** Member variable 'project app composite UI' for {@link ProjectApplicationCompositeAction}. */
	private ProjectApplicationCompositeAction projectAppCompositeUI;

	/** Member variable 'start application composite UI' for {@link StartApplicationCompositeAction}. */
	private StartApplicationCompositeAction startApplicationCompositeUI;
	
	/** The Project create evt composite UI. */
	private ProjectCreateEvtCompositeAction projectCreateEvtCompositeUI;
	
	/** The project delete composite UI. */
	private ProjectDeleteEvtCompositeAction projectDeleteCompositeUI;
	
	/** The project deactivate composite UI. */
	private ProjectDeactivateEvtCompositeAction projectDeactivateCompositeUI;
	
	/** The project activate composite UI. */
	private ProjectActivateEvtCompositeAction projectActivateCompositeUI;
	
	/** The project AA rel assign evt composite UI. */
	private ProjectAARelAssignEvtCompositeAction projectAARelAssignEvtCompositeUI;
	
	/** The notify AA project user composite UI. */
	private NotifyAAProjectUserCompositeAction notifyAAProjectUserCompositeUI;
	
	/** The user project exp grace composite UI. */
	private UserProjectExpGraceCompositeAction userProjectExpGraceCompositeUI;
	
	/** The user project rel exp composite UI. */
	private UserProjectExpCompositeAction userProjectRelExpCompositeUI;
	
	/** The user pro rel assign composite UI. */
	private UserProRelAssignEvtCompositeAction userProRelAssignCompositeUI;
	
	/** The user pro rel remove composite UI. */
	private UserProRelRemoveEvtCompositeAction userProRelRemoveCompositeUI;
	
	/** The template composite UI. */
	private TemplateCompositeAction templateCompositeUI;

	/** The object exp part UI. */
	private ObjectExpPage objectExpPartUI;

	/** Member variable 'eclipse context' for {@link IEclipseContext}. */
	@Inject
	private IEclipseContext eclipseContext;

	/** Member variable 'dirty' for {@link MDirtyable}. */
	@Inject
	private MDirtyable dirty;

	/** Member variable 'window' for {@link MWindow}. */
	@Inject
	private MWindow window;
	
	/** Member variable 'part service' for {@link EPartService}. */
	@Inject
	private EPartService partService;
	
	/** Member variable 'application' for {@link MApplication}. */
	@Inject
	private MApplication application;

	/** Member variable 'model service' for {@link EModelService}. */
	@Inject
	private EModelService modelService;

	/** Member variable 'm trimmed window' for {@link MTrimmedWindow}. */
	@Inject
	private MTrimmedWindow mTrimmedWindow;

	/**
	 * Method for Creates the UI.
	 *
	 * @param parentP {@link Composite}
	 */
	@PostConstruct
	public void createUI(final Composite parentP) {
		try {
			parentP.setLayout(new GridLayout(1, false));
			parentP.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
			buildComponents(parentP);
			setMDirtyable();
		} catch (Exception e) {
			LOGGER.error("Exception occured at creating UI compoents!", e); //$NON-NLS-1$
		}
	}

	/**
	 * Method for Sets the M dirtyable.
	 */
	private void setMDirtyable() {

		IWindowCloseHandler handler = new IWindowCloseHandler() {
			@Override
			public boolean close(MWindow window) {
				if (!partService.getDirtyParts().isEmpty()) {
					boolean response = CustomMessageDialog.openSaveDiscardDialog(Display.getDefault().getActiveShell(),
							messages.dirtyDialogTitle, messages.dirtyDialogMessage);
					if (response) {
						save();
						return response;
					}
				}
				return true;
			}
		};
		window.getContext().set(IWindowCloseHandler.class, handler);
	}

	/**
	 * Method for Builds the components.
	 *
	 * @param parentP {@link Composite}
	 */
	final private void buildComponents(final Composite parentP) {

		this.composite = new Composite(parentP, SWT.NONE);
		this.compLayout = new StackLayout();
		this.composite.setLayout(compLayout);
		this.composite.setLayoutData(new GridData(GridData.FILL_BOTH));

		

		eclipseContext.set(Composite.class, composite);
		
		this.infoDisplayPage = ContextInjectionFactory.make(InfoDisplayPage.class, eclipseContext);
		
		this.siteCompositeUI = ContextInjectionFactory.make(SiteCompositeAction.class, eclipseContext);
		this.siteCompositeUI.setDirtyObject(this.dirty);

		this.adminAreaCompisiteUI = ContextInjectionFactory.make(AdministrationAreaCompositeAction.class,
				eclipseContext);
		this.adminAreaCompisiteUI.setDirtyObject(this.dirty);

		this.projectCompsiteUI = ContextInjectionFactory.make(ProjectCompositeAction.class, eclipseContext);
		this.projectCompsiteUI.setDirtyObject(this.dirty);

		this.userCompositeUI = ContextInjectionFactory.make(UserCompositeAction.class, eclipseContext);
		this.userCompositeUI.setDirtyObject(this.dirty);

		this.groupCompositeUI = ContextInjectionFactory.make(UserGroupCompositeAction.class, eclipseContext);
		this.groupCompositeUI.setDirtyObject(this.dirty);
		
		this.projectGroupCompositeUI = ContextInjectionFactory.make(ProjectGroupCompositeAction.class, eclipseContext);
		this.projectGroupCompositeUI.setDirtyObject(this.dirty);
		
		this.userAppGroupCompositeUI = ContextInjectionFactory.make(UserAppGroupCompositeAction.class, eclipseContext);
		this.userAppGroupCompositeUI.setDirtyObject(this.dirty);
		
		this.projectAppGroupCompositeUI = ContextInjectionFactory.make(ProjectAppGroupCompositeAction.class, eclipseContext);
		this.projectAppGroupCompositeUI.setDirtyObject(this.dirty);

		this.directoryCompositeUI = ContextInjectionFactory.make(DirectoryCompositeAction.class, eclipseContext);
		this.directoryCompositeUI.setDirtyObject(dirty);
		
		this.roleCompositeUI = ContextInjectionFactory.make(RoleCompositeAction.class, eclipseContext);
		this.roleCompositeUI.setDirtyObject(this.dirty);

		this.iconCompsiteUI = ContextInjectionFactory.make(IconsCompositeAction.class, eclipseContext);

		this.smtpConfigCompositeUI=ContextInjectionFactory.make(SmtpConfigCompositeAction.class, eclipseContext);
		this.smtpConfigCompositeUI.setDirtyObject(this.dirty);
		
		this.ldapConfigCompositeUI=ContextInjectionFactory.make(LdapConfigCompositeAction.class, eclipseContext);
		this.ldapConfigCompositeUI.setDirtyObject(this.dirty);
		
		this.singletonAppTimeConfigComopsiteUI=ContextInjectionFactory.make(SingletonAppTimeConfigComopsiteAction.class, eclipseContext);
		this.singletonAppTimeConfigComopsiteUI.setDirtyObject(this.dirty);
		
		this.noProjectPopMessageCompositeUI=ContextInjectionFactory.make(NoProjectPopmessageCompositeAction.class, eclipseContext);
		this.noProjectPopMessageCompositeUI.setDirty(this.dirty);
		
		this.projectExpiryConfigCompositeUI=ContextInjectionFactory.make(ProjectExpiryConfigCompositeAction.class, eclipseContext);
		this.projectExpiryConfigCompositeUI.setDirtyObject(this.dirty);
		
		this.liveMessageConfigUI=ContextInjectionFactory.make(LiveMessageConfigAction.class, eclipseContext);
		this.liveMessageConfigUI.setDirtyObject(this.dirty);
		
		this.notificationCompoUI=ContextInjectionFactory.make(NotificationCompositeAction.class, eclipseContext);
		this.notificationCompoUI.setDirtyObject(this.dirty);
		
		this.baseAppCompositeUI = ContextInjectionFactory.make(BaseAppCompositeAction.class, eclipseContext);
		this.baseAppCompositeUI.setDirtyObject(this.dirty);

		this.startApplicationCompositeUI = ContextInjectionFactory.make(StartApplicationCompositeAction.class,
				eclipseContext);
		this.startApplicationCompositeUI.setDirtyObject(this.dirty);

		this.projectAppCompositeUI = ContextInjectionFactory.make(ProjectApplicationCompositeAction.class,
				eclipseContext);
		this.projectAppCompositeUI.setDirtyObject(this.dirty);

		this.userAppCompositeUI = ContextInjectionFactory.make(UserApplicationCompositeAction.class, eclipseContext);
		this.userAppCompositeUI.setDirtyObject(this.dirty);
		

		this.projectCreateEvtCompositeUI = ContextInjectionFactory.make(ProjectCreateEvtCompositeAction.class, eclipseContext);
		this.projectCreateEvtCompositeUI.setDirtyObject(this.dirty);
		
		this.projectDeleteCompositeUI = ContextInjectionFactory.make(ProjectDeleteEvtCompositeAction.class, eclipseContext);
		this.projectDeleteCompositeUI.setDirtyObject(this.dirty);
		
		this.projectDeactivateCompositeUI = ContextInjectionFactory.make(ProjectDeactivateEvtCompositeAction.class, eclipseContext);
		this.projectDeactivateCompositeUI.setDirtyObject(this.dirty);
		
		this.projectActivateCompositeUI = ContextInjectionFactory.make(ProjectActivateEvtCompositeAction.class, eclipseContext);
		this.projectActivateCompositeUI.setDirtyObject(this.dirty);
		
		this.projectAARelAssignEvtCompositeUI = ContextInjectionFactory.make(ProjectAARelAssignEvtCompositeAction.class, eclipseContext);
		//this.projectAARelAssignEvtCompositeUI.setDirtyObject(this.dirty);
		
		this.notifyAAProjectUserCompositeUI = ContextInjectionFactory.make(NotifyAAProjectUserCompositeAction.class, eclipseContext);
		//this.notifyAAProjectUserCompositeUI.setDirtyObject(this.dirty);
		
		this.userProjectExpGraceCompositeUI = ContextInjectionFactory.make(UserProjectExpGraceCompositeAction.class, eclipseContext);
		//this.notifyAAProjectUserCompositeUI.setDirtyObject(this.dirty);
		
		this.userProjectRelExpCompositeUI = ContextInjectionFactory.make(UserProjectExpCompositeAction.class, eclipseContext);
		this.userProjectRelExpCompositeUI.setDirtyObject(this.dirty);
		
		this.userProRelAssignCompositeUI = ContextInjectionFactory.make(UserProRelAssignEvtCompositeAction.class, eclipseContext);
		this.userProRelAssignCompositeUI.setDirtyObject(this.dirty);
		
		this.userProRelRemoveCompositeUI = ContextInjectionFactory.make(UserProRelRemoveEvtCompositeAction.class, eclipseContext);
		this.userProRelRemoveCompositeUI.setDirtyObject(this.dirty);
		
		this.templateCompositeUI = ContextInjectionFactory.make(TemplateCompositeAction.class, eclipseContext);
		this.templateCompositeUI.setDirtyObject(this.dirty);
		
		this.objectExpPartUI = ContextInjectionFactory.make(ObjectExpPage.class, eclipseContext);
		
		this.compLayout.topControl = this.infoDisplayPage;
	}

	/**
	 * Gets the composite.
	 *
	 * @return the composite
	 */
	public Composite getComposite() {
		return composite;
	}

	/**
	 * Gets the comp layout.
	 *
	 * @return the comp layout
	 */
	public StackLayout getCompLayout() {
		return compLayout;
	}

	/**
	 * Gets the site composite UI.
	 *
	 * @return the site composite UI
	 */
	public SiteCompositeAction getSiteCompositeUI() {
		return siteCompositeUI;
	}

	/**
	 * Gets the info display page.
	 *
	 * @return the info display page
	 */
	public InfoDisplayPage getInfoDisplayPage() {
		return infoDisplayPage;
	}

	/**
	 * Gets the admin area compisite UI.
	 *
	 * @return the admin area compisite UI
	 */
	public AdministrationAreaCompositeAction getAdminAreaCompisiteUI() {
		return adminAreaCompisiteUI;
	}

	/**
	 * Gets the project compsite UI.
	 *
	 * @return the project compsite UI
	 */
	public ProjectCompositeAction getProjectCompsiteUI() {
		return projectCompsiteUI;
	}

	/**
	 * Gets the group composite UI.
	 *
	 * @return the group composite UI
	 */
	public UserGroupCompositeAction getGroupCompositeUI() {
		return groupCompositeUI;
	}

	/**
	 * Gets the project group composite UI.
	 *
	 * @return the project group composite UI
	 */
	public ProjectGroupCompositeAction getProjectGroupCompositeUI() {
		return projectGroupCompositeUI;
	}

	/**
	 * Gets the user app group composite UI.
	 *
	 * @return the user app group composite UI
	 */
	public UserAppGroupCompositeAction getUserAppGroupCompositeUI() {
		return userAppGroupCompositeUI;
	}

	/**
	 * Gets the project app group composite UI.
	 *
	 * @return the project app group composite UI
	 */
	public ProjectAppGroupCompositeAction getProjectAppGroupCompositeUI() {
		return projectAppGroupCompositeUI;
	}

	/**
	 * Gets the directory composite UI.
	 *
	 * @return the directory composite UI
	 */
	public DirectoryCompositeAction getDirectoryCompositeUI() {
		return directoryCompositeUI;
	}

	/**
	 * Gets the icon compsite UI.
	 *
	 * @return the icon compsite UI
	 */
	public IconsCompositeAction getIconCompsiteUI() {
		return iconCompsiteUI;
	}

	/**
	 * Gets the user composite UI.
	 *
	 * @return the user composite UI
	 */
	public UserCompositeAction getUserCompositeUI() {
		return userCompositeUI;
	}


	/**
	 * Gets the role composite UI.
	 *
	 * @return the role composite UI
	 */
	public RoleCompositeAction getRoleCompositeUI() {
		return roleCompositeUI;
	}

	/**
	 * Gets the smtp config composite UI.
	 *
	 * @return the smtp config composite UI
	 */
	public SmtpConfigCompositeAction getSmtpConfigCompositeUI() {
		return smtpConfigCompositeUI;
	}
	
	/**
	 * Gets the ldap config composite UI.
	 *
	 * @return the ldap config composite UI
	 */
	public LdapConfigCompositeAction getLdapConfigCompositeUI() {
		return ldapConfigCompositeUI;
	}

	/**
	 * Gets the singleton app time config comopsite UI.
	 *
	 * @return the singleton app time config comopsite UI
	 */
	public SingletonAppTimeConfigComopsiteAction getSingletonAppTimeConfigComopsiteUI() {
		return singletonAppTimeConfigComopsiteUI;
	}

	/**
	 * Gets the no project pop message composite UI.
	 *
	 * @return the no project pop message composite UI
	 */
	public NoProjectPopmessageCompositeAction getNoProjectPopMessageCompositeUI() {
		return noProjectPopMessageCompositeUI;
	}

	/**
	 * Gets the project expiry config composite UI.
	 *
	 * @return the project expiry config composite UI
	 */
	public ProjectExpiryConfigCompositeAction getProjectExpiryConfigCompositeUI() {
		return projectExpiryConfigCompositeUI;
	}

	/**
	 * Gets the live message config UI.
	 *
	 * @return the live message config UI
	 */
	public LiveMessageConfigAction getLiveMessageConfigUI() {
		return liveMessageConfigUI;
	}

	/**
	 * Sets the live message config UI.
	 *
	 * @param liveMessageConfigUI the new live message config UI
	 */
	public void setLiveMessageConfigUI(LiveMessageConfigAction liveMessageConfigUI) {
		this.liveMessageConfigUI = liveMessageConfigUI;
	}

	/**
	 * Gets the notification compo UI.
	 *
	 * @return the notification compo UI
	 */
	public NotificationCompositeAction getNotificationCompoUI() {
		return notificationCompoUI;
	}

	/**
	 * Sets the notification compo UI.
	 *
	 * @param notificationCompoUI the new notification compo UI
	 */
	public void setNotificationCompoUI(NotificationCompositeAction notificationCompoUI) {
		this.notificationCompoUI = notificationCompoUI;
	}

	/**
	 * Gets the base app composite UI.
	 *
	 * @return the base app composite UI
	 */
	public BaseAppCompositeAction getBaseAppCompositeUI() {
		return baseAppCompositeUI;
	}

	/**
	 * Gets the user app composite UI.
	 *
	 * @return the user app composite UI
	 */
	public UserApplicationCompositeAction getUserAppCompositeUI() {
		return userAppCompositeUI;
	}

	/**
	 * Gets the project app composite UI.
	 *
	 * @return the project app composite UI
	 */
	public ProjectApplicationCompositeAction getProjectAppCompositeUI() {
		return projectAppCompositeUI;
	}

	/**
	 * Gets the start application composite UI.
	 *
	 * @return the start application composite UI
	 */
	public StartApplicationCompositeAction getStartApplicationCompositeUI() {
		return startApplicationCompositeUI;
	}

	
	/**
	 * Gets the project create evt composite UI.
	 *
	 * @return the project create evt composite UI
	 */
	public ProjectCreateEvtCompositeAction getProjectCreateEvtCompositeUI() {
		return projectCreateEvtCompositeUI;
	}

	/**
	 * Gets the project delete composite UI.
	 *
	 * @return the project delete composite UI
	 */
	public ProjectDeleteEvtCompositeAction getProjectDeleteCompositeUI() {
		return projectDeleteCompositeUI;
	}

	/**
	 * Gets the project deactivate composite UI.
	 *
	 * @return the project deactivate composite UI
	 */
	public ProjectDeactivateEvtCompositeAction getProjectDeactivateCompositeUI() {
		return projectDeactivateCompositeUI;
	}

	/**
	 * Gets the project activate composite UI.
	 *
	 * @return the project activate composite UI
	 */
	public ProjectActivateEvtCompositeAction getProjectActivateCompositeUI() {
		return projectActivateCompositeUI;
	}

	/**
	 * Gets the project AA rel assign evt composite UI.
	 *
	 * @return the project AA rel assign evt composite UI
	 */
	public ProjectAARelAssignEvtCompositeAction getProjectAARelAssignEvtCompositeUI() {
		return projectAARelAssignEvtCompositeUI;
	}

	/**
	 * Gets the notify AA project user composite UI.
	 *
	 * @return the notify AA project user composite UI
	 */
	public NotifyAAProjectUserCompositeAction getNotifyAAProjectUserCompositeUI() {
		return notifyAAProjectUserCompositeUI;
	}

	/**
	 * Gets the user project exp grace composite UI.
	 *
	 * @return the user project exp grace composite UI
	 */
	public UserProjectExpGraceCompositeAction getUserProjectExpGraceCompositeUI() {
		return userProjectExpGraceCompositeUI;
	}

	/**
	 * Gets the user project rel exp composite UI.
	 *
	 * @return the user project rel exp composite UI
	 */
	public UserProjectExpCompositeAction getUserProjectRelExpCompositeUI() {
		return userProjectRelExpCompositeUI;
	}

	/**
	 * Gets the user pro rel assign composite UI.
	 *
	 * @return the user pro rel assign composite UI
	 */
	public UserProRelAssignEvtCompositeAction getUserProRelAssignCompositeUI() {
		return userProRelAssignCompositeUI;
	}

	/**
	 * Gets the user pro rel remove composite UI.
	 *
	 * @return the user pro rel remove composite UI
	 */
	public UserProRelRemoveEvtCompositeAction getUserProRelRemoveCompositeUI() {
		return userProRelRemoveCompositeUI;
	}

	/**
	 * Gets the template composite UI.
	 *
	 * @return the template composite UI
	 */
	public TemplateCompositeAction getTemplateCompositeUI() {
		return templateCompositeUI;
	}

	/**
	 * Gets the object exp part UI.
	 *
	 * @return the object exp part UI
	 */
	public ObjectExpPage getObjectExpPartUI() {
		return objectExpPartUI;
	}

	/* (non-Javadoc)
	 * @see com.magna.xmsystem.xmadmin.ui.parts.IEditablePart#isDirty()
	 */
	@Override
	public boolean isDirty() {
		return this.dirty.isDirty();
	}

	/* (non-Javadoc)
	 * @see com.magna.xmsystem.xmadmin.ui.parts.IEditablePart#setDirty(boolean)
	 */
	public void setDirty(boolean dirty) {
		this.dirty.setDirty(dirty);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.magna.xmsystem.xmadmin.ui.parts.IEditablePart#save()
	 */
	@Persist
	public void save() {
		MPart part = (MPart) modelService.find(CommonConstants.PART_ID.INFORMATION_PART_ID, application);
		InformationPart informationPart = (InformationPart) part.getObject();
		StackLayout layout = informationPart.getCompLayout();
		Object topControll = layout.topControl;
		if (topControll instanceof SiteCompositeAction) {
			this.siteCompositeUI.saveSiteHandler();
		} else if (topControll instanceof AdministrationAreaCompositeAction) {
			this.adminAreaCompisiteUI.saveAdminAreaHandler();
		} else if (topControll instanceof ProjectCompositeAction) {
			this.projectCompsiteUI.saveProjectHandler();
		} else if (topControll instanceof UserCompositeAction) {
			this.userCompositeUI.saveUserHandler();
		} else if (topControll instanceof UserGroupCompositeAction) {
			this.groupCompositeUI.saveGroupHandler();
		} else if (topControll instanceof ProjectGroupCompositeAction) {
			this.projectGroupCompositeUI.saveGroupHandler();
		}  else if (topControll instanceof UserAppGroupCompositeAction) {
			this.userAppGroupCompositeUI.saveUserAppGroupHandler();
		}  else if (topControll instanceof ProjectAppGroupCompositeAction) {
			this.projectAppGroupCompositeUI.saveProjectAppGroupHandler();
		}  else if (topControll instanceof DirectoryCompositeAction) {
			this.directoryCompositeUI.saveDirectoryHandler();
		} else if (topControll instanceof RoleCompositeAction) {
			this.roleCompositeUI.saveRoleHandler();
		} else if (topControll instanceof BaseAppCompositeAction) {
			this.baseAppCompositeUI.saveHandler();
		} else if (topControll instanceof StartApplicationCompositeAction) {
			this.startApplicationCompositeUI.saveStartProAppHandler();
		} else if (topControll instanceof ProjectApplicationCompositeAction) {
			this.projectAppCompositeUI.saveHandler();
		} else if (topControll instanceof UserApplicationCompositeAction) {
			this.userAppCompositeUI.saveHandler();
		} else if (topControll instanceof SmtpConfigCompositeAction) {
			this.smtpConfigCompositeUI.saveSmtpConfiguration();
		} else if (topControll instanceof LdapConfigCompositeAction) {
			this.ldapConfigCompositeUI.saveLdapConfiguration();
		} else if (topControll instanceof SingletonAppTimeConfigComopsiteAction) {
			this.singletonAppTimeConfigComopsiteUI.saveSingletonAppTime();
		} else if (topControll instanceof ProjectExpiryConfigCompositeAction) {
			this.projectExpiryConfigCompositeUI.saveProjectExpirySettings();
		} else if (topControll instanceof NotificationCompositeAction) {
			this.notificationCompoUI.saveNotificationHandler();
		} else if (topControll instanceof LiveMessageConfigAction) {
			this.liveMessageConfigUI.saveLiveMessageHandler();
		} else if (topControll instanceof TemplateCompositeAction) {
			this.templateCompositeUI.saveTemplateHandler();
		} else if (topControll instanceof ProjectCreateEvtCompositeAction) {
			this.projectCreateEvtCompositeUI.saveProCreateActionHandler();
		} else if (topControll instanceof ProjectDeleteEvtCompositeAction) {
			this.projectDeleteCompositeUI.saveProDeleteActionHandler();
		} else if (topControll instanceof ProjectDeactivateEvtCompositeAction) {
			this.projectDeactivateCompositeUI.saveProDeactiveActionHandler();
		} else if (topControll instanceof ProjectActivateEvtCompositeAction) {
			this.projectActivateCompositeUI.saveProActiveActionHandler();
		} else if (topControll instanceof UserProRelAssignEvtCompositeAction) {
			this.userProRelAssignCompositeUI.saveUserProRelAssignEvtActionHandler();
		} else if (topControll instanceof UserProRelRemoveEvtCompositeAction) {
			this.userProRelRemoveCompositeUI.saveUserProRelRemoveEvtActionHandler();
		}
		/*if (this.isDirty()) {
			dirty.setDirty(false);
		}*/
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.magna.xmsystem.xmadmin.ui.parts.IEditablePart#discard()
	 */
	@Override
	public void discard() {
		MPart part = (MPart) modelService.find(CommonConstants.PART_ID.INFORMATION_PART_ID, application);
		InformationPart informationPart = (InformationPart) part.getObject();
		StackLayout layout = informationPart.getCompLayout();
		Object topControll = layout.topControl;
		if (topControll instanceof SiteCompositeAction) {
			this.siteCompositeUI.cancelSiteHandler();
		} else if (topControll instanceof AdministrationAreaCompositeAction) {
			this.adminAreaCompisiteUI.cancelAdminAreaHandler();
		} else if (topControll instanceof ProjectCompositeAction) {
			this.projectCompsiteUI.cancelProjectHandler();
		} else if (topControll instanceof UserCompositeAction) {
			this.userCompositeUI.cancelUserHandler();
		} else if (topControll instanceof UserGroupCompositeAction) {
			this.groupCompositeUI.cancelGroupHandler();
		} else if (topControll instanceof ProjectGroupCompositeAction) {
			this.projectGroupCompositeUI.cancelGroupHandler();
		} else if (topControll instanceof UserAppGroupCompositeAction) {
			this.userAppGroupCompositeUI.cancelUserAppGroupHandler();
		} else if (topControll instanceof ProjectAppGroupCompositeAction) {
			this.projectAppGroupCompositeUI.cancelProjectAppGroupHandler();
		} else if (topControll instanceof DirectoryCompositeAction) {
			this.directoryCompositeUI.cancelDirectoryHandler();
		} else if (topControll instanceof RoleCompositeAction) {
			this.roleCompositeUI.cancelRoleHandler();
		} else if (topControll instanceof BaseAppCompositeAction) {
			this.baseAppCompositeUI.cancelHandler();
		} else if (topControll instanceof StartApplicationCompositeAction) {
			this.startApplicationCompositeUI.cancelStartProAppHandler();
		} else if (topControll instanceof ProjectApplicationCompositeAction) {
			this.projectAppCompositeUI.cancelHandler();
		} else if (topControll instanceof UserApplicationCompositeAction) {
			this.userAppCompositeUI.cancelHandler();
		} else if (topControll instanceof SmtpConfigCompositeAction) {
			this.smtpConfigCompositeUI.cancelSmtpConfiguration();
		} else if (topControll instanceof LdapConfigCompositeAction) {
			this.ldapConfigCompositeUI.cancelLdapConfiguration();
		} else if (topControll instanceof SingletonAppTimeConfigComopsiteAction) {
			this.singletonAppTimeConfigComopsiteUI.cancelSingletonAppTimeConfig();
		} else if (topControll instanceof ProjectExpiryConfigCompositeAction) {
			this.projectExpiryConfigCompositeUI.cancelProjectExpiryConfig();
		} else if (topControll instanceof NoProjectPopmessageCompositeAction) {
			this.noProjectPopMessageCompositeUI.cancelNoProjectPopmessage();
		} else if (topControll instanceof NotificationCompositeAction) {
			this.notificationCompoUI.cancelNotificationHandler();
		} else if (topControll instanceof LiveMessageConfigAction) {
			this.liveMessageConfigUI.cancelLiveMessageHandler();
		} else if (topControll instanceof TemplateCompositeAction) {
			this.templateCompositeUI.cancelTemplateHandler();
		} else if (topControll instanceof ProjectCreateEvtCompositeAction) {
			this.projectCreateEvtCompositeUI.cancelProCreateEvtActionHandler();
		} else if (topControll instanceof ProjectDeleteEvtCompositeAction) {
			this.projectDeleteCompositeUI.cancelProDeleteEvtActionHandler();
		} else if (topControll instanceof ProjectDeactivateEvtCompositeAction) {
			this.projectDeactivateCompositeUI.cancelProDeactivateEvtActionHandler();
		} else if (topControll instanceof ProjectActivateEvtCompositeAction) {
			this.projectActivateCompositeUI.cancelProActivateEvtActionHandler();
		} else if (topControll instanceof UserProRelAssignEvtCompositeAction) {
			this.userProRelAssignCompositeUI.cancelUserProRelAssignEvtActionHandler();
		} else if (topControll instanceof UserProRelRemoveEvtCompositeAction) {
			this.userProRelRemoveCompositeUI.cancelUserProRelRemoveEvtActionHandler();
		}
	}
	
	/**
	 * Update permissions.
	 *
	 * @param menuitemId the menu item Id
	 * @param isVisible the is Visible
	 */
	private void setMenuItemVisble(String menuitemId, boolean isVisible) {
		List<MMenuElement> children = mTrimmedWindow.getMainMenu().getChildren();
		
		for (MMenuElement child : children) {
			if (child instanceof MMenu) {
				MMenu menu = (MMenu) child;
				List<MMenuElement> menuItems = menu.getChildren();
				for (MMenuElement mMenuElement : menuItems) {
					if (menuitemId.equals(mMenuElement.getElementId())) {
						mMenuElement.setVisible(isVisible);
					}
				}
			}
		} 
	}
	
	/**
	 * Update UserReview MenuItem.
	 */
	public void updateUserReviewMenuItem() {
		try {
			UserController userController = new UserController();
			boolean isSuperAdmin = userController.isSuperAdmin();
			String menuitemId = CommonConstants.COMMAND_ID.USER_REVIEW;
			if (isSuperAdmin) {
				setMenuItemVisble(menuitemId, true);
			} else {
				setMenuItemVisble(menuitemId, false);
			}
		} catch (Exception e) {
			LOGGER.error("Exception occured while checking for user permission",e);
		}
	}
}