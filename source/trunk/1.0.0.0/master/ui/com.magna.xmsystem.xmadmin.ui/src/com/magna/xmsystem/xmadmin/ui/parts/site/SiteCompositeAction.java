package com.magna.xmsystem.xmadmin.ui.parts.site;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.inject.Inject;

import org.eclipse.core.databinding.Binding;
import org.eclipse.core.databinding.DataBindingContext;
import org.eclipse.core.databinding.UpdateValueStrategy;
import org.eclipse.core.databinding.beans.BeanProperties;
import org.eclipse.core.databinding.observable.value.IObservableValue;
import org.eclipse.core.databinding.observable.value.IValueChangeListener;
import org.eclipse.core.databinding.observable.value.ValueChangeEvent;
import org.eclipse.e4.core.services.nls.Translation;
import org.eclipse.e4.ui.model.application.ui.MDirtyable;
import org.eclipse.jface.databinding.fieldassist.ControlDecorationSupport;
import org.eclipse.jface.databinding.swt.WidgetProperties;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.viewers.TreePath;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.VerifyEvent;
import org.eclipse.swt.events.VerifyListener;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Link;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.widgets.ToolItem;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.client.ResourceAccessException;

import com.magna.xmbackend.entities.LanguagesTbl;
import com.magna.xmbackend.entities.SiteTranslationTbl;
import com.magna.xmbackend.entities.SitesTbl;
import com.magna.xmbackend.vo.jpa.site.SiteRequest;
import com.magna.xmsystem.dependencies.customExceptions.UnauthorizedAccessException;
import com.magna.xmsystem.dependencies.utils.LANG_ENUM;
import com.magna.xmsystem.dependencies.utils.XMSystemUtil;
import com.magna.xmsystem.ui.controls.widgets.CustomMessageDialog;
import com.magna.xmsystem.xmadmin.message.Message;
import com.magna.xmsystem.xmadmin.message.MessageRegistry;
import com.magna.xmsystem.xmadmin.restclient.site.SiteController;
import com.magna.xmsystem.xmadmin.ui.dialogs.lang.ControlModel;
import com.magna.xmsystem.xmadmin.ui.dialogs.lang.TextAreaModifyListener;
import com.magna.xmsystem.xmadmin.ui.dialogs.lang.XMAdminLangTextAreaDialog;
import com.magna.xmsystem.xmadmin.ui.dialogs.lang.XMAdminLangTextDialog;
import com.magna.xmsystem.xmadmin.ui.parts.icons.IconDialog;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.AdminTreeviewer;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.AdminTreeFactory;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.icon.Icon;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.sitemodel.Site;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.sitemodel.Sites;
import com.magna.xmsystem.xmadmin.ui.validation.NameValidation;
import com.magna.xmsystem.xmadmin.ui.validation.StatusValidation;
import com.magna.xmsystem.xmadmin.ui.validation.SymbolValidation;
import com.magna.xmsystem.xmadmin.util.CommonConstants;
import com.magna.xmsystem.xmadmin.util.MessageType;
import com.magna.xmsystem.xmadmin.util.XMAdminUtil;

/**
 * Class for UI of Site Composite.
 *
 * @author shashwat.anand
 */
public class SiteCompositeAction extends SiteCompositeUI {

	/** Logger instance. */
	private static final Logger LOGGER = LoggerFactory.getLogger(SiteCompositeAction.class);

	/** Member variable for site model. */
	private Site siteModel;

	/** Member variable for widgetValue. */
	transient private IObservableValue<?> widgetValue;

	/** Member variable for modelValue. */
	transient private IObservableValue<?> modelValue;

	/** Member variable for binding. */
	transient private Binding bindValue;

	/** Member variable for {@link MessageRegistry}. */
	@Inject
	private MessageRegistry registry;

	/** Member variable for messages. */
	@Inject
	@Translation
	transient private Message messages;

	/**
	 * Member variable for data binding context the DataBindingContext object
	 * will manage the databindings.
	 */
	final transient DataBindingContext dataBindContext = new DataBindingContext();

	/** Member variable to store old model. */
	private Site oldModel;
	/** Member variable for dirty. */
	transient private MDirtyable dirty;

	/**
	 * Constructor.
	 *
	 * @param parent
	 *            the parent
	 */
	@Inject
	public SiteCompositeAction(final Composite parent) {
		super(parent, SWT.NONE);
		initListeners();
	}

	/**
	 * Method for Binding modal to widget.
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public void bindValues() {
		try {
			// Name field binding
			widgetValue = WidgetProperties.text(SWT.Modify).observe(this.txtName);
			modelValue = BeanProperties.value(Site.class, Site.PROPERTY_SITENAME).observe(this.siteModel);
			widgetValue.addValueChangeListener(new IValueChangeListener() {
				/**
				 * handler to update button status
				 */
				@Override
				public void handleValueChange(final ValueChangeEvent event) {
					updateButtonStatus(event);
				}
			});

			// define the UpdateValueStrategy
			final UpdateValueStrategy update = new UpdateValueStrategy();
			update.setAfterGetValidator(new NameValidation(messages, StatusValidation.SITE));
			bindValue = dataBindContext.bindValue(widgetValue, modelValue, update, null);
			ControlDecorationSupport.create(bindValue, SWT.TOP | SWT.LEFT);

			// Action check box binding
			widgetValue = WidgetProperties.selection().observe(this.activeBtn);
			modelValue = BeanProperties.value(Site.class, Site.PROPERTY_ACTIVE).observe(this.siteModel);
			bindValue = dataBindContext.bindValue(widgetValue, modelValue);

			// Symbol toolItem setup
			Icon icon;
			String iconPath;
			if ((icon = this.siteModel.getIcon()) != null && !XMSystemUtil.isEmpty(iconPath = icon.getIconPath())) {
				if (iconPath.contains("null")) { //$NON-NLS-1$
					toolItem.setImage(XMSystemUtil.getInstance().getImage(this.getClass(), "icons/16x16/browse.png")); //$NON-NLS-1$
				} else {
					toolItem.setImage(XMSystemUtil.getInstance().getImage(this.getClass(), iconPath, true, true));
					// Symbol toolItem binding
					widgetValue = WidgetProperties.tooltipText().observe(this.toolItem);
					modelValue = BeanProperties
							.value(Site.class, Site.PROPERTY_ICON + "." + Icon.PROPERTY_ICONNAME, Icon.class)
							.observe(this.siteModel);
					bindValue = dataBindContext.bindValue(widgetValue, modelValue);

					// Symbol Field binding
					widgetValue = WidgetProperties.text(SWT.Modify).observe(this.txtSymbol);
					modelValue = BeanProperties
							.value(Site.class, Site.PROPERTY_ICON + "." + Icon.PROPERTY_ICONNAME, Icon.class)
							.observe(this.siteModel);
					final UpdateValueStrategy symbolUpdate = new UpdateValueStrategy();
					symbolUpdate.setAfterGetValidator(new SymbolValidation());
					bindValue = dataBindContext.bindValue(widgetValue, modelValue, symbolUpdate, null);
					ControlDecorationSupport.create(bindValue, SWT.TOP | SWT.LEFT);
				}
			}
			ControlDecorationSupport.create(bindValue, SWT.TOP | SWT.LEFT);
		} catch (Exception e) {
			LOGGER.error("Exception occured while databinding", e);
		}
	}

	/**
	 * Method to update the button status.
	 *
	 * @param event
	 *            the event
	 */
	@SuppressWarnings("rawtypes")
	private void updateButtonStatus(final ValueChangeEvent event) {
		final String name = (String) event.getObservableValue().getValue();
		if (this.saveBtn != null) {
			if (XMSystemUtil.isEmpty(name) || name.trim().length() == 0
					|| (!name.matches(CommonConstants.RegularExpressions.ALLOWED_SITE_NAME_REGEXP))) {
				this.saveBtn.setEnabled(false);
			} else {
				this.saveBtn.setEnabled(true);
			}
		}
	}

	/**
	 * Gets the site model.
	 *
	 * @return the siteModel
	 */
	public Site getSiteModel() {
		return siteModel;
	}

	/**
	 * Sets the site model.
	 *
	 * @param siteModel
	 *            the siteModel to set
	 */
	public void setSiteModel(final Site siteModel) {
		this.siteModel = siteModel;
	}

	/**
	 * Method to open dialog.
	 *
	 * @param shell
	 *            the shell
	 */
	private void openDescDialog(final Shell shell) {
		if (siteModel == null) {
			return;
		}
		if (siteModel.getOperationMode() != CommonConstants.OPERATIONMODE.VIEW) {
			String text = txtDesc.getText();
			LANG_ENUM currentLocaleEnum = XMAdminUtil.getInstance().getCurrentLocaleEnum();
			siteModel.setDescription(currentLocaleEnum, text);
		}
		final Map<LANG_ENUM, String> obModelMap = new HashMap<>();
		obModelMap.put(LANG_ENUM.ENGLISH, this.siteModel.getDescription(LANG_ENUM.ENGLISH));
		obModelMap.put(LANG_ENUM.GERMAN, this.siteModel.getDescription(LANG_ENUM.GERMAN));
		boolean isEditable = txtDesc.getEditable();
		ControlModel controlModel = new ControlModel(this.messages.objectDescriptionLabel, obModelMap, Site.DESC_LIMIT, false,
				isEditable);
		controlModel.initDefaultLabels(this.messages);
		final XMAdminLangTextDialog dialogArea = new XMAdminLangTextDialog(shell, controlModel);
		final int retVal = dialogArea.open();
		if (retVal == IDialogConstants.OK_ID) {
			final Map<LANG_ENUM, String> descriptionMap = this.siteModel.getDescriptionMap();
			descriptionMap.put(LANG_ENUM.ENGLISH, controlModel.getObjectModel(LANG_ENUM.ENGLISH));
			descriptionMap.put(LANG_ENUM.GERMAN, controlModel.getObjectModel(LANG_ENUM.GERMAN));
			updateDescWidget();
		}
	}

	/**
	 * Method to open dialog.
	 *
	 * @param shell
	 *            the shell
	 */
	private void openRemarkDialog(final Shell shell) {
		if (siteModel == null) {
			return;
		}
		if (siteModel.getOperationMode() != CommonConstants.OPERATIONMODE.VIEW) {
			String text = txtRemarks.getText();
			LANG_ENUM currentLocaleEnum = XMAdminUtil.getInstance().getCurrentLocaleEnum();
			siteModel.setRemarks(currentLocaleEnum, text);
		}
		final Map<LANG_ENUM, String> obModelMap = new HashMap<>();
		obModelMap.put(LANG_ENUM.ENGLISH, this.siteModel.getRemarks(LANG_ENUM.ENGLISH));
		obModelMap.put(LANG_ENUM.GERMAN, this.siteModel.getRemarks(LANG_ENUM.GERMAN));
		boolean isEditable = txtRemarks.getEditable();
		ControlModel controlModel = new ControlModel(this.messages.objectRemarkLabel, obModelMap, Site.REMARK_LIMIT, false,
				isEditable);
		controlModel.initDefaultLabels(this.messages);
		final XMAdminLangTextAreaDialog dialogArea = new XMAdminLangTextAreaDialog(shell, controlModel);
		final int retVal = dialogArea.open();
		if (retVal == IDialogConstants.OK_ID) {
			final Map<LANG_ENUM, String> remarksMap = this.siteModel.getRemarksMap();
			remarksMap.put(LANG_ENUM.ENGLISH, controlModel.getObjectModel(LANG_ENUM.ENGLISH));
			remarksMap.put(LANG_ENUM.GERMAN, controlModel.getObjectModel(LANG_ENUM.GERMAN));
			updateRemarkWidget();
		}
	}

	/**
	 * Add the listener to widgets.
	 */
	private void initListeners() {
		// Add listener to widgets

		// Event handling when users click on desc lang links.
		this.descLink.addSelectionListener(new SelectionAdapter() {
			/**
			 * description link handler
			 */
			@Override
			public void widgetSelected(final SelectionEvent event) {
				final Link linkWidget = (Link) event.widget;
				openDescDialog(linkWidget.getShell());
			}
		});

		// Event handling when users click on remarks lang links.
		this.remarksTranslate.addSelectionListener(new SelectionAdapter() {
			/**
			 * remarks text link handler
			 */
			@Override
			public void widgetSelected(final SelectionEvent event) {
				final Link linkWidget = (Link) event.widget;
				openRemarkDialog(linkWidget.getShell());
			}
		});

		if (this.saveBtn != null) {
			this.saveBtn.addSelectionListener(new SelectionAdapter() {

				/**
				 * Save button handler
				 */
				@Override
				public void widgetSelected(final SelectionEvent event) {
					saveSiteHandler();
				}
			});
		}

		if (this.cancelBtn != null) {
			this.cancelBtn.addSelectionListener(new SelectionAdapter() {

				/**
				 * Cancel button handler
				 */
				@Override
				public void widgetSelected(final SelectionEvent event) {
					cancelSiteHandler();
				}
			});
		}

		this.toolItem.addSelectionListener(new SelectionAdapter() {

			/**
			 * Symbol button handler
			 */
			@Override
			public void widgetSelected(final SelectionEvent event) {
				if ((boolean) toolItem.getData("editable")) {
					final ToolItem widget = (ToolItem) event.widget;
					final IconDialog dialog = new IconDialog(widget.getParent().getShell(),
							messages.browseIconDialogTitle, messages.icontableviewerSecondColumnLabel);

					final int returnVal = dialog.open();
					if (IDialogConstants.OK_ID == returnVal) {
						final Icon checkedIcon = dialog.getCheckedIcon();
						toolItem.setImage(XMSystemUtil.getInstance().getImage(this.getClass(), checkedIcon.getIconPath(), true, true));
						siteModel.setIcon(checkedIcon);
					}
				}
			}
		});

		this.txtRemarks.addModifyListener(new TextAreaModifyListener(this.lblRemarksCount, Site.REMARK_LIMIT));
		this.txtRemarks.addVerifyListener(new VerifyListener() {

			@Override
			public void verifyText(VerifyEvent event) {
				String source = ((Text) event.widget).getText();
				final String remarkText = source.substring(0, event.start) + event.text + source.substring(event.end);
				int length = remarkText.length();
				if (length > Site.REMARK_LIMIT) {
					event.doit = false;

				}

			}
		});
	}

	/**
	 * Validates the model before submit.
	 *
	 * @return boolean
	 */
	protected boolean validate() {
		if (!saveBtn.isEnabled()) {
			return false;
		}
		final String siteName = this.siteModel.getName();
		Icon icon;
		if ((XMSystemUtil.isEmpty(siteName) && (icon = this.siteModel.getIcon()) != null
				&& XMSystemUtil.isEmpty(icon.getIconName()))) {
			CustomMessageDialog.openError(this.getShell(), messages.nameSymbolErrorTitle, messages.nameSymbolError);
			return false;
		}
		if (XMSystemUtil.isEmpty(siteName)) {
			CustomMessageDialog.openError(this.getShell(), messages.nameErrorTitle, messages.nameError);
			return false;
		}
		if ((icon = this.siteModel.getIcon()) != null && XMSystemUtil.isEmpty(icon.getIconName())) {
			CustomMessageDialog.openError(this.getShell(), messages.symbolErrorTitle, messages.symbolError);
			return false;
		}
		final Sites sites = AdminTreeFactory.getInstance().getSites();
		final Collection<IAdminTreeChild> sitesCollection = sites.getSitesCollection();
		if (siteModel.getOperationMode() == CommonConstants.OPERATIONMODE.CHANGE) {
			if (!siteName.equalsIgnoreCase(this.oldModel.getName())) {
				final Map<String, Long> result = sitesCollection.parallelStream()
						.collect(Collectors.groupingBy(site -> ((Site) site).getName().toUpperCase(), Collectors.counting()));
				if (result.containsKey(siteName.toUpperCase())) {
					CustomMessageDialog.openError(this.getShell(), messages.existingSiteNameTitle,
							messages.existingSiteNameError);
					return false;
				}
			}
		} else if (this.siteModel.getOperationMode() == CommonConstants.OPERATIONMODE.CREATE) {
			for (final IAdminTreeChild site : sitesCollection) {
				if (siteName.equalsIgnoreCase(((Site) site).getName())) {
					CustomMessageDialog.openError(this.getShell(), messages.existingSiteNameTitle,
							messages.existingSiteNameError);
					return false;
				}
			}
		}

		return true;
	}

	/**
	 * Method register method function for translation.
	 *
	 * @param registry
	 *            {@link MessageRegistry}
	 */
	public void registerMessages(final MessageRegistry registry) {
		registry.register((text) -> {
			if (grpSite != null && !grpSite.isDisposed()) {
				grpSite.setText(text);
			}
		}, (message) -> {
			if (siteModel != null) {
				if (siteModel.getOperationMode() == CommonConstants.OPERATIONMODE.VIEW) {
					return message.objectGroupDisaplyLabel + " \'" + this.siteModel.getName() + "\'";
				} else if (siteModel.getOperationMode() == CommonConstants.OPERATIONMODE.CHANGE) {
					return message.objectGroupChangeLabel + " \'" + this.siteModel.getName() + "\'";
				} else if (siteModel.getOperationMode() == CommonConstants.OPERATIONMODE.CREATE) {
					return message.siteGroupCreateLabel;
				}
			}

			return CommonConstants.EMPTY_STR;
		});
		registry.register((text) -> {
			if (lblName != null && !lblName.isDisposed()) {
				lblName.setText(text);
			}
		}, (message) -> {
			if (lblName != null && !lblName.isDisposed()) {
				return getUpdatedWidgetText(message.objectNameLabel, lblName);
			}
			return CommonConstants.EMPTY_STR;
		});

		registry.register((text) -> {
			if (lblDescrition != null && !lblDescrition.isDisposed()) {
				lblDescrition.setText(text);
			}
		}, (message) -> {
			if (lblDescrition != null && !lblDescrition.isDisposed()) {
				return getUpdatedWidgetText(message.objectDescriptionLabel, lblDescrition);
			}
			return CommonConstants.EMPTY_STR;
		});
		registry.register((text) -> {
			if (lblActive != null && !lblActive.isDisposed()) {
				lblActive.setText(text);
			}
		}, (message) -> {
			if (lblActive != null && !lblActive.isDisposed()) {
				return getUpdatedWidgetText(message.objectActiveLabel, lblActive);
			}
			return CommonConstants.EMPTY_STR;
		});
		registry.register((text) -> {
			if (lblSymbol != null && !lblSymbol.isDisposed()) {
				lblSymbol.setText(text);
			}
		}, (message) -> {
			if (lblSymbol != null && !lblSymbol.isDisposed()) {
				return getUpdatedWidgetText(message.objectSymbolLabel, lblSymbol);
			}
			return CommonConstants.EMPTY_STR;
		});
		if (saveBtn != null) {
			registry.register((text) -> {
				if (saveBtn != null && !saveBtn.isDisposed()) {
					saveBtn.setText(text);
				}
			}, (message) -> {
				if (saveBtn != null && !saveBtn.isDisposed()) {
					return getUpdatedWidgetText(message.saveButtonText, saveBtn);
				}
				return CommonConstants.EMPTY_STR;
			});
		}
		if (cancelBtn != null) {
			registry.register((text) -> {
				if (cancelBtn != null && !cancelBtn.isDisposed()) {
					cancelBtn.setText(text);
				}
			}, (message) -> {
				if (cancelBtn != null && !cancelBtn.isDisposed()) {
					return getUpdatedWidgetText(message.cancelButtonText, cancelBtn);
				}
				return CommonConstants.EMPTY_STR;
			});
		}
		registry.register((text) -> {
			if (remarksTranslate != null && !remarksTranslate.isDisposed()) {
				remarksTranslate.setText(text);
			}
		}, (message) -> {
			if (remarksTranslate != null && !remarksTranslate.isDisposed()) {
				return getUpdatedWidgetText("<a>" + message.objectTranslationLinkText + "</a>", remarksTranslate);
			}
			return CommonConstants.EMPTY_STR;
		});

		registry.register((text) -> {
			if (remarksLabel != null && !remarksLabel.isDisposed()) {
				remarksLabel.setText(text);
			}
		}, (message) -> {
			if (remarksLabel != null && !remarksLabel.isDisposed()) {
				return getUpdatedWidgetText(message.objectRemarkLabel, remarksLabel);
			}
			return CommonConstants.EMPTY_STR;
		});

		registry.register((text) -> {
			if (descLink != null && !descLink.isDisposed()) {
				descLink.setText(text);
			}
		}, (message) -> {
			if (descLink != null && !descLink.isDisposed()) {
				return getUpdatedWidgetText("<a>" + message.objectTranslationLinkText + "</a>", descLink);
			}
			return CommonConstants.EMPTY_STR;
		});

		registry.register((text) -> {
			if (txtDesc != null && !txtDesc.isDisposed()) {
				txtDesc.setText(text);
				updateDescWidget();
			}
		}, (message) -> {
			if (this.siteModel != null && txtDesc != null && !txtDesc.isDisposed()) {
				final LANG_ENUM langEnum = XMAdminUtil.getInstance().getCurrentLocaleEnum();
				return this.siteModel.getDescription(langEnum) == null ? CommonConstants.EMPTY_STR
						: this.siteModel.getDescription(langEnum);
			}
			return CommonConstants.EMPTY_STR;
		});

		registry.register((text) -> {
			if (txtRemarks != null && !txtRemarks.isDisposed()) {
				txtRemarks.setText(text);
				updateRemarkWidget();
			}
		}, (message) -> {
			if (this.siteModel != null && txtRemarks != null && !txtRemarks.isDisposed()) {
				final LANG_ENUM langEnum = XMAdminUtil.getInstance().getCurrentLocaleEnum();
				return this.siteModel.getRemarks(langEnum) == null ? CommonConstants.EMPTY_STR
						: this.siteModel.getRemarks(langEnum);
			}
			return CommonConstants.EMPTY_STR;
		});
	}

	/**
	 * Gets the new text based on new locale.
	 *
	 * @param message
	 *            {@link String}
	 * @param control
	 *            {@link Control}
	 * @return {@link String} new text
	 */
	private String getUpdatedWidgetText(final String message, final Control control) {
		control.requestLayout();
		control.getParent().redraw();
		control.getParent().getParent().update();
		control.getParent().getParent().getParent().update();
		return message;
	}

	/**
	 * Gets the old model.
	 *
	 * @return the oldModel
	 */
	public Site getOldModel() {
		return oldModel;
	}

	/**
	 * Sets the old model.
	 *
	 * @param oldModel
	 *            the oldModel to set
	 */
	public void setOldModel(final Site oldModel) {
		this.oldModel = oldModel;
	}

	/**
	 * Method for Creates the site operation.
	 */
	private void createSiteOperation() {

		try {
			SiteController siteController = new SiteController();
			SitesTbl sitesVo = siteController.createSite(mapVOObjectWithModel());
			String siteId = sitesVo.getSiteId();
			if (!XMSystemUtil.isEmpty(siteId)) {
				this.siteModel.setSiteId(siteId);
				Collection<SiteTranslationTbl> siteTranslationTblCollection = sitesVo.getSiteTranslationTblCollection();
				for (SiteTranslationTbl siteTranslationTbl : siteTranslationTblCollection) {
					String siteTranslationId = siteTranslationTbl.getSiteTranslationId();
					LanguagesTbl languageCode = siteTranslationTbl.getLanguageCode();
					LANG_ENUM langEnum = LANG_ENUM.getLangEnum(languageCode.getLanguageCode());
					this.siteModel.setTranslationId(langEnum, siteTranslationId);
				}

				AdminTreeFactory instance = AdminTreeFactory.getInstance();
				// Attach model to old model
				if (oldModel == null) { // Attach this to tree
					setOldModel(siteModel.deepCopySite(false, null));
					// getOldModel().getSiteChildren().put(SiteAdministrations.class.getName(), new SiteAdministrations(siteModel));
					instance.getSites().add(siteId, getOldModel());
				}
				this.dirty.setDirty(false);
				AdminTreeviewer adminTree = XMAdminUtil.getInstance().getAdminTree();
				adminTree.refresh(true);

				adminTree.setSelection(new StructuredSelection(instance.getSites()), true);
				TreePath[] selectionPaths = adminTree.getStructuredSelection().getPaths();
				if (selectionPaths != null && selectionPaths.length > 0) {
					adminTree.setExpandedState(selectionPaths[0], true);
				}
				adminTree.setSelection(new StructuredSelection(getOldModel()), true);
				XMAdminUtil.getInstance().updateLogFile(
						messages.siteObject + " " + "'" + this.siteModel.getName() + "'" + " " + messages.objectCreate, MessageType.SUCCESS);
			}
		} catch (UnauthorizedAccessException e) {
			CustomMessageDialog.openError(this.getShell(), messages.objectPermissionDialogTitle,
					messages.objectPermissionDialogMsg);
		}catch (Exception e) {
			if (e instanceof ResourceAccessException) {
				CustomMessageDialog.openError(Display.getCurrent().getActiveShell(), messages.errorDialogTitile,
						messages.serverNotReachable);
			}
			LOGGER.warn("Unable to Save data ! " + e);
		}

	}

	/**
	 * Method for Change site operation.
	 */
	private void changeSiteOperation() {
		try {
			SiteController siteController = new SiteController();
			boolean isUpdated = siteController.updateSite(mapVOObjectWithModel());
			if (isUpdated) {
				setOldModel(siteModel.deepCopySite(true, getOldModel()));
				this.siteModel.setOperationMode(CommonConstants.OPERATIONMODE.VIEW);
				setOperationMode();
				this.dirty.setDirty(false);
				final Sites sites = AdminTreeFactory.getInstance().getSites();
				sites.sort();
				XMAdminUtil.getInstance().getAdminTree().refresh(true);
				XMAdminUtil.getInstance().getAdminTree().setSelection(new StructuredSelection(getOldModel()), true);
				XMAdminUtil.getInstance().updateLogFile(
						messages.siteObject + " " + "'" + this.siteModel.getName() + "'" + " " + messages.objectUpdate,
						MessageType.SUCCESS);
			}
		} catch (UnauthorizedAccessException e) {
			CustomMessageDialog.openError(this.getShell(), messages.objectPermissionDialogTitle,
					messages.objectPermissionDialogMsg);
		}catch (Exception e) {
			if (e instanceof ResourceAccessException) {
				CustomMessageDialog.openError(Display.getCurrent().getActiveShell(), messages.errorDialogTitile,
						messages.serverNotReachable);
			}
			LOGGER.warn("Unable to Update Site data ! " + e);
		}

	}

	/**
	 * Method for Map VO object with model.
	 *
	 * @return the com.magna.xmbackend.vo.jpa.site. site request
	 *         {@link SiteRequest}
	 */
	private com.magna.xmbackend.vo.jpa.site.SiteRequest mapVOObjectWithModel() {
		com.magna.xmbackend.vo.jpa.site.SiteRequest siteRequest = new com.magna.xmbackend.vo.jpa.site.SiteRequest();

		siteRequest.setId(this.siteModel.getSiteId());
		siteRequest.setName(this.siteModel.getName());
		siteRequest.setIconId(this.siteModel.getIcon().getIconId());
		siteRequest.setStatus(this.siteModel.isActive() == true ? com.magna.xmbackend.vo.enums.Status.ACTIVE
				: com.magna.xmbackend.vo.enums.Status.INACTIVE);

		List<com.magna.xmbackend.vo.jpa.site.SiteTranslation> siteTranslationList = new ArrayList<>();
		LANG_ENUM[] lang_values = LANG_ENUM.values();
		for (int index = 0; index < lang_values.length; index++) {
			com.magna.xmbackend.vo.jpa.site.SiteTranslation siteTranslation = new com.magna.xmbackend.vo.jpa.site.SiteTranslation();
			siteTranslation.setLanguageCode(lang_values[index].getLangCode());
			siteTranslation.setRemarks(this.siteModel.getRemarks(lang_values[index]));
			siteTranslation.setDescription(this.siteModel.getDescription(lang_values[index]));
			if (this.siteModel.getOperationMode() == CommonConstants.OPERATIONMODE.CHANGE) {
				String translationId = siteModel.getTranslationId(lang_values[index]);
				siteTranslation.setId(XMSystemUtil.isEmpty(translationId) ? CommonConstants.EMPTY_STR : translationId);
			}
			siteTranslationList.add(siteTranslation);
		}

		siteRequest.setSiteTranslations(siteTranslationList);

		return siteRequest;
	}

	/**
	 * Method for Sets the operation mode.
	 */
	public void setOperationMode() {
		if (this.siteModel != null) {
			if (siteModel.getOperationMode() == CommonConstants.OPERATIONMODE.VIEW) {
				this.txtName.setEditable(false);
				this.activeBtn.setEnabled(false);
				this.txtDesc.setEditable(false);
				this.txtRemarks.setEditable(false);
				this.toolItem.setData("editable", false);
				setShowButtonBar(false);
			} else if (siteModel.getOperationMode() == CommonConstants.OPERATIONMODE.CREATE) {
				this.txtName.setEditable(true);
				this.activeBtn.setEnabled(true);
				this.saveBtn.setEnabled(false);
				this.txtDesc.setEditable(true);
				this.txtRemarks.setEditable(true);
				this.toolItem.setData("editable", true);
				setShowButtonBar(true);
				this.dirty.setDirty(true);
			} else if (siteModel.getOperationMode() == CommonConstants.OPERATIONMODE.CHANGE) {
				this.txtName.setEditable(true);
				this.activeBtn.setEnabled(true);
				this.toolItem.setData("editable", true);
				this.txtDesc.setEditable(true);
				this.txtRemarks.setEditable(true);
				setShowButtonBar(true);
				this.dirty.setDirty(true);
			} else {
				this.txtName.setEditable(false);
				this.activeBtn.setEnabled(false);
				this.txtDesc.setEditable(false);
				this.txtRemarks.setEditable(false);
				this.toolItem.setData("editable", false);
				setShowButtonBar(false);
			}
		}
	}

	

	/**
	 * Save Handler.
	 */
	public void saveSiteHandler() {
		// validate the model
		saveDescAndRemarks();
		if (validate()) {

			if (!siteModel.getName().isEmpty() && siteModel.getIcon().getIconId() != null) {
				if (siteModel.getOperationMode() == CommonConstants.OPERATIONMODE.CREATE) {
					createSiteOperation();
				} else if (siteModel.getOperationMode() == CommonConstants.OPERATIONMODE.CHANGE) {
					changeSiteOperation();
				}
			}
		}
	}

	/**
	 * Save desc and remarks.
	 */
	private void saveDescAndRemarks() {
		LANG_ENUM currentLocaleEnum = XMAdminUtil.getInstance().getCurrentLocaleEnum();
		String desc = txtDesc.getText();
		siteModel.setDescription(currentLocaleEnum, desc);

		String remarks = txtRemarks.getText();
		siteModel.setRemarks(currentLocaleEnum, remarks);
	}

	/**
	 * 7 * Sets the dirty object.
	 *
	 * @param dirty
	 *            the new dirty object
	 */
	public void setDirtyObject(final MDirtyable dirty) {
		this.dirty = dirty;
	}

	/**
	 * Method for updating remarks.
	 */
	public void updateRemarkWidget() {
		if (this.siteModel == null) {
			return;
		}
		int operationMode = this.siteModel.getOperationMode();
		final LANG_ENUM currentLocaleEnum = XMAdminUtil.getInstance().getCurrentLocaleEnum();
		final String remarkForCurLocale = this.siteModel.getRemarks(currentLocaleEnum) == null
				? CommonConstants.EMPTY_STR : this.siteModel.getRemarks(currentLocaleEnum);
		if (operationMode != CommonConstants.OPERATIONMODE.VIEW) {
			this.txtRemarks.setText(remarkForCurLocale);
			return;
		}
		if (operationMode == CommonConstants.OPERATIONMODE.VIEW) {
			if (!XMSystemUtil.isEmpty(remarkForCurLocale)) {
				this.txtRemarks.setText(remarkForCurLocale);
				return;
			}
			final String remarkEN = this.siteModel.getRemarks(LANG_ENUM.ENGLISH);
			final String remarkDE = this.siteModel.getRemarks(LANG_ENUM.GERMAN);
			if (!XMSystemUtil.isEmpty(remarkEN)) {
				this.txtRemarks.setText(remarkEN);
				return;
			}

			if (!XMSystemUtil.isEmpty(remarkDE)) {
				this.txtRemarks.setText(remarkDE);
				return;
			}
		}
	}

	/**
	 * Inits the widgets.
	 */
	public void updateDescWidget() {
		if (this.siteModel == null) {
			return;
		}
		int operationMode = this.siteModel.getOperationMode();
		final LANG_ENUM currentLocaleEnum = XMAdminUtil.getInstance().getCurrentLocaleEnum();
		final String descForCurLocale = this.siteModel.getDescription(currentLocaleEnum) == null
				? CommonConstants.EMPTY_STR : this.siteModel.getDescription(currentLocaleEnum);
		if (operationMode != CommonConstants.OPERATIONMODE.VIEW) {
			this.txtDesc.setText(descForCurLocale);
			return;
		}
		if (operationMode == CommonConstants.OPERATIONMODE.VIEW) {
			if (!XMSystemUtil.isEmpty(descForCurLocale)) {
				this.txtDesc.setText(descForCurLocale);
				return;
			}
			final String descriptionEN = this.siteModel.getDescription(LANG_ENUM.ENGLISH);
			final String descriptionDE = this.siteModel.getDescription(LANG_ENUM.GERMAN);
			if (!XMSystemUtil.isEmpty(descriptionEN)) {
				this.txtDesc.setText(descriptionEN);
				return;
			}

			if (!XMSystemUtil.isEmpty(descriptionDE)) {
				this.txtDesc.setText(descriptionDE);
				return;
			}
		}
	}

	/**
	 * Cancel site handler.
	 */
	public void cancelSiteHandler() {
		if (siteModel == null) {
			dirty.setDirty(false);
			return;
		}
		String siteId = CommonConstants.EMPTY_STR;
		int operationMode = this.siteModel.getOperationMode();
		Site oldModel = getOldModel();
		if (oldModel != null) {
			siteId = oldModel.getSiteId();
		}
		setSiteModel(null);
		setOldModel(null);
		this.saveBtn.setEnabled(true);
		dirty.setDirty(false);
		final AdminTreeviewer adminTree = XMAdminUtil.getInstance().getAdminTree();
		final Sites sites = AdminTreeFactory.getInstance().getSites();
		if (operationMode == CommonConstants.OPERATIONMODE.CHANGE) {
			final IStructuredSelection selection = (IStructuredSelection) adminTree.getSelection();
			final Object firstElement = selection.getFirstElement();
			if (firstElement != null && firstElement.equals(sites.getSitesChildren().get(siteId))) {
				adminTree.setSelection(new StructuredSelection(sites.getSitesChildren().get(siteId)), true);
			}
		} else {
			adminTree.setSelection(new StructuredSelection(sites), true);
		}
	}

	/**
	 * set the site model from selection.
	 */
	public void setSite() {
		try {
			final AdminTreeviewer adminTree = XMAdminUtil.getInstance().getAdminTree();
			final Object selectionObj = adminTree.getSelection();
			if (selectionObj instanceof IStructuredSelection) {
				Object firstElement = ((IStructuredSelection) selectionObj).getFirstElement();
				if (firstElement instanceof Site) {
					setOldModel((Site) firstElement);
					Site rightHandObject = this.getOldModel().deepCopySite(false, null);
					setSiteModel(rightHandObject);
					registerMessages(this.registry);
					bindValues();
					setOperationMode();
				}
			}
		} catch (Exception e) {
			LOGGER.warn("Unable to set site model selection ! " + e);
		}
	}

	/**
	 * Sets the model.
	 *
	 * @param site
	 *            the new model
	 */
	public void setModel(Site site) {
		try {
			setOldModel(null);
			setSiteModel(site);
			registerMessages(this.registry);
			bindValues();
			setOperationMode();
			//updateDescWidget();
			//updateRemarkWidget();
		} catch (Exception e) {
			LOGGER.warn("Unable to set site model ! " + e);
		}
	}

}
