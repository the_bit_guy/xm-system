package com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.directory;


import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.stream.Collectors;

import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.userapplicationmodel.UserApplication;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.refs.RelationObj;

public class DirectoryUserApplications implements IAdminTreeChild {
	
	/** The parent. */
	private IAdminTreeChild parent;
	
	/** The directory application children. */
	private Map<String, IAdminTreeChild> directoryUserAppChildren;
	
	/**
	 * Instantiates a new directory user app model.
	 *
	 * @param parent the parent
	 */
	public DirectoryUserApplications(final IAdminTreeChild parent) {
		this.parent = parent;
		this.directoryUserAppChildren = new LinkedHashMap<>();
	}
	
	/**
	 * Adds the.
	 *
	 * @param directoryUserAppsChildrenId the directory user apps children id
	 * @param child the child
	 * @return the i admin tree child
	 */
	public IAdminTreeChild add(final String directoryUserAppsChildrenId, final IAdminTreeChild child) {
		IAdminTreeChild returnVal = this.directoryUserAppChildren.put(directoryUserAppsChildrenId, child);
		if (child instanceof RelationObj && ((RelationObj) child).getRefObject() instanceof UserApplication) {
			sort();
		}
		return returnVal;
	}
	
	/**
	 * Removes the.
	 *
	 * @param directoryUserAppsChildrenId the directory user apps children id
	 * @return the i admin tree child
	 */
	public IAdminTreeChild remove(final String directoryUserAppsChildrenId) {
		return this.directoryUserAppChildren.remove(directoryUserAppsChildrenId);

	}
	
	/**
	 * Gets the directory user apps children collection.
	 *
	 * @return the directory user apps children collection
	 */
	public Collection<IAdminTreeChild> getDirectoryUserAppsChildrenCollection() {
		return this.directoryUserAppChildren.values();
	}
	
	/**
	 * Gets the directory user apps children children.
	 *
	 * @return the directory user apps children children
	 */
	public Map<String, IAdminTreeChild> getDirectoryUserAppsChildrenChildren() {
		return directoryUserAppChildren;
	}
	
	/* (non-Javadoc)
	 * @see com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild#getParent()
	 */
	@Override
	public IAdminTreeChild getParent() {
		// TODO Auto-generated method stub
		return this.parent;
	}

	/* (non-Javadoc)
	 * @see com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild#setParent(com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild)
	 */
	@Override
	public void setParent(IAdminTreeChild parent) {
		this.parent = parent;

		
	}
	
	/**
	 * Sort.
	 */
	public void sort() {
		final LinkedHashMap<String, IAdminTreeChild> collect = this.directoryUserAppChildren.entrySet().stream().sorted(
				(e1, e2) -> ((UserApplication) e1.getValue()).getName().compareTo(((UserApplication) e2.getValue()).getName()))
				.collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e1, LinkedHashMap::new));
		this.directoryUserAppChildren = collect;
	}

}
