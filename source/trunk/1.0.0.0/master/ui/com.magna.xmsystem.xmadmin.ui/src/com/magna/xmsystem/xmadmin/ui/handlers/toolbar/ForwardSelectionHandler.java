package com.magna.xmsystem.xmadmin.ui.handlers.toolbar;

import org.eclipse.e4.core.di.annotations.CanExecute;
import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.jface.viewers.StructuredSelection;

import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.AdminTreeviewer;
import com.magna.xmsystem.xmadmin.util.NavigationStack;
import com.magna.xmsystem.xmadmin.util.XMAdminUtil;

/**
 * The Class ForwardSelectionHandler.
 */
public class ForwardSelectionHandler {
	
	/**
	 * Execute.
	 */
	@Execute
	public void execute() {
		XMAdminUtil instance = XMAdminUtil.getInstance();
		AdminTreeviewer adminTree = instance.getAdminTree();
		NavigationStack<Object> forwardStack = adminTree.getForwardStack();
		if (forwardStack.size() >= 1) {
			Object lastElement = forwardStack.lastElement();
			forwardStack.remove(lastElement);
			adminTree.setSelection(new StructuredSelection(lastElement), true);
		}
	}
	
	@CanExecute
	public boolean canExecute(){
		XMAdminUtil instance = XMAdminUtil.getInstance();
		AdminTreeviewer adminTree = instance.getAdminTree();
		NavigationStack<Object> forwardStack = adminTree.getForwardStack();
		int stackSize = forwardStack.size();	
		if(stackSize <= 0){
			return false;
		}	
		return true;
	}

}
