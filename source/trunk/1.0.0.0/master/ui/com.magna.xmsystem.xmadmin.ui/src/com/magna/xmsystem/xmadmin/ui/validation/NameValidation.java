package com.magna.xmsystem.xmadmin.ui.validation;

import org.eclipse.core.databinding.validation.IValidator;
import org.eclipse.core.databinding.validation.ValidationStatus;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;

import com.magna.xmsystem.xmadmin.message.Message;
import com.magna.xmsystem.xmadmin.util.CommonConstants;

/**
 * Class for Name Validation
 * 
 * @author Deepak Upadhyay
 *
 */
public class NameValidation implements IValidator {

	/** The str value. */
	transient private String strValue;

	/** The messages. */
	transient private Message messages;

	/** The flag. */
	private StatusValidation flag;

	/**
	 * Instantiates a new name validation.
	 *
	 * @param messages
	 *            the messages
	 */
	public NameValidation(Message messages, StatusValidation flag) {
		this.messages = messages;
		this.flag = flag;
	}

	/**
	 * Method to validate.
	 *
	 * @param value
	 *            the value
	 * @return the Istatus
	 */
	@Override
	public IStatus validate(Object value) {
		if (value instanceof String) {
			strValue = (String) value;
			switch (flag) {
				case SITE:
					if (strValue.trim().length() <= 0) {
						return ValidationStatus.error(this.messages.emptyNameValidateErr);
					} else if (strValue.trim().length() > 0
							&& strValue.matches(CommonConstants.RegularExpressions.ALLOWED_SITE_NAME_REGEXP)) {
						return Status.OK_STATUS;
					} else {
						return ValidationStatus.error(this.messages.whitespaceOrSpeCharValidateErr);
					}
	
				case COMMON_NODE_VALIDATE:
					if (strValue.trim().length() <= 0) {
						return ValidationStatus.error(this.messages.emptyCommonNameValidateErr);
					} else if (strValue.trim().length() > 0
							&& strValue.matches(CommonConstants.RegularExpressions.ALLOWED_NAME_REGEX)) {
						return Status.OK_STATUS;
					} else {
						return ValidationStatus.error(this.messages.specialCharValidateErr);
					}
	
				case PROJECT:
					if (strValue.trim().length() <= 0) {
						return ValidationStatus.error(this.messages.emptyNameValidateErr);
					} else if (strValue.trim().length() > 0
							&& strValue.matches(CommonConstants.RegularExpressions.ALLOWED_PROJECT_NAME_REGEX)) {
						return Status.OK_STATUS;
					} else {
						return ValidationStatus.error(this.messages.whitespaceOrSpeCharValidateErr);
					}
				case APPLICATION:
					if (strValue.trim().length() <= 0) {
						return ValidationStatus.error(this.messages.emptyNameValidateErr);
					} else if (strValue.trim().length() > 0
							&& strValue.matches(CommonConstants.RegularExpressions.ALLOWED_APP_NAME_REGEX)) {
						return Status.OK_STATUS;
					} else {
						return ValidationStatus.error(this.messages.whitespaceOrSpeCharValidateErr);
					}
				case DIRECTORY:
					if (strValue.trim().length() <= 0) {
						return ValidationStatus.error(this.messages.emptyCommonNameValidateErr);
					} else if (strValue.trim().length() > 0
							&& strValue.matches(CommonConstants.RegularExpressions.ALLOWED_APP_NAME_REGEX)) {
						return Status.OK_STATUS;
					} else {
						return ValidationStatus.error(this.messages.specialCharValidateErr);
					}
	
				case EMAIL:
					if (strValue.trim().length() <= 0) {
						return ValidationStatus.error(this.messages.emptyEmailError);
	
					} else if (strValue.trim().length() > 0
							&& strValue.matches(CommonConstants.RegularExpressions.EMAIL_REGEX)) {
						return Status.OK_STATUS;
					} else {
						return ValidationStatus.error(this.messages.emailValidateErr);
					}
	
				case CONTACT:
					if (strValue.trim().length() <= 0) {
						return ValidationStatus.error(this.messages.emptyContactError);
					}
					return Status.OK_STATUS;
				case PORT_NUMBER:
					if (strValue.trim().length() <= 0) {
						return ValidationStatus.error(this.messages.emptyPortError);
	
					} else if (strValue.trim().length() > 0
							&& strValue.matches(CommonConstants.RegularExpressions.PORT_NUMBER)) {
						return Status.OK_STATUS;
	
					} else {
						return ValidationStatus.error(this.messages.portValidateErr);
					}
				case URL:
					if (strValue.trim().length() <= 0) {
						return ValidationStatus.error(this.messages.emptyUrlError);
	
					} else if (strValue.trim().length() > 0
							&& strValue.matches(CommonConstants.RegularExpressions.LDAP_URL)) {
						return Status.OK_STATUS;
	
					} else {
						return ValidationStatus.error(this.messages.ldapUrlValidateErr);
					}
				case SMTP_PASSWORD:
					if (strValue.trim().length() <= 0) {
						return ValidationStatus.error(this.messages.emptyPasswordError);
					} else {
						return Status.OK_STATUS;
					}
	
				case SMTP_SERVER_NAME:
					if (strValue.trim().length() <= 0) {
						return ValidationStatus.error(this.messages.emptyServerNameError);
					} else {
						return Status.OK_STATUS;
					}
	
				case LDAP_USERNAME:
					if (strValue.trim().length() <= 0) {
						return ValidationStatus.error(this.messages.emptyldapUserNameError);
					} else {
						return Status.OK_STATUS;
					}
	
				case LDAP_BASE:
					if (strValue.trim().length() <= 0) {
						return ValidationStatus.error(this.messages.emptyBaseNameError);
					} else {
						return Status.OK_STATUS;
					}
				case LIVE_MESSAGE:
					if (strValue.trim().length() <= 0) {
						return ValidationStatus.error(this.messages.emptyBaseNameError);
					} else {
						return Status.OK_STATUS;
					}
	
				default:
					return ValidationStatus.error(this.messages.emptyNameValidateErr);
			}
		} else {
			throw new RuntimeException("Not supposed to be called for non-strings.");
		}
	}
}
