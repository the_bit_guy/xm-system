package com.magna.xmsystem.xmadmin.ui.parts.userprojectrelexpiry;

import javax.inject.Inject;

import org.eclipse.core.databinding.Binding;
import org.eclipse.core.databinding.DataBindingContext;
import org.eclipse.core.databinding.beans.BeanProperties;
import org.eclipse.core.databinding.observable.value.IObservableValue;
import org.eclipse.e4.core.services.nls.Translation;
import org.eclipse.e4.ui.model.application.ui.MDirtyable;
import org.eclipse.jface.databinding.swt.WidgetProperties;
import org.eclipse.jface.fieldassist.ControlDecoration;
import org.eclipse.jface.fieldassist.FieldDecorationRegistry;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.StyleRange;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.magna.xmsystem.xmadmin.message.Message;
import com.magna.xmsystem.xmadmin.message.MessageRegistry;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.AdminTreeviewer;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.notification.UserProExpEvt;
import com.magna.xmsystem.xmadmin.util.CommonConstants;
import com.magna.xmsystem.xmadmin.util.XMAdminUtil;

// TODO: Auto-generated Javadoc

/**
 * The Class UserProjectExpCompositeAction.
 */
public class UserProjectExpCompositeAction extends UserProjectExpCompositeUI {

	/** Logger instance. */
	private static final Logger LOGGER = LoggerFactory.getLogger(UserProjectExpCompositeAction.class);

	/** The user pro exp evt model. */
	private UserProExpEvt userProExpEvtModel;

	/** The old model. */
	private UserProExpEvt oldModel;

	/** Member variable for {@link MessageRegistry}. */
	@Inject
	private MessageRegistry registry;

	/** Member variable for messages. */
	@Inject
	@Translation
	transient private Message messages;

	/** Member variable for widgetValue. */
	transient private IObservableValue<?> widgetValue;

	/** Member variable for modelValue. */
	transient private IObservableValue<?> modelValue;

	/** Member variable for binding. */
	@SuppressWarnings("unused")
	transient private Binding bindValue;

	/** The data bind context. */
	final transient DataBindingContext dataBindContext = new DataBindingContext();

	/** The message info decroator. */
	private ControlDecoration messageInfoDecroator;

	/** The subject info decroator. */
	private ControlDecoration subjectInfoDecroator;

	/** The dirty. */
	private MDirtyable dirty;

	/**
	 * Instantiates a new user project exp composite action.
	 *
	 * @param parent
	 *            the parent
	 */
	@Inject
	public UserProjectExpCompositeAction(final Composite parent) {
		super(parent, SWT.NONE);
	}

	/**
	 * Register messages.
	 *
	 * @param registry
	 *            the registry
	 */
	public void registerMessages(final MessageRegistry registry) {

		registry.register((text) -> {
			if (lblActive != null && !lblActive.isDisposed()) {
				lblActive.setText(text);
			}
		}, (message) -> {
			if (lblActive != null && !lblActive.isDisposed()) {
				return getUpdatedWidgetText(message.objectActiveLabel, lblActive);
			}
			return CommonConstants.EMPTY_STR;
		});
		
		registry.register((text) -> {
			if (managerCCBtn != null && !managerCCBtn.isDisposed()) {
				managerCCBtn.setText(text);
			}
		}, (message) -> {
			if (managerCCBtn != null && !managerCCBtn.isDisposed()) {
				return getUpdatedWidgetText("Add manager in CC", managerCCBtn);
			}
			return CommonConstants.EMPTY_STR;
		});

		registry.register((text) -> {
			if (lblDescription != null && !lblDescription.isDisposed()) {
				lblDescription.setText(text);
			}
		}, (message) -> {
			if (lblDescription != null && !lblDescription.isDisposed()) {
				return getUpdatedWidgetText(message.objectDescriptionLabel, lblDescription);
			}
			return CommonConstants.EMPTY_STR;
		});

		registry.register((text) -> {
			if (lblMessage != null && !lblMessage.isDisposed()) {
				lblMessage.setText(text);
			}
		}, (message) -> {
			if (lblMessage != null && !lblMessage.isDisposed()) {
				return getUpdatedWidgetText(message.notificationMessageLbl, lblMessage);
			}
			return CommonConstants.EMPTY_STR;
		});

		registry.register((text) -> {
			if (lblSubject != null && !lblSubject.isDisposed()) {
				lblSubject.setText(text);
			}
		}, (message) -> {
			if (lblSubject != null && !lblSubject.isDisposed()) {
				return getUpdatedWidgetText(message.notificationSubjectLbl, lblSubject);
			}
			return CommonConstants.EMPTY_STR;
		});

		if (saveBtn != null) {
			registry.register((text) -> {
				if (saveBtn != null && !saveBtn.isDisposed()) {
					saveBtn.setText(text);
				}
			}, (message) -> {
				if (saveBtn != null && !saveBtn.isDisposed()) {
					return getUpdatedWidgetText(message.saveButtonText, saveBtn);
				}
				return CommonConstants.EMPTY_STR;
			});
		}

		if (cancelBtn != null) {
			registry.register((text) -> {
				if (cancelBtn != null && !cancelBtn.isDisposed()) {
					cancelBtn.setText(text);
				}
			}, (message) -> {
				if (cancelBtn != null && !cancelBtn.isDisposed()) {
					return getUpdatedWidgetText(message.cancelButtonText, cancelBtn);
				}
				return CommonConstants.EMPTY_STR;
			});
		}

	}

	/**
	 * Bind values.
	 */
	@SuppressWarnings("unchecked")
	public void bindValues() {

		widgetValue = WidgetProperties.text(SWT.Modify).observe(this.txtSubject);
		modelValue = BeanProperties.value(UserProExpEvt.class, UserProExpEvt.PROPERTY_SUBJECT)
				.observe(this.userProExpEvtModel);
		bindValue = dataBindContext.bindValue(widgetValue, modelValue);

		widgetValue = WidgetProperties.text(SWT.Modify).observe(this.txtMessage);
		modelValue = BeanProperties.value(UserProExpEvt.class, UserProExpEvt.PROPERTY_MESSAGE)
				.observe(this.userProExpEvtModel);
		bindValue = dataBindContext.bindValue(widgetValue, modelValue);

		widgetValue = WidgetProperties.text(SWT.Modify).observe(this.txtDescription);
		modelValue = BeanProperties.value(UserProExpEvt.class, UserProExpEvt.PROPERTY_DESCRIPTION)
				.observe(this.userProExpEvtModel);
		bindValue = dataBindContext.bindValue(widgetValue, modelValue);

		if (messageInfoDecroator == null) {
			messageInfoDecroator = new ControlDecoration(this.txtMessage, SWT.TOP);
			final Image messageInfoDecoratorImage = FieldDecorationRegistry.getDefault()
					.getFieldDecoration(FieldDecorationRegistry.DEC_INFORMATION).getImage();
			messageInfoDecroator.setImage(messageInfoDecoratorImage);
			messageInfoDecroator.setShowOnlyOnFocus(true);
		}
		messageInfoDecroator.hide();

		if (subjectInfoDecroator == null) {
			subjectInfoDecroator = new ControlDecoration(this.txtSubject, SWT.TOP);
			final Image messageInfoDecoratorImage = FieldDecorationRegistry.getDefault()
					.getFieldDecoration(FieldDecorationRegistry.DEC_INFORMATION).getImage();
			subjectInfoDecroator.setImage(messageInfoDecoratorImage);
			subjectInfoDecroator.setShowOnlyOnFocus(true);
		}
		messageInfoDecroator.hide();

		StyleRange style1 = new StyleRange();
		style1.start = 0;
		style1.length = txtDescription.getText().length();
		style1.fontStyle = SWT.ITALIC;
		txtDescription.setStyleRange(style1);
	}

	/**
	 * Gets the updated widget text.
	 *
	 * @param message
	 *            the message
	 * @param control
	 *            the control
	 * @return the updated widget text
	 */
	private String getUpdatedWidgetText(final String message, final Control control) {
		control.requestLayout();
		control.getParent().redraw();
		control.getParent().getParent().update();
		control.getParent().getParent().getParent().update();
		return message;
	}

	/**
	 * Set operation mode.
	 */
	public void setOperationMode() {
		if (this.userProExpEvtModel != null) {
			if (userProExpEvtModel.getOperationMode() == CommonConstants.OPERATIONMODE.VIEW) {
				this.txtDescription.setEditable(false);
				this.activeBtn.setEnabled(false);
				this.txtSubject.setEditable(false);
				this.txtMessage.setData("editable", false);
				setShowButtonBar(false);
			} else if (userProExpEvtModel.getOperationMode() == CommonConstants.OPERATIONMODE.CHANGE) {
				this.txtMessage.setData("editable", true);
				this.activeBtn.setEnabled(true);
				this.txtDescription.setEditable(true);
				this.txtSubject.setEditable(true);
				setShowButtonBar(true);
				this.dirty.setDirty(true);
			} else {
				this.txtDescription.setEditable(false);
				this.activeBtn.setEnabled(false);
				this.txtSubject.setEditable(false);
				this.txtMessage.setData("editable", false);
				setShowButtonBar(false);
			}
		}
	}

	/**
	 * 7 * Sets the dirty object.
	 *
	 * @param dirty
	 *            the new dirty object
	 */
	public void setDirtyObject(final MDirtyable dirty) {
		this.dirty = dirty;
	}

	/**
	 * Set user pro exp evt.
	 */
	public void setUserProExpEvt() {
		try {
			final AdminTreeviewer adminTree = XMAdminUtil.getInstance().getAdminTree();
			final Object selectionObj = adminTree.getSelection();
			if (selectionObj instanceof IStructuredSelection) {
				Object firstElement = ((IStructuredSelection) selectionObj).getFirstElement();
				if (firstElement instanceof UserProExpEvt) {
					setOldModel((UserProExpEvt) firstElement);
					UserProExpEvt rightHandObject = (UserProExpEvt) this.getOldModel()
							.deepCopyUserProExpEvtAction(false, null);
					setUserProExpEvtModel(rightHandObject);
					registerMessages(this.registry);
					bindValues();
					setOperationMode();
				}
			}
		} catch (Exception e) {
			LOGGER.warn("Unable to set userProjectRelationExp event model selection ! " + e);
		}
	}

	/**
	 * Sets the model.
	 *
	 * @param userProExpEvt
	 *            the new model
	 */
	public void setModel(UserProExpEvt userProExpEvt) {
		try {
			setUserProExpEvtModel(userProExpEvt);
			registerMessages(this.registry);
			bindValues();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Gets the user pro exp evt model.
	 *
	 * @return the user pro exp evt model
	 */
	public UserProExpEvt getUserProExpEvtModel() {
		return userProExpEvtModel;
	}

	/**
	 * Sets the user pro exp evt model.
	 *
	 * @param userProExpEvtModel
	 *            the new user pro exp evt model
	 */
	public void setUserProExpEvtModel(UserProExpEvt userProExpEvtModel) {
		this.userProExpEvtModel = userProExpEvtModel;
	}

	/**
	 * Gets the old model.
	 *
	 * @return the old model
	 */
	public UserProExpEvt getOldModel() {
		return oldModel;
	}

	/**
	 * Sets the old model.
	 *
	 * @param oldModel
	 *            the new old model
	 */
	public void setOldModel(UserProExpEvt oldModel) {
		this.oldModel = oldModel;
	}

}
