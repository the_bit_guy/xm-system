package com.magna.xmsystem.xmadmin.ui.parts.projectcreateevt;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import javax.inject.Inject;

import org.eclipse.core.databinding.Binding;
import org.eclipse.core.databinding.DataBindingContext;
import org.eclipse.core.databinding.UpdateValueStrategy;
import org.eclipse.core.databinding.beans.BeanProperties;
import org.eclipse.core.databinding.observable.value.IObservableValue;
import org.eclipse.core.databinding.observable.value.IValueChangeListener;
import org.eclipse.core.databinding.observable.value.ValueChangeEvent;
import org.eclipse.e4.core.services.nls.Translation;
import org.eclipse.e4.ui.model.application.ui.MDirtyable;
import org.eclipse.jface.databinding.fieldassist.ControlDecorationSupport;
import org.eclipse.jface.databinding.swt.WidgetProperties;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.fieldassist.ControlDecoration;
import org.eclipse.jface.fieldassist.FieldDecorationRegistry;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.viewers.TreePath;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.StyleRange;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Text;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.client.ResourceAccessException;

import com.magna.xmbackend.vo.enums.NotificationEventType;
import com.magna.xmbackend.vo.enums.NotificationVariables;
import com.magna.xmbackend.vo.notification.NotificationConfigResponse;
import com.magna.xmsystem.dependencies.customExceptions.UnauthorizedAccessException;
import com.magna.xmsystem.dependencies.utils.XMSystemUtil;
import com.magna.xmsystem.ui.controls.widgets.CustomMessageDialog;
import com.magna.xmsystem.xmadmin.message.Message;
import com.magna.xmsystem.xmadmin.message.MessageRegistry;
import com.magna.xmsystem.xmadmin.restclient.notify.NotificationController;
import com.magna.xmsystem.xmadmin.ui.parts.dnd.ValidateNotificationTemplate;
import com.magna.xmsystem.xmadmin.ui.template.dialog.BrowseTemplateDialog;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.AdminTreeviewer;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.AdminTreeDataLoad;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.AdminTreeFactory;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.notification.NotificationFilterHelper;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.notification.NotificationTemplate;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.notification.ProjectCreateEvt;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.notification.ProjectCreateEvtAction;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.group.usergroup.UserGroupModel;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.group.usergroup.UserGroupUsers;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.refs.RelationObj;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.users.User;
import com.magna.xmsystem.xmadmin.ui.validation.NameValidation;
import com.magna.xmsystem.xmadmin.ui.validation.StatusValidation;
import com.magna.xmsystem.xmadmin.util.CommonConstants;
import com.magna.xmsystem.xmadmin.util.MessageType;
import com.magna.xmsystem.xmadmin.util.XMAdminUtil;

// TODO: Auto-generated Javadoc
/**
 * The Class ProjectCreateEvtCompositeAction.
 */
public class ProjectCreateEvtCompositeAction extends ProjectCreateEvtCompositeUI {

	/** Logger instance. */
	private static final Logger LOGGER = LoggerFactory.getLogger(ProjectCreateEvtCompositeAction.class);

	/** The project create evt act model. */
	private ProjectCreateEvtAction projectCreateEvtActModel;

	/** The old model. */
	private ProjectCreateEvtAction oldModel;

	/** Member variable for {@link MessageRegistry}. */
	@Inject
	private MessageRegistry registry;

	/** Member variable for messages. */
	@Inject
	@Translation
	transient private Message messages;

	/** Member variable for widgetValue. */
	transient private IObservableValue<?> widgetValue;

	/** Member variable for modelValue. */
	transient private IObservableValue<?> modelValue;

	/** Member variable for binding. */
	transient private Binding bindValue;

	/** The data bind context. */
	final transient DataBindingContext dataBindContext = new DataBindingContext();

	/** The dirty. */
	private MDirtyable dirty;

	/** The notification filter helper. */
	private NotificationFilterHelper notificationFilterHelper;
	
	/**
	 * Instantiates a new project create evt composite action.
	 *
	 * @param parent
	 *            the parent
	 */
	@Inject
	public ProjectCreateEvtCompositeAction(final Composite parent) {
		super(parent, SWT.NONE);
		notificationFilterHelper = new NotificationFilterHelper();
		initListner();
	}

	/**
	 * Init listner.
	 */
	private void initListner() {
		if (this.radioBtnUserGroup != null && !this.radioBtnUserGroup.isDisposed()) {
			this.radioBtnUserGroup.addSelectionListener(new SelectionAdapter() {
				@Override
				public void widgetSelected(SelectionEvent event) {

					if (radioBtnUserGroup.getSelection()) {
						expandeUserFilters(false);
						expandeUserGroupFilters(true);
						stackLayout.topControl = userGroupPGroup;
						stackContainer.layout();
						userFilterCombo.select(-1);
					} else {
						expandeUserFilters(true);
						expandeUserGroupFilters(false);
						stackLayout.topControl = userFilerPGroup;
						stackContainer.layout();
						userGroupFilterCombo.select(-1);
					}
				}
			});
		}

		if (this.toolItemTemplate != null && !this.toolItemTemplate.isDisposed()) {
			this.toolItemTemplate.addSelectionListener(new SelectionAdapter() {

				@Override
				public void widgetSelected(SelectionEvent event) {
					BrowseTemplateDialog dialog = new BrowseTemplateDialog(
							toolItemTemplate.getDisplay().getActiveShell(), messages);
					if (IDialogConstants.OK_ID == dialog.open()) {
						NotificationTemplate selectedTemplate = dialog.getSelectedTemplate();
						if (ValidateNotificationTemplate.validateTemplate(selectedTemplate,
								projectCreateEvtActModel)) {
							txtTemplate.setText(selectedTemplate.getName());
							projectCreateEvtActModel.setTemplate(selectedTemplate);
						} else {
							CustomMessageDialog.openError(toolItemTemplate.getDisplay().getActiveShell(), messages.errorDialogTitile,
									messages.notiInvalidTemplateMsg);
						}

					}
				}
			});
		}

		if (this.saveBtn != null && !this.saveBtn.isDisposed()) {
			this.saveBtn.addSelectionListener(new SelectionAdapter() {

				@Override
				public void widgetSelected(SelectionEvent e) {
					saveProCreateActionHandler();
				}

			});
		}

		if (this.cancelBtn != null) {
			this.cancelBtn.addSelectionListener(new SelectionAdapter() {

				/**
				 * Cancel button handler
				 */
				@Override
				public void widgetSelected(final SelectionEvent event) {
					cancelProCreateEvtActionHandler();
				}
			});
		}
		
		if (this.txtToUsers != null) {
			this.txtToUsers.addModifyListener(new ModifyListener() {

				@Override
				public void modifyText(ModifyEvent paramModifyEvent) {

					List<String> list = new ArrayList<String>();
					final String strValue = txtToUsers.getText();
					list = Arrays.asList(strValue.split(";"));

					final List<StyleRange> styleRangeList = new ArrayList<>();
					final List<Integer> rangeList = new ArrayList<>();
					int fromIndex = 0;
					for (String link : list) {
						final int indexOf = strValue.indexOf(link, fromIndex);
						rangeList.add(indexOf);
						final int length = indexOf + link.length();
						fromIndex = length;
						rangeList.add(length);

						final StyleRange style = new StyleRange();
						style.underline = true;
						style.underlineStyle = SWT.UNDERLINE_LINK;
						style.start = indexOf;
						style.length = link.length();
						styleRangeList.add(style);
					}
					txtToUsers.setStyleRanges(styleRangeList.toArray(new StyleRange[styleRangeList.size()]));
				}
			});
		}

		if (this.txtCCUsers != null) {
			this.txtCCUsers.addModifyListener(new ModifyListener() {

				@Override
				public void modifyText(ModifyEvent paramModifyEvent) {

					List<String> list = new ArrayList<String>();
					final String strValue = txtCCUsers.getText();
					list = Arrays.asList(strValue.split(";"));

					final List<StyleRange> styleRangeList = new ArrayList<>();
					final List<Integer> rangeList = new ArrayList<>();
					int fromIndex = 0;
					for (String link : list) {
						final int indexOf = strValue.indexOf(link, fromIndex);
						rangeList.add(indexOf);
						final int length = indexOf + link.length();
						fromIndex = length;
						rangeList.add(length);

						final StyleRange style = new StyleRange();
						style.underline = true;
						style.underlineStyle = SWT.UNDERLINE_LINK;
						style.start = indexOf;
						style.length = link.length();
						styleRangeList.add(style);
					}
					txtCCUsers.setStyleRanges(styleRangeList.toArray(new StyleRange[styleRangeList.size()]));
				}
			});
		}

		if (this.addtoToBtn != null && !this.addtoToBtn.isDisposed()) {
			this.addtoToBtn.addSelectionListener(new SelectionAdapter() {
				@Override
				public void widgetSelected(SelectionEvent paramSelectionEvent) {

					StringBuilder users = new StringBuilder();
					users.append(txtToUsers.getText());
					Set<String> userNameList = new HashSet<>(Arrays.asList(users.toString().split(";")));
					if (userFilterCombo.getSelectionIndex() >= 0) {
						String userName = userFilterCombo.getItem(userFilterCombo.getSelectionIndex());
						if (!userNameList.contains(userName)) {
							txtToUsers.append(userName);
							txtToUsers.append(";");
						} else {
							CustomMessageDialog.openError(getShell(), messages.notificationUserErrorTitle,
									messages.notificationUserErrorMsg);
						}
						userFilterCombo.select(-1);
					} else if (userGroupFilterCombo.getSelectionIndex() >= 0) {
						Object data = userGroupFilterCombo.getTable().getItem(userGroupFilterCombo.getSelectionIndex())
								.getData();
						if (data instanceof UserGroupModel) {
							UserGroupModel userGroupModel = (UserGroupModel) data;
							UserGroupUsers userGroupUsers = (UserGroupUsers) userGroupModel.getUserGroupChildren()
									.get(UserGroupUsers.class.getSimpleName());
							List<IAdminTreeChild> values = AdminTreeDataLoad.getInstance()
									.loadUserGroupUsersFromService(userGroupUsers);
							for (IAdminTreeChild iAdminTreeChild : values) {
								if (iAdminTreeChild instanceof RelationObj) {
									RelationObj relObj = (RelationObj) iAdminTreeChild;
									IAdminTreeChild refObject = relObj.getRefObject();
									if (refObject instanceof User) {
										User user = (User) refObject;
										txtToUsers.append(user.getName());
										txtToUsers.append(";");
									}
								}
							}
						}
						userGroupFilterCombo.select(-1);
					}

				}
			});
		}

		if (this.addtoCCBtn != null && !this.addtoCCBtn.isDisposed()) {
			this.addtoCCBtn.addSelectionListener(new SelectionAdapter() {
				@Override
				public void widgetSelected(SelectionEvent paramSelectionEvent) {
					StringBuilder users = new StringBuilder();
					users.append(txtCCUsers.getText());
					Set<String> userNameList = new HashSet<>(Arrays.asList(users.toString().split(";")));
					if (userFilterCombo.getSelectionIndex() >= 0) {
						String userName = userFilterCombo.getItem(userFilterCombo.getSelectionIndex());
						if (!userNameList.contains(userName)) {
							txtCCUsers.append(userName);
							txtCCUsers.append(";");
						} else {
							CustomMessageDialog.openError(getShell(), messages.notificationUserErrorTitle,
									messages.notificationUserErrorMsg);
						}
						userFilterCombo.select(-1);
					} else if (userGroupFilterCombo.getSelectionIndex() >= 0) {
						Object data = userGroupFilterCombo.getTable().getItem(userGroupFilterCombo.getSelectionIndex())
								.getData();
						if (data instanceof UserGroupModel) {
							UserGroupModel userGroupModel = (UserGroupModel) data;
							UserGroupUsers userGroupUsers = (UserGroupUsers) userGroupModel.getUserGroupChildren()
									.get(UserGroupUsers.class.getSimpleName());
							List<IAdminTreeChild> values = AdminTreeDataLoad.getInstance()
									.loadUserGroupUsersFromService(userGroupUsers);
							for (IAdminTreeChild iAdminTreeChild : values) {
								if (iAdminTreeChild instanceof RelationObj) {
									RelationObj relObj = (RelationObj) iAdminTreeChild;
									IAdminTreeChild refObject = relObj.getRefObject();
									if (refObject instanceof User) {
										User user = (User) refObject;
										txtCCUsers.append(user.getName());
										txtCCUsers.append(";");
									}
								}
							}
						}
						userGroupFilterCombo.select(-1);
					}
				}
			});
		}

		this.userFilterCombo.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent paramSelectionEvent) {
				addtoToBtn.getParent().forceFocus();
			}
		});

		this.userFilterText.addListener(SWT.FocusOut, new Listener() {

			@Override
			public void handleEvent(final Event event) {
				final String filterText = ((Text) event.widget).getText();
				notificationFilterHelper.filterCombo(filterText,
						notificationFilterHelper.getUsersComboItems(notificationFilterHelper.getUsersObjets()),
						userFilterCombo);
			}
		});

		this.userGroupFilterText.addListener(SWT.FocusOut, new Listener() {

			@Override
			public void handleEvent(final Event event) {
				final String filterText = ((Text) event.widget).getText();
				userGroupFilterCombo.select(-1);
				notificationFilterHelper.filterCombo(filterText,
						notificationFilterHelper.getUserGroupComboItems(notificationFilterHelper.getUserGroupObjects()),
						userGroupFilterCombo);

			}
		});

		this.userFilterButton.addListener(SWT.Selection, new Listener() {
			/**
			 * Overrides handleEvent method for show All users button selection
			 */
			@Override
			public void handleEvent(final Event event) {
				userFilterText.setText(CommonConstants.EMPTY_STR);
				notificationFilterHelper.setItemsWithImages(
						notificationFilterHelper.getUsersComboItems(notificationFilterHelper.getUsersObjets()),
						userFilterCombo);
				userFilterCombo.select(-1);
			}
		});
		this.userGroupFilterButton.addListener(SWT.Selection, new Listener() {
			/**
			 * Overrides handleEvent method for show All users button selection
			 */
			@Override
			public void handleEvent(final Event event) {
				userGroupFilterText.setText(CommonConstants.EMPTY_STR);
				notificationFilterHelper.setItemsWithImages(
						notificationFilterHelper.getUserGroupComboItems(notificationFilterHelper.getUserGroupObjects()),
						userGroupFilterCombo);
				userGroupFilterCombo.select(-1);
			}
		});
		
		this.txtTemplate.addModifyListener(new ModifyListener() {
			ControlDecoration decroator = new ControlDecoration(txtTemplate, SWT.TOP);

			@Override
			public void modifyText(ModifyEvent event) {
				final Image nameDecoratorImage = FieldDecorationRegistry.getDefault()
						.getFieldDecoration(FieldDecorationRegistry.DEC_ERROR).getImage();
				decroator.setImage(nameDecoratorImage);
				Text text = (Text) event.widget;
				if (text.getText().trim().length() <= 0) {
					decroator.setDescriptionText(messages.notiEmptyTemplateError);
					decroator.show();
				} else if (text.getText().trim().length() > 0) {
					decroator.hide();
				}
			}
		});
	}

	/**
	 * Validate variables.
	 *
	 * @param message
	 *            the message
	 * @return true, if successful
	 */
	protected boolean validateVariables(String message) {

		String endParanthPattern = "\\%";
		String startParanthPattern = "\\%";

		Pattern startPattern = Pattern.compile(startParanthPattern);
		Pattern endPattern = Pattern.compile(endParanthPattern);
		Matcher matcher = startPattern.matcher(message);
		int startPatternCount = 0;
		int endPatternCount = 0;
		while (matcher.find()) {
			startPatternCount++;
		}

		matcher = endPattern.matcher(message);
		while (matcher.find()) {
			endPatternCount++;
		}
		if (startPatternCount != endPatternCount) {
			return false;
		}
		List<String> varList = new ArrayList<>();
		TreeSet<String> matchedVarList = new TreeSet<>();
		if (startPatternCount == endPatternCount) {
			String varPatternStr = "\\%.*?\\%";
			Pattern varPattern = Pattern.compile(varPatternStr);
			matcher = varPattern.matcher(message);
			while (matcher.find()) {
				varList.add(message.substring(matcher.start(), matcher.end()));
			}
			int count = 0;
			for (String var : varList) {
				if (var.matches(NotificationVariables.ASSIGNED_USER.toString()) || var.matches(NotificationVariables.MANAGER.toString())) {
					matchedVarList.add(var);
					count++;
				}
			}
			if (count != varList.size()) {
				return false;
			}
		}
		return true;

	}

	/**
	 * Expande user group filters.
	 *
	 * @param isExpand
	 *            the is expand
	 */
	private void expandeUserGroupFilters(boolean isExpand) {
		userGroupPGroup.setExpanded(isExpand);
		pGroupRepaint(userGroupPGroup);
	}

	/**
	 * Expande user filters.
	 *
	 * @param isExpand
	 *            the is expand
	 */
	private void expandeUserFilters(boolean isExpand) {
		userFilerPGroup.setExpanded(isExpand);
		pGroupRepaint(userFilerPGroup);
	}

	/**
	 * Register messages.
	 *
	 * @param registry
	 *            the registry
	 */
	public void registerMessages(final MessageRegistry registry) {

		registry.register((text) -> {
			if (lblName != null && !lblName.isDisposed()) {
				lblName.setText(text);
			}
		}, (message) -> {
			if (lblName != null && !lblName.isDisposed()) {
				return getUpdatedWidgetText(message.objectNameLabel, lblName);
			}
			return CommonConstants.EMPTY_STR;
		});

		registry.register((text) -> {
			if (lblDescription != null && !lblDescription.isDisposed()) {
				lblDescription.setText(text);
			}
		}, (message) -> {
			if (lblDescription != null && !lblDescription.isDisposed()) {
				return getUpdatedWidgetText(message.objectDescriptionLabel, lblDescription);
			}
			return CommonConstants.EMPTY_STR;
		});
		registry.register((text) -> {
			if (lblActive != null && !lblActive.isDisposed()) {
				lblActive.setText(text);
			}
		}, (message) -> {
			if (lblActive != null && !lblActive.isDisposed()) {
				return getUpdatedWidgetText(message.objectActiveLabel, lblActive);
			}
			return CommonConstants.EMPTY_STR;
		});

		registry.register((text) -> {
			if (addtoToBtn != null && !addtoToBtn.isDisposed()) {
				addtoToBtn.setText(text);
			}
		}, (message) -> {
			if (addtoToBtn != null && !addtoToBtn.isDisposed()) {
				return getUpdatedWidgetText(message.notiAddToBtn, addtoToBtn);
			}
			return CommonConstants.EMPTY_STR;
		});

		registry.register((text) -> {
			if (addtoCCBtn != null && !addtoCCBtn.isDisposed()) {
				addtoCCBtn.setText(text);
			}
		}, (message) -> {
			if (addtoCCBtn != null && !addtoCCBtn.isDisposed()) {
				return getUpdatedWidgetText(message.notiAddToCCBtn, addtoCCBtn);
			}
			return CommonConstants.EMPTY_STR;
		});

		registry.register((text) -> {
			if (lblToUsers != null && !lblToUsers.isDisposed()) {
				lblToUsers.setText(text);
			}
		}, (message) -> {
			if (lblToUsers != null && !lblToUsers.isDisposed()) {
				return getUpdatedWidgetText(message.notificationToUserLbl, lblToUsers);
			}
			return CommonConstants.EMPTY_STR;
		});

		registry.register((text) -> {
			if (lblCCUsers != null && !lblCCUsers.isDisposed()) {
				lblCCUsers.setText(text);
			}
		}, (message) -> {
			if (lblCCUsers != null && !lblCCUsers.isDisposed()) {
				return getUpdatedWidgetText(message.notificationCCLbl, lblCCUsers);
			}
			return CommonConstants.EMPTY_STR;
		});

		registry.register((text) -> {
			if (lblTemplate != null && !lblTemplate.isDisposed()) {
				lblTemplate.setText(text);
			}
		}, (message) -> {
			if (lblTemplate != null && !lblTemplate.isDisposed()) {
				return getUpdatedWidgetText(message.notiTemplateLbl, lblTemplate);
			}
			return CommonConstants.EMPTY_STR;
		});

		registry.register((text) -> {
			if (userFilerPGroup != null && !userFilerPGroup.isDisposed()) {
				userFilerPGroup.setText(text);
			}
		}, (message) -> {
			if (userFilerPGroup != null && !userFilerPGroup.isDisposed()) {
				return getUpdatedWidgetText(message.userFilterLabel, userFilerPGroup);
			}
			return CommonConstants.EMPTY_STR;
		});

		registry.register((text) -> {
			if (userGroupPGroup != null && !userGroupPGroup.isDisposed()) {
				userGroupPGroup.setText(text);
			}
		}, (message) -> {
			if (userGroupPGroup != null && !userGroupPGroup.isDisposed()) {
				return getUpdatedWidgetText(message.userGroupFilterLabel, userGroupPGroup);
			}
			return CommonConstants.EMPTY_STR;
		});

		registry.register((text) -> {
			if (radioBtnUser != null && !radioBtnUser.isDisposed()) {
				radioBtnUser.setText(text);
			}
		}, (message) -> {
			if (radioBtnUser != null && !radioBtnUser.isDisposed()) {
				return getUpdatedWidgetText(message.notiUserFilteRadioBtnLbl, radioBtnUser);
			}
			return CommonConstants.EMPTY_STR;
		});

		registry.register((text) -> {
			if (radioBtnUserGroup != null && !radioBtnUserGroup.isDisposed()) {
				radioBtnUserGroup.setText(text);
			}
		}, (message) -> {
			if (radioBtnUserGroup != null && !radioBtnUserGroup.isDisposed()) {
				return getUpdatedWidgetText(message.notiUserGrpFilteRadioBtnLbl, radioBtnUserGroup);
			}
			return CommonConstants.EMPTY_STR;
		});

		if (saveBtn != null) {
			registry.register((text) -> {
				if (saveBtn != null && !saveBtn.isDisposed()) {
					saveBtn.setText(text);
				}
			}, (message) -> {
				if (saveBtn != null && !saveBtn.isDisposed()) {
					return getUpdatedWidgetText(message.saveButtonText, saveBtn);
				}
				return CommonConstants.EMPTY_STR;
			});
		}

		if (cancelBtn != null) {
			registry.register((text) -> {
				if (cancelBtn != null && !cancelBtn.isDisposed()) {
					cancelBtn.setText(text);
				}
			}, (message) -> {
				if (cancelBtn != null && !cancelBtn.isDisposed()) {
					return getUpdatedWidgetText(message.cancelButtonText, cancelBtn);
				}
				return CommonConstants.EMPTY_STR;
			});
		}

		registry.register((text) -> {
			if (userFilterButton != null && !userFilterButton.isDisposed()) {
				userFilterButton.setText(text);
			}
		}, (message) -> {
			if (userFilterButton != null && !userFilterButton.isDisposed()) {
				return getUpdatedWidgetText(message.allBtnLabel, userFilterButton);
			}
			return CommonConstants.EMPTY_STR;
		});

		registry.register((text) -> {
			if (userGroupFilterButton != null && !userGroupFilterButton.isDisposed()) {
				userGroupFilterButton.setText(text);
			}
		}, (message) -> {
			if (userGroupFilterButton != null && !userGroupFilterButton.isDisposed()) {
				return getUpdatedWidgetText(message.allBtnLabel, userGroupFilterButton);
			}
			return CommonConstants.EMPTY_STR;
		});

		initlizeFilterCombo();

	}

	/**
	 * Initlize filter combo.
	 */
	private void initlizeFilterCombo() {
		List<User> usersObjets = this.notificationFilterHelper.getUsersObjets();
		this.notificationFilterHelper.setItemsWithImages(this.notificationFilterHelper.getUsersComboItems(usersObjets),
				this.userFilterCombo);

		List<UserGroupModel> userGroupObjets = this.notificationFilterHelper.getUserGroupObjects();
		this.notificationFilterHelper.setItemsWithImages(
				this.notificationFilterHelper.getUserGroupComboItems(userGroupObjets), this.userGroupFilterCombo);
	}

	/**
	 * Bind values.
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public void bindValues() {

		widgetValue = WidgetProperties.text(SWT.Modify).observe(this.txtName);
		modelValue = BeanProperties.value(ProjectCreateEvtAction.class, ProjectCreateEvtAction.PROPERTY_NAME)
				.observe(this.projectCreateEvtActModel);
		widgetValue.addValueChangeListener(new IValueChangeListener() {
			/**
			 * handler to update button status
			 */
			@Override
			public void handleValueChange(final ValueChangeEvent event) {
				updateButtonStatus(event);
			}
		});

		// define the UpdateValueStrategy
		final UpdateValueStrategy update = new UpdateValueStrategy();
		update.setAfterGetValidator(new NameValidation(messages, StatusValidation.SITE));
		bindValue = dataBindContext.bindValue(widgetValue, modelValue, update, null);
		ControlDecorationSupport.create(bindValue, SWT.TOP | SWT.LEFT);

		widgetValue = WidgetProperties.selection().observe(this.activeBtn);
		modelValue = BeanProperties.value(ProjectCreateEvtAction.class, ProjectCreateEvtAction.PROPERTY_ACTIVE)
				.observe(this.projectCreateEvtActModel);
		bindValue = dataBindContext.bindValue(widgetValue, modelValue);

		widgetValue = WidgetProperties.text(SWT.Modify).observe(this.txtDescription);
		modelValue = BeanProperties.value(ProjectCreateEvtAction.class, ProjectCreateEvtAction.PROPERTY_DESCRIPTION)
				.observe(this.projectCreateEvtActModel);
		bindValue = dataBindContext.bindValue(widgetValue, modelValue);

	}

	
	@SuppressWarnings("rawtypes")
	private void updateButtonStatus(final ValueChangeEvent event) {
		final String name = (String) event.getObservableValue().getValue();
		if (this.saveBtn != null) {
			if (XMSystemUtil.isEmpty(name) || name.trim().length() == 0
					|| (!name.matches(CommonConstants.RegularExpressions.ALLOWED_NAME_REGEX))) {
				this.saveBtn.setEnabled(false);
			} else {
				this.saveBtn.setEnabled(true);
			}
		}
	}
	
	/**
	 * Save pro create action handler.
	 */
	public void saveProCreateActionHandler() {
		if (validateAndSaveUsers() && validate()) {
			if (!projectCreateEvtActModel.getName().isEmpty()) {
				if (projectCreateEvtActModel.getOperationMode() == CommonConstants.OPERATIONMODE.CREATE) {
					createProCreateEvtOperation();
				} else if (projectCreateEvtActModel.getOperationMode() == CommonConstants.OPERATIONMODE.CHANGE) {
					changeProCreateEvtOperation();
				}
			}
		}
	}

	/**
	 * Update template.
	 */
	private void updateTemplate() {
		if (this.projectCreateEvtActModel == null) {
			return;
		}
		NotificationTemplate template = projectCreateEvtActModel.getTemplate();
		if(template != null){
		txtTemplate.setText(template.getName() == null?CommonConstants.EMPTY_STR:template.getName());
		}else{
			txtTemplate.setText(CommonConstants.EMPTY_STR);
		}
	}

	/**
	 * Update to users.
	 *
	 * @param toUsersList
	 *            the to users list
	 */
	private void updateToUsers(final Set<String> toUsersList) {
		StringBuilder users = new StringBuilder();
		for (String userName : toUsersList) {
			if (users.length() > 0) {
				users.append(';').append(userName);
			} else {
				users.append(userName);
			}
		}
		this.txtToUsers.setText(users.toString());
	}

	/**
	 * Update CC users.
	 *
	 * @param ccUsersList
	 *            the cc users list
	 */
	private void updateCCUsers(final Set<String> ccUsersList) {
		StringBuilder users = new StringBuilder();
		for (String userName : ccUsersList) {
			if (users.length() > 0) {
				users.append(';').append(userName);
			} else {
				users.append(userName);
			}
		}
		this.txtCCUsers.setText(users.toString());
	}

	/**
	 * Validate.
	 *
	 * @return true, if successful
	 */
	private boolean validate() {
		final String name = this.projectCreateEvtActModel.getName();
		final ProjectCreateEvt projectCreateEvt = AdminTreeFactory.getInstance().getNotifications()
				.getProjectCreateEvt();
		final Collection<IAdminTreeChild> projectCreateEvtCollection = projectCreateEvt.getProjectCreateEvtCollection();
		if (projectCreateEvtActModel.getOperationMode() == CommonConstants.OPERATIONMODE.CHANGE) {
			if (!name.equalsIgnoreCase(this.oldModel.getName())) {
				final Map<String, Long> result = projectCreateEvtCollection.parallelStream().collect(
						Collectors.groupingBy(event -> ((ProjectCreateEvtAction) event).getName().toUpperCase(),
								Collectors.counting()));
				if (result.containsKey(name.toUpperCase())) {
					CustomMessageDialog.openError(this.getShell(), messages.errorDialogTitile, messages.existingNotificationNameError);
					return false;
				}
			}
		} else if (this.projectCreateEvtActModel.getOperationMode() == CommonConstants.OPERATIONMODE.CREATE) {
			for (final IAdminTreeChild event : projectCreateEvtCollection) {
				if (name.equalsIgnoreCase(((ProjectCreateEvtAction) event).getName())) {
					CustomMessageDialog.openError(this.getShell(),messages.errorDialogTitile, messages.existingNotificationNameError);
					return false;
				}
			}
		}
		if (XMSystemUtil.isEmpty(name)) {
			CustomMessageDialog.openError(this.getShell(), messages.nameErrorTitle, messages.nameError);
			return false;
		}
		
		if (XMSystemUtil.isEmpty(this.txtTemplate.getText())) {
			CustomMessageDialog.openError(this.getShell(), messages.errorDialogTitile,
					messages.notiEmptyTemplateError);
			return false;
		}
		return true;
	}

	/**
	 * Validate and save users.
	 *
	 * @return true, if successful
	 */
	private boolean validateAndSaveUsers() {
		if(!txtToUsers.getText().toString().trim().isEmpty()){
			Set<String> userNameList = new TreeSet<>(Arrays.asList(txtToUsers.getText().toString().split(";")));
			for (String user : userNameList) {
				if ("%".contains(user) && !validateVariables(user)) {
					CustomMessageDialog.openError(addtoToBtn.getDisplay().getActiveShell(),messages.errorDialogTitile,
							messages.notiInvalidToVariableMsg);
					return false;
				}
			}
			projectCreateEvtActModel.setToUsers(userNameList);
		}
		
		
		if(!txtToUsers.getText().toString().trim().isEmpty()){
			Set<String> ccUserNameList = new TreeSet<>(Arrays.asList(txtCCUsers.getText().toString().split(";")));
			for (String user : ccUserNameList) {
				if ("%".contains(user) && !validateVariables(user)) {
					CustomMessageDialog.openError(addtoCCBtn.getDisplay().getActiveShell(), messages.errorDialogTitile,
							messages.notiInvalidCCVariableMsg);
					return false;
				}
			}
			projectCreateEvtActModel.setCcUsers(ccUserNameList);
		}
		
		return true;
	}

	/**
	 * Change pro create evt operation.
	 */
	private void changeProCreateEvtOperation() {
		try {
			NotificationController notificationController = new NotificationController();
			NotificationConfigResponse response = notificationController.updateNotification(mapVOObjectWithModel());
			if (response != null) {
				setOldModel(this.projectCreateEvtActModel.deepCopyProCreateEvtAction(true, getOldModel()));
				this.projectCreateEvtActModel.setOperationMode(CommonConstants.OPERATIONMODE.VIEW);
				setOperationMode();
				this.dirty.setDirty(false);
				final ProjectCreateEvt projectCreateEvt = AdminTreeFactory.getInstance().getNotifications()
						.getProjectCreateEvt();
				projectCreateEvt.sort();
				XMAdminUtil.getInstance().getAdminTree().refresh(true);
				XMAdminUtil.getInstance().getAdminTree().setSelection(new StructuredSelection(getOldModel()), true);
				XMAdminUtil.getInstance().updateLogFile(NotificationEventType.PROJECT_CREATE.name()+" "+messages.actionLbl+ " '"
						+ this.projectCreateEvtActModel.getName() + "' " + messages.objectUpdate,
						MessageType.SUCCESS);
			}
		} catch (UnauthorizedAccessException e) {
			CustomMessageDialog.openError(this.getShell(), messages.objectPermissionDialogTitle,
					messages.objectPermissionDialogMsg);
		} catch (Exception e) {
			if (e instanceof ResourceAccessException) {
				CustomMessageDialog.openError(Display.getCurrent().getActiveShell(), messages.errorDialogTitile,
						messages.serverNotReachable);
			}
			LOGGER.warn("Unable to Update projectCreate event  data ! " + e);
		}

	}

	/**
	 * Create pro create evt operation.
	 */
	private void createProCreateEvtOperation() {
		try {
			NotificationController notificationController = new NotificationController();
			NotificationConfigResponse response = notificationController.createNotification(mapVOObjectWithModel());
			if (response != null) {
				String id = response.getNotifConfigId();
				if (!XMSystemUtil.isEmpty(id)) {
					this.projectCreateEvtActModel.setId(id);
					AdminTreeFactory instance = AdminTreeFactory.getInstance();
					if (oldModel == null) { // Attach this to tree
						setOldModel(projectCreateEvtActModel.deepCopyProCreateEvtAction(false, null));
						instance.getNotifications().getProjectCreateEvt().add(id, getOldModel());
					}
					this.dirty.setDirty(false);
					AdminTreeviewer adminTree = XMAdminUtil.getInstance().getAdminTree();
					adminTree.refresh(true);

					adminTree.setSelection(new StructuredSelection(instance.getNotifications()), true);
					TreePath[] selectionPaths = adminTree.getStructuredSelection().getPaths();
					if (selectionPaths != null && selectionPaths.length > 0) {
						adminTree.setExpandedState(selectionPaths[0], true);
					}
					
					adminTree.setSelection(new StructuredSelection(instance.getNotifications().getProjectCreateEvt()),
							true);
					selectionPaths = adminTree.getStructuredSelection().getPaths();
					if (selectionPaths != null && selectionPaths.length > 0) {
						adminTree.setExpandedState(selectionPaths[0], true);
					}
					adminTree.setSelection(new StructuredSelection(getOldModel()), true);
				}
				XMAdminUtil.getInstance().updateLogFile(
						NotificationEventType.PROJECT_CREATE.name() + " " + messages.actionLbl + " '"
								+ this.projectCreateEvtActModel.getName() + "' " + messages.objectCreate,
						MessageType.SUCCESS);
			}
		} catch (UnauthorizedAccessException e) {
			CustomMessageDialog.openError(this.getShell(), messages.objectPermissionDialogTitle,
					messages.objectPermissionDialogMsg);
		} catch (Exception e) {
			if (e instanceof ResourceAccessException) {
				CustomMessageDialog.openError(Display.getCurrent().getActiveShell(), messages.errorDialogTitile,
						messages.serverNotReachable);
			}
			LOGGER.warn("Unable to Save data ! " + e);
		}

	}
	
	/**
	 * Cancel pro create evt action handler.
	 */
	public void cancelProCreateEvtActionHandler() {
		if (this.projectCreateEvtActModel == null) {
			dirty.setDirty(false);
			return;
		}
		String id = CommonConstants.EMPTY_STR;
		int operationMode = this.projectCreateEvtActModel.getOperationMode();
		ProjectCreateEvtAction oldModel = getOldModel();
		if (oldModel != null) {
			id = oldModel.getId();
		}
		setProjectCreateEvtActModel(null);
		setOldModel(null);
		this.saveBtn.setEnabled(true);
		dirty.setDirty(false);
		final AdminTreeviewer adminTree = XMAdminUtil.getInstance().getAdminTree();
		final ProjectCreateEvt events = AdminTreeFactory.getInstance().getNotifications().getProjectCreateEvt();
		if (operationMode == CommonConstants.OPERATIONMODE.CHANGE) {
			final IStructuredSelection selection = (IStructuredSelection) adminTree.getSelection();
			final Object firstElement = selection.getFirstElement();
			if (firstElement != null && firstElement.equals(events.getProjectCreateEvtChild().get(id))) {
				adminTree.setSelection(new StructuredSelection(events.getProjectCreateEvtChild().get(id)), true);
			}
		} else {
			adminTree.setSelection(new StructuredSelection(events), true);
		}
	}

	/**
	 * Map VO object with model.
	 *
	 * @return the com.magna.xmbackend.vo.notification. notification request
	 */
	private com.magna.xmbackend.vo.notification.NotificationRequest mapVOObjectWithModel() {
		com.magna.xmbackend.vo.notification.NotificationRequest notificationRequest = new com.magna.xmbackend.vo.notification.NotificationRequest();
		notificationRequest.setNotificationConfigId(this.projectCreateEvtActModel.getId());
		notificationRequest.setEmailTemplateId(this.projectCreateEvtActModel.getTemplate().getTemplateId());
		notificationRequest.setNotificationConfigActionName(this.projectCreateEvtActModel.getName());
		notificationRequest.setNotificationEventId(AdminTreeFactory.getInstance().getNotifications().getProjectCreateEvt().getId());
		notificationRequest.setNotificationActionStatus(this.projectCreateEvtActModel.isActive() == true
				? com.magna.xmbackend.vo.enums.Status.ACTIVE : com.magna.xmbackend.vo.enums.Status.INACTIVE);
		notificationRequest.setNotificationEventStatus(
				AdminTreeFactory.getInstance().getNotifications().getProjectCreateEvt().isActive() == true
						? com.magna.xmbackend.vo.enums.Status.ACTIVE : com.magna.xmbackend.vo.enums.Status.INACTIVE);
		notificationRequest.setCcUsersToNotify(new ArrayList<>(this.projectCreateEvtActModel.getCcUsers()));
		notificationRequest.setUsersToNotify(new ArrayList<>(this.projectCreateEvtActModel.getToUsers()));
		return notificationRequest;
	}

	/**
	 * Gets the updated widget text.
	 *
	 * @param message
	 *            the message
	 * @param control
	 *            the control
	 * @return the updated widget text
	 */
	private String getUpdatedWidgetText(final String message, final Control control) {
		control.requestLayout();
		control.getParent().redraw();
		control.getParent().getParent().update();
		control.getParent().getParent().getParent().update();
		return message;
	}

	/**
	 * Gets the project create evt act model.
	 *
	 * @return the project create evt act model
	 */
	public ProjectCreateEvtAction getProjectCreateEvtActModel() {
		return projectCreateEvtActModel;
	}

	/**
	 * Sets the project create evt act model.
	 *
	 * @param projectCreateEvtActModel
	 *            the new project create evt act model
	 */
	public void setProjectCreateEvtActModel(ProjectCreateEvtAction projectCreateEvtActModel) {
		this.projectCreateEvtActModel = projectCreateEvtActModel;
	}

	/**
	 * Gets the old model.
	 *
	 * @return the old model
	 */
	public ProjectCreateEvtAction getOldModel() {
		return oldModel;
	}

	/**
	 * Sets the old model.
	 *
	 * @param oldModel
	 *            the new old model
	 */
	public void setOldModel(ProjectCreateEvtAction oldModel) {
		this.oldModel = oldModel;
	}

	/**
	 * Set operation mode.
	 */
	public void setOperationMode() {
		if (this.projectCreateEvtActModel != null) {
			if (projectCreateEvtActModel.getOperationMode() == CommonConstants.OPERATIONMODE.VIEW) {
				this.txtName.setEditable(false);
				this.activeBtn.setEnabled(false);
				this.txtDescription.setEditable(false);
				setShowButtonBar(false);
			} else if (projectCreateEvtActModel.getOperationMode() == CommonConstants.OPERATIONMODE.CREATE) {
				this.txtName.setEditable(true);
				this.activeBtn.setEnabled(true);
				this.saveBtn.setEnabled(false);
				this.txtDescription.setEditable(true);
				setShowButtonBar(true);
				this.dirty.setDirty(true);
			} else if (projectCreateEvtActModel.getOperationMode() == CommonConstants.OPERATIONMODE.CHANGE) {
				this.txtName.setEditable(true);
				this.activeBtn.setEnabled(true);
				this.txtDescription.setEditable(true);
				setShowButtonBar(true);
				this.dirty.setDirty(true);
			} else {
				this.txtName.setEditable(false);
				this.activeBtn.setEnabled(false);
				this.txtDescription.setEditable(false);
				setShowButtonBar(false);
			}
		}
	}

	/**
	 * 7 * Sets the dirty object.
	 *
	 * @param dirty
	 *            the new dirty object
	 */
	public void setDirtyObject(final MDirtyable dirty) {
		this.dirty = dirty;
	}

	/**
	 * Set project create evt.
	 */
	public void setProjectCreateEvt() {
		try {
			final AdminTreeviewer adminTree = XMAdminUtil.getInstance().getAdminTree();
			final Object selectionObj = adminTree.getSelection();
			if (selectionObj instanceof IStructuredSelection) {
				Object firstElement = ((IStructuredSelection) selectionObj).getFirstElement();
				if (firstElement instanceof ProjectCreateEvtAction) {
					setOldModel((ProjectCreateEvtAction) firstElement);
					ProjectCreateEvtAction rightHandObject = (ProjectCreateEvtAction) this.getOldModel()
							.deepCopyProCreateEvtAction(false, null);
					setProjectCreateEvtActModel(rightHandObject);
					registerMessages(this.registry);
					bindValues();
					setOperationMode();
					updateCCUsers(this.projectCreateEvtActModel.getCcUsers());
					updateToUsers(this.projectCreateEvtActModel.getToUsers());
					updateTemplate();
				}
			}
		} catch (Exception e) {
			LOGGER.warn("Unable to set projectCreate event model selection ! " + e);
		}
	}

	/**
	 * Sets the model.
	 *
	 * @param projectCreateEvtAction
	 *            the new model
	 */
	public void setModel(ProjectCreateEvtAction projectCreateEvtAction) {
		try {
			setOldModel(null);
			setProjectCreateEvtActModel(projectCreateEvtAction);
			registerMessages(this.registry);
			bindValues();
			setOperationMode();
			updateCCUsers(this.projectCreateEvtActModel.getCcUsers());
			updateToUsers(this.projectCreateEvtActModel.getToUsers());
			//this.txtTemplate.setText(CommonConstants.EMPTY_STR);
			updateTemplate();
		} catch (Exception e) {
			LOGGER.warn("Unable to set action model ! " + e);
		}
	}
}
