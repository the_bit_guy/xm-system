package com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.noProjectMessage;

import java.beans.PropertyChangeEvent;

import com.magna.xmsystem.dependencies.utils.XMSystemUtil;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.BeanModel;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild;
import com.magna.xmsystem.xmadmin.util.CommonConstants;

public class NoProjectPopMessage extends BeanModel implements IAdminTreeChild {

	/** The Constant PROPERTY_NO_PROJECT_MESSAGE_ID. */
	public static final String PROPERTY_NO_PROJECT_MESSAGE_ID = "NoprojectMessageId"; //$NON-NLS-1$

	/** The Constant PROPERTY_OPERATIONMODE. */
	public static final String PROPERTY_OPERATIONMODE = "operationMode"; //$NON-NLS-1$

	/** The Constant PROPERTY_STYLED_TEXT. */
	public static final String PROPERTY_STYLED_TEXT = "styledtext"; //$NON-NLS-1$
	
	public static final int DETAILS_LIMIT = 3990;

	/** The no project pop message id. */
	private String noProjectPopMessageId;

	/** The operation mode. */
	private int operationMode;

	/** The styledtext. */
	private String styledtext;

	/**
	 * Instantiates a new no project pop message.
	 *
	 * @param NoprojectMessageId
	 *            the noproject message id
	 * @param operationMode
	 *            the operation mode
	 */
	public NoProjectPopMessage(final String NoprojectMessageId, final int operationMode) {

		this(NoprojectMessageId, null, operationMode);
	}

	/**
	 * Instantiates a new no project pop message.
	 *
	 * @param NoprojectMessageId
	 *            the noproject message id
	 * @param styledText
	 *            the styled text
	 * @param operationMode
	 *            the operation mode
	 */
	public NoProjectPopMessage(final String NoprojectMessageId, final String styledText, final int operationMode) {
		super();
		this.noProjectPopMessageId = NoprojectMessageId;
		this.styledtext = styledText;
		this.operationMode = operationMode;
	}

	/**
	 * Gets the no project pop message id.
	 *
	 * @return the no project pop message id
	 */
	public String getNoProjectPopMessageId() {
		return noProjectPopMessageId;
	}

	/**
	 * Sets the no project pop message id.
	 *
	 * @param noProjectPopMessageId
	 *            the new no project pop message id
	 */
	public void setNoProjectPopMessageId(String noProjectPopMessageId) {
		if (noProjectPopMessageId == null)
			throw new IllegalArgumentException();
		this.propertyChangeSupport.firePropertyChange(noProjectPopMessageId, this.noProjectPopMessageId,
				this.noProjectPopMessageId = noProjectPopMessageId);
	}

	/**
	 * Gets the styledtext.
	 *
	 * @return the styledtext
	 */
	public String getStyledtext() {
		return styledtext;
	}

	/**
	 * Sets the styledtext.
	 *
	 * @param styledtext
	 *            the new styledtext
	 */
	public void setStyledtext(String styledtext) {
		if (styledtext == null)
			throw new IllegalArgumentException();
		this.propertyChangeSupport.firePropertyChange(PROPERTY_STYLED_TEXT, this.styledtext,
				this.styledtext = styledtext);
	}

	/**
	 * Gets the operation mode.
	 *
	 * @return the operation mode
	 */
	public int getOperationMode() {
		return operationMode;
	}

	/**
	 * Sets the operation mode.
	 *
	 * @param operationMode
	 *            the new operation mode
	 */
	public void setOperationMode(int operationMode) {
		this.propertyChangeSupport.firePropertyChange(PROPERTY_OPERATIONMODE, this.operationMode,
				this.operationMode = operationMode);
	}

	/**
	 * Deep copy singleton app time config model.
	 *
	 * @param update the update
	 * @param updateThisObject the update this object
	 * @return the no project pop message
	 */
	public NoProjectPopMessage deepCopyNoProjectPopMessageConfigModel(boolean update,
			NoProjectPopMessage updateThisObject) {

		NoProjectPopMessage clonedNoprojectPopConfig = null;

		try {
			String currentNoprojectMessageId = XMSystemUtil.isEmpty(this.getNoProjectPopMessageId())
					? CommonConstants.EMPTY_STR : this.getNoProjectPopMessageId();

			String currentstyletext = XMSystemUtil.isEmpty(this.getStyledtext()) ? CommonConstants.EMPTY_STR : this.getStyledtext();

			if (update) {
				clonedNoprojectPopConfig = updateThisObject;
			} else {
				clonedNoprojectPopConfig = new NoProjectPopMessage(currentNoprojectMessageId, currentstyletext,
						CommonConstants.OPERATIONMODE.VIEW);
			}
			clonedNoprojectPopConfig.setNoProjectPopMessageId(currentNoprojectMessageId);
			clonedNoprojectPopConfig.setStyledtext(currentstyletext);

			return clonedNoprojectPopConfig;
		} catch (Exception e) {
			return null;
		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.BeanModel#
	 * propertyChange(java.beans.PropertyChangeEvent)
	 */
	@Override
	public void propertyChange(PropertyChangeEvent event) {
		this.propertyChangeSupport.firePropertyChange(event.getPropertyName(), event.getOldValue(),
				event.getNewValue());

	}
}
