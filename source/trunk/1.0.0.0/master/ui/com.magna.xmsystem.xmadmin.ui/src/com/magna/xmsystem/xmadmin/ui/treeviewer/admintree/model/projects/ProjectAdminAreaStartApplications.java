package com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.projects;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.stream.Collectors;

import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.startapplicationmodel.StartApplication;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.refs.RelationObj;

/**
 * Class for Project admin area start applications.
 *
 * @author Chiranjeevi.Akula
 */
public class ProjectAdminAreaStartApplications implements IAdminTreeChild {

	/** Member variable 'parent' for {@link IAdminTreeChild}. */
	private IAdminTreeChild parent;
	
	/** Member variable 'project admin area start app child' for {@link Map<String,IAdminTreeChild>}. */
	private Map<String, IAdminTreeChild> projectAdminAreaStartAppChild;


	/**
	 * Constructor for ProjectAdminAreaStartApplications Class.
	 *
	 * @param parent {@link IAdminTreeChild}
	 */
	public ProjectAdminAreaStartApplications(final IAdminTreeChild parent) {
		this.parent = parent;
		this.projectAdminAreaStartAppChild = new LinkedHashMap<>();
	}
	
	/**
	 * Gets the project admin area start app child.
	 *
	 * @return the project admin area start app child
	 */
	public Map<String, IAdminTreeChild> getProjectAdminAreaStartAppChild() {
		return projectAdminAreaStartAppChild;
	}
	
	/**
	 * Gets the project admin area start app collection.
	 *
	 * @return the project admin area start app collection
	 */
	public Collection<IAdminTreeChild> getProjectAdminAreaStartAppCollection() {
		return this.projectAdminAreaStartAppChild.values();
	}
	
	/**
	 * Method for Adds the.
	 *
	 * @param projectAdminAreaStartAppChildId {@link String}
	 * @param child {@link IAdminTreeChild}
	 * @return the i admin tree child {@link IAdminTreeChild}
	 */
	public IAdminTreeChild add(final String projectAdminAreaStartAppChildId, final IAdminTreeChild child) {
		child.setParent(this);
		IAdminTreeChild returnVal = this.projectAdminAreaStartAppChild.put(projectAdminAreaStartAppChildId, child);
		if (child instanceof RelationObj && ((RelationObj) child).getRefObject() instanceof StartApplication) {
			sort();
		}
		return returnVal;
	}
	
	/**
	 * Method for Removes the.
	 *
	 * @param projectAdminAreaStartAppChildId {@link String}
	 * @return the i admin tree child {@link IAdminTreeChild}
	 */
	public IAdminTreeChild remove(final String projectAdminAreaStartAppChildId) {
		return this.projectAdminAreaStartAppChild.remove(projectAdminAreaStartAppChildId);
	}

	/* (non-Javadoc)
	 * @see com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild#getParent()
	 */
	@Override
	public IAdminTreeChild getParent() {
		return this.parent;
	}

	/* (non-Javadoc)
	 * @see com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild#setParent(com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild)
	 */
	@Override
	public void setParent(final IAdminTreeChild parent) {
		this.parent = parent;
	}
	
	/**
	 * Sort.
	 */
	public void sort() {
		final LinkedHashMap<String, IAdminTreeChild> collect = this.projectAdminAreaStartAppChild.entrySet().stream().sorted(
				(e1, e2) -> ((StartApplication) (((RelationObj) e1.getValue()).getRefObject())).getName()
				.compareTo(((StartApplication) (((RelationObj) e2.getValue()).getRefObject())).getName()))
				.collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e1, LinkedHashMap::new));
		this.projectAdminAreaStartAppChild = collect;
	}


}
