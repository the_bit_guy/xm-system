package com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.roles;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.stream.Collectors;

import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild;

/**
 * The Class Roles.
 * 
 * @author archita.patel
 */
public class Roles implements IAdminTreeChild {

	/** The roles children. */
	private Map<String, IAdminTreeChild> rolesChildren;

	/** The parent. */
	private IAdminTreeChild parent;
	
	/**
	 * Instantiates a new roles.
	 */
	public Roles() {
		this.parent = null;
		this.rolesChildren = new LinkedHashMap<>();
	}

	/**
	 * Add.
	 *
	 * @param rolId
	 *            the rol id
	 * @param child
	 *            the child
	 * @return the i admin tree child
	 */
	public IAdminTreeChild add(final String rolId, final IAdminTreeChild child) {
		child.setParent(this);
		IAdminTreeChild returnVal = this.rolesChildren.put(rolId, child);
		sort();
		return returnVal;
	}

	/**
	 * Remove.
	 *
	 * @param roleId
	 *            the role id
	 * @return the i admin tree child
	 */
	public IAdminTreeChild remove(final String roleId) {
		return this.rolesChildren.remove(roleId);
	}

	/**
	 * Gets the roles collection.
	 *
	 * @return the roles collection
	 */
	public Collection<IAdminTreeChild> getRolesCollection() {
		return this.rolesChildren.values();
	}

	/**
	 * Gets the roles children.
	 *
	 * @return the roles children
	 */
	public Map<String, IAdminTreeChild> getRolesChildren() {
		return rolesChildren;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild#
	 * getParent()
	 */
	@Override
	public IAdminTreeChild getParent() {
		return this.parent;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild#
	 * setParent(com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.
	 * IAdminTreeChild)
	 */
	@Override
	public void setParent(IAdminTreeChild parent) {
		this.parent = parent;

	}

	/**
	 * Sort.
	 */
	public void sort() {
		final LinkedHashMap<String, IAdminTreeChild> collect = this.rolesChildren.entrySet().stream().sorted(
				(e1, e2) -> ((Role) e1.getValue()).getRoleName().compareTo(((Role) e2.getValue()).getRoleName()))
				.collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e1, LinkedHashMap::new));
		this.rolesChildren = collect;
	}
}
