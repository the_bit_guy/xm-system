package com.magna.xmsystem.xmadmin.ui.userreviewdialogs;

// TODO: Auto-generated Javadoc
/**
 * The Class UserAccountStatus.
 */
public class UserAccountStatus {

	/** The User name. */
	private String UserName;
	
	/** The User id. */
	private String UserId;

	/** The Ldap account status. */
	private String LdapAccountStatus;

	/** The Xm system status. */
	private String XmSystemStatus;

	/**
	 * Gets the user name.
	 *
	 * @return the user name
	 */
	public String getUserName() {
		return UserName;
	}

	/**
	 * Sets the user name.
	 *
	 * @param userName
	 *            the new user name
	 */
	public void setUserName(String userName) {
		UserName = userName;
	}

	public String getUserId() {
		return UserId;
	}

	public void setUserId(String userId) {
		UserId = userId;
	}

	/**
	 * Gets the ldap account status.
	 *
	 * @return the ldap account status
	 */
	public String getLdapAccountStatus() {
		return LdapAccountStatus;
	}

	/**
	 * Sets the ldap account status.
	 *
	 * @param ldapAccountStatus
	 *            the new ldap account status
	 */
	public void setLdapAccountStatus(String ldapAccountStatus) {
		LdapAccountStatus = ldapAccountStatus;
	}

	/**
	 * Gets the xm system status.
	 *
	 * @return the xm system status
	 */
	public String getXmSystemStatus() {
		return XmSystemStatus;
	}

	/**
	 * Sets the xm system status.
	 *
	 * @param xmSystemStatus
	 *            the new xm system status
	 */
	public void setXmSystemStatus(String xmSystemStatus) {
		XmSystemStatus = xmSystemStatus;
	}

}
