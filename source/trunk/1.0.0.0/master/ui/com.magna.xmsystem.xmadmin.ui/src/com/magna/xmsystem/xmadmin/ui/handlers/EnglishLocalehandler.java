
package com.magna.xmsystem.xmadmin.ui.handlers;

import java.util.List;
import java.util.Locale;

import javax.inject.Inject;
import javax.inject.Named;

import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.core.services.nls.ILocaleChangeService;
import org.eclipse.e4.core.services.nls.Translation;
import org.eclipse.e4.core.services.translation.TranslationService;
import org.eclipse.e4.ui.model.application.ui.MUIElement;
import org.eclipse.e4.ui.model.application.ui.basic.MPart;
import org.eclipse.e4.ui.model.application.ui.menu.MHandledItem;
import org.eclipse.swt.widgets.Display;

import com.magna.xmsystem.dependencies.utils.LANG_ENUM;
import com.magna.xmsystem.dependencies.utils.XMSystemUtil;
import com.magna.xmsystem.ui.controls.widgets.CustomMessageDialog;
import com.magna.xmsystem.xmadmin.message.Message;
import com.magna.xmsystem.xmadmin.ui.parts.IEditablePart;
import com.magna.xmsystem.xmadmin.util.MessageType;
import com.magna.xmsystem.xmadmin.util.XMAdminUtil;

/**
 * Handler class to change the language to English.
 *
 * @author shashwat.anand
 */
public class EnglishLocalehandler {

	/** Member variable 'messages' for {@link Message}. */
	@Inject
	@Translation
	private Message messages;
	
	/**
	 * Execute.
	 *
	 * @param service the service
	 * @param locale the locale
	 * @param handledItem the handled item
	 */
	@Execute
	public void execute(ILocaleChangeService service, @Named(TranslationService.LOCALE) Locale locale, final MHandledItem handledItem) {
		XMAdminUtil instance = XMAdminUtil.getInstance();
		MPart rightHandMPartOfOldPerspective = instance.getInformationPart();
		if (rightHandMPartOfOldPerspective.getObject() instanceof IEditablePart && rightHandMPartOfOldPerspective.isDirty()) {
			/*boolean response = CustomMessageDialog.openSaveDiscardDialog(
					Display.getDefault().getActiveShell(), "XMAdmin",
					"Data is modified \n\n Do you want to Save");*/
			boolean response = CustomMessageDialog.openSaveDiscardDialog(Display.getDefault().getActiveShell(),
					messages.dirtyDialogTitle, messages.dirtyDialogMessage);
			if (response) {
				// Save
				instance.savePreviousPart();
			} else {
				// Discard
				instance.discardPreviousPart();
			}
		}
		instance.setLocale(Locale.ENGLISH);
		service.changeApplicationLocale(Locale.ENGLISH);
		XMSystemUtil.setLanguage(LANG_ENUM.getLangEnum(Locale.ENGLISH.getLanguage()).name().toLowerCase());
		List<MUIElement> children = handledItem.getParent().getChildren();
		for (MUIElement muiElement : children) {
			if ("com.magna.xmsystem.xmadmin.ui.handledmenuitem.enlocale".equals(muiElement.getElementId())) {
				((MHandledItem) muiElement).setSelected(true);
			} else if ("com.magna.xmsystem.xmadmin.ui.handledmenuitem.delocale".equals(muiElement.getElementId())) {
				((MHandledItem) muiElement).setSelected(false);
			}
		}
		XMAdminUtil.getInstance().updateLogFile(messages.englishLanguageChangeMsg, MessageType.SUCCESS);
	}
}