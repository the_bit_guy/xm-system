package com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.directory;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.stream.Collectors;

import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.projects.Project;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.refs.RelationObj;

public class DirectoryProjects implements IAdminTreeChild {

	/** The parent. */
	private IAdminTreeChild parent;

	/** The directory projects children. */
	private Map<String, IAdminTreeChild> directoryProjectsChildren;

	/**
	 * Instantiates a new directory project model.
	 *
	 * @param parent
	 *            the parent
	 */
	public DirectoryProjects(final IAdminTreeChild parent) {
		this.parent = parent;
		this.directoryProjectsChildren = new LinkedHashMap<>();
	}

	/**
	 * Adds the.
	 *
	 * @param directoryProjectsChildrenId
	 *            the directory projects children id
	 * @param child
	 *            the child
	 * @return the i admin tree child
	 */
	public IAdminTreeChild add(final String directoryProjectsChildrenId, final IAdminTreeChild child) {
		IAdminTreeChild returnVal = this.directoryProjectsChildren.put(directoryProjectsChildrenId, child);
		if (child instanceof RelationObj && ((RelationObj) child).getRefObject() instanceof Project) {
			sort();
		}
		return returnVal;
	}

	/**
	 * Removes the.
	 *
	 * @param directoryProjectsChildrenId
	 *            the directory projects children id
	 * @return the i admin tree child
	 */
	public IAdminTreeChild remove(final String directoryProjectsChildrenId) {
		return this.directoryProjectsChildren.remove(directoryProjectsChildrenId);

	}

	/**
	 * Gets the directory projects children collection.
	 *
	 * @return the directory projects children collection
	 */
	public Collection<IAdminTreeChild> getDirectoryProjectsChildrenCollection() {
		return this.directoryProjectsChildren.values();
	}

	/**
	 * Gets the directory projects children children.
	 *
	 * @return the directory projects children children
	 */
	public Map<String, IAdminTreeChild> getDirectoryProjectsChildrenChildren() {
		return directoryProjectsChildren;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild#
	 * getParent()
	 */
	@Override
	public IAdminTreeChild getParent() {
		// TODO Auto-generated method stub
		return this.parent;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild#
	 * setParent(com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.
	 * IAdminTreeChild)
	 */
	@Override
	public void setParent(IAdminTreeChild parent) {
		this.parent = parent;

	}
	
	/**
	 * Sort.
	 */
	public void sort() {
		final LinkedHashMap<String, IAdminTreeChild> collect = this.directoryProjectsChildren.entrySet().stream().sorted(
				(e1, e2) -> ((Project) e1.getValue()).getName().compareTo(((Project) e2.getValue()).getName()))
				.collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e1, LinkedHashMap::new));
		this.directoryProjectsChildren = collect;
	}

}
