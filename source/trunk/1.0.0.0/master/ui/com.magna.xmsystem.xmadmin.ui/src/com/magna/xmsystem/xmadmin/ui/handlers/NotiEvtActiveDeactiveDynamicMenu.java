
package com.magna.xmsystem.xmadmin.ui.handlers;

import java.util.List;

import javax.inject.Inject;

import org.eclipse.e4.core.services.nls.Translation;
import org.eclipse.e4.ui.di.AboutToShow;
import org.eclipse.e4.ui.model.application.ui.menu.MDirectMenuItem;
import org.eclipse.e4.ui.model.application.ui.menu.MMenuElement;
import org.eclipse.e4.ui.model.application.ui.menu.MMenuFactory;
import org.eclipse.e4.ui.workbench.modeling.ESelectionService;
import org.eclipse.jface.viewers.IStructuredSelection;

import com.magna.xmsystem.xmadmin.message.Message;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.notification.ProjectActivateEvt;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.notification.ProjectActivateEvtAction;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.notification.ProjectCreateEvt;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.notification.ProjectCreateEvtAction;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.notification.ProjectDeactivateEvt;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.notification.ProjectDeactivateEvtAction;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.notification.ProjectDeleteEvt;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.notification.ProjectDeleteEvtAction;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.notification.UserProjectRelAssignEvt;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.notification.UserProjectRelAssignEvtAction;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.notification.UserProjectRelRemoveEvt;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.notification.UserProjectRelRemoveEvtAction;
import com.magna.xmsystem.xmadmin.util.CommonConstants;

public class NotiEvtActiveDeactiveDynamicMenu {

	/** Member variable 'selection service' for {@link ESelectionService}. */
	@Inject
	private ESelectionService selectionService;

	/** Member variable 'messages' for {@link Message}. */
	@Inject
	@Translation
	private Message messages;

	@AboutToShow
	public void aboutToShow(List<MMenuElement> items) {
		final Object selectionObj = selectionService.getSelection();
		if (selectionObj == null) {
			return;
		}
		if (selectionObj instanceof IStructuredSelection) {
			Object firstElement = ((IStructuredSelection) selectionObj).getFirstElement();
			if (firstElement instanceof ProjectCreateEvt) {
				boolean active = ((ProjectCreateEvt) firstElement).isActive();
				setMenuIteam(items, active);
			} else if (firstElement instanceof ProjectCreateEvtAction) {
				boolean active = ((ProjectCreateEvtAction) firstElement).isActive();
				setMenuIteam(items, active);
			} else if (firstElement instanceof ProjectDeleteEvt) {
				boolean active = ((ProjectDeleteEvt) firstElement).isActive();
				setMenuIteam(items, active);
			} else if (firstElement instanceof ProjectDeleteEvtAction) {
				boolean active = ((ProjectDeleteEvtAction) firstElement).isActive();
				setMenuIteam(items, active);
			} else if (firstElement instanceof ProjectDeactivateEvtAction) {
				boolean active = ((ProjectDeactivateEvtAction) firstElement).isActive();
				setMenuIteam(items, active);
			} else if (firstElement instanceof ProjectDeactivateEvt) {
				boolean active = ((ProjectDeactivateEvt) firstElement).isActive();
				setMenuIteam(items, active);
			} else if (firstElement instanceof ProjectActivateEvtAction) {
				boolean active = ((ProjectActivateEvtAction) firstElement).isActive();
				setMenuIteam(items, active);
			} else if (firstElement instanceof ProjectActivateEvt) {
				boolean active = ((ProjectActivateEvt) firstElement).isActive();
				setMenuIteam(items, active);
			} else if (firstElement instanceof UserProjectRelAssignEvt) {
				boolean active = ((UserProjectRelAssignEvt) firstElement).isActive();
				setMenuIteam(items, active);
			} else if (firstElement instanceof UserProjectRelAssignEvtAction) {
				boolean active = ((UserProjectRelAssignEvtAction) firstElement).isActive();
				setMenuIteam(items, active);
			} else if (firstElement instanceof UserProjectRelRemoveEvt) {
				boolean active = ((UserProjectRelRemoveEvt) firstElement).isActive();
				setMenuIteam(items, active);
			} else if (firstElement instanceof UserProjectRelRemoveEvtAction) {
				boolean active = ((UserProjectRelRemoveEvtAction) firstElement).isActive();
				setMenuIteam(items, active);
			}
		}
	}

	private void setMenuIteam(List<MMenuElement> items, boolean active) {
		MDirectMenuItem dynamicItem = MMenuFactory.INSTANCE.createDirectMenuItem();
		if (active) {
			dynamicItem.setLabel(messages.popupmenulabelnodeDeActive);
			dynamicItem.setTooltip(messages.popupmenulabelnodeDeActive);
			dynamicItem.setContributionURI(
					CommonConstants.XMADMIN_UI_BUNDLE + "/" + NotifyDeActiveMenuHandler.class.getCanonicalName());

		} else {
			dynamicItem.setLabel(messages.popupmenulabelnodeActive);
			dynamicItem.setTooltip(messages.popupmenulabelnodeActive);
			dynamicItem.setContributionURI(
					CommonConstants.XMADMIN_UI_BUNDLE + "/" + NotifyActiveMenuHandler.class.getCanonicalName());
		}
		items.add(dynamicItem);
	}
}
