package com.magna.xmsystem.xmadmin.ui.parts;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.eclipse.e4.core.contexts.ContextInjectionFactory;
import org.eclipse.e4.core.contexts.IEclipseContext;
import org.eclipse.jface.layout.GridDataFactory;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;

import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.AdminTreeviewer;

/**
 * Part Class for AdminTree
 * 
 * @author shashwat.anand
 *
 */
public class AdminTreePart {

	/**
	 * Member variable for {@link IEclipseContext}
	 */
	@Inject
	private IEclipseContext eclipseContext;

	/**
	 * Member variable {@link AdminTreeviewer}
	 */
	private AdminTreeviewer adminTreeViewer;

	/**
	 * createComposite is called after all inject happens
	 * 
	 * @param parent
	 */
	@PostConstruct
	public void createComposite(final Composite parent) {
		parent.setLayout(new GridLayout(1, false));
		GridDataFactory.fillDefaults().grab(true, true).span(SWT.FILL, SWT.FILL).applyTo(parent);

		eclipseContext.set(Composite.class, parent);
		AdminTreePart.this.adminTreeViewer = ContextInjectionFactory.make(AdminTreeviewer.class, eclipseContext);
		eclipseContext.getParent().getParent().getParent().set(AdminTreeviewer.class,
				AdminTreePart.this.adminTreeViewer);
	}
}