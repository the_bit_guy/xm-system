package com.magna.xmsystem.xmadmin.ui.parts.livemsgconfig;

import org.eclipse.jface.layout.GridDataFactory;
import org.eclipse.jface.layout.GridLayoutFactory;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.ScrolledComposite;
import org.eclipse.swt.custom.StackLayout;
import org.eclipse.swt.custom.StyledText;
import org.eclipse.swt.events.ControlAdapter;
import org.eclipse.swt.events.ControlEvent;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Link;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.widgets.ToolBar;
import org.eclipse.swt.widgets.ToolItem;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.magna.xmsystem.dependencies.utils.XMSystemUtil;
import com.magna.xmsystem.ui.controls.widgets.CustomDateTime;
import com.magna.xmsystem.xmadmin.ui.parts.notification.FilterPanel;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.livemsgconfig.LiveMessage;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.notification.Notification;
import com.magna.xmsystem.xmadmin.util.XMAdminUtil;

/**
 * The Class LiveMessageConfigUI.
 * 
 * @author archita.patel
 */
public class LiveMessageConfigUI extends Composite {

	/** The Constant LOGGER. */
	private static final Logger LOGGER = LoggerFactory.getLogger(LiveMessageConfigUI.class);

	/** The grplive msg. */
	protected Group grpliveMsg;

	/** The lbl name. */
	protected Label lblName;

	/** The txtname. */
	protected Text txtName;

	/** The lbl to. */
	protected Label lblTo;

	/** The txt to. */
	protected StyledText txtTo;

	/** The lblmessages. */
	protected Label lblmessages;

	/** The txtmessages. */
	protected Text txtMessage;
	
	/** The msgink. */
	protected Link messageLink;

	/** The lbl popup. */
	protected Label lblPopup;

	/** The popup btn. */
	protected Button popupBtn;

	/** The lblExpiryDateTime date time. */
	protected Label lblExpiryDateTime;

	/** The cdtExpiryDateTime. */
	protected CustomDateTime cdtExpiryDate;

	/** The lblStartDateTime date time. */
	protected Label lblStartDateTime;

	/** The cdtStartDateTime. */
	protected CustomDateTime cdtStartDate;

	/** The send btn. */
	protected Button saveBtn;

	/** The grp date time. */
	protected Group grpDateTime;

	/** The cancel btn. */
	protected Button cancelBtn;

	/** The filter panel. */
	protected FilterPanel filterPanel;

	/** The radio btn user. */
	protected Button radioBtnObject;

	/** The radio btn user group. */
	protected Button radioBtnGroups;

	/** The add btn. */
	// protected Button addBtn;

	/** The filter container. */
	protected Composite filterContainer;

	/** The filter container layout. */
	protected StackLayout filterContainerLayout;

	/** The user group filter panel. */
	protected GroupsFilterPanel groupsFilterPanel;

	/** The lbl subject. */
	protected Label lblSubject;

	/** The txt subject. */
	protected Text txtSubject;
	
	protected Link subjectLink;

	protected ToolItem toolitemAddBtn;

	/**
	 * Instantiates a new live message config UI.
	 *
	 * @param parent
	 *            the parent
	 * @param style
	 *            the style
	 */
	public LiveMessageConfigUI(final Composite parent, final int style) {
		super(parent, style);
		this.initGUI();
	}

	/**
	 * Inits the GUI.
	 */
	private void initGUI() {
		try {
			GridLayoutFactory.fillDefaults().applyTo(this);
			this.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));

			this.grpliveMsg = new Group(this, SWT.NONE);
			this.grpliveMsg.setBackgroundMode(SWT.INHERIT_FORCE);
			GridLayoutFactory.fillDefaults().numColumns(1).applyTo(this.grpliveMsg);
			GridDataFactory.fillDefaults().grab(true, true).span(SWT.FILL, SWT.FILL).applyTo(this.grpliveMsg);
			ScrolledComposite scrolledComposite = XMAdminUtil.getInstance().createScrolledComposite(this.grpliveMsg);
			scrolledComposite.setBackgroundMode(SWT.INHERIT_FORCE);
			
			final Composite widgetContainer = new Composite(scrolledComposite, SWT.NONE);
			final GridLayout widgetContLayout = new GridLayout(3, false);
			widgetContLayout.horizontalSpacing = 10;
			widgetContainer.setLayout(widgetContLayout);
			widgetContainer.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));

			this.lblName = new Label(widgetContainer, SWT.NONE);
			this.txtName = new Text(widgetContainer, SWT.BORDER);
			GridDataFactory.fillDefaults().grab(true, false).span(2, 1).align(SWT.FILL, SWT.CENTER)
					.applyTo(this.txtName);
			this.txtName.setTextLimit(LiveMessage.NAME_LIMIT);

			new Label(widgetContainer, SWT.NONE);
			final Composite radioBtnContainer = new Composite(widgetContainer, SWT.NONE);
			GridLayoutFactory.fillDefaults().numColumns(3).equalWidth(false).extendedMargins(0, 0, 0, 0)
					.applyTo(radioBtnContainer);
			GridDataFactory.fillDefaults().grab(true, false).span(2, 1).align(SWT.FILL, SWT.CENTER)
					.applyTo(radioBtnContainer);

			this.radioBtnObject = new Button(radioBtnContainer, SWT.RADIO);
			GridDataFactory.fillDefaults().grab(true, false).span(1, 1).align(SWT.FILL, SWT.CENTER)
					.applyTo(this.radioBtnObject);
			this.radioBtnObject.setSelection(true);

			this.radioBtnGroups = new Button(radioBtnContainer, SWT.RADIO);
			GridDataFactory.fillDefaults().grab(true, false).span(1, 1).align(SWT.FILL, SWT.CENTER)
					.applyTo(this.radioBtnGroups);

			final Composite container = new Composite(widgetContainer, SWT.NONE);
			final GridLayout containerLayout = new GridLayout(2, false);
			container.setLayout(containerLayout);
			GridDataFactory.fillDefaults().grab(true, false).span(3, 1).align(SWT.FILL, SWT.FILL).applyTo(container);

			filterContainer = new Composite(container, SWT.NONE);
			this.filterContainerLayout = new StackLayout();
			filterContainer.setLayout(filterContainerLayout);
			GridDataFactory.fillDefaults().grab(true, false).span(1, 1).align(SWT.FILL, SWT.CENTER)
					.applyTo(filterContainer);

			this.filterPanel = new FilterPanel(filterContainer);
			final GridLayout filterCompLayout = new GridLayout(1, true);
			filterCompLayout.marginRight = 0;
			filterCompLayout.marginLeft = 0;
			filterCompLayout.marginTop = 0;
			filterCompLayout.marginBottom = 0;
			filterCompLayout.marginWidth = 0;
			this.filterPanel.setLayout(filterCompLayout);
			this.filterPanel.setLayoutData(new GridData(SWT.FILL, SWT.DEFAULT, true, false, 1, 1));

			this.groupsFilterPanel = new GroupsFilterPanel(filterContainer);
			final GridLayout groupsfilterCompLayout = new GridLayout(1, true);
			filterCompLayout.marginRight = 0;
			filterCompLayout.marginLeft = 0;
			filterCompLayout.marginTop = 0;
			filterCompLayout.marginBottom = 0;
			filterCompLayout.marginWidth = 0;
			this.groupsFilterPanel.setLayout(groupsfilterCompLayout);
			this.groupsFilterPanel.setLayoutData(new GridData(SWT.FILL, SWT.DEFAULT, true, false, 1, 1));

			this.filterContainerLayout.topControl = this.filterPanel;

			final Composite addBtnContainer = new Composite(container, SWT.NONE);
			final GridLayout addcontainerLayout = new GridLayout(1, false);
			addBtnContainer.setLayout(addcontainerLayout);
			GridData layoutData = new GridData(SWT.CENTER, SWT.CENTER, false, false);
			layoutData.verticalIndent = -50;
			addBtnContainer.setLayoutData(layoutData);

			final ToolBar toolbar = new ToolBar(addBtnContainer, SWT.NONE);
			this.toolitemAddBtn = new ToolItem(toolbar, SWT.FLAT);
			this.toolitemAddBtn.setImage(XMSystemUtil.getInstance().getImage(this.getClass(), "icons/32x32/addButton_32.png"));
			toolbar.setLayoutData(new GridData(SWT.CENTER, SWT.CENTER, true, false));
			this.toolitemAddBtn.setToolTipText("Add");

			this.lblTo = new Label(widgetContainer, SWT.NONE);
			this.lblTo.setLayoutData(new GridData(SWT.LEFT, SWT.FILL, false, false));
			this.txtTo = new StyledText(widgetContainer,
					SWT.BORDER | SWT.WRAP | SWT.MULTI | SWT.H_SCROLL | SWT.V_SCROLL);
			this.txtTo.setBackground(Display.getDefault().getSystemColor(SWT.COLOR_WHITE));
			GridData gridData = new GridData(SWT.FILL, SWT.FILL, true, true);
			gridData.minimumHeight = 30;
			gridData.horizontalSpan = 2;
			this.txtTo.setLayoutData(gridData);

			this.lblSubject = new Label(widgetContainer, SWT.NONE);
			this.txtSubject = new Text(widgetContainer, SWT.BORDER);
			this.txtSubject.setTextLimit(Notification.SUB_LIMIT);
			GridDataFactory.fillDefaults().grab(true, false).span(1, 1).align(SWT.FILL, SWT.CENTER)
					.applyTo(this.txtSubject);
			this.subjectLink = new Link(widgetContainer, SWT.NONE);


			this.lblmessages = new Label(widgetContainer, SWT.NONE);
			GridDataFactory.fillDefaults().grab(false, false).span(1, 1).align(SWT.LEFT, SWT.FILL).applyTo(lblmessages);
			
			//this.messageLink = new Link(widgetContainer, SWT.NONE);
			
			this.txtMessage = new Text(widgetContainer,
					SWT.BORDER | SWT.WRAP | SWT.MULTI | SWT.H_SCROLL | SWT.V_SCROLL);
			gridData = new GridData(SWT.FILL, SWT.FILL, true, true);
			gridData.minimumHeight = 40;
			gridData.horizontalSpan = 1;
			//gridData.verticalIndent = -2;
			this.txtMessage.setLayoutData(gridData);
			this.txtMessage.setTextLimit(LiveMessage.MESSAGE_LIMIT);
			
			/*final Composite btnContainer = new Composite(widgetContainer, SWT.NONE);
			final GridLayout btnContainerLayout = new GridLayout(1, false);
			btnContainer.setLayout(btnContainerLayout);
			GridDataFactory.fillDefaults().grab(false, false).span(1, 1).align(SWT.FILL, SWT.FILL).applyTo(btnContainer);*/
			
			this.messageLink = new Link(widgetContainer, SWT.NONE);
			/*GridDataFactory.fillDefaults().grab(false, false).span(1, 1).align(SWT.RIGHT, SWT.CENTER)
			.applyTo(this.messageLink);*/


			final Composite popupContainer = new Composite(widgetContainer, SWT.NONE);
			final GridLayout popupContLayout = new GridLayout(2, false);
			popupContLayout.marginRight = 0;
			popupContLayout.marginLeft = 0;
			popupContLayout.marginTop = 0;
			popupContLayout.marginBottom = 0;
			popupContLayout.marginWidth = 0;
			popupContLayout.marginHeight = 0;
			popupContLayout.verticalSpacing = 20;
			popupContainer.setLayout(popupContLayout);
			GridDataFactory.fillDefaults().grab(true, false).span(3, 1).align(SWT.FILL, SWT.CENTER)
					.applyTo(popupContainer);

			this.lblStartDateTime = new Label(popupContainer, SWT.NONE);

			GridDataFactory.fillDefaults().grab(false, false).align(SWT.FILL, SWT.CENTER).span(1, 1).indent(1, 2)
					.applyTo(this.lblStartDateTime);
			cdtStartDate = new CustomDateTime(popupContainer);
			GridDataFactory.fillDefaults().grab(true, false).align(SWT.FILL, SWT.CENTER).span(1, 1)
					.applyTo(cdtStartDate);

			this.lblExpiryDateTime = new Label(popupContainer, SWT.NONE);

			GridDataFactory.fillDefaults().grab(false, false).align(SWT.FILL, SWT.CENTER).span(1, 1).indent(1, 2)
					.applyTo(this.lblExpiryDateTime);
			//cdtExpiryDate = new CDateTime(popupContainer, CDT.BORDER | CDT.DATE_SHORT | CDT.TIME_SHORT | CDT.COMPACT | CDT.TAB_FIELDS | CDT.CLOCK_DISCRETE | CDT.SPINNER);
			cdtExpiryDate = new CustomDateTime(popupContainer);
			GridDataFactory.fillDefaults().grab(true, false).align(SWT.FILL, SWT.CENTER).span(1, 1)
					.applyTo(cdtExpiryDate);

			this.popupBtn = new Button(popupContainer, SWT.CHECK | SWT.BOLD);
			GridDataFactory.fillDefaults().grab(false, false).align(SWT.LEFT, SWT.CENTER).span(1, 1)
					.applyTo(this.popupBtn);

			final Composite sendbtnContainer = new Composite(this.grpliveMsg, SWT.NONE);
			final GridLayout sendbtnContLayout = new GridLayout(2, false);
			sendbtnContLayout.marginRight = 0;
			sendbtnContLayout.marginLeft = 0;
			sendbtnContLayout.marginTop = 0;
			sendbtnContLayout.marginBottom = 0;
			sendbtnContLayout.marginWidth = 0;
			sendbtnContLayout.marginHeight = 0;
			sendbtnContainer.setLayout(sendbtnContLayout);
			GridDataFactory.fillDefaults().grab(true, false).span(2, 1).align(SWT.CENTER, SWT.CENTER)
					.applyTo(sendbtnContainer);

			this.saveBtn = new Button(sendbtnContainer, SWT.NONE);
			this.saveBtn.setLayoutData(new GridData(SWT.CENTER, SWT.CENTER, true, false));

			this.cancelBtn = new Button(sendbtnContainer, SWT.NONE);
			this.cancelBtn.setLayoutData(new GridData(SWT.CENTER, SWT.CENTER, true, false));
			
			scrolledComposite.setContent(widgetContainer);
			scrolledComposite.setSize(widgetContainer.getSize());
			scrolledComposite.setExpandVertical(true);
			scrolledComposite.setExpandHorizontal(true);
			scrolledComposite.update();

			scrolledComposite.addControlListener(new ControlAdapter() {
				public void controlResized(final ControlEvent e) {
					Rectangle rectangle = scrolledComposite.getClientArea();
					scrolledComposite.setMinSize(widgetContainer.computeSize(rectangle.width, SWT.DEFAULT));
				}
			});

		} catch (Exception e) {
			LOGGER.error("Unable to crete UI elements", e); //$NON-NLS-1$
		}
	}

	/**
	 * Sets the show button bar.
	 *
	 * @param showButtonBar
	 *            the new show button bar
	 */
	protected void setShowButtonBar(final boolean showButtonBar) {
		if (this.saveBtn != null && !this.saveBtn.isDisposed() && this.cancelBtn != null
				&& !this.cancelBtn.isDisposed()) {
			final GridData layoutData = (GridData) this.saveBtn.getParent().getLayoutData();
			layoutData.exclude = !showButtonBar;
			this.saveBtn.setVisible(showButtonBar);
			this.cancelBtn.setVisible(showButtonBar);
			this.saveBtn.getParent().setVisible(showButtonBar);
			this.saveBtn.getParent().requestLayout();
			this.saveBtn.getParent().redraw();
			this.saveBtn.getParent().getParent().update();
		}
	}
}