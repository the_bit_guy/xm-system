
package com.magna.xmsystem.xmadmin.ui.handlers.directory;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.inject.Inject;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.e4.core.di.annotations.CanExecute;
import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.core.services.nls.Translation;
import org.eclipse.e4.ui.model.application.MApplication;
import org.eclipse.e4.ui.model.application.ui.basic.MPart;
import org.eclipse.e4.ui.workbench.modeling.EModelService;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.swt.widgets.Display;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.client.ResourceAccessException;

import com.magna.xmbackend.vo.directory.DirectoryResponse;
import com.magna.xmsystem.dependencies.customExceptions.UnauthorizedAccessException;
import com.magna.xmsystem.ui.controls.widgets.CustomMessageDialog;
import com.magna.xmsystem.xmadmin.message.Message;
import com.magna.xmsystem.xmadmin.restclient.directory.DirectoryController;
import com.magna.xmsystem.xmadmin.ui.parts.IEditablePart;
import com.magna.xmsystem.xmadmin.ui.parts.InformationPart;
import com.magna.xmsystem.xmadmin.ui.parts.objexp.ObjectExpPage;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.AdminTreeviewer;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.AdminTreeFactory;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.directory.Directories;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.directory.Directory;
import com.magna.xmsystem.xmadmin.util.CommonConstants;
import com.magna.xmsystem.xmadmin.util.MessageType;
import com.magna.xmsystem.xmadmin.util.XMAdminUtil;

/**
 * The Class DeleteDirectory.
 */
public class DeleteDirectory {

	/** The Constant LOGGER. */
	private static final Logger LOGGER = LoggerFactory.getLogger(DeleteDirectory.class);

	/** Member variable for {@link Message}. */
	@Inject
	@Translation
	private Message messages;

	/**
	 * Inject of {@link EModelService}
	 */
	@Inject
	private EModelService modelService;

	/**
	 * Inject of {@link MApplication}
	 */
	@Inject
	private MApplication application;

	/**
	 * Method to delete the directory
	 */
	@Execute
	public void execute() {
		try {
			if (XMAdminUtil.getInstance().isAcessAllowed("DIRECTORY-DELETE")) {
				final AdminTreeviewer adminTree = XMAdminUtil.getInstance().getAdminTree();
				IStructuredSelection selection;
				Object selectionObj;
				MPart mPart = (MPart) this.modelService.find(CommonConstants.PART_ID.INFORMATION_PART_ID, application);
				InformationPart rightSidePart2 = (InformationPart) mPart.getObject();
				ObjectExpPage objectExpPage = rightSidePart2.getObjectExpPartUI();
				if (objectExpPage != null
						&& (selection = objectExpPage.getObjExpTableViewer().getStructuredSelection()) != null
						&& (selectionObj = selection.getFirstElement()) != null && selectionObj instanceof Directory) {
					deleteDirectory(selection);
					objectExpPage.refreshExplorer();
				} else if (adminTree != null && (selection = adminTree.getStructuredSelection()) != null
						&& (selectionObj = selection.getFirstElement()) != null && selectionObj instanceof Directory) {
					deleteDirectory(selection);
				}
				XMAdminUtil.getInstance().clearClipBoardContents();
			} else {
				CustomMessageDialog.openError(Display.getCurrent().getActiveShell(),
						messages.objectPermissionDialogTitle, messages.objectPermissionDialogMsg);
			}
		} catch (ClassCastException e) {
			LOGGER.error("Exception occured while deleting directory " + e);
		} catch (UnauthorizedAccessException e) {
			LOGGER.error("Current user is Unauthorized " + e);
		} catch (Exception e) {
			if (e instanceof ResourceAccessException) {
				CustomMessageDialog.openError(Display.getCurrent().getActiveShell(), messages.errorDialogTitile,
						messages.serverNotReachable);
			}
			LOGGER.error("Exception occured in calling access allowed API " + e);
		}
	}

	/**
	 * Delete directory.
	 *
	 * @param selectionObj
	 *            the selection obj
	 */
	@SuppressWarnings("rawtypes")
	private void deleteDirectory(IStructuredSelection selection) {
		Object selectionObj = selection.getFirstElement();
		final AdminTreeviewer adminTree = XMAdminUtil.getInstance().getAdminTree();
		final Directories directories = AdminTreeFactory.getInstance().getDirectories();
		final String directoryName = ((Directory) selectionObj).getName();
		String confirmDialogTitle = messages.deleteConfirmDialogTitle;
		String confirmDialogMsg;
		if (selection.size() == 1) {
			confirmDialogMsg =  messages.deleteDirectoryConfirmDialogMsg + " \'" + directoryName + "\'?";
		} else {
			confirmDialogMsg = messages.deleteMultiObjectsConfirmDialogMsg + " ?";
		}
		boolean isConfirmed = CustomMessageDialog.openConfirm(adminTree.getControl().getShell(), confirmDialogTitle,
				confirmDialogMsg);
		if (isConfirmed) {
			List selectionList = selection.toList();
			Job job = new Job("Deleting Objects...") {

				@Override
				protected IStatus run(IProgressMonitor monitor) {
					monitor.beginTask("Deleting Objects..", 100);
					monitor.worked(30);
					try {
						Set<String> directoryIds = new HashSet<>();
						Map<String,String> successObjectNames = new HashMap<>();
						Map<String,String> failObjectNames = new HashMap<>();
						for (int i = 0; i < selectionList.size(); i++) {
							String id = ((Directory) selectionList.get(i)).getDirectoryId();
							String name = ((Directory) selectionList.get(i)).getName();
							directoryIds.add(id);
							successObjectNames.put(id, name);
						}
						final DirectoryController directoryController = new DirectoryController();
						final DirectoryResponse directoryResponse = directoryController.deleteMultiDirectory(directoryIds);
						if (directoryResponse != null) {
							if (!directoryResponse.getStatusMaps().isEmpty()) {
								List<Map<String, String>> statusMapList = directoryResponse.getStatusMaps();
								for (Map<String, String> statusMap : statusMapList) {
									String string = statusMap.get("en");
									String[] split = string.split("id");
									directoryIds.remove(split[1].trim());
									failObjectNames.put(split[1].trim(), successObjectNames.get(split[1].trim()));
									successObjectNames.remove(split[1].trim());
								}
								Display.getDefault().asyncExec(new Runnable() {
									public void run() {
										CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
												messages.deleteConfirmDialogTitle,
												messages.deleteMultiObjectsDialogErrMsg);
									}
								});
							}
							for (String directoryId : directoryIds) {
								directories.getDirectoriesChildren().remove(directoryId);
							}
							Display.getDefault().asyncExec(new Runnable() {
								public void run() {
									adminTree.refresh();
									adminTree.setSelection(new StructuredSelection(directories), true);
								}
							});
							if (successObjectNames.size() > 0) {
								Iterator it = successObjectNames.entrySet().iterator();
								while (it.hasNext()) {
									Map.Entry map = (Map.Entry) it.next();
									XMAdminUtil.getInstance().updateLogFile(messages.directoryObject + " " + "'"
											+ map.getValue() + "'" + " " + messages.objectDelete, MessageType.SUCCESS);
								}
							}
							if (failObjectNames.size() > 0) {
								Iterator it = failObjectNames.entrySet().iterator();
								while (it.hasNext()) {
									Map.Entry map = (Map.Entry) it.next();
									XMAdminUtil.getInstance().updateLogFile(messages.deleteErrorMsg + " "
											+ messages.directoryObject + " '" + map.getValue() + "'",
											MessageType.FAILURE);
								}
							}
						}

					} catch (Exception e) {
						LOGGER.error(e.getMessage());
					}

					monitor.worked(70);
					return Status.OK_STATUS;
				}
			};
			job.setUser(true);
			job.schedule();
		}
	}

	/**
	 * Checks if is acess allowed.
	 *
	 * @return true, if is acess allowed
	 */
	/*public boolean isAcessAllowed() {
		boolean returnVal = true;
		try {
			ValidationController controller = new ValidationController();
			ValidationRequest request = new ValidationRequest();
			request.setUserName(RestClientUtil.getInstance().getUserName());
			request.setPermissionType(PermissionType.OBJECT_PERMISSION.name());
			request.setPermissionName("DIRECTORY-DELETE"); //$NON-NLS-1$
			returnVal = controller.getAccessAllowed(request);
		} catch (UnauthorizedAccessException e) {
			LOGGER.error("Current user is Unauthorized " + e);
			returnVal = false;
		} catch (Exception e) {
			LOGGER.error("Exception occured in calling access allowed API " + e);
			returnVal = false;
		}
		return returnVal;
	}*/

	/**
	 * Can execute.
	 *
	 * @return true, if successful
	 */
	@CanExecute
	public boolean canExecute() {
		boolean returnVal = true;
		final XMAdminUtil instance = XMAdminUtil.getInstance();
		MPart rightPart;
		Object rightView;
		if ((rightPart = instance.getInformationPart()) != null
				&& (rightView = rightPart.getObject()) != null && rightView instanceof IEditablePart
				&& ((IEditablePart) rightView).isDirty()) {
			returnVal = false;
		}
		return returnVal;
	}
}