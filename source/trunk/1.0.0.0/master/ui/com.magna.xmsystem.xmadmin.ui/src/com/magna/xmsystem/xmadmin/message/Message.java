package com.magna.xmsystem.xmadmin.message;

import java.text.MessageFormat;

import javax.annotation.PostConstruct;

// TODO: Auto-generated Javadoc
/**
 * Class for Message.
 *
 * @author Chiranjeevi.Akula
 */
public class Message {
	
	/** Member variable 'welcome page message' for {@link String}. */
	public String welcomePageMessage;
	
	/** The admin started message. */
	public String adminStartedMessage;
	
	/** The right side C mlabel node create as. */
	public String rightSideCMlabelNodeCreateAs;
	
	/** The right side C mlabel node change. */
	public String rightSideCMlabelNodeChange;
	
	/** The right side C mlabel node delete. */
	public String rightSideCMlabelNodeDelete;
	
	/** The right side C mlabel node go to. */
	public String rightSideCMlabelNodeGoTo;

	/** Member variable 'window title' for {@link String}. */
	public String windowTitle;

	/** Member variable 'tree group' for {@link String}. */
	public String treeGroup;

	/** Member variable 'tree loading' for {@link String}. */
	public String treeLoading;

	/** Member variable 'sites node' for {@link String}. */
	public String sitesNode;

	/** Member variable 'administration areas node' for {@link String}. */
	public String administrationAreasNode;

	/** Member variable 'users node' for {@link String}. */
	public String usersNode;

	/** Member variable 'projects node' for {@link String}. */
	public String projectsNode;

	/** Member variable 'applications node' for {@link String}. */
	public String applicationsNode;

	/** Member variable 'user applications node' for {@link String}. */
	public String userApplicationsNode;

	/** Member variable 'project applications node' for {@link String}. */
	public String projectApplicationsNode;

	/** Member variable 'event applications node' for {@link String}. */
	public String eventApplicationsNode;

	/** Member variable 'base applications node' for {@link String}. */
	public String baseApplicationsNode;

	/** Member variable 'start applications node' for {@link String}. */
	public String startApplicationsNode;

	/** Member variable 'groups node' for {@link String}. */
	public String groupsNode;
	
	/** Member variable 'userGroups node' for {@link String}. */
	public String userGroupsNode;
	
	/** Member variable 'userGroups node' for {@link String}. */
	public String projectGroupsNode;
	
	/** The user app groups node. */
	public String userAppGroupsNode;
	
	/** The project app groups node. */
	public String projectAppGroupsNode;
	
	/** The directory node. */
	public String directoryNode;

	/** Member variable 'information node' for {@link String}. */
	public String informationNode;

	/** Member variable 'Not fixed node' for {@link String}. */
	public String notFixedNode;

	/** Member variable 'Fixed node' for {@link String}. */
	public String fixedNode;

	/** Member variable 'Protected node' for {@link String}. */
	public String protectedNode;

	/** Member variable 'Allowed node' for {@link String}. */
	public String allowedNode;

	/** Member variable 'Forbidden node' for {@link String}. */
	public String forbiddenNode;

	/**
	 * Member variable 'user child user applications node' for {@link String}.
	 */
	public String userChildUserApplicationsNode;

	/** Member variable 'user app allowed node' for {@link String}. */
	public String userAppAllowedNode;

	/** Member variable 'user app forbidden node' for {@link String}. */
	public String userAppForbiddenNode;

	/** Member variable 'status node' for {@link String}. */
	public String statusNode;

	/** Member variable 'history node' for {@link String}. */
	public String historyNode;
	
	/** The template node. */
	public String templateNode;

	/** Member variable 'object group disaply label' for {@link String}. */
	public String objectGroupDisaplyLabel;

	/** Member variable 'site group create label' for {@link String}. */
	public String siteGroupCreateLabel;

	/** Member variable 'admin area group create label' for {@link String}. */
	public String adminAreaGroupCreateLabel;

	/** Member variable 'user group create label' for {@link String}. */
	public String userGroupCreateLabel;

	/** Member variable 'project group create label' for {@link String}. */
	public String projectGroupCreateLabel;

	/** Member variable 'base app group create label' for {@link String}. */
	public String baseAppGroupCreateLabel;

	/** Member variable 'user app group create label' for {@link String}. */
	public String grpUserAppGroupCreateLabel;

	/** Member variable 'project app group create label' for {@link String}. */
	public String grpProjectAppGroupCreateLabel;

	/** Member variable 'event app group create label' for {@link String}. */
	public String eventAppGroupCreateLabel;

	/** Member variable 'start app group create label' for {@link String}. */
	public String startAppGroupCreateLabel;

	/** Member variable 'group model create label' for {@link String}. */
	public String groupModelCreateLabel;
	
	/** Member variable 'group model create label' for {@link String}. */
	public String groupProjectModelCreateLabel;
	
	/** The user app group create label. */
	public String userAppGroupCreateLabel;
	
	/** The project app group create label. */
	public String projectAppGroupCreateLabel;
	
	/** The directory group create label. */
	public String directoryGroupCreateLabel;
	
	/** Member variable 'role group create label' for {@link String}. */
	public String roleGroupCreateLabel;

	/** Member variable 'object group change label' for {@link String}. */
	public String objectGroupChangeLabel;

	/** Member variable 'object name label' for {@link String}. */
	public String objectNameLabel;
	
	/** The object user name label. */
	public String objectUserNameLabel;
	
	/** The object full name label. */
	public String objectFullNameLabel;
	
	/** Member variable 'object name language label' for {@link String}. */
	public String objectNameLanguageLabel;

	/** Member variable 'object description label' for {@link String}. */
	public String objectDescriptionLabel;

	/** Member variable 'object contact label' for {@link String}. */
	public String objectContactLabel;

	/** Member variable 'object email label' for {@link String}. */
	public String objectEmailLabel;

	/** Member variable 'object time label' for {@link String}. */
	public String objectExpiryLabel;
	
	/** The object time label. */
	public String objectTimeLabel;

	/** Member variable 'time max label' for {@link String}. */
	public String objectSecLabel;

	/** Member variable 'object remark label' for {@link String}. */
	public String objectRemarkLabel;

	/** Member variable 'object active label' for {@link String}. */
	public String objectActiveLabel;

	/** Member variable 'object parent label' for {@link String}. */
	public String objectParentLabel;

	/** Member variable 'object symbol label' for {@link String}. */
	public String objectSymbolLabel;

	/** Member variable 'object symbol btn tooltip' for {@link String}. */
	public String objectSymbolBtnTooltip;

	/** Member variable 'object position label' for {@link String}. */
	public String objectPositionLabel;

	/** Member variable 'object application label' for {@link String}. */
	public String objectApplicationLabel;

	/** Member variable 'save button text' for {@link String}. */
	public String saveButtonText;

	/** Member variable 'cancel button text' for {@link String}. */
	public String cancelButtonText;

	/** Member variable 'object translation link text' for {@link String}. */
	public String objectTranslationLinkText;

	/** Member variable 'object singleton label' for {@link String}. */
	public String objectSingletonLabel;

	/** Member variable 'object platform label' for {@link String}. */
	public String objectPlatformLabel;
	
	/** Member variable 'start Message Label' for {@link String}. */
	public String startMessageLabel;
	
	/** Member variable 'expiry date label' for {@link String}. */
	public String expiryDateLabel;

	/** Member variable 'object program label' for {@link String}. */
	public String objectProgramLabel;

	/** The createdby label. */
	public String remarkCreatedbyLabel;

	/** The date label. */
	public String remarkDateLabel;

	/** The user email label. */
	public String userEmailLabel;
	
	/** The user manager label. */
	public String userManagerLabel;

	/** The user tele phone num label. */
	public String userTelePhoneNumLabel;

	/** The user department label. */
	public String userDepartmentLabel;

	/** The smtp group label. */
	public String smtpGroupLabel;

	/** The smtp email label. */
	public String smtpEmailLabel;

	/** The smtp port label. */
	public String smtpPortLabel;

	/** The smtp server namelabel. */
	public String smtpServerNameLabel;
	
	/** The smtp password label. */
	public String smtpPasswordLabel;

	/** The ldap group label. */
	public String ldapGroupLabel;

	/** The ldap urllabel. */
	public String ldapUrllabel;
	
	/** The ldap user nameabel. */
	public String ldapUserNameLabel;
	
	/** The ldap passwordlabel. */
	public String ldapPasswordlabel;
	
	/** The ldap baselabel. */
	public String ldapBaselabel;

	/** The ldap port label. */
	public String ldapPortLabel;
	
	/** The ldap baselabel. */
	public String msglabel;
	
	/** The singleton app time group label. */
	public String singletonAppTimeGroupLabel;
	
	/** The no project popup group label. */
	public String noProjectPopupGroupLabel;
	
	/** The singleton app timelabel. */
	public String singletonAppTimelabel;
	
	/** The prject pop msglabel. */
	public String noProjectPopMsglabel;
	
	/** The singleton app time seconds label. */
	public String singletonAppTimeSecondsLabel;

	/** The project expiry group label. */
	public String projectExpiryGroupLabel;
	
	/** The project expiry dayspinner label. */
	public String projectExpiryDayspinnerLabel;
	
	/** The project grace day sppinner label. */
	public String projectGraceDaySppinnerLabel;
	
	/** The project expiry days label. */
	public String projectExpiryDaysLabel;
	
	/** The live msg config group label. */
	public String liveMsgConfigGroupLabel;
	
	/** The live msg config to label. */
	public String liveMsgConfigToLabel;
	
	/** The live msg config msg label. */
	public String liveMsgConfigMsgLabel;
	
	/** The live msg config popup label. */
	public String liveMsgConfigPopupLabel;
	
	/** The live msg config start date time label. */
	public String liveMsgConfigStartDateTimeLabel;
	
	/** The live msg config expiry date time label. */
	public String liveMsgConfigExpiryDateTimeLabel;
	
	/** The live msg config send btn. */
	public String liveMsgConfigSendBtn;
	
	/** The live msg config add btn. */
	public String liveMsgConfigAddBtn;
	
	 /** The live msg config subject lbl. */
 	public String liveMsgConfigSubjectLbl;
	 
	 /** The live msg config name lbl. */
 	public String liveMsgConfigNameLbl;
 	
 	/** The live msg group radio btn lbl. */
	 public String liveMsgGroupRadioBtnLbl;
	 
	 /** The live msg add obj radio btn lbl. */
 	public String liveMsgAddObjRadioBtnLbl;
	
	/** The notification site filter group. */
	public String siteFilterGroup;
	
	/** The notification admin area filter group. */
	public String adminAreaFilterGroup;
	
	/** The notification project filter group. */
	public String projectFilterGroup;
	
	/** The notification user filter group. */
	public String userFilterGroup;
	
	/** The notification filter label. */
	//public String filterLabel;
	
	/** The site filter label. */
	public String siteFilterLabel;
	
	/** The adminArea filter label. */
	public String adminAreaFilterLabel;
	
	/** The project filter label. */
	public String projectFilterLabel;
	
	/** The user filter label. */
	public String userFilterLabel;
	
	/** The all btn label. */
	public String allBtnLabel;
	
	/** The user group filter label. */
	public String userGroupFilterLabel;
	
	/** The project group filter label. */
	public String projectGroupFilterLabel;
	
	/** The notification add user radio btn lbl. */
	public String notificationAddUserRadioBtnLbl;
	
	/** The notification user group radio btn lbl. */
	public String notificationUserGroupRadioBtnLbl;
	
	/** The notification edit btn. */
	public String notificationEditBtn;
	
	/** The notification description lbl. */
	public String notificationDescriptionLbl;
	
	/** The notification to user lbl. */
	public String notificationToUserLbl;
	
	/** The notification event type lbl. */
	public String notificationEventTypeLbl;
	
	/** The notification message lbl. */
	public String notificationMessageLbl;
	
	/** The notification subject lbl. */
	public String notificationSubjectLbl;
	
	/** The notification include remark btn. */
	public String notificationIncludeRemarkBtn;
	
	/** The notification send to ass user btn. */
	public String notificationSendToAssUserBtn;
	
	/** The notification project exp notice btn. */
	public String notificationProjectExpNoticeBtn;
	
	/** The notification send btn. */
	public String notificationSendBtn;
	
	/** The notification add btn. */
	public String notificationAddBtn;
	
	/** The notification user error title. */
	public String notificationUserErrorTitle;
	
	/** The notification user error msg. */
	public String notificationUserErrorMsg;
	
	/** Member variable 'windows32' for {@link String}. */
	public String WINDOWS32;

	/** Member variable 'windows64' for {@link String}. */
	public String WINDOWS64;

	/** Member variable 'linux64' for {@link String}. */
	public String LINUX64;
	
	/** The search user user namelabel. */
	public String searchUserUserNamelabel;
	
	/** The search user dialog title. */
	public String searchUserDialogTitle;

	/** The search user ldap urllabel. */
	public String searchUserLdapUrllabel;

	/** The search user search btn. */
	public String searchUserSearchBtn;

	/** The search user group serach result. */
	public String searchUserGroupSerachResult;

	/** The search user property name col. */
	public String searchUserPropertyNameCol;

	/** The search user property value col. */
	public String searchUserPropertyValueCol;
	
	/** The search user error dialog. */
	public String searchUserErrorDialogTitle;
	
	/** The search user error dialog msg. */
	public String searchUserErrorDialogMsg;
	
	/** The search user alrady exist dialog title. */
	public String searchUserAlradyExistDialogTitle;
	
	/** The search user import btn lbl. */
	public String searchUserImportBtnLbl;
	
	/** The search user alrady exist dialog msg. */
	public String searchUserAlradyExistDialogMsg;
	
	/** Member variable 'info group label' for {@link String}. */
	public String infoGroupLabel;

	/** Member variable 'configuration node' for {@link String}. */
	public String configurationNode;
	
	/** Member variable 'admin menu node' for {@link String}. */
	public String adminMenuNode;

	/** Member variable 'icons node' for {@link String}. */
	public String iconsNode;

	/** Member variable 'roles node' for {@link String}. */
	public String rolesNode;
	
	/** The super admin node. */
	public String superAdminNode;

	/** The smtp config node. */
	public String smtpConfigNode;

	/** The ldap config node. */
	public String ldapConfigNode;

	/** The singleton app time node. */
	public String singletonAppTimeNode;
	
	/** The singleton app time node. */
	public String noPopMessageNode;
	
	/** The live msg config node. */
	public String liveMsgConfigNode;
	
	/** The notification node. */
	public String notificationNode;

	/** The project expiry node. */
	public String projectExpiryNode;
	
	/** Member variable 'add icon button' for {@link String}. */
	public String addIconButton;

	/** Member variable 'remove icon button' for {@link String}. */
	public String removeIconButton;

	/**
	 * Member variable 'browse script dialog tree script column label' for
	 * {@link String}.
	 */
	public String browseScriptDialogTreeScriptColumnLabel;

	/**
	 * Member variable 'browse script dialog tree date column label' for
	 * {@link String}.
	 */
	public String browseScriptDialogTreeDateColumnLabel;

	/**
	 * Member variable 'browseScriptDialogBrowseBtn for {@link String}.
	 */
	public String browseScriptDialogBrowseBtn;

	/**
	 * Member variable 'browseScriptDialogClearLnk' for {@link String}.
	 */
	public String browseScriptDialogClearLnk;
	
	/**
	 * Member variable 'icontableviewer first column label' for {@link String}.
	 */
	public String icontableviewerFirstColumnLabel;

	/**
	 * Member variable 'icontableviewer second column label' for {@link String}.
	 */
	public String icontableviewerSecondColumnLabel;

	/** Member variable 'browse icon dialog title' for {@link String}. */
	public String browseIconDialogTitle;

	/** Member variable 'role group lable' for {@link String}. */
	public String roleGroupLable;

	/** Member variable 'role name lable' for {@link String}. */
	public String roleNameLable;
	
	/** Member variable 'role view inactive lable' for {@link String}. */
	public String roleViewInactiveLable;
	
	/** The role obj permission lable. */
	public String roleObjPermissionLable;
	
	/** The role obj relation permission lable. */
	public String roleObjRelationPermissionLable;
	
	/** The permission obj type lable. */
	public String permissionObjTypeLable;
	
	/** The permission create col lable. */
	public String permissionCreateColLable;
	
	/** The permission change col lable. */
	public String permissionChangeColLable;
	
	/** The permission delete col lable. */
	public String permissionDeleteColLable;
	
	/** The permission activate col lable. */
	public String permissionActivateColLable;
	
	/** The permission view inactivr col lable. */
	public String permissionViewInactivrColLable;
	
	/** The permission assign col lable. */
	public String permissionAssignColLable;
	
	/** The permission remove col lable. */
	public String permissionRemoveColLable;
	
	/** The permission inactive assi col lable. */
	public String permissionInactiveAssiColLable;

	/** Member variable 'object type lable' for {@link String}. */
	public String objectTypeLable;

	/** Member variable 'description lable' for {@link String}. */
	public String descriptionLable;
	
	/** Member variable 'existing role name title' for {@link String}. */
	public String existingRoleNameTitle;

	/** Member variable 'existing role name error' for {@link String}. */
	public String existingRoleNameError;
	
	/** The notification message err title. */
	public String notificationMessageErrTitle;
	
	/** The notification message err msg. */
	public String notificationMessageErrMsg;
	
	/** The notification subject err title. */
	public String notificationSubjectErrTitle;
	
	/** The notification subject err msg. */
	public String notificationSubjectErrMsg;
	
	/** The notification send err tiltle. */
	public String notificationSendErrTiltle;
	
	/** The notification send err msg. */
	public String notificationSendErrMsg;
	
	/** The notifi message deco title. */
	public String notifiMessageDecoTitle;
	
	/** The notifi message deco user name. */
	public String notifiMessageDecoUserName;
	
	/** The notifi message deco project name. */
	public String notifiMessageDecoProjectName;
	
	/** The notifi message deco assigned user name. */
	public String notifiMessageDecoAssignedUserName;
	
	/** The notifi message deco project expiry days. */
	public String notifiMessageDecoProjectExpiryDays;
	
	/** The notifi message deco project grace days. */
	public String notifiMessageDecoProjectGraceDays;
	
	/** The notifi message deco admin area name. */
	public String notifiMessageDecoAdminAreaName;
	
	/** The notifi message deco project create date. */
	public String notifiMessageDecoProjectCreateDate;
	
	/** The notifi message deco expire project name. */
	public String notifiMessageDecoExpireProjectName;
	
	/** The notifi message deco assign project name. */
	public String notifiMessageDecoAssignProjectName;
	
	/** The notification invalid msg dialog msg. */
	public String notificationInvalidMsgDialogMsg;
	
	/** The delete multi objects confirm dialog msg. */
	public String deleteMultiObjectsConfirmDialogMsg;
	
	/** The remove multi objects rel confirm dialog msg. */
	public String removeMultiObjectsRelConfirmDialogMsg;
	
	/** Member variable 'delete site confirm dialog msg' for {@link String}. */
	public String deleteSiteConfirmDialogMsg;
	
	/** The delete site relation confirm dialog msg part 1. */
	public String deleteSiteRelationConfirmDialogMsgPart1;
	
	/** The delete site relation confirm dialog msg part 2. */
	public String deleteSiteRelationConfirmDialogMsgPart2; 
	
	/** The delete AA relation confirm dialog msg part 1. */
	public String deleteAARelationConfirmDialogMsgPart1;
	
	/** The delete AA relation confirm dialog msg part 2. */
	public String deleteAARelationConfirmDialogMsgPart2; 
	
	/** The delete user confirm dialog msg part 1. */
	public String deleteUserConfirmDialogMsgPart1;
	
	/** The delete user confirm dialog msg part 2. */
	public String deleteUserConfirmDialogMsgPart2;
	
	/** The delete project confirm dialog msg part 1. */
	public String deleteProjectConfirmDialogMsgPart1;
	
	/** The delete project confirm dialog msg part 1. */
	public String deleteProjectConfirmDialogMsgPart2;
	
	/** The delete project app confirm dialog msg part 1. */
	public String deleteProjectAppConfirmDialogMsgPart1;
	
	/** The delete project app confirm dialog msg part 2. */
	public String deleteProjectAppConfirmDialogMsgPart2;
	
	/** The delete start app confirm dialog msg part 1. */
	public String deleteStartAppConfirmDialogMsgPart1;
	
	/** The delete start app confirm dialog msg part 2. */
	public String deleteStartAppConfirmDialogMsgPart2;
	
	/** The delete user app confirm dialog msg part 1. */
	public String deleteUserAppConfirmDialogMsgPart1;
	
	/** The delete user app confirm dialog msg part 2. */
	public String deleteUserAppConfirmDialogMsgPart2;
	
	/** The delete base app confirm dialog msg part 1. */
	public String deleteBaseAppConfirmDialogMsgPart1;
	
	/** The delete base app confirm dialog msg part 2. */
	public String deleteBaseAppConfirmDialogMsgPart2;
	
	/** The delete site relation msg. */
	public String deleteSiteRelationMsg;
	
	/** The delete admin area relation msg. */
	public String deleteAdminAreaRelationMsg;
	
	/** The delete project relation msg. */
	public String deleteProjectRelationMsg;
	
	/** The delete Admin relation msg. */
	public String deleteAdminMenuRelationMsg;
	
	/** The delete directory msg. */
	public String deleteDirectoryMsg;
	
	/** The delete user relation msg. */
	public String deleteUserRelationMsg;
	
	/** The delete user relation msg. */
	public String deleteRoleRelationMsg;
	
	/** The delete user group relation msg. */
	public String deleteUserGroupRelationMsg;
	
	/** The delete project group relation msg. */
	public String deleteProjectGroupRelationMsg;
	
	/** The delete user app group relation msg. */
	public String deleteUserAppGroupRelationMsg;
	
	/** The delete project app group relation msg. */
	public String deleteProjectAppGroupRelationMsg;
	
	/**
	 * Member variable 'delete admin area confirm dialog msg' for
	 * {@link String}.
	 */
	public String deleteAdminAreaConfirmDialogMsg;

	/** Member variable 'delete user confirm dialog msg' for {@link String}. */
	public String deleteUserConfirmDialogMsg;

	/**
	 * Member variable 'delete project confirm dialog msg' for {@link String}.
	 */
	public String deleteProjectConfirmDialogMsg;

	/**
	 * Member variable 'delete base app confirm dialog msg' for {@link String}.
	 */
	public String deleteBaseAppConfirmDialogMsg;
	
	/** The delete base app relation dialog msg part 1. */
	public String deleteBaseAppRelationDialogMsgPart1;
	
	
	/** The delete base app relation dialog msg part 2. */
	public String deleteBaseAppRelationDialogMsgPart2;
	
	/** The delete base app relation dialog msg. */
	
	public String deleteBaseAppRelationDialogMsg;

	/**
	 * Member variable 'delete user app confirm dialog msg' for {@link String}.
	 */
	public String deleteUserAppConfirmDialogMsg;

	/**
	 * Member variable 'delete project app confirm dialog msg' for
	 * {@link String}.
	 */
	public String deleteProjectAppConfirmDialogMsg;

	/**
	 * Member variable 'delete event app confirm dialog msg' for {@link String}.
	 */
	public String deleteEventAppConfirmDialogMsg;

	/**
	 * Member variable 'delete start app confirm dialog msg' for {@link String}.
	 */
	public String deleteStartAppConfirmDialogMsg;

	/** Member variable 'delete group confirm dialog msg' for {@link String}. */
	public String deleteUserGroupConfirmDialogMsg;
	
	/** Member variable 'delete group confirm dialog msg' for {@link String}. */
	public String deleteProjectGroupConfirmDialogMsg;
	
	/** Member variable 'delete group confirm dialog msg' for {@link String}. */
	public String deleteUserAppGroupConfirmDialogMsg;
	
	/** Member variable 'delete group confirm dialog msg' for {@link String}. */
	public String deleteProjectAppGroupConfirmDialogMsg;
	
	/** The delete directory confirm dialog msg. */
	public String deleteDirectoryConfirmDialogMsg;

	/** Member variable 'delete confirm dialog title' for {@link String}. */
	public String deleteConfirmDialogTitle;

	/**
	 * Member variable 'change status confirm dialog title' for {@link String}.
	 */
	public String changeStatusConfirmDialogTitle;

	/**
	 * Member variable 'activatechange status confirm dialog msg' for
	 * {@link String}.
	 */
	public String activatechangeStatusConfirmDialogMsg;
	
	/** The activate multichange status confirm dialog msg. */
	public String activateMultichangeStatusConfirmDialogMsg;
	
	/** The de activate multichange status confirm dialog msg. */
	public String deActivateMultichangeStatusConfirmDialogMsg;

	/**
	 * Member variable 'de activatechange status confirm dialog msg' for
	 * {@link String}.
	 */
	public String deActivatechangeStatusConfirmDialogMsg;

	/** Member variable 'delete role confirm dialog msg' for {@link String}. */
	public String deleteRoleConfirmDialogMsg;

	/** The delete live msg confirm dialog msg. */
	public String deleteLiveMsgConfirmDialogMsg; 
	/**
	 * Member variable 'user import wizard auth fail dialog title' for
	 * {@link String}.
	 */
	public String userImportWizardAuthFailDialogTitle;

	/**
	 * Member variable 'user import wizard auth fail dialog msg' for
	 * {@link String}.
	 */
	public String userImportWizardAuthFailDialogMsg;

	/**
	 * Member variable 'userImportWizardEnterPasswordMsg' for {@link String}.
	 */
	public String userImportWizardEnterPasswordMsg;
	/**
	 * Member variable 'userImportWizardUnalbefindUserMasg' for {@link String}.
	 */
	public String userImportWizardUnalbefindUserMasg;

	/** Member variable 'browse script dialog title' for {@link String}. */
	public String browseScriptDialogTitle;
	
	/** Member variable 'expiry days dialog title' for {@link String}. */
	public String expirydaysDialogTitle;
	
	/** The object permission dialog title. */
	public String objectPermissionDialogTitle;
	
	/** The object permission dialog msg. */
	public String objectPermissionDialogMsg;
	
	/** The add icon dialog title. */
	public String addIconDialogTitle;

	/** The add icon dialog msg. */
	public String addIconDialogMsg;
	
	/** The remove icon dialog title. */
	public String removeIconDialogTitle;
	
	/** The remove icon dialog msg. */
	public String removeIconDialogMsg;

	/** Member variable 'name error title' for {@link String}. */
	public String nameErrorTitle;

	/** Member variable 'name error' for {@link String}. */
	public String nameError;
	
	/** Member variable 'name error' for {@link String}. */
	public String emptyCommonNameValidateErr;
	
	/** Member variable 'emailNumber error' for {@link String}. */
	public String emailValidateErr;

	/** Member variable 'name error' for {@link String}. */
	public String emptyNameValidateErr;
	
	/** Member variable 'name error' for {@link String}. */
	public String emptyTimeSpinnerValidateErr;
	
	/** The info time spinner validate. */
	public String infoTimeSpinnerValidate;
	
	/** Member variable 'name error' for {@link String}. */
	public String emptyNoProjectMessageValidateErr;
	
	/** The empty notification message err. */
	public String emptyNotificationMessageErr;
	
	/** The empty notification subject err. */
	public String emptyNotificationSubjectErr;
	
	/** The delete multi objectsm dialog err msg. */
	public String deleteMultiObjectsDialogErrMsg;
	
	/** The activate multi object dilaog err msg. */
	public String multiStatusUpadteObjectDilaogErrMsg;
	
	/** Member variable 'contact error' for {@link String}. */
	public String emptyContactError;

	/** Member variable 'name error' for {@link String}. */
	public String emptyEmailError;
	
	/** Member variable 'name error' for {@link String}. */
	public String emptyPortError;
	
	/** Member variable 'name error' for {@link String}. */
	public String emptyUrlError;
	
	/** Member variable 'name error' for {@link String}. */
	public String emptyPasswordError;

	/** Member variable 'name error' for {@link String}. */
	public String emptyServerNameError;
	
	/** Member variable 'basename error' for {@link String}. */
	public String emptyBaseNameError;
	
	/** Member variable 'username error' for {@link String}. */
	public String emptyldapUserNameError;
	
	/** Member variable 'name error' for {@link String}. */
	public String specialCharValidateErr;

	/** Member variable 'name error' for {@link String}. */
	public String whitespaceOrSpeCharValidateErr;

	/** Member variable 'contactNumber error' for {@link String}. */
	public String contactNumberValidateErr;
	
	/** Member variable 'contactNumber error' for {@link String}. */
	public String portValidateErr;

	/** The ldap url validate err. */
	public String ldapUrlValidateErr;
	
	/** Member variable 'symbol error title' for {@link String}. */
	public String symbolErrorTitle;

	/** Member variable 'symbol error' for {@link String}. */
	public String symbolError;

	/** Member variable 'name symbol error title' for {@link String}. */
	public String nameSymbolErrorTitle;

	/** Member variable 'name symbol error' for {@link String}. */
	public String nameSymbolError;

	/** Member variable 'existing site name error' for {@link String}. */
	public String existingSiteNameError;

	/** Member variable 'existing site name title' for {@link String}. */
	public String existingSiteNameTitle;

	/** Member variable 'existing admin area name error' for {@link String}. */
	public String existingAdminAreaNameError;

	/** Member variable 'existing admin area name title' for {@link String}. */
	public String existingAdminAreaNameTitle;

	/** Member variable 'existing user name error' for {@link String}. */
	public String existingUserNameError;

	/** Member variable 'existing user name title' for {@link String}. */
	public String existingUserNameTitle;

	/** Member variable 'existing project name error' for {@link String}. */
	public String existingProjectNameError;

	/** Member variable 'existing project name title' for {@link String}. */
	public String existingProjectNameTitle;

	/** Member variable 'existing group name title' for {@link String}. */
	public String existingUserGroupNameTitle;

	/** Member variable 'existing group name error' for {@link String}. */
	public String existingUserGroupNameError;
	
	/** Member variable 'existing group name title' for {@link String}. */
	public String existingProjectGroupNameTitle;

	/** Member variable 'existing group name error' for {@link String}. */
	public String existingProjectGroupNameError;
	
	/** Member variable 'existing group name title' for {@link String}. */
	public String existingUserAppGroupNameTitle;

	/** Member variable 'existing group name error' for {@link String}. */
	public String existingUserAppGroupNameError;
	
	/** Member variable 'existing group name title' for {@link String}. */
	public String existingProjectAppGroupNameTitle;

	/** Member variable 'existing group name error' for {@link String}. */
	public String existingProjectAppGroupNameError;

	/** The existing directory name title. */
	public String existingDirectoryNameTitle;
	
	/** The existing directory name error. */
	public String existingDirectoryNameError;
	
	/** The existing live msg name title. */
	public String existingLiveMsgNameTitle;
	
	/** The existing live msg name error. */
	public String existingLiveMsgNameError;
	
	/** Member variable 'app name error title' for {@link String}. */
	public String appNameErrorTitle;
	
	/** The supported platf error title. */
	public String supportedPlatfErrorTitle;

	/** Member variable 'app name empty en error' for {@link String}. */
	public String appNameEmptyEnError;
	
	/** The supproted pltf empty en error. */
	public String supprotedPltfEmptyEnError;

	/** Member variable 'existing en name error' for {@link String}. */
	public String existingEnNameError;

	/** Member variable 'existing de name error' for {@link String}. */
	public String existingDeNameError;
	
	/** The app postion empty error. */
	public String appPostionEmptyError;
	
	/** The app program empty error. */
	public String appProgramEmptyError;
	
	/** The app base app empty error. */
	public String appBaseAppEmptyError;
	
	/** The app child position error. */
	public String appChildPositionError;

	/** The dnd error title. */
	public String dndErrorTitle;

	/** The admin area error message prefix. */
	public String adminAreaErrorMessagePrefix;

	/** The unauthorized dialog msg. */
	public String unauthorizedDialogMsg;
	
	/** The unauthorized dialog title. */
	public String unauthorizedDialogTitle;
	
	/** The admin area dnd error msg part 1. */
	public String adminAreaDndErrorMsgPart1;
	
	/** The admin area dnd error msg part 2. */
	public String adminAreaDndErrorMsgPart2;
	
	/** The dnd status bar msg. */
	public String dndStatusBarMsg;
	
	/** The dnd empty selection msg. */
	public String dndEmptySelectionMsg;
	
	/** The dnd template dialog title. */
	public String dndTemplateDialogTitle;
	
	/** The dnd template confirm msg. */
	public String dndTemplateConfirmMsg;

	/** Member variable 'app base app error title' for {@link String}. */
	public String appBaseAppErrorTitle;

	/** Member variable 'app base app error' for {@link String}. */
	public String appBaseAppError;
	
	/** The start app expiry date not set error title. */
	public String startAppExpiryDateNotSetErrorTitle;
	
	/** The live msg invalid expiry date msg. */
	public String liveMsgInvalidExpiryDateMsg; 
	
	/** The start app expiry date not set error. */
	public String startAppExpiryDateNotSetError;

	/** Member variable 'app position error title' for {@link String}. */
	public String appPositionErrorTitle;
	
	/** The app program error title. */
	public String appProgramErrorTitle;

	/** Member variable 'app position error' for {@link String}. */
	public String appPositionError;

	/** Member variable 'parent enable info title' for {@link String}. */
	public String parentEnableInfoTitle;
	
	/** Member variable 'message enable info title' for {@link String}. */
	public String messageEnableInfoTitle;

	/** Member variable 'parent enable info' for {@link String}. */
	public String parentEnableInfo;

	/** Member variable 'message enable info' for {@link String}. */
	public String messageEnableInfo;
	
	/** Member variable 'translation dialog title' for {@link String}. */
	public String translationDialogTitle;

	/** Member variable 'translation lang label' for {@link String}. */
	public String translationLangLabel;

	/** Member variable 'translation lang label txt EN' for {@link String}. */
	public String translationLangLabelTxtEN;

	/** Member variable 'translation lang label txt DE' for {@link String}. */
	public String translationLangLabelTxtDE;

	/** Member variable 'popupmenulabelnode active' for {@link String}. */
	public String popupmenulabelnodeActive;

	/** Member variable 'popupmenulabelnode de active' for {@link String}. */
	public String popupmenulabelnodeDeActive;
	
	/** The popupmenulabelnode remove relation. */
	public String popupmenulabelnodeRemoveRelation;
	
	/** The popupmenuExpiryDays remove relation. */
	public String popupmenuExpiryDays;

	/** Member variable 'dirty dialog title' for {@link String}. */
	public String dirtyDialogTitle;

	/** Member variable 'dirty dialog message' for {@link String}. */
	public String dirtyDialogMessage;
	
	/** The select all btn. */
	public String userReviewSelectAllBtn;
	
	/** The de select all btn. */
	public String userReviewDeSelectAllBtn;
	
	/** The de activate btn. */
	public String userReviewDeActivateBtn;
	
	/** The delete btn. */
	public String userReviewDeleteBtn;
	
	/** The user name col. */
	public String userReviewUserNameCol;
	
	/** The ldap status col. */
	public String userReviewLdapStatusCol;
	
	/** The xm system col. */
	public String userReviewXmSystemStatusCol;
	
	/** The objects selected. */
	public String objectsSelected;
	
	/** The live msg start date error title. */
	public String liveMsgStartDateErrorTitle;
	
	/** The notification variable lbl. */
	public String notificationVariableLbl;
	
	/** The notification CC lbl. */
	public String notificationCCLbl;
	
	/** The noti add to btn. */
	public String notiAddToBtn;
	
	/** The noti add to CC btn. */
	public String notiAddToCCBtn;
	
	/** The noti template lbl. */
	public String notiTemplateLbl;
	
	/** The noti user filte radio btn lbl. */
	public String notiUserFilteRadioBtnLbl;
	
	/** The noti user grp filte radio btn lbl. */
	public String notiUserGrpFilteRadioBtnLbl;
	
	/** The noti invalid template msg. */
	public String notiInvalidTemplateMsg;
	
	/** The noti invalid to variable msg. */
	public String notiInvalidToVariableMsg;
	
	/** The noti invalid CC variable msg. */
	public String notiInvalidCCVariableMsg;
	
	/** The existing notification name error. */
	public String existingNotificationNameError;
	
	/** The noti empty template error. */
	public String notiEmptyTemplateError;
	
	/** The live msg start date error msg. */
	public String liveMsgStartDateErrorMsg;
	
	/** The live msg expiry date error title. */
	public String liveMsgExpiryDateErrorTitle;
	
	/** The live msg expiry date error msg. */
	public String liveMsgExpiryDateErrorMsg;
	
	/** The live msg invalid obj name error title. */
	public String liveMsgInvalidObjNameErrorTitle;
	
	/** The live msg invalid obj name error msg. */
	public String liveMsgInvalidObjNameErrorMsg;
	
	/** The live msg invalid remove error tilte. */
	public String liveMsgInvalidRemoveErrorTilte;
	
	/** The live msg invalid remove error msg. */
	public String liveMsgInvalidRemoveErrorMsg;
	
	/** The live msg obj remove error msg. */
	public String liveMsgObjRemoveErrorMsg;
	
	/** The live msg invalid end date err msg. */
	public String liveMsgInvalidEndDateErrMsg;
	
	/** The live msg invalid obj combination msg part 1. */
	public String liveMsgInvalidObjCombinationMsgPart1;
	
	/** The live msg invalid obj combination msg part 2. */
	public String liveMsgInvalidObjCombinationMsgPart2;
	
	/** The live msg invalid obj combination title. */
	public String liveMsgInvalidObjCombinationTitle;
	
	/** Member variable 'history reload' for {@link String}. */
	public String historyReload;
	
	/** The history export btn. */
	public String historyExportBtn;
	
	/** The history limit lnl. */
	public String historyLimitLbl;
	
	/** The history default btn. */
	public String historyDefaultBtn;
	
	/** The history apply btn. */
	public String historyApplyBtn;
	
	/** The history log time label. */
	public String historyLogTimeLabel;
	
	/** The history start date label. */
	public String historyStartDateLabel;
	
	/** The history end date label. */
	public String historyEndDateLabel;
	
	/** The history id label. */
	public String historyIdLabel;
	
	/** The user history agrs lbl. */
	public String userHistoryAgrsLbl;
	
	/** The user history PID lbl. */
	public String userHistoryPIDLbl;
	
	/** The User history dialog title. */
	public String userHistoryDialogTitle;
	
	/** The user history filter label. */
	public String userHistoryFilterLabel;
	
	/** The user status filter lbl. */
	public String userStatusFilterLbl;
	
	/** The user history btn. */
	public String userHistoryBtn;
	
	/** The user status btn. */
	public String userStatusBtn;
	
	/** The user history user name lbl. */
	public String userHistoryUserNameLbl;
	
	/** The user history host lbl. */
	public String userHistoryHostLbl;
	
	/** The user history site lbl. */
	public String userHistorySiteLbl;
	
	/** The user history admin area lbl. */
	public String userHistoryAdminAreaLbl;
	
	/** The user history project lbl. */
	public String userHistoryProjectLbl;
	
	/** The user history application lbl. */
	public String userHistoryApplicationLbl;
	
	/** The user history result lbl. */
	public String userHistoryResultLbl;

	/** Member variable 'content prop decoration info' for {@link String}. */
	public String contentPropDecorationInfo;

	/** Member variable 'content prop control message' for {@link String}. */
	public String contentPropControlMessage;
	
	/** The activate relchange status confirm dialog msg. */
	public String activateRelchangeStatusConfirmDialogMsg;
	
	/** The de activate relchange status confirm dialog msg. */
	public String deActivateRelchangeStatusConfirmDialogMsg;
	
	/** The activate multi relchange status confirm dialog msg. */
	public String activateMultiRelchangeStatusConfirmDialogMsg;
	
	/** The de activate multi relchange status confirm dialog msg. */
	public String deActivateMultiRelchangeStatusConfirmDialogMsg;

	/** The icon error title. */
	public String iconErrorTitle;

	/** The icon errormsg. */
	public String iconErrorMsg;
	
	/** The info display msg. */
	public String infoDisplayMsg;
	
	/** Member variable 'role xm admin lable' for {@link String}. */
	public String roleXmAdminLable;

	/** Member variable 'role xm hotline lable' for {@link String}. */
	public String roleXmHotlineLable;

	/** Member variable 'permission checkbox edit msg lbl' for {@link String}. */
	public String permissionCheckboxEditMsgLbl;
	
	/** The error dialog titile. */
	public String errorDialogTitile;
	
	/** The unauthorized ldap access message. */
	public String unauthorizedLdapAccessMessage;
	
	/** The history limit error msg. */
	public String historyLimitErrorMsg;
	
	/** The history empty limit msg. */
	public String historyEmptyLimitMsg;
	
	/** The unauthorized user access message. */
	public String unauthorizedUserAccessMessage;
	
	/** The dialog app name. */
	public String dialogAppName;
	
	/** The server not reachable. */
	public String serverNotReachable;
	
	/**  The Admin history dialog title. */
	public String adminHistoryDialogTitle;
	
	/** The admin history rel filter label. */
	public String adminHistoryRelFilterLabel;
	
	/** The admin history base filter label. */
	public String adminHistoryBaseFilterLabel;
	
	/** The admin history rel radio btn. */
	public String adminHistoryRelRadioBtn;
	
	/** The admin history base obj btn. */
	public String adminHistoryBaseObjBtn;
	
	/** The admin history admin name. */
	public String adminHistoryAdminName;
	
	/** The admin history operation. */
	public String adminHistoryOperation;
	
	/** The admin history admin area. */
	public String adminHistoryAdminArea;
	
	/** The admin history project. */
	public String adminHistoryProject;
	
	/** The admin history project app. */
	public String adminHistoryProjectApp;
	
	/** The admin history user name. */
	public String adminHistoryUserName;
	
	/** The admin history start application. */
	public String adminHistoryStartApplication;
	
	/** The admin history relation type. */
	public String adminHistoryRelationType;
	
	/** The admin history status. */
	public String adminHistoryStatus;
	
	/** The admin history site. */
	public String adminHistorySite;
	
	/** The admin history group name. */
	public String adminHistoryGroupName;
	
	/** The admin history user application. */
	public String adminHistoryUserApplication;
	
	/** The admin history directory. */
	public String adminHistoryDirectory;
	
	/** The admin history role. */
	public String adminHistoryRole;
	
	/** The admin history limit. */
	public String adminHistoryLimit;
	
	/** The admin history changes. */
	public String adminHistoryChanges;
	
	/** The admin history error message. */
	public String adminHistoryErrorMessage;
	
	/** The admin history result. */
	public String adminHistoryResult;
	
	/** The admin history object name. */
	public String adminHistoryObjectName;
	
	/** The admin history api path lbl. */
	public String adminHistoryApiPathLbl;
	
	/** The hotline contact error. */
	public String hotlineContactError;
	
	/** The hotline contact error title. */
	public String hotlineContactErrorTitle;
	
	/** The hotline email ID error title. */
	public String hotlineEmailIDErrorTitle;
	
	/** The hotline email ID error. */
	public String hotlineEmailIDError;
	
	/** The about dialog title. */
	public String aboutDialogTitle;
	
	/** The exit dialog title. */
	public String exitDialogTitle;
	
	/** The exit dialog message. */
	public String exitDialogMessage;
	
	/** The login dialog user name. */
	public String loginDialogUserName;
	
	/** The login dialog password. */
	public String loginDialogPassword;
	
	/** The login dialog login btn. */
	public String loginDialogLoginBtn;
	
	/** The login dialog cancel btn. */
	public String loginDialogCancelBtn;
	
	/** The user Review Dialog Title. */
	public String userReviewDialogTitle;
	
	/** The user Review Dialog Msg. */
	public String userReviewDialogMsg;
	
	/** The user Review Error Dialog Msg. */
	public String userReviewErrorDialogMsg;
	
	/** The user Review Error Dialog Title. */
	public String userReviewErrorDialogTitle;
	
	/** The statusbar from lbl. */
	public String statusbarFromLbl;
	
	/** The status bar in lbl. */
	public String statusBarInLbl;
	
	/** The status bar selected lbl. */
	public String statusBarSelectedLbl;
	
	/** The status bar found lbl. */
	public String statusBarFoundLbl;
	
	/** The object delete. */
	public String objectDelete;
	
	/** The object create. */
	public String objectCreate;
	
	/** The object update. */
	public String objectUpdate;
	
	/** The object status upadte. */
	public String objectStatusUpdate;
	
	/** The relation remove. */
	public String relationRemove;
	
	/** The user import. */
	public String userImport;
	
	/** The add icon. */
	public String addIcon;
	
	/** The site object. */
	public String siteObject;
	
	/** The administration area object. */
	public String administrationAreaObject;
	
	/** The user object. */
	public String userObject;
	
	/** The project object. */
	public String projectObject;
	
	/** The user application object. */
	public String userApplicationObject;
	
	/** The project application object. */
	public String projectApplicationObject;
	
	/** The event application object. */
	public String eventApplicationObject;
	
	/** The base application object. */
	public String baseApplicationObject;
	
	/** The start application object. */
	public String startApplicationObject;
	
	/** The user group object. */
	public String userGroupObject;
	
	/** The project group object. */
	public String projectGroupObject;
	
	/** The user app group object. */
	public String userAppGroupObject;
	
	/** The project app group object. */
	public String projectAppGroupObject;
	
	/** The directory object. */
	public String directoryObject;
	
	/** The icon object. */
	public String iconObject;
	
	/** The role object. */
	public String roleObject;
	
	/** The live msg config object. */
	public String liveMsgConfigObject;
	
	/** The template object. */
	public String templateObject;
	
	/** The relations lbl. */
	public String relationsLbl;
	
	/** The relation lbl. */
	public String relationLbl;
	
	/** The between lbl. */
	public String betweenLbl;
	
	/** The and lbl. */
	public String andLbl;
	
	/** The with lbl. */
	public String withLbl;
	
	/** The relation status lbl. */
	public String relationStatusLbl;
	
	/** The delete error msg. */
	public String deleteErrorMsg;
	
	/** The for lbl. */
	public String forLbl;
	
	/** The german language change msg. */
	public String germanLanguageChangeMsg;
	
	/** The english language change msg. */
	public String englishLanguageChangeMsg;
	
	/** The action lbl. */
	public String actionLbl;
	
	/** The assigned lbl. */
	public String assignedLbl;
	
	/** The successfull lbl. */
	public String successfullyLbl;
	
	/** The event lbl. */
	public String eventLbl;
	
	/** Member variable 'workspace lock title' for {@link String}. */
	public String workspaceLockTitle;
	
	/** Member variable 'workspace lock msg' for {@link String}. */
	public String workspaceLockMsg;
	
	/** The icon already exist part 1. */
	public String iconAlreadyExistPart1;
	
	/** The icon already exist part 2. */
	public String iconAlreadyExistPart2;
	
	/** The delete template confirm dialog msg. */
	public String deleteTemplateConfirmDialogMsg;
	
	/** The delete noti action confirm dialog msg. */
	public String deleteNotiActionConfirmDialogMsg;
	
	/** The in correct server URI. */
	public String inCorrectServerURI;
	
	/**
	 * Method for Format.
	 */
	@PostConstruct
	public void format() {
		String resourceBundles = "ResourceBundles";
		welcomePageMessage = MessageFormat.format(welcomePageMessage, resourceBundles); // $NON-NLS-1$
		adminStartedMessage = MessageFormat.format(adminStartedMessage, resourceBundles); // $NON-NLS-1$
		rightSideCMlabelNodeCreateAs = MessageFormat.format(rightSideCMlabelNodeCreateAs, resourceBundles); // $NON-NLS-1$
		rightSideCMlabelNodeChange = MessageFormat.format(rightSideCMlabelNodeChange, resourceBundles); // $NON-NLS-1$
		rightSideCMlabelNodeDelete = MessageFormat.format(rightSideCMlabelNodeDelete, resourceBundles); // $NON-NLS-1$
		rightSideCMlabelNodeGoTo = MessageFormat.format(rightSideCMlabelNodeGoTo, resourceBundles); // $NON-NLS-1$
		windowTitle = MessageFormat.format("windowtitle.label", resourceBundles); // $NON-NLS-1$
		treeGroup = MessageFormat.format(treeGroup, resourceBundles); // $NON-NLS-1$
		treeLoading = MessageFormat.format(treeLoading, resourceBundles); // $NON-NLS-1$
		sitesNode = MessageFormat.format(sitesNode, resourceBundles); // $NON-NLS-1$
		administrationAreasNode = MessageFormat.format(administrationAreasNode, resourceBundles); // $NON-NLS-1$
		usersNode = MessageFormat.format(usersNode, resourceBundles); // $NON-NLS-1$
		projectsNode = MessageFormat.format(projectsNode, resourceBundles); // $NON-NLS-1$
		groupsNode = MessageFormat.format(groupsNode, resourceBundles); // $NON-NLS-1$
		userGroupsNode = MessageFormat.format(userGroupsNode, resourceBundles);// $NON-NLS-1$
		projectGroupsNode = MessageFormat.format(projectGroupsNode, resourceBundles);// $NON-NLS-1$
		userAppGroupsNode = MessageFormat.format(userAppGroupsNode, resourceBundles);// $NON-NLS-1$
		projectAppGroupsNode = MessageFormat.format(projectAppGroupsNode, resourceBundles);// $NON-NLS-1$
		directoryNode = MessageFormat.format(directoryNode, resourceBundles); // $NON-NLS-1$
		informationNode = MessageFormat.format(informationNode, resourceBundles); // $NON-NLS-1$
		notFixedNode = MessageFormat.format(notFixedNode, resourceBundles); // $NON-NLS-1$
		fixedNode = MessageFormat.format(fixedNode, resourceBundles); // $NON-NLS-1$
		protectedNode = MessageFormat.format(protectedNode, resourceBundles); // $NON-NLS-1$
		allowedNode = MessageFormat.format(allowedNode, resourceBundles); // $NON-NLS-1$
		forbiddenNode = MessageFormat.format(forbiddenNode, resourceBundles); // $NON-NLS-1$
		statusNode = MessageFormat.format(statusNode, resourceBundles); // $NON-NLS-1$
		historyNode = MessageFormat.format(historyNode, resourceBundles); // $NON-NLS-1$
		templateNode = MessageFormat.format(templateNode, resourceBundles); // $NON-NLS-1$
		applicationsNode = MessageFormat.format(applicationsNode, resourceBundles); // $NON-NLS-1$
		userApplicationsNode = MessageFormat.format(userApplicationsNode, resourceBundles); // $NON-NLS-1$
		projectApplicationsNode = MessageFormat.format(projectApplicationsNode, resourceBundles); // $NON-NLS-1$
		eventApplicationsNode = MessageFormat.format(eventApplicationsNode, resourceBundles); // $NON-NLS-1$
		baseApplicationsNode = MessageFormat.format(baseApplicationsNode, resourceBundles); // $NON-NLS-1$
		startApplicationsNode = MessageFormat.format(startApplicationsNode, resourceBundles); // $NON-NLS-1$

		configurationNode = MessageFormat.format(configurationNode, resourceBundles); // $NON-NLS-1$
		adminMenuNode = MessageFormat.format(adminMenuNode, resourceBundles); // $NON-NLS-1$
		iconsNode = MessageFormat.format(iconsNode, resourceBundles); // $NON-NLS-1$
		rolesNode = MessageFormat.format(rolesNode, resourceBundles); // $NON-NLS-1$
		superAdminNode = MessageFormat.format(superAdminNode, resourceBundles); // $NON-NLS-1$
		smtpConfigNode = MessageFormat.format(smtpConfigNode, resourceBundles); // $NON-NLS-1$
		ldapConfigNode = MessageFormat.format(ldapConfigNode, resourceBundles); // $NON-NLS-1$
		singletonAppTimeNode = MessageFormat.format(singletonAppTimeNode, resourceBundles); // $NON-NLS-1$
		noPopMessageNode = MessageFormat.format(noPopMessageNode, resourceBundles);// $NON-NLS-1$
		liveMsgConfigNode = MessageFormat.format(liveMsgConfigNode, resourceBundles);// $NON-NLS-1$
		notificationNode =  MessageFormat.format(notificationNode, resourceBundles);// $NON-NLS-1$
		projectExpiryNode = MessageFormat.format(projectExpiryNode, resourceBundles);// $NON-NLS-1$
		infoGroupLabel = MessageFormat.format(infoGroupLabel, resourceBundles); // $NON-NLS-1$

		objectGroupDisaplyLabel = MessageFormat.format(objectGroupDisaplyLabel, resourceBundles); // $NON-NLS-1$
		siteGroupCreateLabel = MessageFormat.format(siteGroupCreateLabel, resourceBundles); // $NON-NLS-1$
		projectGroupCreateLabel = MessageFormat.format(projectGroupCreateLabel, resourceBundles); // $NON-NLS-1$
		groupModelCreateLabel = MessageFormat.format(groupModelCreateLabel, resourceBundles); // $NON-NLS-1$
		groupProjectModelCreateLabel = MessageFormat.format(groupProjectModelCreateLabel, resourceBundles);// $NON-NLS-1$
		directoryGroupCreateLabel = MessageFormat.format(directoryGroupCreateLabel, resourceBundles); // $NON-NLS-1$
		roleGroupCreateLabel = MessageFormat.format(roleGroupCreateLabel, resourceBundles); // $NON-NLS-1$
		objectGroupChangeLabel = MessageFormat.format(objectGroupChangeLabel, resourceBundles); // $NON-NLS-1$
		objectNameLabel = MessageFormat.format(objectNameLabel, resourceBundles); // $NON-NLS-1$
		objectUserNameLabel = MessageFormat.format(objectUserNameLabel, resourceBundles); // $NON-NLS-1$
		objectFullNameLabel = MessageFormat.format(objectFullNameLabel, resourceBundles); // $NON-NLS-1$
		objectActiveLabel = MessageFormat.format(objectActiveLabel, resourceBundles); // $NON-NLS-1$
		objectParentLabel = MessageFormat.format(objectParentLabel, resourceBundles); // $NON-NLS-1$
		objectSymbolLabel = MessageFormat.format(objectSymbolLabel, resourceBundles); // $NON-NLS-1$
		objectSymbolBtnTooltip = MessageFormat.format(objectSymbolBtnTooltip, resourceBundles); // $NON-NLS-1$
		saveButtonText = MessageFormat.format(saveButtonText, resourceBundles); // $NON-NLS-1$
		cancelButtonText = MessageFormat.format(cancelButtonText, resourceBundles); // $NON-NLS-1$
		objectNameLanguageLabel = MessageFormat.format(objectNameLanguageLabel, resourceBundles); // $NON-NLS-1$
		objectDescriptionLabel = MessageFormat.format(objectDescriptionLabel, resourceBundles); // $NON-NLS-1$
		objectContactLabel = MessageFormat.format(objectContactLabel, resourceBundles); // $NON-NLS-1$
		objectEmailLabel = MessageFormat.format(objectEmailLabel, resourceBundles); // $NON-NLS-1$
		objectExpiryLabel = MessageFormat.format(objectExpiryLabel, resourceBundles); // $NON-NLS-1$
		objectTimeLabel = MessageFormat.format(objectTimeLabel, resourceBundles); // $NON-NLS-1$
		objectSecLabel = MessageFormat.format(objectSecLabel, resourceBundles); // $NON-NLS-1$
		objectRemarkLabel = MessageFormat.format(objectRemarkLabel, resourceBundles); // $NON-NLS-1$
		objectTranslationLinkText = MessageFormat.format(objectTranslationLinkText, resourceBundles); // $NON-NLS-1$
		objectSingletonLabel = MessageFormat.format(objectSingletonLabel, resourceBundles); // $NON-NLS-1$
		objectPlatformLabel = MessageFormat.format(objectPlatformLabel, resourceBundles); // $NON-NLS-1$
		startMessageLabel = MessageFormat.format(startMessageLabel, resourceBundles); // $NON-NLS-1$
		expiryDateLabel = MessageFormat.format(expiryDateLabel, resourceBundles); // $NON-NLS-1$
		objectProgramLabel = MessageFormat.format(objectProgramLabel, resourceBundles); // $NON-NLS-1$
		objectPositionLabel = MessageFormat.format(objectPositionLabel, resourceBundles); // $NON-NLS-1$
		objectApplicationLabel = MessageFormat.format(objectApplicationLabel, resourceBundles); // $NON-NLS-1$
		remarkCreatedbyLabel = MessageFormat.format(remarkCreatedbyLabel, resourceBundles); // $NON-NLS-1$
		remarkDateLabel = MessageFormat.format(remarkDateLabel, resourceBundles); // $NON-NLS-1$
		userEmailLabel = MessageFormat.format(userEmailLabel, resourceBundles); // $NON-NLS-1$
		userManagerLabel = MessageFormat.format(userManagerLabel, resourceBundles); // $NON-NLS-1$
		userTelePhoneNumLabel = MessageFormat.format(userTelePhoneNumLabel, resourceBundles); // $NON-NLS-1$
		userDepartmentLabel = MessageFormat.format(userDepartmentLabel, resourceBundles); // $NON-NLS-1$
		smtpGroupLabel = MessageFormat.format(smtpGroupLabel, resourceBundles); // $NON-NLS-1$
		smtpEmailLabel = MessageFormat.format(smtpEmailLabel, resourceBundles); // $NON-NLS-1$
		smtpPortLabel = MessageFormat.format(smtpPortLabel, resourceBundles); // $NON-NLS-1$
		smtpServerNameLabel = MessageFormat.format(smtpServerNameLabel, resourceBundles); // $NON-NLS-1$
		smtpPasswordLabel = MessageFormat.format(smtpPasswordLabel, resourceBundles); // $NON-NLS-1$
		ldapGroupLabel = MessageFormat.format(ldapGroupLabel, resourceBundles); // $NON-NLS-1$
		ldapUrllabel = MessageFormat.format(ldapUrllabel, resourceBundles); // $NON-NLS-1$
		ldapUserNameLabel = MessageFormat.format(ldapUserNameLabel, resourceBundles);// $NON-NLS-1$
		ldapPasswordlabel = MessageFormat.format(ldapPasswordlabel, resourceBundles);// $NON-NLS-1$
		ldapBaselabel = MessageFormat.format(ldapBaselabel, resourceBundles);// $NON-NLS-1$
		ldapPortLabel = MessageFormat.format(ldapPortLabel, resourceBundles); // $NON-NLS-1$
		msglabel = MessageFormat.format(msglabel, resourceBundles);// $NON-NLS-1$
		singletonAppTimeGroupLabel = MessageFormat.format(singletonAppTimeGroupLabel, resourceBundles); // $NON-NLS-1$
		noProjectPopupGroupLabel = MessageFormat.format(noProjectPopupGroupLabel, resourceBundles); // $NON-NLS-1$
		singletonAppTimelabel = MessageFormat.format(singletonAppTimelabel, resourceBundles); // $NON-NLS-1$
		noProjectPopMsglabel = MessageFormat.format(noProjectPopMsglabel, resourceBundles); // $NON-NLS-1$
		singletonAppTimeSecondsLabel = MessageFormat.format(singletonAppTimeSecondsLabel, resourceBundles); // $NON-NLS-1$
		projectExpiryGroupLabel = MessageFormat.format(projectExpiryGroupLabel, resourceBundles); // $NON-NLS-1$
		projectExpiryDayspinnerLabel = MessageFormat.format(projectExpiryDayspinnerLabel, resourceBundles); // $NON-NLS-1$
		projectExpiryDaysLabel = MessageFormat.format(projectExpiryDaysLabel, resourceBundles); // $NON-NLS-1$
		projectGraceDaySppinnerLabel = MessageFormat.format(projectGraceDaySppinnerLabel, resourceBundles); // $NON-NLS-1$
		
		liveMsgConfigGroupLabel = MessageFormat.format(liveMsgConfigGroupLabel, resourceBundles); // $NON-NLS-1$
		liveMsgConfigToLabel = MessageFormat.format(liveMsgConfigToLabel, resourceBundles); // $NON-NLS-1$
		liveMsgConfigMsgLabel = MessageFormat.format(liveMsgConfigMsgLabel, resourceBundles); // $NON-NLS-1$
		liveMsgConfigPopupLabel = MessageFormat.format(liveMsgConfigPopupLabel, resourceBundles); // $NON-NLS-1$
		liveMsgConfigStartDateTimeLabel = MessageFormat.format(liveMsgConfigStartDateTimeLabel, resourceBundles); // $NON-NLS-1$
		liveMsgConfigExpiryDateTimeLabel = MessageFormat.format(liveMsgConfigExpiryDateTimeLabel, resourceBundles); // $NON-NLS-1$
		liveMsgConfigSendBtn = MessageFormat.format(liveMsgConfigSendBtn, resourceBundles); // $NON-NLS-1$
		liveMsgConfigAddBtn = MessageFormat.format(liveMsgConfigAddBtn, resourceBundles); // $NON-NLS-1$
		liveMsgConfigSubjectLbl = MessageFormat.format(liveMsgConfigSubjectLbl, resourceBundles); // $NON-NLS-1$
		liveMsgConfigNameLbl = MessageFormat.format(liveMsgConfigNameLbl, resourceBundles); // $NON-NLS-1$
		liveMsgGroupRadioBtnLbl = MessageFormat.format(liveMsgGroupRadioBtnLbl, resourceBundles); // $NON-NLS-1$
		liveMsgAddObjRadioBtnLbl = MessageFormat.format(liveMsgAddObjRadioBtnLbl, resourceBundles); // $NON-NLS-1$
		
		siteFilterGroup = MessageFormat.format(siteFilterGroup, resourceBundles); // $NON-NLS-1$
		adminAreaFilterGroup = MessageFormat.format(adminAreaFilterGroup, resourceBundles); // $NON-NLS-1$
		projectFilterGroup = MessageFormat.format(projectFilterGroup, resourceBundles); // $NON-NLS-1$
		userFilterGroup = MessageFormat.format(userFilterGroup, resourceBundles); // $NON-NLS-1$
		//filterLabel = MessageFormat.format(filterLabel, resourceBundles); // $NON-NLS-1$
		
		siteFilterLabel = MessageFormat.format(siteFilterLabel, resourceBundles); // $NON-NLS-1$
		adminAreaFilterLabel = MessageFormat.format(adminAreaFilterLabel, resourceBundles); // $NON-NLS-1$
		projectFilterLabel = MessageFormat.format(projectFilterLabel, resourceBundles); // $NON-NLS-1$
		userFilterLabel = MessageFormat.format(userFilterLabel, resourceBundles); // $NON-NLS-1$
		
		allBtnLabel = MessageFormat.format(allBtnLabel, resourceBundles); // $NON-NLS-1$
		userGroupFilterLabel = MessageFormat.format(userGroupFilterLabel, resourceBundles); // $NON-NLS-1$
		projectGroupFilterLabel = MessageFormat.format(projectGroupFilterLabel, resourceBundles); // $NON-NLS-1$
		
		notificationDescriptionLbl = MessageFormat.format(notificationDescriptionLbl, resourceBundles); // $NON-NLS-1$
		notificationToUserLbl = MessageFormat.format(notificationToUserLbl, resourceBundles); // $NON-NLS-1$
		notificationEventTypeLbl = MessageFormat.format(notificationEventTypeLbl, resourceBundles); // $NON-NLS-1$
		notificationMessageLbl = MessageFormat.format(notificationMessageLbl, resourceBundles); // $NON-NLS-1$
		notificationSubjectLbl = MessageFormat.format(notificationSubjectLbl, resourceBundles); // $NON-NLS-1$
		notificationIncludeRemarkBtn = MessageFormat.format(notificationIncludeRemarkBtn, resourceBundles); // $NON-NLS-1$
		notificationSendToAssUserBtn = MessageFormat.format(notificationSendToAssUserBtn, resourceBundles); // $NON-NLS-1$
		notificationProjectExpNoticeBtn = MessageFormat.format(notificationProjectExpNoticeBtn, resourceBundles); // $NON-NLS-1$
		notificationSendBtn = MessageFormat.format(notificationSendBtn, resourceBundles); // $NON-NLS-1$
		notificationAddBtn = MessageFormat.format(notificationAddBtn, resourceBundles); // $NON-NLS-1$
		notificationUserErrorTitle = MessageFormat.format(notificationUserErrorTitle, resourceBundles); // $NON-NLS-1$
		notificationUserErrorMsg = MessageFormat.format(notificationUserErrorMsg, resourceBundles); // $NON-NLS-1$
		notificationAddUserRadioBtnLbl = MessageFormat.format(notificationAddUserRadioBtnLbl, resourceBundles); // $NON-NLS-1$
		notificationUserGroupRadioBtnLbl = MessageFormat.format(notificationUserGroupRadioBtnLbl, resourceBundles); // $NON-NLS-1$
		notificationEditBtn = MessageFormat.format(notificationEditBtn, resourceBundles); // $NON-NLS-1$
		
		WINDOWS32 = MessageFormat.format(WINDOWS32, resourceBundles); // $NON-NLS-1$
		WINDOWS64 = MessageFormat.format(WINDOWS64, resourceBundles); // $NON-NLS-1$
		LINUX64 = MessageFormat.format(LINUX64, resourceBundles); // $NON-NLS-1$
		
		searchUserDialogTitle = MessageFormat.format(searchUserDialogTitle, resourceBundles); // $NON-NLS-1$
		searchUserUserNamelabel = MessageFormat.format(searchUserUserNamelabel, resourceBundles); // $NON-NLS-1$
		searchUserLdapUrllabel = MessageFormat.format(searchUserLdapUrllabel, resourceBundles); // $NON-NLS-1$
		searchUserSearchBtn = MessageFormat.format(searchUserSearchBtn, resourceBundles); // $NON-NLS-1$
		searchUserGroupSerachResult = MessageFormat.format(searchUserGroupSerachResult, resourceBundles); // $NON-NLS-1$
		searchUserPropertyNameCol = MessageFormat.format(searchUserPropertyNameCol, resourceBundles); // $NON-NLS-1$
		searchUserPropertyValueCol = MessageFormat.format(searchUserPropertyValueCol, resourceBundles); // $NON-NLS-1$
		searchUserErrorDialogTitle = MessageFormat.format(searchUserErrorDialogTitle, resourceBundles); // $NON-NLS-1$
		searchUserErrorDialogMsg = MessageFormat.format(searchUserErrorDialogMsg, resourceBundles); // $NON-NLS-1$
		searchUserAlradyExistDialogTitle = MessageFormat.format(searchUserAlradyExistDialogTitle, resourceBundles); // $NON-NLS-1$
		searchUserAlradyExistDialogMsg = MessageFormat.format(searchUserAlradyExistDialogMsg, resourceBundles); // $NON-NLS-1$
		searchUserImportBtnLbl = MessageFormat.format(searchUserImportBtnLbl, resourceBundles); // $NON-NLS-1$

		addIconButton = MessageFormat.format(addIconButton, resourceBundles); // $NON-NLS-1$
		removeIconButton = MessageFormat.format(removeIconButton, resourceBundles); // $NON-NLS-1$
		icontableviewerFirstColumnLabel = MessageFormat.format(icontableviewerFirstColumnLabel, resourceBundles); // $NON-NLS-1$
		icontableviewerSecondColumnLabel = MessageFormat.format(icontableviewerSecondColumnLabel, resourceBundles); // $NON-NLS-1$
		browseIconDialogTitle = MessageFormat.format(browseIconDialogTitle, resourceBundles); // $NON-NLS-1$
		browseScriptDialogTreeScriptColumnLabel = MessageFormat.format(browseScriptDialogTreeScriptColumnLabel,
				resourceBundles); // $NON-NLS-1$
		browseScriptDialogTreeDateColumnLabel = MessageFormat.format(browseScriptDialogTreeDateColumnLabel,
				resourceBundles); // $NON-NLS-1$
		browseScriptDialogBrowseBtn = MessageFormat.format(browseScriptDialogBrowseBtn, resourceBundles); // $NON-NLS-1$
		browseScriptDialogClearLnk = MessageFormat.format(browseScriptDialogClearLnk, resourceBundles); // $NON-NLS-1$

		roleGroupLable = MessageFormat.format(roleGroupLable, resourceBundles); // $NON-NLS-1$
		roleNameLable = MessageFormat.format(roleNameLable, resourceBundles); // $NON-NLS-1$
		roleViewInactiveLable = MessageFormat.format(roleViewInactiveLable, resourceBundles); // $NON-NLS-1$
		roleXmAdminLable = MessageFormat.format(roleXmAdminLable, resourceBundles); // $NON-NLS-1$
		roleXmHotlineLable = MessageFormat.format(roleXmHotlineLable, resourceBundles); // $NON-NLS-1$
		permissionObjTypeLable = MessageFormat.format(permissionObjTypeLable, resourceBundles); // $NON-NLS-1$
		permissionCreateColLable = MessageFormat.format(permissionCreateColLable, resourceBundles); // $NON-NLS-1$
		permissionChangeColLable = MessageFormat.format(permissionChangeColLable, resourceBundles); // $NON-NLS-1$
		permissionDeleteColLable = MessageFormat.format(permissionDeleteColLable, resourceBundles); // $NON-NLS-1$
		permissionActivateColLable = MessageFormat.format(permissionActivateColLable, resourceBundles); // $NON-NLS-1$
		permissionViewInactivrColLable = MessageFormat.format(permissionViewInactivrColLable, resourceBundles); // $NON-NLS-1$
		permissionAssignColLable = MessageFormat.format(permissionAssignColLable, resourceBundles); // $NON-NLS-1$
		permissionRemoveColLable = MessageFormat.format(permissionRemoveColLable, resourceBundles); // $NON-NLS-1$
		permissionInactiveAssiColLable = MessageFormat.format(permissionInactiveAssiColLable, resourceBundles); // $NON-NLS-1$
		
		roleObjPermissionLable = MessageFormat.format(roleObjPermissionLable, resourceBundles); // $NON-NLS-1$
		roleObjRelationPermissionLable = MessageFormat.format(roleObjRelationPermissionLable, resourceBundles); // $NON-NLS-1$
		objectTypeLable = MessageFormat.format(objectTypeLable, resourceBundles); // $NON-NLS-1$
		descriptionLable = MessageFormat.format(descriptionLable, resourceBundles); // $NON-NLS-1$
		existingRoleNameTitle = MessageFormat.format(existingRoleNameTitle, resourceBundles); // $NON-NLS-1$
		existingRoleNameError = MessageFormat.format(existingRoleNameError, resourceBundles); // $NON-NLS-1$

		notificationMessageErrTitle = MessageFormat.format(notificationMessageErrTitle, resourceBundles); // $NON-NLS-1$
		notificationMessageErrMsg = MessageFormat.format(notificationMessageErrMsg, resourceBundles); // $NON-NLS-1$
		notificationSubjectErrTitle = MessageFormat.format(notificationSubjectErrTitle, resourceBundles); // $NON-NLS-1$
		notificationSubjectErrMsg = MessageFormat.format(notificationSubjectErrMsg, resourceBundles); // $NON-NLS-1$
		notificationSendErrTiltle = MessageFormat.format(notificationSendErrTiltle, resourceBundles); // $NON-NLS-1$
		notificationSendErrMsg = MessageFormat.format(notificationSendErrMsg, resourceBundles); // $NON-NLS-1$
		notifiMessageDecoTitle = MessageFormat.format(notifiMessageDecoTitle, resourceBundles); // $NON-NLS-1$
		notifiMessageDecoUserName = MessageFormat.format(notifiMessageDecoUserName, resourceBundles); // $NON-NLS-1$
		notifiMessageDecoProjectName = MessageFormat.format(notifiMessageDecoProjectName, resourceBundles); // $NON-NLS-1$
		notifiMessageDecoAssignedUserName = MessageFormat.format(notifiMessageDecoAssignedUserName, resourceBundles); // $NON-NLS-1$
		notifiMessageDecoProjectExpiryDays = MessageFormat.format(notifiMessageDecoProjectExpiryDays, resourceBundles); // $NON-NLS-1$
		notifiMessageDecoProjectGraceDays = MessageFormat.format(notifiMessageDecoProjectGraceDays, resourceBundles); // $NON-NLS-1$
		notifiMessageDecoAdminAreaName = MessageFormat.format(notifiMessageDecoAdminAreaName, resourceBundles); // $NON-NLS-1$
		notifiMessageDecoProjectCreateDate = MessageFormat.format(notifiMessageDecoProjectCreateDate, resourceBundles); // $NON-NLS-1$
		notifiMessageDecoExpireProjectName = MessageFormat.format(notifiMessageDecoExpireProjectName, resourceBundles); // $NON-NLS-1$
		notifiMessageDecoAssignProjectName = MessageFormat.format(notifiMessageDecoAssignProjectName, resourceBundles); // $NON-NLS-1$
		notificationInvalidMsgDialogMsg = MessageFormat.format(notificationInvalidMsgDialogMsg, resourceBundles); // $NON-NLS-1$
		
		// Dialog Messages
		deleteConfirmDialogTitle = MessageFormat.format(deleteConfirmDialogTitle, resourceBundles); // $NON-NLS-1$
		deleteMultiObjectsConfirmDialogMsg = MessageFormat.format(deleteMultiObjectsConfirmDialogMsg, resourceBundles); // $NON-NLS-1$
		removeMultiObjectsRelConfirmDialogMsg = MessageFormat.format(removeMultiObjectsRelConfirmDialogMsg, resourceBundles); // $NON-NLS-1$
		deleteSiteConfirmDialogMsg = MessageFormat.format(deleteSiteConfirmDialogMsg, resourceBundles); // $NON-NLS-1$
		deleteSiteRelationConfirmDialogMsgPart1 = MessageFormat.format(deleteSiteRelationConfirmDialogMsgPart1, resourceBundles); // $NON-NLS-1$
		deleteSiteRelationConfirmDialogMsgPart2 = MessageFormat.format(deleteSiteRelationConfirmDialogMsgPart2, resourceBundles); // $NON-NLS-1$
		deleteAARelationConfirmDialogMsgPart1 = MessageFormat.format(deleteAARelationConfirmDialogMsgPart1, resourceBundles); // $NON-NLS-1$
		deleteAARelationConfirmDialogMsgPart2 = MessageFormat.format(deleteAARelationConfirmDialogMsgPart2, resourceBundles); // $NON-NLS-1$
		deleteUserConfirmDialogMsgPart1 = MessageFormat.format(deleteUserConfirmDialogMsgPart1, resourceBundles); // $NON-NLS-1$
		deleteUserConfirmDialogMsgPart2 = MessageFormat.format(deleteUserConfirmDialogMsgPart2, resourceBundles); // $NON-NLS-1$
		deleteAdminAreaConfirmDialogMsg = MessageFormat.format(deleteAdminAreaConfirmDialogMsg, resourceBundles); // $NON-NLS-1$
		deleteUserConfirmDialogMsg = MessageFormat.format(deleteUserConfirmDialogMsg, resourceBundles); // $NON-NLS-1$
		deleteProjectConfirmDialogMsg = MessageFormat.format(deleteProjectConfirmDialogMsg, resourceBundles); // $NON-NLS-1$
		deleteProjectConfirmDialogMsgPart1 = MessageFormat.format(deleteProjectConfirmDialogMsgPart1, resourceBundles); // $NON-NLS-1$
		deleteProjectConfirmDialogMsgPart2 = MessageFormat.format(deleteProjectConfirmDialogMsgPart2, resourceBundles); // $NON-NLS-1$	
		deleteUserAppConfirmDialogMsgPart1 = MessageFormat.format(deleteUserAppConfirmDialogMsgPart1, resourceBundles); // $NON-NLS-1$
		deleteUserAppConfirmDialogMsgPart2 = MessageFormat.format(deleteUserAppConfirmDialogMsgPart2, resourceBundles); // $NON-NLS-1$
		deleteBaseAppConfirmDialogMsgPart1 = MessageFormat.format(deleteBaseAppConfirmDialogMsgPart1, resourceBundles); // $NON-NLS-1$		
		deleteBaseAppConfirmDialogMsgPart2 = MessageFormat.format(deleteBaseAppConfirmDialogMsgPart2, resourceBundles); // $NON-NLS-1$
		deleteProjectAppConfirmDialogMsgPart1 = MessageFormat.format(deleteProjectAppConfirmDialogMsgPart1, resourceBundles); // $NON-NLS-1$
		deleteProjectAppConfirmDialogMsgPart2 = MessageFormat.format(deleteProjectAppConfirmDialogMsgPart2, resourceBundles); // $NON-NLS-1$
		deleteStartAppConfirmDialogMsgPart1 = MessageFormat.format(deleteStartAppConfirmDialogMsgPart1, resourceBundles); // $NON-NLS-1$
		deleteStartAppConfirmDialogMsgPart2 = MessageFormat.format(deleteStartAppConfirmDialogMsgPart2, resourceBundles); // $NON-NLS-1$
		deleteBaseAppConfirmDialogMsg = MessageFormat.format(deleteBaseAppConfirmDialogMsg, resourceBundles); // $NON-NLS-1$
		deleteBaseAppRelationDialogMsgPart1 = MessageFormat.format(deleteBaseAppRelationDialogMsgPart1, resourceBundles);// $NON-NLS-1$
		deleteBaseAppRelationDialogMsgPart2 = MessageFormat.format(deleteBaseAppRelationDialogMsgPart2, resourceBundles);// $NON
		deleteBaseAppRelationDialogMsg = MessageFormat.format(deleteBaseAppRelationDialogMsg, resourceBundles);// $NON-NLS-1$
		deleteUserAppConfirmDialogMsg = MessageFormat.format(deleteUserAppConfirmDialogMsg, resourceBundles); // $NON-NLS-1$
		deleteProjectAppConfirmDialogMsg = MessageFormat.format(deleteProjectAppConfirmDialogMsg, resourceBundles); // $NON-NLS-1$
		deleteEventAppConfirmDialogMsg = MessageFormat.format(deleteEventAppConfirmDialogMsg, resourceBundles); // $NON-NLS-1$
		deleteStartAppConfirmDialogMsg = MessageFormat.format(deleteStartAppConfirmDialogMsg, resourceBundles); // $NON-NLS-1$
		deleteUserGroupConfirmDialogMsg = MessageFormat.format(deleteUserGroupConfirmDialogMsg, resourceBundles); // $NON-NLS-1$
		deleteProjectGroupConfirmDialogMsg = MessageFormat.format(deleteProjectGroupConfirmDialogMsg, resourceBundles); // $NON-NLS-1$
		deleteUserAppGroupConfirmDialogMsg = MessageFormat.format(deleteUserAppGroupConfirmDialogMsg, resourceBundles); // $NON-NLS-1$
		deleteProjectAppGroupConfirmDialogMsg = MessageFormat.format(deleteProjectAppGroupConfirmDialogMsg, resourceBundles); // $NON-NLS-1$
		deleteDirectoryConfirmDialogMsg = MessageFormat.format(deleteDirectoryConfirmDialogMsg, resourceBundles); // $NON-NLS-1$
		deleteSiteRelationMsg = MessageFormat.format(deleteSiteRelationMsg, resourceBundles); // $NON-NLS-1$
		deleteAdminAreaRelationMsg = MessageFormat.format(deleteAdminAreaRelationMsg, resourceBundles); // $NON-NLS-1$
		deleteProjectRelationMsg = MessageFormat.format(deleteProjectRelationMsg, resourceBundles); // $NON-NLS-1$
		deleteAdminMenuRelationMsg = MessageFormat.format(deleteAdminMenuRelationMsg, resourceBundles); // $NON-NLS-1$
		deleteDirectoryMsg = MessageFormat.format(deleteDirectoryMsg, resourceBundles);// $NON-NLS-1$
		deleteUserRelationMsg = MessageFormat.format(deleteUserRelationMsg, resourceBundles); // $NON-NLS-1$ 
		deleteRoleRelationMsg = MessageFormat.format(deleteRoleRelationMsg, resourceBundles); // $NON-NLS-1$ 
		deleteUserGroupRelationMsg = MessageFormat.format(deleteUserGroupRelationMsg, resourceBundles); // $NON-NLS-1$ 
		deleteProjectGroupRelationMsg = MessageFormat.format(deleteProjectGroupRelationMsg, resourceBundles); // $NON-NLS-1$ 
		deleteUserAppGroupRelationMsg = MessageFormat.format(deleteUserAppGroupRelationMsg, resourceBundles); // $NON-NLS-1$ 
		deleteProjectAppGroupRelationMsg = MessageFormat.format(deleteProjectAppGroupRelationMsg, resourceBundles);
		changeStatusConfirmDialogTitle = MessageFormat.format(changeStatusConfirmDialogTitle, resourceBundles); // $NON-NLS-1$
		activateRelchangeStatusConfirmDialogMsg = MessageFormat.format(activateRelchangeStatusConfirmDialogMsg, resourceBundles); // $NON-NLS-1$
		deActivateRelchangeStatusConfirmDialogMsg = MessageFormat.format(deActivateRelchangeStatusConfirmDialogMsg, resourceBundles); // $NON-NLS-1$
		activateMultiRelchangeStatusConfirmDialogMsg = MessageFormat.format(activateMultiRelchangeStatusConfirmDialogMsg, resourceBundles); // $NON-NLS-1$
		deActivateMultiRelchangeStatusConfirmDialogMsg = MessageFormat.format(deActivateMultiRelchangeStatusConfirmDialogMsg, resourceBundles); // $NON-NLS-1$
		activatechangeStatusConfirmDialogMsg = MessageFormat.format(activatechangeStatusConfirmDialogMsg,
				resourceBundles); // $NON-NLS-1$
		activateMultichangeStatusConfirmDialogMsg = MessageFormat.format(activateMultichangeStatusConfirmDialogMsg,
				resourceBundles); // $NON-NLS-1$
		deActivateMultichangeStatusConfirmDialogMsg = MessageFormat.format(deActivateMultichangeStatusConfirmDialogMsg,
				resourceBundles); // $NON-NLS-1$
		deActivatechangeStatusConfirmDialogMsg = MessageFormat.format(deActivatechangeStatusConfirmDialogMsg,
				resourceBundles); // $NON-NLS-1$
		deleteRoleConfirmDialogMsg = MessageFormat.format(deleteRoleConfirmDialogMsg, resourceBundles); // $NON-NLS-1$
		deleteLiveMsgConfirmDialogMsg = MessageFormat.format(deleteLiveMsgConfirmDialogMsg, resourceBundles); // $NON-NLS-1$
		userImportWizardAuthFailDialogTitle = MessageFormat.format(userImportWizardAuthFailDialogTitle,
				resourceBundles); // $NON-NLS-1$
		userImportWizardAuthFailDialogMsg = MessageFormat.format(userImportWizardAuthFailDialogMsg, resourceBundles); // $NON-NLS-1$
		userImportWizardEnterPasswordMsg = MessageFormat.format(userImportWizardEnterPasswordMsg, resourceBundles); // $NON-NLS-1$
		userImportWizardUnalbefindUserMasg = MessageFormat.format(userImportWizardUnalbefindUserMasg, resourceBundles); // $NON-NLS-1$
		browseScriptDialogTitle = MessageFormat.format(browseScriptDialogTitle, resourceBundles); // $NON-NLS-1$
		expirydaysDialogTitle = MessageFormat.format(expirydaysDialogTitle, resourceBundles);// $NON-NLS-1$
		objectPermissionDialogMsg = MessageFormat.format(objectPermissionDialogMsg, resourceBundles);// $NON-NLS-1$
		objectPermissionDialogTitle = MessageFormat.format(objectPermissionDialogTitle, resourceBundles);// $NON-NLS-1$
		addIconDialogTitle = MessageFormat.format(addIconDialogTitle, resourceBundles);// $NON-NLS-1$
		addIconDialogMsg = MessageFormat.format(addIconDialogMsg, resourceBundles);// $NON-NLS-1$
		removeIconDialogMsg = MessageFormat.format(removeIconDialogMsg, resourceBundles);// $NON-NLS-1$
		removeIconDialogTitle = MessageFormat.format(removeIconDialogTitle, resourceBundles);// $NON-NLS-1$
		
		iconErrorTitle = MessageFormat.format(iconErrorTitle, resourceBundles);// $NON-NLS-1$
		iconErrorMsg = MessageFormat.format(iconErrorMsg, resourceBundles);// $NON-NLS-1$ 
		infoDisplayMsg = MessageFormat.format(infoDisplayMsg, resourceBundles);// $NON-NLS-1$
		nameErrorTitle = MessageFormat.format(nameErrorTitle, resourceBundles); // $NON-NLS-1$
		nameError = MessageFormat.format(nameError, resourceBundles); // $NON-NLS-1$
		emptyContactError = MessageFormat.format(emptyContactError, resourceBundles);// $NON-NLS-1$
		emptyPortError = MessageFormat.format(emptyPortError, resourceBundles);// $NON-NLS-1$
		emptyUrlError = MessageFormat.format(emptyUrlError, resourceBundles);// $NON-NLS-1$
		emptyPasswordError = MessageFormat.format(emptyPasswordError, resourceBundles);// $NON-NLS-1$
		emptyServerNameError =MessageFormat.format(emptyServerNameError, resourceBundles);
		emptyBaseNameError = MessageFormat.format(emptyBaseNameError, resourceBundles);// $NON-NLS-1$
		emptyldapUserNameError = MessageFormat.format(emptyldapUserNameError, resourceBundles);// $NON-NLS-1$
		emptyEmailError = MessageFormat.format(emptyEmailError, resourceBundles);// $NON-NLS-1$
		symbolErrorTitle = MessageFormat.format(symbolErrorTitle, resourceBundles); // $NON-NLS-1$
		symbolError = MessageFormat.format(symbolError, resourceBundles); // $NON-NLS-1$
		nameSymbolErrorTitle = MessageFormat.format(nameSymbolErrorTitle, resourceBundles); // $NON-NLS-1$
		nameSymbolError = MessageFormat.format(nameSymbolError, resourceBundles); // $NON-NLS-1$
		existingSiteNameError = MessageFormat.format(existingSiteNameError, resourceBundles); // $NON-NLS-1$
		existingSiteNameTitle = MessageFormat.format(existingSiteNameTitle, resourceBundles); // $NON-NLS-1$
		emptyNameValidateErr = MessageFormat.format(emptyNameValidateErr, resourceBundles);// $NON-NLS-1$
		specialCharValidateErr = MessageFormat.format(specialCharValidateErr, resourceBundles);// $NON-NLS-1$
		whitespaceOrSpeCharValidateErr = MessageFormat.format(whitespaceOrSpeCharValidateErr, resourceBundles);// $NON-NLS-1$
		contactNumberValidateErr = MessageFormat.format(contactNumberValidateErr, resourceBundles);// $NON-NLS-1$
		portValidateErr = MessageFormat.format(portValidateErr, resourceBundles); // $NON-NLS-1$
		ldapUrlValidateErr = MessageFormat.format(ldapUrlValidateErr, resourceBundles); // $NON-NLS-1$
		emptyCommonNameValidateErr = MessageFormat.format(emptyCommonNameValidateErr, resourceBundles);// $NON-NLS-1$
		emailValidateErr = MessageFormat.format(emailValidateErr, resourceBundles);// $NON-NLS-1$
		emptyTimeSpinnerValidateErr = MessageFormat.format(emptyTimeSpinnerValidateErr, resourceBundles);// $NON-NLS-1$
		infoTimeSpinnerValidate = MessageFormat.format(infoTimeSpinnerValidate, resourceBundles); // $NON-NLS-1$
		emptyNoProjectMessageValidateErr = MessageFormat.format(emptyNoProjectMessageValidateErr, resourceBundles);// $NON-NLS-1$
		emptyNotificationMessageErr = MessageFormat.format(emptyNotificationMessageErr, resourceBundles); // $NON-NLS-1$
		emptyNotificationSubjectErr = MessageFormat.format(emptyNotificationSubjectErr, resourceBundles); // $NON-NLS-1$
		deleteMultiObjectsDialogErrMsg = MessageFormat.format(deleteMultiObjectsDialogErrMsg, resourceBundles); // $NON-NLS-1$
		multiStatusUpadteObjectDilaogErrMsg = MessageFormat.format(multiStatusUpadteObjectDilaogErrMsg, resourceBundles); // $NON-NLS-1$

		existingAdminAreaNameError = MessageFormat.format(existingAdminAreaNameError, resourceBundles); // $NON-NLS-1$
		existingAdminAreaNameTitle = MessageFormat.format(existingAdminAreaNameTitle, resourceBundles); // $NON-NLS-1$

		existingUserNameError = MessageFormat.format(existingUserNameError, resourceBundles); // $NON-NLS-1$
		existingUserNameTitle = MessageFormat.format(existingUserNameTitle, resourceBundles); // $NON-NLS-1$

		existingProjectNameError = MessageFormat.format(existingProjectNameError, resourceBundles); // $NON-NLS-1$
		existingProjectNameTitle = MessageFormat.format(existingProjectNameTitle, resourceBundles); // $NON-NLS-1$

		existingUserGroupNameError = MessageFormat.format(existingUserGroupNameError, resourceBundles); // $NON-NLS-1$
		existingUserGroupNameTitle = MessageFormat.format(existingUserGroupNameTitle, resourceBundles); // $NON-NLS-1$
		
		existingProjectGroupNameError = MessageFormat.format(existingProjectGroupNameError, resourceBundles); // $NON-NLS-1$
		existingProjectGroupNameTitle = MessageFormat.format(existingProjectGroupNameTitle, resourceBundles); // $NON-NLS-1$
		
		existingUserAppGroupNameError = MessageFormat.format(existingUserAppGroupNameError, resourceBundles); // $NON-NLS-1$
		existingUserAppGroupNameTitle = MessageFormat.format(existingUserAppGroupNameTitle, resourceBundles); // $NON-NLS-1$
		
		existingProjectAppGroupNameError = MessageFormat.format(existingProjectAppGroupNameError, resourceBundles); // $NON-NLS-1$
		existingProjectAppGroupNameTitle = MessageFormat.format(existingProjectAppGroupNameTitle, resourceBundles); // $NON-NLS-1$
		
		existingDirectoryNameTitle = MessageFormat.format(existingDirectoryNameTitle, resourceBundles); // $NON-NLS-1$
		existingDirectoryNameError = MessageFormat.format(existingDirectoryNameError, resourceBundles); // $NON-NLS-1$
		
		existingLiveMsgNameTitle = MessageFormat.format(existingLiveMsgNameTitle, resourceBundles); // $NON-NLS-1$
		existingLiveMsgNameError = MessageFormat.format(existingLiveMsgNameError, resourceBundles); // $NON-NLS-1$

		appNameErrorTitle = MessageFormat.format(appNameErrorTitle, resourceBundles); // $NON-NLS-1$
		appNameEmptyEnError = MessageFormat.format(appNameEmptyEnError, resourceBundles); // $NON-NLS-1$
		existingEnNameError = MessageFormat.format(existingEnNameError, resourceBundles); // $NON-NLS-1$
		existingDeNameError = MessageFormat.format(existingDeNameError, resourceBundles); // $NON-NLS-1$
		appPostionEmptyError = MessageFormat.format(appPostionEmptyError, resourceBundles); // $NON-NLS-1$
		appBaseAppEmptyError = MessageFormat.format(appBaseAppEmptyError, resourceBundles); // $NON-NLS-1$
		appChildPositionError = MessageFormat.format(appChildPositionError, resourceBundles); // $NON-NLS-1$
		supportedPlatfErrorTitle = MessageFormat.format(supportedPlatfErrorTitle, resourceBundles);// $NON-NLS-1$
		supprotedPltfEmptyEnError = MessageFormat.format(supprotedPltfEmptyEnError, resourceBundles);// $NON-NLS-1$
		appProgramEmptyError = MessageFormat.format(appProgramEmptyError, resourceBundles);// $NON-NLS-1$

		dndErrorTitle = MessageFormat.format(dndErrorTitle, resourceBundles); // $NON-NLS-1$
		adminAreaErrorMessagePrefix = MessageFormat.format(adminAreaErrorMessagePrefix, resourceBundles); // $NON-NLS-1$
		unauthorizedDialogMsg = MessageFormat.format(unauthorizedDialogMsg, resourceBundles); // $NON-NLS-1$
		unauthorizedDialogTitle = MessageFormat.format(unauthorizedDialogTitle, resourceBundles); // $NON-NLS-1$
		adminAreaDndErrorMsgPart1 = MessageFormat.format(adminAreaDndErrorMsgPart1, resourceBundles); // $NON-NLS-1$
		adminAreaDndErrorMsgPart2 = MessageFormat.format(adminAreaDndErrorMsgPart2, resourceBundles); // $NON-NLS-1$
		dndStatusBarMsg = MessageFormat.format(dndStatusBarMsg, resourceBundles); // $NON-NLS-1$
		dndEmptySelectionMsg =  MessageFormat.format(dndEmptySelectionMsg, resourceBundles); // $NON-NLS-1$
		dndTemplateDialogTitle = MessageFormat.format(dndTemplateDialogTitle, resourceBundles); // $NON-NLS-1$
		dndTemplateConfirmMsg =  MessageFormat.format(dndTemplateConfirmMsg, resourceBundles); // $NON-NLS-1$
		
		appBaseAppErrorTitle = MessageFormat.format(appBaseAppErrorTitle, resourceBundles); // $NON-NLS-1$
		appBaseAppError = MessageFormat.format(appBaseAppError, resourceBundles); // $NON-NLS-1$
		
		startAppExpiryDateNotSetErrorTitle = MessageFormat.format(startAppExpiryDateNotSetErrorTitle, resourceBundles); // $NON-NLS-1$
		startAppExpiryDateNotSetError = MessageFormat.format(startAppExpiryDateNotSetError, resourceBundles); // $NON-NLS-1$
		liveMsgInvalidExpiryDateMsg = MessageFormat.format(liveMsgInvalidExpiryDateMsg, resourceBundles); // $NON-NLS-1$
		
		appPositionErrorTitle = MessageFormat.format(appPositionErrorTitle, resourceBundles); // $NON-NLS-1$
		appPositionError = MessageFormat.format(appPositionError, resourceBundles); // $NON-NLS-1$
		
		appProgramErrorTitle = MessageFormat.format(appProgramErrorTitle, resourceBundles); // $NON-NLS-1$
		
		parentEnableInfoTitle = MessageFormat.format(parentEnableInfoTitle, resourceBundles); // $NON-NLS-1$
		parentEnableInfo = MessageFormat.format(parentEnableInfo, resourceBundles); // $NON-NLS-1$
		
		messageEnableInfoTitle = MessageFormat.format(messageEnableInfoTitle, resourceBundles);// $NON-NLS-1$
		messageEnableInfo = MessageFormat.format(messageEnableInfo, resourceBundles);// $NON-NLS-1$

		userGroupCreateLabel = MessageFormat.format(userGroupCreateLabel, resourceBundles); // $NON-NLS-1$
		adminAreaGroupCreateLabel = MessageFormat.format(adminAreaGroupCreateLabel, resourceBundles); // $NON-NLS-1$
		projectGroupCreateLabel = MessageFormat.format(projectGroupCreateLabel, resourceBundles); // $NON-NLS-1$
		baseAppGroupCreateLabel = MessageFormat.format(baseAppGroupCreateLabel, resourceBundles); // $NON-NLS-1$
		userAppGroupCreateLabel = MessageFormat.format(userAppGroupCreateLabel, resourceBundles); // $NON-NLS-1$
		projectAppGroupCreateLabel = MessageFormat.format(projectAppGroupCreateLabel, resourceBundles); // $NON-NLS-1$
		grpUserAppGroupCreateLabel = MessageFormat.format(grpUserAppGroupCreateLabel, resourceBundles); // $NON-NLS-1$
		grpProjectAppGroupCreateLabel = MessageFormat.format(grpProjectAppGroupCreateLabel, resourceBundles); // $NON-NLS-1$
		eventAppGroupCreateLabel = MessageFormat.format(eventAppGroupCreateLabel, resourceBundles); // $NON-NLS-1$
		startAppGroupCreateLabel = MessageFormat.format(startAppGroupCreateLabel, resourceBundles); // $NON-NLS-1$

		translationDialogTitle = MessageFormat.format(translationDialogTitle, resourceBundles); // $NON-NLS-1$
		translationLangLabel = MessageFormat.format(translationLangLabel, resourceBundles); // $NON-NLS-1$
		translationLangLabelTxtEN = MessageFormat.format(translationLangLabelTxtEN, resourceBundles); // $NON-NLS-1$
		translationLangLabelTxtDE = MessageFormat.format(translationLangLabelTxtDE, resourceBundles); // $NON-NLS-1$

		popupmenulabelnodeActive = MessageFormat.format(popupmenulabelnodeActive, resourceBundles); // $NON-NLS-1$
		popupmenulabelnodeDeActive = MessageFormat.format(popupmenulabelnodeDeActive, resourceBundles); // $NON-NLS-1$
		popupmenulabelnodeRemoveRelation = MessageFormat.format(popupmenulabelnodeRemoveRelation, resourceBundles); // $NON-NLS-1$
		popupmenuExpiryDays = MessageFormat.format(popupmenuExpiryDays, resourceBundles);// $NON-NLS-1$

		dirtyDialogTitle = MessageFormat.format(dirtyDialogTitle, resourceBundles); // $NON-NLS-1$
		dirtyDialogMessage = MessageFormat.format(dirtyDialogMessage, resourceBundles); // $NON-NLS-1$
		
		userReviewSelectAllBtn = MessageFormat.format(userReviewSelectAllBtn, resourceBundles); // $NON-NLS-1$
		userReviewDeSelectAllBtn = MessageFormat.format(userReviewDeSelectAllBtn, resourceBundles); // $NON-NLS-1$
		userReviewDeActivateBtn = MessageFormat.format(userReviewDeActivateBtn, resourceBundles); // $NON-NLS-1$
		userReviewDeleteBtn = MessageFormat.format(userReviewDeleteBtn, resourceBundles); // $NON-NLS-1$
		userReviewUserNameCol = MessageFormat.format(userReviewUserNameCol, resourceBundles); // $NON-NLS-1$
		userReviewLdapStatusCol = MessageFormat.format(userReviewLdapStatusCol, resourceBundles); // $NON-NLS-1$
		userReviewXmSystemStatusCol = MessageFormat.format(userReviewXmSystemStatusCol, resourceBundles); // $NON-NLS-1$
		
		objectsSelected = MessageFormat.format(objectsSelected, resourceBundles); // $NON-NLS-1$
		
		notificationVariableLbl = MessageFormat.format(notificationVariableLbl, resourceBundles); // $NON-NLS-1$
		notificationCCLbl = MessageFormat.format(notificationCCLbl, resourceBundles); // $NON-NLS-1$
		notiAddToBtn = MessageFormat.format(notiAddToBtn, resourceBundles); // $NON-NLS-1$
		notiAddToCCBtn = MessageFormat.format(notiAddToCCBtn, resourceBundles); // $NON-NLS-1$
		notiTemplateLbl = MessageFormat.format(notiTemplateLbl, resourceBundles); // $NON-NLS-1$
		notiUserFilteRadioBtnLbl = MessageFormat.format(notiUserFilteRadioBtnLbl, resourceBundles); // $NON-NLS-1$
		notiUserGrpFilteRadioBtnLbl = MessageFormat.format(notiUserGrpFilteRadioBtnLbl, resourceBundles); // $NON-NLS-1$
		notiInvalidTemplateMsg = MessageFormat.format(notiInvalidTemplateMsg, resourceBundles); // $NON-NLS-1$
		notiInvalidToVariableMsg = MessageFormat.format(notiInvalidToVariableMsg, resourceBundles); // $NON-NLS-1$
		notiInvalidCCVariableMsg = MessageFormat.format(notiInvalidCCVariableMsg, resourceBundles); // $NON-NLS-1$
		existingNotificationNameError = MessageFormat.format(existingNotificationNameError, resourceBundles); // $NON-NLS-1$
		notiEmptyTemplateError = MessageFormat.format(notiEmptyTemplateError, resourceBundles); // $NON-NLS-1$
		
		liveMsgStartDateErrorTitle = MessageFormat.format(liveMsgStartDateErrorTitle, resourceBundles); // $NON-NLS-1$
		liveMsgStartDateErrorMsg = MessageFormat.format(liveMsgStartDateErrorMsg, resourceBundles); // $NON-NLS-1$
		liveMsgExpiryDateErrorTitle = MessageFormat.format(liveMsgExpiryDateErrorTitle, resourceBundles); // $NON-NLS-1$
		liveMsgExpiryDateErrorMsg = MessageFormat.format(liveMsgExpiryDateErrorMsg, resourceBundles); // $NON-NLS-1$
		liveMsgInvalidObjNameErrorTitle = MessageFormat.format(liveMsgInvalidObjNameErrorTitle, resourceBundles); // $NON-NLS-1$
		liveMsgInvalidObjNameErrorMsg = MessageFormat.format(liveMsgInvalidObjNameErrorMsg, resourceBundles); // $NON-NLS-1$
		liveMsgInvalidRemoveErrorTilte = MessageFormat.format(liveMsgInvalidRemoveErrorTilte, resourceBundles); // $NON-NLS-1$
		liveMsgInvalidRemoveErrorMsg = MessageFormat.format(liveMsgInvalidRemoveErrorMsg, resourceBundles); // $NON-NLS-1$
		liveMsgObjRemoveErrorMsg = MessageFormat.format(liveMsgObjRemoveErrorMsg, resourceBundles); // $NON-NLS-1$
		liveMsgInvalidEndDateErrMsg = MessageFormat.format(liveMsgInvalidEndDateErrMsg, resourceBundles); // $NON-NLS-1$
		liveMsgInvalidObjCombinationMsgPart1 = MessageFormat.format(liveMsgInvalidObjCombinationMsgPart1, resourceBundles); // $NON-NLS-1$
		liveMsgInvalidObjCombinationMsgPart2 = MessageFormat.format(liveMsgInvalidObjCombinationMsgPart2, resourceBundles); // $NON-NLS-1$
		liveMsgInvalidObjCombinationTitle = MessageFormat.format(liveMsgInvalidObjCombinationTitle, resourceBundles); // $NON-NLS-1$
		
		historyReload = MessageFormat.format(historyReload, resourceBundles); // $NON-NLS-1$
		historyExportBtn = MessageFormat.format(historyExportBtn, resourceBundles); // $NON-NLS-1$
		historyLimitLbl = MessageFormat.format(historyLimitLbl, resourceBundles); // $NON-NLS-1$
		historyDefaultBtn = MessageFormat.format(historyDefaultBtn, resourceBundles); // $NON-NLS-1$
		historyApplyBtn = MessageFormat.format(historyApplyBtn, resourceBundles); // $NON-NLS-1$
		historyLogTimeLabel = MessageFormat.format(historyLogTimeLabel, resourceBundles); // $NON-NLS-1$
		historyStartDateLabel = MessageFormat.format(historyStartDateLabel, resourceBundles); // $NON-NLS-1$
		historyEndDateLabel = MessageFormat.format(historyEndDateLabel, resourceBundles); // $NON-NLS-1$
		historyIdLabel = MessageFormat.format(historyIdLabel, resourceBundles); // $NON-NLS-1$
		adminHistoryDialogTitle = MessageFormat.format(adminHistoryDialogTitle, resourceBundles); // $NON-NLS-1$
		userHistoryFilterLabel = MessageFormat.format(userHistoryFilterLabel, resourceBundles); // $NON-NLS-1$
		userStatusFilterLbl = MessageFormat.format(userStatusFilterLbl, resourceBundles); // $NON-NLS-1$
		userHistoryBtn = MessageFormat.format(userHistoryBtn, resourceBundles); // $NON-NLS-1$
		userStatusBtn = MessageFormat.format(userStatusBtn, resourceBundles); // $NON-NLS-1$
		userHistoryUserNameLbl = MessageFormat.format(userHistoryUserNameLbl, resourceBundles); // $NON-NLS-1$
		userHistoryHostLbl = MessageFormat.format(userHistoryHostLbl, resourceBundles); // $NON-NLS-1$
		userHistorySiteLbl = MessageFormat.format(userHistorySiteLbl, resourceBundles); // $NON-NLS-1$
		userHistoryAdminAreaLbl = MessageFormat.format(userHistoryAdminAreaLbl, resourceBundles); // $NON-NLS-1$
		userHistoryProjectLbl = MessageFormat.format(userHistoryProjectLbl, resourceBundles); // $NON-NLS-1$
		userHistoryApplicationLbl = MessageFormat.format(userHistoryApplicationLbl, resourceBundles); // $NON-NLS-1$
		userHistoryResultLbl = MessageFormat.format(userHistoryResultLbl, resourceBundles); // $NON-NLS-1$
		userHistoryAgrsLbl = MessageFormat.format(userHistoryAgrsLbl, resourceBundles); // $NON-NLS-1$
		userHistoryPIDLbl = MessageFormat.format(userHistoryPIDLbl, resourceBundles); // $NON-NLS-1$
		
		contentPropControlMessage = MessageFormat.format(contentPropControlMessage, resourceBundles); // $NON-NLS-1$
		contentPropDecorationInfo = MessageFormat.format(contentPropDecorationInfo, resourceBundles); // $NON-NLS-1$
		
		errorDialogTitile = MessageFormat.format(errorDialogTitile, resourceBundles); // $NON-NLS-1$
		
		historyLimitErrorMsg =  MessageFormat.format(historyLimitErrorMsg, resourceBundles); // $NON-NLS-1$
		historyEmptyLimitMsg = MessageFormat.format(historyEmptyLimitMsg, resourceBundles); // $NON-NLS-1$
		
		unauthorizedLdapAccessMessage = MessageFormat.format(unauthorizedLdapAccessMessage, resourceBundles); // $NON-NLS-1$
		unauthorizedUserAccessMessage = MessageFormat.format(unauthorizedUserAccessMessage, resourceBundles); // $NON-NLS-1$
		
		dialogAppName = MessageFormat.format(dialogAppName, resourceBundles); // $NON-NLS-1$
		serverNotReachable = MessageFormat.format(serverNotReachable, resourceBundles); // $NON-NLS-1$
		adminHistoryDialogTitle = MessageFormat.format(adminHistoryDialogTitle, resourceBundles); // $NON-NLS-1$
		adminHistoryRelFilterLabel = MessageFormat.format(adminHistoryRelFilterLabel, resourceBundles); // $NON-NLS-1$
		adminHistoryBaseFilterLabel = MessageFormat.format(adminHistoryBaseFilterLabel, resourceBundles); // $NON-NLS-1$
		adminHistoryRelRadioBtn = MessageFormat.format(adminHistoryRelRadioBtn, resourceBundles); // $NON-NLS-1$
		adminHistoryBaseObjBtn = MessageFormat.format(adminHistoryBaseObjBtn, resourceBundles); // $NON-NLS-1$
		adminHistoryAdminName = MessageFormat.format(adminHistoryAdminName, resourceBundles); // $NON-NLS-1$
		adminHistoryOperation = MessageFormat.format(adminHistoryOperation, resourceBundles); // $NON-NLS-1$
		adminHistoryAdminArea = MessageFormat.format(adminHistoryAdminArea, resourceBundles); // $NON-NLS-1$
		adminHistoryProject = MessageFormat.format(adminHistoryProject, resourceBundles); // $NON-NLS-1$
		adminHistoryProjectApp = MessageFormat.format(adminHistoryProjectApp, resourceBundles); // $NON-NLS-1$
		adminHistoryUserName = MessageFormat.format(adminHistoryUserName, resourceBundles); // $NON-NLS-1$
		adminHistoryStartApplication = MessageFormat.format(adminHistoryStartApplication, resourceBundles); // $NON-NLS-1$
		adminHistoryRelationType = MessageFormat.format(adminHistoryRelationType, resourceBundles); // $NON-NLS-1$
		adminHistoryStatus = MessageFormat.format(adminHistoryStatus, resourceBundles); // $NON-NLS-1$
		adminHistorySite = MessageFormat.format(adminHistorySite, resourceBundles); // $NON-NLS-1$
		adminHistoryGroupName = MessageFormat.format(adminHistoryGroupName, resourceBundles); // $NON-NLS-1$
		adminHistoryUserApplication = MessageFormat.format(adminHistoryUserApplication, resourceBundles); // $NON-NLS-1$
		adminHistoryDirectory = MessageFormat.format(adminHistoryDirectory, resourceBundles); // $NON-NLS-1$
		adminHistoryRole = MessageFormat.format(adminHistoryRole, resourceBundles); // $NON-NLS-1$
		adminHistoryLimit = MessageFormat.format(adminHistoryLimit, resourceBundles); // $NON-NLS-1$
		adminHistoryChanges = MessageFormat.format(adminHistoryChanges, resourceBundles); // $NON-NLS-1$
		adminHistoryErrorMessage = MessageFormat.format(adminHistoryErrorMessage, resourceBundles); // $NON-NLS-1$
		adminHistoryResult = MessageFormat.format(adminHistoryResult, resourceBundles); // $NON-NLS-1$
		adminHistoryObjectName = MessageFormat.format(adminHistoryObjectName, resourceBundles); // $NON-NLS-1$
		adminHistoryApiPathLbl = MessageFormat.format(adminHistoryApiPathLbl, resourceBundles); // $NON-NLS-1$
		
		hotlineContactError = MessageFormat.format(hotlineContactError, resourceBundles); // $NON-NLS-1$
		hotlineContactErrorTitle = MessageFormat.format(hotlineContactErrorTitle, resourceBundles); // $NON-NLS-1$
		hotlineEmailIDErrorTitle = MessageFormat.format(hotlineEmailIDErrorTitle, resourceBundles); // $NON-NLS-1$
		hotlineEmailIDError = MessageFormat.format(hotlineEmailIDError, resourceBundles); // $NON-NLS-1$
		
		aboutDialogTitle = MessageFormat.format(aboutDialogTitle, resourceBundles); // $NON-NLS-1$
		exitDialogTitle =  MessageFormat.format(exitDialogTitle, resourceBundles); // $NON-NLS-1$
		exitDialogMessage = MessageFormat.format(exitDialogMessage, resourceBundles); // $NON-NLS-1$
		
		loginDialogUserName = MessageFormat.format(loginDialogUserName, resourceBundles); // $NON-NLS-1$
		loginDialogPassword = MessageFormat.format(loginDialogPassword, resourceBundles); // $NON-NLS-1$
		loginDialogLoginBtn =  MessageFormat.format(loginDialogLoginBtn, resourceBundles); // $NON-NLS-1$
		loginDialogCancelBtn = MessageFormat.format(loginDialogCancelBtn, resourceBundles); // $NON-NLS-1$
		
		userReviewDialogTitle = MessageFormat.format(userReviewDialogTitle, resourceBundles); // $NON-NLS-1$
		userReviewDialogMsg = MessageFormat.format(userReviewDialogMsg, resourceBundles); // $NON-NLS-1$
		userReviewErrorDialogMsg = MessageFormat.format(userReviewErrorDialogMsg, resourceBundles); // $NON-NLS-1$
		userReviewErrorDialogTitle = MessageFormat.format(userReviewErrorDialogTitle, resourceBundles); // $NON-NLS-1$
		
		statusbarFromLbl = MessageFormat.format(statusbarFromLbl, resourceBundles); // $NON-NLS-1$
		statusBarInLbl = MessageFormat.format(statusBarInLbl, resourceBundles); // $NON-NLS-1$
		statusBarSelectedLbl = MessageFormat.format(statusBarSelectedLbl, resourceBundles); // $NON-NLS-1$
		statusBarFoundLbl = MessageFormat.format(statusBarFoundLbl, resourceBundles); // $NON-NLS-1$
		
		objectDelete = MessageFormat.format(objectDelete, resourceBundles); // $NON-NLS-1$
		objectCreate = MessageFormat.format(objectCreate, resourceBundles); // $NON-NLS-1$
		objectUpdate = MessageFormat.format(objectUpdate, resourceBundles); // $NON-NLS-1$
		objectStatusUpdate = MessageFormat.format(objectStatusUpdate, resourceBundles); // $NON-NLS-1$
		relationRemove = MessageFormat.format(relationRemove, resourceBundles); // $NON-NLS-1$
		userImport = MessageFormat.format(userImport, resourceBundles); // $NON-NLS-1$
		addIcon = MessageFormat.format(addIcon, resourceBundles); // $NON-NLS-1$
		siteObject = MessageFormat.format(siteObject, resourceBundles); // $NON-NLS-1$
		administrationAreaObject = MessageFormat.format(administrationAreaObject, resourceBundles); // $NON-NLS-1$
		userObject = MessageFormat.format(userObject, resourceBundles); // $NON-NLS-1$
		projectObject = MessageFormat.format(projectObject, resourceBundles); // $NON-NLS-1$
		userApplicationObject = MessageFormat.format(userApplicationObject, resourceBundles); // $NON-NLS-1$
		projectApplicationObject = MessageFormat.format(projectApplicationObject, resourceBundles); // $NON-NLS-1$
		eventApplicationObject = MessageFormat.format(eventApplicationObject, resourceBundles); // $NON-NLS-1$
		baseApplicationObject = MessageFormat.format(baseApplicationObject, resourceBundles); // $NON-NLS-1$
		startApplicationObject = MessageFormat.format(startApplicationObject, resourceBundles); // $NON-NLS-1$
		userGroupObject = MessageFormat.format(userGroupObject, resourceBundles); // $NON-NLS-1$
		projectGroupObject = MessageFormat.format(projectGroupObject, resourceBundles); // $NON-NLS-1$
		userAppGroupObject = MessageFormat.format(userAppGroupObject, resourceBundles); // $NON-NLS-1$
		projectAppGroupObject = MessageFormat.format(projectAppGroupObject, resourceBundles); // $NON-NLS-1$
		directoryObject = MessageFormat.format(directoryObject, resourceBundles); // $NON-NLS-1$
		iconObject = MessageFormat.format(iconObject, resourceBundles); // $NON-NLS-1$
		roleObject = MessageFormat.format(roleObject, resourceBundles); // $NON-NLS-1$
		liveMsgConfigObject = MessageFormat.format(liveMsgConfigObject, resourceBundles); // $NON-NLS-1$
		templateObject = MessageFormat.format(templateObject, resourceBundles);
		relationsLbl = MessageFormat.format(relationsLbl, resourceBundles); // $NON-NLS-1$
		relationLbl = MessageFormat.format(relationLbl, resourceBundles); // $NON-NLS-1$
		betweenLbl = MessageFormat.format(betweenLbl, resourceBundles); // $NON-NLS-1$
		andLbl = MessageFormat.format(andLbl, resourceBundles); // $NON-NLS-1$
		withLbl = MessageFormat.format(withLbl, resourceBundles); // $NON-NLS-1$
		relationStatusLbl = MessageFormat.format(relationStatusLbl, resourceBundles); // $NON-NLS-1$
		deleteErrorMsg = MessageFormat.format(deleteErrorMsg, resourceBundles); // $NON-NLS-1$
		forLbl = MessageFormat.format(forLbl, resourceBundles); // $NON-NLS-1$
		germanLanguageChangeMsg = MessageFormat.format(germanLanguageChangeMsg, resourceBundles); // $NON-NLS-1$
		englishLanguageChangeMsg = MessageFormat.format(englishLanguageChangeMsg, resourceBundles); // $NON-NLS-1$
		actionLbl = MessageFormat.format(actionLbl, resourceBundles); // $NON-NLS-1$
		assignedLbl = MessageFormat.format(assignedLbl, resourceBundles); // $NON-NLS-1$
		successfullyLbl = MessageFormat.format(successfullyLbl, resourceBundles); // $NON-NLS-1$
		eventLbl = MessageFormat.format(eventLbl, resourceBundles); // $NON-NLS-1$
		
		workspaceLockTitle = MessageFormat.format(workspaceLockTitle, resourceBundles); // $NON-NLS-1$
		workspaceLockMsg = MessageFormat.format(workspaceLockMsg, resourceBundles); // $NON-NLS-1$
		
		iconAlreadyExistPart1 = MessageFormat.format(iconAlreadyExistPart1, resourceBundles); // $NON-NLS-1$
		iconAlreadyExistPart2 = MessageFormat.format(iconAlreadyExistPart2, resourceBundles); // $NON-NLS-1$
		deleteTemplateConfirmDialogMsg = MessageFormat.format(deleteTemplateConfirmDialogMsg, resourceBundles); // $NON-NLS-1$
		deleteNotiActionConfirmDialogMsg = MessageFormat.format(deleteNotiActionConfirmDialogMsg, resourceBundles); // $NON-NLS-1$
		
		inCorrectServerURI = MessageFormat.format(inCorrectServerURI, resourceBundles); // $NON-NLS-1$
	}
}
