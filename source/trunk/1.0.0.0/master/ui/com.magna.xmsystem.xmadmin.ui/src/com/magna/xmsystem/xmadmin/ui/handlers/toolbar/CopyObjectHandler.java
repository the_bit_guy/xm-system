package com.magna.xmsystem.xmadmin.ui.handlers.toolbar;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.eclipse.e4.core.contexts.Active;
import org.eclipse.e4.core.di.annotations.CanExecute;
import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.ui.model.application.MApplication;
import org.eclipse.e4.ui.model.application.ui.basic.MPart;
import org.eclipse.e4.ui.model.application.ui.menu.MToolItem;
import org.eclipse.e4.ui.workbench.modeling.EModelService;
import org.eclipse.jface.viewers.ITreeSelection;
import org.eclipse.swt.dnd.Clipboard;
import org.eclipse.swt.dnd.Transfer;
import org.eclipse.swt.widgets.Display;

import com.magna.xmsystem.xmadmin.ui.parts.IEditablePart;
import com.magna.xmsystem.xmadmin.ui.parts.InformationPart;
import com.magna.xmsystem.xmadmin.ui.parts.dnd.ObjectTypeTransfer;
import com.magna.xmsystem.xmadmin.ui.parts.objexp.ObjectExpPage;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.AdminTreeDataLoad;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.projectapplicationmodel.ProjectApplication;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.userapplicationmodel.UserApplication;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.group.projectappgroup.ProjectAppGroupProjectApps;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.group.projectappgroup.ProjectApplicationGroup;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.group.projectgroup.ProjectGroupModel;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.group.projectgroup.ProjectGroupProjects;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.group.userappgroup.UserAppGroupUserApps;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.group.userappgroup.UserApplicationGroup;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.group.usergroup.UserGroupModel;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.group.usergroup.UserGroupUsers;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.projects.Project;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.refs.RelationObj;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.users.User;
import com.magna.xmsystem.xmadmin.util.CommonConstants;
import com.magna.xmsystem.xmadmin.util.XMAdminUtil;

/**
 * The Class CopyObjectHandler.
 * 
 * @author subash.janarthanan
 * 
 */
public class CopyObjectHandler {

	/**
	 * Execute.
	 */
	@SuppressWarnings("unchecked")
	@Execute
	public void execute() {
		Clipboard clipboard = new Clipboard(Display.getCurrent());
		XMAdminUtil instance = XMAdminUtil.getInstance();
		MPart mPart = instance.getInformationPart();
		InformationPart rightSidePart = (InformationPart) mPart.getObject();
		ObjectExpPage objectExpPage = rightSidePart.getObjectExpPartUI();
		ITreeSelection structuredSelection = objectExpPage.getObjExpTableViewer().getStructuredSelection();
		//Collection<StructuredSelection> seletion = structuredSelection.values();
		List<Object> sourceElementList = new ArrayList<>();
		Object[] selectionArray = structuredSelection.toArray();
		for (Object firstElement : selectionArray) {
			//Object firstElement = ((StructuredSelection) selectionObj).getFirstElement();
			if (firstElement instanceof UserGroupModel) {
				UserGroupModel userGroupModel = (UserGroupModel) firstElement;
				Map<String, IAdminTreeChild> userGroupChild = userGroupModel.getUserGroupChildren();
				IAdminTreeChild iAdminTreeChild = userGroupChild
						.get(UserGroupUsers.class.getSimpleName());
				List<IAdminTreeChild> userGroupUsersChild = AdminTreeDataLoad.getInstance().loadUserGroupUsersFromService(iAdminTreeChild);
				for (IAdminTreeChild iAdminTreeChild2 : userGroupUsersChild) {
					RelationObj relObj = (RelationObj) iAdminTreeChild2;
					User user = (User) relObj.getRefObject();
					sourceElementList.add(user);
				}		
			}else if (firstElement instanceof ProjectGroupModel) {
				ProjectGroupModel projectGroupModel = (ProjectGroupModel) firstElement;
				Map<String, IAdminTreeChild> projectGroupChild = projectGroupModel
						.getProjectGroupChildren();
				IAdminTreeChild iAdminTreeChild = projectGroupChild
						.get(ProjectGroupProjects.class.getSimpleName());
				List<IAdminTreeChild> projectGroupProjectsChild = AdminTreeDataLoad.getInstance().loadProjectGroupProjectsFromService(iAdminTreeChild);
				for (IAdminTreeChild iAdminTreeChild2 : projectGroupProjectsChild) {
					RelationObj relObj = (RelationObj) iAdminTreeChild2;
					Project project = (Project) relObj.getRefObject();
					sourceElementList.add(project);
				}
			} else if (firstElement instanceof UserApplicationGroup) {
				UserApplicationGroup userApplicationGroup = (UserApplicationGroup) firstElement;
				Map<String, IAdminTreeChild> userAppGroupChild = userApplicationGroup
						.getUserAppGroupChildren();
				IAdminTreeChild iAdminTreeChild = userAppGroupChild
						.get(UserAppGroupUserApps.class.getSimpleName());
				List<IAdminTreeChild> userAppGroupUserAppsChild = AdminTreeDataLoad.getInstance().loadUserAppGroupUserAppsFromService(iAdminTreeChild);
				for (IAdminTreeChild iAdminTreeChild2 : userAppGroupUserAppsChild) {
					RelationObj relObj = (RelationObj) iAdminTreeChild2;
					UserApplication userApp = (UserApplication) relObj.getRefObject();
					sourceElementList.add(userApp);
				}
			} else if (firstElement instanceof ProjectApplicationGroup) {
				ProjectApplicationGroup projectApplicationGroup = (ProjectApplicationGroup) firstElement;
				Map<String, IAdminTreeChild> projectAppGroupChild = projectApplicationGroup
						.getProjectAppGroupChildren();
				IAdminTreeChild iAdminTreeChild = projectAppGroupChild
						.get(ProjectAppGroupProjectApps.class.getSimpleName());
				List<IAdminTreeChild> projecAppGroupProjectAppsChild = AdminTreeDataLoad.getInstance().loadProjectAppGroupProjectAppsFromService(iAdminTreeChild);
				for (IAdminTreeChild iAdminTreeChild2 : projecAppGroupProjectAppsChild) {
					RelationObj relObj = (RelationObj) iAdminTreeChild2;
					ProjectApplication projectApp = (ProjectApplication) relObj.getRefObject();
					sourceElementList.add(projectApp);
				}
			} else{
			sourceElementList.addAll(structuredSelection.toList());
			break;
			}
		}
		clipboard.setContents(new Object[] { sourceElementList },
				new Transfer[] { ObjectTypeTransfer.getInstance() });
	}

	/**
	 * Can execute.
	 *
	 * @return true, if successful
	 */
	@CanExecute
	public boolean canExecute(@Active MPart part, MApplication application, EModelService service,
			EModelService eModelService) {
		if (part.getElementId().equals(CommonConstants.PART_ID.INFORMATION_PART_ID)) {
			XMAdminUtil instance = XMAdminUtil.getInstance();
			MPart mPart = instance.getInformationPart();
			InformationPart rightSidePart = (InformationPart) mPart.getObject();
			if (rightSidePart instanceof IEditablePart && rightSidePart.isDirty()) {
				return false;
			}
			ObjectExpPage objectExpPage = rightSidePart.getObjectExpPartUI();
			ITreeSelection structuredSelections = objectExpPage.getObjExpTableViewer().getStructuredSelection();
			boolean returnValue = structuredSelections.size() > 0;
			if (returnValue) {
				List<MToolItem> mToolItems = service.findElements(application,
						"com.magna.xmsystem.xmadmin.ui.handledtoolitem.copy", MToolItem.class, null); //$NON-NLS-1$
				if (mToolItems.size() > 0) {
					mToolItems.get(0).setEnabled(returnValue);
				}
				return returnValue;
			}
		}
		return false;
	}
}
