
package com.magna.xmsystem.xmadmin.ui.handlers.adminarea;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.inject.Inject;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.e4.core.di.annotations.CanExecute;
import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.core.services.nls.Translation;
import org.eclipse.e4.ui.model.application.MApplication;
import org.eclipse.e4.ui.model.application.ui.basic.MPart;
import org.eclipse.e4.ui.workbench.modeling.EModelService;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.swt.widgets.Display;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.client.ResourceAccessException;

import com.magna.xmbackend.vo.adminArea.AdminAreaResponse;
import com.magna.xmsystem.dependencies.customExceptions.UnauthorizedAccessException;
import com.magna.xmsystem.ui.controls.widgets.CustomMessageDialog;
import com.magna.xmsystem.xmadmin.message.Message;
import com.magna.xmsystem.xmadmin.restclient.adminArea.AdminAreaController;
import com.magna.xmsystem.xmadmin.ui.parts.IEditablePart;
import com.magna.xmsystem.xmadmin.ui.parts.InformationPart;
import com.magna.xmsystem.xmadmin.ui.parts.objexp.ObjectExpPage;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.AdminTreeviewer;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.AdminTreeFactory;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.adminarea.AdministrationArea;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.adminarea.AdministrationAreas;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.projectapplicationmodel.ProjectAppAdminAreas;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.roles.RoleScopeObjects;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.projects.ProjectAdminAreas;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.projects.ProjectUserAdminAreas;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.sitemodel.SiteAdministrations;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.users.UserAdminAreas;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.users.UserProjectAdminAreas;
import com.magna.xmsystem.xmadmin.util.CommonConstants;
import com.magna.xmsystem.xmadmin.util.MessageType;
import com.magna.xmsystem.xmadmin.util.XMAdminUtil;

/**
 * The Class DeleteAdminArea.
 * 
 * @author Archita.patel
 */
public class DeleteAdminArea {

	/** The Constant LOGGER. */
	private static final Logger LOGGER = LoggerFactory.getLogger(DeleteAdminArea.class);

	/** Member variable for {@link Message}. */
	@Inject
	@Translation
	private Message messages;

	/** Inject of {@link MApplication}. */
	@Inject
	private MApplication application;

	/** Inject of {@link EModelService}. */
	@Inject
	private EModelService modelService;

	/**
	 * Method to delete the adminArea.
	 */
	@Execute
	public void execute() {
		try {
			if (XMAdminUtil.getInstance().isAcessAllowed("ADMINISTRATION_AREA-DELETE")) {
				final AdminTreeviewer adminTree = XMAdminUtil.getInstance().getAdminTree();
				IStructuredSelection selection;
				Object selectionObj;

				MPart mPart = (MPart) this.modelService.find(CommonConstants.PART_ID.INFORMATION_PART_ID, application);
				InformationPart rightSidePart2 = (InformationPart) mPart.getObject();
				ObjectExpPage objectExpPage = rightSidePart2.getObjectExpPartUI();
				if (objectExpPage != null
						&& (selection = objectExpPage.getObjExpTableViewer().getStructuredSelection()) != null
						&& (selectionObj = selection.getFirstElement()) != null
						&& selectionObj instanceof AdministrationArea) {
					deleteAdminArea(selection);
					objectExpPage.refreshExplorer();
				} else if (adminTree != null && (selection = adminTree.getStructuredSelection()) != null
						&& (selectionObj = selection.getFirstElement()) != null
						&& selectionObj instanceof AdministrationArea) {
					deleteAdminArea(selection);
				}
				XMAdminUtil.getInstance().clearClipBoardContents();
			} else {
				CustomMessageDialog.openError(Display.getCurrent().getActiveShell(), messages.objectPermissionDialogTitle,
						messages.objectPermissionDialogMsg);
			}
		} catch (ClassCastException e) {
			LOGGER.error("Exception occured while deleting AdminArea",e);
		} catch (UnauthorizedAccessException e) {
			LOGGER.error("Current user is Unauthorized " + e);
		} catch (Exception e) {
			if (e instanceof ResourceAccessException) {
				CustomMessageDialog.openError(Display.getCurrent().getActiveShell(),
						messages.errorDialogTitile, messages.serverNotReachable);
			}
			LOGGER.error("Exception occured in calling access allowed API " + e);
		}
	}

	/**
	 * Delete admin area.
	 *
	 * @param selectionObj
	 *            the selection obj
	 */
	@SuppressWarnings("rawtypes")
	private void deleteAdminArea(IStructuredSelection selection) {
		Object selectionObj = selection.getFirstElement();
		final AdministrationAreas adminAreas = AdminTreeFactory.getInstance().getAdministrationAreas();
		final AdminTreeviewer adminTree = XMAdminUtil.getInstance().getAdminTree();
		final String adminAreaName = ((AdministrationArea) selectionObj).getName();
		String confirmDialogTitle = messages.deleteConfirmDialogTitle;
		String confirmDialogMsg;
		if (selection.size() == 1) {
			confirmDialogMsg = messages.deleteAARelationConfirmDialogMsgPart1 + " \'" + adminAreaName + "\' "
					+ messages.deleteAARelationConfirmDialogMsgPart2 + " ?";
		} else {
			confirmDialogMsg = messages.deleteMultiObjectsConfirmDialogMsg + " "
					+ messages.deleteAARelationConfirmDialogMsgPart2 + " ?";
		}
		boolean isConfirmed = CustomMessageDialog.openConfirm(adminTree.getControl().getShell(), confirmDialogTitle,
				confirmDialogMsg);
		if (isConfirmed) {
			List selectionList = selection.toList();
			Job job = new Job("Deleting Objects...") {

				@Override
				protected IStatus run(IProgressMonitor monitor) {
					monitor.beginTask("Deleting Objects..", 100);
					monitor.worked(30);
					try {
						Set<String> adminAreaIds = new HashSet<>();
						Map<String,String> successObjectNames = new HashMap<>();
						Map<String,String> failObjectNames = new HashMap<>();
						for (int i = 0; i < selectionList.size(); i++) {
							String id = ((AdministrationArea) selectionList.get(i)).getAdministrationAreaId();
							String name = ((AdministrationArea) selectionList.get(i)).getName();
							adminAreaIds.add(id);
							successObjectNames.put(id, name);
						}
						final AdminAreaController adminAreaController = new AdminAreaController();
						final AdminAreaResponse adminAreaResponse = adminAreaController
								.deleteMultiAdminArea(adminAreaIds);
						if (adminAreaResponse != null) {
							if (!adminAreaResponse.getStatusMaps().isEmpty()) {
								List<Map<String, String>> statusMapList = adminAreaResponse.getStatusMaps();
								for (Map<String, String> statusMap : statusMapList) {
									String string = statusMap.get("en");
									String[] split = string.split("id");
									adminAreaIds.remove(split[1].trim());
									failObjectNames.put(split[1].trim(), successObjectNames.get(split[1].trim()));
									successObjectNames.remove(split[1].trim());
								}
								Display.getDefault().asyncExec(new Runnable() {
									public void run() {
										CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
												messages.deleteConfirmDialogTitle,
												messages.deleteMultiObjectsDialogErrMsg);
									}
								});
							}
							for (String adminAreaId : adminAreaIds) {
								adminAreas.getAdminstrationAreasChildren().remove(adminAreaId);
							}
							Display.getDefault().asyncExec(new Runnable() {
								@SuppressWarnings("unchecked")
								public void run() {
									adminTree.refershBackReference(new Class[] { SiteAdministrations.class,
											ProjectAdminAreas.class, ProjectUserAdminAreas.class,
											UserProjectAdminAreas.class, UserAdminAreas.class,
											ProjectAppAdminAreas.class, RoleScopeObjects.class });
									adminTree.refresh();
									adminTree.setSelection(new StructuredSelection(adminAreas), true);
								}
							});
							if (successObjectNames.size() > 0) {
								Iterator it = successObjectNames.entrySet().iterator();
								while (it.hasNext()) {
									Map.Entry map = (Map.Entry) it.next();
									XMAdminUtil.getInstance()
											.updateLogFile(messages.administrationAreaObject +" '"
													+ map.getValue() + "' "+ messages.objectDelete,
													MessageType.SUCCESS);
								}
							}
							if (failObjectNames.size() > 0) {
								Iterator it = failObjectNames.entrySet().iterator();
								while (it.hasNext()) {
									Map.Entry map = (Map.Entry) it.next();
									XMAdminUtil.getInstance()
											.updateLogFile(messages.deleteErrorMsg+" "+messages.administrationAreaObject+ " '"
													+ map.getValue() + "'",
													MessageType.FAILURE);
								}
							}
						}

					} catch (Exception e) {
						LOGGER.error(e.getMessage());
					}

					monitor.worked(70);
					return Status.OK_STATUS;
				}
			};
			job.setUser(true);
			job.schedule();
		}
	}

	/**
	 * Checks if is acess allowed.
	 *
	 * @return true, if is acess allowed
	 *//*
	public boolean isAcessAllowed() {
		boolean returnVal = true;
		try {
			ValidationController controller = new ValidationController();
			ValidationRequest request = new ValidationRequest();
			request.setUserName(RestClientUtil.getInstance().getUserName());
			request.setPermissionType(PermissionType.OBJECT_PERMISSION.name());
			request.setPermissionName("ADMINISTRATION_AREA-DELETE"); //$NON-NLS-1$
			returnVal = controller.getAccessAllowed(request);
		} catch (UnauthorizedAccessException e) {
			LOGGER.error("Current user is Unauthorized " + e);
			returnVal = false;
		} catch (Exception e) {
			LOGGER.error("Exception occured in calling access allowed API " + e);
			returnVal = false;
		}
		return returnVal;
	}*/

	/**
	 * Can execute.
	 *
	 * @return true, if successful
	 */
	@CanExecute
	public boolean canExecute() {
		boolean returnVal = true;
		final XMAdminUtil instance = XMAdminUtil.getInstance();
		MPart rightPart;
		Object rightView;
		if ((rightPart = instance.getInformationPart()) != null
				&& (rightView = rightPart.getObject()) != null && rightView instanceof IEditablePart
				&& ((IEditablePart) rightView).isDirty()) {
			returnVal = false;
		}
		return returnVal;
	}
}
