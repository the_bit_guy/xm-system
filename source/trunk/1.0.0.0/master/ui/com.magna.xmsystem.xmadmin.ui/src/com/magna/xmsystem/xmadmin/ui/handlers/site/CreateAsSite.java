
package com.magna.xmsystem.xmadmin.ui.handlers.site;

import javax.inject.Inject;

import org.eclipse.e4.core.di.annotations.CanExecute;
import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.core.services.nls.Translation;
import org.eclipse.e4.ui.model.application.MApplication;
import org.eclipse.e4.ui.model.application.ui.basic.MPart;
import org.eclipse.e4.ui.workbench.modeling.EModelService;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.swt.widgets.Display;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.client.ResourceAccessException;

import com.magna.xmsystem.dependencies.customExceptions.UnauthorizedAccessException;
import com.magna.xmsystem.ui.controls.widgets.CustomMessageDialog;
import com.magna.xmsystem.xmadmin.message.Message;
import com.magna.xmsystem.xmadmin.ui.parts.IEditablePart;
import com.magna.xmsystem.xmadmin.ui.parts.InformationPart;
import com.magna.xmsystem.xmadmin.ui.parts.site.SiteCompositeAction;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.AdminTreeviewer;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.sitemodel.Site;
import com.magna.xmsystem.xmadmin.util.CommonConstants;
import com.magna.xmsystem.xmadmin.util.XMAdminUtil;

/**
 * Class for Create As handler
 * 
 * @author shashwat.anand
 *
 */
public class CreateAsSite {
	/** The Constant LOGGER. */
	private static final Logger LOGGER = LoggerFactory.getLogger(CreateAsSite.class);
	
	/**
	 * Inject of {@link EModelService}
	 */
	@Inject
	private EModelService modelService;

	/**
	 * Inject of {@link MApplication}
	 */
	@Inject
	private MApplication application;

	/** Member variable for messages. */
	@Inject
	@Translation
	transient private Message messages;
	
	/**
	 * Execute method of CreateSite handler
	 */

	@Execute
	public void execute() {
		try {
			if (XMAdminUtil.getInstance().isAcessAllowed("SITE-CREATE")) {
				final AdminTreeviewer adminTree = XMAdminUtil.getInstance().getAdminTree();
				IStructuredSelection selection;
				Object selectionObj;
				if (adminTree != null && (selection = adminTree.getStructuredSelection()) != null
						&& (selectionObj = selection.getFirstElement()) != null && selectionObj instanceof Site) {

					MPart part = (MPart) this.modelService.find(CommonConstants.PART_ID.INFORMATION_PART_ID, application);
					InformationPart rightSidePart = (InformationPart) part.getObject();
					SiteCompositeAction siteCompositeUI = rightSidePart.getSiteCompositeUI();
					Site site = ((Site) selectionObj).deepCopySite(false, null);
					site.setSiteId(null);
					site.setOperationMode(CommonConstants.OPERATIONMODE.CREATE);
					siteCompositeUI.setModel(site);
					adminTree.setSelection(null);
				} 
			}else{
				CustomMessageDialog.openError(Display.getCurrent().getActiveShell(), messages.objectPermissionDialogTitle,
						messages.objectPermissionDialogMsg);
			}
		} catch (UnauthorizedAccessException e) {
			LOGGER.error("Current user is Unauthorized " + e);
		} catch (Exception e) {
			if (e instanceof ResourceAccessException) {
				CustomMessageDialog.openError(Display.getCurrent().getActiveShell(),
						messages.errorDialogTitile, messages.serverNotReachable);
			}
			LOGGER.error("Exception occured in calling access allowed API " + e);
		}
	}

	/**
	 * Checks if is acess allowed.
	 *
	 * @return true, if is acess allowed
	 *//*
	public boolean isAcessAllowed(){
		boolean returnVal = true;
		try {
			ValidationController controller = new ValidationController();
			ValidationRequest request = new ValidationRequest();
			request.setUserName(RestClientUtil.getInstance().getUserName());
			request.setPermissionType(PermissionType.OBJECT_PERMISSION.name());
			request.setPermissionName("SITE-CREATE"); //$NON-NLS-1$
			returnVal = controller.getAccessAllowed(request);
		} catch (UnauthorizedAccessException e) {
			LOGGER.error("Current user is Unauthorized " + e);
			returnVal = false;
		} catch (Exception e) {
			LOGGER.error("Exception occured in calling access allowed API " + e);
			returnVal = false;
		}
		return returnVal;
	}*/
	/**
	 * Can execute.
	 *
	 * @return true, if successful
	 */
	@CanExecute
	public boolean canExecute() {
		boolean returnVal = true;
		final XMAdminUtil instance = XMAdminUtil.getInstance();
		MPart rightPart;
		Object rightView;
		if ((rightPart = instance.getInformationPart()) != null
				&& (rightView = rightPart.getObject()) != null && rightView instanceof IEditablePart
				&& ((IEditablePart) rightView).isDirty()) {
			returnVal = false;
		}
		return returnVal;
	}
}