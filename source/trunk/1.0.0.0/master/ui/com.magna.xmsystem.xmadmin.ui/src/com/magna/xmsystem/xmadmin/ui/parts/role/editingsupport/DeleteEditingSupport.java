package com.magna.xmsystem.xmadmin.ui.parts.role.editingsupport;

import com.magna.xmbackend.vo.enums.CreationType;
import com.magna.xmbackend.vo.roles.ObjectPermission;
import com.magna.xmsystem.xmadmin.ui.parts.role.ObjectPermissionsTable;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.roles.ObjectType;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.roles.VoPermContainer;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.roles.objectpermissions.ObjectPermissions;
import com.magna.xmsystem.xmadmin.util.XMAdminUtil;

/**
 * The Class DeleteEditingSupport.
 * 
 * @author shashwat.anand
 */
public class DeleteEditingSupport extends CheckBoxEditingSupport {
	/** Member variable 'instance' for {@link XMAdminUtil}. */
	private XMAdminUtil instance;
	
	/** Member variable 'viewer' for {@link ObjectPermissionsTable}. */
	private ObjectPermissionsTable viewer;
	
	/**
	 * Instantiates a new delete editing support.
	 *
	 * @param viewer the viewer
	 */
	public DeleteEditingSupport(ObjectPermissionsTable viewer) {
		super(viewer);
		this.viewer = viewer;
		this.instance = XMAdminUtil.getInstance();
	}

	/* (non-Javadoc)
	 * @see org.eclipse.jface.viewers.EditingSupport#getValue(java.lang.Object)
	 */
	@Override
	protected Object getValue(final Object element) {
		if (element instanceof ObjectPermissions) {
			return ((ObjectPermissions) element).isDelete();
		}
		return "N";
	}

	/* (non-Javadoc)
	 * @see org.eclipse.jface.viewers.EditingSupport#setValue(java.lang.Object, java.lang.Object)
	 */
	@Override
	protected void setValue(final Object element, final Object value) {
		if (element instanceof ObjectPermissions) {
			ObjectPermissions elem = (ObjectPermissions) element;
			ObjectType objectType = ObjectType.getObjectType(elem.getPermissionName());
			String permissionId = this.instance.getObjectPermissionMap().get(objectType).getDeletePermission().getPermissionId();
			VoPermContainer voObjectPermContainer = new VoPermContainer();
			ObjectPermission objectPermission = new ObjectPermission();
			objectPermission.setPermissionId(permissionId);
			voObjectPermContainer.setObjectPermission(objectPermission);
			if (elem.isDelete() && ! (boolean) value) {
				objectPermission.setCreationType(CreationType.DELETE);
				elem.setDeletePermission(voObjectPermContainer);
			} else if (!elem.isDelete() && (boolean) value) {
				objectPermission.setCreationType(CreationType.ADD);
				elem.setDeletePermission(voObjectPermContainer);
			} else {
				return;
			}
			elem.setDelete((boolean) value);
			this.viewer.getCachedObjPermissionMap().put(permissionId, voObjectPermContainer);
			getViewer().refresh(element);
		}
	}
	
	/* (non-Javadoc)
	 * @see com.magna.xmsystem.xmadmin.ui.parts.role.editingsupport.CheckBoxEditingSupport#canEdit(java.lang.Object)
	 */
	@Override
	protected boolean canEdit(final Object element) {
		if (element instanceof ObjectPermissions) {
			String permissionName = ((ObjectPermissions) element).getPermissionName();
			ObjectType objectType = ObjectType.getObjectType(permissionName);
			if (objectType == ObjectType.PROPERTY_CONFIG) {
				return false;
			}
		}
		return true;
	}
}
