package com.magna.xmsystem.xmadmin.ui.parts.dnd;

import org.eclipse.jface.util.LocalSelectionTransfer;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.swt.dnd.DragSourceAdapter;
import org.eclipse.swt.dnd.DragSourceEvent;

/**
 * The Class NodeDragListener
 *
 * @author subash.janarthanan
 * 
 */
public class NodeDragListener extends DragSourceAdapter {

	/** The tree viewer. */
	private TreeViewer treeViewer;
	private LocalSelectionTransfer transfer;

	/**
	 * Instantiates a new node drag listener.
	 *
	 * @param treeViewer
	 *            the tree viewer
	 * @param transfer 
	 */
	public NodeDragListener(TreeViewer treeViewer, LocalSelectionTransfer transfer) {
		this.treeViewer = treeViewer;
		this.transfer = transfer;
	}

	
	/* (non-Javadoc)
	 * @see org.eclipse.swt.dnd.DragSourceAdapter#dragStart(org.eclipse.swt.dnd.DragSourceEvent)
	 */
	@Override
	public void dragStart(DragSourceEvent event) {
		ISelection selection = treeViewer.getSelection();
		if (selection instanceof IStructuredSelection) {
			transfer.setSelection(new StructuredSelection(new Object[]{ selection }));
		} else{
			event.doit = false;
		}
	}
}