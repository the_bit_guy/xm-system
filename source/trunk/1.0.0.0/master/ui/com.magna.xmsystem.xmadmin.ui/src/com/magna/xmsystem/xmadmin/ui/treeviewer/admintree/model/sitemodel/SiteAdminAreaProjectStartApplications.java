package com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.sitemodel;

import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild;

/**
 * The Class SiteAdminAreaChildStartApplications.
 * 
 * @author subash.janarthanan
 * 
 */
public class SiteAdminAreaProjectStartApplications implements IAdminTreeChild {
	/** The parent. */
	private IAdminTreeChild parent;

	public SiteAdminAreaProjectStartApplications(final IAdminTreeChild parent) {
		this.parent = parent;
	}
	
	/* (non-Javadoc)
	 * @see com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild#getParent()
	 */
	@Override
	public IAdminTreeChild getParent() {
		return this.parent;
	}

	/* (non-Javadoc)
	 * @see com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild#setParent(com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild)
	 */
	@Override
	public void setParent(final IAdminTreeChild parent) {
		this.parent = parent;
	}
}
