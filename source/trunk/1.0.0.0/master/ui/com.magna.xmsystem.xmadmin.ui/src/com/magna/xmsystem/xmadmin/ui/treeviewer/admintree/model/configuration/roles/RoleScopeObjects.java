package com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.roles;

import java.util.LinkedHashMap;
import java.util.Map;

import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild;

/**
 * The Class ScopeObjectParent.
 * 
 * @author shashwat.anand
 */
public class RoleScopeObjects implements IAdminTreeChild {
	/** The parent. */
	private IAdminTreeChild parent;
	/** The role child. */
	private Map<String, IAdminTreeChild> roleScopeObjectsChildren;

	/** Member variable 'name' for {@link String}. */
	private String name;

	/**
	 * Constructor for ScopeObjects Class.
	 *
	 * @param parent
	 *            {@link IAdminTreeChild}
	 * @param name
	 *            {@link String}
	 */
	public RoleScopeObjects(final IAdminTreeChild parent, final String name) {
		super();
		this.parent = parent;
		this.name = name;
		this.roleScopeObjectsChildren = new LinkedHashMap<>();
	}

	/**
	 * Add.
	 *
	 * @param scopeObjId
	 *            {@link String}
	 * @param child
	 *            the child
	 * @return the i admin tree child
	 */
	public IAdminTreeChild add(final String scopeObjId, final IAdminTreeChild child) {
		child.setParent(this);
		return this.roleScopeObjectsChildren.put(scopeObjId, child);
	}

	/**
	 * Remove.
	 *
	 * @param adminAreaId
	 *            the admin area id
	 * @return the i admin tree child
	 */
	public IAdminTreeChild remove(final String adminAreaId) {
		return this.roleScopeObjectsChildren.remove(adminAreaId);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild#
	 * getParent()
	 */
	@Override
	public IAdminTreeChild getParent() {
		return this.parent;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild#
	 * setParent(com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.
	 * IAdminTreeChild)
	 */
	@Override
	public void setParent(final IAdminTreeChild parent) {
		this.parent = parent;
	}

	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Sets the name.
	 *
	 * @param name
	 *            the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Gets the scope objects children.
	 *
	 * @return the scopeObjects
	 */
	public Map<String, IAdminTreeChild> getRoleScopeObjectsChildren() {
		return roleScopeObjectsChildren;
	}
}
