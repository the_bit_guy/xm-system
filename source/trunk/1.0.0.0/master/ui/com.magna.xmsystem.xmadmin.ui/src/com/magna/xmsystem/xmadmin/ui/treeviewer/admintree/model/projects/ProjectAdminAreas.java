package com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.projects;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.stream.Collectors;

import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.adminarea.AdministrationArea;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.icon.Icon;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.refs.RelationObj;

/**
 * Class for Project admin areas.
 *
 * @author Chiranjeevi.Akula
 */
public class ProjectAdminAreas implements IAdminTreeChild {

	/** Member variable 'project admin area children' for {@link Map<String,IAdminTreeChild>}. */
	private Map<String, IAdminTreeChild> projectAdminAreaChildren;

	/** Member variable 'icon' for {@link Icon}. */
	private Icon icon;

	/** Member variable 'active' for {@link Boolean}. */
	private boolean active;

	/** Member variable 'name' for {@link String}. */
	private String name;

	/** Member variable 'parent' for {@link IAdminTreeChild}. */
	private IAdminTreeChild parent;

	/**
	 * Constructor for ProjectAdminAreas Class.
	 *
	 * @param parent {@link IAdminTreeChild}
	 */
	public ProjectAdminAreas(IAdminTreeChild parent) {
		this.parent = parent;
		this.projectAdminAreaChildren = new LinkedHashMap<>();
	}

	/**
	 * Method for Adds the.
	 *
	 * @param projectAdminAreaChildrenId {@link String}
	 * @param child {@link IAdminTreeChild}
	 * @return the i admin tree child {@link IAdminTreeChild}
	 */
	public IAdminTreeChild add(final String projectAdminAreaChildrenId, final IAdminTreeChild child) {
		child.setParent(this);
		IAdminTreeChild returnVal = this.projectAdminAreaChildren.put(projectAdminAreaChildrenId, child);
		if (child instanceof RelationObj && ((RelationObj) child).getRefObject() instanceof AdministrationArea) {
			sort();
		}
		return returnVal;
	}

	/**
	 * Method for Removes the.
	 *
	 * @param projectAdminAreaChildrenId {@link String}
	 * @return the i admin tree child {@link IAdminTreeChild}
	 */
	public IAdminTreeChild remove(final String projectAdminAreaChildrenId) {
		return this.projectAdminAreaChildren.remove(projectAdminAreaChildrenId);
	}

	/**
	 * Method for Removes the all.
	 */
	public void removeAll() {
		this.projectAdminAreaChildren.clear();
	}
	
	/**
	 * Gets the project admin area children.
	 *
	 * @return the project admin area children
	 */
	public Map<String, IAdminTreeChild> getProjectAdminAreaChildren() {
		return projectAdminAreaChildren;
	}

	/**
	 * Gets the project admin area collection.
	 *
	 * @return the project admin area collection
	 */
	public Collection<IAdminTreeChild> getProjectAdminAreaCollection() {
		return this.projectAdminAreaChildren.values();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild#
	 * getParent()
	 */
	@Override
	public IAdminTreeChild getParent() {
		return this.parent;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild#
	 * setParent(com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.
	 * IAdminTreeChild)
	 */
	@Override
	public void setParent(IAdminTreeChild parent) {
		this.parent = parent;
	}

	/**
	 * Gets the icon.
	 *
	 * @return the icon
	 */
	public Icon getIcon() {
		return icon;
	}

	/**
	 * Sets the icon.
	 *
	 * @param icon the new icon
	 */
	public void setIcon(Icon icon) {
		this.icon = icon;
	}

	/**
	 * Checks if is active.
	 *
	 * @return true, if is active
	 */
	public boolean isActive() {
		return active;
	}

	/**
	 * Sets the active.
	 *
	 * @param active the new active
	 */
	public void setActive(boolean active) {
		this.active = active;
	}

	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Sets the name.
	 *
	 * @param name the new name
	 */
	public void setName(String name) {
		this.name = name;
	}
	
	/**
	 * Sort.
	 */
	public void sort() {
		final LinkedHashMap<String, IAdminTreeChild> collect = this.projectAdminAreaChildren.entrySet().stream().sorted(
				(e1, e2) -> ((AdministrationArea) (((RelationObj) e1.getValue()).getRefObject())).getName()
				.compareTo(((AdministrationArea) (((RelationObj) e2.getValue()).getRefObject())).getName()))
				.collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e1, LinkedHashMap::new));
		this.projectAdminAreaChildren = collect;
	}

}
