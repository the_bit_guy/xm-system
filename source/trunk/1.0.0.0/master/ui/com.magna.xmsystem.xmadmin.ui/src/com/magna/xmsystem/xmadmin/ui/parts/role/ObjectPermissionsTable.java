package com.magna.xmsystem.xmadmin.ui.parts.role;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.jface.layout.TableColumnLayout;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.ColumnViewerEditor;
import org.eclipse.jface.viewers.ColumnViewerEditorActivationEvent;
import org.eclipse.jface.viewers.ColumnViewerEditorActivationStrategy;
import org.eclipse.jface.viewers.ColumnWeightData;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TableViewerColumn;
import org.eclipse.jface.viewers.TableViewerEditor;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;

import com.magna.xmsystem.xmadmin.ui.parts.role.editingsupport.ChangeEditingSupport;
import com.magna.xmsystem.xmadmin.ui.parts.role.editingsupport.CreateEditingSupport;
import com.magna.xmsystem.xmadmin.ui.parts.role.editingsupport.DeleteEditingSupport;
import com.magna.xmsystem.xmadmin.ui.parts.role.editingsupport.ObjPermActDeactEditingSupport;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.roles.VoPermContainer;

/**
 * The Class ObjectPermissionsTable.
 */
public class ObjectPermissionsTable extends TableViewer {
	private boolean enableEditorSupport;
	
	/** The cached obj permission map. */
	private Map<String, VoPermContainer> cachedObjPermissionMap;

	/**
	 * Instantiates a new object permissions table.
	 *
	 * @param parent the parent
	 * @param messages 
	 */
	public ObjectPermissionsTable(Composite parent) {
		super(parent, SWT.FULL_SELECTION | SWT.BORDER);
		this.init();
		this.cachedObjPermissionMap = new HashMap<>();
	}

	/**
	 * Inits the.
	 */
	private void init() {
		Table table = this.getTable();
		table.setHeaderVisible(true);
		table.setLinesVisible(true);
		this.initColumn();
		// Setting content provider
		this.setContentProvider(ArrayContentProvider.getInstance());

		// Setting label provider
		final PermissionsLabelProvider labelProvider = new PermissionsLabelProvider(this);
		// new StyleTableLabelAdapter(tree, labelProvider);
		this.setLabelProvider(labelProvider);

		// editor stuff
		ColumnViewerEditorActivationStrategy actSupport = new ColumnViewerEditorActivationStrategy(this) {
			@Override
			protected boolean isEditorActivationEvent(ColumnViewerEditorActivationEvent ev) {
				return (ev.eventType == ColumnViewerEditorActivationEvent.MOUSE_CLICK_SELECTION
						|| (ev.eventType == ColumnViewerEditorActivationEvent.KEY_PRESSED && ev.keyCode == SWT.CR)
						|| ev.eventType == ColumnViewerEditorActivationEvent.PROGRAMMATIC) && isEnableEditorSupport();
			}
		};
		TableViewerEditor.create(this, null, actSupport,
				ColumnViewerEditor.TABBING_HORIZONTAL | ColumnViewerEditor.TABBING_MOVE_TO_ROW_NEIGHBOR
						| ColumnViewerEditor.TABBING_VERTICAL | ColumnViewerEditor.KEYBOARD_ACTIVATION);
	}
	
	/**
	 * @return the enableEditorSupport
	 */
	public boolean isEnableEditorSupport() {
		return enableEditorSupport;
	}

	/**
	 * @param enableEditorSupport the enableEditorSupport to set
	 */
	public void setEnableEditorSupport(boolean enableEditorSupport) {
		this.enableEditorSupport = enableEditorSupport;
	}

	/**
	 * Inits the column.
	 */
	private void initColumn() {
		TableColumnLayout layout = new TableColumnLayout();
		this.getTable().getParent().setLayout(layout);
		TableViewerColumn viewerCol = createTableViewerColumn(layout);
		viewerCol = createTableViewerColumn(layout);
		viewerCol.setEditingSupport(new CreateEditingSupport(this));
		viewerCol = createTableViewerColumn(layout);
		viewerCol.setEditingSupport(new ChangeEditingSupport(this));
		viewerCol = createTableViewerColumn(layout);
		viewerCol.setEditingSupport(new DeleteEditingSupport(this));
		viewerCol = createTableViewerColumn(layout);
		viewerCol.setEditingSupport(new ObjPermActDeactEditingSupport(this));
	}

	/**
	 * Creates the table viewer column.
	 *
	 * @param layout the layout
	 * @return the table viewer column
	 */
	private TableViewerColumn createTableViewerColumn(TableColumnLayout layout) {
		TableViewerColumn viewerCol = new TableViewerColumn(this, SWT.NONE);
		TableColumn column = viewerCol.getColumn();
		column.setWidth(120);
		layout.setColumnData(column, new ColumnWeightData(20));
		return viewerCol;
	}

	/**
	 * @return the cachedObjPermissionMap
	 */
	public Map<String, VoPermContainer> getCachedObjPermissionMap() {
		return cachedObjPermissionMap;
	}
}
