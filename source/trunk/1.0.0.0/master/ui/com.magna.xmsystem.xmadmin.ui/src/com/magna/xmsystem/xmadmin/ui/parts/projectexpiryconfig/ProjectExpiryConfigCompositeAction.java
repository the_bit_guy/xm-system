package com.magna.xmsystem.xmadmin.ui.parts.projectexpiryconfig;

import java.util.EnumMap;
import java.util.List;

import javax.inject.Inject;

import org.eclipse.core.databinding.DataBindingContext;
import org.eclipse.core.databinding.beans.BeanProperties;
import org.eclipse.core.databinding.observable.value.IObservableValue;
import org.eclipse.e4.core.services.nls.Translation;
import org.eclipse.e4.ui.model.application.ui.MDirtyable;
import org.eclipse.jface.databinding.swt.WidgetProperties;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.client.ResourceAccessException;

import com.magna.xmbackend.entities.PropertyConfigTbl;
import com.magna.xmbackend.vo.enums.ProjectExpiryKey;
import com.magna.xmsystem.dependencies.customExceptions.UnauthorizedAccessException;
import com.magna.xmsystem.ui.controls.widgets.CustomMessageDialog;
import com.magna.xmsystem.xmadmin.message.Message;
import com.magna.xmsystem.xmadmin.message.MessageRegistry;
import com.magna.xmsystem.xmadmin.restclient.propertyConfig.PropertyConfigController;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.AdminTreeviewer;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.projectexpiryconfig.ProjectExpiryConfig;
import com.magna.xmsystem.xmadmin.util.CommonConstants;
import com.magna.xmsystem.xmadmin.util.MessageType;
import com.magna.xmsystem.xmadmin.util.XMAdminUtil;

// TODO: Auto-generated Javadoc
/**
 * The Class ProjectExpiryConfigCompositeAction.
 * 
 * @author archita.patel
 */
public class ProjectExpiryConfigCompositeAction extends ProjectExpiryConfigUI {

	/** The Constant LOGGER. */
	private static final Logger LOGGER = LoggerFactory.getLogger(ProjectExpiryConfigCompositeAction.class);

	/** The widget value. */
	transient private IObservableValue<?> widgetValue;

	/** The model value. */
	transient private IObservableValue<?> modelValue;

	/** The data bind context. */
	final transient DataBindingContext dataBindContext = new DataBindingContext();

	/** The registry. */
	@Inject
	private MessageRegistry registry;

	/** The messages. */
	@Inject
	@Translation
	transient private Message messages;

	/** The roject expiry config model. */
	private ProjectExpiryConfig projectExpiryConfigModel;

	/** The old model. */
	private ProjectExpiryConfig oldModel;

	/** The dirty. */
	transient private MDirtyable dirty;

	/** The singleton key value. */
	private EnumMap<ProjectExpiryKey, String> ProjectExipryKeyValue;

	/**
	 * Instantiates a new singleton app time config comopsite action.
	 *
	 * @param parent
	 *            the parent
	 */
	@Inject
	public ProjectExpiryConfigCompositeAction(final Composite parent) {
		super(parent, SWT.NONE);
		initListener();

	}

	/**
	 * Inits the listener.
	 */
	private void initListener() {

		if (this.btnSave != null) {
			this.btnSave.addSelectionListener(new SelectionAdapter() {

				/**
				 * Save button handler
				 */
				@Override
				public void widgetSelected(final SelectionEvent event) {
					saveProjectExpirySettings();
				}
			});
		}
		if (this.btnCancel != null) {
			this.btnCancel.addSelectionListener(new SelectionAdapter() {

				/**
				 * Save button handler
				 */
				@Override
				public void widgetSelected(final SelectionEvent event) {
					cancelProjectExpiryConfig();
				}
			});
		}
	}

	/**
	 * Cancel project expiry config.
	 */
	public void cancelProjectExpiryConfig() {
		if (this.projectExpiryConfigModel == null) {
			dirty.setDirty(false);
			return;
		}
		setProjectExpiryConfigModel(null);
		setOldModel(null);
		dirty.setDirty(false);
		final AdminTreeviewer adminTree = XMAdminUtil.getInstance().getAdminTree();
		final IStructuredSelection selection = (IStructuredSelection) adminTree.getSelection();
		final Object firstElement = selection.getFirstElement();
		adminTree.setSelection(new StructuredSelection(firstElement), true);
	}

	/**
	 * Save project expiry date.
	 */
	public void saveProjectExpirySettings() {
		try {
			PropertyConfigController propertyConfigController = new PropertyConfigController();
			boolean isUpdate = propertyConfigController.UpdateProjectExpiry(mapVOObjectWithModel());
			if (isUpdate) {
				setOldModel(projectExpiryConfigModel.deepCopyProjectExpiryConfigModel(true, getOldModel()));
				projectExpiryConfigModel.setOperationMode(CommonConstants.OPERATIONMODE.VIEW);
				setOperationMode();
				this.dirty.setDirty(false);
				XMAdminUtil.getInstance().getAdminTree().refresh();
				XMAdminUtil.getInstance().getAdminTree().setSelection(new StructuredSelection(getOldModel()), true);
				XMAdminUtil.getInstance().updateLogFile(messages.projectExpiryNode + " " + messages.objectUpdate,
						MessageType.SUCCESS);
			}
		} catch (UnauthorizedAccessException e) {
			CustomMessageDialog.openError(this.getShell(), messages.objectPermissionDialogTitle,
					messages.objectPermissionDialogMsg);
		} catch (Exception e) {
			if (e instanceof ResourceAccessException) {
				CustomMessageDialog.openError(Display.getCurrent().getActiveShell(), messages.errorDialogTitile,
						messages.serverNotReachable);
			}
			LOGGER.error("Exception occured while saving ldap configuration", e);
		}
	}

	/**
	 * Map VO object with model.
	 *
	 * @return the com.magna.xmbackend.vo.prop config. project expiry config
	 *         request
	 */
	private com.magna.xmbackend.vo.propConfig.ProjectExpiryConfigRequest mapVOObjectWithModel() {
		com.magna.xmbackend.vo.propConfig.ProjectExpiryConfigRequest projectexpiryConfigRequest = new com.magna.xmbackend.vo.propConfig.ProjectExpiryConfigRequest();
		this.ProjectExipryKeyValue = new EnumMap<ProjectExpiryKey, String>(ProjectExpiryKey.class);
		ProjectExpiryKey projectExpiry = ProjectExpiryKey.PROJECT_EXPIRY_DAYS;
		this.ProjectExipryKeyValue.put(projectExpiry, this.projectExpiryConfigModel.getProjectExpiryDays());
		this.ProjectExipryKeyValue.put(ProjectExpiryKey.PROJECT_EXPIRY_GRACE_PERIOD, this.projectExpiryConfigModel.getProjectGraceDays());
		projectexpiryConfigRequest.setProjectExpiryKeyValue(ProjectExipryKeyValue);
		return projectexpiryConfigRequest;

	}

	/**
	 * Register messages.
	 *
	 * @param registry
	 *            the registry
	 */
	public void registerMessages(final MessageRegistry registry) {
		registry.register((text) -> {
			if (grpProjectExpiryConfig != null && !grpProjectExpiryConfig.isDisposed()) {
				grpProjectExpiryConfig.setText(text);
			}
		}, (message) -> {
			return message.projectExpiryGroupLabel;
		});

		registry.register((text) -> {
			if (lblProjectExpiryDays != null && !lblProjectExpiryDays.isDisposed()) {
				lblProjectExpiryDays.setText(text);
			}
		}, (message) -> {
			if (lblProjectExpiryDays != null && !lblProjectExpiryDays.isDisposed()) {
				return getUpdatedWidgetText(message.projectExpiryDayspinnerLabel, lblProjectExpiryDays);
			}
			return CommonConstants.EMPTY_STR;
		});

		registry.register((text) -> {
			if (this.lbldays != null && !this.lbldays.isDisposed()) {
				lbldays.setText(text);
			}
		}, (message) -> {
			if (this.lbldays != null && !this.lbldays.isDisposed()) {
				return getUpdatedWidgetText(message.projectExpiryDaysLabel, this.lbldays);
			}
			return CommonConstants.EMPTY_STR;
		});
		
		registry.register((text) -> {
			if (this.lblProjectGraceDays != null && !this.lblProjectGraceDays.isDisposed()) {
				lblProjectGraceDays.setText(text);
			}
		}, (message) -> {
			if (this.lblProjectGraceDays != null && !this.lblProjectGraceDays.isDisposed()) {
				return getUpdatedWidgetText(message.projectGraceDaySppinnerLabel, this.lblProjectGraceDays);
			}
			return CommonConstants.EMPTY_STR;
		});
		
		registry.register((text) -> {
			if (this.lblGracedays != null && !this.lblGracedays.isDisposed()) {
				lblGracedays.setText(text);
			}
		}, (message) -> {
			if (this.lblGracedays != null && !this.lblGracedays.isDisposed()) {
				return getUpdatedWidgetText(message.projectExpiryDaysLabel, this.lblGracedays);
			}
			return CommonConstants.EMPTY_STR;
		});

		registry.register((text) -> {
			if (btnSave != null && !btnSave.isDisposed()) {
				btnSave.setText(text);
			}
		}, (message) -> {
			if (btnSave != null && !btnSave.isDisposed()) {
				return getUpdatedWidgetText(message.saveButtonText, btnSave);
			}
			return CommonConstants.EMPTY_STR;
		});
		registry.register((text) -> {
			if (btnCancel != null && !btnCancel.isDisposed()) {
				btnCancel.setText(text);
			}
		}, (message) -> {
			if (btnCancel != null && !btnCancel.isDisposed()) {
				return getUpdatedWidgetText(message.cancelButtonText, btnCancel);
			}
			return CommonConstants.EMPTY_STR;
		});
	}

	/**
	 * Bind values.
	 */
	@SuppressWarnings({ "unchecked" })
	public void bindValues() {
		try {
			widgetValue = WidgetProperties.selection().observe(this.projectExpirydaySpinner);
			modelValue = BeanProperties
					.value(ProjectExpiryConfig.class, ProjectExpiryConfig.PROPERTY_PROJECT_EXPIRY_DAYS)
					.observe(this.projectExpiryConfigModel);
			dataBindContext.bindValue(widgetValue, modelValue);
			
			widgetValue = WidgetProperties.selection().observe(this.projectGraceDaySpinner);
			modelValue = BeanProperties
					.value(ProjectExpiryConfig.class, ProjectExpiryConfig.PROPERTY_PROJECT_GRACE_DAYS)
					.observe(this.projectExpiryConfigModel);
			dataBindContext.bindValue(widgetValue, modelValue);
		} catch (Exception e) {
			LOGGER.error("Exception occured while databinding", e);
		}

	}

	/**
	 * Sets the operation mode.
	 */
	public void setOperationMode() {
		if (this.projectExpiryConfigModel != null) {
			if (projectExpiryConfigModel.getOperationMode() == CommonConstants.OPERATIONMODE.VIEW) {
				this.projectExpirydaySpinner.setEnabled(false);
				this.projectGraceDaySpinner.setEnabled(false);
				setShowButtonBar(false);
			} else if (projectExpiryConfigModel.getOperationMode() == CommonConstants.OPERATIONMODE.CHANGE) {
				this.projectExpirydaySpinner.setEnabled(true);
				this.projectGraceDaySpinner.setEnabled(true);
				setShowButtonBar(true);
				this.dirty.setDirty(true);
			} else {
				this.projectExpirydaySpinner.setEnabled(false);
				this.projectGraceDaySpinner.setEnabled(false);
				setShowButtonBar(false);
			}
		}
	}

	/**
	 * Sets the show button bar.
	 *
	 * @param showButtonBar
	 *            the new show button bar
	 */
	protected void setShowButtonBar(final boolean showButtonBar) {
		if (this.btnSave != null && !this.btnSave.isDisposed() && this.btnCancel != null
				&& !this.btnCancel.isDisposed()) {
			final GridData layoutData = (GridData) this.btnSave.getParent().getLayoutData();
			layoutData.exclude = !showButtonBar;
			this.btnSave.setVisible(showButtonBar);
			this.btnCancel.setVisible(showButtonBar);
			this.btnSave.getParent().requestLayout();
			this.btnSave.getParent().redraw();
			this.btnSave.getParent().getParent().update();
		}
	}

	/**
	 * Gets the updated widget text.
	 *
	 * @param message
	 *            the message
	 * @param control
	 *            the control
	 * @return the updated widget text
	 */
	private String getUpdatedWidgetText(final String message, final Control control) {
		control.requestLayout();
		control.getParent().redraw();
		control.getParent().getParent().update();
		control.getParent().getParent().getParent().update();
		return message;
	}

	/**
	 * Sets the project expiry config.
	 */
	public void setProjectExpiryConfig() {
		final AdminTreeviewer adminTree = XMAdminUtil.getInstance().getAdminTree();
		final Object selectionObj = adminTree.getSelection();
		if (selectionObj instanceof IStructuredSelection) {
			Object firstElement = ((IStructuredSelection) selectionObj).getFirstElement();
			if (firstElement instanceof ProjectExpiryConfig) {
				projectExpiryConfigModel = (ProjectExpiryConfig) firstElement;
				PropertyConfigController propertyConfigController = new PropertyConfigController();
				List<PropertyConfigTbl> propVOList = propertyConfigController.getAllProjectExpiry(true);
				for (PropertyConfigTbl propertyTbl : propVOList) {
					String property = propertyTbl.getProperty();
					String propertyValue = propertyTbl.getValue();
					if (ProjectExpiryKey.PROJECT_EXPIRY_DAYS.toString().equals(property)) {
						this.projectExpiryConfigModel.setProjectExpiryDays(propertyValue);
					}else if(ProjectExpiryKey.PROJECT_EXPIRY_GRACE_PERIOD.toString().equals(property)){
						this.projectExpiryConfigModel.setProjectGraceDays(propertyValue);
					}
				}
				setOldModel(projectExpiryConfigModel);
				setProjectExpiryConfigModel(this.getOldModel().deepCopyProjectExpiryConfigModel(false, null));
				registerMessages(this.registry);
				bindValues();
				setOperationMode();
			}
		}
	}

	/**
	 * Sets the dirty object.
	 *
	 * @param dirty
	 *            the new dirty object
	 */
	public void setDirtyObject(final MDirtyable dirty) {
		this.dirty = dirty;
	}

	/**
	 * Gets the project expiry config model.
	 *
	 * @return the project expiry config model
	 */
	public ProjectExpiryConfig getProjectExpiryConfigModel() {
		return projectExpiryConfigModel;
	}

	/**
	 * Sets the project expiry config model.
	 *
	 * @param projectExpiryConfig
	 *            the new project expiry config model
	 */
	public void setProjectExpiryConfigModel(final ProjectExpiryConfig projectExpiryConfig) {
		this.projectExpiryConfigModel = projectExpiryConfig;
	}

	/**
	 * Gets the old model.
	 *
	 * @return the old model
	 */
	public ProjectExpiryConfig getOldModel() {
		return oldModel;
	}

	/**
	 * Sets the old model.
	 *
	 * @param oldModel
	 *            the new old model
	 */
	public void setOldModel(ProjectExpiryConfig oldModel) {
		this.oldModel = oldModel;
	}

}
