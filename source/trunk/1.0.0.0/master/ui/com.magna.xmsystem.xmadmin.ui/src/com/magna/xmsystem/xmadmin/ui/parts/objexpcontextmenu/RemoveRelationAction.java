package com.magna.xmsystem.xmadmin.ui.parts.objexpcontextmenu;

import javax.inject.Inject;

import org.eclipse.core.commands.ParameterizedCommand;
import org.eclipse.e4.core.commands.ECommandService;
import org.eclipse.e4.core.commands.EHandlerService;
import org.eclipse.jface.action.Action;

import com.magna.xmsystem.xmadmin.util.CommonConstants;

/**
 * The Class RemoveRelationAction.
 */
@SuppressWarnings("restriction")
public class RemoveRelationAction extends Action {

	/** The handler service. */
	@Inject
	private EHandlerService handlerService;

	/** The command service. */
	@Inject
	private ECommandService commandService;
	


	/**
	 * Execute the command handler
	 */
	@Override
	public void run() {
		final ParameterizedCommand cmd = commandService.createCommand(CommonConstants.COMMAND_ID.REMOVE_RELATION, null);
		if (handlerService.canExecute(cmd)) {
			handlerService.executeHandler(cmd);
		}
	}
}
