package com.magna.xmsystem.xmadmin.ui.parts.role.editingsupport;

import com.magna.xmbackend.vo.enums.CreationType;
import com.magna.xmbackend.vo.roles.ObjectPermission;
import com.magna.xmsystem.xmadmin.ui.parts.role.ObjRelationPermissionsTable;
import com.magna.xmsystem.xmadmin.ui.parts.role.ObjectPermissionsTable;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.roles.ObjectRelationType;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.roles.VoPermContainer;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.roles.objRelationPermissions.ObjectRelationPermissions;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.roles.objectpermissions.ObjectPermissions;
import com.magna.xmsystem.xmadmin.util.XMAdminUtil;

/**
 * The Class ActivateDeactivateEditingSupport.
 * 
 * @author shashwat.anand
 */
public class RelPermActDeactEditingSupport extends CheckBoxEditingSupport {
	/** Member variable 'instance' for {@link XMAdminUtil}. */
	private XMAdminUtil instance;
	
	/** Member variable 'viewer' for {@link ObjectPermissionsTable}. */
	private ObjRelationPermissionsTable viewer;

	/**
	 * Instantiates a new activate deactivate editing support.
	 *
	 * @param viewer the viewer
	 */
	public RelPermActDeactEditingSupport(final ObjRelationPermissionsTable viewer) {
		super(viewer);
		this.viewer = viewer;
		this.instance = XMAdminUtil.getInstance();
	}

	/* (non-Javadoc)
	 * @see org.eclipse.jface.viewers.EditingSupport#getValue(java.lang.Object)
	 */
	@Override
	protected Object getValue(Object element) {
		if (element instanceof ObjectPermissions) {
			return ((ObjectPermissions) element).isActivate();
		} else if (element instanceof ObjectRelationPermissions) {
			return ((ObjectRelationPermissions) element).isActivate();
		}
		return "N";
	}

	/* (non-Javadoc)
	 * @see org.eclipse.jface.viewers.EditingSupport#setValue(java.lang.Object, java.lang.Object)
	 */
	@Override
	protected void setValue(Object element, Object value) {
		if (element instanceof ObjectRelationPermissions) {
			ObjectRelationPermissions elem = (ObjectRelationPermissions) element;
			ObjectRelationType objectRelationType = ObjectRelationType.getObjectType(elem.getPermissionName());
			String permissionId = this.instance.getObjectRelPermissionMap().get(objectRelationType).getActivatePermission().getPermissionId();
			VoPermContainer voPermContainer = new VoPermContainer();
			ObjectPermission objectPermission = new ObjectPermission();
			objectPermission.setPermissionId(permissionId);
			voPermContainer.setObjectPermission(objectPermission);
			if (elem.isActivate() && ! (boolean) value) {
				objectPermission.setCreationType(CreationType.DELETE);
				elem.setActivatePermission(voPermContainer);
			} else if (!elem.isActivate() && (boolean) value) {
				objectPermission.setCreationType(CreationType.ADD);
				elem.setActivatePermission(voPermContainer);
			} else {
				return;
			}
			elem.setActivate((boolean) value);
			this.viewer.getCachedObjPermissionMap().put(permissionId, voPermContainer);
			getViewer().refresh(element);
		}
	}
	
	/* (non-Javadoc)
	 * @see com.magna.xmsystem.xmadmin.ui.parts.role.editingsupport.CheckBoxEditingSupport#canEdit(java.lang.Object)
	 */
	@Override
	protected boolean canEdit(final Object element) {
		if (element instanceof ObjectRelationPermissions) {
			String permissionName = ((ObjectRelationPermissions) element).getPermissionName();
			ObjectRelationType objectRelationType = ObjectRelationType.getObjectType(permissionName);
			if (objectRelationType == ObjectRelationType.DIRECTORY_RELATION || objectRelationType == ObjectRelationType.ADMIN_MENU_CONFIG
					|| objectRelationType == ObjectRelationType.ADMINISTRATION_AREA_TO_ROLE || objectRelationType == ObjectRelationType.GROUP_RELATION
					|| objectRelationType == ObjectRelationType.USER_APPLICATION_TO_USER || objectRelationType == ObjectRelationType.USER_TO_ROLE
					|| objectRelationType == ObjectRelationType.PROJECT_APPLICATION_TO_USER_PROJECT) {
				return false;
			}
		}
		return true;
	}
}
