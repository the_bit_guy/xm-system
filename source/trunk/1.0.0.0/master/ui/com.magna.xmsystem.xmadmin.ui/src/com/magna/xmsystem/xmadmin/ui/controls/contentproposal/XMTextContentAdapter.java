package com.magna.xmsystem.xmadmin.ui.controls.contentproposal;

import org.eclipse.jface.fieldassist.IControlContentAdapter;
import org.eclipse.jface.fieldassist.IControlContentAdapter2;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Text;

/**
 * Class for XM text content adapter.
 *
 * @author Chiranjeevi.Akula
 */
public class XMTextContentAdapter implements IControlContentAdapter, IControlContentAdapter2 {

	/**
	 * Constructor for XMTextContentAdapter Class.
	 */
	public XMTextContentAdapter() {
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.jface.fieldassist.IControlContentAdapter#getControlContents(
	 * org.eclipse.swt.widgets.Control)
	 */
	public String getControlContents(Control control) {
		return ((Text) control).getText();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.jface.fieldassist.IControlContentAdapter#setControlContents(
	 * org.eclipse.swt.widgets.Control, java.lang.String, int)
	 */
	public void setControlContents(Control control, String text, int cursorPosition) {
		((Text) control).setText(text);
		((Text) control).setSelection(cursorPosition, cursorPosition);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jface.fieldassist.IControlContentAdapter#
	 * insertControlContents(org.eclipse.swt.widgets.Control, java.lang.String,
	 * int)
	 */
	public void insertControlContents(Control control, String text, int cursorPosition) {
		Point selection = ((Text) control).getSelection();
		((Text) control).setText(text); // Hot Fix
		if (cursorPosition < text.length())
			((Text) control).setSelection(selection.x + cursorPosition, selection.x + cursorPosition);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.jface.fieldassist.IControlContentAdapter#getCursorPosition(
	 * org.eclipse.swt.widgets.Control)
	 */
	public int getCursorPosition(Control control) {
		return ((Text) control).getCaretPosition();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.jface.fieldassist.IControlContentAdapter#getInsertionBounds(
	 * org.eclipse.swt.widgets.Control)
	 */
	public Rectangle getInsertionBounds(Control control) {
		Text text = (Text) control;
		Point caretOrigin = text.getCaretLocation();
		return new Rectangle(caretOrigin.x + text.getClientArea().x, caretOrigin.y + text.getClientArea().y + 3, 1,
				text.getLineHeight());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.jface.fieldassist.IControlContentAdapter#setCursorPosition(
	 * org.eclipse.swt.widgets.Control, int)
	 */
	public void setCursorPosition(Control control, int position) {
		((Text) control).setSelection(new Point(position, position));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.jface.fieldassist.IControlContentAdapter2#getSelection(org.
	 * eclipse.swt.widgets.Control)
	 */
	public Point getSelection(Control control) {
		return ((Text) control).getSelection();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.jface.fieldassist.IControlContentAdapter2#setSelection(org.
	 * eclipse.swt.widgets.Control, org.eclipse.swt.graphics.Point)
	 */
	public void setSelection(Control control, Point range) {
		((Text) control).setSelection(range);
	}
}