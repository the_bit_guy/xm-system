 
package com.magna.xmsystem.xmadmin.ui.handlers.procreateevt;

import javax.inject.Inject;

import org.eclipse.e4.core.di.annotations.CanExecute;
import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.core.services.nls.Translation;
import org.eclipse.e4.ui.model.application.MApplication;
import org.eclipse.e4.ui.model.application.ui.basic.MPart;
import org.eclipse.e4.ui.workbench.modeling.EModelService;
import org.eclipse.swt.custom.StackLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.client.ResourceAccessException;

import com.magna.xmsystem.dependencies.customExceptions.UnauthorizedAccessException;
import com.magna.xmsystem.ui.controls.widgets.CustomMessageDialog;
import com.magna.xmsystem.xmadmin.message.Message;
import com.magna.xmsystem.xmadmin.ui.parts.IEditablePart;
import com.magna.xmsystem.xmadmin.ui.parts.InformationPart;
import com.magna.xmsystem.xmadmin.ui.parts.projectcreateevt.ProjectCreateEvtCompositeAction;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.AdminTreeviewer;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.notification.ProjectCreateEvtAction;
import com.magna.xmsystem.xmadmin.util.CommonConstants;
import com.magna.xmsystem.xmadmin.util.XMAdminUtil;

// TODO: Auto-generated Javadoc
/**
 * The Class CreateProjectCreateAction.
 */
public class CreateProjectCreateEvtAction {
	
	/** The Constant LOGGER. */
	private static final Logger LOGGER = LoggerFactory.getLogger(CreateProjectCreateEvtAction.class);
	
	/** Inject of {@link EModelService}. */
	@Inject
	private EModelService modelService;

	/** Inject of {@link MApplication}. */
	@Inject
	private MApplication application;

	/** Member variable for messages. */
	@Inject
	@Translation
	transient private Message messages;
	
	/**
	 * Execute.
	 */
	@Execute
	public void execute() {
		try{
			//if (XMAdminUtil.getInstance().isAcessAllowed("SITE-CREATE")) {
			MPart part = (MPart) this.modelService.find(CommonConstants.PART_ID.INFORMATION_PART_ID, application);
			InformationPart rightSidePart = (InformationPart) part.getObject();
			Composite rightcomposite = rightSidePart.getComposite();
			StackLayout rightCompLayout = rightSidePart.getCompLayout();
			ProjectCreateEvtCompositeAction proCreateEvtCompositeAction = rightSidePart
					.getProjectCreateEvtCompositeUI();
			rightCompLayout.topControl = proCreateEvtCompositeAction;
			final AdminTreeviewer adminTree = XMAdminUtil.getInstance().getAdminTree();
			ProjectCreateEvtAction projectCreateEvtAction = new ProjectCreateEvtAction(null, CommonConstants.EMPTY_STR,
					false, CommonConstants.OPERATIONMODE.CREATE);

			proCreateEvtCompositeAction.setModel(projectCreateEvtAction);
			adminTree.setSelection(null);
			adminTree.setPreviousSelection(null);
			rightcomposite.layout();
			/*} else {
				CustomMessageDialog.openError(Display.getCurrent().getActiveShell(),
						messages.objectPermissionDialogTitle, messages.objectPermissionDialogMsg);
			}*/
		} catch (UnauthorizedAccessException e) {
			LOGGER.error("Current user is Unauthorized " + e);
		} catch (Exception e) {
			if (e instanceof ResourceAccessException) {
				CustomMessageDialog.openError(Display.getCurrent().getActiveShell(), messages.errorDialogTitile,
						messages.serverNotReachable);
			}
			LOGGER.error("Exception occured in calling access allowed API " + e);
		}
	}

	/**
	 * Can execute.
	 *
	 * @return true, if successful
	 */
	@CanExecute
	public boolean canExecute() {
		boolean returnVal = true;
		final XMAdminUtil instance = XMAdminUtil.getInstance();
		MPart rightPart;
		Object rightView;
		if ((rightPart = instance.getInformationPart()) != null
				&& (rightView = rightPart.getObject()) != null && rightView instanceof IEditablePart
				&& ((IEditablePart) rightView).isDirty()) {
			returnVal = false;
		}
		return returnVal;
	}
		
}