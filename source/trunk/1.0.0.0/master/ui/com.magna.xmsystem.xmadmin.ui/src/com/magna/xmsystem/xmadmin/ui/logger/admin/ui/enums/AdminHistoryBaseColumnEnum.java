package com.magna.xmsystem.xmadmin.ui.logger.admin.ui.enums;

/**
 * The Enum AdminHistoryBaseColumnEnum.
 */
public enum AdminHistoryBaseColumnEnum {

	ID, LOG_TIME, ADMIN_NAME, API_REQUEST_PATH, OPERATION, OBJECT_NAME, CHANGES, ERROR_MESSAGE, RESULT
}
