package com.magna.xmsystem.xmadmin.ui.logger.user.ui;

/**
 * The Enum UserHistoryColumnEnum.
 */
public enum UserHistoryColumnEnum {

	ID, LOG_TIME, USER_NAME, HOST, SITE, ADMIN_AREA, PROJECT, APPLICATION, PID, RESULT, ARGS, IS_USER_STATUS;
}
