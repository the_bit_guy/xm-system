package com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.startapplicationmodel;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.stream.Collectors;

import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.refs.RelationObj;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.users.User;

/**
 * Class for Start app users.
 *
 * @author Chiranjeevi.Akula
 */
public class StartAppUsers implements IAdminTreeChild {

	/** Member variable 'parent' for {@link IAdminTreeChild}. */
	private IAdminTreeChild parent;

	/** Member variable 'start app users children' for {@link Map<String,IAdminTreeChild>}. */
	private Map<String, IAdminTreeChild> startAppUsersChildren;

	/**
	 * Constructor for StartAppUsers Class.
	 *
	 * @param parent {@link IAdminTreeChild}
	 */
	public StartAppUsers(final IAdminTreeChild parent) {
		this.parent = parent;
		this.startAppUsersChildren = new LinkedHashMap<>();
	}

	/**
	 * Method for Adds the.
	 *
	 * @param startAppUserId {@link String}
	 * @param child {@link IAdminTreeChild}
	 * @return the i admin tree child {@link IAdminTreeChild}
	 */
	public IAdminTreeChild add(final String startAppUserId, final IAdminTreeChild child) {
		child.setParent(this);
		IAdminTreeChild returnVal = this.startAppUsersChildren.put(startAppUserId, child);
		if (child instanceof RelationObj && ((RelationObj) child).getRefObject() instanceof User) {
			sort();
		}
		return returnVal;
	}

	/**
	 * Method for Removes the.
	 *
	 * @param startAppUserId {@link String}
	 * @return the i admin tree child {@link IAdminTreeChild}
	 */
	public IAdminTreeChild remove(final String startAppUserId) {
		return this.startAppUsersChildren.remove(startAppUserId);
	}

	/**
	 * Method for Removes the all.
	 */
	public void removeAll() {
		this.startAppUsersChildren.clear();
	}
	
	/**
	 * Gets the start app users children collection.
	 *
	 * @return the start app users children collection
	 */
	public Collection<IAdminTreeChild> getStartAppUsersChildrenCollection() {
		return this.startAppUsersChildren.values();
	}

	/**
	 * Gets the start app users children.
	 *
	 * @return the start app users children
	 */
	public Map<String, IAdminTreeChild> getStartAppUsersChildren() {
		return startAppUsersChildren;
	}
	
	/* (non-Javadoc)
	 * @see com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild#getParent()
	 */
	@Override
	public IAdminTreeChild getParent() {
		return this.parent;
	}

	/* (non-Javadoc)
	 * @see com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild#setParent(com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild)
	 */
	@Override
	public void setParent(final IAdminTreeChild parent) {
		this.parent = parent;
	}
	
	/**
	 * Sort.
	 */
	public void sort() {
		final LinkedHashMap<String, IAdminTreeChild> collect = this.startAppUsersChildren.entrySet().stream().sorted(
				(e1, e2) -> ((User) e1.getValue()).getName().compareTo(((User) e2.getValue()).getName()))
				.collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e1, LinkedHashMap::new));
		this.startAppUsersChildren = collect;
	}

}
