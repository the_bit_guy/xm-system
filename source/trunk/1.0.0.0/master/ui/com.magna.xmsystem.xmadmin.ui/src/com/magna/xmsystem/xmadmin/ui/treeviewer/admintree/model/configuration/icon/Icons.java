package com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.icon;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild;

/**
 * Parent model element to store all icons
 * 
 * @author shashwat.anand
 *
 */
public class Icons implements IAdminTreeChild {
	/** The parent. */
	private IAdminTreeChild parent;
	/**
	 * List for storing {@link IAdminTreeChild}
	 */
	private transient final Map<String, IAdminTreeChild> iconNodes;

	/**
	 * Constructor
	 */
	public Icons() {
		this.iconNodes = new LinkedHashMap<>();
	}

	/**
	 * Adds the child
	 * @param icon {@link Icon}
	 */
	public void add(final Icon icon) {
		icon.setParent(this);
		this.iconNodes.put(icon.getIconName(), icon);
	}

	/**
	 * Removes the child
	 * @param icon {@link Icon}
	 */
	public void remove(final Icon icon) {
		this.iconNodes.remove(icon.getIconName());
	}

	/**
	 * Finds if icon is already present
	 * @param iconName {@link String}
	 * @return boolean
	 */
	public boolean containsKey(final String iconName) {
		return this.iconNodes.containsKey(iconName);
	}

	/**
	 * @return {@link List} of {@link IAdminTreeChild} 
	 */
	public Collection<IAdminTreeChild> getAllIcons() {
		return this.iconNodes.values();
	}
	
	/**
	 * Returns null as its parent
	 * @see com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild#getParent()
	 */
	@Override
	public IAdminTreeChild getParent() {
		return this.parent;
	}

	/**
	 * @see com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild#setParent(com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild)
	 */
	@Override
	public void setParent(final IAdminTreeChild parent) {
		this.parent = parent;
	}
}
