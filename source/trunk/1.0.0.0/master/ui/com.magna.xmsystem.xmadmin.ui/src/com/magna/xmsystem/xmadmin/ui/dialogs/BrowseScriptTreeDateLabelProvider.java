package com.magna.xmsystem.xmadmin.ui.dialogs;

import java.io.File;
import java.text.SimpleDateFormat;

import org.eclipse.jface.viewers.ColumnLabelProvider;
import org.eclipse.jface.viewers.DelegatingStyledCellLabelProvider.IStyledLabelProvider;
import org.eclipse.jface.viewers.StyledString;

// TODO: Auto-generated Javadoc
/**
 * The Class BrowseScriptTreeDateLabelProvider.
 * 
 * @author Archita.patel
 */
public class BrowseScriptTreeDateLabelProvider extends ColumnLabelProvider implements IStyledLabelProvider {

	/** The date formate. */
	SimpleDateFormat dateFormate = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jface.viewers.DelegatingStyledCellLabelProvider.
	 * IStyledLabelProvider#getStyledText(java.lang.Object)
	 */
	@Override
	public StyledString getStyledText(Object element) {
		if (element instanceof File) {
			File file = (File) element;
			return new StyledString(dateFormate.format(file.lastModified()));
		}
		return null;
	}
}
