package com.magna.xmsystem.xmadmin.ui.template.dialog;

import org.eclipse.jface.viewers.ColumnLabelProvider;

import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.notification.NotificationTemplate;

// TODO: Auto-generated Javadoc
/**
 * The Class BrowseTemplateViewerLabelProvider.
 */
public class BrowseTemplateViewerLabelProvider extends ColumnLabelProvider {

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.jface.viewers.ColumnLabelProvider#getText(java.lang.Object)
	 */
	@Override
	public String getText(Object element) {
		if (element instanceof NotificationTemplate) {
			return ((NotificationTemplate) element).getName();
		}
		return element == null ? "" : element.toString();//$NON-NLS-1$
	}

}
