package com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.notification;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.stream.Collectors;

import com.magna.xmbackend.vo.enums.NotificationEventType;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild;

/**
 * The Class ProjectDeleteEvt.
 * 
 * @author Shashwat.Anand
 */
public class ProjectDeleteEvt extends INotificationEvent {
	
	/** The project delete evt child. */
	private Map<String, IAdminTreeChild> projectDeleteEvtChild;
	
	/** The parent. */
	private IAdminTreeChild parent;
	
	/**
	 * Instantiates a new project delete evt.
	 */
	public ProjectDeleteEvt() {
		this.parent = null;
		this.eventType = NotificationEventType.PROJECT_DELETE;
		this.projectDeleteEvtChild = new LinkedHashMap<>();
	}
	
	/**
	 * Add.
	 *
	 * @param id the id
	 * @param child the child
	 * @return the i admin tree child
	 */
	public IAdminTreeChild add(final String id, final IAdminTreeChild child) {
		child.setParent(this);
		IAdminTreeChild returnVal = this.projectDeleteEvtChild.put(id, child);
		sort();
		return returnVal;
	}
	
	/**
	 * Remove all.
	 */
	public void removeAll() {
		this.projectDeleteEvtChild.clear();
	}

	/* (non-Javadoc)
	 * @see com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild#getParent()
	 */
	@Override
	public IAdminTreeChild getParent() {
		return this.parent;
	}

	/* (non-Javadoc)
	 * @see com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild#setParent(com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild)
	 */
	@Override
	public void setParent(IAdminTreeChild parent) {
		this.parent = parent;
	}
	
	/**
	 * Gets the project delete evt child.
	 *
	 * @return the project delete evt child
	 */
	public Map<String, IAdminTreeChild> getProjectDeleteEvtChild() {
		return this.projectDeleteEvtChild;
	}
	
	/**
	 * Gets the project delete evt collection.
	 *
	 * @return the project delete evt collection
	 */
	public Collection<IAdminTreeChild> getProjectDeleteEvtCollection() {
		return this.projectDeleteEvtChild.values();
	}
	
	/**
	 * Sort.
	 */
	public void sort() {
		final LinkedHashMap<String, IAdminTreeChild> collect = this.projectDeleteEvtChild.entrySet().stream().sorted(
				(e1, e2) -> ((ProjectDeleteEvtAction) e1.getValue()).getName().compareTo(((ProjectDeleteEvtAction) e2.getValue()).getName()))
				.collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e1, LinkedHashMap::new));
		this.projectDeleteEvtChild = collect;
	}
}
