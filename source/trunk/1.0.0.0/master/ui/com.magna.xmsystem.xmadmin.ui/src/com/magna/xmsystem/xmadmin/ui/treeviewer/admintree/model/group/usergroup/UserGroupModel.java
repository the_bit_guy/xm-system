package com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.group.usergroup;

import java.beans.PropertyChangeEvent;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import com.magna.xmbackend.vo.language.Language;
import com.magna.xmsystem.dependencies.utils.LANG_ENUM;
import com.magna.xmsystem.dependencies.utils.XMSystemUtil;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.BeanModel;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.icon.Icon;
import com.magna.xmsystem.xmadmin.util.CommonConstants;

public class UserGroupModel extends BeanModel implements IAdminTreeChild, Cloneable {
	/**
	 * Constant variable for remark text limit
	 */
	public static final int REMARK_LIMIT = 1500;

	/**
	 * Constant variable for desc text limit
	 */
	public static final int DESC_LIMIT = 240;

	/**
	 * Constant variable for name text limit
	 */
	public static final int NAME_LIMIT = 30;
	/**
	 * PROPERTY_GROUPID constant
	 */
	public static final String PROPERTY_GROUPID = "groupId"; //$NON-NLS-1$
	/**
	 * PROPERTY_GROUPNAME constant
	 */
	public static final String PROPERTY_GROUPNAME = "name"; //$NON-NLS-1$

	/**
	 * PROPERTY_DESC_MAP constant
	 */
	public static final String PROPERTY_DESC_MAP = "descriptionMap"; //$NON-NLS-1$

	/**
	 * PROPERTY_NOTES_MAP constant
	 */
	public static final String PROPERTY_NOTES_MAP = "remarksMap"; //$NON-NLS-1$

	/**
	 * PROPERTY_OPERATION_MODE constant
	 */
	public static final String PROPERTY_OPERATION_MODE = "operationMode"; //$NON-NLS-1$

	/**
	 * PROPERTY_ICON constant
	 */
	public static final String PROPERTY_ICON = "icon"; //$NON-NLS-1$

	/** Member variable for group id. */
	private String groupId;

	/** Member variable for group name. */
	private String name;

	/** Member variable for group description. */
	private Map<LANG_ENUM, String> descriptionMap;

	/** Member variable for group notes. */
	private Map<LANG_ENUM, String> remarksMap;

	/** Member variable for mode of operation. */
	private int operationMode;

	/** Member variable for icon. */
	private Icon icon;
	
	/** The project children. */
	private Map<String, IAdminTreeChild> userGroupChildren;

	/**
	 * Constructor.
	 *
	 * @param siteId
	 *            {@link String}
	 * @param name
	 *            {@link String}
	 * @param isActive
	 *            {@link boolean}
	 * @param icon
	 *            {@link Icon}
	 * @param operationMode
	 *            {@link int}
	 */
	public UserGroupModel(final String groupId, final String name, final Icon icon,
			final int operationMode) {
		this(groupId, name, new HashMap<>(), new HashMap<>(), icon, operationMode);
	}

	/**
	 * Constructor.
	 *
	 * @param siteId
	 *            {@link String}
	 * @param name
	 *            {@link String}
	 * @param isActive
	 *            boolean
	 * @param descriptionMap
	 *            {@link Map}
	 * @param remarksMap
	 *            {@link Map}
	 * @param icon
	 *            {@link Icon}
	 * @param operationMode
	 */
	public UserGroupModel(final String groupId, final String name,
			final Map<LANG_ENUM, String> descriptionMap, final Map<LANG_ENUM, String> remarksMap, final Icon icon,
			final int operationMode) {
		super();
		this.groupId = groupId;
		this.name = name;
		this.descriptionMap = descriptionMap;
		this.remarksMap = remarksMap;
		this.icon = icon;
		this.operationMode = operationMode;
		this.userGroupChildren = new LinkedHashMap<>();
		
		addFixedChildren();
	}
	
	/**
	 * Method for Adds the fixed children.
	 */
	public void addFixedChildren() {
		this.userGroupChildren.put(UserGroupUsers.class.getSimpleName(), new UserGroupUsers(this));
	}

	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Sets the name.
	 *
	 * @param name
	 *            the name to set
	 */
	public void setName(final String name) {
		if (name == null)
			throw new IllegalArgumentException();
		this.propertyChangeSupport.firePropertyChange(PROPERTY_GROUPNAME, this.name, this.name = name.trim());
	}


	/**
	 * Gets the user group children.
	 *
	 * @return the user group children
	 */
	public Map<String, IAdminTreeChild> getUserGroupChildren() {
		return userGroupChildren;
	}

	/**
	 * Sets the user group children.
	 *
	 * @param userGroupChildren the user group children
	 */
	public void setUserGroupChildren(Map<String, IAdminTreeChild> userGroupChildren) {
		this.userGroupChildren = userGroupChildren;
	}

	/**
	 * Gets the description map.
	 *
	 * @return the descriptionMap
	 */
	public Map<LANG_ENUM, String> getDescriptionMap() {
		return descriptionMap;
	}

	/**
	 * Gets the description for given language.
	 *
	 * @param lang
	 *            {@link Language}
	 * @return {@link String}
	 */
	public String getDescription(final LANG_ENUM lang) {
		return this.descriptionMap.get(lang);
	}

	/**
	 * Sets the description for given language.
	 *
	 * @param lang
	 *            {@link Language}
	 * @param description
	 *            {@link String}
	 */
	public void setDescription(final LANG_ENUM lang, final String description) {
		if (lang == null || description == null)
			throw new IllegalArgumentException();
		this.propertyChangeSupport.firePropertyChange(PROPERTY_DESC_MAP, this.descriptionMap,
				this.descriptionMap.put(lang, description.trim()));
	}

	/**
	 * Method for Sets the description map.
	 *
	 * @param descriptionMap
	 *            the descriptionMap to set
	 */
	public void setDescriptionMap(final Map<LANG_ENUM, String> descriptionMap) {
		if (descriptionMap == null)
			throw new IllegalArgumentException();
		this.propertyChangeSupport.firePropertyChange(PROPERTY_DESC_MAP, this.descriptionMap,
				this.descriptionMap = descriptionMap);
	}

	/**
	 * Gets the remarks map.
	 *
	 * @return the remarksMap
	 */
	public Map<LANG_ENUM, String> getRemarksMap() {
		return remarksMap;
	}

	/**
	 * Gets the notes for given language.
	 *
	 * @param lang
	 *            {@link Language}
	 * @return {@link String}
	 */
	public String getRemarks(final LANG_ENUM lang) {
		return this.remarksMap.get(lang);
	}

	/**
	 * Method for Sets the remarks map.
	 *
	 * @param remarksMap
	 *            the remarksMap to set
	 */
	public void setRemarksMap(final Map<LANG_ENUM, String> remarksMap) {
		if (remarksMap == null)
			throw new IllegalArgumentException();
		this.propertyChangeSupport.firePropertyChange(PROPERTY_NOTES_MAP, this.remarksMap,
				this.remarksMap = remarksMap);
	}

	/**
	 * Sets the remarks for given language.
	 *
	 * @param lang
	 *            {@link Language}
	 * @param remarks
	 *            {@link String}
	 */
	public void setRemarks(final LANG_ENUM lang, final String remarks) {
		if (lang == null || remarks == null)
			throw new IllegalArgumentException();
		this.propertyChangeSupport.firePropertyChange(PROPERTY_NOTES_MAP, this.remarksMap,
				this.remarksMap.put(lang, remarks));
	}

	/**
	 * Gets the group id.
	 *
	 * @return the groupId
	 */
	/**
	 * @return
	 */
	public String getGroupId() {
		return groupId;
	}

	/**
	 * Sets the group id.
	 *
	 * @param siteId
	 *            the groupId to set
	 */
	public void setGroupId(String groupId) {
		// if (groupId == null) throw new IllegalArgumentException();
		this.propertyChangeSupport.firePropertyChange(PROPERTY_GROUPID, this.groupId, this.groupId = groupId);
	}

	/**
	 * Gets the icon.
	 *
	 * @return the icon
	 */
	public Icon getIcon() {
		return icon;
	}

	/**
	 * Sets the icon.
	 *
	 * @param icon
	 *            the icon to set
	 */
	public void setIcon(Icon icon) {
		if (icon == null)
			throw new IllegalArgumentException();
		this.propertyChangeSupport.firePropertyChange(PROPERTY_ICON, this.icon, this.icon = icon);
	}

	/**
	 * Gets the operation mode.
	 *
	 * @return the operation mode
	 */
	public int getOperationMode() {
		return operationMode;
	}

	/**
	 * Sets the operation mode.
	 *
	 * @param operationMode
	 *            the new operation mode
	 */
	public void setOperationMode(int operationMode) {
		this.propertyChangeSupport.firePropertyChange(PROPERTY_OPERATION_MODE, this.operationMode,
				this.operationMode = operationMode);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.BeanModel#
	 * propertyChange(java.beans.PropertyChangeEvent)
	 */
	@Override
	public void propertyChange(PropertyChangeEvent event) {
		this.propertyChangeSupport.firePropertyChange(event.getPropertyName(), event.getOldValue(),
				event.getNewValue());
	}

	/**
	 * Method for Deep copy group.
	 *
	 * @param update
	 *            {@link boolean}
	 * @return the group {@link GroupModel}
	 */
	public UserGroupModel deepCopyUserGroup(boolean update, UserGroupModel updateThisObject) {
		UserGroupModel clonedGroup = null;
		try {
			String currentGroupId = XMSystemUtil.isEmpty(this.getGroupId()) ? CommonConstants.EMPTY_STR
					: this.getGroupId();
			String currentName = XMSystemUtil.isEmpty(this.getName()) ? CommonConstants.EMPTY_STR : this.getName();
			Map<LANG_ENUM, String> currentTranslationIdMap = new HashMap<>();
			Map<LANG_ENUM, String> currentDescriptionMap = new HashMap<>();
			Map<LANG_ENUM, String> currentRemarksMap = new HashMap<>();

			for (LANG_ENUM langEnum : LANG_ENUM.values()) {
				String translationId = this.getTranslationId(langEnum);
				currentTranslationIdMap.put(langEnum,
						XMSystemUtil.isEmpty(translationId) ? CommonConstants.EMPTY_STR : translationId);

				String description = this.getDescription(langEnum);
				currentDescriptionMap.put(langEnum,
						XMSystemUtil.isEmpty(description) ? CommonConstants.EMPTY_STR : description);

				String remarks = this.getRemarks(langEnum);
				currentRemarksMap.put(langEnum, XMSystemUtil.isEmpty(remarks) ? CommonConstants.EMPTY_STR : remarks);
			}
			//Icon currentIcon = this.getIcon() == null ? null : this.getIcon();
			Icon currentIcon = new Icon(this.getIcon().getIconId(), this.getIcon().getIconName(),
					this.getIcon().getIconPath(), this.getIcon().getIconType());
			if (update) {
				clonedGroup = updateThisObject;
			} else {
				clonedGroup = new UserGroupModel(currentGroupId, currentName, currentIcon,
						CommonConstants.OPERATIONMODE.VIEW);
			}
			clonedGroup.setGroupId(currentGroupId);
			clonedGroup.setName(currentName);
			clonedGroup.setIcon(currentIcon);
			clonedGroup.setTranslationIdMap(currentTranslationIdMap);
			clonedGroup.setDescriptionMap(currentDescriptionMap);
			clonedGroup.setRemarksMap(currentRemarksMap);
			return clonedGroup;
		} catch (Exception e) {
			return null;
		}
	}
}
