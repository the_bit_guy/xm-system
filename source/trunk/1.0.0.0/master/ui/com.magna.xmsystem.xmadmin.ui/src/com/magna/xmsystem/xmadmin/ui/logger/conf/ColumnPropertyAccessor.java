package com.magna.xmsystem.xmadmin.ui.logger.conf;

import java.beans.IntrospectionException;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.lang.reflect.Method;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.nebula.widgets.nattable.data.IColumnPropertyAccessor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.magna.xmsystem.dependencies.utils.XMSystemUtil;

/**
 * The Class ColumnPropertyAccessor.
 *
 * @param <R> the generic type
 * 
 * @author shashwat.anand
 */
public class ColumnPropertyAccessor<R> implements IColumnPropertyAccessor<R> {
	
	/** The Constant LOGGER. */
	private static final Logger LOGGER = LoggerFactory.getLogger(ColumnPropertyAccessor.class);
	
	/** The history log time date format. */
	private final SimpleDateFormat historyLogTimeDateFormat;
	
	/** The property names. */
	private final List<String> propertyNames;
	
	/** The property descriptor map. */
	private Map<String, PropertyDescriptor> propertyDescriptorMap;
	
	/**
	 * Instantiates a new column property accessor.
	 *
	 * @param propertyNames the property names
	 */
	public ColumnPropertyAccessor(String... propertyNames) {
        this.propertyNames = Arrays.asList(propertyNames);
        this.historyLogTimeDateFormat = XMSystemUtil.getHistoryLogTimeDateFormat();
    }
	
	/**
	 * Instantiates a new column property accessor.
	 *
	 * @param propertyNames the property names
	 */
	public ColumnPropertyAccessor(List<String> propertyNames) {
        this.propertyNames = propertyNames;
        this.historyLogTimeDateFormat = XMSystemUtil.getHistoryLogTimeDateFormat();
    }

	/* (non-Javadoc)
	 * @see org.eclipse.nebula.widgets.nattable.data.IColumnAccessor#getDataValue(java.lang.Object, int)
	 */
	@Override
	public Object getDataValue(R rowObject, int columnIndex) {
		try {
			PropertyDescriptor propertyDesc = getPropertyDescriptor(rowObject, columnIndex);
            Method readMethod = propertyDesc.getReadMethod();
            Object retVal = readMethod.invoke(rowObject);
            
			if (columnIndex == 1 && retVal instanceof Date) {
				return this.historyLogTimeDateFormat.format((Date)retVal);
			}
			return retVal;
        } catch (Exception e) {
        	LOGGER.warn(e.toString());;
            throw new RuntimeException(e);
        }
	}

	/* (non-Javadoc)
	 * @see org.eclipse.nebula.widgets.nattable.data.IColumnAccessor#setDataValue(java.lang.Object, int, java.lang.Object)
	 */
	@Override
	public void setDataValue(R rowObject, int columnIndex, Object newValue) {
		// Nothing
	}

	/* (non-Javadoc)
	 * @see org.eclipse.nebula.widgets.nattable.data.IColumnAccessor#getColumnCount()
	 */
	@Override
	public int getColumnCount() {
		return this.propertyNames.size();
	}

	/* (non-Javadoc)
	 * @see org.eclipse.nebula.widgets.nattable.data.IColumnPropertyResolver#getColumnProperty(int)
	 */
	@Override
	public String getColumnProperty(int columnIndex) {
		return this.propertyNames.get(columnIndex);
	}

	/* (non-Javadoc)
	 * @see org.eclipse.nebula.widgets.nattable.data.IColumnPropertyResolver#getColumnIndex(java.lang.String)
	 */
	@Override
	public int getColumnIndex(String propertyName) {
		return this.propertyNames.indexOf(propertyName);
	}
	
	/**
	 * Gets the property descriptor.
	 *
	 * @param rowObj the row obj
	 * @param columnIndex the column index
	 * @return the property descriptor
	 * @throws IntrospectionException the introspection exception
	 */
	private PropertyDescriptor getPropertyDescriptor(R rowObj, int columnIndex) throws IntrospectionException {
        synchronized (rowObj) {
            if (this.propertyDescriptorMap == null) {
                this.propertyDescriptorMap = new HashMap<String, PropertyDescriptor>();
                PropertyDescriptor[] propertyDescriptors =
                        Introspector.getBeanInfo(rowObj.getClass()).getPropertyDescriptors();
                for (PropertyDescriptor propertyDescriptor : propertyDescriptors) {
                    this.propertyDescriptorMap.put(propertyDescriptor.getName(), propertyDescriptor);
                }
            }
        }

        final String propertyName = this.propertyNames.get(columnIndex);
        return this.propertyDescriptorMap.get(propertyName);
    }

}
