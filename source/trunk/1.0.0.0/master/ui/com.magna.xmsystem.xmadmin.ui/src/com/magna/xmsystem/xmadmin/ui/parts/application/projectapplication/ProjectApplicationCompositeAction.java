package com.magna.xmsystem.xmadmin.ui.parts.application.projectapplication;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.inject.Inject;

import org.eclipse.core.databinding.Binding;
import org.eclipse.core.databinding.DataBindingContext;
import org.eclipse.core.databinding.UpdateValueStrategy;
import org.eclipse.core.databinding.beans.BeanProperties;
import org.eclipse.core.databinding.observable.value.IObservableValue;
import org.eclipse.core.databinding.observable.value.IValueChangeListener;
import org.eclipse.core.databinding.observable.value.ValueChangeEvent;
import org.eclipse.e4.core.services.nls.Translation;
import org.eclipse.e4.ui.di.Persist;
import org.eclipse.e4.ui.model.application.ui.MDirtyable;
import org.eclipse.jface.databinding.fieldassist.ControlDecorationSupport;
import org.eclipse.jface.databinding.swt.WidgetProperties;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.fieldassist.ControlDecoration;
import org.eclipse.jface.fieldassist.FieldDecorationRegistry;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.viewers.TreePath;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.VerifyEvent;
import org.eclipse.swt.events.VerifyListener;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Link;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.widgets.ToolItem;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.client.ResourceAccessException;

import com.magna.xmbackend.entities.LanguagesTbl;
import com.magna.xmbackend.entities.ProjectAppTranslationTbl;
import com.magna.xmbackend.entities.ProjectApplicationsTbl;
import com.magna.xmbackend.vo.projectApplication.ProjectApplicationRequest;
import com.magna.xmsystem.dependencies.customExceptions.UnauthorizedAccessException;
import com.magna.xmsystem.dependencies.utils.LANG_ENUM;
import com.magna.xmsystem.dependencies.utils.XMSystemUtil;
import com.magna.xmsystem.ui.controls.widgets.CustomMessageDialog;
import com.magna.xmsystem.xmadmin.message.Message;
import com.magna.xmsystem.xmadmin.message.MessageRegistry;
import com.magna.xmsystem.xmadmin.restclient.application.ProjectAppController;
import com.magna.xmsystem.xmadmin.ui.Application.dialogs.BrowseApplicationDialog;
import com.magna.xmsystem.xmadmin.ui.dialogs.lang.ControlModel;
import com.magna.xmsystem.xmadmin.ui.dialogs.lang.TextAreaModifyListener;
import com.magna.xmsystem.xmadmin.ui.dialogs.lang.XMAdminLangTextAreaDialog;
import com.magna.xmsystem.xmadmin.ui.dialogs.lang.XMAdminLangTextDialog;
import com.magna.xmsystem.xmadmin.ui.parts.icons.IconDialog;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.AdminTreeviewer;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.AdminTreeFactory;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.baseapplicationmodel.BaseApplication;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.projectapplicationmodel.ProjectApplication;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.projectapplicationmodel.ProjectApplicationChild;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.projectapplicationmodel.ProjectApplications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.icon.Icon;
import com.magna.xmsystem.xmadmin.ui.validation.NameValidation;
import com.magna.xmsystem.xmadmin.ui.validation.StatusValidation;
import com.magna.xmsystem.xmadmin.ui.validation.SymbolValidation;
import com.magna.xmsystem.xmadmin.util.CommonConstants;
import com.magna.xmsystem.xmadmin.util.MessageType;
import com.magna.xmsystem.xmadmin.util.XMAdminUtil;

/**
 * Class for Project application composite action.
 *
 * @author Chiranjeevi.Akula
 */
public class ProjectApplicationCompositeAction extends ProjectApplicationCompositeUI {

	/** The Constant PROGRAMMATICALLY. */
	protected static final Object PROGRAMMATICALLY = "Programmatically";

	/** The Constant LOGGER. */
	private static final Logger LOGGER = LoggerFactory.getLogger(ProjectApplicationCompositeAction.class);
	/**
	 * Member variable 'project application model' for
	 * {@link ProjectApplication}.
	 */
	private ProjectApplication projectApplicationModel;
	
	private ProjectApplicationChild projectApplicationChildModel;
	
	/** Member variable for {@link MessageRegistry}. */
	@Inject
	private MessageRegistry registry;

	/** Member variable 'messages' for {@link Message}. */
	@Inject
	@Translation
	private Message messages;

	/** Member variable 'widget value' for {@link IObservableValue<?>}. */
	private IObservableValue<?> widgetValue;

	/** Member variable 'model value' for {@link IObservableValue<?>}. */
	private IObservableValue<?> modelValue;

	/** Member variable 'data bind context' for {@link DataBindingContext}. */
	final transient DataBindingContext dataBindContext = new DataBindingContext();

	/** Member variable 'old model' for {@link ProjectApplication}. */
	private ProjectApplication oldModel;

	/** Member variable 'dirty' for {@link MDirtyable}. */
	private MDirtyable dirty;

	/**
	 * Constructor for ProjectApplicationCompositeAction Class.
	 *
	 * @param parent
	 *            {@link Composite}
	 */
	@Inject
	public ProjectApplicationCompositeAction(final Composite parent) {
		super(parent, SWT.None);
		initListeners();
	}

	/**
	 * Method for Bind values.
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public void bindValues() {
		try {
			// Name field binding
			widgetValue = WidgetProperties.text(SWT.Modify).observe(this.txtName);
			modelValue = BeanProperties.value(ProjectApplication.class, ProjectApplication.PROPERTY_NAME).observe(this.projectApplicationModel);
			widgetValue.addValueChangeListener(new IValueChangeListener() {
				/**
				 * handler to update button status
				 */
				@Override
				public void handleValueChange(final ValueChangeEvent event) {
					updateButtonStatus(event);
				}
			});

			// define the UpdateValueStrategy
			final UpdateValueStrategy update = new UpdateValueStrategy();
			update.setAfterGetValidator(new NameValidation(messages, StatusValidation.APPLICATION));
			Binding bindValue = dataBindContext.bindValue(widgetValue, modelValue, update, null);
			ControlDecorationSupport.create(bindValue, SWT.TOP | SWT.LEFT);

			// Active check box binding
			widgetValue = WidgetProperties.selection().observe(this.activeBtn);
			modelValue = BeanProperties.value(ProjectApplication.class, ProjectApplication.PROPERTY_ACTIVE)
					.observe(this.projectApplicationModel);

			bindValue = dataBindContext.bindValue(widgetValue, modelValue);
			
			//description binding
			widgetValue = WidgetProperties.text(SWT.Modify).observe(this.txtDesc);
			modelValue = BeanProperties.value(ProjectApplication.class, ProjectApplication.PROPERTY_DESC).observe(this.projectApplicationModel);
			bindValue = dataBindContext.bindValue(widgetValue, modelValue);

			// Singleton binding
			widgetValue = WidgetProperties.selection().observe(this.btnSingleton);
			modelValue = BeanProperties.value(ProjectApplication.class, ProjectApplication.PROPERTY_SINGLETON)
					.observe(this.projectApplicationModel);
			bindValue = dataBindContext.bindValue(widgetValue, modelValue);

			// Parent check box binding
			widgetValue = WidgetProperties.selection().observe(this.parentBtn);
			modelValue = BeanProperties.value(ProjectApplication.class, ProjectApplication.PROPERTY_PARENT)
					.observe(this.projectApplicationModel);
			bindValue = dataBindContext.bindValue(widgetValue, modelValue);

			// Symbol toolItem setup
			Icon icon;
			String iconPath;
			if ((icon = this.projectApplicationModel.getIcon()) != null
					&& !XMSystemUtil.isEmpty(iconPath = icon.getIconPath())) {
				if (iconPath.contains("null")) { //$NON-NLS-1$
					toolItem.setImage(XMSystemUtil.getInstance().getImage(this.getClass(), "icons/16x16/browse.png")); //$NON-NLS-1$
				} else {
					toolItem.setImage(XMSystemUtil.getInstance().getImage(this.getClass(), iconPath, true, true));
					// Symbol toolItem binding
					widgetValue = WidgetProperties.tooltipText().observe(this.toolItem);
					modelValue = BeanProperties
							.value(ProjectApplication.class,
									ProjectApplication.PROPERTY_ICON + "." + Icon.PROPERTY_ICONNAME, Icon.class)
							.observe(this.projectApplicationModel);
					bindValue = dataBindContext.bindValue(widgetValue, modelValue);

					// Symbol Field binding
					widgetValue = WidgetProperties.text(SWT.Modify).observe(this.txtSymbol);
					modelValue = BeanProperties
							.value(ProjectApplication.class,
									ProjectApplication.PROPERTY_ICON + "." + Icon.PROPERTY_ICONNAME, Icon.class)
							.observe(this.projectApplicationModel);
					final UpdateValueStrategy symbolUpdate = new UpdateValueStrategy();
					symbolUpdate.setAfterGetValidator(new SymbolValidation());
					bindValue = dataBindContext.bindValue(widgetValue, modelValue, symbolUpdate, null);
					ControlDecorationSupport.create(bindValue, SWT.TOP | SWT.LEFT);
				}
			}
		} catch (Exception e) {
			LOGGER.error("Exception occured while databinding", e);
		}
	}
	
	@SuppressWarnings("rawtypes")
	private void updateButtonStatus(final ValueChangeEvent event) {
		final String name = (String) event.getObservableValue().getValue();
		if (this.saveBtn != null) {
			if (XMSystemUtil.isEmpty(name) || name.trim().length() == 0
					|| (!name.matches(CommonConstants.RegularExpressions.ALLOWED_APP_NAME_REGEX))) {
				this.saveBtn.setEnabled(false);
			} else {
				this.saveBtn.setEnabled(true);
			}
		}
	}

	/**
	 * Gets the project application model.
	 *
	 * @return the project application model
	 */
	public ProjectApplication getProjectApplicationModel() {
		return projectApplicationModel;
	}

	/**
	 * Sets the project application model.
	 *
	 * @param projectApplicationModel
	 *            the new project application model
	 */
	public void setProjectApplicationModel(final ProjectApplication projectApplicationModel) {
		this.projectApplicationModel = projectApplicationModel;
	}

	public ProjectApplicationChild getProjectApplicationChildModel() {
		return projectApplicationChildModel;
	}

	public void setProjectApplicationChildModel(ProjectApplicationChild projectApplicationChildModel) {
		this.projectApplicationChildModel = projectApplicationChildModel;
	}

	/**
	 * Method for Inits the listeners.
	 */
	private void initListeners() {
		// Add listener to widgets
		// this.txtName.addListener(eventType, listener);;
		this.descLink.addSelectionListener(new SelectionAdapter() {

			@Override
			public void widgetSelected(SelectionEvent event) {
				final Link linkWidget = (Link) event.widget;
				openDescDialog(linkWidget.getShell());
			}
		});
		this.nameLink.addSelectionListener(new SelectionAdapter() {

			@Override
			public void widgetSelected(SelectionEvent event) {
				final Link linkWidget = (Link) event.widget;
				openNameDialog(linkWidget.getShell());
			}
		});

		// Event handling when users click on remarks lang links.
		this.remarksLink.addSelectionListener(new SelectionAdapter() {

			@Override
			public void widgetSelected(SelectionEvent event) {
				final Link linkWidget = (Link) event.widget;
				openRemarksDialog(linkWidget.getShell());
			}
		});

		if (this.saveBtn != null) {
			this.saveBtn.addSelectionListener(new SelectionAdapter() {

				/**
				 * Save button handler
				 */
				@Override
				public void widgetSelected(final SelectionEvent event) {
					saveHandler();
				}
			});
		}

		if (this.cancelBtn != null) {
			this.cancelBtn.addSelectionListener(new SelectionAdapter() {

				/**
				 * Cancel button handler
				 */
				@Override
				public void widgetSelected(final SelectionEvent event) {
					cancelHandler();
				}
			});
		}

		this.toolItem.addSelectionListener(new SelectionAdapter() {

			/**
			 * Symbol button handler
			 */
			@Override
			public void widgetSelected(final SelectionEvent event) {
				if ((boolean) toolItem.getData("editable")) {
					final ToolItem widget = (ToolItem) event.widget;
					final IconDialog dialog = new IconDialog(widget.getParent().getShell(),
							messages.browseIconDialogTitle, messages.icontableviewerSecondColumnLabel);

					final int returnVal = dialog.open();
					if (IDialogConstants.OK_ID == returnVal) {
						final Icon checkedIcon = dialog.getCheckedIcon();
						// toolItem.setToolTipText(checkedIcon.getIconName());
						toolItem.setImage(XMSystemUtil.getInstance().getImage(this.getClass(), checkedIcon.getIconPath(), true, true));
						txtSymbol.setText(checkedIcon.getIconName());
						projectApplicationModel.setIcon(checkedIcon);
					}
				}
			}
		});
		this.txtRemarks
				.addModifyListener(new TextAreaModifyListener(this.lblRemarksCount, ProjectApplication.REMARK_LIMIT));

		this.programToolItem.addSelectionListener(new SelectionAdapter() {

			@Override
			public void widgetSelected(final SelectionEvent event) {
				if ((boolean) programToolItem.getData("editable")) {
					final ToolItem widget = (ToolItem) event.widget;
					Map<String, IAdminTreeChild> getapplication = AdminTreeFactory.getInstance()
							.getProjectApplications().getProjectApplications();
					List<IAdminTreeChild> list = new ArrayList<>(getapplication.values());
					List<IAdminTreeChild> parentList = new ArrayList<IAdminTreeChild>();
					boolean parentSelection = parentBtn.getSelection();
					if (!parentSelection) {
						for (IAdminTreeChild adminTreeChild : list) {
							if (adminTreeChild instanceof ProjectApplication) {
								if (((ProjectApplication) adminTreeChild).isParent()&& !((ProjectApplication) adminTreeChild)
										.getProjectApplicationId().equals(projectApplicationModel.getProjectApplicationId())) {
									parentList.add((IAdminTreeChild) adminTreeChild);
								}
							}
						}
					}
					
					final AdminTreeviewer adminTree = XMAdminUtil.getInstance().getAdminTree();
					final Object selectionObj = adminTree.getSelection();
					if (selectionObj instanceof IStructuredSelection) {
						Object firstElement = ((IStructuredSelection) selectionObj).getFirstElement();
						if (firstElement instanceof ProjectApplication) {
							ProjectApplication projectApp = (ProjectApplication) firstElement;
							if (parentList.contains(projectApp.getParent())) {
								parentList.remove(projectApp.getParent());
							}
						}
					}
					BrowseApplicationDialog scriptDialog = new BrowseApplicationDialog(widget.getParent().getShell(),
							messages, parentList, true, parentSelection);
					final int returnVal = scriptDialog.open();
					if (IDialogConstants.OK_ID == returnVal) {
						Object selectedIteam = scriptDialog.getSelectedApplication();
						if (selectedIteam != null && selectedIteam instanceof ProjectApplication) {
							ProjectApplication projectApplication = (ProjectApplication) selectedIteam;
							if (getOldModel()!=null && getOldModel().isParent()) {
								CustomMessageDialog.openError(getShell(), messages.appPositionErrorTitle,
										messages.appChildPositionError);
								parentBtn.setSelection(true);
								txtBaseApplication.setEnabled(false);
								programAppToolItem.setEnabled(false);
								txtBaseApplication.setText(CommonConstants.EMPTY_STR);
								projectApplicationModel.setBaseApplicationId(null);
								notifyBaseAppText();
								return;
							}
							projectApplicationModel.setPosition(projectApplication.getProjectApplicationId());
							parentBtn.setEnabled(false);
						} else if (selectedIteam instanceof String) {
							for (String positionType : CommonConstants.Application.POSITIONS) {
								if (selectedIteam.toString()
										.equalsIgnoreCase(CommonConstants.Application.POSITIONS[1])) {
									projectApplicationModel.setPosition(selectedIteam.toString());
									parentBtn.setEnabled(false);
									break;
								} else if ((selectedIteam.toString()
										.equalsIgnoreCase(CommonConstants.Application.POSITIONS[0])
										&& projectApplicationModel.getBaseApplicationId() == null)
										|| (selectedIteam.toString()
												.equalsIgnoreCase(CommonConstants.Application.POSITIONS[2])
												&& projectApplicationModel.getBaseApplicationId() == null)) {
									projectApplicationModel.setPosition(selectedIteam.toString());
									boolean flag = true;
									if (projectApplicationModel.isParent() /*&& getOldModel()!=null*/) {
										flag = false;
									}
									parentBtn.setEnabled(flag);
									break;
								} else if ((selectedIteam.toString()
										.equalsIgnoreCase(CommonConstants.Application.POSITIONS[0])
										|| projectApplicationModel.getBaseApplicationId() != null)
										|| (selectedIteam.toString()
												.equalsIgnoreCase(CommonConstants.Application.POSITIONS[2])
												|| projectApplicationModel.getBaseApplicationId() != null)) {
									projectApplicationModel.setPosition(selectedIteam.toString());
									parentBtn.setEnabled(false);
									break;
								}
							}
						} else {
							projectApplicationModel.setPosition(null);
						}
					}
				}
				updatePosition();
			}
		});

		this.programAppToolItem.addSelectionListener(new SelectionAdapter() {

			@Override
			public void widgetSelected(final SelectionEvent event) {
				if ((boolean) programAppToolItem.getData("editable")) {

					final ToolItem widget = (ToolItem) event.widget;
					Map<String, IAdminTreeChild> baseApplications = AdminTreeFactory.getInstance().getBaseApplications()
							.getBaseApplications();

					List<IAdminTreeChild> list = new ArrayList<>(baseApplications.values());
					BrowseApplicationDialog scriptDialog = new BrowseApplicationDialog(widget.getParent().getShell(),
							messages, list, false, true);
					final int returnVal = scriptDialog.open();
					if (IDialogConstants.OK_ID == returnVal) {
						Object selectedBaseApp = scriptDialog.getSelectedApplication();
						if (selectedBaseApp != null && selectedBaseApp instanceof BaseApplication) {
							BaseApplication baseApplication = (BaseApplication) selectedBaseApp;
							projectApplicationModel.setBaseApplicationId(baseApplication.getBaseApplicationId());
							parentBtn.setEnabled(false);
						} else {
							projectApplicationModel.setBaseApplicationId(null);
						}
					}
				}
				updateBaseApplication();

			}
		});

		this.parentBtn.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent event) {
				Button button = (Button) event.widget;

				boolean isParent = button.getSelection();
				if (isParent) {
					txtBaseApplication.setText(CommonConstants.EMPTY_STR);
					txtBaseApplication.setEnabled(false);
					programAppToolItem.setEnabled(false);
					btnSingleton.setEnabled(false);
					btnSingleton.setSelection(false);
					projectApplicationModel.setBaseApplicationId(null);
					if (!(PROGRAMMATICALLY.equals(event.data))) {
						CustomMessageDialog.openInformation(getShell(), messages.parentEnableInfoTitle,
								messages.parentEnableInfo);
					}
					notifyBaseAppText();
					if (txtPosition.getText().equalsIgnoreCase(CommonConstants.Application.POSITIONS[1])) {
						txtPosition.setText(CommonConstants.EMPTY_STR);
					}
				} else {
					txtBaseApplication.setEnabled(true);
					programAppToolItem.setEnabled(true);
					if (projectApplicationModel.getOperationMode() == CommonConstants.OPERATIONMODE.VIEW) {
						btnSingleton.setEnabled(false);
					} else {
						btnSingleton.setEnabled(true);
					}
					notifyBaseAppText();
				}
			}
		});
		this.txtPosition.addModifyListener(new ModifyListener() {
			ControlDecoration txtPostionDecroator = new ControlDecoration(txtPosition, SWT.TOP);

			@Override
			public void modifyText(ModifyEvent event) {
				final Image nameDecoratorImage = FieldDecorationRegistry.getDefault()
						.getFieldDecoration(FieldDecorationRegistry.DEC_ERROR).getImage();
				txtPostionDecroator.setImage(nameDecoratorImage);
				Text text = (Text) event.widget;
				if (text.getText().trim().length() <= 0) {
					txtPostionDecroator.setDescriptionText(messages.appPostionEmptyError);
					txtPostionDecroator.show();
				} else if (text.getText().trim().length() > 0) {
					txtPostionDecroator.hide();
				}
			}
		});

		this.txtBaseApplication.addModifyListener(new ModifyListener() {
			ControlDecoration txtBaseAppDecroator = new ControlDecoration(txtBaseApplication, SWT.TOP);

			@Override
			public void modifyText(ModifyEvent event) {
				final Image nameDecoratorImage = FieldDecorationRegistry.getDefault()
						.getFieldDecoration(FieldDecorationRegistry.DEC_ERROR).getImage();
				txtBaseAppDecroator.setImage(nameDecoratorImage);
				Text text = (Text) event.widget;
				if (!parentBtn.getSelection()) {
					if (text.getText().trim().length() <= 0) {
						txtBaseAppDecroator.setDescriptionText(messages.appBaseAppEmptyError);
						txtBaseAppDecroator.show();
					} else if (text.getText().trim().length() > 0) {
						txtBaseAppDecroator.hide();
					}
				} else {
					txtBaseAppDecroator.hide();
				}

			}
		});
		
		this.txtRemarks.addVerifyListener(new VerifyListener() {

			@Override
			public void verifyText(VerifyEvent event) {
				String source = ((Text) event.widget).getText();
				final String remarkText = source.substring(0, event.start) + event.text + source.substring(event.end);
				int length = remarkText.length();
				if (length > ProjectApplication.REMARK_LIMIT) {
					event.doit = false;

				}
			}
		});
	}

	/**
	 * Notify base app text.
	 */
	private void notifyBaseAppText() {
		Event event1 = new Event();
		event1.data = PROGRAMMATICALLY;
		txtBaseApplication.notifyListeners(SWT.Modify, event1);
	}

	/**
	 * Update english name.
	 */
	/*private void updateEnglishName() {
		if (!XMSystemUtil.isEmpty(this.projectApplicationModel.getName(LANG_ENUM.GERMAN))) {
			this.projectApplicationModel.setName(LANG_ENUM.ENGLISH,
					this.projectApplicationModel.getName(LANG_ENUM.GERMAN));
		}
	}*/

	/**
	 * Method for Validate.
	 *
	 * @return true, if successful
	 */
	private boolean validate() {
		if (!saveBtn.isEnabled()) {
			return false;
		}
		String projectApplicationName = this.projectApplicationModel.getName();
		Icon icon;

		if (XMSystemUtil.isEmpty(projectApplicationName) && (icon = this.projectApplicationModel.getIcon()) != null
				&& XMSystemUtil.isEmpty(icon.getIconName())) {
			CustomMessageDialog.openError(this.getShell(), messages.nameSymbolErrorTitle, messages.nameSymbolError);
			return false;
		}
		if (XMSystemUtil.isEmpty(projectApplicationName)) {
			CustomMessageDialog.openError(this.getShell(), messages.appNameErrorTitle, messages.appNameEmptyEnError);
			return false;
		}
		if ((icon = this.projectApplicationModel.getIcon()) != null && XMSystemUtil.isEmpty(icon.getIconName())) {
			CustomMessageDialog.openError(this.getShell(), messages.symbolErrorTitle, messages.symbolError);
			return false;
		}
		final ProjectApplications projectApplications = AdminTreeFactory.getInstance().getProjectApplications();
		final Collection<IAdminTreeChild> projectAppsCollection = projectApplications.getProjectAppCollection();

		if (!XMSystemUtil.isEmpty(projectApplicationName)) {
			final Map<String, Long> result = projectAppsCollection.stream()
					.collect(Collectors.groupingBy(projectApplication -> {
						if (!XMSystemUtil.isEmpty(((ProjectApplication) projectApplication).getName())) {
							return ((ProjectApplication) projectApplication).getName().toUpperCase();
						}
						return CommonConstants.EMPTY_STR;
					}, Collectors.counting()));

			Map<String, String> childResult = new HashMap<String, String>();
			for (IAdminTreeChild iAdminTreeChild : projectAppsCollection) {
				if (iAdminTreeChild instanceof ProjectApplication) {
					ProjectApplication projectApplication = (ProjectApplication) iAdminTreeChild;
					Map<String, IAdminTreeChild> projectApplicationChildren = projectApplication
							.getProjectApplicationChildren();
					Collection<IAdminTreeChild> values = projectApplicationChildren.values();
					for (IAdminTreeChild projectAppChild : values) {
						if (projectAppChild instanceof ProjectApplicationChild) {
							ProjectApplicationChild childProjectApp = (ProjectApplicationChild) projectAppChild;
							childResult.put(childProjectApp.getName().toUpperCase(),
									childProjectApp.getProjectApplicationId());
						}
					}
				}
			}
			if (result.containsKey(projectApplicationName.toUpperCase())
					|| childResult.containsKey(projectApplicationName.toUpperCase())) {
				if ((this.projectApplicationModel.getOperationMode() == CommonConstants.OPERATIONMODE.CHANGE
						&& !projectApplicationName.equalsIgnoreCase(this.oldModel.getName()))
						|| this.projectApplicationModel.getOperationMode() == CommonConstants.OPERATIONMODE.CREATE) {
					CustomMessageDialog.openError(this.getShell(), messages.appNameErrorTitle,
							messages.existingEnNameError);
					return false;
				}
			}
		}
		if (XMSystemUtil.isEmpty(this.txtPosition.getText())) {
			CustomMessageDialog.openError(this.getShell(), messages.appPositionErrorTitle,
					messages.appPostionEmptyError);
			return false;
		}
		if ((!this.parentBtn.getSelection()) && XMSystemUtil.isEmpty(this.txtBaseApplication.getText())) {
			CustomMessageDialog.openError(this.getShell(), messages.appBaseAppErrorTitle,
					messages.appBaseAppEmptyError);
			return false;
		}

		return true;
	}

	/**
	 * Method for Register messages.
	 *
	 * @param registry
	 *            {@link MessageRegistry}
	 */
	public void registerMessages(final MessageRegistry registry) {
		registry.register((text) -> {
			if (grpProjectApplication != null && !grpProjectApplication.isDisposed()) {
				grpProjectApplication.setText(text);
			}
		}, (message) -> {
			if (projectApplicationModel != null) {
				String name = this.projectApplicationModel.getName();
				if (projectApplicationModel.getOperationMode() == CommonConstants.OPERATIONMODE.VIEW) {
					return message.objectGroupDisaplyLabel + " \'" + name + "\'"; //$NON-NLS-1$ //$NON-NLS-2$
				} else if (projectApplicationModel.getOperationMode() == CommonConstants.OPERATIONMODE.CHANGE) {
					return message.objectGroupChangeLabel + " \'" + name + "\'"; //$NON-NLS-1$ //$NON-NLS-2$
				} else if (projectApplicationModel.getOperationMode() == CommonConstants.OPERATIONMODE.CREATE) {
					return message.projectAppGroupCreateLabel;
				}
			}
			return CommonConstants.EMPTY_STR;
		});
		registry.register((text) -> {
			if (lblName != null && !lblName.isDisposed()) {
				lblName.setText(text);
			}
		}, (message) -> {
			if (lblName != null && !lblName.isDisposed()) {
				return getUpdatedWidgetText(message.objectNameLabel, lblName);
			}
			return CommonConstants.EMPTY_STR;
		});
		registry.register((text) -> {
			if (lblDescrition != null && !lblDescrition.isDisposed()) {
				lblDescrition.setText(text);
			}
		}, (message) -> {
			if (lblDescrition != null && !lblDescrition.isDisposed()) {
				return getUpdatedWidgetText(message.objectDescriptionLabel, lblDescrition);
			}
			return CommonConstants.EMPTY_STR;
		});
		registry.register((text) -> {
			if (lblActive != null && !lblActive.isDisposed()) {
				lblActive.setText(text);
			}
		}, (message) -> {
			if (lblActive != null && !lblActive.isDisposed()) {
				return getUpdatedWidgetText(message.objectActiveLabel, lblActive);
			}
			return CommonConstants.EMPTY_STR;
		});
		registry.register((text) -> {
			if (lblSymbol != null && !lblSymbol.isDisposed()) {
				lblSymbol.setText(text);
			}
		}, (message) -> {
			if (lblSymbol != null && !lblSymbol.isDisposed()) {
				return getUpdatedWidgetText(message.objectSymbolLabel, lblSymbol);
			}
			return CommonConstants.EMPTY_STR;
		});
		registry.register((text) -> {
			if (lblSingleton != null && !lblSingleton.isDisposed()) {
				lblSingleton.setText(text);
			}
		}, (message) -> {
			if (lblSingleton != null && !lblSingleton.isDisposed()) {
				return getUpdatedWidgetText(message.objectSingletonLabel, lblSingleton);
			}
			return CommonConstants.EMPTY_STR;
		});
		registry.register((text) -> {
			if (lblPosition != null && !lblPosition.isDisposed()) {
				lblPosition.setText(text);
			}
		}, (message) -> {
			if (lblPosition != null && !lblPosition.isDisposed()) {
				return getUpdatedWidgetText(message.objectPositionLabel, lblPosition);
			}
			return CommonConstants.EMPTY_STR;
		});
		registry.register((text) -> {
			if (lblBaseApplication != null && !lblBaseApplication.isDisposed()) {
				lblBaseApplication.setText(text);
			}
		}, (message) -> {
			if (lblBaseApplication != null && !lblBaseApplication.isDisposed()) {
				return getUpdatedWidgetText(message.objectApplicationLabel, lblBaseApplication);
			}
			return CommonConstants.EMPTY_STR;
		});
		if (saveBtn != null) {
			registry.register((text) -> {
				if (saveBtn != null && !saveBtn.isDisposed()) {
					saveBtn.setText(text);
				}
			}, (message) -> {
				if (saveBtn != null && !saveBtn.isDisposed()) {
					return getUpdatedWidgetText(message.saveButtonText, saveBtn);
				}
				return CommonConstants.EMPTY_STR;
			});
		}
		if (cancelBtn != null) {
			registry.register((text) -> {
				if (cancelBtn != null && !cancelBtn.isDisposed()) {
					cancelBtn.setText(text);
				}
			}, (message) -> {
				if (cancelBtn != null && !cancelBtn.isDisposed()) {
					return getUpdatedWidgetText(message.cancelButtonText, cancelBtn);
				}
				return CommonConstants.EMPTY_STR;
			});
		}

		registry.register((text) -> {
			if (remarksLink != null && !remarksLink.isDisposed()) {
				remarksLink.setText(text);
			}
		}, (message) -> {
			if (remarksLink != null && !remarksLink.isDisposed()) {
				return getUpdatedWidgetText("<a>" + message.objectTranslationLinkText + "</a>", remarksLink);
			}
			return CommonConstants.EMPTY_STR;
		});

		registry.register((text) -> {
			if (remarksLabel != null && !remarksLabel.isDisposed()) {
				remarksLabel.setText(text);
			}
		}, (message) -> {
			if (remarksLabel != null && !remarksLabel.isDisposed()) {
				return getUpdatedWidgetText(message.objectRemarkLabel, remarksLabel);
			}
			return CommonConstants.EMPTY_STR;
		});

		registry.register((text) -> {
			if (descLink != null && !descLink.isDisposed()) {
				descLink.setText(text);
			}
		}, (message) -> {
			if (descLink != null && !descLink.isDisposed()) {
				return getUpdatedWidgetText("<a>" + message.objectTranslationLinkText + "</a>", descLink);
			}
			return CommonConstants.EMPTY_STR;
		});

		registry.register((text) -> {
			if (nameLink != null && !nameLink.isDisposed()) {
				nameLink.setText(text);
			}
		}, (message) -> {
			if (nameLink != null && !nameLink.isDisposed()) {
				return getUpdatedWidgetText("<a>" + message.objectTranslationLinkText + "</a>", nameLink);
			}
			return CommonConstants.EMPTY_STR;
		});
		registry.register((text) -> {
			if (lblParent != null && !lblParent.isDisposed()) {
				lblParent.setText(text);
			}
		}, (message) -> {
			if (lblParent != null && !lblParent.isDisposed()) {
				return getUpdatedWidgetText(message.objectParentLabel, lblParent);
			}
			return CommonConstants.EMPTY_STR;
		});

		registry.register((text) -> {
			if (this.projectApplicationModel != null && txtName != null && !txtName.isDisposed()) {
				txtName.setText(text);
			}
		}, (message) -> {
			if (this.projectApplicationModel != null && txtName != null && !txtName.isDisposed()) {
				return this.projectApplicationModel.getName() == null ? CommonConstants.EMPTY_STR : this.projectApplicationModel.getName();
			}
			return CommonConstants.EMPTY_STR;
		});

		registry.register((text) -> {
			if (this.projectApplicationModel != null && txtDesc != null && !txtDesc.isDisposed()) {
				txtDesc.setText(text);
			}
		}, (message) -> {
			if (this.projectApplicationModel != null && txtDesc != null && !txtDesc.isDisposed()) {
				return this.projectApplicationModel.getDescription() == null ? CommonConstants.EMPTY_STR : this.projectApplicationModel.getDescription();
			}
			return CommonConstants.EMPTY_STR;
		});
		registry.register((text) -> {
			if (this.projectApplicationModel != null && txtRemarks != null && !txtRemarks.isDisposed()) {
				txtRemarks.setText(text);
				updateRemarksWidget();
			}
		}, (message) -> {
			if (this.projectApplicationModel != null && txtRemarks != null && !txtRemarks.isDisposed()) {
				LANG_ENUM langEnum = XMAdminUtil.getInstance().getCurrentLocaleEnum();
				return this.projectApplicationModel.getRemarks(langEnum) == null ? CommonConstants.EMPTY_STR
						: this.projectApplicationModel.getRemarks(langEnum);
			}
			return CommonConstants.EMPTY_STR;
		});
		registry.register((text) -> {
			if (txtPosition != null && !txtPosition.isDisposed()) {
				txtPosition.setText(text);
				updatePosition();
			}
		}, (message) -> {
			if (projectApplicationModel != null && txtPosition != null && !txtPosition.isDisposed()) {
				return this.projectApplicationModel.getPosition() == null ? CommonConstants.EMPTY_STR
						: this.projectApplicationModel.getPosition();
			}
			return CommonConstants.EMPTY_STR;
		});

	}

	/**
	 * Gets the updated widget text.
	 *
	 * @param message
	 *            {@link String}
	 * @param control
	 *            {@link Control}
	 * @return the updated widget text
	 */
	private String getUpdatedWidgetText(String message, Control control) {
		control.requestLayout();
		control.getParent().redraw();
		control.getParent().getParent().update();
		control.getParent().getParent().getParent().update();
		return message;
	}

	/**
	 * Gets the old model.
	 *
	 * @return the old model
	 */
	public ProjectApplication getOldModel() {
		return oldModel;
	}

	/**
	 * Sets the old model.
	 *
	 * @param oldModel
	 *            the new old model
	 */
	public void setOldModel(ProjectApplication oldModel) {
		this.oldModel = oldModel;
	}

	/**
	 * Method for Open desc dialog.
	 *
	 * @param shell
	 *            {@link Shell}
	 */
	private void openDescDialog(final Shell shell) {
		if (projectApplicationModel == null) {
			return;
		}
		/*if (projectApplicationModel.getOperationMode() != CommonConstants.OPERATIONMODE.VIEW) {
			String text = txtDesc.getText();
			projectApplicationModel.setDescription(LANG_ENUM.ENGLISH, XMSystemUtil.isEmpty(this.projectApplicationModel.getDescription(LANG_ENUM.ENGLISH)) ? 
					text : this.projectApplicationModel.getDescription(LANG_ENUM.ENGLISH));
			projectApplicationModel.setDescription(LANG_ENUM.GERMAN, XMSystemUtil.isEmpty(this.projectApplicationModel.getDescription(LANG_ENUM.GERMAN)) ? 
					text : this.projectApplicationModel.getDescription(LANG_ENUM.GERMAN));
		}*/

		Map<LANG_ENUM, String> obModelMap = new HashMap<>();
		obModelMap.put(LANG_ENUM.ENGLISH, this.projectApplicationModel.getDescription(LANG_ENUM.ENGLISH));
		obModelMap.put(LANG_ENUM.GERMAN, this.projectApplicationModel.getDescription(LANG_ENUM.GERMAN));
		boolean isEditable = txtDesc.getEditable();

		ControlModel controlModel = new ControlModel(this.messages.objectDescriptionLabel, obModelMap,
				ProjectApplication.DESCRIPTION_LIMIT, false, isEditable);
		controlModel.initDefaultLabels(this.messages);
		final XMAdminLangTextDialog dialogArea = new XMAdminLangTextDialog(shell, controlModel);
		int retVal = dialogArea.open();
		if (retVal == IDialogConstants.OK_ID) {
			Map<LANG_ENUM, String> descriptionMap = this.projectApplicationModel.getDescriptionMap();
			descriptionMap.put(LANG_ENUM.ENGLISH, controlModel.getObjectModel(LANG_ENUM.ENGLISH));
			descriptionMap.put(LANG_ENUM.GERMAN, controlModel.getObjectModel(LANG_ENUM.GERMAN));
		}
	}

	/**
	 * Method for Open name dialog.
	 *
	 * @param shell
	 *            {@link Shell}
	 */
	private void openNameDialog(final Shell shell) {
		if (projectApplicationModel == null) {
			return;
		}
		/*if (projectApplicationModel.getOperationMode() != CommonConstants.OPERATIONMODE.VIEW) {
			String text = txtName.getText();
			projectApplicationModel.setName(LANG_ENUM.ENGLISH, XMSystemUtil.isEmpty(this.projectApplicationModel.getName(LANG_ENUM.ENGLISH)) ? 
					text : this.projectApplicationModel.getName(LANG_ENUM.ENGLISH));
			projectApplicationModel.setName(LANG_ENUM.GERMAN, XMSystemUtil.isEmpty(this.projectApplicationModel.getName(LANG_ENUM.GERMAN)) ? 
					text : this.projectApplicationModel.getName(LANG_ENUM.GERMAN));
		}*/
		Map<LANG_ENUM, String> obModelMap = new HashMap<>();
		obModelMap.put(LANG_ENUM.ENGLISH, this.projectApplicationModel.getName(LANG_ENUM.ENGLISH));
		obModelMap.put(LANG_ENUM.GERMAN, this.projectApplicationModel.getName(LANG_ENUM.GERMAN));
		boolean isEditable = txtName.getEditable();

		ControlModel controlModel = new ControlModel(this.messages.objectNameLabel, obModelMap,
				ProjectApplication.NAME_LIMIT, false, isEditable);
		controlModel.initDefaultLabels(this.messages);
		final XMAdminLangTextDialog dialogArea = new XMAdminLangTextDialog(shell, controlModel, messages);
		int retVal = dialogArea.open();
		if (retVal == IDialogConstants.OK_ID) {
			Map<LANG_ENUM, String> nameMap = this.projectApplicationModel.getNameMap();
			nameMap.put(LANG_ENUM.ENGLISH, controlModel.getObjectModel(LANG_ENUM.ENGLISH));
			nameMap.put(LANG_ENUM.GERMAN, controlModel.getObjectModel(LANG_ENUM.GERMAN));
		}
	}

	/**
	 * Method for Open remarks dialog.
	 *
	 * @param shell
	 *            {@link Shell}
	 */
	private void openRemarksDialog(final Shell shell) {
		if (projectApplicationModel == null) {
			return;
		}
		if (projectApplicationModel.getOperationMode() != CommonConstants.OPERATIONMODE.VIEW) {
			String text = txtRemarks.getText();
			LANG_ENUM currentLocaleEnum = XMAdminUtil.getInstance().getCurrentLocaleEnum();
			projectApplicationModel.setRemarks(currentLocaleEnum, text);
		}

		Map<LANG_ENUM, String> obModelMap = new HashMap<>();
		obModelMap.put(LANG_ENUM.ENGLISH, this.projectApplicationModel.getRemarks(LANG_ENUM.ENGLISH));
		obModelMap.put(LANG_ENUM.GERMAN, this.projectApplicationModel.getRemarks(LANG_ENUM.GERMAN));
		boolean isEditable = txtRemarks.getEditable();

		ControlModel controlModel = new ControlModel(this.messages.objectRemarkLabel, obModelMap,
				ProjectApplication.REMARK_LIMIT, false, isEditable);
		controlModel.initDefaultLabels(this.messages);
		final XMAdminLangTextAreaDialog dialogArea = new XMAdminLangTextAreaDialog(shell, controlModel);
		int retVal = dialogArea.open();
		if (retVal == IDialogConstants.OK_ID) {
			Map<LANG_ENUM, String> notesMap = this.projectApplicationModel.getRemarksMap();
			notesMap.put(LANG_ENUM.ENGLISH, controlModel.getObjectModel(LANG_ENUM.ENGLISH));
			notesMap.put(LANG_ENUM.GERMAN, controlModel.getObjectModel(LANG_ENUM.GERMAN));
			updateRemarksWidget();
		}
	}

	/**
	 * Method for Creates the project app operation.
	 */
	private void createProjectAppOperation() {
		try {
			ProjectAppController projectAppController = new ProjectAppController();
			ProjectApplicationsTbl projectAppVo = projectAppController.createProjectApplication(mapVOObjectWithModel());

			AdminTreeFactory instance = AdminTreeFactory.getInstance();
			String parentProjectApp = this.projectApplicationModel.getPosition();
			Map<String, IAdminTreeChild> projectApplications = instance.getProjectApplications()
					.getProjectApplications();
			ProjectApplication projectApplication = (ProjectApplication) projectApplications.get(parentProjectApp);
			boolean isParent = this.parentBtn.getSelection();

			String projectApplicationId = projectAppVo.getProjectApplicationId();
			if (!XMSystemUtil.isEmpty(projectApplicationId)) {
				this.projectApplicationModel.setProjectApplicationId(projectApplicationId);
				Collection<ProjectAppTranslationTbl> projectAppTranslationTblCollection = projectAppVo
						.getProjectAppTranslationTblCollection();
				for (ProjectAppTranslationTbl projectAppTranslationTbl : projectAppTranslationTblCollection) {
					String projectAppTranslationId = projectAppTranslationTbl.getProjectAppTranslationId();
					LanguagesTbl languageCode = projectAppTranslationTbl.getLanguageCode();
					LANG_ENUM langEnum = LANG_ENUM.getLangEnum(languageCode.getLanguageCode());
					this.projectApplicationModel.setTranslationId(langEnum, projectAppTranslationId);
				}

				// Attach model to old model
				if (oldModel == null && (isParent || parentProjectApp == null
						|| Arrays.asList(CommonConstants.Application.POSITIONS).contains(parentProjectApp))) { // Attach this to tree
					setOldModel(projectApplicationModel.deepCopyProjectApplication(false, null));
					instance.getProjectApplications().add(projectApplicationId, getOldModel());
				} else if (oldModel == null && parentProjectApp != null) {
					projectApplicationChildModel = new ProjectApplicationChild(projectApplicationId,
							this.projectApplicationModel.getName(),
							this.projectApplicationModel.getDescription(),
							projectApplicationModel.getNameMap(), projectApplicationModel.isActive(),
							projectApplicationModel.getDescriptionMap(), projectApplicationModel.getRemarksMap(),
							projectApplicationModel.getIcon(), projectApplicationModel.isParent(),
							projectApplicationModel.isSingleton(), projectApplicationModel.getPosition(),
							projectApplicationModel.getBaseApplicationId(), projectApplicationModel.getOperationMode());
					projectApplicationChildModel.setTranslationIdMap(projectApplicationModel.getTranslationIdMap());
					setOldModel(projectApplicationChildModel.deepCopyProjectApplicationChild(false, null));
					projectApplication.add(projectApplicationChildModel.getProjectApplicationId(), getOldModel());
				}
				this.dirty.setDirty(false);
				AdminTreeviewer adminTree = XMAdminUtil.getInstance().getAdminTree();
				adminTree.refresh(true);

				XMAdminUtil.getInstance()
				.updateLogFile(messages.projectApplicationObject + " " + "'"
						+ this.projectApplicationModel.getName() + "'" + " " + messages.objectCreate,
						MessageType.SUCCESS);
				
				adminTree.setSelection(new StructuredSelection(instance.getApplications()), true);
				TreePath[] selectionPaths = adminTree.getStructuredSelection().getPaths();
				if (selectionPaths != null && selectionPaths.length > 0) {
					adminTree.setExpandedState(selectionPaths[0], true);
				}

				adminTree.setSelection(new StructuredSelection(instance.getProjectApplications()), true);
				selectionPaths = adminTree.getStructuredSelection().getPaths();
				if (selectionPaths != null && selectionPaths.length > 0) {
					adminTree.setExpandedState(selectionPaths[0], true);
				}
				
				if(!getOldModel().isParent()){
					adminTree.setSelection(new StructuredSelection(getOldModel().getParent()), true);
					selectionPaths = adminTree.getStructuredSelection().getPaths();
					if (selectionPaths != null && selectionPaths.length > 0) {
						adminTree.setExpandedState(selectionPaths[0], true);
					}
				}
				adminTree.setSelection(new StructuredSelection(getOldModel()), true);
			}
		} catch (UnauthorizedAccessException e) {
			CustomMessageDialog.openError(this.getShell(), messages.objectPermissionDialogTitle,
					messages.objectPermissionDialogMsg);
		} catch (Exception e) {
			if (e instanceof ResourceAccessException) {
				CustomMessageDialog.openError(Display.getCurrent().getActiveShell(), messages.errorDialogTitile,
						messages.serverNotReachable);
			}
			LOGGER.warn("Unable to Save data ! " + e);
		}
	}

	/**
	 * Method for Change project app operation.
	 */
	private void changeProjectAppOperation() {
		try {
			ProjectAppController projectAppController = new ProjectAppController();
			AdminTreeFactory instance = AdminTreeFactory.getInstance();
			boolean isUpdated = projectAppController.updateProjectApplication(mapVOObjectWithModel());
			if (isUpdated) {
				String oldPosition = getOldModel().getPosition();
				String newPostion = projectApplicationModel.getPosition();
				boolean isOldPosConstant = false;
				boolean isNewPosConstant = false;
				setOldModel(projectApplicationModel.deepCopyProjectApplication(true, getOldModel()));
				if (!oldPosition.equals(newPostion)) {
					for (String positionType : CommonConstants.Application.POSITIONS) {
						if (oldPosition.equalsIgnoreCase(positionType)) {
							isOldPosConstant = true;
						}
					}
					for (String positionType : CommonConstants.Application.POSITIONS) {
						if (newPostion.equalsIgnoreCase(positionType)) {
							isNewPosConstant = true;
						}
					}
					if (isNewPosConstant && !isOldPosConstant) {
						ProjectApplications projectApplications = instance.getProjectApplications();
						Map<String, IAdminTreeChild> projectApplicationsChild = projectApplications
								.getProjectApplications();
						ProjectApplication projectApplication = (ProjectApplication) projectApplicationsChild
								.get(oldPosition);
						projectApplication.getProjectApplicationChildren()
								.remove(this.projectApplicationModel.getProjectApplicationId());
						if (ProjectApplicationChild.class.getSimpleName().equals(getOldModel().getClass().getSimpleName())) {
							ProjectApplication projectApp = new ProjectApplication(projectApplicationModel.getProjectApplicationId(),
									projectApplicationModel.getName(), projectApplicationModel.getDescription(), projectApplicationModel.getNameMap(),
									projectApplicationModel.isActive(), projectApplicationModel.getDescriptionMap(),
									projectApplicationModel.getRemarksMap(), projectApplicationModel.getIcon(), projectApplicationModel.isParent(),
									projectApplicationModel.isSingleton(), newPostion,projectApplicationModel.getBaseApplicationId(),
									projectApplicationModel.getOperationMode());
							projectApp.setTranslationIdMap(projectApplicationModel.getTranslationIdMap());
							setOldModel(projectApp.deepCopyProjectApplication(false, null));
						}
						projectApplications.add(getOldModel().getProjectApplicationId(), getOldModel());

					} else if (!isNewPosConstant && isOldPosConstant) {
						ProjectApplications projectApplications = instance.getProjectApplications();
						Map<String, IAdminTreeChild> projectApplicationsChild = projectApplications
								.getProjectApplications();
						projectApplicationsChild.remove(this.projectApplicationModel.getProjectApplicationId());
						ProjectApplication projectApplication = (ProjectApplication) projectApplicationsChild
								.get(newPostion);
						projectApplicationChildModel = new ProjectApplicationChild(this.projectApplicationModel.getProjectApplicationId(),
								this.projectApplicationModel.getName(),
								this.projectApplicationModel.getDescription(),
								this.projectApplicationModel.getNameMap(), this.projectApplicationModel.isActive(),
								this.projectApplicationModel.getDescriptionMap(), this.projectApplicationModel.getRemarksMap(),
								this.projectApplicationModel.getIcon(), this.projectApplicationModel.isParent(),
								this.projectApplicationModel.isSingleton(), this.projectApplicationModel.getPosition(),
								this.projectApplicationModel.getBaseApplicationId(),this.projectApplicationModel.getOperationMode());
						projectApplicationChildModel.setTranslationIdMap(this.projectApplicationModel.getTranslationIdMap());
						setOldModel(projectApplicationChildModel.deepCopyProjectApplicationChild(false, null));
						projectApplication.add(getOldModel().getProjectApplicationId(), getOldModel());
					} else if (!isNewPosConstant && !isOldPosConstant) {
						ProjectApplications projectApplications = instance.getProjectApplications();
						Map<String, IAdminTreeChild> projectApplicationsChild = projectApplications
								.getProjectApplications();
						ProjectApplication projectApplication = (ProjectApplication) projectApplicationsChild
								.get(oldPosition);
						projectApplication.getProjectApplicationChildren()
								.remove(this.projectApplicationModel.getProjectApplicationId());
						projectApplication = (ProjectApplication) projectApplicationsChild.get(newPostion);
						projectApplication.add(getOldModel().getProjectApplicationId(), getOldModel());
					}
				}

				this.projectApplicationModel.setOperationMode(CommonConstants.OPERATIONMODE.VIEW);
				setOperationMode();
				this.dirty.setDirty(false);
				final ProjectApplications projectApplications = AdminTreeFactory.getInstance().getProjectApplications();
				projectApplications.sort();
				AdminTreeviewer adminTree = XMAdminUtil.getInstance().getAdminTree();
				adminTree.refresh(true);
				
				XMAdminUtil.getInstance()
				.updateLogFile(messages.projectApplicationObject + " " + "'"
						+ this.projectApplicationModel.getName() + "'" + " " + messages.objectUpdate,
						MessageType.SUCCESS);
				
				if(!getOldModel().isParent()){
					adminTree.setSelection(new StructuredSelection(getOldModel().getParent()), true);
					TreePath[]  selectionPaths = adminTree.getStructuredSelection().getPaths();
					if (selectionPaths != null && selectionPaths.length > 0) {
						adminTree.setExpandedState(selectionPaths[0], true);
					}
				}
				adminTree.setSelection(new StructuredSelection(getOldModel()), true);
				
			}
		} catch (UnauthorizedAccessException e) {
			CustomMessageDialog.openError(this.getShell(), messages.objectPermissionDialogTitle,
					messages.objectPermissionDialogMsg);
		} catch (Exception e) {
			if (e instanceof ResourceAccessException) {
				CustomMessageDialog.openError(Display.getCurrent().getActiveShell(), messages.errorDialogTitile,
						messages.serverNotReachable);
			}
			LOGGER.warn("Unable to Save data ! " + e);
		}
	}

	/**
	 * Method for Map VO object with model.
	 *
	 * @return com.magna.xmbackend.vo.projectApplication.ProjectApplicationRequest
	 *         {@link ProjectApplicationRequest}
	 */
	private com.magna.xmbackend.vo.projectApplication.ProjectApplicationRequest mapVOObjectWithModel() {
		com.magna.xmbackend.vo.projectApplication.ProjectApplicationRequest projectApplicationRequest = new com.magna.xmbackend.vo.projectApplication.ProjectApplicationRequest();
		projectApplicationRequest.setId(this.projectApplicationModel.getProjectApplicationId());
		projectApplicationRequest.setName(this.projectApplicationModel.getName());
		projectApplicationRequest.setDescription(this.projectApplicationModel.getDescription());
		projectApplicationRequest.setIconId(this.projectApplicationModel.getIcon().getIconId());
		projectApplicationRequest.setIsSingleton(Boolean.toString(this.projectApplicationModel.isSingleton()));
		projectApplicationRequest.setStatus(
				this.projectApplicationModel.isActive() == true ? com.magna.xmbackend.vo.enums.Status.ACTIVE.name()
						: com.magna.xmbackend.vo.enums.Status.INACTIVE.name());
		projectApplicationRequest.setPosition(this.projectApplicationModel.getPosition());

		projectApplicationRequest.setBaseAppId(this.projectApplicationModel.getBaseApplicationId());
		projectApplicationRequest.setIsParent(Boolean.toString(this.projectApplicationModel.isParent()));

		List<com.magna.xmbackend.vo.projectApplication.ProjectApplicationTransulation> projectAppTransulationList = new ArrayList<>();
		LANG_ENUM[] lang_values = LANG_ENUM.values();
		for (int index = 0; index < lang_values.length; index++) {
			com.magna.xmbackend.vo.projectApplication.ProjectApplicationTransulation projectAppTransulation = new com.magna.xmbackend.vo.projectApplication.ProjectApplicationTransulation();
			projectAppTransulation.setLanguageCode(lang_values[index].getLangCode());
			projectAppTransulation.setName(this.projectApplicationModel.getName(lang_values[index]));
			projectAppTransulation.setRemarks(this.projectApplicationModel.getRemarks(lang_values[index]));
			projectAppTransulation.setDescription(this.projectApplicationModel.getDescription(lang_values[index]));
			if (this.projectApplicationModel.getOperationMode() == CommonConstants.OPERATIONMODE.CHANGE) {
				String translationId = projectApplicationModel.getTranslationId(lang_values[index]);
				projectAppTransulation
						.setId(XMSystemUtil.isEmpty(translationId) ? CommonConstants.EMPTY_STR : translationId);
			}
			projectAppTransulationList.add(projectAppTransulation);
		}

		projectApplicationRequest.setProjectApplicationTransulations(projectAppTransulationList);

		return projectApplicationRequest;
	}

	/**
	 * Method for Sets the operation mode.
	 */
	public void setOperationMode() {
		if (this.projectApplicationModel != null) {
			if (projectApplicationModel.getOperationMode() == CommonConstants.OPERATIONMODE.VIEW) {
				this.txtName.setEditable(false);
				this.txtDesc.setEditable(false);
				this.txtRemarks.setEditable(false);
				this.activeBtn.setEnabled(false);
				this.parentBtn.setEnabled(false);
				this.btnSingleton.setEnabled(false);
				this.txtBaseApplication.setEnabled(false);
				setShowButtonBar(false);
				this.toolItem.setData("editable", false);
				this.programToolItem.setData("editable", false);
				this.programAppToolItem.setData("editable", false);
			} else if (projectApplicationModel.getOperationMode() == CommonConstants.OPERATIONMODE.CREATE) {
				this.saveBtn.setEnabled(false);
				this.txtName.setEditable(true);
				this.txtDesc.setEditable(true);
				this.txtRemarks.setEditable(true);
				this.activeBtn.setEnabled(true);
				this.parentBtn.setEnabled(true);
				this.btnSingleton.setEnabled(true);
				this.txtBaseApplication.setEnabled(!this.parentBtn.getSelection());
				this.toolItem.setData("editable", true);
				this.programToolItem.setData("editable", true);
				this.programAppToolItem.setData("editable", true);
				setShowButtonBar(true);
				this.dirty.setDirty(true);
			} else if (projectApplicationModel.getOperationMode() == CommonConstants.OPERATIONMODE.CHANGE) {
				this.txtName.setEditable(true);
				this.txtDesc.setEditable(true);
				this.txtRemarks.setEditable(true);
				this.activeBtn.setEnabled(true);
				this.btnSingleton.setEnabled(!getOldModel().isParent());
				this.txtBaseApplication.setEnabled(!this.parentBtn.getSelection());
				this.toolItem.setData("editable", true);
				this.programToolItem.setData("editable", true);
				this.programAppToolItem.setData("editable", true);
				setShowButtonBar(true);
				this.dirty.setDirty(true);
				if (ProjectApplication.class.getSimpleName().equals(projectApplicationModel.getClass().getSimpleName())) {
					this.parentBtn.setEnabled(!projectApplicationModel.isParent());
				} else {
					this.parentBtn.setEnabled(false);
				}
			} else {
				this.txtName.setEditable(false);
				this.txtDesc.setEditable(false);
				this.txtRemarks.setEditable(false);
				this.activeBtn.setEnabled(false);
				this.parentBtn.setEnabled(false);
				this.btnSingleton.setEnabled(false);
				this.txtBaseApplication.setEnabled(false);
				setShowButtonBar(false);
				this.toolItem.setData("editable", false);
				this.programToolItem.setData("editable", false);
				this.programAppToolItem.setData("editable", false);
			}
		}
	}

	/**
	 * Method for Save handler.
	 */
	public void saveHandler() {
		// validate the model
		saveNameDescAndRemarks();
		if (validate()) { // check name map values
			if (projectApplicationModel.getOperationMode() == CommonConstants.OPERATIONMODE.CREATE) {
				createProjectAppOperation();
			} else if (projectApplicationModel.getOperationMode() == CommonConstants.OPERATIONMODE.CHANGE) {
				changeProjectAppOperation();
			}
		}
	}

	/**
	 * Method for Remarks.
	 */
	private void saveNameDescAndRemarks() {
		/*String nameText = txtName.getText();
		projectApplicationModel.setName(LANG_ENUM.ENGLISH, XMSystemUtil.isEmpty(this.projectApplicationModel.getName(LANG_ENUM.ENGLISH)) ? 
				nameText : this.projectApplicationModel.getName(LANG_ENUM.ENGLISH));
		projectApplicationModel.setName(LANG_ENUM.GERMAN, XMSystemUtil.isEmpty(this.projectApplicationModel.getName(LANG_ENUM.GERMAN)) ? 
				nameText : this.projectApplicationModel.getName(LANG_ENUM.GERMAN));*/
		
		/*String descText = txtDesc.getText();
		projectApplicationModel.setDescription(LANG_ENUM.ENGLISH, XMSystemUtil.isEmpty(this.projectApplicationModel.getDescription(LANG_ENUM.ENGLISH)) ? 
				descText : this.projectApplicationModel.getDescription(LANG_ENUM.ENGLISH));
		projectApplicationModel.setDescription(LANG_ENUM.GERMAN, XMSystemUtil.isEmpty(this.projectApplicationModel.getDescription(LANG_ENUM.GERMAN)) ? 
				descText : this.projectApplicationModel.getDescription(LANG_ENUM.GERMAN));*/
		
		final LANG_ENUM currentLocaleEnum = XMAdminUtil.getInstance().getCurrentLocaleEnum();
		final String remarks = txtRemarks.getText();
		projectApplicationModel.setRemarks(currentLocaleEnum, remarks);
	}

	/**
	 * Sets the dirty object.
	 *
	 * @param dirty
	 *            the new dirty object
	 */
	@Persist
	public void setDirtyObject(final MDirtyable dirty) {
		this.dirty = dirty;
	}

	/**
	 * Method for Update remarks widget.
	 */
	public void updateRemarksWidget() {

		if (this.projectApplicationModel == null) {
			return;
		}
		int operationMode = this.projectApplicationModel.getOperationMode();
		final LANG_ENUM currentLocaleEnum = XMAdminUtil.getInstance().getCurrentLocaleEnum();

		String remarkForCurLocale = this.projectApplicationModel.getRemarks(currentLocaleEnum) == null
				? CommonConstants.EMPTY_STR : this.projectApplicationModel.getRemarks(currentLocaleEnum);
		if (operationMode != CommonConstants.OPERATIONMODE.VIEW) {
			this.txtRemarks.setText(remarkForCurLocale);
			return;
		}

		if (operationMode == CommonConstants.OPERATIONMODE.VIEW) {
			if (!XMSystemUtil.isEmpty(remarkForCurLocale)) {
				this.txtRemarks.setText(remarkForCurLocale);
				return;
			}
			final String remarkEN = this.projectApplicationModel.getRemarks(LANG_ENUM.ENGLISH);
			final String remarkDE = this.projectApplicationModel.getRemarks(LANG_ENUM.GERMAN);
			if (!XMSystemUtil.isEmpty(remarkEN)) {
				this.txtRemarks.setText(remarkEN);
				return;
			}

			if (!XMSystemUtil.isEmpty(remarkDE)) {
				this.txtRemarks.setText(remarkDE);
				return;
			}
		}
	}

	/**
	 * Method for Update desc widget.
	 */
	/*public void updateDescWidget() {
		if (this.projectApplicationModel == null) {
			return;
		}
		int operationMode = this.projectApplicationModel.getOperationMode();
		final LANG_ENUM currentLocaleEnum = XMAdminUtil.getInstance().getCurrentLocaleEnum();
		String descForCurLocale = this.projectApplicationModel.getDescription(currentLocaleEnum) == null
				? CommonConstants.EMPTY_STR : this.projectApplicationModel.getDescription(currentLocaleEnum);

		if (operationMode != CommonConstants.OPERATIONMODE.VIEW) {
			this.txtDesc.setText(descForCurLocale);
			return;
		}
		if (operationMode == CommonConstants.OPERATIONMODE.VIEW) {
			if (!XMSystemUtil.isEmpty(descForCurLocale)) {
				this.txtDesc.setText(descForCurLocale);
				return;
			}
			final String descriptionEN = this.projectApplicationModel.getDescription(LANG_ENUM.ENGLISH);
			final String descriptionDE = this.projectApplicationModel.getDescription(LANG_ENUM.GERMAN);
			if (!XMSystemUtil.isEmpty(descriptionEN)) {
				this.txtDesc.setText(descriptionEN);
				return;
			}

			if (!XMSystemUtil.isEmpty(descriptionDE)) {
				this.txtDesc.setText(descriptionDE);
				return;
			}
		}
	}*/

	/**
	 * Method for Update name widget.
	 */
	/*public void updateNameWidget() {
		if (this.projectApplicationModel == null) {
			return;
		}
		int operationMode = this.projectApplicationModel.getOperationMode();
		final String nameEN = this.projectApplicationModel.getName(LANG_ENUM.ENGLISH);
		final String nameDE = this.projectApplicationModel.getName(LANG_ENUM.GERMAN);
		final LANG_ENUM currentLocaleEnum = XMAdminUtil.getInstance().getCurrentLocaleEnum();
		String nameForCurLocale = this.projectApplicationModel.getName(currentLocaleEnum) == null
				? CommonConstants.EMPTY_STR : this.projectApplicationModel.getName(currentLocaleEnum);

		if (operationMode != CommonConstants.OPERATIONMODE.VIEW) {
			//this.txtName.setText(nameForCurLocale);
			if (currentLocaleEnum == LANG_ENUM.ENGLISH) {
				GridData layoutData = (GridData) this.txtNameGermanLang.getLayoutData();
				GridData layoutData2 = (GridData) this.lblNameGerman1.getLayoutData();
				GridData layoutData3 = (GridData) this.lblNameGerman2.getLayoutData();
				layoutData.exclude = true;
				layoutData2.exclude = true;
				layoutData3.exclude = true;
				this.txtNameGermanLang.setVisible(false);
				this.lblNameGerman1.setVisible(false);
				this.lblNameGerman2.setVisible(false);

				this.txtNameGermanLang.getParent().layout(false);
				this.lblNameGerman1.getParent().layout(false);
				this.lblNameGerman2.getParent().layout(false);
			}

			if (!XMSystemUtil.isEmpty(nameEN)) {
				this.txtNameGermanLang.setText(nameEN);
				return;
			} else if (!XMSystemUtil.isEmpty(nameDE)) {
				this.txtNameGermanLang.setText(nameDE);
				return;
			}
			return;
		}
		if (operationMode == CommonConstants.OPERATIONMODE.VIEW) {
			if (!XMSystemUtil.isEmpty(nameForCurLocale)) {
				this.txtName.setText(nameForCurLocale);
				return;
			}

			if (!XMSystemUtil.isEmpty(nameEN)) {
				this.txtName.setText(nameEN);
				return;
			}

			if (!XMSystemUtil.isEmpty(nameDE)) {
				this.txtName.setText(nameDE);
				return;
			}
		}
	}*/

	/**
	 * Method for Update position.
	 */
	public void updatePosition() {
		if (this.projectApplicationModel == null) {
			return;
		}
		AdminTreeFactory instance = AdminTreeFactory.getInstance();
		if (instance != null) {

			String parent = this.projectApplicationModel.getPosition();
			if (!XMSystemUtil.isEmpty(parent)) {
				for (String positionType : CommonConstants.Application.POSITIONS) {
					if (parent.equalsIgnoreCase(positionType)) {
						this.txtPosition.setText(parent);
						return;
					}
				}
				final Map<String, IAdminTreeChild> projectApplications = instance.getProjectApplications().getProjectApplications();
				final ProjectApplication userApplication = (ProjectApplication) projectApplications.get(parent);
				final String name = userApplication.getName();
				if (!XMSystemUtil.isEmpty(name)) {
					this.txtPosition.setText(name);
				}
			}
		}
	}

	/**
	 * Method for Update base application.
	 */
	public void updateBaseApplication() {
		if (this.projectApplicationModel == null) {
			return;
		}
		AdminTreeFactory instance = AdminTreeFactory.getInstance();
		if (instance != null) {
			String applicationComboId = this.projectApplicationModel.getBaseApplicationId();
			if (!XMSystemUtil.isEmpty(applicationComboId)) {
				Map<String, IAdminTreeChild> baseApplications = instance.getBaseApplications().getBaseApplications();
				BaseApplication baseApplication = (BaseApplication) baseApplications.get(applicationComboId);
				String name = baseApplication.getName();
				if (!XMSystemUtil.isEmpty(name)) {
					this.txtBaseApplication.setText(name);
				} 
			} else {
				this.txtBaseApplication.setText(CommonConstants.EMPTY_STR);
			}
		}
	}

	/**
	 * Method for Cancel handler.
	 */
	public void cancelHandler() {
		if (projectApplicationModel == null) {
			dirty.setDirty(false);
			return;
		}
		String projectAppId = CommonConstants.EMPTY_STR;
		int operationMode = this.projectApplicationModel.getOperationMode();
		ProjectApplication oldModel = getOldModel();
		if (oldModel != null) {
			projectAppId = oldModel.getProjectApplicationId();
		}
		setProjectApplicationModel(null);
		setOldModel(null);
		this.saveBtn.setEnabled(true);
		final AdminTreeviewer adminTree = XMAdminUtil.getInstance().getAdminTree();
		AdminTreeFactory instance = AdminTreeFactory.getInstance();
		final ProjectApplications projectApps = instance.getProjectApplications();
		dirty.setDirty(false);
		if (operationMode == CommonConstants.OPERATIONMODE.CHANGE) {
			final IStructuredSelection selection = (IStructuredSelection) adminTree.getSelection();
			final Object firstElement = selection.getFirstElement();
			Map<String, IAdminTreeChild> projectApplications = projectApps.getProjectApplications();

			if (firstElement != null) {
				if (firstElement.equals(projectApplications.get(projectAppId))) {
					adminTree.setSelection(new StructuredSelection(projectApplications.get(projectAppId)), true);
				} else {
					final Collection<IAdminTreeChild> projectAppsCollection = (projectApps).getProjectAppCollection();
					for (IAdminTreeChild iAdminTreeChild : projectAppsCollection) {
						if (iAdminTreeChild instanceof ProjectApplication) {
							ProjectApplication projectApplication = (ProjectApplication) iAdminTreeChild;
							if (projectApplication.isParent()) {
								ProjectApplication childProjectApp = (ProjectApplication) projectApplication
										.getProjectApplicationChildren().get(projectAppId);
								if ((childProjectApp != null)) {
									if (childProjectApp.equals(firstElement)) {
										adminTree.setSelection(new StructuredSelection(childProjectApp), true);
										break;
									}
								}
							}
						}
					}
				}
			}
		} else {
			if (!adminTree.getExpandedState(instance.getApplications())) {
				adminTree.setSelection(new StructuredSelection(instance.getApplications()), true);
			} else {
				adminTree.setSelection(new StructuredSelection(projectApps), true);
			}

		}
	}

	/**
	 * set the side model from selection.
	 */
	public void setProjectApplication() {
		try {
			final AdminTreeviewer adminTree = XMAdminUtil.getInstance().getAdminTree();
			final Object selectionObj = adminTree.getSelection();
			if (selectionObj instanceof IStructuredSelection) {
				Object firstElement = ((IStructuredSelection) selectionObj).getFirstElement();
				
				if (ProjectApplication.class.getSimpleName().equals(firstElement.getClass().getSimpleName())) {
					setOldModel((ProjectApplication) firstElement);
					ProjectApplication rightHandObject = getOldModel().deepCopyProjectApplication(false, null);
					setProjectApplicationModel(rightHandObject);
				} else {
					setOldModel((ProjectApplicationChild) firstElement);
					ProjectApplicationChild rightHandObject = ((ProjectApplicationChild) getOldModel())
							.deepCopyProjectApplicationChild(false, null);
					setProjectApplicationModel(rightHandObject);

				}
				registerMessages(this.registry);
				bindValues();
				setOperationMode();
				updateBaseApplication();
				initDefaultValue();
				updatePosition();

			}
		} catch (Exception e) {
			LOGGER.warn("Unable to set  ProjectApplication model selection ! " + e);
		}
	}

	/**
	 * Sets the model.
	 *
	 * @param projectApplication
	 *            the new model
	 */
	public void setModel(ProjectApplication projectApplication) {
		try {
			setOldModel(null);
			setProjectApplicationModel(projectApplication);
			registerMessages(this.registry);
			bindValues();
			setOperationMode();
			//updateDescWidget();
			//updateNameWidget();
			updateRemarksWidget();
			updateBaseApplication();
			updatePosition();
			initDefaultValue();
		} catch (Exception e) {
			LOGGER.warn("Unable to set project  model ! " + e);
		}
	}

	private void initDefaultValue() {
		Event event = new Event();
		event.data = PROGRAMMATICALLY;
		this.parentBtn.notifyListeners(SWT.Selection, event);
	}

}
