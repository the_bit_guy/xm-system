package com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.users;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.stream.Collectors;

import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.projects.Project;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.refs.RelationObj;

/**
 * The Class UserProjects.
 * 
 * @author subash.janarthanan
 * 
 */
public class UserProjects implements IAdminTreeChild {
	/** The parent. */
	private IAdminTreeChild parent;

	/** The project children. */
	private Map<String, IAdminTreeChild> projectChildren;

	/**
	 * Instantiates a new user projects.
	 */
	public UserProjects(final IAdminTreeChild parent) {
		this.parent = parent;
		this.projectChildren = new LinkedHashMap<>();
	}

	/**
	 * Add.
	 *
	 * @param projectId
	 *            the project id
	 * @param child
	 *            the child
	 * @return the i admin tree child
	 */
	public IAdminTreeChild add(final String projectId, final IAdminTreeChild child) {
		child.setParent(this);
		IAdminTreeChild returnVal = this.projectChildren.put(projectId, child);
		if (child instanceof RelationObj && ((RelationObj) child).getRefObject() instanceof Project) {
			sort();
		}
		return returnVal;
	}

	/**
	 * Remove.
	 *
	 * @param projectId
	 *            the project id
	 * @return the i admin tree child
	 */
	public IAdminTreeChild remove(final String projectId) {
		return this.projectChildren.remove(projectId);
	}

	/**
	 * Method for Removes the all.
	 */
	public void removeAll() {
		this.projectChildren.clear();
	}
	
	/**
	 * Gets the projects collection.
	 *
	 * @return the projects collection
	 */
	public Collection<IAdminTreeChild> getUserProjectsCollection() {
		return this.projectChildren.values();
	}

	/**
	 * Gets the projects children.
	 *
	 * @return the projects children
	 */
	public Map<String, IAdminTreeChild> getUserProjectsChildren() {
		return projectChildren;
	}
	/**
	 * Returns null as its parent
	 * @see com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild#getParent()
	 */
	@Override
	public IAdminTreeChild getParent() {
		return this.parent;
	}

	/**
	 * @see com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild#setParent(com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild)
	 */
	@Override
	public void setParent(final IAdminTreeChild parent) {
		this.parent = parent;
	}
	
	/**
	 * Sort.
	 */
	public void sort() {
		final LinkedHashMap<String, IAdminTreeChild> collect = this.projectChildren.entrySet().stream().sorted(
				(e1, e2) -> ((Project) (((RelationObj) e1.getValue()).getRefObject())).getName()
				.compareTo(((Project) (((RelationObj) e2.getValue()).getRefObject())).getName()))
				.collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e1, LinkedHashMap::new));
		this.projectChildren = collect;
	}

}
