 
package com.magna.xmsystem.xmadmin.ui.handlers;

import javax.inject.Inject;

import org.eclipse.e4.core.di.annotations.CanExecute;
import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.ui.model.application.MApplication;
import org.eclipse.e4.ui.model.application.ui.basic.MPart;
import org.eclipse.e4.ui.workbench.modeling.EModelService;
import org.eclipse.swt.widgets.Control;

import com.magna.xmsystem.xmadmin.ui.parts.InformationPart;
import com.magna.xmsystem.xmadmin.ui.parts.objexp.ObjectExpPage;
import com.magna.xmsystem.xmadmin.util.CommonConstants;

public class InvertMenuHandler {
	
	/** Inject of {@link EModelService}. */
	@Inject
	private EModelService modelService;

	/** Inject of {@link MApplication}. */
	@Inject
	private MApplication application;
	
	@Execute
	public void execute() {
		final MPart mPart = (MPart) this.modelService.find(CommonConstants.PART_ID.INFORMATION_PART_ID, application);
		final InformationPart rightSidePart2 = (InformationPart) mPart.getObject();
		final ObjectExpPage objectExpPage = rightSidePart2.getObjectExpPartUI();
		objectExpPage.invertSelection();
		
	}
		
	@CanExecute
	public boolean canExecute(MApplication application, EModelService eModelService) {

		final MPart mPart = (MPart) this.modelService.find(CommonConstants.PART_ID.INFORMATION_PART_ID, application);
		final InformationPart rightSidePart = (InformationPart) mPart.getObject();
		final ObjectExpPage objectExpPage = rightSidePart.getObjectExpPartUI();
		Control control = rightSidePart.getCompLayout().topControl;
		if (control instanceof ObjectExpPage && objectExpPage.isInvertEnable()) {
			return true;
		}
		return false;
	}
}