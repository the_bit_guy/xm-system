package com.magna.xmsystem.xmadmin.ui.parts.application.startapplication;

import org.eclipse.jface.layout.GridDataFactory;
import org.eclipse.jface.layout.GridLayoutFactory;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.ScrolledComposite;
import org.eclipse.swt.events.ControlAdapter;
import org.eclipse.swt.events.ControlEvent;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Link;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.widgets.ToolBar;
import org.eclipse.swt.widgets.ToolItem;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.magna.xmsystem.dependencies.utils.XMSystemUtil;
import com.magna.xmsystem.ui.controls.widgets.CustomDateTime;
import com.magna.xmsystem.xmadmin.ui.dialogs.lang.XMAdminLangTextAreaDialog;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.startapplicationmodel.StartApplication;
import com.magna.xmsystem.xmadmin.util.XMAdminUtil;

/**
 * Class for Start application composite UI.
 *
 * @author Deepak upadhyay
 */
public abstract class StartApplicationCompositeUI extends Composite {

	/** The Constant LOGGER. */
	private static final Logger LOGGER = LoggerFactory.getLogger(StartApplicationCompositeUI.class);

	/** Member variable 'cancel btn' for {@link Button}. */
	protected Button saveBtn, cancelBtn;

	/** Member variable 'grp start application' for {@link Group}. */
	protected Group grpStartApplication;

	/** Member variable 'lbl name' for {@link Label}. */
	protected Label lblName;

	/** Member variable 'lbl descrition' for {@link Label}. */
	protected Label lblDescrition;

	/** Member variable 'lbl active' for {@link Label}. */
	protected Label lblActive;
	
	/** Member variable 'lbl Is Start message' for {@link Label}. */
	protected Label lblIsMessage;
	
	/** The lbl expiry date. */
	protected Label lblExpiryDate;

	/**
	 * Member variable for label Application
	 */
	protected Label lblApplication;

	/** Member variable 'lbl symbol' for {@link Label}. */
	protected Label lblSymbol;

	/** Member variable 'active btn' for {@link Button}. */
	protected Button activeBtn;
	
	/** Member variable 'start message btn' for {@link Button}. */
	protected Button isMessageBtn;
	
	/** The cdt expiry date. */
	protected CustomDateTime cdtExpiryDate;

	/** Member variable 'txt symbol' for {@link Text}. */
	protected Text txtSymbol;

	/** Member variable 'parent shell' for {@link Shell}. */
	protected Shell parentShell;

	/** Member variable 'tool item' for {@link ToolItem}. */
	protected ToolItem toolItem;

	/** Member variable 'combo position' for {@link Combo}. */
	protected Combo comboPosition;

	/** Member variable 'txt base application' for {@link Text}. */
	protected Text txtBaseApplication;

	/** Member variable 'txt desc' for {@link Text}. */
	protected Text txtDesc;

	/** Member variable 'txt name' for {@link Text}. */
	protected Text txtName;

	/** Member variable 'desc translate link' for {@link Link}. */
	protected Link descTranslateLink;

	/** Member variable 'remarks label' for {@link Label}. */
	protected Label remarksLabel;

	/** Member variable 'txt remarks' for {@link Text}. */
	protected Text txtRemarks;

	/** Member variable 'remarks translate link' for {@link Link}. */
	protected Link remarksTranslateLink;

	/** Member variable 'lbl remarks count' for {@link Label}. */
	protected Label lblRemarksCount;

	/** The program app tool item. */
	protected ToolItem programAppToolItem;

	/**
	 * Constructor for StartApplicationCompositeUI Class.
	 *
	 * @param parent
	 *            {@link Composite}
	 * @param style
	 *            {@link int}
	 */
	public StartApplicationCompositeUI(final Composite parent, final int style) {
		super(parent, style);
		this.initGUI();
		setShowButtonBar(false);
	}

	/**
	 * Method for Inits the GUI.
	 */
	private void initGUI() {
		try {
			GridLayoutFactory.fillDefaults().applyTo(this);
			this.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));

			this.grpStartApplication = new Group(this, SWT.NONE);
			this.grpStartApplication.setBackgroundMode(SWT.INHERIT_FORCE);
			GridLayoutFactory.fillDefaults().numColumns(1).applyTo(this.grpStartApplication);
			GridDataFactory.fillDefaults().grab(true, true).align(SWT.FILL, SWT.FILL).applyTo(this.grpStartApplication);
			final ScrolledComposite scrolledComposite = XMAdminUtil.getInstance().createScrolledComposite(this.grpStartApplication);
			scrolledComposite.setBackgroundMode(SWT.INHERIT_FORCE);

			final Composite widgetContainer = new Composite(scrolledComposite, SWT.NONE);
			final GridLayout widgetContLayout = new GridLayout(3, false);

			widgetContainer.setLayout(widgetContLayout);
			widgetContainer.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));

			this.lblName = new Label(widgetContainer, SWT.NONE);
			this.txtName = new Text(widgetContainer, SWT.BORDER);
			this.txtName.setTextLimit(StartApplication.NAME_LIMIT);
			GridDataFactory.fillDefaults().grab(true, false).align(SWT.FILL, SWT.CENTER).span(2, 1).applyTo(this.txtName);

			this.lblActive = new Label(widgetContainer, SWT.NONE);
			this.activeBtn = new Button(widgetContainer, SWT.CHECK);

			GridDataFactory.fillDefaults().grab(false, false).align(SWT.LEFT, SWT.CENTER)
					.applyTo(this.lblActive);
			GridDataFactory.fillDefaults().grab(false, false).align(SWT.LEFT, SWT.CENTER).span(2, 1)
					.applyTo(this.activeBtn);
			
			this.lblIsMessage = new Label(widgetContainer, SWT.NONE);
			this.isMessageBtn = new Button(widgetContainer, SWT.CHECK);

			GridDataFactory.fillDefaults().grab(false, false).align(SWT.LEFT, SWT.CENTER)
					.applyTo(this.lblIsMessage);
			GridDataFactory.fillDefaults().grab(true, false).align(SWT.LEFT, SWT.CENTER).span(2, 1)
					.applyTo(this.isMessageBtn);
			
			this.lblExpiryDate = new Label(widgetContainer, SWT.NONE);
			this.cdtExpiryDate = new CustomDateTime(widgetContainer);

			GridDataFactory.fillDefaults().grab(false, false).align(SWT.LEFT, SWT.CENTER)
					.applyTo(this.lblExpiryDate);
			GridDataFactory.fillDefaults().grab(true, false).align(SWT.FILL, SWT.CENTER).span(1, 1)
					.applyTo(this.cdtExpiryDate);
			
			new Label(widgetContainer, SWT.NONE);

			this.lblDescrition = new Label(widgetContainer, SWT.NONE);
			this.txtDesc = new Text(widgetContainer, SWT.BORDER);
			this.txtDesc.setTextLimit(StartApplication.DESC_LIMIT);
			this.txtDesc.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));
			this.descTranslateLink = new Link(widgetContainer, SWT.NONE);

			this.lblSymbol = new Label(widgetContainer, SWT.NONE);
			this.txtSymbol = new Text(widgetContainer, SWT.BORDER);
			this.txtSymbol.setEditable(false);
			GridDataFactory.fillDefaults().grab(true, false).span(1, 1).align(SWT.FILL, SWT.CENTER)
					.applyTo(this.txtSymbol);
			final ToolBar toolbar = new ToolBar(widgetContainer, SWT.NONE);
			this.toolItem = new ToolItem(toolbar, SWT.FLAT);
			this.toolItem
					.setImage(XMSystemUtil.getInstance().getImage(this.getClass(), "icons/16x16/browse.png"));
			toolbar.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false));

			this.remarksLabel = new Label(widgetContainer, SWT.NONE);
			this.remarksTranslateLink = new Link(widgetContainer, SWT.NONE);
			this.remarksTranslateLink.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, true, false, 1, 1));

			this.lblRemarksCount = new Label(widgetContainer, SWT.BORDER | SWT.CENTER);
			final GridData gridDataRemarksCount = new GridData(SWT.LEFT, SWT.CENTER, false, false);
			gridDataRemarksCount.widthHint = 70;
			this.lblRemarksCount.setLayoutData(gridDataRemarksCount);

			this.txtRemarks = new Text(widgetContainer,
					SWT.BORDER | SWT.MULTI | SWT.WRAP | SWT.H_SCROLL | SWT.V_SCROLL);
			final GridData gridData = new GridData(SWT.FILL, SWT.FILL, true, true);
			gridData.horizontalSpan = 3;
			this.txtRemarks.setLayoutData(gridData);
			this.txtRemarks.setTextLimit(StartApplication.REMARK_LIMIT);
			this.txtRemarks.setEditable(false);
			this.lblRemarksCount.setText(XMAdminLangTextAreaDialog.LIMIT_COUNT_PADDING
					+ this.txtRemarks.getText().length() + XMAdminLangTextAreaDialog.SLASH
					+ StartApplication.REMARK_LIMIT + XMAdminLangTextAreaDialog.LIMIT_COUNT_PADDING);

			this.lblApplication = new Label(widgetContainer, SWT.NONE);
			this.txtBaseApplication = new Text(widgetContainer, SWT.BORDER);
			GridDataFactory.fillDefaults().grab(true, false).span(1, 1).indent(2, 0).align(SWT.FILL, SWT.CENTER)
			.applyTo(this.txtBaseApplication);
			this.txtBaseApplication.setEditable(false);
			
			this.programAppToolItem = new ToolItem(new ToolBar(widgetContainer, SWT.NONE), SWT.FLAT);
			this.programAppToolItem.setText(" ... ");

			toolbar.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false));

			final Composite buttonBarComp = new Composite(this.grpStartApplication, SWT.NONE);
			final GridLayout btnBarCompLayout = new GridLayout(2, true);
			btnBarCompLayout.marginRight = 0;
			btnBarCompLayout.marginLeft = 0;
			btnBarCompLayout.marginTop = 0;
			btnBarCompLayout.marginBottom = 0;
			btnBarCompLayout.marginWidth = 0;
			buttonBarComp.setLayout(btnBarCompLayout);
			buttonBarComp.setLayoutData(new GridData(SWT.FILL, SWT.DEFAULT, true, false, 3, 1));
			createButtonBar(buttonBarComp);
			
			scrolledComposite.setContent(widgetContainer);
			scrolledComposite.setSize(widgetContainer.getSize());
			scrolledComposite.setExpandVertical(true);
			scrolledComposite.setExpandHorizontal(true);
			scrolledComposite.update();

			scrolledComposite.addControlListener(new ControlAdapter() {
				public void controlResized(final ControlEvent e) {
					Rectangle rectangle = scrolledComposite.getClientArea();
					scrolledComposite.setMinSize(widgetContainer.computeSize(rectangle.width, SWT.DEFAULT));
				}
			});
		} catch (Exception ex) {
			LOGGER.error("Unable to crete UI elements", ex); //$NON-NLS-1$
		}
	}

	/**
	 * Method for Creates the button bar.
	 *
	 * @param buttonBarComp
	 *            {@link Composite}
	 */
	private void createButtonBar(final Composite buttonBarComp) {
		this.saveBtn = new Button(buttonBarComp, SWT.NONE);
		this.saveBtn.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, true, false));

		this.cancelBtn = new Button(buttonBarComp, SWT.NONE);
		this.cancelBtn.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, true, false));
	}

	/**
	 * Gets the start app.
	 *
	 * @return the start app
	 */
	public Group getStartApp() {
		return grpStartApplication;
	}

	/**
	 * Gets the lbl name.
	 *
	 * @return the lbl name
	 */
	public Label getLblName() {
		return lblName;
	}

	/**
	 * Gets the parent shell.
	 *
	 * @return the parent shell
	 */
	public Shell getParentShell() {
		return parentShell;
	}

	/**
	 * Gets the lbl descrition.
	 *
	 * @return the lbl descrition
	 */
	public Label getLblDescrition() {
		return lblDescrition;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.swt.widgets.Widget#dispose()
	 */
	@Override
	public void dispose() {
		super.dispose();
	}

	/**
	 * Sets the show button bar.
	 *
	 * @param showButtonBar
	 *            the new show button bar
	 */
	public void setShowButtonBar(final boolean showButtonBar) {
		if (this.saveBtn != null && !this.saveBtn.isDisposed() && this.cancelBtn != null
				&& !this.cancelBtn.isDisposed()) {
			GridData layoutData = (GridData) this.saveBtn.getParent().getLayoutData();
			layoutData.exclude = !showButtonBar;
			this.saveBtn.setVisible(showButtonBar);
			this.cancelBtn.setVisible(showButtonBar);
			this.saveBtn.getParent().setVisible(showButtonBar);
			this.saveBtn.getParent().requestLayout();
			this.saveBtn.getParent().redraw();
			this.saveBtn.getParent().getParent().update();
		}
	}
}
