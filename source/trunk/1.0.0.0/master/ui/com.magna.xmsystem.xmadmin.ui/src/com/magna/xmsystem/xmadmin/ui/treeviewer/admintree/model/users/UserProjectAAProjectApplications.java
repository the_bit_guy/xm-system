package com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.users;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.stream.Collectors;

import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.projectapplicationmodel.ProjectApplication;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.refs.RelationObj;

/**
 * The Class UserProjectApplications.
 * 
 * @author subash.janarthanan
 * 
 */
public class UserProjectAAProjectApplications implements IAdminTreeChild {
	/** The parent. */
	private IAdminTreeChild parent;

	/** The user project app child. */
	private Map<String, IAdminTreeChild> userProjectAppChild;

	/**
	 * Instantiates a new user project applications.
	 */
	public UserProjectAAProjectApplications(final IAdminTreeChild parent) {
		this.parent = parent;
		this.userProjectAppChild = new LinkedHashMap<>();
		addFixedChildren();
	}

	/**
	 * Method for Adds the fixed children.
	 */
	public void addFixedChildren() {
		this.userProjectAppChild.put(UserProjectAAProjectAppAllowed.class.getName(), new UserProjectAAProjectAppAllowed(this));
		this.userProjectAppChild.put(UserProjectAAProjectAppForbidden.class.getName(), new UserProjectAAProjectAppForbidden(this));
	}
	/**
	 * Gets the user project app child collection.
	 *
	 * @return the user project app child collection
	 */
	public Collection<IAdminTreeChild> getUserProjectAppChildCollection() {
		return this.userProjectAppChild.values();
	}

	/**
	 * Gets the project app child children.
	 *
	 * @return the project app child children
	 */
	public Map<String, IAdminTreeChild> getProjectAppChildChildren() {
		return userProjectAppChild;
	}
	
	/**
	 * Add.
	 *
	 * @param userProjectAppChildId the user project app child id
	 * @param child the child
	 * @return the i admin tree child
	 */
	public IAdminTreeChild add(final String userProjectAppChildId, final IAdminTreeChild child) {
		child.setParent(this);
		IAdminTreeChild returnVal = this.userProjectAppChild.put(userProjectAppChildId, child);
		if (child instanceof RelationObj && ((RelationObj) child).getRefObject() instanceof ProjectApplication) {
			sort();
		}
		return returnVal;
	}

	/**
	 * Remove.
	 *
	 * @param userProjectAppChildId the user project app child id
	 * @return the i admin tree child
	 */
	public IAdminTreeChild remove(final String userProjectAppChildId) {
		return this.userProjectAppChild.remove(userProjectAppChildId);
	}

	/**
	 * Returns null as its parent
	 * 
	 * @see com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild#getParent()
	 */
	@Override
	public IAdminTreeChild getParent() {
		return this.parent;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild#
	 * setParent(com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.
	 * IAdminTreeChild)
	 */
	@Override
	public void setParent(final IAdminTreeChild parent) {
		this.parent = parent;
	}
	
	/**
	 * Sort.
	 */
	public void sort() {
		final LinkedHashMap<String, IAdminTreeChild> collect = this.userProjectAppChild.entrySet().stream().sorted(
				(e1, e2) -> ((ProjectApplication) (((RelationObj) e1.getValue()).getRefObject())).getName()
				.compareTo(((ProjectApplication) (((RelationObj) e2.getValue()).getRefObject())).getName()))
				.collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e1, LinkedHashMap::new));
		this.userProjectAppChild = collect;
	}
}
