package com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.users;

import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild;

/**
 * The Class UserAppForbidden.
 * 
 * @author shashwat.anand
 */
public class UserAAUserAppForbidden implements IAdminTreeChild {
	/** The parent. */
	private IAdminTreeChild parent;

	/**
	 * Instantiates a new user app allowed.
	 *
	 * @param parent the parent
	 */
	public UserAAUserAppForbidden(final IAdminTreeChild parent) {
		this.parent = parent;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild#
	 * getParent()
	 */
	@Override
	public IAdminTreeChild getParent() {
		return this.parent;
	}

	/*
	 * com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild#
	 * setParent(com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.
	 * IAdminTreeChild)
	 */
	@Override
	public void setParent(final IAdminTreeChild parent) {
		this.parent = parent;
	}
}
