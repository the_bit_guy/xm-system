package com.magna.xmsystem.xmadmin.ui.importuser.wizard.model;

import java.util.ArrayList;
import java.util.List;

import com.magna.xmbackend.vo.ldap.LdapUser;

public class LdapUsers {
	private List<LdapUser> ldapUsers;
	
	public LdapUsers() {
		this.setLdapUsers(new ArrayList<LdapUser>());
	}

	/**
	 * @return the ldapUsers
	 */
	public List<LdapUser> getLdapUsers() {
		return ldapUsers;
	}

	/**
	 * @param ldapUsers the ldapUsers to set
	 */
	public void setLdapUsers(final List<LdapUser> ldapUsers) {
		this.ldapUsers = ldapUsers;
	}
}
