package com.magna.xmsystem.ui.controls.util;

import org.eclipse.e4.ui.workbench.swt.internal.copy.PatternFilter;

/**
 * class PatternFilterTree.java Here used internal Filtered Tree as e4 has moved
 * Filter tree api to internal package other wise we can use
 * org.eclipse.ui.workbench in dependency
 * 
 * @author Shashwat Anand
 */
@SuppressWarnings("restriction")
public class PatternFilterTree extends PatternFilter {

	/**
	 * The pattern string for which this filter should select elements in the
	 * viewer.
	 * 
	 * @param patternString {@link String}
	 */
	public void setPattern(String patternString) {
		if (patternString != null && patternString.length() > 0) {
			patternString = "*" + patternString; //$NON-NLS-1$
		}
		super.setPattern(patternString);
	}
}
