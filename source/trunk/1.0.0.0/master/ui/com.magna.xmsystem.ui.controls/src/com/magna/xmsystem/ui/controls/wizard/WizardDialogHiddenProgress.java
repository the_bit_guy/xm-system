package com.magna.xmsystem.ui.controls.wizard;

import java.lang.reflect.InvocationTargetException;

import org.eclipse.jface.operation.IRunnableWithProgress;
import org.eclipse.jface.wizard.IWizard;
import org.eclipse.jface.wizard.ProgressMonitorPart;
import org.eclipse.jface.wizard.WizardDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Shell;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The Class WizardDialogHiddenProgress.
 * 
 * @author shashwat.anand
 */
public class WizardDialogHiddenProgress extends WizardDialog {

	/** The Constant LOGGER. */
	private final static Logger LOGGER = LoggerFactory.getLogger(WizardDialogHiddenProgress.class);

	/**
	 * Instantiates a new wizard dialog hidden progress.
	 *
	 * @param parentShell the parent shell
	 * @param newWizard the new wizard
	 */
	public WizardDialogHiddenProgress(final Shell parentShell, final IWizard newWizard) {
		super(parentShell, newWizard);

	}

	/**
	 * @see org.eclipse.jface.wizard.WizardDialog#run(boolean, boolean,
	 * org.eclipse.jface.operation.IRunnableWithProgress)
	 */
	@Override
	public void run(final boolean fork, final boolean cancelable, final IRunnableWithProgress runnable)
			throws InvocationTargetException, InterruptedException {
		try {
			enable(true);
			super.run(fork, cancelable, runnable);
		} finally {
			enable(false);
		}
	}

	/**
	 * Enable.
	 * @param flag the flag
	 */
	private void enable(final boolean flag) {
		try {
			GridData gridData;
			ProgressMonitorPart pro = (ProgressMonitorPart) getProgressMonitor();
			if (pro == null)
				return;
			gridData = (GridData) pro.getLayoutData();
			if (gridData != null) {
				if (flag) {
					gridData.exclude = false;
					gridData.heightHint = -1;
				} else {
					gridData.exclude = true;
					gridData.heightHint = 0;
				}
				pro.getParent().layout(true);
			}
		} catch (Exception ex) {
			LOGGER.warn("Failed to enable progress", ex); //$NON-NLS-1$
		}
	}

	/**
	 * @see
	 * org.eclipse.jface.wizard.WizardDialog#createDialogArea(org.eclipse.swt.
	 * widgets.Composite)
	 */
	@Override
	protected Control createDialogArea(Composite parent) {
		parent.setBackgroundMode(SWT.INHERIT_DEFAULT);
		Control area = super.createDialogArea(parent);
		enable(false);
		return area;
	}
}
