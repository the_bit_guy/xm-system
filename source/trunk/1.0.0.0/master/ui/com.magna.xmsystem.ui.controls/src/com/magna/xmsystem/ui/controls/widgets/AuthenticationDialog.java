package com.magna.xmsystem.ui.controls.widgets;

import java.net.URL;

import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.Path;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import org.osgi.framework.Bundle;
import org.osgi.framework.FrameworkUtil;

import com.magna.xmbackend.vo.user.AuthResponse;

/**
 * Class for Authentication dialog. Used in the XMenu Switch User.
 *
 * @author Chiranjeevi.Akula
 */
@Deprecated
public class AuthenticationDialog extends Dialog {

	/** Member variable 'username' for {@link String}. */
	private String username;

	/** Member variable 'username text' for {@link Text}. */
	private Text usernameText;

	/** Member variable 'password text' for {@link Text}. */
	private Text passwordText;

	/** Member variable 'login composite' for {@link Composite}. */
	protected Composite loginComposite;

	/** Member variable 'title image' for {@link Image}. */
	private Image titleImage;

	/** Member variable 'image descriptor' for {@link ImageDescriptor}. */
	private ImageDescriptor imageDescriptor;

	/** Member variable 'ok button' for {@link Button}. */
	private Button okButton;
	
	private Button cancelButton;

	/** Member variable 'error msg label' for {@link Label}. */
	private Label errorMsgLabel;

	/** Member variable 'application name' for {@link String}. */
	private String applicationName;

	/** Member variable 'shell title' for {@link String}. */
	private String shellTitle;

	/** Member variable 'auth response' for {@link AuthResponse}. */
	private AuthResponse authResponse;

	/** Member variable 'label font' for {@link Font}. */
	private Font labelFont = new Font(Display.getDefault(), "Arabic Transparent", 9, SWT.BOLD);
	
	private String loginDialogUserName;
	
	private String loginDialogPassword;
	
	private String loginDialogLoginBtn;
	
	private String loginDialogCancelBtn;
	

	/**
	 * Constructor for AuthenticationDialog Class.
	 *
	 * @param parentShell
	 *            {@link Shell}
	 * @param shellTitle
	 *            {@link String}
	 * @param applicationName
	 *            {@link String}
	 * @param loginDialogCancelBtn 
	 * @param loginDialogLoginBtn 
	 * @param loginDialogPassword 
	 * @param loginDialogUserName 
	 */
	public AuthenticationDialog(final Shell parentShell, final String shellTitle, final String applicationName, String loginDialogUserName, 
			String loginDialogPassword, String loginDialogLoginBtn, String loginDialogCancelBtn) {
		super(parentShell);
		this.applicationName = applicationName;
		this.shellTitle = shellTitle;
		this.loginDialogUserName = loginDialogUserName;
		this.loginDialogPassword = loginDialogPassword;
		this.loginDialogLoginBtn = loginDialogLoginBtn;
		this.loginDialogCancelBtn = loginDialogCancelBtn;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.jface.window.Window#configureShell(org.eclipse.swt.widgets.
	 * Shell)
	 */
	@Override
	protected void configureShell(Shell shell) {
		super.configureShell(shell);
		shell.setText(shellTitle != null ? shellTitle : "");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.jface.dialogs.Dialog#createDialogArea(org.eclipse.swt.widgets
	 * .Composite)
	 */
	@Override
	protected Control createDialogArea(Composite parent) {
		parent.setBackgroundMode(SWT.INHERIT_DEFAULT);
		Composite control = createContentArea(parent);

		// control.setData("org.eclipse.e4.ui.css.id", "LoginDialog");
		// Rectangle controlRect = control.getBounds();
		// looks strange in multi monitor environments
		// Rectangle displayBounds = shell.getDisplay().getBounds();
		// Monitor primary = shell.getDisplay().getPrimaryMonitor();
		// Rectangle displayBounds = primary.getBounds();
		// int x = (displayBounds.width - controlRect.width) / 2;
		// int y = (displayBounds.height - controlRect.height) / 2;
		// shell.setBounds(x, y, controlRect.width, controlRect.height);

		return control;
	}

	/**
	 * Method for Creates the content area.
	 *
	 * @param parent
	 *            {@link Composite}
	 * @return the composite {@link Composite}
	 */
	protected Composite createContentArea(Composite parent) {
		Composite composite = new Composite(parent, SWT.NONE);
		composite.setBackgroundMode(SWT.INHERIT_DEFAULT);
		GridLayout gridLayout = new GridLayout();
		gridLayout.numColumns = 1;
		gridLayout.marginHeight = 0;
		gridLayout.marginWidth = 0;
		composite.setLayout(gridLayout);

		if (imageDescriptor == null) {
			final Bundle bundle = FrameworkUtil.getBundle(this.getClass());
			URL url = FileLocator.find(bundle, new Path("icons/large/engineering_l_old.gif"), null);
			imageDescriptor = imageDescriptorFromURI(url);
		}
		if (imageDescriptor != null) {
			titleImage = imageDescriptor.createImage();
			Label imageLabel = new Label(composite, SWT.NONE);

			GridData data = new GridData();
			data.horizontalAlignment = GridData.FILL;
			data.verticalAlignment = GridData.BEGINNING;
			data.horizontalSpan = 1;
			imageLabel.setLayoutData(data);
			imageLabel.setImage(titleImage);
		}

		GridData errorMsgGridData = new GridData(SWT.HORIZONTAL, SWT.TOP, false, true);
		errorMsgGridData.widthHint = 300;
		errorMsgGridData.horizontalIndent = 25;
		errorMsgLabel = new Label(composite, SWT.WRAP);
		errorMsgLabel.setLayoutData(errorMsgGridData);
		errorMsgLabel.setForeground(Display.getDefault().getSystemColor(SWT.COLOR_RED));
		errorMsgLabel.setVisible(false);

		Composite userPasswordComposite = new Composite(composite, SWT.NONE);
		// userPasswordComposite.setData("org.eclipse.e4.ui.css.id",
		// "LoginDialog");
		GridLayout gridLayout2 = new GridLayout(2, false);
		gridLayout2.marginHeight = 10;
		gridLayout2.marginWidth = 30;
		userPasswordComposite.setLayout(gridLayout2);
		GridData gridData = new GridData();
		gridData.grabExcessHorizontalSpace = true;
		gridData.horizontalAlignment = GridData.FILL;
		userPasswordComposite.setLayoutData(gridData);

		Label userLabel = new Label(userPasswordComposite, SWT.NONE);
		userLabel.setFont(labelFont);
		userLabel.setText(loginDialogUserName);

		usernameText = new Text(userPasswordComposite, SWT.BORDER);
		usernameText.setText(username != null ? username : "");
		usernameText.selectAll();
		gridData = new GridData();
		gridData.grabExcessHorizontalSpace = true;
		gridData.horizontalAlignment = GridData.FILL;
		usernameText.setLayoutData(gridData);

		Label passwordLabel = new Label(userPasswordComposite, SWT.NONE);
		passwordLabel.setFont(labelFont);
		//userLabel.setText("Username ");
		passwordLabel.setText(loginDialogPassword);

		passwordText = new Text(userPasswordComposite, SWT.PASSWORD | SWT.BORDER);

		gridData = new GridData();
		gridData.grabExcessHorizontalSpace = true;
		gridData.horizontalAlignment = GridData.FILL;
		passwordText.setLayoutData(gridData);

		initListeners();

		return composite;
	}

	/**
	 * Method for Inits the listeners.
	 */
	private void initListeners() {
		LoginModifyListener loginModifyListener = new LoginModifyListener();
		usernameText.addModifyListener(loginModifyListener);
		passwordText.addModifyListener(loginModifyListener);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.jface.dialogs.Dialog#createButtonsForButtonBar(org.eclipse.
	 * swt.widgets.Composite)
	 */
	@Override
	protected void createButtonsForButtonBar(Composite parent) {
		// create OK and Cancel buttons by default
		parent.setBackgroundMode(SWT.INHERIT_DEFAULT);
		super.createButtonsForButtonBar(parent);

		okButton = getButton(IDialogConstants.OK_ID);
		okButton.setText(loginDialogLoginBtn);
		okButton.setEnabled(false);
		
		cancelButton = getButton(IDialogConstants.CANCEL_ID);
		cancelButton.setText(loginDialogCancelBtn);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jface.dialogs.Dialog#close()
	 */
	@Override
	public boolean close() {
		if (titleImage != null) {
			titleImage.dispose();
		}
		if (labelFont != null) {
			labelFont.dispose();
		}
		return super.close();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jface.dialogs.Dialog#okPressed()
	 */
	@Override
	protected void okPressed() {
		if (applicationName != null && !applicationName.isEmpty()) {
			String errorMessage = null;
			try {
				// AuthController authController = new AuthController();
				authResponse = null;
				// authResponse = authController.authorizeLogin(usernameText.getText(), passwordText.getText(), applicationName);
			} catch (Exception e) {
				errorMessage = "Server Not Reachable! Please check the connection..";
			} finally {
				if (authResponse != null) {
					boolean isValidUser = authResponse.isValidUser();
					errorMessage = authResponse.getMessage();
					if (isValidUser) {
						super.okPressed();
					}
				}

				if (errorMessage != null && !errorMsgLabel.isDisposed()) {
					errorMsgLabel.setText("*" + errorMessage);
					errorMsgLabel.setVisible(true);
					errorMsgLabel.getParent().pack();
					errorMsgLabel.getParent().getParent().pack();
					errorMsgLabel.getParent().getParent().getParent().pack();
				}
			}
		} else {
			CustomMessageDialog.openError(this.getShell(), this.getShell().getText(),
					"Unauthorized application name!!");
		}
	}

	/**
	 * Method for Image descriptor from URI.
	 *
	 * @param url
	 *            {@link URL}
	 * @return the image descriptor {@link ImageDescriptor}
	 */
	public ImageDescriptor imageDescriptorFromURI(URL url) {
		try {
			return ImageDescriptor.createFromURL(url);
		} catch (Exception e) {
			System.err.println(
					"iconURI \"" + url.toExternalForm() + "\" is invalid, a \"missing image\" icon will be shown");
			return ImageDescriptor.getMissingImageDescriptor();
		}
	}

	/**
	 * Sets the username.
	 *
	 * @param user
	 *            the new username
	 */
	public void setUsername(String user) {
		this.username = user;
	}

	/**
	 * Gets the auth response.
	 *
	 * @return the auth response
	 */
	public AuthResponse getAuthResponse() {
		return authResponse;
	}

	/**
	 * The listener interface for receiving loginModify events. The class that
	 * is interested in processing a loginModify event implements this
	 * interface, and the object created with that class is registered with a
	 * component using the component's <code>addLoginModifyListener<code>
	 * method. When the loginModify event occurs, that object's appropriate
	 * method is invoked.
	 *
	 * @see LoginModifyEvent
	 */
	private class LoginModifyListener implements ModifyListener {

		/*
		 * (non-Javadoc)
		 * 
		 * @see
		 * org.eclipse.swt.events.ModifyListener#modifyText(org.eclipse.swt.
		 * events.ModifyEvent)
		 */
		@Override
		public void modifyText(ModifyEvent event) {
			if (usernameText != null && passwordText != null) {
				if (!usernameText.getText().isEmpty() && !passwordText.getText().isEmpty()) {
					if (okButton != null) {
						okButton.setEnabled(true);
					}
				} else {
					if (okButton != null) {
						okButton.setEnabled(false);
					}
				}
			}
		}
	}
}