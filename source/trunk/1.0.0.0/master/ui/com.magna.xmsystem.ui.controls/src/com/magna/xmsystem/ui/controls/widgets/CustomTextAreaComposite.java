package com.magna.xmsystem.ui.controls.widgets;

import org.eclipse.jface.layout.GridLayoutFactory;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Link;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * class for CustomXMAdminTextArea created control.
 *
 * @author Deepak upadhyay
 */
public class CustomTextAreaComposite extends Composite {
	/** The Constant LOGGER. */
	private static final Logger LOGGER = LoggerFactory.getLogger(CustomTextAreaComposite.class);

	/** Instance for Max limit character. */
	static final int MAX_LIMIT = 1000;

	/** Instance for String slash. */
	public static final String SLASH = "/";

	/** Instance for Limit_count_padding. */
	public static final String LIMIT_COUNT_PADDING = "  ";

	/** Instance of MainLabel. */
	transient protected Label lblMainLabel;

	/** Instance of label count. */
	transient protected Label lblCount;

	/** Instance of txtAreaRemarks. */
	transient protected Text textArea;

	/** Instance of linkLangauage. */
	transient private Link linkAddLang;

	/** Instance of StringMaXLimit. */
	private int maxLimit;

	/** Instance of dialog title. */
	private String dialogTitle;

	/** Instance of dialogArea. */
	transient private CustomTextAreaDialog dialogArea;

	/** Instance of dialogLabel. */
	private String dialogLabel;

	/**
	 * Parameterized Constructor.
	 *
	 * @param parent
	 *            {@link Composite}
	 * @param style
	 *            {@link int}
	 * @param maxLimitP
	 *            {@link int}
	 */
	public CustomTextAreaComposite(final Composite parent, final int style, final int maxLimitP) {
		super(parent, style);
		this.maxLimit = maxLimitP;
		initGui();
		initListener();
		this.lblCount.setText(
				LIMIT_COUNT_PADDING + this.textArea.getText().length() + SLASH + this.maxLimit + LIMIT_COUNT_PADDING);
	}

	/**
	 * Parameterized Constructor.
	 *
	 * @param parent
	 *            {@link Composite}
	 * @param style
	 *            {@link int}
	 * @param strMainLabelP
	 *            {@link String}
	 * @param strLangLabelP
	 *            {@link String}
	 */
	public CustomTextAreaComposite(final Composite parent, final int style, final String strMainLabelP,
			final String strLangLabelP) {
		this(parent, style, MAX_LIMIT);
	}

	/**
	 * Method char count for letter count.
	 */
	public void countCharactor() {
		this.lblCount.setText(
				LIMIT_COUNT_PADDING + this.textArea.getText().length() + SLASH + this.maxLimit + LIMIT_COUNT_PADDING);
		this.lblCount.getParent().layout();
		this.lblCount.getParent().update();
	}

	/**
	 * method for initialize Graphical user interface.
	 */
	private void initGui() {
		try {
			GridLayoutFactory.fillDefaults().applyTo(this);
			this.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
			final Composite widgetContainer = new Composite(this, SWT.NONE);
			final GridLayout containerLayout = new GridLayout(1, false);
			containerLayout.marginRight = 0;
			containerLayout.marginLeft = 0;
			containerLayout.marginTop = 0;
			containerLayout.marginBottom = 0;
			containerLayout.marginWidth = 0;
			containerLayout.marginHeight = 0;
			widgetContainer.setLayout(containerLayout);
			widgetContainer.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
			createTopBar(widgetContainer);
			createBottom(widgetContainer);
		} catch (Exception e) {
			LOGGER.error("Unable to create UI elements", e);
		}
	}

	/**
	 * Method initListener for Listener.
	 */
	private void initListener() {
		this.linkAddLang.addSelectionListener(new SelectionAdapter() {
			/**
			 * handler for widgetSelected
			 */
			@Override
			public void widgetSelected(final SelectionEvent event) {
				final Link linkWidget = (Link) event.widget;
				openDialogs(linkWidget.getShell());
			}
		});

		this.textArea.addModifyListener(new ModifyListener() {

			/**
			 * handler for modifyText
			 */
			@Override
			public void modifyText(ModifyEvent event) {
				countCharactor();
			}
		});
	}

	/**
	 * Method opendialog for open new dialog.
	 *
	 * @param shell
	 *            {@link Shell}
	 */
	private void openDialogs(final Shell shell) {
		dialogArea = new CustomTextAreaDialog(shell, this.getDialogTitle(), this.dialogLabel, this.maxLimit,
				textArea.getEditable()) {
			/**
			 * method for notes dialog binding
			 */
			@Override
			protected void bindDialog() {
				bindNotesDialog();
			}
		};
		dialogArea.open();
	}

	/**
	 * method for notes dialog binding.
	 */
	protected void bindNotesDialog() {
		// Nothing
	}

	/**
	 * Method createTopBar for creating topbar widget.
	 *
	 * @param widgetContainer
	 *            {@link Composite}
	 */

	private void createTopBar(final Composite widgetContainer) {
		final Composite topBar = new Composite(widgetContainer, SWT.None);
		final GridLayout widgetContainerLayout = new GridLayout(4, false);
		widgetContainerLayout.marginRight = 0;
		widgetContainerLayout.marginLeft = 0;
		widgetContainerLayout.marginTop = 0;
		widgetContainerLayout.marginBottom = 0;
		widgetContainerLayout.marginWidth = 0;
		widgetContainerLayout.verticalSpacing = 0;
		topBar.setLayout(widgetContainerLayout);
		topBar.setLayout(widgetContainerLayout);
		topBar.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false));

		this.lblMainLabel = new Label(topBar, SWT.NONE);
		this.lblMainLabel.setLayoutData(new GridData(SWT.CENTER, SWT.CENTER, false, false, 1, 1));

		final Label lblEmpty = new Label(topBar, SWT.NONE);
		lblEmpty.setText("");
		lblEmpty.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));

		this.linkAddLang = new Link(topBar, SWT.FLAT);
		this.linkAddLang.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, true, 1, 1));

		this.lblCount = new Label(topBar, SWT.BORDER);
		this.lblCount.setLayoutData(new GridData(SWT.CENTER, SWT.CENTER, false, false));
	}

	/**
	 * method createBottom for creating bottom widget.
	 *
	 * @param widgetContainer
	 *            {@link Composite}
	 */
	private void createBottom(final Composite widgetContainer) {
		this.textArea = new Text(widgetContainer, SWT.MULTI | SWT.BORDER | SWT.WRAP | SWT.V_SCROLL);
		this.textArea.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 2, 1));
		this.textArea.setTextLimit(this.maxLimit);
	}

	/**
	 * Getter max Limit.
	 *
	 * @return the max limit
	 */
	public int getMaxLimit() {
		return maxLimit;
	}

	/**
	 * Setter method.
	 *
	 * @param maxLimit
	 *            the new max limit
	 */

	public void setMaxLimit(final int maxLimit) {
		this.maxLimit = maxLimit;
	}

	/**
	 * getter for LabelMainLabel.
	 *
	 * @return the lbl main label
	 */

	public Label getLblMainLabel() {
		return lblMainLabel;
	}

	/**
	 * method getlblcount.
	 *
	 * @return the lbl count
	 */
	public Label getLblCount() {
		return lblCount;
	}

	/**
	 * method getLinkAddLang.
	 *
	 * @return the link add lang
	 */
	public Link getLinkAddLang() {
		return linkAddLang;
	}

	/**
	 * Gets the dialog title.
	 *
	 * @return the dialogTitle
	 */
	public String getDialogTitle() {
		return dialogTitle;
	}

	/**
	 * Sets the dialog title.
	 *
	 * @param dialogTitle
	 *            the dialogTitle to set
	 */
	public void setDialogTitle(final String dialogTitle) {
		this.dialogTitle = dialogTitle;
	}

	/**
	 * Gets the text area.
	 *
	 * @return the txtArea
	 */
	public Text getTextArea() {
		return textArea;
	}

	/**
	 * Gets the dialog area.
	 *
	 * @return the dialogArea
	 */
	public CustomTextAreaDialog getDialogArea() {
		return dialogArea;
	}

	/**
	 * Gets the dialog label.
	 *
	 * @return the dialog label
	 */
	public String getDialogLabel() {
		return dialogLabel;
	}

	/**
	 * Sets the dialog label.
	 *
	 * @param dialogLabel
	 *            the new dialog label
	 */
	public void setDialogLabel(String dialogLabel) {
		this.dialogLabel = dialogLabel;
	}
}
