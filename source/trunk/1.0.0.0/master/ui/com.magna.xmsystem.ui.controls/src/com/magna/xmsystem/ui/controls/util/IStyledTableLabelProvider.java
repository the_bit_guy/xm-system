package com.magna.xmsystem.ui.controls.util;

import org.eclipse.jface.viewers.StyledString;
import org.eclipse.swt.graphics.Image;

/**
 * Interface marking a label provider that provides styled text labels and
 * images.
 * <p>
 * The {@link DelegatingStyledCellLabelProvider.IStyledLabelProvider} can
 * optionally implement {@link IColorProvider} and {@link IFontProvider} to
 * provide foreground and background color and a default font.
 * </p>
 */
public interface IStyledTableLabelProvider {

	/**
	 * Returns the styled text label for the given element
	 * 
	 * @param element
	 *            the element to evaluate the styled string for
	 * 
	 * @return the styled string.
	 */
	public StyledString getStyledText(Object element, int idx);

	/**
	 * Returns the image for the label of the given element. The image is owned
	 * by the label provider and must not be disposed directly. Instead, dispose
	 * the label provider when no longer needed.
	 * 
	 * @param element
	 *            the element for which to provide the label image
	 * @return the image used to label the element, or <code>null</code> if
	 *         there is no image for the given object
	 */
	public Image getImage(Object element, int idx);
}