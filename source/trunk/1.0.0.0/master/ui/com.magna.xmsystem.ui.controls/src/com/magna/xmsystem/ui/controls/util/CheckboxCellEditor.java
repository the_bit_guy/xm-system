package com.magna.xmsystem.ui.controls.util;

import org.eclipse.jface.viewers.CellEditor;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;

/**
 * The Class CheckboxCellEditor.
 * 
 * @author shashwat.anand
 */
public class CheckboxCellEditor extends CellEditor {
	
	/** The button. */
	private Button button;

	/**
	 * Instantiates a new checkbox cell editor.
	 *
	 * @param parent the parent
	 * @param style the style
	 */
	public CheckboxCellEditor(final Composite parent, final int style) {
		super(parent, style);
	}

	/* (non-Javadoc)
	 * @see org.eclipse.jface.viewers.CellEditor#createControl(org.eclipse.swt.widgets.Composite)
	 */
	@Override
	protected Control createControl(final Composite parent) {
		return this.button = new Button(parent, SWT.CHECK);
	}

	/* (non-Javadoc)
	 * @see org.eclipse.jface.viewers.CellEditor#doGetValue()
	 */
	@Override
	protected Object doGetValue() {
		return this.button.getSelection();
	}

	/* (non-Javadoc)
	 * @see org.eclipse.jface.viewers.CellEditor#doSetFocus()
	 */
	@Override
	protected void doSetFocus() {
		// Nothing
	}

	/* (non-Javadoc)
	 * @see org.eclipse.jface.viewers.CellEditor#doSetValue(java.lang.Object)
	 */
	@Override
	protected void doSetValue(final Object value) {
		this.button.setSelection(((Boolean) value).booleanValue());
	}

}
