package com.magna.xmsystem.ui.controls.widgets;

import org.eclipse.e4.ui.css.core.dom.properties.ICSSPropertyHandler;
import org.eclipse.e4.ui.css.core.dom.properties.converters.ICSSValueConverter;
import org.eclipse.e4.ui.css.core.engine.CSSEngine;
import org.eclipse.e4.ui.css.swt.properties.AbstractCSSPropertySWTHandler;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.widgets.Control;
import org.w3c.dom.css.CSSValue;

@SuppressWarnings("restriction")
public class CustomButtonCSSHandler extends AbstractCSSPropertySWTHandler implements ICSSPropertyHandler {

	private static final String BACKGROUND_COLOR = "label-background-color";
	private static final String FOREGROUND_COLOR = "label-foreground-color";

	@Override
	protected void applyCSSProperty(Control control, String property, CSSValue value, String pseudo, CSSEngine engine)
			throws Exception {
		
		if (control instanceof CustomButton) {
			CustomButton customButton = (CustomButton) control;
            if (BACKGROUND_COLOR.equalsIgnoreCase(property) && (value.getCssValueType() == CSSValue.CSS_PRIMITIVE_VALUE)) {
                Color newColor = (Color) engine.convert(value, Color.class, control.getDisplay());
                customButton.setBackground(newColor);
            } 
            
            if (FOREGROUND_COLOR.equalsIgnoreCase(property) && (value.getCssValueType() == CSSValue.CSS_PRIMITIVE_VALUE)) {
                Color newColor = (Color) engine.convert(value, Color.class, control.getDisplay());
                customButton.setForeground(newColor);
            } 
        }

	}

	@Override
	protected String retrieveCSSProperty(Control control, String property, String pseudo, CSSEngine engine) throws Exception {
		if (control instanceof CustomButton) {
			CustomButton customButton = (CustomButton) control;
            if (BACKGROUND_COLOR.equalsIgnoreCase(property)) {
                ICSSValueConverter cssValueConverter = engine.getCSSValueConverter(String.class);
                return cssValueConverter.convert(customButton.getBackgroundColor(), engine, null);
            } 
            
            if (BACKGROUND_COLOR.equalsIgnoreCase(property)) {
                ICSSValueConverter cssValueConverter = engine.getCSSValueConverter(String.class);
                return cssValueConverter.convert(customButton.getForegroundColor(), engine, null);
            }
        }
        return null;
	}

}
