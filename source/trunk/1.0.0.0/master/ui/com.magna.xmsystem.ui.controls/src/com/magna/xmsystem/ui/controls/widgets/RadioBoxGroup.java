package com.magna.xmsystem.ui.controls.widgets;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.PaintEvent;
import org.eclipse.swt.events.PaintListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Canvas;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Layout;
import org.eclipse.swt.widgets.Widget;

import com.magna.xmsystem.ui.controls.util.GraphicUtil;

public class RadioBoxGroup extends Canvas implements PaintListener {
	protected Button button;
	private final Composite content;
	private final List<SelectionListener> selectionListeners;

	private boolean transparent = false;

	/**
	 * Constructs a new instance of this class given its parent and a style value describing its behavior and appearance.
	 * 
	 * @param parent a widget which will be the parent of the new instance (cannot be null)
	 * @param style the style of widget to construct
	 * @see Composite#Composite(Composite, int)
	 * @see SWT#BORDER
	 * @see Widget#getStyle
	 */
	public RadioBoxGroup(final Composite parent, final int style) {
		super(parent, style);
		super.setLayout(new GridLayout());
		this.selectionListeners = new ArrayList<SelectionListener>();
		createRadioBoxButton();

		this.content = new Composite(this, style);
		this.content.setLayoutData(new GridData(GridData.FILL, GridData.FILL, true, true));

		this.addPaintListener(this);
	}

	private void createRadioBoxButton() {
		this.button = new Button(this, SWT.RADIO);
		final GridData gdButton = new GridData(GridData.BEGINNING, GridData.CENTER, true, false);
		gdButton.horizontalIndent = 15;
		this.button.setLayoutData(gdButton);
		this.button.setSelection(true);
		this.button.pack();

		this.button.addSelectionListener(new SelectionAdapter() {
			/**
			 * @see org.eclipse.swt.events.SelectionAdapter#widgetSelected(org.eclipse.swt.events.SelectionEvent)
			 */
			@Override
			public void widgetSelected(final SelectionEvent e) {
				e.doit = fireSelectionListeners(e);
				if (!e.doit) {
					return;
				}
				if (RadioBoxGroup.this.button.getSelection()) {
					RadioBoxGroup.this.activate();
				} else {
					RadioBoxGroup.this.deactivate();
				}
			}
		});
	}

	/**
	 * Fire the selection listeners
	 * 
	 * @param selectionEvent mouse event
	 * @return true if the selection could be changed, false otherwise
	 */
	private boolean fireSelectionListeners(final SelectionEvent selectionEvent) {
		selectionEvent.widget = this;
		for (final SelectionListener listener : this.selectionListeners) {
			listener.widgetSelected(selectionEvent);
			if (!selectionEvent.doit) {
				return false;
			}
		}
		return true;
	}

	/**
	 * Activate the content
	 */
	public void activate() {
		this.button.setSelection(true);
		GraphicUtil.enableAllChildrenWidgets(this.content);
	}

	/**
	 * Adds the listener to the collection of listeners who will be notified when the user changes the receiver's selection, 
	 * by sending it one of the messages defined in the <code>SelectionListener</code> interface.
	 * 
	 * @param listener the listener which should be notified when the user changes the receiver's selection
	 */
	public void addSelectionListener(final SelectionListener listener) {
		checkWidget();
		this.selectionListeners.add(listener);
	}

	/**
	 * Deactivate the content
	 */
	public void deactivate() {
		this.button.setSelection(false);
		GraphicUtil.disableAllChildrenWidgets(this.content);
	}

	/**
	 * @return <code>true</code> if the content is activated, <code>false</code> otherwise
	 */
	public boolean isActivated() {
		return this.button.getSelection();
	}

	/**
	 * @see org.eclipse.swt.widgets.Composite#getLayout()
	 */
	@Override
	public Layout getLayout() {
		return this.content.getLayout();
	}

	/**
	 * Removes the listener from the collection of listeners who will be notified when the user changes the receiver's selection.
	 */
	public void removeSelectionListener(final SelectionListener listener) {
		checkWidget();
		this.selectionListeners.remove(listener);
	}

	/**
	 * @see org.eclipse.swt.widgets.Composite#setFocus()
	 */
	@Override
	public boolean setFocus() {
		return this.content.setFocus();
	}

	/**
	 * @see org.eclipse.swt.widgets.Composite#setLayout(org.eclipse.swt.widgets.Layout)
	 */
	@Override
	public void setLayout(final Layout layout) {
		this.content.setLayout(layout);
	}

	// ------------------------------------ Getters and Setters

	/**
	 * @return the text of the button
	 */
	public String getText() {
		return this.button.getText();
	}

	/**
	 * @param text the text of the button to set
	 */
	public void setText(final String text) {
		this.button.setText(text);
	}

	/**
	 * @return the font of the button
	 */
	@Override
	public Font getFont() {
		return this.button.getFont();
	}

	/**
	 * @param font the font to set
	 */
	@Override
	public void setFont(final Font font) {
		this.button.setFont(font);
	}

	/**
	 * @return the content of the group
	 */
	public Composite getContent() {
		return this.content;
	}

	public boolean isTransparent() {
		return this.transparent;
	}

	public void setTransparent(final boolean transparent) {
		this.transparent = transparent;
		if (transparent) {
			setBackgroundMode(SWT.INHERIT_DEFAULT);
			this.content.setBackgroundMode(SWT.INHERIT_DEFAULT);
		}
	}

	@Override
	public void paintControl(final PaintEvent paintEvent) {
		if (paintEvent.widget == this) {
			drawWidget(paintEvent.gc);
		}
	}

	/**
	 * Draws the widget
	 */
	private void drawWidget(final GC gc) {
		final Rectangle rect = this.getClientArea();
		final int margin = (int) (this.button.getSize().y * 1.5);
		final int startY = margin / 2;

		gc.setForeground(this.getDisplay().getSystemColor(SWT.COLOR_WIDGET_NORMAL_SHADOW));
		gc.drawRoundRectangle(1, startY, rect.width - 2, rect.height - startY - 2, 2, 2);

		gc.setForeground(this.getDisplay().getSystemColor(SWT.COLOR_WIDGET_LIGHT_SHADOW));
		gc.drawRoundRectangle(2, startY + 1, rect.width - 4, rect.height - startY - 4, 2, 2);
	}
}
