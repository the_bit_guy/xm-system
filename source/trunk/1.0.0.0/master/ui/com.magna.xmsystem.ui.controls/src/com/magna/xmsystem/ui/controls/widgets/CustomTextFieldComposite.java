package com.magna.xmsystem.ui.controls.widgets;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Link;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * class for CustomXMAdminDescriptionArea create control
 * 
 * @author Deepak Upadhyay
 *
 */
public class CustomTextFieldComposite extends Composite {
	/** The Constant LOGGER. */
	private static final Logger LOGGER = LoggerFactory.getLogger(CustomTextFieldComposite.class);
	/**
	 * Instance of linkLangauage
	 */
	private Link linkAddLang;
	/**
	 * Member variable for description Label label
	 */
	protected Label lblTextField;
	/**
	 * Member variable for textdescription
	 */
	private Text textField;
	/**
	 * Instance of dialog title
	 */
	private String dialogTitle;
	/**
	 * Instance of CustomtextField
	 */
	private CustomTextFieldDialog dialogArea;
	/**
	 * Instance of dialogLabel.
	 */
	private String dialogLabel;

	/**
	 * Parameterizes constructor
	 * 
	 * @param parent
	 * @param style
	 */
	public CustomTextFieldComposite(final Composite parent, final int style) {
		super(parent, style);
		initGui();
		initListener();

	}

	/**
	 * method for initialize Graphical user interface
	 */
	private void initGui() {
		try {
			final GridLayout widgetContainerLayout = new GridLayout(2, false);
			widgetContainerLayout.marginRight = 0;
			widgetContainerLayout.marginLeft = 0;
			widgetContainerLayout.marginTop = 0;
			widgetContainerLayout.marginBottom = 0;
			widgetContainerLayout.marginWidth = 0;
			widgetContainerLayout.marginHeight = 2;
			this.setLayout(widgetContainerLayout);
			this.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
			createDescriptionControl();
		} catch (Exception e) {
			LOGGER.error("Unable to create UI elements", e);
		}
	}

	/**
	 * Method createDescriptionControl for creating topbar widget
	 * 
	 * @param widgetContainer
	 */
	private void createDescriptionControl() {
		try {
			this.textField = new Text(this, SWT.BORDER);
			this.textField.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));

			this.linkAddLang = new Link(this, SWT.FLAT);
			this.linkAddLang.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, true));
		} catch (Exception e) {
			LOGGER.error("Unable to create UI elements", e);
		}
	}

	/**
	 * Method initListner for Listener
	 */
	private void initListener() {
		this.linkAddLang.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent event) {
				final Link linkWidget = (Link) event.widget;
				openDialogs(linkWidget.getShell());
			}
		});
	}

	/**
	 * Method open dialog for open new dialog
	 * 
	 * @param composit
	 */
	private void openDialogs(final Shell shell) {
		dialogArea = new CustomTextFieldDialog(shell, this.getDialogTitle(), this.dialogLabel, this.getTextFieldLimit(),
				textField.getEditable()) {
			@Override
			protected void bindDialog() {
				bindDialogWidgets();
			}
		};
		dialogArea.open();
	}

	protected void bindDialogWidgets() {
		// Nothing
	}

	/**
	 * @return the linkAddLang
	 */
	public Link getLinkAddLang() {
		return linkAddLang;
	}

	/**
	 * @return the lblDescrition
	 */
	public Label getLblTextField() {
		return lblTextField;
	}

	/**
	 * @return the textfield
	 * 
	 */
	public Text getTextField() {
		return textField;
	}

	/**
	 * @return the dialogTitle
	 */
	public String getDialogTitle() {
		return dialogTitle;
	}

	/**
	 * @param dialogTitle
	 *            the dialogTitle to set
	 */
	public void setDialogTitle(final String dialogTitle) {
		this.dialogTitle = dialogTitle;
	}

	/**
	 * @return the dialogArea
	 */
	public CustomTextFieldDialog getDialogArea() {
		return dialogArea;
	}

	/**
	 * Gets the dialog label.
	 *
	 * @return the dialog label
	 */
	public String getDialogLabel() {
		return dialogLabel;
	}

	/**
	 * Sets the dialog label.
	 *
	 * @param dialogLabel
	 *            the new dialog label
	 */
	public void setDialogLabel(String dialogLabel) {
		this.dialogLabel = dialogLabel;
	}

	/**
	 * @return the textFieldLimit
	 */
	public int getTextFieldLimit() {
		return this.textField.getTextLimit();
	}

	/**
	 * @param textFieldLimit
	 *            the textFieldLimit to set
	 */
	public void setTextFieldLimit(int textFieldLimit) {
		this.textField.setTextLimit(textFieldLimit);
	}
}
