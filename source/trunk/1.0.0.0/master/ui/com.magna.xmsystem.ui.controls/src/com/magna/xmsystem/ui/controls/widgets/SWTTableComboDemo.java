package com.magna.xmsystem.ui.controls.widgets;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.MouseAdapter;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableItem;

/**
 * @author Steven Holzner
 * 
 */
public class SWTTableComboDemo {
  public static void main(String[] args) {
    Display display = new Display();
    Shell shell = new Shell(display);
    shell.setSize(300, 500);
    shell.setText("Button Example");
    shell.setLayout(new GridLayout());

    MagnaCustomCombo tableCombo = new MagnaCustomCombo(shell, SWT.BORDER | SWT.READ_ONLY);
    tableCombo.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));
    tableCombo.setVisibleItemCount(10);
    tableCombo.setEditable(false);
    Table table = tableCombo.getTable();
    for(int i=0;i<10;i++) {
    	TableItem item = new TableItem(table, SWT.NONE);
    	item.setText("ABC"+ i);
    	item = new TableItem(table, SWT.NONE);
    	item.setText("ACD"+ i);
    	item = new TableItem(table, SWT.NONE);
    	item.setText("BCD"+ i);
    }
    
    tableCombo.addSelectionListener(new SelectionListener() {
		
		@Override
		public void widgetSelected(SelectionEvent e) {
			System.out.println("Item Selected");
		}
		
		@Override
		public void widgetDefaultSelected(SelectionEvent e) {
			System.out.println("Item Selected");
		}
	});
    
    Button arrowControl = tableCombo.getArrowControl();
	arrowControl.addSelectionListener(new SelectionAdapter() {

		@Override
		public void widgetSelected(SelectionEvent arg0) {
			if(tableCombo.isDropped()) {
				System.out.println("focusssssssssed");
				/*System.out.println("selection Index : "+tableCombo.getSelectionIndex());
				tableCombo.select(-1);
				tableCombo.clearSelection();*/
				//table.removeAll();
			}			
		}
	});

    tableCombo.addMouseListener(new MouseAdapter() {

		@Override
		public void mouseDown(MouseEvent arg0) {
			System.out.println("focusssssssssed");
		}
	});
    
    shell.open();
    while (!shell.isDisposed()) {
      if (!display.readAndDispatch())
        display.sleep();
    }
    display.dispose();
  }
}