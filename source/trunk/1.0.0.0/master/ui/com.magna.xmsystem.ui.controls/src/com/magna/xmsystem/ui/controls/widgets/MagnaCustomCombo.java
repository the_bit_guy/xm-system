package com.magna.xmsystem.ui.controls.widgets;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.stream.Collectors;

import org.eclipse.swt.SWT;
import org.eclipse.swt.accessibility.ACC;
import org.eclipse.swt.accessibility.AccessibleAdapter;
import org.eclipse.swt.accessibility.AccessibleControlAdapter;
import org.eclipse.swt.accessibility.AccessibleControlEvent;
import org.eclipse.swt.accessibility.AccessibleEvent;
import org.eclipse.swt.accessibility.AccessibleTextAdapter;
import org.eclipse.swt.accessibility.AccessibleTextEvent;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.events.KeyAdapter;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.KeyListener;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.MouseAdapter;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.events.VerifyListener;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Layout;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.widgets.TypedListener;

import com.magna.xmsystem.dependencies.utils.OSValidator;
import com.magna.xmsystem.dependencies.utils.XMSystemUtil;

/**
 * Class for Magna custom combo.
 *
 * @author Chiranjeevi.Akula
 */
public class MagnaCustomCombo extends Composite {
	/** Member variable 'popup' for {@link Shell}. */
	private Shell popup;
	/** Member variable 'arrow' for {@link Button}. */
	private Button arrow;
	/** Member variable 'selected image' for {@link Label}. */
	private Label selectedImage;

	/** Member variable 'text' for {@link Text}. */
	private Text text;

	/** Member variable 'table' for {@link Table}. */
	private Table table;

	/** Member variable 'font' for {@link Font}. */
	private Font font;

	/** Member variable 'has focus' for {@link Boolean}. */
	private boolean hasFocus;

	/** Member variable 'visible item count' for {@link Int}. */
	private int visibleItemCount = 7;

	/** Member variable 'listener' for {@link Listener}. */
	private Listener listener;

	/** Member variable 'focus filter' for {@link Listener}. */
	private Listener focusFilter;

	/** Member variable 'display column index' for {@link Int}. */
	private int displayColumnIndex = 0;

	/** Member variable 'foreground' for {@link Color}. */
	private Color foreground;

	/** Member variable 'background' for {@link Color}. */
	private Color background;

	/** Member variable 'column widths' for {@link Int[]}. */
	private int[] columnWidths;

	/** Member variable 'table width percentage' for {@link Int}. */
	private int tableWidthPercentage = 100;

	/** Member variable 'show image within selection' for {@link Boolean}. */
	private boolean showImageWithinSelection = true;

	/** Member variable 'show color within selection' for {@link Boolean}. */
	private boolean showColorWithinSelection = true;

	/** Member variable 'show font within selection' for {@link Boolean}. */
	private boolean showFontWithinSelection = true;

	/** Member variable 'close pupup after selection' for {@link Boolean}. */
	private boolean closePupupAfterSelection = true;

	/**
	 * Constructor for MagnaCustomCombo Class.
	 *
	 * @param parent
	 *            {@link Composite}
	 * @param style
	 *            {@link int}
	 */
	public MagnaCustomCombo(Composite parent, int style) {
		super(parent, style = checkStyle(style));
		parent.getShell().addListener(SWT.Move, new Listener() {
			@Override
			public void handleEvent(Event event) {
				dropDown(false);
			}
		});

		// set the label style
		int textStyle = SWT.SINGLE;
		if ((style & SWT.READ_ONLY) != 0) {
			textStyle |= SWT.READ_ONLY;
		}
		if ((style & SWT.FLAT) != 0) {
			textStyle |= SWT.FLAT;
		}

		// set control background to white
		setBackground(Display.getCurrent().getSystemColor(SWT.COLOR_WHITE));

		// create label to hold image if necessary.
		selectedImage = new Label(this, SWT.NONE);
		selectedImage.setAlignment(SWT.RIGHT);
		selectedImage.setBackground(Display.getCurrent().getSystemColor(SWT.COLOR_WHITE));

		getLayout();

		// create the control to hold the display text of what the user
		// selected.
		text = new Text(this, textStyle);
		text.setBackground(Display.getCurrent().getSystemColor(SWT.COLOR_WHITE));

		// set the arrow style.
		int arrowStyle = SWT.ARROW | SWT.DOWN;
		if ((style & SWT.FLAT) != 0) {
			arrowStyle |= SWT.FLAT;
		}

		// create the down arrow button
		arrow = new Button(this, arrowStyle);

		createListeners();

		// set the listeners for this control
		int[] comboEvents = { SWT.Dispose, SWT.FocusIn, SWT.Move, SWT.Resize };
		for (int i = 0; i < comboEvents.length; i++) {
			this.addListener(comboEvents[i], listener);
		}

		int[] textEvents = { SWT.DefaultSelection, SWT.KeyDown, SWT.KeyUp, SWT.MenuDetect, SWT.Modify, SWT.MouseDown,
				SWT.MouseUp, SWT.MouseDoubleClick, SWT.MouseWheel, SWT.Traverse, SWT.FocusIn, SWT.Verify };
		for (int i = 0; i < textEvents.length; i++) {
			text.addListener(textEvents[i], listener);
		}

		// set the listeners for the arrow image
		int[] arrowEvents = { SWT.Selection, SWT.FocusIn, SWT.MenuDetect };
		for (int i = 0; i < arrowEvents.length; i++) {
			arrow.addListener(arrowEvents[i], listener);
		}

		// initialize the drop down
		createPopup(-1);

		initAccessible();

		addDisposeListener(new DisposeListener() {

			public void widgetDisposed(DisposeEvent e) {
				popup.dispose();
			}
		});
	}

	/**
	 * Method for Creates the listeners.
	 */
	private void createListeners() {

		// create new focus listener
		focusFilter = new Listener() {
			public void handleEvent(Event event) {
				if (isDisposed()) {
					return;
				}
				Shell shell = ((Control) event.widget).getShell();

				if (shell == MagnaCustomCombo.this.getShell()) {
					handleFocus(SWT.FocusOut);
				}
			}
		};

		// now add a listener to listen to the events we are interested in.
		listener = new Listener() {
			public void handleEvent(Event event) {
				if (isDisposed()) {
					return;
				}

				// check for a popup event
				if (popup == event.widget) {
					popupEvent(event);
					return;
				}

				if (text == event.widget) {
					textEvent(event);
					return;
				}

				// check for a table event
				if (table == event.widget) {
					tableEvent(event);
					return;
				}

				// check for arrow event
				if (arrow == event.widget) {
					arrowEvent(event);
					return;
				}

				// check for this widget's event
				if (MagnaCustomCombo.this == event.widget) {
					comboEvent(event);
					return;
				}

				// check for shell event
				if (getShell() == event.widget) {
					getDisplay().asyncExec(new Runnable() {
						public void run() {
							if (isDisposed()) {
								return;
							}
							handleFocus(SWT.FocusOut);
						}
					});
				}
			}
		};
	}

	/**
	 * Method for Check style.
	 *
	 * @param style
	 *            {@link int}
	 * @return the int {@link int}
	 */
	private static int checkStyle(int style) {
		int mask = SWT.BORDER | SWT.READ_ONLY | SWT.FLAT | SWT.LEFT_TO_RIGHT | SWT.RIGHT_TO_LEFT;
		return SWT.NO_FOCUS | (style & mask);
	}

	/**
	 * Method for Adds the modify listener.
	 *
	 * @param listener
	 *            {@link ModifyListener}
	 */
	public void addModifyListener(ModifyListener listener) {
		checkWidget();
		if (listener == null) {
			SWT.error(SWT.ERROR_NULL_ARGUMENT);
		}
		TypedListener typedListener = new TypedListener(listener);
		addListener(SWT.Modify, typedListener);
	}

	/**
	 * Method for Adds the selection listener.
	 *
	 * @param listener
	 *            {@link SelectionListener}
	 */
	public void addSelectionListener(SelectionListener listener) {
		checkWidget();
		if (listener == null) {
			SWT.error(SWT.ERROR_NULL_ARGUMENT);
		}

		TypedListener typedListener = new TypedListener(listener);
		addListener(SWT.Selection, typedListener);
		addListener(SWT.DefaultSelection, typedListener);
	}

	/**
	 * Method for Adds the text control key listener.
	 *
	 * @param listener
	 *            {@link KeyListener}
	 */
	public void addTextControlKeyListener(KeyListener listener) {
		checkWidget();
		if (listener == null) {
			SWT.error(SWT.ERROR_NULL_ARGUMENT);
		}
		text.addKeyListener(listener);
	}

	/**
	 * Method for Removes the text control key listener.
	 *
	 * @param listener
	 *            {@link KeyListener}
	 */
	public void removeTextControlKeyListener(KeyListener listener) {
		checkWidget();
		if (listener == null) {
			SWT.error(SWT.ERROR_NULL_ARGUMENT);
		}
		text.removeKeyListener(listener);
	}

	/**
	 * Method for Adds the verify listener.
	 *
	 * @param listener
	 *            {@link VerifyListener}
	 */
	public void addVerifyListener(VerifyListener listener) {
		checkWidget();
		if (listener == null) {
			SWT.error(SWT.ERROR_NULL_ARGUMENT);
		}
		TypedListener typedListener = new TypedListener(listener);
		addListener(SWT.Verify, typedListener);
	}

	/**
	 * Method for Arrow event.
	 *
	 * @param event
	 *            {@link Event}
	 */
	private void arrowEvent(Event event) {
		switch (event.type) {
			case SWT.FocusIn: {
				handleFocus(SWT.FocusIn);
				break;
			}
			case SWT.MouseDown: {
				Event mouseEvent = new Event();
				mouseEvent.button = event.button;
				mouseEvent.count = event.count;
				mouseEvent.stateMask = event.stateMask;
				mouseEvent.time = event.time;
				mouseEvent.x = event.x;
				mouseEvent.y = event.y;
				notifyListeners(SWT.MouseDown, mouseEvent);
				event.doit = mouseEvent.doit;
				break;
			}
			case SWT.MouseUp: {
				Event mouseEvent = new Event();
				mouseEvent.button = event.button;
				mouseEvent.count = event.count;
				mouseEvent.stateMask = event.stateMask;
				mouseEvent.time = event.time;
				mouseEvent.x = event.x;
				mouseEvent.y = event.y;
				notifyListeners(SWT.MouseUp, mouseEvent);
				event.doit = mouseEvent.doit;
				break;
			}
			case SWT.Selection: {
				text.setFocus();
				dropDown(!isDropped());
				break;
			}
			case SWT.MenuDetect : {
				text.setFocus();
				dropDown(!isDropped());
				break;
			}
		}
	}

	/**
	 * Method for Clear selection.
	 */
	public void clearSelection() {
		checkWidget();
		text.clearSelection();
		table.deselectAll();
	}

	/**
	 * Method for Combo event.
	 *
	 * @param event
	 *            {@link Event}
	 */
	private void comboEvent(Event event) {
		switch (event.type) {
		case SWT.Dispose:
			removeListener(SWT.Dispose, listener);
			notifyListeners(SWT.Dispose, event);
			event.type = SWT.None;

			if (popup != null && !popup.isDisposed()) {
				table.removeListener(SWT.Dispose, listener);
				popup.dispose();
			}
			Shell shell = getShell();
			shell.removeListener(SWT.Deactivate, listener);
			Display display = getDisplay();
			display.removeFilter(SWT.FocusIn, focusFilter);
			popup = null;
			text = null;
			table = null;
			arrow = null;
			selectedImage = null;
			break;
		case SWT.FocusIn:
			Control focusControl = getDisplay().getFocusControl();
			if (focusControl == arrow || focusControl == table) {
				return;
			}
			if (isDropped()) {
				table.setFocus();
			} else {
				text.setFocus();
			}
			break;
		case SWT.Move:
			dropDown(false);
			break;
		case SWT.Resize:
			internalLayout(false, true);
			break;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.swt.widgets.Control#computeSize(int, int, boolean)
	 */
	public Point computeSize(int wHint, int hHint, boolean changed) {
		checkWidget();

		int overallWidth = 0;
		int overallHeight = 0;
		int borderWidth = getBorderWidth();

		// use user defined values if they are specified.
		if (wHint != SWT.DEFAULT && hHint != SWT.DEFAULT) {
			overallWidth = wHint;
			overallHeight = hHint;
		} else {
			TableItem[] tableItems = table.getItems();

			GC gc = new GC(text);
			int spacer = gc.stringExtent(" ").x; //$NON-NLS-1$
			int maxTextWidth = gc.stringExtent(text.getText()).x;
			int colIndex = getDisplayColumnIndex();
			int maxImageHeight = 0;
			int currTextWidth = 0;

			// calculate the maximum text width and image height.
			for (int i = 0; i < tableItems.length; i++) {
				currTextWidth = gc.stringExtent(tableItems[i].getText(colIndex)).x;

				// take image into account if there is one for the tableitem.
				if (tableItems[i].getImage() != null) {
					currTextWidth += tableItems[i].getImage().getBounds().width;
					maxImageHeight = Math.max(tableItems[i].getImage().getBounds().height, maxImageHeight);
				}
				maxTextWidth = Math.max(currTextWidth, maxTextWidth);
			}

			gc.dispose();
			Point textSize = text.computeSize(SWT.DEFAULT, SWT.DEFAULT, changed);
			Point arrowSize = arrow.computeSize(SWT.DEFAULT, SWT.DEFAULT, changed);

			overallHeight = Math.max(textSize.y, arrowSize.y);
			overallHeight = Math.max(maxImageHeight, overallHeight);
			overallWidth = maxTextWidth + 2 * spacer + arrowSize.x + 2 * borderWidth;

			// use user specified if they were entered.
			if (wHint != SWT.DEFAULT) {
				overallWidth = wHint;
			}
			if (hHint != SWT.DEFAULT) {
				overallHeight = hHint;
			}
		}
		return new Point(overallWidth + 2 * borderWidth, overallHeight + 2 * borderWidth);
	}

	/**
	 * Method for Copy.
	 */
	public void copy() {
		checkWidget();
		text.copy();
	}

	/**
	 * Method for Creates the popup.
	 *
	 * @param selectionIndex
	 *            {@link int}
	 */
	void createPopup(int selectionIndex) {
		// create shell and table
		popup = new Shell(getShell(), SWT.NO_TRIM | SWT.ON_TOP);

		// create table
		table = new Table(popup, SWT.SINGLE | SWT.FULL_SELECTION);

		if (font != null) {
			table.setFont(font);
		}
		if (foreground != null) {
			table.setForeground(foreground);
		}
		if (background != null) {
			table.setBackground(background);
		}

		// Add popup listeners
		int[] popupEvents = { SWT.Close, SWT.Paint, SWT.Deactivate, SWT.Help };
		for (int i = 0; i < popupEvents.length; i++) {
			popup.addListener(popupEvents[i], listener);
		}

		// add table listeners
		/*
		 * int[] tableEvents = { SWT.MouseUp, SWT.Selection, SWT.Traverse,
		 * SWT.KeyDown, SWT.KeyUp, SWT.FocusIn, SWT.Dispose }; for (int i = 0; i
		 * < tableEvents.length; i++) { table.addListener(tableEvents[i],
		 * listener); }
		 */

		table.addMouseListener(new MouseAdapter() { // Hot Fix : Chiranjeevi
													// Akula
			@Override
			public void mouseDown(MouseEvent e) {
				selectIndex(table.getSelectionIndex());
			}
		});

		table.addKeyListener(new KeyAdapter() { // Hot Fix : Chiranjeevi Akula

			@Override
			public void keyPressed(KeyEvent event) {
				table.setFocus();
				if (event.character == SWT.CR) {
					if (table.getSelectionIndex() != -1) {
						selectIndex(table.getSelectionIndex());
					}
				}
			}
		});

		table.deselectAll(); // Hot Fix : Chiranjeevi Akula
		// set the selection
		/*
		 * if (selectionIndex != -1) { table.setSelection(selectionIndex); }
		 */
	}

	/**
	 * Method for Select index.
	 *
	 * @param index
	 *            {@link int}
	 */
	private void selectIndex(int index) {
		checkWidget();

		if (index == -1) {
			table.deselectAll();
			text.setText("");
			selectedImage.setImage(null);
			return;
		}

		if (0 <= index && index < table.getItemCount()) {
			refreshText(index);
			this.notifyListeners(SWT.Selection, new Event());
		}
	}

	/**
	 * Gets the arrow control.
	 *
	 * @return the arrow control
	 */
	public Button getArrowControl() {
		return arrow;
	}

	/**
	 * Method for Cut.
	 */
	public void cut() {
		checkWidget();
		text.cut();
	}

	/**
	 * Method for Drop down.
	 *
	 * @param drop
	 *            {@link boolean}
	 */
	void dropDown(boolean drop) {

		// if already dropped then return
		if (drop == isDropped()) {
			return;
		}

		// closing the dropDown
		if (!drop) {
			popup.setVisible(false);
			if (!isDisposed() && isFocusControl()) {
				text.setFocus();
			}
			return;
		}

		// if not visible then return
		if (!isVisible()) {
			return;
		}

		int selectionIndex = getSelectionIndex();
		if (selectionIndex != -1) { // Hot Fix : Chiranjeevi Akula
			table.select(selectionIndex);
		}

		updateSize();

		// set the popup visible
		popup.setVisible(true);

		// set focus on the table.
		table.setFocus(); // Hot Fix : Chiranjeevi Akula
	}

	/**
	 * Method for Update size.
	 */
	public void updateSize() {
		// get the size of the TableCombo.
		Point tableComboSize = getSize();

		// calculate the table height.
		int itemCount = table.getItemCount();
		itemCount = (itemCount == 0) ? visibleItemCount : Math.min(visibleItemCount, itemCount);
		int itemHeight = (table.getItemHeight() * itemCount);

		// add 1 to the table height if the table item count is less than the
		// visible item count.
		if (table.getItemCount() <= visibleItemCount) {
			itemHeight += 1;
		}

		if (itemCount <= visibleItemCount) {
			if (table.getHorizontalBar() != null && !table.getHorizontalBar().isVisible()) {
				itemHeight -= table.getHorizontalBar().getSize().y;
			}
		}

		// add height of header if the header is being displayed.
		if (table.getHeaderVisible()) {
			itemHeight += table.getHeaderHeight();
		}

		// get table column references
		TableColumn[] tableColumns = table.getColumns();
		int totalColumns = (tableColumns == null ? 0 : tableColumns.length);

		// check to make sure at least one column has been specified. if it
		// hasn't
		// then just create a blank one.
		if (table.getColumnCount() == 0) {
			new TableColumn(table, SWT.NONE);
			totalColumns = 1;
			tableColumns = table.getColumns();
		}

		int totalColumnWidth = 0;
		// now pack any columns that do not have a explicit value set for them.
		for (int colIndex = 0; colIndex < totalColumns; colIndex++) {
			if (!wasColumnWidthSpecified(colIndex)) {
				tableColumns[colIndex].pack();
			}
			totalColumnWidth += tableColumns[colIndex].getWidth();
		}

		// reset the last column's width to the preferred size if it has a
		// explicit value.
		int lastColIndex = totalColumns - 1;
		if (wasColumnWidthSpecified(lastColIndex)) {
			tableColumns[lastColIndex].setWidth(columnWidths[lastColIndex]);
		}

		// calculate the table size after making adjustments.
		Point tableSize = table.computeSize(SWT.DEFAULT, itemHeight, false);

		// calculate the table width and table height.
		double pct = tableWidthPercentage / 100d;
		int tableWidth = (int) (Math.max(tableComboSize.x - 2, tableSize.x) * pct);
		int tableHeight = tableSize.y;

		// add the width of a horizontal scrollbar to the table height if we are
		// not viewing the full table.
		if (tableWidthPercentage < 100) {
			tableHeight += table.getHorizontalBar().getSize().y;
		}

		// set the bounds on the table.
		table.setBounds(1, 1, tableWidth, tableHeight);

		// check to see if we can adjust the table width to by the amount the
		// vertical
		// scrollbar would have taken since the table auto allocates the space
		// whether
		// it is needed or not.
		if (!table.getVerticalBar().getVisible()
				&& tableSize.x - table.getVerticalBar().getSize().x >= tableComboSize.x - 2) {

			tableWidth = tableWidth - table.getVerticalBar().getSize().x;

			// reset the bounds on the table.
			table.setBounds(1, 1, tableWidth, tableHeight);
		}

		// adjust the last column to make sure that there is no empty space.
		autoAdjustColumnWidthsIfNeeded(tableColumns, tableWidth, totalColumnWidth);

		// set the table top index if there is a valid selection.
		int index = table.getSelectionIndex();
		if (index != -1) {
			table.setTopIndex(index);
		}

		// calculate popup dimensions.
		Display display = getDisplay();
		Rectangle tableRect = table.getBounds();
		Rectangle parentRect = display.map(getParent(), null, getBounds());
		Point comboSize = getSize();
		Rectangle displayRect = getMonitor().getClientArea();

		int overallWidth = 0;

		// now set what the overall width should be.
		if (tableWidthPercentage < 100) {
			overallWidth = tableRect.width + 2;
		} else {
			overallWidth = Math.max(comboSize.x, tableRect.width + 2);
		}

		int overallHeight = tableRect.height + 2;
		int x = parentRect.x;
		int y = parentRect.y + comboSize.y;
		if (y + overallHeight > displayRect.y + displayRect.height) {
			y = parentRect.y - overallHeight;
		}
		if (x + overallWidth > displayRect.x + displayRect.width) {
			x = displayRect.x + displayRect.width - tableRect.width;
		}

		// set the bounds of the popup
		popup.setBounds(x, y, overallWidth, overallHeight);
	}

	/**
	 * Gets the associated label.
	 *
	 * @return the associated label
	 */
	/*
	 * Return the Label immediately preceding the receiver in the z-order, or
	 * null if none.
	 */
	private Label getAssociatedLabel() {
		Control[] siblings = getParent().getChildren();
		for (int i = 0; i < siblings.length; i++) {
			if (siblings[i] == MagnaCustomCombo.this) {
				if (i > 0 && siblings[i - 1] instanceof Label) {
					return (Label) siblings[i - 1];
				}
			}
		}
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.swt.widgets.Composite#getChildren()
	 */
	public Control[] getChildren() {
		checkWidget();
		return new Control[0];
	}

	/**
	 * Gets the editable.
	 *
	 * @return the editable
	 */
	public boolean getEditable() {
		checkWidget();
		return text.getEditable();
	}

	/**
	 * Gets the item.
	 *
	 * @param index
	 *            {@link int}
	 * @return the item
	 */
	public String getItem(int index) {
		checkWidget();
		return table.getItem(index).getText(getDisplayColumnIndex());
	}

	/**
	 * Gets the item count.
	 *
	 * @return the item count
	 */
	public int getItemCount() {
		checkWidget();
		return table.getItemCount();
	}

	/**
	 * Gets the item height.
	 *
	 * @return the item height
	 */
	public int getItemHeight() {
		checkWidget();
		return table.getItemHeight();
	}

	/**
	 * Gets the items.
	 *
	 * @return the items
	 */
	public String[] getItems() {
		checkWidget();

		// get a list of the table items.
		TableItem[] tableItems = table.getItems();

		int totalItems = (tableItems == null ? 0 : tableItems.length);

		// create string array to hold the total number of items.
		String[] stringItems = new String[totalItems];

		int colIndex = getDisplayColumnIndex();

		// now copy the display string from the tableitems.
		for (int index = 0; index < totalItems; index++) {
			stringItems[index] = tableItems[index].getText(colIndex);
		}

		return stringItems;
	}

	/**
	 * Gets the selection.
	 *
	 * @return the selection
	 */
	public Point getSelection() {
		checkWidget();
		return text.getSelection();
	}

	/**
	 * Gets the selection index.
	 *
	 * @return the selection index
	 */
	public int getSelectionIndex() {
		checkWidget();
		// return table.getSelectionIndex();
		return getItemIndex(text.getText()); // Hot Fix: Chiranjeevi Akula
	}

	/**
	 * Gets the item index.
	 *
	 * @param text
	 *            {@link String}
	 * @return the item index
	 */
	private int getItemIndex(String text) {
		if (text != null) {
			TableItem[] items = table.getItems();
			for (TableItem item : items) {
				if (text.equals(item.getText())) {
					return table.indexOf(item);
				}
			}
		}
		return -1;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.swt.widgets.Widget#getStyle()
	 */
	public int getStyle() {
		checkWidget();

		int style = super.getStyle();
		style &= ~SWT.READ_ONLY;
		if (!text.getEditable()) {
			style |= SWT.READ_ONLY;
		}
		return style;
	}

	/**
	 * Gets the text.
	 *
	 * @return the text
	 */
	public String getText() {
		checkWidget();
		return text.getText();
	}

	/**
	 * Gets the text height.
	 *
	 * @return the text height
	 */
	public int getTextHeight() {
		checkWidget();
		return text.getLineHeight();
	}

	/**
	 * Gets the visible item count.
	 *
	 * @return the visible item count
	 */
	public int getVisibleItemCount() {
		checkWidget();
		return visibleItemCount;
	}

	/**
	 * Method for Handle focus.
	 *
	 * @param type
	 *            {@link int}
	 */
	private void handleFocus(int type) {
		switch (type) {
		case SWT.FocusIn: {
			if (hasFocus) {
				return;
			}
			if (getEditable()) {
				text.selectAll();
			}
			hasFocus = true;
			Shell shell = getShell();
			shell.removeListener(SWT.Deactivate, listener);
			shell.addListener(SWT.Deactivate, listener);
			Display display = getDisplay();
			display.removeFilter(SWT.FocusIn, focusFilter);
			display.addFilter(SWT.FocusIn, focusFilter);
			Event e = new Event();
			notifyListeners(SWT.FocusIn, e);
			break;
		}
		case SWT.FocusOut: {
			if (!hasFocus) {
				return;
			}
			Control focusControl = getDisplay().getFocusControl();
			if (focusControl == arrow || focusControl == table || focusControl == text) {
				return;
			}
			hasFocus = false;
			Shell shell = getShell();
			shell.removeListener(SWT.Deactivate, listener);
			Display display = getDisplay();
			display.removeFilter(SWT.FocusIn, focusFilter);
			Event e = new Event();
			notifyListeners(SWT.FocusOut, e);
			break;
		}
		}
	}

	/**
	 * Method for Index of.
	 *
	 * @param string
	 *            {@link String}
	 * @return the int {@link int}
	 */
	public int indexOf(String string) {
		checkWidget();
		if (string == null) {
			SWT.error(SWT.ERROR_NULL_ARGUMENT);
		}

		// get a list of the table items.
		TableItem[] tableItems = table.getItems();

		int totalItems = (tableItems == null ? 0 : tableItems.length);
		int colIndex = getDisplayColumnIndex();

		// now copy the display string from the tableitems.
		for (int index = 0; index < totalItems; index++) {
			if (string.equals(tableItems[index].getText(colIndex))) {
				return index;
			}
		}

		return -1;
	}

	/**
	 * Method for Index of.
	 *
	 * @param string
	 *            {@link String}
	 * @param start
	 *            {@link int}
	 * @return the int {@link int}
	 */
	public int indexOf(String string, int start) {
		checkWidget();
		if (string == null) {
			SWT.error(SWT.ERROR_NULL_ARGUMENT);
		}

		// get a list of the table items.
		TableItem[] tableItems = table.getItems();

		int totalItems = (tableItems == null ? 0 : tableItems.length);

		if (start < totalItems) {

			int colIndex = getDisplayColumnIndex();

			// now copy the display string from the tableitems.
			for (int index = start; index < totalItems; index++) {
				if (string.equals(tableItems[index].getText(colIndex))) {
					return index;
				}
			}
		}

		return -1;
	}

	/**
	 * Sets the show table lines.
	 *
	 * @param showTableLines
	 *            the new show table lines
	 */
	public void setShowTableLines(boolean showTableLines) {
		checkWidget();
		table.setLinesVisible(showTableLines);
	}

	/**
	 * Sets the show table header.
	 *
	 * @param showTableHeader
	 *            the new show table header
	 */
	public void setShowTableHeader(boolean showTableHeader) {
		checkWidget();
		table.setHeaderVisible(showTableHeader);
	}

	/**
	 * Method for Inits the accessible.
	 */
	void initAccessible() {
		AccessibleAdapter accessibleAdapter = new AccessibleAdapter() {
			public void getName(AccessibleEvent e) {
				String name = null;
				Label label = getAssociatedLabel();
				if (label != null) {
					name = stripMnemonic(text.getText());
				}
				e.result = name;
			}

			public void getKeyboardShortcut(AccessibleEvent e) {
				String shortcut = null;
				Label label = getAssociatedLabel();
				if (label != null) {
					String text = label.getText();
					if (text != null) {
						char mnemonic = _findMnemonic(text);
						if (mnemonic != '\0') {
							shortcut = "Alt+" + mnemonic; //$NON-NLS-1$
						}
					}
				}
				e.result = shortcut;
			}

			public void getHelp(AccessibleEvent e) {
				e.result = getToolTipText();
			}
		};

		getAccessible().addAccessibleListener(accessibleAdapter);
		text.getAccessible().addAccessibleListener(accessibleAdapter);
		table.getAccessible().addAccessibleListener(accessibleAdapter);

		arrow.getAccessible().addAccessibleListener(new AccessibleAdapter() {
			public void getName(AccessibleEvent e) {
				e.result = isDropped() ? SWT.getMessage("SWT_Close") : SWT.getMessage("SWT_Open"); //$NON-NLS-1$ //$NON-NLS-2$
			}

			public void getKeyboardShortcut(AccessibleEvent e) {
				e.result = "Alt+Down Arrow"; //$NON-NLS-1$
			}

			public void getHelp(AccessibleEvent e) {
				e.result = getToolTipText();
			}
		});

		getAccessible().addAccessibleTextListener(new AccessibleTextAdapter() {
			public void getCaretOffset(AccessibleTextEvent e) {
				e.offset = text.getCaretPosition();
			}

			public void getSelectionRange(AccessibleTextEvent e) {
				Point sel = text.getSelection();
				e.offset = sel.x;
				e.length = sel.y - sel.x;
			}
		});

		getAccessible().addAccessibleControlListener(new AccessibleControlAdapter() {
			public void getChildAtPoint(AccessibleControlEvent e) {
				Point testPoint = toControl(e.x, e.y);
				if (getBounds().contains(testPoint)) {
					e.childID = ACC.CHILDID_SELF;
				}
			}

			public void getLocation(AccessibleControlEvent e) {
				Rectangle location = getBounds();
				Point pt = getParent().toDisplay(location.x, location.y);
				e.x = pt.x;
				e.y = pt.y;
				e.width = location.width;
				e.height = location.height;
			}

			public void getChildCount(AccessibleControlEvent e) {
				e.detail = 0;
			}

			public void getRole(AccessibleControlEvent e) {
				e.detail = ACC.ROLE_COMBOBOX;
			}

			public void getState(AccessibleControlEvent e) {
				e.detail = ACC.STATE_NORMAL;
			}

			public void getValue(AccessibleControlEvent e) {
				e.result = text.getText();
			}
		});

		text.getAccessible().addAccessibleControlListener(new AccessibleControlAdapter() {
			public void getRole(AccessibleControlEvent e) {
				e.detail = text.getEditable() ? ACC.ROLE_TEXT : ACC.ROLE_LABEL;
			}
		});

		arrow.getAccessible().addAccessibleControlListener(new AccessibleControlAdapter() {
			public void getDefaultAction(AccessibleControlEvent e) {
				e.result = isDropped() ? SWT.getMessage("SWT_Close") : SWT.getMessage("SWT_Open"); //$NON-NLS-1$ //$NON-NLS-2$
			}
		});
	}

	/**
	 * Checks if is dropped.
	 *
	 * @return true, if is dropped
	 */
	public boolean isDropped() { // public menthod Hot fix Chiranjeevi Akula
		return popup.getVisible();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.swt.widgets.Control#isFocusControl()
	 */
	public boolean isFocusControl() {
		checkWidget();
		// if (label.isFocusControl () || arrow.isFocusControl () ||
		// table.isFocusControl () || popup.isFocusControl ()) {
		if (arrow.isFocusControl() || table.isFocusControl() || popup.isFocusControl()) {
			return true;
		}
		return super.isFocusControl();
	}

	/**
	 * Method for Internal layout.
	 *
	 * @param changed
	 *            {@link boolean}
	 * @param closeDropDown
	 *            {@link boolean}
	 */
	private void internalLayout(boolean changed, boolean closeDropDown) {
		if (closeDropDown && isDropped()) {
			dropDown(false);
		}
		Rectangle rect = getClientArea();
		int width = rect.width;
		int height = rect.height;
		Point arrowSize = arrow.computeSize(SWT.DEFAULT, height, changed);

		// calculate text vertical alignment.
		int textYPos = 0;
		Point textSize = text.computeSize(SWT.DEFAULT, SWT.DEFAULT);
		if (textSize.y < height) {
			textYPos = (height - textSize.y) / 2;
		}

		// does the selected entry have a image associated with it?
		if (selectedImage.getImage() == null) {
			// set image, text, and arrow boundaries
			selectedImage.setBounds(0, 0, 0, 0);
			text.setBounds(0, textYPos, width - arrowSize.x, textSize.y);
			arrow.setBounds(width - arrowSize.x, 0, arrowSize.x, arrowSize.y);
		} else {
			// calculate the amount of width left in the control after taking
			// into account the arrow selector
			int remainingWidth = width - arrowSize.x;
			Point imageSize = selectedImage.computeSize(SWT.DEFAULT, height, changed);
			int imageWidth = imageSize.x + 2;

			// handle the case where the image is larger than the available
			// space in the control.
			if (imageWidth > remainingWidth) {
				imageWidth = remainingWidth;
				remainingWidth = 0;
			} else {
				remainingWidth = remainingWidth - imageWidth;
			}

			// set the width of the text.
			int textWidth = remainingWidth;

			// set image, text, and arrow boundaries
			selectedImage.setBounds(0, 0, imageWidth, imageSize.y);
			text.setBounds(imageWidth, textYPos, textWidth, textSize.y);
			arrow.setBounds(imageWidth + textWidth, 0, arrowSize.x, arrowSize.y);
		}
	}

	/**
	 * Method for Table event.
	 *
	 * @param event
	 *            {@link Event}
	 */
	private void tableEvent(Event event) {
		switch (event.type) {
		case SWT.FocusIn: {
			handleFocus(SWT.FocusIn);
			break;
		}
		case SWT.MouseUp: {
			if (event.button != 1) {
				return;
			}
			if (closePupupAfterSelection) {
				dropDown(false);
			}
			break;
		}
		case SWT.Selection: {
			int index = table.getSelectionIndex();
			if (index == -1) {
				return;
			}

			// refresh the text.
			refreshText(index);

			// set the selection in the table.
			table.setSelection(index);

			Event e = new Event();
			e.time = event.time;
			e.stateMask = event.stateMask;
			e.doit = event.doit;
			notifyListeners(SWT.Selection, e);
			event.doit = e.doit;
			break;
		}
		case SWT.Traverse: {
			switch (event.detail) {
			case SWT.TRAVERSE_RETURN:
			case SWT.TRAVERSE_ESCAPE:
			case SWT.TRAVERSE_ARROW_PREVIOUS:
			case SWT.TRAVERSE_ARROW_NEXT:
				event.doit = false;
				break;
			case SWT.TRAVERSE_TAB_NEXT:
			case SWT.TRAVERSE_TAB_PREVIOUS:
				event.doit = text.traverse(event.detail);
				event.detail = SWT.TRAVERSE_NONE;
				if (event.doit) {
					dropDown(false);
				}
				return;
			}
			Event e = new Event();
			e.time = event.time;
			e.detail = event.detail;
			e.doit = event.doit;
			e.character = event.character;
			e.keyCode = event.keyCode;
			notifyListeners(SWT.Traverse, e);
			event.doit = e.doit;
			event.detail = e.detail;
			break;
		}
		case SWT.KeyUp: {
			Event e = new Event();
			e.time = event.time;
			e.character = event.character;
			e.keyCode = event.keyCode;
			e.stateMask = event.stateMask;
			notifyListeners(SWT.KeyUp, e);
			break;
		}
		case SWT.KeyDown: {
			if (event.character == SWT.ESC) {
				// Escape key cancels popup list
				dropDown(false);
			}
			if ((event.stateMask & SWT.ALT) != 0
					&& (event.keyCode == SWT.ARROW_UP || event.keyCode == SWT.ARROW_DOWN)) {
				dropDown(false);
			}
			if (event.character == SWT.CR) {
				// Enter causes default selection
				dropDown(false);
				Event e = new Event();
				e.time = event.time;
				e.stateMask = event.stateMask;
				notifyListeners(SWT.DefaultSelection, e);
			}
			// At this point the widget may have been disposed.
			// If so, do not continue.
			if (isDisposed()) {
				break;
			}
			Event e = new Event();
			e.time = event.time;
			e.character = event.character;
			e.keyCode = event.keyCode;
			e.stateMask = event.stateMask;
			notifyListeners(SWT.KeyDown, e);
			break;

		}
		}
	}

	/**
	 * Method for Paste.
	 */
	public void paste() {
		checkWidget();
		text.paste();
	}

	/**
	 * Method for Popup event.
	 *
	 * @param event
	 *            {@link Event}
	 */
	private void popupEvent(Event event) {
		switch (event.type) {
		case SWT.Paint:
			// draw rectangle around table
			Rectangle tableRect = table.getBounds();
			event.gc.setForeground(getDisplay().getSystemColor(SWT.COLOR_LIST_SELECTION));
			event.gc.drawRectangle(0, 0, tableRect.width + 1, tableRect.height + 1);
			break;
		case SWT.Close:
			event.doit = false;
			dropDown(false);
			break;
		case SWT.Deactivate:
			/*
			 * Bug in GTK. When the arrow button is pressed the popup control
			 * receives a deactivate event and then the arrow button receives a
			 * selection event. If we hide the popup in the deactivate event,
			 * the selection event will show it again. To prevent the popup from
			 * showing again, we will let the selection event of the arrow
			 * button hide the popup. In Windows, hiding the popup during the
			 * deactivate causes the deactivate to be called twice and the
			 * selection event to be disappear.
			 */
			/*if (!"carbon".equals(SWT.getPlatform())) {
				Point point = arrow.toControl(getDisplay().getCursorLocation());
				Point size = arrow.getSize();
				Rectangle rect = new Rectangle(0, 0, size.x, size.y);
				if (!rect.contains(point)) {
					dropDown(false);
				}
			} else {
				dropDown(false);
			}*/
			dropDown(false);
			break;

		case SWT.Help:
			if (isDropped()) {
				dropDown(false);
			}
			Composite comp = MagnaCustomCombo.this;
			do {
				if (comp.getListeners(event.type) != null && comp.getListeners(event.type).length > 0) {
					comp.notifyListeners(event.type, event);
					break;
				}
				comp = comp.getParent();
			} while (null != comp);
			break;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.swt.widgets.Control#redraw()
	 */
	public void redraw() {
		super.redraw();
		text.redraw();
		arrow.redraw();
		if (popup.isVisible()) {
			table.redraw();
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.swt.widgets.Control#redraw(int, int, int, int, boolean)
	 */
	public void redraw(int x, int y, int width, int height, boolean all) {
		super.redraw(x, y, width, height, true);
	}

	/**
	 * Method for Removes the modify listener.
	 *
	 * @param listener
	 *            {@link ModifyListener}
	 */
	public void removeModifyListener(ModifyListener listener) {
		checkWidget();
		if (listener == null) {
			SWT.error(SWT.ERROR_NULL_ARGUMENT);
		}
		removeListener(SWT.Modify, listener);
	}

	/**
	 * Method for Removes the selection listener.
	 *
	 * @param listener
	 *            {@link SelectionListener}
	 */
	public void removeSelectionListener(SelectionListener listener) {
		checkWidget();
		if (listener == null) {
			SWT.error(SWT.ERROR_NULL_ARGUMENT);
		}
		removeListener(SWT.Selection, listener);
		removeListener(SWT.DefaultSelection, listener);
	}

	/**
	 * Method for Removes the verify listener.
	 *
	 * @param listener
	 *            {@link VerifyListener}
	 */
	public void removeVerifyListener(VerifyListener listener) {
		checkWidget();
		if (listener == null) {
			SWT.error(SWT.ERROR_NULL_ARGUMENT);
		}
		removeListener(SWT.Verify, listener);
	}

	/**
	 * Method for Select.
	 *
	 * @param index
	 *            {@link int}
	 */
	public void select(int index) {
		checkWidget();

		// deselect if a value of -1 is passed in.
		if (index == -1) {
			table.deselectAll();
			text.setText("");
			selectedImage.setImage(null);
			return;
		}

		if (0 <= index && index < table.getItemCount()) {
			if (index != getSelectionIndex()) {

				// refresh the text field and image label
				refreshText(index);

				// select the row in the table.
				table.setSelection(index);
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.swt.widgets.Control#setBackground(org.eclipse.swt.graphics.
	 * Color)
	 */
	public void setBackground(Color color) {
		super.setBackground(color);
		background = color;
		if (text != null) {
			text.setBackground(color);
		}
		if (selectedImage != null) {
			selectedImage.setBackground(color);
		}
		if (table != null) {
			table.setBackground(color);
		}
		if (arrow != null) {
			arrow.setBackground(color);
		}
	}

	/**
	 * Sets the editable.
	 *
	 * @param editable
	 *            the new editable
	 */
	public void setEditable(boolean editable) {
		checkWidget();
		text.setEditable(editable);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.swt.widgets.Control#setEnabled(boolean)
	 */
	public void setEnabled(boolean enabled) {
		super.setEnabled(enabled);
		if (popup != null) {
			popup.setVisible(false);
		}
		if (selectedImage != null) {
			selectedImage.setEnabled(enabled);
		}
		if (text != null) {
			text.setEnabled(enabled);
		}
		if (arrow != null) {
			arrow.setEnabled(enabled);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.swt.widgets.Composite#setFocus()
	 */
	public boolean setFocus() {
		checkWidget();
		if (!isEnabled() || !isVisible()) {
			return false;
		}
		if (isFocusControl()) {
			return true;
		}

		return text.setFocus();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.swt.widgets.Control#setFont(org.eclipse.swt.graphics.Font)
	 */
	public void setFont(Font font) {
		super.setFont(font);
		this.font = font;
		text.setFont(font);
		table.setFont(font);
		internalLayout(true, true);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.swt.widgets.Control#setForeground(org.eclipse.swt.graphics.
	 * Color)
	 */
	public void setForeground(Color color) {
		super.setForeground(color);
		foreground = color;
		if (text != null) {
			text.setForeground(color);
		}
		if (table != null) {
			table.setForeground(color);
		}
		if (arrow != null) {
			arrow.setForeground(color);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.swt.widgets.Composite#setLayout(org.eclipse.swt.widgets.
	 * Layout)
	 */
	public void setLayout(Layout layout) {
		checkWidget();
		return;
	}

	/**
	 * Sets the table visible.
	 *
	 * @param visible
	 *            the new table visible
	 */
	public void setTableVisible(boolean visible) {
		checkWidget();
		dropDown(visible);
	}

	/**
	 * Sets the selection.
	 *
	 * @param selection
	 *            the new selection
	 */
	public void setSelection(Point selection) {
		checkWidget();
		if (selection == null) {
			SWT.error(SWT.ERROR_NULL_ARGUMENT);
		}
		text.setSelection(selection.x, selection.y);
	}

	/**
	 * Sets the text.
	 *
	 * @param string
	 *            the new text
	 */
	public void setText(String string) {
		checkWidget();
		if (string == null) {
			SWT.error(SWT.ERROR_NULL_ARGUMENT);
		}

		// find the index of the given string.
		int index = indexOf(string);

		if (index == -1) {
			table.deselectAll();
			text.setText(string);
			return;
		}

		// select the text and table row.
		select(index);
	}

	/**
	 * Sets the text limit.
	 *
	 * @param limit
	 *            the new text limit
	 */
	public void setTextLimit(int limit) {
		checkWidget();
		text.setTextLimit(limit);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.swt.widgets.Control#setToolTipText(java.lang.String)
	 */
	public void setToolTipText(String tipText) {
		checkWidget();
		super.setToolTipText(tipText);
		if (selectedImage != null) {
			selectedImage.setToolTipText(tipText);
		}
		if (text != null) {
			text.setToolTipText(tipText);
		}
		if (arrow != null) {
			arrow.setToolTipText(tipText);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.swt.widgets.Control#setVisible(boolean)
	 */
	public void setVisible(boolean visible) {
		super.setVisible(visible);
		/*
		 * At this point the widget may have been disposed in a FocusOut event.
		 * If so then do not continue.
		 */
		if (isDisposed()) {
			return;
		}
		// TEMPORARY CODE
		if (popup == null || popup.isDisposed()) {
			return;
		}
		if (!visible) {
			popup.setVisible(false);
		}
	}

	/**
	 * Sets the visible item count.
	 *
	 * @param count
	 *            the new visible item count
	 */
	public void setVisibleItemCount(int count) {
		checkWidget();
		if (count > 0) {
			visibleItemCount = count;
		}
	}

	/**
	 * Method for Strip mnemonic.
	 *
	 * @param string
	 *            {@link String}
	 * @return the string {@link String}
	 */
	private String stripMnemonic(String string) {
		int index = 0;
		int length = string.length();
		do {
			while ((index < length) && (string.charAt(index) != '&')) {
				index++;
			}
			if (++index >= length) {
				return string;
			}
			if (string.charAt(index) != '&') {
				return string.substring(0, index - 1) + string.substring(index, length);
			}
			index++;
		} while (index < length);
		return string;
	}

	/**
	 * Method for Define columns.
	 *
	 * @param columnHeaders
	 *            {@link String[]}
	 */
	public void defineColumns(String[] columnHeaders) {
		if (columnHeaders != null && columnHeaders.length > 0) {
			defineColumnsInternal(columnHeaders, null, columnHeaders.length);
		}
	}

	/**
	 * Method for Define columns.
	 *
	 * @param columnBounds
	 *            {@link int[]}
	 */
	public void defineColumns(int[] columnBounds) {
		this.columnWidths = columnBounds;

		if (columnBounds != null && columnBounds.length > 0) {
			defineColumnsInternal(null, columnBounds, columnBounds.length);
		}
	}

	/**
	 * Method for Define columns.
	 *
	 * @param numberOfColumnsToCreate
	 *            {@link int}
	 */

	public void defineColumns(int numberOfColumnsToCreate) {
		if (numberOfColumnsToCreate > 0) {
			defineColumnsInternal(null, null, numberOfColumnsToCreate);
		}

	}

	/**
	 * Method for Define columns.
	 *
	 * @param columnHeaders
	 *            {@link String[]}
	 * @param columnBounds
	 *            {@link int[]}
	 */
	public void defineColumns(String[] columnHeaders, int[] columnBounds) {
		if (columnHeaders != null || columnBounds != null) {
			int total = columnHeaders == null ? 0 : columnHeaders.length;
			if (columnBounds != null && columnBounds.length > total) {
				total = columnBounds.length;
			}

			this.columnWidths = columnBounds;

			// define the columns
			defineColumnsInternal(columnHeaders, columnBounds, total);
		}
	}

	/**
	 * Method for Define columns internal.
	 *
	 * @param columnHeaders
	 *            {@link String[]}
	 * @param columnBounds
	 *            {@link int[]}
	 * @param totalColumnsToBeCreated
	 *            {@link int}
	 */
	private void defineColumnsInternal(String[] columnHeaders, int[] columnBounds, int totalColumnsToBeCreated) {

		checkWidget();

		int totalColumnHeaders = columnHeaders == null ? 0 : columnHeaders.length;
		int totalColBounds = columnBounds == null ? 0 : columnBounds.length;

		if (totalColumnsToBeCreated > 0) {

			for (int index = 0; index < totalColumnsToBeCreated; index++) {
				TableColumn column = new TableColumn(table, SWT.NONE);

				if (index < totalColumnHeaders) {
					column.setText(columnHeaders[index]);
				}

				if (index < totalColBounds) {
					column.setWidth(columnBounds[index]);
				}

				column.setResizable(true);
				column.setMoveable(true);
			}
		}
	}

	/**
	 * Sets the table width percentage.
	 *
	 * @param ddWidthPct
	 *            the new table width percentage
	 */
	public void setTableWidthPercentage(int ddWidthPct) {
		checkWidget();

		// don't accept invalid input.
		if (ddWidthPct > 0 && ddWidthPct <= 100) {
			this.tableWidthPercentage = ddWidthPct;
		}
	}

	/**
	 * Sets the display column index.
	 *
	 * @param displayColumnIndex
	 *            the new display column index
	 */
	public void setDisplayColumnIndex(int displayColumnIndex) {
		checkWidget();

		if (displayColumnIndex >= 0) {
			this.displayColumnIndex = displayColumnIndex;
		}
	}

	/**
	 * Sets the close popup after selection.
	 *
	 * @param closePopupAfterSelection
	 *            the new close popup after selection
	 */
	public void setClosePopupAfterSelection(boolean closePopupAfterSelection) {
		this.closePupupAfterSelection = closePopupAfterSelection;
	}

	/**
	 * Gets the display column index.
	 *
	 * @return the display column index
	 */
	private int getDisplayColumnIndex() {
		// make sure the requested column index is valid.
		return (displayColumnIndex <= (table.getColumnCount() - 1) ? displayColumnIndex : 0);
	}

	/**
	 * Method for Find mnemonic.
	 *
	 * @param string
	 *            {@link String}
	 * @return the char {@link char}
	 */
	/*
	 * Return the lowercase of the first non-'&' character following an '&'
	 * character in the given string. If there are no '&' characters in the
	 * given string, return '\0'.
	 */
	private char _findMnemonic(String string) {
		if (string == null) {
			return '\0';
		}
		int index = 0;
		int length = string.length();
		do {
			while (index < length && string.charAt(index) != '&') {
				index++;
			}
			if (++index >= length) {
				return '\0';
			}
			if (string.charAt(index) != '&') {
				return Character.toLowerCase(string.charAt(index));
			}
			index++;
		} while (index < length);
		return '\0';
	}

	/**
	 * Method for Refresh text.
	 *
	 * @param index
	 *            {@link int}
	 */
	private void refreshText(int index) {

		// get a reference to the selected TableItem
		TableItem tableItem = table.getItem(index);

		// get the TableItem index to use for displaying the text.
		int colIndexToUse = getDisplayColumnIndex();

		// set image if requested
		if (showImageWithinSelection) {
			// set the selected image
			selectedImage.setImage(tableItem.getImage(colIndexToUse));

			// refresh the layout of the widget
			internalLayout(false, closePupupAfterSelection);
		}

		// set color if requested
		if (showColorWithinSelection) {
			text.setForeground(tableItem.getForeground(colIndexToUse));
		}

		// set font if requested
		if (showFontWithinSelection) {
			// set the selected font
			text.setFont(tableItem.getFont(colIndexToUse));
		}

		// set the label text.
		text.setText(tableItem.getText(colIndexToUse));
		text.selectAll();
	}

	/**
	 * Sets the show image within selection.
	 *
	 * @param showImageWithinSelection
	 *            the new show image within selection
	 */
	public void setShowImageWithinSelection(boolean showImageWithinSelection) {
		checkWidget();
		this.showImageWithinSelection = showImageWithinSelection;
	}

	/**
	 * Sets the show color within selection.
	 *
	 * @param showColorWithinSelection
	 *            the new show color within selection
	 */
	public void setShowColorWithinSelection(boolean showColorWithinSelection) {
		checkWidget();
		this.showColorWithinSelection = showColorWithinSelection;
	}

	/**
	 * Sets the show font within selection.
	 *
	 * @param showFontWithinSelection
	 *            the new show font within selection
	 */
	public void setShowFontWithinSelection(boolean showFontWithinSelection) {
		checkWidget();
		this.showFontWithinSelection = showFontWithinSelection;
	}

	/**
	 * Gets the table.
	 *
	 * @return the table
	 */
	public Table getTable() {
		checkWidget();
		return table;
	}

	/**
	 * Method for Was column width specified.
	 *
	 * @param columnIndex
	 *            {@link int}
	 * @return true, if successful
	 */
	private boolean wasColumnWidthSpecified(int columnIndex) {
		return (columnWidths != null && columnWidths.length > columnIndex && columnWidths[columnIndex] != SWT.DEFAULT);
	}

	/**
	 * Method for Text event.
	 *
	 * @param event
	 *            {@link Event}
	 */
	void textEvent(Event event) {
		switch (event.type) {
		case SWT.FocusIn: {
			handleFocus(SWT.FocusIn);
			break;
		}
		case SWT.DefaultSelection: {
			dropDown(false);
			Event e = new Event();
			e.time = event.time;
			e.stateMask = event.stateMask;
			notifyListeners(SWT.DefaultSelection, e);
			break;
		}
		case SWT.KeyDown: {
			Event keyEvent = new Event();
			keyEvent.time = event.time;
			keyEvent.character = event.character;
			keyEvent.keyCode = event.keyCode;
			keyEvent.stateMask = event.stateMask;
			notifyListeners(SWT.KeyDown, keyEvent);
			if (isDisposed()) {
				break;
			}
			event.doit = keyEvent.doit;
			if (!event.doit) {
				break;
			}
			if (/* event.keyCode == SWT.ARROW_UP || */event.keyCode == SWT.ARROW_DOWN) {
				event.doit = false;

				boolean dropped = isDropped();
				text.selectAll();
				if (!dropped) {
					setFocus();
				}
				dropDown(!dropped);
				break;

				/*
				 * if ((event.stateMask & SWT.ALT) != 0) { boolean dropped =
				 * isDropped(); text.selectAll(); if (!dropped) { setFocus(); }
				 * dropDown(!dropped); break; }
				 * 
				 * int oldIndex = table.getSelectionIndex(); if (event.keyCode
				 * == SWT.ARROW_UP) { select(Math.max(oldIndex - 1, 0)); } else
				 * { select(Math.min(oldIndex + 1, getItemCount() - 1)); } if
				 * (oldIndex != table.getSelectionIndex()) { Event e = new
				 * Event(); e.time = event.time; e.stateMask = event.stateMask;
				 * notifyListeners(SWT.Selection, e); } if (isDisposed()) {
				 * break; }
				 */
			}

			// Further work : Need to add support for incremental search in
			// pop up list as characters typed in text widget
			break;
		}
		case SWT.KeyUp: {
			Event e = new Event();
			e.time = event.time;
			e.character = event.character;
			e.keyCode = event.keyCode;
			e.stateMask = event.stateMask;
			notifyListeners(SWT.KeyUp, e);
			event.doit = e.doit;
			break;
		}
		case SWT.MenuDetect: {
			Event e = new Event();
			e.time = event.time;
			notifyListeners(SWT.MenuDetect, e);
			break;
		}
		case SWT.Modify: {
			table.deselectAll();
			Event e = new Event();
			e.time = event.time;
			notifyListeners(SWT.Modify, e);
			break;
		}
		case SWT.MouseDown: {
			Event mouseEvent = new Event();
			mouseEvent.button = event.button;
			mouseEvent.count = event.count;
			mouseEvent.stateMask = event.stateMask;
			mouseEvent.time = event.time;
			mouseEvent.x = event.x;
			mouseEvent.y = event.y;
			notifyListeners(SWT.MouseDown, mouseEvent);
			if (isDisposed()) {
				break;
			}
			event.doit = mouseEvent.doit;
			if (!event.doit) {
				break;
			}
			if (event.button != 1) {
				return;
			}
			if (text.getEditable()) {
				return;
			}
			boolean dropped = isDropped();
			text.selectAll();
			if (!dropped) {
				setFocus();
			}
			dropDown(!dropped);
			break;
		}
		case SWT.MouseUp: {
			Event mouseEvent = new Event();
			mouseEvent.button = event.button;
			mouseEvent.count = event.count;
			mouseEvent.stateMask = event.stateMask;
			mouseEvent.time = event.time;
			mouseEvent.x = event.x;
			mouseEvent.y = event.y;
			notifyListeners(SWT.MouseUp, mouseEvent);
			if (isDisposed()) {
				break;
			}
			event.doit = mouseEvent.doit;
			if (!event.doit) {
				break;
			}
			if (event.button != 1) {
				return;
			}
			if (text.getEditable()) {
				return;
			}
			text.selectAll();
			break;
		}
		case SWT.MouseDoubleClick: {
			Event mouseEvent = new Event();
			mouseEvent.button = event.button;
			mouseEvent.count = event.count;
			mouseEvent.stateMask = event.stateMask;
			mouseEvent.time = event.time;
			mouseEvent.x = event.x;
			mouseEvent.y = event.y;
			notifyListeners(SWT.MouseDoubleClick, mouseEvent);
			break;
		}
		case SWT.MouseWheel: {
			Event keyEvent = new Event();
			keyEvent.time = event.time;
			keyEvent.keyCode = event.count > 0 ? SWT.ARROW_UP : SWT.ARROW_DOWN;
			keyEvent.stateMask = event.stateMask;
			notifyListeners(SWT.KeyDown, keyEvent);
			if (isDisposed()) {
				break;
			}
			event.doit = keyEvent.doit;
			if (!event.doit) {
				break;
			}
			if (event.count != 0) {
				event.doit = false;
				int oldIndex = table.getSelectionIndex();
				if (event.count > 0) {
					select(Math.max(oldIndex - 1, 0));
				} else {
					select(Math.min(oldIndex + 1, getItemCount() - 1));
				}
				if (oldIndex != table.getSelectionIndex()) {
					Event e = new Event();
					e.time = event.time;
					e.stateMask = event.stateMask;
					notifyListeners(SWT.Selection, e);
				}
				if (isDisposed()) {
					break;
				}
			}
			break;
		}
		case SWT.Traverse: {
			switch (event.detail) {
			case SWT.TRAVERSE_ARROW_PREVIOUS:
			case SWT.TRAVERSE_ARROW_NEXT:
				// The enter causes default selection and
				// the arrow keys are used to manipulate the list contents so
				// do not use them for traversal.
				event.doit = false;
				break;
			case SWT.TRAVERSE_TAB_PREVIOUS:
				event.doit = traverse(SWT.TRAVERSE_TAB_PREVIOUS);
				event.detail = SWT.TRAVERSE_NONE;
				return;
			}
			Event e = new Event();
			e.time = event.time;
			e.detail = event.detail;
			e.doit = event.doit;
			e.character = event.character;
			e.keyCode = event.keyCode;
			notifyListeners(SWT.Traverse, e);
			event.doit = e.doit;
			event.detail = e.detail;
			break;
		}
		case SWT.Verify: {
			Event e = new Event();
			e.text = event.text;
			e.start = event.start;
			e.end = event.end;
			e.character = event.character;
			e.keyCode = event.keyCode;
			e.stateMask = event.stateMask;
			notifyListeners(SWT.Verify, e);
			event.doit = e.doit;
			break;
		}
		}
	}

	/**
	 * Method for Auto adjust column widths if needed.
	 *
	 * @param tableColumns
	 *            {@link TableColumn[]}
	 * @param totalAvailWidth
	 *            {@link int}
	 * @param totalColumnWidthUsage
	 *            {@link int}
	 */
	private void autoAdjustColumnWidthsIfNeeded(TableColumn[] tableColumns, int totalAvailWidth,
			int totalColumnWidthUsage) {

		if (OSValidator.isWindows()) { // Hot Fix : Chiranjeevi Akula
			int scrollBarSize = 0;
			int totalColumns = (tableColumns == null ? 0 : tableColumns.length);

			// determine if the vertical scroll bar needs to be taken into
			// account
			if (table.getVerticalBar().getVisible()) {
				scrollBarSize = (table.getVerticalBar() == null ? 0 : table.getVerticalBar().getSize().x);
			}

			// is there any extra space that the table is not using?
			if (totalAvailWidth > totalColumnWidthUsage + scrollBarSize) {
				int totalAmtToBeAllocated = (totalAvailWidth - totalColumnWidthUsage - scrollBarSize);

				// add unused space to the last column.
				if (totalAmtToBeAllocated > 0) {
					tableColumns[totalColumns - 1]
							.setWidth(tableColumns[totalColumns - 1].getWidth() + totalAmtToBeAllocated);
				}
			}
		}
	}

	/**
	 * Gets the text control.
	 *
	 * @return the text control
	 */
	public Text getTextControl() {
		checkWidget();
		return text;
	}

	/**
	 * Show item.
	 *
	 * @param itemText the item text
	 */
	public void showItem(final String itemText) {
		if (XMSystemUtil.isEmpty(itemText)) {
			return;
		}
		int itemCount = table.getItemCount();
		for (int index = 0; index < itemCount; index++) {
			TableItem tableItem = table.getItem(index);
			String text = tableItem.getText();
			if (text.equals(itemText)) {
				table.showItem(tableItem);
				table.select(index);
				break;
			}
		}
	}

	/**
	 * Gets the popup.
	 *
	 * @return the popup
	 */
	public Shell getPopup() {
		return popup;
	}
	
	public void setItemsWithImages(final Map<String, Object> items) {
		if (items == null) {
			return;
		}
		if (items.size() <= 0) {
			return;
		}
		Map<String, TableItem> alreadyPresent = new TreeMap<>();
		List<Integer> removeIndex = new ArrayList<>();
		List<String> tableComboElements = new ArrayList<>();
		final Table table = this.getTable();
		int itemCount = table.getItemCount();
		if (itemCount == 0 && items.size() > 0) {
			if (table.getColumnCount() <= 0) {
				new TableColumn(table, SWT.NONE);
			}
			Set<String> keySet = items.keySet();
			for (String key : keySet) {
				Object itemImage = items.get(key);
				TableItem item = new TableItem(table, SWT.NONE);
				if (!XMSystemUtil.isEmpty(key)) {
					item.setText(key);
					if (itemImage instanceof Image) {
						item.setImage((Image) itemImage);
					}
				}
			}
		} else {
			for (int index = 0; index < itemCount; index++) {
				TableItem tableItem = table.getItem(index);
				String text = tableItem.getText();
				if (!items.containsKey(text)) {
					// Remove this object
					removeIndex.add(index);
				} else {
					alreadyPresent.put(text, tableItem);
					tableComboElements.add(text);
				}
			}

			final Iterator<Integer> descendingIterator = removeIndex.stream()
					.collect(Collectors.toCollection(LinkedList::new)).descendingIterator();
			while (descendingIterator.hasNext()) {
				Integer index = (Integer) descendingIterator.next();
				table.remove(index);
			}
			List<String> keyList = Arrays.asList(items.keySet().toArray(new String[items.size()]));
			for (String key : keyList) {
				if (!tableComboElements.contains(key)) {
					int index = keyList.indexOf(key);
					Object itemImage = items.get(key);
					TableItem item = new TableItem(table, SWT.NONE, index);
					if (!XMSystemUtil.isEmpty(key)) {
						item.setText(key);
						if (itemImage instanceof Image) {
							item.setImage((Image) itemImage);
						}
					}
				} else {
					TableItem tableItem;
					if ((tableItem = alreadyPresent.get(key)) != null) {
						Object itemImage = items.get(key);
						if (itemImage instanceof Image) {
							tableItem.setImage((Image) itemImage);
						}
					}
				}
			}
		}
		updateSelectedImage();
	}


	/**
	 * Update selected image when all items are already updated
	 */
	public void updateSelectedImage() {
		int selectionIndex = getSelectionIndex();
		if (selectionIndex != -1) {
			TableItem item = getTable().getItem(selectionIndex);
			Image image = item.getImage();
			if (image != null && !image.isDisposed()) {
				this.selectedImage.setImage(image);
			}
		}
	}
	
	/**
	 * Update selected image.
	 *
	 * @param selectedImageL the selected image
	 */
	public void updateSelectedImage(final Image selectedImageL) {
		if (selectedImageL != null && !selectedImageL.isDisposed()) {
			this.selectedImage.setImage(selectedImageL);
		}
	}
}
