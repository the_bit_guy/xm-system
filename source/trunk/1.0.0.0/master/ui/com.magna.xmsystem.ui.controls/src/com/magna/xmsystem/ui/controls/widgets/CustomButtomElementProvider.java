package com.magna.xmsystem.ui.controls.widgets;

import org.eclipse.e4.ui.css.core.dom.IElementProvider;
import org.eclipse.e4.ui.css.core.engine.CSSEngine;
import org.w3c.dom.Element;

@SuppressWarnings("restriction")
public class CustomButtomElementProvider implements IElementProvider {

	@Override
	public Element getElement(Object element, CSSEngine engine) {
		if (element instanceof CustomButton) {
			return new CustomButtonElementAdapter((CustomButton) element, engine);
		}
		return null;
	}
}
