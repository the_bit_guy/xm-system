package com.magna.xmsystem.xmenu.ui.rearrangeapps;

import java.util.List;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.layout.GridDataFactory;
import org.eclipse.jface.layout.GridLayoutFactory;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.magna.xmsystem.dependencies.utils.XMSystemUtil;
import com.magna.xmsystem.xmenu.message.Message;
import com.magna.xmsystem.xmenu.ui.model.ProjectApplication;
import com.magna.xmsystem.xmenu.ui.model.UserApplication;
import com.magna.xmsystem.xmenu.ui.utils.CommonConstants;
import com.magna.xmsystem.xmenu.ui.utils.XmMenuUtil;

/**
 * The Class ReArrangeApplicationsDialog.
 * 
 * @author subash.janarthanan
 * 
 */
public class ReArrangeApplicationsDialog extends Dialog {

	/** The Constant LOGGER. */
	private static final Logger LOGGER = LoggerFactory.getLogger(ReArrangeApplicationsDialog.class);

	/** The parent shell. */
	private Shell parentShell;

	/** The messages. */
	private Message messages;

	/** The lbl user applications. */
	protected Label lblUserApplications;

	/** The lbl project applications. */
	protected Label lblProjectApplications;

	/** The grp user applications. */
	private Group grpUserApplications;

	/** The grp project applications. */
	private Group grpProjectApplications;

	/** The user app table viewer. */
	private UserAppTableViewer userAppTableViewer;

	/** The project app table viewer. */
	private ProjectAppTableViewer projectAppTableViewer;

	/** The down btn selection listener. */
	private SelectionListener projectAppBtnDownSelectionListener;

	/** The up btn selection listener. */
	private SelectionListener projectAppBtnUpSelectionListener;
	
	/** The down btn selection listener. */
	private SelectionListener userAppBtnDownSelectionListener;

	/** The up btn selection listener. */
	private SelectionListener userAppBtnUpSelectionListener;

	/** The user app list. */
	private List<UserApplication> userAppList;

	/** The project app list. */
	private List<ProjectApplication> projectAppList;

	/**
	 * Instantiates a new re arrange applications dialog.
	 *
	 * @param parentShell
	 *            the parent shell
	 * @param messages
	 *            the messages
	 * @param userAppList
	 *            the user app list 2
	 * @param projectAppList
	 *            the project app list 2
	 */
	public ReArrangeApplicationsDialog(final Shell parentShell, final Message messages,
			final List<UserApplication> userAppList, final List<ProjectApplication> projectAppList) {
		super(parentShell);
		this.parentShell = parentShell;
		this.messages = messages;
		this.userAppList = userAppList;
		this.projectAppList = projectAppList;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.jface.window.Window#configureShell(org.eclipse.swt.widgets.
	 * Shell)
	 */
	@Override
	protected void configureShell(final Shell newShell) {
		try {
			super.configureShell(newShell);
			newShell.setText(messages.reaarangeDialogTitle);
			newShell.setParent(this.parentShell);
		} catch (Exception e) {
			LOGGER.error("Exception occurred while configuring ReArrangeApplicationsDialog shell!", e); //$NON-NLS-1$
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.jface.dialogs.Dialog#createButtonsForButtonBar(org.eclipse.
	 * swt.widgets.Composite)
	 */
	@Override
	protected void createButtonsForButtonBar(final Composite parent) {
		parent.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));
		parent.setData("org.eclipse.e4.ui.css.id", "MyCSSTagForComposite");

		final Button resetBtn = createButton(parent, IDialogConstants.NO_ID, messages.reaarangeDialogResetToDefaultsBtn, true);
		resetBtn.addSelectionListener(new SelectionAdapter() {

			@Override
			public void widgetSelected(SelectionEvent selectionevent) {
				XmMenuUtil.getInstance().sortUserAppsAndItsChild(userAppList);
				XmMenuUtil.getInstance().sortProjectAppsAndItsChild(projectAppList);
				userAppTableViewer.refresh();
				projectAppTableViewer.refresh();
			}
		});

		Label spacer = new Label(parent, SWT.NONE);
		spacer.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));
		spacer.setData("org.eclipse.e4.ui.css.id", "MyCSSTagForComposite");

		GridLayout layout = (GridLayout) parent.getLayout();
		layout.numColumns++;
		layout.makeColumnsEqualWidth = false;

		
		super.createButtonsForButtonBar(parent);
		final Button importBtn = getButton(IDialogConstants.OK_ID);
		importBtn.setText(messages.reaarangeDialogSaveOrderBtn);
		importBtn.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));
		
		final Button cancelButton = getButton(IDialogConstants.CANCEL_ID);
		cancelButton.setText(messages.reaarangeDialogCancelBtn);
	}
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.jface.dialogs.Dialog#createDialogArea(org.eclipse.swt.widgets
	 * .Composite)
	 */
	@Override
	protected Control createDialogArea(final Composite parent) {
		try {
			parent.setData("org.eclipse.e4.ui.css.id", "MyCSSTagForComposite");
			final Composite widgetContainer = new Composite(parent, SWT.BORDER);
			widgetContainer.setLayout(new GridLayout(1, false));
			GridDataFactory.fillDefaults().grab(true, true).align(SWT.FILL, SWT.FILL).hint(350, 400).applyTo(widgetContainer);
			widgetContainer.setData("org.eclipse.e4.ui.css.id", "MyCSSTagForComposite");
			initGUI(widgetContainer);
			parent.getShell().setLocation(parent.getLocation());
		} catch (Exception e) {
			LOGGER.error("Unable to create ReArrangeApplicationsDialog UI elements", e);
		}
		return parent;
	}

	/**
	 * Init GUI.
	 *
	 * @param parent
	 *            the parent
	 */
	private void initGUI(final Composite parent) {
		Composite userAppMainComposite = new Composite(parent, SWT.NONE);
		userAppMainComposite.setLayout(new GridLayout(2, false));
		GridData layoutData = new GridData(SWT.FILL, SWT.FILL, true, true);
		layoutData.heightHint = 200;
		layoutData.minimumHeight = 150;
		userAppMainComposite.setLayoutData(layoutData);
		userAppMainComposite.setData("org.eclipse.e4.ui.css.id", "MyCSSTagForComposite");
		
		// Group for User Applications table
		this.grpUserApplications = new Group(userAppMainComposite, SWT.NONE);
		GridLayoutFactory.fillDefaults().numColumns(1).applyTo(this.grpUserApplications);
		this.grpUserApplications.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		this.grpUserApplications.setText(messages.userApplicationsLabel);
		grpUserApplications.setData("org.eclipse.e4.ui.css.id", "MyCSSTagForComposite");

		final Composite userAppTblComposite = new Composite(this.grpUserApplications, SWT.NONE);
		userAppTblComposite.setLayout(new GridLayout(1, false));
		userAppTblComposite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		userAppTblComposite.setData("org.eclipse.e4.ui.css.id", "MyCSSTagForComposite");
		
		this.userAppTableViewer = new UserAppTableViewer(userAppTblComposite);
		this.userAppTableViewer.setLabelProvider(new UserAppTableLableProvider());
		this.userAppTableViewer.setContentProvider(ArrayContentProvider.getInstance());
		this.userAppTableViewer.setInput(userAppList);

		// Button composite for userApp table
		final Composite userAppButtonComposite = new Composite(userAppMainComposite, SWT.NONE);
		userAppButtonComposite.setLayout(new GridLayout(1, false));
		userAppButtonComposite.setData("org.eclipse.e4.ui.css.id", "MyCSSTagForComposite");

		final GridData buttonGridData = new GridData();
		buttonGridData.widthHint = 30;
		
		final Button userAppBtnUp = new Button(userAppButtonComposite, SWT.PUSH);
		userAppBtnUp.setEnabled(false);
		userAppBtnUp.setImage(XMSystemUtil.getInstance().getImage(this.getClass(), "icons/arrow_up.gif"));
		userAppBtnUp.setLayoutData(buttonGridData);

		final Button userAppBtnDown = new Button(userAppButtonComposite, SWT.PUSH);
		userAppBtnDown.setEnabled(false);
		userAppBtnDown.setImage(XMSystemUtil.getInstance().getImage(this.getClass(), "icons/arrow_down.gif"));
		userAppBtnDown.setLayoutData(buttonGridData);

		Composite projectAppMainComposite = new Composite(parent, SWT.NONE);
		projectAppMainComposite.setLayout(new GridLayout(2, false));
		projectAppMainComposite.setLayoutData(layoutData);
		projectAppMainComposite.setData("org.eclipse.e4.ui.css.id", "MyCSSTagForComposite");
		
		// Group for Project Applications table
		this.grpProjectApplications = new Group(projectAppMainComposite, SWT.NONE);
		this.grpProjectApplications.setText(messages.projectApplicationsLabel);
		GridLayoutFactory.fillDefaults().numColumns(1).applyTo(this.grpProjectApplications);
		this.grpProjectApplications.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		grpProjectApplications.setData("org.eclipse.e4.ui.css.id", "MyCSSTagForComposite");

		final Composite projectAppTblComposite = new Composite(this.grpProjectApplications, SWT.NONE);
		projectAppTblComposite.setLayout(new GridLayout(1, false));
		projectAppTblComposite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		projectAppTblComposite.setData("org.eclipse.e4.ui.css.id", "MyCSSTagForComposite");
		
		this.projectAppTableViewer = new ProjectAppTableViewer(projectAppTblComposite);
		this.projectAppTableViewer.setLabelProvider(new ProjectAppTableLableProvider());
		this.projectAppTableViewer.setContentProvider(ArrayContentProvider.getInstance());
		this.projectAppTableViewer.setInput(projectAppList);

		// Button composite for projectApp table
		final Composite projectAppButtonComposite = new Composite(projectAppMainComposite, SWT.NONE);
		projectAppButtonComposite.setLayout(new GridLayout(1, false));
		projectAppButtonComposite.setData("org.eclipse.e4.ui.css.id", "MyCSSTagForComposite");

		final Button projectAppBtnUp = new Button(projectAppButtonComposite, SWT.PUSH);
		projectAppBtnUp.setEnabled(false);
		projectAppBtnUp.setImage(XMSystemUtil.getInstance().getImage(this.getClass(), "icons/arrow_up.gif"));
		projectAppBtnUp.setLayoutData(buttonGridData);

		final Button projectAppBtnDown = new Button(projectAppButtonComposite, SWT.PUSH);
		projectAppBtnDown.setEnabled(false);
		projectAppBtnDown.setImage(XMSystemUtil.getInstance().getImage(this.getClass(), "icons/arrow_down.gif"));
		projectAppBtnDown.setLayoutData(buttonGridData);
		
		// Selection listener for userApp/projectApp button composite 
		projectAppBtnUpSelectionListener = new ActionsUpButtonSelectionListener(projectAppTableViewer, userAppList, projectAppList);
		projectAppBtnDownSelectionListener = new ActionsDownButtonSelectionListener(projectAppTableViewer, userAppList, projectAppList);
		
		userAppBtnUpSelectionListener = new ActionsUpButtonSelectionListener(userAppTableViewer, userAppList, projectAppList);
		userAppBtnDownSelectionListener = new ActionsDownButtonSelectionListener(userAppTableViewer, userAppList, projectAppList);
		
		projectAppBtnUp.addSelectionListener(projectAppBtnUpSelectionListener);
		projectAppBtnDown.addSelectionListener(projectAppBtnDownSelectionListener);
		
		userAppBtnUp.addSelectionListener(userAppBtnUpSelectionListener);
		userAppBtnDown.addSelectionListener(userAppBtnDownSelectionListener);
		
		userAppTableViewer.addSelectionChangedListener(new ISelectionChangedListener() {
			
			@Override
			public void selectionChanged(SelectionChangedEvent selectionchangedevent) {
				userAppBtnUp.setEnabled(true);
				userAppBtnDown.setEnabled(true);			
			}
		});

		projectAppTableViewer.addSelectionChangedListener(new ISelectionChangedListener() {
			
			@Override
			public void selectionChanged(SelectionChangedEvent selectionchangedevent) {
				projectAppBtnUp.setEnabled(true);
				projectAppBtnDown.setEnabled(true);			
			}
		});
	}
	
	/* (non-Javadoc)
	 * @see org.eclipse.jface.dialogs.Dialog#okPressed()
	 */
	@Override
	protected void okPressed() {
		XmMenuUtil.getInstance().setINEclipsePrefs(
				XMSystemUtil.getProjectName() + CommonConstants.ORDERED_PROJECT_KEY.PROJECT_USER_APPLICATION,
				userAppList);
		XmMenuUtil.getInstance().setINEclipsePrefs(
				XMSystemUtil.getProjectName() + CommonConstants.ORDERED_PROJECT_KEY.PROJECT_PROJECT_APPLICATION,
				projectAppList);
		setReturnCode(OK);
		super.okPressed();
	}
	
	/* (non-Javadoc)
	 * @see org.eclipse.jface.dialogs.Dialog#cancelPressed()
	 */
	@Override
	protected void cancelPressed() {
		setReturnCode(CANCEL);
		super.cancelPressed();
	}
	
	/* (non-Javadoc)
	 * @see org.eclipse.jface.dialogs.Dialog#isResizable()
	 */
	@Override
	protected boolean isResizable() {
	    return true;
	}
}
