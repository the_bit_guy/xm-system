package com.magna.xmsystem.xmenu.ui.rearrangeapps;

import org.eclipse.jface.viewers.ILabelProvider;
import org.eclipse.jface.viewers.ILabelProviderListener;
import org.eclipse.swt.graphics.Image;

import com.magna.xmsystem.dependencies.utils.XMSystemUtil;
import com.magna.xmsystem.xmenu.ui.model.Icon;
import com.magna.xmsystem.xmenu.ui.model.UserApplication;
import com.magna.xmsystem.xmenu.ui.utils.XmMenuUtil;

public class UserAppTableLableProvider  implements ILabelProvider{
	
	public UserAppTableLableProvider() {
		super();
	}

	@Override
	public void addListener(ILabelProviderListener arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean isLabelProperty(Object arg0, String arg1) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void removeListener(ILabelProviderListener arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Image getImage(Object element) {
		if (element instanceof UserApplication) {
			final Icon icon = ((UserApplication) element).getIcon();
			final boolean active = ((UserApplication) element).isActive();
			Image image = XmMenuUtil.getInstance().getImage(icon, active);
			return image;
		}
		return null;
	}

	@Override
	public String getText(Object element) {		
		if (element instanceof UserApplication) {
			UserApplication userApplication = (UserApplication) element;				
			String name = userApplication.getName(XmMenuUtil.getInstance().getCurrentLocaleEnum());
			if (!XMSystemUtil.isEmpty(name)) {
				return name;
			}
			return userApplication.getName();	
		}
		
		return null;
	}

}
