package com.magna.xmsystem.xmenu.ui.utils;

/**
 * The Class CommonConstants.
 * 
 * @author subash.janarthanan
 * 
 */
public class CommonConstants {

	/** The Constant XMENU_UI_BUNDLE. */
	public static final String XMENU_UI_BUNDLE = "bundleclass://com.magna.xmsystem.xmenu.ui"; //$NON-NLS-1$
	
	/** The Constant XMMENU_WINDOW_LABEL. */
	public static final String WINDOW_SCRIPT_BAT_EXT = ".bat";

	/** The Constant LINUX_SCRIPT_SH_EXT. */
	public static final String LINUX_SCRIPT_SH_EXT = ".sh";
	
	/** The Constant LINUX_SCRIPT_KSH_EXT. */
	public static final String LINUX_SCRIPT_KSH_EXT = ".ksh";
	
	/** The Constant EMPTY_STR. */
	public static final String EMPTY_STR = "";
	
	/** The Constant APP_WINDOW_X_COORDINATE. */
	public static final int APP_WINDOW_X_COORDINATE = 550;
	
	/** The Constant APP_WINDOW_Y_COORDINATE. */
	public static final int APP_WINDOW_Y_COORDINATE = 150;
	
	/** The Constant APP_WINDOW_DEFAULT_WIDTH. */
	public static final int APP_WINDOW_DEFAULT_WIDTH = 500;
	
	/** The Constant APP_WINDOW_DEFAULT_HEIGHT. */
	public static final int APP_WINDOW_DEFAULT_HEIGHT = 600;

	/**
	 * The Class EVENT_BROKER.
	 */
	public static class EVENT_BROKER {

		/** The Constant STATUSBAR_MESSAGE. */
		public static final String STATUSBAR_MESSAGE = "statusbar";
		
		/** The Constant CONSOLE_MESSAGE. */
		public static final String CONSOLE_MESSAGE = "livemessage";

	}

	/**
	 * The Class TOOLBAR.
	 */
	public static class TOOLBAR {

		/** The Constant USAERAPPS_ID. */
		public static final String USAERAPPS_ID = "com.magna.xmsystem.xmenu.ui.toolbar.userapps";
		
		/** The Constant PROJECTAPPS_ID. */
		public static final String PROJECTAPPS_ID = "com.magna.xmsystem.xmenu.ui.toolbar.projectapps";

	}
	
	/**
	 * The Class MAINMENU.
	 */
	public static class MAINMENU {

		/** The Constant USERMENU_ID. */
		public static final String USERMENU_ID = "com.magna.xmsystem.xmenu.ui.mainmenu.usermenu";
		
		/** The Constant SEPARATOR_ID. */
		public static final String SEPARATOR_ID = "com.magna.xmsystem.xmenu.ui.menuseparator.separator1";
		
		/** The Constant XMENU_ADMINSMENU_ID. */
		public static final String XMENU_ADMINSMENU_ID = "com.magna.xmsystem.xmenu.ui.mainmenu.admin";
		
		/** The Constant XMENU_ADMINSMENU_SWITCHUSER_ID. */
		public static final String XMENU_ADMINSMENU_SWITCHUSER_ID = "com.magna.xmsystem.xmenu.ui.handledmenuitem.switchuser";
		
		/** The Constant XMENU_USERMENU_DUMMY_ID. */
		public static final String XMENU_USERMENU_DUMMY_ID = "com.magna.xmsystem.xmenu.ui.dynamicmenucontribution.usermenu.dummy";
	}
	
	/**
	 * The Class STARTSCRIPT.
	 */
	public static class STARTSCRIPT {
		
		/** The Constant START. */
		public static final String START = "1";
		
		/** The Constant END. */
		public static final String END = "0";
	}
	
	/**
	 * The Class PART_ID.
	 */
	public static class PART_ID {
		
		/** The Constant XMENU_ID. */
		public static final String XMENU_ID = "com.magna.xmsystem.xmenu.ui.part.xmenu";
		
	}
	
	/**
	 * The Class PREFERENCE_KEY.
	 */
	public static class PREFERENCE_KEY {
		
		/** The Constant ADMIN_MENU_ACCESS_USERNAME. */
		public static final String ADMIN_MENU_ACCESS_USERNAME = "adminMenuAccessUsername";
		
		/** The Constant ADMIN_MENU_ACCESS_TKT. */
		public static final String ADMIN_MENU_ACCESS_TKT = "adminMenuAccessTicket";
		
		/** The Constant USERNAME. */
		public static final String USERNAME = "username";
		
		/** The Constant USER_TKT. */
		public static final String USER_TKT = "userTicket";
		
		/** The Constant SITE. */
		public static final String SITE = "site";
		
		/** The Constant ADMINISTRATION_AREA. */
		public static final String ADMINISTRATION_AREA = "administrationArea";
		
		/** The Constant PROJECT. */
		public static final String PROJECT = "projects";
		
		/** The Constant USER_APPLICATIONS. */
		public static final String USER_APPLICATIONS = "userApplications";
		
		/** The Constant PROJECT_APPLICATIONS. */
		public static final String PROJECT_APPLICATIONS = "projectApplications";
		
		/** The Constant START_APPLICATIONS. */
		public static final String START_APPLICATIONS = "startApplications";
		
		/** The Constant IS_HORIZONTAL_VIEW. */
		public static final String HORIZONTAL_VIEW = "horizontalView";
		
		/** The Constant USERSETTINGS. */
		public static final String USERSETTINGS = "usersettings";
	}
	
	/**
	 * The Class USER_SETTINGS.
	 */
	public static class USER_SETTINGS {
		/** The Constant HOST_NAME. */
		public static final String HOST_NAME = "HOST_NAME";
		/** The Constant WINDOW_X. */
		public static final String WINDOW_X = "WINDOW_X";
		/** The Constant WINDOW_Y. */
		public static final String WINDOW_Y = "WINDOW_Y";
		/** The Constant WINDOW_W. */
		public static final String WINDOW_W = "WINDOW_W";
		/** The Constant WINDOW_H. */
		public static final String WINDOW_H = "WINDOW_H";		
		/** The Constant LANGUAGE. */
		public static final String LANGUAGE = "LANGUAGE";
	}
	
	/**
	 * The Class ORDERED_PROJECT_KEY.
	 */
	public static class ORDERED_PROJECT_KEY {
		
		/** The Constant PROJECT_USER_APPLICATION. */
		public static final String PROJECT_USER_APPLICATION = "userapplication";

		/** The Constant PROJECT_PROJECT_APPLICATION. */
		public static final String PROJECT_PROJECT_APPLICATION = "projectapplication";
	}
	
	/**
	 * The Class LIVE_MESSAGE_EXE_TIME_INTERVALS.
	 */
	public static class LIVE_MESSAGE_EXE_TIME_INTERVALS {
		
		/** The Constant INITIAL_TIME_INTERVAL. */
		public static final long INITIAL_TIME_INTERVAL = 30000;  // thirty seconds (milliseconds)

		/** The Constant CONSTANT_TIME_INTERVAL. */
		public static final long CONSTANT_TIME_INTERVAL = 300000;  // five minute (milliseconds)
	}
	
	/**
	 * The Class RegularExpressions.
	 */
	public static class RegularExpressions {
		/** The Constant URL. */
		public static final String URL = "^(https?|ftp|file)://[-a-zA-Z0-9+&@#/%?=~_|!:,.;]*[-a-zA-Z0-9+&@#/%=~_|]";
		//public static final String URL ="(http:\\/\\/www\\.|https:\\/\\/www\\.|http:\\/\\/|https:\\/\\/)?[a-z0-9]+([\\-\\.]{1}[a-z0-9]+)*\\.[a-z]{2,5}(:[0-9]{1,5})?(\\/.*)?";
	}
}
