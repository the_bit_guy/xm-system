package com.magna.xmsystem.xmenu.ui.rearrangeapps;

import java.util.Collections;
import java.util.List;

import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableItem;

import com.magna.xmsystem.xmenu.ui.model.ProjectApplication;
import com.magna.xmsystem.xmenu.ui.model.UserApplication;

/**
 * The listener interface for receiving actionsUpButtonSelection events. The
 * class that is interested in processing a actionsUpButtonSelection event
 * implements this interface, and the object created with that class is
 * registered with a component using the component's
 * <code>addActionsUpButtonSelectionListener<code> method. When the
 * actionsUpButtonSelection event occurs, that object's appropriate method is
 * invoked.
 *
 * @see ActionsUpButtonSelectionEvent
 * 
 * @author subash.janarthanan
 */
public class ActionsUpButtonSelectionListener extends SelectionAdapter {

	/** The table viewer. */
	private TableViewer tableViewer;

	/** The user app list. */
	private List<UserApplication> userAppList;

	/** The project app list. */
	private List<ProjectApplication> projectAppList;

	/**
	 * Instantiates a new actions up button selection listener.
	 *
	 * @param tableViewer
	 *            the table viewer
	 * @param userAppList
	 *            the user app list
	 * @param projectAppList
	 *            the project app list
	 */
	public ActionsUpButtonSelectionListener(final TableViewer tableViewer, final List<UserApplication> userAppList,
			final List<ProjectApplication> projectAppList) {
		this.tableViewer = tableViewer;
		this.userAppList = userAppList;
		this.projectAppList = projectAppList;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.swt.events.SelectionAdapter#widgetSelected(org.eclipse.swt.
	 * events.SelectionEvent)
	 */
	@Override
	public void widgetSelected(final SelectionEvent event) {
		final ISelection rowSelection = tableViewer.getSelection();
		final int selectionIndex = tableViewer.getTable().getSelectionIndex();
		final int totalCount = tableViewer.getTable().getItemCount();
		if (rowSelection instanceof IStructuredSelection) {
			final IStructuredSelection rowStSelection = (IStructuredSelection) rowSelection;
			final Object selectedRowObject = rowStSelection.getFirstElement();
			if (selectedRowObject instanceof ProjectApplication) {
				ProjectApplication currentRowData = (ProjectApplication) selectedRowObject;
				ProjectApplication previousRowData = (ProjectApplication) getNextRowData(selectionIndex);
				if (previousRowData != null) {
					Collections.swap(projectAppList, selectionIndex, selectionIndex - 1);
					tableViewer.setSelection(new StructuredSelection(tableViewer.getElementAt((selectionIndex - 1 + totalCount) % totalCount)), true);
					tableViewer.setSelection(new StructuredSelection(currentRowData));
				}
			} else if (selectedRowObject instanceof UserApplication) {
				UserApplication currentRowData = (UserApplication) selectedRowObject;
				UserApplication previousRowData = (UserApplication) getNextRowData(selectionIndex);
				if (previousRowData != null) {
					Collections.swap(userAppList, selectionIndex, selectionIndex - 1);
					tableViewer.setSelection(new StructuredSelection(tableViewer.getElementAt((selectionIndex - 1 + totalCount) % totalCount)), true);
					tableViewer.setSelection(new StructuredSelection(currentRowData));
				}
			}
		}
		
		tableViewer.getTable().setFocus();
		tableViewer.refresh();
	}

	/**
	 * Gets the next row data.
	 *
	 * @param selectionIndex
	 *            the selection index
	 * @return the next row data
	 */
	private Object getNextRowData(final int selectionIndex) {
		final Table table = tableViewer.getTable();
		if (selectionIndex > 0) {
			TableItem item = table.getItem(selectionIndex - 1);
			if (item != null) {
				Object data = item.getData();
				return data;
			}
		}
		return null;
	}
}
