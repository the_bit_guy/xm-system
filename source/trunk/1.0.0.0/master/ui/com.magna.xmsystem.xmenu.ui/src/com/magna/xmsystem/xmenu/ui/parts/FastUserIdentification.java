package com.magna.xmsystem.xmenu.ui.parts;

import javax.inject.Inject;

import org.eclipse.e4.core.services.nls.Translation;
import org.eclipse.e4.ui.model.application.MApplication;
import org.eclipse.e4.ui.model.application.ui.basic.MWindow;
import org.eclipse.e4.ui.workbench.modeling.EModelService;
import org.eclipse.swt.widgets.Display;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.magna.xmsystem.dependencies.utils.XMSystemUtil;
import com.magna.xmsystem.xmadmin.restclient.util.RestClientUtil;
import com.magna.xmsystem.xmenu.message.Message;
import com.magna.xmsystem.xmenu.ui.utils.CaxMenuObjectModelUtil;
import com.magna.xmsystem.xmenu.ui.utils.CommonConstants;

/**
 * The Class FastUserIdentification.
 * 
 * @author subash.janarthanan
 * 
 */
public class FastUserIdentification {
	
	/** The Constant LOGGER. */
	private static final Logger LOGGER = LoggerFactory.getLogger(FastUserIdentification.class);
	
	/** The application. */
	@Inject
	private MApplication application;
	
	/** The model service. */
	@Inject
	private EModelService modelService;
	
	/** Member variable for messages. */
	@Inject
	@Translation
	private Message message;
	
	/**
	 * Method for Update.
	 */
	public void update() {
		try {
			final String systemUserName = RestClientUtil.getInstance().getUserName();
			final String hostName = XMSystemUtil.getHostName();
			final CaxMenuObjectModelUtil instance = CaxMenuObjectModelUtil.getInstance();
			final String siteName = instance.getSiteName();
			final String adminAreaName = instance.getAdminAreaName();
			final String projectName = instance.getProjectName();

			Display.getDefault().asyncExec(new Runnable() {

				@Override
				public void run() {
					String mainWindowLabel = message.windowTitleLabel + " " + message.caxmenuVersion;
					String userDetails = CommonConstants.EMPTY_STR;
					if (!XMSystemUtil.isEmpty(systemUserName)) {
						userDetails += systemUserName;
					}
					if (!XMSystemUtil.isEmpty(hostName)) {
						userDetails += "@" + hostName;
					}
					if (!XMSystemUtil.isEmpty(siteName)) {
						userDetails += "/" + siteName;
					}
					if (!XMSystemUtil.isEmpty(adminAreaName)) {
						userDetails += "/" + adminAreaName;
					}
					if (!XMSystemUtil.isEmpty(projectName)) {
						userDetails += "/" + projectName;
					}
					if (!XMSystemUtil.isEmpty(userDetails)) {
						mainWindowLabel += " (" + userDetails + ")";
					}

					final MWindow window = (MWindow) modelService.find("org.eclipse.e4.window.main", application);
					window.setLabel(mainWindowLabel);
				}
			});
		} catch (Exception e) {
			LOGGER.error("Exception while getting the user name!", e); //$NON-NLS-1$
		}
	}
}
