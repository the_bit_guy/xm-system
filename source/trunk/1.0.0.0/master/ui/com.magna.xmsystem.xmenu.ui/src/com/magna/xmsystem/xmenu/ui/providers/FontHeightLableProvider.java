package com.magna.xmsystem.xmenu.ui.providers;

import org.eclipse.jface.viewers.ILabelProvider;
import org.eclipse.jface.viewers.LabelProvider;

/**
 * The Class FontHeightLableProvider.
 */
public class FontHeightLableProvider extends LabelProvider implements ILabelProvider {
	
	/* (non-Javadoc)
	 * @see org.eclipse.jface.viewers.LabelProvider#getText(java.lang.Object)
	 */
	@Override
	public String getText(Object element) {
		return element.toString();
	}
}