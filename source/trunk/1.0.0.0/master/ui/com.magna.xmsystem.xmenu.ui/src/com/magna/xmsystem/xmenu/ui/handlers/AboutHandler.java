package com.magna.xmsystem.xmenu.ui.handlers;

import javax.inject.Inject;

import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.core.services.nls.Translation;
import org.eclipse.swt.widgets.Shell;

import com.magna.xmsystem.dependencies.utils.XMSystemUtil;
import com.magna.xmsystem.dependencies.utils.XmSystemEnvProcess;
import com.magna.xmsystem.ui.controls.widgets.CustomMessageDialog;
import com.magna.xmsystem.xmenu.message.Message;

/**
 * The Class AboutHandler.
 * 
 * @author subash.janarthanan
 * 
 */
public class AboutHandler {
	
	/** Member variable 'message' for {@link Message}. */
	@Inject
	@Translation
	private Message message;

	/**
	 * Method for Execute.
	 *
	 * @param shell
	 *            {@link Shell}
	 */
	@Execute
	public void execute(final Shell shell) {
		CustomMessageDialog.openInformation(shell, message.aboutDialogTitle,
				"caxStartMenu Version " + XmSystemEnvProcess.getInstance().getEnvironmentMap().get("XM_VERSION")
				+ "\nBuild Number " + XMSystemUtil.getBuildNumber());
	}
}
