package com.magna.xmsystem.xmenu.ui.dialogs;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.layout.GridDataFactory;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.ScrolledComposite;
import org.eclipse.swt.events.ControlAdapter;
import org.eclipse.swt.events.ControlEvent;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.program.Program;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Link;
import org.eclipse.swt.widgets.Shell;

import com.magna.xmsystem.dependencies.utils.XMSystemUtil;
import com.magna.xmsystem.xmenu.ui.utils.CommonConstants;

/**
 * The Class NoProjectDialog.
 * 
 * @author shashwat.anand
 */
public class NoProjectDialog extends MessageDialog {
	
	/**
	 * Instantiates a new no project dialog.
	 *
	 * @param parentShell the parent shell
	 * @param dialogTitle the dialog title
	 * @param dialogTitleImage the dialog title image
	 * @param dialogMessage the dialog message
	 * @param dialogImageType the dialog image type
	 * @param defaultIndex the default index
	 * @param dialogButtonLabels the dialog button labels
	 */
	public NoProjectDialog(final Shell parentShell, final String dialogTitle, final Image dialogTitleImage, final String dialogMessage,
			final int dialogImageType, final int defaultIndex, final String[] dialogButtonLabels) {
		super(parentShell, dialogTitle, dialogTitleImage, dialogMessage, dialogImageType, defaultIndex, dialogButtonLabels);
	}
	
	/**
	 * Open.
	 *
	 * @param kind the kind
	 * @param parent the parent
	 * @param title the title
	 * @param message the message
	 * @param style the style
	 * @return true, if successful
	 */
	public static boolean open(int kind, Shell parent, String title, String message, int style) {
		NoProjectDialog dialog = new NoProjectDialog(parent, title, null, message, kind, 0, getButtonLabels(kind));
		style &= SWT.SHEET;
		dialog.setShellStyle(dialog.getShellStyle() | style);
		return dialog.open() == 0;
	}

	/**
	 * Method for Open error with hyper link.
	 *
	 * @param parent {@link Shell}
	 * @param title {@link String}
	 * @param message {@link String}
	 */
	public static void openErrorWithHyperLink(Shell parent, String title, String message) {
		open(1, parent, title, message, SWT.NONE);
	}
	
	@Override
	protected Control createDialogArea(final Composite parentP) {
		parentP.setData("org.eclipse.e4.ui.css.id", "MyCSSTagForComposite");
		ScrolledComposite scrolledComposite = new ScrolledComposite(parentP, SWT.V_SCROLL | SWT.H_SCROLL);
		GridData data1 = new GridData(SWT.FILL, SWT.FILL, true, true);
		scrolledComposite.setLayoutData(data1);
		scrolledComposite.setData("org.eclipse.e4.ui.css.id", "MyCSSTagForComposite");

		Composite parent = new Composite(scrolledComposite, SWT.NONE);
		final GridLayout compositeContLayout = new GridLayout(2, false);
		parent.setLayout(compositeContLayout);
		parent.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		parent.setData("org.eclipse.e4.ui.css.id", "MyCSSTagForComposite");

		scrolledComposite.setContent(parent);
		scrolledComposite.setExpandHorizontal(true);
		scrolledComposite.setExpandVertical(true);
		scrolledComposite.addControlListener(new ControlAdapter() {
			public void controlResized(final ControlEvent e) {
				scrolledComposite.setMinSize(parent.computeSize(SWT.DEFAULT, SWT.DEFAULT));
			}
		});
		parentP.setBackgroundMode(SWT.INHERIT_FORCE);
		return super.createDialogArea(parent);
	}
	
	/* (non-Javadoc)
	 * @see org.eclipse.jface.dialogs.IconAndMessageDialog#createMessageArea(org.eclipse.swt.widgets.Composite)
	 */
	@Override
	protected Control createMessageArea(final Composite composite) {
		// create image
		Image image = getImage();
		if (image != null) {
			imageLabel = new Label(composite, SWT.NULL);
			image.setBackground(imageLabel.getBackground());
			imageLabel.setImage(image);
			GridDataFactory.fillDefaults().align(SWT.CENTER, SWT.BEGINNING).applyTo(imageLabel);
		}
		
		// create message
		if (message != null) {
			Pattern pattern = Pattern.compile(CommonConstants.RegularExpressions.URL, Pattern.MULTILINE);
		    Matcher matcher = pattern.matcher(this.message);
		    List<String> urls = new ArrayList<>();
		    // Check all occurrences
		    while (matcher.find()) {
		        urls.add(matcher.group());
		    }
		    if (urls.size() > 0) {
		    	String firstPart = message.substring(0, message.indexOf(urls.get(0)));
		    	String secondPart = message.substring(message.lastIndexOf(urls.get(urls.size() - 1)) + urls.get(urls.size() - 1).length());
		    	if (!XMSystemUtil.isEmpty(firstPart)) {
		    		messageLabel = new Label(composite, getMessageLabelStyle());
		    		messageLabel.setText(firstPart);
		    	}
		    	urls.forEach(url -> {
		    		new Label(composite, getMessageLabelStyle());
		    		Link link = new Link(composite, SWT.WRAP );
		    		link.setText( "<a>" + url + "</a>" );
		    		link.addSelectionListener(new SelectionAdapter() {
		    			@Override
		    			public void widgetSelected(SelectionEvent event) {
		    				try {
		    					Program.launch(new URI(url).toString());
		    				} catch (URISyntaxException e) {
		    					e.printStackTrace();
		    				}					
		    			}
		    		});
		    	});
		    	if (!XMSystemUtil.isEmpty(secondPart)) {
		    		new Label(composite, getMessageLabelStyle());
		    		Label secondLbl = new Label(composite, getMessageLabelStyle());
		    		secondLbl.setText(secondPart);
		    	}
		    } else {
		    	messageLabel = new Label(composite, getMessageLabelStyle());
	    		messageLabel.setText(this.message);
		    }
			if (messageLabel != null && !messageLabel.isDisposed()) {
				GridDataFactory.fillDefaults().align(SWT.FILL, SWT.BEGINNING).grab(true, false)
				.hint(convertHorizontalDLUsToPixels(IDialogConstants.MINIMUM_MESSAGE_AREA_WIDTH), SWT.DEFAULT)
				.applyTo(messageLabel);
			}
		}
		return composite;
	}
	
	/**
	 * Gets the button labels.
	 *
	 * @param kind the kind
	 * @return the button labels
	 */
	static String[] getButtonLabels(int kind) {
		String[] dialogButtonLabels;
		switch (kind) {
			case ERROR:
			case INFORMATION:
			case WARNING: {
				dialogButtonLabels = new String[] { IDialogConstants.OK_LABEL };
				break;
			}
			default: {
				throw new IllegalArgumentException("Illegal value for kind in MessageDialog.open()"); //$NON-NLS-1$
			}
		}
		return dialogButtonLabels;
	}
}
