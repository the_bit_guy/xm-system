package com.magna.xmsystem.xmenu.ui.service;

import java.io.File;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.inject.Inject;

import org.apache.commons.io.FileUtils;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.e4.core.services.nls.Translation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.client.ResourceAccessException;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.magna.xmbackend.entities.AdminAreaTranslationTbl;
import com.magna.xmbackend.entities.AdminAreasTbl;
import com.magna.xmbackend.entities.BaseAppTranslationTbl;
import com.magna.xmbackend.entities.BaseApplicationsTbl;
import com.magna.xmbackend.entities.IconsTbl;
import com.magna.xmbackend.entities.LiveMessageTbl;
import com.magna.xmbackend.entities.ProjectAppTranslationTbl;
import com.magna.xmbackend.entities.ProjectApplicationsTbl;
import com.magna.xmbackend.entities.ProjectTranslationTbl;
import com.magna.xmbackend.entities.ProjectsTbl;
import com.magna.xmbackend.entities.PropertyConfigTbl;
import com.magna.xmbackend.entities.SitesTbl;
import com.magna.xmbackend.entities.StartAppTranslationTbl;
import com.magna.xmbackend.entities.StartApplicationsTbl;
import com.magna.xmbackend.entities.UserAppTranslationTbl;
import com.magna.xmbackend.entities.UserApplicationsTbl;
import com.magna.xmbackend.entities.UserSettingsTbl;
import com.magna.xmbackend.entities.UserTranslationTbl;
import com.magna.xmbackend.entities.UsersTbl;
import com.magna.xmbackend.vo.adminMenu.AdminMenuConfigResponseWrapper;
import com.magna.xmbackend.vo.css.CssSettingsRequest;
import com.magna.xmbackend.vo.css.CssSettingsResponse;
import com.magna.xmbackend.vo.enums.Application;
import com.magna.xmbackend.vo.enums.ApplicationPosition;
import com.magna.xmbackend.vo.enums.LiveMessageStatus;
import com.magna.xmbackend.vo.liveMessage.LiveMessageStatusRequest;
import com.magna.xmbackend.vo.liveMessage.LiveMessageStatusResponseWrapper;
import com.magna.xmbackend.vo.projectApplication.ProjectApplciationMenuWrapper;
import com.magna.xmbackend.vo.projectApplication.ProjectApplicationMenuResponse;
import com.magna.xmbackend.vo.rearrange.RearrangeApplicationsRequest;
import com.magna.xmbackend.vo.rearrange.RearrangeApplicationsResponse;
import com.magna.xmbackend.vo.user.AuthResponse;
import com.magna.xmbackend.vo.userApplication.UserApplicationMenuResponse;
import com.magna.xmbackend.vo.userApplication.UserApplicationMenuWrapper;
import com.magna.xmbackend.vo.userHistory.UserHistoryRequest;
import com.magna.xmbackend.vo.userSettings.UserSettingsRequest;
import com.magna.xmsystem.dependencies.utils.LANG_ENUM;
import com.magna.xmsystem.dependencies.utils.OSValidator;
import com.magna.xmsystem.dependencies.utils.XMSystemUtil;
import com.magna.xmsystem.xmadmin.restclient.adminArea.AdminAreaController;
import com.magna.xmsystem.xmadmin.restclient.application.ProjectAppController;
import com.magna.xmsystem.xmadmin.restclient.application.StartAppController;
import com.magna.xmsystem.xmadmin.restclient.application.UserAppController;
import com.magna.xmsystem.xmadmin.restclient.css.CssSettingsController;
import com.magna.xmsystem.xmadmin.restclient.livemsg.LiveMessageController;
import com.magna.xmsystem.xmadmin.restclient.project.ProjectController;
import com.magna.xmsystem.xmadmin.restclient.propertyConfig.PropertyConfigController;
import com.magna.xmsystem.xmadmin.restclient.rearrange.ApplicationsRearrangeController;
import com.magna.xmsystem.xmadmin.restclient.relation.AdminMenuConfigRelController;
import com.magna.xmsystem.xmadmin.restclient.site.SiteController;
import com.magna.xmsystem.xmadmin.restclient.userhistory.UserHistoryController;
import com.magna.xmsystem.xmadmin.restclient.users.UserController;
import com.magna.xmsystem.xmadmin.restclient.usersettings.UserSettingsController;
import com.magna.xmsystem.xmadmin.restclient.validation.AuthController;
import com.magna.xmsystem.xmenu.message.Message;
import com.magna.xmsystem.xmenu.ui.enums.StartApplicationType;
import com.magna.xmsystem.xmenu.ui.enums.UserHistotyOperation;
import com.magna.xmsystem.xmenu.ui.model.AdminMenuApplications;
import com.magna.xmsystem.xmenu.ui.model.AdministrationArea;
import com.magna.xmsystem.xmenu.ui.model.BaseApplication;
import com.magna.xmsystem.xmenu.ui.model.Icon;
import com.magna.xmsystem.xmenu.ui.model.Project;
import com.magna.xmsystem.xmenu.ui.model.ProjectApplication;
import com.magna.xmsystem.xmenu.ui.model.StartApplication;
import com.magna.xmsystem.xmenu.ui.model.User;
import com.magna.xmsystem.xmenu.ui.model.UserApplication;
import com.magna.xmsystem.xmenu.ui.utils.CaxMenuObjectModelUtil;
import com.magna.xmsystem.xmenu.ui.utils.CommonConstants;
import com.magna.xmsystem.xmenu.ui.utils.JobSchedulingRule;
import com.magna.xmsystem.xmenu.ui.utils.XmMenuUtil;

/**
 * Class for Load data from service.
 *
 * @author Chiranjeevi.Akula
 */
public class LoadDataFromService {

	/** Member variable 'this ref' for {@link LoadDataFromService}. */
	private static LoadDataFromService thisRef;

	/** The is in offline. */
	private static boolean isInOffline = false;

	/** The Constant LOGGER. */
	private static final Logger LOGGER = LoggerFactory.getLogger(LoadDataFromService.class);

	/** Member variable for messages. */
	@Inject
	@Translation
	transient protected Message messages;

	/** Member variable 'user statusjob sch rule' for {@link JobSchedulingRule}. */
	private JobSchedulingRule userStatusjobSchRule;

	private String userLastProject;
	
	private Map<String, String> userSettings;
	
	/**
	 * Constructor for LoadDataFromService Class.
	 */
	public LoadDataFromService() {
		setInstance(this);
	}

	/**
	 * Sets the instance.
	 *
	 * @param thisRef
	 *            the new instance
	 */
	public static void setInstance(LoadDataFromService thisRef) {
		LoadDataFromService.thisRef = thisRef;
	}

	/**
	 * Gets the single instance of LoadDataFromService.
	 *
	 * @return single instance of LoadDataFromService
	 */
	public static LoadDataFromService getInstance() {
		return thisRef;
	}

	/**
	 * Gets the user projects.
	 *
	 * @return the user projects
	 */
	public List<Project> getUserProjects(final boolean isInitialLoad) {
		String siteId = CaxMenuObjectModelUtil.getInstance().getSiteId();
		final List<Project> projects = new ArrayList<>();
		if (isInitialLoad && isInOffline) {
			XmMenuUtil.getInstance().displayOfflineMessage();
			String eclipsePrefs = XmMenuUtil.getInstance().getEclipsePrefs(CommonConstants.PREFERENCE_KEY.PROJECT);
			if (!XMSystemUtil.isEmpty(eclipsePrefs)) {
				Project project = (new Gson()).fromJson(eclipsePrefs, Project.class);
				if (project != null) {
					projects.add(project);
				}
			}
		} else {
			try {
				ProjectController projectController = new ProjectController();
				List<ProjectsTbl> projectsTbls = projectController.getUserProjectsBySiteId(siteId);
				setOfflineMode(false);
				if (projectsTbls != null) {
					for (ProjectsTbl projectsTbl : projectsTbls) {
						Project project = getUpdatedProject(projectsTbl);
						projects.add(project);
					}
				}
			} catch (Exception e) {
				if (e instanceof ResourceAccessException || e instanceof IllegalArgumentException) {
					setOfflineMode(true);
					XmMenuUtil.getInstance().displayOfflineMessage();
					String eclipsePrefs = XmMenuUtil.getInstance()
							.getEclipsePrefs(CommonConstants.PREFERENCE_KEY.PROJECT);
					if (!XMSystemUtil.isEmpty(eclipsePrefs)) {
						Project project = (new Gson()).fromJson(eclipsePrefs, Project.class);
						if (project != null) {
							projects.add(project);
						}
					}
				} else {
					LOGGER.error("Exception while getting user projects! " + e); //$NON-NLS-1$
				}
			}
		}

		return projects;
	}

	/**
	 * Gets the user user applications.
	 *
	 * @param adminAreaId
	 *            {@link String}
	 * @return the user user applications
	 */
	public Map<String, List<UserApplication>> getUserUserApplications(final String adminAreaId) {
		final Map<String, List<UserApplication>> userApplications = new HashMap<String, List<UserApplication>>();
		final List<UserApplication> buttonApplications = new ArrayList<>();
		final List<UserApplication> iconApplications = new ArrayList<>();
		final List<UserApplication> menuApplications = new ArrayList<>();

		XmMenuUtil instance = XmMenuUtil.getInstance();

		if (isInOffline) {
			String eclipsePrefs = instance.getEclipsePrefs(CommonConstants.PREFERENCE_KEY.USER_APPLICATIONS);
			if (!XMSystemUtil.isEmpty(eclipsePrefs)) {
				Type type = new TypeToken<Map<String, List<UserApplication>>>() {
				}.getType();

				return (new Gson()).fromJson(eclipsePrefs, type);
			}
		} else {
			try {
				UserAppController userAppController = new UserAppController();
				UserApplicationMenuWrapper userApplicationMenuWrapper = userAppController
						.getUserAppByAAIdAndUserName(adminAreaId);
				if (userApplicationMenuWrapper != null) {
					Iterable<UserApplicationMenuResponse> userAppMenuResponses = userApplicationMenuWrapper
							.getUserAppMenuResponses();
					if (userAppMenuResponses != null) {
						Iterator<UserApplicationMenuResponse> iterator = userAppMenuResponses.iterator();
						while (iterator.hasNext()) {
							UserApplicationMenuResponse userApplicationMenuResponse = iterator.next();
							UserApplicationsTbl userApplicationsTbl = userApplicationMenuResponse
									.getUserApplicationsTbl();
							UserApplication userApplication = getUpdatedUserApplication(userApplicationsTbl);
							if (!isPlatformSupportedApp(userApplication.getBaseApplication())) {
								continue;
							}

							Iterable<UserApplicationsTbl> userAppChildren = userApplicationMenuResponse
									.getUserAppChildren();

							if (userAppChildren != null) {
								Iterator<UserApplicationsTbl> iteratorChildren = userAppChildren.iterator();
								while (iteratorChildren.hasNext()) {
									UserApplicationsTbl userApplicationsTblChild = iteratorChildren.next();
									UserApplication userApplicationChild = getUpdatedUserApplication(
											userApplicationsTblChild);
									if (!isPlatformSupportedApp(userApplicationChild.getBaseApplication())) {
										continue;
									}

									userApplication.addChildUserApplication(userApplicationChild);
								}
							}

							if (ApplicationPosition.BUTTONTASK.toString().equals(userApplication.getPosition())) {
								if (userApplication.isParent()
										&& (userApplication.getChildUserApplications()).size() != 0) {
									buttonApplications.add(userApplication);
								} else if (!userApplication.isParent()) {
									buttonApplications.add(userApplication);
								}
							} else if (ApplicationPosition.MENUTASK.toString().equals(userApplication.getPosition())) {
								if (userApplication.isParent()
										&& (userApplication.getChildUserApplications()).size() != 0) {
									menuApplications.add(userApplication);
								} else if (!userApplication.isParent()) {
									menuApplications.add(userApplication);
								}
							} else if (ApplicationPosition.ICONTASK.toString().equals(userApplication.getPosition())) {
								iconApplications.add(userApplication);
							}
						}
					}
				}
			} catch (Exception e) {
				if (e instanceof ResourceAccessException) {
					String eclipsePrefs = instance.getEclipsePrefs(CommonConstants.PREFERENCE_KEY.USER_APPLICATIONS);
					if (!XMSystemUtil.isEmpty(eclipsePrefs)) {
						Type type = new TypeToken<Map<String, List<UserApplication>>>() {
						}.getType();

						return (new Gson()).fromJson(eclipsePrefs, type);
					}
				} else {
					LOGGER.error("Exception while getting user userApplications! " + e); //$NON-NLS-1$
				}
			}
		}

		instance.sortUserAppsAndItsChild(buttonApplications);
		instance.sortUserAppsAndItsChild(menuApplications);
		instance.sortUserAppsAndItsChild(iconApplications);

		userApplications.put(ApplicationPosition.BUTTONTASK.name(), buttonApplications);
		userApplications.put(ApplicationPosition.MENUTASK.name(), menuApplications);
		userApplications.put(ApplicationPosition.ICONTASK.name(), iconApplications);

		instance.setINEclipsePrefs(CommonConstants.PREFERENCE_KEY.USER_APPLICATIONS, userApplications);

		return userApplications;
	}

	/**
	 * Gets the user project applications.
	 *
	 * @param adminAreaId
	 *            {@link String}
	 * @param projectId
	 *            {@link String}
	 * @return the user project applications
	 */
	public Map<String, List<ProjectApplication>> getUserProjectApplications(final String adminAreaId,
			final String projectId) {

		final Map<String, List<ProjectApplication>> projectApplications = new HashMap<String, List<ProjectApplication>>();
		final List<ProjectApplication> buttonApplications = new ArrayList<>();
		final List<ProjectApplication> iconApplications = new ArrayList<>();
		final List<ProjectApplication> menuApplications = new ArrayList<>();

		XmMenuUtil instance = XmMenuUtil.getInstance();

		if (isInOffline) {
			String eclipsePrefs = instance.getEclipsePrefs(CommonConstants.PREFERENCE_KEY.PROJECT_APPLICATIONS);
			if (!XMSystemUtil.isEmpty(eclipsePrefs)) {
				Type type = new TypeToken<Map<String, List<ProjectApplication>>>() {
				}.getType();

				return (new Gson()).fromJson(eclipsePrefs, type);
			}
		} else {
			try {
				ProjectAppController userAppController = new ProjectAppController();
				ProjectApplciationMenuWrapper projectApplciationMenuWrapper = userAppController
						.getProjectAppByAAIdProjectIdUserName(adminAreaId, projectId);
				if (projectApplciationMenuWrapper != null) {
					Iterable<ProjectApplicationMenuResponse> projectAppMenuResponses = projectApplciationMenuWrapper
							.getProjectAppMenuResponses();
					if (projectAppMenuResponses != null) {
						Iterator<ProjectApplicationMenuResponse> iterator = projectAppMenuResponses.iterator();
						while (iterator.hasNext()) {
							ProjectApplicationMenuResponse projectApplicationMenuResponse = iterator.next();
							ProjectApplicationsTbl projectApplicationsTbl = projectApplicationMenuResponse
									.getProjectApplicationsTbl();
							ProjectApplication projectApplication = getUpdatedProjectApplication(
									projectApplicationsTbl);
							if (!isPlatformSupportedApp(projectApplication.getBaseApplication())) {
								continue;
							}

							Iterable<ProjectApplicationsTbl> projectAppChildren = projectApplicationMenuResponse
									.getProjectAppChildren();
							if (projectAppChildren != null) {
								Iterator<ProjectApplicationsTbl> iteratorChildren = projectAppChildren.iterator();
								while (iteratorChildren.hasNext()) {
									ProjectApplicationsTbl projectApplicationsTblChild = iteratorChildren.next();
									ProjectApplication projectApplicationChild = getUpdatedProjectApplication(
											projectApplicationsTblChild);
									if (!isPlatformSupportedApp(projectApplicationChild.getBaseApplication())) {
										continue;
									}

									projectApplication.addChildProjectApplication(projectApplicationChild);
								}
							}

							if (ApplicationPosition.BUTTONTASK.toString().equals(projectApplication.getPosition())) {
								if (projectApplication.isParent()
										&& (projectApplication.getChildProjectApplications()).size() != 0) {
									buttonApplications.add(projectApplication);
								} else if (!projectApplication.isParent()) {
									buttonApplications.add(projectApplication);
								}
							} else if (ApplicationPosition.MENUTASK.toString()
									.equals(projectApplication.getPosition())) {
								if (projectApplication.isParent()
										&& (projectApplication.getChildProjectApplications()).size() != 0) {
									menuApplications.add(projectApplication);
								} else if (!projectApplication.isParent()) {
									menuApplications.add(projectApplication);
								}
							} else if (ApplicationPosition.ICONTASK.toString()
									.equals(projectApplication.getPosition())) {
								iconApplications.add(projectApplication);
							}
						}
					}
				}
			} catch (Exception e) {
				if (e instanceof ResourceAccessException) {
					String eclipsePrefs = instance.getEclipsePrefs(CommonConstants.PREFERENCE_KEY.PROJECT_APPLICATIONS);
					if (!XMSystemUtil.isEmpty(eclipsePrefs)) {
						Type type = new TypeToken<Map<String, List<ProjectApplication>>>() {
						}.getType();

						return (new Gson()).fromJson(eclipsePrefs, type);
					}
				} else {
					LOGGER.error("Exception while getting user projectApplications! " + e); //$NON-NLS-1$
				}
			}
		}

		instance.sortProjectAppsAndItsChild(buttonApplications);
		instance.sortProjectAppsAndItsChild(menuApplications);
		instance.sortProjectAppsAndItsChild(iconApplications);

		projectApplications.put(ApplicationPosition.BUTTONTASK.name(), buttonApplications);
		projectApplications.put(ApplicationPosition.MENUTASK.name(), menuApplications);
		projectApplications.put(ApplicationPosition.ICONTASK.name(), iconApplications);

		instance.setINEclipsePrefs(CommonConstants.PREFERENCE_KEY.PROJECT_APPLICATIONS, projectApplications);

		return projectApplications;
	}

	/**
	 * Gets the all user start applications.
	 *
	 * @param adminAreaId
	 *            {@link String}
	 * @param projectId
	 *            {@link String}
	 * @return the all user start applications
	 */
	public Map<StartApplicationType, List<StartApplication>> getAllUserStartApplications(final String adminAreaId,
			final String projectId) {

		final Map<StartApplicationType, List<StartApplication>> startApplications = new HashMap<StartApplicationType, List<StartApplication>>();
		final List<StartApplication> startScripts = new ArrayList<StartApplication>();
		final List<StartApplication> startMessages = new ArrayList<StartApplication>();

		if (isInOffline) {
			String eclipsePrefs = XmMenuUtil.getInstance()
					.getEclipsePrefs(CommonConstants.PREFERENCE_KEY.START_APPLICATIONS);
			if (!XMSystemUtil.isEmpty(eclipsePrefs)) {
				Type type = new TypeToken<HashMap<StartApplicationType, List<StartApplication>>>() {
				}.getType();
				return (new Gson()).fromJson(eclipsePrefs, type);
			}
		} else {
			try {
				StartAppController startAppController = new StartAppController();

				List<StartApplicationsTbl> startApplicationsTbls = startAppController.getStartAppByAAId(adminAreaId);
				if (startApplicationsTbls != null) {
					for (StartApplicationsTbl startApplicationsTbl : startApplicationsTbls) {
						StartApplication startApplication = getUpdatedStartApplication(startApplicationsTbl);
						if (startApplication.isMessage()) {
							startMessages.add(startApplication);
						} else {
							startScripts.add(startApplication);
						}
					}
				}

				startApplicationsTbls = startAppController.getStartAppsByAAIdProjectId(adminAreaId, projectId);
				if (startApplicationsTbls != null) {
					for (StartApplicationsTbl startApplicationsTbl : startApplicationsTbls) {
						StartApplication startApplication = getUpdatedStartApplication(startApplicationsTbl);
						if (startApplication.isMessage()) {
							startMessages.add(startApplication);
						} else {
							startScripts.add(startApplication);
						}
					}
				}

				startApplicationsTbls = startAppController.getUserStartApplications();
				if (startApplicationsTbls != null) {
					for (StartApplicationsTbl startApplicationsTbl : startApplicationsTbls) {
						StartApplication startApplication = getUpdatedStartApplication(startApplicationsTbl);
						if (startApplication.isMessage()) {
							startMessages.add(startApplication);
						} else {
							startScripts.add(startApplication);
						}
					}
				}
			} catch (Exception e) {
				if (e instanceof ResourceAccessException) {
					String eclipsePrefs = XmMenuUtil.getInstance()
							.getEclipsePrefs(CommonConstants.PREFERENCE_KEY.START_APPLICATIONS);
					if (!XMSystemUtil.isEmpty(eclipsePrefs)) {
						Type type = new TypeToken<HashMap<StartApplicationType, List<StartApplication>>>() {
						}.getType();
						return (new Gson()).fromJson(eclipsePrefs, type);
					}
				} else {
					LOGGER.error("Exception while getting user startApplications! " + e); //$NON-NLS-1$
				}
			}
		}

		startApplications.put(StartApplicationType.SCRIPT, startScripts);
		startApplications.put(StartApplicationType.MESSAGE, startMessages);

		XmMenuUtil.getInstance().setINEclipsePrefs(CommonConstants.PREFERENCE_KEY.START_APPLICATIONS,
				startApplications);

		return startApplications;
	}

	/**
	 * Gets the users last project.
	 *
	 * @return the users last project
	 */
	/*public String getUsersLastProject() {

		if (isInOffline) {
			String eclipsePrefs = XmMenuUtil.getInstance().getEclipsePrefs(CommonConstants.PREFERENCE_KEY.PROJECT);
			if (!XMSystemUtil.isEmpty(eclipsePrefs)) {
				Project project = (new Gson()).fromJson(eclipsePrefs, Project.class);
				if (project != null) {
					return project.getName();
				}
			}
		} else {
			try {
				String siteId = CaxMenuObjectModelUtil.getInstance().getSiteId();
				UserSettingsController userSettingsController = new UserSettingsController();
				UserSettingsTbl userSettingsTbl = userSettingsController.getUserSettingsBySiteId(siteId);
				if (userSettingsTbl != null) {
					ProjectsTbl projectId = userSettingsTbl.getProjectId();
					if (projectId != null) {
						Project project = getUpdatedProject(projectId);
						XmMenuUtil.getInstance().setINEclipsePrefs(CommonConstants.PREFERENCE_KEY.PROJECT, project);
						return project.getName();
					}
				}
			} catch (Exception e) {
				if (e instanceof ResourceAccessException) {
					String eclipsePrefs = XmMenuUtil.getInstance()
							.getEclipsePrefs(CommonConstants.PREFERENCE_KEY.PROJECT);
					if (!XMSystemUtil.isEmpty(eclipsePrefs)) {
						Project project = (new Gson()).fromJson(eclipsePrefs, Project.class);
						if (project != null) {
							return project.getName();
						}
					}
				}
			}
		}

		return null;
	}*/

	public void loadUserSettings() {
		UserSettingsTbl userSettingsTbl = null;
		if (isInOffline) {
			String eclipsePrefs = XmMenuUtil.getInstance().getEclipsePrefs(CommonConstants.PREFERENCE_KEY.USERSETTINGS);
			if (!XMSystemUtil.isEmpty(eclipsePrefs)) {
				userSettingsTbl = (new Gson()).fromJson(eclipsePrefs, UserSettingsTbl.class);
			}
		} else {
			try {
				String siteId = CaxMenuObjectModelUtil.getInstance().getSiteId();
				UserSettingsController userSettingsController = new UserSettingsController();
				userSettingsTbl = userSettingsController.getUserSettingsBySiteId(siteId);
				XmMenuUtil.getInstance().setINEclipsePrefs(CommonConstants.PREFERENCE_KEY.USERSETTINGS, userSettingsTbl);
			} catch (Exception e) {
				if (e instanceof ResourceAccessException) {
					String eclipsePrefs = XmMenuUtil.getInstance().getEclipsePrefs(CommonConstants.PREFERENCE_KEY.USERSETTINGS);
					if (!XMSystemUtil.isEmpty(eclipsePrefs)) {
						userSettingsTbl = (new Gson()).fromJson(eclipsePrefs, UserSettingsTbl.class);
					}
				}
			}
		}
		
		Project project = null;
		String projectName = null;
		Map<String, String> userSettings = new HashMap<>();
		if(userSettingsTbl != null) {
			ProjectsTbl projectId = userSettingsTbl.getProjectId();
			if (projectId != null) {
				project = getUpdatedProject(projectId);
				projectName = project.getName();
			}	
			String settings = userSettingsTbl.getSettings();
			if(settings != null) {
				userSettings = (new Gson()).fromJson(settings, new TypeToken<Map<String, String>>() {}.getType());
			}
		}
		
		XmMenuUtil.getInstance().setINEclipsePrefs(CommonConstants.PREFERENCE_KEY.PROJECT, project);
		this.userLastProject = projectName;
		this.userSettings = userSettings;
	}
	
	/**
	 * Gets the user last project.
	 *
	 * @return the user last project
	 */
	public String getUserLastProject() {
		return this.userLastProject;
	}

	/**
	 * Gets the user settings.
	 *
	 * @return the user settings
	 */
	public Map<String, String> getUserSettings() {
		return this.userSettings;
	}

	/**
	 * Method for Update project in user settings.
	 *
	 * @param projectId
	 *            {@link String}
	 * @return true, if successful
	 */
	public boolean updateProjectInUserSettings(final String projectId) {
		boolean isUpdated = false;
		Project project = null;

		if (isInOffline) {
			return isUpdated;
		}

		try {
			String siteId = CaxMenuObjectModelUtil.getInstance().getSiteId();
			UserSettingsController userSettingsController = new UserSettingsController();
			UserSettingsRequest userSettingsRequest = new UserSettingsRequest();
			userSettingsRequest.setSiteId(siteId);
			userSettingsRequest.setProjectId(projectId);

			UserSettingsTbl userSettingsTbl = userSettingsController.updateProjectInUserSettings(userSettingsRequest);
			if (userSettingsTbl != null) {
				ProjectsTbl projectsTbl = userSettingsTbl.getProjectId();
				if (projectsTbl != null) {
					project = getUpdatedProject(projectsTbl);
				}
				isUpdated = true;
			}
		} catch (Exception e) {
			if (e instanceof ResourceAccessException) {
				return isUpdated;
			}
		}
		XmMenuUtil.getInstance().setINEclipsePrefs(CommonConstants.PREFERENCE_KEY.PROJECT, project);

		return isUpdated;
	}

	/**
	 * Method for Update settings in user settings.
	 */
	public void updateSettingsInUserSettings() {
		if (!isInOffline) {
			try {
				String siteId = CaxMenuObjectModelUtil.getInstance().getSiteId();
				UserSettingsController userSettingsController = new UserSettingsController();
				UserSettingsRequest userSettingsRequest = new UserSettingsRequest();
				userSettingsRequest.setSiteId(siteId);

				if (this.userSettings == null) {
					this.userSettings = new HashMap<>();
				}
				
				Map<String, String> mainWindowDimensionsMap = XmMenuUtil.getInstance().getMainWindowDimensionsMap();
				
				if (mainWindowDimensionsMap != null && !mainWindowDimensionsMap.isEmpty()) {					
					this.userSettings.putAll(mainWindowDimensionsMap);
				}
				
				this.userSettings.put(CommonConstants.USER_SETTINGS.HOST_NAME, XMSystemUtil.getHostName());
				this.userSettings.put(CommonConstants.USER_SETTINGS.LANGUAGE, XMSystemUtil.getLanguage());

				String settings = (new Gson()).toJson(this.userSettings);
				userSettingsRequest.setSettings(settings);

				userSettingsController.updateSettingsInUserSettings(userSettingsRequest);
			} catch (Exception e) {
				LOGGER.error("Exception while updating user settings! " + e);
			}
		}
	}
	
	/**
	 * Gets the user admin area.
	 *
	 * @param projectId
	 *            {@link String}
	 * @param siteId
	 *            {@link String}
	 * @return the user admin area
	 */
	public AdministrationArea getUserAdminArea(final String projectId, final String siteId) {
		AdministrationArea administrationArea = null;

		if (isInOffline) {
			String eclipsePrefs = XmMenuUtil.getInstance()
					.getEclipsePrefs(CommonConstants.PREFERENCE_KEY.ADMINISTRATION_AREA);
			if (!XMSystemUtil.isEmpty(eclipsePrefs)) {
				return (AdministrationArea) (new Gson()).fromJson(eclipsePrefs, AdministrationArea.class);
			}
		} else {
			try {
				AdminAreaController adminAreaController = new AdminAreaController();
				List<AdminAreasTbl> adminAreasTbls = adminAreaController.getAAByProjectIdAndSiteId(projectId, siteId);
				if (adminAreasTbls != null && !adminAreasTbls.isEmpty()) {
					administrationArea = getUpdatedAdminArea(adminAreasTbls.get(0));
				}
				XmMenuUtil.getInstance().setINEclipsePrefs(CommonConstants.PREFERENCE_KEY.ADMINISTRATION_AREA,
						administrationArea);
			} catch (Exception e) {
				if (e instanceof ResourceAccessException) {
					String eclipsePrefs = XmMenuUtil.getInstance()
							.getEclipsePrefs(CommonConstants.PREFERENCE_KEY.ADMINISTRATION_AREA);
					if (!XMSystemUtil.isEmpty(eclipsePrefs)) {
						return (AdministrationArea) (new Gson()).fromJson(eclipsePrefs, AdministrationArea.class);
					}
				} else {
					LOGGER.error("Exception while getting user adminArea! " + e); //$NON-NLS-1$
				}
			}
		}

		return administrationArea;
	}

	/**
	 * Gets the site.
	 *
	 * @param siteName
	 *            {@link String}
	 * @return the site
	 */
	public SitesTbl getSite(final String siteName) {
		if (isInOffline) {
			String eclipsePrefs = XmMenuUtil.getInstance().getEclipsePrefs(CommonConstants.PREFERENCE_KEY.SITE);
			if (!XMSystemUtil.isEmpty(eclipsePrefs)) {
				return (SitesTbl) (new Gson()).fromJson(eclipsePrefs, SitesTbl.class);
			}
		} else {
			try {
				SiteController siteController = new SiteController();
				SitesTbl site = siteController.getSiteByName(siteName);
				XmMenuUtil.getInstance().setINEclipsePrefs(CommonConstants.PREFERENCE_KEY.SITE, site);
				return site;
			} catch (Exception e) {
				if (e instanceof ResourceAccessException) {
					String eclipsePrefs = XmMenuUtil.getInstance().getEclipsePrefs(CommonConstants.PREFERENCE_KEY.SITE);
					if (!XMSystemUtil.isEmpty(eclipsePrefs)) {
						return (SitesTbl) (new Gson()).fromJson(eclipsePrefs, SitesTbl.class);
					}
				}
			}
		}

		return null;
	}

	/**
	 * Sets the offline mode.
	 *
	 * @param isOfflineMode
	 *            the new offline mode
	 */
	private static void setOfflineMode(final boolean isOfflineMode) {
		isInOffline = isOfflineMode;
	}
	
	/**
	 * @return the isInOffline
	 */
	public static boolean isInOffline() {
		return isInOffline;
	}
	
	/**
	 * Gets the no project message.
	 *
	 * @return the no project message
	 */
	public String getNoProjectMessage() {

		String message = null;
		if (!isInOffline) {
			try {
				PropertyConfigController propertyConfigController = new PropertyConfigController();
				List<PropertyConfigTbl> allNoProjectPopMessage = propertyConfigController.getNoProjectPopMessage();
				if (allNoProjectPopMessage != null) {
					for (PropertyConfigTbl propertyConfigTbl : allNoProjectPopMessage) {
						message = propertyConfigTbl.getValue();
					}
				}
			} catch (Exception e) {
				if (e instanceof ResourceAccessException) {

				}
			}
		}
		return message;
	}

	/**
	 * Gets the updated admin area.
	 *
	 * @param adminAreaTbl
	 *            {@link AdminAreasTbl}
	 * @return the updated admin area
	 */
	private AdministrationArea getUpdatedAdminArea(final AdminAreasTbl adminAreaTbl) {
		final File iconFolder = XMSystemUtil.getXMSystemServerIconsFolder();
		/*if (iconFolder == null) {
			CustomMessageDialog.openError(Display.getDefault().getActiveShell(), "Error",
					"Icon folder is not reachable");
			return null;
		}*/
		AdministrationArea adminArea = null;
		try {
			final String adminAreaId = adminAreaTbl.getAdminAreaId();
			final String adminAreaName = adminAreaTbl.getName();
			boolean isActive = com.magna.xmbackend.vo.enums.Status.ACTIVE.name().equals(adminAreaTbl.getStatus()) ? true
					: false;
			String contact = adminAreaTbl.getHotlineContactNumber();
			String email = adminAreaTbl.getHotlineContactEmail();
			Long singletonAppTimeout = adminAreaTbl.getSingletonAppTimeout();
			IconsTbl iconTbl = adminAreaTbl.getIconId();
			Icon icon = iconFolder == null ? null : new Icon(iconTbl.getIconId(), iconTbl.getIconName(),
					iconFolder + File.separator + iconTbl.getIconName(), iconTbl.getIconType());

			Map<LANG_ENUM, String> descriptionMap = new HashMap<>();
			Map<LANG_ENUM, String> remarksMap = new HashMap<>();
			Map<LANG_ENUM, String> translationIdMap = new HashMap<>();

			Collection<AdminAreaTranslationTbl> adminAreaTranslationTblList = adminAreaTbl
					.getAdminAreaTranslationTblCollection();
			for (AdminAreaTranslationTbl siteTranslationTbl : adminAreaTranslationTblList) {
				LANG_ENUM langEnum = LANG_ENUM.getLangEnum(siteTranslationTbl.getLanguageCode().getLanguageCode());
				String translationId = siteTranslationTbl.getAdminAreaTranslationId();
				translationIdMap.put(langEnum, translationId);
				descriptionMap.put(langEnum, siteTranslationTbl.getDescription());
				remarksMap.put(langEnum, siteTranslationTbl.getRemarks());
			}

			adminArea = new AdministrationArea(adminAreaId, adminAreaName, isActive, icon, contact, email,
					singletonAppTimeout);
			adminArea.setTranslationIdMap(translationIdMap);
		} catch (Exception e) {
			LOGGER.error("Exception while updating AdminArea! " + e);
		}
		return adminArea;
	}

	/**
	 * Gets the updated project.
	 *
	 * @param projectsTbl
	 *            {@link ProjectsTbl}
	 * @return the updated project
	 */
	private Project getUpdatedProject(final ProjectsTbl projectsTbl) {
		final File iconFolder = XMSystemUtil.getXMSystemServerIconsFolder();
		/*if (iconFolder == null) {
			CustomMessageDialog.openError(Display.getDefault().getActiveShell(), "Error",
					"Icon folder is not reachable");
			return null;
		}*/
		Project project = null;
		try {
			final String id = projectsTbl.getProjectId();
			final String projectName = projectsTbl.getName();
			boolean isActive = com.magna.xmbackend.vo.enums.Status.ACTIVE.name().equals(projectsTbl.getStatus()) ? true
					: false;

			IconsTbl iconTbl = projectsTbl.getIconId();
			Icon icon = iconFolder == null ? null : new Icon(iconTbl.getIconId(), iconTbl.getIconName(),
					iconFolder + File.separator + iconTbl.getIconName(), iconTbl.getIconType());

			Map<LANG_ENUM, String> descriptionMap = new HashMap<>();
			Map<LANG_ENUM, String> remarksMap = new HashMap<>();
			Map<LANG_ENUM, String> translationMap = new HashMap<>();

			Collection<ProjectTranslationTbl> projectTranslationTblList = projectsTbl
					.getProjectTranslationTblCollection();
			for (ProjectTranslationTbl projectTranslationTbl : projectTranslationTblList) {
				LANG_ENUM langEnum = LANG_ENUM.getLangEnum(projectTranslationTbl.getLanguageCode().getLanguageCode());
				String translationId = projectTranslationTbl.getProjectTranslationId();
				translationMap.put(langEnum, translationId);
				descriptionMap.put(langEnum, projectTranslationTbl.getDescription());
				remarksMap.put(langEnum, projectTranslationTbl.getRemarks());
			}

			project = new Project(id, projectName, isActive, descriptionMap, remarksMap, icon);
			project.setTranslationIdMap(translationMap);

		} catch (Exception e) {
			LOGGER.error("Exception while getting updated project! " + e); //$NON-NLS-1$
		}
		return project;
	}

	/**
	 * Gets the updated user application.
	 *
	 * @param userApplicationsTbl
	 *            {@link UserApplicationsTbl}
	 * @return the updated user application
	 */
	public UserApplication getUpdatedUserApplication(final UserApplicationsTbl userApplicationsTbl) {
		final File iconFolder = XMSystemUtil.getXMSystemServerIconsFolder();
		/*if (iconFolder == null) {
			CustomMessageDialog.openError(Display.getDefault().getActiveShell(), "Error",
					"Icon folder is not reachable");
			return null;
		}*/
		UserApplication userApplication = null;
		try {
			final String userAppId = userApplicationsTbl.getUserApplicationId();
			final String userAppName = userApplicationsTbl.getName();
			final String userAppDesc = userApplicationsTbl.getDescription();
			boolean isActive = com.magna.xmbackend.vo.enums.Status.ACTIVE.name().equals(userApplicationsTbl.getStatus())
					? true : false;
			String position = userApplicationsTbl.getPosition();
			boolean isParent = Boolean.valueOf(userApplicationsTbl.getIsParent());
			boolean isSingleton = Boolean.parseBoolean(userApplicationsTbl.getIsSingleton());
			BaseApplication baseApplication = getUpdatedBasetApplication(userApplicationsTbl.getBaseApplicationId());

			IconsTbl iconTbl = userApplicationsTbl.getIconId();
			Icon icon = iconFolder == null ? null : new Icon(iconTbl.getIconId(), iconTbl.getIconName(),
					iconFolder + File.separator + iconTbl.getIconName(), iconTbl.getIconType());

			Map<LANG_ENUM, String> nameMap = new HashMap<>();
			Map<LANG_ENUM, String> descriptionMap = new HashMap<>();
			Map<LANG_ENUM, String> remarksMap = new HashMap<>();
			Map<LANG_ENUM, String> translationMap = new HashMap<>();

			Collection<UserAppTranslationTbl> userAppTranslationTblList = userApplicationsTbl
					.getUserAppTranslationTblCollection();
			for (UserAppTranslationTbl userAppTranslationTbl : userAppTranslationTblList) {
				LANG_ENUM langEnum = LANG_ENUM.getLangEnum(userAppTranslationTbl.getLanguageCode().getLanguageCode());
				String translationId = userAppTranslationTbl.getUserAppTranslationId();
				translationMap.put(langEnum, translationId);
				nameMap.put(langEnum, userAppTranslationTbl.getName());
				descriptionMap.put(langEnum, userAppTranslationTbl.getDescription());
				remarksMap.put(langEnum, userAppTranslationTbl.getRemarks());
			}
			userApplication = new UserApplication(userAppId, userAppName, userAppDesc, nameMap, isActive,
					descriptionMap, remarksMap, icon, isParent, isSingleton, position, baseApplication);
			userApplication.setTranslationIdMap(translationMap);

		} catch (Exception e) {
			LOGGER.error("Exception while getting updated userApplication! " + e); //$NON-NLS-1$
		}
		return userApplication;
	}

	/**
	 * Gets the updated project application.
	 *
	 * @param projectApplicationsTbl
	 *            {@link ProjectApplicationsTbl}
	 * @return the updated project application
	 */
	public ProjectApplication getUpdatedProjectApplication(final ProjectApplicationsTbl projectApplicationsTbl) {
		final File iconFolder = XMSystemUtil.getXMSystemServerIconsFolder();
		/*if (iconFolder == null) {
			CustomMessageDialog.openError(Display.getDefault().getActiveShell(), "Error",
					"Icon folder is not reachable");
			return null;
		}*/
		ProjectApplication projectApplication = null;
		try {
			final String projectApplicationId = projectApplicationsTbl.getProjectApplicationId();
			final String name = projectApplicationsTbl.getName();
			final String description = projectApplicationsTbl.getDescription();
			boolean isActive = com.magna.xmbackend.vo.enums.Status.ACTIVE.name()
					.equals(projectApplicationsTbl.getStatus()) ? true : false;

			IconsTbl iconTbl = projectApplicationsTbl.getIconId();
			Icon icon = iconFolder == null ? null : new Icon(iconTbl.getIconId(), iconTbl.getIconName(),
					iconFolder + File.separator + iconTbl.getIconName(), iconTbl.getIconType());
			boolean isParent = Boolean.parseBoolean(projectApplicationsTbl.getIsParent());
			String position = projectApplicationsTbl.getPosition();
			boolean isSingleton = Boolean.parseBoolean(projectApplicationsTbl.getIsSingleton());
			BaseApplication baseApplication = getUpdatedBasetApplication(projectApplicationsTbl.getBaseApplicationId());

			Map<LANG_ENUM, String> nameMap = new HashMap<>();
			Map<LANG_ENUM, String> descriptionMap = new HashMap<>();
			Map<LANG_ENUM, String> remarksMap = new HashMap<>();
			Map<LANG_ENUM, String> translationMap = new HashMap<>();

			Collection<ProjectAppTranslationTbl> projectAppTranslationTblList = projectApplicationsTbl
					.getProjectAppTranslationTblCollection();
			for (ProjectAppTranslationTbl projectAppTranslationTbl : projectAppTranslationTblList) {
				LANG_ENUM langEnum = LANG_ENUM
						.getLangEnum(projectAppTranslationTbl.getLanguageCode().getLanguageCode());
				String translationId = projectAppTranslationTbl.getProjectAppTranslationId();
				translationMap.put(langEnum, translationId);
				nameMap.put(langEnum, projectAppTranslationTbl.getName());
				descriptionMap.put(langEnum, projectAppTranslationTbl.getDescription());
				remarksMap.put(langEnum, projectAppTranslationTbl.getRemarks());
			}

			projectApplication = new ProjectApplication(projectApplicationId, name, description, nameMap, isActive,
					descriptionMap, remarksMap, icon, isParent, isSingleton, position, baseApplication);
			projectApplication.setTranslationIdMap(translationMap);
		} catch (Exception e) {
			LOGGER.error("Exception while getting updated projectApplication! " + e); //$NON-NLS-1$
		}
		return projectApplication;
	}

	/**
	 * Gets the updated baset application.
	 *
	 * @param baseApplicationsTbl
	 *            {@link BaseApplicationsTbl}
	 * @return the updated baset application
	 */
	private BaseApplication getUpdatedBasetApplication(final BaseApplicationsTbl baseApplicationsTbl) {
		final File iconFolder = XMSystemUtil.getXMSystemServerIconsFolder();
		/*if (iconFolder == null) {
			CustomMessageDialog.openError(Display.getDefault().getActiveShell(), "Error",
					"Icon folder is not reachable");
			return null;
		}*/
		BaseApplication baseApplication = null;
		try {
			if (baseApplicationsTbl != null) {
				String baseApplicationId = baseApplicationsTbl.getBaseApplicationId();
				String name = baseApplicationsTbl.getName();
				boolean isActive = com.magna.xmbackend.vo.enums.Status.ACTIVE.name()
						.equals(baseApplicationsTbl.getStatus()) ? true : false;
				String program = baseApplicationsTbl.getProgram();

				IconsTbl iconTbl = baseApplicationsTbl.getIconId();
				Icon icon = iconFolder == null ? null : new Icon(iconTbl.getIconId(), iconTbl.getIconName(),
						iconFolder + File.separator + iconTbl.getIconName(), iconTbl.getIconType());

				Map<LANG_ENUM, String> descriptionMap = new HashMap<>();
				Map<LANG_ENUM, String> remarksMap = new HashMap<>();
				Map<LANG_ENUM, String> translationMap = new HashMap<>();

				Collection<BaseAppTranslationTbl> baseAppTranslationTblCollection = baseApplicationsTbl
						.getBaseAppTranslationTblCollection();
				for (BaseAppTranslationTbl baseAppTranslationTbl : baseAppTranslationTblCollection) {
					LANG_ENUM langEnum = LANG_ENUM
							.getLangEnum(baseAppTranslationTbl.getLanguageCode().getLanguageCode());
					String translationId = baseAppTranslationTbl.getBaseAppTranslationId();
					translationMap.put(langEnum, translationId);
					descriptionMap.put(langEnum, baseAppTranslationTbl.getDescription());
					remarksMap.put(langEnum, baseAppTranslationTbl.getRemarks());
				}

				Map<String, Boolean> platformMap = new HashMap<>();
				String platforms = baseApplicationsTbl.getPlatforms();
				if (!XMSystemUtil.isEmpty(platforms)) {
					String[] platformArray = platforms.split(";");
					for (String paltform : platformArray) {
						if (BaseApplication.SUPPORTED_PLATFORMS.contains(paltform)) {
							platformMap.put(paltform, true);
						}
					}
				}
				baseApplication = new BaseApplication(baseApplicationId, name, isActive, descriptionMap, remarksMap,
						platformMap, program, icon);
				baseApplication.setTranslationIdMap(translationMap);
			}
		} catch (Exception e) {
			LOGGER.error("Exception while getting updated baseApplication! " + e); //$NON-NLS-1$
		}
		return baseApplication;
	}

	/**
	 * Gets the updated start application.
	 *
	 * @param startApplicationsTbl
	 *            {@link StartApplicationsTbl}
	 * @return the updated start application
	 */
	private StartApplication getUpdatedStartApplication(final StartApplicationsTbl startApplicationsTbl) {
		final File iconFolder = XMSystemUtil.getXMSystemServerIconsFolder();
		/*if (iconFolder == null) {
			CustomMessageDialog.openError(Display.getDefault().getActiveShell(), "Error",
					"Icon folder is not reachable"); //$NON-NLS-1$
			return null;
		}*/
		StartApplication startApplication = null;
		try {
			final String startAppId = startApplicationsTbl.getStartApplicationId();
			final String name = startApplicationsTbl.getName();
			boolean isActive = com.magna.xmbackend.vo.enums.Status.ACTIVE.name()
					.equals(startApplicationsTbl.getStatus()) ? true : false;

			IconsTbl iconTbl = startApplicationsTbl.getIconId();
			Icon icon = iconFolder == null ? null : new Icon(iconTbl.getIconId(), iconTbl.getIconName(),
					iconFolder + File.separator + iconTbl.getIconName(), iconTbl.getIconType());

			boolean isMessage = Boolean.valueOf(startApplicationsTbl.getIsMessage());
			Date expiryDate = startApplicationsTbl.getStartMessageExpiryDate();

			BaseApplication baseApplication = getUpdatedBasetApplication(startApplicationsTbl.getBaseApplicationId());

			Map<LANG_ENUM, String> descriptionMap = new HashMap<>();
			Map<LANG_ENUM, String> remarksMap = new HashMap<>();
			Map<LANG_ENUM, String> translationMap = new HashMap<>();

			Collection<StartAppTranslationTbl> starttAppTranslationTblList = startApplicationsTbl
					.getStartAppTranslationTblCollection();
			for (StartAppTranslationTbl startAppTranslationTbl : starttAppTranslationTblList) {
				LANG_ENUM langEnum = LANG_ENUM.getLangEnum(startAppTranslationTbl.getLanguageCode().getLanguageCode());
				String translationId = startAppTranslationTbl.getStartAppTranslationId();
				translationMap.put(langEnum, translationId);
				descriptionMap.put(langEnum, startAppTranslationTbl.getDescription());
				remarksMap.put(langEnum, startAppTranslationTbl.getRemarks());
			}

			startApplication = new StartApplication(startAppId, name, isActive, descriptionMap, remarksMap, icon,
					isMessage, expiryDate, baseApplication);
			startApplication.setTranslationIdMap(translationMap);

		} catch (Exception e) {
			LOGGER.error("Exception while updating StartApplication! " + e); //$NON-NLS-1$
		}
		return startApplication;
	}

	/**
	 * Checks if is platform supported app.
	 *
	 * @param baseApplication
	 *            {@link BaseApplication}
	 * @return true, if is platform supported app
	 */
	private boolean isPlatformSupportedApp(BaseApplication baseApplication) {
		if (baseApplication != null) {
			String platformString = baseApplication.getPlatformString();
			if (OSValidator.isWindows()) {
				if (platformString.toLowerCase().contains("windows")) {
					return true;
				}
			} else if (OSValidator.isUnixOrLinux()) {
				if (platformString.toLowerCase().contains("linux")) {
					return true;
				}
			} else {
				return false;
			}
		} else {
			return true;
		}
		return false;
	}

	/**
	 * Gets the all users.
	 *
	 * @return the all users
	 */
	public List<User> getAllUsers() {

		List<User> listOfUser = new ArrayList<>();

		if (!isInOffline) {
			final File iconFolder = XMSystemUtil.getXMSystemServerIconsFolder();
			/*if (iconFolder == null) {
				CustomMessageDialog.openError(Display.getDefault().getActiveShell(), "Error",
						"Icon folder is not reachable"); //$NON-NLS-1$
				return null;
			}*/

			UserController userController = new UserController();
			List<UsersTbl> userResponseList = userController.getAllUsers(true);

			for (UsersTbl usersTbl : userResponseList) {
				String userId = usersTbl.getUserId();
				String userName = usersTbl.getUsername();
				String userFullName = usersTbl.getFullName();
				String userEmailId = usersTbl.getEmailId();
				String userTelephone = usersTbl.getTelephoneNumber();
				String userDept = usersTbl.getDepartment();
				boolean isActive = com.magna.xmbackend.vo.enums.Status.ACTIVE.name().equals(usersTbl.getStatus()) ? true
						: false;

				IconsTbl iconTbl = usersTbl.getIconId();
				Icon icon = iconFolder == null ? null : new Icon(iconTbl.getIconId(), iconTbl.getIconName(),
						iconFolder + File.separator + iconTbl.getIconName(), iconTbl.getIconType());

				Map<LANG_ENUM, String> descriptionMap = new HashMap<>();
				Map<LANG_ENUM, String> remarksMap = new HashMap<>();
				Map<LANG_ENUM, String> translationIdMap = new HashMap<>();

				Collection<UserTranslationTbl> userTranslationTblList = usersTbl.getUserTranslationTblCollection();
				for (UserTranslationTbl userTranslationTbl : userTranslationTblList) {
					LANG_ENUM langEnum = LANG_ENUM.getLangEnum(userTranslationTbl.getLanguageCode().getLanguageCode());
					String translationId = userTranslationTbl.getUserTranslationId();
					translationIdMap.put(langEnum, translationId);
					descriptionMap.put(langEnum, userTranslationTbl.getDescription());
					remarksMap.put(langEnum, userTranslationTbl.getRemarks());
				}

				User user = new User(userId, userName, userFullName, isActive, userEmailId, userTelephone, userDept,
						descriptionMap, remarksMap, icon);
				listOfUser.add(user);
			}
		}

		return listOfUser;
	}

	/**
	 * Gets the admins menu applications.
	 *
	 * @return the admins menu applications
	 */
	public AdminMenuApplications getAdminsMenuApplications() {
		final AdminMenuApplications adminMenuApplications = new AdminMenuApplications();
		final List<UserApplication> userApplicationsList = new ArrayList<>();
		final List<ProjectApplication> projectApplicationsList = new ArrayList<>();

		XmMenuUtil instance = XmMenuUtil.getInstance();

		boolean isAdminMenuUser = false;
		
		if (!isInOffline) {
			try {
				AdminMenuConfigRelController AdminMenuConfigRelController = new AdminMenuConfigRelController();
				AdminMenuConfigResponseWrapper adminMenuConfigResponseWrapper = AdminMenuConfigRelController
						.getAllAdminMenuConfigRelApplications();
				if (adminMenuConfigResponseWrapper != null) {
					isAdminMenuUser = adminMenuConfigResponseWrapper.isAdminMenuUser();
					Set<UserApplicationMenuResponse> userApplicationMenuResponses = adminMenuConfigResponseWrapper
							.getUserApplicationMenuResponse();
					for (UserApplicationMenuResponse userApplicationMenuResponse : userApplicationMenuResponses) {
						UserApplicationsTbl userApplicationsTbl = userApplicationMenuResponse.getUserApplicationsTbl();
						UserApplication userApplication = getUpdatedUserApplication(userApplicationsTbl);

						if (!isPlatformSupportedApp(userApplication.getBaseApplication())) {
							continue;
						}

						Iterable<UserApplicationsTbl> userAppChildren = userApplicationMenuResponse
								.getUserAppChildren();

						if (userAppChildren != null) {
							Iterator<UserApplicationsTbl> iteratorChildren = userAppChildren.iterator();
							while (iteratorChildren.hasNext()) {
								UserApplicationsTbl userApplicationsTblChild = iteratorChildren.next();
								UserApplication userApplicationChild = getUpdatedUserApplication(
										userApplicationsTblChild);
								if (!isPlatformSupportedApp(userApplicationChild.getBaseApplication())) {
									continue;
								}

								userApplication.addChildUserApplication(userApplicationChild);
							}
						}

						if (userApplication.isParent() && (userApplication.getChildUserApplications()).size() != 0) {
							userApplicationsList.add(userApplication);
						} else if (!userApplication.isParent()) {
							userApplicationsList.add(userApplication);
						}
					}

					Set<ProjectApplicationMenuResponse> projectApplicationMenuResponses = adminMenuConfigResponseWrapper
							.getProjectApplicationMenuResponses();
					for (ProjectApplicationMenuResponse projectApplicationMenuResponse : projectApplicationMenuResponses) {
						ProjectApplicationsTbl projectApplicationsTbl = projectApplicationMenuResponse
								.getProjectApplicationsTbl();
						ProjectApplication projectApplication = getUpdatedProjectApplication(projectApplicationsTbl);

						if (!isPlatformSupportedApp(projectApplication.getBaseApplication())) {
							continue;
						}

						Iterable<ProjectApplicationsTbl> projectAppChildren = projectApplicationMenuResponse
								.getProjectAppChildren();

						if (projectAppChildren != null) {
							Iterator<ProjectApplicationsTbl> iteratorChildren = projectAppChildren.iterator();
							while (iteratorChildren.hasNext()) {
								ProjectApplicationsTbl rojectApplicationsTblChild = iteratorChildren.next();
								ProjectApplication projectApplicationChild = getUpdatedProjectApplication(
										rojectApplicationsTblChild);
								if (!isPlatformSupportedApp(projectApplicationChild.getBaseApplication())) {
									continue;
								}

								projectApplication.addChildProjectApplication(projectApplicationChild);
							}
						}

						if (projectApplication.isParent()
								&& (projectApplication.getChildProjectApplications()).size() != 0) {
							projectApplicationsList.add(projectApplication);
						} else if (!projectApplication.isParent()) {
							projectApplicationsList.add(projectApplication);
						}
					}
				}
			} catch (Exception e) {
				LOGGER.error("Exception while getting AdminMenuUserApplicationsList! " + e); //$NON-NLS-1$
			}
		}

		adminMenuApplications.setAdminMenuUser(isAdminMenuUser);
		
		if (!userApplicationsList.isEmpty()) {
			instance.sortUserAppsAndItsChild(userApplicationsList);
			adminMenuApplications.setUserApplicationsList(userApplicationsList);
		}

		if (!projectApplicationsList.isEmpty()) {
			instance.sortProjectAppsAndItsChild(projectApplicationsList);
			adminMenuApplications.setProjectApplicationsList(projectApplicationsList);
		}

		return adminMenuApplications;
	}

	/**
	 * Gets the live message response list.
	 *
	 * @param siteId {@link String}
	 * @param adminAreaId {@link String}
	 * @param projectId {@link String}
	 * @return the live message response list
	 */
	public Set<LiveMessageTbl> getLiveMessageResponseList(final String siteId, final String adminAreaId,
			final String projectId) {
		if (!isInOffline) {
			try {
				LiveMessageStatusRequest liveMsgRequest = new LiveMessageStatusRequest();

				liveMsgRequest.setSiteId(siteId);
				liveMsgRequest.setAdminAreaId(adminAreaId);
				liveMsgRequest.setProjectId(projectId);

				LiveMessageController liveMessageController = new LiveMessageController();
				LiveMessageStatusResponseWrapper liveMessageResponseWrapper = liveMessageController
						.getLiveMessageByStatus(liveMsgRequest);
				if(liveMessageResponseWrapper != null) {
					return liveMessageResponseWrapper.getLiveMessageTbls();
				}
			} catch (Exception e) {
				LOGGER.error("Exception while getting Live Message table! " + e); //$NON-NLS-1$
			}
		}

		return null;
	}

	/**
	 * Method for Update live message status.
	 *
	 * @param liveMessageId {@link String}
	 */
	public void updateLiveMessageStatus(final String liveMessageId) {

		if (!isInOffline) {
			try {
				LiveMessageStatusRequest liveMessageStatusRequest = new LiveMessageStatusRequest();
				liveMessageStatusRequest.setLiveMessageId(liveMessageId);
				liveMessageStatusRequest.setStatus(LiveMessageStatus.READ.name());
				LiveMessageController liveMessageController = new LiveMessageController();
				liveMessageController.updateLiveMessageStatus(liveMessageStatusRequest);
			} catch (Exception e) {
				LOGGER.error("Exception while updating the Live Message status! " + e); //$NON-NLS-1$
			}
		}
	}
	
	/**
	 * Method for Log user status.
	 *
	 * @param task {@link String}
	 */
	public void logUserStatus(final String task) {

		if(userStatusjobSchRule == null) {
			userStatusjobSchRule = new JobSchedulingRule();
		}
		
		if (!isInOffline) {
			Job userStatusJob = new Job("Logging User Status...") {
				
				@Override
				protected IStatus run(IProgressMonitor monitor) {
					try {
						if (task != null && !task.isEmpty()) {
							UserHistoryRequest userHistoryRequest = new UserHistoryRequest();
							userHistoryRequest.setLogTime(new Date());
							userHistoryRequest.setHost(XMSystemUtil.getHostName());
							userHistoryRequest.setSite(XMSystemUtil.getSiteName());
							if (task.equals(UserHistotyOperation.OPENUSER.toString())
									|| task.equals(UserHistotyOperation.OPENSITE.toString())) {
								userHistoryRequest.setAdminArea(null);
								userHistoryRequest.setProject(null);
							} /*else if (task.equals(UserHistotyOperation.OPENADMINAREA.toString())) {
								userHistoryRequest.setAdminArea(XMSystemUtil.getAdminAreaName());
								userHistoryRequest.setProject(null);
							} */else {
								userHistoryRequest.setAdminArea(XMSystemUtil.getAdminAreaName());
								userHistoryRequest.setProject(XMSystemUtil.getProjectName());
							}
							userHistoryRequest.setApplication(task);
							userHistoryRequest.setPid(0);
							userHistoryRequest.setResult("Success");
							userHistoryRequest.setArgs(null);

							UserHistoryController historyController = new UserHistoryController();
							historyController.logUserStatus(userHistoryRequest);
						}
					} catch (Exception e) {
						LOGGER.error("Exception while logging the user status! " + e); //$NON-NLS-1$
					}
					
					return Status.OK_STATUS;
				}
			};
			userStatusJob.setRule(userStatusjobSchRule);
			userStatusJob.schedule();
		}
	}

	/**
	 * Method for Update user status to history.
	 */
	public void updateUserStatusToHistory() {

		if (!isInOffline) {
			try {
				UserHistoryController historyController = new UserHistoryController();
				historyController.updateUserStatusToHistory();
			} catch (Exception e) {
				LOGGER.error("Exception while updating user status to history! " + e); //$NON-NLS-1$
			}
		}
	}
	
	/**
	 * Checks if is valid user.
	 *
	 * @param userName the user name
	 * @return true, if is valid user
	 */
	public AuthResponse authorizeUser(final String userName) {
		AuthResponse authorizeLogin = null;
		try {
			String encryptTicket = XmMenuUtil.getInstance().getUserTicket();
			long startTimeMillis = System.currentTimeMillis();
			AuthController authController = new AuthController();
			authorizeLogin = authController.authorizeLogin(encryptTicket, Application.CAX_START_MENU.name());
			long endTimeMillis = System.currentTimeMillis();
			setOfflineMode(false);
			boolean validUser = authorizeLogin.isValidUser();
			if(validUser) {
				XmMenuUtil.getInstance().setINEclipsePrefs(CommonConstants.PREFERENCE_KEY.USERNAME,
						userName);
				XmMenuUtil.getInstance().setINEclipsePrefs(CommonConstants.PREFERENCE_KEY.USER_TKT,
						XMSystemUtil.getInstance().getTicket());
			}	
			LOGGER.info("Webservice response time :" + (endTimeMillis - startTimeMillis) + "ms");
		} catch (Exception e) {
			if (e instanceof ResourceAccessException || e instanceof IllegalArgumentException || e.getMessage().equals("404 Invalid resource access")) {
				setOfflineMode(true);
				String eclipsePrefs = XmMenuUtil.getInstance()
						.getEclipsePrefs(CommonConstants.PREFERENCE_KEY.USERNAME);
				if (!XMSystemUtil.isEmpty(eclipsePrefs)) {
					String prefUserName = (new Gson()).fromJson(eclipsePrefs, String.class);
					if (prefUserName.equals(userName)) {
						eclipsePrefs = XmMenuUtil.getInstance()
								.getEclipsePrefs(CommonConstants.PREFERENCE_KEY.USER_TKT);
						if (!XMSystemUtil.isEmpty(eclipsePrefs)) {
							XMSystemUtil.getInstance().setTicket((new Gson()).fromJson(eclipsePrefs, String.class));
						}
					}
				}
			} else {
				LOGGER.error("Error while calling authorizeLogin REST Service!", e); //$NON-NLS-1$
			}
		}

		return authorizeLogin;
	}
	
	/**
	 * Creates the CSS settings for user.
	 *
	 * @return the css settings response
	 */
	public CssSettingsResponse getCssFileExistenceRes() {
		if (!isInOffline) {
			try {
				CssSettingsController cssSettingsController = new CssSettingsController();
				CssSettingsResponse cssFileExistenceRes = cssSettingsController.getCssFileExistenceRes();
				if(cssFileExistenceRes != null) {
					return cssFileExistenceRes;
				}
			} catch (Exception e) {
				LOGGER.error("Exception while getting CSS File existence response! " + e); //$NON-NLS-1$
			}
		}
		
		return null;
	}
	
	/**
	 * Creates the css file for user.
	 *
	 * @param file the file
	 * @param isApplied the is applied
	 */
	public void createCssFileForUser(final File file, final boolean isApplied) {
		if (!isInOffline) {
			try {
				CssSettingsController cssSettingsController = new CssSettingsController();
				CssSettingsRequest cssSettingsRequest = new CssSettingsRequest();
				cssSettingsRequest.setApplied(isApplied);
				cssSettingsRequest.setFileName(file.getName());
				cssSettingsRequest.setFileContent(FileUtils.readFileToString(file, "UTF-8"));
				cssSettingsController.createCssSetting(cssSettingsRequest);
			} catch (Exception e) {
				LOGGER.error("Exception while creating CSS file for user! " + e); //$NON-NLS-1$
			}
		}
	}
	
	/**
	 * Delete css file for user.
	 */
	public void deleteCssFileForUser() {
		if (!isInOffline) {
			try {
				CssSettingsController cssSettingsController = new CssSettingsController();
				cssSettingsController.deleteCssSetting();
			} catch (Exception e) {
				LOGGER.error("Exception while deleting CSS file for user! " + e); //$NON-NLS-1$
			}
		}
	}
	
	/**
	 * Update css settings.
	 *
	 * @param isApplied the is applied
	 */
	public void updateCssSettings(final boolean isApplied) {
		if (!isInOffline) {
			try {
				CssSettingsController cssSettingsController = new CssSettingsController();
				cssSettingsController.updateCssSetting(isApplied);
			} catch (Exception e) {
				LOGGER.error("Exception while updating CSS settings for user! " + e); //$NON-NLS-1$
			}
		}
	}
	
	/**
	 * Checks if is rearrange settings available.
	 *
	 * @param siteId the site id
	 * @param projectId the project id
	 * @return the rearrange applications response
	 */
	public RearrangeApplicationsResponse isRearrangeSettingsAvailable(final String siteId, final String projectId) {
		if (!isInOffline) {
			try {
				ApplicationsRearrangeController applicationsRearrangeController = new ApplicationsRearrangeController();
				RearrangeApplicationsRequest rearrangeApplicationsRequest = new RearrangeApplicationsRequest();
				rearrangeApplicationsRequest.setSiteId(siteId);
				rearrangeApplicationsRequest.setProjectId(projectId);
				RearrangeApplicationsResponse rearrangeApplicationsResponse = applicationsRearrangeController
						.getRearrangeSettingsRes(rearrangeApplicationsRequest);
				if (rearrangeApplicationsResponse != null) {
					return rearrangeApplicationsResponse;
				}
			} catch (Exception e) {
				LOGGER.error("Exception while getting reaarange settings response! " + e); //$NON-NLS-1$
			}
		}

		return null;
	}
	
	/**
	 * Gets the updated user app list.
	 *
	 * @param userApplicationsString the user applications string
	 * @param userAppNameList the user app name list
	 * @return the updated user app list
	 */
	public List<UserApplication> getUpdatedUserAppList(final String userApplicationsString,
			final List<String> userAppNameList) {
		List<UserApplication> finalUserAppList = new ArrayList<>();
		if(userApplicationsString != null) {
			List<String> userApplicationsListString = Arrays.asList(userApplicationsString.split("\\s*,\\s*"));
			List<String> finalUserNameList = new ArrayList<>();
			for (String userAppName : userApplicationsListString) {
				if (userAppNameList.contains(userAppName)) {
					finalUserNameList.add(userAppName);
				}
			}
			
			for (String userAppName : userAppNameList) {
				if (!userApplicationsListString.contains(userAppName)) {
					finalUserNameList.add(userAppName);
				}
			}
			
			for (String userApplicationName : finalUserNameList) {
				UserApplication userApplicationWithChild = getUserApplicationWithChild(userApplicationName);
				if(userApplicationWithChild != null) {
					finalUserAppList.add(userApplicationWithChild);
				}
			}
		} else {
			for (String userApplicationName : userAppNameList) {
				UserApplication userApplicationWithChild = getUserApplicationWithChild(userApplicationName);
				if(userApplicationWithChild != null) {
					finalUserAppList.add(userApplicationWithChild);
				}
			}
		}

		return finalUserAppList;
	}
	
	/**
	 * Gets the updated project app list.
	 *
	 * @param projectApplicationsString the project applications string
	 * @param projectAppNameList the project app name list
	 * @return the updated project app list
	 */
	public List<ProjectApplication> getUpdatedProjectAppList(final String projectApplicationsString,
			final List<String> projectAppNameList) {
		List<ProjectApplication> finalProjectAppList = new ArrayList<>();
		if(projectApplicationsString != null) {
			List<String> projectApplicationsListString = Arrays.asList(projectApplicationsString.split("\\s*,\\s*"));
			List<String> finalProjectNameList = new ArrayList<>();
			for (String projectAppName : projectApplicationsListString) {
				if(projectAppNameList.contains(projectAppName)) {
					finalProjectNameList.add(projectAppName);
				}
			}
			
			for (String projectAppName : projectAppNameList) {
				if(!projectApplicationsListString.contains(projectAppName)) {
					finalProjectNameList.add(projectAppName);
				}
			}
			
			for (String projectApplicationName : finalProjectNameList) {
				ProjectApplication projectApplicationWithChild = getProjectApplicationWithChild(projectApplicationName);
				if(projectApplicationWithChild != null) {
					finalProjectAppList.add(projectApplicationWithChild);
				}
			}
		} else {
			for (String projectApplicationName : projectAppNameList) {
				ProjectApplication projectApplicationWithChild = getProjectApplicationWithChild(projectApplicationName);
				if (projectApplicationWithChild != null) {
					finalProjectAppList.add(projectApplicationWithChild);
				}
			}
		}
		
		return finalProjectAppList;
	}
	
	/**
	 * Save or update reaarange settings.
	 *
	 * @param userAppFromJSonList the user app from J son list
	 * @param projectAppFromJSonList the project app from J son list
	 * @param projectId the project id
	 */
	public void saveOrUpdateReaarangeSettings(final List<UserApplication> userAppFromJSonList,
			final List<ProjectApplication> projectAppFromJSonList, final String projectId) {
		String siteId = CaxMenuObjectModelUtil.getInstance().getSiteId();
		if (!isInOffline) {
			try {
				List<String> userAppNameStringList = new ArrayList<>();
				List<String> projectAppNameStringList = new ArrayList<>();
				for (UserApplication userApplication : userAppFromJSonList) {
					String userAppName = userApplication.getName();
					if(userAppName != null){
						userAppNameStringList.add(userAppName);
					}
				}
				
				for (ProjectApplication projectApplication : projectAppFromJSonList) {
					String projectAppName = projectApplication.getName();
					if(projectAppName != null) {
						projectAppNameStringList.add(projectAppName);
					}
				}
				
				String userApplications = String.join(",", userAppNameStringList);
				String projectApplciations = String.join(",", projectAppNameStringList);
				ApplicationsRearrangeController applicationsRearrangeController = new ApplicationsRearrangeController();
				RearrangeApplicationsRequest rearrangeApplicationsRequest = new RearrangeApplicationsRequest();
				rearrangeApplicationsRequest.setUserApplications(userApplications);
				rearrangeApplicationsRequest.setProjectApplciations(projectApplciations);
				rearrangeApplicationsRequest.setSiteId(siteId);
				rearrangeApplicationsRequest.setProjectId(projectId);
				boolean createOrUpdateRearrangeSetting = applicationsRearrangeController.createOrUpdateRearrangeSetting(rearrangeApplicationsRequest);
				if (createOrUpdateRearrangeSetting) {
					LOGGER.info("Rearrange of applications is successful");
				}
			} catch (Exception e) {
				LOGGER.error("Exception while creating or updating reaarange settings! " + e); //$NON-NLS-1$
			}
		}
	}
	
	/**
	 * Gets the project application with child.
	 *
	 * @param projectApplicationName the project application name
	 * @return the project application with child
	 */
	private ProjectApplication getProjectApplicationWithChild(final String projectApplicationName) {
		try{
			ProjectAppController projectAppController = new ProjectAppController();
			ProjectApplciationMenuWrapper projectApplciationMenuWrapper = projectAppController.getProjectAppByProjectAppName(projectApplicationName);
			
			if (projectApplciationMenuWrapper != null) {
				Iterable<ProjectApplicationMenuResponse> projectAppMenuResponses = projectApplciationMenuWrapper
						.getProjectAppMenuResponses();
				if (projectAppMenuResponses != null) {
					Iterator<ProjectApplicationMenuResponse> iterator = projectAppMenuResponses.iterator();
					while (iterator.hasNext()) {
						ProjectApplicationMenuResponse projectApplicationMenuResponse = iterator.next();
						ProjectApplicationsTbl projectApplicationsTbl = projectApplicationMenuResponse
								.getProjectApplicationsTbl();
						ProjectApplication projectApplication = getUpdatedProjectApplication(
								projectApplicationsTbl);
						if (!isPlatformSupportedApp(projectApplication.getBaseApplication())) {
							continue;
						}

						Iterable<ProjectApplicationsTbl> projectAppChildren = projectApplicationMenuResponse
								.getProjectAppChildren();
						if (projectAppChildren != null) {
							Iterator<ProjectApplicationsTbl> iteratorChildren = projectAppChildren.iterator();
							while (iteratorChildren.hasNext()) {
								ProjectApplicationsTbl projectApplicationsTblChild = iteratorChildren.next();
								ProjectApplication projectApplicationChild = getUpdatedProjectApplication(
										projectApplicationsTblChild);
								if (!isPlatformSupportedApp(projectApplicationChild.getBaseApplication())) {
									continue;
								}

								projectApplication.addChildProjectApplication(projectApplicationChild);
							}
						}
						
						return projectApplication;
					}
				}
			}
			
		} catch(Exception e) {
			LOGGER.error("Exception while getting projectApplication by using projectApplication name! " + e); //$NON-NLS-1$
		}
		
		return null;
	}
	
	/**
	 * Gets the user application with child.
	 *
	 * @param userAppname the user appname
	 * @return the user application with child
	 */
	private UserApplication getUserApplicationWithChild(final String userAppname) {
		try {
			UserAppController userAppController = new UserAppController();
			UserApplicationMenuWrapper userApplicationMenuWrapper = userAppController.getUserAppByUserAppName(userAppname);
			
			if (userApplicationMenuWrapper != null) {
				Iterable<UserApplicationMenuResponse> userAppMenuResponses = userApplicationMenuWrapper
						.getUserAppMenuResponses();
				if (userAppMenuResponses != null) {
					Iterator<UserApplicationMenuResponse> iterator = userAppMenuResponses.iterator();
					while (iterator.hasNext()) {
						UserApplicationMenuResponse userApplicationMenuResponse = iterator.next();
						UserApplicationsTbl userApplicationsTbl = userApplicationMenuResponse
								.getUserApplicationsTbl();
						UserApplication userApplication = getUpdatedUserApplication(userApplicationsTbl);
						if (!isPlatformSupportedApp(userApplication.getBaseApplication())) {
							continue;
						}

						Iterable<UserApplicationsTbl> userAppChildren = userApplicationMenuResponse
								.getUserAppChildren();

						if (userAppChildren != null) {
							Iterator<UserApplicationsTbl> iteratorChildren = userAppChildren.iterator();
							while (iteratorChildren.hasNext()) {
								UserApplicationsTbl userApplicationsTblChild = iteratorChildren.next();
								UserApplication userApplicationChild = getUpdatedUserApplication(
										userApplicationsTblChild);
								if (!isPlatformSupportedApp(userApplicationChild.getBaseApplication())) {
									continue;
								}

								userApplication.addChildUserApplication(userApplicationChild);
							}
						}
						return userApplication;
					}
				}
			}
			
		} catch (Exception e) {
			LOGGER.error("Exception while getting userApplication by using userApplication name! " + e); //$NON-NLS-1$
		}
		
		return null;
	}
	
	/**
	 * Gets the project id from project name.
	 *
	 * @param projectName the project name
	 * @return the project id from project name
	 */
	public String getProjectIdFromProjectName(final String projectName) {
		if (!isInOffline) {
			try {
				if(projectName != null) {
					ProjectController projectController = new ProjectController();
					ProjectsTbl projectByName = projectController.getProjectByName(projectName);
					String projectId = projectByName.getProjectId();
					if(projectId != null) {
						return projectId;
					}
				}
			} catch (Exception e) {
				LOGGER.error("Exception while getting reaarange settings response! " + e); //$NON-NLS-1$
			}
		}
		
		return null;
	}
}

