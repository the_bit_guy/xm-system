package com.magna.xmsystem.xmenu.ui.rearrangeapps;

import org.eclipse.jface.layout.TableColumnLayout;
import org.eclipse.jface.viewers.ColumnWeightData;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TableViewerColumn;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The Class UserAppTableViewer.
 * 
 * @author subash.janarthanan
 * 
 */
public class UserAppTableViewer extends TableViewer {

	/** The Constant LOGGER. */
	private static final Logger LOGGER = LoggerFactory.getLogger(UserAppTableViewer.class);

	/**
	 * Instantiates a new user app table viewer.
	 *
	 * @param parent
	 *            the parent
	 */
	public UserAppTableViewer(final Composite parent) {
		super(parent, SWT.FULL_SELECTION | SWT.V_SCROLL | SWT.NO_SCROLL | SWT.BORDER | SWT.SINGLE);
		init();
	}

	/**
	 * Init.
	 */
	private void init() {
		try {
			final Table table = this.getTable();
			table.setLinesVisible(true);
			table.setLayoutData(new GridData(GridData.FILL_BOTH));
			initColumn();
		} catch (Exception e) {
			LOGGER.error("Execption ocuured at creating the UI", e); //$NON-NLS-1$
		}
	}

	/**
	 * Init column.
	 */
	private void initColumn() {
		try {
			final TableColumnLayout layout = new TableColumnLayout();
			this.getTable().getParent().setLayout(layout);
			createTableViewerColumn(layout);
		} catch (Exception e) {
			LOGGER.error("Execption ocuured at creating the columns", e); //$NON-NLS-1$
		}
	}

	/**
	 * Create table viewer column.
	 *
	 * @param layout
	 *            the layout
	 */
	private void createTableViewerColumn(final TableColumnLayout layout) {
		final TableViewerColumn viewerCol = new TableViewerColumn(this, SWT.NONE);
		final TableColumn column = viewerCol.getColumn();
		column.setWidth(100);
		layout.setColumnData(column, new ColumnWeightData(50));
	}
}
