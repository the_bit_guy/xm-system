 
package com.magna.xmsystem.xmenu.ui.handlers;

import javax.inject.Inject;
import javax.inject.Named;

import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.ui.css.swt.internal.theme.ThemeEngine;
import org.eclipse.e4.ui.css.swt.theme.IThemeEngine;
import org.eclipse.e4.ui.model.application.MApplication;
import org.eclipse.e4.ui.model.application.ui.basic.MPart;
import org.eclipse.e4.ui.model.application.ui.basic.MWindow;
import org.eclipse.e4.ui.services.IServiceConstants;
import org.eclipse.e4.ui.workbench.modeling.EModelService;
import org.eclipse.swt.widgets.Shell;

import com.magna.xmsystem.xmenu.ui.parts.XmMenuPart;
import com.magna.xmsystem.xmenu.ui.parts.XmMenuPartAction;
import com.magna.xmsystem.xmenu.ui.service.LoadDataFromService;
import com.magna.xmsystem.xmenu.ui.utils.CommonConstants;
import com.magna.xmsystem.xmenu.ui.utils.XmMenuUtil;

/**
 * The Class ResetHandler.
 * 
 * @author subash.janarthanan
 */
@SuppressWarnings("restriction")
public class ResetHandler {
	
	/**
	 * Inject of {@link EModelService}
	 */
	@Inject
	private EModelService modelService;
	
	/**
	 * Execute.
	 */
	@Execute
	public void execute(final MApplication application, final IThemeEngine engine, @Named(IServiceConstants.ACTIVE_SHELL) final Shell shell) {
		MPart part = (MPart) this.modelService.find(CommonConstants.PART_ID.XMENU_ID, application);
		final XmMenuPart xmMenuPart = (XmMenuPart) part.getObject();
		final XmMenuPartAction xmMenuPartAction = xmMenuPart.getXmMenuPartAction();
		resetWindowSize(application, shell);
		resetTheme(engine, xmMenuPartAction);
	}

	/**
	 * Reset theme.
	 *
	 * @param engine the engine
	 * @param xmMenuPartAction 
	 */
	private void resetTheme(final IThemeEngine engine, XmMenuPartAction xmMenuPartAction) {
		if(engine != null) {
			((ThemeEngine) engine).resetCurrentTheme();
			engine.setTheme(((ThemeEngine) engine).getThemes().get(0), true);
			LoadDataFromService.getInstance().updateCssSettings(false);
			xmMenuPartAction.reloadApplications();
			xmMenuPartAction.updateXMenuPanel();
		}
	}

	/**
	 * Reset window size.
	 *
	 * @param application the application
	 * @param shell 
	 */
	private void resetWindowSize(final MApplication application, final Shell shell) {
		if(shell != null && application != null) {
			shell.setMaximized(false);
			int appWindowXCoordinate = XmMenuUtil.getInstance().getAppWindowXCoordinate();
			int appWindowYCoordinate = XmMenuUtil.getInstance().getAppWindowYCoordinate();
			
			MWindow mWindow = application.getChildren().get(0);
			mWindow.setX(appWindowXCoordinate);
			mWindow.setY(appWindowYCoordinate);
			mWindow.setWidth(CommonConstants.APP_WINDOW_DEFAULT_WIDTH);
			mWindow.setHeight(CommonConstants.APP_WINDOW_DEFAULT_HEIGHT);
		}
	}
	
}