package com.magna.xmsystem.xmenu.ui.enums;

import java.io.Serializable;

/**
 * Enum for Start application type.
 *
 * @author Chiranjeevi.Akula
 */
public enum StartApplicationType implements Serializable {
	SCRIPT, MESSAGE
}
