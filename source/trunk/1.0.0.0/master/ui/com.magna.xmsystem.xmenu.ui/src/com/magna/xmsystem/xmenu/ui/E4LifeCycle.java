package com.magna.xmsystem.xmenu.ui;

import java.util.Map;

import javax.inject.Inject;
import javax.inject.Named;

import org.eclipse.e4.core.contexts.ContextInjectionFactory;
import org.eclipse.e4.core.contexts.IEclipseContext;
import org.eclipse.e4.core.di.annotations.Optional;
import org.eclipse.e4.core.services.events.IEventBroker;
import org.eclipse.e4.core.services.nls.Translation;
import org.eclipse.e4.ui.model.application.MApplication;
import org.eclipse.e4.ui.model.application.ui.basic.MPart;
import org.eclipse.e4.ui.model.application.ui.basic.MWindow;
import org.eclipse.e4.ui.services.IServiceConstants;
import org.eclipse.e4.ui.workbench.UIEvents;
import org.eclipse.e4.ui.workbench.lifecycle.PostContextCreate;
import org.eclipse.e4.ui.workbench.lifecycle.PreSave;
import org.eclipse.e4.ui.workbench.lifecycle.ProcessAdditions;
import org.eclipse.e4.ui.workbench.lifecycle.ProcessRemovals;
import org.eclipse.e4.ui.workbench.modeling.EModelService;
import org.eclipse.e4.ui.workbench.modeling.IWindowCloseHandler;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Monitor;
import org.eclipse.swt.widgets.Shell;
import org.osgi.service.event.Event;
import org.osgi.service.event.EventHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;
import com.magna.xmbackend.vo.user.AuthResponse;
import com.magna.xmsystem.dependencies.utils.APPLICATION;
import com.magna.xmsystem.dependencies.utils.XMSystemUtil;
import com.magna.xmsystem.dependencies.utils.XmSystemEnvProcess;
import com.magna.xmsystem.ui.controls.widgets.CustomMessageDialog;
import com.magna.xmsystem.xmenu.message.Message;
import com.magna.xmsystem.xmenu.ui.parts.BaseAppExecution;
import com.magna.xmsystem.xmenu.ui.parts.XmMenuPart;
import com.magna.xmsystem.xmenu.ui.parts.XmMenuPartAction;
import com.magna.xmsystem.xmenu.ui.service.LoadDataFromService;
import com.magna.xmsystem.xmenu.ui.utils.CaxMenuObjectModelUtil;
import com.magna.xmsystem.xmenu.ui.utils.CommonConstants;
import com.magna.xmsystem.xmenu.ui.utils.XmMenuUtil;

/**
 * This is a stub implementation containing e4 LifeCycle annotated
 * methods.<br />
 * There is a corresponding entry in <em>plugin.xml</em> (under the
 * <em>org.eclipse.core.runtime.products' extension point</em>) that references
 * this class.
 * 
 * @author shashwat.anand
 **/
@SuppressWarnings("restriction")
public class E4LifeCycle {
	
	/**
	 * Logger instance
	 */
	private static final Logger LOGGER = LoggerFactory.getLogger(E4LifeCycle.class);
	/**
	 * Injecting event broker
	 */
	@Inject
	private IEventBroker eventBroker;

	/**
	 * Inject {@link IEclipseContext}
	 */
	@Inject
	private IEclipseContext eclipseContext;

	/** Inject of {@link EModelService}. */
	@Inject
	private EModelService modelService;

	/**
	 * Inject of {@link MApplication}
	 */
	@Inject
	@Optional
	private MApplication application;
	
	/** The is main window shell initialized. */
	private boolean isMainWindowShellInitialized;
	
	/** Member variable 'message' for {@link Message}. */
	@Inject
	@Translation
	private Message message;
	
	/** Member variable 'user settings' for {@link Map<String,String>}. */
	private Map<String, String> userSettings;

	@PostContextCreate
	void postContextCreate(IEclipseContext workbenchContext) {
		/*
		 * final Shell shell = new Shell(SWT.INHERIT_NONE); if
		 * (!isEvnVariablesPresent(shell)) { // we don't have a workbench yet...
		 * System.exit(0); }
		 */

		XmSystemEnvProcess.getInstance().start(APPLICATION.XMMENU);

		if(XMSystemUtil.getXMSystemServerScriptFolder() == null) {
			CustomMessageDialog.openError(Display.getDefault().getActiveShell(), message.errorDialogTitile, message.scriptsNotReachableMsg);
			System.exit(0);
		}
		
		initXmMenuUtil();
		initLoadDataFromService();
		initCaxMenuObjectModelUtil();
		initBaseAppExecution();
		
		String adminAccessUsernamePrefs = XmMenuUtil.getInstance().getEclipsePrefs(CommonConstants.PREFERENCE_KEY.ADMIN_MENU_ACCESS_USERNAME);
		String adminAccessTktPrefs = XmMenuUtil.getInstance().getEclipsePrefs(CommonConstants.PREFERENCE_KEY.ADMIN_MENU_ACCESS_TKT);
		if (!XMSystemUtil.isEmpty(adminAccessUsernamePrefs.replace("\"", "")) && !XMSystemUtil.isEmpty(adminAccessTktPrefs.replace("\"", ""))) {
			XmMenuUtil.getInstance().setINEclipsePrefs(CommonConstants.PREFERENCE_KEY.ADMIN_MENU_ACCESS_USERNAME, "");
			XmMenuUtil.getInstance().setINEclipsePrefs(CommonConstants.PREFERENCE_KEY.ADMIN_MENU_ACCESS_TKT, "");
			
			String adminAccessUsername = (new Gson()).fromJson(adminAccessUsernamePrefs, String.class);
			String adminAccessTkt = (new Gson()).fromJson(adminAccessTktPrefs, String.class);
			
			XMSystemUtil.setSystemUserName(adminAccessUsername);
			XMSystemUtil.getInstance().setTicket(adminAccessTkt);
			XmMenuUtil.getInstance().setSwitchedUser(true);
		} else {
			userLogin();
		}
		
		//loading required user settings
		LoadDataFromService loadDataFromService = LoadDataFromService.getInstance();
		loadDataFromService.loadUserSettings();
		userSettings = loadDataFromService.getUserSettings();
		if(userSettings != null) {
			String language = userSettings.get(CommonConstants.USER_SETTINGS.LANGUAGE);
			XmMenuUtil.getInstance().setDefaultLocale(language);
		}
	}

	@PreSave
	void preSave(IEclipseContext workbenchContext) {
	}

	@ProcessAdditions
	void processAdditions(IEclipseContext workbenchContext, MApplication mApplication) {
		eventBroker.subscribe(UIEvents.UILifeCycle.APP_STARTUP_COMPLETE, new EventHandler() {
			/**
			 * Handles event
			 */
			@Override
			public void handleEvent(final Event event) {
				appStartupComplete(workbenchContext, mApplication);
				
				MWindow mainWindow = findMainWindow(mApplication);

				mainWindow.getContext().set(IWindowCloseHandler.class, new IWindowCloseHandler() {

					@Override
					public boolean close(final MWindow window) {
						MPart part = (MPart) modelService.find(CommonConstants.PART_ID.XMENU_ID, application);
						XmMenuPart xmMenuPart = (XmMenuPart) part.getObject();
						XmMenuPartAction xmMenuPartAction = xmMenuPart.getXmMenuPartAction();
						xmMenuPartAction.closeApplication();
						return false;
					}
				});
			}
		});
	}
	
	/**
	 * Find main window.
	 *
	 * @param application the application
	 * @return the m window
	 */
	private MWindow findMainWindow(final MApplication application) {
		return application.getChildren().get(0);
	}

	@ProcessRemovals
	void processRemovals(IEclipseContext workbenchContext) {
	}

	/*
	 * private boolean isEvnVariablesPresent(final Shell shell) {
	 * XmSystemEnvProcess instance1 = XmSystemEnvProcess.getInstance();
	 * instance1.start(APPLICATION.XMMENU); setSystemSpecificEnvVariables();
	 * initXmMenuUtil(); initLoadDataFromService();
	 * initCaxMenuObjectModelUtil(); initBaseAppExecution(); return true; }
	 */

	/**
	 * Method for App startup complete.
	 *
	 * @param workbenchContext
	 *            {@link IEclipseContext}
	 * @param mApplication
	 *            {@link MApplication}
	 */
	private void appStartupComplete(final IEclipseContext workbenchContext, final MApplication mApplication) {
		// copyServerImageFiles();

		XMSystemUtil.cleanupFilesOnExit();

		MPart part;
		if ((part = (MPart) this.modelService.find(CommonConstants.PART_ID.XMENU_ID, mApplication)) != null) {
			final Object partObj = part.getObject();
			if (partObj instanceof XmMenuPart) {
				XmMenuPartAction xmMenuPartAction = ((XmMenuPart) partObj).getXmMenuPartAction();
				if (xmMenuPartAction != null) {
					xmMenuPartAction.initApplication();
				}
			}
		}
	}

	/*private void copyServerImageFiles() {
		try {
			File serverIconFolder;
			if ((serverIconFolder = XMSystemUtil.getXMSystemServerIconsFolder()) != null) {
				String localIconDirectoryPath = XmMenuUtil.getInstance().getLocalIconDirectoryPath();
				File localIconDirectory = new File(localIconDirectoryPath);
				if (localIconDirectory.exists()) {
					FileUtils.deleteDirectory(localIconDirectory);
				}
				
				if(localIconDirectory.mkdirs()){
					FileUtils.copyDirectory(serverIconFolder, localIconDirectory);
				}
			}
		} catch (IOException e) {
			LOGGER.error("Error occured while copying icons ! " + e);
		}
	}*/

	/**
	 * Method to init XMAdminUtil
	 */
	private void initXmMenuUtil() {
		XmMenuUtil util = ContextInjectionFactory.make(XmMenuUtil.class, eclipseContext);
		eclipseContext.set(XmMenuUtil.class, util);
	}

	/**
	 * Method for Inits the cax menu object model util.
	 */
	private void initCaxMenuObjectModelUtil() {
		CaxMenuObjectModelUtil caxMenuObjectModelUtil = ContextInjectionFactory.make(CaxMenuObjectModelUtil.class,
				eclipseContext);
		eclipseContext.set(CaxMenuObjectModelUtil.class, caxMenuObjectModelUtil);
	}

	/**
	 * Method for Inits the load data from service.
	 */
	private void initLoadDataFromService() {
		LoadDataFromService loadDataFromService = ContextInjectionFactory.make(LoadDataFromService.class,
				eclipseContext);
		eclipseContext.set(LoadDataFromService.class, loadDataFromService);
	}

	/**
	 * Method for Inits the base app execution.
	 */
	private void initBaseAppExecution() {
		BaseAppExecution baseAppExecution = ContextInjectionFactory.make(BaseAppExecution.class, eclipseContext);
		eclipseContext.set(BaseAppExecution.class, baseAppExecution);
	}

	/**
	 * Receive active shell.
	 * 
	 * @param shell
	 *            the shell
	 */
	@Inject
	@Optional
	public void receiveActiveShell(@Named(IServiceConstants.ACTIVE_SHELL) final Shell shell) {
		try {
			if (shell != null && !isMainWindowShellInitialized) {
				isMainWindowShellInitialized = true;

				final Display display = shell.getDisplay();
				final Monitor primary = display.getPrimaryMonitor();
				final Rectangle displayBounds = primary.getBounds();
				final Point size = shell.getSize();
				int xAxis = (int) (displayBounds.width - size.x) / 2;
				int yAxis = (int) (displayBounds.height - size.y) / 2;
				
				XmMenuUtil.getInstance().setAppWindowXCoordinate(xAxis);
				XmMenuUtil.getInstance().setAppWindowYCoordinate(yAxis);
				
				int width = size.x;
				int height = size.y;

				Map<String, String> userSettingsMap = LoadDataFromService.getInstance().getUserSettings();
				
				if (userSettingsMap != null) {
					String HOST_NAME = userSettingsMap.get(CommonConstants.USER_SETTINGS.HOST_NAME);
					if (HOST_NAME != null && HOST_NAME.equals(XMSystemUtil.getHostName())) {
						String WINDOW_X = userSettingsMap.get(CommonConstants.USER_SETTINGS.WINDOW_X);
						String WINDOW_Y = userSettingsMap.get(CommonConstants.USER_SETTINGS.WINDOW_Y);
						String WINDOW_W = userSettingsMap.get(CommonConstants.USER_SETTINGS.WINDOW_W);
						String WINDOW_H = userSettingsMap.get(CommonConstants.USER_SETTINGS.WINDOW_H);

						if (WINDOW_X != null && WINDOW_Y != null && WINDOW_W != null && WINDOW_H != null) {
							int x = Integer.parseInt(WINDOW_X);
							int y = Integer.parseInt(WINDOW_Y);
							int w = Integer.parseInt(WINDOW_W);
							int h = Integer.parseInt(WINDOW_H);

							Rectangle currentDispalyBounds = display.getBounds();

							if (currentDispalyBounds.width > x && currentDispalyBounds.height > y) {
								xAxis = x;
								yAxis = y;
								width = w;
								height = h;
							}
						}
					}
				}

				shell.setLocation(xAxis, yAxis);
				shell.setSize(width, height);
				if (shell != null) {
					XMSystemUtil.forceActive(shell);
				}
				XmMenuUtil.getInstance().setMainWindowShell(shell);
			}
		} catch (Exception e) {
			LOGGER.error("Execption ocuured in Active Shell", e); //$NON-NLS-1$
		}
	}
	
    /**
    * Init valid user login.
    */
	private void userLogin() {
		final String systemUserName = XMSystemUtil.getSystemUserName();
		AuthResponse authorizeUser = LoadDataFromService.getInstance().authorizeUser(systemUserName);
		
		if(authorizeUser != null){
			boolean isvalidUser = authorizeUser.isValidUser();
			if (!isvalidUser) {
				CustomMessageDialog.openError(Display.getCurrent().getActiveShell(), message.windowTitleLabel, authorizeUser.getMessage());
				System.exit(0);
			} 
		} else {
			CustomMessageDialog.openError(Display.getCurrent().getActiveShell(), message.windowTitleLabel, message.serverNotReachable);
		}		
	}

}
