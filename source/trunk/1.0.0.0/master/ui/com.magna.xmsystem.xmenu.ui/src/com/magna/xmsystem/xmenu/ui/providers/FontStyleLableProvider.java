package com.magna.xmsystem.xmenu.ui.providers;

import org.eclipse.jface.viewers.LabelProvider;

/**
 * The Class FontStyleLableProvider.
 */
public class FontStyleLableProvider extends LabelProvider {
	
	/* (non-Javadoc)
	 * @see org.eclipse.jface.viewers.LabelProvider#getText(java.lang.Object)
	 */
	@Override
	public String getText(Object element) {
		return element.toString();
	}

}
