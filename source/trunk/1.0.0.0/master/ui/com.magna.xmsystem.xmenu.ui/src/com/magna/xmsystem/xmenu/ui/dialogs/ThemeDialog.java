package com.magna.xmsystem.xmenu.ui.dialogs;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;

import org.eclipse.core.runtime.Platform;
import org.eclipse.e4.ui.css.core.dom.CSSProperty;
import org.eclipse.e4.ui.css.core.engine.CSSEngine;
import org.eclipse.e4.ui.css.swt.engine.CSSSWTEngineImpl;
import org.eclipse.e4.ui.css.swt.internal.theme.ThemeEngine;
import org.eclipse.e4.ui.css.swt.theme.ITheme;
import org.eclipse.e4.ui.css.swt.theme.IThemeEngine;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.layout.GridDataFactory;
import org.eclipse.jface.resource.JFaceResources;
import org.eclipse.jface.resource.LocalResourceManager;
import org.eclipse.jface.resource.ResourceManager;
import org.eclipse.jface.viewers.ColumnLabelProvider;
import org.eclipse.jface.viewers.ComboViewer;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.jface.viewers.TreeViewerColumn;
import org.eclipse.jface.viewers.ViewerCell;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.TreeEditor;
import org.eclipse.swt.events.MouseAdapter;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.FontData;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.RGB;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.ColorDialog;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.widgets.Tree;
import org.eclipse.swt.widgets.TreeColumn;
import org.eclipse.swt.widgets.TreeItem;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.css.sac.InputSource;
import org.w3c.dom.css.CSSRuleList;
import org.w3c.dom.css.CSSStyleSheet;

import com.magna.xmbackend.entities.CssSettingsTbl;
import com.magna.xmbackend.vo.css.CssSettingsResponse;
import com.magna.xmsystem.dependencies.utils.XMSystemUtil;
import com.magna.xmsystem.ui.controls.widgets.CustomMessageDialog;
import com.magna.xmsystem.xmenu.message.Message;
import com.magna.xmsystem.xmenu.ui.parts.XmMenuPartAction;
import com.magna.xmsystem.xmenu.ui.providers.CssThemeLabelProvider;
import com.magna.xmsystem.xmenu.ui.providers.CssTreeContentProvider;
import com.magna.xmsystem.xmenu.ui.providers.ThemeListContentProvider;
import com.magna.xmsystem.xmenu.ui.service.LoadDataFromService;
import com.magna.xmsystem.xmenu.ui.utils.XmMenuUtil;
import com.steadystate.css.dom.CSSStyleRuleImpl;
import com.steadystate.css.dom.CSSStyleSheetImpl;
import com.steadystate.css.dom.CSSValueImpl;
import com.steadystate.css.dom.Property;
import com.steadystate.css.format.CSSFormat;
import com.steadystate.css.parser.CSSOMParser;
import com.steadystate.css.parser.SACParserCSS21;

/**
 * The Class ThemeDialog.
 */
@SuppressWarnings({ "restriction","unused" })
public class ThemeDialog extends Dialog {
	
	/** The Constant LOGGER. */
	private static final Logger LOGGER = LoggerFactory.getLogger(ThemeDialog.class);
	
	/** The num of themes. */
	private final int NUM_OF_THEMES = 2;
	
	/** The engine. */
	private IThemeEngine engine;
	
	/** The combo viewer. */
	private ComboViewer comboViewer;
	
	/** The first element. */
	private ITheme firstElement;
	
	/** The i CSS theme. */
	private File iCSSTheme;
	
	/** The tree viewer. */
	private TreeViewer treeViewer;
	
	/** The stylesheet. */
	private CSSStyleSheetImpl stylesheet;
	
	/** The btn delete theme. */
	private Button btnDeleteTheme;
	
	/** The text 1. */
	private Text text_1;
	
	/** The btn add theme. */
	private Button btnAddTheme;
	
	/** The css folder. */
	private File cssFolder = new File(Platform.getInstanceLocation().getURL().getPath() + "themecss");
	
	/** The file. */
	private File file = new File(cssFolder.getAbsolutePath() + System.getProperty("file.separator") + "custom.css");
	
	/** The lbl theme. */
	private Label lblTheme;
	
	/** The lbl new label. */
	private Label lblNewLabel;
	
	/** The is applied. */
	boolean isApplied = false;

	// make sure you dispose these buttons when viewer input changes
	/** The is labels. */
	Map<Object, Label> labels = new HashMap<Object, Label>();
	
	/** The is Tree Editor. */
	Map<Object, TreeEditor> editors = new HashMap<Object, TreeEditor>();
	
	/** The font pixel. */
	private String fontPixel;
	
	/** The newfont. */
	private String newfont;
	
	/** The messages. */
	private Message messages;
	
	/** The xm menu part action. */
	private XmMenuPartAction xmMenuPartAction;
	
	/** Member variable 'parent shell' for {@link Shell}. */
	private Shell parentShell;
	
	/**
	 * Create the dialog.
	 *
	 * @param parentShell the parent shell
	 * @param engine the engine
	 * @param xmMenuPartAction 
	 */
	public ThemeDialog(final Shell parentShell, final IThemeEngine engine, final Message messages, XmMenuPartAction xmMenuPartAction) {
		super(parentShell);
		this.parentShell = parentShell;
		this.engine = engine;
		this.messages = messages;
		this.xmMenuPartAction = xmMenuPartAction;
	}

	/* (non-Javadoc)
	 * @see org.eclipse.jface.window.Window#configureShell(org.eclipse.swt.widgets.Shell)
	 */
	@Override
	protected void configureShell(Shell shell) {
		super.configureShell(shell);
		shell.setText(messages.themeDialogTitle);
		shell.setParent(this.parentShell);
		//shell.setSize(500, 400);
	}

	/**
	 * Create contents of the dialog.
	 *
	 * @param parent the parent
	 * @return the control
	 */
	@Override
	protected Control createDialogArea(final Composite parent) {
		parent.setData("org.eclipse.e4.ui.css.id", "MyCSSTagForComposite");
		
		Composite container = (Composite) super.createDialogArea(parent);
		container.setLayout(new GridLayout(3, false));
		GridDataFactory.fillDefaults().grab(true, true).align(SWT.FILL, SWT.FILL).hint(500, 300).applyTo(container);
		container.setData("org.eclipse.e4.ui.css.id", "MyCSSTagForComposite");

		lblTheme = new Label(container, SWT.NONE);
		lblTheme.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblTheme.setText(messages.themeName);
		lblTheme.setData("org.eclipse.e4.ui.css.id", "MyCSSTagForlblTheme");

		comboViewer = new ComboViewer(container, SWT.READ_ONLY);
		Combo combo = comboViewer.getCombo();
		combo.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));

		comboViewer.setContentProvider(new ThemeListContentProvider());
		LabelProvider cssItemsLabelProvider = new CssThemeLabelProvider();
		comboViewer.setLabelProvider(cssItemsLabelProvider);
		comboViewer.addSelectionChangedListener(new ISelectionChangedListener() {

			private Object element;

			@Override
			public void selectionChanged(SelectionChangedEvent event) {
				IStructuredSelection iStructuredSelection = (IStructuredSelection) event.getSelection();
				if (iStructuredSelection.isEmpty()) {
					return;
				}
				element = iStructuredSelection.getFirstElement();
				if (element instanceof File) {
					File file = (File) element;
					CSSStyleSheet cssFile = parseCssFile(file);
					btnDeleteTheme.setEnabled(true);
					btnAddTheme.setEnabled(false);
					disposeTableEditors();
					treeViewer.setInput(cssFile);
					treeViewer.expandAll();
					String str = file.getName();
					String str1 = str.replace(".css", "");
				} else {
					btnDeleteTheme.setEnabled(false);
					ITheme iTheme = (ITheme) element;
					disposeTableEditors();
					CSSEngine object = (CSSSWTEngineImpl) ((ThemeEngine) engine).getCSSEngines().toArray()[0];
					treeViewer.setInput((object.getDocumentCSS()).getStyleSheets().item(0));
					treeViewer.expandAll();
				}
			}
		});

		ArrayList<ITheme> themes = new ArrayList<ITheme>(((ThemeEngine) engine).getThemes());
		comboViewer.setInput(themes);
		new Label(container, SWT.NONE);

		treeViewer = new TreeViewer(container, SWT.BORDER | SWT.FULL_SELECTION);
		Tree tree = treeViewer.getTree();
		tree.setLinesVisible(true);
		tree.setHeaderVisible(true);
		tree.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 2, 1));

		TreeViewerColumn treeViewerColumn_1 = new TreeViewerColumn(treeViewer, SWT.NONE);
		TreeColumn trclmnNewColumn = treeViewerColumn_1.getColumn();
		trclmnNewColumn.setWidth(140);
		trclmnNewColumn.setText(messages.columnName);
		treeViewerColumn_1.setLabelProvider(new ColumnLabelProvider() {
			@Override
			public String getText(Object element) {
				if (element instanceof CSSStyleRuleImpl) {
					CSSStyleRuleImpl cssrule = (CSSStyleRuleImpl) element;
					if (cssrule.getSelectorText().toString().equals("Composite Label")) {
						return messages.cssTreeViewerColumnText;
					} else if (cssrule.getSelectorText().toString().equals("Composite")) {
						return messages.cssTreeViewerColumnApplication;
					} else if (cssrule.getSelectorText().toString().equals("#ApplicationLabel")) {
						return messages.cssTreeViewerColumnUAPA;
					} else if (cssrule.getSelectorText().toString().equals("#ProjectLabel")) {
						return messages.cssTreeViewerColumnProjectList;
					} else if (cssrule.getSelectorText().toString().equals("CustomButton")) {
						return messages.cssTreeViewerColumnBtnApp;
					} 
					return cssrule.getSelectorText();
				} else if (element instanceof Property) {
					Property property = (Property) element;
					if (property.getName().toString().equals("color")) {
						return messages.cssTreeViewerForegroundProp;
					} else if (property.getName().toString().equals("background-color")) {
						return messages.cssTreeViewerBackgroundProp;
					} else if (property.getName().toString().equals("label-background-color")) {
						return messages.cssTreeViewerBackgroundProp;
					} else if (property.getName().toString().equals("label-foreground-color")) {
						return messages.cssTreeViewerTextProp;
					} else if (property.getName().toString().equals("font")) {
						return messages.cssTreeViewerFontProp;
					} else if (property.getName().toString().equals("font-weight")) {
						return messages.cssTreeViewerFontWeightProp;
					} else if (property.getName().toString().equals("font-style")) {
						return messages.cssTreeViewerFontStyleProp;
					} else {
						String substring = property.getName();
						return substring;
					}
				} else if (element instanceof org.eclipse.e4.ui.css.core.impl.dom.CSSStyleRuleImpl) {
					org.eclipse.e4.ui.css.core.impl.dom.CSSStyleRuleImpl cssStyleRuleImpl = (org.eclipse.e4.ui.css.core.impl.dom.CSSStyleRuleImpl) element;

					if (cssStyleRuleImpl.getSelectorText().toString().equals("*[class=\"MPartStack\"]")) {
						return null;
					} else if (cssStyleRuleImpl.getSelectorText().toString().equals("*#ApplicationLabel")) {
						return messages.cssTreeViewerColumnUAPA;
					} else if (cssStyleRuleImpl.getSelectorText().toString().equals("*#ProjectLabel")) {
						return messages.cssTreeViewerColumnProjectList;
					} else if (cssStyleRuleImpl.getSelectorText().toString().equals("Composite Label")) {
						return messages.cssTreeViewerColumnText;
					} else if (cssStyleRuleImpl.getSelectorText().toString().equals("*#MyCSSTagForlblTheme")) {
						return null;
					} else if (cssStyleRuleImpl.getSelectorText().toString().equals("*#MyCSSTagForComposite")) {
						return null;
					} else if (cssStyleRuleImpl.getSelectorText().toString().equals("*#MyCSSTagFortbComboProjects")) {
						return null;
					} 

					return cssStyleRuleImpl.getSelectorText();
				} else if (element instanceof CSSProperty) {
					CSSProperty cssProperty = (CSSProperty) element;
					if (cssProperty.getName().toString().startsWith("swt")) {
						return null;
					} else if (cssProperty.getName().toString().equals("color")) {
						return messages.cssTreeViewerForegroundProp;
					} else if (cssProperty.getName().toString().equals("font")) {
						return messages.cssTreeViewerFontProp;
					} else if (cssProperty.getName().toString().equals("font-weight")) {
						return messages.cssTreeViewerFontWeightProp;
					} else if (cssProperty.getName().toString().equals("font-style")) {
						return messages.cssTreeViewerFontStyleProp;
					} else {
						String substring = cssProperty.getName();
						return substring;
					}
				}

				return null;
			}
		});

		TreeViewerColumn treeViewerColumn_2 = new TreeViewerColumn(treeViewer, SWT.NONE);
		TreeColumn trclmnNewColumn_1 = treeViewerColumn_2.getColumn();
		trclmnNewColumn_1.setWidth(140);
		trclmnNewColumn_1.setText(messages.columnValue);
		treeViewerColumn_2.setLabelProvider(new ColumnLabelProvider(){
			@Override
			public String getText(Object element) {
				if (element instanceof Property) {
					Property property = (Property) element;
					return property.getValue().getCssText();
				} else if (element instanceof CSSProperty) {
					CSSProperty cssProperty = (CSSProperty) element;
					if (cssProperty.getValue().getCssText().startsWith("false")) {
						return null;
					} else if(cssProperty.getValue().getCssText().contains("px")) {
						return cssProperty.getValue().getCssText().replaceFirst("\\d+.\\d*px.*?$", " ");
					} else if(cssProperty.getValue().getCssText().contains("bold")) {
						return cssProperty.getValue().getCssText().replaceFirst("\\d+.\\d*px.*?$", " ");
					} else {
						return cssProperty.getValue().getCssText();
					}
				} 
				return null;
			}
		});

		TreeViewerColumn treeViewerColumn_3 = new TreeViewerColumn(treeViewer, SWT.NONE);
		TreeColumn trclmnNewColumn_2 = treeViewerColumn_3.getColumn();
		trclmnNewColumn_2.setWidth(56);
		trclmnNewColumn_2.setText(messages.columnPreview);
		treeViewerColumn_3.setLabelProvider(new ColumnLabelProvider(){
			
			@Override
			public String getText(Object element) {
				return "";
			}
			@Override
			public Color getBackground(Object element) {

				Color color = null;
				if (element instanceof CSSProperty) {
					CSSProperty cssProperty = (CSSProperty) element;
					String cssText = cssProperty.getValue().getCssText();
					if (cssText.startsWith("rgb")) {
						String[] values = cssText.split(",");
						Integer red = Integer.parseInt(values[0].substring(4, values[0].length()).trim());
						Integer green = Integer.parseInt(values[1].trim());
						Integer blue = Integer.parseInt(values[2].substring(0, values[2].length() - 1).trim());
						return new Color(Display.getDefault(), new RGB(red, green, blue));
					}
				} else if (element instanceof Property) {
					Property property = (Property) element;
					String cssText1 = property.getValue().getCssText();
					if (cssText1.startsWith("rgb")) {
						String[] values1 = cssText1.split(",");
						Integer red1 = Integer.parseInt(values1[0].substring(4, values1[0].length()).trim());
						Integer green1 = Integer.parseInt(values1[1].trim());
						Integer blue1 = Integer.parseInt(values1[2].substring(0, values1[2].length() - 1).trim());

						return new Color(Display.getDefault(), new RGB(red1, green1, blue1));
					}
				}
				return color;
			}
		});

		TreeViewerColumn treeViewerColumn_4 = new TreeViewerColumn(treeViewer, SWT.NONE);
		TreeColumn trclmnNewColumn_3 = treeViewerColumn_4.getColumn();
		trclmnNewColumn_3.setWidth(20);
		trclmnNewColumn_3.setText("");

		treeViewerColumn_4.setLabelProvider(new ColumnLabelProvider() {
			@Override
			public void update(ViewerCell cell) {
				TreeItem item = (TreeItem) cell.getItem();
				if (item.getData() instanceof Property) {
					Label label;
					if (labels.containsKey(cell.getElement())) {
						label = labels.get(cell.getElement());
					} else {
						Image image = XMSystemUtil.getInstance().getImage(this.getClass(), "icons/16x16/rsz_edit.png");
						label = new Label((Composite) cell.getViewerRow().getControl(), SWT.NONE);
						label.setImage(image);
						label.setToolTipText(messages.editSettings);
						label.setBackground(new Color(Display.getCurrent(), new RGB(255, 255, 255)));

						label.setSize(16, 16);

						label.setData(item.getData());
						labels.put(cell.getElement(), label);

						label.addMouseListener(new MouseAdapter() {
							@Override
							public void mouseUp(MouseEvent event) {
								super.mouseUp(event);

								if (event.getSource() instanceof Label) {
									Label label = (Label) event.getSource();

									Object element = label.getData();
									if (element instanceof Property) {
										Property property = (Property) element;

										String key = property.getName();
										if (key.equals("font")) {
											configureFont(property);
										} else if (key.equals("color") || key.equals("background-color")
												|| key.equals("header-background-color")
												|| key.equals("foreground-color")
												|| key.equals("label-background-color")
												|| key.equals("label-background-color")
												|| key.equals("label-foreground-color")) {
											{
												configureColor(property);
											}
										}
										treeViewer.refresh();
									}
								}
							}
						});
					}
					TreeEditor editor = new TreeEditor(item.getParent());
					editor.grabHorizontal = true;
					editor.grabVertical = true;
					editor.setEditor(label, item, cell.getColumnIndex());
					editor.layout();
				} else {
					if (editors.get(cell.getElement()) != null) {
						editors.get(cell.getElement()).getEditor().dispose();
						editors.get(cell.getElement()).dispose();
						editors.remove(cell.getElement());
						labels.remove(cell.getElement());
					}
					cell.getControl().redraw();
				}
			}
		});

		treeViewer.setContentProvider(new CssTreeContentProvider());
		
		// disiabling on hover on tree
		tree.addListener(SWT.EraseItem, new  Listener() {
			public void handleEvent(Event event) {
				
				//allow selection in tree?
				event.detail &= ~SWT.HOT;
				if ((event.detail & SWT.SELECTED) == 0) return; /// item not selected

				Tree tree =(Tree)event.widget;
		        TreeItem item =(TreeItem) event.item;
       
		      Object data =  item.getData();
		       // getBackground(data, 2);
		        TreeColumn tc = tree.getColumn(2);
		        int clientWidth = tree.getColumn(2).getWidth();
		        int xCord = tree.getColumn(0).getWidth()+tree.getColumn(1).getWidth();
		    
		       // int clientWidth = tree.getClientArea().width;
		        GC gc = event.gc;               
		        Color oldForeground = gc.getForeground();
		        Color oldBackground = gc.getBackground();

		        Color background = getBackground(data, 2);
		        if(background != null){
				gc.setBackground( background);
			      gc.setForeground(new Color(Display.getDefault(), new RGB(0, 0, 0)));              
		        gc.fillRectangle(xCord, event.y, clientWidth, event.height);
		        } else{
		  	        gc.setForeground(new Color(Display.getDefault(), new RGB(0, 0, 0)));
				     gc.setBackground(oldBackground);
		        }
		        event.detail &= ~SWT.SELECTED;
			}
		});

		Composite composite = new Composite(container, SWT.NONE);
		composite.setLayout(new GridLayout(1, false));
		composite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, false, false, 1, 1));
		composite.setData("org.eclipse.e4.ui.css.id", "MyCSSTagForComposite");

		btnAddTheme = new Button(composite, SWT.NONE);
		btnAddTheme.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));
		btnAddTheme.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				addNewTheme(btnAddTheme);
			}
		});
		btnAddTheme.setText(messages.addThemeBtnName);
		btnAddTheme.setEnabled(validateThemeCount());
		btnDeleteTheme = new Button(composite, SWT.NONE);
		btnDeleteTheme.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));
		btnDeleteTheme.addSelectionListener(new SelectionAdapter() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				IStructuredSelection selection = (IStructuredSelection) comboViewer.getSelection();
				Object firstElement2 = selection.getFirstElement();
				if (firstElement2 instanceof File) {
					File file = (File) firstElement2;
					boolean openConfirm = CustomMessageDialog.openConfirm(Display.getDefault().getActiveShell(), messages.warningDialogName,
							messages.themeDelete);
					if (openConfirm) {
						file.delete();
						themes.remove(file);
						((ThemeEngine) engine).resetCurrentTheme();
						engine.setTheme(((ThemeEngine) engine).getThemes().get(0), true);
						comboViewer.refresh();
						final ISelection cvSelection = new StructuredSelection(
								((ThemeEngine) engine).getThemes().get(0));
						comboViewer.setSelection(cvSelection);
						btnAddTheme.setEnabled(true);
						LoadDataFromService.getInstance().deleteCssFileForUser();
					}
				}
			}
		});
		btnDeleteTheme.setText(messages.deleteThemeBtnName);

		CssSettingsResponse cssFileExistenceRes = LoadDataFromService.getInstance().getCssFileExistenceRes();
		if (cssFileExistenceRes != null) {
			CssSettingsTbl cssSettingsTbl = cssFileExistenceRes.getCssSettingsTbl();
			if (cssSettingsTbl != null) {
				String appliedStr = cssSettingsTbl.getIsApplied();
				isApplied = Boolean.valueOf(appliedStr);
			}
		}
		if (file.exists() && isApplied) {
			final ISelection cvSelection = new StructuredSelection(file);
			comboViewer.setSelection(cvSelection);
			btnAddTheme.setEnabled(false);
			btnDeleteTheme.setEnabled(true);
		} else {
			final ISelection cvSelection = new StructuredSelection(((ThemeEngine) engine).getThemes().get(0));
			comboViewer.setSelection(cvSelection);
			btnAddTheme.setEnabled(validateThemeCount());
			btnDeleteTheme.setEnabled(false);
		}
		
		final Rectangle parentSize = this.parentShell.getBounds();
		Point computeSize = parent.computeSize(SWT.DEFAULT, SWT.DEFAULT);
		Point defaultLocation = new Point((int) (parentSize.width - computeSize.x) / 2,
				(int) (parentSize.height - computeSize.y) / 2);
		parent.getShell().setLocation(defaultLocation);

		return container;
	}

	/**
	 * Validate theme count.
	 *
	 * @return true, if successful
	 */
	private boolean validateThemeCount() {
		int itemCount = comboViewer.getCombo().getItemCount();
		return itemCount < NUM_OF_THEMES;
	}

	/**
	 * Parses the css file.
	 *
	 * @param cssFile the css file
	 * @return the CSS style sheet
	 */
	@SuppressWarnings("unchecked")
	private CSSStyleSheet parseCssFile(File cssFile) {
		try {
			InputSource inputSource = new InputSource(new FileReader(cssFile));
			CSSOMParser parser = new CSSOMParser(new SACParserCSS21());
			stylesheet = (CSSStyleSheetImpl) parser.parseStyleSheet(inputSource, null, null);
			CSSRuleList cssRules = stylesheet.getCssRules();
			int length = cssRules.getLength();
			@SuppressWarnings("rawtypes")
			ArrayList rules = new ArrayList();
			CSSStyleSheetImpl newStyleSheetImpl = new CSSStyleSheetImpl();

			int j = 0;
			for (int i = 0; i < length; i++) {
				CSSStyleRuleImpl item = (CSSStyleRuleImpl) cssRules.item(i);
				if (item.getSelectorText().equals("Composite Label") || item.getSelectorText().equals("Composite")
						|| item.getSelectorText().equals("#ApplicationLabel")
						|| item.getSelectorText().equals("#ProjectLabel")
						|| item.getSelectorText().equals("CustomButton")
						|| item.getSelectorText().equals("#MyCSSTagForlblTheme")
						|| item.getSelectorText().equals("#MyCSSTagForComposite")) {
					rules.add(item);
					String cssText = item.getCssText();
					newStyleSheetImpl.insertRule(cssText, j);
					j++;
				}
			}

			stylesheet = newStyleSheetImpl;
			return newStyleSheetImpl;
		} catch (FileNotFoundException e) {
			LOGGER.error("FileNotFound Exception while parsing CSS file!" + e); //$NON-NLS-1$
		} catch (IOException e) {
			LOGGER.error("IOException while parsing CSS file!" + e); //$NON-NLS-1$
		}
		return null;
	}

	/**
	 * Create contents of the button bar.
	 *
	 * @param parent the parent
	 */
	@Override
	protected void createButtonsForButtonBar(Composite parent) {
		parent.setData("org.eclipse.e4.ui.css.id", "MyCSSTagForComposite");
		
		final Button resetBtn = createButton(parent, IDialogConstants.CANCEL_ID, messages.resetToDefaultsBtn, true);
		resetBtn.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		
		resetBtn.addSelectionListener(new SelectionAdapter() {

			@Override
			public void widgetSelected(SelectionEvent selectionevent) {
				if(engine != null) {
					((ThemeEngine) engine).resetCurrentTheme();
					engine.setTheme(((ThemeEngine) engine).getThemes().get(0), true);
					LoadDataFromService.getInstance().updateCssSettings(false);
					xmMenuPartAction.reloadApplications();
					xmMenuPartAction.updateXMenuPanel();
				}
			}
		});
		
		GridLayout layout = (GridLayout) parent.getLayout();
		layout.numColumns++;
		layout.makeColumnsEqualWidth = false;
		
		final Button okBtn = createButton(parent, IDialogConstants.OK_ID, IDialogConstants.OK_LABEL, false);
		final Button cancelBtn = createButton(parent, IDialogConstants.CANCEL_ID, messages.themeDialogCancelBtn, false);
	}

	/**
	 * Return the initial size of the dialog.
	 *
	 * @return the initial size
	 */
	/*@Override
	protected Point getInitialSize() {
		return new Point(450, 300);
	}*/

	/* (non-Javadoc)
	 * @see org.eclipse.jface.dialogs.Dialog#okPressed()
	 */
	@Override
	protected void okPressed() {
		IStructuredSelection selection = (IStructuredSelection) comboViewer.getSelection();
		if (selection.getFirstElement() instanceof ITheme) {
			firstElement = (ITheme) selection.getFirstElement();
			LoadDataFromService.getInstance().updateCssSettings(false);
		} else {
			iCSSTheme = (File) selection.getFirstElement();
			CSSFormat format = new CSSFormat();
			format.setRgbAsHex(true);
			try {
				FileOutputStream fos = new FileOutputStream(iCSSTheme);
				String cssText = this.stylesheet.getCssText(format);
				fos.write(cssText.getBytes());
				fos.close();
			} catch (FileNotFoundException e) {
				LOGGER.error("FileNotFound Exception while applying CSS file!" + e); //$NON-NLS-1$
			} catch (IOException e) {
				LOGGER.error("IOException while applying CSS file!" + e); //$NON-NLS-1$
			}
			LoadDataFromService.getInstance().createCssFileForUser(iCSSTheme, true);
		}
		super.okPressed();
	}

	/**
	 * Gets the selected theme.
	 *
	 * @return the selected theme
	 */
	public ITheme getSelectedTheme() {
		return firstElement;
	}

	/**
	 * Gets the CSS engine.
	 *
	 * @return the CSS engine
	 */
	public File getCSSEngine() {
		return iCSSTheme;
	}

	/**
	 * Adds the new theme.
	 *
	 * @param btnAddTheme the btn add theme
	 */
	private void addNewTheme(Button btnAddTheme) {
		try {
			if(!cssFolder.isDirectory()) {
				cssFolder.mkdirs();
				createCSSFile();
			} else {
				createCSSFile();
			}
		} catch (IOException e1) {
			LOGGER.error("IOException while adding new CSS file!" + e1); //$NON-NLS-1$
		}
		comboViewer.refresh();
		btnAddTheme.setEnabled(validateThemeCount());
	}

	/**
	 * Creates the CSS file.
	 *
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	private void createCSSFile() throws IOException {
		if (!file.exists()) {
			file.createNewFile();
			CSSStyleSheetImpl newStyleSheetImpl = new CSSStyleSheetImpl();
			newStyleSheetImpl.insertRule("Composite Label { font: Calibri; color: rgb(128,128,192) } \n", 0);
			newStyleSheetImpl.insertRule("Composite { background-color: rgb(240, 240, 240) ; } \n", 1);
			newStyleSheetImpl.insertRule("#ApplicationLabel {font: Calibri ; } \n", 2);
			newStyleSheetImpl.insertRule("#ProjectLabel {font: Calibri; } \n", 3);
			newStyleSheetImpl.insertRule("CustomButton { label-background-color: rgb(230, 230, 230);label-foreground-color: rgb(0, 0, 0) } \n", 4);
			newStyleSheetImpl.insertRule("#MyCSSTagForlblTheme {background-color: rgb(240,240,240); font: Calibri; } \n", 5);
			newStyleSheetImpl.insertRule("#MyCSSTagForComposite {background-color: rgb(240,240,240) } \n", 6);
			
			CSSFormat format = new CSSFormat();
			format.setRgbAsHex(true);

			try {
				FileOutputStream fos = new FileOutputStream(file);
				String cssText = newStyleSheetImpl.getCssText(format);
				fos.write(cssText.getBytes());
				fos.close();
			} catch (FileNotFoundException e1) {
				LOGGER.error("FileNotFoundException while creating CSS file!" + e1); //$NON-NLS-1$
			} catch (IOException e2) {
				LOGGER.error("IOException while creating CSS file!" + e2); //$NON-NLS-1$
			}
			
			LoadDataFromService.getInstance().createCssFileForUser(file, false);
		}
	}
	
	/**
	 * Gets the background.
	 *
	 * @param element the element
	 * @param columnIndex the column index
	 * @return the background
	 */
	public Color getBackground(Object element, int columnIndex) {
		Color color = null;
		if (element instanceof CSSProperty) {
			CSSProperty cssProperty = (CSSProperty) element;
			switch (columnIndex) {

			case 2:
				String cssText = cssProperty.getValue().getCssText();
				if (cssText.startsWith("rgb")) {
					String[] values = cssText.split(",");
					Integer red = Integer.parseInt(values[0].substring(4, values[0].length()).trim());
					Integer green = Integer.parseInt(values[1].trim());
					Integer blue = Integer.parseInt(values[2].substring(0, values[2].length() - 1).trim());
					return new Color(Display.getDefault(), new RGB(red, green, blue));
					// return null;
				}
				break;
			default:
				break;
			}

		} else if (element instanceof Property) {
			Property property = (Property) element;
			switch (columnIndex) {
			case 2:
				String cssText1 = property.getValue().getCssText();
				if (cssText1.startsWith("rgb")) {
					String[] values1 = cssText1.split(",");
					Integer red1 = Integer.parseInt(values1[0].substring(4, values1[0].length()).trim());
					Integer green1 = Integer.parseInt(values1[1].trim());
					Integer blue1 = Integer.parseInt(values1[2].substring(0, values1[2].length() - 1).trim());

					return new Color(Display.getDefault(), new RGB(red1, green1, blue1));
				}
				break;
			default:
				break;
			}
		}
		return color;

	}	
	
	/**
	 * Configure color.
	 *
	 * @param property the property
	 */
	private void configureColor(Property property) {
		String cssText = property.getValue().getCssText();
		RGB oldRGB = null;
		if (cssText.startsWith("rgb")) {
			String rgbString = cssText.replace("rgb(", "").replace(")", "");
			String[] split = rgbString.split(",");

			int oldRed = Integer.parseInt(split[0].trim());
			int oldGreen = Integer.parseInt(split[1].trim());
			int oldBlue = Integer.parseInt(split[2].trim());

			oldRGB = new RGB(oldRed, oldGreen, oldBlue);
		}
		ColorDialog colorDialog = new ColorDialog(Display.getDefault().getActiveShell());

		if (cssText != null)
			colorDialog.setRGB(oldRGB);
		RGB rgb = colorDialog.open();

		if (rgb == null) {
			return;
		}
		int red = rgb.red;
		int green = rgb.green;
		int blue = rgb.blue;

		String rbgColor = "rgb(" + red + "," + green + "," + blue + ")";
		CSSValueImpl val = new CSSValueImpl();
		val.setValue(rbgColor);
		property.setValue(val);
	}

	/**
	 * Configure font.
	 *
	 * @param property the property
	 */
	private void configureFont(Property property) {
		String cssText = property.getValue().getCssText();
		java.util.regex.Pattern p = java.util.regex.Pattern.compile("([0-9])");
		String font = "";
		int fontSize = 0;
		String fontStyle = "";
		boolean fnt = true;
		boolean sty = false;

		String[] splitString = cssText.split(" ");
		for (String string : splitString) {
			Matcher m = p.matcher(string);
			boolean b = m.find();
			if (b) {
				String replace = string.replace("px", "");
				fontSize = Integer.parseInt(replace);
				fnt = false;
				sty = true;
			} else if (fnt) {
				font += string + " ";
			} else if (sty) {
				fontStyle = string;
			}
		}

		CustomFontDialog cusFD = new CustomFontDialog(Display.getDefault().getActiveShell(), SWT.NONE, font, fontSize,
				fontStyle);
		int open = cusFD.open();

		if (open == IDialogConstants.OK_ID) {
			FontData fontData = cusFD.getFontData();
			String fontName = fontData.getName();
			if (fontData.getHeight() > 10) {
				fontPixel = "10" + "px";
			} else {
				fontPixel = fontData.getHeight() + "px";
			}

			newfont = "\"" +fontName + "\"";
		}

		if (open == IDialogConstants.CANCEL_ID) {
			newfont = font;
		}

		CSSValueImpl val = new CSSValueImpl();
		val.setValue(newfont);
		property.setValue(val);
	}
	
	/**
	 * Dispose table editors.
	 */
	private void disposeTableEditors() {
		Collection<Label> values = labels.values();
		for (Label button : values) {
			button.dispose();
		}
		
		Collection<TreeEditor> editorList = editors.values();
		for (TreeEditor treeEditor : editorList) {
			treeEditor.getEditor().dispose();
			treeEditor.dispose();
		}
		
		editors.clear();
		labels.clear();
	}
	
}
