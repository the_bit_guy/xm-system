package com.magna.xmsystem.xmenu.ui.switchuser;

import java.util.List;

import org.eclipse.jface.viewers.ILazyTreeContentProvider;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.jface.viewers.Viewer;

/**
 * Class for Switch user lazy content provider.
 *
 * @author subash.janarthanan
 */
public class SwitchUserLazyContentProvider implements ILazyTreeContentProvider {

	/** Member variable 'tree viewer' for {@link TreeViewer}. */
	private TreeViewer treeViewer;

	/**
	 * Constructor for SwitchUserLazyContentProvider Class.
	 *
	 * @param treeViewer {@link TreeViewer}
	 */
	public SwitchUserLazyContentProvider(final TreeViewer treeViewer) {
		this.treeViewer = treeViewer;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.jface.viewers.IContentProvider#inputChanged(org.eclipse.jface
	 * .viewers.Viewer, java.lang.Object, java.lang.Object)
	 */
	@Override
	public void inputChanged(Viewer viewer, Object oldInput, Object newInput) {
		if (newInput instanceof List<?>) {
			ILazyTreeContentProvider.super.inputChanged(viewer, oldInput, ((List<?>) newInput).toArray());
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.jface.viewers.ILazyTreeContentProvider#updateElement(java.
	 * lang.Object, int)
	 */
	@Override
	public void updateElement(Object parent, int index) {

		Object element;
		if (parent instanceof List<?>) {
			int size = ((List<?>) parent).size();
			if (size > index) {
				element = ((List<?>) parent).get(index);
				this.treeViewer.replace(parent, index, element);
				updateChildCount(element, -1);
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.jface.viewers.ILazyTreeContentProvider#updateChildCount(java.
	 * lang.Object, int)
	 */
	@Override
	public void updateChildCount(Object element, int currentChildCount) {
		if (element instanceof List<?>) {
			this.treeViewer.setChildCount(element, ((List<?>) element).size());
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.jface.viewers.ILazyTreeContentProvider#getParent(java.lang.
	 * Object)
	 */
	@Override
	public Object getParent(Object element) {
		return null;
	}

}
