package com.magna.xmsystem.xmenu.ui.handlers;

import javax.inject.Inject;

import org.eclipse.core.commands.ParameterizedCommand;
import org.eclipse.e4.core.di.annotations.CanExecute;
import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.core.services.nls.Translation;
import org.eclipse.e4.ui.model.application.MApplication;
import org.eclipse.e4.ui.model.application.commands.MCommand;
import org.eclipse.e4.ui.model.application.ui.menu.MItem;

import com.magna.xmsystem.dependencies.utils.XMSystemUtil;
import com.magna.xmsystem.xmenu.message.Message;
import com.magna.xmsystem.xmenu.ui.model.BaseApplication;
import com.magna.xmsystem.xmenu.ui.model.ProjectApplication;
import com.magna.xmsystem.xmenu.ui.model.UserApplication;
import com.magna.xmsystem.xmenu.ui.parts.BaseAppExecution;
import com.magna.xmsystem.xmenu.ui.utils.XmMenuUtil;

/**
 * Class for Execute application handler.
 *
 * @author Chiranjeevi.Akula
 */
public class ExecuteApplicationHandler {

	/** Member variable 'messages' for {@link Message}. */
	@Inject
	@Translation
	transient protected Message messages;

	/**
	 * Method for Execute.
	 *
	 * @param commandParameters
	 *            {@link ParameterizedCommand}
	 * @param mApplication
	 *            {@link MApplication}
	 */
	@Execute
	public void execute(ParameterizedCommand commandParameters, MApplication mApplication, MItem mItem) {
		
		BaseApplication baseApplication = null;
		String commandId = commandParameters.getId();
		if (!XMSystemUtil.isEmpty(commandId)) {
			MCommand command = mApplication.getCommand(commandId);
			Object application = command.getTransientData().get("Data");
			boolean isSingleton = false;
			boolean isParent = true;
			if (application instanceof UserApplication) {
				isParent = ((UserApplication) application).isParent();
				isSingleton = ((UserApplication) application).isSingleton();
				baseApplication = ((UserApplication) application).getBaseApplication();
			} else if (application instanceof ProjectApplication) {
				isParent = ((ProjectApplication) application).isParent();
				isSingleton = ((ProjectApplication) application).isSingleton();
				baseApplication = ((ProjectApplication) application).getBaseApplication();
			}

			if(!isParent && isSingleton) {  //if(!isAdminMenuItem(mItem.getParent()) && isSingleton)
				XmMenuUtil.getInstance().applyApplicationTimeOut(mItem);
			}	
			
			String localApplicationName = XmMenuUtil.getInstance().getLocaleApplicationName(application);
			XmMenuUtil.getInstance().updateStatusBarAndMessageConsole(
					messages.launchApplication + " " + localApplicationName);
			
			XMSystemUtil.setTaskName(localApplicationName);

			BaseAppExecution.getInstance().excute(baseApplication, false);		
		}
	}

	@CanExecute
	public boolean execute(MItem mItem) {
		return mItem.isEnabled();
	}
	
	/**
	 * Checks if is admin menu item.
	 *
	 * @param mElementContainer {@link MElementContainer<MUIElement>}
	 * @return true, if is admin menu item
	 */
	/*private boolean isAdminMenuItem(MElementContainer<MUIElement> mElementContainer) {
		if(mElementContainer != null) {
			if(CommonConstants.MAINMENU.XMENU_ADMINSMENU_ID.equals(mElementContainer.getElementId())) {
				return true;
			} else {
				return isAdminMenuItem(mElementContainer.getParent());
			}
		}
		return false;
	}*/
}