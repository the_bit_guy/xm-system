package com.magna.xmsystem.xmenu.ui.messageconsloe;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.eclipse.e4.core.di.annotations.Optional;
import org.eclipse.e4.core.services.nls.Translation;
import org.eclipse.e4.ui.di.UIEventTopic;
import org.eclipse.e4.ui.di.UISynchronize;
import org.eclipse.jface.layout.GridDataFactory;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.StyledText;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.magna.xmsystem.xmenu.message.Message;
import com.magna.xmsystem.xmenu.ui.utils.CommonConstants;
import com.magna.xmsystem.xmenu.ui.utils.XmMenuUtil;

/**
 * The Class MessageConsoleUI.
 * 
 * @author shashwat.anand
 */
public class MessageConsoleUI extends Composite {

	/** Logger instance. */
	private static final Logger LOGGER = LoggerFactory.getLogger(MessageConsoleUI.class);

	/** {@link UISynchronize} instance. */
	@Inject
	private UISynchronize uiSynchronize;

	private StyledText styleText;

	/** Member variable for messages. */
	@Inject
	@Translation
	transient private Message messages;

	/**
	 * Constructor.
	 *
	 * @param parent
	 *            the parent
	 */
	@Inject
	public MessageConsoleUI(final Composite parent) {
		super(parent, SWT.NONE);
	}

	/**
	 * Creates the UI.
	 */
	@PostConstruct
	private void createUI() {
		final GridLayout gridLayout = new GridLayout(1, false);
		gridLayout.marginHeight = 0;
		gridLayout.marginBottom = 0;
		gridLayout.marginLeft = 0;
		gridLayout.marginRight = 0;
		gridLayout.marginTop = 0;
		gridLayout.horizontalSpacing = 0;
		gridLayout.verticalSpacing = 0;
		gridLayout.marginWidth = 0;
		this.setLayout(gridLayout);
		this.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		initUI();
	}

	/**
	 * Gets the event.
	 *
	 * @param message
	 *            the message
	 * @return the event
	 */
	@Inject
	@Optional
	public void getEvent(@UIEventTopic(CommonConstants.EVENT_BROKER.CONSOLE_MESSAGE) final String message) {
		updateInterface(message);
	}

	/**
	 * Update interface.
	 *
	 * @param message
	 *            the message
	 */
	private void updateInterface(final String message) {
		try {	
			this.uiSynchronize.syncExec(new Runnable() {
				@Override
				public void run() {
					try {
						if (!styleText.isDisposed()) {
							setLiveData(message);
						}
					} catch (Exception exc) {
						LOGGER.error("Exception occured while setting script live mesaage text", exc); //$NON-NLS-1$
					}
				}
			});
		} catch (Exception exception) {
			LOGGER.error("Exception occured while setting script live message text in update interface method", //$NON-NLS-1$
					exception);
		}
	}

	/**
	 * Method for Inits the UI.
	 */
	private void initUI() {
		this.styleText = new StyledText(this, SWT.WRAP | SWT.V_SCROLL | SWT.MULTI);
		GridDataFactory.fillDefaults().grab(true, true).align(SWT.FILL, SWT.FILL).applyTo(styleText);
		styleText.setEditable(false);
		styleText.setBackground(styleText.getDisplay().getSystemColor(SWT.COLOR_WHITE));
		setLiveData(XmMenuUtil.getInstance().getMsgConsoleStrBuf().toString());
	}

	/**
	 * Sets the live data.
	 *
	 * @param message
	 *            the new live data
	 */
	private void setLiveData(final String message) {
		try {
			this.styleText.append(message);
			this.styleText.setCaretOffset(this.styleText.getText().length());
			this.styleText.setTopIndex(this.styleText.getLineCount() - 1);
		} catch (Exception e) {
			LOGGER.error("Exception occured while setting message ! ", e);
		}
	}

	/**
	 * Sets the script exec live data.
	 *
	 * @param startAppScript
	 *            the new script exec live data
	 *//*
	public void setStartScriptExecLiveData(final String startAppScript) {
		try {
			final StringBuffer stb = new StringBuffer(this.styleText.getText());
			stb.append(this.dateTimeFormat.format(LocalDateTime.now())).append(": ")
					.append("Executing the Start Script ").append(startAppScript).append(" ....").append("\n");
			this.styleText.setText(stb.toString());
			this.styleText.setCaretOffset(this.styleText.getText().length());
		} catch (Exception e) {
			LOGGER.error("Exception occured while setting startAppScript message ! ", e);
		}
	}

	*//**
	 * Sets the user app script exec live data.
	 *
	 * @param userAppScript
	 *            the new user app script exec live data
	 *//*
	public void setUserAppScriptExecLiveData(final String userAppScript) {
		try {
			final StringBuffer stb = new StringBuffer(this.styleText.getText());
			stb.append(this.dateTimeFormat.format(LocalDateTime.now())).append(": ")
					.append("Executing the UserApplication Script ").append(userAppScript).append(" ....").append("\n");
			this.styleText.setText(stb.toString());
			this.styleText.setCaretOffset(this.styleText.getText().length());
		} catch (Exception e) {
			LOGGER.error("Exception occured while setting userAppScript message ! ", e);
		}
	}

	*//**
	 * Sets the project app script exec live data.
	 *
	 * @param projectAppScript
	 *            the new project app script exec live data
	 *//*
	public void setProjectAppScriptExecLiveData(final String projectAppScript) {
		try {
			StringBuffer stb = new StringBuffer(this.styleText.getText());
			stb.append(this.dateTimeFormat.format(LocalDateTime.now())).append(": ")
					.append("Executing the ProjectApplication Script ").append(projectAppScript).append(" ....")
					.append("\n");
			this.styleText.setText(stb.toString());
			this.styleText.setCaretOffset(this.styleText.getText().length());
		} catch (Exception e) {
			LOGGER.error("Exception occured while setting projectAppScript message ! ", e);
		}
	}*/

}
