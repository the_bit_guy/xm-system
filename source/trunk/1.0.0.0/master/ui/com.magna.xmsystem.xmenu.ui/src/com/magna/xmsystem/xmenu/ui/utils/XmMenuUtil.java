package com.magna.xmsystem.xmenu.ui.utils;

import java.io.File;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.inject.Named;

import org.eclipse.core.runtime.preferences.IEclipsePreferences;
import org.eclipse.e4.core.contexts.IEclipseContext;
import org.eclipse.e4.core.di.annotations.Optional;
import org.eclipse.e4.core.di.extensions.Preference;
import org.eclipse.e4.core.services.events.IEventBroker;
import org.eclipse.e4.core.services.nls.Translation;
import org.eclipse.e4.core.services.translation.TranslationService;
import org.eclipse.e4.ui.model.application.ui.menu.MItem;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.swt.widgets.Shell;
import org.osgi.service.prefs.BackingStoreException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.magna.xmsystem.dependencies.utils.APPLICATION;
import com.magna.xmsystem.dependencies.utils.LANG_ENUM;
import com.magna.xmsystem.dependencies.utils.OSValidator;
import com.magna.xmsystem.dependencies.utils.XMSystemUtil;
import com.magna.xmsystem.dependencies.utils.XmSystemEnvProcess;
import com.magna.xmsystem.ui.controls.widgets.CustomMessageDialog;
import com.magna.xmsystem.xmenu.message.Message;
import com.magna.xmsystem.xmenu.ui.model.Icon;
import com.magna.xmsystem.xmenu.ui.model.ProjectApplication;
import com.magna.xmsystem.xmenu.ui.model.StartApplication;
import com.magna.xmsystem.xmenu.ui.model.UserApplication;
import com.piterion.security.manager.windows.TicketManager;

@SuppressWarnings("restriction")
public class XmMenuUtil {
	
	/** The Constant LOGGER. */
	private static final Logger LOGGER = LoggerFactory.getLogger(XmMenuUtil.class);
	
	/** Member variable 'this ref' for {@link XmMenuUtil}. */
	private static XmMenuUtil thisRef;

	/** Member variable 'locale' for {@link Locale}. */
	@Inject
	private @Named(TranslationService.LOCALE) Locale locale;
	
	@Inject
	private IEclipseContext iEclipseContext ;

	/** The event broker. */
	@Inject
	private IEventBroker eventBroker;

	/** Member variable 'preferences' for {@link IEclipsePreferences}. */
	@Inject
	@Preference
	private IEclipsePreferences preferences;

	/** Member variable for messages. */
	@Inject
	@Translation
	transient protected Message messages;

	/** Member variable 'msg console str buf' for {@link StringBuffer}. */
	private StringBuffer msgConsoleStrBuf;

	/** Member variable 'date time format' for {@link DateTimeFormatter}. */
	private DateTimeFormatter dateTimeFormat;

	/** The process map. */
	private Map<Process, String> processMap;
	
	/** The main window shell. */
	private Shell mainWindowShell;
	
	/** The app window x coordinate. */
	public int appWindowXCoordinate;
	
	/** The app window y coordinate. */
	public int appWindowYCoordinate;
	
	/** The is switched user. */
	private boolean isSwitchedUser;
	
	/**
	 * Constructor for XMAdminUtil Class.
	 */
	public XmMenuUtil() {
		setInstance(this);
	}

	/**
	 * Gets the single instance of XMAdminUtil.
	 *
	 * @return single instance of XMAdminUtil
	 */
	public static XmMenuUtil getInstance() {
		return thisRef;
	}

	/**
	 * Sets the instance.
	 *
	 * @param thisRef
	 *            the new instance
	 */
	private void setInstance(final XmMenuUtil thisRef) {
		XmMenuUtil.thisRef = thisRef;
	}

	/**
	 * Method for Post construct.
	 */
	@PostConstruct
	private void postConstruct() {
		initMsgConsloeBuffer();
		processMap = new HashMap<Process, String>();
	}
	
	/**
	 * Method for Inits the msg consloe buffer.
	 */
	private void initMsgConsloeBuffer() {
		this.dateTimeFormat = DateTimeFormatter.ofLocalizedDateTime(FormatStyle.MEDIUM);
		msgConsoleStrBuf = new StringBuffer();
		msgConsoleStrBuf.append(this.dateTimeFormat.format(LocalDateTime.now())).append(": ").append(messages.menuStartedMessage).append("\n");
	}
	
	/**
	 * Sets the locale.
	 *
	 * @param locale
	 *            the new locale
	 */
	@Inject
	public void setLocale(@Optional @Named(TranslationService.LOCALE) Locale locale) {
		Locale localeP = locale;
		if (locale == null) {
			localeP = Locale.ENGLISH;
			LOGGER.error("changing the application language to english as Local is null");
		}
		final String language = this.locale.getLanguage();
		LANG_ENUM langEnum = LANG_ENUM.getLangEnum(language);
		if (langEnum == null) {
			LOGGER.error("changing the application language to english as system langauge is neither english nor german");
			langEnum = LANG_ENUM.ENGLISH;
		}
		this.locale = localeP;
		XMSystemUtil.setLanguage(langEnum.name().toLowerCase());
	}

	/**
	 * Sets the default locale.
	 *
	 * @param localeString the new default locale
	 */
	public void setDefaultLocale(String localeString) {
		if(!XMSystemUtil.isEmpty(localeString)) {
			Locale defaultLocale = LANG_ENUM.valueOf(localeString.toUpperCase()).getLocale();
			if(defaultLocale != null) {
				Locale.setDefault(defaultLocale);				
				iEclipseContext.set(TranslationService.LOCALE, defaultLocale);
				setLocale(defaultLocale);
			}			
		}
	}
	
	/**
	 * Gets the current locale enum.
	 *
	 * @return the current locale enum
	 */
	public LANG_ENUM getCurrentLocaleEnum() {
		return LANG_ENUM.getLangEnum(this.locale.getLanguage());
	}

	/**
	 * Gets the default locale enum.
	 *
	 * @return the default locale enum
	 */
	public LANG_ENUM getDefaultLocaleEnum() {
		return LANG_ENUM.ENGLISH;
	}

	/**
	 * Gets the XmMenu icon folder path
	 * 
	 * @return String
	 */
	@Deprecated
	public String getLocalIconDirectoryPath() {
		String xmSystemInstallationPath = XmSystemEnvProcess.getInstance().getenv("XM_PATH");
		if (!XMSystemUtil.isEmpty(xmSystemInstallationPath)) { //$NON-NLS-1$
			return xmSystemInstallationPath.trim() + File.separator + APPLICATION.XMMENU.getApplicationName() + File.separator
					+ "icons";
		} 
		
		return null;
	}
	
	/**
	 * Gets the local css directory path.
	 *
	 * @return the local css directory path
	 */
	public String getLocalCssDirectoryPath() {
		String xmSystemInstallationPath = XmSystemEnvProcess.getInstance().getenv("XM_PATH");
		if (!XMSystemUtil.isEmpty(xmSystemInstallationPath)) { //$NON-NLS-1$
			return xmSystemInstallationPath.trim() + File.separator + APPLICATION.XMMENU.getApplicationName() + File.separator
					+ "css";
		} 
		
		return null;
	}

	/**
	 * Get XmMenu local icon folder
	 * 
	 * @return {@link File}
	 */
	@Deprecated
	public File getLocalIconDirectory() {
		File file = new File(getLocalIconDirectoryPath());
		if (file.exists()) {
			return file;
		}
		
		return null;
	}

	/**
	 * Gets the image.
	 *
	 * @param icon the icon
	 * @param isActive the is active
	 * @return the image
	 */
	public Image getImage(final Icon icon, final boolean isActive) {
		if(icon != null) {
			File localIconDirectory = XMSystemUtil.getXMSystemServerIconsFolder();
			if (localIconDirectory != null) {
				final File iconFile = new File(localIconDirectory.getAbsolutePath() + File.separator + icon.getIconName());
				if (iconFile != null && iconFile.exists()) {
					String iconPath = iconFile.getAbsolutePath();
					return XMSystemUtil.getInstance().getImage(this.getClass(), iconPath, isActive, true);
				}
			}
		}	

		return null;
	}

	/**
	 * Gets the icon path.
	 *
	 * @param iconName
	 *            {@link String}
	 * @return the icon path
	 */
	public String getIconPath(final Icon icon) {
		if(icon != null) {
			File localIconDirectory = XMSystemUtil.getXMSystemServerIconsFolder();
			if (localIconDirectory != null) {
				final File iconFile = new File(localIconDirectory.getAbsolutePath() + File.separator + icon.getIconName());
				if (iconFile != null && iconFile.exists()) {
					String iconPath = iconFile.getAbsolutePath();
					return iconPath;
				}
			}
		}

		return null;
	}

	/**
	 * Update status bar and message console.
	 *
	 * @param message the message
	 */
	public void updateStatusBarAndMessageConsole(final String message) {
		updateStatusBar(message);
		updateMessageConsole(message);
	}
	
	/**
	 * Method for Update message console.
	 *
	 * @param message {@link String}
	 */
	public void updateMessageConsole(final String message) {
		String consoleMsg = this.dateTimeFormat.format(LocalDateTime.now()) + ": " + message + "\n";
		this.msgConsoleStrBuf.append(consoleMsg);
		this.eventBroker.send(CommonConstants.EVENT_BROKER.CONSOLE_MESSAGE, consoleMsg);
		LOGGER.info(consoleMsg);
	}
	
	/**
	 * Method for Update status bar.
	 *
	 * @param message {@link String}
	 */
	public void updateStatusBar(final String message) {
		this.eventBroker.send(CommonConstants.EVENT_BROKER.STATUSBAR_MESSAGE, message);
	}
	
	/**
	 * Gets the msg console str buf.
	 *
	 * @return the msg console str buf
	 */
	public StringBuffer getMsgConsoleStrBuf() {
		return msgConsoleStrBuf;
	}

	/**
	 * Gets the local application name.
	 *
	 * @param application
	 *            {@link Object}
	 * @return the local application name
	 */
	public String getLocaleApplicationName(final Object application) {
		XmMenuUtil instance = XmMenuUtil.getInstance();
		if (application != null) {
			if (application instanceof UserApplication) {
				UserApplication userApplication = (UserApplication) application;				
				String name = userApplication.getName(instance.getCurrentLocaleEnum());
				if (!XMSystemUtil.isEmpty(name)) {
					return name;
				}
				return userApplication.getName();		
			} else if (application instanceof ProjectApplication) {
				ProjectApplication projectApplication = (ProjectApplication) application;
				String name = projectApplication.getName(instance.getCurrentLocaleEnum());
				if (!XMSystemUtil.isEmpty(name)) {
					return name;
				}
				return projectApplication.getName();	
			} else if (application instanceof StartApplication) {
				StartApplication startApplication = (StartApplication) application;
				String name = startApplication.getName();
				if (!XMSystemUtil.isEmpty(name)) {
					return name;
				}
			}
		}
		return CommonConstants.EMPTY_STR;
	}

	/**
	 * Gets the local application description.
	 *
	 * @param application
	 *            {@link Object}
	 * @return the local application description
	 */
	public String getLocaleApplicationDescription(final Object application) {
		XmMenuUtil instance = XmMenuUtil.getInstance();
		if (application != null) {
			if (application instanceof UserApplication) {
				UserApplication userApplication = (UserApplication) application;				
				String description = userApplication.getDescription(instance.getCurrentLocaleEnum());
				if (!XMSystemUtil.isEmpty(description)) {
					return description;
				}
				return userApplication.getDescription();	
			} else if (application instanceof ProjectApplication) {
				ProjectApplication projectApplication = (ProjectApplication) application;
				String description = projectApplication.getDescription(instance.getCurrentLocaleEnum());
				if (!XMSystemUtil.isEmpty(description)) {
					return description;
				}
				return projectApplication.getDescription();
			} else if (application instanceof StartApplication) {
				StartApplication startApplication = (StartApplication) application;
				String description = startApplication.getDescription(instance.getCurrentLocaleEnum());
				description = (description == null ? startApplication.getDescription(instance.getDefaultLocaleEnum())
						: description);
				if (description != null) {
					return description;
				}
			}
		}
		return CommonConstants.EMPTY_STR;
	}

	/**
	 * Gets the locale application remarks.
	 *
	 * @param application {@link Object}
	 * @return the locale application remarks
	 */
	public String getLocaleApplicationRemarks(final Object application) {
		XmMenuUtil instance = XmMenuUtil.getInstance();
		if (application != null) {
			if (application instanceof UserApplication) {
				UserApplication userApplication = (UserApplication) application;
				String remarks = userApplication.getRemarks(instance.getCurrentLocaleEnum());
				remarks = (remarks == null ? userApplication.getRemarks(instance.getDefaultLocaleEnum()) : remarks);
				if (remarks != null) {
					return remarks;
				}
			} else if (application instanceof ProjectApplication) {
				ProjectApplication projectApplication = (ProjectApplication) application;
				String remarks = projectApplication.getRemarks(instance.getCurrentLocaleEnum());
				remarks = (remarks == null ? projectApplication.getRemarks(instance.getDefaultLocaleEnum()) : remarks);
				if (remarks != null) {
					return remarks;
				}
			} else if (application instanceof StartApplication) {
				StartApplication startApplication = (StartApplication) application;
				String remarks = startApplication.getRemarks(instance.getCurrentLocaleEnum());
				remarks = (remarks == null ? startApplication.getRemarks(instance.getDefaultLocaleEnum()) : remarks);
				if (remarks != null) {
					return remarks;
				}
			}
		}
		return CommonConstants.EMPTY_STR;
	}
	
	/**
	 * Method for Sets the IN eclipse prefs.
	 *
	 * @param key {@link String}
	 * @param object {@link Object}
	 */
	public void setINEclipsePrefs(final String key, final Object object) {
		try {
			Gson gson = new GsonBuilder().create();
			String jsonString = gson.toJson(object);

			preferences.put(key, jsonString);
			preferences.flush();
		} catch (BackingStoreException e) {
			LOGGER.error("Error while setting eclipse preferences for object", e); //$NON-NLS-1$
		}
	}

	/**
	 * Gets the eclipse prefs.
	 *
	 * @param key {@link String}
	 * @return the eclipse prefs
	 */
	public String getEclipsePrefs(final String key) {
		try {
			return preferences.get(key, CommonConstants.EMPTY_STR);
		} catch (Exception e) {
			LOGGER.error("Error while getting eclipse preferences from key", e); //$NON-NLS-1$
		}
		
		return null;
	}
	
	/**
	 * Method for Display offline message.
	 */
	public void displayOfflineMessage() {
		
		Display.getDefault().asyncExec(new Runnable() {
			@Override
			public void run() {
				CustomMessageDialog.openError(Display.getCurrent().getActiveShell(),
						messages.offlineModeTitle, messages.offlineModeMessage);
			} 			
		});		
	}
	
	/**
	 * Method for Sort user apps and its child.
	 *
	 * @param userApplications {@link List<UserApplication>}
	 */
	public void sortUserAppsAndItsChild(final List<UserApplication> userApplications) {
		XmMenuUtil instance = XmMenuUtil.getInstance();
		if (userApplications != null && !userApplications.isEmpty()) {
			userApplications.sort((userApplication1, userApplication2) -> {
				String userAppName1 = userApplication1.getName(instance.getCurrentLocaleEnum());
				if (XMSystemUtil.isEmpty(userAppName1)) {
					userAppName1 = userApplication1.getName();
				}
				String userAppName2 = userApplication2.getName(instance.getCurrentLocaleEnum());
				if (XMSystemUtil.isEmpty(userAppName2)) {
					userAppName2 = userApplication2.getName();
				}
				return userAppName1.compareTo(userAppName2);
			});

			for (UserApplication userApplication : userApplications) {
				List<UserApplication> childUserApplications = userApplication.getChildUserApplications();
				if (childUserApplications != null && !childUserApplications.isEmpty()) {
					childUserApplications.sort((userApplication1, userApplication2) -> {
						String userAppName1 = userApplication1.getName(instance.getCurrentLocaleEnum());
						if (XMSystemUtil.isEmpty(userAppName1)) {
							userAppName1 = userApplication1.getName();
						}
						String userAppName2 = userApplication2.getName(instance.getCurrentLocaleEnum());
						if (XMSystemUtil.isEmpty(userAppName2)) {
							userAppName2 = userApplication2.getName();
						}
						return userAppName1.compareTo(userAppName2);
					});
				}
			}
		}
	}

	/**
	 * Method for Sort project apps and its child.
	 *
	 * @param projectApplications {@link List<ProjectApplication>}
	 */
	public void sortProjectAppsAndItsChild(final List<ProjectApplication> projectApplications) {
		XmMenuUtil instance = XmMenuUtil.getInstance();
		if (projectApplications != null && !projectApplications.isEmpty()) {
			projectApplications.sort((projectApplication1, projectApplication2) -> {
				String projectAppName1 = projectApplication1.getName(instance.getCurrentLocaleEnum());
				if (XMSystemUtil.isEmpty(projectAppName1)) {
					projectAppName1 = projectApplication1.getName();
				}
				String projectAppName2 = projectApplication2.getName(instance.getCurrentLocaleEnum());
				if (XMSystemUtil.isEmpty(projectAppName2)) {
					projectAppName2 = projectApplication2.getName();
				}
				return projectAppName1.compareTo(projectAppName2);
			});

			for (ProjectApplication projectApplication : projectApplications) {
				List<ProjectApplication> childProjectApplications = projectApplication.getChildProjectApplications();
				if (childProjectApplications != null && !childProjectApplications.isEmpty()) {
					childProjectApplications.sort((projectApplication1, projectApplication2) -> {
						String projectAppName1 = projectApplication1.getName(instance.getCurrentLocaleEnum());
						if (XMSystemUtil.isEmpty(projectAppName1)) {
							projectAppName1 = projectApplication1.getName();
						}
						String projectAppName2 = projectApplication2.getName(instance.getCurrentLocaleEnum());
						if (XMSystemUtil.isEmpty(projectAppName2)) {
							projectAppName2 = projectApplication2.getName();
						}
						return projectAppName1.compareTo(projectAppName2);
					});
				}
			}
		}
	}
	/*
	public void updateUserHistoryStatus(){
		LoadDataFromService.getInstance().updateUserStatusToHistory();
	}*/
	
	/**
	 * Method for Apply application time out.
	 *
	 * @param object {@link Object}
	 */
	public void applyApplicationTimeOut(Object object) {
		Long singletonTimeInterval = CaxMenuObjectModelUtil.getInstance().getSingletonAppTimeout();
		if (singletonTimeInterval != null) {
			final int intValue = singletonTimeInterval.intValue();
			if (intValue > 0) {
				if (object instanceof Composite) {
					if (!((Composite) object).isDisposed()) {
						((Composite) object).setEnabled(false);
					}
				} else if (object instanceof MenuItem) {
					if (!((MenuItem) object).isDisposed()) {
						((MenuItem) object).setEnabled(false);
					}
				} else if (object instanceof MItem) {
					if (((MItem) object).isVisible()) {
						((MItem) object).setEnabled(false);
					}
				}

				Display.getDefault().timerExec(intValue * 1000, new Runnable() {
					@Override
					public void run() {
						if (object instanceof Composite) {
							if (!((Composite) object).isDisposed()) {
								((Composite) object).setEnabled(true);
							}
						} else if (object instanceof MenuItem) {
							if (!((MenuItem) object).isDisposed()) {
								((MenuItem) object).setEnabled(true);
							}
						} else if (object instanceof MItem) {
							if (((MItem) object).isVisible()) {
								((MItem) object).setEnabled(true);
							}
						}
					}
				});
			}
		}
	}
	
	/**
	 * Method for Adds the running process.
	 *
	 * @param process {@link Process}
	 * @param baseAppName {@link String}
	 */
	@Deprecated
	public void addRunningProcess(final Process process, final String baseAppName) {
		processMap.put(process, baseAppName);
	}
	
	/**
	 * Gets the running application.
	 *
	 * @return the running application
	 */
	@Deprecated
	public String getRunningApplication() {
		String baseAppName = null;
		boolean isAlive = false;
		
		for (Map.Entry<Process, String> entry : processMap.entrySet())
		{
		    isAlive = entry.getKey().isAlive();
		    if(isAlive) {
		    	baseAppName = entry.getValue();
		    	break;
		    }
		}		
		if(!isAlive && !processMap.isEmpty()) {
			processMap.clear();
		}
		
		return baseAppName;
	}

	/**
	 * Sets the main window shell.
	 *
	 * @param mainWindowShell the new main window shell
	 */
	public void setMainWindowShell(Shell mainWindowShell) {
		this.mainWindowShell = mainWindowShell;
	}
	
	/**
	 * Gets the main window dimensions map.
	 *
	 * @return the main window dimensions map
	 */
	public Map<String, String> getMainWindowDimensionsMap() {
		final Map<String, String> windowDimensionsMap = new HashMap<>();
		
		Display.getDefault().syncExec(new Runnable() {

			@Override
			public void run() {
				if(mainWindowShell != null && !mainWindowShell.isDisposed()) {
					Rectangle bounds = mainWindowShell.getBounds();
					if(bounds != null) {
						windowDimensionsMap.put(CommonConstants.USER_SETTINGS.WINDOW_X, String.valueOf(bounds.x));
						windowDimensionsMap.put(CommonConstants.USER_SETTINGS.WINDOW_Y, String.valueOf(bounds.y));
						windowDimensionsMap.put(CommonConstants.USER_SETTINGS.WINDOW_W, String.valueOf(bounds.width));
						windowDimensionsMap.put(CommonConstants.USER_SETTINGS.WINDOW_H, String.valueOf(bounds.height));
					}
				}				
			}
		});	
		
		return windowDimensionsMap;
	}
	
	/**
	 * Gets the user ticket.
	 *
	 * @return the user ticket
	 */
	public String getUserTicket() {
		long startTime = System.currentTimeMillis();
		String encryptTicket = null;
		if (OSValidator.isWindows()) {
			try {
				TicketManager ticketManager = new TicketManager();
				encryptTicket = ticketManager.generateTicket();
			} catch (Exception e) {
				LOGGER.error("Execption ocuured while launching the application: ", e); //$NON-NLS-1$
			}

		} else if (OSValidator.isUnixOrLinux()) {
			try {
				com.piterion.security.manager.linux.TicketManager ticketManager = new com.piterion.security.manager.linux.TicketManager();
				encryptTicket = ticketManager.generateTicket();
			} catch (Exception e) {
				LOGGER.error("Execption ocuured while launching the application: ", e); //$NON-NLS-1$
			}
		}
		long endTime = System.currentTimeMillis();
		LOGGER.info("Time taken to generate the ticket at caxstartmenu : " + (endTime - startTime));
		return encryptTicket;
	}

	/**
	 * Checks if is switched user.
	 *
	 * @return true, if is switched user
	 */
	public boolean isSwitchedUser() {
		return isSwitchedUser;
	}

	/**
	 * Sets the switched user.
	 *
	 * @param isSwitchedUser the new switched user
	 */
	public void setSwitchedUser(boolean isSwitchedUser) {
		this.isSwitchedUser = isSwitchedUser;
	}

	/**
	 * Gets the app window X coordinate.
	 *
	 * @return the app window X coordinate
	 */
	public int getAppWindowXCoordinate() {
		return appWindowXCoordinate;
	}

	/**
	 * Sets the app window X coordinate.
	 *
	 * @param appWindowXCoordinate the new app window X coordinate
	 */
	public void setAppWindowXCoordinate(final int appWindowXCoordinate) {
		this.appWindowXCoordinate = appWindowXCoordinate;
	}

	/**
	 * Gets the app window Y coordinate.
	 *
	 * @return the app window Y coordinate
	 */
	public int getAppWindowYCoordinate() {
		return appWindowYCoordinate;
	}

	/**
	 * Sets the app window Y coordinate.
	 *
	 * @param appWindowYCoordinate the new app window Y coordinate
	 */
	public void setAppWindowYCoordinate(final int appWindowYCoordinate) {
		this.appWindowYCoordinate = appWindowYCoordinate;
	}
	
	/**
	 * Force active.
	 * @deprecated : Don't use this method until it is necessary
	 * as its uses SWT internal OS code
	 *
	 * @param shell the shell
	 */
	/*public static void forceActive(final Shell shell) {
		int hFrom = (int) OS.GetForegroundWindow();

		if (hFrom <= 0) {
			OS.SetForegroundWindow(shell.handle);
			return;
		}

		if (shell.handle == hFrom) {
			return;
		}

		int pid = OS.GetWindowThreadProcessId(hFrom, null);
		int _threadid = OS.GetWindowThreadProcessId(shell.handle, null);

		if (_threadid == pid) {
			OS.SetForegroundWindow(shell.handle);
			return;
		}

		if (pid > 0) {
			if (!OS.AttachThreadInput(_threadid, pid, true)) {
				return;
			}
			OS.SetForegroundWindow(shell.handle);
			OS.AttachThreadInput(_threadid, pid, false);
		}

		OS.BringWindowToTop(shell.handle);
		OS.UpdateWindow(shell.handle);
		OS.SetActiveWindow(shell.handle);
	}*/
}
