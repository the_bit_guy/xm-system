package com.magna.xmsystem.xmenu.ui.model;

import java.beans.PropertyChangeEvent;
import java.util.HashMap;
import java.util.Map;

import com.magna.xmsystem.dependencies.utils.LANG_ENUM;

/**
 * Class for Administration area.
 *
 * @author Chiranjeevi.Akula
 */
public class AdministrationArea extends BeanModel implements ICaxStartMenu{

	/** The Constant PROPERTY_ADMINAREAID. */
	public static final String PROPERTY_ADMINAREAID = "adminAreaId"; //$NON-NLS-1$

	/** The Constant PROPERTY_ADMINAREANAME. */
	public static final String PROPERTY_ADMINAREANAME = "name"; //$NON-NLS-1$

	/** The Constant PROPERTY_HOTLINE_CONTACT_NUMBER. */
	public static final String PROPERTY_HOTLINE_CONTACT_NUMBER = "hotlineContactNumber"; //$NON-NLS-1$

	/** The Constant PROPERTY_HOTLINE_CONTACT_EMAIL. */
	public static final String PROPERTY_HOTLINE_CONTACT_EMAIL = "hotlineContactEmail"; //$NON-NLS-1$

	/** The Constant PROPERTY_SINGLETON_APP_TIMEOUT. */
	public static final String PROPERTY_SINGLETON_APP_TIMEOUT = "singletonAppTimeout"; //$NON-NLS-1$

	/** The Constant PROPERTY_ACTIVE. */
	public static final String PROPERTY_ACTIVE = "active"; //$NON-NLS-1$

	/** The Constant PROPERTY_DESC_MAP. */
	public static final String PROPERTY_DESC_MAP = "descriptionMap"; //$NON-NLS-1$

	/** The Constant PROPERTY_REMARKS_MAP. */
	public static final String PROPERTY_REMARKS_MAP = "remarksMap"; //$NON-NLS-1$

	/** The Constant PROPERTY_ICON. */
	public static final String PROPERTY_ICON = "icon"; //$NON-NLS-1$

	/** Member variable 'admin area id' for {@link String}. */
	private String adminAreaId;

	/** Member variable 'name' for {@link String}. */
	private String name;

	/** Member variable 'active' for {@link Boolean}. */
	private boolean active;

	/** Member variable 'description map' for {@link Map<LANG_ENUM,String>}. */
	private Map<LANG_ENUM, String> descriptionMap;

	/** Member variable 'hotline contact number' for {@link String}. */
	private String hotlineContactNumber;

	/** Member variable 'hotline contact email' for {@link String}. */
	private String hotlineContactEmail;

	/** Member variable 'singleton app timeout' for {@link Long}. */
	private Long singletonAppTimeout;

	/** Member variable 'remarks map' for {@link Map<LANG_ENUM,String>}. */
	private Map<LANG_ENUM, String> remarksMap;

	/** Member variable 'icon' for {@link Icon}. */
	private Icon icon;

	/** Member variable 'relation id' for {@link String}. */
	private String relationId;

	/**
	 * Constructor for AdministrationArea Class.
	 *
	 * @param AdminAreaId
	 *            {@link String}
	 * @param name
	 *            {@link String}
	 * @param isActive
	 *            {@link boolean}
	 * @param icon
	 *            {@link Icon}
	 * @param hotlineContactNumber
	 *            {@link String}
	 * @param hotlineContactEmail
	 *            {@link String}
	 * @param singletonAppTimeout
	 *            {@link Long}
	 */
	public AdministrationArea(final String AdminAreaId, final String name, final boolean isActive, final Icon icon,
			final String hotlineContactNumber, final String hotlineContactEmail, final Long singletonAppTimeout) {
		this(AdminAreaId, name, isActive, new HashMap<>(), hotlineContactNumber, hotlineContactEmail,
				singletonAppTimeout, new HashMap<>(), icon);

	}

	/**
	 * Constructor for AdministrationArea Class.
	 *
	 * @param adminAreaId
	 *            {@link String}
	 * @param name
	 *            {@link String}
	 * @param isActive
	 *            {@link boolean}
	 * @param descriptionMap
	 *            {@link Map<LANG_ENUM,String>}
	 * @param hotlineContactNumber
	 *            {@link String}
	 * @param hotlineContactEmail
	 *            {@link String}
	 * @param singletonAppTimeout
	 *            {@link Long}
	 * @param remarksMap
	 *            {@link Map<LANG_ENUM,String>}
	 * @param icon
	 *            {@link Icon}
	 */
	public AdministrationArea(final String adminAreaId, final String name, final boolean isActive,
			final Map<LANG_ENUM, String> descriptionMap, final String hotlineContactNumber,
			final String hotlineContactEmail, final Long singletonAppTimeout, final Map<LANG_ENUM, String> remarksMap,
			final Icon icon) {
		super();
		this.adminAreaId = adminAreaId;
		this.name = name;
		this.active = isActive;
		this.descriptionMap = descriptionMap;
		this.hotlineContactNumber = hotlineContactNumber;
		this.hotlineContactEmail = hotlineContactEmail;
		this.singletonAppTimeout = singletonAppTimeout;
		this.remarksMap = remarksMap;
		this.icon = icon;
	}

	/**
	 * Gets the administration area id.
	 *
	 * @return the administration area id
	 */
	public String getAdministrationAreaId() {
		return adminAreaId;
	}

	/**
	 * Sets the administration area id.
	 *
	 * @param adminAreaID
	 *            the new administration area id
	 */
	public void setAdministrationAreaId(final String adminAreaID) {
		this.propertyChangeSupport.firePropertyChange(PROPERTY_ADMINAREAID, this.adminAreaId,
				this.adminAreaId = adminAreaID);
	}

	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Sets the name.
	 *
	 * @param name
	 *            the new name
	 */
	public void setName(final String name) {
		if (name == null) {
			throw new IllegalArgumentException();
		}
		this.propertyChangeSupport.firePropertyChange(PROPERTY_ADMINAREANAME, this.name, this.name = name.trim());
	}

	/**
	 * Checks if is active.
	 *
	 * @return true, if is active
	 */
	public boolean isActive() {
		return active;
	}

	/**
	 * Sets the active.
	 *
	 * @param isActive
	 *            the new active
	 */
	public void setActive(final boolean isActive) {
		this.propertyChangeSupport.firePropertyChange(PROPERTY_ACTIVE, this.active, this.active = isActive);
	}

	/**
	 * Gets the hotline contact number.
	 *
	 * @return the hotline contact number
	 */
	public String getHotlineContactNumber() {
		return hotlineContactNumber;
	}

	/**
	 * Sets the hotline contact number.
	 *
	 * @param hotlineContactNumber
	 *            the new hotline contact number
	 */
	public void setHotlineContactNumber(String hotlineContactNumber) {
		if (hotlineContactNumber == null) {
			throw new IllegalArgumentException();
		}
		this.propertyChangeSupport.firePropertyChange(PROPERTY_HOTLINE_CONTACT_NUMBER, this.hotlineContactNumber,
				this.hotlineContactNumber = hotlineContactNumber.trim());
	}

	/**
	 * Gets the hotline contact email.
	 *
	 * @return the hotline contact email
	 */
	public String getHotlineContactEmail() {
		return hotlineContactEmail;
	}

	/**
	 * Sets the hotline contact email.
	 *
	 * @param hotlineContactEmail
	 *            the new hotline contact email
	 */
	public void setHotlineContactEmail(String hotlineContactEmail) {
		if (hotlineContactEmail == null) {
			throw new IllegalArgumentException();
		}
		this.propertyChangeSupport.firePropertyChange(PROPERTY_HOTLINE_CONTACT_EMAIL, this.hotlineContactEmail,
				this.hotlineContactEmail = hotlineContactEmail.trim());
	}

	/**
	 * Gets the singleton app timeout.
	 *
	 * @return the singleton app timeout
	 */
	public Long getSingletonAppTimeout() {
		return singletonAppTimeout;
	}

	/**
	 * Sets the singleton app timeout.
	 *
	 * @param singletonAppTimeout
	 *            the new singleton app timeout
	 */
	public void setSingletonAppTimeout(Long singletonAppTimeout) {
		if (singletonAppTimeout == null) {
			throw new IllegalArgumentException();
		}
		this.propertyChangeSupport.firePropertyChange(PROPERTY_SINGLETON_APP_TIMEOUT, this.singletonAppTimeout,
				this.singletonAppTimeout = singletonAppTimeout);
	}

	/**
	 * Gets the description map.
	 *
	 * @return the description map
	 */
	public Map<LANG_ENUM, String> getDescriptionMap() {
		return descriptionMap;
	}

	/**
	 * Method for Sets the description map.
	 *
	 * @param descriptionMap
	 *            {@link Map<LANG_ENUM,String>}
	 */
	public void setDescriptionMap(final Map<LANG_ENUM, String> descriptionMap) {
		if (descriptionMap == null) {
			throw new IllegalArgumentException();
		}
		this.propertyChangeSupport.firePropertyChange(PROPERTY_DESC_MAP, this.descriptionMap,
				this.descriptionMap = descriptionMap);
	}

	/**
	 * Gets the description.
	 *
	 * @param lang
	 *            {@link LANG_ENUM}
	 * @return the description
	 */
	public String getDescription(final LANG_ENUM lang) {
		return this.descriptionMap.get(lang);
	}

	/**
	 * Method for Sets the description.
	 *
	 * @param lang
	 *            {@link LANG_ENUM}
	 * @param description
	 *            {@link String}
	 */
	public void setDescription(final LANG_ENUM lang, final String description) {
		if (lang == null || description == null) {
			throw new IllegalArgumentException();
		}
		this.propertyChangeSupport.firePropertyChange(PROPERTY_DESC_MAP, this.descriptionMap,
				this.descriptionMap.put(lang, description.trim()));
	}

	/**
	 * Gets the remarks map.
	 *
	 * @return the remarks map
	 */
	public Map<LANG_ENUM, String> getRemarksMap() {
		return remarksMap;
	}

	/**
	 * Method for Sets the remarks map.
	 *
	 * @param remarksMap
	 *            {@link Map<LANG_ENUM,String>}
	 */
	public void setRemarksMap(final Map<LANG_ENUM, String> remarksMap) {
		if (remarksMap == null) {
			throw new IllegalArgumentException();
		}
		this.propertyChangeSupport.firePropertyChange(PROPERTY_REMARKS_MAP, this.remarksMap,
				this.remarksMap = remarksMap);
	}

	/**
	 * Gets the remarks.
	 *
	 * @param lang
	 *            {@link LANG_ENUM}
	 * @return the remarks
	 */
	public String getRemarks(final LANG_ENUM lang) {
		return this.remarksMap.get(lang);
	}

	/**
	 * Method for Sets the remarks.
	 *
	 * @param lang
	 *            {@link LANG_ENUM}
	 * @param remarks
	 *            {@link String}
	 */
	public void setRemarks(final LANG_ENUM lang, final String remarks) {
		if (lang == null || remarks == null) {
			throw new IllegalArgumentException();
		}
		this.propertyChangeSupport.firePropertyChange(PROPERTY_REMARKS_MAP, this.remarksMap,
				this.remarksMap.put(lang, remarks));
	}

	/**
	 * Gets the icon.
	 *
	 * @return the icon
	 */
	public Icon getIcon() {
		return icon;
	}

	/**
	 * Sets the icon.
	 *
	 * @param icon
	 *            the new icon
	 */
	public void setIcon(final Icon icon) {
		if (icon == null) {
			throw new IllegalArgumentException();
		}
		this.propertyChangeSupport.firePropertyChange(PROPERTY_ICON, this.icon, this.icon = icon);
	}

	/**
	 * Gets the relation id.
	 *
	 * @return the relation id
	 */
	public String getRelationId() {
		return relationId;
	}

	/**
	 * Sets the relation id.
	 *
	 * @param relationId
	 *            the new relation id
	 */
	public void setRelationId(String relationId) {
		this.relationId = relationId;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.BeanModel#
	 * propertyChange(java.beans.PropertyChangeEvent)
	 */
	@Override
	public void propertyChange(final PropertyChangeEvent event) {
		this.propertyChangeSupport.firePropertyChange(event.getPropertyName(), event.getOldValue(),
				event.getNewValue());
	}
}
