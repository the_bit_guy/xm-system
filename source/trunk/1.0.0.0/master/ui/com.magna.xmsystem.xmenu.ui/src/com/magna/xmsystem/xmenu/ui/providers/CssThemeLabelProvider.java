package com.magna.xmsystem.xmenu.ui.providers;

import java.io.File;

import org.eclipse.e4.ui.css.swt.theme.ITheme;
import org.eclipse.jface.viewers.LabelProvider;

/**
 * The Class CssThemeLabelProvider.
 */
@SuppressWarnings("restriction")
public class CssThemeLabelProvider extends LabelProvider {

	/* (non-Javadoc)
	 * @see org.eclipse.jface.viewers.LabelProvider#getText(java.lang.Object)
	 */
	@Override
	public String getText(Object element) {
		if (element instanceof ITheme) {
			ITheme iTheme = (ITheme) element;
			return iTheme.getLabel();
		} else if (element instanceof File) {
			File file = (File) element;
			String name = file.getName();
			String s1 = name.replace(".css", "");
			String s2 = s1.toUpperCase();
			return s2;
		}
		return super.getText(element);
	}
}

