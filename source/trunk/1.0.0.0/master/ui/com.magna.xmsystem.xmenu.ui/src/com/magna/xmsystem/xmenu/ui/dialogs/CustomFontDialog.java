package com.magna.xmsystem.xmenu.ui.dialogs;

import java.util.ArrayList;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.layout.GridDataFactory;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.ListViewer;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.FontData;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.List;
import org.eclipse.swt.widgets.Shell;

import com.magna.xmsystem.xmenu.ui.providers.FontNameLableProvider;

/**
 * The Class CustomFontDialog.
 */
public class CustomFontDialog extends Dialog {

	/** The list viewer. */
	private ListViewer listViewer;

	/** The txt aabbccyyzz. */
	private Label txtAabbccyyzz;

	/** The ft name. */
	private String ftName = null;

	/** The size. */
	private int fSize = 10;

	/** The dfont. */
	private String dfont;

	/** The dfont size. */
	@SuppressWarnings("unused")
	private int dfontSize;

	/** The dfont style. */
	@SuppressWarnings("unused")
	private String dfontStyle;

	/** The fnt data. */
	private FontData fntData;

	/** The lbl font. */
	private Label lblFont;

	/** Member variable 'parent shell' for {@link Shell}. */
	private Shell parentShell;

	/**
	 * Instantiates a new custom font dialog.
	 *
	 * @param parentShell
	 *            the parent shell
	 * @param Style
	 *            the style
	 * @param dfont
	 *            the dfont
	 * @param dfontSize
	 *            the dfont size
	 * @param dfontStyle
	 *            the dfont style
	 */
	public CustomFontDialog(Shell parentShell, int Style, String dfont, int dfontSize, String dfontStyle) {
		super(parentShell);
		this.parentShell = parentShell;
		this.dfont = dfont;
		this.dfontSize = dfontSize;
		this.dfontStyle = dfontStyle;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.jface.window.Window#configureShell(org.eclipse.swt.widgets.
	 * Shell)
	 */
	@Override
	protected void configureShell(Shell shell) {
		super.configureShell(shell);
		shell.setText("Select Font");
		shell.setParent(this.parentShell);
		//shell.setSize(300, 350);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.jface.dialogs.Dialog#createDialogArea(org.eclipse.swt.widgets
	 * .Composite)
	 */
	@Override
	protected Control createDialogArea(final Composite parent) {
		parent.setData("org.eclipse.e4.ui.css.id", "MyCSSTagForComposite");
		Composite composite = new Composite(parent, SWT.NONE);
		composite.setLayout(new GridLayout(1, false));
		GridDataFactory.fillDefaults().grab(true, true).align(SWT.FILL, SWT.FILL).hint(400, 300).applyTo(composite);
		composite.setData("org.eclipse.e4.ui.css.id", "MyCSSTagForComposite");

		lblFont = new Label(composite, SWT.NONE);
		lblFont.setText("Font");
		lblFont.setData("org.eclipse.e4.ui.css.id", "MyCSSTagForlblTheme");

		listViewer = new ListViewer(composite, SWT.BORDER | SWT.V_SCROLL);
		List list = listViewer.getList();
		list.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));

		txtAabbccyyzz = new Label(composite, SWT.BORDER | SWT.CENTER);
		txtAabbccyyzz.setText("aAbBcCyYzZ");
		GridData gd_txtAabbccyyzz = new GridData(SWT.FILL, SWT.FILL, true, false, 3, 1);
		gd_txtAabbccyyzz.heightHint = 25;
		txtAabbccyyzz.setLayoutData(gd_txtAabbccyyzz);
		txtAabbccyyzz.setData("org.eclipse.e4.ui.css.id", "MyCSSTagForlblTheme");

		// Seting the font name/ style on selection change
		ISelectionChangedListener listner = new ISelectionChangedListener() {

			@Override
			public void selectionChanged(SelectionChangedEvent event) {
				IStructuredSelection selection = (IStructuredSelection) listViewer.getSelection();
				ftName = (!selection.isEmpty()) ? selection.getFirstElement().toString() : "Arial";
				fSize = 10;
				txtAabbccyyzz.setFont(new org.eclipse.swt.graphics.Font(Display.getCurrent(), ftName, fSize, SWT.BOLD));
			}
		};

		listViewer.addSelectionChangedListener(listner);
		ArrayList<String> fontName = new ArrayList<>();
		
		//String fonts[] = GraphicsEnvironment.getLocalGraphicsEnvironment().getAvailableFontFamilyNames();
		String fonts[] = {"Arial", "Calibri", "Cambria", "Chiller", "Elephant", "Forte", "Gigi", "Harrington", "Lucida Calligraphy", "Perpetua", "Pristina", "Script MT Bold", "Segoe Script", "Times New Roman"};
		for (int i = 0; i < fonts.length; i++) {
			fontName.add(fonts[i]);
		}
		
		listViewer.setContentProvider(new ArrayContentProvider());
		listViewer.setLabelProvider(new FontNameLableProvider());
		listViewer.setInput(fontName);

		final ISelection nmSelection = new StructuredSelection(this.dfont.trim());
		listViewer.setSelection(nmSelection);

		return composite;
	}

	@Override
	protected void createButtonsForButtonBar(Composite parent) {
		parent.setData("org.eclipse.e4.ui.css.id", "MyCSSTagForComposite");
		createButton(parent, IDialogConstants.OK_ID, IDialogConstants.OK_LABEL, true);
		createButton(parent, IDialogConstants.CANCEL_ID, IDialogConstants.CANCEL_LABEL, false);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jface.dialogs.Dialog#okPressed()
	 */
	@Override
	protected void okPressed() {
		IStructuredSelection selection = (IStructuredSelection) listViewer.getSelection();

		ftName = (!selection.isEmpty()) ? selection.getFirstElement().toString() : "Arial";
		fSize = 10;
		
		fntData = new FontData(ftName, fSize, SWT.BOLD);
		super.okPressed();
	}

	/**
	 * Gets the font data.
	 *
	 * @return the font data
	 */
	public FontData getFontData() {
		return fntData;
	}
}
