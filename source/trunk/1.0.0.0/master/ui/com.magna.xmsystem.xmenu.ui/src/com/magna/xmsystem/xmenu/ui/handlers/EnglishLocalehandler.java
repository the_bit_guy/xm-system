
package com.magna.xmsystem.xmenu.ui.handlers;

import java.util.List;
import java.util.Locale;

import javax.inject.Named;

import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.core.services.nls.ILocaleChangeService;
import org.eclipse.e4.core.services.translation.TranslationService;
import org.eclipse.e4.ui.model.application.ui.MUIElement;
import org.eclipse.e4.ui.model.application.ui.menu.MHandledItem;

import com.magna.xmsystem.xmenu.ui.utils.XmMenuUtil;

/**
 * Handler class to change the language to English.
 *
 * @author subash.janarthanan
 */
public class EnglishLocalehandler {

	/**
	 * Execute.
	 *
	 * @param service the service
	 * @param locale the locale
	 * @param handledItem the handled item
	 */
	@Execute
	public void execute(ILocaleChangeService service, @Named(TranslationService.LOCALE) Locale locale, final MHandledItem handledItem) {
		XmMenuUtil instance = XmMenuUtil.getInstance();
		instance.setLocale(Locale.ENGLISH);
		service.changeApplicationLocale(Locale.ENGLISH);
		List<MUIElement> children = handledItem.getParent().getChildren();
		for (MUIElement muiElement : children) {
			if ("com.magna.xmsystem.xmenu.ui.handledmenuitem.enlocale".equals(muiElement.getElementId())) {
				((MHandledItem) muiElement).setSelected(true);
			} else if ("com.magna.xmsystem.xmenu.ui.handledmenuitem.delocale".equals(muiElement.getElementId())) {
				((MHandledItem) muiElement).setSelected(false);
			}
		}
		// XMSystemUtil.setLangName("English");
	}
}