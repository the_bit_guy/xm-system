/**
 * 
 */
package com.magna.xmsystem.xmenu.ui.enums;

/**
 * @author subhash
 *
 */
public enum UserHistotyOperation {

	OPENUSER("*OpenUser*"), OPENSITE("*OpenSite*"), OPENADMINAREA("*OpenAdminarea*"), OPENPROJECT("*OpenProject*");

	private String value;

	private UserHistotyOperation(String value) {
		this.value = value;
	}

	public String toString() {
		return this.value;
	}

}
