package com.magna.xmsystem.xmenu.ui.handlers;

import javax.inject.Inject;

import org.eclipse.e4.core.di.annotations.CanExecute;
import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.core.services.nls.Translation;
import org.eclipse.e4.ui.model.application.MApplication;
import org.eclipse.e4.ui.model.application.ui.basic.MPart;
import org.eclipse.e4.ui.workbench.modeling.EModelService;

import com.magna.xmsystem.xmenu.message.Message;
import com.magna.xmsystem.xmenu.ui.parts.XmMenuPart;
import com.magna.xmsystem.xmenu.ui.parts.XmMenuPartAction;
import com.magna.xmsystem.xmenu.ui.utils.CommonConstants;

/**
 * The Class ReloadHandler.
 * 
 * @author subash.janarthanan
 * 
 */
public class ReloadHandler {
	
	/**
	 * Inject of {@link EModelService}
	 */
	@Inject
	private EModelService modelService;

	/**
	 * Inject of {@link MApplication}
	 */
	@Inject
	private MApplication application;
	
	/** Member variable for messages. */
	@Inject
	@Translation
	transient protected Message messages;
	
	/**
	 * Execute.
	 */
	@Execute
	public void execute(){
		MPart part = (MPart) this.modelService.find(CommonConstants.PART_ID.XMENU_ID, application);
		XmMenuPart xmMenuPart = (XmMenuPart) part.getObject();
		XmMenuPartAction xmMenuPartAction = xmMenuPart.getXmMenuPartAction();
		xmMenuPartAction.reloadApplications();		
	}
	
	/**
	 * Can execute.
	 *
	 * @return true, if successful
	 */
	@CanExecute
	public boolean canExecute(){
		MPart part = (MPart) this.modelService.find(CommonConstants.PART_ID.XMENU_ID, application);
		XmMenuPart xmMenuPart = (XmMenuPart) part.getObject();
		XmMenuPartAction xmMenuPartAction = xmMenuPart.getXmMenuPartAction();
		return xmMenuPartAction.isProjectListEnabled();
	}
}
