package com.magna.xmsystem.xmenu.ui.parts;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.e4.core.contexts.ContextInjectionFactory;
import org.eclipse.e4.core.contexts.IEclipseContext;
import org.eclipse.e4.ui.model.application.MApplication;
import org.eclipse.e4.ui.model.application.commands.MCommand;
import org.eclipse.e4.ui.model.application.commands.MHandler;
import org.eclipse.e4.ui.model.application.ui.SideValue;
import org.eclipse.e4.ui.model.application.ui.basic.MTrimBar;
import org.eclipse.e4.ui.model.application.ui.basic.MTrimElement;
import org.eclipse.e4.ui.model.application.ui.basic.MTrimmedWindow;
import org.eclipse.e4.ui.model.application.ui.menu.MDynamicMenuContribution;
import org.eclipse.e4.ui.model.application.ui.menu.MHandledMenuItem;
import org.eclipse.e4.ui.model.application.ui.menu.MHandledToolItem;
import org.eclipse.e4.ui.model.application.ui.menu.MMenu;
import org.eclipse.e4.ui.model.application.ui.menu.MMenuElement;
import org.eclipse.e4.ui.model.application.ui.menu.MMenuSeparator;
import org.eclipse.e4.ui.model.application.ui.menu.MToolBar;
import org.eclipse.e4.ui.model.application.ui.menu.MToolBarElement;
import org.eclipse.e4.ui.workbench.IWorkbench;
import org.eclipse.e4.ui.workbench.modeling.EModelService;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ArmEvent;
import org.eclipse.swt.events.ArmListener;
import org.eclipse.swt.events.MouseAdapter;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.MouseMoveListener;
import org.eclipse.swt.events.MouseTrackAdapter;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.swt.widgets.ToolBar;
import org.eclipse.swt.widgets.ToolItem;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.magna.xmbackend.entities.LiveMessageTbl;
import com.magna.xmbackend.entities.LiveMessageTranslationTbl;
import com.magna.xmbackend.entities.RearrangeApplicationsTbl;
import com.magna.xmbackend.vo.enums.ApplicationPosition;
import com.magna.xmbackend.vo.rearrange.RearrangeApplicationsResponse;
import com.magna.xmsystem.dependencies.utils.LANG_ENUM;
import com.magna.xmsystem.dependencies.utils.XMSystemUtil;
import com.magna.xmsystem.ui.controls.widgets.CustomButton;
import com.magna.xmsystem.ui.controls.widgets.CustomMessageDialog;
import com.magna.xmsystem.xmenu.message.MessageRegistry;
import com.magna.xmsystem.xmenu.ui.dialogs.NoProjectDialog;
import com.magna.xmsystem.xmenu.ui.enums.StartApplicationType;
import com.magna.xmsystem.xmenu.ui.enums.UserHistotyOperation;
import com.magna.xmsystem.xmenu.ui.handlers.DummyMenuItem;
import com.magna.xmsystem.xmenu.ui.handlers.ExecuteApplicationHandler;
import com.magna.xmsystem.xmenu.ui.model.AdminMenuApplications;
import com.magna.xmsystem.xmenu.ui.model.Icon;
import com.magna.xmsystem.xmenu.ui.model.Project;
import com.magna.xmsystem.xmenu.ui.model.ProjectApplication;
import com.magna.xmsystem.xmenu.ui.model.StartApplication;
import com.magna.xmsystem.xmenu.ui.model.UserApplication;
import com.magna.xmsystem.xmenu.ui.service.LoadDataFromService;
import com.magna.xmsystem.xmenu.ui.utils.CaxMenuObjectModelUtil;
import com.magna.xmsystem.xmenu.ui.utils.CommonConstants;
import com.magna.xmsystem.xmenu.ui.utils.XmMenuUtil;

/**
 * Class for Xm menu part action.
 *
 * @author Chiranjeevi.Akula
 */
public class XmMenuPartAction extends XmMenuPartUI {

	/** The Constant LOGGER. */
	private static final Logger LOGGER = LoggerFactory.getLogger(XmMenuPartAction.class);

	/** The Constant LEFT_BUTTON. */
	private static final int LEFT_MOUSE_BUTTON = 1;

	/** Member variable 'eclipse context' for {@link IEclipseContext}. */
	@Inject
	private IEclipseContext eclipseContext;

	/** Member variable 'registry' for {@link MessageRegistry}. */
	@Inject
	private MessageRegistry registry;

	/** Member variable 'model service' for {@link EModelService}. */
	@Inject
	private EModelService modelService;

	/** Member variable 'm trimmed window' for {@link MTrimmedWindow}. */
	@Inject
	private MTrimmedWindow mTrimmedWindow;

	/** Member variable 'm application' for {@link MApplication}. */
	@Inject
	private MApplication mApplication;

	@Inject
	private IWorkbench workbench;

	/**
	 * Member variable 'fast user identification' for
	 * {@link FastUserIdentification}.
	 */
	private FastUserIdentification fastUserIdentification;

	/** Member variable 'project list' for {@link List<Project>}. */
	private List<Project> projectList;

	/**
	 * Member variable 'user applications map' for
	 * {@link Map<ApplicationPosition,List<UserApplication>>}.
	 */
	private Map<String, List<UserApplication>> userApplicationsMap;

	/**
	 * Member variable 'project applications map' for
	 * {@link Map<ApplicationPosition,List<ProjectApplication>>}.
	 */
	private Map<String, List<ProjectApplication>> projectApplicationsMap;

	/**
	 * Member variable 'start applications' for
	 * {@link Map<StartApplicationType,List<StartApplication>>}.
	 */
	private Map<StartApplicationType, List<StartApplication>> startApplications;

	/** The user apps tool bar items. */
	private List<MToolBarElement> userAppsToolBarItems;

	/** The project apps tool bar items. */
	private List<MToolBarElement> projectAppsToolBarItems;

	/** The applications menu items. */
	private List<MMenuElement> applicationsMenuItems;

	/** The admins menu items. */
	private List<MMenuElement> adminsMenuItems;
	
	/** The updated user app list. */
	private List<UserApplication> updatedUserAppList;
	
	/** The updated project app list. */
	private List<ProjectApplication> updatedProjectAppList;

	/** Member variable 'live messagejob' for {@link Job}. */
	private Job liveMessagejob;

	/**
	 * Constructor for XmMenuPartAction Class.
	 *
	 * @param parent
	 *            {@link Composite}
	 */
	@Inject
	public XmMenuPartAction(final Composite parent) {
		super(parent, SWT.NONE);
	}

	/**
	 * Method for Creates the UI.
	 */
	@PostConstruct
	private void createUI() {
		super.createUI(this.eclipseContext);
		fastUserIdentification = ContextInjectionFactory.make(FastUserIdentification.class, eclipseContext);
		initListeners();
		registerMessages(registry);
	}

	/**
	 * Method for Inits the application.
	 */
	public void initApplication() {

		LoadDataFromService.getInstance().logUserStatus(UserHistotyOperation.OPENSITE.toString());
		LoadDataFromService.getInstance().logUserStatus(UserHistotyOperation.OPENUSER.toString());

		fastUserIdentification.update();

		Job initAppJob = new Job("Initializing Applications...") {

			@Override
			protected IStatus run(IProgressMonitor monitor) {
				monitor.beginTask("Initializing Applications...", 100);
				monitor.worked(20);

				setComboEnabled(false);

				projectList = LoadDataFromService.getInstance().getUserProjects(true);
				String lastProject = LoadDataFromService.getInstance().getUserLastProject();
				if (!projectList.isEmpty()) {
					updatedProjectListInCombo();
					if (XMSystemUtil.isEmpty(lastProject) && projectList.size() == 1) {
						Project singlePro = projectList.get(0);
						lastProject = singlePro.getName();
						LoadDataFromService.getInstance().updateProjectInUserSettings(singlePro.getProjectId());
					}
					if (!XMSystemUtil.isEmpty(lastProject)) {

						String lastProjectId = null;
						for (Project project : projectList) {
							if (lastProject.equals(project.getName())) {
								lastProjectId = project.getProjectId();
								break;
							}
						}
						if (XMSystemUtil.isEmpty(lastProjectId)) {
							Display.getDefault().syncExec(new Runnable() {

								@Override
								public void run() {
									CustomMessageDialog.openInformation(Display.getCurrent().getActiveShell(),
											messages.projectAccessRemovedTitle, messages.projectAccessRemovedMessage);
								}
							});
							XmMenuUtil.getInstance()
									.updateStatusBarAndMessageConsole(messages.projectAccessRemovedMessage);
							Project project = projectList.get(0);
							lastProject = project.getName();
							XMSystemUtil.setProjectName(project.getName());
							//LoadDataFromService.getInstance().updateProjectInUserSettings(null);
							lastProjectId = project.getProjectId();
							LoadDataFromService.getInstance().updateProjectInUserSettings(lastProjectId);
						}
						
						loadStartScripts(lastProjectId);
						XMSystemUtil.setProjectName(lastProject);
						int exitValue = runProjectStartScripts();
						if (exitValue != 0) {
							XMSystemUtil.setProjectName(null);
							LoadDataFromService.getInstance().updateProjectInUserSettings(null);
							setProjectInCombo(null);
						} else {
							setProjectInCombo(lastProject);

							loadApplications(lastProjectId);
						}
						Display.getDefault().asyncExec(new Runnable() {

							@Override
							public void run() {
								Table table = tbComboProjects.getTable();

								String projectSwichMessage = CommonConstants.EMPTY_STR;
								if (tbComboProjects.getSelectionIndex() != -1) {
									TableItem selectedItem = table.getItem(tbComboProjects.getSelectionIndex());
									projectSwichMessage = messages.projectSwitchSuccess + "'" + selectedItem.getText()
											+ "'";

									LoadDataFromService.getInstance()
											.logUserStatus(UserHistotyOperation.OPENADMINAREA.toString());
									LoadDataFromService.getInstance()
											.logUserStatus(UserHistotyOperation.OPENPROJECT.toString());
								} else {
									projectSwichMessage = messages.projectSwitchFailure;
								}

								XmMenuUtil.getInstance().updateStatusBarAndMessageConsole(projectSwichMessage);
							}
						});
					}
				} else {
					if (!XMSystemUtil.isEmpty(lastProject)) {
						XMSystemUtil.setProjectName(null);
						LoadDataFromService.getInstance().updateProjectInUserSettings(null);

						Display.getDefault().asyncExec(new Runnable() {

							@Override
							public void run() {
								CustomMessageDialog.openError(Display.getCurrent().getActiveShell(),
										messages.projectAccessRemovedTitle, messages.projectAccessRemovedMessage);

							}
						});

						XmMenuUtil.getInstance().updateStatusBarAndMessageConsole(messages.projectAccessRemovedMessage);
					}
					projectRequestPopup();
				}

				loadAdminsMenuApplications();

				monitor.worked(50);

				updatePanel();
				
				updateTopComposite();

				fastUserIdentification.update();

				updateMenuAndToolItemWidgets();

				initLiveMessageJob();

				setComboEnabled(true);

				monitor.worked(70);
				return Status.OK_STATUS;
			}

		};
		initAppJob.setUser(true);
		initAppJob.schedule();
	}
	
	

	/**
	 * Live message job.
	 */
	private void initLiveMessageJob() {
		liveMessagejob = new Job("Fetching Live messages...") {
			boolean isPopupDisplayed = false;

			@Override
			protected IStatus run(IProgressMonitor monitor) {
				try {
					if (!isPopupDisplayed) {
						String siteId = CaxMenuObjectModelUtil.getInstance().getSiteId();
						String adminAreaId = CaxMenuObjectModelUtil.getInstance().getAdminAreaId();
						String projectId = getProjectId();

						Set<LiveMessageTbl> liveMessageTbls = LoadDataFromService.getInstance()
								.getLiveMessageResponseList(siteId, adminAreaId, projectId);

						if (liveMessageTbls != null && !liveMessageTbls.isEmpty()) {
							for (LiveMessageTbl liveMessageTbl : liveMessageTbls) {
								LANG_ENUM currentLocaleEnum = XmMenuUtil.getInstance().getCurrentLocaleEnum();
								//LANG_ENUM defaultLocaleEnum = XmMenuUtil.getInstance().getDefaultLocaleEnum();
								final String popUp = liveMessageTbl.getIspopup();
								String liveMessageId = liveMessageTbl.getLiveMessageId();
								final Boolean isPopUp = Boolean.valueOf(popUp);
								
								Map<LANG_ENUM, String> subjectMap = new HashMap<>();
								Map<LANG_ENUM, String> MessageMap = new HashMap<>();
								
								Collection<LiveMessageTranslationTbl> liveMessageTranslationTbls = liveMessageTbl.getLiveMessageTranslationTbls();
								for (LiveMessageTranslationTbl liveMessageTranslationTbl : liveMessageTranslationTbls) {
									LANG_ENUM langEnum = LANG_ENUM.getLangEnum(liveMessageTranslationTbl.getLanguageCode().getLanguageCode());
									subjectMap.put(langEnum, liveMessageTranslationTbl.getSubject());
									MessageMap.put(langEnum, liveMessageTranslationTbl.getMessage());
								}
								
								String liveMessageSubject = subjectMap.get(currentLocaleEnum);
								//liveMessageSubject = !(XMSystemUtil.isEmpty(liveMessageSubject)) ? liveMessageSubject : subjectMap.get(defaultLocaleEnum);
								liveMessageSubject = !(XMSystemUtil.isEmpty(liveMessageSubject)) ? liveMessageSubject : liveMessageTbl.getSubject();
								
								String liveMessage = MessageMap.get(currentLocaleEnum);
								//liveMessage = !(XMSystemUtil.isEmpty(liveMessage)) ? liveMessage : MessageMap.get(defaultLocaleEnum);
								liveMessage = !(XMSystemUtil.isEmpty(liveMessage)) ? liveMessage : liveMessageTbl.getMessage();
								
								final String liveMessageSubjectFinal = liveMessageSubject;
								final String liveMessageFinal = liveMessage;

								if (isPopUp) {
									isPopupDisplayed = true;

									Display.getDefault().asyncExec(new Runnable() {
										@Override
										public void run() {
											if (liveMessageSubjectFinal != null && liveMessageFinal != null) {
												CustomMessageDialog.openInformation(
														Display.getDefault().getActiveShell(), liveMessageSubjectFinal,
														liveMessageFinal);
											}
										}
									});

									isPopupDisplayed = false;
									XmMenuUtil.getInstance().updateMessageConsole(liveMessage);
								} else {
									XmMenuUtil.getInstance().updateMessageConsole(liveMessage);
								}

								LoadDataFromService.getInstance().updateLiveMessageStatus(liveMessageId);
							}
						}
					}
				} catch (Exception e) {
					LOGGER.error("Live messages Job: " + e.getMessage());
				} finally {
					this.schedule(CommonConstants.LIVE_MESSAGE_EXE_TIME_INTERVALS.CONSTANT_TIME_INTERVAL);
				}

				return Status.OK_STATUS;
			}
		};
		liveMessagejob.schedule(CommonConstants.LIVE_MESSAGE_EXE_TIME_INTERVALS.INITIAL_TIME_INTERVAL);
	}

	/**
	 * Gets the current selected project id.
	 *
	 * @return the project id
	 */
	public String getProjectId() {

		final String[] selectedProjectId = new String[1];
		Display.getDefault().syncExec(new Runnable() {

			@Override
			public void run() {
				if (!tbComboProjects.isDisposed()) {
					String selectedProject = tbComboProjects.getText();

					if (!XMSystemUtil.isEmpty(selectedProject)) {
						TableItem selectedItem = null;
						TableItem[] items = tbComboProjects.getTable().getItems();
						for (TableItem tableItem : items) {
							if (tableItem.getText().equals(selectedProject)) {
								selectedItem = tableItem;
								break;
							}
						}

						if (selectedItem != null) {
							selectedProjectId[0] = (String) selectedItem.getData("id");
						}
					}
				}
			}
		});

		return selectedProjectId[0];
	}

	/**
	 * Method for Project request popup.
	 */
	private void projectRequestPopup() {
		String projectRequestLink = LoadDataFromService.getInstance().getNoProjectMessage();
		if (!XMSystemUtil.isEmpty(projectRequestLink)) {
			Display.getDefault().asyncExec(new Runnable() {

				@Override
				public void run() {
					NoProjectDialog.openErrorWithHyperLink(Display.getCurrent().getActiveShell(), messages.projectRequestTitle, projectRequestLink);
					/*CustomMessageDialog.openErrorWithHyperLink(Display.getCurrent().getActiveShell(),
							messages.projectRequestTitle, messages.projectRequestMessage, projectRequestLink.toString());*/
				}
			});
		}
	}

	/**
	 * Method for Load applications.
	 *
	 * @param projectId
	 *            {@link String}
	 */
	private void loadApplications(final String projectId) {
		if (userApplicationsMap != null) {
			userApplicationsMap.clear();
		}
		if (projectApplicationsMap != null) {
			projectApplicationsMap.clear();
		}

		if (!XMSystemUtil.isEmpty(projectId)) {
			String adminAreaId = CaxMenuObjectModelUtil.getInstance().getAdminAreaId(projectId);
			if (!XMSystemUtil.isEmpty(adminAreaId)) {

				userApplicationsMap = LoadDataFromService.getInstance().getUserUserApplications(adminAreaId);
				projectApplicationsMap = LoadDataFromService.getInstance().getUserProjectApplications(adminAreaId,
						projectId);
			}
		}

		Display.getDefault().asyncExec(new Runnable() {

			@Override
			public void run() {
				buttonViewDisplay(userApplicationsMap.get(ApplicationPosition.BUTTONTASK.name()),
						projectApplicationsMap.get(ApplicationPosition.BUTTONTASK.name()), projectId);
				menuViewDisplay(userApplicationsMap.get(ApplicationPosition.MENUTASK.name()),
						projectApplicationsMap.get(ApplicationPosition.MENUTASK.name()));
				iconViewDisplay(userApplicationsMap.get(ApplicationPosition.ICONTASK.name()),
						projectApplicationsMap.get(ApplicationPosition.ICONTASK.name()));
			}
		});
	}

	/**
	 * Method for Button view display.
	 *
	 * @param userApplications
	 *            {@link List<UserApplication>}
	 * @param projectApplications
	 *            {@link List<ProjectApplication>}
	 * @param projectId
	 * @param siteId
	 */
	private void buttonViewDisplay(final List<UserApplication> userApplications,
			final List<ProjectApplication> projectApplications, String projectId) {
		String siteId = CaxMenuObjectModelUtil.getInstance().getSiteId();
		
		RearrangeApplicationsResponse rearrangeSettingsRes = LoadDataFromService.getInstance()
				.isRearrangeSettingsAvailable(siteId, projectId);

		if (rearrangeSettingsRes != null) {
			String isRearrangeSettingsAvailable = rearrangeSettingsRes.getIsRearrangeSettingsAvailable();
			boolean isReaarangeSettingsAvail = Boolean.valueOf(isRearrangeSettingsAvailable);
			if (isReaarangeSettingsAvail) {
				RearrangeApplicationsTbl rearrangeApplicationsTbl = rearrangeSettingsRes.getRearrangeApplicationsTbl();
				if (rearrangeApplicationsTbl != null) {
					String userApplicationsString = rearrangeApplicationsTbl.getUserApplications();
					String projectApplicationsString = rearrangeApplicationsTbl.getProjectApplications();
					List<String> userAppNameList = new ArrayList<>();
					List<String> projectAppNameList = new ArrayList<>();
					for (UserApplication userApplication : userApplications) {
						String userAppName = userApplication.getName();
						userAppNameList.add(userAppName);
					}
					
					for (ProjectApplication projectApplication : projectApplications) {
						String projectAppName = projectApplication.getName();
						projectAppNameList.add(projectAppName);
					}
					
					this.updatedUserAppList = LoadDataFromService.getInstance()
							.getUpdatedUserAppList(userApplicationsString, userAppNameList);
					this.updatedProjectAppList = LoadDataFromService.getInstance()
							.getUpdatedProjectAppList(projectApplicationsString, projectAppNameList);
					
					horizontalViewContents(updatedUserAppList, updatedProjectAppList);
					verticalViewContents(updatedUserAppList, updatedProjectAppList);
					
					userApplicationsMap.put(ApplicationPosition.BUTTONTASK.name(), updatedUserAppList);
					projectApplicationsMap.put(ApplicationPosition.BUTTONTASK.name(), updatedProjectAppList);
				}
			} else {
				horizontalViewContents(userApplications, projectApplications);
				verticalViewContents(userApplications, projectApplications);
			}
		} else {
			horizontalViewContents(userApplications, projectApplications);
			verticalViewContents(userApplications, projectApplications);
		}
	}

	/**
	 * Method for Updated project list in combo.
	 */
	private void updatedProjectListInCombo() {
		Display.getDefault().syncExec(new Runnable() {

			@Override
			public void run() {
				Table table = tbComboProjects.getTable();
				table.removeAll();
				for (Project project : projectList) {
					TableItem item = new TableItem(table, SWT.NONE);
					item.setData(project);
					String text = project.getName();
					if (text != null) {
						item.setText(text);
					}
					Image image = XmMenuUtil.getInstance().getImage(project.getIcon(), project.isActive());
					if (image != null) {
						item.setImage(image);
					}
					String id = project.getProjectId();
					if (id != null) {
						item.setData("id", id);
					}
				}
				tbComboProjects.updateSelectedImage();
				tbComboProjects.updateSize();
				// table.notifyListeners(SWT.Resize, new Event());
			}
		});
	}

	/**
	 * Method for Removes the projects in combo.
	 */
	private void removeProjectsInCombo() {
		Display.getDefault().syncExec(new Runnable() {

			@Override
			public void run() {
				Table table = tbComboProjects.getTable();
				table.removeAll();
			}
		});
	}

	/**
	 * Sets the project in combo.
	 *
	 * @param lastProject
	 *            the new project in combo
	 */
	private void setProjectInCombo(final String lastProject) {
		Display.getDefault().asyncExec(new Runnable() {

			@Override
			public void run() {
				if (!XMSystemUtil.isEmpty(lastProject)) {
					Table table = tbComboProjects.getTable();
					for (int index = 0; index < table.getItemCount(); index++) {
						TableItem item = table.getItem(index);
						if (lastProject.equals(item.getText())) {
							tbComboProjects.select(index);
							break;
						}
					}
				} else {
					tbComboProjects.select(-1);
					tbComboProjects.clearSelection();
					XMSystemUtil.setAdminAreaName(null);
				}
			}
		});
	}

	/**
	 * Method for Inits the listeners.
	 */
	private void initListeners() {

		/*
		 * this.tbComboProjects.addFocusListener(new FocusAdapter() {
		 * 
		 * @Override public void focusGained(FocusEvent event) {
		 * 
		 * Control focusControl = getDisplay().getFocusControl(); if (focusControl
		 * instanceof Button) { if (tbComboArrowButton == null) { tbComboArrowButton =
		 * ((Button) focusControl); tbComboArrowButton.addSelectionListener(new
		 * SelectionAdapter() {
		 * 
		 * @Override public void widgetSelected(SelectionEvent arg0) {
		 * refreshProjectList(); } }); } } } });
		 */

		this.tbComboProjects.getArrowControl().addSelectionListener(new SelectionAdapter() {

			@Override
			public void widgetSelected(SelectionEvent arg0) {
				if (tbComboProjects.isDropped()) {
					Job job = new Job("Loading Projects...") {

						@Override
						protected IStatus run(IProgressMonitor monitor) {

							monitor.beginTask("Loading Projects..", 100);
							monitor.worked(30);

							refreshProjectList();

							monitor.worked(70);
							return Status.OK_STATUS;
						}
					};
					job.setUser(true);
					job.schedule();
				}
			}
		});

		this.tbComboProjects.addMouseListener(new MouseAdapter() {

			@Override
			public void mouseDown(MouseEvent arg0) {
				Job job = new Job("Loading Projects...") {

					@Override
					protected IStatus run(IProgressMonitor monitor) {

						monitor.beginTask("Loading Projects..", 100);
						monitor.worked(30);

						refreshProjectList();

						monitor.worked(70);
						return Status.OK_STATUS;
					}
				};
				job.setUser(true);
				job.schedule();
			}
		});

		this.tbComboProjects.addSelectionListener(new SelectionAdapter() {

			@Override
			public void widgetSelected(final SelectionEvent event) {

				setComboEnabled(false);
				Table table = tbComboProjects.getTable();
				int selectionIndex = tbComboProjects.getSelectionIndex();
				if (selectionIndex != -1) {
					TableItem selectedItem = table.getItem(selectionIndex);
					final String selectedProjectId = (String) selectedItem.getData("id");
					final String selectedProjectName = selectedItem.getText();
					final String lastSelectedProjectName = XMSystemUtil.getProjectName();

					String projectId = null;
					if (!XMSystemUtil.isEmpty(lastSelectedProjectName)) {
						TableItem[] items = table.getItems();
						for (TableItem item : items) {
							if (lastSelectedProjectName.equals(item.getText())) {
								projectId = (String) item.getData("id");
								break;
							}
						}
					}

					final String lastSelectedProjectId = projectId;

					Job projectSwitchJob = new Job("Switching project...") {

						@Override
						protected IStatus run(IProgressMonitor monitor) {
							monitor.beginTask("Switching project...", 100);
							monitor.worked(20);
							/*
							 * String runningAppName = XmMenuUtil.getInstance().getRunningApplication(); if
							 * (!XMSystemUtil.isEmpty(runningAppName)) { Display.getDefault().asyncExec(new
							 * Runnable() {
							 * 
							 * @Override public void run() {
							 * CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
							 * messages.errorDialogTitile, messages.format(
							 * messages.switchProjectColseRunningAppMessage, runningAppName));
							 * 
							 * if (!XMSystemUtil.isEmpty(lastSelectedProjectId)) {
							 * setProjectInCombo(lastSelectedProjectName); } } }); } else {
							 * 
							 * }
							 */

							String finalProjectName = null;
							String finalProjectId = null;

							clearAppsInPanel();

							if (!XMSystemUtil.isEmpty(lastSelectedProjectId)) {
								loadStartScripts(lastSelectedProjectId);
								int exitValue = runProjectEndScripts();
								if (exitValue != 0) {
									// setProjectInCombo(null);
									finalProjectName = lastSelectedProjectName;
									finalProjectId = lastSelectedProjectId;

									setProjectInCombo(lastSelectedProjectName);
									loadApplications(lastSelectedProjectId);
								} else {
									loadStartScripts(selectedProjectId);
									XMSystemUtil.setProjectName(selectedProjectName);
									exitValue = runProjectStartScripts();
									if (exitValue != 0) {
										loadStartScripts(lastSelectedProjectId);
										XMSystemUtil.setProjectName(lastSelectedProjectName);
										exitValue = runProjectStartScripts();
										if (exitValue != 0) {
											setProjectInCombo(null);
										} else {
											finalProjectName = lastSelectedProjectName;
											finalProjectId = lastSelectedProjectId;

											setProjectInCombo(lastSelectedProjectName);
											loadApplications(lastSelectedProjectId);
										}
									} else {
										finalProjectName = selectedProjectName;
										finalProjectId = selectedProjectId;

										loadApplications(selectedProjectId);
									}
								}
							} else {
								loadStartScripts(selectedProjectId);
								XMSystemUtil.setProjectName(selectedProjectName);
								int exitValue = runProjectStartScripts();
								if (exitValue != 0) {
									setProjectInCombo(null);
								} else {
									finalProjectName = selectedProjectName;
									finalProjectId = selectedProjectId;

									loadApplications(selectedProjectId);
								}
							}

							monitor.worked(50);

							XMSystemUtil.setProjectName(finalProjectName);
							LoadDataFromService.getInstance().updateProjectInUserSettings(finalProjectId);

							String projectSwichMessage = CommonConstants.EMPTY_STR;
							if (finalProjectName != null) {
								projectSwichMessage = messages.projectSwitchSuccess + "'" + finalProjectName + "'";

								LoadDataFromService.getInstance()
										.logUserStatus(UserHistotyOperation.OPENADMINAREA.toString());
								LoadDataFromService.getInstance()
										.logUserStatus(UserHistotyOperation.OPENPROJECT.toString());
							} else {
								projectSwichMessage = messages.projectSwitchFailure;
							}
							XmMenuUtil.getInstance().updateStatusBarAndMessageConsole(projectSwichMessage);

							fastUserIdentification.update();

							updatePanel();

							updateMenuAndToolItemWidgets();

							setComboEnabled(true);

							monitor.worked(70);

							return Status.OK_STATUS;
						}
					};
					projectSwitchJob.setUser(true);
					projectSwitchJob.schedule();
				}
			}
		});

		final Image verticalImage = XMSystemUtil.getInstance().getImage(this.getClass(), "icons/vertical.png", true, false);
		final Image horizontalImage = XMSystemUtil.getInstance().getImage(this.getClass(), "icons/horizontal.png", true, false);

		this.toolItem.addListener(SWT.Selection, event -> {
			if (this.verticalScrolledComposite != null && !this.verticalScrolledComposite.isDisposed()
					&& this.horizontalComposite != null && !this.horizontalComposite.isDisposed()) {
				boolean isHorizontalView = (boolean) ((ToolItem) event.widget).getData("Horizontal");
				if (isHorizontalView) {
					this.contentPanelLayout.topControl = this.verticalScrolledComposite;
					contentPanel.layout();
					this.toolItem.setImage(verticalImage);
					this.toolItem.setData("Horizontal", false);
					registry.register((text) -> {
						if (this.toolItem != null && !this.toolItem.isDisposed()) {
							this.toolItem.setToolTipText(text);
						}
					}, (message) -> {
						return message.horizontalIconTootip;
					});
				} else {
					this.contentPanelLayout.topControl = this.horizontalComposite;
					contentPanel.layout();
					this.toolItem.setImage(horizontalImage);
					this.toolItem.setData("Horizontal", true);
					registry.register((text) -> {
						if (this.toolItem != null && !this.toolItem.isDisposed()) {
							this.toolItem.setToolTipText(text);
						}
					}, (message) -> {
						return message.verticalIconTooltip;
					});
				}
				XmMenuUtil.getInstance().setINEclipsePrefs(CommonConstants.PREFERENCE_KEY.HORIZONTAL_VIEW,
						Boolean.valueOf(!isHorizontalView));
			}
		});

		this.resetSwitchedUserItem.addSelectionListener(new SelectionAdapter() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				workbench.restart();
			}

		});

	}

	/**
	 * Method for Refresh project list.
	 */
	private void refreshProjectList() {

		removeProjectsInCombo();

		projectList = LoadDataFromService.getInstance().getUserProjects(false);

		updatedProjectListInCombo();

		String lastSelectedProject = XMSystemUtil.getProjectName();
		if (!XMSystemUtil.isEmpty(lastSelectedProject)) {
			boolean isProjExists = false;
			for (Project project : projectList) {
				if (lastSelectedProject.equals(project.getName())) {
					isProjExists = true;
					break;
				}
			}
			if (!isProjExists) {
				clearAppsInPanel();

				setProjectInCombo(null);
				XMSystemUtil.setProjectName(null);
				LoadDataFromService.getInstance().updateProjectInUserSettings(null);

				fastUserIdentification.update();

				updatePanel();

				Display.getDefault().syncExec(new Runnable() {
					@Override
					public void run() {
						CustomMessageDialog.openError(Display.getCurrent().getActiveShell(),
								messages.projectAccessRemovedTitle, messages.projectAccessRemovedMessage);
					}
				});

				XmMenuUtil.getInstance().updateStatusBarAndMessageConsole(messages.projectAccessRemovedMessage);

				if (projectList != null && projectList.isEmpty()) {
					projectRequestPopup();
				}
			}
		}
	}
	
	/**
	 * Update X menu panel.
	 */
	public void updateXMenuPanel(){
		updateTopComposite();
	}

	/**
	 * Method for Vertical view contents.
	 *
	 * @param userAppList
	 *            {@link List<UserApplication>}
	 * @param projectAppList
	 *            {@link List<ProjectApplication>}
	 */
	private void verticalViewContents(final List<UserApplication> userAppList,
			final List<ProjectApplication> projectAppList) {
		if (userAppList != null && projectAppList != null) {
			GridData gridData = new GridData(SWT.FILL, SWT.FILL, true, false);
			// gridData.widthHint = 180;

			loadButtons(userAppList, projectAppList, vUserAppComposite, vProjectAppComposite, gridData);

			/*
			 * vUserAppComposite.notifyListeners(SWT.Resize, new Event());
			 * vProjectAppComposite.notifyListeners(SWT.Resize, new Event());
			 */
			verticalScrolledComposite.notifyListeners(SWT.Resize, new Event());

			vUserAppComposite.layout();
			vProjectAppComposite.layout();
			vProjectAppComposite.getParent().layout();
			vProjectAppComposite.getParent().update();
		}
	}

	/**
	 * Method for Horizontal view contents.
	 *
	 * @param userAppList
	 *            {@link List<UserApplication>}
	 * @param projectAppList
	 *            {@link List<ProjectApplication>}
	 */
	private void horizontalViewContents(final List<UserApplication> userAppList,
			final List<ProjectApplication> projectAppList) {
		if (userAppList != null && projectAppList != null) {

			GridData gridData = new GridData(SWT.FILL, SWT.FILL, true, false);
			gridData.widthHint = 120;

			loadButtons(userAppList, projectAppList, hUserAppComposite, hProjectAppComposite, gridData);

			hUserAppscrolledComposite.notifyListeners(SWT.Resize, new Event());
			hUserAppComposite.layout();
			hUserAppComposite.getParent().update();

			hProjectAppscrolledComposite.notifyListeners(SWT.Resize, new Event());
			hProjectAppComposite.layout();
			hProjectAppComposite.getParent().update();
		}
	}

	/**
	 * Method for Load buttons.
	 *
	 * @param userAppList
	 *            {@link List<UserApplication>}
	 * @param projectAppList
	 *            {@link List<ProjectApplication>}
	 * @param userAppComposite
	 *            {@link Composite}
	 * @param projectAppComposite
	 *            {@link Composite}
	 * @param gridData
	 *            {@link GridData}
	 */
	private void loadButtons(final List<UserApplication> userAppList, final List<ProjectApplication> projectAppList,
			final Composite userAppComposite, final Composite projectAppComposite, final GridData gridData) {

		for (UserApplication userApplication : userAppList) {
			final Icon icon = userApplication.getIcon();
			final List<UserApplication> childUserApplications = userApplication.getChildUserApplications();
			boolean parent = userApplication.isParent();

			CustomButton userAppButton = new CustomButton(userAppComposite, SWT.NONE);
			userAppButton.setLayoutData(gridData);
			userAppButton.setData(userApplication);

			Image image = XmMenuUtil.getInstance().getImage(icon, userApplication.isActive());
			if (image != null) {
				userAppButton.setImage(image);
			}

			if (parent || (childUserApplications != null && !childUserApplications.isEmpty())) {
				userAppButton.setArrow(true);
				hookChildUserAppsToButton(userAppButton, childUserApplications);
			} else {
				userAppButton.addMouseListener(new MouseAdapter() {

					@Override
					public void mouseUp(MouseEvent event) {
						if (event.button != LEFT_MOUSE_BUTTON) {
							return;
						}
						Composite composite = ((Composite) event.widget).getParent();
						if (composite instanceof CustomButton) {
							UserApplication userApplication = (UserApplication) ((CustomButton) composite).getData();
							if (userApplication.isSingleton()) {
								XmMenuUtil.getInstance().applyApplicationTimeOut(composite);
							}

							String localeApplicationName = XmMenuUtil.getInstance()
									.getLocaleApplicationName(userApplication);
							XmMenuUtil.getInstance().updateStatusBarAndMessageConsole(
									messages.launchApplication + " " + localeApplicationName);

							XMSystemUtil.setTaskName(localeApplicationName);

							BaseAppExecution.getInstance().excute(userApplication.getBaseApplication(), false);
						}
					}
				});
			}

			userAppButton.addMouseTrackListener(new MouseTrackAdapter() {
				@Override
				public void mouseEnter(MouseEvent event) {
					XmMenuUtil.getInstance().updateStatusBar(userApplication.getName());
				}
			});

			registry.register((text) -> {
				if (userAppButton != null && !userAppButton.isDisposed()) {
					userAppButton.setText(text);
					userAppButton.layout();
				}
			}, (message) -> {
				if (userAppButton != null && !userAppButton.isDisposed()) {
					return XmMenuUtil.getInstance().getLocaleApplicationName(userAppButton.getData());
				}
				return CommonConstants.EMPTY_STR;
			});

			registry.register((text) -> {
				if (userAppButton != null && !userAppButton.isDisposed()) {
					userAppButton.setToolTipText(text);
				}
			}, (message) -> {
				if (userAppButton != null && !userAppButton.isDisposed()) {
					return XmMenuUtil.getInstance().getLocaleApplicationDescription(userAppButton.getData());
				}
				return CommonConstants.EMPTY_STR;
			});
		}

		for (ProjectApplication projectApplication : projectAppList) {
			final Icon icon = projectApplication.getIcon();
			final List<ProjectApplication> childProjectApplications = projectApplication.getChildProjectApplications();
			boolean parent = projectApplication.isParent();

			CustomButton projectAppBtn = new CustomButton(projectAppComposite, SWT.NONE);

			projectAppBtn.setLayoutData(gridData);
			projectAppBtn.setData(projectApplication);

			Image image = XmMenuUtil.getInstance().getImage(icon, projectApplication.isActive());
			if (image != null) {
				projectAppBtn.setImage(image);
			}

			if (parent || (childProjectApplications != null && !childProjectApplications.isEmpty())) {
				projectAppBtn.setArrow(true);
				hookChildProjectAppsToButton(projectAppBtn, childProjectApplications);
			} else {
				projectAppBtn.addMouseListener(new MouseAdapter() {

					@Override
					public void mouseUp(MouseEvent event) {
						if (event.button != LEFT_MOUSE_BUTTON) {
							return;
						}
						Composite composite = ((Composite) event.widget).getParent();
						if (composite instanceof CustomButton) {
							ProjectApplication projectApplication = (ProjectApplication) ((CustomButton) composite)
									.getData();
							if (projectApplication.isSingleton()) {
								XmMenuUtil.getInstance().applyApplicationTimeOut(composite);
							}

							String localeApplicationName = XmMenuUtil.getInstance()
									.getLocaleApplicationName(projectApplication);
							XmMenuUtil.getInstance().updateStatusBarAndMessageConsole(
									messages.launchApplication + " " + localeApplicationName);

							XMSystemUtil.setTaskName(localeApplicationName);

							BaseAppExecution.getInstance().excute(projectApplication.getBaseApplication(), false);
						}
					}
				});
			}

			projectAppBtn.addMouseTrackListener(new MouseTrackAdapter() {
				@Override
				public void mouseEnter(MouseEvent event) {
					XmMenuUtil.getInstance().updateStatusBar(projectApplication.getName());
				}
			});

			registry.register((text) -> {
				if (projectAppBtn != null && !projectAppBtn.isDisposed()) {
					projectAppBtn.setText(text);
					projectAppBtn.layout();
				}
			}, (message) -> {
				if (projectAppBtn != null && !projectAppBtn.isDisposed()) {
					return XmMenuUtil.getInstance().getLocaleApplicationName(projectAppBtn.getData());
				}
				return CommonConstants.EMPTY_STR;
			});

			registry.register((text) -> {
				if (projectAppBtn != null && !projectAppBtn.isDisposed()) {
					projectAppBtn.setToolTipText(text);
				}
			}, (message) -> {
				if (projectAppBtn != null && !projectAppBtn.isDisposed()) {
					return XmMenuUtil.getInstance().getLocaleApplicationDescription(projectAppBtn.getData());
				}
				return CommonConstants.EMPTY_STR;
			});
		}
	}

	/**
	 * Method for Register messages.
	 *
	 * @param registry
	 *            {@link MessageRegistry}
	 */
	private void registerMessages(final MessageRegistry registry) {
		registry.register((text) -> {
			if (lblProjectList != null && !lblProjectList.isDisposed()) {
				updatedWidgetText(text, lblProjectList);
			}
		}, (message) -> {
			if (lblProjectList != null && !lblProjectList.isDisposed()) {
				return message.projectListLabel;
			}
			return CommonConstants.EMPTY_STR;
		});

		registry.register((text) -> {
			if (hLblUserApplications != null && !hLblUserApplications.isDisposed()) {
				updatedWidgetText(text, hLblUserApplications);
			}
		}, (message) -> {
			if (hLblUserApplications != null && !hLblUserApplications.isDisposed()) {
				return message.userApplicationsLabel;
			}
			return CommonConstants.EMPTY_STR;
		});

		registry.register((text) -> {
			if (vLblUserApplications != null && !vLblUserApplications.isDisposed()) {
				updatedWidgetText(text, vLblUserApplications);
			}
		}, (message) -> {
			if (vLblUserApplications != null && !vLblUserApplications.isDisposed()) {
				return message.userApplicationsLabel;
			}
			return CommonConstants.EMPTY_STR;
		});

		registry.register((text) -> {
			if (hLblProjectApplications != null && !hLblProjectApplications.isDisposed()) {
				updatedWidgetText(text, hLblProjectApplications);
			}
		}, (message) -> {
			if (hLblProjectApplications != null && !hLblProjectApplications.isDisposed()) {
				return message.projectApplicationsLabel;
			}
			return CommonConstants.EMPTY_STR;
		});

		registry.register((text) -> {
			if (vLblProjectApplications != null && !vLblProjectApplications.isDisposed()) {
				updatedWidgetText(text, vLblProjectApplications);
			}
		}, (message) -> {
			if (vLblProjectApplications != null && !vLblProjectApplications.isDisposed()) {
				return message.projectApplicationsLabel;
			}
			return CommonConstants.EMPTY_STR;
		});

		registry.register((text) -> {
			if (resetSwitchedUserItem != null && !resetSwitchedUserItem.isDisposed()) {
				resetSwitchedUserItem.setToolTipText(text);
			}
		}, (message) -> {
			if (resetSwitchedUserItem != null && !resetSwitchedUserItem.isDisposed()) {
				return message.resetSwitchedUserIconTooltip;
			}
			return CommonConstants.EMPTY_STR;
		});

	}

	/**
	 * Method for Updated widget text.
	 *
	 * @param message
	 *            {@link String}
	 * @param control
	 *            {@link Control}
	 */
	private void updatedWidgetText(final String message, final Control control) {
		if (control instanceof Label && message != null) {
			((Label) control).setText(message);
			control.requestLayout();
			control.getParent().redraw();
			control.getParent().getParent().update();
		}
	}

	/**
	 * Method for Hook child user apps to button.
	 *
	 * @param userAppBtn
	 *            {@link CustomButton}
	 * @param childUserApplications
	 *            {@link List<UserApplication>}
	 */
	private void hookChildUserAppsToButton(final CustomButton userAppBtn,
			final List<UserApplication> childUserApplications) {
		final Menu menu = new Menu(userAppBtn.getShell(), SWT.POP_UP);
		menu.setData("org.eclipse.e4.ui.css.id", "MyCSSTagFormenuItem");
		for (UserApplication userApplication : childUserApplications) {
			final MenuItem menuItem = new MenuItem(menu, SWT.PUSH);
			menuItem.setData(userApplication);
			menuItem.setData("org.eclipse.e4.ui.css.id", "MyCSSTagFormenuItem");

			Image image = XmMenuUtil.getInstance().getImage(userApplication.getIcon(),
					userApplication.isActive());
			if (image != null) {
				menuItem.setImage(image);
			}
			menuItem.addSelectionListener(new SelectionAdapter() {

				@Override
				public void widgetSelected(SelectionEvent event) {
					MenuItem menuItem = (MenuItem) event.widget;

					UserApplication userApplication = (UserApplication) menuItem.getData();
					if (userApplication.isSingleton()) {
						XmMenuUtil.getInstance().applyApplicationTimeOut(menuItem);
					}
					
					String localeApplicationName = XmMenuUtil.getInstance().getLocaleApplicationName(userApplication);
					XmMenuUtil.getInstance()
							.updateStatusBarAndMessageConsole(messages.launchApplication + " " + localeApplicationName);

					XMSystemUtil.setTaskName(localeApplicationName);

					BaseAppExecution.getInstance().excute(userApplication.getBaseApplication(), false);
				}
			});

			menuItem.addArmListener(new ArmListener() {
				@Override
				public void widgetArmed(ArmEvent armevent) {
					XmMenuUtil.getInstance().updateStatusBar(userApplication.getName());
				}
			});

			registry.register((text) -> {
				if (menuItem != null && !menuItem.isDisposed()) {
					menuItem.setText(text);
				}
			}, (message) -> {
				if (menuItem != null && !menuItem.isDisposed()) {
					return XmMenuUtil.getInstance().getLocaleApplicationName(menuItem.getData());
				}
				return CommonConstants.EMPTY_STR;
			});

			registry.register((text) -> {
				if (menuItem != null && !menuItem.isDisposed()) {
					menuItem.setToolTipText(text);
				}
			}, (message) -> {
				if (menuItem != null && !menuItem.isDisposed()) {
					return XmMenuUtil.getInstance().getLocaleApplicationDescription(menuItem.getData());
				}
				return CommonConstants.EMPTY_STR;
			});
		}
		userAppBtn.setMenu(menu);

		userAppBtn.addMouseListener(new MouseAdapter() {

			@Override
			public void mouseDown(MouseEvent event) {
				Composite composite = ((Composite) event.widget).getParent();
				if (composite instanceof CustomButton) {
					Menu menu = ((CustomButton) composite).getMenu();
					if (menu != null) {
						menu.setVisible(true);
					}
				}
			}
		});
	}

	/**
	 * Method for Hook child project apps to button.
	 *
	 * @param projectAppBtn
	 *            {@link CustomButton}
	 * @param childProjectApplications
	 *            {@link List<ProjectApplication>}
	 */
	private void hookChildProjectAppsToButton(final CustomButton projectAppBtn,
			final List<ProjectApplication> childProjectApplications) {
		final Menu menu = new Menu(projectAppBtn.getShell(), SWT.POP_UP);
		for (ProjectApplication projectApplication : childProjectApplications) {
			final MenuItem menuItem = new MenuItem(menu, SWT.PUSH);
			menuItem.setData(projectApplication);

			Image image = XmMenuUtil.getInstance().getImage(projectApplication.getIcon(),
					projectApplication.isActive());
			if (image != null) {
				menuItem.setImage(image);
			}

			menuItem.addSelectionListener(new SelectionAdapter() {

				@Override
				public void widgetSelected(SelectionEvent event) {
					MenuItem menuItem = (MenuItem) event.widget;

					ProjectApplication projectApplication = (ProjectApplication) menuItem.getData();
					if (projectApplication.isSingleton()) {
						XmMenuUtil.getInstance().applyApplicationTimeOut(menuItem);
					}

					String localeApplicationName = XmMenuUtil.getInstance()
							.getLocaleApplicationName(projectApplication);
					XmMenuUtil.getInstance()
							.updateStatusBarAndMessageConsole(messages.launchApplication + " " + localeApplicationName);

					XMSystemUtil.setTaskName(localeApplicationName);

					BaseAppExecution.getInstance().excute(projectApplication.getBaseApplication(), false);
				}
			});

			menuItem.addArmListener(new ArmListener() {
				@Override
				public void widgetArmed(ArmEvent armevent) {
					XmMenuUtil.getInstance().updateStatusBar(projectApplication.getName());
				}
			});

			registry.register((text) -> {
				if (menuItem != null && !menuItem.isDisposed()) {
					menuItem.setText(text);
				}
			}, (message) -> {
				if (menuItem != null && !menuItem.isDisposed()) {
					return XmMenuUtil.getInstance().getLocaleApplicationName(menuItem.getData());
				}
				return CommonConstants.EMPTY_STR;
			});

			registry.register((text) -> {
				if (menuItem != null && !menuItem.isDisposed()) {
					menuItem.setToolTipText(text);
				}
			}, (message) -> {
				if (menuItem != null && !menuItem.isDisposed()) {
					return XmMenuUtil.getInstance().getLocaleApplicationDescription(menuItem.getData());
				}
				return CommonConstants.EMPTY_STR;
			});
		}
		projectAppBtn.setMenu(menu);

		projectAppBtn.addMouseListener(new MouseAdapter() {

			@Override
			public void mouseDown(MouseEvent event) {
				Composite composite = ((Composite) event.widget).getParent();
				if (composite instanceof CustomButton) {
					Menu menu = ((CustomButton) composite).getMenu();
					if (menu != null) {
						menu.setVisible(true);
					}
				}
			}
		});
	}

	/**
	 * Method for Icon view display.
	 *
	 * @param userApplications
	 *            {@link List<UserApplication>}
	 * @param projectApplications
	 *            {@link List<ProjectApplication>}
	 */
	private void iconViewDisplay(final List<UserApplication> userApplications,
			final List<ProjectApplication> projectApplications) {

		this.userAppsToolBarItems = getToolBarItems(CommonConstants.TOOLBAR.USAERAPPS_ID);
		this.projectAppsToolBarItems = getToolBarItems(CommonConstants.TOOLBAR.PROJECTAPPS_ID);

		clearToolItems(userAppsToolBarItems);
		clearToolItems(projectAppsToolBarItems);

		for (UserApplication userApplication : userApplications) {
			createToolItem(userApplication, userAppsToolBarItems);
		}

		for (ProjectApplication projectApplication : projectApplications) {
			createToolItem(projectApplication, projectAppsToolBarItems);
		}
	}

	/**
	 * Method for Menu view display.
	 *
	 * @param userApplications
	 *            {@link List<UserApplication>}
	 * @param projectApplications
	 *            {@link List<ProjectApplication>}
	 */
	private void menuViewDisplay(final List<UserApplication> userApplications,
			final List<ProjectApplication> projectApplications) {

		this.applicationsMenuItems = getMainMenuItems(CommonConstants.MAINMENU.USERMENU_ID);

		clearUserMenuItems(applicationsMenuItems);

		for (UserApplication userApplication : userApplications) {
			createMenuItem(userApplication, applicationsMenuItems);
		}

		if (!userApplications.isEmpty() && !projectApplications.isEmpty()) {
			addSeparatorToMenu(applicationsMenuItems);
		}

		for (ProjectApplication projectApplication : projectApplications) {
			createMenuItem(projectApplication, applicationsMenuItems);
		}

	}

	/**
	 * Method for Adds the separator to menu.
	 *
	 * @param mainMenuItems
	 *            {@link List<MMenuElement>}
	 */
	private void addSeparatorToMenu(final List<MMenuElement> mainMenuItems) {
		Display.getDefault().asyncExec(new Runnable() {

			@Override
			public void run() {
				final MMenuSeparator menuSeparator = modelService.createModelElement(MMenuSeparator.class);
				menuSeparator.setElementId(CommonConstants.MAINMENU.SEPARATOR_ID);
				mainMenuItems.add(menuSeparator);
			}
		});
	}

	/**
	 * Gets the tool bar items.
	 *
	 * @param toolBarId
	 *            {@link String}
	 * @return the tool bar items
	 */
	private List<MToolBarElement> getToolBarItems(final String toolBarId) {
		MTrimBar trim = modelService.getTrim(mTrimmedWindow, SideValue.TOP);
		List<MTrimElement> children = trim.getChildren();
		for (MTrimElement child : children) {
			if (child instanceof MToolBar) {
				MToolBar mToolBar = (MToolBar) child;
				if (toolBarId != null && toolBarId.equals(mToolBar.getElementId())) {
					List<MToolBarElement> toolItems = mToolBar.getChildren();
					mToolBar.setToBeRendered(true);
					mToolBar.setVisible(true);
					return toolItems;
				}
			}
		}
		return null;
	}

	/**
	 * Method for Clear tool items.
	 *
	 * @param toolBarElements
	 *            {@link List<MToolBarElement>}
	 */
	private void clearToolItems(final List<MToolBarElement> toolBarElements) {
		for (MToolBarElement mToolBarElement : toolBarElements) {
			// For some reason removing the element from it's parent doesn't
			// hide the element, so make it invisible
			// Important - change the visibility before removing from the
			// toolbar
			mToolBarElement.setToBeRendered(false);
			mToolBarElement.setVisible(false);
		}
		toolBarElements.clear();
	}

	/**
	 * Method for Clear menu items.
	 *
	 * @param menuElements
	 *            {@link List<MMenuElement>}
	 */
	private void clearUserMenuItems(final List<MMenuElement> menuElements) {
		Display.getDefault().asyncExec(new Runnable() {

			@Override
			public void run() {
				for (MMenuElement menuElement : menuElements) {
					// For some reason removing the element from it's parent
					// doesn't hide the element, so make it invisible
					// Important - change the visibility before removing from
					// the toolbar
					if (!CommonConstants.MAINMENU.XMENU_USERMENU_DUMMY_ID.equals(menuElement.getElementId())) {
						menuElement.setToBeRendered(false);
						menuElement.setVisible(false);
					}
				}
				menuElements.clear();
			}
		});
	}

	/**
	 * Method for Clear admin menu items.
	 *
	 * @param menuElements
	 *            {@link List<MMenuElement>}
	 */
	private void clearAdminMenuItems(final List<MMenuElement> menuElements) {
		Display.getDefault().asyncExec(new Runnable() {

			@Override
			public void run() {
				for (MMenuElement menuElement : menuElements) {
					// For some reason removing the element from it's parent
					// doesn't hide the element, so make it invisible
					// Important - change the visibility before removing from
					// the toolbar
					if (!CommonConstants.MAINMENU.XMENU_ADMINSMENU_SWITCHUSER_ID.equals(menuElement.getElementId())) {
						menuElement.setToBeRendered(false);
						menuElement.setVisible(false);
					}
				}
			}
		});
	}

	/**
	 * Method for Creates the tool item.
	 *
	 * @param application
	 *            {@link Object}
	 * @param toolBarElements
	 *            {@link List<MToolBarElement>}
	 */
	private void createToolItem(final Object application, final List<MToolBarElement> toolBarElements) {
		if (application != null && toolBarElements != null) {
			String iconPath = null;
			String applicationId = null;
			XmMenuUtil instance = XmMenuUtil.getInstance();

			if (application instanceof UserApplication) {
				applicationId = ((UserApplication) application).getUserApplicationId();
				iconPath = instance.getIconPath(((UserApplication) application).getIcon());
			} else if (application instanceof ProjectApplication) {
				applicationId = ((ProjectApplication) application).getProjectApplicationId();
				iconPath = instance.getIconPath(((ProjectApplication) application).getIcon());
			}

			if (!XMSystemUtil.isEmpty(iconPath) && !XMSystemUtil.isEmpty(applicationId)) {
				final MHandledToolItem toolItem = modelService.createModelElement(MHandledToolItem.class);

				/*
				 * final MCommand mCommand = modelService.createModelElement(MCommand.class);
				 * mCommand.setCommandName(applicationId); mCommand.setElementId(applicationId);
				 * mCommand.getTransientData().put("Data", application);
				 * 
				 * final MHandler mHandler = modelService.createModelElement(MHandler.class);
				 * mHandler.setElementId(applicationId); mHandler.setContributionURI(
				 * CommonConstants.XMENU_UI_BUNDLE + "/" +
				 * ExecuteApplicationHandler.class.getCanonicalName());
				 * mHandler.setCommand(mCommand);
				 */

				final MCommand mCommand = getCommand(applicationId);
				mCommand.getTransientData().put("Data", application);
				final MHandler mHandler = getHandler(applicationId);
				mHandler.setCommand(mCommand);

				if (!XMSystemUtil.isEmpty(iconPath)) {
					toolItem.setIconURI("file:///" + iconPath);
				}

				toolItem.setCommand(mCommand);
				toolItem.setToBeRendered(true);
				toolItem.setEnabled(true);

				mApplication.getCommands().add(mCommand);
				mApplication.getHandlers().add(mHandler);

				registry.register((text) -> {
					if (toolItem != null) {
						toolItem.setTooltip(text);
					}
				}, (message) -> {
					return XmMenuUtil.getInstance()
							.getLocaleApplicationName(toolItem.getCommand().getTransientData().get("Data"));
				});

				toolBarElements.add(toolItem);
			}
		}
	}

	/**
	 * Method for Creates the menu item.
	 *
	 * @param application
	 *            {@link Object}
	 * @param menuElements
	 *            {@link List<MMenuElement>}
	 */
	@SuppressWarnings("unused")
	private void createMenuItem(final Object application, final List<MMenuElement> menuElements) {
		Display.getDefault().asyncExec(new Runnable() {

			@Override
			public void run() {
				if (application != null && menuElements != null) {
					String iconPath = null;
					String applicationId = null;
					XmMenuUtil instance = XmMenuUtil.getInstance();
					boolean isParent = false;
					List<UserApplication> childUserApplications = null;
					List<ProjectApplication> childProjectApplications = null;

					if (application instanceof UserApplication) {
						applicationId = ((UserApplication) application).getUserApplicationId();
						iconPath = instance.getIconPath(((UserApplication) application).getIcon());
						childUserApplications = ((UserApplication) application).getChildUserApplications();
						isParent = ((UserApplication) application).isParent();
					} else if (application instanceof ProjectApplication) {
						applicationId = ((ProjectApplication) application).getProjectApplicationId();
						iconPath = instance.getIconPath(((ProjectApplication) application).getIcon());
						childProjectApplications = ((ProjectApplication) application).getChildProjectApplications();
						isParent = ((ProjectApplication) application).isParent();
					}

					if (!XMSystemUtil.isEmpty(applicationId)) {
						if (isParent) {
							final MMenu menu = modelService.createModelElement(MMenu.class);
							menu.setElementId(applicationId);
							

							if (!XMSystemUtil.isEmpty(iconPath)) {
								menu.setIconURI("file:///" + iconPath);
							}

							menu.getTransientData().put("Data", application);
							if (childUserApplications != null && !childUserApplications.isEmpty()) {
								for (UserApplication userApplication : childUserApplications) {
									createMenuItem(userApplication, menu.getChildren());
								}
							} else if (childProjectApplications != null && !childProjectApplications.isEmpty()) {
								for (ProjectApplication projectApplication : childProjectApplications) {
									createMenuItem(projectApplication, menu.getChildren());
								}
							} else {
								final MDynamicMenuContribution mDynamicMenuContribution = modelService
										.createModelElement(MDynamicMenuContribution.class);
								mDynamicMenuContribution.setElementId(applicationId);
								mDynamicMenuContribution.setContributionURI(
										CommonConstants.XMENU_UI_BUNDLE + "/" + DummyMenuItem.class.getCanonicalName());
								menu.getChildren().add(mDynamicMenuContribution);
							}

							registry.register((text) -> {
								if (menu != null) {
									menu.setLabel(text);
								}
							}, (message) -> {
								if (menu != null) {
									return XmMenuUtil.getInstance()
											.getLocaleApplicationName(menu.getTransientData().get("Data"));
								}
								return CommonConstants.EMPTY_STR;
							});

							registry.register((text) -> {
								if (menu != null) {
									menu.setTooltip(text);
								}
							}, (message) -> {
								if (menu != null) {
									String localeDescription = XmMenuUtil.getInstance()
											.getLocaleApplicationDescription(menu.getTransientData().get("Data"));
									if (!XMSystemUtil.isEmpty(localeDescription)) {
										return localeDescription;
									}
									return XmMenuUtil.getInstance()
											.getLocaleApplicationName(menu.getTransientData().get("Data"));
								}
								return CommonConstants.EMPTY_STR;
							});

							menuElements.add(menu);
						} else {
							final MHandledMenuItem menuItem = modelService.createModelElement(MHandledMenuItem.class);

							/*
							 * final MCommand mCommand = modelService.createModelElement(MCommand.class);
							 * mCommand.setCommandName(applicationId); mCommand.setElementId(applicationId);
							 * mCommand.getTransientData().put("Data", application);
							 * 
							 * final MHandler mHandler = modelService.createModelElement(MHandler.class);
							 * mHandler.setElementId(applicationId); mHandler.setContributionURI(
							 * CommonConstants.XMENU_UI_BUNDLE + "/" +
							 * ExecuteApplicationHandler.class.getCanonicalName());
							 * mHandler.setCommand(mCommand);
							 */

							if (!XMSystemUtil.isEmpty(iconPath)) {
								menuItem.setIconURI("file:///" + iconPath);
							}

							final MCommand mCommand = getCommand(applicationId);
							mCommand.getTransientData().put("Data", application);
							final MHandler mHandler = getHandler(applicationId);
							mHandler.setCommand(mCommand);

							menuItem.setCommand(mCommand);
							menuItem.setToBeRendered(true);
							menuItem.setEnabled(true);

							mApplication.getCommands().add(mCommand);
							mApplication.getHandlers().add(mHandler);

							registry.register((text) -> {
								if (menuItem != null) {
									menuItem.setLabel(text);
								}
							}, (message) -> {
								if (menuItem != null) {
									return XmMenuUtil.getInstance().getLocaleApplicationName(
											menuItem.getCommand().getTransientData().get("Data"));
								}
								return CommonConstants.EMPTY_STR;
							});

							registry.register((text) -> {
								if (menuItem != null) {
									menuItem.setTooltip(text);
								}
							}, (message) -> {
								if (menuItem != null) {
									String localeDescription = XmMenuUtil.getInstance().getLocaleApplicationDescription(
											menuItem.getCommand().getTransientData().get("Data"));
									if (!XMSystemUtil.isEmpty(localeDescription)) {
										return localeDescription;
									}
									return XmMenuUtil.getInstance().getLocaleApplicationName(
											menuItem.getCommand().getTransientData().get("Data"));
								}
								return CommonConstants.EMPTY_STR;
							});

							menuElements.add(menuItem);
						}
					}
				}
			}
		});
	}

	/**
	 * Gets the command.
	 *
	 * @param commandId
	 *            {@link String}
	 * @return the command
	 */
	private MCommand getCommand(final String commandId) {
		MCommand mCommand = mApplication.getCommand(commandId);
		if (mCommand == null) {
			mCommand = modelService.createModelElement(MCommand.class);
			mCommand.setCommandName(commandId);
			mCommand.setElementId(commandId);
		}
		return mCommand;
	}

	/**
	 * Gets the handler.
	 *
	 * @param handlerId
	 *            {@link String}
	 * @return the handler
	 */
	private MHandler getHandler(final String handlerId) {
		MHandler mHandler = null;
		List<MHandler> handlers = mApplication.getHandlers();
		for (MHandler handler : handlers) {
			if (handler.getElementId().equals(handlerId)) {
				mHandler = handler;
				break;
			}
		}
		if (mHandler == null) {
			mHandler = modelService.createModelElement(MHandler.class);
			mHandler.setElementId(handlerId);
			mHandler.setContributionURI(
					CommonConstants.XMENU_UI_BUNDLE + "/" + ExecuteApplicationHandler.class.getCanonicalName());
		}
		return mHandler;
	}

	/**
	 * Gets the main menu items.
	 *
	 * @param menuId
	 *            {@link String}
	 * @return the main menu items
	 */
	private List<MMenuElement> getMainMenuItems(final String menuId) {
		List<MMenuElement> children = mTrimmedWindow.getMainMenu().getChildren();
		for (MMenuElement child : children) {
			if (child instanceof MMenu) {
				MMenu menu = (MMenu) child;
				if (menuId != null && menuId.equals(menu.getElementId())) {
					List<MMenuElement> menuItems = menu.getChildren();
					menu.setToBeRendered(true);
					menu.setVisible(true);
					return menuItems;
				}
			}
		}
		return null;
	}

	/**
	 * Checks if is main menu visible.
	 *
	 * @param menuId
	 *            {@link String}
	 * @return true, if is main menu visible
	 */
	private boolean isMainMenuVisible(final String menuId) {
		List<MMenuElement> children = mTrimmedWindow.getMainMenu().getChildren();
		for (MMenuElement child : children) {
			if (child instanceof MMenu) {
				MMenu menu = (MMenu) child;
				if (menuId != null && menuId.equals(menu.getElementId())) {
					return menu.isToBeRendered() && menu.isVisible();
				}
			}
		}
		return false;
	}

	/**
	 * Method for Sets the menu visible.
	 *
	 * @param menuId
	 *            {@link String}
	 * @param isVisible
	 *            {@link boolean}
	 */
	private void setMenuVisible(final String menuId, final boolean isVisible) {
		Display.getDefault().asyncExec(new Runnable() {

			@Override
			public void run() {
				List<MMenuElement> children = mTrimmedWindow.getMainMenu().getChildren();
				for (MMenuElement child : children) {
					if (child instanceof MMenu) {
						MMenu menu = (MMenu) child;
						if (menuId != null && menuId.equals(menu.getElementId())) {
							menu.setToBeRendered(isVisible);
							menu.setVisible(isVisible);
						}
					}
				}
			}
		});
	}

	/**
	 * Method for Load start scripts.
	 *
	 * @param projectId
	 *            {@link String}
	 */
	private void loadStartScripts(final String projectId) {
		if (startApplications != null) {
			startApplications.clear();
		}

		if (!XMSystemUtil.isEmpty(projectId)) {
			String adminAreaId = CaxMenuObjectModelUtil.getInstance().getAdminAreaId(projectId);
			if (!XMSystemUtil.isEmpty(adminAreaId)) {
				startApplications = LoadDataFromService.getInstance().getAllUserStartApplications(adminAreaId,
						projectId);
			}
		}
	}

	/**
	 * Method for Run project start scripts.
	 *
	 * @return the int {@link int}
	 */
	private int runProjectStartScripts() {
		int exitValue = 0;
		XMSystemUtil.setINIT(CommonConstants.STARTSCRIPT.START);
		List<StartApplication> startScripts = startApplications.get(StartApplicationType.SCRIPT);

		for (int startIndex = 0; startIndex < startScripts.size(); startIndex++) {
			StartApplication startScript = startScripts.get(startIndex);
			exitValue = BaseAppExecution.getInstance().excute(startScript.getBaseApplication(), true);

			// Executing scripts in reverse order if any start failure
			if (exitValue != 0) {
				XMSystemUtil.setINIT(CommonConstants.STARTSCRIPT.END);
				for (int endIndex = (startIndex - 1); endIndex >= 0; endIndex--) {
					StartApplication endScript = startScripts.get(endIndex);
					if (BaseAppExecution.getInstance().excute(endScript.getBaseApplication(), true) != 0) {
						return exitValue;
					}
				}
				return exitValue;
			}
		}

		if (exitValue == 0) {
			runProjectStartMessages();
		}

		return exitValue;
	}

	/**
	 * Method for Run project end scripts.
	 *
	 * @return the int {@link int}
	 */
	private int runProjectEndScripts() {
		int exitValue = 0;
		XMSystemUtil.setINIT(CommonConstants.STARTSCRIPT.END);
		List<StartApplication> startScripts = startApplications.get(StartApplicationType.SCRIPT);

		for (int endIndex = (startScripts.size() - 1); endIndex >= 0; endIndex--) {
			StartApplication endScript = startScripts.get(endIndex);
			exitValue = BaseAppExecution.getInstance().excute(endScript.getBaseApplication(), true);
			if (exitValue != 0) {
				XMSystemUtil.setINIT(CommonConstants.STARTSCRIPT.START);
				// for (int startIndex = 0; startIndex < endIndex; startIndex++) {
				for (int startIndex = endIndex + 1; startIndex <= (startScripts.size() - 1); startIndex++) {
					StartApplication startScript = startScripts.get(startIndex);
					if (BaseAppExecution.getInstance().excute(startScript.getBaseApplication(), true) != 0) {
						return exitValue;
					}
				}
				return exitValue;
			}
		}

		return exitValue;
	}

	/**
	 * Method for Run project start messages.
	 */
	private void runProjectStartMessages() {
		List<StartApplication> startMessages = startApplications.get(StartApplicationType.MESSAGE);

		for (StartApplication startMessage : startMessages) {
			Date expiryDate = startMessage.getExpiryDate();
			if ((new Date()).before(expiryDate)) {
				String name = XmMenuUtil.getInstance().getLocaleApplicationName(startMessage);
				// String description =
				// XmMenuUtil.getInstance().getLocaleApplicationDescription(startMessage);
				String remarks = XmMenuUtil.getInstance().getLocaleApplicationRemarks(startMessage);
				if (!XMSystemUtil.isEmpty(name) && !XMSystemUtil.isEmpty(remarks)) {
					Display.getDefault().asyncExec(new Runnable() {

						@Override
						public void run() {
							CustomMessageDialog.openInformation(Display.getCurrent().getActiveShell(), name, remarks);
						}
					});
				}
			}
		}
	}

	/**
	 * Method for Load admins menu applications.
	 */
	private void loadAdminsMenuApplications() {
		AdminMenuApplications adminsMenuApplications = LoadDataFromService.getInstance().getAdminsMenuApplications();

		if (adminsMenuApplications != null && adminsMenuApplications.isAdminMenuUser()) {

			this.adminsMenuItems = getMainMenuItems(CommonConstants.MAINMENU.XMENU_ADMINSMENU_ID);

			clearAdminMenuItems(adminsMenuItems);

			List<UserApplication> adminsMenuUserApplicationsList = adminsMenuApplications.getUserApplicationsList();
			List<ProjectApplication> adminsMenuProjectApplicationsList = adminsMenuApplications
					.getProjectApplicationsList();

			if ((adminsMenuUserApplicationsList != null && !adminsMenuUserApplicationsList.isEmpty())
					|| (adminsMenuProjectApplicationsList != null && !adminsMenuProjectApplicationsList.isEmpty())) {

				addSeparatorToMenu(adminsMenuItems);

				if (adminsMenuUserApplicationsList != null) {
					for (UserApplication userApplication : adminsMenuUserApplicationsList) {
						createMenuItem(userApplication, adminsMenuItems);
					}
				}

				if (adminsMenuUserApplicationsList != null && !adminsMenuUserApplicationsList.isEmpty()
						&& adminsMenuProjectApplicationsList != null && !adminsMenuProjectApplicationsList.isEmpty()) {
					addSeparatorToMenu(adminsMenuItems);
				}

				if (adminsMenuProjectApplicationsList != null) {
					for (ProjectApplication projectApplication : adminsMenuProjectApplicationsList) {
						createMenuItem(projectApplication, adminsMenuItems);
					}
				}
			}
		} else {
			setMenuVisible(CommonConstants.MAINMENU.XMENU_ADMINSMENU_ID, false);
		}
	}

	/**
	 * Method for Reload applications.
	 */
	public void reloadApplications() {
		Job reloadAppJob = new Job("Reloading Applications...") {

			@Override
			protected IStatus run(IProgressMonitor monitor) {
				monitor.beginTask("Reloading Applications...", 100);
				monitor.worked(20);

				setComboEnabled(false);

				clearAppsInPanel();

				refreshProjectList();

				monitor.worked(50);

				String selectedProjectId = getProjectId();
				if (selectedProjectId != null) {
					loadApplications(selectedProjectId);
				}

				loadAdminsMenuApplications();

				monitor.worked(70);

				try {
					Thread.sleep(100);
				} catch (InterruptedException e) {
					LOGGER.error("InterruptedException while reloading applications!" + e); //$NON-NLS-1$
				}

				updateMenuAndToolItemWidgets();

				updatePanel();
				
				updateTopComposite();

				setComboEnabled(true);

				monitor.worked(100);
				return Status.OK_STATUS;
			}
		};
		reloadAppJob.setUser(true);
		reloadAppJob.schedule();
	}

	/**
	 * Checks if is project selected.
	 *
	 * @return true, if is project selected
	 */
	public boolean isProjectSelected() {
		if (!XMSystemUtil.isEmpty(tbComboProjects.getText())) {
			return true;
		}
		return false;
	}

	/**
	 * Gets the user application buttons map.
	 *
	 * @return the user application buttons map
	 */
	public List<UserApplication> getUserApplicationButtonsMap() {
		return (new ArrayList<>(userApplicationsMap.get(ApplicationPosition.BUTTONTASK.name())));
	}

	/**
	 * Gets the project application buttons map.
	 *
	 * @return the project application buttons map
	 */
	public List<ProjectApplication> getProjectApplicationButtonsMap() {
		return (new ArrayList<>(projectApplicationsMap.get(ApplicationPosition.BUTTONTASK.name())));
	}

	/**
	 * Method for Re order applications.
	 *
	 * @param userApplications
	 *            {@link List<UserApplication>}
	 * @param projectApplications
	 *            {@link List<ProjectApplication>}
	 */
	public void reOrderApplications(final List<UserApplication> userApplications,
			final List<ProjectApplication> projectApplications, final String projectId) {

		if (userApplications != null && projectApplications != null
				&& (!userApplications.isEmpty() || !projectApplications.isEmpty())) {
			refreshPart();

			userApplicationsMap.put(ApplicationPosition.BUTTONTASK.name(), userApplications);
			projectApplicationsMap.put(ApplicationPosition.BUTTONTASK.name(), projectApplications);

			buttonViewDisplay(userApplications, projectApplications, projectId);

			updatePanel();
		}
	}

	/**
	 * Method for Clear apps in panel.
	 */
	private void clearAppsInPanel() {
		Display.getDefault().asyncExec(new Runnable() {

			@Override
			public void run() {
				refreshPart();

				userAppsToolBarItems = getToolBarItems(CommonConstants.TOOLBAR.USAERAPPS_ID);
				projectAppsToolBarItems = getToolBarItems(CommonConstants.TOOLBAR.PROJECTAPPS_ID);
				applicationsMenuItems = getMainMenuItems(CommonConstants.MAINMENU.USERMENU_ID);

				clearUserMenuItems(applicationsMenuItems);
				clearToolItems(userAppsToolBarItems);
				clearToolItems(projectAppsToolBarItems);
			}
		});
	}

	/**
	 * Sets the combo enable.
	 *
	 * @param editable
	 *            the new combo enable
	 */
	private void setComboEnabled(final boolean enabled) {
		Display.getDefault().asyncExec(new Runnable() {

			@Override
			public void run() {
				tbComboProjects.setEnabled(enabled);
			}
		});
	}

	/**
	 * Method for Close application.
	 *
	 * @return true, if successful
	 */
	public void closeApplication() {

		final Shell activeShell = Display.getDefault().getActiveShell();
		final XmMenuUtil instance = XmMenuUtil.getInstance();
		if (CustomMessageDialog.openConfirm(activeShell, messages.exitDialogTitle, messages.exitDialogMessage)) {
			/*
			 * String runningAppName = XmMenuUtil.getInstance().getRunningApplication(); if
			 * (!XMSystemUtil.isEmpty(runningAppName)) {
			 * CustomMessageDialog.openError(activeShell, messages.errorDialogTitile,
			 * messages.format(messages.exitColseRunningAppMessage, runningAppName)); } else
			 * {
			 * 
			 * }
			 */

			Job job = new Job("Closing Application") {
				public IStatus run(IProgressMonitor monitor) {

					Display.getDefault().syncExec(new Runnable() {

						@Override
						public void run() {
							XmMenuPartAction.this.getShell().setEnabled(false);
						}
					});

					// monitor.beginTask("Running Project end scripts...", 100);
					monitor.beginTask(messages.closeApplication, 100);
					monitor.worked(10);

					instance.updateMessageConsole(messages.closeApplication);
					instance.updateStatusBarAndMessageConsole(messages.runProjectEndScripts);

					int exitValue = 0;
					final String projectId = getProjectId();
					if (!XMSystemUtil.isEmpty(projectId)) {
						loadStartScripts(projectId);
						exitValue = runProjectEndScripts();
					}

					// monitor.worked(100);
					// monitor.beginTask("Closing Application...", 100);

					monitor.worked(50);
					final boolean[] confirmClose = { false };
					if (exitValue == 0) {
						confirmClose[0] = true;
					} else {
						instance.updateStatusBarAndMessageConsole(messages.endScriptsExecutionFailed);
						Display.getDefault().syncExec(new Runnable() {

							@Override
							public void run() {
								confirmClose[0] = CustomMessageDialog.openConfirm(activeShell, messages.exitDialogTitle,
										messages.exitConfirmScriptExecutionFailMsg);
							}
						});
					}

					if (confirmClose[0]) {
						if (liveMessagejob != null && liveMessagejob.getState() == Job.RUNNING) {
							liveMessagejob.cancel();
						}
						LoadDataFromService loadDataFromService = LoadDataFromService.getInstance();
						loadDataFromService.updateSettingsInUserSettings();
						monitor.worked(70);
						loadDataFromService.updateUserStatusToHistory();
						monitor.worked(100);

						Display.getDefault().syncExec(new Runnable() {

							@Override
							public void run() {
								workbench.close();
							}
						});
					} else {
						instance.updateStatusBarAndMessageConsole(messages.unableToCloseMenu);
						Display.getDefault().syncExec(new Runnable() {

							@Override
							public void run() {
								XmMenuPartAction.this.getShell().setEnabled(true);
							}
						});
						monitor.worked(100);
						monitor.done();
					}

					return Status.OK_STATUS;
				}
			};
			job.setUser(true);
			job.schedule();
		}
	}

	/**
	 * Method for Update menu and tool item widgets.
	 */
	private void updateMenuAndToolItemWidgets() {
		Display.getDefault().syncExec(new Runnable() {

			@Override
			public void run() {
				List<MMenuElement> userMenuItems = getMainMenuItems(CommonConstants.MAINMENU.USERMENU_ID);
				if (userMenuItems != null && userMenuItems.size() > 0) {
					MenuItem obj;
					if ((obj = getMenuWidget(CommonConstants.MAINMENU.USERMENU_ID)) != null) {
						Menu menu = obj.getMenu();
						menu.notifyListeners(SWT.Show, new Event());
						MenuItem[] items = menu.getItems();
						Menu item;
						for (MenuItem menuItem : items) {
							if (menuItem != null && (item = menuItem.getMenu()) != null) {
								item.notifyListeners(SWT.Show, new Event());
							}
						}
					}
					for (MMenuElement menuElement : userMenuItems) {
						updateMainMenuItemWidgets(menuElement);
					}
				}

				if (isMainMenuVisible(CommonConstants.MAINMENU.XMENU_ADMINSMENU_ID)) {
					MenuItem obj;
					if ((obj = getMenuWidget(CommonConstants.MAINMENU.XMENU_ADMINSMENU_ID)) != null) {
						Menu menu = obj.getMenu();
						menu.notifyListeners(SWT.Show, new Event());
						MenuItem[] items = menu.getItems();
						Menu item;
						for (MenuItem menuItem : items) {
							if (menuItem != null && (item = menuItem.getMenu()) != null) {
								item.notifyListeners(SWT.Show, new Event());
							}
						}
					}
					List<MMenuElement> adminMenuItems = getMainMenuItems(CommonConstants.MAINMENU.XMENU_ADMINSMENU_ID);
					if (adminMenuItems != null) {
						for (MMenuElement menuElement : adminMenuItems) {
							updateMainMenuItemWidgets(menuElement);
						}
					}
				}

				List<MToolBarElement> userAppToolBarItems = getToolBarItems(CommonConstants.TOOLBAR.USAERAPPS_ID);
				if (userAppToolBarItems != null) {
					for (MToolBarElement mToolBarElement : userAppToolBarItems) {
						updateToolBarItemWidgets(mToolBarElement);
					}
				}

				List<MToolBarElement> projectAppToolBarItems = getToolBarItems(CommonConstants.TOOLBAR.PROJECTAPPS_ID);
				if (projectAppToolBarItems != null) {
					for (MToolBarElement mToolBarElement : projectAppToolBarItems) {
						updateToolBarItemWidgets(mToolBarElement);
					}
				}
			}
		});
	}

	/**
	 * Method for Update main menu item widgets.
	 *
	 * @param menuElement
	 *            {@link MMenuElement}
	 */
	private void updateMainMenuItemWidgets(final MMenuElement menuElement) {
		if (menuElement instanceof MHandledMenuItem) {
			Object widget = ((MHandledMenuItem) menuElement).getWidget();
			if (widget instanceof MenuItem) {
				Object appObject = ((MHandledMenuItem) menuElement).getCommand().getTransientData().get("Data");
				if (appObject != null && widget != null) {
					updateItemFromModel(appObject, ((MenuItem) widget));
				}
			}
		} else if (menuElement instanceof MMenu) {
			Object widget = menuElement.getWidget();
			if (widget instanceof Menu) {
				Object appObject = menuElement.getTransientData().get("Data");
				MenuItem parentItem = ((Menu) menuElement.getWidget()).getParentItem();
				if (appObject != null && parentItem != null) {
					updateItemFromModel(appObject, parentItem);
				}
			}
			List<MMenuElement> mMenuChildren = ((MMenu) menuElement).getChildren();
			for (MMenuElement childMMenuElement : mMenuChildren) {
				updateMainMenuItemWidgets(childMMenuElement);
			}
		}
	}

	/**
	 * Update item from model.
	 *
	 * @param appObject
	 *            the app object
	 * @param item
	 *            the item
	 */
	void updateItemFromModel(final Object appObject, final MenuItem item) {
		final String[] applicationName = { "" };
		Icon icon = null;
		boolean isActive = true;
		if (appObject instanceof UserApplication) {
			UserApplication userApplication = (UserApplication) appObject;
			applicationName[0] = userApplication.getName();
			icon = userApplication.getIcon();
			isActive = userApplication.isActive();
		} else if (appObject instanceof ProjectApplication) {
			ProjectApplication projectApplication = (ProjectApplication) appObject;
			applicationName[0] = projectApplication.getName();
			icon = projectApplication.getIcon();
			isActive = projectApplication.isActive();
		}

		item.addArmListener(new ArmListener() {
			@Override
			public void widgetArmed(final ArmEvent event) {
				XmMenuUtil.getInstance().updateStatusBar(applicationName[0]);
			}
		});

		if (!isActive) {
			Image image = XmMenuUtil.getInstance().getImage(icon, isActive);
			((MenuItem) item).setImage(image);
		}
	}

	/**
	 * Gets the main menu.
	 *
	 * @param menuId
	 *            the menu id
	 * @return the main menu
	 */
	private MMenu getMainMenu(final String menuId) {
		List<MMenuElement> children = mTrimmedWindow.getMainMenu().getChildren();
		for (MMenuElement child : children) {
			if (child instanceof MMenu) {
				MMenu menu = (MMenu) child;
				if (menuId != null && menuId.equals(menu.getElementId())) {
					return menu;
				}
			}
		}
		return null;
	}

	/**
	 * Gets the menu widget.
	 *
	 * @param menuId
	 *            the menu id
	 * @return the menu widget
	 */
	private MenuItem getMenuWidget(final String menuId) {
		Object firstWidget = null;
		MMenu menu;
		if ((menu = getMainMenu(menuId)) != null) {
			if (menu.getWidget() == null) {
				List<MMenuElement> children = menu.getChildren();
				for (MMenuElement mMenuElement : children) {
					if (mMenuElement.getWidget() != null) {
						firstWidget = mMenuElement.getWidget();
						break;
					}
				}
			} else {
				firstWidget = menu.getWidget();
				if (firstWidget instanceof Menu) {
					return ((Menu) firstWidget).getParentItem();
				}
			}
			if (firstWidget != null) {
				if (firstWidget instanceof MenuItem) {
					return ((MenuItem) firstWidget).getParent().getParentItem();
				}
			}
		}
		return null;
	}

	/**
	 * Method for Update tool bar item widgets.
	 *
	 * @param mToolBarElement
	 *            {@link MToolBarElement}
	 */
	private void updateToolBarItemWidgets(final MToolBarElement mToolBarElement) {
		if (mToolBarElement instanceof MHandledToolItem) {
			String applicationName = "";
			Icon icon = null;
			boolean isActive = true;
			Object appObject = ((MHandledToolItem) mToolBarElement).getCommand().getTransientData().get("Data");
			if (appObject instanceof UserApplication) {
				UserApplication userApplication = (UserApplication) appObject;
				applicationName = userApplication.getName();
				icon = userApplication.getIcon();
				isActive = userApplication.isActive();
			} else if (appObject instanceof ProjectApplication) {
				ProjectApplication projectApplication = (ProjectApplication) appObject;
				applicationName = projectApplication.getName();
				icon = projectApplication.getIcon();
				isActive = projectApplication.isActive();
			}

			Object widget = ((MHandledToolItem) mToolBarElement).getWidget();
			if (widget instanceof ToolItem) {
				((ToolItem) widget).setData(applicationName);

				if (!isActive) {
					Image image = XmMenuUtil.getInstance().getImage(icon, isActive);
					((ToolItem) widget).setImage(image);
				}

				ToolBar toolBar = ((ToolItem) widget).getParent();
				Listener[] listeners = toolBar.getListeners(SWT.MouseMove);
				if (!(listeners != null && listeners.length > 0)) {
					toolBar.addMouseMoveListener(new MouseMoveListener() {

						@Override
						public void mouseMove(MouseEvent mouseevent) {
							Point point = new Point(mouseevent.x, mouseevent.y);
							if(toolBar != null && !toolBar.isDisposed()) {
								ToolItem item = toolBar.getItem(point);
								if(item != null) {
									Object data = item.getData();
									if(data != null) {
										XmMenuUtil.getInstance().updateStatusBar(String.valueOf(data));
									}
								}
							}
						}
					});
				}
			}
		}
	}
}