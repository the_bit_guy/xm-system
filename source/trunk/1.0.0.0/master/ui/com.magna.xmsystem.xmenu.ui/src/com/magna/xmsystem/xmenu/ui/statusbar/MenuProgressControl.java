package com.magna.xmsystem.xmenu.ui.statusbar;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.Objects;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.IJobChangeEvent;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.core.runtime.jobs.JobChangeAdapter;
import org.eclipse.core.runtime.jobs.ProgressProvider;
import org.eclipse.e4.ui.di.UISynchronize;
import org.eclipse.jface.layout.GridDataFactory;
import org.eclipse.jface.layout.GridLayoutFactory;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CLabel;
import org.eclipse.swt.custom.StackLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.ProgressBar;

/**
 * Menu Progress Control class for any jobs running in caxMenu.
 *
 * @author shashwat.anand
 */
public class MenuProgressControl {
	
	/** The sync. */
	private final UISynchronize sync;

	/** The progress bar. */
	private ProgressBar progressBar;
	
	/** The monitor. */
	private XMsystemProgressMonitor monitor;

	/** The parent composite. */
	private Composite parentComposite;

	/** The progress bar label. */
	private Label progressBarLabel;
	
	/** The lbl timer. */
	private CLabel lblTimer;
	
	/** The date time format. */
	private DateTimeFormatter dateTimeFormat;

	private StackLayout stackLayout;

	private Composite progressBarComposite;

	private Composite clockComposite;

	private Composite mainComposite;

	/**
	 * Instantiates a new menu progress control.
	 *
	 * @param sync the sync
	 * @param parent the parent
	 */
	@Inject
	public MenuProgressControl(final UISynchronize sync, final Composite parent) {
		this.sync = Objects.requireNonNull(sync);
		this.parentComposite = parent;
		this.dateTimeFormat = DateTimeFormatter.ofLocalizedTime(FormatStyle.SHORT);
	}

	/**
	 * Creates the controls.
	 *
	 * @param parent the parent
	 */
	@PostConstruct
	public void createControls(final Composite parent) {
		mainComposite = new Composite(parent, SWT.NONE);
		GridLayoutFactory.fillDefaults().numColumns(1).applyTo(mainComposite);
		GridDataFactory.fillDefaults().align(SWT.RIGHT, SWT.FILL).grab(false, true).applyTo(mainComposite);
		
		this.stackLayout = new StackLayout();
		mainComposite.setLayout(stackLayout);
		
		clockControl(mainComposite);		
		progressBarControl(mainComposite);
		
		this.stackLayout.topControl = clockComposite;
	}
	
	private void progressBarControl(final Composite mainComposite) {
		progressBarComposite = new Composite(mainComposite, SWT.NONE);
		GridDataFactory.fillDefaults().align(SWT.RIGHT, SWT.FILL).grab(false, true).applyTo(progressBarComposite);
		GridLayoutFactory.fillDefaults().numColumns(2).equalWidth(false).margins(0, 0).spacing(0, 0).applyTo(progressBarComposite);
		
		this.progressBarLabel = new Label(progressBarComposite, SWT.NONE);
		this.progressBarLabel.setVisible(false);
		
		GridData gridDataForProgressBar = new GridData();
		gridDataForProgressBar.widthHint = 70;
		gridDataForProgressBar.horizontalAlignment = SWT.RIGHT;
		
		this.progressBar = new ProgressBar(progressBarComposite, SWT.SMOOTH);
		this.progressBar.setLayoutData(gridDataForProgressBar);
		this.progressBar.setVisible(false);
		
		this.monitor = new XMsystemProgressMonitor();

		Job.getJobManager().setProgressProvider(new ProgressProvider() {
			@Override
			public IProgressMonitor createMonitor(Job job) {
				if(job.isUser()) {
					return monitor.addJob(job);
				}
				return null;
			}
		});
	}
	
	private void clockControl(final Composite mainComposite) {
		clockComposite = new Composite(mainComposite, SWT.NONE);
		GridDataFactory.fillDefaults().align(SWT.RIGHT, SWT.TOP).grab(true, true).applyTo(clockComposite);
		GridLayoutFactory.fillDefaults().margins(0, 0).spacing(0, 0).applyTo(clockComposite);
		
		this.lblTimer = new CLabel(clockComposite, SWT.BORDER);
		GridDataFactory.fillDefaults().align(SWT.RIGHT, SWT.TOP).grab(true, true).applyTo(this.lblTimer);
		this.lblTimer.setMargins(4, 2, 3, 2);
		LocalDateTime dt = LocalDateTime.now();
		lblTimer.setText(this.dateTimeFormat.format(dt));
		
		Job job = new Job("Timer") {
			
			@Override
			protected IStatus run(IProgressMonitor monitor) {
				try {
					if (lblTimer == null || lblTimer.isDisposed()) {
						return Status.OK_STATUS;
					}
					Display display = lblTimer.getDisplay();
					display.asyncExec(new Runnable() {
						@Override
						public void run() {
							if (lblTimer != null && !lblTimer.isDisposed()) {
								final LocalDateTime dt = LocalDateTime.now();
								lblTimer.setText(dateTimeFormat.format(dt));
							}
						}
					});
				} finally {
					this.schedule(1000);
				}
				return Status.OK_STATUS;
			}
		};
		
		job.schedule(1000);
	}
	
	/**
	 * Gets the progress bar.
	 *
	 * @return the progressBar
	 */
	public ProgressBar getProgressBar() {
		return progressBar;
	}

	/**
	 * Gets the parent composite.
	 *
	 * @return the parentComposite
	 */
	public Composite getParentComposite() {
		return parentComposite;
	}

	/**
	 * The Class XMsystemProgressMonitor.
	 */
	private final class XMsystemProgressMonitor extends NullProgressMonitor {
		
		/** The running tasks. */
		private long runningTasks = 0L;

		/* (non-Javadoc)
		 * @see org.eclipse.core.runtime.NullProgressMonitor#beginTask(java.lang.String, int)
		 */
		@Override
		public void beginTask(final String name, final int totalWork) {
			sync.syncExec(new Runnable() {

				@Override
				public void run() {
					progressBarLabel.setText(name + "  ");
					progressBarLabel.setVisible(true);
					progressBar.setVisible(true);
					
					stackLayout.topControl = progressBarComposite;					
					mainComposite.layout();
					mainComposite.getParent().layout();
					mainComposite.update();
					mainComposite.getParent().update();					
					
					if (runningTasks <= 0) {
						// --- no task is running at the moment ---
						progressBar.setSelection(0);
						progressBar.setMaximum(totalWork);

					} else {
						// --- other tasks are running ---
						progressBar.setMaximum(progressBar.getMaximum() + totalWork);
					}

					runningTasks++;
					progressBar.setToolTipText("Currently running: " + runningTasks + "\nLast task: " + name);
				}
			});
		}

		/**
		 * Adds the job.
		 *
		 * @param job the job
		 * @return the i progress monitor
		 */
		public IProgressMonitor addJob(Job job) {
			if (job != null) {
				job.addJobChangeListener(new JobChangeAdapter() {
					@Override
					public void done(IJobChangeEvent event) {
						sync.syncExec(new Runnable() {

							@Override
							public void run() {
								runningTasks--;
								if (progressBar != null && !progressBar.isDisposed()) {
									if (runningTasks > 0) {
										// --- some tasks are still running ---
										progressBar.setToolTipText("Currently running: " + runningTasks);
									} else {
										// --- all tasks are done (a reset of
										// selection could also be done) ---
										progressBar.setToolTipText("Currently no background progress running.");
										progressBar.setVisible(false);
										progressBarLabel.setText("");
										progressBarLabel.setVisible(false);										
										
										stackLayout.topControl = clockComposite;
										mainComposite.layout();
										mainComposite.getParent().layout();
										mainComposite.update();
										mainComposite.getParent().update();			
									}
								}
							}
						});

						// clean-up
						event.getJob().removeJobChangeListener(this);
					}
				});
			}
			return this;
		}

		/* (non-Javadoc)
		 * @see org.eclipse.core.runtime.NullProgressMonitor#worked(int)
		 */
		@Override
		public void worked(final int work) {
			sync.syncExec(new Runnable() {

				@Override
				public void run() {
					if (progressBar != null && !progressBar.isDisposed()) {
						progressBar.setSelection(progressBar.getSelection() + work);
					}
				}
			});
		}
		
		/* (non-Javadoc)
		 * @see org.eclipse.core.runtime.NullProgressMonitor#done()
		 */
		@Override
		public void done() {
			sync.syncExec(new Runnable() {

				@Override
				public void run() {
					if (progressBar != null && !progressBar.isDisposed()) {
						progressBar.setSelection(0);
					}
				}
			});
		}
	}
}
