package com.magna.xmsystem.xmenu.ui.model;

import java.beans.PropertyChangeEvent;
import java.util.HashMap;
import java.util.Map;

import com.magna.xmsystem.dependencies.utils.LANG_ENUM;

/**
 * The Class User.
 * 
 * @author subash.janarthanan
 * 
 */
public class User extends BeanModel implements ICaxStartMenu{

	/** PROPERTY_USERID constant. */
	public static final String PROPERTY_USERID = "userId"; //$NON-NLS-1$

	/** PROPERTY_USERNAME constant. */
	public static final String PROPERTY_USERNAME = "name"; //$NON-NLS-1$

	/** The Constant PROPERTY_FULLNAME. */
	public static final String PROPERTY_FULLNAME = "fullName"; //$NON-NLS-1$

	/** PROPERTY_ACTIVE constant. */
	public static final String PROPERTY_ACTIVE = "active"; //$NON-NLS-1$

	/** PROPERTY_DESC_MAP constant. */
	public static final String PROPERTY_DESC_MAP = "descriptionMap"; //$NON-NLS-1$

	/** PROPERTY_REMARKS_MAP constant. */
	public static final String PROPERTY_REMARKS_MAP = "remarksMap"; //$NON-NLS-1$

	/** PROPERTY_OPERATION_MODE constant. */
	public static final String PROPERTY_OPERATION_MODE = "operationMode"; //$NON-NLS-1$

	/** PROPERTY_ICON constant. */
	public static final String PROPERTY_ICON = "icon"; //$NON-NLS-1$

	/** The Constant PROPERTY_EMAIL. */
	public static final String PROPERTY_EMAIL = "email";

	/** The Constant PROPERTY_TELEPHONENUM. */
	public static final String PROPERTY_TELEPHONENUM = "telePhoneNum";

	/** The Constant PROPERTY_DEPARTMENT. */
	public static final String PROPERTY_DEPARTMENT = "department";

	/** Member variable for user id. */
	private String userId;

	/** Member variable for user name. */
	private String name;

	/** The full name. */
	private String fullName;

	/** The email. */
	private String email;

	/** The tele phone num. */
	private String telePhoneNum;

	/** The department. */
	private String department;

	/** Member variable to store is user is active. */
	private boolean active;

	/** Member variable for user description. */
	private Map<LANG_ENUM, String> descriptionMap;

	/** Member variable for user remarksMap. */
	private Map<LANG_ENUM, String> remarksMap;

	/** Member variable for icon. */
	private Icon icon;

	/**
	 * Instantiates a new user.
	 *
	 * @param userId
	 *            the user id
	 * @param name
	 *            the name
	 * @param isActive
	 *            the is active
	 * @param email
	 *            the email
	 * @param telePhoneNum
	 *            the tele phone num
	 * @param department
	 *            the department
	 * @param icon
	 *            the icon
	 * @param operationMode
	 *            the operation mode
	 */
	public User(final String userId, final String name, final String fullName, final boolean isActive,
			final String email, final String telePhoneNum, final String department, final Icon icon) {
		this(userId, name, fullName, isActive, email, telePhoneNum, department, new HashMap<>(), new HashMap<>(), icon);
	}

	/**
	 * Instantiates a new user.
	 *
	 * @param userId
	 *            the user id
	 * @param name
	 *            the name
	 * @param isActive
	 *            the is active
	 * @param email
	 *            the email
	 * @param telePhoneNum
	 *            the tele phone num
	 * @param department
	 *            the department
	 * @param descriptionMap
	 *            the description map
	 * @param remarksMap
	 *            the remarks map
	 * @param icon
	 *            the icon
	 * @param operationMode
	 *            the operation mode
	 */
	public User(final String userId, final String name, final String fullName, final boolean isActive,
			final String email, final String telePhoneNum, final String department,
			final Map<LANG_ENUM, String> descriptionMap, final Map<LANG_ENUM, String> remarksMap, final Icon icon) {
		super();
		this.userId = userId;
		this.name = name;
		this.fullName = fullName;
		this.active = isActive;
		this.email = email;
		this.telePhoneNum = telePhoneNum;
		this.department = department;
		this.descriptionMap = descriptionMap;
		this.remarksMap = remarksMap;
		this.icon = icon;
	}

	/**
	 * Gets the userId.
	 *
	 * @return the userId
	 */
	public String getUserId() {
		return userId;
	}

	/**
	 * Sets the userId.
	 *
	 * @param userID
	 *            the new user id
	 */
	public void setUserId(final String userID) {
		this.propertyChangeSupport.firePropertyChange(PROPERTY_USERID, this.userId, this.userId = userID);
	}

	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Sets the name.
	 *
	 * @param name
	 *            the name to set
	 */
	public void setName(final String name) {
		if (name == null) {
			throw new IllegalArgumentException();
		}
		this.propertyChangeSupport.firePropertyChange(PROPERTY_USERNAME, this.name, this.name = name.trim());
	}

	/**
	 * Gets the full name.
	 *
	 * @return the full name
	 */
	public String getFullName() {
		return fullName;
	}

	/**
	 * Sets the full name.
	 *
	 * @param fullName
	 *            the new full name
	 */
	public void setFullName(String fullName) {
		if (fullName == null) {
			throw new IllegalArgumentException();
		}
		this.propertyChangeSupport.firePropertyChange(PROPERTY_FULLNAME, this.fullName,
				this.fullName = fullName.trim());
	}

	/**
	 * Checks if is active.
	 *
	 * @return the isActive
	 */
	public boolean isActive() {
		return active;
	}

	/**
	 * Sets the active.
	 *
	 * @param isActive
	 *            the isActive to set
	 */
	public void setActive(final boolean isActive) {
		this.propertyChangeSupport.firePropertyChange(PROPERTY_ACTIVE, this.active, this.active = isActive);
	}

	/**
	 * Gets the description map.
	 *
	 * @return the description
	 */

	public Map<LANG_ENUM, String> getDescriptionMap() {
		return descriptionMap;
	}

	/**
	 * Sets the description map.
	 *
	 * @param descriptionMap
	 *            the description map
	 */
	public void setDescriptionMap(final Map<LANG_ENUM, String> descriptionMap) {
		if (descriptionMap == null) {
			throw new IllegalArgumentException();
		}
		this.propertyChangeSupport.firePropertyChange(PROPERTY_DESC_MAP, this.descriptionMap,
				this.descriptionMap = descriptionMap);
	}

	/**
	 * Gets the description for given LANG_ENUM.
	 *
	 * @param lang
	 *            {@link LANG_ENUM}
	 * @return {@link String}
	 */
	public String getDescription(final LANG_ENUM lang) {
		return this.descriptionMap.get(lang);
	}

	/**
	 * Sets the description for given LANG_ENUM.
	 *
	 * @param lang
	 *            {@link LANG_ENUM}
	 * @param description
	 *            {@link String}
	 */
	public void setDescription(final LANG_ENUM lang, final String description) {
		if (lang == null || description == null) {
			throw new IllegalArgumentException();
		}
		this.propertyChangeSupport.firePropertyChange(PROPERTY_DESC_MAP, this.descriptionMap,
				this.descriptionMap.put(lang, description));
	}

	/**
	 * Gets the remarks map.
	 *
	 * @return the remarksMap
	 */
	public Map<LANG_ENUM, String> getRemarksMap() {
		return remarksMap;
	}

	/**
	 * Method for Sets the remarks map.
	 *
	 * @param remarksMap
	 *            the remarksMap to set
	 */
	public void setRemarksMap(final Map<LANG_ENUM, String> remarksMap) {
		if (remarksMap == null) {
			throw new IllegalArgumentException();
		}
		this.propertyChangeSupport.firePropertyChange(PROPERTY_REMARKS_MAP, this.remarksMap,
				this.remarksMap = remarksMap);
	}

	/**
	 * Gets the remarks for given LANG_ENUM.
	 *
	 * @param lang
	 *            {@link LANG_ENUM}
	 * @return {@link String}
	 */
	public String getRemarks(final LANG_ENUM lang) {
		return this.remarksMap.get(lang);
	}

	/**
	 * Sets the remarks for given LANG_ENUM.
	 *
	 * @param lang
	 *            {@link LANG_ENUM}
	 * @param remarks
	 *            {@link String}
	 */
	public void setRemarks(final LANG_ENUM lang, final String remarks) {
		if (lang == null || remarks == null) {
			throw new IllegalArgumentException();
		}
		this.propertyChangeSupport.firePropertyChange(PROPERTY_REMARKS_MAP, this.remarksMap,
				this.remarksMap.put(lang, remarks));
	}

	/**
	 * Gets the icon.
	 *
	 * @return the icon
	 */
	public Icon getIcon() {
		return icon;
	}

	/**
	 * Sets the icon.
	 *
	 * @param icon
	 *            the icon to set
	 */
	public void setIcon(final Icon icon) {
		if (icon == null) {
			throw new IllegalArgumentException();
		}
		this.propertyChangeSupport.firePropertyChange(PROPERTY_ICON, this.icon, this.icon = icon);
	}

	/**
	 * Gets the email.
	 *
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * Sets the email.
	 *
	 * @param email
	 *            the new email
	 */
	public void setEmail(String email) {
		if (email == null) {
			throw new IllegalArgumentException();
		}
		this.propertyChangeSupport.firePropertyChange(PROPERTY_EMAIL, this.email, this.email = email);
	}

	/**
	 * Gets the tele phone num.
	 *
	 * @return the tele phone num
	 */
	public String getTelePhoneNum() {
		return telePhoneNum;
	}

	/**
	 * Sets the tele phone num.
	 *
	 * @param telePhoneNum
	 *            the new tele phone num
	 */
	public void setTelePhoneNum(String telePhoneNum) {
		if (telePhoneNum == null) {
			throw new IllegalArgumentException();
		}
		this.propertyChangeSupport.firePropertyChange(PROPERTY_TELEPHONENUM, this.telePhoneNum,
				this.telePhoneNum = telePhoneNum);
	}

	/**
	 * Gets the department.
	 *
	 * @return the department
	 */
	public String getDepartment() {
		return department;
	}

	/**
	 * Sets the department.
	 *
	 * @param department
	 *            the new department
	 */
	public void setDepartment(String department) {
		if (department == null) {
			throw new IllegalArgumentException();
		}
		this.propertyChangeSupport.firePropertyChange(PROPERTY_DEPARTMENT, this.department,
				this.department = department.trim());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.BeanModel#
	 * propertyChange(java.beans.PropertyChangeEvent)
	 */
	@Override
	public void propertyChange(PropertyChangeEvent event) {
		this.propertyChangeSupport.firePropertyChange(event.getPropertyName(), event.getOldValue(),
				event.getNewValue());
	}
}
