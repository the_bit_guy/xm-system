package com.magna.xmsystem.xmenu.ui.celleditor;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.List;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JColorChooser;

import org.eclipse.jface.viewers.CellEditor;

/**
 * The Class ColorChooserButton.
 */
public class ColorChooserButton extends JButton {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The current. */
	private Color current;

	/**
	 * Instantiates a new color chooser button.
	 *
	 * @param c the c
	 */
	public ColorChooserButton(Color c) {
		setSelectedColor(c);
		addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				Color newColor = JColorChooser.showDialog(null, "Choose a color", current);
				setSelectedColor(newColor);
			}
		});
	}

	/**
	 * Gets the selected color.
	 *
	 * @return the selected color
	 */
	public Color getSelectedColor() {
		return current;
	}

	/**
	 * Sets the selected color.
	 *
	 * @param newColor the new selected color
	 */
	public void setSelectedColor(Color newColor) {
		setSelectedColor(newColor, true);
	}

	/**
	 * Sets the selected color.
	 *
	 * @param newColor the new color
	 * @param notify the notify
	 */
	public void setSelectedColor(Color newColor, boolean notify) {

		if (newColor == null)
			return;

		current = newColor;
		setIcon(createIcon(current, 16, 16));
		repaint();

		if (notify) {
			// Notify everybody that may be interested.
			for (ColorChangedListener l : listeners) {
				l.colorChanged(newColor);
			}
		}
	}

	/**
	 * The listener interface for receiving colorChanged events.
	 * The class that is interested in processing a colorChanged
	 * event implements this interface, and the object created
	 * with that class is registered with a component using the
	 * component's <code>addColorChangedListener<code> method. When
	 * the colorChanged event occurs, that object's appropriate
	 * method is invoked.
	 *
	 * @see ColorChangedEvent
	 */
	public static interface ColorChangedListener {
		
		/**
		 * Color changed.
		 *
		 * @param newColor the new color
		 */
		public void colorChanged(Color newColor);

		/**
		 * Color changed.
		 *
		 * @param newColor the new color
		 */
		void colorChanged(CellEditor newColor);
	}

	/** The listeners. */
	private List<ColorChangedListener> listeners = new ArrayList<ColorChangedListener>();

	/**
	 * Adds the color changed listener.
	 *
	 * @param toAdd the to add
	 */
	public void addColorChangedListener(ColorChangedListener toAdd) {
		listeners.add(toAdd);
	}

	/**
	 * Creates the icon.
	 *
	 * @param main the main
	 * @param width the width
	 * @param height the height
	 * @return the image icon
	 */
	public static ImageIcon createIcon(Color main, int width, int height) {
		BufferedImage image = new BufferedImage(width, height, java.awt.image.BufferedImage.TYPE_INT_RGB);
		Graphics2D graphics = image.createGraphics();
		graphics.setColor(main);
		graphics.fillRect(0, 0, width, height);
		graphics.setXORMode(Color.DARK_GRAY);
		graphics.drawRect(0, 0, width - 1, height - 1);
		image.flush();
		ImageIcon icon = new ImageIcon(image);
		return icon;
	}
}
