package com.magna.xmsystem.xmenu.ui.utils;

import org.eclipse.core.runtime.jobs.ISchedulingRule;

/**
 * Class for Job scheduling rule.
 *
 * @author Chiranjeevi.Akula
 */
public class JobSchedulingRule implements ISchedulingRule {

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.core.runtime.jobs.ISchedulingRule#isConflicting(org.eclipse.
	 * core.runtime.jobs.ISchedulingRule)
	 */
	public boolean isConflicting(ISchedulingRule rule) {
		return rule == this;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.core.runtime.jobs.ISchedulingRule#contains(org.eclipse.core.
	 * runtime.jobs.ISchedulingRule)
	 */
	public boolean contains(ISchedulingRule rule) {
		return rule == this;
	}
}