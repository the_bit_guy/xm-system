package com.magna.xmsystem.xmenu.message;

import java.text.MessageFormat;

import javax.annotation.PostConstruct;

/**
 * Class for Message.
 *
 * @author Chiranjeevi.Akula
 */
public class Message {

	/** Member variable 'window title label' for {@link String}. */
	public String windowTitleLabel;

	/** Member variable 'user applications label' for {@link String}. */
	public String userApplicationsLabel;
	
	/** Member variable 'project applications label' for {@link String}. */
	public String projectApplicationsLabel;
	
	/** Member variable 'caxmenu version' for {@link String}. */
	public String caxmenuVersion;
	
	/** Member variable 'project list label' for {@link String}. */
	public String projectListLabel;
	
	/** Member variable 'no base app found title' for {@link String}. */
	public String noBaseAppFoundTitle;
	
	/** Member variable 'no base app found message' for {@link String}. */
	public String noBaseAppFoundMessage;
	
	/** Member variable 'in active base app message' for {@link String}. */
	public String inActiveBaseAppMessage;
	
	/** Member variable 'no script found message' for {@link String}. */
	public String noScriptFoundMessage;
	
	/** Member variable 'script exection' for {@link String}. */
	public String scriptExection;
	
	/** Member variable 'launch application' for {@link String}. */
	public String launchApplication;
	
	/** Member variable 'project switch success' for {@link String}. */
	public String projectSwitchSuccess;
	
	/** Member variable 'project switch failure' for {@link String}. */
	public String projectSwitchFailure;
	
	/** Member variable 'menu started message' for {@link String}. */
	public String menuStartedMessage;
	
	/** Member variable 'project access removed title' for {@link String}. */
	public String projectAccessRemovedTitle;
	
	/** Member variable 'project access removed message' for {@link String}. */
	public String projectAccessRemovedMessage;
	
	/** Member variable 'project request title' for {@link String}. */
	public String projectRequestTitle;
	
	/** Member variable 'project request message' for {@link String}. */
	public String projectRequestMessage;
	
	/** Member variable 'horizontal icon tootip' for {@link String}. */
	public String horizontalIconTootip;
	
	/** Member variable 'vertical icon tooltip' for {@link String}. */
	public String verticalIconTooltip;
	
	/** Member variable 'offline mode title' for {@link String}. */
	public String offlineModeTitle;
	
	/** Member variable 'offline mode message' for {@link String}. */
	public String offlineModeMessage;
	
	/** Member variable 'hot line contact title' for {@link String}. */
	public String hotLineContactTitle;
	
	/** Member variable 'hot line contact no message' for {@link String}. */
	public String hotLineContactNoMessage;
	
	/** Member variable 'hot line contact email message' for {@link String}. */
	public String hotLineContactEmailMessage;
	
	/** Member variable 'object permission dialog title' for {@link String}. */
	public String objectPermissionDialogTitle;
	
	/** Member variable 'admin menu config permission dialog msg' for {@link String}. */
	public String adminMenuConfigPermissionDialogMsg;
	
	public String resetSwitchedUserIconTooltip;
	
	/** Member variable 'unauthorized user access message' for {@link String}. */
	public String unauthorizedUserAccessMessage;
	
	/** Member variable 'server not reachable' for {@link String}. */
	public String serverNotReachable;
	
	/** Member variable 'unable to switch user' for {@link String}. */
	public String switchUserDialogTitle;
	
	/** Member variable 'unable to switch user' for {@link String}. */
	public String unableToSwitchUser;
	
	/** Member variable 'user not selected' for {@link String}. */
	public String userNotSelected;
	
	/** Member variable 'about dialog title' for {@link String}. */
	public String aboutDialogTitle;
	
	/** Member variable 'exit dialog title' for {@link String}. */
	public String exitDialogTitle;
	
	/** Member variable 'exit dialog message' for {@link String}. */
	public String exitDialogMessage;
	
	/** Member variable 'login dialog user name' for {@link String}. */
	public String loginDialogUserName;
	
	/** Member variable 'login dialog password' for {@link String}. */
	public String loginDialogPassword;
	
	/** Member variable 'login dialog login btn' for {@link String}. */
	public String loginDialogLoginBtn;
	
	/** Member variable 'login dialog cancel btn' for {@link String}. */
	public String loginDialogCancelBtn;
	
	/** Member variable 'user list dialog title' for {@link String}. */
	public String userListDialogTitle;
	
	/** Member variable 'user list dialog select user lbl' for {@link String}. */
	public String userListDialogSelectUserLbl;
	
	/** Member variable 'user list dialog select btn' for {@link String}. */
	public String userListDialogSelectBtn;
	
	/** Member variable 'user list dialog cancel btn' for {@link String}. */
	public String userListDialogCancelBtn;
	
	/** Member variable 'user switch error title' for {@link String}. */
	public String userSwitchErrorTitle;
	
	/** Member variable 'user switch error message' for {@link String}. */
	public String userSwitchErrorMessage;
	
	/** The user switch acess denied message. */
	public String userSwitchAcessDeniedMessage;
	
	/** Member variable 'reaarange dialog title' for {@link String}. */
	public String reaarangeDialogTitle;
	
	/** Member variable 'reaarange dialog reset to defaults btn' for {@link String}. */
	public String reaarangeDialogResetToDefaultsBtn;
	
	/** Member variable 'reaarange dialog save order btn' for {@link String}. */
	public String reaarangeDialogSaveOrderBtn;
	
	/** Member variable 'reaarange dialog cancel btn' for {@link String}. */
	public String reaarangeDialogCancelBtn;
	
	/** Member variable 'error dialog titile' for {@link String}. */
	public String errorDialogTitile;
	
	/** Member variable 'exit colse running app message' for {@link String}. */
	public String exitColseRunningAppMessage;
	
	/** Member variable 'switch project colse running app message' for {@link String}. */
	public String switchProjectColseRunningAppMessage;
	
	/** Member variable 'scripts not reachable msg' for {@link String}. */
	public String scriptsNotReachableMsg;
	
	/** Member variable 'close application' for {@link String}. */
	public String closeApplication;
	
	/** Member variable 'run project end scripts' for {@link String}. */
	public String runProjectEndScripts;
	
	/** Member variable 'end scripts execution failed' for {@link String}. */
	public String endScriptsExecutionFailed;
	
	/** Member variable 'unable to close menu' for {@link String}. */
	public String unableToCloseMenu;
	
	/** Member variable 'exit confirm script execution fail msg' for {@link String}. */
	public String exitConfirmScriptExecutionFailMsg;
	
	/** The theme dialog title. */
	public String themeDialogTitle;
	
	/** The column name. */
	public String columnName;
	
	/** The column value. */
	public String columnValue;
	
	/** The column preview. */
	public String columnPreview;
	
	/** The edit settings. */
	public String editSettings;
	
	/** The add theme btn name. */
	public String addThemeBtnName;
	
	/** The delete theme btn name. */
	public String deleteThemeBtnName;
	
	/** The reset to defaults btn. */
	public String resetToDefaultsBtn;
	
	/** The theme dialog cancel btn. */
	public String themeDialogCancelBtn;
	
	/** The theme name. */
	public String themeName;
	
	/** The warning dialog name. */
	public String warningDialogName;
	
	/** The theme delete. */
	public String themeDelete;
	
	/** The css tree viewer column text. */
	public String cssTreeViewerColumnText;
	
	/** The css tree viewer column application. */
	public String cssTreeViewerColumnApplication;
	
	/** The css tree viewer column UAPA. */
	public String cssTreeViewerColumnUAPA;
	
	/** The css tree viewer column project list. */
	public String cssTreeViewerColumnProjectList;
	
	/** The css tree viewer column btn app. */
	public String cssTreeViewerColumnBtnApp;
	
	/** The css tree viewer foreground prop. */
	public String cssTreeViewerForegroundProp;
	
	/** The css tree viewer background prop. */
	public String cssTreeViewerBackgroundProp;
	
	/** The css tree viewer text prop. */
	public String cssTreeViewerTextProp;
	
	/** The css tree viewer font prop. */
	public String cssTreeViewerFontProp;
	
	/** The css tree viewer font weight prop. */
	public String cssTreeViewerFontWeightProp;
	
	/** The css tree viewer font style prop. */
	public String cssTreeViewerFontStyleProp;
	
	/**
	 * Method for Format.
	 */
	@PostConstruct
	public void format() {
		String resourceBundles = "ResourceBundles";
		
		windowTitleLabel = MessageFormat.format(windowTitleLabel, resourceBundles); // $NON-NLS-1$
		projectListLabel =  MessageFormat.format(projectListLabel, resourceBundles); // $NON-NLS-1$
		userApplicationsLabel = MessageFormat.format(userApplicationsLabel, resourceBundles); // $NON-NLS-1$
		projectApplicationsLabel = MessageFormat.format(projectApplicationsLabel, resourceBundles); // $NON-NLS-1$
		caxmenuVersion =  MessageFormat.format(caxmenuVersion, resourceBundles); // $NON-NLS-1$
		
		noBaseAppFoundTitle =  MessageFormat.format(noBaseAppFoundTitle, resourceBundles); // $NON-NLS-1$
		noBaseAppFoundMessage =  MessageFormat.format(noBaseAppFoundMessage, resourceBundles); // $NON-NLS-1$
		
		inActiveBaseAppMessage =  MessageFormat.format(inActiveBaseAppMessage, resourceBundles); // $NON-NLS-1$
		
		noScriptFoundMessage =  MessageFormat.format(noScriptFoundMessage, resourceBundles); // $NON-NLS-1$
		
		scriptExection = MessageFormat.format(scriptExection, resourceBundles); // $NON-NLS-1$
		launchApplication = MessageFormat.format(launchApplication, resourceBundles); // $NON-NLS-1$
		
		projectSwitchSuccess = MessageFormat.format(projectSwitchSuccess, resourceBundles); // $NON-NLS-1$
		projectSwitchFailure = MessageFormat.format(projectSwitchFailure, resourceBundles); // $NON-NLS-1$
		
		menuStartedMessage = MessageFormat.format(menuStartedMessage, resourceBundles); // $NON-NLS-1$
		
		projectAccessRemovedTitle = MessageFormat.format(projectAccessRemovedTitle, resourceBundles); // $NON-NLS-1$
		projectAccessRemovedMessage = MessageFormat.format(projectAccessRemovedMessage, resourceBundles); // $NON-NLS-1$
		
		projectRequestTitle = MessageFormat.format(projectRequestTitle, resourceBundles); // $NON-NLS-1$
		projectRequestMessage = MessageFormat.format(projectRequestMessage, resourceBundles); // $NON-NLS-1$
		
		horizontalIconTootip = MessageFormat.format(horizontalIconTootip, resourceBundles); // $NON-NLS-1$
		verticalIconTooltip = MessageFormat.format(verticalIconTooltip, resourceBundles); // $NON-NLS-1$
		
		offlineModeTitle = MessageFormat.format(offlineModeTitle, resourceBundles); // $NON-NLS-1$
		offlineModeMessage = MessageFormat.format(offlineModeMessage, resourceBundles); // $NON-NLS-1$
		
		hotLineContactTitle = MessageFormat.format(hotLineContactTitle, resourceBundles); // $NON-NLS-1$
		hotLineContactNoMessage = MessageFormat.format(hotLineContactNoMessage, resourceBundles); // $NON-NLS-1$
		hotLineContactEmailMessage = MessageFormat.format(hotLineContactEmailMessage, resourceBundles); // $NON-NLS-1$
		
		objectPermissionDialogTitle = MessageFormat.format(objectPermissionDialogTitle, resourceBundles); // $NON-NLS-1$
		adminMenuConfigPermissionDialogMsg = MessageFormat.format(adminMenuConfigPermissionDialogMsg, resourceBundles); // $NON-NLS-1$
		resetSwitchedUserIconTooltip = MessageFormat.format(resetSwitchedUserIconTooltip, resourceBundles); // $NON-NLS-1$
		
		unauthorizedUserAccessMessage = MessageFormat.format(unauthorizedUserAccessMessage, resourceBundles); // $NON-NLS-1$
		serverNotReachable = MessageFormat.format(serverNotReachable, resourceBundles); // $NON-NLS-1$
		
		switchUserDialogTitle = MessageFormat.format(switchUserDialogTitle, resourceBundles); // $NON-NLS-1$
		unableToSwitchUser = MessageFormat.format(unableToSwitchUser, resourceBundles); // $NON-NLS-1$
		userNotSelected = MessageFormat.format(userNotSelected, resourceBundles); // $NON-NLS-1$
		
		aboutDialogTitle = MessageFormat.format(aboutDialogTitle, resourceBundles); // $NON-NLS-1$
		exitDialogTitle =  MessageFormat.format(exitDialogTitle, resourceBundles); // $NON-NLS-1$
		exitDialogMessage = MessageFormat.format(exitDialogMessage, resourceBundles); // $NON-NLS-1$

		loginDialogUserName = MessageFormat.format(loginDialogUserName, resourceBundles); // $NON-NLS-1$
		loginDialogPassword = MessageFormat.format(loginDialogPassword, resourceBundles); // $NON-NLS-1$
		loginDialogLoginBtn =  MessageFormat.format(loginDialogLoginBtn, resourceBundles); // $NON-NLS-1$
		loginDialogCancelBtn = MessageFormat.format(loginDialogCancelBtn, resourceBundles); // $NON-NLS-1$
		
		userListDialogTitle = MessageFormat.format(userListDialogTitle, resourceBundles); // $NON-NLS-1$
		userListDialogSelectUserLbl = MessageFormat.format(userListDialogSelectUserLbl, resourceBundles); // $NON-NLS-1$
		userListDialogSelectBtn = MessageFormat.format(userListDialogSelectBtn, resourceBundles); // $NON-NLS-1$
		userListDialogCancelBtn = MessageFormat.format(userListDialogCancelBtn, resourceBundles); // $NON-NLS-1$
		userSwitchErrorTitle = MessageFormat.format(userSwitchErrorTitle, resourceBundles); // $NON-NLS-1$
		userSwitchErrorMessage = MessageFormat.format(userSwitchErrorMessage, resourceBundles); // $NON-NLS-1$
		userSwitchAcessDeniedMessage = MessageFormat.format(userSwitchAcessDeniedMessage, resourceBundles); // $NON-NLS-1$
		
		reaarangeDialogTitle = MessageFormat.format(reaarangeDialogTitle, resourceBundles); // $NON-NLS-1$ 
		reaarangeDialogResetToDefaultsBtn = MessageFormat.format(reaarangeDialogResetToDefaultsBtn, resourceBundles); // $NON-NLS-1$ 
		reaarangeDialogSaveOrderBtn = MessageFormat.format(reaarangeDialogSaveOrderBtn, resourceBundles); // $NON-NLS-1$
		reaarangeDialogCancelBtn = MessageFormat.format(reaarangeDialogCancelBtn, resourceBundles); // $NON-NLS-1$
		
		errorDialogTitile = MessageFormat.format(errorDialogTitile, resourceBundles); // $NON-NLS-1$
		//exitColseRunningAppMessage = MessageFormat.format(exitColseRunningAppMessage, resourceBundles); // $NON-NLS-1$
		//switchProjectColseRunningAppMessage = MessageFormat.format(switchProjectColseRunningAppMessage, resourceBundles); // $NON-NLS-1$
		
		scriptsNotReachableMsg = MessageFormat.format(scriptsNotReachableMsg, resourceBundles); // $NON-NLS-1$
		
		closeApplication = MessageFormat.format(closeApplication, resourceBundles); // $NON-NLS-1$
		runProjectEndScripts = MessageFormat.format(runProjectEndScripts, resourceBundles); // $NON-NLS-1$
		endScriptsExecutionFailed = MessageFormat.format(endScriptsExecutionFailed, resourceBundles); // $NON-NLS-1$
		unableToCloseMenu = MessageFormat.format(unableToCloseMenu, resourceBundles); // $NON-NLS-1$
		exitConfirmScriptExecutionFailMsg = MessageFormat.format(exitConfirmScriptExecutionFailMsg, resourceBundles); // $NON-NLS-1$
		
		themeDialogTitle = MessageFormat.format(themeDialogTitle, resourceBundles); // $NON-NLS-1$
		columnName = MessageFormat.format(columnName, resourceBundles); // $NON-NLS-1$
		columnValue = MessageFormat.format(columnValue, resourceBundles); // $NON-NLS-1$
		columnPreview = MessageFormat.format(columnPreview, resourceBundles); // $NON-NLS-1$
		editSettings = MessageFormat.format(editSettings, resourceBundles); // $NON-NLS-1$
		addThemeBtnName = MessageFormat.format(addThemeBtnName, resourceBundles); // $NON-NLS-1$
		deleteThemeBtnName = MessageFormat.format(deleteThemeBtnName, resourceBundles); // $NON-NLS-1$
		resetToDefaultsBtn = MessageFormat.format(resetToDefaultsBtn, resourceBundles); // $NON-NLS-1$
		themeDialogCancelBtn = MessageFormat.format(themeDialogCancelBtn, resourceBundles); // $NON-NLS-1$
		themeName =  MessageFormat.format(themeName, resourceBundles); // $NON-NLS-1$
		warningDialogName = MessageFormat.format(warningDialogName, resourceBundles); // $NON-NLS-1$
		themeDelete = MessageFormat.format(themeDelete, resourceBundles); // $NON-NLS-1$
		cssTreeViewerColumnText = MessageFormat.format(cssTreeViewerColumnText, resourceBundles); // $NON-NLS-1$
		cssTreeViewerColumnApplication = MessageFormat.format(cssTreeViewerColumnApplication, resourceBundles); // $NON-NLS-1$
		cssTreeViewerColumnUAPA = MessageFormat.format(cssTreeViewerColumnUAPA, resourceBundles); // $NON-NLS-1$
		cssTreeViewerColumnProjectList = MessageFormat.format(cssTreeViewerColumnProjectList, resourceBundles); // $NON-NLS-1$
		cssTreeViewerColumnBtnApp = MessageFormat.format(cssTreeViewerColumnBtnApp, resourceBundles); // $NON-NLS-1$
		cssTreeViewerForegroundProp = MessageFormat.format(cssTreeViewerForegroundProp, resourceBundles); // $NON-NLS-1$
		cssTreeViewerBackgroundProp = MessageFormat.format(cssTreeViewerBackgroundProp, resourceBundles); // $NON-NLS-1$
		cssTreeViewerTextProp = MessageFormat.format(cssTreeViewerTextProp, resourceBundles); // $NON-NLS-1$
		cssTreeViewerFontProp = MessageFormat.format(cssTreeViewerFontProp, resourceBundles); // $NON-NLS-1$
		cssTreeViewerFontWeightProp = MessageFormat.format(cssTreeViewerFontWeightProp, resourceBundles); // $NON-NLS-1$
		cssTreeViewerFontStyleProp = MessageFormat.format(cssTreeViewerFontStyleProp, resourceBundles); // $NON-NLS-1$
	}
	
	/**
	 * Method for Format.
	 *
	 * @param pattern {@link String}
	 * @param arguments {@link Object[]}
	 * @return the string {@link String}
	 */
	public String format(String pattern, Object ... arguments) {
		return MessageFormat.format(pattern, arguments);
	}
}
