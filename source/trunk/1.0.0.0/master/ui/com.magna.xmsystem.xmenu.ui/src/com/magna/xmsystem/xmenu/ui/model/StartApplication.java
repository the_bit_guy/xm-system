package com.magna.xmsystem.xmenu.ui.model;

import java.beans.PropertyChangeEvent;
import java.util.Date;
import java.util.Map;

import com.magna.xmsystem.dependencies.utils.LANG_ENUM;

// TODO: Auto-generated Javadoc
/**
 * Class for Start application.
 *
 * @author Chiranjeevi.Akula
 */
public class StartApplication extends BeanModel implements ICaxStartMenu{
	
	/** The Constant PROPERTY_STARTID. */
	public static final String PROPERTY_STARTID = "startApplicationId"; //$NON-NLS-1$

	/** The Constant PROPERTY_NAME. */
	public static final String PROPERTY_NAME = "name"; //$NON-NLS-1$

	/** The Constant PROPERTY_ACTIVE. */
	public static final String PROPERTY_ACTIVE = "active"; //$NON-NLS-1$

	/** The Constant PROPERTY_MESSAGE. */
	public static final String PROPERTY_MESSAGE = "message"; //$NON-NLS-1$

	/** The Constant PROPERTY_EXPIRYDATE. */
	public static final String PROPERTY_EXPIRYDATE = "expiryDate"; //$NON-NLS-1$

	/** The Constant PROPERTY_DESC_MAP. */
	public static final String PROPERTY_DESC_MAP = "descriptionMap"; //$NON-NLS-1$

	/** The Constant PROPERTY_REMARKS_MAP. */
	public static final String PROPERTY_REMARKS_MAP = "remarksMap"; //$NON-NLS-1$

	/** The Constant PROPERTY_ICON. */
	public static final String PROPERTY_ICON = "icon"; //$NON-NLS-1$
	
	/** The Constant NAME_LIMIT. */
	public static final int NAME_LIMIT = 30; // $NON-NLS-1$

	/** The Constant DESC_LIMIT. */
	public static final int DESC_LIMIT = 240; // $NON-NLS-1$

	/** The Constant REMARK_LIMIT. */
	public static final int REMARK_LIMIT = 1500; // $NON-NLS-1$

	/** Member variable 'start application id' for {@link String}. */
	private String startApplicationId;

	/** The name. */
	private String name;

	/** Member variable 'active' for {@link Boolean}. */
	private boolean active;

	/** Member variable 'message' for {@link Boolean}. */
	private boolean message;

	/** Member variable 'expiry date' for {@link Date}. */
	private Date expiryDate;

	/** Member variable 'description map' for {@link Map<LANG_ENUM,String>}. */
	private Map<LANG_ENUM, String> descriptionMap;

	/** Member variable 'remarks map' for {@link Map<LANG_ENUM,String>}. */
	private Map<LANG_ENUM, String> remarksMap;

	/** Member variable 'icon' for {@link Icon}. */
	private Icon icon;

	/** Member variable 'base application' for {@link BaseApplication}. */
	private BaseApplication baseApplication;

	/**
	 * Instantiates a new start application.
	 *
	 * @param startPrgmApplicationId the start prgm application id
	 * @param name the name
	 * @param isActive the is active
	 * @param descriptionMap the description map
	 * @param remarksMap the remarks map
	 * @param icon the icon
	 * @param message the message
	 * @param expiryDate the expiry date
	 * @param baseApplication the base application
	 */
	public StartApplication(final String startPrgmApplicationId, final String name, final boolean isActive, final Map<LANG_ENUM, String> descriptionMap,
			final Map<LANG_ENUM, String> remarksMap, final Icon icon, final boolean message, final Date expiryDate, final BaseApplication baseApplication) {
		super();
		this.startApplicationId = startPrgmApplicationId;
		this.name = name;
		this.active = isActive;
		this.descriptionMap = descriptionMap;
		this.remarksMap = remarksMap;
		this.icon = icon;
		this.message = message;
		this.expiryDate = expiryDate;

		this.baseApplication = baseApplication;
	}

	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	public String getName() {
		return this.name;
	}

	/**
	 * Sets the name.
	 *
	 * @param name the new name
	 */
	public void setName(final String name) {
		if (name == null) throw new IllegalArgumentException();
		this.propertyChangeSupport.firePropertyChange(PROPERTY_NAME, this.name, this.name = name.trim());
	}

	/**
	 * Checks if is active.
	 *
	 * @return true, if is active
	 */
	public boolean isActive() {
		return active;
	}

	/**
	 * Sets the active.
	 *
	 * @param isActive the new active
	 */
	public void setActive(final boolean isActive) {
		this.propertyChangeSupport.firePropertyChange(PROPERTY_ACTIVE, this.active, this.active = isActive);
	}

	/**
	 * Checks if is message.
	 *
	 * @return true, if is message
	 */
	public boolean isMessage() {
		return message;
	}

	/**
	 * Sets the message.
	 *
	 * @param message the new message
	 */
	public void setMessage(boolean message) {
		this.propertyChangeSupport.firePropertyChange(PROPERTY_MESSAGE, this.message, this.message = message);
	}

	/**
	 * Gets the expiry date.
	 *
	 * @return the expiry date
	 */
	public Date getExpiryDate() {
		return expiryDate;
	}

	/**
	 * Sets the expiry date.
	 *
	 * @param expiryDate the new expiry date
	 */
	public void setExpiryDate(final Date expiryDate) {
		this.propertyChangeSupport.firePropertyChange(PROPERTY_EXPIRYDATE, this.expiryDate,
				this.expiryDate = expiryDate);
	}

	/**
	 * Gets the description map.
	 *
	 * @return the description map
	 */
	public Map<LANG_ENUM, String> getDescriptionMap() {
		return descriptionMap;
	}

	/**
	 * Gets the description.
	 *
	 * @param lang {@link LANG_ENUM}
	 * @return the description
	 */
	public String getDescription(final LANG_ENUM lang) {
		return this.descriptionMap.get(lang);
	}

	/**
	 * Method for Sets the description.
	 *
	 * @param lang {@link LANG_ENUM}
	 * @param description {@link String}
	 */
	public void setDescription(final LANG_ENUM lang, final String description) {
		if (lang == null || description == null)
			throw new IllegalArgumentException();
		this.propertyChangeSupport.firePropertyChange(PROPERTY_DESC_MAP, this.descriptionMap,
				this.descriptionMap.put(lang, description.trim()));
	}

	/**
	 * Method for Sets the description map.
	 *
	 * @param descriptionMap {@link Map<LANG_ENUM,String>}
	 */
	public void setDescriptionMap(final Map<LANG_ENUM, String> descriptionMap) {
		if (descriptionMap == null)
			throw new IllegalArgumentException();
		this.propertyChangeSupport.firePropertyChange(PROPERTY_DESC_MAP, this.descriptionMap,
				this.descriptionMap = descriptionMap);
	}

	/**
	 * Gets the remarks map.
	 *
	 * @return the remarks map
	 */
	public Map<LANG_ENUM, String> getRemarksMap() {
		return remarksMap;
	}

	/**
	 * Gets the remarks.
	 *
	 * @param lang {@link LANG_ENUM}
	 * @return the remarks
	 */
	public String getRemarks(final LANG_ENUM lang) {
		return this.remarksMap.get(lang);
	}

	/**
	 * Method for Sets the remarks map.
	 *
	 * @param remarksMap {@link Map<LANG_ENUM,String>}
	 */
	public void setRemarksMap(final Map<LANG_ENUM, String> remarksMap) {
		if (remarksMap == null)
			throw new IllegalArgumentException();
		this.propertyChangeSupport.firePropertyChange(PROPERTY_REMARKS_MAP, this.remarksMap,
				this.remarksMap = remarksMap);
	}

	/**
	 * Method for Sets the remarks.
	 *
	 * @param lang {@link LANG_ENUM}
	 * @param remarks {@link String}
	 */
	public void setRemarks(final LANG_ENUM lang, final String remarks) {
		if (lang == null || remarks == null)
			throw new IllegalArgumentException();
		this.propertyChangeSupport.firePropertyChange(PROPERTY_REMARKS_MAP, this.remarksMap,
				this.remarksMap.put(lang, remarks));
	}

	/**
	 * Gets the start prgm application id.
	 *
	 * @return the start prgm application id
	 */
	public String getStartPrgmApplicationId() {
		return startApplicationId;
	}

	/**
	 * Sets the start prgm application id.
	 *
	 * @param startApplicationId the new start prgm application id
	 */
	public void setStartPrgmApplicationId(String startApplicationId) {
		this.propertyChangeSupport.firePropertyChange(PROPERTY_STARTID, this.startApplicationId,
				this.startApplicationId = startApplicationId);
	}

	/**
	 * Gets the icon.
	 *
	 * @return the icon
	 */
	public Icon getIcon() {
		return icon;
	}

	/**
	 * Gets the base application.
	 *
	 * @return the base application
	 */
	public BaseApplication getBaseApplication() {
		return baseApplication;
	}

	/**
	 * Sets the base application.
	 *
	 * @param baseApplication the new base application
	 */
	public void setBaseApplication(BaseApplication baseApplication) {
		this.baseApplication = baseApplication;
	}

	/**
	 * Sets the icon.
	 *
	 * @param icon the new icon
	 */
	public void setIcon(Icon icon) {
		if (icon == null)
			throw new IllegalArgumentException();
		this.propertyChangeSupport.firePropertyChange(PROPERTY_ICON, this.icon, this.icon = icon);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.BeanModel#
	 * propertyChange(java.beans.PropertyChangeEvent)
	 */
	@Override
	public void propertyChange(PropertyChangeEvent event) {
		this.propertyChangeSupport.firePropertyChange(event.getPropertyName(), event.getOldValue(),
				event.getNewValue());
	}
}
