package com.magna.xmsystem.xmenu.ui.parts;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.util.Arrays;
import java.util.Scanner;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.inject.Inject;

import org.apache.commons.io.FilenameUtils;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.e4.core.services.nls.Translation;
import org.eclipse.swt.widgets.Display;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.magna.xmsystem.dependencies.utils.OSValidator;
import com.magna.xmsystem.dependencies.utils.XMSystemUtil;
import com.magna.xmsystem.dependencies.utils.XmSystemEnvProcess;
import com.magna.xmsystem.ui.controls.widgets.CustomMessageDialog;
import com.magna.xmsystem.xmenu.message.Message;
import com.magna.xmsystem.xmenu.ui.model.BaseApplication;
import com.magna.xmsystem.xmenu.ui.service.LoadDataFromService;
import com.magna.xmsystem.xmenu.ui.utils.CommonConstants;
import com.magna.xmsystem.xmenu.ui.utils.XmMenuUtil;

/**
 * Class for Base app execution.
 *
 * @author Chiranjeevi.Akula
 */
public class BaseAppExecution {

	/** The Constant LOGGER. */
	private static final Logger LOGGER = LoggerFactory.getLogger(BaseAppExecution.class);

	/** Member variable 'this ref' for {@link BaseAppExecution}. */
	private static BaseAppExecution thisRef;

	/** Member variable 'messages' for {@link Message}. */
	@Inject
	@Translation
	transient private Message messages;

	/** Member variable 'process builder' for {@link ProcessBuilder}. */
	private ProcessBuilder processBuilder;

	/**
	 * Constructor for BaseAppExecution Class.
	 */
	public BaseAppExecution() {
		setInstance(this);
	}

	/**
	 * Sets the instance.
	 *
	 * @param thisRef
	 *            the new instance
	 */
	public static void setInstance(BaseAppExecution thisRef) {
		BaseAppExecution.thisRef = thisRef;
	}

	/**
	 * Gets the single instance of BaseAppExecution.
	 *
	 * @return single instance of BaseAppExecution
	 */
	public static BaseAppExecution getInstance() {
		return thisRef;
	}

	/**
	 * Method for Excute.
	 *
	 * @param baseApplication
	 *            {@link BaseApplication}
	 * @param isStartApp
	 *            {@link boolean}
	 * @return the int {@link int}
	 */
	public int excute(BaseApplication baseApplication, final boolean isStartApp) {
		int exitValue = 1;
		try {
			if (baseApplication != null) {
				final String baseAppName = baseApplication.getName();
				
				LoadDataFromService.getInstance().logUserStatus(baseAppName);
				LOGGER.debug("Trying to execute " + baseAppName + " name base application");
				
				boolean active = baseApplication.isActive();
				final XmMenuUtil instance = XmMenuUtil.getInstance();
				if (active) {
					final String program = baseApplication.getProgram();

					String mainScript = "";
					String parameters = "";

					int indexOf = program.indexOf(" ");
					if (indexOf != -1) {
						mainScript = program.substring(0, indexOf);
						parameters = program.substring(indexOf, program.length());
					} else {
						mainScript = program;
					}

					String canonicalScriptPath = "";
					if (mainScript.contains("\\") || mainScript.contains("/")) {
						canonicalScriptPath = mainScript;
					} else {
						final File scriptsLocFile = XMSystemUtil.getXMSystemServerScriptFolder();
						if (scriptsLocFile == null) {
							instance.updateMessageConsole(messages.scriptsNotReachableMsg);
							Display.getDefault().syncExec(new Runnable() {
								@Override
								public void run() {
									CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
											messages.errorDialogTitile, messages.scriptsNotReachableMsg);
								}
							});

							return exitValue;
						}
						canonicalScriptPath = scriptsLocFile + File.separator + mainScript;
					}
					
					String extension = FilenameUtils.getExtension(canonicalScriptPath);
					if (XMSystemUtil.isEmpty(extension)) {
						if (OSValidator.isWindows()) {
							canonicalScriptPath = canonicalScriptPath + CommonConstants.WINDOW_SCRIPT_BAT_EXT;
						} else if (OSValidator.isUnixOrLinux()) {
							if (!(new File(canonicalScriptPath).exists())) {
								String tempScriptPath = canonicalScriptPath + CommonConstants.LINUX_SCRIPT_KSH_EXT;
								if ((new File(tempScriptPath).exists())) {
									canonicalScriptPath = tempScriptPath;
								} else {
									canonicalScriptPath = canonicalScriptPath + CommonConstants.LINUX_SCRIPT_SH_EXT;
								}
							}							
						}
					} else {
						if (OSValidator.isWindows()) {
							String tempScriptPath = canonicalScriptPath + CommonConstants.WINDOW_SCRIPT_BAT_EXT;
							if ((new File(tempScriptPath).exists())) {
								canonicalScriptPath = tempScriptPath;
							}
							/*if(CommonConstants.LINUX_SCRIPT_SH_EXT.equals("." + extension) || CommonConstants.LINUX_SCRIPT_KSH_EXT.equals("." + extension))  {
								pathToScript = pathToScript + CommonConstants.WINDOW_SCRIPT_BAT_EXT;
							}*/
						}
					}

					if ((new File(canonicalScriptPath).exists())) {
						instance.updateStatusBarAndMessageConsole(messages.scriptExection + " " + baseAppName);

						exitValue = scriptExecutionViaProcessBuilder(canonicalScriptPath, parameters, baseAppName, isStartApp);
					} else {
						final String canonicalScriptPath2 = canonicalScriptPath;
						Display.getDefault().syncExec(new Runnable() {
							@Override
							public void run() {
								CustomMessageDialog.openWarning(Display.getCurrent().getActiveShell(), baseAppName,
										messages.noScriptFoundMessage + "\n" + canonicalScriptPath2);
								LOGGER.debug(messages.noScriptFoundMessage  + " " + canonicalScriptPath2);
							}
						});						
					}
				} else {
					Display.getDefault().syncExec(new Runnable() {
						@Override
						public void run() {
							CustomMessageDialog.openWarning(Display.getCurrent().getActiveShell(), baseAppName,
									messages.inActiveBaseAppMessage);
							LOGGER.debug(messages.inActiveBaseAppMessage + " " + baseAppName);
						}
					});		
					
					/*if (isStartApp) {
						exitValue = 0;
					} else {
						CustomMessageDialog.openWarning(Display.getCurrent().getActiveShell(), baseAppName,
								messages.inActiveBaseAppMessage);
					}*/
				}
			} else {
				Display.getDefault().syncExec(new Runnable() {
					@Override
					public void run() {
						CustomMessageDialog.openWarning(Display.getCurrent().getActiveShell(), messages.noBaseAppFoundTitle,
								messages.noBaseAppFoundMessage);
						LOGGER.debug(messages.noBaseAppFoundMessage);
					}
				});		
				
				/*if (isStartApp) {
					exitValue = 0;
				} else {
					CustomMessageDialog.openWarning(Display.getCurrent().getActiveShell(), messages.noBaseAppFoundTitle,
							messages.noBaseAppFoundMessage);
				}*/
			}
		} catch (Exception e) {
			LOGGER.error(e.toString());
			Display.getDefault().syncExec(new Runnable() {
				@Override
				public void run() {
					CustomMessageDialog.openError(Display.getCurrent().getActiveShell(), "Error", e.getLocalizedMessage());
				}
			});			
		}
		return exitValue;
	}

	/**
	 * Method for Script execution via process builder.
	 *
	 * @param canonicalScriptPath
	 *            {@link String}
	 * @param baseAppName
	 *            {@link String}
	 * @return the int {@link int}
	 */
	private int scriptExecutionViaProcessBuilder(final String canonicalScriptPath, final String parameters,
			final String baseAppName, final boolean isStartApp) {
		int exitValue = 1;
		if (!XMSystemUtil.isEmpty(parameters)) {
			if (OSValidator.isWindows()) {
				processBuilder = new ProcessBuilder("cmd", "/c", canonicalScriptPath + " " + parameters);
				// processBuilder = new
				// ProcessBuilder("C:/WINDOWS/system32/cmd.exe", "/C",
				// pathToScript + " " + parameters);
			} else if (OSValidator.isUnixOrLinux()) {
				String[] command = { "sh", canonicalScriptPath };
				String[] params = parameters.trim().split(" ");
				String[] commandWithParams = Stream.concat(Arrays.stream(command), Arrays.stream(params))
						.toArray(String[]::new);
				processBuilder = new ProcessBuilder(commandWithParams);
			}
		} else {
			if (OSValidator.isWindows()) {
				processBuilder = new ProcessBuilder("cmd", "/c", canonicalScriptPath);
			} else if (OSValidator.isUnixOrLinux()) {
				processBuilder = new ProcessBuilder("sh", canonicalScriptPath);
			}
		}

		// processBuilder.redirectOutput(Redirect.INHERIT);
		processBuilder.environment().putAll(XmSystemEnvProcess.getInstance().getEnvironmentMap());

		if (isStartApp) {
			exitValue = startProcessAndWait(baseAppName, isStartApp);
		} else {
			Job job = new Job("Executing...") {

				@Override
				protected IStatus run(IProgressMonitor monitor) {
					startProcessAndWait(baseAppName, isStartApp);
					return Status.OK_STATUS;
				}
			};
			job.setUser(true);
			job.schedule();
		}
		return exitValue;
	}

	/**
	 * Method for Start.
	 *
	 * @param baseAppName
	 *            {@link String}
	 * @return the int {@link int}
	 */
	private int startProcessAndWait(final String baseAppName, final boolean isStartApp) {
		int exitValue = 1;
		if (processBuilder != null) {
			try {
				Process process = processBuilder.start();
				inheriteIO(process.getInputStream(), System.out);
				// XmMenuUtil.getInstance().addRunningProcess(process,
				// baseAppName);
				process.waitFor();
				// exitValue = process.exitValue();
				// if (exitValue != 0) {
				InputStream errorStream = process.getErrorStream();
				if (errorStream != null) {
					final String errorMsg = new BufferedReader(new InputStreamReader(errorStream)).lines()
							.collect(Collectors.joining("\n"));

					if (!XMSystemUtil.isEmpty(errorMsg)) {
						LOGGER.error(baseAppName + " : " + errorMsg);
						if (isStartApp) {
							Display.getDefault().syncExec(new Runnable() {
								@Override
								public void run() {
									CustomMessageDialog.openError(Display.getDefault().getActiveShell(), baseAppName,
											errorMsg);
								}
							});
						}
					} else {
						exitValue = process.exitValue();
					}
				}
				// }
			} catch (Exception exception) {
				LOGGER.error(baseAppName + " : " + exception);
				Display.getDefault().syncExec(new Runnable() {
					@Override
					public void run() {
						CustomMessageDialog.openError(Display.getDefault().getActiveShell(), "Info",
								exception.getLocalizedMessage());
					}
				});
			}
		}
		return exitValue;
	}
	
	/**
	 * Method for Start process.
	 *
	 * @param baseAppName {@link String}
	 */
	/*private void startProcess(final String baseAppName) {
		if (processBuilder != null) {
			try {
				Process process = processBuilder.start();
				inheriteIO(process.getInputStream(), System.out);
				
				InputStream errorStream = process.getErrorStream();
				if (errorStream != null) {
					final String errorMsg = new BufferedReader(new InputStreamReader(errorStream)).lines()
							.collect(Collectors.joining("\n"));
					if(!XMSystemUtil.isEmpty(errorMsg)) {
						LOGGER.error(baseAppName + " : " + errorMsg);
						Display.getDefault().asyncExec(new Runnable() {
							@Override
							public void run() {
								CustomMessageDialog.openError(Display.getDefault().getActiveShell(), baseAppName, errorMsg);
							}
						});
					}					
				}
			} catch (Exception exception) {
				LOGGER.error(baseAppName + " : " + exception);
				Display.getDefault().asyncExec(new Runnable() {
					@Override
					public void run() {
						CustomMessageDialog.openError(Display.getDefault().getActiveShell(), "Info",
								exception.getLocalizedMessage());
					}
				});
			}
		}
	}*/
	
	/**
	 * Method for Inherite IO.
	 *
	 * @param src {@link InputStream}
	 * @param dest {@link PrintStream}
	 */
	private void inheriteIO(final InputStream src, final PrintStream dest) {
		new Thread(new Runnable() {
			@Override
			public void run() {
				Scanner scanner = new Scanner(src);
				while (scanner.hasNextLine()) {
					dest.println(scanner.nextLine());
				}
				scanner.close();
			}
		}).start();
	}
}
