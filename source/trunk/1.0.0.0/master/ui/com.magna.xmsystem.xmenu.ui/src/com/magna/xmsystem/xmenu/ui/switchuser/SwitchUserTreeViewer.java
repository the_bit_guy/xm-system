package com.magna.xmsystem.xmenu.ui.switchuser;

import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Item;
import org.eclipse.swt.widgets.Tree;
import org.eclipse.swt.widgets.TreeItem;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Class for Switch user tree viewer.
 *
 * @author Chiranjeevi.Akula
 */
public class SwitchUserTreeViewer extends TreeViewer {

	/** The Constant LOGGER. */
	private static final Logger LOGGER = LoggerFactory.getLogger(SwitchUserTreeViewer.class);

	/** Member variable 'yellow' for {@link Color}. */
	final private Color yellow;

	/** Member variable 'white' for {@link Color}. */
	final private Color white;

	/**
	 * Constructor for SwitchUserTreeViewer Class.
	 *
	 * @param parent {@link Composite}
	 */
	public SwitchUserTreeViewer(final Composite parent) {
		super(parent, SWT.FULL_SELECTION | SWT.V_SCROLL | SWT.BORDER | SWT.VIRTUAL);
		this.init();
		final Display display = parent.getDisplay();
		this.yellow = new Color(display, 255, 255, 235);
		this.white = new Color(display, 255, 255, 255);
		parent.addDisposeListener(new DisposeListener() {
			/**
			 * overriding widget disposed method
			 */
			@Override
			public void widgetDisposed(final DisposeEvent event) {
				SwitchUserTreeViewer.this.yellow.dispose();
				SwitchUserTreeViewer.this.white.dispose();
			}
		});
		
		this.setLabelProvider(new SwitchUserTreeLableProvider());
		this.setContentProvider(new SwitchUserLazyContentProvider(this));
		
		this.setUseHashlookup(true);
	}

	/**
	 * Method for Inits the.
	 */
	private void init() {
		try {
			this.setAutoExpandLevel(30);
			final Tree tree = this.getTree();
			tree.setLinesVisible(true);
			tree.setHeaderVisible(false);
		} catch (Exception e) {
			LOGGER.error("Execption ocuured at creating the UI", e); //$NON-NLS-1$
		}
	}

	/* (non-Javadoc)
	 * @see org.eclipse.jface.viewers.AbstractTreeViewer#doUpdateItem(org.eclipse.swt.widgets.Item, java.lang.Object)
	 */
	@Override
	protected void doUpdateItem(final Item item, final Object element) {
		super.doUpdateItem(item, element);
		this.setColor((TreeItem) item);
	}

	/**
	 * Sets the color.
	 *
	 * @param item the new color
	 */
	private void setColor(final TreeItem item) {
		final Color color = this.yellow;
		if ((item.getParentItem() == null ? item.getParent().indexOf(item) : item.getParentItem().indexOf(item))
				% 2 == 0) {
			item.setBackground(color);
		} else {
			item.setBackground(this.white);
		}
	}

}
