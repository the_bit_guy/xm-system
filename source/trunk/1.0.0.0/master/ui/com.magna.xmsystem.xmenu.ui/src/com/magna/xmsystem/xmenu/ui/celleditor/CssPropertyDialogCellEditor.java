package com.magna.xmsystem.xmenu.ui.celleditor;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.viewers.DialogCellEditor;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.FontData;
import org.eclipse.swt.graphics.RGB;
import org.eclipse.swt.widgets.ColorDialog;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;

import com.magna.xmsystem.xmenu.ui.dialogs.CustomFontDialog;
import com.steadystate.css.dom.CSSValueImpl;
import com.steadystate.css.dom.Property;

/**
 * The Class CssPropertyDialogCellEditor.
 */
public class CssPropertyDialogCellEditor extends DialogCellEditor {

	/** The tree viewer. */
	private TreeViewer treeViewer;
	
	/** The font pixel. */
	private String fontPixel;
	
	/** The newfont. */
	private String newfont;

	/**
	 * Instantiates a new css property dialog cell editor.
	 *
	 * @param treeViewer the tree viewer
	 */
	public CssPropertyDialogCellEditor(TreeViewer treeViewer) {
		super(treeViewer.getTree());
		this.treeViewer = treeViewer;
	}

	/* (non-Javadoc)
	 * @see org.eclipse.jface.viewers.DialogCellEditor#openDialogBox(org.eclipse.swt.widgets.Control)
	 */
	@Override
	protected Object openDialogBox(Control cellEditorWindow) {
		IStructuredSelection iStructuredSelection = (IStructuredSelection) treeViewer.getSelection();
		Object element = iStructuredSelection.getFirstElement();
		if (element instanceof Property) {
			Property property = (Property) element;

			String key = property.getName();
			if (key.equals("font")) {
				configureFont(property);
			} else if (key.equals("color") || key.equals("background-color") || key.equals("header-background-color")
					|| key.equals("foreground-color") || key.equals("label-background-color")
					|| key.equals("label-background-color") || key.equals("label-foreground-color")) {
				{
					configureColor(property);
				}
			}
			treeViewer.refresh();
		}
		return null;
	}

	/**
	 * Configure color.
	 *
	 * @param property the property
	 */
	private void configureColor(Property property) {
		String cssText = property.getValue().getCssText();
		RGB oldRGB = null;
		if (cssText.startsWith("rgb")) {
			String rgbString = cssText.replace("rgb(", "").replace(")", "");
			String[] split = rgbString.split(",");

			int oldRed = Integer.parseInt(split[0].trim());
			int oldGreen = Integer.parseInt(split[1].trim());
			int oldBlue = Integer.parseInt(split[2].trim());

			oldRGB = new RGB(oldRed, oldGreen, oldBlue);
		}
		ColorDialog colorDialog = new ColorDialog(Display.getDefault().getActiveShell());

		if (cssText != null)
			colorDialog.setRGB(oldRGB);
		RGB rgb = colorDialog.open();

		if (rgb == null) {
			return;
		}
		int red = rgb.red;
		int green = rgb.green;
		int blue = rgb.blue;

		String rbgColor = "rgb(" + red + "," + green + "," + blue + ")";
		CSSValueImpl val = new CSSValueImpl();
		val.setValue(rbgColor);
		property.setValue(val);
	}

	/**
	 * Configure font.
	 *
	 * @param property the property
	 */
	private void configureFont(Property property) {
		String cssText = property.getValue().getCssText();
		Pattern p = Pattern.compile("([0-9])");
		String font = "";
		int fontSize = 0;
		String fontStyle = "";
		boolean fnt = true;
		boolean sty = false;

		String[] splitString = cssText.split(" ");
		for (String string : splitString) {
			Matcher m = p.matcher(string);
			boolean b = m.find();
			if (b) {
				String replace = string.replace("px", "");
				fontSize = Integer.parseInt(replace);
				fnt = false;
				sty = true;
			} else if (fnt) {
				font += string + " ";
			} else if (sty) {
				fontStyle = string;
			}
		}

		CustomFontDialog cusFD = new CustomFontDialog(Display.getDefault().getActiveShell(), SWT.NONE, font, fontSize,
				fontStyle);
		int open = cusFD.open();

		if (open == IDialogConstants.OK_ID) {
			FontData fontData = cusFD.getFontData();
			String fontName = fontData.getName();
			//Integer fontStyl = fontData.getStyle();
			/*switch (fontStyl) {
			case SWT.NONE:
				fontStyle = "Normal";
				break;
			case SWT.ITALIC:
				fontStyle = "Italic";
				break;
			case SWT.BOLD:
				fontStyle = "Bold";
				break;
			default:
				break;
			}*/

			if (fontData.getHeight() > 10) {
				fontPixel = "10" + "px";
			} else {
				fontPixel = fontData.getHeight() + "px";
			}

			newfont = "\"" +fontName + "\"" + " " + fontPixel + " " + "Bold";
		}
		
		if (open == IDialogConstants.CANCEL_ID) {
			
			newfont = font + " " + Integer.toString(fontSize) +"px" + " " + "Bold";
			
		}

		CSSValueImpl val = new CSSValueImpl();
		val.setValue(newfont);
		property.setValue(val);
	}

}
