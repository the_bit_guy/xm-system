package com.magna.xmsystem.xmhotline.ui.parts;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.regex.Pattern;

import javax.inject.Inject;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.e4.core.services.events.IEventBroker;
import org.eclipse.e4.core.services.nls.Translation;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.ScrolledComposite;
import org.eclipse.swt.events.MouseAdapter;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.magna.xmbackend.entities.AdminAreaProjectRelTbl;
import com.magna.xmbackend.entities.AdminAreasTbl;
import com.magna.xmbackend.entities.UserProjAppRelTbl;
import com.magna.xmbackend.entities.UserProjectRelTbl;
import com.magna.xmbackend.entities.UsersTbl;
import com.magna.xmbackend.exception.CannotCreateObjectException;
import com.magna.xmbackend.response.rel.useruserapp.UserUserAppRelResponseWrapper;
import com.magna.xmbackend.response.rel.useruserapp.UserUserAppRelation;
import com.magna.xmbackend.vo.adminArea.AdminAreaResponse;
import com.magna.xmbackend.vo.enums.ApplicationRelationType;
import com.magna.xmbackend.vo.enums.UserRelationType;
import com.magna.xmbackend.vo.rel.UserProjectAppRelBatchRequest;
import com.magna.xmbackend.vo.rel.UserProjectAppRelBatchResponse;
import com.magna.xmbackend.vo.rel.UserProjectAppRelRequest;
import com.magna.xmbackend.vo.rel.UserProjectAppRelResponse;
import com.magna.xmbackend.vo.rel.UserProjectRelBatchRequest;
import com.magna.xmbackend.vo.rel.UserProjectRelBatchResponse;
import com.magna.xmbackend.vo.rel.UserProjectRelRequest;
import com.magna.xmbackend.vo.rel.UserUserAppRelBatchRequest;
import com.magna.xmbackend.vo.rel.UserUserAppRelBatchResponse;
import com.magna.xmbackend.vo.rel.UserUserAppRelRequest;
import com.magna.xmsystem.dependencies.customExceptions.UnauthorizedAccessException;
import com.magna.xmsystem.dependencies.utils.XMSystemUtil;
import com.magna.xmsystem.ui.controls.widgets.CustomMessageDialog;
import com.magna.xmsystem.ui.controls.widgets.MagnaCustomCombo;
import com.magna.xmsystem.xmadmin.restclient.relation.AdminAreaProjectRelController;
import com.magna.xmsystem.xmadmin.restclient.relation.UserProjectAppRelController;
import com.magna.xmsystem.xmadmin.restclient.relation.UserProjectRelController;
import com.magna.xmsystem.xmadmin.restclient.relation.UserUserAppRelController;
import com.magna.xmsystem.xmadmin.restclient.validation.ValidationController;
import com.magna.xmsystem.xmhotline.model.AdminAreaModel;
import com.magna.xmsystem.xmhotline.model.MainModel;
import com.magna.xmsystem.xmhotline.model.ProjectApplicationModel;
import com.magna.xmsystem.xmhotline.model.ProjectModel;
import com.magna.xmsystem.xmhotline.model.SiteModel;
import com.magna.xmsystem.xmhotline.model.UserApplicationModel;
import com.magna.xmsystem.xmhotline.model.UserModel;
import com.magna.xmsystem.xmhotline.model.interfaces.IXmHotlineModel;
import com.magna.xmsystem.xmhotline.ui.message.Message;
import com.magna.xmsystem.xmhotline.ui.message.MessageRegistry;
import com.magna.xmsystem.xmhotline.util.Constants;
import com.magna.xmsystem.xmhotline.util.CurrentSelectionModel;
import com.magna.xmsystem.xmhotline.util.XmHotlineUtil;

/**
 * The Class HotLineCompositeAction.
 */
public class HotLineCompositeAction extends HotLineCompositeUI {

	/** The Constant LOGGER. */
	private static final Logger LOGGER = LoggerFactory.getLogger(HotLineCompositeAction.class);

	/** The current selection model. */
	private CurrentSelectionModel currentSelectionModel;

	/** The project list by site id. */
	private List<IXmHotlineModel> projectListBySiteId;

	/** The project list by AA id. */
	private List<IXmHotlineModel> projectListByAAId;

	/** The project list by user id. */
	private List<IXmHotlineModel> projectListByUserId;

	/** The user appl list. */
	private List<IXmHotlineModel> userApplList;

	/** The project appl list. */
	private List<IXmHotlineModel> projectApplList;

	/** The label composites. */
	private CustomLabelComposite[] labelComposites;

	/** The event broker. */
	@Inject
	private IEventBroker eventBroker;
	
	/** The messages. */
	@Inject
	@Translation
	protected Message messages;
	
	/** The registry. */
	@Inject
	private MessageRegistry registry;
	
	/** The admin area map by site. */
	private Map<String, IXmHotlineModel> adminAreaMapBySite = new HashMap<String, IXmHotlineModel>();

	/**
	 * Instantiates a new hot line composite action.
	 *
	 * @param parent the parent
	 * @param style the style
	 * @param registry the registry
	 * @param eventBroker the event broker
	 * @param messages the messages
	 */
	@Inject
	public HotLineCompositeAction(final Composite parent, final MessageRegistry registry) {
		super(parent, SWT.NONE , registry);
		initListeners();

		siteComboListener();
		adminAreaComboListener();
		userComboListener();
		projectComboListener();

		saveAction();
		resetAction();
		this.currentSelectionModel = new CurrentSelectionModel();
	}

	/**
	 * Reset action.
	 */
	private void resetAction() {
		resetButton.addListener(SWT.Selection, new Listener() {

			@Override
			public void handleEvent(Event event) {
				final Shell activeShell = Display.getDefault().getActiveShell();
				final MainModel mainModel = xmHotlineHelper.getMainModel();
				if (!isModified(mainModel)) {
					CustomMessageDialog.openInformation(activeShell, messages.windowtitle, messages.notModified);
					return;
				}
				final boolean answer = CustomMessageDialog.openQuestion(activeShell, messages.windowtitle,
						messages.cancelMsg);
				if (answer) {
					resetProjectUserRelation(mainModel);
					resetUserApplications(mainModel);
					resetProjectApplications(mainModel);
				}
			}
		});
	}

	/**
	 * Checks if is modified.
	 *
	 * @param mainModel the main model
	 * @return true, if is modified
	 */
	private boolean isModified(final MainModel mainModel) {
		return !mainModel.getProjectUserRelationAddList().isEmpty()
				|| !mainModel.getProjectUserRelationDeleteList().isEmpty()
				|| !mainModel.getUserAppUserRelationAddList().isEmpty()
				|| !mainModel.getUserAppUserRelationDeleteList().isEmpty()
				|| !mainModel.getProjectAppUserRelationAddList().isEmpty()
				|| !mainModel.getProjectAppUserRelationDeleteList().isEmpty();
	}

	/**
	 * Reset project user relation.
	 *
	 * @param mainModel the main model
	 */
	private void resetProjectUserRelation(final MainModel mainModel) {
		final List<ProjectModel> projectUserRelationAddList = mainModel.getProjectUserRelationAddList();
		final List<ProjectModel> projectUserRelationDeleteList = mainModel.getProjectUserRelationDeleteList();
		Control[] children = projectComp.getChildren();
		for (Control control : children) {
			CustomLabelComposite customLabelComposite = (CustomLabelComposite) control;
			if (projectUserRelationAddList.contains(customLabelComposite.getModel())) {
				customLabelComposite.setSelection(false);
			} else if (projectUserRelationDeleteList.contains(customLabelComposite.getModel())) {
				customLabelComposite.setSelection(true);
			}
		}
		projectUserRelationAddList.clear();
		projectUserRelationDeleteList.clear();
	}
	
	/**
	 * Reset project user add relation.
	 *
	 * @param mainModel the main model
	 */
	private void resetProjectUserAddRelation(final MainModel mainModel) {
		final List<ProjectModel> projectUserRelationAddList = mainModel.getProjectUserRelationAddList();
		Control[] children = projectComp.getChildren();
		for (Control control : children) {
			CustomLabelComposite customLabelComposite = (CustomLabelComposite) control;
			if (projectUserRelationAddList.contains(customLabelComposite.getModel())) {
				customLabelComposite.setSelection(false);
			}
		}
		projectUserRelationAddList.clear();
	}

	/**
	 * Reset user applications.
	 *
	 * @param mainModel the main model
	 */
	private void resetUserApplications(final MainModel mainModel) {
		List<UserApplicationModel> userAppUserRelationAddList = mainModel.getUserAppUserRelationAddList();
		List<UserApplicationModel> userAppUserRelationDeleteList = mainModel.getUserAppUserRelationDeleteList();
		Control[] fixedUserAppChildren = fixedUserAppComo.getChildren();
		resetWidgets(userAppUserRelationAddList, userAppUserRelationDeleteList, fixedUserAppChildren);
		Control[] notFixedUserAppChildren = notFixedUserAppComp.getChildren();
		resetWidgets(userAppUserRelationAddList, userAppUserRelationDeleteList, notFixedUserAppChildren);
		Control[] protectedUserAppChildren = protectedUserAppComp.getChildren();
		resetWidgets(userAppUserRelationAddList, userAppUserRelationDeleteList, protectedUserAppChildren);
		userAppUserRelationAddList.clear();
		userAppUserRelationDeleteList.clear();
	}
	
	/**
	 * Reset add user applications.
	 *
	 * @param mainModel the main model
	 */
	private void resetAddUserApplications(final MainModel mainModel) {
		List<UserApplicationModel> userAppUserRelationAddList = mainModel.getUserAppUserRelationAddList();
		Control[] fixedUserAppChildren = fixedUserAppComo.getChildren();
		resetWidgets(userAppUserRelationAddList, fixedUserAppChildren);
		Control[] notFixedUserAppChildren = notFixedUserAppComp.getChildren();
		resetWidgets(userAppUserRelationAddList, notFixedUserAppChildren);
		Control[] protectedUserAppChildren = protectedUserAppComp.getChildren();
		resetWidgets(userAppUserRelationAddList, protectedUserAppChildren);
		userAppUserRelationAddList.clear();
	}
	
	/**
	 * Reset delete user applications.
	 *
	 * @param mainModel the main model
	 */
	private void resetDeleteUserApplications(final MainModel mainModel) {
		List<UserApplicationModel> userAppUserRelationDeleteList = mainModel.getUserAppUserRelationDeleteList();
		Control[] fixedUserAppChildren = fixedUserAppComo.getChildren();
		delResetWidgets(userAppUserRelationDeleteList, fixedUserAppChildren);
		Control[] notFixedUserAppChildren = notFixedUserAppComp.getChildren();
		delResetWidgets(userAppUserRelationDeleteList, notFixedUserAppChildren);
		Control[] protectedUserAppChildren = protectedUserAppComp.getChildren();
		delResetWidgets(userAppUserRelationDeleteList, protectedUserAppChildren);
		userAppUserRelationDeleteList.clear();
	}
	
	/**
	 * Reset delete project applications.
	 *
	 * @param mainModel the main model
	 */
	private void resetDeleteProjectApplications(final MainModel mainModel) {
		final List<ProjectApplicationModel> projectAppUserRelationDeleteList = mainModel
				.getProjectAppUserRelationDeleteList();
		Control[] fixedProjectAppChildren = fixedProjectAppComp.getChildren();
		delResetWidgets(projectAppUserRelationDeleteList, fixedProjectAppChildren);
		Control[] notFixedProjectAppChildren = notFixedProjectAppComp.getChildren();
		delResetWidgets(projectAppUserRelationDeleteList, notFixedProjectAppChildren);
		Control[] protectedProjectAppChildren = protectedProjectAppComp.getChildren();
		delResetWidgets(projectAppUserRelationDeleteList, protectedProjectAppChildren);
		projectAppUserRelationDeleteList.clear();
	}

	/**
	 * Reset project applications.
	 *
	 * @param mainModel the main model
	 */
	private void resetProjectApplications(final MainModel mainModel) {
		final List<ProjectApplicationModel> projectAppUserRelationAddList = mainModel
				.getProjectAppUserRelationAddList();
		final List<ProjectApplicationModel> projectAppUserRelationDeleteList = mainModel
				.getProjectAppUserRelationDeleteList();
		Control[] fixedProjectAppChildren = fixedProjectAppComp.getChildren();
		resetWidgets(projectAppUserRelationAddList, projectAppUserRelationDeleteList, fixedProjectAppChildren);
		Control[] notFixedProjectAppChildren = notFixedProjectAppComp.getChildren();
		resetWidgets(projectAppUserRelationAddList, projectAppUserRelationDeleteList, notFixedProjectAppChildren);
		Control[] protectedProjectAppChildren = protectedProjectAppComp.getChildren();
		resetWidgets(projectAppUserRelationAddList, projectAppUserRelationDeleteList, protectedProjectAppChildren);
		projectAppUserRelationAddList.clear();
		projectAppUserRelationDeleteList.clear();
	}
	
	/**
	 * Reset add project applications.
	 *
	 * @param mainModel the main model
	 */
	private void resetAddProjectApplications(final MainModel mainModel) {
		final List<ProjectApplicationModel> projectAppUserRelationAddList = mainModel
				.getProjectAppUserRelationAddList();
		Control[] fixedProjectAppChildren = fixedProjectAppComp.getChildren();
		resetWidgets(projectAppUserRelationAddList, fixedProjectAppChildren);
		Control[] notFixedProjectAppChildren = notFixedProjectAppComp.getChildren();
		resetWidgets(projectAppUserRelationAddList, notFixedProjectAppChildren);
		Control[] protectedProjectAppChildren = protectedProjectAppComp.getChildren();
		resetWidgets(projectAppUserRelationAddList, protectedProjectAppChildren);
		projectAppUserRelationAddList.clear();
	}

	/**
	 * Reset widgets.
	 *
	 * @param addList the add list
	 * @param deleteList the delete list
	 * @param children the children
	 */
	private void resetWidgets(List<?> addList, List<?> deleteList, Control[] children) {
		for (Control control : children) {
			CustomLabelComposite customLabelComposite = (CustomLabelComposite) control;
			if (addList.contains(customLabelComposite.getModel())) {
				customLabelComposite.setSelection(false);
			} else if (deleteList.contains(customLabelComposite.getModel())) {
				customLabelComposite.setSelection(true);
			}
		}
	}
	
	/**
	 * Reset widgets.
	 *
	 * @param addList the add list
	 * @param children the children
	 */
	private void resetWidgets(List<?> addList, Control[] children) {
		for (Control control : children) {
			CustomLabelComposite customLabelComposite = (CustomLabelComposite) control;
			if (addList.contains(customLabelComposite.getModel())) {
				customLabelComposite.setSelection(false);
			}
		}
	}
	
	private void delResetWidgets(List<?> delList, Control[] children) {
		for (Control control : children) {
			CustomLabelComposite customLabelComposite = (CustomLabelComposite) control;
			if (delList.contains(customLabelComposite.getModel())) {
				customLabelComposite.setSelection(true);
			}
		}
	}

	/**
	 * Save action.
	 */
	private void saveAction() {
		saveButton.addListener(SWT.Selection, new Listener() {
			
			@Override
			public void handleEvent(final Event event) {
				final Shell activeShell = Display.getDefault().getActiveShell();
				final MainModel mainModel = xmHotlineHelper.getMainModel();
				if (!isModified(mainModel)) {
					CustomMessageDialog.openInformation(activeShell, messages.windowtitle, messages.notModified);
					return;
				}
				/*if (!isUserAllowed()) {
					CustomMessageDialog.openInformation(activeShell, messages.windowtitle, messages.savePermissionMsg);
					return;
				}*/
				String selectedSiteId = currentSelectionModel.getCurrentSiteId();
				String selectedAdminAreaId = currentSelectionModel.getCurrentAdminAreaId();
				String userId = currentSelectionModel.getCurrentUserId();
				String userName = currentSelectionModel.getCurrentUserName();
				String siteAdminAreaRelId = xmHotlineHelper.getSiteAdminAreaRelId(selectedAdminAreaId);
				if (!XMSystemUtil.isEmpty(selectedSiteId) && !XMSystemUtil.isEmpty(userId)) {
					final boolean answer = CustomMessageDialog.openQuestion(activeShell, messages.windowtitle, xmHotlineHelper.makeQuestionStr(userName, messages) + messages.saveInfo);
					if (answer) {
						try {
							StringBuilder res = new StringBuilder();
							res.append(addProjectUserRelation(mainModel, userId));
							res.append(deleteProjectUserRelation(mainModel));
							res.append(addUserAppAdminAreaRelation(mainModel, siteAdminAreaRelId, selectedAdminAreaId));
							res.append(deleteUserAppAdminAreaRelation(mainModel, siteAdminAreaRelId, selectedAdminAreaId));
							String selectedProjectId = currentSelectionModel.getCurrentProjectId();
							String userProjectRelId, adminAreaProjectRelId;
							if ((userProjectRelId = getCurrentProjectUserRelation(userId, selectedProjectId)) != null
									&& (adminAreaProjectRelId = getCurrentAdminAreaProjectRelation(siteAdminAreaRelId, selectedProjectId)) != null) {
								res.append(addProAppUserAdminAreaProjectRelation(mainModel, userProjectRelId, adminAreaProjectRelId, selectedAdminAreaId));
								res.append(deleteProAppUserAdminAreaProjectRelation(mainModel, userProjectRelId, adminAreaProjectRelId, selectedAdminAreaId));
							}
							if (XMSystemUtil.isEmpty(res.toString().trim())) {
								CustomMessageDialog.openInformation(activeShell, messages.windowtitle, messages.relationCreated);
							} else {
								CustomMessageDialog.openError(activeShell, messages.windowtitle, res.toString().trim());
							}
							updateProjectCombo();
							// reload();
						} catch (final Exception e) {
							CustomMessageDialog.openError(activeShell, messages.windowtitle, messages.saveFailMsg + e);
						}
					}
				}
			}
		});
	}

	/**
	 * Init listeners.
	 */
	private void initListeners() {
		try {
			this.adminAreaFilterCombo.getArrowControl().addSelectionListener(new SelectionAdapter() {

				@Override
				public void widgetSelected(SelectionEvent selectionEvent) {

					if (adminAreaFilterCombo.isDropped()) {
						Job job = new Job("Loading AdminArea...") {

							@Override
							protected IStatus run(IProgressMonitor monitor) {
								monitor.beginTask("Loading AdminArea..", 100);
								monitor.worked(30);

								Display.getDefault().asyncExec(new Runnable() {
									@Override
									public void run() {
										long updateAAandProjectStart = System.currentTimeMillis();
										xmHotlineHelper.updateAdminAreaFromService();
										xmHotlineHelper.updateProjectsFromService();
										long updateAAandProjectEnd = System.currentTimeMillis();
										LOGGER.debug("Service call: Time taken for updating AA and project: " + (updateAAandProjectEnd - updateAAandProjectStart) + "ms");
										
										adminAreaFilterCombo.getTable().setRedraw(false);
										
										long updateAAComboStart = System.currentTimeMillis();
										updateAdminAreaCombo(Display.getDefault().getActiveShell(), currentSelectionModel.getCurrentSiteId());
										long updateAAComboEnd = System.currentTimeMillis();
										LOGGER.debug("Service call: Time taken for updating AA combo and map: " + (updateAAComboEnd - updateAAComboStart) + "ms");
										
										long clearComboStart = System.currentTimeMillis();
										String adminAreaSelected = adminAreaFilterCombo.getText();
										if (!XMSystemUtil.isEmpty(adminAreaSelected) && xmHotlineHelper.getAdminAreaMap().get(adminAreaSelected) == null) {
											setAAComboEmpty();
											clearUserAppComp();
											clearProjectAppComp();
										}
										
										String adminAreaFilterText = currentSelectionModel.getAdminAreaFilterText();
										if (!XMSystemUtil.isEmpty(adminAreaFilterText)) {
											filterCombo(adminAreaFilterText, xmHotlineHelper.getAdminAreaComboItems(), adminAreaFilterCombo);
										}
										adminAreaFilterCombo.updateSize();
										adminAreaFilterCombo.getTable().setRedraw(true);
										long clearComboEnd = System.currentTimeMillis();
										LOGGER.debug("UI Operation: Time taken for clearing, updating and filtering AA combo: " + (clearComboEnd - clearComboStart) + "ms");
									}
								});
								monitor.worked(70);
								return Status.OK_STATUS;
							}
						};
						job.setUser(true);
						job.schedule();
					}
				}
			});

			this.adminAreaFilterCombo.addMouseListener(new MouseAdapter() {

				@Override
				public void mouseDown(MouseEvent mouseEvent) {
					Job job = new Job("Loading AdminArea...") {
						@Override
						protected IStatus run(IProgressMonitor monitor) {
							monitor.beginTask("Loading AdminArea..", 100);
							monitor.worked(30);

							Display.getDefault().asyncExec(new Runnable() {
								@Override
								public void run() {
									long updateAAandProjectStart = System.currentTimeMillis();
									xmHotlineHelper.updateAdminAreaFromService();
									xmHotlineHelper.updateProjectsFromService();
									long updateAAandProjectEnd = System.currentTimeMillis();
									LOGGER.debug("Service call: Time taken for updating AA and project: " + (updateAAandProjectEnd - updateAAandProjectStart) + "ms");
									adminAreaFilterCombo.getTable().setRedraw(false);
									
									long updateAAComboStart = System.currentTimeMillis();
									updateAdminAreaCombo(Display.getDefault().getActiveShell(), currentSelectionModel.getCurrentSiteId());
									long updateAAComboEnd = System.currentTimeMillis();
									LOGGER.debug("Service call: Time taken for updating AA combo and map: " + (updateAAComboEnd - updateAAComboStart) + "ms");
									
									long clearComboStart = System.currentTimeMillis();
									String adminAreaSelected = adminAreaFilterCombo.getText();
									if (!XMSystemUtil.isEmpty(adminAreaSelected) && xmHotlineHelper.getAdminAreaMap().get(adminAreaSelected) == null) {
										setAAComboEmpty();
										clearUserAppComp();
										clearProjectAppComp();
									}
									
									String adminAreaFilterText = currentSelectionModel.getAdminAreaFilterText();
									if (!XMSystemUtil.isEmpty(adminAreaFilterText)) {
										filterCombo(adminAreaFilterText, xmHotlineHelper.getAdminAreaComboItems(), adminAreaFilterCombo);
									}
									adminAreaFilterCombo.updateSize();
									adminAreaFilterCombo.getTable().setRedraw(true);
									long clearComboEnd = System.currentTimeMillis();
									LOGGER.debug("UI Operation: Time taken for clearing, updating and filtering AA combo: " + (clearComboEnd - clearComboStart) + "ms");
								}
							});
							monitor.worked(70);
							return Status.OK_STATUS;
						}
					};
					job.setUser(true);
					job.schedule();
				}
			});

			this.siteFilterCombo.getArrowControl().addSelectionListener(new SelectionAdapter() {

				@Override
				public void widgetSelected(SelectionEvent selectionEvent) {

					if (siteFilterCombo.isDropped()) {
						Job job = new Job("Loading Sites...") {

							@Override
							protected IStatus run(IProgressMonitor monitor) {
								monitor.beginTask("Loading Sites..", 100);
								monitor.worked(30);

								Display.getDefault().asyncExec(new Runnable() {
									@Override
									public void run() {
										long updateSiteandProjectStart = System.currentTimeMillis();
										xmHotlineHelper.updateSiteFromService();
										xmHotlineHelper.updateProjectsFromService();
										long updateSiteandProjectEnd = System.currentTimeMillis();
										LOGGER.debug("Service call: Time taken for updating site and project: " + (updateSiteandProjectEnd - updateSiteandProjectStart) + "ms");
										
										long loadSiteComboStart = System.currentTimeMillis();
										siteFilterCombo.getTable().setRedraw(false);
										xmHotlineHelper.setSitesToCombo(siteFilterCombo);
										String siteSelected = siteFilterCombo.getText();
										if (!XMSystemUtil.isEmpty(siteSelected) && xmHotlineHelper.getSiteMap().get(siteSelected) == null) {
											clearAllSelectionAndUpdateModel();
										}
										String siteFilterText = currentSelectionModel.getSiteFilterText();
										if (!XMSystemUtil.isEmpty(siteFilterText)) {
											currentSelectionModel.setSiteFilterText(siteFilterText);
											filterCombo(siteFilterText, xmHotlineHelper.getSiteComboItems(), siteFilterCombo);
										}	
										siteFilterCombo.updateSize();
										siteFilterCombo.getTable().setRedraw(true);
										long loadSiteComboEnd = System.currentTimeMillis();
										LOGGER.debug("UI Operation: Time taken for updating, filtering and clearing other combo selections: " + (loadSiteComboEnd - loadSiteComboStart) + "ms");
									}
								});
								monitor.worked(70);
								return Status.OK_STATUS;
							}
						};
						job.setUser(true);
						job.schedule();
					}
				}
			});

			this.siteFilterCombo.addMouseListener(new MouseAdapter() {

				@Override
				public void mouseDown(MouseEvent mouseEvent) {
					Job job = new Job("Loading Sites...") {
						@Override
						protected IStatus run(IProgressMonitor monitor) {
							monitor.beginTask("Loading Sites..", 100);
							monitor.worked(30);

							Display.getDefault().asyncExec(new Runnable() {
								@Override
								public void run() {
									long updateSiteandProjectStart = System.currentTimeMillis();
									xmHotlineHelper.updateSiteFromService();
									xmHotlineHelper.updateProjectsFromService();
									long updateSiteandProjectEnd = System.currentTimeMillis();
									LOGGER.debug("Service call: Time taken for updating site and project: " + (updateSiteandProjectEnd - updateSiteandProjectStart) + "ms");
									
									long loadSiteComboStart = System.currentTimeMillis();
									siteFilterCombo.getTable().setRedraw(false);
									xmHotlineHelper.setSitesToCombo(siteFilterCombo);
									String siteSelected = siteFilterCombo.getText();
									if (!XMSystemUtil.isEmpty(siteSelected) && xmHotlineHelper.getSiteMap().get(siteSelected) == null) {
										clearAllSelectionAndUpdateModel();
									}
									String siteFilterText = currentSelectionModel.getSiteFilterText();
									if (!XMSystemUtil.isEmpty(siteFilterText)) {
										currentSelectionModel.setSiteFilterText(siteFilterText);
										filterCombo(siteFilterText, xmHotlineHelper.getSiteComboItems(), siteFilterCombo);
									}	
									siteFilterCombo.updateSize();
									siteFilterCombo.getTable().setRedraw(true);
									long loadSiteComboEnd = System.currentTimeMillis();
									LOGGER.debug("UI Operation: Time taken for updating, filtering and clearing other combo selections: " + (loadSiteComboEnd - loadSiteComboStart) + "ms");
								}
							});
							monitor.worked(70);
							return Status.OK_STATUS;
						}
					};
					job.setUser(true);
					job.schedule();
				}
			});

			this.userFilterCombo.getArrowControl().addSelectionListener(new SelectionAdapter() {

				@Override
				public void widgetSelected(SelectionEvent selectionEvent) {

					if (userFilterCombo.isDropped()) {
						Job job = new Job("Loading Users...") {

							@Override
							protected IStatus run(IProgressMonitor monitor) {
								monitor.beginTask("Loading Users..", 100);
								monitor.worked(30);
								
								Display.getDefault().asyncExec(new Runnable() {
									@Override
									public void run() {
										userFilterCombo.getTable().setRedraw(false);
										long updateUserListStart = System.currentTimeMillis();
										List<UsersTbl> updateUserMapFromService = xmHotlineHelper.updateUserMapFromService();
										final List<IXmHotlineModel> userList = xmHotlineHelper.getAllUsersListByUserTbl(updateUserMapFromService);
										long updateUserListEnd = System.currentTimeMillis();
										LOGGER.debug("Service call: Time taken for updating user: " + (updateUserListEnd - updateUserListStart) + "ms");
										
										long loaduserToComboStart = System.currentTimeMillis();
										xmHotlineHelper.setUserToCombo(userFilterCombo, userList);
										String userFilterText = currentSelectionModel.getUserFilterText();
										if (!XMSystemUtil.isEmpty(userFilterText)) {
											filterCombo(userFilterText, xmHotlineHelper.getUserComboItems(), userFilterCombo);
										}
										
										String userSelected = userFilterCombo.getText();
										if (!XMSystemUtil.isEmpty(userSelected) && xmHotlineHelper.getUsersMap().get(userSelected) == null) {
											setUserComboEmpty();
											clearUserAppComp();
										}
										userFilterCombo.updateSize();
										userFilterCombo.getTable().setRedraw(true);
										long loaduserToComboEnd = System.currentTimeMillis();
										LOGGER.debug("UI Operation: Time taken for updating, filtering user and clearing other combo selections: " + (loaduserToComboEnd - loaduserToComboStart) + "ms");
									}
								});
								monitor.worked(70);
								return Status.OK_STATUS;
							}
						};
						job.setUser(true);
						job.schedule();
					}
				}
			});

			this.userFilterCombo.addMouseListener(new MouseAdapter() {

				@Override
				public void mouseDown(MouseEvent mouseEvent) {
					Job job = new Job("Loading Users...") {
						@Override
						protected IStatus run(IProgressMonitor monitor) {
							monitor.beginTask("Loading Users..", 100);
							monitor.worked(30);
							
							Display.getDefault().asyncExec(new Runnable() {
								@Override
								public void run() {
									userFilterCombo.getTable().setRedraw(false);
									long updateUserListStart = System.currentTimeMillis();
									List<UsersTbl> updateUserMapFromService = xmHotlineHelper.updateUserMapFromService();
									final List<IXmHotlineModel> userList = xmHotlineHelper.getAllUsersListByUserTbl(updateUserMapFromService);
									long updateUserListEnd = System.currentTimeMillis();
									LOGGER.debug("Service call: Time taken for updating user: " + (updateUserListEnd - updateUserListStart) + "ms");
									
									long loaduserToComboStart = System.currentTimeMillis();
									xmHotlineHelper.setUserToCombo(userFilterCombo, userList);
									String userFilterText = currentSelectionModel.getUserFilterText();
									if (!XMSystemUtil.isEmpty(userFilterText)) {
										filterCombo(userFilterText, xmHotlineHelper.getUserComboItems(), userFilterCombo);
									}
									
									String userSelected = userFilterCombo.getText();
									if (!XMSystemUtil.isEmpty(userSelected) && xmHotlineHelper.getUsersMap().get(userSelected) == null) {
										setUserComboEmpty();
										clearUserAppComp();
									}
									userFilterCombo.updateSize();
									userFilterCombo.getTable().setRedraw(true);
									long loaduserToComboEnd = System.currentTimeMillis();
									LOGGER.debug("UI Operation: Time taken for updating, filtering user and clearing other combo selections: " + (loaduserToComboEnd - loaduserToComboStart) + "ms");
								}
							});
							monitor.worked(70);
							return Status.OK_STATUS;
						}
					};
					job.setUser(true);
					job.schedule();
				}
			});

			this.projectFilterCombo.getArrowControl().addSelectionListener(new SelectionAdapter() {

				@Override
				public void widgetSelected(SelectionEvent selectionEvent) {

					if (projectFilterCombo.isDropped()) {
						Job job = new Job("Loading Projects...") {

							@Override
							protected IStatus run(IProgressMonitor monitor) {
								monitor.beginTask("Loading Projects..", 100);
								monitor.worked(30);

								Display.getDefault().asyncExec(new Runnable() {
									@Override
									public void run() {
										long updateProjectListStart = System.currentTimeMillis();
										xmHotlineHelper.updateProjectsFromService();
										long updateProjectListEnd = System.currentTimeMillis();
										LOGGER.debug("Service call: Time taken for updating project: " + (updateProjectListEnd - updateProjectListStart) + "ms");
										
										String currentProjectId = currentSelectionModel.getCurrentProjectId();
										String currentUserId = currentSelectionModel.getCurrentUserId();
										
										long relationExistCheckStart = System.currentTimeMillis();
										boolean projectUserRelationExist = xmHotlineHelper.isProjectUserRelationExist(currentProjectId, currentUserId);
										long relationExistCheckEnd = System.currentTimeMillis();
										LOGGER.debug("Service call: Time taken for checking the user project relation: " + (relationExistCheckEnd - relationExistCheckStart) + "ms");
										
										long loadProjectComboStart = System.currentTimeMillis();
										projectFilterCombo.getTable().setRedraw(false);
										String projectSelected = projectFilterCombo.getText();
										updateProjectCombo();
										if (!XMSystemUtil.isEmpty(projectSelected) && xmHotlineHelper.getProjectMap().get(projectSelected) == null) {
											clearProjectAndUpdateModel();
										} else if (!projectUserRelationExist && currentProjectId != null
												&& currentUserId != null) {
											clearProjectAndUpdateModel();
										}
										
										String projectFilterText = currentSelectionModel.getProjectFilterText();
										if (!XMSystemUtil.isEmpty(projectFilterText)) {
											filterCombo(projectFilterText, xmHotlineHelper.getProjectComboItems(), projectFilterCombo);
										}
										projectFilterCombo.updateSize();
										projectFilterCombo.getTable().setRedraw(true);
										long loadProjectComboEnd = System.currentTimeMillis();
										LOGGER.debug("UI Operation: Time taken for updating, filtering project and clearing other required combo: " + (loadProjectComboEnd - loadProjectComboStart) + "ms");
									}
								});
								monitor.worked(70);
								return Status.OK_STATUS;
							}
						};
						job.setUser(true);
						job.schedule();
					}
				}
			});

			this.projectFilterCombo.addMouseListener(new MouseAdapter() {

				@Override
				public void mouseDown(MouseEvent mouseEvent) {
					Job job = new Job("Loading Projects...") {
						@Override
						protected IStatus run(IProgressMonitor monitor) {
							monitor.beginTask("Loading Projects..", 100);
							monitor.worked(30);

							Display.getDefault().asyncExec(new Runnable() {
								@Override
								public void run() {
									long updateProjectListStart = System.currentTimeMillis();
									xmHotlineHelper.updateProjectsFromService();
									long updateProjectListEnd = System.currentTimeMillis();
									LOGGER.debug("Service call: Time taken for updating project: " + (updateProjectListEnd - updateProjectListStart) + "ms");
									
									String currentProjectId = currentSelectionModel.getCurrentProjectId();
									String currentUserId = currentSelectionModel.getCurrentUserId();
									
									long relationExistCheckStart = System.currentTimeMillis();
									boolean projectUserRelationExist = xmHotlineHelper.isProjectUserRelationExist(currentProjectId, currentUserId);
									long relationExistCheckEnd = System.currentTimeMillis();
									LOGGER.debug("Service call: Time taken for checking the user project relation: " + (relationExistCheckEnd - relationExistCheckStart) + "ms");
									
									long loadProjectComboStart = System.currentTimeMillis();
									projectFilterCombo.getTable().setRedraw(false);
									String projectSelected = projectFilterCombo.getText();
									updateProjectCombo();
									if (!XMSystemUtil.isEmpty(projectSelected) && xmHotlineHelper.getProjectMap().get(projectSelected) == null) {
										clearProjectAndUpdateModel();
									} else if (!projectUserRelationExist && currentProjectId != null
											&& currentUserId != null) {
										clearProjectAndUpdateModel();
									}
									
									String projectFilterText = currentSelectionModel.getProjectFilterText();
									if (!XMSystemUtil.isEmpty(projectFilterText)) {
										filterCombo(projectFilterText, xmHotlineHelper.getProjectComboItems(), projectFilterCombo);
									}
									projectFilterCombo.updateSize();
									projectFilterCombo.getTable().setRedraw(true);
									long loadProjectComboEnd = System.currentTimeMillis();
									LOGGER.debug("UI Operation: Time taken for updating, filtering project and clearing other required combo: " + (loadProjectComboEnd - loadProjectComboStart) + "ms");
								}
							});
							monitor.worked(70);
							return Status.OK_STATUS;
						}
					};
					job.setUser(true);
					job.schedule();
				}
			});

			siteFilterText.addListener(SWT.FocusOut, new Listener() {
				/**
				 * Overrides handleEvent method for site filter focus-out
				 */
				@Override
				public void handleEvent(final Event event) {
					final String filterText = ((Text) event.widget).getText();
					currentSelectionModel.setSiteFilterText(filterText);
					filterCombo(filterText, xmHotlineHelper.getSiteComboItems(), siteFilterCombo);
				}
			});

			adminAreaFilterText.addListener(SWT.FocusOut, new Listener() {
				/**
				 * Overrides handleEvent method for admin area filter focus-out
				 */
				@Override
				public void handleEvent(final Event event) {
					final String filterText = ((Text) event.widget).getText();
					currentSelectionModel.setAdminAreaFilterText(filterText);
					filterCombo(filterText, xmHotlineHelper.getAdminAreaComboItems(), adminAreaFilterCombo);
				}
			});

			userFilterText.addListener(SWT.FocusOut, new Listener() {
				/**
				 * Overrides handleEvent method for user filter focus-out
				 */
				@Override
				public void handleEvent(final Event event) {
					final String filterText = ((Text) event.widget).getText();
					currentSelectionModel.setUserFilterText(filterText);
					filterCombo(filterText, xmHotlineHelper.getUserComboItems(), userFilterCombo);
				}
			});

			projectFilterText.addListener(SWT.FocusOut, new Listener() {
				/**
				 * Overrides handleEvent method for project filter focus-out
				 */
				@Override
				public void handleEvent(final Event event) {
					final String filterText = ((Text) event.widget).getText();
					currentSelectionModel.setProjectFilterText(filterText);
					filterCombo(filterText, xmHotlineHelper.getProjectComboItems(), projectFilterCombo);
				}
			});

			siteFilterButton.addListener(SWT.Selection, new Listener() {
				/**
				 * Overrides handleEvent method for show All sites button
				 * selection
				 */
				@Override
				public void handleEvent(final Event event) {
					siteFilterText.setText(Constants.EMPTY_STR);
					currentSelectionModel.setSiteFilterText(Constants.EMPTY_STR);
					siteFilterCombo.setItemsWithImages(xmHotlineHelper.getSiteComboItems());
				}
			});

			adminAreaFilterButton.addListener(SWT.Selection, new Listener() {
				/**
				 * Overrides handleEvent method for show All admin areas button
				 * selection
				 */
				@Override
				public void handleEvent(final Event event) {
					adminAreaFilterText.setText(Constants.EMPTY_STR);
					currentSelectionModel.setAdminAreaFilterText(Constants.EMPTY_STR);
					adminAreaFilterCombo.setItemsWithImages(xmHotlineHelper.getAdminAreaComboItems());
				}
			});

			userFilterButton.addListener(SWT.Selection, new Listener() {
				/**
				 * Overrides handleEvent method for show All users button
				 * selection
				 */
				@Override
				public void handleEvent(final Event event) {
					userFilterText.setText(Constants.EMPTY_STR);
					currentSelectionModel.setUserFilterText(Constants.EMPTY_STR);
					userFilterCombo.setItemsWithImages(xmHotlineHelper.getUserComboItems());
				}
			});

			projectFilterButton.addListener(SWT.Selection, new Listener() {
				/**
				 * Overrides handleEvent method for show All projects button
				 * selection
				 */
				@Override
				public void handleEvent(final Event event) {
					projectFilterText.setText(Constants.EMPTY_STR);
					currentSelectionModel.setProjectFilterText(Constants.EMPTY_STR);
					projectFilterCombo.setItemsWithImages(xmHotlineHelper.getProjectComboItems());
				}
			});
		} catch (Exception e) {
			LOGGER.error("Execption in Hotline Listeners", e); //$NON-NLS-1$
		}
	}

	/**
	 * Site combo listener.
	 */
	private void siteComboListener() {
		siteFilterCombo.addSelectionListener(new SelectionAdapter() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				final File iconFolder = XMSystemUtil.getXMSystemServerIconsFolder();
				final Shell activeShell = Display.getDefault().getActiveShell();
				if (iconFolder == null) {
					CustomMessageDialog.openError(activeShell, messages.errorDialogTitile , messages.iconNotReachableMsg);
					return;
				}
				String lastSiteId = null;
				lastSiteId = currentSelectionModel.getCurrentSiteId();
				String siteId = getSelectedSiteId();
				if (lastSiteId != null && !lastSiteId.equals(siteId)) {
					long clearingOtherComboStart = System.currentTimeMillis();
					
					setAAComboEmpty();
					adminAreaFilterText.setText(Constants.EMPTY_STR);
					currentSelectionModel.setAdminAreaFilterText(Constants.EMPTY_STR);
					setAdminAreaComboAndFilterEnabled(true);

					setUserComboEmpty();
					userFilterText.setText(Constants.EMPTY_STR);
					currentSelectionModel.setUserFilterText(Constants.EMPTY_STR);
					
					userFilterCombo.setItemsWithImages(xmHotlineHelper.getUserComboItems());

					setProjectComboEmpty();
					projectFilterText.setText(Constants.EMPTY_STR);
					currentSelectionModel.setProjectFilterText(Constants.EMPTY_STR);
					setProjectComboAndFilterEnabled(false);
					long clearingOtherComboEnd = System.currentTimeMillis();
					
					LOGGER.debug("UI Operation: Time taken for clearing other combo selections:" + (clearingOtherComboEnd - clearingOtherComboStart) + "ms");
					
				} else {
					setAdminAreaComboAndFilterEnabled(true);
				}
				if (XMSystemUtil.isEmpty(siteId)) {
					return;
				} 

				final Job job = new Job("Loading Site Projects..") {

					@Override
					protected IStatus run(IProgressMonitor monitor) {
						monitor.beginTask("Loading Site Projects..", 100);
						monitor.worked(30);
						Display.getDefault().asyncExec(new Runnable() {

							@Override
							public void run() {
								selectSite(activeShell, siteId);
							}
						});
						return Status.OK_STATUS;
					}
				};
				job.schedule();
			}
		});
	}

	/**
	 * Update status bar.
	 *
	 * @param selection the selection
	 */
	protected void updateStatusBar(final String selection) {
		if (selection != null) {
			this.eventBroker.send(Constants.STATUSBAR, selection);
		} else {
			this.eventBroker.send(Constants.STATUSBAR, "CAXHotline");
		}
	}

	/**
	 * Admin area combo listener.
	 */
	private void adminAreaComboListener() {
		this.adminAreaFilterCombo.addSelectionListener(new SelectionAdapter() {

			@Override
			public void widgetSelected(final SelectionEvent event) {
				long adminAreaSelectionStart = System.currentTimeMillis();
				String lastAAId = null;
				final MainModel mainModel = xmHotlineHelper.getMainModel();
				lastAAId = currentSelectionModel.getCurrentAdminAreaId();
				final String adminAreaId = getSelectedAdminAreaId();
				String userId = currentSelectionModel.getCurrentUserId();
				resetUserApplications(mainModel);
				// On AdminArea selection change
				if (lastAAId != null && !lastAAId.equals(adminAreaId) && userId != null) {
					setProjectComboEmpty();
					projectFilterText.setText(Constants.EMPTY_STR);
					currentSelectionModel.setProjectFilterText(Constants.EMPTY_STR);
					setProjectComboAndFilterEnabled(false);
				}
				
				long adminAreaSelectionEnd = System.currentTimeMillis();
				LOGGER.debug("UI Operation: Time taken for reset UA and set project combo empty: " + (adminAreaSelectionEnd - adminAreaSelectionStart) + "ms");
				
				if (!XMSystemUtil.isEmpty(adminAreaId) && userId == null) {
					Job job = new Job("Loading Administration Area Projects..") {
						@Override
						protected IStatus run(IProgressMonitor monitor) {
							monitor.beginTask("Loading Administration Area Projects..", 100);
							monitor.worked(10);
							setUserComboAndFilterEnabled(false);
							long getProjectsByAAIDStart = System.currentTimeMillis();
							projectListByAAId = xmHotlineHelper.getProjetsByAdminAreaId(adminAreaId);
							long getProjectsByAAIDEnd = System.currentTimeMillis();
							LOGGER.debug("Service call: Time taken for getting projects by AA ID: " + (getProjectsByAAIDEnd - getProjectsByAAIDStart) + "ms");
							monitor.worked(60);
							Display.getDefault().asyncExec(new Runnable() {
								@Override
								public void run() {
									long clearUserAppProjectAppStart = System.currentTimeMillis();
									setAdminArea();
									clearUserAppComp();
									clearProjectAppComp();
									setProjectComboEmpty();
									long clearUserAppProjectAppEnd = System.currentTimeMillis();
									LOGGER.debug("UI Operation: Time taken for clearing UserApp, ProjectApp and project combo: " + (clearUserAppProjectAppEnd - clearUserAppProjectAppStart) + "ms");
								}
							});
							setUserComboAndFilterEnabled(true);
							monitor.worked(80);
							return Status.OK_STATUS;
						}
					};
					job.setUser(true);
					job.schedule();
				} else if (!XMSystemUtil.isEmpty(adminAreaId) && userId != null) {
					Job job = new Job("Loading Administration Area Projects..") {
						@Override
						protected IStatus run(IProgressMonitor monitor) {
							monitor.beginTask("Loading Administration Area Projects..", 100);
							monitor.worked(10);
							setUserComboAndFilterEnabled(false);
							long getProjectsByAAIDStart = System.currentTimeMillis();
							projectListByAAId = xmHotlineHelper.getProjetsByAdminAreaId(adminAreaId);
							long getProjectsByAAIDEnd = System.currentTimeMillis();
							LOGGER.debug("Service call: Time taken for getting projects by AA ID: " + (getProjectsByAAIDEnd - getProjectsByAAIDStart) + "ms");
							monitor.worked(60);
							long userComboSelectionStart = System.currentTimeMillis();
							Display.getDefault().asyncExec(new Runnable() {
								@Override
								public void run() {
									setAdminArea();
									userFilterCombo.notifyListeners(SWT.Selection, new Event());
								}
							});
							long userComboSelectionEnd = System.currentTimeMillis();
							LOGGER.debug("Service call and UI call: Time taken for user selection: " + (userComboSelectionEnd - userComboSelectionStart) + "ms");
							setUserComboAndFilterEnabled(true);
							monitor.worked(80);
							return Status.OK_STATUS;
						}
					};
					job.setUser(true);
					job.schedule();
				}
				projectInfoScrollComp.setFocus();
			}
		});
	}

	/**
	 * User combo listener.
	 */
	private void userComboListener() {
		this.userFilterCombo.addSelectionListener(new SelectionAdapter() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				long userComboSelectionStart = System.currentTimeMillis();
				setProjectComboAndFilterEnabled(false);
				
				// find userId for selected user in user filter combo.
				final Shell activeShell = Display.getDefault().getActiveShell();
				clearUserAppComp();
							
				String userId = getSelectedUserId();
				if (XMSystemUtil.isEmpty(userId)) {
					setProjectComboEmpty();
					clearProjectAppComp();
					return;
				}
				String userName = currentSelectionModel.getCurrentUserName();
				SortedMap<String, IXmHotlineModel> userMap = xmHotlineHelper.getUsersMap();
				String adminAreaId = currentSelectionModel.getCurrentAdminAreaId();
				UserModel userModel = (UserModel) userMap.get(userName);
				// make enabled and selection true for the projects which are assigned to selected user in user filter combo.
				userId = userModel.getUserId();
				
				String currentSiteId = currentSelectionModel.getCurrentSiteId();
				long userComboSelectionEnd = System.currentTimeMillis();
				LOGGER.debug("UI Operation: Time taken for clearing UserApp comp, ProjectApp comp and project combo: " + (userComboSelectionEnd - userComboSelectionStart) + "ms");
				
				long getProjectBySiteIdStart = System.currentTimeMillis();
				projectListBySiteId = xmHotlineHelper.getProjetsBySiteId(currentSiteId);
				long getProjectBySiteIdEnd = System.currentTimeMillis();
				LOGGER.debug("Service call: Time taken for getting projects by site ID: " + (getProjectBySiteIdEnd - getProjectBySiteIdStart) + "ms");
				long loadProjectsBySiteStart = System.currentTimeMillis();
				if (projectListBySiteId != null && projectListBySiteId.size() > 0) {
					createControl(projectInfoScrollComp, projectComp, projectListBySiteId);
					projectInfoGroup.layout(true);
				}
				long loadProjectsBySiteEnd = System.currentTimeMillis();
				LOGGER.debug("UI Operation: Time taken for loading projects by site id in project composite: " + (loadProjectsBySiteEnd - loadProjectsBySiteStart) + "ms");
				
				long getProjectsByAAStart = System.currentTimeMillis();
				projectListByAAId = xmHotlineHelper.getProjetsByAdminAreaId(adminAreaId);
				long getProjectsByAAEnd = System.currentTimeMillis();
				LOGGER.debug("Service call: Time taken for getting projects by AA ID: " + (getProjectsByAAEnd - getProjectsByAAStart) + "ms");
				long loadProjectsByAAStart = System.currentTimeMillis();
				if (projectListByAAId != null && projectListByAAId.size() > 0) {
					xmHotlineHelper.setProjectToCombo(projectFilterCombo, projectListByAAId);
					createControl(projectInfoScrollComp, projectComp, projectListByAAId);
					projectInfoGroup.layout(true);
				}
				long loadProjectsByAAEnd = System.currentTimeMillis();
				LOGGER.debug("UI Operation: Time taken for loading projects by AA id in project composite: " + (loadProjectsByAAEnd - loadProjectsByAAStart) + "ms");
				
				if(userId != null && currentSiteId != null) {
					long getProjectsByUserStart = System.currentTimeMillis();
					projectListByUserId = xmHotlineHelper.getProjectsByUserId(userId, currentSiteId);
					long getProjectsByUserEnd = System.currentTimeMillis();
					LOGGER.debug("Service call: Time taken for getting projects by User ID: " + (getProjectsByUserEnd - getProjectsByUserStart) + "ms");
				}
				
				if (projectListByUserId != null && projectListByUserId.size() > 0) {
					long updateProjectCompByUserStart = System.currentTimeMillis();
					updateProCompByUserProRel(userId, true, true);
					long updateProjectCompByUserEnd = System.currentTimeMillis();
					LOGGER.debug("UI Operation: Time taken for updating project composite by user pro rel: " + (updateProjectCompByUserEnd - updateProjectCompByUserStart) + "ms");
				} else {
					CustomMessageDialog.openInformation(activeShell, messages.windowtitle,
							messages.projNotFoundMsg + " " + userName + ".");
					enableProjectObjectsInComp();
					xmHotlineHelper.setProjectToCombo(projectFilterCombo, null);
					projectInfoGroup.layout(true);
				}

				if (userApplList != null) {
					userApplList.clear();
				}
				
				if (!XMSystemUtil.isEmpty(adminAreaId)) {
					long getUserAppByAAIdStart = System.currentTimeMillis();
					userApplList = xmHotlineHelper.getUserApplicationByAAId(adminAreaId, userId);
					long getUserAppByAAIdEnd = System.currentTimeMillis();
					LOGGER.debug("Service call: Time taken for getting userApp by AA ID: " + (getUserAppByAAIdEnd - getUserAppByAAIdStart) + "ms");
					long getUserAppByAAAndUserIdStart = System.currentTimeMillis();
					List<UserUserAppRelation> userUserAppRelation = xmHotlineHelper.getUserUserAppRelsByUserIdAAId(userId, adminAreaId);
					long getUserAppByAAAndUserIdEnd = System.currentTimeMillis();
					LOGGER.debug("Service call: Time taken for getting userApp by AA and user ID: " + (getUserAppByAAAndUserIdEnd - getUserAppByAAAndUserIdStart) + "ms");
					if (userApplList != null && userApplList.size() > 0) {
						long loadUserAppCompStart = System.currentTimeMillis();
						loadUserAppComposite(activeShell, userName, userUserAppRelation);
						long loadUserAppCompEnd = System.currentTimeMillis();
						LOGGER.debug("UI Operation: Time taken for loading UserApp composite by user AA rel: " + (loadUserAppCompEnd - loadUserAppCompStart) + "ms");
					} else {
						CustomMessageDialog.openInformation(activeShell, messages.windowtitle,
								messages.userAppNotFoundForAAMsg + currentSelectionModel.getCurrentAdminAreaName());
					}
				}
				setProjectComboEmpty();
				clearProjectAppComp();
				fixUserApplScrollComp.setFocus();
				projectFilterText.setText(Constants.EMPTY_STR);
				currentSelectionModel.setProjectFilterText(Constants.EMPTY_STR);
				setProjectComboAndFilterEnabled(true);
				int selectionIndex = userFilterCombo.getSelectionIndex();
				if (selectionIndex != -1) {
					String name = userFilterCombo.getItem(selectionIndex);
					updateStatusBar(name);
				}
			}
		});
	}
	
	/**
	 * Enable project comp.
	 */
	private void enableProjectObjectsInComp() {
		final Control[] children = projectComp.getChildren();
		for (final Control control : children) {
			final CustomLabelComposite comp = (CustomLabelComposite) control;
			comp.setSelection(false);
			comp.setEnabled(true);
		}
	}
	
	/**
	 * Enable project comp objects.
	 */
	private void enableProjectCompObjects(final boolean value) {
		final Control[] children = projectComp.getChildren();
		for (final Control control : children) {
			final CustomLabelComposite comp = (CustomLabelComposite) control;
			comp.setEnabled(value);
		}
	}

	/**
	 * Change user app selection.
	 *
	 * @param userUserAppRelations the user user app relations
	 */
	protected void changeUserAppSelection(List<UserUserAppRelation> userUserAppRelations) {
		Control[] fixedChildren = fixedUserAppComo.getChildren();
		for (Control control : fixedChildren) {
			CustomLabelComposite comp = ((CustomLabelComposite) control);
			IXmHotlineModel model = comp.getModel();
			if (model instanceof UserApplicationModel) {
				final UserApplicationModel userApplicationModel = (UserApplicationModel) model;
				for (UserUserAppRelation userUserAppRelation : userUserAppRelations) {
					if (userUserAppRelation.getUserApplicationId().getUserApplicationId()
							.equals(userApplicationModel.getUserAppId())) {
						final String userId = userUserAppRelation.getUserId().getUserId();
						userApplicationModel.setUserUserAppRelId(userUserAppRelation.getUserUserAppRelId());
						userApplicationModel.setUserId(userId);
						if (UserRelationType.FORBIDDEN.name().equals(userUserAppRelation.getUserRelType())
								&& userApplicationModel.getUserAppId()
										.equals(userUserAppRelation.getUserApplicationId().getUserApplicationId())) {
							comp.setSelection(false);
						} else if (UserRelationType.ALLOWED.name().equals(userUserAppRelation.getUserRelType())
								&& userApplicationModel.getUserAppId()
										.equals(userUserAppRelation.getUserApplicationId().getUserApplicationId())) {
							comp.setSelection(true);
						}
					}
				}
			}
		}
		Control[] notFixedChildren = notFixedUserAppComp.getChildren();
		for (Control control : notFixedChildren) {
			CustomLabelComposite comp = ((CustomLabelComposite) control);
			IXmHotlineModel model = comp.getModel();
			if (model instanceof UserApplicationModel) {
				final UserApplicationModel userApplicationModel = (UserApplicationModel) model;
				for (UserUserAppRelation userUserAppRelation : userUserAppRelations) {
					if (userUserAppRelation.getUserApplicationId().getUserApplicationId()
							.equals(userApplicationModel.getUserAppId())) {
						final String userId = userUserAppRelation.getUserId().getUserId();
						userApplicationModel.setUserUserAppRelId(userUserAppRelation.getUserUserAppRelId());
						userApplicationModel.setUserId(userId);
						if (UserRelationType.FORBIDDEN.name().equals(userUserAppRelation.getUserRelType())
								&& userApplicationModel.getUserAppId()
										.equals(userUserAppRelation.getUserApplicationId().getUserApplicationId())) {
							comp.setSelection(false);
						} else if (UserRelationType.ALLOWED.name().equals(userUserAppRelation.getUserRelType())
								&& userApplicationModel.getUserAppId()
										.equals(userUserAppRelation.getUserApplicationId().getUserApplicationId())) {
							comp.setSelection(true);
						}
					}
				}
			}
		}
		Control[] protectedChildren = protectedUserAppComp.getChildren();
		for (Control control : protectedChildren) {
			CustomLabelComposite comp = ((CustomLabelComposite) control);
			IXmHotlineModel model = comp.getModel();
			if (model instanceof UserApplicationModel) {
				UserApplicationModel userApplicationModel = (UserApplicationModel) model;
				for (UserUserAppRelation userUserAppRelation : userUserAppRelations) {
					if (userUserAppRelation.getUserApplicationId().getUserApplicationId()
							.equals(userApplicationModel.getUserAppId())) {
						final String userId = userUserAppRelation.getUserId().getUserId();
						userApplicationModel.setUserUserAppRelId(userUserAppRelation.getUserUserAppRelId());
						userApplicationModel.setUserId(userId);
						if (UserRelationType.FORBIDDEN.name().equals(userUserAppRelation.getUserRelType())
								&& userApplicationModel.getUserAppId()
										.equals(userUserAppRelation.getUserApplicationId().getUserApplicationId())) {
							comp.setSelection(false);
						} else if (UserRelationType.ALLOWED.name().equals(userUserAppRelation.getUserRelType())
								&& userApplicationModel.getUserAppId()
										.equals(userUserAppRelation.getUserApplicationId().getUserApplicationId())) {
							comp.setSelection(true);
						}
					}
				}
			}
		}
	}

	/**
	 * Project combo listener.
	 */
	private void projectComboListener() {
		this.projectFilterCombo.addSelectionListener(new SelectionAdapter() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				long projectSelectionStart = System.currentTimeMillis();
				final MainModel mainModel = xmHotlineHelper.getMainModel();
				Shell activeShell = Display.getDefault().getActiveShell();
				String projectId = getSelectedProjectId();
				clearProjectAppComp();
				boolean isExistingAA = false;
				if (XMSystemUtil.isEmpty(projectId)) {
					return;
				}
				resetProjectApplications(mainModel);
				String userId = currentSelectionModel.getCurrentUserId();
				if (XMSystemUtil.isEmpty(userId)) {
					CustomMessageDialog.openError(activeShell, messages.errorDialogTitile , messages.selectUserMsg);
					return;
				}
				String projectName = currentSelectionModel.getCurrentProjectName();
				SortedMap<String, IXmHotlineModel> projectMap = xmHotlineHelper.getProjectMap();
				ProjectModel projectModel = (ProjectModel) projectMap.get(projectName);
				projectId = projectModel.getProjectId();
				String adminAreaId = currentSelectionModel.getCurrentAdminAreaId();
				String adminAreaName = currentSelectionModel.getCurrentAdminAreaName();
				String userName = currentSelectionModel.getCurrentUserName();
				String siteId = currentSelectionModel.getCurrentSiteId();
				long projectSelectionEnd = System.currentTimeMillis();
				LOGGER.debug("UI Operation: Time taken for getting data from model and clearing projectApp comp: " + (projectSelectionEnd - projectSelectionStart) + "ms");
				
				long getAABySiteProStart = System.currentTimeMillis();
				List<AdminAreasTbl> adminAreaBySiteAndProjectId = xmHotlineHelper.getAdminAreaBySiteAndProjectId(siteId, projectId);
				long getAABySiteProEnd = System.currentTimeMillis();
				LOGGER.debug("Service call: Time taken for getting AA by site and project ID: " + (getAABySiteProEnd - getAABySiteProStart) + "ms");
				if (adminAreaBySiteAndProjectId != null) {
					AdminAreasTbl adminAreasTbl = adminAreaBySiteAndProjectId.get(0);
					String adminAreaNameFromTbl = adminAreasTbl.getName();

					if (adminAreaName != null) {
						if (adminAreaName.equals(adminAreaNameFromTbl)) {
							isExistingAA = true;
						}
					}
					if (!XMSystemUtil.isEmpty(adminAreaId) && isExistingAA) {
						long getPAByAAandProjectIdStart = System.currentTimeMillis();
						projectApplList = xmHotlineHelper.getProjectApplicationByAAIdandProjectId(adminAreaId, projectId, userId);
						Iterable<UserProjAppRelTbl> userProjAppRelsByUserProjAAId = xmHotlineHelper.getUserProjAppRelsByUserProjAAId(userId, projectId, adminAreaId);
						long getPAByAAandProjectIdEnd = System.currentTimeMillis();
						LOGGER.debug("Service call: Time taken for getting projectApp by AA and project id: " + (getPAByAAandProjectIdEnd - getPAByAAandProjectIdStart) + "ms");
						if (projectApplList != null && projectApplList.size() > 0) {
							long loadProjectAppCompStart = System.currentTimeMillis();
							loadProjectAppComposite(activeShell, userProjAppRelsByUserProjAAId);
							long loadProjectAppCompEnd = System.currentTimeMillis();
							LOGGER.debug("UI Operation: Time taken for loading projectApp comp: " + (loadProjectAppCompEnd - loadProjectAppCompStart) + "ms");
						} else {
							CustomMessageDialog.openInformation(activeShell, messages.windowtitle,
									messages.projAppNotFoundForAAMsg + " " + adminAreaName + " " + messages.andProjMsg + projectName + ".");
							clearProjectAppComp();
						}
					} else {
						
						// Dynamically select AA based on the project selection
						String[] items = adminAreaFilterCombo.getItems();
						for (String item : items) {
							if(item.equals(adminAreaNameFromTbl)){
								int indexOf = adminAreaFilterCombo.indexOf(item);
								adminAreaFilterCombo.select(indexOf);
							}
						}
						
						getSelectedAdminAreaId();
						String updatedAdminAreaId = currentSelectionModel.getCurrentAdminAreaId();
						
						long getProByAAidStart = System.currentTimeMillis();
						projectListByAAId = xmHotlineHelper.getProjetsByAdminAreaId(updatedAdminAreaId);
						long getProByAAidEnd = System.currentTimeMillis();
						LOGGER.debug("Service call: Time taken for getting project by AA id: " + (getProByAAidEnd - getProByAAidStart) + "ms");
						
						long loadProCompByAAStart = System.currentTimeMillis();
						if (projectListByAAId != null && projectListByAAId.size() > 0) {
							xmHotlineHelper.setProjectToCombo(projectFilterCombo, projectListByAAId);
							createControl(projectInfoScrollComp, projectComp, projectListByAAId);
							projectInfoGroup.layout(true);
						}
						enableProjectCompObjects(true);
						
						updateProCompByUserProRel(userId, false, true);
						long loadProCompByAAEnd = System.currentTimeMillis();
						LOGGER.debug("UI Operation: Time taken for loading project comp and updating project comp by user pro rel: " + (loadProCompByAAEnd - loadProCompByAAStart) + "ms");
						
						if (!XMSystemUtil.isEmpty(updatedAdminAreaId)) {
							long getProAppByAAandProStart = System.currentTimeMillis();
							projectApplList = xmHotlineHelper.getProjectApplicationByAAIdandProjectId(updatedAdminAreaId, projectId,
									userId);
							Iterable<UserProjAppRelTbl> userProjAppRelsByUserProjAAId = xmHotlineHelper
									.getUserProjAppRelsByUserProjAAId(userId, projectId, updatedAdminAreaId);
							long getProAppByAAandProEnd = System.currentTimeMillis();
							LOGGER.debug("Service call: Time taken for getting projectApp by AA and pro id: " + (getProAppByAAandProEnd - getProAppByAAandProStart) + "ms");
							if (projectApplList != null && projectApplList.size() > 0) {
								long loadProjectAppCompStart = System.currentTimeMillis();
								loadProjectAppComposite(activeShell, userProjAppRelsByUserProjAAId);
								long loadProjectAppCompEnd = System.currentTimeMillis();
								LOGGER.debug("UI Operation: Time taken for loading projectApp comp: " + (loadProjectAppCompEnd - loadProjectAppCompStart) + "ms");
							} else {
								CustomMessageDialog.openInformation(activeShell, messages.windowtitle,
										messages.projAppNotFoundForAAMsg + " " + currentSelectionModel.getCurrentAdminAreaName() + " " + messages.andProjMsg 
										+ projectName + ".");
								clearProjectAppComp();
							}
							
							long getUserAppByAAStart = System.currentTimeMillis();
							userApplList = xmHotlineHelper.getUserApplicationByAAId(updatedAdminAreaId, userId);
							List<UserUserAppRelation> userUserAppRelation = xmHotlineHelper
									.getUserUserAppRelsByUserIdAAId(userId, updatedAdminAreaId);
							long getUserAppByAAEnd = System.currentTimeMillis();
							LOGGER.debug("Service call: Time taken for getting userApp by AA id: " + (getUserAppByAAEnd - getUserAppByAAStart) + "ms");
							if (userApplList != null && userApplList.size() > 0) {
								long loadUserAppCompStart = System.currentTimeMillis();
								loadUserAppComposite(activeShell, userName, userUserAppRelation);
								long loadUserAppCompEnd = System.currentTimeMillis();
								LOGGER.debug("UI Operation: Time taken for loading UsertApp comp: " + (loadUserAppCompEnd - loadUserAppCompStart) + "ms");
							} else {
								CustomMessageDialog.openInformation(activeShell, messages.windowtitle,
										messages.userAppNotFoundForAAMsg + currentSelectionModel.getCurrentAdminAreaName());
								clearUserAppComp();
							}
						}
					}
				} else {
					long clearUIandModelStart = System.currentTimeMillis();
					clearProjectAndUpdateModel();
					clearUserAppComp();
					clearProjectComp();
					setAAComboEmpty();
					long clearUIandModelEnd = System.currentTimeMillis();
					LOGGER.debug("UI Operation: Time taken for clearing project, UserApp, projectApp and AA combo empty: " + (clearUIandModelEnd - clearUIandModelStart) + "ms");
					long getProjectBySiteStart = System.currentTimeMillis();
					projectListBySiteId = xmHotlineHelper.getProjetsBySiteId(siteId);
					long getProjectBySiteEnd = System.currentTimeMillis();
					LOGGER.debug("Service call: Time taken for getting project by site id: " + (getProjectBySiteEnd - getProjectBySiteStart) + "ms");
					
					long loadProjectCompBySiteStart = System.currentTimeMillis();
					if (projectListBySiteId != null && projectListBySiteId.size() > 0) {
						createControl(projectInfoScrollComp, projectComp, projectListBySiteId);
						projectInfoGroup.layout(true);
					}
					long loadProjectCompBySiteEnd = System.currentTimeMillis();
					LOGGER.debug("UI Operation: Time taken for loading project comp by site id: " + (loadProjectCompBySiteEnd - loadProjectCompBySiteStart) + "ms");
					
					long getProjectByUserStart = System.currentTimeMillis();
					projectListByUserId = xmHotlineHelper.getProjectsByUserId(userId, siteId);
					List<IXmHotlineModel> projectsAssignedToSelectedUser = new ArrayList<>();
					long getProjectByUserEnd = System.currentTimeMillis();
					LOGGER.debug("Service call: Time taken for getting project by user id: " + (getProjectByUserEnd - getProjectByUserStart) + "ms");
					
					if (projectListByUserId != null && projectListByUserId.size() > 0) {
						long updateProCompByUserStart = System.currentTimeMillis();
						projectsAssignedToSelectedUser = updateProCompByUserProRel(userId, true, true);
						long updateProCompByUserEnd = System.currentTimeMillis();
						LOGGER.debug("UI Operation: Time taken for updating project comp by user rel: " + (updateProCompByUserEnd - updateProCompByUserStart) + "ms");
						CustomMessageDialog.openInformation(activeShell, messages.windowtitle,
								"Project " + "'" + projectName + "' doesn't have the relation with the admin area '" + adminAreaName + "'");
					} else {
						CustomMessageDialog.openInformation(activeShell, messages.windowtitle,
								messages.projNotFoundMsg + userName + ".");
						enableProjectObjectsInComp();
						xmHotlineHelper.setProjectToCombo(projectFilterCombo, projectsAssignedToSelectedUser);
						projectInfoGroup.layout(true);
					}
				}
				
				fixProjApplScrollComp.setFocus();
				int selectionIndex = siteFilterCombo.getSelectionIndex();
				if (selectionIndex != -1) {
					String siteName = siteFilterCombo.getItem(selectionIndex);
					updateStatusBar(siteName);
				}
			}
		});
	}

	/**
	 * Change proj app selection.
	 *
	 * @param userProjAppRelsByUserProjAAId the user proj app rels by user proj AA id
	 */
	protected void changeProjAppSelection(Iterable<UserProjAppRelTbl> userProjAppRelsByUserProjAAId) {
		Control[] fixedChildren = fixedProjectAppComp.getChildren();
		for (Control control : fixedChildren) {
			CustomLabelComposite comp = ((CustomLabelComposite) control);
			IXmHotlineModel model = comp.getModel();
			if (model instanceof ProjectApplicationModel) {
				ProjectApplicationModel projectApplicationModel = (ProjectApplicationModel) model;
				Iterator<UserProjAppRelTbl> iterator = userProjAppRelsByUserProjAAId.iterator();
				while (iterator.hasNext()) {
					UserProjAppRelTbl userProjAppRelTbl = iterator.next();
					if (userProjAppRelTbl.getProjectApplicationId().getProjectApplicationId()
							.equals(projectApplicationModel.getProjectAppId())) {
						if (UserRelationType.FORBIDDEN.name().equals(userProjAppRelTbl.getUserRelType())
								&& projectApplicationModel.getProjectAppId().equals(
										userProjAppRelTbl.getProjectApplicationId().getProjectApplicationId())) {
							comp.setSelection(false);
						} else if (UserRelationType.ALLOWED.name().equals(userProjAppRelTbl.getUserRelType())
								&& projectApplicationModel.getProjectAppId().equals(
										userProjAppRelTbl.getProjectApplicationId().getProjectApplicationId())) {
							comp.setSelection(true);
						}
					}
				}
			}
		}
		Control[] notFixedChildren = notFixedProjectAppComp.getChildren();
		for (Control control : notFixedChildren) {
			CustomLabelComposite comp = ((CustomLabelComposite) control);
			IXmHotlineModel model = comp.getModel();
			if (model instanceof ProjectApplicationModel) {
				ProjectApplicationModel projectApplicationModel = (ProjectApplicationModel) model;
				Iterator<UserProjAppRelTbl> iterator = userProjAppRelsByUserProjAAId.iterator();
				while (iterator.hasNext()) {
					UserProjAppRelTbl userProjAppRelTbl = iterator.next();
					if (userProjAppRelTbl.getProjectApplicationId().getProjectApplicationId()
							.equals(projectApplicationModel.getProjectAppId())) {
						if (UserRelationType.FORBIDDEN.name().equals(userProjAppRelTbl.getUserRelType())
								&& projectApplicationModel.getProjectAppId().equals(
										userProjAppRelTbl.getProjectApplicationId().getProjectApplicationId())) {
							comp.setSelection(false);
						} else if (UserRelationType.ALLOWED.name().equals(userProjAppRelTbl.getUserRelType())
								&& projectApplicationModel.getProjectAppId().equals(
										userProjAppRelTbl.getProjectApplicationId().getProjectApplicationId())) {
							comp.setSelection(true);
						}
					}
				}
			}
		}
		Control[] protectedChildren = protectedProjectAppComp.getChildren();
		for (Control control : protectedChildren) {
			CustomLabelComposite comp = ((CustomLabelComposite) control);
			IXmHotlineModel model = comp.getModel();
			if (model instanceof ProjectApplicationModel) {
				ProjectApplicationModel projectApplicationModel = (ProjectApplicationModel) model;
				Iterator<UserProjAppRelTbl> iterator = userProjAppRelsByUserProjAAId.iterator();
				while (iterator.hasNext()) {
					UserProjAppRelTbl userProjAppRelTbl = iterator.next();
					if (userProjAppRelTbl.getProjectApplicationId().getProjectApplicationId()
							.equals(projectApplicationModel.getProjectAppId())) {
						if (UserRelationType.FORBIDDEN.name().equals(userProjAppRelTbl.getUserRelType())
								&& projectApplicationModel.getProjectAppId().equals(
										userProjAppRelTbl.getProjectApplicationId().getProjectApplicationId())) {
							comp.setSelection(false);
						} else if (UserRelationType.ALLOWED.name().equals(userProjAppRelTbl.getUserRelType())
								&& projectApplicationModel.getProjectAppId().equals(
										userProjAppRelTbl.getProjectApplicationId().getProjectApplicationId())) {
							comp.setSelection(true);
						}
					}
				}
			}
		}
	}

	/**
	 * Filter combo.
	 *
	 * @param filterText the filter text
	 * @param items the items
	 * @param combo the combo
	 */
	private void filterCombo(final String filterText, final Map<String, Object> items,
			final MagnaCustomCombo combo) {
		try {
			if (items != null && !items.isEmpty()) {
				final Map<String, Object> filteredItems = new TreeMap<>();
				Set<String> keySet = items.keySet();
				for (String itemText : keySet) {
					if (itemText != null && itemText instanceof String
							&& Pattern.compile(Pattern.quote(filterText), Pattern.CASE_INSENSITIVE)
							.matcher((String) itemText).find()) {
						final Object itemImage = items.get(itemText);
						filteredItems.put((String)itemText, itemImage);
					}
				}
				combo.setItemsWithImages(filteredItems);
			}
		} catch (Exception e) {
			CustomMessageDialog.openError(Display.getDefault().getActiveShell(), messages.errorDialogTitile, messages.filterComboMsg);
		}
	}

	/**
	 * Create control.
	 *
	 * @param scrollComp the scroll comp
	 * @param composite the composite
	 * @param list the list
	 */
	protected void createControl(final ScrolledComposite scrollComp, Composite composite,
			final List<IXmHotlineModel> list) {
		scrollComp.setRedraw(false);

		// remove exiting model objects and dispose existing control.
		Control[] children = composite.getChildren();
		for (int i = 0; i < children.length; i++) {
			CustomLabelComposite comp = (CustomLabelComposite) children[i];
			comp.dispose();
		}
		// add create new control with updated list.
		setDataToControl(composite, list);
		scrollComp.setContent(composite);
		Point point = composite.computeSize(SWT.DEFAULT, SWT.DEFAULT);
		composite.setSize(point);
		scrollComp.setMinSize(point);
		
		composite.setBackgroundMode(SWT.INHERIT_FORCE);
		scrollComp.setBackgroundMode(SWT.INHERIT_FORCE);
		
		scrollComp.setRedraw(true);
	}

	/**
	 * Set data to control.
	 *
	 * @param controlComposite the control composite
	 * @param list the list
	 */
	private void setDataToControl(final Composite controlComposite, final List<IXmHotlineModel> list) {
		/*LANG_ENUM defLocaleEnum = XmHotlineUtil.getInstance().getDefaultLocaleEnum();
		LANG_ENUM currentLocaleEnum = XmHotlineUtil.getInstance().getCurrentLocaleEnum();
		if (currentLocaleEnum == null) {
			currentLocaleEnum = defLocaleEnum;
		}*/
		if (list != null) {
			int listSize = list.size();
			labelComposites = new CustomLabelComposite[listSize];
			for (int indx = 0; indx < labelComposites.length; indx++) {
				IXmHotlineModel hotlineModel = list.get(indx);
				if (hotlineModel instanceof ProjectModel) {
					ProjectModel project = (ProjectModel) hotlineModel;
					labelComposites[indx] = new CustomLabelComposite(controlComposite, project, this.registry,
							this.eventBroker);
					// labelComposites[indx].setToolTipText(project.getProjectDescMap().get(currentLocaleEnum));
					labelComposites[indx].setModel(project);
				} else if (hotlineModel instanceof UserApplicationModel) {
					UserApplicationModel userApp = (UserApplicationModel) hotlineModel;
					labelComposites[indx] = new CustomLabelComposite(controlComposite, userApp, this.registry,
							this.eventBroker);
					// labelComposites[indx].setToolTipText(userApp.getUserAppDescMap().get(currentLocaleEnum));
					labelComposites[indx].setModel(userApp);
				} else if (hotlineModel instanceof ProjectApplicationModel) {
					ProjectApplicationModel projectAppl = (ProjectApplicationModel) hotlineModel;
					labelComposites[indx] = new CustomLabelComposite(controlComposite, projectAppl, this.registry,
							this.eventBroker);
					// labelComposites[indx].setToolTipText(projectAppl.getProjectAppDescMap().get(currentLocaleEnum));
					labelComposites[indx].setModel(projectAppl);
				}
			}
		}
	}

	/**
	 * Add project user relation.
	 *
	 * @param mainModel the main model
	 * @param userId the user id
	 * @return the string
	 */
	private String addProjectUserRelation(final MainModel mainModel, final String userId) {
		final StringBuilder stb = new StringBuilder();
		try {
			List<ProjectModel> projectUserRelationAddList;
			List<UserProjectRelRequest> projectRelRequestList = new ArrayList<>();
			if ((projectUserRelationAddList = mainModel.getProjectUserRelationAddList()) != null
					&& projectUserRelationAddList.size() > 0) {
				for (ProjectModel projectModel : projectUserRelationAddList) {
					UserProjectRelRequest userProjectRelRequest = new UserProjectRelRequest();
					userProjectRelRequest.setProjectId(projectModel.getProjectId());
					userProjectRelRequest.setUserId(userId);
					userProjectRelRequest.setStatus(com.magna.xmbackend.vo.enums.Status.ACTIVE.name());
					projectRelRequestList.add(userProjectRelRequest);
				}
				UserProjectRelController adminAreaProjectRelController = new UserProjectRelController();
				UserProjectRelBatchResponse userProjectRelBatchResponse = adminAreaProjectRelController
						.createUserProjectRel(new UserProjectRelBatchRequest(projectRelRequestList));
				
				List<Map<String, String>> statusMapList = userProjectRelBatchResponse.getStatusMap();
				
				if (statusMapList != null && !statusMapList.isEmpty()) {
					resetProjectUserAddRelation(mainModel);
					String errorMessage = "";
					for (Map<String, String> statusMap : statusMapList) {
						errorMessage += statusMap.get(XmHotlineUtil.getInstance().getCurrentLocaleEnum().getLangCode());
						errorMessage += "\n";
						return errorMessage;
					}
				}
				
				LOGGER.info("Inside addProjectUserRelation method and executed createUserProjectRel : "
						+ userProjectRelBatchResponse);
				mainModel.getProjectUserRelationAddList().clear();
				//stb.append(messages.projectUserAddSuccessMsg);
			}
		} catch (final UnauthorizedAccessException e) {
			stb.append(messages.projectUserAddErrorMsg);
			resetProjectUserAddRelation(mainModel);
		} catch (final CannotCreateObjectException e) {
			stb.append(messages.relAlreadyExistMsg);
		}
		return stb.append("\n").toString();
	}

	/**
	 * Delete project user relation.
	 *
	 * @param mainModel the main model
	 * @return the string
	 */
	private String deleteProjectUserRelation(final MainModel mainModel) {
		final StringBuilder stb = new StringBuilder();
		try {
			List<ProjectModel> projectUserRelationDeleteList;
			if ((projectUserRelationDeleteList = mainModel.getProjectUserRelationDeleteList()) != null
					&& projectUserRelationDeleteList.size() > 0) {
				String userProjectRelId = Constants.EMPTY_STR;
				UserProjectRelController userProjectRelController = new UserProjectRelController();
				for (ProjectModel projectModel : projectUserRelationDeleteList) {
					String projectId = projectModel.getProjectId();
					List<UserProjectRelTbl> userProjectRelResponse = userProjectRelController
							.getUserProjectRelByProjectId(projectId);
					if (userProjectRelResponse != null) {
						for (UserProjectRelTbl userProjectRelTbl : userProjectRelResponse) {
							if (userProjectRelTbl.getProjectId().getProjectId().equals(projectId)
									&& userProjectRelTbl.getUserId().getUserId().equals(projectModel.getUserId())) {
								userProjectRelId = userProjectRelTbl.getUserProjectRelId();
								break;
							}
						}
					}
					boolean deleteUserProjectRel = userProjectRelController.deleteUserProjectRel(userProjectRelId);
					LOGGER.info("Inside deleteProjectUserRelation method and executed deleteUserProjectRel : "
							+ deleteUserProjectRel);
					if (deleteUserProjectRel) {
						if (currentSelectionModel.getCurrentProjectId() != null
								&& currentSelectionModel.getCurrentProjectId().equals(projectId)) {
							projectFilterText.setText(Constants.EMPTY_STR);
							currentSelectionModel.setProjectFilterText(Constants.EMPTY_STR);
							setProjectComboEmpty();
							clearProjectAppComp();
							updateProjectCombo();
						}
					}
				}
				mainModel.getProjectUserRelationDeleteList().clear();
				//stb.append(messages.projectUserRemoveSuccessMsg);
			}
		} catch (final UnauthorizedAccessException e) {
			stb.append(messages.projectUserRemoveErrorMsg);
			resetProjectUserRelation(mainModel);
		}
		return stb.append("\n").toString();
	}

	/**
	 * Add user app admin area relation.
	 *
	 * @param mainModel the main model
	 * @param siteAdminAreaRelId the site admin area rel id
	 * @param selectedAdminAreaId the selected admin area id
	 * @return the string
	 */
	private String addUserAppAdminAreaRelation(final MainModel mainModel, final String siteAdminAreaRelId,
			final String selectedAdminAreaId) {
		final StringBuilder stb = new StringBuilder();
		try {
			List<UserApplicationModel> userAppAdminAreaRelationAddList;
			if ((userAppAdminAreaRelationAddList = mainModel.getUserAppUserRelationAddList()) != null
					&& userAppAdminAreaRelationAddList.size() > 0) {
				final List<UserUserAppRelRequest> userUserAppRelRequestList = new ArrayList<>();
				for (final UserApplicationModel userApplicationModel : userAppAdminAreaRelationAddList) {
					if (!XMSystemUtil.isEmpty(siteAdminAreaRelId)) {
						if (ApplicationRelationType.FIXED.name().equals(userApplicationModel.getRelType())) {
							deleteUserAppAAIdRel(userApplicationModel, selectedAdminAreaId);
						} else if (ApplicationRelationType.NOTFIXED.name().equals(userApplicationModel.getRelType())
								|| ApplicationRelationType.PROTECTED.name().equals(userApplicationModel.getRelType())) {
							UserUserAppRelRequest userUserAppRelRequest = new UserUserAppRelRequest();
							userUserAppRelRequest.setHotlineRequest(true);
							userUserAppRelRequest.setUserId(userApplicationModel.getUserId());
							userUserAppRelRequest.setUserAppId(userApplicationModel.getUserAppId());
							userUserAppRelRequest.setSiteAdminAreaRelId(siteAdminAreaRelId);
							userUserAppRelRequest.setUserRelationType(UserRelationType.ALLOWED.name());
							userUserAppRelRequestList.add(userUserAppRelRequest);
						}
					}
				}
				
				if(userUserAppRelRequestList.size() > 0){
					UserUserAppRelController userAppRelController = new UserUserAppRelController(selectedAdminAreaId);
					UserUserAppRelBatchResponse userUserAppRelBatchResponse = userAppRelController
							.createUserUserAppRel(new UserUserAppRelBatchRequest(userUserAppRelRequestList));
					
					List<Map<String, String>> statusMapList = userUserAppRelBatchResponse.getStatusMap();
					if (statusMapList != null && !statusMapList.isEmpty()) {
						resetAddUserApplications(mainModel);
						String errorMessage = "";
						for (Map<String, String> statusMap : statusMapList) {
							errorMessage += statusMap.get(XmHotlineUtil.getInstance().getCurrentLocaleEnum().getLangCode());
							errorMessage += "\n";
							return errorMessage;
						}
					}
					
					LOGGER.info("Inside addUserAppAdminAreaRelation method and executed createUserUserAppRel : "
							+ userUserAppRelBatchResponse);
				}
				mainModel.getUserAppUserRelationAddList().clear();
				//stb.append(messages.userAppAddSuccessMsg);
			}
		} catch (UnauthorizedAccessException e) {
			stb.append(messages.userAppAddErrorMsg);
			resetAddUserApplications(mainModel);
		} catch (CannotCreateObjectException e) {
			stb.append(messages.relAlreadyExistMsg);
		}
		return stb.append("\n").toString();
	}

	/**
	 * Delete user app admin area relation.
	 *
	 * @param mainModel the main model
	 * @param siteAdminAreaRelId the site admin area rel id
	 * @param selectedAdminAreaId the selected admin area id
	 * @return the string
	 */
	private String deleteUserAppAdminAreaRelation(final MainModel mainModel, final String siteAdminAreaRelId,
			final String selectedAdminAreaId) {
		final StringBuilder stb = new StringBuilder();
		try {
			List<UserApplicationModel> userAppAdminAreaRelationDeleteList;
			if ((userAppAdminAreaRelationDeleteList = mainModel.getUserAppUserRelationDeleteList()) != null
					&& userAppAdminAreaRelationDeleteList.size() > 0) {
				final List<UserUserAppRelRequest> userUserAppRelRequestList = new ArrayList<>();
				for (final UserApplicationModel userApplicationModel : userAppAdminAreaRelationDeleteList) {
					if (ApplicationRelationType.FIXED.name().equals(userApplicationModel.getRelType())) {
						UserUserAppRelRequest userUserAppRelRequest = new UserUserAppRelRequest();
						userUserAppRelRequest.setHotlineRequest(true);
						userUserAppRelRequest.setUserId(userApplicationModel.getUserId());
						userUserAppRelRequest.setUserAppId(userApplicationModel.getUserAppId());
						userUserAppRelRequest.setSiteAdminAreaRelId(siteAdminAreaRelId);
						userUserAppRelRequest.setUserRelationType(UserRelationType.FORBIDDEN.name());
						userUserAppRelRequestList.add(userUserAppRelRequest);
					} else if (ApplicationRelationType.NOTFIXED.name().equals(userApplicationModel.getRelType())
							|| ApplicationRelationType.PROTECTED.name().equals(userApplicationModel.getRelType())) {
						deleteUserAppAAIdRel(userApplicationModel, selectedAdminAreaId);
					}
				}
				
				if(userUserAppRelRequestList.size() > 0){
					UserUserAppRelController userAppRelController = new UserUserAppRelController(selectedAdminAreaId);
					UserUserAppRelBatchResponse userUserAppRelBatchResponse = userAppRelController
							.createUserUserAppRel(new UserUserAppRelBatchRequest(userUserAppRelRequestList));
					
					List<Map<String, String>> statusMapList = userUserAppRelBatchResponse.getStatusMap();
					if (statusMapList != null && !statusMapList.isEmpty()) {
						resetDeleteUserApplications(mainModel);
						String errorMessage = "";
						for (Map<String, String> statusMap : statusMapList) {
							errorMessage += statusMap.get(XmHotlineUtil.getInstance().getCurrentLocaleEnum().getLangCode());
							errorMessage += "\n";
							return errorMessage;
						}
					}
					LOGGER.info("Inside deleteUserAppAdminAreaRelation method and executed createUserUserAppRel : "
							+ userUserAppRelBatchResponse);
				}
				
				mainModel.getUserAppUserRelationDeleteList().clear();
				//stb.append(messages.userAppRemoveSuccessMsg);
			}
		} catch (final UnauthorizedAccessException e) {
			stb.append(messages.userAppRemoveErrorMsg);
			resetUserApplications(mainModel);
		}
		return stb.append("\n").toString();
	}

	/**
	 * Delete user app AA id rel.
	 *
	 * @param userApplicationModel the user application model
	 * @param selectedAdminAreaId the selected admin area id
	 * @return the string
	 * @throws UnauthorizedAccessException the unauthorized access exception
	 */
	private String deleteUserAppAAIdRel(final UserApplicationModel userApplicationModel,
			final String selectedAdminAreaId) throws UnauthorizedAccessException {
		final UserUserAppRelController userUserAppRelController = new UserUserAppRelController(selectedAdminAreaId);
		String userUserAppRelId = Constants.EMPTY_STR;
		String adminAreaId = userApplicationModel.getAdminAreaId();
		String userId = userApplicationModel.getUserId();
		UserUserAppRelResponseWrapper userUserAppRelResponseWrapper = userUserAppRelController
				.getUserUserAppRelsByUserIdAAId(userId, adminAreaId);
		if (userUserAppRelResponseWrapper != null) {
			List<UserUserAppRelation> userUserAppRelations = userUserAppRelResponseWrapper.getUserUserAppRelations();
			if (userUserAppRelations != null && userUserAppRelations.size() > 0) {
				for (UserUserAppRelation userUserAppRelation : userUserAppRelations) {
					if (userUserAppRelation.getUserApplicationId().getUserApplicationId()
							.equals(userApplicationModel.getUserAppId())) {
						userUserAppRelId = userUserAppRelation.getUserUserAppRelId();
						break;
					}
				}
			}
			boolean deleteUserUserAppRel = userUserAppRelController.deleteUserUserAppRel(userUserAppRelId);
			LOGGER.info(
					"Inside deleteUserAppAAIdRel method and executed deleteUserUserAppRel : " + deleteUserUserAppRel);
			return userUserAppRelId;
		}
		return null;
	}

	/**
	 * Add pro app user admin area project relation.
	 *
	 * @param mainModel the main model
	 * @param userProjectRelId the user project rel id
	 * @param adminAreaProjectRelId the admin area project rel id
	 * @param selectedAdminAreaId the selected admin area id
	 * @return the string
	 */
	private String addProAppUserAdminAreaProjectRelation(MainModel mainModel, String userProjectRelId,
			String adminAreaProjectRelId, final String selectedAdminAreaId) {
		final StringBuilder stb = new StringBuilder();
		try {
			List<ProjectApplicationModel> projectAppUserRelationAddList;
			if ((projectAppUserRelationAddList = mainModel.getProjectAppUserRelationAddList()) != null
					&& projectAppUserRelationAddList.size() > 0) {
				final List<UserProjectAppRelRequest> userProjectAppRelRequestList = new ArrayList<>();
				for (final ProjectApplicationModel projectApplicationModel : projectAppUserRelationAddList) {
					if (ApplicationRelationType.FIXED.name().equals(projectApplicationModel.getRelType())) {
						deleteUserProjAppRel(projectApplicationModel, selectedAdminAreaId);
					} else if (ApplicationRelationType.NOTFIXED.name().equals(projectApplicationModel.getRelType())
							|| ApplicationRelationType.PROTECTED.name().equals(projectApplicationModel.getRelType())) {
						UserProjectAppRelRequest userProjectAppRelRequest = new UserProjectAppRelRequest();
						userProjectAppRelRequest.setHotlineRequest(true);
						userProjectAppRelRequest.setUserProjectRelId(userProjectRelId);
						userProjectAppRelRequest.setAdminAreaProjRelId(adminAreaProjectRelId);
						userProjectAppRelRequest.setProjectAppId(projectApplicationModel.getProjectAppId());
						userProjectAppRelRequest.setUserRelationType(UserRelationType.ALLOWED.name());
						userProjectAppRelRequestList.add(userProjectAppRelRequest);
					}
				}
				
				if(userProjectAppRelRequestList.size() > 0){
					UserProjectAppRelController userProjectAppRelController = new UserProjectAppRelController(selectedAdminAreaId);
					UserProjectAppRelBatchResponse userProjectAppRelBatchResponse = userProjectAppRelController.createUserProjectAppRel(new UserProjectAppRelBatchRequest(userProjectAppRelRequestList));
					List<Map<String, String>> statusMapList = userProjectAppRelBatchResponse.getStatusMap();
					if (statusMapList != null && !statusMapList.isEmpty()) {
						resetAddProjectApplications(mainModel);
						String errorMessage = "";
						for (Map<String, String> statusMap : statusMapList) {
							errorMessage += statusMap.get(XmHotlineUtil.getInstance().getCurrentLocaleEnum().getLangCode());
							errorMessage += "\n";
							return errorMessage;
						}
					}
					LOGGER.info("Inside addProAppUserAdminAreaProjectRelation method and executed createUserProjectAppRel : "+ userProjectAppRelBatchResponse);
				}
				mainModel.getProjectAppUserRelationAddList().clear();
				//stb.append(messages.projectAppAddSuccessMsg);
			}
		} catch (UnauthorizedAccessException e) {
			stb.append(messages.projectAppAddErrorMsg);
			resetAddProjectApplications(mainModel);
		} catch (CannotCreateObjectException e) {
			stb.append(messages.relAlreadyExistsMsg);
		}
		return stb.append("\n").toString();
	}

	/**
	 * Delete pro app user admin area project relation.
	 *
	 * @param mainModel the main model
	 * @param userProjectRelId the user project rel id
	 * @param adminAreaProjectRelId the admin area project rel id
	 * @param selectedAdminAreaId the selected admin area id
	 * @return the string
	 */
	private String deleteProAppUserAdminAreaProjectRelation(MainModel mainModel, String userProjectRelId,
			String adminAreaProjectRelId, final String selectedAdminAreaId) {
		final StringBuilder stb = new StringBuilder();
		try {
			List<ProjectApplicationModel> projectAppUserRelationDeleteList;
			if ((projectAppUserRelationDeleteList = mainModel.getProjectAppUserRelationDeleteList()) != null
					&& projectAppUserRelationDeleteList.size() > 0) {
				final List<UserProjectAppRelRequest> userProjectAppRelRequestList = new ArrayList<>();
				for (final ProjectApplicationModel projectApplicationModel : projectAppUserRelationDeleteList) {
					if (ApplicationRelationType.FIXED.name().equals(projectApplicationModel.getRelType())) {
						UserProjectAppRelRequest userProjectAppRelRequest = new UserProjectAppRelRequest();
						userProjectAppRelRequest.setHotlineRequest(true);
						userProjectAppRelRequest.setUserProjectRelId(userProjectRelId);
						userProjectAppRelRequest.setAdminAreaProjRelId(adminAreaProjectRelId);
						userProjectAppRelRequest.setProjectAppId(projectApplicationModel.getProjectAppId());
						userProjectAppRelRequest.setUserRelationType(UserRelationType.FORBIDDEN.name());
						userProjectAppRelRequestList.add(userProjectAppRelRequest);
					} else if (ApplicationRelationType.NOTFIXED.name().equals(projectApplicationModel.getRelType())
							|| ApplicationRelationType.PROTECTED.name().equals(projectApplicationModel.getRelType())) {
						deleteUserProjAppRel(projectApplicationModel, selectedAdminAreaId);
					}
				}
				
				if(userProjectAppRelRequestList.size() > 0){
					UserProjectAppRelController userProjectAppRelController = new UserProjectAppRelController(selectedAdminAreaId);
					UserProjectAppRelBatchResponse userProjectAppRelBatchResponse = userProjectAppRelController.createUserProjectAppRel(new UserProjectAppRelBatchRequest(userProjectAppRelRequestList));
					
					List<Map<String, String>> statusMapList = userProjectAppRelBatchResponse.getStatusMap();
					if (statusMapList != null && !statusMapList.isEmpty()) {
						resetDeleteProjectApplications(mainModel);
						String errorMessage = "";
						for (Map<String, String> statusMap : statusMapList) {
							errorMessage += statusMap.get(XmHotlineUtil.getInstance().getCurrentLocaleEnum().getLangCode());
							errorMessage += "\n";
							return errorMessage;
						}
					}
					LOGGER.info("Inside deleteProAppUserAdminAreaProjectRelation method and executed createUserProjectAppRel : "+ userProjectAppRelBatchResponse);
				}
				mainModel.getProjectAppUserRelationDeleteList().clear();
				//stb.append(messages.projectAppRemoveSuccessMsg);
			}
		} catch (final UnauthorizedAccessException e) {
			stb.append(messages.projectAppRemoveErrorMsg);
			resetProjectApplications(mainModel);
		}
		return stb.append("\n").toString();
	}

	/**
	 * Delete user proj app rel.
	 *
	 * @param projectApplicationModel the project application model
	 * @param selectedAdminAreaId the selected admin area id
	 * @return the string
	 * @throws UnauthorizedAccessException the unauthorized access exception
	 */
	private String deleteUserProjAppRel(final ProjectApplicationModel projectApplicationModel,
			final String selectedAdminAreaId) throws UnauthorizedAccessException {
		final UserProjectAppRelController userProjectAppRelController = new UserProjectAppRelController(
				selectedAdminAreaId);
		String userProjAppRelId = Constants.EMPTY_STR;
		final String adminAreaId = projectApplicationModel.getAdminAreaId();
		final String userId = projectApplicationModel.getUserId();
		final String projId = projectApplicationModel.getProjectId();
		UserProjectAppRelResponse userProjectAppRelResponse = userProjectAppRelController
				.getUserProjAppRelsByUserProjAAId(userId, projId, adminAreaId);
		if (userProjectAppRelResponse != null) {
			Iterable<UserProjAppRelTbl> userProjAppRelTbls = userProjectAppRelResponse.getUserProjAppRelTbls();
			for (UserProjAppRelTbl userProjAppRelTbl : userProjAppRelTbls) {
				if (userProjAppRelTbl.getProjectApplicationId().getProjectApplicationId()
						.equals(projectApplicationModel.getProjectAppId())) {
					userProjAppRelId = userProjAppRelTbl.getUserProjAppRelId();
					break;
				}
			}
			boolean deleteUserProjAppRel = userProjectAppRelController.deleteUserProjAppRel(userProjAppRelId);
			LOGGER.info("Inside deleteUserProjAppRel method and executed deleteUserProjAppRel : " + deleteUserProjAppRel);
			return userProjAppRelId;
		}
		return null;
	}

	/**
	 * Update project combo.
	 */
	private void updateProjectCombo() {
		final String userId = currentSelectionModel.getCurrentUserId();
		String currentSiteId = currentSelectionModel.getCurrentSiteId();
		if (!XMSystemUtil.isEmpty(userId) && !XMSystemUtil.isEmpty(currentSiteId)) {
			projectListByUserId = xmHotlineHelper.getProjectsByUserId(userId, currentSiteId);
			if (projectListByUserId != null && projectListByUserId.size() > 0) {
				updateProCompByUserProRel(userId, true, false);
			} else {
				xmHotlineHelper.setProjectToCombo(projectFilterCombo, null);
				setProjectComboEmpty();
			}
		}
	}

	/**
	 * Update admin area combo.
	 *
	 * @param activeShell the active shell
	 * @param siteId the site id
	 */
	private void updateAdminAreaCombo(final Shell activeShell, final String siteId) {
		if (!XMSystemUtil.isEmpty(siteId)) {
			try {
				adminAreaMapBySite.clear();
				AdminAreaResponse adminAreaResponse = adminAreaController.getAABySiteId(siteId);
				if (adminAreaResponse != null) {
					Iterable<AdminAreasTbl> adminAreasTbls = adminAreaResponse.getAdminAreasTbls();
					long size = adminAreasTbls.spliterator().getExactSizeIfKnown();
					if(size > 0) {
						final Map<String, Object> itemMap = new TreeMap<>();
						for (AdminAreasTbl adminAreasTbl : adminAreasTbls) {
							final AdminAreaModel adminAreaModel = xmHotlineHelper.getAdminAreaByTbl(adminAreasTbl);
							if (adminAreaModel != null) {
								itemMap.put(adminAreaModel.getAdminAreaName(), adminAreaModel.getImage());
								adminAreaMapBySite.put(adminAreaModel.getAdminAreaName(), adminAreaModel);
							}
						}
						
						adminAreaFilterCombo.setItemsWithImages(itemMap);
						xmHotlineHelper.setAdminAreaComboItems(itemMap);
						if(adminAreaMapBySite != null) {
							setAdminAreaMapBySite(adminAreaMapBySite);
						}
					} else {
						setAAComboEmpty();
						adminAreaFilterCombo.getTable().removeAll();
						clearProjectComp();
					}
				} else {
					setAAComboEmpty();
					adminAreaFilterCombo.getTable().removeAll();
					clearProjectComp();
				}
			} catch (UnauthorizedAccessException e1) {
				CustomMessageDialog.openError(activeShell, messages.errorDialogTitile, messages.permissionErrorMsg);
			}
		}
	}

	/**
	 * Gets the selected admin area id.
	 *
	 * @return the selected admin area id
	 */
	public String getSelectedAdminAreaId() {
		try {
			int selectionIndex = adminAreaFilterCombo.getSelectionIndex();
			if (selectionIndex != -1) {
				final String adminAreaName = adminAreaFilterCombo.getItem(selectionIndex);
				final SortedMap<String, IXmHotlineModel> adminAreaMap = this.xmHotlineHelper.getAdminAreaMap();
				final AdminAreaModel adminArea = (AdminAreaModel) adminAreaMap.get(adminAreaName);
				if (adminArea != null) {
					String adminAreaId = adminArea.getAdminAreaId();
					currentSelectionModel.setCurrentAdminAreaComboIndex(selectionIndex);
					currentSelectionModel.setCurrentAdminAreaId(adminAreaId);
					currentSelectionModel.setCurrentAdminAreaName(adminAreaName);
					return adminAreaId;
				}
			} else {
				currentSelectionModel.setCurrentAdminAreaComboIndex(-1);
				currentSelectionModel.setCurrentAdminAreaId(null);
				currentSelectionModel.setCurrentAdminAreaName(null);
			}
		} catch (IllegalArgumentException e) {
			CustomMessageDialog.openInformation(Display.getDefault().getActiveShell(), messages.windowtitle,
					messages.adminAreaSelectionMsg);
		}
		return null;
	}
	
	/**
	 * Update admin area model.
	 */
	public void updateAdminAreaModel() {
		try {
			int selectionIndex = adminAreaFilterCombo.getSelectionIndex();
			if (selectionIndex != -1) {
				final String adminAreaName = adminAreaFilterCombo.getItem(selectionIndex);
				final SortedMap<String, IXmHotlineModel> adminAreaMap = this.xmHotlineHelper.getAdminAreaMap();
				final AdminAreaModel adminArea = (AdminAreaModel) adminAreaMap.get(adminAreaName);
				if (adminArea != null) {
					String adminAreaId = adminArea.getAdminAreaId();
					currentSelectionModel.setCurrentAdminAreaComboIndex(selectionIndex);
					currentSelectionModel.setCurrentAdminAreaId(adminAreaId);
					currentSelectionModel.setCurrentAdminAreaName(adminAreaName);
				}
			} else {
				currentSelectionModel.setCurrentAdminAreaComboIndex(-1);
				currentSelectionModel.setCurrentAdminAreaId(null);
				currentSelectionModel.setCurrentAdminAreaName(null);
			}
		} catch (IllegalArgumentException e) {
			CustomMessageDialog.openInformation(Display.getDefault().getActiveShell(), messages.windowtitle,
					messages.adminAreaSelectionMsg);
		}
	}

	/**
	 * Gets the selected site id.
	 *
	 * @return the selected site id
	 */
	public String getSelectedSiteId() {
		try {
			int selectionIndex = siteFilterCombo.getSelectionIndex();
			if (selectionIndex != -1) {
				final String siteName = siteFilterCombo.getItem(selectionIndex);
				final SortedMap<String, IXmHotlineModel> siteMap = this.xmHotlineHelper.getSiteMap();
				final SiteModel siteModel = (SiteModel) siteMap.get(siteName);
				if (siteModel != null) {
					String siteId = siteModel.getSiteId();
					currentSelectionModel.setCurrentSiteComboIndex(selectionIndex);
					currentSelectionModel.setCurentSiteName(siteName);
					currentSelectionModel.setCurrentSiteId(siteId);
					return siteId;
				}
			} else {
				currentSelectionModel.setCurrentSiteComboIndex(-1);
				currentSelectionModel.setCurentSiteName(null);
				currentSelectionModel.setCurrentSiteId(null);
			}
		} catch (IllegalArgumentException e) {
			CustomMessageDialog.openInformation(Display.getDefault().getActiveShell(), messages.windowtitle,
					messages.selectSiteMsg);
		}
		
		return null;
	}

	/**
	 * Gets the selected user id.
	 *
	 * @return the selected user id
	 */
	public String getSelectedUserId() {
		final int selectionIndex = userFilterCombo.getSelectionIndex();
		try {
			if (selectionIndex != -1) {
				final String userName = userFilterCombo.getItem(selectionIndex);
				final SortedMap<String, IXmHotlineModel> userMap = this.xmHotlineHelper.getUsersMap();
				final UserModel userModel = (UserModel) userMap.get(userName);
				final String userId = userModel.getUserId();
				currentSelectionModel.setCurrentUserComboIndex(selectionIndex);
				currentSelectionModel.setCurrentUserId(userId);
				currentSelectionModel.setCurrentUserName(userName);
				return userId;
			} else {
				currentSelectionModel.setCurrentUserComboIndex(-1);
				currentSelectionModel.setCurrentUserId(null);
				currentSelectionModel.setCurrentUserName(null);
			}
		} catch (IllegalArgumentException e) {
			LOGGER.info("Inside method getSelectedUserId, No user selected combo selected index : " + selectionIndex);
		}
		return null;
	}

	/**
	 * Gets the selected project id.
	 *
	 * @return the selected project id
	 */
	public String getSelectedProjectId() {
		int selectionIndex = projectFilterCombo.getSelectionIndex();
		try {
			if (selectionIndex != -1) {
				final String projectName = projectFilterCombo.getItem(selectionIndex);
				final SortedMap<String, IXmHotlineModel> projectMap = this.xmHotlineHelper.getProjectMap();
				final ProjectModel projectModel = (ProjectModel) projectMap.get(projectName);
				final String projectId = projectModel.getProjectId();
				currentSelectionModel.setCurrentProjectComboIndex(selectionIndex);
				currentSelectionModel.setCurrentProjectId(projectId);
				currentSelectionModel.setCurrentProjectName(projectName);
				return projectId;
			} else {
				currentSelectionModel.setCurrentProjectComboIndex(-1);
				currentSelectionModel.setCurrentProjectId(null);
				currentSelectionModel.setCurrentProjectName(null);
			}
		} catch (IllegalArgumentException e) {
			LOGGER.info("Inside method getSelectedProjectId, No project selected combo selected index : " + selectionIndex);
		}
		
		return null;
	}

	/**
	 * Gets the current admin area project relation.
	 *
	 * @param siteAdminAreaRelId the site admin area rel id
	 * @param projectId the project id
	 * @return the current admin area project relation
	 */
	private String getCurrentAdminAreaProjectRelation(final String siteAdminAreaRelId, final String projectId) {
		final AdminAreaProjectRelController controller = new AdminAreaProjectRelController();
		final AdminAreaProjectRelTbl adminAreaProjectRelTbl = controller
				.getAAProjectRelBySiteAdminAreaIdAndProjectId(siteAdminAreaRelId, projectId);
		if (adminAreaProjectRelTbl != null) {
			return adminAreaProjectRelTbl.getAdminAreaProjectRelId();
		}
		
		return null;
	}

	/**
	 * Gets the current project user relation.
	 *
	 * @param userId the user id
	 * @param projectId the project id
	 * @return the current project user relation
	 */
	private String getCurrentProjectUserRelation(final String userId, final String projectId) {
		final UserProjectRelController controller = new UserProjectRelController();
		final UserProjectRelTbl userProjectRelTbl = controller.getUserProjectRelByUserIdAndProjectId(userId, projectId);
		if (userProjectRelTbl != null) {
			return userProjectRelTbl.getUserProjectRelId();
		}
		
		return null;
	}

	/**
	 * Reload.
	 */
	public void reload() {
		final Shell activeShell = Display.getDefault().getActiveShell();
		final Job job = new Job("Reload..") {

			@Override
			protected IStatus run(IProgressMonitor monitor) {
				monitor.beginTask("Reload..", 100);
				monitor.worked(5);
				final MainModel mainModel = xmHotlineHelper.getMainModel();

				// Disable all the combo when reload starts
				setComboAndFilterEnabled(false);

				final String siteName = currentSelectionModel.getCurrentSiteName();
				final String siteId = currentSelectionModel.getCurrentSiteId();
				final String adminAreaName = currentSelectionModel.getCurrentAdminAreaName();
				final String adminAreaId = currentSelectionModel.getCurrentAdminAreaId();
				final String userName = currentSelectionModel.getCurrentUserName();
				final String userId = currentSelectionModel.getCurrentUserId();
				final String projectName = currentSelectionModel.getCurrentProjectName();
				final String projectId = currentSelectionModel.getCurrentProjectId();

				// Relation existence check
				boolean adminAreaSiteRelationExist = xmHotlineHelper.isAdminAreaSiteRelationExist(adminAreaId);
				boolean projectAdminAreaRelationExist = xmHotlineHelper.isProjectAdminAreaRelationExist(projectId, siteId);
				boolean projectUserRelationExist = xmHotlineHelper.isProjectUserRelationExist(projectId, userId);

				// update site, adminArea, user and project map
				Display.getDefault().syncExec(new Runnable() {
					@Override
					public void run() {
						xmHotlineHelper.updateSiteFromService();
						xmHotlineHelper.updateAdminAreaFromService();
						xmHotlineHelper.updateUserMapFromService();
						xmHotlineHelper.updateProjectsFromService();
					}
				});

				if (XMSystemUtil.isEmpty(siteName) || (xmHotlineHelper.getSiteMap().get(siteName) == null)) {
					Display.getDefault().syncExec(new Runnable() {
						public void run() {
							// Clear all the composites and combo items if site
							// is not present (or) Invalid
							clearAllSelectionAndUpdateModel();
							setSiteComboAndFilterEnabled(true);
						};
					});
					return Status.OK_STATUS;
				}

				if (XMSystemUtil.isEmpty(adminAreaName)
						|| (xmHotlineHelper.getAdminAreaMap().get(adminAreaName) == null) || !adminAreaSiteRelationExist) {
					Display.getDefault().syncExec(new Runnable() {
						@Override
						public void run() {
							clearAdminAreaAndUpdateModel();
							final String userId = currentSelectionModel.getCurrentUserId();
							if (userId != null) {
								projectListBySiteId = xmHotlineHelper.getProjetsBySiteId(siteId);
								if (projectListBySiteId != null && projectListBySiteId.size() > 0) {
									createControl(projectInfoScrollComp, projectComp, projectListBySiteId);
									projectListByUserId = xmHotlineHelper.getProjectsByUserId(userId, siteId);
									if (projectListByUserId != null && projectListByUserId.size() > 0) {
										updateProCompByUserProRel(userId, false, true);
									}
								} else {
									CustomMessageDialog.openInformation(activeShell, messages.windowtitle,
											messages.projNotFoundForSiteMsg + currentSelectionModel.getCurrentSiteName() + ".");
									clearProjectComp();
								}
							} else {
								projectListBySiteId = xmHotlineHelper.getProjetsBySiteId(siteId);
								if (projectListBySiteId != null && projectListBySiteId.size() > 0) {
									createControl(projectInfoScrollComp, projectComp, projectListBySiteId);
								} else {
									clearProjectComp();
								}
							}
							projectInfoGroup.layout(true);
							setSiteComboAndFilterEnabled(true);
							setAdminAreaComboAndFilterEnabled(true);
							setUserComboAndFilterEnabled(true);
							clearUserAppComp();
							clearProjectAppComp();
						}
					});
				}

				if (XMSystemUtil.isEmpty(userName) || (xmHotlineHelper.getUsersMap().get(userName) == null)) {
					Display.getDefault().syncExec(new Runnable() {

						@Override
						public void run() {
							clearUserAndUpdateModel();
							projectListByAAId = xmHotlineHelper.getProjetsByAdminAreaId(adminAreaId);
							if (projectListByAAId != null && projectListByAAId.size() > 0) {
								createControl(projectInfoScrollComp, projectComp, projectListByAAId);
								projectInfoGroup.layout(true);
							}
							setSiteComboAndFilterEnabled(true);
							setAdminAreaComboAndFilterEnabled(true);
							setUserComboAndFilterEnabled(true);
							updateAdminAreaCombo(activeShell, siteId);
							updateAllComboImages(siteName, adminAreaName, userName, projectName);
						}
					});
					return Status.OK_STATUS;
				} else {
					Display.getDefault().syncExec(new Runnable() {
						@Override
						public void run() {
							updateAdminAreaCombo(activeShell, siteId);
							enableProjectCompObjects(true);
							projectListByAAId = xmHotlineHelper.getProjetsByAdminAreaId(adminAreaId);
							if (projectListByAAId != null && projectListByAAId.size() > 0) {
								createControl(projectInfoScrollComp, projectComp, projectListByAAId);
								projectInfoGroup.layout(true);
								enableProjectCompObjects(true);
								clearUserAppComp();
								projectListByUserId = xmHotlineHelper.getProjectsByUserId(userId, siteId);
								if (projectListByUserId != null && projectListByUserId.size() > 0) {
									updateProCompByUserProRel(userId, false, true);
								}
								userApplList = xmHotlineHelper.getUserApplicationByAAId(adminAreaId, userId);
								List<UserUserAppRelation> userUserAppRelation = xmHotlineHelper.getUserUserAppRelsByUserIdAAId(userId, adminAreaId);
								if (userApplList != null && userApplList.size() > 0) {
									loadUserAppComposite(activeShell, userName, userUserAppRelation);
								}
							} else {
								projectListBySiteId = xmHotlineHelper.getProjetsBySiteId(siteId);
								if (projectListBySiteId != null && projectListBySiteId.size() > 0) {
									createControl(projectInfoScrollComp, projectComp, projectListBySiteId);
								} else {
									clearProjectComp();
								}
								projectListByUserId = xmHotlineHelper.getProjectsByUserId(userId, siteId);
								updateProCompByUserProRel(userId, false, true);
							}
						}
					});
				}

				if (XMSystemUtil.isEmpty(projectName) || (xmHotlineHelper.getProjectMap().get(projectName) == null)) {
					Display.getDefault().syncExec(new Runnable() {
						@Override
						public void run() {
							clearProjectAndUpdateModel();
						}
					});
				} else if (!projectAdminAreaRelationExist) {
					Display.getDefault().asyncExec(new Runnable() {
						@Override
						public void run() {
							CustomMessageDialog.openError(activeShell, messages.errorDialogTitile,
									"Relation between the project '" + projectName + "' and admin area '" + adminAreaName + "' does not exist.");
							clearProjectAndUpdateModel();
						}
					});
				} else if (!projectUserRelationExist) {
					Display.getDefault().asyncExec(new Runnable() {
						@Override
						public void run() {
							CustomMessageDialog.openError(activeShell, messages.errorDialogTitile,
									"Relation between the project '" + projectName + "' and user '" + userName + "' does not exist.");
							clearProjectAndUpdateModel();
						}
					});
				} else {
					Display.getDefault().syncExec(new Runnable() {
						@Override
						public void run() {
							clearProjectAppComp();
							projectApplList = xmHotlineHelper.getProjectApplicationByAAIdandProjectId(adminAreaId,
									projectId, userId);
							Iterable<UserProjAppRelTbl> userProjAppRelsByUserProjAAId = xmHotlineHelper
									.getUserProjAppRelsByUserProjAAId(userId, projectId, adminAreaId);
							if (projectApplList != null && projectApplList.size() > 0) {
								loadProjectAppComposite(activeShell, userProjAppRelsByUserProjAAId);
							}
						}
					});
				}

				// Remove the unsaved data from the model on reload
				Display.getDefault().asyncExec(new Runnable() {
					@Override
					public void run() {
						resetProjectUserRelation(mainModel);
						resetUserApplications(mainModel);
						resetProjectApplications(mainModel);
						updateAllComboImages(siteName, adminAreaName, userName, projectName);
					}
				});
				
				// Enable all the combo when reload ends
				setComboAndFilterEnabled(true);
				monitor.worked(80);
				return Status.OK_STATUS;
			}
		};
		job.schedule();
	}

	/**
	 * Update all combo images.
	 *
	 * @param siteName the site name
	 * @param adminAreaName the admin area name
	 * @param userName the user name
	 * @param projectName the project name
	 */
	private void updateAllComboImages(final String siteName, final String adminAreaName, final String userName,
			final String projectName) {
		if (siteFilterCombo.getSelectionIndex() != -1) {
			IXmHotlineModel iXmHotlineModel = xmHotlineHelper.getSiteMap().get(siteName);
			siteFilterCombo.updateSelectedImage(((SiteModel) iXmHotlineModel).getImage());
		}

		if (adminAreaFilterCombo.getSelectionIndex() != -1) {
			IXmHotlineModel iXmHotlineModel = getAdminAreaMapBySite(adminAreaName);
			adminAreaFilterCombo.updateSelectedImage(((AdminAreaModel) iXmHotlineModel).getImage());
		}

		if (userFilterCombo.getSelectionIndex() != -1) {
			IXmHotlineModel iXmHotlineModel = xmHotlineHelper.getUsersMap().get(userName);
			userFilterCombo.updateSelectedImage(((UserModel) iXmHotlineModel).getImage());
		}

		if (projectFilterCombo.getSelectionIndex() != -1) {
			IXmHotlineModel iXmHotlineModel = xmHotlineHelper.getProjectMap().get(projectName);
			Image projectImage = xmHotlineHelper.getImage(((ProjectModel) iXmHotlineModel).getIcon(),
					((ProjectModel) iXmHotlineModel).isStatus());
			projectFilterCombo.updateSelectedImage(projectImage);
		}
	}
	
	/**
	 * Clear all selection and update model.
	 */
	private void clearAllSelectionAndUpdateModel() {

		// remove all the combo selection and update model
		setSiteComboEmpty();
		setAAComboEmpty();
		setUserComboEmpty();
		setProjectComboEmpty();
		
		// Set updated site objects to combo from siteMap
		siteFilterCombo.setItemsWithImages(xmHotlineHelper.getSiteComboItems());

		// Disable the adminArea, user and project combo
		setAdminAreaComboAndFilterEnabled(false);
		setUserComboAndFilterEnabled(false);
		setProjectComboAndFilterEnabled(false);

		// clear all composite data
		clearAllCompsites();
	}
	
	/**
	 * Clear user and update model.
	 */
	private void clearUserAndUpdateModel() {
		setUserComboEmpty();
		setProjectComboEmpty();
		
		userFilterCombo.setItemsWithImages(xmHotlineHelper.getUserComboItems());

		// Disable project combo when no user is selected
		projectFilterText.setEditable(false);
		projectFilterButton.setEnabled(false);
		projectFilterCombo.setEnabled(false);

		clearUserAppComp();
		clearProjectAppComp();		
	}
	
	/**
	 * Clear admin area and update model.
	 */
	private void clearAdminAreaAndUpdateModel() {
		setAAComboEmpty();
		adminAreaFilterCombo.setItemsWithImages(xmHotlineHelper.getAdminAreaComboItems());
		setProjectComboEmpty();
		clearUserAppComp();
		clearProjectAppComp();
	}
	
	/**
	 * Clear project and update model.
	 */
	private void clearProjectAndUpdateModel() {
		setProjectComboEmpty();
		clearProjectAppComp();
	}

	/**
	 * Clear user app comp.
	 */
	public void clearUserAppComp() {
		clearComposite(fixUserApplScrollComp, fixedUserAppComo);
		fixUserAppInfoGroup.layout(true);

		clearComposite(notFixUserAppScrollComp, notFixedUserAppComp);
		nonFixUserAppInfoGroup.layout(true);

		clearComposite(protectedUserAppScrollComp, protectedUserAppComp);
		protectedUserAppInfoGroup.layout(true);
	}

	/**
	 * Clear project comp.
	 */
	public void clearProjectComp() {
		clearComposite(projectInfoScrollComp, projectComp);
		projectInfoGroup.layout(true);
	}

	/**
	 * Clear project app comp.
	 */
	public void clearProjectAppComp() {
		clearComposite(fixProjApplScrollComp, fixedProjectAppComp);
		fixProjAppInfoGroup.layout(true);

		clearComposite(notFixProjAppScrollComp, notFixedProjectAppComp);
		nonFixProjAppInfoGroup.layout(true);

		clearComposite(protectedProjAppScrollComp, protectedProjectAppComp);
		protectedProjAppInfoGroup.layout(true);
	}

	/**
	 * Clear composite.
	 *
	 * @param scrollComp the scroll comp
	 * @param composite the composite
	 */
	public void clearComposite(final ScrolledComposite scrollComp, final Composite composite) {
		scrollComp.setRedraw(false);

		// remove exiting model objects and dispose existing control.
		Control[] children = composite.getChildren();
		for (int i = 0; i < children.length; i++) {
			CustomLabelComposite comp = (CustomLabelComposite) children[i];
			comp.dispose();
		}

		// add create new control with updated list.
		scrollComp.setContent(composite);
		Point point = composite.computeSize(SWT.DEFAULT, SWT.DEFAULT);
		composite.setSize(point);
		scrollComp.setMinSize(point);

		composite.setBackgroundMode(SWT.INHERIT_FORCE);
		scrollComp.setBackgroundMode(SWT.INHERIT_FORCE);

		scrollComp.setRedraw(true);
	}

	/**
	 * Select site.
	 *
	 * @param activeShell the active shell
	 * @param siteId the site id
	 */
	private void selectSite(final Shell activeShell, final String siteId) {
		long getProjectsBySiteStart = System.currentTimeMillis();
		projectListBySiteId = xmHotlineHelper.getProjetsBySiteId(siteId);
		long getProjectsBySiteEnd = System.currentTimeMillis();
		LOGGER.debug("Service call: Time taken for getting projects by site ID: " + (getProjectsBySiteEnd - getProjectsBySiteStart) + "ms");
		if (projectListBySiteId != null && projectListBySiteId.size() > 0) {
			long loadingProjectsInUiStart = System.currentTimeMillis();
			createControl(projectInfoScrollComp, projectComp, projectListBySiteId);
			projectInfoGroup.layout(true);
			long loadingProjectsInUIEnd = System.currentTimeMillis();
			LOGGER.debug("UI Operation: Time taken for loading projects in UI: " + (loadingProjectsInUIEnd - loadingProjectsInUiStart) + "ms");
		} else {
			CustomMessageDialog.openInformation(activeShell, messages.windowtitle,
					messages.projNotFoundForSiteMsg + currentSelectionModel.getCurrentSiteName() + ".");
			clearProjectComp();
		}
		
		long upadateAAComboStart = System.currentTimeMillis();
		updateAdminAreaCombo(activeShell, siteId);
		long upadateAAComboEnd = System.currentTimeMillis();
		LOGGER.debug("Service call: Time taken for updating AA based on site selection: " + (upadateAAComboEnd - upadateAAComboStart) + "ms");
		setUserComboAndFilterEnabled(true);
		setAAComboEmpty();
		clearUserAppComp();
		setProjectComboEmpty();
		clearProjectAppComp();
		projectInfoScrollComp.setFocus();
		int selectionIndex = siteFilterCombo.getSelectionIndex();
		if (selectionIndex != -1) {
			String siteName = siteFilterCombo.getItem(selectionIndex);
			updateStatusBar(siteName);
		}
		
		final String userId = currentSelectionModel.getCurrentUserId();
		if(userId != null && !XMSystemUtil.isEmpty(userId)){
			long getProjectsByUserIdStart = System.currentTimeMillis();
			projectListByUserId = xmHotlineHelper.getProjectsByUserId(userId, siteId);
			long getProjectsByUserIdEnd = System.currentTimeMillis();
			LOGGER.debug("Service call: Time taken for Get projects by user id: " + (getProjectsByUserIdEnd - getProjectsByUserIdStart) + "ms");
			if (projectListByUserId != null && projectListByUserId.size() > 0) {
				long updateProCompBUserStart = System.currentTimeMillis();
				updateProCompByUserProRel(userId, true, true);
				long updateProCompBUserEnd = System.currentTimeMillis();
				LOGGER.debug("UI Operation: Time taken for updating project composite based on user id: " + (updateProCompBUserEnd - updateProCompBUserStart) + "ms");
			} else {
				CustomMessageDialog.openInformation(activeShell, messages.windowtitle,
						messages.projNotFoundMsg + " " + currentSelectionModel.getCurrentUserName() + ".");
				enableProjectObjectsInComp();
				xmHotlineHelper.setProjectToCombo(projectFilterCombo, null);
				projectInfoGroup.layout(true);
			}
		} else {
			enableProjectCompObjects(false);
		}
	}

	/**
	 * Set admin area.
	 */
	private void setAdminArea() {
		if (projectListByAAId != null && projectListByAAId.size() > 0) {
			xmHotlineHelper.setProjectToCombo(projectFilterCombo, projectListByAAId);
			createControl(projectInfoScrollComp, projectComp, projectListByAAId);
			projectInfoGroup.layout(true);
			return;
		}
		clearProjectComp();
	}

	/**
	 * Clear all compsites.
	 */
	public void clearAllCompsites() {
		clearProjectComp();
		clearUserAppComp();
		clearProjectAppComp();
	}

	/**
	 * Checks if is user allowed.
	 *
	 * @return true, if is user allowed
	 */
	protected boolean isUserAllowed() {
		boolean checkHotlinePermission = false;
		ValidationController valController = new ValidationController(currentSelectionModel.getCurrentAdminAreaId());
		checkHotlinePermission = valController.checkHotlinePermission();
		return checkHotlinePermission;
	}
	
	/**
	 * Sets the project combo and filter enabled.
	 *
	 * @param enabled the new project combo and filter enabled
	 */
	private void setProjectComboAndFilterEnabled(final boolean enabled) {
		Display.getDefault().syncExec(new Runnable() {

			@Override
			public void run() {
				projectFilterText.setEditable(enabled);
				projectFilterButton.setEnabled(enabled);
				projectFilterCombo.setEnabled(enabled);
			}
		});
	}
	
	/**
	 * Sets the site combo and filter enabled.
	 *
	 * @param enabled the new site combo and filter enabled
	 */
	private void setSiteComboAndFilterEnabled(final boolean enabled) {
		Display.getDefault().syncExec(new Runnable() {

			@Override
			public void run() {
				siteFilterText.setEditable(enabled);
				siteFilterButton.setEnabled(enabled);
				siteFilterCombo.setEnabled(enabled);
			}
		});
	}
	
	/**
	 * Sets the admin area combo and filter enabled.
	 *
	 * @param enabled the new admin area combo and filter enabled
	 */
	private void setAdminAreaComboAndFilterEnabled(final boolean enabled) {
		Display.getDefault().syncExec(new Runnable() {

			@Override
			public void run() {
				adminAreaFilterText.setEditable(enabled);
				adminAreaFilterButton.setEnabled(enabled);
				adminAreaFilterCombo.setEnabled(enabled);
			}
		});
	}
	
	/**
	 * Sets the user combo and filter enabled.
	 *
	 * @param enabled the new user combo and filter enabled
	 */
	private void setUserComboAndFilterEnabled(final boolean enabled) {
		Display.getDefault().syncExec(new Runnable() {

			@Override
			public void run() {
				userFilterText.setEditable(enabled);
				userFilterButton.setEnabled(enabled);
				userFilterCombo.setEnabled(enabled);
			}
		});
	}
	
	/**
	 * Sets the combo and filter enabled.
	 *
	 * @param enabled the new combo and filter enabled
	 */
	private void setComboAndFilterEnabled(final boolean enabled) {
		setSiteComboAndFilterEnabled(enabled);
		setAdminAreaComboAndFilterEnabled(enabled);
		setUserComboAndFilterEnabled(enabled);
		setProjectComboAndFilterEnabled(enabled);
	}
	
	/**
	 * Load user app composite.
	 *
	 * @param activeShell the active shell
	 * @param userName the user name
	 * @param userUserAppRelation the user user app relation
	 */
	private void loadUserAppComposite(final Shell activeShell, final String userName,
			final List<UserUserAppRelation> userUserAppRelation) {
		List<IXmHotlineModel> fixedUserAppList = new ArrayList<IXmHotlineModel>();
		List<IXmHotlineModel> notFixedUserAppList = new ArrayList<IXmHotlineModel>();
		List<IXmHotlineModel> protectedUserAppList = new ArrayList<IXmHotlineModel>();

		for (IXmHotlineModel userAppModel : userApplList) {
			UserApplicationModel userApp = (UserApplicationModel) userAppModel;

			String relType = userApp.getRelType();
			if (ApplicationRelationType.FIXED.name().equalsIgnoreCase(relType)) {
				fixedUserAppList.add(userApp);
			} else if (ApplicationRelationType.NOTFIXED.name().equalsIgnoreCase(relType)) {
				notFixedUserAppList.add(userApp);
			} else if (ApplicationRelationType.PROTECTED.name().equalsIgnoreCase(relType)) {
				protectedUserAppList.add(userApp);
			} else {
				CustomMessageDialog.openInformation(activeShell, messages.windowtitle, messages.projNotFoundMsg + userName + ".");
			}
		}
		if (fixedUserAppList.size() > 0) {
			createControl(fixUserApplScrollComp, fixedUserAppComo, fixedUserAppList);
			fixUserAppInfoGroup.layout(true);
		}
		if (notFixedUserAppList.size() > 0) {
			createControl(notFixUserAppScrollComp, notFixedUserAppComp, notFixedUserAppList);
			nonFixUserAppInfoGroup.layout(true);
		}
		if (protectedUserAppList.size() > 0) {
			createControl(protectedUserAppScrollComp, protectedUserAppComp, protectedUserAppList);
			protectedUserAppInfoGroup.layout(true);
		}
		if (userUserAppRelation != null && userUserAppRelation.size() > 0) {
			changeUserAppSelection(userUserAppRelation);
		}
	}
	
	/**
	 * Load project app composite.
	 *
	 * @param activeShell the active shell
	 * @param userProjAppRelsByUserProjAAId the user proj app rels by user proj AA id
	 */
	private void loadProjectAppComposite(final Shell activeShell,
			final Iterable<UserProjAppRelTbl> userProjAppRelsByUserProjAAId) {
		List<IXmHotlineModel> fixedProjectAppList = new ArrayList<IXmHotlineModel>();
		List<IXmHotlineModel> notFixedProjectAppList = new ArrayList<IXmHotlineModel>();
		List<IXmHotlineModel> protectedProjectAppList = new ArrayList<IXmHotlineModel>();

		for (IXmHotlineModel projectAppModel : projectApplList) {
			ProjectApplicationModel projectApp = (ProjectApplicationModel) projectAppModel;
			final String relType = projectApp.getRelType();
			if (ApplicationRelationType.FIXED.name().equalsIgnoreCase(relType)) {
				fixedProjectAppList.add(projectApp);
			} else if (ApplicationRelationType.NOTFIXED.name().equalsIgnoreCase(relType)) {
				notFixedProjectAppList.add(projectApp);
			} else if (ApplicationRelationType.PROTECTED.name().equalsIgnoreCase(relType)) {
				protectedProjectAppList.add(projectApp);
			} else {
				CustomMessageDialog.openError(activeShell, messages.errorDialogTitile,
						messages.undefProjAppMsg);
			}
		}

		if (fixedProjectAppList.size() > 0) {
			createControl(fixProjApplScrollComp, fixedProjectAppComp, fixedProjectAppList);
			fixProjAppInfoGroup.layout(true);
		}

		if (notFixedProjectAppList.size() > 0) {
			createControl(notFixProjAppScrollComp, notFixedProjectAppComp, notFixedProjectAppList);
			nonFixProjAppInfoGroup.layout(true);
		}

		if (protectedProjectAppList.size() > 0) {
			createControl(protectedProjAppScrollComp, protectedProjectAppComp, protectedProjectAppList);
			protectedProjAppInfoGroup.layout(true);
		}
		if (userProjAppRelsByUserProjAAId != null && userProjAppRelsByUserProjAAId.iterator().hasNext()) {
			changeProjAppSelection(userProjAppRelsByUserProjAAId);
		}
	}
	
	/**
	 * Update pro comp by user pro rel.
	 *
	 * @param userId the user id
	 * @param updateCombo the update combo
	 * @param updateSelection the update selection
	 * @return the list
	 */
	private List<IXmHotlineModel> updateProCompByUserProRel(final String userId, final boolean updateCombo,
			final boolean updateSelection) {
		final List<IXmHotlineModel> projectsAssignedToSelectedUser = new ArrayList<>();

		if (userId != null) {
			final Control[] children = projectComp.getChildren();
			Map<ProjectModel, ProjectModel> selectedMap = new HashMap<>();
			for (final Control control : children) {
				final CustomLabelComposite comp = (CustomLabelComposite) control;
				final IXmHotlineModel model = comp.getModel();
				if (model instanceof ProjectModel) {
					final ProjectModel projectModel = (ProjectModel) model;
					comp.setEnabled(true);

					if(projectListByUserId != null){
						for (IXmHotlineModel ixmHotlineModel : projectListByUserId) {
							if (ixmHotlineModel instanceof ProjectModel) {
								if (((ProjectModel) ixmHotlineModel).getProjectId().equals(projectModel.getProjectId())) {
									selectedMap.put(projectModel, (ProjectModel) ixmHotlineModel);
									break;
								}
							}
						}
					}
				}
			}

			for (final Control control : children) {
				final CustomLabelComposite comp = (CustomLabelComposite) control;

				final IXmHotlineModel model = comp.getModel();

				if (model instanceof ProjectModel) {
					final ProjectModel projectModel = (ProjectModel) model;
					if (selectedMap.containsKey(projectModel)) {
						ProjectModel userProjectModel = selectedMap.get(projectModel);
						Image image = xmHotlineHelper.getImage(userProjectModel.getIcon(), userProjectModel.isStatus());
						comp.updateWidgetImage(image);
						projectModel.setUserId(userId);
						projectModel.setUserName(currentSelectionModel.getCurrentUserName());
						if (updateSelection) {
							comp.setSelection(true);
						}
						projectsAssignedToSelectedUser.add(projectModel);
					} else {
						projectModel.setUserId(null);
						projectModel.setUserName(null);
						if (updateSelection) {
							comp.setSelection(false);
						}
						projectsAssignedToSelectedUser.remove(projectModel);
					}
				}
			}
		}

		if (updateCombo) {
			xmHotlineHelper.setProjectToCombo(projectFilterCombo, projectsAssignedToSelectedUser);
			projectInfoGroup.layout(true);
		}

		return projectsAssignedToSelectedUser;
	}
	
	/**
	 * Set site combo empty.
	 */
	private void setSiteComboEmpty(){
		siteFilterCombo.select(-1);
		currentSelectionModel.setCurrentSiteId(null);
		currentSelectionModel.setCurentSiteName(null);
		currentSelectionModel.setCurrentSiteComboIndex(-1);
	}
	
	/**
	 * Set AA combo empty.
	 */
	private void setAAComboEmpty(){
		adminAreaFilterCombo.select(-1);
		currentSelectionModel.setCurrentAdminAreaId(null);
		currentSelectionModel.setCurrentAdminAreaName(null);
		currentSelectionModel.setCurrentAdminAreaComboIndex(-1);
	}
	
	/**
	 * Set user combo empty.
	 */
	private void setUserComboEmpty(){
		userFilterCombo.select(-1);
		currentSelectionModel.setCurrentUserId(null);
		currentSelectionModel.setCurrentUserName(null);
		currentSelectionModel.setCurrentUserComboIndex(-1);
	}
	
	/**
	 * Set project combo empty.
	 */
	private void setProjectComboEmpty(){
		projectFilterCombo.select(-1);
		currentSelectionModel.setCurrentProjectId(null);
		currentSelectionModel.setCurrentProjectName(null);
		currentSelectionModel.setCurrentProjectComboIndex(-1);
	}

	/**
	 * Gets the admin area map by site.
	 *
	 * @return the admin area map by site
	 */
	private IXmHotlineModel getAdminAreaMapBySite(final String adminAreaName) {
		return adminAreaMapBySite.get(adminAreaName);
	}

	/**
	 * Sets the admin area map by site.
	 *
	 * @param adminAreaMapBySite the admin area map by site
	 */
	public void setAdminAreaMapBySite(Map<String, IXmHotlineModel> adminAreaMapBySite) {
		this.adminAreaMapBySite = adminAreaMapBySite;
	}

}
