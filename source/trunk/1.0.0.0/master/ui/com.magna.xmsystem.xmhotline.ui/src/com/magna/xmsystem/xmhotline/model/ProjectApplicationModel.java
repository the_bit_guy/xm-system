/**
 * 
 */
package com.magna.xmsystem.xmhotline.model;

import java.util.Map;

import org.eclipse.swt.graphics.Image;

import com.magna.xmsystem.dependencies.utils.LANG_ENUM;
import com.magna.xmsystem.xmhotline.model.interfaces.IXmHotlineModel;

/**
 * This class contains project Application id, name, type(FIXED, NOT_FIXED, PROTECTED), status and Image.
 */
public class ProjectApplicationModel implements IXmHotlineModel {

	/** Member variable 'projectAppId' for {@link ProjectApplicationModel}. */
	private String projectAppId;
	
	/** The project app internal name. */
	private String projectAppInternalName;
	
	/** The project app internal desc. */
	private String projectAppInternalDesc;
	
	/** Member variable 'relType' for {@link ProjectApplicationModel}. */
	private String relType;
	
	/** Member variable 'status' for {@link ProjectApplicationModel}. */
	private boolean status;
	
	/** Member variable 'image' for {@link ProjectApplicationModel}. */
	private Image image;

	/** The admin area id. */
	private String adminAreaId;

	/** The user id. */
	private String userId;

	/** The project id. */
	private String projectId;

	/** The project app name map. */
	private Map<LANG_ENUM, String> projectAppNameMap;

	/** The project app desc map. */
	private Map<LANG_ENUM, String> projectAppDescMap;
	
	/**
	 * Constructor.
	 *
	 * @param projectAppId the project app id
	 * @param projectAppName the project app name
	 * @param relType            project application type
	 * @param status            project application status
	 * @param image            project application image
	 * @param adminAreaId the admin area id
	 * @param userId the user id
	 * @param projectId the project id
	 * @param projectAppNameMap the project app name map
	 * @param projectAppDescMap the project app desc map
	 */
	public ProjectApplicationModel(final String projectAppId, final String projectAppInternalName, final String projectAppInternalDesc, final String relType, 
			final boolean status, final Image image, final String adminAreaId, final String userId, final String projectId, 
			final Map<LANG_ENUM, String> projectAppNameMap, final Map<LANG_ENUM, String> projectAppDescMap) {
		this.projectAppId = projectAppId;
		this.projectAppInternalName = projectAppInternalName;
		this.projectAppInternalDesc = projectAppInternalDesc;
		this.relType = relType;
		this.status = status;
		this.image = image;
		this.adminAreaId = adminAreaId;
		this.userId = userId;
		this.projectId = projectId;
		this.projectAppNameMap = projectAppNameMap;
		this.projectAppDescMap = projectAppDescMap;
	}

	/**
	 * Gets the project app id.
	 *
	 * @return the projectApplicationId
	 */
	public final String getProjectAppId() {
		return projectAppId;
	}

	/**
	 * Sets the project app id.
	 *
	 * @param projectAppId the projectAppId  to set
	 */
	public final void setProjectAppId(final String projectAppId) {
		this.projectAppId = projectAppId;
	}

	/**
	 * @return the projectAppInternalName
	 */
	public String getProjectAppInternalName() {
		return projectAppInternalName;
	}

	/**
	 * @param projectAppInternalName the projectAppInternalName to set
	 */
	public void setProjectAppInternalName(String projectAppInternalName) {
		this.projectAppInternalName = projectAppInternalName;
	}

	/**
	 * @return the projectAppInternalDesc
	 */
	public String getProjectAppInternalDesc() {
		return projectAppInternalDesc;
	}

	/**
	 * @param projectAppInternalDesc the projectAppInternalDesc to set
	 */
	public void setProjectAppInternalDesc(String projectAppInternalDesc) {
		this.projectAppInternalDesc = projectAppInternalDesc;
	}

	/**
	 * Gets the rel type.
	 *
	 * @return the relType
	 */
	public final String getRelType() {
		return relType;
	}

	/**
	 * Sets the rel type.
	 *
	 * @param relType the new rel type
	 */
	public final void setRelType(final String relType) {
		this.relType = relType;
	}

	/**
	 * Checks if is status.
	 *
	 * @return the status
	 */
	public final boolean isStatus() {
		return status;
	}

	/**
	 * Sets the status.
	 *
	 * @param status the status to set
	 */
	public final void setStatus(final boolean status) {
		this.status = status;
	}

	/**
	 * Gets the image.
	 *
	 * @return the image
	 */
	public final Image getImage() {
		return image;
	}

	/**
	 * Sets the image.
	 *
	 * @param image the image to set
	 */
	public final void setImage(final Image image) {
		this.image = image;
	}

	/**
	 * Gets the admin area id.
	 *
	 * @return the adminAreaId
	 */
	public String getAdminAreaId() {
		return adminAreaId;
	}

	/**
	 * Sets the admin area id.
	 *
	 * @param adminAreaId the adminAreaId to set
	 */
	public void setAdminAreaId(String adminAreaId) {
		this.adminAreaId = adminAreaId;
	}

	/**
	 * Gets the user id.
	 *
	 * @return the userId
	 */
	public String getUserId() {
		return userId;
	}

	/**
	 * Sets the user id.
	 *
	 * @param userId the userId to set
	 */
	public void setUserId(String userId) {
		this.userId = userId;
	}

	/**
	 * Gets the project id.
	 *
	 * @return the projectId
	 */
	public String getProjectId() {
		return projectId;
	}

	/**
	 * Sets the project id.
	 *
	 * @param projectId the projectId to set
	 */
	public void setProjectId(String projectId) {
		this.projectId = projectId;
	}

	/**
	 * Gets the project app name map.
	 *
	 * @return the projectAppNameMap
	 */
	public Map<LANG_ENUM, String> getProjectAppNameMap() {
		return projectAppNameMap;
	}

	/**
	 * Sets the project app name map.
	 *
	 * @param projectAppNameMap the projectAppNameMap to set
	 */
	public void setProjectAppNameMap(Map<LANG_ENUM, String> projectAppNameMap) {
		this.projectAppNameMap = projectAppNameMap;
	}

	/**
	 * Gets the project app desc map.
	 *
	 * @return the projectAppDescMap
	 */
	public Map<LANG_ENUM, String> getProjectAppDescMap() {
		return projectAppDescMap;
	}

	/**
	 * Sets the project app desc map.
	 *
	 * @param projectAppDescMap the projectAppDescMap to set
	 */
	public void setProjectAppDescMap(Map<LANG_ENUM, String> projectAppDescMap) {
		this.projectAppDescMap = projectAppDescMap;
	}
	
}
