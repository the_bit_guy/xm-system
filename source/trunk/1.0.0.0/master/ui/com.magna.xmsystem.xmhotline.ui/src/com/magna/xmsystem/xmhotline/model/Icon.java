package com.magna.xmsystem.xmhotline.model;

import java.beans.PropertyChangeEvent;

/**
 * Model class icon.
 *
 * @author shashwat.anand
 */
public class Icon extends BeanModel {
	/**
	 * PROPERTY_ICONID constant
	 */
	public static final String PROPERTY_ICONID = "iconId"; //$NON-NLS-1$
	
	/**
	 * PROPERTY_ICONNAME constant
	 */
	public static final String PROPERTY_ICONNAME = "iconName"; //$NON-NLS-1$
	
	/** The Constant PROPERTY_ICONTYPE. */
	public static final String PROPERTY_ICONTYPE = "iconType"; //$NON-NLS-1$

	/** Member variable for iconId. */
	private String iconId;

	/** Member variable for iconName. */
	private String iconName;

	/** Member variable for iconPath. */
	private String iconPath;
	
	/** Member variable 'icon type' for {@link String}. */
	private String iconType;

	
	/**
	 * Constructor for Icon Class.
	 *
	 * @param iconId {@link String}
	 * @param iconName {@link String}
	 * @param iconPath {@link String}
	 * @param iconType {@link String}
	 */
	public Icon(final String iconId, final String iconName, final String iconPath, String iconType) {
		this.iconId = iconId;
		this.iconName = iconName;
		this.iconPath = iconPath;
		this.iconType = iconType;
	}

	/**
	 * Constructor for Icon Class.
	 */
	public Icon() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * Gets the icon id.
	 *
	 * @return the iconId
	 */
	public String getIconId() {
		return iconId;
	}

	/**
	 * Sets the icon id.
	 *
	 * @param iconId
	 *            the iconId to set
	 */
	public void setIconId(final String iconId) {
		if (iconId == null) throw new IllegalArgumentException();
		this.propertyChangeSupport.firePropertyChange(PROPERTY_ICONID, this.iconId, this.iconId = iconId);
	}

	/**
	 * Gets the icon name.
	 *
	 * @return the iconName
	 */
	public String getIconName() {
		return iconName;
	}

	/**
	 * Sets the icon name.
	 *
	 * @param iconName
	 *            the iconName to set
	 */
	public void setIconName(final String iconName) {
		if (iconName == null) throw new IllegalArgumentException();
		this.propertyChangeSupport.firePropertyChange(PROPERTY_ICONNAME, this.iconName, this.iconName = iconName);
	}

	/**
	 * Gets the icon path.
	 *
	 * @return the iconPath
	 */
	public String getIconPath() {
		return iconPath;
	}

	/**
	 * Sets the icon path.
	 *
	 * @param iconPath
	 *            the iconPath to set
	 */
	public void setIconPath(final String iconPath) {
		this.iconPath = iconPath;
	}

	/**
	 * @return the iconType
	 */
	public String getIconType() {
		return iconType;
	}

	/**
	 * @param iconType the iconType to set
	 */
	public void setIconType(String iconType) {
		this.iconType = iconType;
	}

	/**
	 * Method for property change.
	 *
	 * @param event
	 *            {@link PropertyChangeEvent}
	 */
	@Override
	public void propertyChange(PropertyChangeEvent event) {
		this.propertyChangeSupport.firePropertyChange(event.getPropertyName(), event.getOldValue(), event.getNewValue());
	}
}
