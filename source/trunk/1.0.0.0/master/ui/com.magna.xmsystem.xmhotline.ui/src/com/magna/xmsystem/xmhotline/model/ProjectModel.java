package com.magna.xmsystem.xmhotline.model;

import java.util.Map;

import com.magna.xmsystem.dependencies.utils.LANG_ENUM;
import com.magna.xmsystem.xmhotline.model.interfaces.IXmHotlineModel;

/**
 * 
 * This class contains project id, name, image and status.
 * 
 */
public class ProjectModel implements IXmHotlineModel {
	
	/** Member variable 'Id' for {@link ProjectModel}. */
	private String projectId;
	
	/** Member variable 'projectName' for {@link ProjectModel}. */
	private String projectName;
	
	/** The icon. */
	private Icon icon;
	
	/** Member variable 'status' for {@link ProjectModel}. */
	private boolean status;
	
	/** The user id. */
	private String userId;
	
	/** The user name. */
	private String userName;

	/** The project desc map. */
	private Map<LANG_ENUM, String> projectDescMap;

	/**
	 * Constructor.
	 * @param id
	 *            project id
	 * @param projectName
	 *            project name
	 * @param image
	 *            project icon
	 * @param status
	 *            project status
	 * @param projectAppDescMap 
	 */
	public ProjectModel(final String id, final String projectName, final Icon icon,
			final boolean status, Map<LANG_ENUM, String> projectDescMap) {
		this.projectId = id;
		this.projectName = projectName;
		this.icon = icon;
		this.status = status;
		this.projectDescMap = projectDescMap;
	}

	/**
	 * @return the userId
	 */
	public String getUserId() {
		return userId;
	}

	/**
	 * @param userId the userId to set
	 */
	public void setUserId(String userId) {
		this.userId = userId;
	}

	/**
	 * @return the projectId
	 */
	public final String getProjectId() {
		return projectId;
	}
	
	/**
	 * @param id the id to set
	 */
	public final void setProjectId(final String id) {
		this.projectId = id;
	}
	
	/**
	 * @return the name
	 */
	public final String getProjectName() {
		return projectName;
	}
	
	/**
	 * @param projectName the projectName to set
	 */
	public final void setProjectName(final String name) {
		this.projectName = name;
	}
	
	/**
	 * @return the status
	 */
	public final boolean isStatus() {
		return status;
	}
	
	/**
	 * @param status the status to set
	 */
	public final void setStatus(final boolean status) {
		this.status = status;
	}

	/**
	 * @return the projectDescMap
	 */
	public Map<LANG_ENUM, String> getProjectDescMap() {
		return projectDescMap;
	}

	/**
	 * @param projectDescMap the projectDescMap to set
	 */
	public void setProjectDescMap(Map<LANG_ENUM, String> projectDescMap) {
		this.projectDescMap = projectDescMap;
	}

	/**
	 * @return the userName
	 */
	public String getUserName() {
		return userName;
	}

	/**
	 * @param userName the userName to set
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}

	/**
	 * @return the icon
	 */
	public Icon getIcon() {
		return icon;
	}

	/**
	 * @param icon the icon to set
	 */
	public void setIcon(Icon icon) {
		this.icon = icon;
	}

}
