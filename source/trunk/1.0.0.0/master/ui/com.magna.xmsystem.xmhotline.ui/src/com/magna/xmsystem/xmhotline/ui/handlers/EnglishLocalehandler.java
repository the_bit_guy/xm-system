
package com.magna.xmsystem.xmhotline.ui.handlers;

import java.util.List;
import java.util.Locale;

import javax.inject.Named;

import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.core.services.nls.ILocaleChangeService;
import org.eclipse.e4.core.services.translation.TranslationService;
import org.eclipse.e4.ui.model.application.ui.MUIElement;
import org.eclipse.e4.ui.model.application.ui.menu.MHandledItem;

import com.magna.xmsystem.dependencies.utils.LANG_ENUM;
import com.magna.xmsystem.dependencies.utils.XMSystemUtil;
import com.magna.xmsystem.xmhotline.util.XmHotlineUtil;

/**
 * Handler class to change the language to English.
 *
 * @author shashwat.anand
 */
public class EnglishLocalehandler {
	
	/**
	 * Execute.
	 *
	 * @param service the service
	 * @param locale the locale
	 * @param handledItem the handled item
	 */
	@Execute
	public void execute(ILocaleChangeService service, @Named(TranslationService.LOCALE) Locale locale, final MHandledItem handledItem) {
		XmHotlineUtil.getInstance().setLocale(Locale.ENGLISH);
		service.changeApplicationLocale(Locale.ENGLISH);
		XMSystemUtil.setLanguage(LANG_ENUM.getLangEnum(Locale.ENGLISH.getLanguage()).name().toLowerCase());
		List<MUIElement> children = handledItem.getParent().getChildren();
		for (MUIElement muiElement : children) {
			if ("com.magna.xmsystem.xmhotline.ui.handledmenuitem.english".equals(muiElement.getElementId())) {
				((MHandledItem) muiElement).setSelected(true);
			} else if ("com.magna.xmsystem.xmhotline.ui.handledmenuitem.german".equals(muiElement.getElementId())) {
				((MHandledItem) muiElement).setSelected(false);
			}
		}
	}
}