package com.magna.xmsystem.xmhotline.model;

import org.eclipse.swt.graphics.Image;

import com.magna.xmsystem.xmhotline.model.interfaces.IXmHotlineModel;

/**
 * This class contains User id, name and image.
 */
public class UserModel implements IXmHotlineModel {
	
	/** Member variable 'userId' for {@link String}. */
	private String userId;
	
	/** Member variable 'projectUserRelId' for {@link String}. */
	private String projectUserRelId;
	
	/** Member variable 'userName' for {@link String}. */
	private String userName;
	
	/** Member variable 'image' for {@link String}. */
	private Image image;

	/** The status. */
	private boolean status;
	
	/**
	 * Instantiates a new user model.
	 *
	 * @param userId the user id
	 * @param userName the user name
	 * @param status the status
	 * @param image the image
	 */
	public UserModel(final String userId, final String userName, final boolean status, final Image image) {
		this.userId = userId;
		this.userName = userName;
		this.status = status;
		this.image = image;
	}
	
	/**
	 * Gets the user id.
	 *
	 * @return the userId
	 */
	public final String getUserId() {
		return userId;
	}
	
	/**
	 * Sets the user id.
	 *
	 * @param userId the userId to set
	 */
	public final void setUserId(final String userId) {
		this.userId = userId;
	}
	
	/**
	 * @return the status
	 */
	public boolean isStatus() {
		return status;
	}

	/**
	 * @param status the status to set
	 */
	public void setStatus(boolean status) {
		this.status = status;
	}

	/**
	 * Gets the project user rel id.
	 *
	 * @return the projectUserRelId
	 */
	public final String getProjectUserRelId() {
		return projectUserRelId;
	}

	/**
	 * Sets the project user rel id.
	 *
	 * @param projectUserRelId the projectUserRelId to set
	 */
	public final void setProjectUserRelId(final String projectUserRelId) {
		this.projectUserRelId = projectUserRelId;
	}

	/**
	 * Gets the user name.
	 *
	 * @return the userName
	 */
	public final String getUserName() {
		return userName;
	}
	
	/**
	 * Sets the user name.
	 *
	 * @param userName the userName to set
	 */
	public final void setUserName(final String userName) {
		this.userName = userName;
	}
	
	/**
	 * Gets the image.
	 *
	 * @return the image
	 */
	public final Image getImage() {
		return image;
	}
	
	/**
	 * Sets the image.
	 *
	 * @param image the image to set
	 */
	public final void setImage(final Image image) {
		this.image = image;
	}	
	
}
