package com.magna.xmsystem.xmhotline.ui.parts;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.SashForm;
import org.eclipse.swt.custom.ScrolledComposite;
import org.eclipse.swt.events.ControlAdapter;
import org.eclipse.swt.events.ControlEvent;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.FontData;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Text;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.magna.xmbackend.vo.Message;
import com.magna.xmsystem.dependencies.utils.XMSystemUtil;
import com.magna.xmsystem.ui.controls.widgets.MagnaCustomCombo;
import com.magna.xmsystem.xmadmin.restclient.adminArea.AdminAreaController;
import com.magna.xmsystem.xmadmin.restclient.project.ProjectController;
import com.magna.xmsystem.xmadmin.restclient.relation.AdminAreaProjectAppRelController;
import com.magna.xmsystem.xmadmin.restclient.relation.AdminAreaUserAppRelController;
import com.magna.xmsystem.xmadmin.restclient.site.SiteController;
import com.magna.xmsystem.xmadmin.restclient.users.UserController;
import com.magna.xmsystem.xmhotline.ui.message.MessageRegistry;
import com.magna.xmsystem.xmhotline.util.XmHotlineHelper;

/**
 * Class for Hot-line composite UI.
 *
 * @author Chiranjeevi.Akula
 */
public class HotLineCompositeUI extends Composite {

	/** Member variable Constant Log4j 'LOGGER'. */
	private static final Logger LOGGER = LoggerFactory.getLogger(HotLineCompositeUI.class);
	
	/** Member variable 'registry' for {@link MessageRegistry}. */
	protected final MessageRegistry registry;
	
	/** Member variable 'group title font' for {@link Font}. */
	private Font groupTitleFont;

	/** Member variable 'site filter text' for {@link Text}. */
	protected Text siteFilterText;
	
	/** Member variable 'admin area filter text' for {@link Text}. */
	protected Text adminAreaFilterText;
	
	/** Member variable 'user filter text' for {@link Text}. */
	protected Text userFilterText;
	
	/** Member variable 'project filter text' for {@link Text}. */
	protected Text projectFilterText;

	/** Member variable 'site filter button' for {@link Button}. */
	protected Button siteFilterButton;
	
	/** Member variable 'admin area filter button' for {@link Button}. */
	protected Button adminAreaFilterButton;
	
	/** Member variable 'user filter button' for {@link Button}. */
	protected Button userFilterButton;
	
	/** Member variable 'project filter button' for {@link Button}. */
	protected Button projectFilterButton;

	/** Member variable 'site filter combo' for {@link CustomTableCombo}. */
	protected MagnaCustomCombo siteFilterCombo;
	
	/** Member variable 'admin area filter combo' for {@link CustomTableCombo}. */
	protected MagnaCustomCombo adminAreaFilterCombo;
	
	/** Member variable 'user filter combo' for {@link CustomTableCombo}. */
	protected MagnaCustomCombo userFilterCombo;
	
	/** Member variable 'project filter combo' for {@link CustomTableCombo}. */
	protected MagnaCustomCombo projectFilterCombo;

	/** Member variable 'save button' for {@link Button}. */
	protected Button saveButton;
	
	/** Member variable 'reset button' for {@link Button}. */
	protected Button resetButton;

	/** Member variable 'porjectInfoScrollComp' for {@link ScrolledComposite}. */
	protected ScrolledComposite projectInfoScrollComp;
	
	/** Member variable 'fixUserApplScrollComp' for {@link ScrolledComposite}. */
	protected ScrolledComposite fixUserApplScrollComp;
	
	/** Member variable 'notFixUserAppScrollComp' for {@link ScrolledComposite}. */
	protected ScrolledComposite notFixUserAppScrollComp;
	
	/** Member variable 'protectedUserAppScrollComp' for {@link ScrolledComposite}. */
	protected ScrolledComposite protectedUserAppScrollComp;
	
	/** Member variable 'fixProjApplScrollComp' for {@link ScrolledComposite}. */
	protected ScrolledComposite fixProjApplScrollComp;
	
	/** Member variable 'notFixProjAppScrollComp' for {@link ScrolledComposite}. */
	protected ScrolledComposite notFixProjAppScrollComp;
	
	/** Member variable 'protectedProjAppScrollComp' for {@link ScrolledComposite}. */
	protected ScrolledComposite protectedProjAppScrollComp;
	
	/** Member variable 'projectComp' for {@link Composite}. */
	protected Composite projectComp;
	
	/** Member variable 'fixedUserComo' for {@link Composite}. */
	protected Composite fixedUserAppComo;
	
	/** Member variable 'notFixedUserComp' for {@link Composite}. */
	protected Composite notFixedUserAppComp;
	
	/** Member variable 'protectedUserComp' for {@link Composite}. */
	protected Composite protectedUserAppComp;
	
	/** Member variable 'fixedProjectComp' for {@link Composite}. */
	protected Composite fixedProjectAppComp;
	
	/** Member variable 'notFixedProjectComp' for {@link Composite}. */
	protected Composite notFixedProjectAppComp;
	
	/** Member variable 'protectedProject' for {@link Composite}. */
	protected Composite protectedProjectAppComp;

	/** Member variable 'siteController' for {@link SiteController}. */
	protected SiteController siteController;
	
	/** Member variable 'adminAreaController' for {@link ProjectController}. */
	protected AdminAreaController adminAreaController;
	
	/** Member variable 'projectController' for {@link Message}. */
	protected ProjectController projectController;
	
	/** Member variable 'userController' for {@link UserController}. */
	protected UserController userController;
	
	/** Member variable 'adminAreaUserAppRelController' for {@link AdminAreaUserAppRelController}. */
	protected AdminAreaUserAppRelController adminAreaUserAppRelController;
	
	/** Member variable 'adminAreaProjectAppRelController' for {@link AdminAreaProjectAppRelController}. */
    protected AdminAreaProjectAppRelController adminAreaProjectAppRelController;

    /** Member variable 'xmHotlineHelper' for {@link XmHotlineHelper}. */
    protected XmHotlineHelper xmHotlineHelper;
    
    /** Member variable 'projectInfoGroup' for {@link Group}. */
	protected Group projectInfoGroup;

	/** Member variable 'protectedUserAppInfoGroup' for {@link Group}. */
	protected Group fixUserAppInfoGroup;

	/** Member variable 'fixUserAppInfoGroup' for {@link Group}. */
	protected Group nonFixUserAppInfoGroup;

	/** Member variable 'protectedUserAppInfoGroup' for {@link Group}. */
	protected Group protectedUserAppInfoGroup;

	/** Member variable 'fixProjAppInfoGroup' for {@link Group}. */
	protected Group fixProjAppInfoGroup;

	/** Member variable 'nonFixProjAppInfoGroup' for {@link Group}. */
	protected Group nonFixProjAppInfoGroup;

	/** Member variable 'protectedProjAppInfoGroup' for {@link Group}. */
	protected Group protectedProjAppInfoGroup;

	/**
	 * Constructor for HotLineCompositeUI Class.
	 *
	 * @param parent {@link Composite}
	 * @param style {@link int}
	 * @param registry {@link MessageRegistry}
	 */
	public HotLineCompositeUI(final Composite parent, final int style, final MessageRegistry registry) {
		super(parent, style);
		this.registry = registry;
		
		xmHotlineHelper = XmHotlineHelper.getInstance();
		
		siteController = xmHotlineHelper.getSiteController();
		adminAreaController = xmHotlineHelper.getAdminAreaController();
		userController = xmHotlineHelper.getUserController();
		projectController = xmHotlineHelper.getProjectController();
		adminAreaUserAppRelController = xmHotlineHelper.getAdminAreaUserAppRelController();
		adminAreaProjectAppRelController = xmHotlineHelper.getAdminAreaProjectAppRelController();
		
		this.initGUI();
	}

	/**
	 * Method for build the GUI.
	 */
	private void initGUI() {
		try {
			this.setLayout(new GridLayout(1, false));
			this.setLayoutData(
					new GridData(GridData.HORIZONTAL_ALIGN_BEGINNING, GridData.VERTICAL_ALIGN_BEGINNING, true, true));

			final Composite filterComposite = new Composite(this, SWT.NONE);
			filterComposite.setLayout(new GridLayout(4, true));
			filterComposite.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));

			final Composite infoComposite = new Composite(this, SWT.NONE);
			infoComposite.setLayout(new GridLayout(1, true));
			infoComposite.setLayoutData(new GridData(GridData.FILL_BOTH));

			final Composite buttonComposite = new Composite(this, SWT.BORDER);
			buttonComposite.setLayout(new GridLayout(1, false));
			buttonComposite.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));

			final FontData groupTitleFontData = new FontData();
			groupTitleFontData.setStyle(SWT.BOLD | SWT.ITALIC);
			groupTitleFontData.setHeight(12);
			this.groupTitleFont = new Font(Display.getCurrent(), groupTitleFontData);

			this.constructFiltersPanel(filterComposite);
			this.constructInfoPanel(infoComposite);
			this.constructButtonsPanel(buttonComposite);
			this.resetButton.setFocus();
		} catch (Exception ex) {
			LOGGER.error("Unable to crete UI elements", ex); //$NON-NLS-1$
		}
	}

	/**
	 * Method for Construct the filters panel.
	 *
	 * @param filterComposite {@link Composite}
	 */
	private void constructFiltersPanel(final Composite filterComposite) {
		//filterComposite.setData("org.eclipse.e4.ui.css.CssClassName", "FilterComp");

		final GridLayout filterGroupGridLayout = new GridLayout(4, false);
		
		final GridData filterGroupGridData = new GridData(GridData.FILL_HORIZONTAL);
		filterGroupGridData.minimumWidth = 200;
		
		final GridData filterTextGridData = new GridData(GridData.FILL_HORIZONTAL);
		filterTextGridData.minimumWidth = 50;
		
		final GridData filterButtonsGridData = new GridData();
		filterButtonsGridData.widthHint = 30;

		final GridData filterComboGridData = new GridData(GridData.FILL_HORIZONTAL);
		filterComboGridData.minimumWidth = 100;
		
		final Group siteFilterGroup = new Group(filterComposite, SWT.NONE);
		siteFilterGroup.setLayout(filterGroupGridLayout);
		siteFilterGroup.setFont(this.groupTitleFont);
		siteFilterGroup.setLayoutData(filterGroupGridData);
		this.registry.register(siteFilterGroup::setText, (message) -> message.siteFilterGroup);

		final Group adminAreaFilterGroup = new Group(filterComposite, SWT.NONE);
		adminAreaFilterGroup.setLayout(filterGroupGridLayout);
		adminAreaFilterGroup.setFont(this.groupTitleFont);
		adminAreaFilterGroup.setLayoutData(filterGroupGridData);
		this.registry.register(adminAreaFilterGroup::setText, (message) -> message.adminAreaFilterGroup);

		final Group userFilterGroup = new Group(filterComposite, SWT.NONE);
		userFilterGroup.setLayout(filterGroupGridLayout);
		userFilterGroup.setFont(this.groupTitleFont);
		userFilterGroup.setLayoutData(filterGroupGridData);
		this.registry.register(userFilterGroup::setText, (message) -> message.userFilterGroup);

		final Group projectFilterGroup = new Group(filterComposite, SWT.NONE);
		projectFilterGroup.setLayout(filterGroupGridLayout);
		projectFilterGroup.setFont(this.groupTitleFont);
		projectFilterGroup.setLayoutData(filterGroupGridData);
		this.registry.register(projectFilterGroup::setText, (message) -> message.projectFilterGroup);

		Label filterLabel = new Label(siteFilterGroup, SWT.NONE);
		this.registry.register(filterLabel::setText, (message) -> message.filterLabel);
		this.siteFilterText = new Text(siteFilterGroup, SWT.BORDER);
		this.siteFilterText.setLayoutData(filterTextGridData);
		this.siteFilterButton = new Button(siteFilterGroup, SWT.PUSH);
		this.siteFilterButton.setLayoutData(filterButtonsGridData);
		this.registry.register(this.siteFilterButton::setText, (message) -> message.showAllButton);			 
		this.siteFilterCombo = new MagnaCustomCombo(siteFilterGroup, SWT.READ_ONLY | SWT.BORDER);
		this.siteFilterCombo.setVisibleItemCount(15);
		xmHotlineHelper.setSitesToCombo(siteFilterCombo);
		this.siteFilterCombo.setLayoutData(filterComboGridData);		

		filterLabel = new Label(adminAreaFilterGroup, SWT.NONE);
		this.registry.register(filterLabel::setText, (message) -> message.filterLabel);
		this.adminAreaFilterText = new Text(adminAreaFilterGroup, SWT.BORDER);
		this.adminAreaFilterText.setLayoutData(filterTextGridData);
		this.adminAreaFilterText.setEditable(false);
		this.adminAreaFilterButton = new Button(adminAreaFilterGroup, SWT.PUSH);
		this.adminAreaFilterButton.setLayoutData(filterButtonsGridData);
		this.adminAreaFilterButton.setEnabled(false);
		this.registry.register(this.adminAreaFilterButton::setText, (message) -> message.showAllButton);		
		this.adminAreaFilterCombo = new MagnaCustomCombo(adminAreaFilterGroup, SWT.READ_ONLY | SWT.BORDER);
		this.adminAreaFilterCombo.setVisibleItemCount(15);
		this.adminAreaFilterCombo.setLayoutData(filterComboGridData);
		this.adminAreaFilterCombo.setEnabled(false);

		filterLabel = new Label(userFilterGroup, SWT.NONE);
		this.registry.register(filterLabel::setText, (message) -> message.filterLabel);
		this.userFilterText = new Text(userFilterGroup, SWT.BORDER);
		this.userFilterText.setLayoutData(filterTextGridData);
		this.userFilterText.setEditable(false);
		this.userFilterButton = new Button(userFilterGroup, SWT.PUSH);
		this.userFilterButton.setLayoutData(filterButtonsGridData);
		this.userFilterButton.setEnabled(false);
		this.registry.register(this.userFilterButton::setText, (message) -> message.showAllButton);
		this.userFilterCombo = new MagnaCustomCombo(userFilterGroup, SWT.READ_ONLY | SWT.BORDER);
		this.userFilterCombo.setVisibleItemCount(15);
		this.userFilterCombo.setEnabled(false);
		xmHotlineHelper.setUserToCombo(userFilterCombo, xmHotlineHelper.getAllUsersList()); 
		this.userFilterCombo.setLayoutData(filterComboGridData);

		filterLabel = new Label(projectFilterGroup, SWT.NONE);
		this.registry.register(filterLabel::setText, (message) -> message.filterLabel);
		this.projectFilterText = new Text(projectFilterGroup, SWT.BORDER);
		this.projectFilterText.setLayoutData(filterTextGridData);
		this.projectFilterText.setEditable(false);
		this.projectFilterButton = new Button(projectFilterGroup, SWT.PUSH);
		this.projectFilterButton.setLayoutData(filterButtonsGridData);
		this.projectFilterButton.setEnabled(false);
		this.registry.register(this.projectFilterButton::setText, (message) -> message.showAllButton);
		this.projectFilterCombo = new MagnaCustomCombo(projectFilterGroup, SWT.READ_ONLY | SWT.BORDER);
		this.projectFilterCombo.setVisibleItemCount(15);
		this.projectFilterCombo.setEnabled(false);
		//xmHotlineHelper.setProjectToCombo(projectFilterCombo, xmHotlineHelper.getProjectMap().values());
		this.projectFilterCombo.setLayoutData(filterComboGridData);
		
		filterComposite.addControlListener(new ControlAdapter() {
			public void controlResized(final ControlEvent event) {
				siteFilterCombo.setSelection(new Point(0, 0));
				adminAreaFilterCombo.setSelection(new Point(0, 0));
				userFilterCombo.setSelection(new Point(0, 0));
				projectFilterCombo.setSelection(new Point(0, 0));
			}
		});
	}

	/**
	 * Method for Construct the information panel.
	 *
	 * @param infoComposite {@link Composite}
	 */
	private void constructInfoPanel(final Composite infoComposite) {

		final GridLayout infoGridLayout = new GridLayout(1, false);
		final GridData infoGridData = new GridData(GridData.FILL_BOTH);

		final SashForm horizontalSash = new SashForm(infoComposite, SWT.HORIZONTAL | SWT.NONE);
		horizontalSash.setLayout(infoGridLayout);
		horizontalSash.setLayoutData(infoGridData);

		final Composite projectInfoComp = new Composite(horizontalSash, SWT.NONE);
		projectInfoComp.setLayout(infoGridLayout);
		projectInfoComp.setLayoutData(infoGridData);

		final Composite uaserAppInfoComp = new Composite(horizontalSash, SWT.NONE);
		uaserAppInfoComp.setLayout(infoGridLayout);
		uaserAppInfoComp.setLayoutData(infoGridData);

		final SashForm verticalSashUserApp = new SashForm(uaserAppInfoComp, SWT.VERTICAL | SWT.NONE);
		verticalSashUserApp.setLayout(infoGridLayout);
		verticalSashUserApp.setLayoutData(infoGridData);

		final Composite projectAppInfoComp = new Composite(horizontalSash, SWT.NONE);
		projectAppInfoComp.setLayout(infoGridLayout);
		projectAppInfoComp.setLayoutData(infoGridData);

		final SashForm verticalSashProjectApp = new SashForm(projectAppInfoComp, SWT.VERTICAL | SWT.NONE);
		verticalSashProjectApp.setLayout(infoGridLayout);
		verticalSashProjectApp.setLayoutData(infoGridData);

		projectInfoGroup = new Group(projectInfoComp, SWT.NONE);
		projectInfoGroup.setLayout(infoGridLayout);
		projectInfoGroup.setFont(this.groupTitleFont);
		projectInfoGroup.setLayoutData(infoGridData);
		this.registry.register(projectInfoGroup::setText, (message) -> message.projectInfoGroup);
		projectInfoScrollComp = new ScrolledComposite(projectInfoGroup, SWT.V_SCROLL);
		projectComp = getComposite(projectInfoScrollComp);
		addActivateListenerToScrollComp(projectInfoScrollComp);
		
		fixUserAppInfoGroup = new Group(verticalSashUserApp, SWT.NONE);
		fixUserAppInfoGroup.setLayout(infoGridLayout);
		fixUserAppInfoGroup.setFont(this.groupTitleFont);
		fixUserAppInfoGroup.setLayoutData(infoGridData);
		this.registry.register(fixUserAppInfoGroup::setText, (message) -> message.fixUaserAppInfoGroup);		
		fixUserApplScrollComp = new ScrolledComposite(fixUserAppInfoGroup, SWT.V_SCROLL);
		fixedUserAppComo = getComposite(fixUserApplScrollComp);
		addActivateListenerToScrollComp(fixUserApplScrollComp);
		
		nonFixUserAppInfoGroup = new Group(verticalSashUserApp, SWT.NONE);
		nonFixUserAppInfoGroup.setLayout(infoGridLayout);
		nonFixUserAppInfoGroup.setFont(this.groupTitleFont);
		nonFixUserAppInfoGroup.setLayoutData(infoGridData);
		this.registry.register(nonFixUserAppInfoGroup::setText, (message) -> message.nonFixUaserAppInfoGroup);
		notFixUserAppScrollComp = new ScrolledComposite(nonFixUserAppInfoGroup, SWT.V_SCROLL);
		notFixedUserAppComp = getComposite(notFixUserAppScrollComp);
		addActivateListenerToScrollComp(notFixUserAppScrollComp);
		
		protectedUserAppInfoGroup = new Group(verticalSashUserApp, SWT.NONE);
		protectedUserAppInfoGroup.setLayout(infoGridLayout);
		protectedUserAppInfoGroup.setFont(this.groupTitleFont);
		protectedUserAppInfoGroup.setLayoutData(infoGridData);
		this.registry.register(protectedUserAppInfoGroup::setText, (message) -> message.protectedUserAppInfoGroup);
		protectedUserAppScrollComp = new ScrolledComposite(protectedUserAppInfoGroup, SWT.V_SCROLL);
		protectedUserAppComp = getComposite(protectedUserAppScrollComp);
		addActivateListenerToScrollComp(protectedUserAppScrollComp);

		fixProjAppInfoGroup = new Group(verticalSashProjectApp, SWT.NONE);
		fixProjAppInfoGroup.setLayout(infoGridLayout);
		fixProjAppInfoGroup.setFont(this.groupTitleFont);
		fixProjAppInfoGroup.setLayoutData(infoGridData);
		this.registry.register(fixProjAppInfoGroup::setText, (message) -> message.fixProjectAppInfoGroup);
		fixProjApplScrollComp = new ScrolledComposite(fixProjAppInfoGroup, SWT.V_SCROLL);
		fixedProjectAppComp = getComposite(fixProjApplScrollComp);
		addActivateListenerToScrollComp(fixProjApplScrollComp);

		nonFixProjAppInfoGroup = new Group(verticalSashProjectApp, SWT.NONE);
		nonFixProjAppInfoGroup.setLayout(infoGridLayout);
		nonFixProjAppInfoGroup.setFont(this.groupTitleFont);
		nonFixProjAppInfoGroup.setLayoutData(infoGridData);
		this.registry.register(nonFixProjAppInfoGroup::setText, (message) -> message.nonFixProjectAppInfoGroup);
		notFixProjAppScrollComp = new ScrolledComposite(nonFixProjAppInfoGroup, SWT.V_SCROLL);
		notFixedProjectAppComp = getComposite(notFixProjAppScrollComp);
		addActivateListenerToScrollComp(notFixProjAppScrollComp);
		
		protectedProjAppInfoGroup = new Group(verticalSashProjectApp, SWT.NONE);
		protectedProjAppInfoGroup.setLayout(infoGridLayout);
		protectedProjAppInfoGroup.setFont(this.groupTitleFont);
		protectedProjAppInfoGroup.setLayoutData(infoGridData);
		this.registry.register(protectedProjAppInfoGroup::setText, (message) -> message.protectedProjectAppInfoGroup);
		protectedProjAppScrollComp = new ScrolledComposite(protectedProjAppInfoGroup, SWT.V_SCROLL);
		protectedProjectAppComp = getComposite(protectedProjAppScrollComp);
		addActivateListenerToScrollComp(protectedProjAppScrollComp);
	}
	
	/**
	 * Adds the activate listener to scroll comp.
	 *
	 * @param scrollComp the scroll comp
	 */
	private void addActivateListenerToScrollComp(final ScrolledComposite scrollComp) {
		scrollComp.addListener(SWT.Activate, new Listener() {
			public void handleEvent(Event e) {
				scrollComp.setFocus();
			}
		});
	}

	/**
	 * 
	 * @param scrollComp
	 * @return composite
	 * 
	 */
	protected Composite getComposite(final ScrolledComposite scrollComp) {
		scrollComp.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		scrollComp.setExpandVertical(true);
		scrollComp.setExpandHorizontal(true);
		Composite composite = new Composite(scrollComp, SWT.NONE);
		
		final GridLayout widgetContainerLayout = new GridLayout(1, false);
		widgetContainerLayout.verticalSpacing = 1;
		widgetContainerLayout.horizontalSpacing = 1;
		widgetContainerLayout.marginRight = 0;
		widgetContainerLayout.marginLeft = 0;
		widgetContainerLayout.marginTop = 0;
		widgetContainerLayout.marginBottom = 0;
		widgetContainerLayout.marginWidth = 0;
		widgetContainerLayout.marginHeight = 0;
		composite.setLayout(widgetContainerLayout);
		composite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));	
		
		/*scrollComp.setAlwaysShowScrollBars(true);
		scrollComp.setContent(composite);
		Point point = composite.computeSize(SWT.DEFAULT, SWT.DEFAULT);
		composite.setSize(point);
		scrollComp.setMinSize(point);*/
		
		return composite;
	}

	/**
	 * Method for Construct the buttons panel.
	 *
	 * @param buttonComposite {@link Composite}
	 */
	private void constructButtonsPanel(final Composite buttonComposite) {

		final GridData buttonsGridData = new GridData();
		buttonsGridData.widthHint = 120;

		final Composite buttonsDisplayComposite = new Composite(buttonComposite, SWT.NONE);
		buttonsDisplayComposite.setLayout(new GridLayout(2, false));
		buttonsDisplayComposite.setLayoutData(new GridData(SWT.CENTER, SWT.FILL, true, true));

		this.saveButton = new Button(buttonsDisplayComposite, SWT.PUSH);
		this.saveButton.setLayoutData(buttonsGridData);
		this.saveButton.setImage(XMSystemUtil.getInstance().getImage(this.getClass(), "icons/16x16/ok_16.png"));
		this.registry.register(this.saveButton::setText, (message) -> " " + message.saveButton);

		this.resetButton = new Button(buttonsDisplayComposite, SWT.PUSH);
		this.resetButton.setLayoutData(buttonsGridData);
		this.resetButton.setImage(XMSystemUtil.getInstance().getImage(this.getClass(), "icons/16x16/cancel_16.png"));
		this.registry.register(this.resetButton::setText, (message) -> " " + message.resetButton);
	}
	
}
