
package com.magna.xmsystem.xmhotline.ui.handlers;

import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.ui.model.application.MApplication;
import org.eclipse.e4.ui.model.application.ui.basic.MPart;
import org.eclipse.e4.ui.workbench.modeling.EModelService;

import com.magna.xmsystem.xmhotline.ui.parts.HotLineMainPart;

/**
 * The Class ReloadHandler.
 * @author shashwat.anand
 */
public class ReloadHandler {
	
	/**
	 * Execute.
	 *
	 * @param application the application
	 * @param modelService the model service
	 */
	@Execute
	public void execute(final MApplication application, final EModelService modelService) {
		MPart mPart = (MPart) modelService.find(HotLineMainPart.ID, application);
		HotLineMainPart hotLineMainPart = (HotLineMainPart) mPart.getObject();
		hotLineMainPart.getHotlineCompAction().reload();
	}
}