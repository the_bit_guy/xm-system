package com.magna.xmsystem.xmhotline.ui.statusbar;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.eclipse.e4.core.contexts.ContextInjectionFactory;
import org.eclipse.e4.core.contexts.IEclipseContext;
import org.eclipse.e4.core.di.annotations.Creatable;
import org.eclipse.e4.core.di.annotations.Optional;
import org.eclipse.e4.ui.di.UIEventTopic;
import org.eclipse.e4.ui.di.UISynchronize;
import org.eclipse.jface.layout.GridDataFactory;
import org.eclipse.jface.layout.GridLayoutFactory;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.magna.xmsystem.xmhotline.util.Constants;

@Creatable
public class StatusBarToolControl extends Composite {
	/** Logger instance. */
	private static final Logger LOGGER = LoggerFactory.getLogger(StatusBarToolControl.class);

	/** {@link UISynchronize} instance. */
	@Inject
	private UISynchronize uiSynchronize;
	
	/**
	 * Member variable for {@link IEclipseContext}
	 */
	@Inject
	private IEclipseContext eclipseContext;


	/**
	 * Label for status
	 */
	private Label statusLabel;

	private ProgressControl progressControl;

	/**
	 * Constructor
	 * 
	 * @since 20.12.2013
	 * @param parent
	 */
	@Inject
	public StatusBarToolControl(final Composite parent) {
		super(parent, SWT.NONE);
	}

	/**
	 * Listen for any event message
	 * 
	 * @param message
	 *            {@link String}
	 */
	@Inject
	@Optional
	public void getEvent(@UIEventTopic(Constants.STATUSBAR) final String message) {
		updateInterface(message);
	}

	/**
	 * Create UI of status bar
	 * 
	 * @param parent
	 *            {@link Composite}
	 */
	@PostConstruct
	public void createGui() {
		GridLayoutFactory.fillDefaults().spacing(0, 0).margins(0, 0).extendedMargins(0, 0, 0, 0)
				.applyTo(this.getParent());
		GridLayoutFactory.fillDefaults().numColumns(2).equalWidth(false).spacing(0, 0).margins(0, 0)
				.extendedMargins(0, 0, 0, 0).applyTo(this);
		GridDataFactory.fillDefaults().align(SWT.FILL, SWT.FILL).grab(true, true).applyTo(this);
		this.statusLabel = createLabel(this);
		eclipseContext.set(UISynchronize.class, uiSynchronize);
		eclipseContext.set(Composite.class, this);
		this.progressControl = ContextInjectionFactory.make(ProgressControl.class, eclipseContext);
	}

	/**
	 * Creates the label in status bar
	 * 
	 * @param parent
	 *            {@link Composite}
	 * @return {@link Label}
	 */
	private Label createLabel(final Composite parent) {
		final Composite body = new Composite(parent, SWT.NONE);
		GridDataFactory.fillDefaults().align(SWT.FILL, SWT.FILL).grab(true, true).applyTo(body);
		GridLayoutFactory.fillDefaults().margins(0, 0).spacing(0, 0).applyTo(body);
		final Label label = new Label(body, SWT.NONE);
		GridDataFactory.fillDefaults().align(SWT.FILL, SWT.FILL).indent(5, 0).grab(true, true).applyTo(label);
		return label;
	}

	/**
	 * Updates the UI
	 * 
	 * @param message
	 *            {@link String}
	 */
	public void updateInterface(final String message) {
		try {
			this.uiSynchronize.asyncExec(new Runnable() {
				/**
				 * Run method
				 */
				@Override
				public void run() {
					try {
						if (statusLabel != null && !statusLabel.isDisposed()) {
							statusLabel.setText(message);
							statusLabel.requestLayout();
							statusLabel.getParent().redraw();
							statusLabel.getParent().getParent().update();
						}
					} catch (Exception exc) {
						LOGGER.error("Exception occured while setting status bar text", exc);
					}
				}
			});
		} catch (Exception exception) {
			LOGGER.error("Exception occured while setting status bar text in update interface method", exception);
		}
	}

	/**
	 * @return the progressControl
	 */
	public ProgressControl getProgressControl() {
		return progressControl;
	}
}