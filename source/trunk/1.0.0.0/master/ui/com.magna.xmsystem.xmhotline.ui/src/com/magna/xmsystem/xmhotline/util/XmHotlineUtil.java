package com.magna.xmsystem.xmhotline.util;

import java.util.Collection;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import javax.inject.Inject;
import javax.inject.Named;

import org.eclipse.e4.core.contexts.IEclipseContext;
import org.eclipse.e4.core.di.annotations.Optional;
import org.eclipse.e4.core.services.translation.TranslationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.magna.xmbackend.entities.AdminAreasTbl;
import com.magna.xmbackend.entities.ProjectAppTranslationTbl;
import com.magna.xmbackend.entities.ProjectApplicationsTbl;
import com.magna.xmbackend.entities.ProjectTranslationTbl;
import com.magna.xmbackend.entities.ProjectsTbl;
import com.magna.xmbackend.entities.SitesTbl;
import com.magna.xmbackend.entities.UserAppTranslationTbl;
import com.magna.xmbackend.entities.UserApplicationsTbl;
import com.magna.xmsystem.dependencies.utils.LANG_ENUM;
import com.magna.xmsystem.dependencies.utils.XMSystemUtil;

/**
 * The Class XmHotlineUtil.
 * 
 * @author shashwat.anand
 */
public class XmHotlineUtil {
	
	/** The Constant LOGGER. */
	private static final Logger LOGGER = LoggerFactory.getLogger(XmHotlineUtil.class);
	
	/** Member variable 'this ref' for {@link XmHotlineUtil}. */
	private static XmHotlineUtil thisRef;
	
	/** Member variable 'locale' for {@link Locale}. */
	@Inject
	private @Named(TranslationService.LOCALE) Locale locale;
	
	/** Member variable 'eclipse context' for {@link IEclipseContext}. */
	@Inject
	private IEclipseContext eclipseContext;
	
	/**
	 * Constructor for XmHotlineUtil Class.
	 */
	public XmHotlineUtil() {
		setInstance(this);
	}
	
	/**
	 * Gets the single instance of XMAdminUtil.
	 *
	 * @return single instance of XMAdminUtil
	 */
	public static XmHotlineUtil getInstance() {
		return thisRef;
	}
	
	/**
	 * Sets the instance.
	 *
	 * @param thisRef
	 *            the new instance
	 */
	public static void setInstance(XmHotlineUtil thisRef) {
		XmHotlineUtil.thisRef = thisRef;
	}
	
	/**
	 * Sets the locale.
	 *
	 * @param locale
	 *            the new locale
	 */
	@Inject
	public void setLocale(@Optional @Named(TranslationService.LOCALE) Locale locale) {
		Locale localeP = locale;
		if (locale == null) {
			localeP = Locale.ENGLISH;
			LOGGER.error("changing the application language to english as Local is null");
		}
		if (!(locale.getLanguage().equalsIgnoreCase(Locale.ENGLISH.getLanguage())) && 
				!(locale.getLanguage().equalsIgnoreCase(Locale.GERMAN.getLanguage()))) {
			localeP = Locale.ENGLISH;
			LOGGER.error("changing the application language to english as system langauge is neither english nor german");
		}
		this.locale = localeP;
	}
	
	/**
	 * Sets the default locale.
	 */
	public void setDefaultLocale() {
		final String localeString = XMSystemUtil.getLanguage();
		if(!XMSystemUtil.isEmpty(localeString)) {
			Locale defaultLocale = LANG_ENUM.valueOf(localeString.toUpperCase()).getLocale();
			if(defaultLocale != null) {
				Locale.setDefault(defaultLocale);				
				eclipseContext.set(TranslationService.LOCALE, defaultLocale);
				setLocale(defaultLocale);
			}			
		} else {
			XMSystemUtil.setLanguage(LANG_ENUM.getLangEnum(this.locale.getLanguage()).name().toLowerCase());
		}
	}
	
	/**
	 * Gets the current locale enum.
	 *
	 * @return the current locale enum
	 */
	public LANG_ENUM getCurrentLocaleEnum() {
		return LANG_ENUM.getLangEnum(this.locale.getLanguage());
	}
	
	/**
	 * Gets the default locale enum.
	 *
	 * @return the default locale enum
	 */
	public LANG_ENUM getDefaultLocaleEnum() {
		return LANG_ENUM.ENGLISH;
	}
	
	/**
	 * Gets the project name.
	 *
	 * @param projectsTbl the projects tbl
	 * @return the project name
	 */
	public String getProjectName(final ProjectsTbl projectsTbl) {
		return projectsTbl.getName();
	}
	
	/**
	 * Gets the site name.
	 *
	 * @param sitesTbl the sites tbl
	 * @return the site name
	 */
	public String getSiteName(final SitesTbl sitesTbl) {
		return sitesTbl.getName();
	}
	
	/**
	 * Gets the admin area name.
	 *
	 * @param adminAreasTbl the admin areas tbl
	 * @return the admin area name
	 */
	public String getAdminAreaName(final AdminAreasTbl adminAreasTbl) {
		return adminAreasTbl.getName();
	}

	/**
	 * Gets the display project application name.
	 *
	 * @param projectApplicationsTbl the project applications tbl
	 * @return the display project application name
	 */
	public String getDisplayProjectApplicationName(final ProjectApplicationsTbl projectApplicationsTbl) {
		String returnValue = "";
		final Collection<ProjectAppTranslationTbl> projectAppTranslationTblCollection = projectApplicationsTbl.getProjectAppTranslationTblCollection();
		LANG_ENUM langEnum = getDefaultLocaleEnum();
		if (getCurrentLocaleEnum() != null) {
			langEnum = getCurrentLocaleEnum();
		}
		
		for (final ProjectAppTranslationTbl projectAppTranslationTbl : projectAppTranslationTblCollection) {
			if (projectAppTranslationTbl.getLanguageCode().getLanguageCode().equals(langEnum.getLangCode())) {
				returnValue = projectAppTranslationTbl.getName();
			}
		}
		return returnValue;
	}
	
	/**
	 * Gets the project application name.
	 *
	 * @param projectApplicationsTbl the project applications tbl
	 * @return the project application name
	 */
	public String getProjectApplicationName(final ProjectApplicationsTbl projectApplicationsTbl) {
		return projectApplicationsTbl.getName();
	}
	
	/**
	 * Gets the display user application name.
	 *
	 * @param userApplicationsTbl the user applications tbl
	 * @return the display user application name
	 */
	public String getDisplayUserApplicationName(final UserApplicationsTbl userApplicationsTbl) {
		String returnValue = "";
		Collection<UserAppTranslationTbl> userAppTranslationTblCollection = userApplicationsTbl.getUserAppTranslationTblCollection();
		LANG_ENUM langEnum = getDefaultLocaleEnum();
		if (getCurrentLocaleEnum() != null) {
			langEnum = getCurrentLocaleEnum();
		}
		
		for (final UserAppTranslationTbl userAppTranslationTbl : userAppTranslationTblCollection) {
			if (userAppTranslationTbl.getLanguageCode().getLanguageCode().equals(langEnum.getLangCode())) {
				returnValue = userAppTranslationTbl.getName();
			}
		}
		return returnValue;
	}
	
	/**
	 * Gets the user application name.
	 *
	 * @param userApplicationsTbl the user applications tbl
	 * @return the user application name
	 */
	public String getUserApplicationName(final UserApplicationsTbl userApplicationsTbl) {
		return userApplicationsTbl.getName();
	}

	/**
	 * Gets the user app name map.
	 *
	 * @param userApplicationsTbl the user applications tbl
	 * @return the user app name map
	 */
	public Map<LANG_ENUM, String> getUserAppNameMap(UserApplicationsTbl userApplicationsTbl) {
		Map<LANG_ENUM, String> userAppNameMap = new HashMap<>();
		String enName = null;
		String deName = null;
		Collection<UserAppTranslationTbl> userAppTranslationTblCollection = userApplicationsTbl.getUserAppTranslationTblCollection();
		for (final UserAppTranslationTbl userAppTranslationTbl : userAppTranslationTblCollection) {
			if (userAppTranslationTbl.getLanguageCode().getLanguageCode().equals(LANG_ENUM.ENGLISH.getLangCode())) {
				enName = userAppTranslationTbl.getName();
			} else if (userAppTranslationTbl.getLanguageCode().getLanguageCode().equals(LANG_ENUM.GERMAN.getLangCode())) {
				deName = userAppTranslationTbl.getName();
			}
		}
		userAppNameMap.put(LANG_ENUM.ENGLISH, XMSystemUtil.isEmpty(enName) ? "" : enName);
		userAppNameMap.put(LANG_ENUM.GERMAN, XMSystemUtil.isEmpty(deName) ? "" : deName);
		return userAppNameMap;
	}
	
	/**
	 * Gets the user app desc map.
	 *
	 * @param userApplicationsTbl the user applications tbl
	 * @return the user app desc map
	 */
	public Map<LANG_ENUM, String> getUserAppDescMap(UserApplicationsTbl userApplicationsTbl) {
		Map<LANG_ENUM, String> userAppDescMap = new HashMap<>();
		String enDesc = null;
		String deDesc = null;
		Collection<UserAppTranslationTbl> userAppTranslationTblCollection = userApplicationsTbl.getUserAppTranslationTblCollection();
		for (final UserAppTranslationTbl userAppTranslationTbl : userAppTranslationTblCollection) {
			if (userAppTranslationTbl.getLanguageCode().getLanguageCode().equals(LANG_ENUM.ENGLISH.getLangCode())) {
				enDesc = userAppTranslationTbl.getDescription();
			} else if (userAppTranslationTbl.getLanguageCode().getLanguageCode().equals(LANG_ENUM.GERMAN.getLangCode())) {
				deDesc = userAppTranslationTbl.getDescription();
			}
		}
		userAppDescMap.put(LANG_ENUM.ENGLISH, XMSystemUtil.isEmpty(enDesc) ? "" : enDesc);
		userAppDescMap.put(LANG_ENUM.GERMAN, XMSystemUtil.isEmpty(deDesc) ? "" : deDesc);
		return userAppDescMap;
	}
	
	/**
	 * Gets the project app name map.
	 *
	 * @param projectApplicationsTbl the project applications tbl
	 * @return the project app name map
	 */
	public Map<LANG_ENUM, String> getProjectAppNameMap(ProjectApplicationsTbl projectApplicationsTbl) {
		Map<LANG_ENUM, String> projectAppNameMap = new HashMap<>();
		String enName = null;
		String deName = null;
		Collection<ProjectAppTranslationTbl> projectAppTranslationTblCollection = projectApplicationsTbl.getProjectAppTranslationTblCollection();
		for (final ProjectAppTranslationTbl projectAppTranslationTbl : projectAppTranslationTblCollection) {
			if (projectAppTranslationTbl.getLanguageCode().getLanguageCode().equals(LANG_ENUM.ENGLISH.getLangCode())) {
				enName = projectAppTranslationTbl.getName();
			} else if (projectAppTranslationTbl.getLanguageCode().getLanguageCode().equals(LANG_ENUM.GERMAN.getLangCode())) {
				deName = projectAppTranslationTbl.getName();
			}
		}
		projectAppNameMap.put(LANG_ENUM.ENGLISH, XMSystemUtil.isEmpty(enName) ? "" : enName);
		projectAppNameMap.put(LANG_ENUM.GERMAN, XMSystemUtil.isEmpty(deName) ? "" : deName);
		return projectAppNameMap;
	}
	
	/**
	 * Gets the project app desc map.
	 *
	 * @param projectApplicationsTbl the project applications tbl
	 * @return the project app desc map
	 */
	public Map<LANG_ENUM, String> getProjectAppDescMap(ProjectApplicationsTbl projectApplicationsTbl) {
		Map<LANG_ENUM, String> projectAppDescMap = new HashMap<>();
		String enDesc = null;
		String deDesc = null;
		Collection<ProjectAppTranslationTbl> projectAppTranslationTblCollection = projectApplicationsTbl.getProjectAppTranslationTblCollection();
		for (final ProjectAppTranslationTbl projectAppTranslationTbl : projectAppTranslationTblCollection) {
			if (projectAppTranslationTbl.getLanguageCode().getLanguageCode().equals(LANG_ENUM.ENGLISH.getLangCode())) {
				enDesc = projectAppTranslationTbl.getDescription();
			} else if (projectAppTranslationTbl.getLanguageCode().getLanguageCode().equals(LANG_ENUM.GERMAN.getLangCode())) {
				deDesc = projectAppTranslationTbl.getDescription();
			}
		}
		projectAppDescMap.put(LANG_ENUM.ENGLISH, XMSystemUtil.isEmpty(enDesc) ? "" : enDesc);
		projectAppDescMap.put(LANG_ENUM.GERMAN, XMSystemUtil.isEmpty(deDesc) ? "" : deDesc);
		return projectAppDescMap;
	}

	/**
	 * Gets the project desc map.
	 *
	 * @param projectsTblVo the projects tbl vo
	 * @return the project desc map
	 */
	public Map<LANG_ENUM, String> getProjectDescMap(ProjectsTbl projectsTblVo) {
		Map<LANG_ENUM, String> projectDescMap = new HashMap<>();
		String enDesc = null;
		String deDesc = null;
		Collection<ProjectTranslationTbl> projectTranslationTblCollection = projectsTblVo.getProjectTranslationTblCollection();
		for (final ProjectTranslationTbl projectTranslationTbl : projectTranslationTblCollection) {
			if (projectTranslationTbl.getLanguageCode().getLanguageCode().equals(LANG_ENUM.ENGLISH.getLangCode())) {
				enDesc = projectTranslationTbl.getDescription();
			} else if (projectTranslationTbl.getLanguageCode().getLanguageCode().equals(LANG_ENUM.GERMAN.getLangCode())) {
				deDesc = projectTranslationTbl.getDescription();
			}
		}
		if (XMSystemUtil.isEmpty(deDesc)  &&  !XMSystemUtil.isEmpty(enDesc)) {
			deDesc = enDesc;
		} else if (XMSystemUtil.isEmpty(enDesc)  &&  !XMSystemUtil.isEmpty(deDesc)) {
			enDesc = deDesc;
		}
		projectDescMap.put(LANG_ENUM.ENGLISH, enDesc);
		projectDescMap.put(LANG_ENUM.GERMAN, deDesc);
		return projectDescMap;
	}
}
