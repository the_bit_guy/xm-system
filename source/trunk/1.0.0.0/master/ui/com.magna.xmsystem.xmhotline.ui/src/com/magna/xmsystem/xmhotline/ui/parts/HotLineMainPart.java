package com.magna.xmsystem.xmhotline.ui.parts;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.eclipse.e4.core.contexts.ContextInjectionFactory;
import org.eclipse.e4.core.contexts.IEclipseContext;
import org.eclipse.e4.core.services.nls.Translation;
import org.eclipse.e4.ui.di.Persist;
import org.eclipse.swt.widgets.Composite;

import com.magna.xmsystem.xmhotline.ui.message.Message;
import com.magna.xmsystem.xmhotline.ui.message.MessageRegistry;

/**
 * Class for Hot-line main part.
 *
 * @author Chiranjeevi.Akula
 */
public class HotLineMainPart {
	
	/** The Constant ID. */
	public static final String ID = "com.magna.xmsystem.xmhotline.ui.part.HotlinePart";
	
	/** Member variable 'registry' for {@link MessageRegistry}. */
	@Inject
	private MessageRegistry registry;
	
	/** Member variable 'message' for {@link Message}. */
	@Inject
	@Translation
	private Message messages;

	/** The hotline comp action. */
	private HotLineCompositeAction hotlineCompAction;
	
	/** The eclipse context. */
	@Inject
	private IEclipseContext eclipseContext;
	
	/** The eclipse context message reg. */
	@Inject
	private IEclipseContext eclipseContextMessageReg;
	
	/**
	 * Method for Creates the composite.
	 *
	 * @param parent {@link Composite}
	 */
	@PostConstruct
	public void createComposite(final Composite parent) {
		eclipseContext.set(Composite.class, parent);
		eclipseContextMessageReg.set(MessageRegistry.class, registry);
		hotlineCompAction = ContextInjectionFactory.make(HotLineCompositeAction.class, eclipseContext, eclipseContextMessageReg);
		//this.hotlineCompAction = new HotLineCompositeAction(parent, SWT.NONE, registry, eventBroker, messages);
	}

	/**
	 * @return the hotlineCompAction
	 */
	public HotLineCompositeAction getHotlineCompAction() {
		return hotlineCompAction;
	}

	/**
	 * Method for Save.
	 */
	@Persist
	public void save() {
		//TO save Method Implementation
	}
}