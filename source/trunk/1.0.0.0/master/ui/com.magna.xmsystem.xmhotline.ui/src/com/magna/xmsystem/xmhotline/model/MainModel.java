
package com.magna.xmsystem.xmhotline.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.magna.xmbackend.vo.enums.CreationType;
import com.magna.xmsystem.xmhotline.model.interfaces.IXmHotlineModel;

/**
 *  This class contains model list which contains projects, user application and project application.
 */
public class MainModel implements IXmHotlineModel {
	
	/** The project user relation. */
	private Map<CreationType, List<ProjectModel>> projectUserRelation;
	
	/** The user app user relation. */
	private Map<CreationType, List<UserApplicationModel>> userAppUserRelation;
	
	/** The project app user relation. */
	private Map<CreationType, List<ProjectApplicationModel>> projectAppUserRelation;

	/**
	 * Instantiates a new main model.
	 */
	public MainModel() {
		projectUserRelation = new HashMap<>();
		userAppUserRelation = new HashMap<>();
		projectAppUserRelation = new HashMap<>();
	}

	/**
	 * @return the projectUserRelation
	 */
	public Map<CreationType, List<ProjectModel>> getProjectUserRelation() {
		return projectUserRelation;
	}
	
	/**
	 * Gets the project user relation add list.
	 *
	 * @return the project user relation add list
	 */
	public List<ProjectModel> getProjectUserRelationAddList() {
		if (this.projectUserRelation.get(CreationType.ADD) == null) {
			this.projectUserRelation.put(CreationType.ADD, new ArrayList<>());
		}
		return this.projectUserRelation.get(CreationType.ADD);
	}
	
	/**
	 * Gets the project user relation delete list.
	 *
	 * @return the project user relation delete list
	 */
	public List<ProjectModel> getProjectUserRelationDeleteList() {
		if (this.projectUserRelation.get(CreationType.DELETE) == null) {
			this.projectUserRelation.put(CreationType.DELETE, new ArrayList<>());
		}
		return this.projectUserRelation.get(CreationType.DELETE);
	}

	/**
	 * @return the userAppUserRelation
	 */
	public Map<CreationType, List<UserApplicationModel>> getUserAppUserRelation() {
		return userAppUserRelation;
	}
	
	/**
	 * Gets the user app user relation add list.
	 *
	 * @return the user app user relation add list
	 */
	public List<UserApplicationModel> getUserAppUserRelationAddList() {
		if (this.userAppUserRelation.get(CreationType.ADD) == null) {
			this.userAppUserRelation.put(CreationType.ADD, new ArrayList<>());
		}
		return this.userAppUserRelation.get(CreationType.ADD);
	}
	
	/**
	 * Gets the user app user relation delete list.
	 *
	 * @return the user app user relation delete list
	 */
	public List<UserApplicationModel> getUserAppUserRelationDeleteList() {
		if (this.userAppUserRelation.get(CreationType.DELETE) == null) {
			this.userAppUserRelation.put(CreationType.DELETE, new ArrayList<>());
		}
		return this.userAppUserRelation.get(CreationType.DELETE);
	}

	/**
	 * @return the projectAppUserRelation
	 */
	public Map<CreationType, List<ProjectApplicationModel>> getProjectAppUserRelation() {
		return projectAppUserRelation;
	}
	
	/**
	 * Gets the project app user relation add list.
	 *
	 * @return the project app user relation add list
	 */
	public List<ProjectApplicationModel> getProjectAppUserRelationAddList() {
		if (this.projectAppUserRelation.get(CreationType.ADD) == null) {
			this.projectAppUserRelation.put(CreationType.ADD, new ArrayList<>());
		}
		return this.projectAppUserRelation.get(CreationType.ADD);
	}
	
	/**
	 * Gets the project app user relation delete list.
	 *
	 * @return the project app user relation delete list
	 */
	public List<ProjectApplicationModel> getProjectAppUserRelationDeleteList() {
		if (this.projectAppUserRelation.get(CreationType.DELETE) == null) {
			this.projectAppUserRelation.put(CreationType.DELETE, new ArrayList<>());
		}
		return this.projectAppUserRelation.get(CreationType.DELETE);
	}
}
