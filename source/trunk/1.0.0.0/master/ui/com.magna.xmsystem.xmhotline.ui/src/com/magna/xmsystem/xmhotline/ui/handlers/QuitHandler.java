package com.magna.xmsystem.xmhotline.ui.handlers;

import javax.inject.Inject;

import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.core.services.nls.Translation;
import org.eclipse.e4.ui.workbench.IWorkbench;
import org.eclipse.swt.widgets.Shell;

import com.magna.xmsystem.ui.controls.widgets.CustomMessageDialog;
import com.magna.xmsystem.xmhotline.ui.message.Message;


/**
 * Class for Quit handler.
 *
 * @author Chiranjeevi.Akula
 */
public class QuitHandler {
	
	/** Member variable 'message' for {@link Message}. */
	@Inject
	@Translation
	private Message message;
	
	/**
	 * Method for Execute.
	 *
	 * @param workbench {@link IWorkbench}
	 * @param shell {@link Shell}
	 */
	@Execute
	public void execute(final IWorkbench workbench, final Shell shell){
		if (CustomMessageDialog.openConfirm(shell, message.exitDialogTitle,
				message.exitDialogMessage)) {
			workbench.close();
		}
	}
}
