package com.magna.xmsystem.xmhotline.ui;

import java.io.File;
import java.io.IOException;

import javax.inject.Inject;
import javax.inject.Named;

import org.eclipse.core.runtime.Platform;
import org.eclipse.e4.core.contexts.ContextInjectionFactory;
import org.eclipse.e4.core.contexts.IEclipseContext;
import org.eclipse.e4.core.di.annotations.Optional;
import org.eclipse.e4.core.services.events.IEventBroker;
import org.eclipse.e4.core.services.nls.Translation;
import org.eclipse.e4.ui.model.application.MApplication;
import org.eclipse.e4.ui.model.application.ui.basic.MWindow;
import org.eclipse.e4.ui.services.IServiceConstants;
import org.eclipse.e4.ui.workbench.UIEvents;
import org.eclipse.e4.ui.workbench.lifecycle.PostContextCreate;
import org.eclipse.e4.ui.workbench.lifecycle.PreSave;
import org.eclipse.e4.ui.workbench.lifecycle.ProcessAdditions;
import org.eclipse.e4.ui.workbench.lifecycle.ProcessRemovals;
import org.eclipse.e4.ui.workbench.modeling.IWindowCloseHandler;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Monitor;
import org.eclipse.swt.widgets.Shell;
import org.osgi.service.event.Event;
import org.osgi.service.event.EventHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.ResourceAccessException;

import com.magna.xmbackend.vo.enums.Application;
import com.magna.xmbackend.vo.user.AuthResponse;
import com.magna.xmsystem.dependencies.customExceptions.UnauthorizedAccessException;
import com.magna.xmsystem.dependencies.utils.APPLICATION;
import com.magna.xmsystem.dependencies.utils.OSValidator;
import com.magna.xmsystem.dependencies.utils.XMSystemUtil;
import com.magna.xmsystem.dependencies.utils.XmSystemEnvProcess;
import com.magna.xmsystem.ui.controls.widgets.CustomMessageDialog;
import com.magna.xmsystem.xmadmin.restclient.validation.AuthController;
import com.magna.xmsystem.xmhotline.ui.message.Message;
import com.magna.xmsystem.xmhotline.util.Constants;
import com.magna.xmsystem.xmhotline.util.XmHotlineUtil;
import com.piterion.security.manager.windows.TicketManager;

/**
 * This is a stub implementation containing e4 LifeCycle annotated methods.<br />
 * There is a corresponding entry in <em>plugin.xml</em> (under the
 * <em>org.eclipse.core.runtime.products' extension point</em>) that references
 * this class.
 *
 * @author shashwat.anand
 */
@SuppressWarnings("restriction")
public class E4LifeCycle {
	
	/**
	 * Logger instance
	 */
	private static final Logger LOGGER = LoggerFactory.getLogger(E4LifeCycle.class);
	
	/**
	 * Injecting event broker
	 */
	@Inject
	private IEventBroker eventBroker;
	
	/**
	 * Inject {@link IEclipseContext}
	 */
	@Inject
	private IEclipseContext eclipseContext;
	
	/** Member variable 'message' for {@link Message}. */
	@Inject
	@Translation
	private Message message;
	
	/** The Constant LOGIN_THEME. */
	public static final String LOGIN_THEME = "com.magna.xmhotline.themes.login";
	
	/**
	 * Member to store default location of XmHotline shell
	 */
	private Point defaultLocation;

	/**
	 * Method for Post context create.
	 *
	 * @param workbenchContext {@link IEclipseContext}
	 */
	@PostContextCreate
	void postContextCreate(final IEclipseContext context) {
		XmSystemEnvProcess.getInstance().start(APPLICATION.XMHOTLINE);

		try {
			boolean isWorkspaceLocked = Platform.getInstanceLocation().isLocked();
			if (isWorkspaceLocked) {
				CustomMessageDialog.openError(Display.getDefault().getActiveShell(), message.workspaceLockTitle,
						message.workspaceLockMsg);
				System.exit(0);
			}

			final File iconFolder = XMSystemUtil.getXMSystemServerIconsFolder();
			if (iconFolder == null) {
				CustomMessageDialog.openError(Display.getDefault().getActiveShell(), message.iconErrorTitle,
						message.iconErrorMsg);
				System.exit(0);
			}
		} catch (IOException e) {
			LOGGER.error("Execption ocuured while launching the application: ", e); //$NON-NLS-1$
		}

		initXmHotlineUtil();
		XmHotlineUtil.getInstance().setDefaultLocale();

		/*
		 * final AuthenticationWindow authenticationWindow = new
		 * AuthenticationWindow(message.windowtitle,
		 * Application.CAX_START_HOTLINE.name(), message.loginDialogUserName,
		 * message.loginDialogPassword, message.loginDialogLoginBtn,
		 * message.loginDialogCancelBtn);
		 * authenticationWindow.setUsername(XMSystemUtil.getSystemUserName());
		 */

		/*
		 * authenticationDialog.create();
		 * authenticationDialog.getShell().setText(message.dialogAppName);
		 */

		/*
		 * loginDialog.getShell().setText(message.dialogAppName); String cssTheme =
		 * CommonConstants.THEMES.LOGIN_THEME; context.set(E4Application.THEME_ID,
		 * cssTheme); String cssURI = "css/login.css";
		 * context.set(E4Workbench.CSS_URI_ARG, cssURI);
		 * PartRenderingEngine.initializeStyling(shell.getDisplay(), context);
		 */

		/*
		 * if (authenticationWindow.open() != Window.OK) { System.exit(0); } else {
		 */
		String encryptTicket = null;
		long startTime = System.currentTimeMillis();
		if (OSValidator.isWindows()) {
			try {
				TicketManager ticketManager = new TicketManager();
				encryptTicket = ticketManager.generateTicket();
			} catch (Exception e) {
				LOGGER.error("Execption ocuured while launching the application: ", e); //$NON-NLS-1$
			}

		} else if (OSValidator.isUnixOrLinux()) {
			try {
				com.piterion.security.manager.linux.TicketManager ticketManager = new com.piterion.security.manager.linux.TicketManager();
				encryptTicket = ticketManager.generateTicket();
			} catch (Exception e) {
				LOGGER.error("Execption ocuured while launching the application: ", e); //$NON-NLS-1$
			}
		}
		long endTime = System.currentTimeMillis();
		LOGGER.info("Time taken to generate the ticket at caxstarthotline : " + (endTime - startTime));

		try {
			long startTimeMillis = System.currentTimeMillis();
			AuthController authController = new AuthController();
			AuthResponse authResponse = authController.authorizeLogin(encryptTicket,
					Application.CAX_START_HOTLINE.name());
			long endTimeMillis = System.currentTimeMillis();
			if (authResponse != null) {
				boolean isValidUser = authResponse.isValidUser();
				long ldapResponseTime = authResponse.getLdapResponseTime();
				if (!isValidUser) {
					CustomMessageDialog.openError(Display.getCurrent().getActiveShell(), message.errorDialogTitile,
							authResponse.getMessage());
					System.exit(0);
				}
				XMSystemUtil.setSystemUserName(authResponse.getUsername());
				LOGGER.info("Ldap response time :" + ldapResponseTime);
				LOGGER.info("Webservice response time :" + (endTimeMillis - (startTimeMillis + ldapResponseTime) + "ms"));
			} else {
				System.exit(0);
			}
		} catch (RuntimeException e) {
			if (e instanceof ResourceAccessException) {
				CustomMessageDialog.openError(Display.getCurrent().getActiveShell(), message.errorDialogTitile, message.serverNotReachable);
				System.exit(0);
			} else if(e instanceof UnauthorizedAccessException) {
				CustomMessageDialog.openError(Display.getCurrent().getActiveShell(),
						message.errorDialogTitile, message.unauthorizedUserAccessMessage);
				System.exit(0);
			} else if(e instanceof IllegalArgumentException) {
				if (e.getMessage().equals("URI is not absolute")) {
					CustomMessageDialog.openError(Display.getCurrent().getActiveShell(), message.errorDialogTitile, message.inCorrectServerURI);
					System.exit(0);
				} else {
					LOGGER.error("Error while calling authorizeLogin REST Service!", e); //$NON-NLS-1$
				}
			} else if(e instanceof HttpClientErrorException) {
				if (e.getMessage().equals("404 Invalid resource access")) {
					CustomMessageDialog.openError(Display.getCurrent().getActiveShell(), message.errorDialogTitile, message.inCorrectServerURI);
					System.exit(0);
				} else {
					LOGGER.error("Error while calling authorizeLogin REST Service!", e); //$NON-NLS-1$
				}
			} else {
				LOGGER.error("Error while calling authorizeLogin REST Service!", e); //$NON-NLS-1$
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling authorizeLogin REST Service!", e); //$NON-NLS-1$
		}
	}
//	}

	/**
	 * Method for Pre save.
	 *
	 * @param workbenchContext {@link IEclipseContext}
	 */
	@PreSave
	void preSave(final IEclipseContext workbenchContext) {
	}

	/**
	 * Method to process additions
	 * 
	 * @param workbenchContext
	 *            {@link IEclipseContext}
	 * @param eventBroker
	 *            {@link IEventBroker}
	 */
	@ProcessAdditions
	void processAdditions(final IEclipseContext workbenchContext, final IEventBroker eventBroker,
			MApplication mApplication) {
		eventBroker.subscribe(UIEvents.UILifeCycle.APP_STARTUP_COMPLETE, new EventHandler() {
			/**
			 * Handles event
			 */
			@Override
			public void handleEvent(final Event event) {
				appStartupComplete(workbenchContext);

				MWindow mainWindow = findMainWindow(mApplication);

				mainWindow.getContext().set(IWindowCloseHandler.class, new IWindowCloseHandler() {

					@Override
					public boolean close(MWindow window) {
						if (CustomMessageDialog.openConfirm(Display.getDefault().getActiveShell(), message.exitDialogTitle,
								message.exitDialogMessage)) {
							return true;
						}
						return false;
					}
				});
			}
		});
	}
	
	private MWindow findMainWindow(final MApplication application) {
		return application.getChildren().get(0);
	}
	
	/**
	 * Method for Process removals.
	 *
	 * @param workbenchContext {@link IEclipseContext}
	 */
	@ProcessRemovals
	void processRemovals(final IEclipseContext workbenchContext) {
	}
	
	/**
	 * Method to init XmHotlineUtil
	 */
	private void initXmHotlineUtil() {
		XmHotlineUtil util = ContextInjectionFactory.make(XmHotlineUtil.class, eclipseContext);
		eclipseContext.set(XmHotlineUtil.class, util);
	}
	
	/**
	 * Receive active shell.
	 * @param shell the shell
	 */
	@Inject
	@Optional
	public void receiveActiveShell(@Named(IServiceConstants.ACTIVE_SHELL) final Shell shell) {
		try {
			if (shell != null) {
				if (this.defaultLocation == null) {
					final Display display = shell.getDisplay();
					final Monitor primary = display.getPrimaryMonitor();
					final Rectangle displayBounds = primary.getBounds();
					shell.setSize(displayBounds.width - 100, displayBounds.height - 100);
					final Point size = shell.getSize();
					this.defaultLocation = new Point((int) (displayBounds.width - size.x) / 2, (int) (displayBounds.height - size.y) / 2);
					shell.setLocation(this.defaultLocation);
				}
			}
		} catch (Exception e) {
			LOGGER.error("Execption ocuured in Active Shell", e); //$NON-NLS-1$
		}
	}
	
	/**
	 * Method call after startup is complete
	 * 
	 * @param workbenchContext
	 *            {@link IEclipseContext}
	 */
	private void appStartupComplete(final IEclipseContext workbenchContext) {
		XMSystemUtil.cleanupFilesOnExit();
		this.eventBroker.send(Constants.STATUSBAR, message.windowtitle);
	}
}
