package com.magna.xmsystem.xmhotline.ui;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;

// TODO: Auto-generated Javadoc
// TODO scp 2017-03-27: not used. by the way, a bundle context can also be injected
/**
 * Class for Activator.
 *
 * @author Chiranjeevi.Akula
 */
public class Activator implements BundleActivator {

	/** Member variable 'context' for {@link BundleContext}. */
	private static BundleContext context;

	/**
	 * Gets the context.
	 *
	 * @return the context
	 */
	static BundleContext getContext() {
		return context;
	}

	/* (non-Javadoc)
	 * @see org.osgi.framework.BundleActivator#start(org.osgi.framework.BundleContext)
	 */
	public void start(final BundleContext bundleContext) throws Exception {
		Activator.context = bundleContext;
	}

	/* (non-Javadoc)
	 * @see org.osgi.framework.BundleActivator#stop(org.osgi.framework.BundleContext)
	 */
	public void stop(final BundleContext bundleContext) throws Exception {
		Activator.context = null;
	}

}
