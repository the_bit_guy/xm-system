package com.magna.xmsystem.xmhotline.model;

import org.eclipse.swt.graphics.Image;

import com.magna.xmsystem.xmhotline.model.interfaces.IXmHotlineModel;

/**
 * This class contains Site id, name and image.
 */
public class SiteModel implements IXmHotlineModel {
	
	/** Member variable 'userId' for {@link SiteModel}. */
	private String siteId;
	
	/** Member variable 'userName' for {@link SiteModel}. */
	private String siteName;
	
	/** Member variable 'image' for {@link SiteModel}. */
	private Image image;

	/** The status. */
	private boolean status;
	
	/**
	 * Instantiates a new site model.
	 *
	 * @param siteId the site id
	 * @param siteName the site name
	 * @param status the status
	 * @param image the image
	 */
	public SiteModel(final String siteId, final String siteName, final boolean status, final Image image) {
		this.siteId = siteId;
		this.status = status;
		this.siteName = siteName;
		this.image = image;
	}
	
	/**
	 * Gets the site id.
	 *
	 * @return the siteId
	 */
	public final String getSiteId() {
		return siteId;
	}

	/**
	 * Sets the site id.
	 *
	 * @param siteId the siteId to set
	 */
	public final void setSiteId(final String siteId) {
		this.siteId = siteId;
	}

	/**
	 * @return the status
	 */
	public boolean isStatus() {
		return status;
	}

	/**
	 * @param status the status to set
	 */
	public void setStatus(boolean status) {
		this.status = status;
	}

	/**
	 * Gets the site name.
	 *
	 * @return the siteName
	 */
	public final String getSiteName() {
		return siteName;
	}

	/**
	 * Sets the site name.
	 *
	 * @param siteName the siteName to set
	 */
	public final void setSiteName(final String siteName) {
		this.siteName = siteName;
	}

	/**
	 * Gets the image.
	 *
	 * @return the image
	 */
	public final Image getImage() {
		return image;
	}

	/**
	 * Sets the image.
	 *
	 * @param image the image to set
	 */
	public final void setImage(final Image image) {
		this.image = image;
	}

}
