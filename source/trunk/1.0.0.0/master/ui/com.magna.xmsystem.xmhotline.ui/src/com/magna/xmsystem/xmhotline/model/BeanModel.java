package com.magna.xmsystem.xmhotline.model;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.HashMap;
import java.util.Map;

import com.magna.xmsystem.dependencies.utils.LANG_ENUM;

// TODO: Auto-generated Javadoc
/**
 * Abstract BeanModel Class which implements PropertyChangeListner interface.
 *
 * @author Roshan
 */
public abstract class BeanModel implements PropertyChangeListener {

	private Map<LANG_ENUM, String> translationIdMap;
	
	/** Instance of PropertyChangeSupport. */
	transient protected final PropertyChangeSupport propertyChangeSupport;

	/**
	 * Constructor.
	 */
	public BeanModel() {
		this.translationIdMap = new HashMap<>();
		this.propertyChangeSupport = new PropertyChangeSupport(this);
	}
	
	
	/**
	 * Gets the translation object.
	 *
	 * @return the translation object
	 */
	public Map<LANG_ENUM, String> getTranslationIdMap() {
		return this.translationIdMap;
	}
	
	/**
	 * Gets the translation object.
	 *
	 * @param lang_ENUM the lang ENUM
	 * @return the translation object
	 */
	public String getTranslationId(LANG_ENUM lang_ENUM) {
		return this.translationIdMap.get(lang_ENUM);
	}
	
	/**
	 * Sets the translation object.
	 *
	 * @param translationObject the translation object
	 */
	public void setTranslationIdMap(Map<LANG_ENUM, String> translationObject) {
		this.translationIdMap = translationObject;
	}
	
	/**
	 * Sets the translation object.
	 *
	 * @param lang_ENUM the lang ENUM
	 * @param translationId the translation id
	 */
	public void setTranslationId(LANG_ENUM lang_ENUM, String translationId) {
		this.translationIdMap.put(lang_ENUM, translationId);
	}

	/**
	 * abstract Method to propertyChange.
	 *
	 * @param event            {@link PropertyChangeEvent}
	 */
	@Override
	abstract public void propertyChange(final PropertyChangeEvent event);

	/**
	 * Method to addPropertyChangeListener.
	 *
	 * @param propertyName            {@link String}
	 * @param listener            {@link PropertyChangeListener}
	 */
	public void addPropertyChangeListener(final String propertyName, final PropertyChangeListener listener) {
		this.propertyChangeSupport.addPropertyChangeListener(propertyName, listener);
	}

	/**
	 * Method to removePropertyChangeListener.
	 *
	 * @param listener            {@link PropertyChangeListener}
	 */
	public void removePropertyChangeListener(final PropertyChangeListener listener) {
		this.propertyChangeSupport.removePropertyChangeListener(listener);
	}
}
