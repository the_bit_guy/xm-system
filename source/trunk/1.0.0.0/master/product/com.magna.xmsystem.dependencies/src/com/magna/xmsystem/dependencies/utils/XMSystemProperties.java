package com.magna.xmsystem.dependencies.utils;

import java.io.FileInputStream;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Class for XMSystem Properties
 * 
 * @author shashwat.anand
 *
 */
public final class XMSystemProperties extends Properties {
	private static final long serialVersionUID = 1L;
	/**
	 * Logger instance
	 */
	private static final Logger LOGGER = LoggerFactory.getLogger(XMSystemProperties.class);
	/**
	 * XMSystemProperties static reference
	 */
	private static XMSystemProperties thisRef;

	/**
	 * XMSystem properties files
	 */
	//private File xmSystemPropFile;

	/**
	 * Constructor
	 */
	private XMSystemProperties() {
		super();
		try {
			thisRef = this;
			/*final File xmSystemRootFolder = XMSystemUtil.getXMSystemRootFolder(true);
			this.xmSystemPropFile = new File(
					xmSystemRootFolder.getAbsolutePath() + File.separator + Constants.XMSYSTEM_PORPS_FILE);
			if (!xmSystemPropFile.exists()) {
				xmSystemPropFile.createNewFile();
			}*/
			String xmConfig = XmSystemEnvProcess.getInstance().getenv("XM_CONFIG");
			if (!XMSystemUtil.isEmpty(xmConfig)) { //$NON-NLS-1$
				this.load(new FileInputStream(xmConfig.trim()));
			} 
		} catch (Exception ex) {
			LOGGER.error("Execption ocuured in reading properties file", ex); //$NON-NLS-1$
		}
	}

	/**
	 * Gets the instance of {@link XMSystemProperties}
	 * 
	 * @return {@link XMSystemProperties}
	 */
	public static XMSystemProperties getInstance() {
		if (thisRef == null) {
			new XMSystemProperties();
		}
		return thisRef;
	}

	/**
	 * Get int property
	 * 
	 * @param key
	 *            {@link String}
	 * @return int
	 */
	/*public int getIntProperty(final String key) {
		return Integer.parseInt(getProperty(key));
	}*/

	/**
	 * Gets XMBackend Service Url
	 * 
	 * @return {@link String}
	 */
	/*public String getXMBackendServiceUrl() {
		try {
			if (this.getProperty("XMBACKEND_SERVICE_URL") == null) { //$NON-NLS-1$
				this.setProperty("XMBACKEND_SERVICE_URL", "http://localhost:8080/xmsystem/rest/"); //$NON-NLS-1$
				this.store(new FileOutputStream(this.xmSystemPropFile), null);
			}
		} catch (IOException e) {
			LOGGER.error("Execption ocuured in storing properties file", e); //$NON-NLS-1$
		}
		return this.getProperty("XMBACKEND_SERVICE_URL"); //$NON-NLS-1$
	}*/

	/**
	 * Gets XMBackend Service Url
	 * 
	 * @return {@link String}
	 */
	/*public String getXMSystemIconsLocation() {
		try {
			if (this.getProperty("SERVER_ICON_FOLDER") == null) { //$NON-NLS-1$
				this.setProperty("SERVER_ICON_FOLDER", "\\\\192.168.1.107\\magna_share\\icons"); //$NON-NLS-1$
				this.store(new FileOutputStream(this.xmSystemPropFile), null);
			}
		} catch (IOException e) {
			LOGGER.error("Execption ocuured in storing properties file", e); //$NON-NLS-1$
		}
		return this.getProperty("SERVER_ICON_FOLDER"); //$NON-NLS-1$
	}*/

	/**
	 * Gets XMBackend Service Url
	 * 
	 * @return {@link String}
	 */
	/*public String getXMSystemScriptsLocation() {
		try {
			if (this.getProperty("SERVER_SCRIPTS_FOLDER") == null) { //$NON-NLS-1$
				this.setProperty("SERVER_SCRIPTS_FOLDER", "\\\\192.168.1.107\\magna_share\\scripts"); //$NON-NLS-1$
				this.store(new FileOutputStream(this.xmSystemPropFile), null);
			}
		} catch (IOException e) {
			LOGGER.error("Execption ocuured in storing properties file", e); //$NON-NLS-1$
		}
		return this.getProperty("SERVER_SCRIPTS_FOLDER"); //$NON-NLS-1$
	}*/
}
