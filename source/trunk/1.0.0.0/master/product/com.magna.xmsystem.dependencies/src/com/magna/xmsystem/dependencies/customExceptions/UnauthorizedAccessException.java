package com.magna.xmsystem.dependencies.customExceptions;

/**
 * The Class UnauthorizedAccessException.
 * 
 * @author shashwat.anand
 */
public class UnauthorizedAccessException extends RuntimeException {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Instantiates a new unauthorized access exception.
	 *
	 * @param message the message
	 */
	public UnauthorizedAccessException(final String message) {
		super(message);
	}
}
