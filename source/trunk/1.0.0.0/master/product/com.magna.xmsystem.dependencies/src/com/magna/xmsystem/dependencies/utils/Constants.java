package com.magna.xmsystem.dependencies.utils;

/**
 * Interface contains the string constants
 * 
 * @author shashwat.anand
 *
 */
public interface Constants {
	/**
	 * XMSYSTEM_ROOT_FOLDER this folder exists at <user.root>/xmsystem
	 */
	String XMSYSTEM_ROOT_FOLDER = "xmsystem";
	/**
	 * XMADMIN_ROOT_FOLDER this folder exists at <user.root>/xmsystem/xmadmin
	 */
	String XMADMIN_ROOT_FOLDER = "xmsystem/xmadmin";
	/**
	 * XMADMIN_ROOT_FOLDER this folder exists at <user.root>/xmsystem/xmhotline
	 */
	String XMHOTLINE_ROOT_FOLDER = "xmsystem/xmhotline";
	/**
	 * XMADMIN_ROOT_FOLDER this folder exists at <user.root>/xmsystem/xmenu
	 */
	String XMMENU_ROOT_FOLDER = "xmsystem/xmenu";
	/**
	 * XMSYSTEM_ICON_FOLDER this folder exists at <user.root>/xmsystem/icons
	 */
	String XMSYSTEM_ICON_FOLDER = "icons";
	/**
	 * XMSYSTEM_PORPS_FILE properties file for xmsystem
	 */
	String XMSYSTEM_PORPS_FILE = "xmsystem.properties";
	/**
	 * SUPPORTED_ICON_EXT for xmsystem
	 */
	String[] SUPPORTED_ICON_EXT = {"*.gif", "*.png"};
	
	/** The debug mode. */
	boolean DEBUG_MODE = false;
}
