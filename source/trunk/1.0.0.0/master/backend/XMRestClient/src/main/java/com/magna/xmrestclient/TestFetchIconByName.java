/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magna.xmrestclient;

import com.magna.xmrestclient.client.icon.IconInvoker;

/**
 *
 * @author dhana
 */
public class TestFetchIconByName {
    public static void main(String[] args) {
        System.out.println("> main");
        IconInvoker iconInvoker = new IconInvoker();
        iconInvoker.getIconByName("ICONTest.jpeg");
        System.out.println("< main");
    }
}
