/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magna.xmrestclient;

import com.magna.xmrestclient.client.language.LanguageInvoker;

/**
 *
 * @author dhana
 */
public class TestLanguageGetAll {
    public static void main(String[] args) {
        System.out.println("Inside main");
        LanguageInvoker invoker = new LanguageInvoker();
        invoker.getAllLanguages();
        System.out.println("Exit main");
    }
}
