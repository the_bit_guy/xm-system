/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magna.xmrestclient;

import com.magna.xmbackend.vo.icon.Icon;
import com.magna.xmbackend.vo.XMObject;
import com.magna.xmrestclient.client.XMAdminIconClient;

/**
 *
 * @author dhana
 */
public class TestCreateIconByID {

    public static void main(String[] args) {
        System.out.println("Inside main");
        Icon icon = new Icon("ICONTest.jpeg", XMObject.ICON);
        XMAdminIconClient xMAdminIconClient = new XMAdminIconClient();
        xMAdminIconClient.createIconObject(icon);
        xMAdminIconClient.close();
        System.out.println("Exit main");
    }
}
