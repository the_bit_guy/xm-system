package com.magna.xmbackend.jpa.rel.dao;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.magna.xmbackend.entities.AdminAreaStartAppRelTbl;
import com.magna.xmbackend.entities.SiteAdminAreaRelTbl;
import com.magna.xmbackend.entities.StartApplicationsTbl;

/**
 *
 * @author vijay
 */
@Transactional
public interface AdminAreaStartAppRelJpaDao extends CrudRepository<AdminAreaStartAppRelTbl, String> {

    /**
     *
     * @param status
     * @param id
     * @return integer
     */
    @Modifying
    @Query("UPDATE AdminAreaStartAppRelTbl aasart set aasart.status = :status where aasart.adminAreaStartAppRelId = :id")
    int setStatusForAdminAreaStartAppRelTbl(@Param("status") String status, @Param("id") String id);

    /**
     * 
     * @param startApplicationId
     * @return List<AdminAreaStartAppRelTbl>
     */
    List<AdminAreaStartAppRelTbl> findByStartApplicationId(StartApplicationsTbl startApplicationId);
    

    /**
     * Find by site admin area rel id in.
     *
     * @param siteAdminAreaTbls the site admin area tbls
     * @return the list
     */
    List<AdminAreaStartAppRelTbl> findBySiteAdminAreaRelIdIn(List<SiteAdminAreaRelTbl> siteAdminAreaTbls);
}
