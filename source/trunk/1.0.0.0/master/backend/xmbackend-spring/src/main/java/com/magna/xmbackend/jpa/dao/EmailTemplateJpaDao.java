/**
 * 
 */
package com.magna.xmbackend.jpa.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.magna.xmbackend.entities.EmailTemplateTbl;

/**
 * @author Bhabadyuti Bal
 *
 */
public interface EmailTemplateJpaDao extends JpaRepository<EmailTemplateTbl, String>{

	EmailTemplateTbl findByName(String name);
	
	@Query("select ett.messageTemplate from EmailNotificationConfigTbl enct, EmailTemplateTbl ett, EmailNotificationEventTbl enet "
			+ "where enct.emailNotificationEventId = enet.emailNotificationEventId and enct.emailTemplateId = ett.emailTemplateId "
			+ " and enet.event=:event")
	String findMessageTemplateByEvent(@Param("event") String event);

	
}
