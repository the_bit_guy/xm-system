/**
 * 
 */
package com.magna.xmbackend.audit.mgr;

import javax.servlet.http.HttpServletRequest;

import com.magna.xmbackend.entities.DirectoryTbl;
import com.magna.xmbackend.vo.directory.DirectoryRequest;

/**
 * @author Bhabadyuti Bal
 *
 */
public interface DirectoryAuditMgr {
	
	void directoryCreateSuccessAudit(HttpServletRequest httpServletRequest, DirectoryTbl dt);
	
	void directoryCreateFailureAudit(HttpServletRequest httpServletRequest, DirectoryRequest dr, String errMsg);
	
	void dirDeleteFailureAudit(HttpServletRequest hsr, String id, String errMsg);
	
	void dirDeleteSuccessAudit(HttpServletRequest hsr, String id, String name);
	
	void dirUpdateSuccessAudit(HttpServletRequest hsr, String id, String name);

    void dirUpdateFailureAudit(HttpServletRequest hsr, DirectoryRequest dr,
            String errMsg);
    

}
