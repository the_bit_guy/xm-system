/**
 * 
 */
package com.magna.xmbackend.audit.rel.mgr;

import javax.servlet.http.HttpServletRequest;

import com.magna.xmbackend.entities.AdminAreaProjectRelTbl;
import com.magna.xmbackend.vo.rel.AdminAreaProjectRelBatchRequest;
import com.magna.xmbackend.vo.rel.AdminAreaProjectRelBatchResponse;

/**
 * @author Bhabadyuti Bal
 *
 */
public interface AdminAreaProjectAuditMgr {

	void adminAreaProjectMultiSaveFailureAuditor(HttpServletRequest httpServletRequest, Exception ex);

	void adminAreaProjectMultiSaveAuditor(AdminAreaProjectRelBatchRequest adminAreaProjectRelBatchRequest,
			AdminAreaProjectRelBatchResponse aaprbr, HttpServletRequest httpServletRequest);

	void userProjectMultiDeleteSuccessAudit(AdminAreaProjectRelTbl adminAreaProjectRelTbl,
			HttpServletRequest httpServletRequest);

	void adminAreaProjectStatusUpdateAuditor(boolean updateStatusById, String status, String id,
			HttpServletRequest httpServletRequest);

	void adminAreaProjectStatusUpdateFailureAuditor(String id, String status, Exception ex,
			HttpServletRequest httpServletRequest);

}
