package com.magna.xmbackend.mgr;

import java.util.List;
import java.util.Set;

import com.magna.xmbackend.entities.BaseApplicationsTbl;
import com.magna.xmbackend.vo.baseApplication.BaseApplicationRequest;
import com.magna.xmbackend.vo.baseApplication.BaseApplicationResponse;
import com.magna.xmbackend.vo.permission.ValidationRequest;
import javax.servlet.http.HttpServletRequest;

// TODO: Auto-generated Javadoc
/**
 * The Interface BaseApplicationMgr.
 *
 * @author dhana
 */
public interface BaseApplicationMgr {

    /**
     * Find all.
     *
     * @param validationRequest the validation request
     * @return BaseApplicationResponse
     */
    BaseApplicationResponse findAll(final ValidationRequest validationRequest);

    /**
     * Find by id.
     *
     * @param id the id
     * @return BaseApplicationsTbl
     */
    BaseApplicationsTbl findById(final String id);

    /**
     * Creates the.
     *
     * @param baseApplicationRequest the base application request
     * @return BaseApplicationsTbl
     */
    BaseApplicationsTbl create(final BaseApplicationRequest baseApplicationRequest);

    /**
     * Update.
     *
     * @param baseApplicationRequest the base application request
     * @return BaseApplicationsTbl
     */
    BaseApplicationsTbl update(final BaseApplicationRequest baseApplicationRequest);

    /**
     * Update status by id.
     *
     * @param status the status
     * @param id the id
     * @return boolean
     */
    boolean updateStatusById(final String status, final String id);

    /**
     * Delete by id.
     *
     * @param id the id
     * @return boolean
     */
    boolean deleteById(final String id);

    /**
     * Multi delete.
     *
     * @param baseAppIds the base app ids
     * @param httpServletRequest the http servlet request
     * @return BaseApplicationResponse
     */
    BaseApplicationResponse multiDelete(final Set<String> baseAppIds,
            final HttpServletRequest httpServletRequest);

    /**
     * Find by name.
     *
     * @param name the name
     * @return the base applications tbl
     */
    BaseApplicationsTbl findByName(String name);

	/**
	 * Multi update.
	 *
	 * @param baseAppRequests the base app requests
	 * @param httpServletRequest the http servlet request
	 * @return the base application response
	 */
	BaseApplicationResponse multiUpdate(List<BaseApplicationRequest> baseAppRequests,
			HttpServletRequest httpServletRequest);

	/**
	 * Update for batch.
	 *
	 * @param baseApplicationRequest the base application request
	 * @return the base applications tbl
	 */
	BaseApplicationsTbl updateForBatch(BaseApplicationRequest baseApplicationRequest);
}
