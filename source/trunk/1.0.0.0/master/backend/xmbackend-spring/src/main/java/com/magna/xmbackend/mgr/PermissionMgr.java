package com.magna.xmbackend.mgr;

import com.magna.xmbackend.entities.PermissionTbl;
import com.magna.xmbackend.vo.permission.PermissionApiResponse;
import com.magna.xmbackend.vo.permission.PermissionResponse;
import com.magna.xmbackend.vo.permission.ValidationRequest;

/**
 *
 * @author vijay
 */
public interface PermissionMgr {

    /**
     *
     * @param roleId
     * @return PermissionResponse
     */
    PermissionResponse findObjectPermissionByRoleId(final String roleId);

    /**
     *
     * @param roleId
     * @return PermissionResponse
     */
    PermissionResponse findObjectRelPermissionByRoleId(final String roleId);

    /**
     *
     * @return PermissionResponse
     */
    PermissionResponse findAllObjectRelationPermission();

    /**
     *
     * @return PermissionResponse
     */
    PermissionResponse findAllObjectPermission();

    /**
     *
     * @return PermissionResponse
     */
    PermissionResponse findAllAABasedRelationPermission();

    /**
     *
     * @return PermissionResponse
     */
    PermissionResponse findAll();

    /**
     *
     * @param apiPath
     * @return boolean
     */
    boolean isExclusionApi(final String apiPath);

    /**
     *
     * @param apiPath
     * @param userName
     * @param adminAreaId
     * @return boolean
     */
    boolean validateUserApiHavingPermission(final String apiPath,
            final String userName, final String adminAreaId);

    /**
     *
     * @param validationRequest
     * @return PermissionTbl
     */
    PermissionTbl isObjectAccessAllowed(final ValidationRequest validationRequest);

    /**
     *
     * @return PermissionApiResponse
     */
    PermissionApiResponse retrieveAllPermissionApiMappingRelation();

	boolean isAccessAllowed(String requestPath, String userName, String adminAreaId);
}
