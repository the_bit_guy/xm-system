package com.magna.xmbackend.mgr;

import java.util.List;
import java.util.Map;

import com.magna.xmbackend.vo.adminHistory.AdminHistoryRequest;

/**
 * The Interface AdminHistoryMgr.
 * 
 * @author shashwat.anand
 */
public interface AdminHistoryMgr {
	
	/**
	 * Find admin base obj history.
	 *
	 * @param adminHistoryRequest the admin history request
	 * @return the list
	 */
	List<Map<String, Object>> findAdminBaseObjHistory(final AdminHistoryRequest adminHistoryRequest);
	
	/**
	 * Find admin rel history.
	 *
	 * @param adminHistoryRequest the admin history request
	 * @return the list
	 */
	List<Map<String, Object>> findAdminRelHistory(final AdminHistoryRequest adminHistoryRequest);
}
