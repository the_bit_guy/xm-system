package com.magna.xmbackend.mgr.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Component;

import com.magna.xmbackend.audit.mgr.UserAppAuditMgr;
import com.magna.xmbackend.controller.AuthController;
import com.magna.xmbackend.entities.AdminAreaUserAppRelTbl;
import com.magna.xmbackend.entities.AdminAreasTbl;
import com.magna.xmbackend.entities.BaseApplicationsTbl;
import com.magna.xmbackend.entities.DirectoryRefTbl;
import com.magna.xmbackend.entities.IconsTbl;
import com.magna.xmbackend.entities.LanguagesTbl;
import com.magna.xmbackend.entities.SiteAdminAreaRelTbl;
import com.magna.xmbackend.entities.UserAppTranslationTbl;
import com.magna.xmbackend.entities.UserApplicationsTbl;
import com.magna.xmbackend.entities.UserTkt;
import com.magna.xmbackend.entities.UserUserAppRelTbl;
import com.magna.xmbackend.entities.UsersTbl;
import com.magna.xmbackend.exception.CannotCreateObjectException;
import com.magna.xmbackend.exception.XMObjectNotFoundException;
import com.magna.xmbackend.jpa.dao.UserAppTranslationJpaDao;
import com.magna.xmbackend.jpa.dao.UserApplicationJpaDao;
import com.magna.xmbackend.jpa.dao.UserTktJpaDao;
import com.magna.xmbackend.jpa.rel.dao.AdminAreaUserAppRelJpaDao;
import com.magna.xmbackend.jpa.rel.dao.DirectoryRelJpaDao;
import com.magna.xmbackend.jpa.rel.dao.SiteAdminAreaRelJpaDao;
import com.magna.xmbackend.jpa.rel.dao.UserUserAppRelJpaDao;
import com.magna.xmbackend.mgr.UserApplicationMgr;
import com.magna.xmbackend.mgr.UserMgr;
import com.magna.xmbackend.utils.MessageMaker;
import com.magna.xmbackend.utils.Validator;
import com.magna.xmbackend.vo.enums.Application;
import com.magna.xmbackend.vo.enums.ApplicationPosition;
import com.magna.xmbackend.vo.enums.ApplicationRelationType;
import com.magna.xmbackend.vo.enums.Status;
import com.magna.xmbackend.vo.enums.UserRelationType;
import com.magna.xmbackend.vo.permission.ValidationRequest;
import com.magna.xmbackend.vo.userApplication.UserApplicationMenuResponse;
import com.magna.xmbackend.vo.userApplication.UserApplicationMenuWrapper;
import com.magna.xmbackend.vo.userApplication.UserApplicationRequest;
import com.magna.xmbackend.vo.userApplication.UserApplicationResponse;
import com.magna.xmbackend.vo.userApplication.UserApplicationTranslation;

/**
 *
 * @author dhana
 */
@Component
public class UserApplicationMgrImpl implements UserApplicationMgr {

    private static final Logger LOG = LoggerFactory.getLogger(UserApplicationMgrImpl.class);

    @Autowired
    private UserApplicationJpaDao userApplicationJpaDao;

    @Autowired
    private UserAppTranslationJpaDao userAppTranslationJpaDao;

    @Autowired
    private UserMgr userMgr;

    @Autowired
    private DirectoryRelJpaDao directoryRelJpaDao;

    @Autowired
    SiteAdminAreaRelJpaDao siteAdminAreaRelJpaDao;

    @Autowired
    AdminAreaUserAppRelJpaDao adminAreaUserAppRelJpaDao;

    @Autowired
    UserUserAppRelJpaDao userUserAppRelJpaDao;

    @Autowired
    private Validator validator;
    @Autowired
    private MessageMaker messageMaker;

    @Autowired
    UserTktJpaDao userTktJpaDao;

    @Autowired
    UserAppAuditMgr userAppAuditMgr;
    
    @Autowired
    private AuthController authController;

    /**
     *
     * @param validationRequest
     * @return UserApplicationResponse
     */
    @Override
    public final UserApplicationResponse findAll(final ValidationRequest validationRequest) {
        LOG.info(">> findAll");
        Iterable<UserApplicationsTbl> uatsUserApplicationsTbls = this.userApplicationJpaDao.findAll();
        final boolean isViewInactive = validator.isViewInactiveAllowed(validationRequest);
        LOG.info("findAll isViewInactive={}", isViewInactive);
        uatsUserApplicationsTbls = validator.filterUserApplicationResponse(isViewInactive, uatsUserApplicationsTbls);
        final UserApplicationResponse uar = new UserApplicationResponse(uatsUserApplicationsTbls);
        LOG.info("<< findAll");
        return uar;
    }

    /**
     *
     * @param id
     * @return UserApplicationsTbl
     */
    @Override
    public final UserApplicationsTbl findById(final String id) {
        LOG.info(">> findById {}", id);
        final UserApplicationsTbl uat = this.userApplicationJpaDao.findOne(id);
        LOG.info("<< findById");
        return uat;
    }

    /**
     *
     * @param id
     * @param validationRequest
     * @return UserApplicationsTbl
     */
    @Override
    public final UserApplicationsTbl findById(final String id,
            final ValidationRequest validationRequest) {
        LOG.info(">> findById {}", id);
        final boolean isViewInactive = validator.isViewInactiveAllowed(validationRequest);
        LOG.info("findById isViewInactive={}", isViewInactive);
        UserApplicationsTbl uat = this.userApplicationJpaDao.findOne(id);
        uat = validator.filterUserApplicationResponse(isViewInactive, uat);
        LOG.info("<< findById");
        return uat;
    }

    /**
     *
     * @param id
     * @return boolean
     */
    @Override
    public final boolean delete(final String id) {
        LOG.info(">> delete {}", id);
        boolean isDeleted = false;
        try {
            this.removeDirectoryRelations(id);
            isDeleted = true;
            this.userApplicationJpaDao.delete(id);
        } catch (Exception e) {
            if (e instanceof EmptyResultDataAccessException) {
                final String[] param = {id};
                //throw from here and catch this in multidelete catch block
                throw new XMObjectNotFoundException("User not found", "P_ERR0013", param);
            }
        }

        LOG.info("<< delete");
        return isDeleted;
    }

    /**
     *
     * @param userAppIds
     * @param httpServletRequest
     * @return UserApplicationResponse
     */
    @Override
    public UserApplicationResponse multiDelete(final Set<String> userAppIds,
            final HttpServletRequest httpServletRequest) {
        LOG.info(">> multiDelete");
        final List<Map<String, String>> statusMaps = new ArrayList<>();
        userAppIds.forEach(userAppId -> {
            UserApplicationsTbl uat = null;
            try {
                uat = findById(userAppId);
                this.delete(userAppId);
                this.userAppAuditMgr.userAppDeleteStatusAuditor(userAppId,
                        httpServletRequest, true, uat);
            } catch (XMObjectNotFoundException objectNotFound) {
                Map<String, String> statusMap = messageMaker.extractFromException(objectNotFound);
                statusMaps.add(statusMap);
                this.userAppAuditMgr.userAppDeleteStatusAuditor(userAppId,
                        httpServletRequest, false, uat);
            }
        });
        UserApplicationResponse userAppResponse = new UserApplicationResponse(statusMaps);
        LOG.info(">> multiDelete");
        return userAppResponse;
    }

    /**
     *
     * @param id
     */
    private void removeDirectoryRelations(final String id) {
        List<DirectoryRefTbl> directoryRefTbls = this.directoryRelJpaDao.findByObjectId(id);
        if (!directoryRefTbls.isEmpty()) {
            this.directoryRelJpaDao.delete(directoryRefTbls);
        }
    }

    /**
     *
     * @param userApplicationRequest
     * @param isUpdate
     * @return UserApplicationsTbl
     */
    @Override
    public final UserApplicationsTbl createOrUpdate(final UserApplicationRequest userApplicationRequest,
            final boolean isUpdate) {
        LOG.info(">> createOrUpdate --> isUpdate {}", isUpdate);
        /*if (!isUpdate) {
            this.checkIfObjectAlreadyExist(userApplicationRequest);
        }*/
        final UserApplicationsTbl uatIn = this.convert2Entity(userApplicationRequest, isUpdate);
        final UserApplicationsTbl uatOut = this.userApplicationJpaDao.save(uatIn);
        LOG.info("<< createOrUpdate");
        return uatOut;
    }

    /*public void checkIfObjectAlreadyExist(UserApplicationRequest userApplicationRequest) {
        List<UserApplicationTranslation> userApplicationTranslations = userApplicationRequest.getUserApplicationTranslations();
        if (!userApplicationTranslations.isEmpty()) {
            for (UserApplicationTranslation userApplicationTranslation : userApplicationTranslations) {
                String userAppName = userApplicationTranslation.getName();
                if (userAppName != null) {
                    String languageCode = userApplicationTranslation.getLanguageCode();
                    UserAppTranslationTbl userAppTranslationTbl = this.userAppTranslationJpaDao
                            .findByNameIgnoreCaseAndLanguageCode(userAppName, new LanguagesTbl(languageCode));
                    if (userAppTranslationTbl != null) {
                        Map<String, String[]> paramMap = getUserAppWithi18nCode(userAppTranslationTbl);
                        throw new CannotCreateObjectException("Record Already Exist", "UA_ERR00011", paramMap);
                    }
                }
            }
        }
    }*/
    /**
     *
     * @param uarIn
     * @param isUpdate
     * @return UserApplicationsTbl
     */
    private UserApplicationsTbl convert2Entity(final UserApplicationRequest uarIn,
            final boolean isUpdate) {
        String id = UUID.randomUUID().toString();
        final Date date = new Date();
        if (isUpdate) {
            id = uarIn.getId();
        }
        final String baseAppId = uarIn.getBaseAppId();
        final String name = uarIn.getName();
        final String description = uarIn.getDescription();
        final String iconId = uarIn.getIconId();
        final String isParent = uarIn.getIsParent();
        final String position = uarIn.getPosition();
        final String isSingleTon = uarIn.getIsSingleTon();
        final String status = uarIn.getStatus();
        final List<UserApplicationTranslation> translations = uarIn.getUserApplicationTranslations();

        if (!isUpdate && null != this.findUserAppIdForName(name)) {
            final Map<String, String[]> paramMap
                    = this.messageMaker.getUserAppNameWithi18nCode(translations, name);
            throw new CannotCreateObjectException(
                    "User application name already found", "ERR0002", paramMap);
        }

        final UserApplicationsTbl uat = new UserApplicationsTbl(id);
        BaseApplicationsTbl bat2Set = null;
        if (baseAppId != null) {
            bat2Set = new BaseApplicationsTbl(baseAppId);
        }
        uat.setBaseApplicationId(bat2Set);
        uat.setName(name);
        uat.setDescription(description);
        uat.setCreateDate(date);
        uat.setUpdateDate(date);
        uat.setIconId(new IconsTbl(iconId));
        uat.setIsParent(isParent);
        uat.setPosition(position);
        uat.setIsSingleton(isSingleTon);
        uat.setStatus(status);

        final List<UserAppTranslationTbl> userAppTranslationTbls = new ArrayList<>();

        if(translations != null) {
        	for (UserApplicationTranslation translation : translations) {
                String userAppTransId = UUID.randomUUID().toString();
                final String displayName = translation.getName();
                final String displayDescription = translation.getDescription();
                final String remarks = translation.getRemarks();
                final String languageCode = translation.getLanguageCode();
                if (isUpdate) {
                    userAppTransId = translation.getId();
                }

                final UserAppTranslationTbl uatt = new UserAppTranslationTbl(userAppTransId);
                uatt.setCreateDate(date);
                uatt.setUpdateDate(date);
                uatt.setName(displayName);
                uatt.setDescription(displayDescription);
                uatt.setRemarks(remarks);
                uatt.setLanguageCode(new LanguagesTbl(languageCode));
                uatt.setUserApplicationId(new UserApplicationsTbl(id));
                userAppTranslationTbls.add(uatt);
            }
        } else {
        	UserAppTranslationTbl enTranslation = new UserAppTranslationTbl(UUID.randomUUID().toString(), date, date, new LanguagesTbl("en"), uat);
        	UserAppTranslationTbl geTranslation = new UserAppTranslationTbl(UUID.randomUUID().toString(), date, date, new LanguagesTbl("de"), uat);
        	userAppTranslationTbls.add(enTranslation);
        	userAppTranslationTbls.add(geTranslation);
        }
        

        uat.setUserAppTranslationTblCollection(userAppTranslationTbls);
        return uat;
    }

    /**
     * Find user app id for name.
     *
     * @param name the name
     * @return the string
     */
    private String findUserAppIdForName(final String name) {
        final UserApplicationsTbl userApplicationsTbl = this.userApplicationJpaDao.findByNameIgnoreCase(name);
        String userApplicationsId = null;
        if (userApplicationsTbl != null) {
            userApplicationsId = userApplicationsTbl.getUserApplicationId();
        }
        LOG.info(">> findUserAppIdForName - User applcaition id for name is {} - {}", name, userApplicationsId);
        return userApplicationsId;
    }

    /**
     *
     * @param status
     * @param id
     * @return boolean
     */
    @Override
    public final boolean updateStatusById(final String status, final String id) {
        LOG.info(">> updateStatusById");
        boolean isUpdated = false;
        final int out = this.userApplicationJpaDao.setStatusForUserApplicationsTbl(status, id);
        LOG.debug("is Modified status value {}", out);
        if (out > 0) {
            isUpdated = true;
        }
        LOG.info("<< updateStatusById");
        return isUpdated;
    }

    /**
     *
     * @param id
     * @param validationRequest
     * @return UserApplicationResponse
     */
    @Override
    public final UserApplicationResponse findUserApplicationsByBaseAppId(final String id,
            final ValidationRequest validationRequest) {
        UserApplicationResponse userApplicationResponse = null;
        LOG.info(">> findUserApplicationsByBaseAppId {}", id);
        final boolean isViewInactive = validator.isViewInactiveAllowed(validationRequest);
        LOG.info("findUserApplicationsByBaseAppId isViewInactive={}", isViewInactive);
        final BaseApplicationsTbl baseApplicationsTbl = validator.validateBaseApp(id, isViewInactive);
        final Collection<UserApplicationsTbl> userApplicationsTbls = baseApplicationsTbl
                .getUserApplicationsTblCollection();
        if (!userApplicationsTbls.isEmpty()) {
            final Iterable<UserApplicationsTbl> userAppTbls
                    = validator.filterUserApplicationResponse(isViewInactive, userApplicationsTbls);
            userApplicationResponse = new UserApplicationResponse(userAppTbls);
        } else {
            throw new XMObjectNotFoundException("No Base App User App Relation found", "BA_ERR0004");
        }
        LOG.info("<< findUserApplicationsByBaseAppId");
        return userApplicationResponse;
    }

    /**
     *
     * @param position
     * @param validationRequest
     * @return UserApplicationResponse
     */
    @Override
    public UserApplicationResponse findUserAppByPosition(final String position,
            final ValidationRequest validationRequest) {
        LOG.info(">> findUserAppByPosition {}", position);
        final boolean isViewInactive = validator.isViewInactiveAllowed(validationRequest);
        LOG.info("findUserAppByPosition isViewInactive={}", isViewInactive);
        Iterable<UserApplicationsTbl> userApplicationsTbls = this.userApplicationJpaDao.findByPosition(position);
        userApplicationsTbls = validator.filterUserApplicationResponse(isViewInactive, userApplicationsTbls);
        final UserApplicationResponse uar = new UserApplicationResponse(userApplicationsTbls);
        LOG.info("<< findUserAppByPosition ");
        return uar;
    }

    /**
     *
     * @param status
     * @return UserApplicationResponse
     */
    @Override
    public UserApplicationResponse findAllUserAppByStatus(final String status) {
        LOG.info(">> findAllUserAppByStatus {}", status);
        final Iterable<UserApplicationsTbl> uats = this.userApplicationJpaDao.findByStatus(status);
        final UserApplicationResponse uar = new UserApplicationResponse(uats);
        LOG.info("<< findAllUserAppByStatus ");
        return uar;
    }

    /**
     *
     * @return UserApplicationResponse
     * @param validationRequest
     */
    @Override
    public UserApplicationResponse findAllUserAppByPositions(final ValidationRequest validationRequest) {
        LOG.info(">> findAllUserAppByPositions ");
        final boolean isViewInactive = validator.isViewInactiveAllowed(validationRequest);
        LOG.info("findAllUserAppByPositions isViewInactive={}", isViewInactive);
        Iterable<UserApplicationsTbl> uats = this.userApplicationJpaDao.findByPositionPattern(
                ApplicationPosition.ICONTASK.toString(), ApplicationPosition.BUTTONTASK.toString(),
                ApplicationPosition.MENUTASK.toString());
        uats = validator.filterUserApplicationResponse(isViewInactive, uats);
        final UserApplicationResponse uar = new UserApplicationResponse(uats);
        LOG.info("<< findAllUserAppByPositions ");
        return uar;
    }

    /**
     * @param name
     * @return UserApplicationsTbl
     */
    @Override
    public final UserApplicationsTbl findByName(final String name) {
        LOG.info(">> findByName {}", name);
        final UserApplicationsTbl userApplicationsTbl = this.userApplicationJpaDao.findByNameIgnoreCase(name);
        if (null == userApplicationsTbl) {
            final String[] param = {name};
            throw new XMObjectNotFoundException("User application with name not found", "U_ERR00010", param);
        }
        LOG.info("<< findByName");
        return userApplicationsTbl;
    }

    /**
     * Find by user name and language code.
     *
     * @param name the name
     * @param languageCode the language code
     * @return the user applications tbl
     */
    @Override
    public final UserApplicationsTbl findByUserNameAndLanguageCode(final String name, final String languageCode) {
        LOG.info(">> findByUserNameAndLanguageCode {}", name, languageCode);
        final UserAppTranslationTbl userAppTranslationTbl = this.userAppTranslationJpaDao.findByNameIgnoreCaseAndLanguageCode(name,
                new LanguagesTbl(languageCode));
        final UserApplicationsTbl userApplicationsTbl = userAppTranslationTbl.getUserApplicationId();
        LOG.info("<< findByUserNameAndLanguageCode");
        return userApplicationsTbl;
    }

    /**
     *
     * @param id
     * @return UserApplicationResponse
     */
    @Override
    public final UserApplicationResponse findUserApplicationsByUserId(final String id) {
        UserApplicationResponse userApplicationResponse = null;
        LOG.info(">> findUserApplicationsByBaseAppId {}", id);
        final UsersTbl usersTbl = this.userMgr.findById(id);
        if (null != usersTbl) {
            final Collection<UserUserAppRelTbl> userUserAppRelTbls = usersTbl
                    .getUserUserAppRelTblCollection();
            if (!userUserAppRelTbls.isEmpty()) {
                final Collection<UserApplicationsTbl> userApplicationsTbls = new ArrayList<>();
                for (final UserUserAppRelTbl userUserAppRelTbl : userUserAppRelTbls) {
                    final UserApplicationsTbl userApplicationsTbl = userUserAppRelTbl.getUserApplicationId();
                    if (null != userApplicationsTbl) {
                        userApplicationsTbls.add(userApplicationsTbl);
                    }
                }
                userApplicationResponse = new UserApplicationResponse(userApplicationsTbls);
            } else {
                throw new RuntimeException("No User User App Relation found");
            }
        } else {
            throw new RuntimeException("No UserId found");
        }
        LOG.info("<< findUserApplicationsByBaseAppId");
        return userApplicationResponse;
    }

    /**
     *
     * @param adminAreaId
     * @return UserApplicationResponse
     */
    @Override
    public UserApplicationResponse findAllUserAppsByAAIdAndActive(final String adminAreaId) {
        final List<SiteAdminAreaRelTbl> adminAreaRelTbls = this.siteAdminAreaRelJpaDao.findByAdminAreaId(new AdminAreasTbl(adminAreaId));
        List<AdminAreaUserAppRelTbl> adminAreaUserAppRelTbls = this.adminAreaUserAppRelJpaDao.findBySiteAdminAreaRelIdInAndStatus(adminAreaRelTbls, "ACTIVE");
        List<UserApplicationsTbl> userApplicationsTbls = new ArrayList<>();
        if (!adminAreaUserAppRelTbls.isEmpty()) {
            for (AdminAreaUserAppRelTbl aauartbl : adminAreaUserAppRelTbls) {
                userApplicationsTbls.add(aauartbl.getUserApplicationId());
            }
            if (userApplicationsTbls.isEmpty()) {
                final String[] param = {adminAreaId};
                throw new XMObjectNotFoundException("No User App found", "S_ERR0010", param);
            }
        }

        UserApplicationResponse response = new UserApplicationResponse(userApplicationsTbls);
        return response;
    }

    /**
     *
     * @param userId
     * @param adminAreaId
     * @return UserApplicationResponse
     */
    @Override
    public UserApplicationResponse getAllUserAppsByUserAAId(final String userId,
            final String adminAreaId) {
        LOG.info(">> getAllUserAppsByUserAAId");
        List<SiteAdminAreaRelTbl> siteAdminAreaRelTbls = this.siteAdminAreaRelJpaDao.findByAdminAreaId(new AdminAreasTbl(adminAreaId));
        List<UserApplicationsTbl> userApplicationsTbls = new ArrayList<>();
        if (!siteAdminAreaRelTbls.isEmpty()) {
            final List<AdminAreaUserAppRelTbl> adminAreaUserAppRelTbls = this.adminAreaUserAppRelJpaDao.findBySiteAdminAreaRelIdIn(siteAdminAreaRelTbls);
            if (!adminAreaUserAppRelTbls.isEmpty()) {
                for (final AdminAreaUserAppRelTbl adminAreaUserAppRelTbl : adminAreaUserAppRelTbls) {
                    userApplicationsTbls.add(adminAreaUserAppRelTbl.getUserApplicationId());
                }
            }
        }
        final List<UserUserAppRelTbl> userUserAppRelTbls = this.userUserAppRelJpaDao.findByUserApplicationIdInAndUserId(userApplicationsTbls, new UsersTbl(userId));
        final List<UserApplicationsTbl> userApplicationsTbls2 = new ArrayList<>();
        if (!userUserAppRelTbls.isEmpty()) {
            for (final UserUserAppRelTbl userUserAppRelTbl : userUserAppRelTbls) {
                userApplicationsTbls2.add(userUserAppRelTbl.getUserApplicationId());
            }
        }
        final UserApplicationResponse response = new UserApplicationResponse(userApplicationsTbls2);
        LOG.info("<< getAllUserAppsByUserAAId");
        return response;
    }

    /*
    @Override
    public final UserApplicationMenuWrapper findUserAppByAAIdAndUserName(final String aaId,
            final String userName, final ValidationRequest validationRequest) {
        LOG.info(">> findUserAppByAAIdAndUserName");
        final List<UserApplicationMenuResponse> userApplicationMenuResponses = new ArrayList<>();
        List<AdminAreaUserAppRelTbl> adminAreaUserAppRelTbls
                = this.adminAreaUserAppRelJpaDao.findAdminAreaUserAppRelTbls(new AdminAreasTbl(aaId),
                        Status.ACTIVE.name(), Status.ACTIVE.name());
        List<UserUserAppRelTbl> userUserAppRelTbls
                = this.userUserAppRelJpaDao.findUserUserAppRelTbls(userName,
                        new AdminAreasTbl(aaId), Status.ACTIVE.name());
        final boolean isViewInactive = validator.isViewInactiveAllowed(validationRequest);
        LOG.info("findUserAppByAAIdAndUserName isViewInactive={}", isViewInactive);
        for (AdminAreaUserAppRelTbl adminAreaUserAppRelTbl : adminAreaUserAppRelTbls) {
            UserApplicationsTbl reqUserApplicationsTbl = null;
            String siteAdminAreaRelId = adminAreaUserAppRelTbl.getSiteAdminAreaRelId().getSiteAdminAreaRelId();
            UserApplicationsTbl userApplicationsTbl = adminAreaUserAppRelTbl.getUserApplicationId();
            userApplicationsTbl = validator.filterUserApplicationResponse(isViewInactive, userApplicationsTbl);
            if (null != userApplicationsTbl
                    && null != userApplicationsTbl.getUserApplicationId()) {
                String userApplicationId = userApplicationsTbl.getUserApplicationId();
                if (ApplicationRelationType.FIXED.name().equals(adminAreaUserAppRelTbl.getRelType())) {
                    boolean isAllowed = true;
                    for (UserUserAppRelTbl userUserAppRelTbl : userUserAppRelTbls) {
                        if (siteAdminAreaRelId.equals(userUserAppRelTbl.getSiteAdminAreaRelId().getSiteAdminAreaRelId())
                                && userApplicationId
                                        .equals(userUserAppRelTbl.getUserApplicationId().getUserApplicationId())
                                && UserRelationType.NOTALLOWED.name().equals(userUserAppRelTbl.getUserRelType())) {
                            isAllowed = false;
                            break;
                        }
                    }
                    if (isAllowed) {
                        reqUserApplicationsTbl = userApplicationsTbl;
                    }
                } else if (ApplicationRelationType.NOTFIXED.name().equals(adminAreaUserAppRelTbl.getRelType())
                        || ApplicationRelationType.PROTECTED.name().equals(adminAreaUserAppRelTbl.getRelType())) {
                    for (UserUserAppRelTbl userUserAppRelTbl : userUserAppRelTbls) {
                        if (siteAdminAreaRelId.equals(userUserAppRelTbl.getSiteAdminAreaRelId().getSiteAdminAreaRelId())
                                && userApplicationId
                                        .equals(userUserAppRelTbl.getUserApplicationId().getUserApplicationId())
                                && UserRelationType.ALLOWED.name().equals(userUserAppRelTbl.getUserRelType())) {
                            reqUserApplicationsTbl = userApplicationsTbl;
                            break;
                        }
                    }
                }

                if (reqUserApplicationsTbl != null) {
                    UserApplicationMenuResponse userApplicationMenuResponse = new UserApplicationMenuResponse();
                    userApplicationMenuResponse.setUserApplicationsTbl(reqUserApplicationsTbl);
                    boolean isParent = Boolean.valueOf(reqUserApplicationsTbl.getIsParent());
                    if (isParent) {
                        Iterable<UserApplicationsTbl> userAppChildren
                                = this.userApplicationJpaDao.findByPosition(reqUserApplicationsTbl.getUserApplicationId());
                        userApplicationMenuResponse.setUserAppChildren(userAppChildren);
                    }
                    userApplicationMenuResponses.add(userApplicationMenuResponse);
                }
            }
        }

        LOG.info("<< findUserAppByAAIdAndUserName");
        return new UserApplicationMenuWrapper(userApplicationMenuResponses);
    }*/
    
    /**
     * Find user applications by AA id and user name. XMenu
     *
     * @param aaId the aa id
     * @param tkt the tkt
     * @param userName the user name
     * @param validationRequest the validation request
     * @return the user application menu wrapper
     */
    @Override
    public final UserApplicationMenuWrapper findUserAppByAAIdAndUserName(final String aaId, final String tkt,
            final String userName, final ValidationRequest validationRequest) {
        LOG.info(">> findUserAppByAAIdAndUserName");

        List<String> status = new ArrayList<>();
        status.add(Status.ACTIVE.name());
        boolean isViewInactive = validator.isViewInactiveAllowed(validationRequest);

        if (!isViewInactive) {
        	final UserTkt userTktTbl = this.userTktJpaDao.findByTktAndApplicationName(this.authController.hash(tkt),
                    Application.CAX_ADMIN_MENU.name());
            if (userTktTbl != null) {
                validationRequest.setUserName(userTktTbl.getUsername());
                isViewInactive = validator.isViewInactiveAllowed(validationRequest);
            }
        }
        
        final List<UserApplicationsTbl> userApplications = new ArrayList<>();
        
        final List<UserApplicationsTbl> activeUserApplications = getUserApplicationsBasedonStatus(aaId, userName, status);
        
        if(activeUserApplications != null) {
        	userApplications.addAll(activeUserApplications);
        }
        if(isViewInactive) {
        	status.add(Status.INACTIVE.name());
        	final List<UserApplicationsTbl> activeInactiveUserApplications = getUserApplicationsBasedonStatus(aaId, userName, status);
        	if (activeInactiveUserApplications != null && userApplications.size() != activeInactiveUserApplications.size()) {
				for (UserApplicationsTbl activeInactiveUserApplication : activeInactiveUserApplications) {
					if (!userApplications.contains(activeInactiveUserApplication)) {
						activeInactiveUserApplication.setStatus(Status.INACTIVE.name());
						userApplications.add(activeInactiveUserApplication);
					}
				}				
			} 
        }
        
        final List<UserApplicationMenuResponse> userApplicationMenuResponses = new ArrayList<>();
        
        for(UserApplicationsTbl userApplication : userApplications) {
        	UserApplicationMenuResponse userApplicationMenuResponse = new UserApplicationMenuResponse();
			userApplicationMenuResponse.setUserApplicationsTbl(userApplication);
			boolean isParent = Boolean.valueOf(userApplication.getIsParent());
			if (isParent) {
				Iterable<UserApplicationsTbl> userAppChildren = this.userApplicationJpaDao
						.findByPosition(userApplication.getUserApplicationId());
				if (userAppChildren != null) {
					userAppChildren = validator.filterUserApplicationResponse(isViewInactive, userAppChildren);
				}
				userApplicationMenuResponse.setUserAppChildren(userAppChildren);
			}
			userApplicationMenuResponses.add(userApplicationMenuResponse);
        }

        LOG.info("<< findUserAppByAAIdAndUserName");
        return new UserApplicationMenuWrapper(userApplicationMenuResponses);
    }
    
    
    
	@Override
	public UserApplicationMenuWrapper findUserAppAndChildByName(String userAppName, String tkt, String userName,
			ValidationRequest validationRequest) {
		LOG.info(">> findUserAppAndChildByName");
		boolean isViewInactive = validator.isViewInactiveAllowed(validationRequest);

		if (!isViewInactive) {
			final UserTkt userTktTbl = this.userTktJpaDao.findByTktAndApplicationName(this.authController.hash(tkt),
					Application.CAX_ADMIN_MENU.name());
			if (userTktTbl != null) {
				validationRequest.setUserName(userTktTbl.getUsername());
				isViewInactive = validator.isViewInactiveAllowed(validationRequest);
			}
		}
		final List<UserApplicationMenuResponse> userApplicationMenuResponses = new ArrayList<>();

		UserApplicationsTbl userApplicationsTbl = this.findByName(userAppName);
		UserApplicationMenuResponse userApplicationMenuResponse = new UserApplicationMenuResponse();
		userApplicationMenuResponse.setUserApplicationsTbl(userApplicationsTbl);

		boolean isParent = Boolean.valueOf(userApplicationsTbl.getIsParent());
		if (isParent) {
			Iterable<UserApplicationsTbl> userAppChildren = this.userApplicationJpaDao
					.findByPosition(userApplicationsTbl.getUserApplicationId());
			if (userAppChildren != null) {
				userAppChildren = validator.filterUserApplicationResponse(isViewInactive, userAppChildren);
			}
			userApplicationMenuResponse.setUserAppChildren(userAppChildren);
		}
		userApplicationMenuResponses.add(userApplicationMenuResponse);
		LOG.info("<< findUserAppAndChildByName");
		return new UserApplicationMenuWrapper(userApplicationMenuResponses);
	}
    
    

	/**
	 * Gets the user applications basedon status. XMenu
	 *
	 * @param aaId the aa id
	 * @param userName the user name
	 * @param status the status
	 * @return the user applications basedon status
	 */
	private List<UserApplicationsTbl> getUserApplicationsBasedonStatus(final String aaId, final String userName, final List<String> status) {
		final List<UserApplicationsTbl> userApplications = new ArrayList<>();
		
		List<AdminAreaUserAppRelTbl> adminAreaUserAppRelTbls = this.adminAreaUserAppRelJpaDao
				.findAdminAreaUserAppRelTbls(new AdminAreasTbl(aaId), status);
		List<UserUserAppRelTbl> userUserAppRelTbls = this.userUserAppRelJpaDao.findUserUserAppRelTbls(userName,
				new AdminAreasTbl(aaId), status);
		for (AdminAreaUserAppRelTbl adminAreaUserAppRelTbl : adminAreaUserAppRelTbls) {
			UserApplicationsTbl reqUserApplicationsTbl = null;
			String siteAdminAreaRelId = adminAreaUserAppRelTbl.getSiteAdminAreaRelId().getSiteAdminAreaRelId();
			UserApplicationsTbl userApplicationsTbl = adminAreaUserAppRelTbl.getUserApplicationId();
			if (null != userApplicationsTbl && null != userApplicationsTbl.getUserApplicationId()) {
				String userApplicationId = userApplicationsTbl.getUserApplicationId();
				if (ApplicationRelationType.FIXED.name().equals(adminAreaUserAppRelTbl.getRelType())) {
					boolean isAllowed = true;
					for (UserUserAppRelTbl userUserAppRelTbl : userUserAppRelTbls) {
						if (siteAdminAreaRelId.equals(userUserAppRelTbl.getSiteAdminAreaRelId().getSiteAdminAreaRelId())
								&& userApplicationId
										.equals(userUserAppRelTbl.getUserApplicationId().getUserApplicationId())
								&& UserRelationType.FORBIDDEN.name().equals(userUserAppRelTbl.getUserRelType())) {
							isAllowed = false;
							break;
						}
					}
					if (isAllowed) {
						reqUserApplicationsTbl = userApplicationsTbl;
					}
				} else if (ApplicationRelationType.NOTFIXED.name().equals(adminAreaUserAppRelTbl.getRelType())
						|| ApplicationRelationType.PROTECTED.name().equals(adminAreaUserAppRelTbl.getRelType())) {
					for (UserUserAppRelTbl userUserAppRelTbl : userUserAppRelTbls) {
						if (siteAdminAreaRelId.equals(userUserAppRelTbl.getSiteAdminAreaRelId().getSiteAdminAreaRelId())
								&& userApplicationId
										.equals(userUserAppRelTbl.getUserApplicationId().getUserApplicationId())
								&& UserRelationType.ALLOWED.name().equals(userUserAppRelTbl.getUserRelType())) {
							reqUserApplicationsTbl = userApplicationsTbl;
							break;
						}
					}
				}

				if (reqUserApplicationsTbl != null) {
					userApplications.add(reqUserApplicationsTbl);
				}
			}
		}
		
		return userApplications;
	}

	@Override
	public UserApplicationResponse multiUpdate(List<UserApplicationRequest> userApplicationRequests,
			HttpServletRequest httpServletRequest) {
		LOG.info(">> multiUpdate");
		List<String> userAppNames = new ArrayList<>();
		userApplicationRequests.forEach(userApplicationRequest -> {
			String userAppName = userApplicationRequest.getName();
			String status = userApplicationRequest.getStatus();
			try {
				boolean updateStatusById = this.updateStatusById(status, userApplicationRequest.getId());
				if (updateStatusById) {
					// success & audit it
					this.userAppAuditMgr.userAppStatusUpdateSuccessAuditor(userAppName, status, httpServletRequest);
				} else {
					userAppNames.add(userAppName);
				}
			} catch (Exception ex) {
				this.userAppAuditMgr.userAppStatusUpdateFailureAuditor(userAppName, status, ex, httpServletRequest);
			}

		});
		UserApplicationResponse userApplicationResponse = new UserApplicationResponse();
		userApplicationResponse.setStatusUpdatationFailedList(userAppNames);
		LOG.info("<< multiUpdate");
		return userApplicationResponse;
	}

	@Override
	public UserApplicationsTbl updateForBatch(UserApplicationRequest userApplicationRequest) {
		LOG.info(">> updateForBatch");
		UserApplicationsTbl applicationsTbl = this.findByName(userApplicationRequest.getName());
		if (applicationsTbl == null) {
			throw new XMObjectNotFoundException("user app not found", "AA_ERR0017", new String[]{userApplicationRequest.getName()});
		}
		String status = (userApplicationRequest.getStatus() == null)?(applicationsTbl.getStatus()):userApplicationRequest.getStatus();
		
		String iconId = (userApplicationRequest.getIconId() == null) ? (applicationsTbl.getIconId().getIconId())
				: userApplicationRequest.getIconId();
		
		String baseAppId = null;
		if(applicationsTbl.getBaseApplicationId()!=null){
			baseAppId = applicationsTbl.getBaseApplicationId().getBaseApplicationId();
		}
		baseAppId = (userApplicationRequest.getBaseAppId() == null ) ? ( baseAppId) : (userApplicationRequest.getBaseAppId());
		String isParent = (userApplicationRequest.getIsParent() == null ) ? (applicationsTbl.getIsParent()) : (userApplicationRequest.getIsParent());
		String isSingleton = (userApplicationRequest.getIsSingleTon() == null ) ? (applicationsTbl.getIsSingleton()) : (userApplicationRequest.getIsSingleTon());
		String pos = (userApplicationRequest.getPosition() == null ) ? (applicationsTbl.getPosition()) : (userApplicationRequest.getPosition());
		
		if(isParent.equals(true)){
			// set singleton to false and application to null
			baseAppId = null;
			isSingleton = "false";
		} 
		userApplicationRequest.setId(applicationsTbl.getUserApplicationId());
		userApplicationRequest.setStatus(status);
		userApplicationRequest.setIconId(iconId);
		userApplicationRequest.setBaseAppId(baseAppId);
		userApplicationRequest.setIsParent(isParent);
		userApplicationRequest.setIsSingleTon(isSingleton);
		userApplicationRequest.setPosition(pos);
		
		List<UserApplicationTranslation> userApplicationTranslations = userApplicationRequest.getUserApplicationTranslations();
		if (userApplicationTranslations != null && userApplicationTranslations.size() > 0) {
			for (UserApplicationTranslation userApplicationTranslation : userApplicationTranslations) {
				for (UserAppTranslationTbl userAppTranslationTbl : applicationsTbl.getUserAppTranslationTblCollection()) {
					if (userAppTranslationTbl.getLanguageCode().getLanguageCode()
							.equals(userApplicationTranslation.getLanguageCode())) {
						userApplicationTranslation.setId(userAppTranslationTbl.getUserAppTranslationId());
					}
				}
			}
		}
		
		
		final UserApplicationsTbl uaIn = this.convert2Entity(userApplicationRequest, true);
        final UserApplicationsTbl uatOut = this.userApplicationJpaDao.save(uaIn);
		
		LOG.info("<< updateForBatch");
		return uatOut;
	}

}
