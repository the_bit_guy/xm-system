/**
 * 
 */
package com.magna.xmbackend.utils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import com.magna.xmbackend.vo.enums.IDateTimeFormatConst;

/**
 * @author Bhabadyuti Bal
 *
 */
@Component
public class DateUtil {

	private static final Logger LOG = LoggerFactory.getLogger(DateUtil.class);
	
	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	public static String getFormattedDateAsString(final Date date){
		String dateStr = null;
		try {
			DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
			dateStr = df.format(date);
		} catch (Exception e) {
			LOG.error("Error/Exception occured while date formatting {}", e.getMessage());
		}
		return dateStr;
	}
	
	
	public String getDBDateTimeFormat() {
		return this.jdbcTemplate.queryForObject("SELECT value FROM NLS_SESSION_PARAMETERS where parameter='NLS_TIMESTAMP_FORMAT'",String.class);
	}
	
	
	public String getDbSpecificDate(String date) {
		date = date.replaceAll("'", "");
		String sql = "SELECT to_char(to_timestamp(?,"+IDateTimeFormatConst.DB_DATE_TIME_FORMAT+")) from dual";
		/*DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MMM-yyyy hh:mm:ss a", Locale.ENGLISH);
		LocalDateTime dateTime = LocalDateTime.parse(date, formatter);
		DateTimeFormatter formatter2 = DateTimeFormatter.ofPattern("dd-MMM-yy HH:mm:ss", Locale.ENGLISH);
		String format = formatter2.format(dateTime);*/
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern(IDateTimeFormatConst.UI_DATE_TIME_FORMAT, Locale.ENGLISH);
		LocalDateTime dateTime = LocalDateTime.parse(date, formatter);
		String format = formatter.format(dateTime);
		String newDate = this.jdbcTemplate.queryForObject(sql, new Object[]{format}, String.class);
		return "'"+newDate+"'";
	}
	
	/*@Transactional
	public void alterDbSessionDate() {
		int update = this.jdbcTemplate.update("ALTER SESSION SET NLS_TIMESTAMP_FORMAT = 'YYYY/MM/DD HH24:MI:SS'");
	}*/
}
