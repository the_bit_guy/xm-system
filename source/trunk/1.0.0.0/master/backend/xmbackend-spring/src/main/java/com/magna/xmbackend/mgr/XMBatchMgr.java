/**
 * 
 */
package com.magna.xmbackend.mgr;

import java.sql.SQLSyntaxErrorException;

import com.magna.xmbackend.xmbatch.XmbatchResponse;

/**
 * @author Bhabadyuti Bal
 *
 */
public interface XMBatchMgr {

	public XmbatchResponse findResultSet(final String query) throws SQLSyntaxErrorException;
}
