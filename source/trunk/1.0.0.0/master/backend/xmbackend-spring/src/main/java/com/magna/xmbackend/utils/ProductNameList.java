/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magna.xmbackend.utils;

import java.util.List;

/**
 *
 * @author Admin
 */
public class ProductNameList {
    private List<String> productNames;
    private String featureType;

    /**
     * @return the productNames
     */
    public List<String> getProductNames() {
        return productNames;
    }

    /**
     * @param productNames the productNames to set
     */
    public void setProductNames(List<String> productNames) {
        this.productNames = productNames;
    }

    /**
     * @return the featureType
     */
    public String getFeatureType() {
        return featureType;
    }

    /**
     * @param featureType the featureType to set
     */
    public void setFeatureType(String featureType) {
        this.featureType = featureType;
    }
}
