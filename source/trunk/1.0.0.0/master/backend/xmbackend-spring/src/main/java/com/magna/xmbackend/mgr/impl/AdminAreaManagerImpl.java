package com.magna.xmbackend.mgr.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Component;

import com.magna.xmbackend.audit.mgr.AdminAreaAudiMgr;
import com.magna.xmbackend.controller.AuthController;
import com.magna.xmbackend.entities.AdminAreaProjAppRelTbl;
import com.magna.xmbackend.entities.AdminAreaProjectRelTbl;
import com.magna.xmbackend.entities.AdminAreaStartAppRelTbl;
import com.magna.xmbackend.entities.AdminAreaTranslationTbl;
import com.magna.xmbackend.entities.AdminAreaUserAppRelTbl;
import com.magna.xmbackend.entities.AdminAreasTbl;
import com.magna.xmbackend.entities.IconsTbl;
import com.magna.xmbackend.entities.LanguagesTbl;
import com.magna.xmbackend.entities.ProjectApplicationsTbl;
import com.magna.xmbackend.entities.ProjectsTbl;
import com.magna.xmbackend.entities.SiteAdminAreaRelTbl;
import com.magna.xmbackend.entities.SitesTbl;
import com.magna.xmbackend.entities.StartApplicationsTbl;
import com.magna.xmbackend.entities.UserApplicationsTbl;
import com.magna.xmbackend.entities.UserTkt;
import com.magna.xmbackend.exception.CannotCreateObjectException;
import com.magna.xmbackend.exception.XMObjectNotFoundException;
import com.magna.xmbackend.jpa.dao.AdminAreaJpaDao;
import com.magna.xmbackend.jpa.dao.SiteJpaDao;
import com.magna.xmbackend.jpa.dao.UserTktJpaDao;
import com.magna.xmbackend.mgr.AdminAreaManager;
import com.magna.xmbackend.mgr.ProjectApplicationMgr;
import com.magna.xmbackend.mgr.ProjectMgr;
import com.magna.xmbackend.mgr.StartApplicationMgr;
import com.magna.xmbackend.mgr.UserApplicationMgr;
import com.magna.xmbackend.utils.MessageMaker;
import com.magna.xmbackend.utils.Validator;
import com.magna.xmbackend.vo.adminArea.AdminAreaRequest;
import com.magna.xmbackend.vo.adminArea.AdminAreaResponse;
import com.magna.xmbackend.vo.adminArea.AdminAreaTranslation;
import com.magna.xmbackend.vo.enums.Application;
import com.magna.xmbackend.vo.enums.Status;
import com.magna.xmbackend.vo.permission.ValidationRequest;

/**
 *
 * @author dhana
 */
@Component
public class AdminAreaManagerImpl implements AdminAreaManager {

    private static final Logger LOG
            = LoggerFactory.getLogger(AdminAreaManagerImpl.class);

    @Autowired
    private AdminAreaJpaDao adminAreaJpaDao;

    @Autowired
    private ProjectMgr projectMgr;

    @Autowired
    private StartApplicationMgr startAppMgr;

    @Autowired
    private UserApplicationMgr userApplicationMgr;

    @Autowired
    private ProjectApplicationMgr projectApplicationMgr;

    @Autowired
    private StartApplicationMgr startApplicationMgr;

    @Autowired
    private Validator validator;

    @Autowired
    private MessageMaker messageMaker;
    @Autowired
    UserTktJpaDao userTktJpaDao;

    @Autowired
    private AdminAreaAudiMgr adminAreaAudiMgr;
    
    @Autowired
    private SiteJpaDao siteJpaDao;
    
    @Autowired
    private AuthController authController;

    /**
     *
     * @param validationRequest
     * @return AdminAreaResponse
     */
    @Override
    public final AdminAreaResponse findAll(final ValidationRequest validationRequest) {
        LOG.info(">> findAll");
        Iterable<AdminAreasTbl> adminAreasTbls = this.adminAreaJpaDao.findAll();
        final boolean isViewInactive = validator.isViewInactiveAllowed(validationRequest);
        LOG.info("findAll isViewInactive={}", isViewInactive);
        adminAreasTbls = validator.filterAdminAreaResponse(isViewInactive, adminAreasTbls);
        final AdminAreaResponse aar = new AdminAreaResponse(adminAreasTbls);
        LOG.info("<< findAll");
        return aar;
    }

    /**
     *
     * @param id
     * @return AdminAreasTbl
     */
    @Override
    public final AdminAreasTbl findById(final String id) {
        LOG.info(">> findById");
        final AdminAreasTbl aat = this.adminAreaJpaDao.findOne(id);
        LOG.info("<< findById");
        return aat;
    }

    /**
     *
     * @param adminAreaRequest
     * @return AdminAreasTbl
     */
    @Override
    public final AdminAreasTbl save(final AdminAreaRequest adminAreaRequest) {
        LOG.info(">> save");
        final AdminAreasTbl aatConvert = this.convert2Entity(adminAreaRequest, false);
        final AdminAreasTbl aatIn = this.adminAreaJpaDao.save(aatConvert);
        LOG.info("<< save");
        return aatIn;
    }

    /**
     *
     * @param adminAreaRequest
     * @return AdminAreasTbl
     */
    @Override
    public final AdminAreasTbl update(final AdminAreaRequest adminAreaRequest) {
        LOG.info(">> update");
        final AdminAreasTbl aaIn = this.convert2Entity(adminAreaRequest, true);
        final AdminAreasTbl aaOut = this.adminAreaJpaDao.save(aaIn);
        LOG.info("<< update");
        return aaOut;
    }
    
    
	@Override
	public AdminAreasTbl updateForBatch(AdminAreaRequest adminAreaRequest, HttpServletRequest httpServletRequest) {
		LOG.info(">> updateForBatch");
		AdminAreasTbl areasTbl = this.findByName(adminAreaRequest.getName());
		if (areasTbl == null) {
			throw new XMObjectNotFoundException("Admin area not found", "P_ERR0000", new String[]{adminAreaRequest.getName()});
		}
		String status = (adminAreaRequest.getStatus() == null)?(areasTbl.getStatus()):adminAreaRequest.getStatus();
		String iconId = (adminAreaRequest.getIconId() == null) ? (areasTbl.getIconId().getIconId()): adminAreaRequest.getIconId();
		Long singleTonAppTimeOut = (Long.valueOf(adminAreaRequest.getSingleTonAppTimeOut()) == null)?(areasTbl.getSingletonAppTimeout()):adminAreaRequest.getSingleTonAppTimeOut();
		String hotlineContactEmail = (adminAreaRequest.getHotlineContactEmail() == null) ? (areasTbl.getHotlineContactEmail()): adminAreaRequest.getHotlineContactEmail();
		String contactNo = (adminAreaRequest.getHotlineContactNo() == null) ? (areasTbl.getHotlineContactNumber()): adminAreaRequest.getHotlineContactNo();
		
		adminAreaRequest.setId(areasTbl.getAdminAreaId());
		adminAreaRequest.setStatus(status);
		adminAreaRequest.setIconId(iconId);
		adminAreaRequest.setSingleTonAppTimeOut(singleTonAppTimeOut);
		adminAreaRequest.setHotlineContactEmail(hotlineContactEmail);
		adminAreaRequest.setHotlineContactNo(contactNo);

		
		List<AdminAreaTranslation> adminAreaTranslations = adminAreaRequest.getAdminAreaTranslations();
		if (adminAreaTranslations != null && adminAreaTranslations.size() > 0) {
			for (AdminAreaTranslation adminAreaTranslation : adminAreaTranslations) {
				for (AdminAreaTranslationTbl adminAreaTranslationTbl : areasTbl.getAdminAreaTranslationTblCollection()) {
					if (adminAreaTranslationTbl.getLanguageCode().getLanguageCode()
							.equals(adminAreaTranslation.getLanguageCode())) {
						adminAreaTranslation.setId(adminAreaTranslationTbl.getAdminAreaTranslationId());
					}
				}
			}
		}
		
		AdminAreasTbl outOut = null;
		try {
			final AdminAreasTbl aaIn = this.convert2Entity(adminAreaRequest, true);
			outOut = this.adminAreaJpaDao.save(aaIn);
			this.adminAreaAudiMgr.admainAreaUpdateStatusAuditor(status, outOut.getAdminAreaId(), httpServletRequest, true, outOut);
		} catch (Exception ex) {
			String id = adminAreaRequest.getId();
			LOG.error("Exception / Error occured in updating " + "Admin Area with id {}", id);
			this.adminAreaAudiMgr.adminAreaUpdateFailureAuditor(adminAreaRequest, httpServletRequest);
			throw ex;
		}
		LOG.info("<< updateForBatch");
		return outOut;
	}
    

	@Override
	public AdminAreaResponse multiUpdate(List<AdminAreaRequest> adminAreaRequests, HttpServletRequest servletRequest) {
		LOG.info(">> multiUpdate");
		List<String> aaNames = new ArrayList<>();
		adminAreaRequests.forEach(adminAreaRequest -> {
			String adminAreaName = adminAreaRequest.getName();
			String status = adminAreaRequest.getStatus();
			try {
				final int stat = this.adminAreaJpaDao.setStatusForAdminAreasTbl(status, adminAreaRequest.getId());
				if (stat > 0) {
					// success & audit it
					this.adminAreaAudiMgr.adminAreaStatusUpdateSuccessAuditor(adminAreaName, status, servletRequest);
				} else {
					aaNames.add(adminAreaName);
				}
			} catch (Exception ex) {
				this.adminAreaAudiMgr.adminAreaStatusUpdateFailureAuditor(adminAreaName, status, ex, servletRequest);
			}

		});
		AdminAreaResponse adminAreaResponse = new AdminAreaResponse();
		adminAreaResponse.setStatusUpdatationFailedList(aaNames);
		LOG.info("<< multiUpdate");
		return adminAreaResponse;
	}
    
    

    /**
     *
     * @param adminAreaRequest
     * @param isUpdate
     * @return AdminAreasTbl
     */
    private AdminAreasTbl convert2Entity(final AdminAreaRequest adminAreaRequest,
            final boolean isUpdate) {
        String id = UUID.randomUUID().toString();
        if (isUpdate) {
            id = adminAreaRequest.getId();
        }
        final Date date = new Date();
        final AdminAreasTbl adminAreasTbl = new AdminAreasTbl(id);
        final String name = adminAreaRequest.getName();
        final String iconId = adminAreaRequest.getIconId();
        final String status = adminAreaRequest.getStatus();
        final List<AdminAreaTranslation> translations = adminAreaRequest.getAdminAreaTranslations();
        if (!isUpdate && null != this.findAdminAreaIdForName(name)) {
            final Map<String, String[]> paramMap
                    = this.messageMaker.getAdminAreaNameWithi18nCode(translations, name);
            throw new CannotCreateObjectException(
                    "Administration area name already found", "ERR0002", paramMap);
        }
        adminAreasTbl.setName(name);
        adminAreasTbl.setCreateDate(date);
        adminAreasTbl.setUpdateDate(date);
        adminAreasTbl.setSingletonAppTimeout(adminAreaRequest.getSingleTonAppTimeOut());
        adminAreasTbl.setHotlineContactNumber(adminAreaRequest.getHotlineContactNo());
        adminAreasTbl.setHotlineContactEmail(adminAreaRequest.getHotlineContactEmail());
        adminAreasTbl.setIconId(new IconsTbl(iconId));
        adminAreasTbl.setStatus(status);

        final List<AdminAreaTranslationTbl> aatts = new ArrayList<>();

        if(translations != null) { 
        	for (final AdminAreaTranslation translation : translations) {
                String aatid = UUID.randomUUID().toString();

                if (isUpdate) {
                    aatid = translation.getId();
                }
                final String languageCode = translation.getLanguageCode();
                final String description = translation.getDescription();
                final String remarks = translation.getRemarks();

                final AdminAreaTranslationTbl aatt = new AdminAreaTranslationTbl(aatid);
                aatt.setCreateDate(date);
                aatt.setUpdateDate(date);
                aatt.setAdminAreaId(new AdminAreasTbl(id));
                aatt.setDescription(description);
                aatt.setLanguageCode(new LanguagesTbl(languageCode));
                aatt.setRemarks(remarks);

                aatts.add(aatt);
            }
        } else {
        	AdminAreaTranslationTbl enTranslation = new AdminAreaTranslationTbl(UUID.randomUUID().toString(), date, date, new LanguagesTbl("en"), adminAreasTbl);
        	AdminAreaTranslationTbl geTranslation = new AdminAreaTranslationTbl(UUID.randomUUID().toString(), date, date, new LanguagesTbl("de"), adminAreasTbl);
        	aatts.add(enTranslation);
        	aatts.add(geTranslation);
        }
        
        adminAreasTbl.setAdminAreaTranslationTblCollection(aatts);

        return adminAreasTbl;
    }

    /**
     * Find admin area id for name.
     *
     * @param name the name
     * @return the string
     */
    private String findAdminAreaIdForName(final String name) {
        final AdminAreasTbl adminAreasTbl = this.adminAreaJpaDao.findByNameIgnoreCase(name);
        String adminAreasId = null;
        if (adminAreasTbl != null) {
            adminAreasId = adminAreasTbl.getAdminAreaId();
        }
        LOG.info(">> findAdminAreaIdForName - Administration area id for name is {} - {}", name, adminAreasId);
        return adminAreasId;
    }

    /**
     *
     * @param status
     * @param id
     * @return boolean
     */
    @Override
    public final boolean updateStatusById(final String status,
            final String id) {
        boolean isUpdated = false;
        final int stat = this.adminAreaJpaDao.setStatusForAdminAreasTbl(status, id);
        if (stat > 0) {
            isUpdated = true;
        }
        return isUpdated;
    }

    /**
     *
     * @param id
     * @return boolean
     */
    @Override
    public final boolean deleteById(final String id) {
        LOG.info(">> deleteById");
        boolean isDeleted = false;
        try {
            this.adminAreaJpaDao.delete(id);
            isDeleted = true;
        } catch (Exception e) {
            if (e instanceof EmptyResultDataAccessException) {
                final String[] param = {id};
                //throw from here and catch this in multidelete catch block
                throw new XMObjectNotFoundException("Admin area not found", "P_ERR0012", param);
            }
        }

        LOG.info("<< deleteById");
        return isDeleted;
    }

    /**
     *
     * @param aaIds
     * @param httpServletRequest
     * @return AdminAreaResponse
     */
    @Override
    public AdminAreaResponse multiDelete(final Set<String> aaIds,
            final HttpServletRequest httpServletRequest) {
        LOG.info(">> multiDelete");
        final List<Map<String, String>> statusMaps = new ArrayList<>();
        aaIds.forEach(adminAreaId -> {
            AdminAreasTbl adminAreasTbl = null;
            try {
                adminAreasTbl = this.findById(adminAreaId);
                this.deleteById(adminAreaId);
                this.adminAreaAudiMgr.adminAreaDeleteStatusAuditor(adminAreaId,
                        httpServletRequest, true, adminAreasTbl);
            } catch (XMObjectNotFoundException objectNotFound) {
                Map<String, String> statusMap = messageMaker.extractFromException(objectNotFound);
                statusMaps.add(statusMap);
                this.adminAreaAudiMgr.adminAreaDeleteStatusAuditor(adminAreaId,
                        httpServletRequest, false, adminAreasTbl);
            }
        });
        AdminAreaResponse userResponse = new AdminAreaResponse(statusMaps);
        LOG.info(">> multiDelete");
        return userResponse;
    }

    /**
     *
     * @param id
     * @return AdminAreaResponse
     */
    @Override
    public AdminAreaResponse findAAByProjectId(final String id) {
        AdminAreaResponse adminAreaResponse = null;
        LOG.info(">> findAAByProjectId {}", id);
        final ProjectsTbl projectsTbl = this.projectMgr.findById(id);
        if (null != projectsTbl) {
            final Collection<AdminAreaProjectRelTbl> adminAreaProjectRelTblCollection = projectsTbl.getAdminAreaProjectRelTblCollection();
            if (adminAreaProjectRelTblCollection.isEmpty()) {
                throw new XMObjectNotFoundException("No Admin Area Project Relation found", "AA_ERR0001");
            }
            final Collection<AdminAreasTbl> adminAreasTbls = new ArrayList<>();
            for (AdminAreaProjectRelTbl adminAreaProjectRelTbl : adminAreaProjectRelTblCollection) {
                final SiteAdminAreaRelTbl siteAdminAreaRelTbl = adminAreaProjectRelTbl.getSiteAdminAreaRelId();
                final AdminAreasTbl adminAreasTbl = siteAdminAreaRelTbl.getAdminAreaId();
                adminAreasTbls.add(adminAreasTbl);
            }
            if (adminAreasTbls.isEmpty()) {
                throw new XMObjectNotFoundException("No Admin Area By Projects found", "AA_ERR0002");
            }
            adminAreaResponse = new AdminAreaResponse(adminAreasTbls);
        } else {
            final String[] param = {id};
            throw new XMObjectNotFoundException("No Projects found", "S_ERR0007", param);
        }
        LOG.info("<< findAAByProjectId");
        return adminAreaResponse;
    }

    /**
     *
     * @param id
     * @return AdminAreaResponse
     */
    @Override
    public final AdminAreaResponse findAAByUserAppId(String id) {
        AdminAreaResponse adminAreaResponse = null;
        LOG.info(">> findAAByUserAppId {}", id);
        final UserApplicationsTbl userApplicationsTbl = this.userApplicationMgr.findById(id);
        if (null != userApplicationsTbl) {
            final Collection<AdminAreaUserAppRelTbl> adminAreaUserAppRelTblCollection = userApplicationsTbl.getAdminAreaUserAppRelTblCollection();
            if (adminAreaUserAppRelTblCollection.isEmpty()) {
                throw new XMObjectNotFoundException("No Admin Area User Application relation found", "AA_ERR0003");
            }
            final Collection<AdminAreasTbl> adminAreasTbls = new ArrayList<>();
            for (AdminAreaUserAppRelTbl adminAreaUserAppRelTbl : adminAreaUserAppRelTblCollection) {
                final SiteAdminAreaRelTbl siteAdminAreaRelTbl = adminAreaUserAppRelTbl.getSiteAdminAreaRelId();
                final AdminAreasTbl adminAreasTbl = siteAdminAreaRelTbl.getAdminAreaId();
                adminAreasTbls.add(adminAreasTbl);
            }
            if (adminAreasTbls.isEmpty()) {
                throw new XMObjectNotFoundException("Admin Area By User Application not found", "AA_ERR0004");
            }
            adminAreaResponse = new AdminAreaResponse(adminAreasTbls);
        } else {
            throw new XMObjectNotFoundException("No User Application found", "AA_ERR0005");
        }
        LOG.info("<< findAAByUserAppId");
        return adminAreaResponse;
    }

    /**
     *
     * @param id
     * @return AdminAreaResponse
     */
    @Override
    public final AdminAreaResponse findAAByUserProjectAppId(final String id) {
        AdminAreaResponse adminAreaResponse = null;
        LOG.info(">> findAAByUserProjectAppId {}", id);
        final ProjectApplicationsTbl projectApplicationsTbl = this.projectApplicationMgr.findById(id);
        if (null != projectApplicationsTbl) {
            final Collection<AdminAreaProjAppRelTbl> adminAreaProjAppRelTblCollection = projectApplicationsTbl.getAdminAreaProjAppRelTblCollection();
            if (adminAreaProjAppRelTblCollection.isEmpty()) {
                throw new XMObjectNotFoundException("No Admin Area Project Application relation found", "AA_ERR0006");
            }
            final Collection<AdminAreasTbl> adminAreasTbls = new ArrayList<>();
            for (AdminAreaProjAppRelTbl adminAreaProjAppRelTbl : adminAreaProjAppRelTblCollection) {
                final AdminAreaProjectRelTbl adminAreaProjectRelTbl = adminAreaProjAppRelTbl.getAdminAreaProjectRelId();
                final SiteAdminAreaRelTbl siteAdminAreaRelTbl = adminAreaProjectRelTbl.getSiteAdminAreaRelId();
                final AdminAreasTbl adminAreasTbl = siteAdminAreaRelTbl.getAdminAreaId();
                adminAreasTbls.add(adminAreasTbl);
            }
            if (adminAreasTbls.isEmpty()) {
                throw new XMObjectNotFoundException("Admin Area User Project App not found", "AA_ERR0007");
            }
            adminAreaResponse = new AdminAreaResponse(adminAreasTbls);
        } else {
            throw new XMObjectNotFoundException("No Project Application found", "AA_ERR0008");
        }
        LOG.info("<< findAAByUserProjectAppId");
        return adminAreaResponse;
    }

    /**
     *
     * @param id
     * @return AdminAreaResponse
     */
    @Override
    public final AdminAreaResponse findAAByStartAppId(String id) {
        AdminAreaResponse adminAreaResponse = null;
        LOG.info(">> findAAByStartAppId {}", id);
        final StartApplicationsTbl startApplicationsTbl = this.startApplicationMgr.findById(id);
        if (null != startApplicationsTbl) {
            final Collection<AdminAreaStartAppRelTbl> adminAreaStartAppRelTblCollection = startApplicationsTbl.getAdminAreaStartAppRelTblCollection();
            if (adminAreaStartAppRelTblCollection.isEmpty()) {
                throw new XMObjectNotFoundException("No Admin Area Start Application relation found", "AA_ERR0009");
            }
            final Collection<AdminAreasTbl> adminAreasTbls = new ArrayList<>();
            for (AdminAreaStartAppRelTbl adminAreaStartAppRelTbl : adminAreaStartAppRelTblCollection) {
                final SiteAdminAreaRelTbl siteAdminAreaRelTbl = adminAreaStartAppRelTbl.getSiteAdminAreaRelId();
                final AdminAreasTbl adminAreasTbl = siteAdminAreaRelTbl.getAdminAreaId();
                adminAreasTbls.add(adminAreasTbl);
            }
            if (adminAreasTbls.isEmpty()) {
                throw new XMObjectNotFoundException("Admin Area By Start Application not found", "AA_ERR0010");
            }
            adminAreaResponse = new AdminAreaResponse(adminAreasTbls);
        } else {
            throw new XMObjectNotFoundException("No Start Application found", "AA_ERR0011");
        }
        LOG.info("<< findAAByStartAppId");
        return adminAreaResponse;
    }

    /**
     * @param name
     * @return AdminAreasTbl
     */
    @Override
    public final AdminAreasTbl findByName(final String name) {
        LOG.info(">> findByName {}", name);
        final AdminAreasTbl adminAreasTbl = this.adminAreaJpaDao.findByNameIgnoreCase(name);
        if (null == adminAreasTbl) {
            final String[] param = {name};
            throw new XMObjectNotFoundException("Admin Area with name not found", "AA_ERR0012", param);
        }
        LOG.info("<< findByName");
        return adminAreasTbl;
    }

    /**
     * Find AA by project id and site id.
     *
     * @param projectId the project id
     * @param siteId the site id
     * @param validationRequest
     * @return the admin area response
     */
    @Override
    public AdminAreaResponse findAAByProjectIdAndSiteId(final String projectId, final String tkt,
            final String siteId, final ValidationRequest validationRequest) {
        AdminAreaResponse adminAreaResponse = null;
        LOG.info(">> findAAByProjectIdAndSiteId {} {}", projectId, siteId);
        boolean isViewInactive = validator.isViewInactiveAllowed(validationRequest);

        if (!isViewInactive) {
            final UserTkt userTktTbl = this.userTktJpaDao.findByTktAndApplicationName(this.authController.hash(tkt),
                    Application.CAX_ADMIN_MENU.name());
            if (userTktTbl != null) {
                validationRequest.setUserName(userTktTbl.getUsername());
                isViewInactive = validator.isViewInactiveAllowed(validationRequest);
            }
        }

        LOG.info("findAAByProjectIdAndSiteId isViewInactive={}", isViewInactive);
        final ProjectsTbl projectsTbl = validator.validateProject(projectId, isViewInactive);
        validator.validateSite(siteId, isViewInactive);
        Collection<AdminAreaProjectRelTbl> adminAreaProjectRelTblCollection = projectsTbl.getAdminAreaProjectRelTblCollection();
        if (adminAreaProjectRelTblCollection.isEmpty()) {
            throw new XMObjectNotFoundException("No Admin Area Project Relation found", "AA_ERR0001");
        }
        adminAreaProjectRelTblCollection = validator.filterAdminAreaProjectRel(isViewInactive, adminAreaProjectRelTblCollection);
        final Collection<AdminAreasTbl> adminAreasTbls = new ArrayList<>();
        for (final AdminAreaProjectRelTbl adminAreaProjectRelTbl : adminAreaProjectRelTblCollection) {
            SiteAdminAreaRelTbl siteAdminAreaRelTbl = adminAreaProjectRelTbl.getSiteAdminAreaRelId();
            siteAdminAreaRelTbl = validator.filterSiteAdminAreaRel(isViewInactive, siteAdminAreaRelTbl);
            if (siteAdminAreaRelTbl != null) {
            	final SitesTbl siteTbl = siteAdminAreaRelTbl.getSiteId();
                if (siteTbl.getSiteId().equals(siteId)) {
                    AdminAreasTbl adminAreasTbl = siteAdminAreaRelTbl.getAdminAreaId();
                    adminAreasTbl = validator.filterAdminAreaResponse(isViewInactive, adminAreasTbl);
                    if (null != adminAreasTbl && null != adminAreasTbl.getAdminAreaId()) {
                        adminAreasTbls.add(adminAreasTbl);
                    }
                }
            }
        }
        if (adminAreasTbls.isEmpty()) {
            throw new XMObjectNotFoundException("No Admin Area By Projects found", "AA_ERR0002");
        }
        adminAreaResponse = new AdminAreaResponse(adminAreasTbls);
        LOG.info("<< findAAByProjectIdAndSiteId");
        return adminAreaResponse;
    }

    /**
     * Find AA by start app id and site id.
     *
     * @param startAppId the start app id
     * @param siteId the site id
     * @return the admin area response
     */
    @Override
    public AdminAreaResponse findAAByStartAppIdAndSiteId(final String startAppId, final String siteId) {
        AdminAreaResponse adminAreaResponse = null;
        LOG.info(">> findAAByStartAppIdAndSiteId {}", startAppId, siteId);
        final StartApplicationsTbl startAppTbl = this.startAppMgr.findById(startAppId);
        if (null != startAppTbl) {
            final Collection<AdminAreaStartAppRelTbl> adminAreaStartAppRelTblCollection = startAppTbl.getAdminAreaStartAppRelTblCollection();
            if (adminAreaStartAppRelTblCollection.isEmpty()) {
                throw new XMObjectNotFoundException("No Admin Area Start Application Relation found", "AA_ERR0009");
            }
            final Collection<AdminAreasTbl> adminAreasTbls = new ArrayList<>();
            for (AdminAreaStartAppRelTbl adminAreaStartAppRelTbl : adminAreaStartAppRelTblCollection) {
                final SiteAdminAreaRelTbl siteAdminAreaRelTbl = adminAreaStartAppRelTbl.getSiteAdminAreaRelId();
                SitesTbl siteTbl = siteAdminAreaRelTbl.getSiteId();
                if (siteTbl.getSiteId().equals(siteId)) {
                    final AdminAreasTbl adminAreasTbl = siteAdminAreaRelTbl.getAdminAreaId();
                    adminAreasTbls.add(adminAreasTbl);
                }
            }
            if (adminAreasTbls.isEmpty()) {
                throw new XMObjectNotFoundException("No Admin Area By Start Application found", "AA_ERR0013");
            }
            adminAreaResponse = new AdminAreaResponse(adminAreasTbls);
        } else {
            throw new XMObjectNotFoundException("No Start Application found", "AA_ERR0011");
        }
        LOG.info("<< findAAByStartAppIdAndSiteId");
        return adminAreaResponse;
    }

    /**
     * Find AA by user app id and site id.
     *
     * @param userAppId the start app id
     * @param siteId the site id
     * @return the admin area response
     */
    @Override
    public AdminAreaResponse findAAByUserAppIdAndSiteId(final String userAppId, final String siteId) {
        AdminAreaResponse adminAreaResponse = null;
        LOG.info(">> findAAByUserAppIdAndSiteId {}", userAppId, siteId);
        final UserApplicationsTbl userApplicationsTbl = this.userApplicationMgr.findById(userAppId);
        if (null != userApplicationsTbl) {
            final Collection<AdminAreaUserAppRelTbl> adminAreaUserAppRelTblCollection = userApplicationsTbl.getAdminAreaUserAppRelTblCollection();
            if (adminAreaUserAppRelTblCollection.isEmpty()) {
                throw new XMObjectNotFoundException("No Start Application found", "AA_ERR0003");
            }
            final Collection<AdminAreasTbl> adminAreasTbls = new ArrayList<>();
            for (AdminAreaUserAppRelTbl adminAreaUserAppRelTbl : adminAreaUserAppRelTblCollection) {
                final SiteAdminAreaRelTbl siteAdminAreaRelTbl = adminAreaUserAppRelTbl.getSiteAdminAreaRelId();
                SitesTbl siteTbl = siteAdminAreaRelTbl.getSiteId();
                if (siteTbl.getSiteId().equals(siteId)) {
                    final AdminAreasTbl adminAreasTbl = siteAdminAreaRelTbl.getAdminAreaId();
                    adminAreasTbls.add(adminAreasTbl);
                }
            }
            if (adminAreasTbls.isEmpty()) {
                throw new XMObjectNotFoundException("No Admin Area By User Application found", "AA_ERR0014");
            }
            adminAreaResponse = new AdminAreaResponse(adminAreasTbls);
        } else {
            throw new XMObjectNotFoundException("No User Application found", "AA_ERR0005");
        }
        LOG.info("<< findAAByUserAppIdAndSiteId");
        return adminAreaResponse;
    }

    /**
     * Find AA by project app id and site id.
     *
     * @param projectAppId the start app id
     * @param siteId the site id
     * @return the admin area response
     */
    @Override
    public AdminAreaResponse findAAByProjectAppIdAndSiteId(final String projectAppId,
            final String siteId) {
        AdminAreaResponse adminAreaResponse = null;
        LOG.info(">> findAAByProjectAppIdAndSiteId {}", projectAppId, siteId);
        final ProjectApplicationsTbl projectAppTbl = this.projectApplicationMgr.findById(projectAppId);
        if (null != projectAppTbl) {
            final Collection<AdminAreaProjAppRelTbl> adminAreaProjectAppRelTblCollection = projectAppTbl.getAdminAreaProjAppRelTblCollection();
            if (adminAreaProjectAppRelTblCollection.isEmpty()) {
                throw new XMObjectNotFoundException("No Admin Area Project Application Relation found", "AA_ERR0006");
            }
            final Collection<AdminAreasTbl> adminAreasTbls = new ArrayList<>();
            for (AdminAreaProjAppRelTbl adminAreaProjectAppRelTbl : adminAreaProjectAppRelTblCollection) {
                final AdminAreaProjectRelTbl adminAreaProjectRelTbl = adminAreaProjectAppRelTbl.getAdminAreaProjectRelId();
                final SiteAdminAreaRelTbl siteAdminAreaRelTbl = adminAreaProjectRelTbl.getSiteAdminAreaRelId();
                final SitesTbl siteTbl = siteAdminAreaRelTbl.getSiteId();
                if (siteTbl.getSiteId().equals(siteId)) {
                    final AdminAreasTbl adminAreasTbl = siteAdminAreaRelTbl.getAdminAreaId();
                    adminAreasTbls.add(adminAreasTbl);
                }
            }
            if (adminAreasTbls.isEmpty()) {
                throw new XMObjectNotFoundException("No Admin Area By Project Relation found", "AA_ERR0015");
            }
            adminAreaResponse = new AdminAreaResponse(adminAreasTbls);
        } else {
            throw new XMObjectNotFoundException("No Project Application found", "AA_ERR0008");
        }
        LOG.info("<< findAAByProjectAppIdAndSiteId");
        return adminAreaResponse;
    }

	@Override
	public AdminAreaResponse findAndminAreasBySiteId(String siteId, ValidationRequest validationRequest) {
		LOG.info(">> findAndminAreasBySiteId");
		SitesTbl sitesTbl = this.siteJpaDao.findOne(siteId);
		if(sitesTbl == null) {
			throw new XMObjectNotFoundException("Site not found", "ERR0022");
		}
		boolean isViewInactive = validator.isViewInactiveAllowed(validationRequest);
		SitesTbl validatedSite = this.validator.validateSite(siteId, isViewInactive);
		List<AdminAreasTbl> adminAreas = new ArrayList<>();
		if (null != validatedSite) {
			adminAreas = this.getAdminAreas(validatedSite, isViewInactive);
		}
		/*
		Collection<SiteAdminAreaRelTbl> siteAdminAreaRelTblCollection = sitesTbl.getSiteAdminAreaRelTblCollection();
		siteAdminAreaRelTblCollection.forEach(siteAdminArea -> {
			AdminAreasTbl adminAreaTbl = siteAdminArea.getAdminAreaId();
			if(sitesTbl.getStatus().equals(Status.INACTIVE.name())) {
				adminAreaTbl.setStatus(Status.INACTIVE.name());
			}
			siteAdminAreas.add(adminAreaTbl);
		});*/
		LOG.info("<< findAndminAreasBySiteId");
		AdminAreaResponse adminAreaResponse = new AdminAreaResponse(adminAreas);
		return adminAreaResponse;
	}

	private List<AdminAreasTbl> getAdminAreas(SitesTbl sitesTbl, boolean isViewInactive) {
		Collection<SiteAdminAreaRelTbl> siteAdminAreaRelTbls = sitesTbl.getSiteAdminAreaRelTblCollection();
		if (siteAdminAreaRelTbls.isEmpty()) {
			throw new XMObjectNotFoundException("Admin Areas not available for site", "S_ERR0003");
		}
		siteAdminAreaRelTbls = validator.filterSiteAdminAreaRel(isViewInactive, siteAdminAreaRelTbls);
		final List<AdminAreasTbl> adminAreasTbls = new ArrayList<>();
		for (SiteAdminAreaRelTbl siteAdminAreaRelTbl : siteAdminAreaRelTbls) {
			String siteAAStatus = siteAdminAreaRelTbl.getStatus();
			String siteStatus = siteAdminAreaRelTbl.getSiteId().getStatus();
			AdminAreasTbl filterAdminAreaTbl = validator.filterAdminAreaResponse(isViewInactive,
					siteAdminAreaRelTbl.getAdminAreaId());
			if (null != filterAdminAreaTbl) {
				if(siteAAStatus.equals(Status.INACTIVE.name()) || siteStatus.equals(Status.INACTIVE.name())) {
					filterAdminAreaTbl.setStatus(Status.INACTIVE.name());
				}
			}
			adminAreasTbls.add(filterAdminAreaTbl);
		}
		return adminAreasTbls;
	}

}
