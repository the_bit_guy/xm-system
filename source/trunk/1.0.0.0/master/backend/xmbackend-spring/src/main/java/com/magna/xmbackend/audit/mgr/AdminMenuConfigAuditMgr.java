/**
 * 
 */
package com.magna.xmbackend.audit.mgr;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.magna.xmbackend.entities.AdminMenuConfigTbl;
import com.magna.xmbackend.vo.adminMenu.AdminMenuConfigRequest;
import com.magna.xmbackend.vo.adminMenu.AdminMenuConfigResponse;

/**
 * @author Bhabadyuti Bal
 *
 */
public interface AdminMenuConfigAuditMgr {
	
	void amcCreateSuccessAudit(HttpServletRequest httpServletRequest, List<AdminMenuConfigRequest> adminMenuConfigRequestList, AdminMenuConfigResponse amcr);
	
    void amcUpdateFailureAudit(HttpServletRequest hsr, AdminMenuConfigRequest amcr,
            String errMsg);
    
    void amcMultiDeleteAuditor(AdminMenuConfigTbl adminMenuConfigTbl, 
            HttpServletRequest httpServletRequest);

}
