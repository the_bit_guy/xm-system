/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magna.xmbackend.controller;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.magna.xmbackend.mgr.UserAccountStatusMgr;
import com.magna.xmbackend.vo.userAccountStatus.UserAccountStatusResponse;

/**
 *
 * @author dhana
 */
@RestController
@RequestMapping(value = "/userAccountStatus")
public class UserAccountStatusController {

	private static final Logger LOG = LoggerFactory.getLogger(UserAccountStatusController.class);

	@Autowired
	private UserAccountStatusMgr userAccountStatusMgr;

	@RequestMapping(value = "/findAll", method = RequestMethod.GET, consumes = { MediaType.APPLICATION_JSON_VALUE,
			MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_FORM_URLENCODED_VALUE }, produces = {
					MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE,
					MediaType.APPLICATION_FORM_URLENCODED_VALUE })
	public @ResponseBody final ResponseEntity<UserAccountStatusResponse> findAll(
			HttpServletRequest httpServletRequest) {
		LOG.info("> findAll");
		UserAccountStatusResponse uasr = this.userAccountStatusMgr.findAll();
		LOG.info("< findAll");
		return new ResponseEntity<>(uasr, HttpStatus.OK);
	}
}
