/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magna.xmbackend.jpa.dao;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.magna.xmbackend.entities.AdminAreasTbl;
import com.magna.xmbackend.entities.ProjectApplicationsTbl;
import com.magna.xmbackend.entities.ProjectsTbl;
import com.magna.xmbackend.entities.SitesTbl;

@Transactional
public interface ProjectApplicationJpaDao extends CrudRepository<ProjectApplicationsTbl, String> {

    @Modifying
    @Query("update ProjectApplicationsTbl pat set pat.status = :status where pat.projectApplicationId = :id")
    int setStatusForProjectApplicationsTbl(@Param("status") String status, @Param("id") String id);

    Iterable<ProjectApplicationsTbl> findByPosition(String position);

    Iterable<ProjectApplicationsTbl> findByStatus(String status);
    
    @Query("select pat from ProjectApplicationsTbl pat where pat.position=:iconPat or pat.position=:buttonPat or pat.position=:menuPat")
    Iterable<ProjectApplicationsTbl> findByPositionPattern(@Param("iconPat")String iconPat, @Param("buttonPat")String buttonPat, @Param("menuPat")String menuPat);

    ProjectApplicationsTbl findByProjectApplicationId(String projectApplicationId);
    
    
    
    /* Old Quey for findPATblBySiteAAProjId method
     * @Query("from AdminAreaProjectRelTbl aaprt where aaprt.siteAdminAreaRelId in (select SAART.siteAdminAreaRelId from SiteAdminAreaRelTbl SAART "
    		+ "where SAART.siteId=:siteId and SAART.adminAreaId=:adminAreaId) and aaprt.projectId=:projectId")*/
    
    @Query("from AdminAreaProjAppRelTbl aapart where aapart.adminAreaProjectRelId = ( "
    		+ "SELECT aaprt.adminAreaProjectRelId FROM AdminAreaProjectRelTbl aaprt where "
    		+ "aaprt.projectId=:projectId and aaprt.siteAdminAreaRelId = "
    		+ "(select SAART.siteAdminAreaRelId from SiteAdminAreaRelTbl SAART where SAART.siteId=:siteId "
    		+ "and SAART.adminAreaId=:adminAreaId))")
    Iterable<ProjectApplicationsTbl> findPATblBySiteAAProjId(@Param("siteId") SitesTbl siteId, @Param("adminAreaId") AdminAreasTbl adminAreaId, @Param("projectId") ProjectsTbl projectId);

	@Query(value = "SELECT * from PROJECT_APPLICATIONS_TBL where PROJECT_APPLICATION_ID IN "
			+ "(SELECT PROJECT_APPLICATION_ID from USER_PROJ_APP_REL_TBL where USER_PROJECT_REL_ID = "
			+ "(select USER_PROJECT_REL_ID from USER_PROJECT_REL_TBL where USER_ID=?1 and PROJECT_ID=?2))", 
			nativeQuery = true)
	Iterable<ProjectApplicationsTbl> findPATblByUserProjectId(String userId, String projectId);
	
	@Query(value="SELECT * FROM PROJECT_APPLICATIONS_TBL WHERE PROJECT_APPLICATION_ID IN "
			+ "(SELECT PROJECT_APPLICATION_ID from ADMIN_AREA_PROJ_APP_REL_TBL where ADMIN_AREA_PROJECT_REL_ID IN "
			+ "(SELECT ADMIN_AREA_PROJECT_REL_ID FROM ADMIN_AREA_PROJECT_REL_TBL aaprt WHERE "
			+ "aaprt.SITE_ADMIN_AREA_REL_ID IN (SELECT SAART.SITE_ADMIN_AREA_REL_ID FROM SITE_ADMIN_AREA_REL_TBL SAART "
			+ "WHERE SAART.SITE_ID=:siteId AND SAART.ADMIN_AREA_ID=:adminAreaId) AND aaprt.PROJECT_ID=:projectId) and "
			+ "REL_TYPE=:relType)", nativeQuery=true)
	Iterable<ProjectApplicationsTbl> findPATblBySiteAAProjIdAndRelType(@Param("siteId") String siteId,
			@Param("adminAreaId") String adminAreaId, @Param("projectId") String projectId,
			@Param("relType") String relType);

	/**
	 * Find by name containing ignore case.
	 *
	 * @param name the name
	 * @return the project applications tbl
	 */
	ProjectApplicationsTbl findByNameIgnoreCase(String name);
}
