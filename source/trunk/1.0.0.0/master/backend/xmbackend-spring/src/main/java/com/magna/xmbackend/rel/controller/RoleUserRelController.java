package com.magna.xmbackend.rel.controller;

import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.magna.xmbackend.audit.rel.mgr.RoleUserAuditMgr;
import com.magna.xmbackend.rel.mgr.RoleUserRelMgr;
import com.magna.xmbackend.utils.Validator;
import com.magna.xmbackend.vo.permission.ValidationRequest;
import com.magna.xmbackend.vo.rel.RoleUserRelRequest;
import com.magna.xmbackend.vo.rel.RoleUserRelResponse;

/**
 *
 * @author vijay
 */
@RestController
@RequestMapping(value = "/roleUserRelation")
public class RoleUserRelController {

    /** The Constant LOG. */
    private static final Logger LOG = LoggerFactory.getLogger(RoleUserRelController.class);

    /** The role user rel mgr. */
    @Autowired
    private RoleUserRelMgr roleUserRelMgr;
    
    /** The validator. */
	@Autowired
    private Validator validator;
	@Autowired
	private RoleUserAuditMgr roleUserAuditMgr;

    /**
     *
     * @param httpServletRequest
     * @param roleUserRelRequestList
     * @return RoleUserRelResponse
     */
    @RequestMapping(value = "/save",
            method = RequestMethod.POST,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<RoleUserRelResponse> save(
            HttpServletRequest httpServletRequest,
            @RequestBody final List<RoleUserRelRequest> roleUserRelRequestList) {
        LOG.info("> save");
        final String userName = (String) httpServletRequest.getAttribute("userName");
        LOG.info("userName={}", userName);
        final RoleUserRelResponse roleUserRelResponse
                = this.roleUserRelMgr.create(roleUserRelRequestList, userName);
        LOG.info("< save");
        return new ResponseEntity<>(roleUserRelResponse, HttpStatus.ACCEPTED);
    }

    /**
     *
     * @param httpServletRequest
     * @param roleUserRelRequestList
     * @return RoleUserRelResponse
     */
    @RequestMapping(value = "/saveAll",
            method = RequestMethod.POST,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<RoleUserRelResponse> saveAll(
            HttpServletRequest httpServletRequest,
            @RequestBody final List<RoleUserRelRequest> roleUserRelRequestList) {
        LOG.info("> saveAll");
        final String userName = (String) httpServletRequest.getAttribute("userName");
        LOG.info("userName={}", userName);
        RoleUserRelResponse roleUserRelResponse = null;
		try {
			roleUserRelResponse = this.roleUserRelMgr.create(roleUserRelRequestList, userName);
			this.roleUserAuditMgr.roleUserMultiSaveAuditor(roleUserRelRequestList, roleUserRelResponse, httpServletRequest);
		} catch (Exception ex) {
			this.roleUserAuditMgr.roleUserMultiSaveFailureAuditor(httpServletRequest, ex);
		}
        LOG.info("< saveAll");
        return new ResponseEntity<>(roleUserRelResponse, HttpStatus.ACCEPTED);
    }

    /**
     *
     * @param httpServletRequest
     * @param roleuserRelId
     * @return boolean
     */
    @RequestMapping(value = "/delete/{roleuserRelId}",
            method = RequestMethod.DELETE,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<Boolean> delete(
            HttpServletRequest httpServletRequest,
            @PathVariable String roleuserRelId) {
        LOG.info("> delete");
        final boolean isDeleted = this.roleUserRelMgr.deleteById(roleuserRelId);
        LOG.info("< delete");
        return new ResponseEntity<>(isDeleted, HttpStatus.ACCEPTED);
    }
    
    
    
    
    @RequestMapping(value = "/multiDelete",
            method = RequestMethod.DELETE,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<RoleUserRelResponse> multiDelete(
            HttpServletRequest httpServletRequest,
            @RequestBody Set<String> ids) {
        LOG.info("> multiDelete");
        final RoleUserRelResponse roleUserRelResponse = this.roleUserRelMgr.multiDelete(ids, httpServletRequest);
        LOG.info("< multiDelete");
        return new ResponseEntity<>(roleUserRelResponse, HttpStatus.ACCEPTED);
    }

    /**
     *
     * @param httpServletRequest
     * @param roleId
     * @return RoleUserRelResponse
     */
    @RequestMapping(value = "/findRoleUserRelTblByRoleId/{roleId}",
            method = RequestMethod.GET,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<RoleUserRelResponse> findRoleUserRelTblByRoleId(
            HttpServletRequest httpServletRequest,
            @PathVariable String roleId) {
        LOG.info("> findRoleUserRelTblByRoleId");
        final ValidationRequest validationRequest = validator.formObjectValidationRequest(httpServletRequest, "AREA");
        final RoleUserRelResponse roleUserRelResponse = this.roleUserRelMgr.findRoleUserRelTblByRoleId(roleId, validationRequest);
        LOG.info("< findRoleUserRelTblByRoleId");
        return new ResponseEntity<>(roleUserRelResponse, HttpStatus.ACCEPTED);
    }

    /**
     *
     * @param httpServletRequest
     * @param roleId
     * @param roleAdminAreaRelId
     * @return RoleUserRelResponse
     */
    @RequestMapping(value = "/findRoleUserRelTblByRoleIdRoleAARelId/{roleId}/{roleAdminAreaRelId}",
            method = RequestMethod.GET,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<RoleUserRelResponse> findRoleUserRelTblByRoleIdRoleAARelId(
            HttpServletRequest httpServletRequest,
            @PathVariable String roleId, @PathVariable String roleAdminAreaRelId) {
        LOG.info("> findRoleUserRelTblByRoleIdRoleAARelId");
        final ValidationRequest validationRequest = validator.formObjectValidationRequest(httpServletRequest, "AREA");
        final RoleUserRelResponse roleUserRelResponse = this.roleUserRelMgr.findRoleUserRelTblByRoleIdRoleAARelId(roleId, roleAdminAreaRelId, validationRequest);
        LOG.info("< findRoleUserRelTblByRoleIdRoleAARelId");
        return new ResponseEntity<>(roleUserRelResponse, HttpStatus.ACCEPTED);
    }
}
