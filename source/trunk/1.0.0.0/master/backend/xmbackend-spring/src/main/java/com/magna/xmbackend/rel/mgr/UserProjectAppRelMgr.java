package com.magna.xmbackend.rel.mgr;

import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import com.magna.xmbackend.entities.UserProjAppRelTbl;
import com.magna.xmbackend.vo.permission.ValidationRequest;
import com.magna.xmbackend.vo.rel.UserProjectAppRelBatchRequest;
import com.magna.xmbackend.vo.rel.UserProjectAppRelBatchResponse;
import com.magna.xmbackend.vo.rel.UserProjectAppRelRequest;
import com.magna.xmbackend.vo.rel.UserProjectAppRelResponse;

/**
 *
 * @author vijay
 */
public interface UserProjectAppRelMgr {

    /**
     *
     * @return UserProjectAppRelResponse
     */
    UserProjectAppRelResponse findAll(final ValidationRequest validationRequest);

    /**
     *
     * @param id
     * @return UserProjAppRelTbl
     */
    UserProjAppRelTbl findById(String id);

    /**
     *
     * @param userProjectAppRelRequest
     * @param userName
     * @return UserProjAppRelTbl
     */
    UserProjAppRelTbl create(final UserProjectAppRelRequest userProjectAppRelRequest,
            final String userName);

    /**
     *
     * @param userProjectAppRelBatchRequest
     * @param userName
     * @return UserProjectAppRelBatchResponse
     */
    UserProjectAppRelBatchResponse createBatch(final UserProjectAppRelBatchRequest userProjectAppRelBatchRequest,
            final String userName);

    /**
     *
     * @param id
     * @return boolean
     */
    boolean delete(final String id);

    UserProjectAppRelResponse multiDelete(final Set<String> ids, HttpServletRequest httpServletRequest);

    /**
     *
     * @param userProjectRelId
     * @return UserProjectAppRelResponse
     */
    UserProjectAppRelResponse findUserProjectAppRelsByUserProjectRelId(final String userProjectRelId);

    /**
     *
     * @param id
     * @param aaProjRelId
     * @param validationRequest
     * @return UserProjectAppRelResponse
     */
    UserProjectAppRelResponse findUserProjectAppRelsByUserProjRelIdAndAAPRelId(final String id,
            final String aaProjRelId, final ValidationRequest validationRequest);

    /**
     *
     * @param userProjectRelId
     * @param userRelType
     * @return UserProjectAppRelResponse
     */
    UserProjectAppRelResponse findUserProjectAppRelsByUserProjectRelIdAndUserRelType(final String userProjectRelId,
            final String userRelType);

    /**
     *
     * @param userProjectRelId
     * @param adminAreaProjRelId
     * @param userRelType
     * @return UserProjectAppRelResponse
     */
    UserProjectAppRelResponse findUserProjectAppRelsByUserProjectRelIdAndAAPRelIdAndUserRelType(final String userProjectRelId,
            final String adminAreaProjRelId, final String userRelType, final ValidationRequest validationRequest);

    /**
     *
     * @param projectAppId
     * @return UserProjectAppRelResponse
     */
    UserProjectAppRelResponse findUserProjectAppRelsByProjectAppId(final String projectAppId);

    /**
     *
     * @param userId
     * @param projId
     * @param aaId
     * @return UserProjectAppRelResponse
     */
    List<UserProjAppRelTbl> findByUserIdProjectIdAAId(final String userId,
            final String projId, final String aaId,final ValidationRequest validationRequest);

    /**
     *
     * @param userProjectAppRelBatchRequest
     * @return UserProjectAppRelBatchResponse
     */
    UserProjectAppRelBatchResponse updateUserRelsTypeByIds(final UserProjectAppRelBatchRequest userProjectAppRelBatchRequest,
    						HttpServletRequest httpServletRequest);

}
