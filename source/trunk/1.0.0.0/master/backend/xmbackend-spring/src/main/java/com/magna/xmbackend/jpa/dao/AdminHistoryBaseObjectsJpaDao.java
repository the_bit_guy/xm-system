/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magna.xmbackend.jpa.dao;

import com.magna.xmbackend.entities.AdminHistoryBaseObjectsTbl;
import javax.transaction.Transactional;
import org.springframework.data.repository.CrudRepository;

/**
 *
 * @author dhana
 */
@Transactional
public interface AdminHistoryBaseObjectsJpaDao extends CrudRepository<AdminHistoryBaseObjectsTbl, String> {

}
