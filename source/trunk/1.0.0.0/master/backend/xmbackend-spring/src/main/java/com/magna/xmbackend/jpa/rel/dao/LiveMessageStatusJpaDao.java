/**
 * 
 */
package com.magna.xmbackend.jpa.rel.dao;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.magna.xmbackend.entities.LiveMessageStatusTbl;
import com.magna.xmbackend.entities.LiveMessageTbl;
import com.magna.xmbackend.entities.UsersTbl;

/**
 * @author Bhabadyuti Bal
 *
 */
@Transactional
public interface LiveMessageStatusJpaDao extends CrudRepository<LiveMessageStatusTbl, String>{

	List<LiveMessageStatusTbl> findByLiveMessageId(LiveMessageTbl liveMessageId);
	
	LiveMessageStatusTbl findByLiveMessageIdAndUserId(LiveMessageTbl liveMessageId, UsersTbl userId);
	
	@Modifying
	@Query("update LiveMessageStatusTbl lmst set lmst.status=:status where lmst.liveMessageStatusId=:liveMessageStatusId")
	int updateLiveMsgStatus(@Param("liveMessageStatusId") String liveMessageStatusId, @Param("status") String status);
}
