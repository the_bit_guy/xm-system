package com.magna.xmbackend.jpa.rel.dao;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.magna.xmbackend.entities.AdminAreaProjectRelTbl;
import com.magna.xmbackend.entities.AdminAreasTbl;
import com.magna.xmbackend.entities.ProjectApplicationsTbl;
import com.magna.xmbackend.entities.ProjectsTbl;
import com.magna.xmbackend.entities.UserProjAppRelTbl;
import com.magna.xmbackend.entities.UserProjectRelTbl;

/**
 *
 * @author vijay
 */
@Transactional
public interface UserProjectAppRelJpaDao extends CrudRepository<UserProjAppRelTbl, String> {

	List<UserProjAppRelTbl> findByUserProjectRelId(UserProjectRelTbl userProjectRelId);

	List<UserProjAppRelTbl> findByUserProjectRelIdAndAdminAreaProjectRelId(UserProjectRelTbl userProjectRelId,
			AdminAreaProjectRelTbl adminAreaProjectRelId);

	List<UserProjAppRelTbl> findByUserProjectRelIdAndUserRelType(UserProjectRelTbl userProjectRelId,
			String userRelType);

	List<UserProjAppRelTbl> findByUserProjectRelIdAndAdminAreaProjectRelIdAndUserRelType(UserProjectRelTbl userProjectRelId, AdminAreaProjectRelTbl adminAreaProjectRelId, String userRelType);

	
	UserProjAppRelTbl findByUserProjectRelIdAndAdminAreaProjectRelIdAndProjectApplicationIdAndUserRelType(UserProjectRelTbl userProjectRelId, AdminAreaProjectRelTbl adminAreaProjectRelId, ProjectApplicationsTbl projectApplicationId, String userRelType);
	
	
	List<UserProjAppRelTbl> findByProjectApplicationId(ProjectApplicationsTbl projectApplicationId);

	/**
	 * 
	 * @param userProjectRelTbls
	 * @return List<UserProjAppRelTbl>
	 */
	List<UserProjAppRelTbl> findByUserProjectRelIdIn(List<UserProjectRelTbl> userProjectRelTbls);

	/**
	 * 
	 * @param userProjectRelTbls
	 * @param adminAreaProjectRelTbls
	 * @return List<UserProjAppRelTbl>
	 */
	List<UserProjAppRelTbl> findByUserProjectRelIdInAndAdminAreaProjectRelIdIn(
			List<UserProjectRelTbl> userProjectRelTbls, List<AdminAreaProjectRelTbl> adminAreaProjectRelTbls);
	
	/**
	 * 
	 * @param userProjAppRelId
	 * @param userRelType
	 * @return
	 */
	@Modifying
	@Query("update UserProjAppRelTbl upart set upart.userRelType=:userRelType where upart.userProjAppRelId=:userProjAppRelId")
	int updateUserRelsTypeByIds(@Param("userProjAppRelId") String userProjAppRelId, @Param("userRelType") String userRelType);
	
	/***
	 * 
	 * @param id
	 * @return UserProjAppRelTbl
	 */
	UserProjAppRelTbl findByUserProjAppRelId(String id);
	
	
	/**
	 * 
	 * @param adminAreaId
	 * @param upartProjectId
	 * @param userName
	 * @param aaprtProjectId
	 * @param status
	 * @return List<UserProjAppRelTbl>
	 */
	@Query("FROM UserProjAppRelTbl UPART WHERE UPART.userProjectRelId IN (SELECT UPRT.userProjectRelId FROM UserProjectRelTbl "
			+ "UPRT WHERE UPRT.projectId = :upartProjectId AND UPRT.userId = ((select ut.userId from UsersTbl ut where ut.username=:userName))) "
			+ "AND UPART.adminAreaProjectRelId IN (SELECT "
			+ "AAPRT.adminAreaProjectRelId FROM AdminAreaProjectRelTbl AAPRT, SiteAdminAreaRelTbl SAART WHERE "
			+ "AAPRT.siteAdminAreaRelId = SAART.siteAdminAreaRelId AND AAPRT.projectId = :aaprtProjectId AND SAART.siteAdminAreaRelId = "
			+ "(SELECT SAART.siteAdminAreaRelId FROM SiteAdminAreaRelTbl SAART WHERE SAART.adminAreaId = :adminAreaId)) AND "
			+ "UPART.projectApplicationId IN (SELECT PAT.projectApplicationId FROM ProjectApplicationsTbl PAT WHERE PAT.status IN (:status) "
			+ "AND PAT.position IN ('*ButtonTask*', '*IconTask*', '*MenuTask*'))")
	List<UserProjAppRelTbl> findUserProjAppRelTbls(@Param("adminAreaId") AdminAreasTbl adminAreaId, @Param("upartProjectId") ProjectsTbl upartProjectId, 
			@Param("userName") String userName, @Param("aaprtProjectId") ProjectsTbl aaprtProjectId,  @Param("status") List<String> status);
	
	/**
	 * 
	 * @param userProjectRelId
	 * @param projectApplicationId
	 * @return UserProjAppRelTbl
	 */
	UserProjAppRelTbl findByUserProjectRelIdAndProjectApplicationId(UserProjectRelTbl userProjectRelId, ProjectApplicationsTbl projectApplicationId);
	
	@Modifying
	@Query("update UserProjAppRelTbl upart  set upart.userRelType=:userRelType where upart.userProjAppRelId=:userProjAppRelId")
	int updateUserRelType(@Param("userRelType") String userRelType, @Param("userProjAppRelId") String userProjAppRelId);
	
	List<UserProjAppRelTbl> findByUserProjectRelIdAndAdminAreaProjectRelIdAndProjectApplicationId(UserProjectRelTbl userProjectRelId, AdminAreaProjectRelTbl adminAreaProjectRelId, ProjectApplicationsTbl projectApplicationId);
	
}
