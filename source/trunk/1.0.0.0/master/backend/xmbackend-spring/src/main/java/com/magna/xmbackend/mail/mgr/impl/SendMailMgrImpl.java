/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magna.xmbackend.mail.mgr.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.magna.xmbackend.entities.MailQueueTbl;
import com.magna.xmbackend.entities.PropertyConfigTbl;
import com.magna.xmbackend.entities.UsersTbl;
import com.magna.xmbackend.exception.XMObjectNotFoundException;
import com.magna.xmbackend.jpa.dao.MailQueueJpaDao;
import com.magna.xmbackend.jpa.dao.PropertyConfigJpaDao;
import com.magna.xmbackend.jpa.dao.UserJpaDao;
import com.magna.xmbackend.mail.mgr.MailSupportMgr;
import com.magna.xmbackend.mail.mgr.SendMailMgr;
import com.magna.xmbackend.utils.MessageMaker;
import com.magna.xmbackend.vo.notification.SendMailRequest;
import com.magna.xmbackend.vo.notification.SendMailResponse;

/**
 *
 * @author dhana
 */
/**
 * @author Bhabadyuti Bal
 *
 */
/**
 * @author Bhabadyuti Bal
 *
 */
@Component
public class SendMailMgrImpl implements SendMailMgr {

    private static final Logger LOG
            = LoggerFactory.getLogger(SendMailMgrImpl.class);

    @Autowired
    private MailSupportMgr mailSupportMgr;
    @Autowired
    private MailQueueJpaDao mailQueueJpaDao;
    @Autowired
    private PropertyConfigJpaDao propertyConfigJpaDao;
    @Autowired
    private MessageMaker messageMaker;
    @Autowired
    private UserJpaDao userJpaDao;
    

    @Override
	public SendMailResponse sendMail(SendMailRequest sendMailRequest) {
		LOG.info(">> sendMail");
		final List<Map<String, String>> statusMaps = new ArrayList<>();
		List<String> usersToNotify = sendMailRequest.getUsersToNotify();
		List<String> ccUsersToNotify = sendMailRequest.getCcUsersToNotify();
		List<String> validToEmails = new ArrayList<>();
		List<String> invalidToUsers = this.validateAndGetEmails(usersToNotify, validToEmails);
		
		List<String> validCcEmails = new ArrayList<>();
		List<String> invalidCcUsers = this.validateAndGetEmails(ccUsersToNotify, validCcEmails);
		invalidToUsers.addAll(invalidCcUsers);
		if (invalidToUsers.size() > 0) {
			try {
				String commaSeparatedUsernames = String.join(",", invalidToUsers);
				String[] param = { commaSeparatedUsernames };
				throw new XMObjectNotFoundException("No Users Found", "USR_ERR0001", param);
			} catch (XMObjectNotFoundException objectNotFoundException) {
				Map<String, String> statusMap = messageMaker.extractFromException(objectNotFoundException);
				statusMaps.add(statusMap);
			}
		}
		String subject = sendMailRequest.getSubject();
		String message = sendMailRequest.getMessage();
		Date date = new Date();
		//List<String> invalidUserNames = new ArrayList<>();
		List<MailQueueTbl> mailQueueTbls = new ArrayList<>();
		
		SendMailResponse ack = this.successAck(statusMaps);
		
		Map<String, String> eventAttributes = new HashMap<>();
		eventAttributes.put("userName", "Username");
		eventAttributes.put("SUBJECT", subject);
		eventAttributes.put("MESSAGE", message);
		
		String configuredFromEmail = this.getConfiguredFromEmail();
		String event = sendMailRequest.getEvent();
		
		String textMsg = this.mailSupportMgr.bindSendMailDirectTemplate2Data(eventAttributes);
		String uuid = UUID.randomUUID().toString();
		String semicolSeparatedToEmails = String.join(";", validToEmails);
		String semicolSeparatedCcEmails = String.join(";", validCcEmails);
		MailQueueTbl mailQueueTbl = new MailQueueTbl(uuid, "username", semicolSeparatedToEmails, semicolSeparatedCcEmails, 
				configuredFromEmail, subject, textMsg, event, "new", "0", date, date);
		mailQueueTbl.setDelaySend("false");
		this.mailQueueJpaDao.save(mailQueueTbls);
		LOG.info("<< sendMail");
		return ack;
	}

    
    /**
     * @param userToNotify
     * @return List of invalid usernames or valid emails
     */
    private List<String> validateAndGetEmails(List<String> userToNotify, List<String> validEmails) {
    	List<String> invalidUsers = new ArrayList<>();
    	userToNotify.forEach(username -> {
    		UsersTbl usersTbl = this.userJpaDao.findByUsernameIgnoreCase(username);
    		if (usersTbl == null) {
    			invalidUsers.add(username);
    		} else {
    			validEmails.add(usersTbl.getEmailId());
    		}
    	});
    	if (invalidUsers.size() == 0) {
    		return validEmails;
    	}
    	validEmails.clear();
		return invalidUsers;
	}

	private SendMailResponse successAck(List<Map<String, String>> statusMaps) {
        return new SendMailResponse("Mail posted to Queue - Success", statusMaps);
    }
    
    /*private String getSMTPValue(String key) {
        PropertyConfigTbl propertyConfigTbl
                = this.propertyConfigJpaDao
                        .findByCategoryAndProperty("SMTP", key);
        String value = propertyConfigTbl.getValue();
        return value;
    }*/
    
    private String getConfiguredFromEmail() {
    	PropertyConfigTbl propertyConfigTbl = this.propertyConfigJpaDao.findByCategoryAndProperty("SMTP", "SMTP_EMAIL_ID");
    	String fromEmailId = propertyConfigTbl.getValue();
    	return fromEmailId;
    }

}
