/**
 * 
 */
package com.magna.xmbackend.controller;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.magna.xmbackend.entities.CssSettingsTbl;
import com.magna.xmbackend.mgr.CssSettingsMgr;
import com.magna.xmbackend.vo.css.CssSettingsRequest;
import com.magna.xmbackend.vo.css.CssSettingsResponse;

/**
 * @author Bhabadyuti Bal
 *
 */

@RestController
@RequestMapping(value="/css")
public class CssSettingsController {

	private static final Logger LOG = LoggerFactory.getLogger(CssSettingsController.class);
	
	@Autowired
	private CssSettingsMgr cssSettingsMgr;
	
	
	/*@PostMapping(value="/save", consumes = "multipart/form-data")
	public ResponseEntity<Boolean> save(
			 HttpServletRequest httpServletRequest,
			@RequestParam("isApplied") @Valid String isApplied,
			@RequestPart("file") @NotNull MultipartFile file) {
		LOG.info("> save");
		String userName = httpServletRequest.getHeader("USER_NAME");
		boolean saved = this.cssSettingsMgr.save(isApplied, file, userName);
		LOG.info("< save");
		return new ResponseEntity<Boolean>(saved, HttpStatus.OK);
	}*/
	
	
	@PostMapping(value="/save", consumes = { MediaType.APPLICATION_JSON_VALUE,
			MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_FORM_URLENCODED_VALUE,
			MediaType.MULTIPART_FORM_DATA_VALUE}, produces = {
					MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE,
					MediaType.APPLICATION_FORM_URLENCODED_VALUE })
	public ResponseEntity<Boolean> save(
			 HttpServletRequest httpServletRequest,
			@RequestBody CssSettingsRequest cssSettingsRequest ) {
		LOG.info("> save");
		String userName = httpServletRequest.getHeader("USER_NAME");
		boolean saved = this.cssSettingsMgr.createOrUpdate(cssSettingsRequest, userName);
		LOG.info("< save");
		return new ResponseEntity<Boolean>(saved, HttpStatus.OK); 
	}
	
	
	@GetMapping(value="/findCssSettings", consumes = { MediaType.APPLICATION_JSON_VALUE,
			MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_FORM_URLENCODED_VALUE }, produces = {
					MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE,
					MediaType.APPLICATION_FORM_URLENCODED_VALUE })
	public ResponseEntity<CssSettingsResponse> findCssSettingsForUser(HttpServletRequest httpServletRequest) {
		LOG.info("> getCssSettingsForUser");
		String userName = httpServletRequest.getHeader("USER_NAME");
		CssSettingsTbl cssSettingsTbl = this.cssSettingsMgr.findCssSettingsForUser(userName);
		CssSettingsResponse response = new CssSettingsResponse(cssSettingsTbl);
		LOG.info("< getCssSettingsForUser");
		return new ResponseEntity<>(response, HttpStatus.OK);
	}
	
	
	@DeleteMapping(value="/deleteCssSettings", consumes = { MediaType.APPLICATION_JSON_VALUE,
			MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_FORM_URLENCODED_VALUE }, produces = {
					MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE,
					MediaType.APPLICATION_FORM_URLENCODED_VALUE })
	public ResponseEntity<Boolean> deleteCssSettings(HttpServletRequest httpServletRequest) {
		LOG.info("> deleteCssSettings");
		String userName = httpServletRequest.getHeader("USER_NAME");
		boolean isDeleted = this.cssSettingsMgr.deleteCssSettings(userName);
		LOG.info("< deleteCssSettings");
		return new ResponseEntity<>(isDeleted, HttpStatus.OK);
	}
	
	
	@PutMapping(value="/updateCssSettings/{isApplied}", consumes = { MediaType.APPLICATION_JSON_VALUE,
			MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_FORM_URLENCODED_VALUE }, produces = {
					MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE,
					MediaType.APPLICATION_FORM_URLENCODED_VALUE })
	public ResponseEntity<Boolean> updateCssSettings(HttpServletRequest httpServletRequest,
				@PathVariable String isApplied ) {
		LOG.info("> deleteCssSettings");
		String userName = httpServletRequest.getHeader("USER_NAME");
		boolean isUpdated = this.cssSettingsMgr.updateCssSettings(userName, isApplied);
		LOG.info("< deleteCssSettings");
		return new ResponseEntity<>(isUpdated, HttpStatus.OK);
	}
}
