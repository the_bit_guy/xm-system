package com.magna.xmbackend.mgr.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Component;

import com.magna.xmbackend.audit.mgr.StartAppAuditMgr;
import com.magna.xmbackend.controller.AuthController;
import com.magna.xmbackend.entities.AdminAreaProjectRelTbl;
import com.magna.xmbackend.entities.AdminAreasTbl;
import com.magna.xmbackend.entities.BaseApplicationsTbl;
import com.magna.xmbackend.entities.IconsTbl;
import com.magna.xmbackend.entities.LanguagesTbl;
import com.magna.xmbackend.entities.ProjectStartAppRelTbl;
import com.magna.xmbackend.entities.ProjectsTbl;
import com.magna.xmbackend.entities.StartAppTranslationTbl;
import com.magna.xmbackend.entities.StartApplicationsTbl;
import com.magna.xmbackend.entities.UserStartAppRelTbl;
import com.magna.xmbackend.entities.UserTkt;
import com.magna.xmbackend.entities.UsersTbl;
import com.magna.xmbackend.exception.CannotCreateObjectException;
import com.magna.xmbackend.exception.XMObjectNotFoundException;
import com.magna.xmbackend.jpa.dao.StartApplicationJpaDao;
import com.magna.xmbackend.jpa.dao.UserTktJpaDao;
import com.magna.xmbackend.jpa.rel.dao.AdminAreaProjectRelJpaDao;
import com.magna.xmbackend.jpa.rel.dao.AdminAreaStartAppRelJpaDao;
import com.magna.xmbackend.jpa.rel.dao.ProjectStartAppRelJpaDao;
import com.magna.xmbackend.jpa.rel.dao.SiteAdminAreaRelJpaDao;
import com.magna.xmbackend.mgr.AdminAreaManager;
import com.magna.xmbackend.mgr.ProjectMgr;
import com.magna.xmbackend.mgr.StartApplicationMgr;
import com.magna.xmbackend.mgr.UserMgr;
import com.magna.xmbackend.utils.MessageMaker;
import com.magna.xmbackend.utils.Validator;
import com.magna.xmbackend.vo.enums.Application;
import com.magna.xmbackend.vo.enums.Status;
import com.magna.xmbackend.vo.permission.ValidationRequest;
import com.magna.xmbackend.vo.startApplication.StartApplicationRequest;
import com.magna.xmbackend.vo.startApplication.StartApplicationResponse;
import com.magna.xmbackend.vo.startApplication.StartApplicationTranslation;

/**
 *
 * @author dhana
 */
@Component
public class StartApplicationMgrImpl implements StartApplicationMgr {

    private static final Logger LOG
            = LoggerFactory.getLogger(StartApplicationMgrImpl.class);

    @Autowired
    private StartApplicationJpaDao startApplicationJpaDao;
    @Autowired
    private UserMgr userMgr;
    @Autowired
    private ProjectMgr projectMgr;
    @Autowired
    private AdminAreaManager adminAreaManager;
    @Autowired
    private Validator validator;
    @Autowired
    SiteAdminAreaRelJpaDao siteAdminAreaRelJpaDao;
    @Autowired
    AdminAreaStartAppRelJpaDao adminAreaStartAppRelJpaDao;
    @Autowired
    AdminAreaProjectRelJpaDao adminAreaProjectRelJpaDao;
    @Autowired
    ProjectStartAppRelJpaDao projectStartAppRelJpaDao;
    @Autowired
    private MessageMaker messageMaker;
    @Autowired
    UserTktJpaDao userTktJpaDao;
    @Autowired
    StartAppAuditMgr startAppAuditMgr;
    @Autowired
    private AuthController authController;

    /**
     *
     * @param validationRequest
     * @return StartApplicationResponse
     */
    @Override
    public final StartApplicationResponse findAll(final ValidationRequest validationRequest) {
        LOG.info(">> findAll");
        Iterable<StartApplicationsTbl> startApplicationsTbls
                = startApplicationJpaDao.findAll();
        final boolean isViewInactive = validator.isViewInactiveAllowed(validationRequest);
        LOG.info("findAll isViewInactive={}", isViewInactive);
        startApplicationsTbls = validator.filterStartApplicationResponse(isViewInactive, startApplicationsTbls);
        final StartApplicationResponse sar
                = new StartApplicationResponse(startApplicationsTbls);
        LOG.info("<< findAll");
        return sar;
    }

    /**
     *
     * @param id
     * @return StartApplicationsTbl
     */
    @Override
    public final StartApplicationsTbl findById(final String id) {
        LOG.info(">> findById {}", id);
        final StartApplicationsTbl sat = startApplicationJpaDao.findOne(id);
        LOG.info("<< findById");
        return sat;
    }

    /**
     *
     * @param id
     * @return boolean
     */
    @Override
    public final boolean delete(final String id) {
        LOG.info(">> delete {}", id);
        boolean isDeleted = false;
        try {
            startApplicationJpaDao.delete(id);
            isDeleted = true;
        } catch (Exception e) {
            if (e instanceof EmptyResultDataAccessException) {
                final String[] param = {id};
                throw new XMObjectNotFoundException("User not found", "P_ERR0015", param);
            }
        }

        LOG.info("<< deleteById");
        return isDeleted;
    }

    /**
     *
     * @param startAppIds
     * @param httpServletRequest
     * @return StartApplicationResponse
     */
    @Override
    public StartApplicationResponse multiDelete(final Set<String> startAppIds,
            final HttpServletRequest httpServletRequest) {
        LOG.info(">> multiDelete");
        final List<Map<String, String>> statusMaps = new ArrayList<>();
        startAppIds.forEach(startAppId -> {
            StartApplicationsTbl sat = null;
            try {
                sat = findById(startAppId);
                this.delete(startAppId);
                this.startAppAuditMgr.startAppDeleteStatusAuditor(startAppId, httpServletRequest, true, sat);
            } catch (XMObjectNotFoundException objectNotFound) {
                Map<String, String> statusMap = messageMaker.extractFromException(objectNotFound);
                statusMaps.add(statusMap);
                this.startAppAuditMgr.startAppDeleteStatusAuditor(startAppId, httpServletRequest, false, sat);
            }
        });
        final StartApplicationResponse userResponse = new StartApplicationResponse(statusMaps);
        LOG.info(">> multiDelete");
        return userResponse;
    }

    /**
     *
     * @param startApplicationRequest
     * @param isUpdate
     * @return StartApplicationsTbl
     */
    @Override
    public final StartApplicationsTbl createOrUpdate(
            StartApplicationRequest startApplicationRequest, boolean isUpdate) {
        LOG.info(">> createOrUpdate:: isUpdate --> {}", isUpdate);
        final StartApplicationsTbl satIn
                = this.convert2Entity(startApplicationRequest, isUpdate);
        final StartApplicationsTbl satOut = startApplicationJpaDao.save(satIn);
        LOG.info("<< createOrUpdate");
        return satOut;
    }

    /**
     *
     * @param startApplicationRequest
     * @param isUpdate
     * @return StartApplicationsTbl
     */
    private StartApplicationsTbl convert2Entity(
            final StartApplicationRequest startApplicationRequest,
            final boolean isUpdate) {
        String id = UUID.randomUUID().toString();
        LOG.info("Start App Id={}", id);
        final Date date = new Date();
        if (isUpdate) {
            id = startApplicationRequest.getId();
        }
        final String baseAppId = startApplicationRequest.getBaseAppId();
        final String iconId = startApplicationRequest.getIconId();
        final String status = startApplicationRequest.getStatus();
        final String isMessage = startApplicationRequest.getIsMessage().toString();
        final Date startMsgExpiryDate = startApplicationRequest.getStartMsgExpiryDate();
        final String name = startApplicationRequest.getName();
        final List<StartApplicationTranslation> translations = startApplicationRequest.getStartApplicationTranslations();

        if (!isUpdate
                && null != this.findStartAppIdForName(name)) {
            final Map<String, String[]> paramMap
                    = this.messageMaker.getStartAppNameWithi18nCode(translations, name);
            throw new CannotCreateObjectException(
                    "Start application name already found", "ERR0002", paramMap);
        }

        final StartApplicationsTbl sat = new StartApplicationsTbl(id);
        sat.setName(name);
        BaseApplicationsTbl batToSet = null;
        if (baseAppId != null) {
            batToSet = new BaseApplicationsTbl(baseAppId);
            sat.setBaseApplicationId(batToSet);
        }
        sat.setCreateDate(date);
        sat.setUpdateDate(date);
        sat.setIconId(new IconsTbl(iconId));
        sat.setStatus(status);
        sat.setIsMessage(isMessage);
        sat.setStartMessageExpiryDate(startMsgExpiryDate);

        final List<StartAppTranslationTbl> satts = new ArrayList<>();

        if(translations != null ) { 
        	for (StartApplicationTranslation translation : translations) {
                String startAppTransId = UUID.randomUUID().toString();
                final String description = translation.getDescription();
                final String remarks = translation.getRemarks();
                final String languageCode = translation.getLanguageCode();
                if (isUpdate) {
                    startAppTransId = translation.getId();
                }

                final StartAppTranslationTbl satt
                        = new StartAppTranslationTbl(startAppTransId);
                satt.setCreateDate(date);
                satt.setUpdateDate(date);
                satt.setDescription(description);
                satt.setRemarks(remarks);
                satt.setLanguageCode(new LanguagesTbl(languageCode));
                satt.setStartApplicationId(new StartApplicationsTbl(id));
                satts.add(satt);
            }
        } else {
        	StartAppTranslationTbl enTranslation = new StartAppTranslationTbl(UUID.randomUUID().toString(), date, date, new LanguagesTbl("en"), sat);
        	StartAppTranslationTbl geTranslation = new StartAppTranslationTbl(UUID.randomUUID().toString(), date, date, new LanguagesTbl("de"), sat);
        	satts.add(enTranslation);
        	satts.add(geTranslation);
        }
        
        sat.setStartAppTranslationTblCollection(satts);
        return sat;
    }

    /**
     * Find start app id for name.
     *
     * @param name the name
     * @return the string
     */
    private String findStartAppIdForName(final String name) {
        final StartApplicationsTbl startApplicationsTbl = this.startApplicationJpaDao.findByNameIgnoreCase(name);
        String startAppId = null;
        if (startApplicationsTbl != null) {
            startAppId = startApplicationsTbl.getStartApplicationId();
        }
        LOG.info(">> findStartAppIdForName - Start App id for name is {} - {}", name, startAppId);
        return startAppId;
    }

    /**
     * Find by name.
     *
     * @param name the name
     * @return the StartApplicationsTbl
     */
    @Override
    public final StartApplicationsTbl findByName(final String name) {
        LOG.info(">> findByName {}", name);
        final StartApplicationsTbl startApplicationsTbl = this.startApplicationJpaDao.findByNameIgnoreCase(name);
        if (null == startApplicationsTbl) {
            final String[] param = {name};
            throw new XMObjectNotFoundException("Start application with name not found", "U_ERR00012", param);
        }
        LOG.info("<< findByName");
        return startApplicationsTbl;
    }

    /**
     *
     * @param status
     * @param id
     * @return boolean
     */
    @Override
    public boolean updateStatusById(final String status, final String id) {
        LOG.info(">> updateStatusById");
        boolean isUpdated = false;
        final int out = this.startApplicationJpaDao.setStatusForStartApplicationsTbl(status, id);
        LOG.debug("is Modified status value {}", out);
        if (out > 0) {
            isUpdated = true;
        }
        LOG.info("<< updateStatusById");
        return isUpdated;
    }

    /**
     *
     * @param id
     * @param validationRequest
     * @return ProjectApplicationResponse
     */
    @Override
    public final StartApplicationResponse findStartApplicationsByBaseAppId(final String id,
            final ValidationRequest validationRequest) {
        StartApplicationResponse startApplicationResponse = null;
        LOG.info(">> findStartApplicationsByBaseAppId {}", id);
        final boolean isViewInactive = validator.isViewInactiveAllowed(validationRequest);
        LOG.info("findStartApplicationsByBaseAppId isViewInactive={}", isViewInactive);
        final BaseApplicationsTbl baseApplicationsTbl = validator.validateBaseApp(id, isViewInactive);
        Collection<StartApplicationsTbl> startApplicationsTbls = baseApplicationsTbl.getStartApplicationsTblCollection();
        if (!startApplicationsTbls.isEmpty()) {
            startApplicationsTbls = validator.filterStartApplicationResponse(isViewInactive, startApplicationsTbls);
            startApplicationResponse = new StartApplicationResponse(startApplicationsTbls);
        } else {
            throw new XMObjectNotFoundException("No Base App Start App Relation found", "SA_ERR0001");
        }
        LOG.info("<< findStartApplicationsByBaseAppId");
        return startApplicationResponse;
    }

    /**
     *
     * @param id
     * @return ProjectApplicationResponse
     */
    @Override
    public final StartApplicationResponse findStartApplicationsByUserId(final String id) {
        StartApplicationResponse startApplicationResponse = null;
        LOG.info(">> findStartApplicationsByUserId {}", id);
        final UsersTbl usersTbl = this.userMgr.findById(id);
        if (null != usersTbl) {
            final Collection<UserStartAppRelTbl> userStartAppRelTbls = usersTbl.getUserStartAppRelTblCollection();
            if (!userStartAppRelTbls.isEmpty()) {
                final Collection<StartApplicationsTbl> startApplicationsTbls = new ArrayList<>();
                for (final UserStartAppRelTbl userStartAppRelTbl : userStartAppRelTbls) {
                    final StartApplicationsTbl startApplicationsTbl = userStartAppRelTbl.getStartApplicationId();
                    if (null != startApplicationsTbl) {
                        startApplicationsTbls.add(startApplicationsTbl);
                    }
                }
                startApplicationResponse = new StartApplicationResponse(startApplicationsTbls);
            } else {
                throw new RuntimeException("No User Start App Relation found");
            }
        } else {
            throw new RuntimeException("No UserId found");
        }
        LOG.info("<< findStartApplicationsByUserId");
        return startApplicationResponse;
    }

    /**
     *
     * @param adminAreaId
     * @param projectId
     * @param validationRequest
     * @return ProjectApplicationResponse
     */
    @Override
    public final StartApplicationResponse findStartAppByAAIdProjectId(final String adminAreaId, final String tkt,
            final String projectId, final ValidationRequest validationRequest) {
        LOG.info(">> findStartAppByProjectIdAAId");
        final AdminAreasTbl adminAreasTbl = adminAreaManager.findById(adminAreaId);
        if (null == adminAreasTbl) {
            throw new RuntimeException("Admin Area not found");
        }
        final ProjectsTbl projectsTbl = projectMgr.findById(projectId);
        if (null == projectsTbl) {
            throw new RuntimeException("Project not found");

        }
        List<String> status = new ArrayList<>();
        status.add(Status.ACTIVE.name());
        Iterable<StartApplicationsTbl> startApplicationsTbls = null;
        final boolean isViewInactive = validator.isViewInactiveAllowed(validationRequest);
        LOG.info("findStartAppsByAAId isViewInactive={}", isViewInactive);

        if (isViewInactive) {
            status.add(Status.INACTIVE.name());
        } else {
            final UserTkt userTktTbl = this.userTktJpaDao.findByTktAndApplicationName(this.authController.hash(tkt),
                    Application.CAX_ADMIN_MENU.name());
            if (userTktTbl != null) {
                validationRequest.setUserName(userTktTbl.getUsername());
                final boolean isViewInactive2 = validator.isViewInactiveAllowed(validationRequest);
                if (isViewInactive2) {
                    status.add(Status.INACTIVE.name());
                }
            }
        }

        startApplicationsTbls = this.startApplicationJpaDao.findStartAppByProjectIdAdminAreaId(new AdminAreasTbl(adminAreaId), new ProjectsTbl(projectId), status);

        /* final boolean isViewInactive = validator.isViewInactiveAllowed(validationRequest);
        LOG.info("findStartAppByProjectIdAAId isViewInactive={}", isViewInactive);
        Iterable<StartApplicationsTbl> startApplicationsTbls = this.startApplicationJpaDao
                .findStartAppByProjectIdAdminAreaId(new AdminAreasTbl(adminAreaId), new ProjectsTbl(projectId),
                        satStatus, stStatus, aatStatus, ptStatus, saartStatus, aaprtStatus, psartStatus);
        startApplicationsTbls = validator.filterStartApplicationResponse(isViewInactive, startApplicationsTbls);*/
        final StartApplicationResponse startApplicationResponse = new StartApplicationResponse(startApplicationsTbls);
        LOG.info("<< findStartAppByProjectIdAAId");
        return startApplicationResponse;
    }

    /**
     *
     * @param adminAreaId
     * @param validationRequest
     * @return StartApplicationResponse
     */
    /*  @Override
    public StartApplicationResponse findStartAppsByAAId(String adminAreaId, String tkt,
            final ValidationRequest validationRequest) {
        String startAppStatus = Status.ACTIVE.name();
        String siteStatus = Status.ACTIVE.name();
        String adminAreaStatus = Status.ACTIVE.name();
        String siteAdminAreaRelStatus = Status.ACTIVE.name();
        String adminAreaSrartAppRelStatus = Status.ACTIVE.name();
        final boolean isViewInactive = validator.isViewInactiveAllowed(validationRequest);
        LOG.info("findStartAppsByAAId isViewInactive={}", isViewInactive);
        Iterable<StartApplicationsTbl> startApplicationsTbls = this.startApplicationJpaDao.findStartAppByAdminAreaId(
                new AdminAreasTbl(adminAreaId), startAppStatus, siteStatus, adminAreaStatus, siteAdminAreaRelStatus,
                adminAreaSrartAppRelStatus);
        startApplicationsTbls = validator.filterStartApplicationResponse(isViewInactive, startApplicationsTbls);
        final StartApplicationResponse startApplicationResponse = new StartApplicationResponse(startApplicationsTbls);
        return startApplicationResponse;
    }*/
    @Override
    public StartApplicationResponse findStartAppsByAAId(String adminAreaId, String tkt,
            final ValidationRequest validationRequest) {
        List<String> status = new ArrayList<>();
        status.add(Status.ACTIVE.name());
        Iterable<StartApplicationsTbl> startApplicationsTbls = null;
        final boolean isViewInactive = validator.isViewInactiveAllowed(validationRequest);
        LOG.info("findStartAppsByAAId isViewInactive={}", isViewInactive);

        if (isViewInactive) {
            status.add(Status.INACTIVE.name());
        } else {
            final UserTkt userTktTbl = this.userTktJpaDao.findByTktAndApplicationName(this.authController.hash(tkt),
                    Application.CAX_ADMIN_MENU.name());
            if (userTktTbl != null) {
                validationRequest.setUserName(userTktTbl.getUsername());
                final boolean isViewInactive2 = validator.isViewInactiveAllowed(validationRequest);
                if (isViewInactive2) {
                    status.add(Status.INACTIVE.name());
                }
            }
        }
        startApplicationsTbls = this.startApplicationJpaDao.findStartAppByAdminAreaId(
                new AdminAreasTbl(adminAreaId), status);

        final StartApplicationResponse startApplicationResponse = new StartApplicationResponse(startApplicationsTbls);
        return startApplicationResponse;
    }

    /**
     *
     * @param projectId
     * @return StartApplicationResponse
     */
    @Override
    public StartApplicationResponse findStartAppsByProjectId(String projectId) {
        LOG.info(">> findStartAppsByProjectId");
        List<AdminAreaProjectRelTbl> adminAreaProjectRelTbls = this.adminAreaProjectRelJpaDao.findByProjectId(new ProjectsTbl(projectId));
        List<StartApplicationsTbl> startApplicationsTbls = null;
        if (!adminAreaProjectRelTbls.isEmpty()) {
            List<ProjectStartAppRelTbl> projectStartAppRelTbls = this.projectStartAppRelJpaDao.findByAdminAreaProjectRelIdIn(adminAreaProjectRelTbls);
            startApplicationsTbls = new ArrayList<>();
            if (!projectStartAppRelTbls.isEmpty()) {
                for (ProjectStartAppRelTbl psart : projectStartAppRelTbls) {
                    startApplicationsTbls.add(psart.getStartApplicationId());
                }
            } else {
                throw new XMObjectNotFoundException("No Start App found", "P_ERR0006");
            }
        }
        StartApplicationResponse response = new StartApplicationResponse(startApplicationsTbls);
        LOG.info("<< findStartAppsByProjectId");
        return response;
    }

    /**
     *
     * @param userName
     * @param validationRequest
     * @return StartApplicationResponse
     *//*
   @Override
   public StartApplicationResponse findUserStartApplicationsByUserName(final String userName, final String tkt,
           final ValidationRequest validationRequest) {

       List<String> status = new ArrayList<>();
       status.add(Status.ACTIVE.name());
       String userStatus = Status.ACTIVE.name();
       String userStrartAppRelStatus = Status.ACTIVE.name();
       Iterable<StartApplicationsTbl> startApplicationsTbls = null;
       final boolean isViewInactive = validator.isViewInactiveAllowed(validationRequest);
       LOG.info("findStartAppsByAAId isViewInactive={}", isViewInactive);
       //vijay view inactive implementation
       Iterable<StartApplicationsTbl> startApplicationsTbls = this.startApplicationJpaDao
               .findStartAppByUserName(userName, startAppStatus, userStatus, userStrartAppRelStatus);
       startApplicationsTbls = validator.filterStartApplicationResponse(isViewInactive, startApplicationsTbls);
       
       if(isViewInactive){
    	   status.add(Status.INACTIVE.name());
           userStatus = Status.ACTIVE.name()+","+Status.INACTIVE.name();
           userStrartAppRelStatus = Status.ACTIVE.name()+","+Status.INACTIVE.name();
           startApplicationsTbls = this.startApplicationJpaDao.findStartAppByUserName(userName, status, status, status);
		} else {
			final UserTkt userTktTbl = this.userTktJpaDao.findByTktAndApplicationName(this.authController.hash(tkt),
					Application.CAX_ADMIN_MENU.name());
			if (userTktTbl != null) {
				
				validationRequest.setUserName(userTktTbl.getUsername());
				final boolean isViewInactive2ndCheck = validator.isViewInactiveAllowed(validationRequest);
				if (isViewInactive2ndCheck) {
					status.add(Status.INACTIVE.name());
					
					 * userStatus =
					 * Status.ACTIVE.name()+","+Status.INACTIVE.name();
					 * userStrartAppRelStatus =
					 * Status.ACTIVE.name()+","+Status.INACTIVE.name();
					 
					startApplicationsTbls = this.startApplicationJpaDao.findStartAppByUserName(userName, status, status,
							status);
				} else {
					startApplicationsTbls = this.startApplicationJpaDao.findStartAppByUserName(userName, status, status,
							status);
				}
			} else {
				startApplicationsTbls = this.startApplicationJpaDao.findStartAppByUserName(userName, status, status,
						status);
			}

		}
       
       final StartApplicationResponse startApplicationResponse = new StartApplicationResponse(startApplicationsTbls);
       LOG.info("<< findStartApplicationsByUserId");
       return startApplicationResponse;
   }*/

    /**
     *
     * @param userName
     * @param validationRequest
     * @return StartApplicationResponse
     */
    @Override
    public StartApplicationResponse findUserStartApplicationsByUserName(final String userName, final String tkt,
            final ValidationRequest validationRequest) {

        List<String> status = new ArrayList<>();
        status.add(Status.ACTIVE.name());
        Iterable<StartApplicationsTbl> startApplicationsTbls = null;
        final boolean isViewInactive = validator.isViewInactiveAllowed(validationRequest);
        LOG.info("findStartAppsByAAId isViewInactive={}", isViewInactive);

        if (isViewInactive) {
            status.add(Status.INACTIVE.name());
        } else {
            final UserTkt userTktTbl = this.userTktJpaDao.findByTktAndApplicationName(this.authController.hash(tkt),
                    Application.CAX_ADMIN_MENU.name());
            if (userTktTbl != null) {
                validationRequest.setUserName(userTktTbl.getUsername());
                final boolean isViewInactive2 = validator.isViewInactiveAllowed(validationRequest);
                if (isViewInactive2) {
                    status.add(Status.INACTIVE.name());
                }
            }
        }

        startApplicationsTbls = this.startApplicationJpaDao.findStartAppByUserName(userName, status);

        final StartApplicationResponse startApplicationResponse = new StartApplicationResponse(startApplicationsTbls);
        LOG.info("<< findStartApplicationsByUserId");
        return startApplicationResponse;
    }

	@Override
	public StartApplicationResponse multiUpdate(List<StartApplicationRequest> startApplicationRequests,
			HttpServletRequest httpServletRequest) {
		LOG.info(">> multiUpdate");
		List<String> startAppNames = new ArrayList<>();
		startApplicationRequests.forEach(startApplicationRequest -> {
			String startAppName = startApplicationRequest.getName();
			String status = startApplicationRequest.getStatus();
			try {
				boolean updateStatusById = this.updateStatusById(status, startApplicationRequest.getId());
				if (updateStatusById) {
					// success & audit it
					this.startAppAuditMgr.startAppStatusUpdateSuccessAuditor(startAppName, status, httpServletRequest);
				} else {
					startAppNames.add(startAppName);
				}
			} catch (Exception ex) {
				this.startAppAuditMgr.startAppStatusUpdateFailureAuditor(startAppName, status, ex, httpServletRequest);
			}

		});
		StartApplicationResponse startApplicationResponse = new StartApplicationResponse();
		startApplicationResponse.setStatusUpdatationFailedList(startAppNames);
		LOG.info("<< multiUpdate");
		return startApplicationResponse;
	}

	@Override
	public StartApplicationsTbl updateForBatch(StartApplicationRequest startApplicationRequest) {
		LOG.info(">> updateForBatch");
		StartApplicationsTbl applicationsTbl = this.findByName(startApplicationRequest.getName());
		if (applicationsTbl == null) {
			throw new XMObjectNotFoundException("start app not found", "AA_ERR0019", new String[]{startApplicationRequest.getId()});
		}
		String status = (startApplicationRequest.getStatus() == null)?(applicationsTbl.getStatus()):startApplicationRequest.getStatus();
		String iconId = (startApplicationRequest.getIconId() == null) ? (applicationsTbl.getIconId().getIconId())
				: startApplicationRequest.getIconId();
		boolean isMsg = (startApplicationRequest.getIsMessage() == null) ? (Boolean.valueOf(applicationsTbl.getIsMessage())) : (startApplicationRequest.getIsMessage());
		
		String baseAppId = null;
		if(applicationsTbl.getBaseApplicationId()!=null){
			baseAppId = applicationsTbl.getBaseApplicationId().getBaseApplicationId();
			if(applicationsTbl.getIsMessage().equals("true")){
				baseAppId = null;
			}
		}
		baseAppId = (startApplicationRequest.getBaseAppId() == null ) ? (baseAppId) : (startApplicationRequest.getBaseAppId());
		Date startMsgExpDate = (startApplicationRequest.getStartMsgExpiryDate() == null) ? (applicationsTbl.getStartMessageExpiryDate()) : (startApplicationRequest.getStartMsgExpiryDate());
		
		startApplicationRequest.setId(applicationsTbl.getStartApplicationId());
		startApplicationRequest.setStatus(status);
		startApplicationRequest.setIconId(iconId);
		startApplicationRequest.setIsMessage(isMsg);
		if(isMsg){
			//  set application to null
			baseAppId = null;
		} else if(!isMsg){
			// set expDate to null
			startMsgExpDate = null;
		}
		startApplicationRequest.setBaseAppId(baseAppId);
		startApplicationRequest.setStartMsgExpiryDate(startMsgExpDate);
		
		List<StartApplicationTranslation> startApplicationTranslations = startApplicationRequest.getStartApplicationTranslations();
		if (startApplicationTranslations != null && startApplicationTranslations.size() > 0) {
			for (StartApplicationTranslation startApplicationTranslation : startApplicationTranslations) {
				for (StartAppTranslationTbl startAppTranslationTbl : applicationsTbl.getStartAppTranslationTblCollection()) {
					if (startAppTranslationTbl.getLanguageCode().getLanguageCode()
							.equals(startApplicationTranslation.getLanguageCode())) {
						startApplicationTranslation.setId(startAppTranslationTbl.getStartAppTranslationId());
					}
				}
			}
		}
		
		StartApplicationsTbl startApplicationsTblIn = this.convert2Entity(startApplicationRequest, true);
		StartApplicationsTbl startApplicationsTblOut = this.startApplicationJpaDao.save(startApplicationsTblIn);
		
		LOG.info("<< updateForBatch");
		return startApplicationsTblOut;
	}

}
