/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magna.xmbackend.jpa.dao;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.magna.xmbackend.entities.LiveMessageTbl;

/**
 *
 * @author dhana
 */
@Transactional
public interface LiveMessageJpaDao extends CrudRepository<LiveMessageTbl, String> {

	@Query("from LiveMessageTbl LMT,LiveMessageConfigTbl LMCT  where LMT.liveMessageId=LMCT.liveMessageId and LMCT.siteId=:siteId and LMCT.projectId=:projectId "
			+ "and LMCT.adminAreaId=:adminAreaId and LMCT.userId=:userId")
	List<LiveMessageTbl> findLiveMessages(@Param("siteId") String siteId, @Param("projectId") String projectId, @Param("adminAreaId") String adminAreaId, 
			@Param("userId") String userId);
	
	LiveMessageTbl findByLiveMessageNameIgnoreCase(String liveMessageName);
}
