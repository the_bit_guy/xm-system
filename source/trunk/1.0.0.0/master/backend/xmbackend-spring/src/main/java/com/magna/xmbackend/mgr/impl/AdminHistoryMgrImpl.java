package com.magna.xmbackend.mgr.impl;

import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import com.magna.xmbackend.mgr.AdminHistoryMgr;
import com.magna.xmbackend.utils.DateUtil;
import com.magna.xmbackend.vo.adminHistory.AdminHistoryRequest;

/**
 * The Class AdminHistoryMgrImpl.
 * 
 * @author shashwat.anand
 */
@Component
public class AdminHistoryMgrImpl implements AdminHistoryMgr {
	
	/** The Constant LOG. */
	private static final Logger LOG = LoggerFactory.getLogger(AdminHistoryMgrImpl.class);

	/** The jdbc template. */
	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	@Value("${database.queryLimitKeyword}")
	private String dbLimitKeyword;
	@Autowired
	private DateUtil dateUtil;

	/* (non-Javadoc)
	 * @see com.magna.xmbackend.mgr.AdminHistoryMgr#findAdminBaseObjHistory(com.magna.xmbackend.vo.adminHistory.AdminHistoryRequest)
	 */
	@Override
	public List<Map<String, Object>> findAdminBaseObjHistory(final AdminHistoryRequest adminHistoryRequest) {
		LOG.info(">> findAdminBaseObjHistory");
		List<Map<String, Object>> queryResultSet = getResultSetForBaseObjects(adminHistoryRequest);
		LOG.info("<< findAdminBaseObjHistory");
		return queryResultSet;
	}
	
	/* (non-Javadoc)
	 * @see com.magna.xmbackend.mgr.AdminHistoryMgr#findAdminRelHistory(com.magna.xmbackend.vo.adminHistory.AdminHistoryRequest)
	 */
	@Override
	public List<Map<String, Object>> findAdminRelHistory(final AdminHistoryRequest adminHistoryRequest) {
		LOG.info(">> findAdminRelHistory");
		List<Map<String, Object>> queryResultSet = getResultSetForRelations(adminHistoryRequest);
		LOG.info("<< findAdminRelHistory");
		return queryResultSet;
	}

	/**
	 * Gets the result set.
	 *
	 * @param adminHistoryRequest the admin history request
	 * @param tableName the table name
	 * @return the result set
	 */
	private List<Map<String, Object>> getResultSetForBaseObjects(final AdminHistoryRequest adminHistoryRequest) {
		List<Map<String, Object>> queryResultSet = null;
		final int queryLimit = adminHistoryRequest.getQueryLimit();
		String queryCondition = adminHistoryRequest.getQueryCondition();
		
		//this.dateUtil.alterDbSessionDate();
		String newQueryCondition = null;
		String newQueryCondition2 = null;
		if (queryCondition != null) {
			String firstPattern = "LOG_TIME.*?\'.*?\'";
			Pattern pattern = Pattern.compile(firstPattern);
			Matcher matcher = pattern.matcher(queryCondition);
			int i = 1;
				while (matcher.find()) {
					String logtimeStr = queryCondition.substring(matcher.start(), matcher.end());
					LOG.info(logtimeStr);
					String secondPattern = "\'.*?\'";
					Pattern pattern2 = Pattern.compile(secondPattern);
					Matcher matcher2 = pattern2.matcher(logtimeStr);
					if (matcher2.find()) {
						String logTimeValue = logtimeStr.substring(matcher2.start(), matcher2.end());
						String dbSpecificDate = this.dateUtil.getDbSpecificDate(logTimeValue);
						if(i == 1) {
							newQueryCondition = queryCondition.replace(logTimeValue, dbSpecificDate);
						} else {
							newQueryCondition2 = newQueryCondition.replace(logTimeValue, dbSpecificDate);
						}
					}
					i++;
				}
				if (newQueryCondition == null && newQueryCondition2 == null ) {
					newQueryCondition2 = queryCondition;
				} else if (newQueryCondition != null && newQueryCondition2 == null) {
					newQueryCondition2 = newQueryCondition;
				}
			
		}
		
		final String firstPartQuery = "SELECT * FROM (SELECT * FROM ADMIN_HISTORY_BASE_OBJECTS_TBL ORDER BY LOG_TIME DESC)";
		//final String orderBy = "  ORDER BY LOG_TIME DESC ";
		
		if (newQueryCondition2 != null) {
			newQueryCondition2 = Pattern.compile(Pattern.quote("WHERE"), Pattern.CASE_INSENSITIVE).matcher(queryCondition)
					.find() ? newQueryCondition2 : " WHERE " + newQueryCondition2;
			final String query = firstPartQuery + newQueryCondition2 + "AND "+ dbLimitKeyword +" <= " + queryLimit;
			queryResultSet = jdbcTemplate.queryForList(query);
		} else {
			final String query = firstPartQuery + " WHERE "+ dbLimitKeyword +" <= " + queryLimit;
			queryResultSet = jdbcTemplate.queryForList(query);
		}
		return queryResultSet;
	}
	
	
	private List<Map<String, Object>> getResultSetForRelations(final AdminHistoryRequest adminHistoryRequest) {
		List<Map<String, Object>> queryResultSet = null;
		final int queryLimit = adminHistoryRequest.getQueryLimit();
		String queryCondition = adminHistoryRequest.getQueryCondition();

		//this.dateUtil.alterDbSessionDate();
		String newQueryCondition = null;
		String newQueryCondition2 = null;
		if (queryCondition != null) {
			String firstPattern = "LOG_TIME.*?\'.*?\'";
			Pattern pattern = Pattern.compile(firstPattern);
			Matcher matcher = pattern.matcher(queryCondition);
			int i = 1;
			while (matcher.find()) {
				String logtimeStr = queryCondition.substring(matcher.start(), matcher.end());
				LOG.info(logtimeStr);
				String secondPattern = "\'.*?\'";
				Pattern pattern2 = Pattern.compile(secondPattern);
				Matcher matcher2 = pattern2.matcher(logtimeStr);
				if (matcher2.find()) {
					String logTimeValue = logtimeStr.substring(matcher2.start(), matcher2.end());
					String dbSpecificDate = this.dateUtil.getDbSpecificDate(logTimeValue);
					if (i == 1) {
						newQueryCondition = queryCondition.replace(logTimeValue, dbSpecificDate);
					} else {
						newQueryCondition2 = newQueryCondition.replace(logTimeValue, dbSpecificDate);
					}
				}
				i++;
			}
			if (newQueryCondition == null && newQueryCondition2 == null ) {
				newQueryCondition2 = queryCondition;
			} else if (newQueryCondition != null && newQueryCondition2 == null) {
				newQueryCondition2 = newQueryCondition;
			}
		}

		final String firstPartQuery = "SELECT * FROM (SELECT * FROM ADMIN_HISTORY_RELATIONS_TBL ORDER BY LOG_TIME DESC)";
		// final String orderBy = " ORDER BY LOG_TIME DESC ";

		if (newQueryCondition2 != null) {
			newQueryCondition2 = Pattern.compile(Pattern.quote("WHERE"), Pattern.CASE_INSENSITIVE)
					.matcher(newQueryCondition2).find() ? newQueryCondition2 : " WHERE " + newQueryCondition2;
			final String query = firstPartQuery + newQueryCondition2 + "AND " + dbLimitKeyword + " <= " + queryLimit;
			queryResultSet = jdbcTemplate.queryForList(query);
		} else {
			final String query = firstPartQuery + " WHERE " + dbLimitKeyword + " <= " + queryLimit;
			queryResultSet = jdbcTemplate.queryForList(query);
		}
		return queryResultSet;
	}
}
