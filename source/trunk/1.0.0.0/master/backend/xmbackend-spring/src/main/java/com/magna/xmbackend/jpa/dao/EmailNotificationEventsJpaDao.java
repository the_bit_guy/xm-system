/**
 * 
 */
package com.magna.xmbackend.jpa.dao;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.magna.xmbackend.entities.EmailNotificationEventTbl;

/**
 * @author Bhabadyuti Bal
 *
 */
@Transactional
public interface EmailNotificationEventsJpaDao extends JpaRepository<EmailNotificationEventTbl, String>{

	@Modifying
	@Query("update EmailNotificationEventTbl enet set enet.status = :status where enet.event = :event")
	int updateEventStatus(@Param("status") String status, @Param("event") String event);

	@Query("select enet.status from EmailNotificationEventTbl enet where enet.event=:event")
    String findStatusByEvent(@Param("event") String event);
	
	
	EmailNotificationEventTbl findByEvent(String event);

	EmailNotificationEventTbl findByEventAndStatus(String event, String status);
	
}
