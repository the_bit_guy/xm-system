package com.magna.xmbackend.rel.mgr;

import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import com.magna.xmbackend.entities.AdminAreaProjectRelTbl;
import com.magna.xmbackend.entities.SiteAdminAreaRelTbl;
import com.magna.xmbackend.response.rel.adminareaproject.AdminAreaProjectRelResponseWrapper;
import com.magna.xmbackend.response.rel.adminareaproject.AdminAreaProjectResponse;
import com.magna.xmbackend.response.rel.adminareaproject.AdminAreaProjectResponseWrapper;
import com.magna.xmbackend.vo.permission.ValidationRequest;
import com.magna.xmbackend.vo.rel.AdminAreaProjectRelBatchRequest;
import com.magna.xmbackend.vo.rel.AdminAreaProjectRelBatchResponse;
import com.magna.xmbackend.vo.rel.AdminAreaProjectRelIdWithProjectAppRelResponse;
import com.magna.xmbackend.vo.rel.AdminAreaProjectRelIdWithProjectStartAppRelResponse;
import com.magna.xmbackend.vo.rel.AdminAreaProjectRelRequest;
import com.magna.xmbackend.vo.rel.AdminAreaProjectRelResponse;

/**
 *
 * @author vijay
 */
public interface AdminAreaProjectRelMgr {

    /**
     *
     * @return AdminAreaProjectRelResponse
     */
    AdminAreaProjectRelResponse findAll();

    /**
     *
     * @param id
     * @return AdminAreaProjectRelTbl
     */
    AdminAreaProjectRelTbl findById(String id);

    /**
     *
     * @param adminAreaProjectRelRequest
     * @param userName
     * @return AdminAreaProjAppRelTbl
     */
    AdminAreaProjectRelTbl create(final AdminAreaProjectRelRequest adminAreaProjectRelRequest,
            final String userName);

    /**
     *
     * @param adminAreaProjectRelBatchRequest
     * @param userName
     * @return AdminAreaProjAppRelTbl
     */
    AdminAreaProjectRelBatchResponse createBatch(final AdminAreaProjectRelBatchRequest adminAreaProjectRelBatchRequest,
            final String userName);

    /**
     *
     * @param id
     * @return boolean
     */
    boolean delete(final String id);
    
    
    AdminAreaProjectRelResponse multiDelete(final Set<String> ids, HttpServletRequest httpServletRequest);

    /**
     *
     * @param status
     * @param id
     * @return boolean
     */
    boolean updateStatusById(final String status, final String id, final HttpServletRequest httpServletRequest);

    /**
     *
     * @param siteAdminAreaRelTbl
     * @return AdminAreaProjectRelTbl
     */
    Iterable<AdminAreaProjectRelTbl> findAdminAreaProjectRelTblsBySiteAdminAreaRelId(SiteAdminAreaRelTbl siteAdminAreaRelTbl);

    /**
     *
     * @param id
     * @param validationRequest
     * @return AdminAreaProjectRelIdWithProjectAppRelResponse
     */
    AdminAreaProjectRelIdWithProjectAppRelResponse
            findAllProjectAppsByAdminAreaProjectRelId(final String id,
                    final ValidationRequest validationRequest);

    /**
     *
     * @param id
     * @param relType
     * @param validationRequest
     * @return AdminAreaProjectRelIdWithProjectAppRelResponse
     */
    AdminAreaProjectRelIdWithProjectAppRelResponse
            findAllProjectAppsByAdminAreaProjectRelId(final String id,
                    final String relType,
                    final ValidationRequest validationRequest);

    /**
     *
     * @param id
     * @param validationRequest
     * @return AdminAreaProjectRelIdWithProjectStartAppRelResponse
     */
    AdminAreaProjectRelIdWithProjectStartAppRelResponse
            findAllStartAppsByAdminAreaProjectRelId(final String id,
                    final ValidationRequest validationRequest);

    /**
     * Find by project id.
     *
     * @param projectId the project id
     * @param validationRequest
     * @return the list
     */
    List<AdminAreaProjectResponse> findByProjectId(final String projectId,
            final ValidationRequest validationRequest);

    /**
     *
     * @param siteAdminAreaProjId
     * @param projAppId
     * @param validationRequest
     * @return AdminAreaProjectRelResponseWrapper
     */
    AdminAreaProjectRelResponseWrapper findBySAARProjectAppId(final String siteAdminAreaProjId,
            final String projAppId, final ValidationRequest validationRequest);

    /**
     * Find by site admin area rel id and project id.
     *
     * @param siteAdminAreaRelId the site admin area rel id
     * @param projectId the project id
     * @param validationRequest
     * @return the admin area project rel tbl
     */
    AdminAreaProjectRelTbl findBySiteAdminAreaRelIdAndProjectId(final String siteAdminAreaRelId,
            final String projectId, final ValidationRequest validationRequest);
    
    /**
     * Find by site admin area rel id and project id.
     *
     * @param siteAdminAreaRelId the site admin area rel id
     * @param projectId the project id
     * @return the admin area project rel tbl
     */
    AdminAreaProjectRelTbl findBySiteAdminAreaRelIdAndProjectId(final String siteAdminAreaRelId,
            final String projectId);

    /**
     * 
     * @param adminAreaProjRequests
     * @param httpServletRequest
     * @return AdminAreaProjectResponseWrapper
     */
	AdminAreaProjectResponseWrapper multiUpdate(List<AdminAreaProjectRelRequest> adminAreaProjRequests,
			HttpServletRequest httpServletRequest);
}
