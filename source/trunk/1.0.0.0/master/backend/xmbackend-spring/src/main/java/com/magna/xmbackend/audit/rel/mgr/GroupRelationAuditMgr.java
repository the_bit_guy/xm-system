/**
 * 
 */
package com.magna.xmbackend.audit.rel.mgr;

import javax.servlet.http.HttpServletRequest;

import com.magna.xmbackend.entities.GroupRefTbl;
import com.magna.xmbackend.vo.group.GroupRelBatchRequest;
import com.magna.xmbackend.vo.group.GroupRelBatchResponse;

/**
 * @author Bhabadyuti Bal
 *
 */
public interface GroupRelationAuditMgr {

	void groupRelMultiSaveAuditor(GroupRelBatchRequest groupRelBatchRequest, GroupRelBatchResponse response,
			HttpServletRequest httpServletRequest);

	void groupRelMultiSaveFailureAuditor(HttpServletRequest httpServletRequest, Exception ex);

	void groupRelationMultiDeleteSuccessAudit(GroupRefTbl groupRefTbl, HttpServletRequest httpServletRequest);

}
