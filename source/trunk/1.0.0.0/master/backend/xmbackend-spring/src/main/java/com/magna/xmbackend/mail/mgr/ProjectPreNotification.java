/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magna.xmbackend.mail.mgr;

import com.magna.xmbackend.entities.ProjectsTbl;

/**
 *
 * @author dhana
 */
public interface ProjectPreNotification {

    void postToQueue(ProjectsTbl projectsTbl, String event);
}
