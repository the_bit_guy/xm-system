/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magna.xmbackend.audit.mgr.impl;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import com.magna.xmbackend.audit.mgr.ProjectAuditMgr;
import com.magna.xmbackend.audit.mgr.utils.AdminHistoryBaseObjectUtils;
import com.magna.xmbackend.entities.AdminHistoryBaseObjectsTbl;
import com.magna.xmbackend.entities.ProjectsTbl;
import com.magna.xmbackend.jpa.dao.AdminHistoryBaseObjectsJpaDao;
import com.magna.xmbackend.vo.project.ProjectRequest;

/**
 *
 * @author dhana
 */
@Component
public class ProjectAuditMgrImpl implements ProjectAuditMgr {

    private static final Logger LOG
            = LoggerFactory.getLogger(ProjectAuditMgrImpl.class);
    
    final String OBJECT_NAME = "ProjectTbl";
    final String RESULT_SUCCESS = "Success";
    final String RESULT_FAILURE = "Failure";

    @Autowired
    private AdminHistoryBaseObjectsJpaDao adminHistoryBaseObjectsJpaDao;

    @Autowired
    private AdminHistoryBaseObjectUtils adminHistoryBaseObjectUtils;

    @Override
    @Async("threadPoolTaskExecutor")
    @Transactional
    public void projectCreateAuditor(ProjectsTbl projectsTbl,
            HttpServletRequest servletRequest) {
        LOG.info(">>>> projectCreateAuditor");
        String projectId = projectsTbl.getProjectId();
        String name = projectsTbl.getName();

        LOG.debug("project id {} with name {} created", projectId, name);

        AdminHistoryBaseObjectsTbl ahbot
                = this.adminHistoryBaseObjectUtils.makeAdminHistoryBaseObjectsTbl(servletRequest,
                        "Project Created with name " + name, "", OBJECT_NAME,
                        "project create", "Success");
        this.adminHistoryBaseObjectsJpaDao.save(ahbot);
        LOG.info("<<<< projectCreateAuditor");
    }

    @Override
    public void projectCreateFailAuditor(ProjectRequest projectRequest,
            String errMsg,
            HttpServletRequest servletRequest) {
        LOG.info(">>>> projectCreateFailAuditor");
        AdminHistoryBaseObjectsTbl ahbot
                = this.adminHistoryBaseObjectUtils.makeAdminHistoryBaseObjectsTbl(servletRequest, "", errMsg,
                		OBJECT_NAME + projectRequest.getName(),
                        "project create", "Failure");
        AdminHistoryBaseObjectsTbl ahbotOut
                = this.adminHistoryBaseObjectsJpaDao.save(ahbot);
        LOG.debug("ahbotOut {}", ahbotOut);
        LOG.info("<<<< projectCreateFailAuditor");
    }

    public void projectDeleteFailureAuditor(String projectId,
            String errMsg,
            HttpServletRequest servletRequest) {
        LOG.info(">>>> projectDeleteAuditor");
        AdminHistoryBaseObjectsTbl ahbot
                = this.adminHistoryBaseObjectUtils.makeAdminHistoryBaseObjectsTbl(servletRequest, "",
                        errMsg,
                        OBJECT_NAME,
                        "project delete", "Failure");
        AdminHistoryBaseObjectsTbl ahbotOut
                = this.adminHistoryBaseObjectsJpaDao.save(ahbot);
        LOG.debug("ahbotOut {}", ahbotOut);
        LOG.info("<<<< projectDeleteAuditor");
    }

    @Override
    public void projectDeleteSuccessAuditor(String projectId, String name,
            HttpServletRequest servletRequest) {
        LOG.info(">>>> projectDeleteAuditor");
        AdminHistoryBaseObjectsTbl ahbot
                = this.adminHistoryBaseObjectUtils.makeAdminHistoryBaseObjectsTbl(servletRequest,
                        "Project Deleted with "
                        + " name " + name, "",
                        OBJECT_NAME,
                        "project delete", "Success");
        AdminHistoryBaseObjectsTbl ahbotOut
                = this.adminHistoryBaseObjectsJpaDao.save(ahbot);
        LOG.debug("ahbotOut {}", ahbotOut);
        LOG.info("<<<< projectDeleteAuditor");
    }

    
    @SuppressWarnings("unused")
	private String extractErrMsg(Map<String, String> statusMap) {
        String errMsg = "";
        for (Map.Entry<String, String> entry : statusMap.entrySet()) {
            errMsg = errMsg + entry.getKey() + " : " + entry.getValue() + " ";
        }
        return errMsg;
    }

    @Override
    public void projectUpdateFailAuditor(String projectId, String errMsg, HttpServletRequest servletRequest) {
        LOG.info(">>>> projectUpdateFailAuditor for projectId {}", projectId);
        AdminHistoryBaseObjectsTbl ahbot
                = this.adminHistoryBaseObjectUtils.makeAdminHistoryBaseObjectsTbl(servletRequest,
                        "", errMsg,
                        OBJECT_NAME, "Project Update",
                        "Failure");
        this.adminHistoryBaseObjectsJpaDao.save(ahbot);
        LOG.info("<<<< projectUpdateFailAuditor");
    }
    
    
    @Override
    public void projectCreateFailureBatchAuditor(ProjectRequest projectRequest,
            HttpServletRequest servletRequest) {
        LOG.info(">>>> projectCreateFailAuditor");
        String name = projectRequest.getName();
        String errMsg = "Project with name "+ name +" already exist";
        AdminHistoryBaseObjectsTbl ahbot
                = this.adminHistoryBaseObjectUtils.makeAdminHistoryBaseObjectsTbl(servletRequest, "", errMsg,
                		OBJECT_NAME + projectRequest.getName(),
                        "project create", "Failure");
        AdminHistoryBaseObjectsTbl ahbotOut
                = this.adminHistoryBaseObjectsJpaDao.save(ahbot);
        LOG.debug("ahbotOut {}", ahbotOut);
        LOG.info("<<<< projectCreateFailAuditor");
    }

    @Override
    public void projectUpdateSuccessAuditor(ProjectsTbl projectsTbl, HttpServletRequest servletRequest) {
        String projectId = projectsTbl.getProjectId();
        String name = projectsTbl.getName();
        LOG.info(">>>> projectUpdateSuccessAuditor with projectId {} and name {}", projectId, name);
        String changes = "Project with name " + name + " updated Successfully";
        AdminHistoryBaseObjectsTbl ahbot = this.adminHistoryBaseObjectUtils.makeAdminHistoryBaseObjectsTbl(servletRequest, changes, "", "ProjectTbl", "project update", "Success");
        this.adminHistoryBaseObjectsJpaDao.save(ahbot);
        LOG.info("<<<< projectUpdateSuccessAuditor");
    }

    @Override
    public void projectUpdateStautsSuccessAuditor(String projectId, String projectName, String status, HttpServletRequest servletRequest) {
        LOG.info(">>>> projectUpdateStautsSuccessAuditor for id {} with status {}", projectId, status);
        String changes = "ProjectTbl with name "
                + projectName + "updated with status " + status;
        AdminHistoryBaseObjectsTbl ahbot
                = this.adminHistoryBaseObjectUtils
                        .makeAdminHistoryBaseObjectsTbl(
                                servletRequest, changes, "", OBJECT_NAME,
                                "project update", "Success");
        this.adminHistoryBaseObjectsJpaDao.save(ahbot);
        LOG.info("<<<< projectUpdateStautsSuccessAuditor");
    }

	@Override
	public void projectStatusUpdateSuccessAuditor(String projectName, String status,
			HttpServletRequest httpServletRequest) {
		LOG.info(">>>> projectStatusUpdateSuccessAuditor");
		String errorMsg = "";
		final StringBuilder changeBuff = new StringBuilder();
		changeBuff.append("Project with name ").append(projectName).append(" updated ").append(" with status ")
				.append(status);

		AdminHistoryBaseObjectsTbl ahbot = this.adminHistoryBaseObjectUtils
				.makeAdminHistoryBaseObjectsTbl(
						httpServletRequest, changeBuff.toString(), errorMsg,
						OBJECT_NAME, "Project status update", RESULT_SUCCESS);
		this.adminHistoryBaseObjectsJpaDao.save(ahbot);
		LOG.info("<<<< projectStatusUpdateSuccessAuditor");
		
	}

	@Override
	public void projectStatusUpdateFailureAuditor(String projectName, String status, Exception ex,
			HttpServletRequest httpServletRequest) {
		LOG.info(">>>> projectStatusUpdateFailureAuditor");
		
		String errorMsg = ex.getMessage();
		final StringBuilder changeBuff = new StringBuilder();
		changeBuff.append("Project with name ").append(projectName).append(" updated ").append(" with status ")
				.append(status);

		AdminHistoryBaseObjectsTbl ahbot = this.adminHistoryBaseObjectUtils
				.makeAdminHistoryBaseObjectsTbl(
						httpServletRequest, changeBuff.toString(), errorMsg,
						OBJECT_NAME, "Project status update", RESULT_FAILURE);
		this.adminHistoryBaseObjectsJpaDao.save(ahbot);
		LOG.info("<<<< projectStatusUpdateFailureAuditor");
		
	}

}
