/**
 * 
 */
package com.magna.xmbackend.jpa.dao;

import java.util.Date;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.magna.xmbackend.entities.UserHistoryTbl;

/**
 * @author Bhabadyuti Bal
 *
 */
@Transactional
public interface UserHistoryJpaDao extends CrudRepository<UserHistoryTbl, Long>{

	@Query("select max(logTime) from UserHistoryTbl uht where uht.project=:project and uht.userName=:userName")
	Date findMaxLogTimeByProject(@Param("project") String project, @Param("userName") String username);
	
}
