/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magna.xmbackend.mgr;

import com.magna.xmbackend.entities.IconsTbl;
import com.magna.xmbackend.vo.icon.IkonRequest;
import com.magna.xmbackend.vo.icon.IkonResponse;

/**
 *
 * @author dhana
 */
public interface IconMgr {

    IkonResponse findAll();

    IconsTbl create(IkonRequest ikonRequest);

    IconsTbl update(IkonRequest ikonRequest);

    IconsTbl findByName(String iconName);

    IconsTbl findById(String iconId);

    IkonResponse findByType(String type);

    boolean isIconUsedById(String id);

    boolean isIconUsedByName(String name);
    
    boolean delete(String id);
}
