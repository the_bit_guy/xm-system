package com.magna.xmbackend.mgr.impl;

import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.magna.xmbackend.entities.AdminAreasTbl;
import com.magna.xmbackend.entities.PermissionApiMappingTbl;
import com.magna.xmbackend.entities.PermissionTbl;
import com.magna.xmbackend.entities.RoleAdminAreaRelTbl;
import com.magna.xmbackend.entities.RolePermissionRelTbl;
import com.magna.xmbackend.entities.RoleUserRelTbl;
import com.magna.xmbackend.entities.RolesTbl;
import com.magna.xmbackend.entities.UsersTbl;
import com.magna.xmbackend.exception.XMObjectNotFoundException;
import com.magna.xmbackend.jpa.dao.PermissionApIMappingJpaDao;
import com.magna.xmbackend.jpa.dao.PermissionJpaDao;
import com.magna.xmbackend.jpa.dao.RolePermissionRelJpaDao;
import com.magna.xmbackend.jpa.rel.dao.RoleUserRelJpaDao;
import com.magna.xmbackend.mgr.PermissionMgr;
import com.magna.xmbackend.mgr.RoleMgr;
import com.magna.xmbackend.mgr.UserMgr;
import com.magna.xmbackend.rel.mgr.RoleAdminAreaRelMgr;
import com.magna.xmbackend.utils.PermissionFilter;
import com.magna.xmbackend.vo.enums.PermissionType;
import com.magna.xmbackend.vo.permission.PermissionApiResponse;
import com.magna.xmbackend.vo.permission.PermissionResponse;
import com.magna.xmbackend.vo.permission.ValidationRequest;

/**
 *
 * @author vijay
 */
@Component
public class PermissionMgrImpl implements PermissionMgr {

    private static final Logger LOG
            = LoggerFactory.getLogger(PermissionMgrImpl.class);
    @Autowired
    private PermissionJpaDao permissionJpaDao;

    @Autowired
    private PermissionApIMappingJpaDao permissionApIMappingJpaDao;

    @Autowired
    private RoleMgr roleMgr;

    @Autowired
    RoleAdminAreaRelMgr roleAdminAreaRelMgr;
    
    @Autowired
    RoleUserRelJpaDao roleUserRelJpaDao;

    @Autowired
    private PermissionFilter permissionFilter;
    
    @Autowired
    RolePermissionRelJpaDao rolePermissionRelJpaDao;
    
    @Autowired
    private UserMgr userMgr;

    /**
     *
     * @param roleId
     * @return PermissionResponse
     */
    @Override
    public final PermissionResponse findObjectPermissionByRoleId(final String roleId) {
        PermissionResponse permissionResponse = null;
        LOG.info(">> findObjectPermissionByRoleId");
        final RolesTbl rolesTbl = roleMgr.findById(roleId);
        if (null != rolesTbl) {
            final Iterable<PermissionTbl> permissionTbls = permissionJpaDao.findObjectPermissionByRoleId(rolesTbl);
            if (null != permissionTbls) {
                permissionResponse = new PermissionResponse(permissionTbls);
            }
        } else {
            throw new XMObjectNotFoundException("RoleId not found", "P_ERR0001");
        }
        LOG.info("<< findObjectPermissionByRoleId");
        return permissionResponse;
    }

    /**
     *
     * @param roleId
     * @return PermissionResponse
     */
    @Override
    public final PermissionResponse findObjectRelPermissionByRoleId(final String roleId) {
        PermissionResponse permissionResponse = null;
        LOG.info(">> findObjectPermissionByRoleId");
        final RolesTbl rolesTbl = roleMgr.findById(roleId);
        if (null != rolesTbl) {
            final Iterable<PermissionTbl> permissionTbls = permissionJpaDao.findObjectPermissionByRoleId(rolesTbl);
            if (null != permissionTbls) {
                permissionResponse = new PermissionResponse(permissionTbls);
            }
        } else {
            throw new XMObjectNotFoundException("RoleId not found", "P_ERR0001");
        }
        LOG.info("<< findObjectPermissionByRoleId");
        return permissionResponse;
    }

    /**
     *
     * @return PermissionResponse
     */
    @Override
    public final PermissionResponse findAllObjectPermission() {
        LOG.info(">> findAllObjectPermission");
        final Iterable<PermissionTbl> permissionTbls = permissionJpaDao.findAllObjectPermission();
        final PermissionResponse permissionResponse = new PermissionResponse(permissionTbls);
        LOG.info("<< findAllObjectPermission");
        return permissionResponse;
    }

    /**
     *
     * @return PermissionResponse
     */
    @Override
    public final PermissionResponse findAllObjectRelationPermission() {
        LOG.info(">> findAllObjectRelationPermission");
        final Iterable<PermissionTbl> permissionTbls = permissionJpaDao.findAllObjectRelationPermission();
        final PermissionResponse permissionResponse = new PermissionResponse(permissionTbls);
        LOG.info("<< findAllObjectRelationPermission");
        return permissionResponse;
    }

    /**
     *
     * @return PermissionResponse
     */
    @Override
    public final PermissionResponse findAllAABasedRelationPermission() {
        LOG.info(">> findAllAABasedRelationPermission");
        final Iterable<PermissionTbl> permissionTbls = permissionJpaDao.findAllAABasedRelationPermission();
        final PermissionResponse permissionResponse = new PermissionResponse(permissionTbls);
        LOG.info("<< findAllAABasedRelationPermission");
        return permissionResponse;
    }

    /**
     *
     * @return PermissionResponse
     */
    @Override
    public final PermissionResponse findAll() {
        LOG.info(">> findAll");
        final Iterable<PermissionTbl> permissionTbls = permissionJpaDao.findAll();
        final PermissionResponse permissionResponse = new PermissionResponse(permissionTbls);
        LOG.info("<< findAll");
        return permissionResponse;
    }

    /**
     *
     * @param apiPath
     * @return PermissionResponse
     */
    @Override
    public final boolean isExclusionApi(final String apiPath) {
        boolean isUnauthorized = false;
        LOG.info(">> isExclusionApi");
//        final PermissionTbl permissionTbl = permissionJpaDao.isExclusionApi(apiPath);
//        if (null != permissionTbl && null != permissionTbl.getName()) {
//            LOG.debug("permissionTbl name={}", permissionTbl.getName());
//            isUnauthorized = true;
//        }
        // Retrieve the PERMISSION_API_MAPPING_TBL Key (PERMISSION_API_MAPPING_ID)
        final List<String> apiList = permissionFilter.getRelationApiMapKey(apiPath);
        if (!apiList.isEmpty()) {
            // check whether PERMISSION_API_MAPPING_ID is available permission relation map
            if (permissionFilter.getPerRelationApiMap().containsKey(apiList.get(0))) {
                // If available, retrieve the PERMISSION_ID
                final String permissionId = permissionFilter.getPerRelationApiMap().get(apiList.get(0));
                // check whether the permission id is available in the unathorized map, then return true
                if (permissionFilter.getUnauthorizedPerMap().containsKey(permissionId)) {
                    isUnauthorized = true;
                }
            }
        }
        LOG.info("<< isExclusionApi");
        return isUnauthorized;
    }

    /**
     *
     * @param apiPath
     * @param userName
     * @param adminAreaId
     * @return boolean
     */
    @Override
    public final boolean validateUserApiHavingPermission(final String apiPath,
            final String userName, final String adminAreaId) {
        boolean isAccessAllowed = false;
        PermissionTbl permissionTbl = null;
        LOG.info(">> validateUserApiHavingPermission");
        if (null == adminAreaId) {
            permissionTbl = permissionJpaDao.validateUserApiHavingPermission(apiPath, userName);
        } else {
            final RoleAdminAreaRelTbl roleAdminAreaRelTbl = roleAdminAreaRelMgr.findRoleAdminAreaIdByAAId(adminAreaId);
            if (null != roleAdminAreaRelTbl) {
                permissionTbl = permissionJpaDao.validateUserApiHavingPermissionWithAA(apiPath, userName, roleAdminAreaRelTbl);
            }
        }
        if (null != permissionTbl && null != permissionTbl.getName()) {
            LOG.debug("permissionTbl name={}", permissionTbl.getName());
            final PermissionType permissionType = PermissionType.valueOf(permissionTbl.getPermissionType());
            switch (permissionType) {
                case OBJECT_PERMISSION:
                    final Map<String, String> objectPerMap = permissionFilter.getObjectPerMap();
                    if (objectPerMap.containsKey(permissionTbl.getPermissionId())
                            && objectPerMap.containsValue(permissionTbl.getName())) {
                        isAccessAllowed = true;
                    }
                    break;
                case RELATION_PERMISSION:
                    final Map<String, String> relationPerMap = permissionFilter.getRelationPerMap();
                    if (relationPerMap.containsKey(permissionTbl.getPermissionId())
                            && relationPerMap.containsValue(permissionTbl.getName())) {
                        isAccessAllowed = true;
                    }
                    break;
                case AA_BASED_RELATION_PERMISSION:
                    final Map<String, String> aaPerMap = permissionFilter.getAaBasedPerMap();
                    if (aaPerMap.containsKey(permissionTbl.getPermissionId())
                            && aaPerMap.containsValue(permissionTbl.getName())) {
                        isAccessAllowed = true;
                    }
                    break;
                default:
                    break;
            }
        }
        LOG.info("<< validateUserApiHavingPermission");
        return isAccessAllowed;
    }

    /**
     *
     * @param validationRequest
     * @return PermissionTbl
     */
    @Override
    public PermissionTbl isObjectAccessAllowed(final ValidationRequest validationRequest) {
        LOG.info(">> isObjectAccessAllowed");
        final PermissionTbl permissionTbl
                = permissionJpaDao.isObjectAccessAllowed(validationRequest.getUserName(),
                        validationRequest.getPermissionName());
        LOG.info("<< isObjectAccessAllowed");
        return permissionTbl;
    }

    /**
     *
     * @return PermissionApiResponse
     */
    @Override
    public PermissionApiResponse retrieveAllPermissionApiMappingRelation() {
        LOG.info(">> retrieveAllPermissionApiMappingRelation");
        final Iterable<PermissionApiMappingTbl> permissionApiMappingTbls
                = permissionApIMappingJpaDao.findRelationPermissionApi();
        final PermissionApiResponse permissionApiResponse = new PermissionApiResponse(permissionApiMappingTbls);
        LOG.info("<< retrieveAllPermissionApiMappingRelation");
        return permissionApiResponse;
    }

	@Override
	public boolean isAccessAllowed(String requestPath, String userName, String adminAreaId) {
		PermissionApiMappingTbl permissionApiMappingTbl = this.permissionApIMappingJpaDao.findByApi(requestPath);
		if (permissionApiMappingTbl == null) {
			return false;
		}
		UsersTbl userTbl = this.userMgr.findByName(userName);
		if (userTbl == null) {
			return false;
		}
		PermissionTbl permissionTbl = permissionApiMappingTbl.getPermissionId();
		List<RoleUserRelTbl> roleUserRelTbls = roleUserRelJpaDao.findByUserId(userTbl);
		final PermissionType permissionType = PermissionType.valueOf(permissionTbl.getPermissionType());
		switch (permissionType) {
		case OBJECT_PERMISSION:
		case RELATION_PERMISSION:
			for (RoleUserRelTbl roleUserRelTbl : roleUserRelTbls) {
				RolesTbl rolesTbl = roleUserRelTbl.getRoleId();
				Iterable<RolePermissionRelTbl> rolePermissionRelTbls = rolePermissionRelJpaDao.findRolePermissionRelTblByRoleId(rolesTbl);
				for (RolePermissionRelTbl rolePermissionRelTbl : rolePermissionRelTbls) {
					PermissionTbl permssionTblByRole = rolePermissionRelTbl.getPermissionId();
					if (permissionTbl.getPermissionId().equals(permssionTblByRole.getPermissionId())) {
						return true;
					}
				}
			}
			break;
		case AA_BASED_RELATION_PERMISSION:
			for (RoleUserRelTbl roleUserRelTbl : roleUserRelTbls) {
				RolesTbl rolesTbl = roleUserRelTbl.getRoleId();
				RoleAdminAreaRelTbl roleAdminAreaRelTbl;
				if ((roleAdminAreaRelTbl = roleUserRelTbl.getRoleAdminAreaRelId()) != null) {
					// check for that scope object
					if (null == adminAreaId) {
						return false;
					}
					AdminAreasTbl adminAreasTblByRole = roleAdminAreaRelTbl.getAdminAreaId();
					Iterable<RolePermissionRelTbl> rolePermissionRelTbls = rolePermissionRelJpaDao.findRolePermissionRelTblByRoleId(rolesTbl);
					for (RolePermissionRelTbl rolePermissionRelTbl : rolePermissionRelTbls) {
						PermissionTbl permssionTblByRole = rolePermissionRelTbl.getPermissionId();
						if (permissionTbl.getPermissionId().equals(permssionTblByRole.getPermissionId())) {
							if (adminAreaId.equals(adminAreasTblByRole.getAdminAreaId())) {
								return true;
							}
						}
					}
				} else {
					Iterable<RolePermissionRelTbl> rolePermissionRelTbls = rolePermissionRelJpaDao.findRolePermissionRelTblByRoleId(rolesTbl);
					for (RolePermissionRelTbl rolePermissionRelTbl : rolePermissionRelTbls) {
						PermissionTbl permssionTblByRole = rolePermissionRelTbl.getPermissionId();
						if (permissionTbl.getPermissionId().equals(permssionTblByRole.getPermissionId())) {
							return true;
						}
					}
				}
			}
			break;
		default:
			break;
		}
		return false;
	}
}
