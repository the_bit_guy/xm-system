/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magna.xmbackend.mgr.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Component;

import com.magna.xmbackend.audit.mgr.LiveMessageAuditMgr;
import com.magna.xmbackend.entities.AdminAreasTbl;
import com.magna.xmbackend.entities.LanguagesTbl;
import com.magna.xmbackend.entities.LiveMessageConfigTbl;
import com.magna.xmbackend.entities.LiveMessageStatusTbl;
import com.magna.xmbackend.entities.LiveMessageTbl;
import com.magna.xmbackend.entities.LiveMessageTranslationTbl;
import com.magna.xmbackend.entities.ProjectsTbl;
import com.magna.xmbackend.entities.SitesTbl;
import com.magna.xmbackend.entities.UserTkt;
import com.magna.xmbackend.entities.UsersTbl;
import com.magna.xmbackend.exception.CannotCreateObjectException;
import com.magna.xmbackend.exception.XMObjectNotFoundException;
import com.magna.xmbackend.jpa.dao.AdminAreaJpaDao;
import com.magna.xmbackend.jpa.dao.LiveMessageJpaDao;
import com.magna.xmbackend.jpa.dao.ProjectJpaDao;
import com.magna.xmbackend.jpa.dao.SiteJpaDao;
import com.magna.xmbackend.jpa.dao.UserJpaDao;
import com.magna.xmbackend.jpa.dao.UserTktJpaDao;
import com.magna.xmbackend.jpa.rel.dao.LiveMessageConfigJpaDao;
import com.magna.xmbackend.jpa.rel.dao.LiveMessageStatusJpaDao;
import com.magna.xmbackend.mgr.LiveMessageMgr;
import com.magna.xmbackend.mgr.LiveMessageStatusMgr;
import com.magna.xmbackend.utils.MessageMaker;
import com.magna.xmbackend.vo.enums.Application;
import com.magna.xmbackend.vo.liveMessage.LiveMessageConfigCreateRequest;
import com.magna.xmbackend.vo.liveMessage.LiveMessageConfigResponse;
import com.magna.xmbackend.vo.liveMessage.LiveMessageCreateRequest;
import com.magna.xmbackend.vo.liveMessage.LiveMessageResponse;
import com.magna.xmbackend.vo.liveMessage.LiveMessageResponseWrapper;
import com.magna.xmbackend.vo.liveMessage.LiveMessageStatusRequest;
import com.magna.xmbackend.vo.liveMessage.LiveMessageStatusResponseWrapper;
import com.magna.xmbackend.vo.liveMessage.LiveMessageTranslation;

/**
 *
 * @author dhana
 */
@Component
public class LiveMessageMgrImpl implements LiveMessageMgr {
    
    private static final Logger LOG
            = LoggerFactory.getLogger(LiveMessageMgrImpl.class);
    
    @Autowired
    private LiveMessageJpaDao liveMessageJpaDao;
    @Autowired
    private SiteJpaDao siteJpaDao;
    @Autowired
    private AdminAreaJpaDao adminAreaJpaDao;
    @Autowired
    private UserJpaDao userJpaDao;
    @Autowired
    private ProjectJpaDao projectJpaDao;
    @Autowired
    private LiveMessageConfigJpaDao liveMessageConfigJpaDao;
    @Autowired
    LiveMessageStatusJpaDao liveMessageStatusJpaDao;
    @Autowired
    LiveMessageStatusMgr liveMessageStatusMgr;
    @Autowired
    private MessageMaker messageMaker;
    @Autowired
    UserTktJpaDao userTktJpaDao;
    @Autowired
    LiveMessageAuditMgr liveMessageAuditMgr;
    
    @Override
    public LiveMessageTbl createLiveMessage(
            LiveMessageCreateRequest liveMessageCreateRequest) {
        LOG.info(">> createLiveMessage");
        LiveMessageTbl liveMessageTbl = this.convert2Entity(liveMessageCreateRequest, false);
        LiveMessageTbl liveMessageTblOut
                = liveMessageJpaDao.save(liveMessageTbl);
        LOG.info("<< createLiveMessage");
        return liveMessageTblOut;
    }
    
	private LiveMessageTbl convert2Entity(LiveMessageCreateRequest liveMessageCreateRequest, boolean isUpdate) {
		LiveMessageTbl liveMessageTbl;
		Date startDatetime = liveMessageCreateRequest.getStartDatetime();
		Date endDatetime = liveMessageCreateRequest.getEndDatetime();
		// validate this field in bean validation
		String liveMessageName = liveMessageCreateRequest.getLiveMessageName();
		String ispopup = liveMessageCreateRequest.getIspopup();
		String subject = liveMessageCreateRequest.getSubject();
		String message = liveMessageCreateRequest.getMessage();
		List<LiveMessageTranslation> liveMsgTranslations = liveMessageCreateRequest.getLiveMsgTranslations();
		Date date = new Date();
		if (isUpdate) {
			liveMessageTbl = new LiveMessageTbl(liveMessageCreateRequest.getLiveMessageId(), liveMessageName, subject,
					ispopup, startDatetime, endDatetime, date, date);
			liveMessageTbl.setMessage(message);
		} else {
			if (this.findByLiveMessageName(liveMessageName)) {
				throw new CannotCreateObjectException("LiveMsg already found", "ERR0002");
			}
			liveMessageTbl = new LiveMessageTbl(UUID.randomUUID().toString().toUpperCase(), liveMessageName, subject,
					ispopup, startDatetime, endDatetime, date, date);
			liveMessageTbl.setMessage(message);
		}
		List<LiveMessageTranslationTbl> liveMessageTranslationTbls = new ArrayList<>();
		if (liveMsgTranslations != null) {
			liveMsgTranslations.forEach(lmtt -> {
				String id = UUID.randomUUID().toString().toUpperCase();
				if (isUpdate) {
					id = lmtt.getLiveMsgTransId();
				}
				LiveMessageTranslationTbl liveMessageTranslationTbl = new LiveMessageTranslationTbl(id);
				liveMessageTranslationTbl.setLiveMessageId(liveMessageTbl);
				liveMessageTranslationTbl.setSubject(lmtt.getSubject());
				liveMessageTranslationTbl.setMessage(lmtt.getMessage());
				liveMessageTranslationTbl.setLanguageCode(new LanguagesTbl(lmtt.getLanguageCode()));
				liveMessageTranslationTbl.setCreateDate(date);
				liveMessageTranslationTbl.setUpdateDate(date);
				liveMessageTranslationTbls.add(liveMessageTranslationTbl);
			});
		}

		liveMessageTbl.setLiveMessageTranslationTbls(liveMessageTranslationTbls);
		// }

		Collection<LiveMessageConfigCreateRequest> liveMessageConfigCreateRequests = liveMessageCreateRequest
				.getLiveMessageConfigCreateRequest();
		Collection<LiveMessageConfigTbl> liveMessageConfigTbls = new ArrayList<>();
		if (liveMessageConfigCreateRequests != null) {
			liveMessageConfigCreateRequests.forEach(lmConfig -> {
				String adminAreaId = lmConfig.getAdminAreaId();
				String projectId = lmConfig.getProjectId();
				String siteId = lmConfig.getSiteId();
				String userId = lmConfig.getUserId();
				LiveMessageConfigTbl configTbl = new LiveMessageConfigTbl(UUID.randomUUID().toString(), date, date);

				configTbl.setAdminAreaId(adminAreaId);
				configTbl.setProjectId(projectId);
				configTbl.setSiteId(siteId);
				configTbl.setUserId(userId);
				configTbl.setLiveMessageId(liveMessageTbl);
				liveMessageConfigTbls.add(configTbl);
			});
		}

		liveMessageTbl.setLiveMessageConfigTblCollection(liveMessageConfigTbls);

		return liveMessageTbl;
	}

	private boolean findByLiveMessageName(String liveMessageName) {
		LiveMessageTbl liveMessageTbl = this.liveMessageJpaDao.findByLiveMessageNameIgnoreCase(liveMessageName);
		if(liveMessageTbl != null) {
			return true;
		}
		return false;
	}

	@Override
	public LiveMessageResponseWrapper findAll() {
		LOG.info(">> findAll");
		Iterable<LiveMessageTbl> liveMessagesIterable = this.liveMessageJpaDao.findAll();
		Iterator<LiveMessageTbl> iterator = liveMessagesIterable.iterator();
		List<LiveMessageResponse> liveMessageResponses = new ArrayList<>();
		while(iterator.hasNext()){
			LiveMessageTbl liveMessageTbl = iterator.next();
			LiveMessageResponse liveMessageResponse = new LiveMessageResponse();
			
			liveMessageResponse.setLiveMessageId(liveMessageTbl.getLiveMessageId());
			liveMessageResponse.setLiveMessageName(liveMessageTbl.getLiveMessageName());
			liveMessageResponse.setIspopup(liveMessageTbl.getIspopup());
			liveMessageResponse.setStartDatetime(liveMessageTbl.getStartDatetime());
			liveMessageResponse.setEndDatetime(liveMessageTbl.getEndDatetime());
			liveMessageResponse.setSubject(liveMessageTbl.getSubject());
			liveMessageResponse.setMessage(liveMessageTbl.getMessage());
			
			List<LiveMessageTranslation> messageTranslations = new ArrayList<>();
			Collection<LiveMessageTranslationTbl> liveMessageTranslationTbls = liveMessageTbl.getLiveMessageTranslationTbls();
			liveMessageTranslationTbls.forEach(liveMessageTranslationTbl -> {
				LiveMessageTranslation liveMessageTranslation = new LiveMessageTranslation();
				liveMessageTranslation.setLiveMsgTransId(liveMessageTranslationTbl.getId());
				liveMessageTranslation.setSubject(liveMessageTranslationTbl.getSubject());
				liveMessageTranslation.setMessage(liveMessageTranslationTbl.getMessage());
				liveMessageTranslation.setLanguageCode(liveMessageTranslationTbl.getLanguageCode().getLanguageCode());
				messageTranslations.add(liveMessageTranslation);
			});
			
			List<LiveMessageConfigResponse> configResponses = new ArrayList<>();
			Collection<LiveMessageConfigTbl> liveMessageConfigTbls = liveMessageTbl.getLiveMessageConfigTblCollection();
			liveMessageConfigTbls.forEach(liveMessageConfigTbl -> {
				LiveMessageConfigResponse liveMessageConfigResponse = new LiveMessageConfigResponse();
				liveMessageConfigResponse.setLiveMessageConfigId(liveMessageConfigTbl.getLiveMessageConfigId());
				String siteId = liveMessageConfigTbl.getSiteId();
				if(null != siteId){
					SitesTbl sitesTbl = this.siteJpaDao.findOne(siteId);
					liveMessageConfigResponse.setSitesTbl(sitesTbl);
				}
				
				String adminAreaId = liveMessageConfigTbl.getAdminAreaId();
				if(null != adminAreaId){
					AdminAreasTbl adminAreasTbl = this.adminAreaJpaDao.findOne(adminAreaId);
					liveMessageConfigResponse.setAdminAreasTbl(adminAreasTbl);
				}
				
				String projectId = liveMessageConfigTbl.getProjectId();
				if (null != projectId) {
					ProjectsTbl projectsTbl = this.projectJpaDao.findOne(projectId);
					liveMessageConfigResponse.setProjectsTbl(projectsTbl);
				}
				
				String userId = liveMessageConfigTbl.getUserId();
				if (null != userId) {
					UsersTbl usersTbl = this.userJpaDao.findOne(userId);
					liveMessageConfigResponse.setUsersTbl(usersTbl);
				}
				configResponses.add(liveMessageConfigResponse);
				
			});
			liveMessageResponse.setLiveMessageConfigResponses(configResponses);
			liveMessageResponse.setLiveMessageTranslations(messageTranslations);
			liveMessageResponses.add(liveMessageResponse);
		}
		LiveMessageResponseWrapper liveMessageResponseWrapper = new LiveMessageResponseWrapper(liveMessageResponses);
		LOG.info("<< findAll");
		return liveMessageResponseWrapper;
	}

	@Override
	public boolean updateLiveMessage(LiveMessageCreateRequest liveMessageCreateRequest) {
		LOG.info(">> updateLiveMessage");
		String liveMsgId = liveMessageCreateRequest.getLiveMessageId();
		LiveMessageTbl liveMessageTbl = this.liveMessageJpaDao.findOne(liveMsgId);
		
		if(null != liveMessageTbl){
			//if live msg config & status is already present delete them and recreate.
			List<LiveMessageConfigTbl> liveMessageConfigTbls = this.liveMessageConfigJpaDao.findByLiveMessageId(liveMessageTbl);
			List<LiveMessageStatusTbl> liveMsgStatusTbls = this.liveMessageStatusJpaDao.findByLiveMessageId(liveMessageTbl);
			this.liveMessageConfigJpaDao.delete(liveMessageConfigTbls);
			this.liveMessageStatusJpaDao.delete(liveMsgStatusTbls);
			liveMessageTbl = this.convert2Entity(liveMessageCreateRequest, true);
			liveMessageJpaDao.save(liveMessageTbl);
		} else {
			String[] param = {liveMsgId};
			throw new XMObjectNotFoundException("Live message not found", "LM_ERR0001", param);
		}
		LOG.info(">> updateLiveMessage");
		return true;
	}

	@Override
	public boolean delteById(String liveMsgId) {
		LOG.info(">> deleteByLiveMsgId");
		boolean isDeleted = false;
		try {
			this.liveMessageJpaDao.delete(liveMsgId);
			isDeleted = true;
		} catch (Exception e) {
			if (e instanceof EmptyResultDataAccessException) {
				final String[] param = {liveMsgId};
				throw new XMObjectNotFoundException("User not found", "P_ERR0021", param);
			}
		}

		LOG.info("<< deleteById");
		return isDeleted;
	}
	
	
	@Override
	public LiveMessageResponse multiDelete(Set<String> liveMsgIds, HttpServletRequest hsr) {
		LOG.info(">> multiDelete");
		final List<Map<String, String>> statusMaps = new ArrayList<>();
		liveMsgIds.forEach(liveMsgId -> {
			String name = "";
			try {
				LiveMessageTbl liveMsgTbl = this.liveMessageJpaDao.findOne(liveMsgId);
				if(liveMsgTbl != null) {
					name = liveMsgTbl.getLiveMessageName();
				}	
				this.delteById(liveMsgId);
				this.liveMessageAuditMgr.lmDeleteSuccessAudit(hsr, liveMsgId, name);
			} catch (XMObjectNotFoundException objectNotFound) {
				String errorMsg;
                if (!"".equalsIgnoreCase(liveMsgId)) {
                    errorMsg = "LiveMsg with id " + liveMsgId + " cannot be deleted";
                } else {
                    errorMsg = "LiveMsg with id " + liveMsgId + " and with name "
                            + name + "cannot be deleted";
                }
                this.liveMessageAuditMgr.lmDeleteFailureAudit(hsr, liveMsgId, errorMsg);
				Map<String, String> statusMap = messageMaker.extractFromException(objectNotFound);
				statusMaps.add(statusMap);
			}
		});
		LiveMessageResponse liveMessageResponse = new LiveMessageResponse(statusMaps);
		LOG.info(">> multiDelete");
		return liveMessageResponse;
	}

	@Override
	public LiveMessageStatusResponseWrapper findLiveMessagesByStatus(LiveMessageStatusRequest liveMsgStatusReq, 
			String userName, String tkt) {
		LiveMessageStatusResponseWrapper liveMessageStatusResponseWrapper = null;
		UserTkt userTktTbl = userTktJpaDao.findByTktAndApplicationName(tkt, Application.CAX_START_ADMIN.name());
		if (userTktTbl == null) {
			String adminAreaId = liveMsgStatusReq.getAdminAreaId();
			String projectId = liveMsgStatusReq.getProjectId();
			String siteId = liveMsgStatusReq.getSiteId();
			UsersTbl usersTbl = this.userJpaDao.findByUsernameIgnoreCase(userName);
			liveMsgStatusReq.setUserId(usersTbl.getUserId());
			Set<LiveMessageTbl> liveMessageTbls = new HashSet<>();
			if (null != siteId) { // only site
				if (null != siteId && null != adminAreaId) {// site & admin area
					if (null != siteId && null != adminAreaId && null != projectId) {// site,aa,project
						// execute for site & admin area & project
						List<LiveMessageConfigTbl> liveMsgConfigTbls = this.liveMessageConfigJpaDao.findBySiteIdAndAdminAreaIdAndProjectId(siteId, adminAreaId, projectId);
						liveMessageTbls.addAll(this.getLiveMessages(liveMsgConfigTbls));
					}
					// execute for site & admin area
					List<LiveMessageConfigTbl> liveMsgConfigTbls = this.liveMessageConfigJpaDao.findBySiteIdAndAdminAreaId(siteId, adminAreaId);
					liveMessageTbls.addAll(this.getLiveMessages(liveMsgConfigTbls));
				}
				// execute for site only
				List<LiveMessageConfigTbl> liveMsgConfigTbls = this.liveMessageConfigJpaDao.findBySiteId(siteId);
				liveMessageTbls.addAll(this.getLiveMessages(liveMsgConfigTbls));
			}
			
			//only user
			List<LiveMessageConfigTbl> liveMsgConfigTblsForUser = this.liveMessageConfigJpaDao.findByUserId(usersTbl.getUserId());
			liveMessageTbls.addAll(this.getLiveMessages(liveMsgConfigTblsForUser));
			
			//only project
			List<LiveMessageConfigTbl> liveMsgConfigTblsForProj = this.liveMessageConfigJpaDao.findByProjectId(projectId);
			liveMessageTbls.addAll(this.getLiveMessages(liveMsgConfigTblsForProj));
			/*if (null != siteId && null != adminAreaId) {// site & admin area
				List<LiveMessageConfigTbl> liveMsgConfigTbls = this.liveMessageConfigJpaDao
						.findBySiteIdAndAdminAreaId(siteId, adminAreaId);
				liveMessageTbls.addAll(this.getLiveMessages(liveMsgConfigTbls));
			}
			if (null != siteId && null != adminAreaId && null != projectId) {// site & admin area & project
				List<LiveMessageConfigTbl> liveMsgConfigTbls = this.liveMessageConfigJpaDao
						.findBySiteIdAndAdminAreaIdAndProjectId(siteId, adminAreaId, projectId);
				liveMessageTbls.addAll(this.getLiveMessages(liveMsgConfigTbls));
			}
			if (null != projectId) {// only project
				List<LiveMessageConfigTbl> liveMsgConfigTbls = this.liveMessageConfigJpaDao.findByProjectId(projectId);
				liveMessageTbls.addAll(this.getLiveMessages(liveMsgConfigTbls));
			}
			if (null == projectId && null == siteId && null == adminAreaId) {// only user
				List<LiveMessageConfigTbl> liveMsgConfigTbls = this.liveMessageConfigJpaDao
						.findByUserId(usersTbl.getUserId());
				liveMessageTbls.addAll(this.getLiveMessages(liveMsgConfigTbls));
			}*/

			Set<LiveMessageTbl> liveMessageTblsSet = new HashSet<>();

			liveMessageTbls.forEach(liveMsgTbl -> {
				Date date = new Date();
				Date startDatetime = liveMsgTbl.getStartDatetime();
				Date endDatetime = liveMsgTbl.getEndDatetime();
				if (date.after(startDatetime) && date.before(endDatetime)) {
					LiveMessageStatusTbl liveMessageIdAndUserId = this.liveMessageStatusJpaDao
							.findByLiveMessageIdAndUserId(liveMsgTbl, usersTbl);
					if (liveMessageIdAndUserId == null) {
						liveMessageTblsSet.add(liveMsgTbl);
					}
				}
			});

			liveMessageStatusResponseWrapper = new LiveMessageStatusResponseWrapper(liveMessageTblsSet);
		}

		return liveMessageStatusResponseWrapper;
	}

	
	private List<LiveMessageTbl> getLiveMessages(List<LiveMessageConfigTbl> liveMessageConfigTbls){
		List<LiveMessageTbl> liveMsgTbls = new ArrayList<>();
		liveMessageConfigTbls.forEach(liveMsgConfTbl -> {
				liveMsgTbls.add(liveMsgConfTbl.getLiveMessageId());
		});
		return liveMsgTbls;
	}
	
/*	private List<LiveMessageTbl> getLiveMessages(LiveMessageStatusRequest liveMsgStatusReq, String userId){
		String adminAreaId = liveMsgStatusReq.getAdminAreaId();
		String projectId = liveMsgStatusReq.getProjectId();
		String siteId = liveMsgStatusReq.getSiteId();
		List<LiveMessageConfigTbl> liveMessageConfigTbls = this.liveMessageConfigJpaDao.findBySiteIdAndAdminAreaIdAndProjectIdAndUserId(siteId, adminAreaId, projectId, userId);
		List<LiveMessageTbl> liveMsgTbls = new ArrayList<>();
		liveMessageConfigTbls.forEach(liveMsgConfTbl -> {
				liveMsgTbls.add(liveMsgConfTbl.getLiveMessageId());
		});
		return liveMsgTbls;
	}*/

	@Override
	public boolean updateLiveMessageStatus(LiveMessageStatusRequest liveMessageStatusRequest, String userName, String tkt) {
		LOG.info(">> updateLiveMessageStatus");
		boolean isUpdated = false;
		
		UserTkt userTktTbl = userTktJpaDao.findByTktAndApplicationName(tkt, Application.CAX_START_ADMIN.name());
		if(userTktTbl == null){
			UsersTbl usersTbl = this.userJpaDao.findByUsernameIgnoreCase(userName);
			liveMessageStatusRequest.setUserId(usersTbl.getUserId());
			LiveMessageStatusTbl liveMesgStatusTbl = this.liveMessageStatusMgr.create(liveMessageStatusRequest);
			if(liveMesgStatusTbl != null)
				isUpdated = true;
		}
		
		LOG.info("<< updateLiveMessageStatus");
		return isUpdated;
	}

	@Override
	public LiveMessageTbl findById(String liveMsgId) {
		LiveMessageTbl liveMsgTbl = this.liveMessageJpaDao.findOne(liveMsgId);
		return liveMsgTbl;
	}


	
}
