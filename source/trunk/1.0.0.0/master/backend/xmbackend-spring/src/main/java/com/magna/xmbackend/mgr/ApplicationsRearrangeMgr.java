/**
 * 
 */
package com.magna.xmbackend.mgr;

import com.magna.xmbackend.vo.rearrange.RearrangeApplicationsRequest;
import com.magna.xmbackend.vo.rearrange.RearrangeApplicationsResponse;

// TODO: Auto-generated Javadoc
/**
 * The Interface ApplicationsRearrangeMgr.
 *
 * @author Bhabadyuti Bal
 */
public interface ApplicationsRearrangeMgr {

	
	/**
	 * Creates the or update.
	 *
	 * @param applicationsRequest the applications request
	 * @param userName the user name
	 * @return true, if successful
	 */
	boolean createOrUpdate(RearrangeApplicationsRequest applicationsRequest, String userName);

	/**
	 * Delete.
	 *
	 * @param applicationsRequest the applications request
	 * @param userName the user name
	 * @return true, if successful
	 */
	boolean delete(RearrangeApplicationsRequest applicationsRequest, String userName);

	/**
	 * Find settings.
	 *
	 * @param applicationsRequest the applications request
	 * @param userName the user name
	 * @return the css settings tbl
	 */
	RearrangeApplicationsResponse findSettings(RearrangeApplicationsRequest applicationsRequest, String userName);

}
