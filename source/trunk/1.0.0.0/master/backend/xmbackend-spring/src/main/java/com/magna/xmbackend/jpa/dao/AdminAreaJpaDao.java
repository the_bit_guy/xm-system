/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magna.xmbackend.jpa.dao;


import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import com.magna.xmbackend.entities.AdminAreasTbl;

@Transactional(readOnly = true)
public interface AdminAreaJpaDao extends CrudRepository<AdminAreasTbl, String> {

    @Modifying
    @Query("update AdminAreasTbl aat set aat.status = :status where aat.adminAreaId = :id")
    int setStatusForAdminAreasTbl(@Param("status") String status, @Param("id") String id);

    /**
     * Find by name.
     *
     * @param name the name
     * @return the admin areas tbl
     */
    AdminAreasTbl findByNameIgnoreCase(String name);
}
