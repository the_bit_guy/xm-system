/**
 * 
 */
package com.magna.xmbackend.mgr;

import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import com.magna.xmbackend.entities.GroupsTbl;
import com.magna.xmbackend.vo.group.GroupCreateRequest;
import com.magna.xmbackend.vo.group.GroupResponse;

/**
 * @author Bhabadyuti Bal
 *
 */
public interface GroupsMgr {

	GroupsTbl create(GroupCreateRequest groupCreateRequest);
	
	List<GroupsTbl> findAll();
	
	GroupsTbl update(GroupCreateRequest groupCreateRequest);
	
	boolean deleteById(String groupId);
	
	GroupResponse multiDelete(final Set<String> groupIds, HttpServletRequest httpServletRequest);
	
	List<GroupsTbl> findByGroupType(final String groupType);
	
	GroupsTbl findById(final String id);
}
