/**
 * 
 */
package com.magna.xmbackend.jpa.dao;

import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.data.repository.CrudRepository;

import com.magna.xmbackend.entities.UserLdapAccountStatusTbl;
import com.magna.xmbackend.entities.UsersTbl;

/**
 * @author Bhabadyuti Bal
 *
 */
@Transactional
public interface UserLdapAccountStatusJpaDao extends CrudRepository<UserLdapAccountStatusTbl, String> {

	Optional<UserLdapAccountStatusTbl> findByUserId(UsersTbl userId);
}
