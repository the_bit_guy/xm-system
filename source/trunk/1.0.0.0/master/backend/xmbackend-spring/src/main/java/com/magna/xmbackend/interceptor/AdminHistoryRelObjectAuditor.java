/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magna.xmbackend.interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author dhana
 */
@Component
public class AdminHistoryRelObjectAuditor implements HandlerInterceptor {

    private static final Logger LOG
            = LoggerFactory.getLogger(AdminHistoryRelObjectAuditor.class);

    @Override
    public boolean preHandle(HttpServletRequest hsr, HttpServletResponse hsr1, Object o) throws Exception {
        LOG.info(">>> AdminHistoryRelObjectAuditor > preHandle < <<<");
        return true;
    }

    @Override
    public void postHandle(HttpServletRequest hsr, HttpServletResponse hsr1, Object o, ModelAndView mav) throws Exception {
        LOG.info(">>> AdminHistoryRelObjectAuditor > postHandle < <<<");
    }

    @Override
    public void afterCompletion(HttpServletRequest hsr, HttpServletResponse hsr1, Object o, Exception excptn) throws Exception {
        LOG.info(">>> AdminHistoryRelObjectAuditor > afterCompletion < <<<");
    }

}
