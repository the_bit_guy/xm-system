package com.magna.xmbackend.rel.controller;

import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.magna.xmbackend.audit.rel.mgr.GroupRelationAuditMgr;
import com.magna.xmbackend.rel.mgr.GroupRelMgr;
import com.magna.xmbackend.vo.group.GroupRelBatchRequest;
import com.magna.xmbackend.vo.group.GroupRelBatchResponse;
import com.magna.xmbackend.vo.group.GroupRelResponse;

/**
 * @author Bhabadyuti Bal
 *
 */
@RestController
@RequestMapping(value = "/groupRelation")
public class GroupRelationController {

    private static final Logger LOG = LoggerFactory.getLogger(GroupRelationController.class);

    @Autowired
    GroupRelMgr groupRelMgr;
    @Autowired
    private GroupRelationAuditMgr groupRelationAuditMgr;

    /**
     *
     * @param httpServletRequest
     * @param groupRelBatchRequest
     * @return GroupRelBatchResponse
     */
    @RequestMapping(value = "/multi/save",
            method = RequestMethod.POST,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<GroupRelBatchResponse> batchSave(
            HttpServletRequest httpServletRequest,
            @RequestBody final GroupRelBatchRequest groupRelBatchRequest) {
        LOG.info("> group batchSave");
        final String userName = (String) httpServletRequest.getAttribute("userName");
        LOG.info("userName={}", userName);
        GroupRelBatchResponse response = null;
		try {
			response = groupRelMgr.createBatch(groupRelBatchRequest, userName);
			this.groupRelationAuditMgr.groupRelMultiSaveAuditor(groupRelBatchRequest, response, httpServletRequest);
		} catch (Exception ex) {
			this.groupRelationAuditMgr.groupRelMultiSaveFailureAuditor(httpServletRequest, ex);
		}
        LOG.info("< group batchSave");
        return new ResponseEntity<>(response, HttpStatus.ACCEPTED);
    }

    /**
     *
     * @param httpServletRequest
     * @param groupId
     * @return GroupRelBatchResponse
     */
    @RequestMapping(value = "/findGroupRelsByGroupId/{groupId}",
            method = RequestMethod.GET,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<GroupRelBatchResponse> findGroupRelsByGroupId(
            HttpServletRequest httpServletRequest,
            @PathVariable String groupId) {
        LOG.info("> findGroupRelsByGroupId");
        final String userName = (String) httpServletRequest.getAttribute("userName");
        LOG.info("userName={}", userName);
        final List<GroupRelResponse> groupRelResponses = this.groupRelMgr.findGroupRelsByGroupId(groupId, userName);
        final GroupRelBatchResponse response = new GroupRelBatchResponse(groupRelResponses);
        LOG.info("< findGroupRelsByGroupId");
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    /**
     *
     * @param httpServletRequest
     * @param groupRefIds
     * @return Boolean
     */
    @RequestMapping(value = "/multi/delete",
            method = RequestMethod.POST,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<Boolean> multiDelete(
            HttpServletRequest httpServletRequest,
            @RequestBody final Set<String> groupRefIds) {
        LOG.info("> multiDelete");
        final Boolean isDeleted = this.groupRelMgr.multiDelete(groupRefIds, httpServletRequest);
        LOG.info("< multiDelete");
        return new ResponseEntity<>(isDeleted, HttpStatus.OK);
    }

}
