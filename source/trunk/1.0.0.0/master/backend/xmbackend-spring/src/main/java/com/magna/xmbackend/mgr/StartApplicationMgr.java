package com.magna.xmbackend.mgr;

import java.util.List;
import java.util.Set;

import com.magna.xmbackend.entities.StartApplicationsTbl;
import com.magna.xmbackend.vo.permission.ValidationRequest;
import com.magna.xmbackend.vo.startApplication.StartApplicationRequest;
import com.magna.xmbackend.vo.startApplication.StartApplicationResponse;
import javax.servlet.http.HttpServletRequest;

// TODO: Auto-generated Javadoc
/**
 * The Interface StartApplicationMgr.
 *
 * @author dhana
 */
public interface StartApplicationMgr {

    /**
     * Find all.
     *
     * @param validationRequest the validation request
     * @return StartApplicationResponse
     */
    StartApplicationResponse findAll(final ValidationRequest validationRequest);

    /**
     * Find by id.
     *
     * @param id the id
     * @return StartApplicationsTbl
     */
    StartApplicationsTbl findById(final String id);

    /**
     * Creates the or update.
     *
     * @param startApplicationRequest the start application request
     * @param isUpdate the is update
     * @return StartApplicationsTbl
     */
    StartApplicationsTbl createOrUpdate(final StartApplicationRequest startApplicationRequest,
            final boolean isUpdate);

    /**
     * Delete.
     *
     * @param id the id
     * @return boolean
     */
    boolean delete(final String id);

    /**
     * Multi delete.
     *
     * @param startAppIds the start app ids
     * @param httpServletRequest the http servlet request
     * @return StartApplicationResponse
     */
    StartApplicationResponse multiDelete(final Set<String> startAppIds,
            final HttpServletRequest httpServletRequest);

    /**
     * Update status by id.
     *
     * @param status the status
     * @param id the id
     * @return boolean
     */
    boolean updateStatusById(final String status, final String id);

    /**
     * Find start applications by base app id.
     *
     * @param id the id
     * @param validationRequest the validation request
     * @return StartApplicationResponse
     */
    StartApplicationResponse findStartApplicationsByBaseAppId(final String id,
            final ValidationRequest validationRequest);

    /**
     * Find start applications by user id.
     *
     * @param id the id
     * @return StartApplicationResponse
     */
    StartApplicationResponse findStartApplicationsByUserId(final String id);

    /**
     * Find user start applications by user name.
     *
     * @param userName the user name
     * @param tkt the tkt
     * @param validationRequest the validation request
     * @return StartApplicationResponse
     */
    StartApplicationResponse findUserStartApplicationsByUserName(final String userName, final String tkt,
            final ValidationRequest validationRequest);

    /**
     * Find start app by AA id project id.
     *
     * @param adminAreaId the admin area id
     * @param tkt the tkt
     * @param projectId the project id
     * @param validationRequest the validation request
     * @return StartApplicationResponse
     */
    StartApplicationResponse findStartAppByAAIdProjectId(final String adminAreaId, final String tkt,
            final String projectId, final ValidationRequest validationRequest);

    /**
     * Find start apps by AA id.
     *
     * @param adminAreaId the admin area id
     * @param tkt the tkt
     * @param validationRequest the validation request
     * @return StartApplicationResponse
     */
    StartApplicationResponse findStartAppsByAAId(final String adminAreaId, final String tkt,
            final ValidationRequest validationRequest);

    /**
     * Find start apps by project id.
     *
     * @param projectId the project id
     * @return StartApplicationResponse
     */
    StartApplicationResponse findStartAppsByProjectId(final String projectId);

    /**
     * Find by name.
     *
     * @param name the name
     * @return the start applications tbl
     */
    StartApplicationsTbl findByName(String name);

	/**
	 * Multi update.
	 *
	 * @param startApplicationRequests the start application requests
	 * @param httpServletRequest the http servlet request
	 * @return the start application response
	 */
	StartApplicationResponse multiUpdate(List<StartApplicationRequest> startApplicationRequests,
			HttpServletRequest httpServletRequest);

	/**
	 * Update for batch.
	 *
	 * @param startApplicationRequest the start application request
	 * @return the start applications tbl
	 */
	StartApplicationsTbl updateForBatch(StartApplicationRequest startApplicationRequest);
}
