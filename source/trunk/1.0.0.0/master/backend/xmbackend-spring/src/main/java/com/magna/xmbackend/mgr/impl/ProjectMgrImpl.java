package com.magna.xmbackend.mgr.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Component;

import com.magna.xmbackend.audit.mgr.ProjectAuditMgr;
import com.magna.xmbackend.controller.AuthController;
import com.magna.xmbackend.entities.AdminAreaProjectRelTbl;
import com.magna.xmbackend.entities.AdminAreasTbl;
import com.magna.xmbackend.entities.IconsTbl;
import com.magna.xmbackend.entities.LanguagesTbl;
import com.magna.xmbackend.entities.ProjectApplicationsTbl;
import com.magna.xmbackend.entities.ProjectStartAppRelTbl;
import com.magna.xmbackend.entities.ProjectTranslationTbl;
import com.magna.xmbackend.entities.ProjectsTbl;
import com.magna.xmbackend.entities.SiteAdminAreaRelTbl;
import com.magna.xmbackend.entities.SitesTbl;
import com.magna.xmbackend.entities.StartApplicationsTbl;
import com.magna.xmbackend.entities.UserProjectRelTbl;
import com.magna.xmbackend.entities.UserTkt;
import com.magna.xmbackend.entities.UsersTbl;
import com.magna.xmbackend.exception.CannotCreateObjectException;
import com.magna.xmbackend.exception.XMObjectNotFoundException;
import com.magna.xmbackend.jpa.dao.EmailNotificationEventsJpaDao;
import com.magna.xmbackend.jpa.dao.ProjectJpaDao;
import com.magna.xmbackend.jpa.dao.UserTktJpaDao;
import com.magna.xmbackend.jpa.rel.dao.AdminAreaProjectRelJpaDao;
import com.magna.xmbackend.jpa.rel.dao.SiteAdminAreaRelJpaDao;
import com.magna.xmbackend.mail.mgr.ProjectPreNotification;
import com.magna.xmbackend.mgr.AdminAreaManager;
import com.magna.xmbackend.mgr.ProjectApplicationMgr;
import com.magna.xmbackend.mgr.ProjectMgr;
import com.magna.xmbackend.mgr.SiteMgr;
import com.magna.xmbackend.rel.mgr.AdminAreaProjectRelMgr;
import com.magna.xmbackend.utils.MessageMaker;
import com.magna.xmbackend.utils.Validator;
import com.magna.xmbackend.vo.enums.Application;
import com.magna.xmbackend.vo.enums.NotificationEventType;
import com.magna.xmbackend.vo.enums.Status;
import com.magna.xmbackend.vo.permission.ValidationRequest;
import com.magna.xmbackend.vo.project.ProjectRequest;
import com.magna.xmbackend.vo.project.ProjectResponse;
import com.magna.xmbackend.vo.project.ProjectTranslation;

// TODO: Auto-generated Javadoc
/**
 * The Class ProjectMgrImpl.
 *
 * @author dhana
 */
@Component
public class ProjectMgrImpl implements ProjectMgr {

    /** The Constant LOG. */
    private static final Logger LOG
            = LoggerFactory.getLogger(ProjectMgrImpl.class);
    
    /** The project jpa dao. */
    @Autowired
    private ProjectJpaDao projectJpaDao;

    /** The site mgr. */
    @Autowired
    private SiteMgr siteMgr;

    /** The project app mgr. */
    @Autowired
    private ProjectApplicationMgr projectAppMgr;

    /** The admin area manager. */
    @Autowired
    private AdminAreaManager adminAreaManager;

    /** The site admin area rel jpa dao. */
    @Autowired
    SiteAdminAreaRelJpaDao siteAdminAreaRelJpaDao;

    /** The admin area project rel jpa dao. */
    @Autowired
    AdminAreaProjectRelJpaDao adminAreaProjectRelJpaDao;

    /** The project create pre notification. */
    @Autowired
    private ProjectPreNotification projectCreatePreNotification;

    /** The validator. */
    @Autowired
    private Validator validator;

    /** The message maker. */
    @Autowired
    private MessageMaker messageMaker;
    
    /** The user tkt jpa dao. */
    @Autowired
    UserTktJpaDao userTktJpaDao;

    /** The project audit mgr. */
    @Autowired
    private ProjectAuditMgr projectAuditMgr;
    
    /** The admin area project rel mgr. */
    @Autowired
    private AdminAreaProjectRelMgr adminAreaProjectRelMgr;
    
    @Autowired
    private AuthController authController;
    
    @Autowired
    private EmailNotificationEventsJpaDao eventsJpaDao;

    /**
     * Find all.
     *
     * @param validationRequest the validation request
     * @return ProjectResponse
     */
    @Override
    public ProjectResponse findAll(final ValidationRequest validationRequest) {
        LOG.info(">> findAll");
        Iterable<ProjectsTbl> projectsTbls = this.projectJpaDao.findAll();
        final boolean isViewInactive = validator.isViewInactiveAllowed(validationRequest);
        LOG.info("findAll isViewInactive={}", isViewInactive);
        projectsTbls = validator.filterProjectResponse(isViewInactive, projectsTbls);
        final ProjectResponse projectResponse = new ProjectResponse(projectsTbls);
        LOG.info("<< findAll");
        return projectResponse;
    }

    /**
     * Save.
     *
     * @param projectRequest the project request
     * @return ProjectsTbl
     */
    @Override
    public final ProjectsTbl save(final ProjectRequest projectRequest) {
        LOG.info(">> save");
        final ProjectsTbl ptIn = this.convert2Entity(projectRequest, false);
        final ProjectsTbl ptOut = this.projectJpaDao.save(ptIn);
        //Send mail notification for "PROJECT_CREATE" if enabled
        String status = this.eventsJpaDao.findStatusByEvent(NotificationEventType.PROJECT_CREATE.name());
        if (status.equals(Status.ACTIVE.name())) {
        	this.projectCreatePreNotification.postToQueue(ptOut, NotificationEventType.PROJECT_CREATE.name());
        }
        LOG.info("<< save");
        return ptOut;
    }

    /**
     * Convert 2 entity.
     *
     * @param projectRequest the project request
     * @param isUpdate the is update
     * @return ProjectsTbl
     */
    private ProjectsTbl convert2Entity(final ProjectRequest projectRequest,
            boolean isUpdate) {
        String id = UUID.randomUUID().toString();
        final Date date = new Date();
        if (isUpdate) {
            id = projectRequest.getId();
        }
        final String iconId = projectRequest.getIconId();
        final String status = projectRequest.getStatus();
        final String projectName = projectRequest.getName();
        final List<ProjectTranslation> translations = projectRequest.getProjectTranslations();

        if (!isUpdate && null != this.findProjectIdForName(projectName)) {
            final Map<String, String[]> paramMap
                    = this.messageMaker.getProjectNameWithi18nCode(translations, projectName);
            throw new CannotCreateObjectException(
                    "Project name already found", "ERR0002", paramMap);
        }

        final ProjectsTbl projectsTbl = new ProjectsTbl(id);
        projectsTbl.setIconId(new IconsTbl(iconId));
        projectsTbl.setStatus(status);
        projectsTbl.setName(projectName);

        projectsTbl.setCreateDate(date);
        projectsTbl.setUpdateDate(date);

        final List<ProjectTranslationTbl> ptts = new ArrayList<>();

        if(translations != null ) {
        	for (final ProjectTranslation translation : translations) {
                String pTransId = UUID.randomUUID().toString();
                if (isUpdate) {
                    pTransId = translation.getId();
                }

                final String description = translation.getDescription();
                final String remarks = translation.getRemarks();
                final String languageCode = translation.getLanguageCode();

                final ProjectTranslationTbl ptt = new ProjectTranslationTbl(pTransId, date, date);
                ptt.setDescription(description);
                ptt.setLanguageCode(new LanguagesTbl(languageCode));
                ptt.setRemarks(remarks);
                ptt.setProjectId(new ProjectsTbl(id));
                ptts.add(ptt);
            }
        } else {
        	ProjectTranslationTbl enTranslation = new ProjectTranslationTbl(UUID.randomUUID().toString(), date, date, new LanguagesTbl("en"), projectsTbl);
        	ProjectTranslationTbl geTranslation = new ProjectTranslationTbl(UUID.randomUUID().toString(), date, date, new LanguagesTbl("de"), projectsTbl);
        	ptts.add(enTranslation);
        	ptts.add(geTranslation);
        }
        
        projectsTbl.setProjectTranslationTblCollection(ptts);

        return projectsTbl;
    }

    /**
     * Find project id for name.
     *
     * @param name the name
     * @return the string
     */
    private String findProjectIdForName(final String name) {
        final ProjectsTbl projectsTbl = this.projectJpaDao.findByNameIgnoreCase(name);
        String projectId = null;
        if (projectsTbl != null) {
            projectId = projectsTbl.getProjectId();
        }
        LOG.info(">> findProjectIdForName - Project id for name is {} - {}", name, projectId);
        return projectId;
    }

    /**
     * Find by name.
     *
     * @param name the name
     * @return ProjectsTbl
     */
    @Override
    public final ProjectsTbl findByName(final String name) {
        LOG.info(">> findByName {}", name);
        final ProjectsTbl projectsTbl = this.projectJpaDao.findByNameIgnoreCase(name);
        if (null == projectsTbl) {
            final String[] param = {name};
            throw new XMObjectNotFoundException("Project with name not found", "PR_ERR001", param);
        }
        LOG.info("<< findByName");
        return projectsTbl;
    }

    /**
     * Update.
     *
     * @param projectRequest the project request
     * @return ProjectsTbl
     */
    @Override
    public final ProjectsTbl update(final ProjectRequest projectRequest) {
        LOG.info(">> update");
        final ProjectsTbl ptIn = this.convert2Entity(projectRequest, true);
        final ProjectsTbl ptOut = this.projectJpaDao.save(ptIn);
        LOG.info("<< update");

        return ptOut;
    }
    
    
    @Override
    public final ProjectsTbl updateForBatch(final ProjectRequest projectRequest) {
        LOG.info(">> update");
        
        ProjectsTbl tbl = this.findByName(projectRequest.getName());
        if (tbl == null) {
        	throw new XMObjectNotFoundException("Project not found", "S_ERR0020", new String[]{projectRequest.getName()});
        }
		String status = (projectRequest.getStatus() == null)?(tbl.getStatus()):projectRequest.getStatus();
		String iconId = (projectRequest.getIconId() == null) ? (tbl.getIconId().getIconId())
				: projectRequest.getIconId();
		
		projectRequest.setId(tbl.getProjectId());
		projectRequest.setStatus(status);
		projectRequest.setIconId(iconId);
		
		List<ProjectTranslation> projectTranslations = projectRequest.getProjectTranslations();
		if (projectTranslations != null && projectTranslations.size() > 0) {
			for (ProjectTranslation projectTranslation : projectTranslations) {
				for (ProjectTranslationTbl projectTranslationTbl : tbl.getProjectTranslationTblCollection()) {
					if (projectTranslationTbl.getLanguageCode().getLanguageCode()
							.equals(projectTranslation.getLanguageCode())) {
						projectTranslation.setId(projectTranslationTbl.getProjectTranslationId());
					}
				}
			}
		}
		
		final ProjectsTbl projectsTbl = this.convert2Entity(projectRequest, true);
        final ProjectsTbl projectTbl = this.projectJpaDao.save(projectsTbl);
		return projectTbl;
    }

    /**
     * Find by id.
     *
     * @param id the id
     * @return ProjectsTbl
     */
    @Override
    public final ProjectsTbl findById(final String id) {
        LOG.info(">> findById");
        final ProjectsTbl pt = this.projectJpaDao.findOne(id);
        LOG.info("<< findById");

        return pt;
    }

    /**
     * Delete.
     *
     * @param id the id
     * @return boolean
     */
    @Override
    public final boolean delete(final String id) {
        LOG.info(">> delete {}", id);
        boolean isDeleted = false;
        try {
            //this.removeDirectoryRelations(id);
            ProjectsTbl ptOut = this.findById(id);
            this.projectJpaDao.delete(id);
            isDeleted = true;
            String status = this.eventsJpaDao.findStatusByEvent(NotificationEventType.PROJECT_DELETE.name());
            if (status.equals(Status.ACTIVE.name())) {
            	this.projectCreatePreNotification.postToQueue(ptOut, NotificationEventType.PROJECT_DELETE.name());
            }
            //Send mail notification for "PROJECT_DELETE"
            
        } catch (Exception e) {
            if (e instanceof EmptyResultDataAccessException) {
                final String[] param = {id};
                //throw from here and catch this in multidelete catch block
                throw new XMObjectNotFoundException("Project not found", "P_ERR0011", param);
            }
        }

        LOG.info("<< delete");
        return isDeleted;
    }

    /* (non-Javadoc)
     * @see com.magna.xmbackend.mgr.ProjectMgr#multiDelete(javax.servlet.http.HttpServletRequest, java.util.Set)
     */
    @Override
    public ProjectResponse multiDelete(HttpServletRequest hsr, Set<String> projectIds) {
        LOG.info(">> multiDelete");
        final List<Map<String, String>> statusMaps = new ArrayList<>();
        
        projectIds.forEach(id -> {
        	String name = "";
            try {
                ProjectsTbl projectsTbl2Del = this.projectJpaDao.findByProjectId(id);
                if(projectsTbl2Del != null){
                	name = projectsTbl2Del.getName();
                }    
                this.delete(id);
                this.projectAuditMgr.projectDeleteSuccessAuditor(id, name, hsr);
            } catch (XMObjectNotFoundException objectNotFound) {
                String errorMsg = "project with id " + id + "not found for deletion";
                this.projectAuditMgr.projectDeleteFailureAuditor(id, errorMsg, hsr);
                Map<String, String> statusMap = messageMaker.extractFromException(objectNotFound);
                statusMaps.add(statusMap);
            }
        });
        ProjectResponse response = new ProjectResponse(statusMaps);
        LOG.info(">> multiDelete");
        return response;
    }

    /**
     * Upate status by id.
     *
     * @param status the status
     * @param id the id
     * @return boolean
     */
    @Override
    public final boolean upateStatusById(final String status, final String id) {
        LOG.info(">> updateById {}", id);
        final Date date = new Date();
        boolean isUpdated = false;
        final int out = this.projectJpaDao
                .setStatusAndUpdateDateForProjectsTbl(status, date, id);
        LOG.debug("is Modified status value {}", out);
        if (out > 0) {
            isUpdated = true;
            ProjectsTbl ptOut = this.findById(id);
            final Status statusValue = Status.valueOf(status);
            String event = "";
            switch (statusValue) {
                case ACTIVE:
                    event = "PROJECT_ACTIVATE";
                    break;
                case INACTIVE:
                    event = "PROJECT_DEACTIVATE";
                    break;
                default:
                    break;
            }

            //Send mail notification for "PROJECT_DEACTIVATE or PROJECT_ACTIVATE"
            String staatus = this.eventsJpaDao.findStatusByEvent(event);
            if (staatus.equals(Status.ACTIVE.name())) {
            	this.projectCreatePreNotification.postToQueue(ptOut, event);
            }
            

        }
        LOG.info("<< updateById");
        return isUpdated;
    }

    /**
     * Find projects by user id.
     *
     * @param userId the user id
     * @param validationRequest the validation request
     * @return ProjectResponse
     */
    @Override
    public final ProjectResponse findProjectsByUserId(final String userId,
            final ValidationRequest validationRequest) {
        LOG.info(">> findProjectsByUserId {}", userId);
        ProjectResponse projectResponse = null;
        final boolean isViewInactive = validator.isViewInactiveAllowed(validationRequest);
        LOG.info("findProjectsByUserId isViewInactive={}", isViewInactive);
        final UsersTbl usersTbl = validator.validateUser(userId, isViewInactive);
        Collection<UserProjectRelTbl> userProjectRelTbls = usersTbl.getUserProjectRelTblCollection();
        userProjectRelTbls = validator.filterUserProjectRel(isViewInactive, userProjectRelTbls);
        if (!userProjectRelTbls.isEmpty()) {
            final Collection<ProjectsTbl> projectsTbls = new ArrayList<>();
            for (final UserProjectRelTbl userProjectRelTbl : userProjectRelTbls) {
                ProjectsTbl projectsTbl = userProjectRelTbl.getProjectId();
                projectsTbl = validator.filterProjectResponse(isViewInactive, projectsTbl);
                if (null != projectsTbl && null != projectsTbl.getProjectId()) {
                    projectsTbls.add(projectsTbl);
                }
            }
            projectResponse = new ProjectResponse(projectsTbls);
        } else {
            throw new XMObjectNotFoundException("No User Project relationship found for the user id", "P_ERR0007");
        }
        LOG.info("<< findProjectsByUserId");
        return projectResponse;
    }

    /**
     * Find projects by start app id.
     *
     * @param startAppId the start app id
     * @param validationRequest the validation request
     * @return ProjectResponse
     */
    @Override
    public final ProjectResponse findProjectsByStartAppId(final String startAppId,
            final ValidationRequest validationRequest) {
        ProjectResponse projectResponse = null;
        LOG.info(">> findProjectsByStartAppId {}", startAppId);
        final boolean isViewInactive = validator.isViewInactiveAllowed(validationRequest);
        LOG.info("findProjectsByStartAppId isViewInactive={}", isViewInactive);
        final StartApplicationsTbl startApplicationsTbl = validator.validateStartApp(startAppId, isViewInactive);
        Collection<ProjectStartAppRelTbl> projectStartAppRelTbl = startApplicationsTbl.getProjectStartAppRelTblCollection();
        if (!projectStartAppRelTbl.isEmpty()) {
            final Collection<ProjectsTbl> projectsTbls = new ArrayList<>();
            // Filter project start app by isviewinactive flag
            projectStartAppRelTbl = validator.filterProjectStartAppRel(isViewInactive, projectStartAppRelTbl);
            for (ProjectStartAppRelTbl projectStartAppRelTbl1 : projectStartAppRelTbl) {
                AdminAreaProjectRelTbl adminAreaProjectRelTbl = projectStartAppRelTbl1.getAdminAreaProjectRelId();
                // Filter admin area project by isviewinactive flag
                adminAreaProjectRelTbl = validator.filterAdminAreaProjectRel(isViewInactive, adminAreaProjectRelTbl);
                if (null != adminAreaProjectRelTbl) {
                    ProjectsTbl projectsTbl = adminAreaProjectRelTbl.getProjectId();
                    projectsTbl = validator.filterProjectResponse(isViewInactive, projectsTbl);
                    if (null != projectsTbl && null != projectsTbl.getProjectId()) {
                        projectsTbls.add(projectsTbl);
                    }
                }
            }
            if (projectsTbls.isEmpty()) {
                throw new XMObjectNotFoundException("No Admin Area Project relationship found for the StartAppId", "P_ERR0004");
            }
            projectResponse = new ProjectResponse(projectsTbls);
        } else {
            throw new XMObjectNotFoundException("No Project Start App relationship found for the Start App", "P_ERR0005");
        }

        LOG.info("<< findProjectsByStartAppId ");
        return projectResponse;
    }

    /**
     * Find all projects by project app site AA id.
     *
     * @author shashwat.anand
     * @param projectAppId the project app id
     * @param siteId the site id
     * @param adminAreaId the admin area id
     * @return the project response
     */
    @Override
    public final ProjectResponse findAllProjectsByProjectAppSiteAAId(final String projectAppId,
            final String siteId, final String adminAreaId) {
        LOG.info(">> findAllProjectsByProjectAppSiteAAId");
        final SitesTbl sitesTbl = this.siteMgr.findById(siteId);
        if (null == sitesTbl) {
            throw new XMObjectNotFoundException("Site not found", "P_ERR0001");
        }
        final AdminAreasTbl adminAreasTbl = adminAreaManager.findById(adminAreaId);
        if (null == adminAreasTbl) {
            throw new XMObjectNotFoundException("Admin Area not found", "P_ERR0002");
        }
        final ProjectApplicationsTbl projectApplicationsTbl = this.projectAppMgr.findById(projectAppId);
        if (null == projectApplicationsTbl) {
            throw new XMObjectNotFoundException("Project Application not found", "P_ERR0003");
        }
        final Iterable<ProjectsTbl> projectsTbls
                = projectJpaDao.findProjectTblBySiteAAProjAppId(sitesTbl, adminAreasTbl, projectApplicationsTbl);
        final ProjectResponse projectResponse = new ProjectResponse(projectsTbls);
        LOG.info("<< findAllProjectsByProjectAppSiteAAId");
        return projectResponse;
    }

    /**
     * Find all projects by AA id.
     *
     * @param aaId the aa id
     * @param validationRequest the validation request
     * @return ProjectResponse
     */
    @Override
    public final ProjectResponse findAllProjectsByAAId(final String aaId, final ValidationRequest validationRequest) {
        final boolean isViewInactive = validator.isViewInactiveAllowed(validationRequest);
        LOG.info("findAllProjectsByAAId isViewInactive={}", isViewInactive);
        final AdminAreasTbl adminAreasTbl = validator.validateAdminArea(aaId, isViewInactive);
        List<SiteAdminAreaRelTbl> siteAdminAreaRelTbls = this.siteAdminAreaRelJpaDao.findByAdminAreaId(adminAreasTbl);
        siteAdminAreaRelTbls = validator.filterSiteAdminAreaRel(isViewInactive, siteAdminAreaRelTbls);
        if (siteAdminAreaRelTbls.isEmpty()) {
            throw new XMObjectNotFoundException("Site Admin Area Relation not found", "AA_ERR0016");
        }
        final List<SiteAdminAreaRelTbl> filterdSiteAdminAreaRelTbls = new ArrayList<>();
        for (final SiteAdminAreaRelTbl siteAdminAreaRelTbl : siteAdminAreaRelTbls) {
            SitesTbl siteTbl = validator.filterSiteResponse(isViewInactive, siteAdminAreaRelTbl.getSiteId());
            if (null != siteTbl) {
                filterdSiteAdminAreaRelTbls.add(siteAdminAreaRelTbl);
            }
        }
        final List<AdminAreaProjectRelTbl> adminAreaProjectRelTbls = this.adminAreaProjectRelJpaDao.findBySiteAdminAreaRelIdIn(filterdSiteAdminAreaRelTbls);
        if (adminAreaProjectRelTbls.isEmpty()) {
            throw new XMObjectNotFoundException("Projects not found", "S_ERR0007");
        }
        List<AdminAreaProjectRelTbl> filteredAdminAreaProjectRel = validator.filterAdminAreaProjectRel(isViewInactive, adminAreaProjectRelTbls);
        final List<ProjectsTbl> projectsTbls = new ArrayList<>();
        for (final AdminAreaProjectRelTbl aaprtbl : filteredAdminAreaProjectRel) {
            ProjectsTbl projectsTbl = aaprtbl.getProjectId();
            projectsTbl = validator.filterProjectResponse(isViewInactive, projectsTbl);
            if (null != projectsTbl && null != projectsTbl.getProjectId()) {
                projectsTbls.add(aaprtbl.getProjectId());
            }
        }
        if (projectsTbls.isEmpty()) {
            throw new XMObjectNotFoundException("No Projects found", "S_ERR0019");
        }
        final ProjectResponse response = new ProjectResponse(projectsTbls);
        LOG.info("<< findAllProjectsByAAId");
        return response;
    }

    /**
     * Find all projects by AA id and active.
     *
     * @param aaId the aa id
     * @param validationRequest the validation request
     * @return ProjectResponse
     */
    @Override
    public final ProjectResponse findAllProjectsByAAIdAndActive(final String aaId,
            final ValidationRequest validationRequest) {
        LOG.info(">> findAllProjectsByAAIdAndActive");
        final boolean isViewInactive = validator.isViewInactiveAllowed(validationRequest);
        LOG.info("findAllProjectsByAAIdAndActive isViewInactive={}", isViewInactive);
        final AdminAreasTbl adminAreasTbl = validator.validateAdminArea(aaId, isViewInactive);
        List<SiteAdminAreaRelTbl> adminAreaRelTbls = this.siteAdminAreaRelJpaDao.findByAdminAreaId(adminAreasTbl);
        adminAreaRelTbls = validator.filterSiteAdminAreaRel(isViewInactive, adminAreaRelTbls);
        if (adminAreaRelTbls.isEmpty()) {
            throw new XMObjectNotFoundException("Site Admin Area Relation not found", "AA_ERR0016");
        }
        final List<AdminAreaProjectRelTbl> adminAreaProjectRelTbls
                = this.adminAreaProjectRelJpaDao.findBySiteAdminAreaRelIdInAndStatus(adminAreaRelTbls, "ACTIVE");
        if (adminAreaProjectRelTbls.isEmpty()) {
            throw new XMObjectNotFoundException("No Admin Area Project relationship found for the Site Admin Area", "S_ERR0005");
        }
        final List<ProjectsTbl> projectsTbls = new ArrayList<>();
        for (final AdminAreaProjectRelTbl aaprtbl : adminAreaProjectRelTbls) {
            ProjectsTbl projectsTbl = aaprtbl.getProjectId();
            projectsTbl = validator.filterProjectResponse(isViewInactive, projectsTbl);
            if (null != projectsTbl && null != projectsTbl.getProjectId()) {
                projectsTbls.add(aaprtbl.getProjectId());
            }
        }
        if (projectsTbls.isEmpty()) {
            throw new XMObjectNotFoundException("No Projects found", "S_ERR0019");
        }
        final ProjectResponse response = new ProjectResponse(projectsTbls);
        LOG.info("<< findAllProjectsByAAIdAndActive");
        return response;
    }

    /**
     * Find user projects by site id and username.
     *
     * @param siteId the site id
     * @param tkt the tkt
     * @param userName the user name
     * @param validationRequest the validation request
     * @return ProjectResponse
     */
    @Override
    public ProjectResponse findUserProjectsBySiteIdAndUsername(final String siteId, final String tkt,
            final String userName, final ValidationRequest validationRequest) {
        LOG.info(">> findUserProjectsBySiteIdAndUsername");
        
        List<String> status = new ArrayList<>();
		status.add(Status.ACTIVE.name());
		boolean isViewInactive = validator.isViewInactiveAllowed(validationRequest);
		LOG.info("findStartAppsByAAId isViewInactive={}", isViewInactive);
		if (!isViewInactive) {
			final UserTkt userTktTbl = this.userTktJpaDao.findByTktAndApplicationName(this.authController.hash(tkt),
					Application.CAX_ADMIN_MENU.name());
			if (userTktTbl != null) {
				validationRequest.setUserName(userTktTbl.getUsername());
				isViewInactive = validator.isViewInactiveAllowed(validationRequest);
			}
		}

		final List<ProjectsTbl> projectsTbls = new ArrayList<>();
		List<ProjectsTbl> activeProjectsTbls = this.projectJpaDao.findProjectsTbls(siteId, userName, status);

		if(activeProjectsTbls != null) {
			projectsTbls.addAll(activeProjectsTbls);
		}
		
		if (isViewInactive) {
			status.add(Status.INACTIVE.name());
			List<ProjectsTbl> activeInactiveProjectsTbls = this.projectJpaDao.findProjectsTbls(siteId, userName, status);
			if (activeInactiveProjectsTbls != null && projectsTbls.size() != activeInactiveProjectsTbls.size()) {
				for (ProjectsTbl activeInactiveProject : activeInactiveProjectsTbls) {
					if (!projectsTbls.contains(activeInactiveProject)) {
						activeInactiveProject.setStatus(Status.INACTIVE.name());
						projectsTbls.add(activeInactiveProject);
					}
				}				
			} 
		}
        
        if (projectsTbls.isEmpty()) {
            throw new XMObjectNotFoundException("No Projects Found", "S_ERR0019");
        }
        final ProjectResponse response = new ProjectResponse(projectsTbls);
        LOG.info("<< findUserProjectsBySiteIdAndUsername");
        return response;
    }

	/* (non-Javadoc)
	 * @see com.magna.xmbackend.mgr.ProjectMgr#multiUpdate(java.util.List, javax.servlet.http.HttpServletRequest)
	 */
	@Override
	public ProjectResponse multiUpdate(List<ProjectRequest> projectRequests, HttpServletRequest httpServletRequest) {
		LOG.info(">> multiUpdate");
		List<String> projNames = new ArrayList<>();
		projectRequests.forEach(projectRequest -> {
			String projectName = projectRequest.getName();
			String status = projectRequest.getStatus();
			try {
				boolean upateStatusById = this.upateStatusById(status, projectRequest.getId());
				if (upateStatusById) {
					// success & audit it
					this.projectAuditMgr.projectStatusUpdateSuccessAuditor(projectName, status, httpServletRequest);
				} else {
					projNames.add(projectName);
				}
			} catch (Exception ex) {
				this.projectAuditMgr.projectStatusUpdateFailureAuditor(projectName, status, ex, httpServletRequest);
			}

		});
		ProjectResponse projectResponse = new ProjectResponse();
		projectResponse.setStatusUpdatationFailedList(projNames);
		LOG.info("<< multiUpdate");
		return projectResponse;
	}

	/* (non-Javadoc)
	 * @see com.magna.xmbackend.mgr.ProjectMgr#findProjectsBySiteId(java.lang.String, com.magna.xmbackend.vo.permission.ValidationRequest)
	 */
	@Override
	public ProjectResponse findProjectsBySiteId(String id, ValidationRequest validationRequest) {
		ProjectResponse projectResponse = null;
        LOG.info(">> findProjectById {}", id);
        final boolean isViewInactive = validator.isViewInactiveAllowed(validationRequest);
        LOG.info("findProjectById isViewInactive={}", isViewInactive);
        final SitesTbl sitesTbl = validator.validateSite(id, isViewInactive);
        if (null != sitesTbl) {
            projectResponse = this.getProjects(sitesTbl, isViewInactive);
        } else {
            final String[] param = {id};
            throw new XMObjectNotFoundException("Given SiteId not found", "S_ERR0002", param);
        }
        LOG.info("<< findProjectById");
        return projectResponse;
	}

	/**
	 * Gets the projects.
	 *
	 * @param sitesTbl the sites tbl
	 * @param isViewInactive the is view inactive
	 * @return the projects
	 */
	private ProjectResponse getProjects(SitesTbl sitesTbl, boolean isViewInactive) {
		ProjectResponse projectResponse;
		String siteStatus = sitesTbl.getStatus();
		Collection<SiteAdminAreaRelTbl> siteAdminAreaRelTbls = sitesTbl.getSiteAdminAreaRelTblCollection();
		if (siteAdminAreaRelTbls.isEmpty()) {
			throw new XMObjectNotFoundException("Admin Areas not available for site", "S_ERR0003");
		}
		// Filter site admin area relation by isViewInactive allowed
		siteAdminAreaRelTbls = validator.filterSiteAdminAreaRel(isViewInactive, siteAdminAreaRelTbls);
		final List<ProjectsTbl> projectsTbls = new ArrayList<>();
		for (SiteAdminAreaRelTbl siteAdminAreaRelTbl : siteAdminAreaRelTbls) {
			String siteAAStatus = siteAdminAreaRelTbl.getStatus();
			AdminAreasTbl filterAdminAreaTbl = validator.filterAdminAreaResponse(isViewInactive,
					siteAdminAreaRelTbl.getAdminAreaId());
			if (null != filterAdminAreaTbl) {
				Iterable<AdminAreaProjectRelTbl> adminAreaProjectRelTbls = adminAreaProjectRelMgr
						.findAdminAreaProjectRelTblsBySiteAdminAreaRelId(siteAdminAreaRelTbl);
				if (null != adminAreaProjectRelTbls) {
					// Filter admin area project relation by isViewInactive
					// allowed
					adminAreaProjectRelTbls = validator.filterAdminAreaProjectRel(isViewInactive,
							adminAreaProjectRelTbls);
					for (AdminAreaProjectRelTbl adminAreaProjectRelTbl : adminAreaProjectRelTbls) {
						ProjectsTbl projectsTbl = adminAreaProjectRelTbl.getProjectId();
						if (siteStatus.equals(Status.INACTIVE.name()) || siteAAStatus.equals(Status.INACTIVE.name())
								|| adminAreaProjectRelTbl.getStatus().equals(Status.INACTIVE.name())) {
							projectsTbl.setStatus(Status.INACTIVE.name());
						}
						LOG.info("projectsTbl={}", projectsTbl.getProjectId());
						projectsTbl = validator.filterProjectResponse(isViewInactive, projectsTbl);
						if (null != projectsTbl && null != projectsTbl.getProjectId()) {
							projectsTbls.add(projectsTbl);
						}
					}
				}
			}
		}
		if (projectsTbls.isEmpty()) {
			throw new XMObjectNotFoundException("No project relationship found for the site", "S_ERR0004");
		}
		projectResponse = new ProjectResponse(projectsTbls);
		return projectResponse;
	}

	/* (non-Javadoc)
	 * @see com.magna.xmbackend.mgr.ProjectMgr#findProjectsByAdminAreaId(java.lang.String, com.magna.xmbackend.vo.permission.ValidationRequest)
	 */
	@Override
	public ProjectResponse findProjectsByAdminAreaId(String adminAreaId, ValidationRequest validationRequest) {
        final boolean isViewInactive = validator.isViewInactiveAllowed(validationRequest);
        LOG.info("findAllProjectsByAAId isViewInactive={}", isViewInactive);
        final AdminAreasTbl adminAreasTbl = validator.validateAdminArea(adminAreaId, isViewInactive);
        List<ProjectsTbl> projectsTbls = getProjectsByAdminarea(adminAreasTbl, isViewInactive);
        
        if (projectsTbls.isEmpty()) {
            throw new XMObjectNotFoundException("No Projects found", "S_ERR0019");
        }
        final ProjectResponse response = new ProjectResponse(projectsTbls);
        LOG.info("<< findAllProjectsByAAId");
        return response;
    }

	
	/**
	 * Gets the projects by adminarea.
	 *
	 * @param adminAreasTbl the admin areas tbl
	 * @param isViewInactive the is view inactive
	 * @return the projects by adminarea
	 */
	private List<ProjectsTbl> getProjectsByAdminarea(final AdminAreasTbl adminAreasTbl, final boolean isViewInactive) {
		String adminAreaStatus = adminAreasTbl.getStatus();
		List<SiteAdminAreaRelTbl> siteAdminAreaRelTbls = this.siteAdminAreaRelJpaDao.findByAdminAreaId(adminAreasTbl);
        siteAdminAreaRelTbls = validator.filterSiteAdminAreaRel(isViewInactive, siteAdminAreaRelTbls);
        if (siteAdminAreaRelTbls.isEmpty()) {
            throw new XMObjectNotFoundException("Site Admin Area Relation not found", "AA_ERR0016");
        }
        final List<SiteAdminAreaRelTbl> filterdSiteAdminAreaRelTbls = new ArrayList<>();
        for (final SiteAdminAreaRelTbl siteAdminAreaRelTbl : siteAdminAreaRelTbls) {
            SitesTbl siteTbl = validator.filterSiteResponse(isViewInactive, siteAdminAreaRelTbl.getSiteId());
            if (null != siteTbl) {
                filterdSiteAdminAreaRelTbls.add(siteAdminAreaRelTbl);
            }
        }
        final List<AdminAreaProjectRelTbl> adminAreaProjectRelTbls = this.adminAreaProjectRelJpaDao.findBySiteAdminAreaRelIdIn(filterdSiteAdminAreaRelTbls);
        if (adminAreaProjectRelTbls.isEmpty()) {
            throw new XMObjectNotFoundException("Projects not found", "S_ERR0007");
        }
        List<AdminAreaProjectRelTbl> filteredAdminAreaProjectRel = validator.filterAdminAreaProjectRel(isViewInactive, adminAreaProjectRelTbls);
        final List<ProjectsTbl> projectsTbls = new ArrayList<>();
        for (final AdminAreaProjectRelTbl aaprtbl : filteredAdminAreaProjectRel) {
            ProjectsTbl projectsTbl = aaprtbl.getProjectId();
            projectsTbl = validator.filterProjectResponse(isViewInactive, projectsTbl);
            if(adminAreaStatus.equals(Status.INACTIVE.name()) 
            		|| aaprtbl.getStatus().equals(Status.INACTIVE.name())
            		|| aaprtbl.getSiteAdminAreaRelId().getSiteId().getStatus().equals(Status.INACTIVE.name())
            		|| aaprtbl.getSiteAdminAreaRelId().getStatus().equals(Status.INACTIVE.name())) {
            	
            	projectsTbl.setStatus(Status.INACTIVE.name());
            }
            if (null != projectsTbl && null != projectsTbl.getProjectId()) {
                projectsTbls.add(aaprtbl.getProjectId());
            }
        }
        return projectsTbls;
	}

	/* (non-Javadoc)
	 * @see com.magna.xmbackend.mgr.ProjectMgr#findAllProjectsByUserId(java.lang.String, com.magna.xmbackend.vo.permission.ValidationRequest)
	 */
	@Override
	public ProjectResponse findAllProjectsByUserId(String userId, ValidationRequest validationRequest) {
		LOG.info(">> findProjectsByUserId {}", userId);
		ProjectResponse projectResponse = null;
		final boolean isViewInactive = validator.isViewInactiveAllowed(validationRequest);
		LOG.info("findProjectsByUserId isViewInactive={}", isViewInactive);
		final UsersTbl usersTbl = validator.validateUser(userId, isViewInactive);
		String userStatus = usersTbl.getStatus();
		Collection<UserProjectRelTbl> userProjectRelTbls = usersTbl.getUserProjectRelTblCollection();
		userProjectRelTbls = validator.filterUserProjectRel(isViewInactive, userProjectRelTbls);
		if (!userProjectRelTbls.isEmpty()) {
			final Collection<ProjectsTbl> projectsTbls = new ArrayList<>();
			for (final UserProjectRelTbl userProjectRelTbl : userProjectRelTbls) {
				ProjectsTbl projectsTbl = userProjectRelTbl.getProjectId();
				projectsTbl = validator.filterProjectResponse(isViewInactive, projectsTbl);
				if (userStatus.equals(Status.INACTIVE.name())
						|| userProjectRelTbl.getStatus().equals(Status.INACTIVE.name())) {
					projectsTbl.setStatus(Status.INACTIVE.name());
				}
				if (null != projectsTbl && null != projectsTbl.getProjectId()) {
					projectsTbls.add(projectsTbl);
				}
			}
			projectResponse = new ProjectResponse(projectsTbls);
		} else {
			throw new XMObjectNotFoundException("No User Project relationship found for the user id", "P_ERR0007");
		}
		LOG.info("<< findProjectsByUserId");
		return projectResponse;
	}
	
	
	@Override
	public ProjectResponse findProjectsByUserIdAndSiteId(String userId, String siteId,
			ValidationRequest validationRequest) {
		LOG.info(">> findProjectsByUserIdAndSiteId");
		ProjectResponse projectResponse = null;
		final boolean isViewInactive = validator.isViewInactiveAllowed(validationRequest);
		LOG.info("findProjectsByUserId isViewInactive={}", isViewInactive);
		final UsersTbl usersTbl = validator.validateUser(userId, isViewInactive);
		final SitesTbl sitesTbl = validator.validateSite(siteId, isViewInactive);
		String userStatus = usersTbl.getStatus();
		Collection<UserProjectRelTbl> userProjectRelTbls = usersTbl.getUserProjectRelTblCollection();
		userProjectRelTbls = validator.filterUserProjectRel(isViewInactive, userProjectRelTbls);
		if (!userProjectRelTbls.isEmpty()) {
			final Collection<ProjectsTbl> projectsTbls = new ArrayList<>();
			for (final UserProjectRelTbl userProjectRelTbl : userProjectRelTbls) {
				ProjectsTbl projectsTbl = userProjectRelTbl.getProjectId();
				projectsTbl = validator.filterProjectResponse(isViewInactive, projectsTbl);
				if(projectsTbl != null) {
					Collection<AdminAreaProjectRelTbl> adminAreaProjectRelTbls = projectsTbl.getAdminAreaProjectRelTblCollection();

					for (AdminAreaProjectRelTbl adminAreaProjectRelTbl : adminAreaProjectRelTbls) {
						if (adminAreaProjectRelTbl.getSiteAdminAreaRelId().getSiteId().equals(sitesTbl)) {
							if (userStatus.equals(Status.INACTIVE.name())
									|| userProjectRelTbl.getStatus().equals(Status.INACTIVE.name())
									|| sitesTbl.getStatus().equals(Status.INACTIVE.name())
									|| adminAreaProjectRelTbl.getStatus().equals(Status.INACTIVE.name())
									|| adminAreaProjectRelTbl.getProjectId().getStatus().equals(Status.INACTIVE.name())
									|| adminAreaProjectRelTbl.getSiteAdminAreaRelId().getStatus().equals(Status.INACTIVE.name())
									|| adminAreaProjectRelTbl.getSiteAdminAreaRelId().getAdminAreaId().getStatus().equals(Status.INACTIVE.name())) {
								projectsTbl.setStatus(Status.INACTIVE.name());
							}
						}
					}

					if (null != projectsTbl && null != projectsTbl.getProjectId()) {
						projectsTbls.add(projectsTbl);
					}
				}
			}
			projectResponse = new ProjectResponse(projectsTbls);
		} else {
			throw new XMObjectNotFoundException("No User Project relationship found for the user id", "P_ERR0007");
		}
		LOG.info("<< findProjectsByUserId");

		LOG.info(">> findProjectsByUserIdAndSiteId");
		return projectResponse;
	}

}
