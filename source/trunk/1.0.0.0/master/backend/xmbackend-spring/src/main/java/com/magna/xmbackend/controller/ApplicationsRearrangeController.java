/**
 * 
 */
package com.magna.xmbackend.controller;

import javax.servlet.http.HttpServletRequest;
import javax.validation.constraints.NotNull;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.magna.xmbackend.mgr.ApplicationsRearrangeMgr;
import com.magna.xmbackend.vo.rearrange.RearrangeApplicationsRequest;
import com.magna.xmbackend.vo.rearrange.RearrangeApplicationsResponse;

/**
 * The Class ApplicationsRearrangeController.
 *
 * @author Bhabadyuti Bal
 */
@RestController
@RequestMapping(value="/rearrange")
public class ApplicationsRearrangeController {

	/** The Constant LOG. */
	private static final Logger LOG = LoggerFactory.getLogger(ApplicationsRearrangeController.class);
	
	/** The applications rearrange mgr. */
	@Autowired
	private ApplicationsRearrangeMgr applicationsRearrangeMgr;
	
	
	/**
	 * Save.
	 *
	 * @param httpServletRequest the http servlet request
	 * @param applicationsRequest the applications request
	 * @return the response entity
	 */
	@PostMapping(value="/save", consumes = { MediaType.APPLICATION_JSON_VALUE,
			MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_FORM_URLENCODED_VALUE,
			MediaType.MULTIPART_FORM_DATA_VALUE}, produces = {
					MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE,
					MediaType.APPLICATION_FORM_URLENCODED_VALUE })
	public ResponseEntity<Boolean> save(
			 HttpServletRequest httpServletRequest,
			@RequestBody @NotNull RearrangeApplicationsRequest applicationsRequest ) {
		LOG.info("> save");
		String userName = httpServletRequest.getHeader("USER_NAME");
		boolean saved = this.applicationsRearrangeMgr.createOrUpdate(applicationsRequest, userName);
		LOG.info("< save");
		return new ResponseEntity<Boolean>(saved, HttpStatus.OK); 
	}
	
	
	/**
	 * Find css settings for user.
	 *
	 * @param httpServletRequest the http servlet request
	 * @param applicationsRequest the applications request
	 * @return the response entity
	 */
	@PostMapping(value="/findSettings", consumes = { MediaType.APPLICATION_JSON_VALUE,
			MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_FORM_URLENCODED_VALUE }, produces = {
					MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE,
					MediaType.APPLICATION_FORM_URLENCODED_VALUE })
	public ResponseEntity<RearrangeApplicationsResponse> findCssSettingsForUser(HttpServletRequest httpServletRequest,
			@RequestBody @NotNull RearrangeApplicationsRequest applicationsRequest) {
		LOG.info("> getCssSettingsForUser");
		String userName = httpServletRequest.getHeader("USER_NAME");
		RearrangeApplicationsResponse response = this.applicationsRearrangeMgr.findSettings(applicationsRequest, userName);
		LOG.info("< getCssSettingsForUser");
		return new ResponseEntity<>(response, HttpStatus.OK);
	}
	
	
	/**
	 * Delete.
	 *
	 * @param httpServletRequest the http servlet request
	 * @param applicationsRequest the applications request
	 * @return the response entity
	 */
	@DeleteMapping(value="/delete", consumes = { MediaType.APPLICATION_JSON_VALUE,
			MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_FORM_URLENCODED_VALUE }, produces = {
					MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE,
					MediaType.APPLICATION_FORM_URLENCODED_VALUE })
	public ResponseEntity<Boolean> delete(HttpServletRequest httpServletRequest,
			@RequestBody @NotNull RearrangeApplicationsRequest applicationsRequest) {
		LOG.info("> deleteCssSettings");
		String userName = httpServletRequest.getHeader("USER_NAME");
		boolean isDeleted = this.applicationsRearrangeMgr.delete(applicationsRequest, userName);
		LOG.info("< deleteCssSettings");
		return new ResponseEntity<>(isDeleted, HttpStatus.OK);
	}
}
