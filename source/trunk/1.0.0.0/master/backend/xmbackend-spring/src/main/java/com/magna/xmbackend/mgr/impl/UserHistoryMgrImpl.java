/**
 * 
 */
package com.magna.xmbackend.mgr.impl;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import com.magna.xmbackend.entities.UserHistoryTbl;
import com.magna.xmbackend.entities.UserTkt;
import com.magna.xmbackend.jpa.dao.UserHistoryJpaDao;
import com.magna.xmbackend.jpa.dao.UserTktJpaDao;
import com.magna.xmbackend.mgr.UserHistoryMgr;
import com.magna.xmbackend.utils.DateUtil;
import com.magna.xmbackend.vo.enums.Application;
import com.magna.xmbackend.vo.userHistory.UserHistoryRequest;

/**
 * @author Bhabadyuti Bal
 *
 */
@Component
public class UserHistoryMgrImpl implements UserHistoryMgr {

	private static final Logger LOG = LoggerFactory.getLogger(UserHistoryMgrImpl.class);
	
	@Autowired
	private UserHistoryJpaDao userHistoryJpaDao;
	@Autowired
	private JdbcTemplate jdbcTemplate;
	@Autowired
	private UserTktJpaDao userTktJpaDao;
	
	@Value("${database.queryLimitKeyword}")
	private String dbLimitKeyword;
	@Autowired
	private DateUtil dateUtil;

	@Override
	public UserHistoryTbl create(UserHistoryRequest userHistoryRequest, final String userName, final String tkt) {
		LOG.info(">> create");
		UserHistoryTbl userHistoryOut = null;
		UserTkt userTktTbl = userTktJpaDao.findByTktAndApplicationName(tkt, Application.CAX_START_ADMIN.name());
		if(userTktTbl == null){
			userHistoryRequest.setUserName(userName);
			UserHistoryTbl userHistoryEntity = this.convert2Entity(userHistoryRequest, false);
			userHistoryOut = this.userHistoryJpaDao.save(userHistoryEntity);
			LOG.info("<< create");
		}
		
		return userHistoryOut;
	}
	
	private UserHistoryTbl convert2Entity(UserHistoryRequest userHistoryRequest, boolean isUpdate){
		LOG.info(">> convert2Entity");
		String adminArea = userHistoryRequest.getAdminArea();
		String site = userHistoryRequest.getSite();
		String application = userHistoryRequest.getApplication();
		String args = userHistoryRequest.getArgs();
		String host = userHistoryRequest.getHost();
		Integer pid = userHistoryRequest.getPid();
		String project = userHistoryRequest.getProject();
		String result = userHistoryRequest.getResult();
		Date logTime = userHistoryRequest.getLogTime();
		String isUserStatus = userHistoryRequest.getUserStatus();
		String userName = userHistoryRequest.getUserName();
		
		UserHistoryTbl userHistoryTbl = new UserHistoryTbl();
		userHistoryTbl.setAdminArea(adminArea);
		userHistoryTbl.setSite(site);
		userHistoryTbl.setApplication(application);
		userHistoryTbl.setApplication(application);
		userHistoryTbl.setArgs(args);
		userHistoryTbl.setHost(host);
		userHistoryTbl.setPid(pid);
		userHistoryTbl.setProject(project);
		userHistoryTbl.setResult(result);
		userHistoryTbl.setLogTime(logTime);
		userHistoryTbl.setIsUserStatus(isUserStatus);
		userHistoryTbl.setUserName(userName);
		LOG.info("<< convert2Entity");
		return userHistoryTbl;
	}

	@Override
	public boolean updateStatusToHistory(String userName, String tkt) {
		LOG.info(">> updateStatus");
		boolean isUpdated = false;
		UserTkt userTktTbl = userTktJpaDao.findByTktAndApplicationName(tkt, Application.CAX_START_ADMIN.name());
		if (userTktTbl == null) {
			final String isUserStatus = "false";
			Object[] params = { isUserStatus, userName };
			int updateStatus = jdbcTemplate
					.update("UPDATE USER_HISTORY_TBL SET IS_USER_STATUS = ? WHERE USER_NAME = ? ", params);
			if (updateStatus > 0)
				isUpdated = true;
		}
		LOG.info(">> updateStatus");
		return isUpdated;
	}

	@Override
	public Iterable<UserHistoryTbl> findAll() {
		LOG.info(">> findAll");
		Iterable<UserHistoryTbl> userHistoryTbls = this.userHistoryJpaDao.findAll();
		LOG.info("<< findAll");
		return userHistoryTbls;
	}

	@Override
	public List<Map<String, Object>> findUserStatus(UserHistoryRequest historyRequest) {
		LOG.info(">> findUserStatus");
		List<Map<String, Object>> queryResultSet = null;
		final int queryLimit = historyRequest.getQueryLimit();
		String queryCondition = historyRequest.getQueryCondition();
		//this.dateUtil.alterDbSessionDate();
		String newQueryCondition = null;
		String newQueryCondition2 = null;
		if (queryCondition != null) {
			String firstPattern = "LOG_TIME.*?\'.*?\'";
			Pattern pattern = Pattern.compile(firstPattern);
			Matcher matcher = pattern.matcher(queryCondition);
			int i = 1;
				while (matcher.find()) {
					String logtimeStr = queryCondition.substring(matcher.start(), matcher.end());
					LOG.info(logtimeStr);
					String secondPattern = "\'.*?\'";
					Pattern pattern2 = Pattern.compile(secondPattern);
					Matcher matcher2 = pattern2.matcher(logtimeStr);
					if (matcher2.find()) {
						String logTimeValue = logtimeStr.substring(matcher2.start(), matcher2.end());
						String dbSpecificDate = this.dateUtil.getDbSpecificDate(logTimeValue);
						if(i == 1) {
							newQueryCondition = queryCondition.replace(logTimeValue, dbSpecificDate);
						} else {
							newQueryCondition2 = newQueryCondition.replace(logTimeValue, dbSpecificDate);
						}
					}
					i++;
				}
				if (newQueryCondition == null && newQueryCondition2 == null ) {
					newQueryCondition2 = queryCondition;
				} else if (newQueryCondition != null && newQueryCondition2 == null) {
					newQueryCondition2 = newQueryCondition;
				}
			
		}
		
		final String firstPartQuery = "SELECT * FROM (SELECT * FROM USER_HISTORY_TBL ORDER BY LOG_TIME DESC) ";
		final String firstCondition = "IS_USER_STATUS = 'true' ";
		//final String orderBy = " ORDER BY LOG_TIME DESC ";
		if (newQueryCondition2 != null) {
			newQueryCondition2 = Pattern.compile(Pattern.quote("WHERE"), Pattern.CASE_INSENSITIVE).matcher(newQueryCondition2)
					.find() ? queryCondition : " WHERE " + newQueryCondition2;
			final String query = firstPartQuery + newQueryCondition2 + " AND "+firstCondition + "AND "+ dbLimitKeyword +" <= " + queryLimit;
			queryResultSet = jdbcTemplate.queryForList(query);
		} else {
			final String query = firstPartQuery + " WHERE "+firstCondition + " AND "+ dbLimitKeyword +" <= " + queryLimit;
			queryResultSet = jdbcTemplate.queryForList(query);
		}
		LOG.info("<< findUserStatus");
		return queryResultSet;
	}

	@Override
	public List<Map<String, Object>> findUserHistory(UserHistoryRequest historyRequest) {
		LOG.info(">> findUserHistory");
		List<Map<String, Object>> queryResultSet = null;
		final int queryLimit = historyRequest.getQueryLimit();
		String queryCondition = historyRequest.getQueryCondition();
		//this.dateUtil.alterDbSessionDate();
		String newQueryCondition = null;
		String newQueryCondition2 = null;
		if (queryCondition != null) {
			String firstPattern = "LOG_TIME.*?\'.*?\'";
			Pattern pattern = Pattern.compile(firstPattern);
			Matcher matcher = pattern.matcher(queryCondition);
			int i = 1;
				while (matcher.find()) {
					String logtimeStr = queryCondition.substring(matcher.start(), matcher.end());
					LOG.info(logtimeStr);
					String secondPattern = "\'.*?\'";
					Pattern pattern2 = Pattern.compile(secondPattern);
					Matcher matcher2 = pattern2.matcher(logtimeStr);
					if (matcher2.find()) {
						String logTimeValue = logtimeStr.substring(matcher2.start(), matcher2.end());
						String dbSpecificDate = this.dateUtil.getDbSpecificDate(logTimeValue);
						if(i == 1) {
							newQueryCondition = queryCondition.replace(logTimeValue, dbSpecificDate);
						} else {
							newQueryCondition2 = newQueryCondition.replace(logTimeValue, dbSpecificDate);
						}
					}
					i++;
				}
				if (newQueryCondition == null && newQueryCondition2 == null ) {
					newQueryCondition2 = queryCondition;
				} else if (newQueryCondition != null && newQueryCondition2 == null) {
					newQueryCondition2 = newQueryCondition;
				}
			
		}
		
		
		final String firstPartQuery = "SELECT * FROM (SELECT * FROM USER_HISTORY_TBL ORDER BY LOG_TIME DESC) ";
		final String firstCondition = " IS_USER_STATUS = 'false' ";
		//final String orderBy = " ORDER BY LOG_TIME DESC ";
		
		
		if (newQueryCondition2 != null) {
			newQueryCondition2 = Pattern.compile(Pattern.quote("WHERE"), Pattern.CASE_INSENSITIVE).matcher(newQueryCondition2)
					.find() ? newQueryCondition2 : " WHERE " + newQueryCondition2;
			
			final String query = firstPartQuery + newQueryCondition2 + " AND " + firstCondition+" AND "+ dbLimitKeyword +" <= " + queryLimit;
			queryResultSet = jdbcTemplate.queryForList(query);
			
		}else{
			
			final String query = firstPartQuery + " WHERE " + firstCondition + " AND "+ dbLimitKeyword +" <= " + queryLimit;
			queryResultSet = jdbcTemplate.queryForList(query);
			
		}
		LOG.info("<< findUserHistory");
		return queryResultSet;
	}
}
