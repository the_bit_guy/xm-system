package com.magna.xmbackend.mgr;

import java.util.List;
import java.util.Set;

import com.magna.xmbackend.entities.SitesTbl;
import com.magna.xmbackend.vo.jpa.site.SiteAdminAreaRelIdWithAdminAreaResponse;
import com.magna.xmbackend.vo.jpa.site.SiteRequest;
import com.magna.xmbackend.vo.jpa.site.SiteResponse;
import com.magna.xmbackend.vo.permission.ValidationRequest;
import com.magna.xmbackend.vo.project.ProjectResponse;
import javax.servlet.http.HttpServletRequest;

public interface SiteMgr {

    /**
     *
     * @param validationRequest
     * @return SiteResponse
     */
    public SiteResponse findAll(final ValidationRequest validationRequest);

    /**
     *
     * @param id
     * @return SitesTbl
     */
    public SitesTbl findById(final String id);

    /**
     *
     * @param siteRequest
     * @return SitesTbl
     */
    public SitesTbl create(final SiteRequest siteRequest, HttpServletRequest httpServletRequest);

    /**
     *
     * @param siteRequest
     * @return SitesTbl
     */
    public SitesTbl update(final SiteRequest siteRequest);

    /**
     *
     * @param status
     * @param id
     * @return boolean
     */
    public boolean updateById(final String status, final String id);

    /**
     *
     * @param id
     * @return boolean
     */
    public boolean deleteById(final String id);
    
    /**
     * 
     * @param siteIds
     * @param httpServletRequest
     * @return SiteResponse
     */
    public SiteResponse multiDelete(final Set<String> siteIds, 
            HttpServletRequest httpServletRequest);

    /**
     *
     * @param id
     * @param validationRequest
     * @return SiteAdminAreaRelIdWithAdminAreaResponse
     */
    public SiteAdminAreaRelIdWithAdminAreaResponse findAdminAreaById(final String id,
            final ValidationRequest validationRequest);

    /**
     *
     * @param name
     * @param validationRequest
     * @return SiteAdminAreaRelIdWithAdminAreaResponse
     */
    public SiteAdminAreaRelIdWithAdminAreaResponse findAdminAreaByName(final String name,
            final ValidationRequest validationRequest);

    /**
     *
     * @param name
     * @param validationRequest
     * @return SitesTbl
     */
    public SitesTbl findByName(final String name, final String tkt,
            final ValidationRequest validationRequest);
    
    
    public SitesTbl findByNameForBatch(final String name);

    /**
     *
     * @param id
     * @param validationRequest
     * @return ProjectResponse
     */
    public ProjectResponse findProjectById(final String id,
            final ValidationRequest validationRequest);

    /**
     *
     * @param name
     * @param validationRequest
     * @return ProjectResponse
     */
    public ProjectResponse findProjectByName(final String name,
            final ValidationRequest validationRequest);

    /**
     *
     * @param projectId
     * @return SiteResponse
     */
    public SiteResponse findSiteByProjectId(final String projectId);

    /**
     *
     * @param userAppId
     * @return SiteResponse
     */
    public SiteResponse findSitesByUserAppId(final String userAppId);

    /**
     *
     * @param projectAppId
     * @return SiteResponse
     */
    public SiteResponse findSitesByProjectAppId(final String projectAppId);

    /**
     *
     * @param startAppId
     * @return SiteResponse
     */
    public SiteResponse findSitesByStartAppId(final String startAppId);

    /**
     *
     * @param adminAreaId
     * @param validationRequest
     * @return SiteResponse
     */
    public SiteResponse findSitesByAdminAreaId(final String adminAreaId,
            final ValidationRequest validationRequest);

    /**
     * 
     * @param siteRequests
     * @param httpServletRequest
     * @return
     */
	public SiteResponse multiUpdate(List<SiteRequest> siteRequests, HttpServletRequest httpServletRequest);

	/**
	 * 
	 * @param siteRequest
	 * @return
	 */
	public SitesTbl updateForBatch(SiteRequest siteRequest, HttpServletRequest httpServletRequest);




}
