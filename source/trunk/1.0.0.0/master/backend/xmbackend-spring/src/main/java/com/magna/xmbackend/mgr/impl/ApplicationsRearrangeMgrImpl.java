/**
 * 
 */
package com.magna.xmbackend.mgr.impl;

import java.util.Date;
import java.util.Optional;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.magna.xmbackend.entities.ProjectsTbl;
import com.magna.xmbackend.entities.RearrangeApplicationsTbl;
import com.magna.xmbackend.entities.SitesTbl;
import com.magna.xmbackend.entities.UsersTbl;
import com.magna.xmbackend.exception.XMObjectNotFoundException;
import com.magna.xmbackend.jpa.dao.ApplicationsRearrangeJpaDao;
import com.magna.xmbackend.jpa.dao.UserJpaDao;
import com.magna.xmbackend.mgr.ApplicationsRearrangeMgr;
import com.magna.xmbackend.vo.rearrange.RearrangeApplicationsRequest;
import com.magna.xmbackend.vo.rearrange.RearrangeApplicationsResponse;

/**
 * @author Bhabadyuti Bal
 *
 */
@Service
public class ApplicationsRearrangeMgrImpl implements ApplicationsRearrangeMgr {
	
	private static final Logger LOG = LoggerFactory.getLogger(ApplicationsRearrangeMgrImpl.class);

	@Autowired
	private ApplicationsRearrangeJpaDao appRearrangeJpaDao;
	@Autowired
	private UserJpaDao userJpaDao;
	
	@Override
	public boolean createOrUpdate(RearrangeApplicationsRequest applicationsRequest, String username) {
		LOG.info(">> createOrUpdate");
		boolean isSaved = false;
		try {
			final RearrangeApplicationsTbl rearrangeApplicationsTbl = this.convert2Entity(applicationsRequest, username);
			this.appRearrangeJpaDao.save(rearrangeApplicationsTbl);
			isSaved = true;
		} catch (Exception e) {
			LOG.error("Error / Exception occured while saving to rearrange tbl - {}", e.getCause());
		}
		LOG.info("<< createOrUpdate");
		return isSaved;
	}

	private RearrangeApplicationsTbl convert2Entity(RearrangeApplicationsRequest applicationsRequest, String username) {
		UsersTbl usersTbl = this.userJpaDao.findByUsernameIgnoreCase(username);
		if (usersTbl == null) {
			throw new XMObjectNotFoundException("User not found", "P_ERR0008");
		}
		String siteId = applicationsRequest.getSiteId();
		String projectId = applicationsRequest.getProjectId();
		String userApplications = applicationsRequest.getUserApplications();
		String projectApplciations = applicationsRequest.getProjectApplciations();
		Date date = new Date();
		
		String id = UUID.randomUUID().toString().toUpperCase();
		Optional<RearrangeApplicationsTbl> optionalTbl = this.appRearrangeJpaDao.findByUserIdAndSiteIdAndProjectId(usersTbl, new SitesTbl(siteId), new ProjectsTbl(projectId));
		
		if(optionalTbl.isPresent()) {
			id = optionalTbl.get().getRearrangeId();
		}
		
		RearrangeApplicationsTbl applicationsTbl = new RearrangeApplicationsTbl(id);
		applicationsTbl.setUserId(usersTbl);
		applicationsTbl.setProjectId(new ProjectsTbl(projectId));
		applicationsTbl.setSiteId(new SitesTbl(siteId));
		applicationsTbl.setUserApplications(userApplications);
		applicationsTbl.setProjectApplications(projectApplciations);
		applicationsTbl.setCreateDate(date);
		applicationsTbl.setUpdateDate(date);
		return applicationsTbl;
	}

	@Override
	public boolean delete(RearrangeApplicationsRequest applicationsRequest, String userName) {
		boolean isDeleted = false;
		UsersTbl usersTbl = this.userJpaDao.findByUsernameIgnoreCase(userName);
		if (usersTbl == null) {
			throw new XMObjectNotFoundException("User not found", "P_ERR0008");
		}
		String siteId = applicationsRequest.getSiteId();
		String projectId = applicationsRequest.getProjectId();
		
		Optional<RearrangeApplicationsTbl> optionalTbl = this.appRearrangeJpaDao.findByUserIdAndSiteIdAndProjectId(usersTbl, new SitesTbl(siteId), new ProjectsTbl(projectId));
		optionalTbl.orElseThrow(XMObjectNotFoundException::new);
		try {
			this.appRearrangeJpaDao.delete(optionalTbl.get());
			isDeleted = true;
		} catch (Exception e) {
			LOG.error("Error / Exception occured while deletion of rearrange settings - {}", e.getCause());
		}
		return isDeleted;
	}

	@Override
	public RearrangeApplicationsResponse findSettings(RearrangeApplicationsRequest applicationsRequest, String userName) {
		UsersTbl usersTbl = this.userJpaDao.findByUsernameIgnoreCase(userName);
		if (usersTbl == null) {
			throw new XMObjectNotFoundException("User not found", "P_ERR0008");
		}
		String siteId = applicationsRequest.getSiteId();
		String projectId = applicationsRequest.getProjectId();
		RearrangeApplicationsResponse response = new RearrangeApplicationsResponse();
		Optional<RearrangeApplicationsTbl> optionalTbl = this.appRearrangeJpaDao.findByUserIdAndSiteIdAndProjectId(usersTbl, new SitesTbl(siteId), new ProjectsTbl(projectId));
		optionalTbl.ifPresent(tbl -> {
			response.setIsRearrangeSettingsAvailable("true");
			response.setRearrangeApplicationsTbl(tbl);
		});
		
		return response;
	}

}
