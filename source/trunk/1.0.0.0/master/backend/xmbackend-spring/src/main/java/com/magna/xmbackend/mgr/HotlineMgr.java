/**
 * 
 */
package com.magna.xmbackend.mgr;

import com.magna.xmbackend.vo.hotline.HotlineSaveRequest;
import com.magna.xmbackend.vo.hotline.HotlineSaveResponse;
import com.magna.xmbackend.vo.permission.ValidationRequest;

/**
 * @author Bhabadyuti Bal
 *
 */
public interface HotlineMgr {

	/**
	 * 
	 * @param hotlineSaveRequest
	 * @param validationRequest 
	 * @return HotlineSaveResponse
	 */
	HotlineSaveResponse save(final HotlineSaveRequest hotlineSaveRequest, final String username, final ValidationRequest validationRequest);
	
}
