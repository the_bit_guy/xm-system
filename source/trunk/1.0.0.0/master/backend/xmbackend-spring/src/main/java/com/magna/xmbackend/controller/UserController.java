package com.magna.xmbackend.controller;

import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.magna.xmbackend.audit.mgr.UserAuditMgr;
import com.magna.xmbackend.entities.UserTkt;
import com.magna.xmbackend.entities.UsersTbl;
import com.magna.xmbackend.jpa.dao.UserJpaDao;
import com.magna.xmbackend.jpa.dao.UserTktJpaDao;
import com.magna.xmbackend.mgr.UserMgr;
import com.magna.xmbackend.utils.Validator;
import com.magna.xmbackend.vo.permission.ValidationRequest;
import com.magna.xmbackend.vo.user.UserRequest;
import com.magna.xmbackend.vo.user.UserResponse;

@RestController
@RequestMapping(value = "/user")
public class UserController {

    private static final Logger LOG = LoggerFactory.getLogger(UserController.class);

    @Autowired
    private UserMgr userMgr;

    @Autowired
    private Validator validator;
    
    @Autowired
    private UserAuditMgr userAuditMgr;
    
    @Autowired
    private AuthController authController;
    
    @Autowired
    private UserTktJpaDao userTktJpaDao;
    
    @Autowired
    private UserJpaDao userJpaDao;

    /**
     *
     * @param httpServletRequest
     * @return UserResponse
     */
    @RequestMapping(value = "/findAll", method = RequestMethod.GET, consumes = {MediaType.APPLICATION_JSON_VALUE,
        MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_FORM_URLENCODED_VALUE}, produces = {
        MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE,
        MediaType.APPLICATION_FORM_URLENCODED_VALUE})
    public @ResponseBody
    final ResponseEntity<UserResponse> findAll(HttpServletRequest httpServletRequest) {
        LOG.info("> findAll");
        final ValidationRequest validationRequest
                = validator.formObjectValidationRequest(httpServletRequest, "USER");
        final UserResponse userResponse = this.userMgr.findAll(validationRequest);
        LOG.info("< findAll");
        return new ResponseEntity<>(userResponse, HttpStatus.OK);
    }

    /**
     *
     * @param httpServletRequest
     * @param startWith
     * @return UserResponse
     */
    @RequestMapping(value = "/findAllUserNameWith/{startWith}", method = RequestMethod.GET, consumes = {
        MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE,
        MediaType.APPLICATION_FORM_URLENCODED_VALUE}, produces = {MediaType.APPLICATION_JSON_VALUE,
        MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_FORM_URLENCODED_VALUE})
    public @ResponseBody
    final ResponseEntity<UserResponse> findUserNameStartWith(HttpServletRequest httpServletRequest,
            @PathVariable String startWith) {
        LOG.info("> findUserNameWith");
        final ValidationRequest validationRequest
                = validator.formObjectValidationRequest(httpServletRequest, "USER");
        final UserResponse userResponse = this.userMgr.findUserNameStartWith(startWith, validationRequest);
        LOG.info("< findUserNameWith");
        return new ResponseEntity<>(userResponse, HttpStatus.OK);
    }

    /**
     *
     * @param httpServletRequest
     * @param id
     * @return UserResponse
     */
    @RequestMapping(value = "/findUsersByProjectId/{id}", method = RequestMethod.GET, consumes = {
        MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE,
        MediaType.APPLICATION_FORM_URLENCODED_VALUE}, produces = {MediaType.APPLICATION_JSON_VALUE,
        MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_FORM_URLENCODED_VALUE})
    public @ResponseBody
    final ResponseEntity<UserResponse> findUsersByProjectId(HttpServletRequest httpServletRequest,
            @PathVariable String id) {
        LOG.info("> findUsersByProjectId");
         final ValidationRequest validationRequest
                = validator.formObjectValidationRequest(httpServletRequest, "USER");
        final UserResponse userResponse 
                = this.userMgr.findUsersByProjectId(id, validationRequest);
        LOG.info("< findUsersByProjectId");
        return new ResponseEntity<>(userResponse, HttpStatus.OK);
    }

    /**
     *
     * @param httpServletRequest
     * @param id
     * @return UserResponse
     */
    @RequestMapping(value = "/findUsersByUserAppId/{id}", method = RequestMethod.GET, consumes = {
        MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE,
        MediaType.APPLICATION_FORM_URLENCODED_VALUE}, produces = {MediaType.APPLICATION_JSON_VALUE,
        MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_FORM_URLENCODED_VALUE})
    public @ResponseBody
    final ResponseEntity<UserResponse> findUsersByUserAppId(HttpServletRequest httpServletRequest,
            @PathVariable String id) {
        LOG.info("> findUsersByUserAppId");
        final UserResponse userResponse = this.userMgr.findUsersByUserAppId(id);
        LOG.info("< findUsersByUserAppId");
        return new ResponseEntity<>(userResponse, HttpStatus.OK);
    }

    /**
     *
     * @param httpServletRequest
     * @param id
     * @return UserResponse
     */
    @RequestMapping(value = "/findUsersByProjectAppId/{id}", method = RequestMethod.GET, consumes = {
        MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE,
        MediaType.APPLICATION_FORM_URLENCODED_VALUE}, produces = {MediaType.APPLICATION_JSON_VALUE,
        MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_FORM_URLENCODED_VALUE})
    public @ResponseBody
    final ResponseEntity<UserResponse> findUsersByProjectAppId(
            HttpServletRequest httpServletRequest, @PathVariable String id) {
        LOG.info("> findUsersByProjectAppId");
        final ValidationRequest validationRequest
                = validator.formObjectValidationRequest(httpServletRequest, "USER");
        final UserResponse userResponse = this.userMgr.findUsersByProjectAppId(id, validationRequest);
        LOG.info("< findUsersByProjectAppId");
        return new ResponseEntity<>(userResponse, HttpStatus.OK);
    }

    /**
     *
     * @param httpServletRequest
     * @param id
     * @return UserResponse
     */
    @RequestMapping(value = "/findUsersByStartAppId/{id}", method = RequestMethod.GET, consumes = {
        MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE,
        MediaType.APPLICATION_FORM_URLENCODED_VALUE}, produces = {MediaType.APPLICATION_JSON_VALUE,
        MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_FORM_URLENCODED_VALUE})
    public @ResponseBody
    final ResponseEntity<UserResponse> findUsersByStartAppId(HttpServletRequest httpServletRequest,
            @PathVariable String id) {
        LOG.info("> findUsersByStartAppId");
        final UserResponse userResponse = this.userMgr.findUsersByStartAppId(id);
        LOG.info("< findUsersByStartAppId");
        return new ResponseEntity<>(userResponse, HttpStatus.OK);
    }

    /**
     *
     * @param httpServletRequest
     * @param id
     * @return UsersTbl
     */
    @RequestMapping(value = "/find/{id}", method = RequestMethod.GET, consumes = {MediaType.APPLICATION_JSON_VALUE,
        MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_FORM_URLENCODED_VALUE}, produces = {
        MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE,
        MediaType.APPLICATION_FORM_URLENCODED_VALUE})
    public @ResponseBody
    final ResponseEntity<UsersTbl> findById(HttpServletRequest httpServletRequest,
            @PathVariable String id) {
        LOG.info("> findById");
        final ValidationRequest validationRequest
                = validator.formObjectValidationRequest(httpServletRequest, "USER");
        final UsersTbl user = this.userMgr.findById(id, validationRequest);
        LOG.info("< findById");
        return new ResponseEntity<>(user, HttpStatus.OK);
    }

    /**
     * 
     * @param httpServletRequest
     * @param userRequest
     * @return UsersTbl
     */
    @RequestMapping(value = "/findByName", method = RequestMethod.POST, consumes = {
        MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE,
        MediaType.APPLICATION_FORM_URLENCODED_VALUE}, produces = {MediaType.APPLICATION_JSON_VALUE,
        MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_FORM_URLENCODED_VALUE})
    public @ResponseBody
    final ResponseEntity<UsersTbl> findByName(HttpServletRequest httpServletRequest,
            @RequestBody UserRequest userRequest) {
        LOG.info("> findByName");
        final String userName = userRequest.getUserName();
        final ValidationRequest validationRequest
                = validator.formObjectValidationRequest(httpServletRequest, "USER");
        final UsersTbl user = this.userMgr.findByName(userName, validationRequest);
        LOG.info("< findByName");
        return new ResponseEntity<>(user, HttpStatus.OK);
    }

    /**
     * Batch Save.
     *
     * @param httpServletRequest the http servlet request
     * @param userRequests the user request
     * @return the response entity
     */
    @RequestMapping(value = "/batchSave", method = RequestMethod.POST,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE
            })
    public @ResponseBody
    final ResponseEntity<UserResponse> batchSave(HttpServletRequest httpServletRequest,
            @RequestBody List<UserRequest> userRequests) {
        LOG.info("> batchSave");
        UserResponse response = null;
		try {
			response = this.userMgr.createBatch(userRequests);
			this.userAuditMgr.userMultiSaveSuccessAuditor(httpServletRequest, userRequests, response);
		} catch (Exception ex) {
			this.userAuditMgr.userMultiSaveFailureAuditor(httpServletRequest, ex);
		}
        LOG.info("< batchSave");
        return new ResponseEntity<>(response, HttpStatus.ACCEPTED);
    }

    /**
     * Save.
     *
     * @param httpServletRequest the http servlet request
     * @param userRequest the user request
     * @return the response entity
     */
    @RequestMapping(value = "/save", method = RequestMethod.POST, consumes = {MediaType.APPLICATION_JSON_VALUE,
        MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_FORM_URLENCODED_VALUE}, produces = {
        MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE,
        MediaType.APPLICATION_FORM_URLENCODED_VALUE})
    public @ResponseBody
    final ResponseEntity<UsersTbl> save(HttpServletRequest httpServletRequest,
            @RequestBody @Valid UserRequest userRequest) {
        LOG.info("> save");
        UsersTbl userTbl = this.userMgr.create(userRequest, httpServletRequest);
        LOG.info("< save");
        return new ResponseEntity<>(userTbl, HttpStatus.ACCEPTED);
    }

    /**
     * Update.
     *
     * @param httpServletRequest the http servlet request
     * @param userRequest the user request
     * @return the response entity
     */
    @RequestMapping(value = "/update", method = RequestMethod.POST, consumes = {MediaType.APPLICATION_JSON_VALUE,
        MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_FORM_URLENCODED_VALUE}, produces = {
        MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE,
        MediaType.APPLICATION_FORM_URLENCODED_VALUE})
    public @ResponseBody
    final ResponseEntity<UsersTbl> update(HttpServletRequest httpServletRequest,
            @RequestBody UserRequest userRequest) {
        LOG.info("> update");
        UsersTbl usersTbl;
		try {
			usersTbl = this.userMgr.update(userRequest);
			this.userAuditMgr.userUpdateSuccessAudit(
                    httpServletRequest, userRequest);
		} catch (Exception ex) {
			String name = userRequest.getUserName();
            LOG.error("Exception / Error in updating user with name {} "
                    + "and exception msg {}", name, ex.getMessage());
            this.userAuditMgr.userUpdateFailureAudit(httpServletRequest,
            		userRequest, ex.getMessage());
            throw ex;
		}
        LOG.info("< update");
        return new ResponseEntity<>(usersTbl, HttpStatus.ACCEPTED);
    }

    /**
     * Update Status.
     *
     * @param httpServletRequest the http servlet request
     * @param status the status
     * @param id the id
     * @return the response entity
     */
    @RequestMapping(value = "/updateStatus/{status}/{id}", method = RequestMethod.POST, consumes = {
        MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE,
        MediaType.APPLICATION_FORM_URLENCODED_VALUE}, produces = {MediaType.APPLICATION_JSON_VALUE,
        MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_FORM_URLENCODED_VALUE})
    public @ResponseBody
    final ResponseEntity<Boolean> update(HttpServletRequest httpServletRequest,
            @PathVariable String status, @PathVariable String id) {
        LOG.info("> update");
        final boolean updateStatusById = this.userMgr.updateById(status, id);
        LOG.info("< update");
        return new ResponseEntity<>(updateStatusById, HttpStatus.ACCEPTED);
    }
    
    
    @RequestMapping(value = "/multiStatusUpdate",
			method = RequestMethod.POST,
			consumes = {MediaType.APPLICATION_JSON_VALUE,
				MediaType.APPLICATION_XML_VALUE,
				MediaType.APPLICATION_FORM_URLENCODED_VALUE},
			produces = {MediaType.APPLICATION_JSON_VALUE,
				MediaType.APPLICATION_XML_VALUE,
				MediaType.APPLICATION_FORM_URLENCODED_VALUE}
	)
	public @ResponseBody
	final ResponseEntity<UserResponse> multiUpdate(
			HttpServletRequest httpServletRequest,
			@RequestBody List<UserRequest> userRequests) {
		LOG.info("> multiUpdate");
		UserResponse userResponse = this.userMgr.multiUpdate(userRequests, httpServletRequest);
		LOG.info("< multiUpdate");
		return new ResponseEntity<>(userResponse, HttpStatus.ACCEPTED);
	}
    

    /**
     * Delete.
     *
     * @param httpServletRequest the http servlet request
     * @param id the id
     * @return the response entity
     */
    @RequestMapping(value = "/delete/{id}", method = RequestMethod.POST, consumes = {MediaType.APPLICATION_JSON_VALUE,
        MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_FORM_URLENCODED_VALUE}, produces = {
        MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE,
        MediaType.APPLICATION_FORM_URLENCODED_VALUE})
    public @ResponseBody
    final ResponseEntity<Boolean> delete(HttpServletRequest httpServletRequest,
            @PathVariable String id) {
        LOG.info("> delete");
        httpServletRequest.setAttribute("userId", id);
        final boolean isDeleted = this.userMgr.deleteById(id);
        LOG.info("< delete");
        return new ResponseEntity<>(isDeleted, HttpStatus.ACCEPTED);
    }
    
    
    @RequestMapping(value = "/multiDelete", method = RequestMethod.POST, consumes = {MediaType.APPLICATION_JSON_VALUE,
            MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_FORM_URLENCODED_VALUE}, produces = {
            MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE,
            MediaType.APPLICATION_FORM_URLENCODED_VALUE})
        public @ResponseBody
        final ResponseEntity<UserResponse> delete(HttpServletRequest httpServletRequest,
                @RequestBody Set<String> userIds) {
            LOG.info("> multiDelete");
            httpServletRequest.setAttribute("userIds", userIds);
            final UserResponse userResponse = this.userMgr.multiDelete(userIds, httpServletRequest);
            LOG.info("< multiDelete");
            return new ResponseEntity<>(userResponse, HttpStatus.ACCEPTED);
        }
    

    /**
     *
     * @param httpServletRequest
     * @param adminAreaId
     * @return UserResponse
     */
    @RequestMapping(value = "/findUsersByAAId/{adminAreaId}", method = RequestMethod.GET, consumes = {
        MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE,
        MediaType.APPLICATION_FORM_URLENCODED_VALUE}, produces = {MediaType.APPLICATION_JSON_VALUE,
        MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_FORM_URLENCODED_VALUE})
    public @ResponseBody
    final ResponseEntity<UserResponse> findUsersByAAId(HttpServletRequest httpServletRequest,
            @PathVariable String adminAreaId) {
        LOG.info("> findUsersByProjectId");
        final UserResponse userResponse = this.userMgr.findUsersByAAId(adminAreaId);
        LOG.info("< findUsersByProjectId");
        return new ResponseEntity<>(userResponse, HttpStatus.OK);
    }

    /**
     * Find users by admin area id.
     *
     * @param httpServletRequest the http servlet request
     * @param adminAreaId the admin area id
     * @return the response entity
     */
    @RequestMapping(value = "/findUsersByAdminAreaId/{adminAreaId}", method = RequestMethod.GET, consumes = {
        MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE,
        MediaType.APPLICATION_FORM_URLENCODED_VALUE}, produces = {MediaType.APPLICATION_JSON_VALUE,
        MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_FORM_URLENCODED_VALUE})
    public @ResponseBody
    final ResponseEntity<UserResponse> findUsersByAdminAreaId(
            HttpServletRequest httpServletRequest, @PathVariable String adminAreaId) {
        LOG.info("> findUsersByAdminAreaId");
        final ValidationRequest validationRequest
                = validator.formObjectValidationRequest(httpServletRequest, "USER");
        final UserResponse userResponse = this.userMgr.findUsersByAdminAreaId(adminAreaId, validationRequest);
        LOG.info("< findUsersByAdminAreaId");
        return new ResponseEntity<>(userResponse, HttpStatus.OK);
    }

    /**
     * Find user by site id.
     *
     * @param httpServletRequest the http servlet request
     * @param siteId the site id
     * @return the response entity
     */
    @RequestMapping(value = "/findUserBySiteId/{siteId}", method = RequestMethod.GET, consumes = {
        MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE,
        MediaType.APPLICATION_FORM_URLENCODED_VALUE}, produces = {MediaType.APPLICATION_JSON_VALUE,
        MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_FORM_URLENCODED_VALUE})
    public @ResponseBody
    final ResponseEntity<UserResponse> findUserBySiteId(
            HttpServletRequest httpServletRequest, @PathVariable String siteId) {
        LOG.info("> findUserBySiteId");
        final ValidationRequest validationRequest
                = validator.formObjectValidationRequest(httpServletRequest, "USER");
        final UserResponse userResponse = this.userMgr.findUserBySiteId(siteId, validationRequest);
        LOG.info("< findUserBySiteId");
        return new ResponseEntity<>(userResponse, HttpStatus.OK);
    }

    /**
     * Save from cax start batch.
     *
     * @param httpServletRequest the http servlet request
     * @param userRequest the user request
     * @return the response entity
     */
    @RequestMapping(value = "/caxstartbatch/save", method = RequestMethod.POST, consumes = {MediaType.APPLICATION_JSON_VALUE,
        MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_FORM_URLENCODED_VALUE}, produces = {
        MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE,
        MediaType.APPLICATION_FORM_URLENCODED_VALUE})
    public @ResponseBody
    final ResponseEntity<Boolean> saveFromCaxStartBatch(HttpServletRequest httpServletRequest,
            @Valid @RequestBody UserRequest userRequest) {
        LOG.info("> save");
        UsersTbl userTbl = null;
		userTbl = this.userMgr.create(userRequest, httpServletRequest);
        LOG.info("< save");
        if (userTbl != null) {
        	this.userAuditMgr.createSuccessAuditForBatch(userTbl, httpServletRequest);
        	return new ResponseEntity<>(Boolean.TRUE, HttpStatus.ACCEPTED);
        } else {
        	this.userAuditMgr.createFailureAuditForBatch(userRequest, httpServletRequest);
        	return new ResponseEntity<>(Boolean.FALSE, HttpStatus.ACCEPTED);
        }
    }
	
	/**
	 * Update from cax start batch.
	 *
	 * @param httpServletRequest the http servlet request
	 * @param userRequest the user request
	 * @return the response entity
	 */
	@RequestMapping(value = "/caxstartbatch/update", method = RequestMethod.POST, consumes = {
			MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE,
			MediaType.APPLICATION_FORM_URLENCODED_VALUE }, produces = { MediaType.APPLICATION_JSON_VALUE,
					MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_FORM_URLENCODED_VALUE })
	public @ResponseBody final ResponseEntity<Boolean> updateFromCaxStartBatch(HttpServletRequest httpServletRequest,
			@NotNull @RequestBody UserRequest userRequest) {
		LOG.info("> update");
		final UsersTbl usersTbl1 = this.userMgr.updateForBatch(userRequest);
		LOG.info("< update");
		if (usersTbl1 != null) {
			this.userAuditMgr.userUpdateSuccessAudit(httpServletRequest, userRequest);
			return new ResponseEntity<>(Boolean.TRUE, HttpStatus.ACCEPTED);
		} else {
			this.userAuditMgr.userUpdateFailureAudit(httpServletRequest, userRequest, "User update failed");
			return new ResponseEntity<>(Boolean.FALSE, HttpStatus.ACCEPTED);
		}
	}
	
    /**
     * Delete by name.
     *
     * @param httpServletRequest the http servlet request
     * @param name the name
     * @return the response entity
     */
    @RequestMapping(value = "/caxstartbatch/delete", method = RequestMethod.POST, consumes = {MediaType.APPLICATION_JSON_VALUE,
        MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_FORM_URLENCODED_VALUE}, produces = {
        MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE,
        MediaType.APPLICATION_FORM_URLENCODED_VALUE})
    public @ResponseBody
    final ResponseEntity<Boolean> deleteByName(HttpServletRequest httpServletRequest,
            @RequestBody String name) {
        LOG.info("> delete");
        UsersTbl usersTbl = this.userMgr.findByName(name);
        String username = usersTbl.getUsername();
        String userId = usersTbl.getUserId();
        boolean isDeleted = false;
        if (null != usersTbl) {
        	httpServletRequest.setAttribute("userIds", usersTbl.getUserId());
        	isDeleted = this.userMgr.deleteById(usersTbl.getUserId());
			if (isDeleted) {
				this.userAuditMgr.userDeleteSuccessAudit(httpServletRequest, userId, username);
			} else {
				this.userAuditMgr.userDeleteFailureAudit(httpServletRequest, userId, username);
			}
        }
        LOG.info("< delete");
        return new ResponseEntity<>(isDeleted, HttpStatus.ACCEPTED);
    }
    
    
    @RequestMapping(value = "/isSuperAdmin", method = RequestMethod.GET, consumes = {MediaType.APPLICATION_JSON_VALUE,
            MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_FORM_URLENCODED_VALUE}, produces = {
            MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE,
            MediaType.APPLICATION_FORM_URLENCODED_VALUE})
        public @ResponseBody
        final ResponseEntity<Boolean> isSuperAdmin(HttpServletRequest httpServletRequest) {
            LOG.info("> isSuperAdmin");
            boolean isSuperAdmin = false;
            String ticket = httpServletRequest.getHeader("TKT");
			if(null != ticket) {
				String hashedTkt = this.authController.hash(ticket);
				UserTkt userTkt = this.userTktJpaDao.findByTkt(hashedTkt);
				if(null != userTkt) {
					String username = userTkt.getUsername();
					Set<String> superAdminUsers = this.userJpaDao.findSuperAdminUsers();
					if (superAdminUsers.contains(username)) {
						isSuperAdmin = true;
					}
					/*for(String user : superAdminUsers){
						if(username.equals(this.authController.hash(user))) {
							isSuperAdmin = true;
							break;
						}
					}*/
				}
			}
            
            LOG.info("< isSuperAdmin");
            return new ResponseEntity<>(isSuperAdmin, HttpStatus.ACCEPTED);
        }
}
