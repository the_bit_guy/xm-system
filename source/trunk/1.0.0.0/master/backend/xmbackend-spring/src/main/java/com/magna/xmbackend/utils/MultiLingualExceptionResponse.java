/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magna.xmbackend.utils;

import java.util.Map;

/**
 *
 * @author dhana
 */
public class MultiLingualExceptionResponse {

    private int code;
    private String errCode;
    private String description;

    private Map<String, String> lingualMessages;

    public MultiLingualExceptionResponse() {
    }

    public MultiLingualExceptionResponse(int code, String errCode,
            String description, Map<String, String> lingualMessages) {
        this.code = code;
        this.errCode = errCode;
        this.description = description;
        this.lingualMessages = lingualMessages;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    /**
     * @return the errCode
     */
    public String getErrCode() {
        return errCode;
    }

    /**
     * @param errCode the errCode to set
     */
    public void setErrCode(String errCode) {
        this.errCode = errCode;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return the lingualMessages
     */
    public Map<String, String> getLingualMessages() {
        return lingualMessages;
    }

    /**
     * @param lingualMessages the lingualMessages to set
     */
    public void setLingualMessages(Map<String, String> lingualMessages) {
        this.lingualMessages = lingualMessages;
    }

}
