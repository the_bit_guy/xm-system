/**
 * 
 */
package com.magna.xmbackend.audit.mgr;

import javax.servlet.http.HttpServletRequest;

import com.magna.xmbackend.entities.IconsTbl;
import com.magna.xmbackend.vo.icon.IkonRequest;

/**
 * @author Bhabadyuti Bal
 *
 */
public interface IconAuditMgr {

	/**
	 * 
	 * @param it
	 * @param httpServletRequest
	 */
	void iconCreateSuccessAudit(IconsTbl it, HttpServletRequest httpServletRequest);

	/**
	 * 
	 * @param httpServletRequest
	 * @param ikonRequest
	 * @param string
	 */
	void iconCreateFailureAudit(HttpServletRequest httpServletRequest, IkonRequest ikonRequest, String errMsg);

	/**
	 * 
	 * @param httpServletRequest
	 * @param iconsTbl
	 * @param isDeleted
	 */
	void iconDeleteSuccessAudit(HttpServletRequest httpServletRequest, IconsTbl iconsTbl, boolean isDeleted);

	/**
	 * 
	 * @param httpServletRequest
	 * @param id
	 * @param errMsg
	 */
	void iconDeleteFailureAudit(HttpServletRequest httpServletRequest, String id, String errMsg);
	
	
	

}
