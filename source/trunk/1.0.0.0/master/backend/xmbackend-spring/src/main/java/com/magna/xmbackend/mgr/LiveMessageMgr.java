/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magna.xmbackend.mgr;

import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import com.magna.xmbackend.entities.LiveMessageTbl;
import com.magna.xmbackend.vo.liveMessage.LiveMessageCreateRequest;
import com.magna.xmbackend.vo.liveMessage.LiveMessageResponse;
import com.magna.xmbackend.vo.liveMessage.LiveMessageResponseWrapper;
import com.magna.xmbackend.vo.liveMessage.LiveMessageStatusRequest;
import com.magna.xmbackend.vo.liveMessage.LiveMessageStatusResponseWrapper;

/**
 *
 * @author dhana
 */
public interface LiveMessageMgr {

	/**
	 * 
	 * @param liveMessageCreateRequest
	 * @return LiveMessageTbl
	 */
    LiveMessageTbl createLiveMessage(final LiveMessageCreateRequest liveMessageCreateRequest);
    
    /**
     * 
     * @return LiveMessageResponseWrapper
     */
    LiveMessageResponseWrapper findAll();
    
    /**
     * 
     * @return boolean
     */
    boolean updateLiveMessage(final LiveMessageCreateRequest liveMessageCreateRequest);
    
    /**
     * 
     * @param liveMsgId
     * @return boolean
     */
    boolean delteById(String liveMsgId);
    
    
    
    LiveMessageResponse multiDelete(final Set<String> ids, HttpServletRequest hsr);
    
    /**
     * 
     * @param siteId
     * @param userName
     * @return LiveMessageResponseWrapper
     */
    LiveMessageStatusResponseWrapper findLiveMessagesByStatus(final LiveMessageStatusRequest request, 
    		final String userName, final String tkt);
    
    
    boolean updateLiveMessageStatus(final LiveMessageStatusRequest liveMessageStatusRequest,final String userName, final String tkt);


    LiveMessageTbl findById(final String liveMsgId);
}
