package com.magna.xmbackend.rel.mgr.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Component;

import com.magna.xmbackend.audit.rel.mgr.AdminAreaUserAppAuditMgr;
import com.magna.xmbackend.entities.AdminAreaUserAppRelTbl;
import com.magna.xmbackend.entities.AdminAreasTbl;
import com.magna.xmbackend.entities.LanguagesTbl;
import com.magna.xmbackend.entities.SiteAdminAreaRelTbl;
import com.magna.xmbackend.entities.UserAppTranslationTbl;
import com.magna.xmbackend.entities.UserApplicationsTbl;
import com.magna.xmbackend.exception.CannotCreateRelationshipException;
import com.magna.xmbackend.exception.XMObjectNotFoundException;
import com.magna.xmbackend.jpa.rel.dao.AdminAreaUserAppRelJpaDao;
import com.magna.xmbackend.jpa.rel.dao.SiteAdminAreaRelJpaDao;
import com.magna.xmbackend.rel.mgr.AdminAreaUserAppRelMgr;
import com.magna.xmbackend.rel.mgr.SiteAdminAreaRelMgr;
import com.magna.xmbackend.response.rel.adminareauserapp.AdminAreaUserAppRelWrapper;
import com.magna.xmbackend.response.rel.adminareauserapp.AdminAreaUserAppRelation;
import com.magna.xmbackend.utils.MessageMaker;
import com.magna.xmbackend.utils.Validator;
import com.magna.xmbackend.vo.enums.Status;
import com.magna.xmbackend.vo.permission.ValidationRequest;
import com.magna.xmbackend.vo.rel.AdminAreaUserAppRelBatchRequest;
import com.magna.xmbackend.vo.rel.AdminAreaUserAppRelBatchResponse;
import com.magna.xmbackend.vo.rel.AdminAreaUserAppRelRequest;
import com.magna.xmbackend.vo.rel.AdminAreaUserAppRelResponse;

/**
 *
 * @author vijay
 */
@Component
public class AdminAreaUserAppRelMgrImpl implements AdminAreaUserAppRelMgr {

    private static final Logger LOG
            = LoggerFactory.getLogger(AdminAreaUserAppRelMgrImpl.class);

    @Autowired
    private AdminAreaUserAppRelJpaDao adminAreaUserAppRelJpaDao;

    @Autowired
    private SiteAdminAreaRelMgr siteAdminAreaRelMgr;

    @Autowired
    private MessageMaker messageMaker;

    @Autowired
    private SiteAdminAreaRelJpaDao siteAdminAreaRelJpaDao;

    @Autowired
    private Validator validator;
    
    @Autowired
    private AdminAreaUserAppAuditMgr adminAreaUserAppAuditMgr;
    

    /**
     *
     * @return AdminAreaUserAppRelResponse
     */
    @Override
    public final AdminAreaUserAppRelResponse findAll() {
        final Iterable<AdminAreaUserAppRelTbl> adminAreaUserAppRelTbls
                = adminAreaUserAppRelJpaDao.findAll();
        final AdminAreaUserAppRelResponse adminAreaUserAppRelResponse
                = new AdminAreaUserAppRelResponse(adminAreaUserAppRelTbls);
        return adminAreaUserAppRelResponse;
    }

    /**
     *
     * @param id
     * @return AdminAreaUserAppRelTbl
     */
    @Override
    public final AdminAreaUserAppRelTbl findById(String id) {
        LOG.info(">> findById {}", id);
        final AdminAreaUserAppRelTbl adminAreaUserAppRelTbl
                = this.adminAreaUserAppRelJpaDao.findOne(id);
        LOG.info("<< findById");
        return adminAreaUserAppRelTbl;
    }

    /**
     *
     * @param adminAreaUserAppRelRequest
     * @param userName
     * @return AdminAreaProjAppRelTbl
     */
    @Override
    public final AdminAreaUserAppRelTbl create(
            final AdminAreaUserAppRelRequest adminAreaUserAppRelRequest,
            final String userName) {
        AdminAreaUserAppRelTbl aauarOut = null;
        LOG.info(">> create");
        final Map<String, Object> assignmentAllowedMap
                = isInActiveAssignmentsAllowed(adminAreaUserAppRelRequest, userName);
        LOG.info("assignmentAllowedMap={}", assignmentAllowedMap);
        if (assignmentAllowedMap.containsKey("isAssignmentAllowed")) {
            boolean isAssignmentAllowed = (boolean) assignmentAllowedMap.get("isAssignmentAllowed");
            if (isAssignmentAllowed) {
                this.checkIfUserAppExistsInAA(adminAreaUserAppRelRequest);
                final AdminAreaUserAppRelTbl aauarIn
                        = this.convert2Entity(adminAreaUserAppRelRequest);
                aauarOut = adminAreaUserAppRelJpaDao.save(aauarIn);
            } else {
                if (assignmentAllowedMap.containsKey("userApplicationsTbl")) {
                    final Map<String, String> adminAreaTransMap
                            = messageMaker.getAdminAreaNames((AdminAreasTbl) assignmentAllowedMap.get("adminAreasTbl"));
                    final Map<String, String> userAppTransMap
                            = messageMaker.getUserAppNames((UserApplicationsTbl) assignmentAllowedMap.get("userApplicationsTbl"));
                    final Map<String, String[]> paramMap = messageMaker.geti18nCodeMap(adminAreaTransMap, userAppTransMap);
                    LOG.debug("paramMap={}", paramMap);
                    throw new CannotCreateRelationshipException("Inactive Assignments not allowed", "AAUA_ERR0001", paramMap);
                }
            }
        }
        LOG.info("<< create");

        return aauarOut;
    }

    /**
     *
     * @param userStartAppRelRequest
     * @param userName
     * @return Map
     */
    private Map<String, Object> isInActiveAssignmentsAllowed(final AdminAreaUserAppRelRequest adminAreaUserAppRelRequest,
            final String userName) {
        final boolean isViewInactiveAllowed = isViewInActiveAllowed(userName);
        LOG.debug("isViewInactiveAllowed={}", isViewInactiveAllowed);
        final Map<String, Object> assignmentAllowedMap
                = validator.isUserAppAdminAreaInactiveAssignmentAllowed(adminAreaUserAppRelRequest,
                        isViewInactiveAllowed);
        return assignmentAllowedMap;
    }

    /**
     *
     * @param userName
     * @return boolean
     */
    private boolean isViewInActiveAllowed(final String userName) {
        final ValidationRequest validationRequest
                = validator.formSaveRelationValidationRequest(userName, "USERAPP_ADMINAREA");
        LOG.info("validationRequest={}", validationRequest);
        final boolean isInactiveAssignment = validator.isViewInactiveAllowed(validationRequest);
        LOG.info("isInactiveAssignment={}", isInactiveAssignment);
        return isInactiveAssignment;
    }

    /**
     *
     * @param aauarr
     */
    private void checkIfUserAppExistsInAA(final AdminAreaUserAppRelRequest aauarr) {
        final String siteAdminAreaRelIdIn = aauarr.getSiteAdminAreaRelId();
        final String userAppIdIn = aauarr.getUserAppId();

        LOG.debug("siteAdminAreaRelIdIn {}, userAppIdIn {}",
                siteAdminAreaRelIdIn, userAppIdIn);

        final SiteAdminAreaRelTbl siteAdminAreaRelTbl
                = this.siteAdminAreaRelMgr.findById(siteAdminAreaRelIdIn);
        final Collection<AdminAreaUserAppRelTbl> adminAreaUserAppRelTblCollection
                = siteAdminAreaRelTbl.getAdminAreaUserAppRelTblCollection();
        for (final AdminAreaUserAppRelTbl adminAreaUserAppRelTbl
                : adminAreaUserAppRelTblCollection) {
            final UserApplicationsTbl userApplicationsTbl
                    = adminAreaUserAppRelTbl.getUserApplicationId();
            final String userApplicationIdFromTbl
                    = userApplicationsTbl.getUserApplicationId();

            if (userApplicationIdFromTbl != null
                    && userApplicationIdFromTbl.equalsIgnoreCase(userAppIdIn)) {
                //throw exception
                //Error Message: User Application 'name' already exists in Administration Area 'name'
                final Map<String, String> userApplicationNames = this.getUserApplicationNames(userApplicationsTbl);
                final AdminAreasTbl adminAreasTbl = siteAdminAreaRelTbl.getAdminAreaId();
                final Map<String, String> adminAreaNames = this.messageMaker.getAdminAreaNames(adminAreasTbl);
                final Map<String, String[]> i18nCodeMap
                        = this.messageMaker.geti18nCodeMap(userApplicationNames, adminAreaNames);
                throw new CannotCreateRelationshipException(
                        "Realtionship exists", "ERR0006", i18nCodeMap);
            }

        }
    }

    /**
     *
     * @param userApplicationsTbl
     * @return Map
     */
    private Map<String, String> getUserApplicationNames(final UserApplicationsTbl userApplicationsTbl) {
        final String name = userApplicationsTbl.getName();
        final Collection<UserAppTranslationTbl> userAppTranslationTblCollection = userApplicationsTbl.getUserAppTranslationTblCollection();
        final Map<String, String> userAppNames = new HashMap<>();
        for (UserAppTranslationTbl userAppTranslationTbl
                : userAppTranslationTblCollection) {
            final LanguagesTbl languagesTbl = userAppTranslationTbl.getLanguageCode();
            final String languageCode = languagesTbl.getLanguageCode();
            userAppNames.put(languageCode, name);
        }
        return userAppNames;
    }

    /**
     *
     * @param status
     * @param id
     * @return boolean
     */
    @Override
    public final boolean updateStatusById(final String status,
            final String id) {
        LOG.info(">> updateStatusById");
        boolean isUpdated = false;
        final int out
                = this.adminAreaUserAppRelJpaDao
                        .setStatusForAdminAreaUserAppRelTbl(status, id);
        LOG.debug("is Modified status value {}", out);
        if (out > 0) {
            isUpdated = true;
        }
        LOG.info("<< updateStatusById");
        return isUpdated;
    }

    /**
     *
     * @param id
     * @return boolean
     */
    @Override
    public final boolean delete(final String id) {
        LOG.info(">> delete {}", id);
        boolean isDeleted = false;
        try {
            this.adminAreaUserAppRelJpaDao.delete(id);
            isDeleted = true;
        } catch (Exception e) {
            if (e instanceof EmptyResultDataAccessException) {
                final String[] param = {id};
                throw new XMObjectNotFoundException("User not found", "P_ERR0028", param);
            }
        }

        LOG.info("<< deleteById");
        return isDeleted;
    }

    @Override
    public AdminAreaUserAppRelResponse multiDelete(Set<String> ids, HttpServletRequest httpServletRequest) {
        LOG.info(">> multiDelete");
        final List<Map<String, String>> statusMaps = new ArrayList<>();
        ids.forEach(id -> {
        	AdminAreaUserAppRelTbl adminAreaUserAppRelTbl = this.adminAreaUserAppRelJpaDao.findOne(id);
            try {
            		this.delete(id);
            		this.adminAreaUserAppAuditMgr.adminAreaUserAppMultiDeleteSuccessAudit(adminAreaUserAppRelTbl, httpServletRequest);
            } catch (XMObjectNotFoundException objectNotFound) {
                Map<String, String> statusMap = messageMaker.extractFromException(objectNotFound);
                statusMaps.add(statusMap);
            }
        });
        AdminAreaUserAppRelResponse relResponse = new AdminAreaUserAppRelResponse(statusMaps);
        LOG.info(">> multiDelete");
        return relResponse;
    }

    /**
     *
     * @param siteAdminAreaRelTbl
     * @param relType
     * @return
     */
    @Override
    public final Iterable<AdminAreaUserAppRelTbl> getAdminAreaUsrAppOnRelType(final SiteAdminAreaRelTbl siteAdminAreaRelTbl,
            final String relType) {
        LOG.info(">> getAdminAreaUsrAppOnRelType {} {}", siteAdminAreaRelTbl, relType);
        final Iterable<AdminAreaUserAppRelTbl> aauart = this.adminAreaUserAppRelJpaDao.getAdminAreaUsrAppOnRelType(siteAdminAreaRelTbl, relType);
        LOG.info("<< getAdminAreaUsrAppOnRelType");
        return aauart;
    }

    /**
     *
     * @param adminAreaUserAppRelRequest
     * @return AdminAreaUserAppRelTbl
     */
    private AdminAreaUserAppRelTbl convert2Entity(final AdminAreaUserAppRelRequest adminAreaUserAppRelRequest) {
        final String id = UUID.randomUUID().toString();
        final String status = adminAreaUserAppRelRequest.getStatus();
        final String siteAdminAreaRelId = adminAreaUserAppRelRequest.getSiteAdminAreaRelId();
        final String userAppId = adminAreaUserAppRelRequest.getUserAppId();
        final String relationType = adminAreaUserAppRelRequest.getRelationType();
        final AdminAreaUserAppRelTbl adminAreaUserAppRelTbl = new AdminAreaUserAppRelTbl(id);
        adminAreaUserAppRelTbl.setSiteAdminAreaRelId(new SiteAdminAreaRelTbl(siteAdminAreaRelId));
        adminAreaUserAppRelTbl.setStatus(status);
        adminAreaUserAppRelTbl.setRelType(relationType);
        adminAreaUserAppRelTbl.setUserApplicationId(new UserApplicationsTbl(userAppId));
        return adminAreaUserAppRelTbl;
    }

    /**
     *
     * @param adminAreaUserAppRelBatchRequest
     * @return AdminAreaUserAppRelBatchResponse
     */
    @Override
    public AdminAreaUserAppRelBatchResponse updateRelTypesByIds(AdminAreaUserAppRelBatchRequest adminAreaUserAppRelBatchRequest,
    								HttpServletRequest httpServletRequest) {
        LOG.info(">> updateRelTypesByIds");
        List<AdminAreaUserAppRelRequest> adminAreaUserAppRelRequests = adminAreaUserAppRelBatchRequest.getAdminAreaUserAppRelRequests();
        List<AdminAreaUserAppRelTbl> adminAreaUserAppRelTbls = null;
        if (!adminAreaUserAppRelRequests.isEmpty()) {
            adminAreaUserAppRelTbls = new ArrayList<>();
            for (AdminAreaUserAppRelRequest adminAreaUserAppRelRequest : adminAreaUserAppRelRequests) {
                AdminAreaUserAppRelTbl adminAreaUserAppRelTbl = this.adminAreaUserAppRelJpaDao.findByAdminAreaUserAppRelId(adminAreaUserAppRelRequest.getId());
                adminAreaUserAppRelTbl.setRelType(adminAreaUserAppRelRequest.getRelationType());
                AdminAreaUserAppRelTbl areaUserAppRelTbl = this.adminAreaUserAppRelJpaDao.save(adminAreaUserAppRelTbl);
                this.adminAreaUserAppAuditMgr.adminAreaUserAppStatusUpdateAuditor(true, areaUserAppRelTbl.getStatus(), areaUserAppRelTbl.getAdminAreaUserAppRelId(), httpServletRequest);
                adminAreaUserAppRelTbls.add(areaUserAppRelTbl);
                
            }
        }
        List<Map<String, String>> statusMaps = null;
        AdminAreaUserAppRelBatchResponse batchResponse = new AdminAreaUserAppRelBatchResponse(adminAreaUserAppRelTbls, statusMaps);
        LOG.info("<< updateRelTypesByIds");
        return batchResponse;
    }

    /**
     *
     * @param adminAreaUserAppRelBatchRequest
     * @param userName
     * @return AdminAreaUserAppRelBatchResponse
     */
    @Override
    public final AdminAreaUserAppRelBatchResponse createBatch(final AdminAreaUserAppRelBatchRequest adminAreaUserAppRelBatchRequest,
            final String userName) {
        final List<AdminAreaUserAppRelTbl> adminAreaStartAppRelTbls = new ArrayList<>();
        final List<Map<String, String>> statusMaps = new ArrayList<>();
        final List<AdminAreaUserAppRelRequest> adminAreaUserAppRelRequests
                = adminAreaUserAppRelBatchRequest.getAdminAreaUserAppRelRequests();
        for (final AdminAreaUserAppRelRequest adminAreaUserAppRelRequest : adminAreaUserAppRelRequests) {
            try {
                final AdminAreaUserAppRelTbl aauartOut = this.create(adminAreaUserAppRelRequest, userName);
                adminAreaStartAppRelTbls.add(aauartOut);
            } catch (CannotCreateRelationshipException ccre) {
                final Map<String, String> statusMap = messageMaker.extractFromException(ccre);
                statusMaps.add(statusMap);
            }
        }
        final AdminAreaUserAppRelBatchResponse adminAreaUserAppRelBatchResponse
                = new AdminAreaUserAppRelBatchResponse(adminAreaStartAppRelTbls, statusMaps);
        return adminAreaUserAppRelBatchResponse;
    }

    /**
     *
     * @param userAppId
     * @param validationRequest
     * @return AdminAreaUserAppRelWrapper
     */
    @Override
    public AdminAreaUserAppRelWrapper findAAUARelByUserAppId(final String userAppId,
            final ValidationRequest validationRequest) {
        LOG.info(">> findAAUARelByUserAppId");
        final boolean isInactiveAssignment = validator.isViewInactiveAllowed(validationRequest);
        LOG.info("findAAUARelByUserAppId isInactiveAssignment={}", isInactiveAssignment);
        final UserApplicationsTbl userApplicationsTbl = validator.validateUserApp(userAppId, isInactiveAssignment);
        List<AdminAreaUserAppRelTbl> adminAreaUserAppRelTbls = this.adminAreaUserAppRelJpaDao.findByUserApplicationId(userApplicationsTbl);
        adminAreaUserAppRelTbls = validator.filterAdminAreaUserAppRel(isInactiveAssignment, adminAreaUserAppRelTbls);
        if (adminAreaUserAppRelTbls.isEmpty()) {
            throw new XMObjectNotFoundException("No Admin Area User App relationship found for the User App", "S_ERR0009");
        }
        final List<AdminAreaUserAppRelation> aaUserAppRels = new ArrayList<>();
        for (final AdminAreaUserAppRelTbl aauart : adminAreaUserAppRelTbls) {
            try {
                validateAdminAreaStartAppRel(aauart, isInactiveAssignment);
                final AdminAreaUserAppRelation aauarel = new AdminAreaUserAppRelation();
                aauarel.setAdminAreaUserAppRelId(aauart.getAdminAreaUserAppRelId());
                aauarel.setRelType(aauart.getRelType());
                aauarel.setStatus(aauart.getStatus());
                aauarel.setSiteAdminAreaRelId(aauart.getSiteAdminAreaRelId());
                aauarel.setUserApplicationId(aauart.getUserApplicationId());
                aaUserAppRels.add(aauarel);
            } catch (XMObjectNotFoundException e) {
                LOG.info("XMObjectNotFoundException", e);
            }
        }
        final AdminAreaUserAppRelWrapper relWrapper = new AdminAreaUserAppRelWrapper(aaUserAppRels);
        LOG.info("<< findAAUARelByUserAppId");
        return relWrapper;
    }

    /**
     *
     * @param adminAreaUserAppRelTbl
     * @param isViewInactive
     */
    private void validateAdminAreaStartAppRel(final AdminAreaUserAppRelTbl adminAreaUserAppRelTbl,
            final boolean isViewInactive) {
        final String siteAdminAreaRelId = adminAreaUserAppRelTbl.getSiteAdminAreaRelId().getSiteAdminAreaRelId();
        final String siteId = adminAreaUserAppRelTbl.getSiteAdminAreaRelId().getSiteId().getSiteId();
        final String adminAreaId = adminAreaUserAppRelTbl.getSiteAdminAreaRelId().getAdminAreaId().getAdminAreaId();
        validator.validateSiteAdminArea(siteAdminAreaRelId, isViewInactive);
        validator.validateSite(siteId, isViewInactive);
        validator.validateAdminArea(adminAreaId, isViewInactive);
    }

    /**
     *
     * @param adminAreaUserAppRelTbl
     * @param isViewInactive
     */
    private void validateAdminAreaUserAppRel(final AdminAreaUserAppRelTbl adminAreaUserAppRelTbl,
            final boolean isViewInactive) {
        final String userApplicationId = adminAreaUserAppRelTbl.getUserApplicationId().getUserApplicationId();
        final String siteId = adminAreaUserAppRelTbl.getSiteAdminAreaRelId().getSiteId().getSiteId();
        validator.validateUserApp(userApplicationId, isViewInactive);
        validator.validateSite(siteId, isViewInactive);
    }

    /**
     *
     * @param adminAreaId
     * @param validationRequest
     * @return AdminAreaUserAppRelWrapper
     */
    @Override
    public AdminAreaUserAppRelWrapper findAAUserAppRelsByAAId(final String adminAreaId,
            final ValidationRequest validationRequest) {
        LOG.info(">> findAAUserAppRelsByAAId");
        List<AdminAreaUserAppRelation> adminAreaUserAppRels = null;
        final boolean isInactiveAssignment = validator.isViewInactiveAllowed(validationRequest);
        LOG.info("findAAUserAppRelsByAAId isInactiveAssignment={}", isInactiveAssignment);
        final AdminAreasTbl adminAreasTbl = validator.validateAdminArea(adminAreaId, isInactiveAssignment);
        List<SiteAdminAreaRelTbl> siteAdminAreaRelTbls = this.siteAdminAreaRelJpaDao.findByAdminAreaId(adminAreasTbl);
        siteAdminAreaRelTbls = validator.filterSiteAdminAreaRel(isInactiveAssignment, siteAdminAreaRelTbls);
        if (!siteAdminAreaRelTbls.isEmpty()) {
            List<AdminAreaUserAppRelTbl> adminAreaUserAppRelTbls = this.adminAreaUserAppRelJpaDao.findBySiteAdminAreaRelIdIn(siteAdminAreaRelTbls);
            adminAreaUserAppRelTbls = validator.filterAdminAreaUserAppRel(isInactiveAssignment, adminAreaUserAppRelTbls);
            if (!adminAreaUserAppRelTbls.isEmpty()) {
                adminAreaUserAppRels = new ArrayList<>();
                for (final AdminAreaUserAppRelTbl aauart : adminAreaUserAppRelTbls) {
                    try {
                        validateAdminAreaUserAppRel(aauart, isInactiveAssignment);
                        final AdminAreaUserAppRelation aauar = new AdminAreaUserAppRelation();
                        aauar.setAdminAreaUserAppRelId(aauart.getAdminAreaUserAppRelId());
                        aauar.setSiteAdminAreaRelId(aauart.getSiteAdminAreaRelId());
                        aauar.setUserApplicationId(aauart.getUserApplicationId());
                    	aauar.setStatus(aauart.getStatus());
                        aauar.setRelType(aauart.getRelType());
                        adminAreaUserAppRels.add(aauar);
                    } catch (XMObjectNotFoundException e) {
                        LOG.info("XMObjectNotFoundException", e);
                    }
                }
            } else {
                throw new XMObjectNotFoundException("No Relation Found", "AA_ERR0003");
            }
        } else {
            throw new XMObjectNotFoundException("Admin Areas not available for site", "S_ERR0003");
        }
        final AdminAreaUserAppRelWrapper relWrapper = new AdminAreaUserAppRelWrapper(adminAreaUserAppRels);
        LOG.info(">> findAAUserAppRelsByAAId");
        return relWrapper;
    }

	@Override
	public AdminAreaUserAppRelBatchResponse multiUpdate(List<AdminAreaUserAppRelRequest> adminAreaUserAppRequests,
			HttpServletRequest httpServletRequest) {
		LOG.info(">> multiUpdate");
		List<String> relIds = new ArrayList<>();
		adminAreaUserAppRequests.forEach(adminAreaUserAppRequest -> {
			String id = adminAreaUserAppRequest.getId();
			String status = adminAreaUserAppRequest.getStatus();
			try {
				boolean updateStatusById = this.updateStatusById(status, id);
				if (updateStatusById) {
					// success & audit it
					this.adminAreaUserAppAuditMgr.adminAreaUserAppStatusUpdateAuditor(updateStatusById, status, id, httpServletRequest);
				} else {
					relIds.add(id);
				}
			} catch (Exception ex) {
				this.adminAreaUserAppAuditMgr.adminAreaUserAppStatusUpdateFailureAuditor(id, status, ex, httpServletRequest);
			}

		});
		AdminAreaUserAppRelBatchResponse adminAreaUserAppRelBatchResponse = new AdminAreaUserAppRelBatchResponse();
		adminAreaUserAppRelBatchResponse.setStatusUpdatationFailedList(relIds);
		LOG.info("<< multiUpdate");
		return adminAreaUserAppRelBatchResponse;
	}

	
	
	@Override
	public AdminAreaUserAppRelWrapper findUserAppsByAAId(String adminAreaId, ValidationRequest validationRequest) {
		LOG.info(">> findAAUserAppRelsByAAId");
		List<AdminAreaUserAppRelation> adminAreaUserAppRels = null;
		final boolean isInactiveAssignment = validator.isViewInactiveAllowed(validationRequest);
		LOG.info("findAAUserAppRelsByAAId isInactiveAssignment={}", isInactiveAssignment);
		final AdminAreasTbl adminAreasTbl = validator.validateAdminArea(adminAreaId, isInactiveAssignment);
		List<SiteAdminAreaRelTbl> siteAdminAreaRelTbls = this.siteAdminAreaRelJpaDao.findByAdminAreaId(adminAreasTbl);
		siteAdminAreaRelTbls = validator.filterSiteAdminAreaRel(isInactiveAssignment, siteAdminAreaRelTbls);
		if (!siteAdminAreaRelTbls.isEmpty()) {
			List<AdminAreaUserAppRelTbl> adminAreaUserAppRelTbls = this.adminAreaUserAppRelJpaDao
					.findBySiteAdminAreaRelIdIn(siteAdminAreaRelTbls);
			adminAreaUserAppRelTbls = validator.filterAdminAreaUserAppRel(isInactiveAssignment,
					adminAreaUserAppRelTbls);
			if (!adminAreaUserAppRelTbls.isEmpty()) {
				adminAreaUserAppRels = new ArrayList<>();
				for (final AdminAreaUserAppRelTbl aauart : adminAreaUserAppRelTbls) {
					try {
						validateAdminAreaUserAppRel(aauart, isInactiveAssignment);
						final AdminAreaUserAppRelation aauar = new AdminAreaUserAppRelation();
						aauar.setAdminAreaUserAppRelId(aauart.getAdminAreaUserAppRelId());
						aauar.setSiteAdminAreaRelId(aauart.getSiteAdminAreaRelId());
						aauar.setUserApplicationId(aauart.getUserApplicationId());
						String userAppStatus = aauart.getUserApplicationId().getStatus();
						if (aauart.getStatus().equals(Status.INACTIVE.name()) || userAppStatus.equals(Status.INACTIVE.name())) {
							aauar.setStatus(Status.INACTIVE.name());
							aauar.getUserApplicationId().setStatus(Status.INACTIVE.name());
						} else {
							aauar.setStatus(aauart.getStatus());
						}
						aauar.setRelType(aauart.getRelType());
						adminAreaUserAppRels.add(aauar);
					} catch (XMObjectNotFoundException e) {
						LOG.info("XMObjectNotFoundException", e);
					}
				}
			} else {
				throw new XMObjectNotFoundException("No Relation Found", "AA_ERR0003");
			}
		} else {
			throw new XMObjectNotFoundException("Admin Areas not available for site", "S_ERR0003");
		}
		final AdminAreaUserAppRelWrapper relWrapper = new AdminAreaUserAppRelWrapper(adminAreaUserAppRels);
		LOG.info(">> findAAUserAppRelsByAAId");
		return relWrapper;
	}
}
