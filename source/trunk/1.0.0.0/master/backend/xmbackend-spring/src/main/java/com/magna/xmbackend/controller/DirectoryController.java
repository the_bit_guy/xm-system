/**
 * 
 */
package com.magna.xmbackend.controller;

import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.magna.xmbackend.audit.mgr.DirectoryAuditMgr;
import com.magna.xmbackend.entities.DirectoryTbl;
import com.magna.xmbackend.exception.XMObjectNotFoundException;
import com.magna.xmbackend.mgr.DirectoryManager;
import com.magna.xmbackend.vo.directory.DirectoryRequest;
import com.magna.xmbackend.vo.directory.DirectoryResponse;

/**
 * @author Bhabadyuti Bal
 *
 */
@RestController
@RequestMapping(value = "/directory")
public class DirectoryController {
	
	private static final Logger LOG = LoggerFactory.getLogger(DirectoryController.class);
	
	@Autowired
	private DirectoryManager directoryManager;
	@Autowired
	private DirectoryAuditMgr directoryAuditMgr;

	@RequestMapping(value = "/save",
            method = RequestMethod.POST,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
	public @ResponseBody final ResponseEntity<DirectoryTbl> save(HttpServletRequest request,
			@RequestBody DirectoryRequest directoryRequest){
		LOG.info("> directory save");
        DirectoryTbl dt = null;
		try {
			dt = this.directoryManager.create(directoryRequest);
			this.directoryAuditMgr.directoryCreateSuccessAudit(request, dt);
		} catch (Exception ex) {
			String directoryName = directoryRequest.getName();
			LOG.error(
                    "Exception / Error occured while creating directory with name {}",
                    directoryName);
			this.directoryAuditMgr.directoryCreateFailureAudit(request, directoryRequest, ex.getMessage().split("with ErrorCode")[0]);
		}
        LOG.info("< directory save");
        return new ResponseEntity<>(dt, HttpStatus.ACCEPTED);
	}
	
	
	/**
    *
    * @param httpServletRequest
    * @return DirectoryResponse
    */
   @RequestMapping(value = "/findAll",
           method = RequestMethod.GET,
           consumes = {MediaType.APPLICATION_JSON_VALUE,
               MediaType.APPLICATION_XML_VALUE,
               MediaType.APPLICATION_FORM_URLENCODED_VALUE},
           produces = {MediaType.APPLICATION_JSON_VALUE,
               MediaType.APPLICATION_XML_VALUE,
               MediaType.APPLICATION_FORM_URLENCODED_VALUE}
   )
   public @ResponseBody
   final ResponseEntity<DirectoryResponse> findAll(
           HttpServletRequest httpServletRequest) {
       LOG.info("> findAll");
       final DirectoryResponse dr = this.directoryManager.findAll();
       LOG.info("< findAll");
       return new ResponseEntity<>(dr, HttpStatus.OK);
   }
	
   
   /**
   *
   * @param httpServletRequest
   * @param directoryRequest
   * @return DirectoryTbl
   */
  @RequestMapping(value = "/update",
          method = RequestMethod.POST,
          consumes = {MediaType.APPLICATION_JSON_VALUE,
              MediaType.APPLICATION_XML_VALUE,
              MediaType.APPLICATION_FORM_URLENCODED_VALUE},
          produces = {MediaType.APPLICATION_JSON_VALUE,
              MediaType.APPLICATION_XML_VALUE,
              MediaType.APPLICATION_FORM_URLENCODED_VALUE}
  )
  public @ResponseBody
  final ResponseEntity<DirectoryTbl> update(
          HttpServletRequest httpServletRequest,
			@RequestBody DirectoryRequest directoryRequest) {
		LOG.info("> update");
		DirectoryTbl dt = null;
		try {
			dt = this.directoryManager.update(directoryRequest);
			final String name = dt.getName();
			final String id = dt.getDirectoryId();
			this.directoryAuditMgr.dirUpdateSuccessAudit(httpServletRequest, id, name);
		} catch (Exception ex) {
			final String name = directoryRequest.getName();
			LOG.error("Exception / Error in updating directory with name {} "
                    + "and exception msg {}", directoryRequest.getName(), ex.getMessage());
			String errMsg = "Error in updating directory with name " + name + " : " + ex.getMessage().split("with ErrorCode")[0];
			this.directoryAuditMgr.dirUpdateFailureAudit(httpServletRequest, directoryRequest, errMsg);
			throw ex;
		}
		LOG.info("< update");
		return new ResponseEntity<>(dt, HttpStatus.ACCEPTED);
	}
  
  
  /**
  *
  * @param httpServletRequest
  * @param id
  * @return boolean
  */
 @RequestMapping(value = "/delete/{id}",
         method = RequestMethod.DELETE,
         consumes = {MediaType.APPLICATION_JSON_VALUE,
             MediaType.APPLICATION_XML_VALUE,
             MediaType.APPLICATION_FORM_URLENCODED_VALUE},
         produces = {MediaType.APPLICATION_JSON_VALUE,
             MediaType.APPLICATION_XML_VALUE,
             MediaType.APPLICATION_FORM_URLENCODED_VALUE}
 )
 public @ResponseBody
 final ResponseEntity<Boolean> deleteById(
         HttpServletRequest httpServletRequest,
			@PathVariable String id) {
		LOG.info("> delete");
		String name = "";
		boolean isDeleted = false;
		try {
			final DirectoryTbl directoryTbl = this.directoryManager.findById(id);

			if (null != directoryTbl) {
				isDeleted = this.directoryManager.deleteById(id);
				this.directoryAuditMgr.dirDeleteSuccessAudit(httpServletRequest, id, directoryTbl.getName());
			} else {
				throw new XMObjectNotFoundException("Directory not found with id "+id, "ERR0022");
			}
		} catch (Exception ex) {

			LOG.error("Exception / Error in deleting directory with id " + id);
			String errMsg = "Error in deleting directory with id " + id + " and name " + name +" reason" +" : " + ex.getMessage().split("with ErrorCode")[0];
			this.directoryAuditMgr.dirDeleteFailureAudit(httpServletRequest, id, errMsg);
			throw ex;
		}
		LOG.info("< delete");
		return new ResponseEntity<>(isDeleted, HttpStatus.ACCEPTED);
	}
 
 
 @RequestMapping(value = "/multiDelete",
         method = RequestMethod.DELETE,
         consumes = {MediaType.APPLICATION_JSON_VALUE,
             MediaType.APPLICATION_XML_VALUE,
             MediaType.APPLICATION_FORM_URLENCODED_VALUE},
         produces = {MediaType.APPLICATION_JSON_VALUE,
             MediaType.APPLICATION_XML_VALUE,
             MediaType.APPLICATION_FORM_URLENCODED_VALUE}
 )
 public @ResponseBody
 final ResponseEntity<DirectoryResponse> multiDelete(
         HttpServletRequest httpServletRequest,
         @RequestBody Set<String> ids) {
     LOG.info("> multiDelete");
     final DirectoryResponse directoryResponse = this.directoryManager.multiDelete(ids, httpServletRequest);
     LOG.info("< multiDelete");
     return new ResponseEntity<>(directoryResponse, HttpStatus.ACCEPTED);
 }
 
}
