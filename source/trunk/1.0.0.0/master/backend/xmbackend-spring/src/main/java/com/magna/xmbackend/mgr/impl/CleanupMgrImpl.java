/**
 * 
 */
package com.magna.xmbackend.mgr.impl;

import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.magna.xmbackend.entities.AdminMenuConfigTbl;
import com.magna.xmbackend.entities.DirectoryRefTbl;
import com.magna.xmbackend.entities.GroupRefTbl;
import com.magna.xmbackend.entities.ProjectApplicationsTbl;
import com.magna.xmbackend.entities.ProjectsTbl;
import com.magna.xmbackend.entities.UserApplicationsTbl;
import com.magna.xmbackend.entities.UsersTbl;
import com.magna.xmbackend.jpa.dao.AdminMenuConfigJpaDao;
import com.magna.xmbackend.jpa.rel.dao.DirectoryRelJpaDao;
import com.magna.xmbackend.jpa.rel.dao.GroupRelJpaDao;
import com.magna.xmbackend.mgr.CleanupMgr;
import com.magna.xmbackend.mgr.ProjectApplicationMgr;
import com.magna.xmbackend.mgr.ProjectMgr;
import com.magna.xmbackend.mgr.UserApplicationMgr;
import com.magna.xmbackend.mgr.UserMgr;

/**
 * @author Bhabadyuti Bal
 *
 */
@Service
public class CleanupMgrImpl implements CleanupMgr {
	
	private static final Logger LOG = LoggerFactory.getLogger(CleanupMgrImpl.class);

	@Autowired
    DirectoryRelJpaDao directoryRelJpaDao;

    @Autowired
    GroupRelJpaDao groupRelJpaDao;

    @Autowired
    AdminMenuConfigJpaDao adminMenuConfigJpaDao;
    
    @Autowired
    UserMgr userMgr;

    @Autowired
    ProjectMgr projectMgr;

    @Autowired
    UserApplicationMgr userApplicationMgr;

    @Autowired
    ProjectApplicationMgr projectApplicationMgr;


	@Override
	public void removeRelationObjects(HttpServletRequest servletRequest, String parseRequestPath) {

        String subUrl = parseRequestPath.split("/")[1];
        switch (subUrl) {
            case "user":
                LOG.info("user delted!");
                Object userAttr = servletRequest.getAttribute("userId");
                if (null != userAttr) {
                    String userId = userAttr.toString();
                    UsersTbl usersTbl = this.userMgr.findById(userId);
                    if (null == usersTbl) {
                        this.removeObjectFromDirectoryRelations(userId);
                        this.removeObjectFromGroupRelations(userId);
                        this.removeObjectFromAdminMenuConfig(userId);
                    }
                }

                @SuppressWarnings("unchecked") Set<String> userIds = (Set<String>) servletRequest.getAttribute("userIds");
                if (null != userIds) {
                    userIds.forEach(uId -> {
                        UsersTbl usersTbl = this.userMgr.findById(uId);
                        if (null == usersTbl) {
                            this.removeObjectFromDirectoryRelations(uId);
                            this.removeObjectFromGroupRelations(uId);
                            this.removeObjectFromAdminMenuConfig(uId);
                        }
                    });
                }
                break;

            case "project":
                LOG.info("project deleted!");
                Object projectAttr = servletRequest.getAttribute("projectId");
                if (null != projectAttr) {
                    String projectId = projectAttr.toString();
                    ProjectsTbl projectsTbl = this.projectMgr.findById(projectId);
                    if (null == projectsTbl) {
                        this.removeObjectFromDirectoryRelations(projectId);
                        this.removeObjectFromGroupRelations(projectId);
                    }
                }

                @SuppressWarnings("unchecked") Set<String> projectIds = (Set<String>) servletRequest.getAttribute("projectIds");
                if (null != projectIds) {
                    projectIds.forEach(pId -> {
                        UsersTbl usersTbl = this.userMgr.findById(pId);
                        if (null == usersTbl) {
                            this.removeObjectFromDirectoryRelations(pId);
                            this.removeObjectFromGroupRelations(pId);
                        }
                    });
                }
                break;
            case "userApplication":
                LOG.info("userApplication deleted!");
                Object userAppAttr = servletRequest.getAttribute("userAppId");
                if (null != userAppAttr) {
                    String userAppId = userAppAttr.toString();
                    UserApplicationsTbl userApplicationsTbl = this.userApplicationMgr.findById(userAppId);
                    if (null == userApplicationsTbl) {
                        this.removeObjectFromDirectoryRelations(userAppId);
                        this.removeObjectFromGroupRelations(userAppId);
                        this.removeObjectFromAdminMenuConfig(userAppId);
                    }
                }

                @SuppressWarnings("unchecked") Set<String> userAppIds = (Set<String>) servletRequest.getAttribute("userAppIds");
                if (null != userAppIds) {
                    userAppIds.forEach(userApId -> {
                        UserApplicationsTbl userApplicationsTbl = this.userApplicationMgr.findById(userApId);
                        if (null == userApplicationsTbl) {
                            this.removeObjectFromDirectoryRelations(userApId);
                            this.removeObjectFromGroupRelations(userApId);
                            this.removeObjectFromAdminMenuConfig(userApId);
                        }
                    });
                }
                break;
            case "projectApplication":
                LOG.info("projectApplication deleted!");
                Object projectAppAttr = servletRequest.getAttribute("projectAppId");
                if (null != projectAppAttr) {
                    String projectAppId = projectAppAttr.toString();
                    ProjectApplicationsTbl projectApplicationsTbl = this.projectApplicationMgr.findById(projectAppId);
                    if (null == projectApplicationsTbl) {
                        this.removeObjectFromDirectoryRelations(projectAppId);
                        this.removeObjectFromGroupRelations(projectAppId);
                        this.removeObjectFromAdminMenuConfig(projectAppId);
                    }
                }

                @SuppressWarnings("unchecked") Set<String> projectAppIds = (Set<String>) servletRequest.getAttribute("projectAppIds");
                if (null != projectAppIds) {
                    projectAppIds.forEach(projApId -> {
                        ProjectApplicationsTbl projectApplicationsTbl = this.projectApplicationMgr.findById(projApId);
                        if (null == projectApplicationsTbl) {
                            this.removeObjectFromDirectoryRelations(projApId);
                            this.removeObjectFromGroupRelations(projApId);
                            this.removeObjectFromAdminMenuConfig(projApId);
                        }
                    });
                }
                break;
            default:
                break;
        }
    
		
	}

	@Override
	public void removeObjectFromDirectoryRelations(String objectId) {
		final List<DirectoryRefTbl> directoryRefTbls = this.directoryRelJpaDao.findByObjectId(objectId);
        if (!directoryRefTbls.isEmpty()) {
            this.directoryRelJpaDao.delete(directoryRefTbls);
        }
	}

	@Override
	public void removeObjectFromGroupRelations(String objectId) {
		List<GroupRefTbl> grpRefTbls = this.groupRelJpaDao.findByObjectId(objectId);
        if (!grpRefTbls.isEmpty()) {
            this.groupRelJpaDao.delete(grpRefTbls);
        }
	}

	@Override
	public void removeObjectFromAdminMenuConfig(String objectId) {
		List<AdminMenuConfigTbl> adminMenuConfTbls = this.adminMenuConfigJpaDao.findByObjectId(objectId);
        if (!adminMenuConfTbls.isEmpty()) {
            this.adminMenuConfigJpaDao.delete(adminMenuConfTbls);
        }
	}

}
