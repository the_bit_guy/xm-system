/**
 * 
 */
package com.magna.xmbackend.mgr.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import javax.persistence.EntityManager;
import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Component;

import com.magna.xmbackend.audit.mgr.DirectoryAuditMgr;
import com.magna.xmbackend.entities.DirectoryTbl;
import com.magna.xmbackend.entities.DirectoryTranslationTbl;
import com.magna.xmbackend.entities.IconsTbl;
import com.magna.xmbackend.entities.LanguagesTbl;
import com.magna.xmbackend.exception.CannotCreateObjectException;
import com.magna.xmbackend.exception.XMObjectNotFoundException;
import com.magna.xmbackend.jpa.dao.DirectoryJpaDao;
import com.magna.xmbackend.mgr.DirectoryManager;
import com.magna.xmbackend.utils.MessageMaker;
import com.magna.xmbackend.vo.directory.DirectoryRequest;
import com.magna.xmbackend.vo.directory.DirectoryResponse;
import com.magna.xmbackend.vo.directory.DirectoryTransalation;

/**
 * @author Bhabadyuti Bal
 *
 */
@Component
public class DirectoryManagerImpl implements DirectoryManager {

	private static final Logger LOG = LoggerFactory.getLogger(DirectoryManagerImpl.class);

	@Autowired
	DirectoryJpaDao directoryJpaDao;

	@Autowired
	EntityManager entityManager;
	@Autowired
	private MessageMaker messageMaker;
	@Autowired
	private DirectoryAuditMgr directoryAuditMgr;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.magna.xmbackend.mgr.DirectoryManager#create(com.magna.xmbackend.vo.
	 * directory.DirectoryRequest)
	 */
	@Override
	public final DirectoryTbl create(DirectoryRequest directoryRequest) {
		LOG.info(">> directory create");
		DirectoryTbl dirTblIn = this.convert2Entity(directoryRequest, false);

		DirectoryTbl dirTblOut = directoryJpaDao.save(dirTblIn);
		LOG.info("<< directory create");
		return dirTblOut;
	}
	
   /**
    * Find directory id for name.
    *
    * @param dirName the dir name
    * @return the string
    */
   private String findDirectoryIdForName(final String dirName) {
	   final DirectoryTbl directoryTbl = this.directoryJpaDao.findByNameIgnoreCase(dirName);
       String dirId = null;
       if (directoryTbl != null) {
           dirId = directoryTbl.getDirectoryId();
       }
       LOG.info(">> findDirectoryIdForName - Directory id for name is {} - {}",  dirName, dirId);
       return dirId;
   }

	/**
	 * Find directory for name.
	 *
	 * @param dirName the dir name
	 * @return the directory tbl
	 */
	public DirectoryTbl findDirectoryForName(String dirName) {
		LOG.info(">> directory findDirectoryIdForName");
		final DirectoryTbl directoryTbl = directoryJpaDao.findByNameIgnoreCase(dirName);

		if (null == directoryTbl) {
            final String[] param = {dirName};
            throw new XMObjectNotFoundException("Directory with name not found", "U_ERR0005", param);
        }
		
		LOG.info("<< directory findDirectoryIdForName");
		return directoryTbl;
	}

	private DirectoryTbl convert2Entity(DirectoryRequest directoryRequest, final boolean isUpdate) {
		LOG.info(">> convert2Entity");
		String id = UUID.randomUUID().toString();
		LOG.info(">>> ID: " + id);
		if (isUpdate) {
			id = directoryRequest.getId();
		}
		final String dirName = directoryRequest.getName();
		final Date date = new Date();
		final String iconId = directoryRequest.getIconId();
		final List<DirectoryTransalation> directoryTranslations = directoryRequest.getDirectoryTranslations();

		LOG.info("dirName>>> " + dirName);
		if (!isUpdate && null != this.findDirectoryIdForName(dirName)) {
			throw new CannotCreateObjectException("Directory name already found", "ERR0002");
		}
		
		final DirectoryTbl directoryTbl = new DirectoryTbl(id);
		directoryTbl.setName(dirName);
		directoryTbl.setIconId(new IconsTbl(iconId));
		directoryTbl.setCreateDate(date);
		directoryTbl.setUpdateDate(date);

		final List<DirectoryTranslationTbl> directoryTranslationTbls = new ArrayList<>();

		for (DirectoryTransalation dt : directoryTranslations) {
			String transId = UUID.randomUUID().toString();
			LOG.info("transId>>> " + transId);
			if (isUpdate) {
				transId = dt.getId();
			}

			final String languageCode = dt.getLanguageCode();
			final String remarks = dt.getRemarks();
			final String dirDesc = dt.getDescription();
			final DirectoryTranslationTbl directoryTranslationTbl = new DirectoryTranslationTbl(transId);
			directoryTranslationTbl.setCreateDate(date);
			directoryTranslationTbl.setUpdateDate(date);
			directoryTranslationTbl.setDescription(dirDesc);
			directoryTranslationTbl.setRemarks(remarks);
			directoryTranslationTbl.setLanguageCode(new LanguagesTbl(languageCode));
			directoryTranslationTbl.setDirectoryId(new DirectoryTbl(id));
			directoryTranslationTbls.add(directoryTranslationTbl);
		}

		directoryTbl.setDirectoryTranslationTblCollection(directoryTranslationTbls);
		LOG.info("<< convert2Entity");
		return directoryTbl;
	}

	@Override
	public DirectoryResponse findAll() {
		LOG.info(">> findAll");
		final Iterable<DirectoryTbl> diIterables = this.directoryJpaDao.findAll();
		final DirectoryResponse directoryResponse = new DirectoryResponse(diIterables);
		LOG.info("<< findAll");
		return directoryResponse;
	}
	
	
	@Override
	public DirectoryTbl update(DirectoryRequest directoryRequest) {
		LOG.info(">> update");
        final DirectoryTbl drIn = this.convert2Entity(directoryRequest, true);
        final DirectoryTbl drOut = this.directoryJpaDao.save(drIn);
        LOG.info("<< update");
		return drOut;
	}

	@Override
	public boolean deleteById(String id) {
		LOG.info("> deleteById");
		boolean isDeleted = false;
		try {
			this.directoryJpaDao.delete(id);
			isDeleted = true;
		} catch (Exception e) {
			if (e instanceof EmptyResultDataAccessException) {
				final String[] param = {id};
				throw new XMObjectNotFoundException("Directory not found", "P_ERR0018", param);
			}
		}
		
		LOG.info("< deleteById");
		return isDeleted;
	}

	@Override
	public DirectoryResponse multiDelete(Set<String> dirIds, HttpServletRequest hsr) {
		LOG.info(">> multiDelete");
		final List<Map<String, String>> statusMaps = new ArrayList<>();
		dirIds.forEach(dirId -> {
			String name = "";
			try {
				DirectoryTbl dirTbl2Del = this.findById(dirId);
				if (dirTbl2Del != null) {
					name = dirTbl2Del.getName();
				}
				this.directoryAuditMgr.dirDeleteSuccessAudit(hsr, dirId, name);
			} catch (XMObjectNotFoundException objectNotFound) {
                String errorMsg;
                if (!"".equalsIgnoreCase(dirId)) {
                    errorMsg = "Directory with id " + dirId + " cannot be deleted";
                } else {
                    errorMsg = "Directory with id " + dirId + " and with name "
                            + name + "cannot be deleted";
                }
                this.directoryAuditMgr.dirDeleteFailureAudit(hsr, name, errorMsg);
                Map<String, String> statusMap = messageMaker.extractFromException(objectNotFound);
                statusMaps.add(statusMap);
            }
		});
		DirectoryResponse directoryResponse = new DirectoryResponse(statusMaps);
		LOG.info(">> multiDelete");
		return directoryResponse;
	}

	@Override
	public DirectoryTbl findById(String id) { 
		DirectoryTbl dirTbl = null;
		if(null != id) {
			dirTbl = this.directoryJpaDao.findOne(id);
		}
		return dirTbl;
	}

}
