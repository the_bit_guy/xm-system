package com.magna.xmbackend.mgr.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.magna.xmbackend.entities.RolePermissionRelTbl;
import com.magna.xmbackend.entities.RolesTbl;
import com.magna.xmbackend.jpa.dao.RolePermissionRelJpaDao;
import com.magna.xmbackend.mgr.RolePermissionRelMgr;

// TODO: Auto-generated Javadoc
/**
 * The Class RolePermissionRelMgrImpl.
 */
@Component
public class RolePermissionRelMgrImpl implements RolePermissionRelMgr {
	
	/** The Constant LOG. */
	private static final Logger LOG = LoggerFactory.getLogger(RolePermissionRelMgrImpl.class);

	/** The role permission rel jpa dao. */
	@Autowired
	private RolePermissionRelJpaDao rolePermissionRelJpaDao;

	/* (non-Javadoc)
	 * @see com.magna.xmbackend.mgr.RolePermissionRelMgr#findPermissionByRoleId(java.lang.String)
	 */
	@Override
	public Iterable<RolePermissionRelTbl> findPermissionByRoleId(String roleId) {
		LOG.info(">> findPermissionByRoleId");
        Iterable<RolePermissionRelTbl> rolePermissionRelTbls = this.rolePermissionRelJpaDao.findRolePermissionRelTblByRoleId(new RolesTbl(roleId));
        LOG.info("<< findById");
        return rolePermissionRelTbls;
	}
}
