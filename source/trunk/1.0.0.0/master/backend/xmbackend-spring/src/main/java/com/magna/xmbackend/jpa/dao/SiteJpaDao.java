/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magna.xmbackend.jpa.dao;

import java.util.Date;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.magna.xmbackend.entities.SitesTbl;

/**
 *
 * @author dhana
 */
@Transactional
public interface SiteJpaDao extends CrudRepository<SitesTbl, String> {

    @Modifying
    @Query("update SitesTbl st set st.status = :status, st.updateDate = :updateDate where st.siteId = :id")
    int setStatusAndUpdateDateForSiteTbl(
            @Param("status") String status,
            @Param("updateDate") Date updateDate,
            @Param("id") String id);
    
    SitesTbl findByNameIgnoreCase(String name);
}
