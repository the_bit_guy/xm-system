/**
 *
 */
package com.magna.xmbackend.rel.mgr;

import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import com.magna.xmbackend.entities.GroupRefTbl;
import com.magna.xmbackend.vo.group.GroupRelBatchRequest;
import com.magna.xmbackend.vo.group.GroupRelBatchResponse;
import com.magna.xmbackend.vo.group.GroupRelRequest;
import com.magna.xmbackend.vo.group.GroupRelResponse;

/**
 * @author Bhabadyuti Bal
 *
 */
public interface GroupRelMgr {

    /**
     *
     * @param batchRequest
     * @param userName
     * @return GroupRelResponse
     */
    GroupRelBatchResponse createBatch(final GroupRelBatchRequest batchRequest,
            final String userName);

    /**
     *
     * @param groupRelRequest
     * @return GroupRefTbl
     */
    public GroupRefTbl create(final GroupRelRequest groupRelRequest);

    /**
     *
     * @param groupId
     * @param userName
     * @return GroupRelResponse
     */
    public List<GroupRelResponse> findGroupRelsByGroupId(final String groupId,
            final String userName);

    /**
     *
     * @param groupReIds
     * @return boolean
     */
    boolean multiDelete(final Set<String> groupReIds, HttpServletRequest httpServletRequest);
}
