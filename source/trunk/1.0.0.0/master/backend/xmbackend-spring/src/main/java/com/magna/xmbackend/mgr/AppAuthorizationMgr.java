/**
 * 
 */
package com.magna.xmbackend.mgr;

import com.magna.xmbackend.vo.user.UserCredential;

/**
 * @author Bhabadyuti Bal
 *
 */
public interface AppAuthorizationMgr {

	public boolean isAppUser(final UserCredential userCredential);
	
	public boolean isAppUserActive(final UserCredential userCredential);
	
	public boolean isXMSystemUser(String username);

	public boolean isXMSystemUserActive(String username);

	public boolean checkXMSystemUserPermissionForAdminHotlineBatchAccess(String username, String applicationName);
}
