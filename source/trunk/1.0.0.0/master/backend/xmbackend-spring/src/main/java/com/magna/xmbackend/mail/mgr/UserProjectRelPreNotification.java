/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magna.xmbackend.mail.mgr;

import com.magna.xmbackend.entities.EmailNotificationConfigTbl;
import com.magna.xmbackend.entities.UserProjectRelTbl;

/**
 *
 * @author dhana
 */
public interface UserProjectRelPreNotification {

    void postToQueue(UserProjectRelTbl userProjectRelTbl, String event);
    
    void postToQueueForProjectExpiry(UserProjectRelTbl userProjectRelTbl, EmailNotificationConfigTbl configTbl, long projectUsageGap, long gracePeriod);
}
