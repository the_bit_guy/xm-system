/**
 * 
 */
package com.magna.xmbackend.controller;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.magna.xmbackend.mgr.HotlineMgr;
import com.magna.xmbackend.utils.Validator;
import com.magna.xmbackend.vo.hotline.HotlineSaveRequest;
import com.magna.xmbackend.vo.hotline.HotlineSaveResponse;
import com.magna.xmbackend.vo.permission.ValidationRequest;

/**
 * @author Bhabadyuti Bal
 *
 */
@RestController
@RequestMapping(value = "/hotline")
public class HotlineController {
	private static Logger LOG = LoggerFactory.getLogger(HotlineController.class);
	
	@Autowired
	private HotlineMgr hotlineMgr;

	@Autowired
	private Validator validator;

	@RequestMapping(value = "/save",
            method = RequestMethod.POST,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
	public @ResponseBody final ResponseEntity<HotlineSaveResponse> hotlineSave(HttpServletRequest httpServletRequest,
            @RequestBody HotlineSaveRequest hotlineSaveRequest){
		LOG.info("> save");
		String userName = (String)httpServletRequest.getAttribute("userName");
		final ValidationRequest validationRequest = validator.formObjectValidationRequest(httpServletRequest, "SITE");
		HotlineSaveResponse hotlineSaveResponse = this.hotlineMgr.save(hotlineSaveRequest, userName, validationRequest);
		LOG.info("< save");
		return new ResponseEntity<>(hotlineSaveResponse, HttpStatus.ACCEPTED);
	}
}
