/**
 * 
 */
package com.magna.xmbackend.mgr.impl;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.magna.xmbackend.entities.PermissionTbl;
import com.magna.xmbackend.entities.RoleUserRelTbl;
import com.magna.xmbackend.entities.RolesTbl;
import com.magna.xmbackend.entities.UsersTbl;
import com.magna.xmbackend.jpa.dao.PermissionJpaDao;
import com.magna.xmbackend.jpa.dao.UserJpaDao;
import com.magna.xmbackend.jpa.rel.dao.RoleUserRelJpaDao;
import com.magna.xmbackend.mgr.AppAuthorizationMgr;
import com.magna.xmbackend.mgr.UserMgr;
import com.magna.xmbackend.vo.enums.Application;
import com.magna.xmbackend.vo.enums.Status;
import com.magna.xmbackend.vo.user.UserCredential;

/**
 * @author Bhabadyuti Bal
 *
 */
@Component
public class AppAuthorizationMgrImpl implements AppAuthorizationMgr {
	
	private static final Logger LOG = LoggerFactory.getLogger(AppAuthorizationMgrImpl.class);
	
	@Autowired
	private UserJpaDao userJpaDao;
	@Autowired
	private RoleUserRelJpaDao roleUserRelJpaDao;
	@Autowired
	private PermissionJpaDao permissionJpaDao;
	@Autowired
	private UserMgr userMgr;
	

	
	
	@Override
	public boolean isAppUser(UserCredential userCredential) {
		LOG.info(">> isAppUser");
		boolean isAppUser = false;
		try {
			String username = userCredential.getUsername();
			UsersTbl usersTbl = this.userJpaDao.findByUsernameIgnoreCase(username);
			if (usersTbl != null) {
				isAppUser = true;
			}
		} catch (Exception ex) {
			LOG.error("Error / Exception occured in isAppUser > "+ex.getMessage());
		}
		LOG.info("<< isAppUser");
		return isAppUser;
	}

	@Override
	public boolean isAppUserActive(UserCredential userCredential) {
		LOG.info(">> isAppUserActive");
		boolean isActiveUser = false;
		String username = userCredential.getUsername();
		UsersTbl usersTbl = this.userJpaDao.findByUsernameIgnoreCaseAndStatus(username, Status.ACTIVE.name());
		if (usersTbl != null) {
			isActiveUser = true;
		}
		LOG.info("<< isAppUserActive");
		return isActiveUser;
	}
	


	/* Check if the current user is a xmsystem user or not. User should be available in UsersTbl
	 * @return boolean
	 * @see com.magna.xmbackend.mgr.AppAuthorizationMgr#isXMSystemUser(java.lang.String)
	 */
	@Override
	public boolean isXMSystemUser(String username) {
		LOG.info(">> isXMSystemUser");
		boolean isAppUser = false;
		try {
			UsersTbl usersTbl = this.userJpaDao.findByUsername(username);
			if (usersTbl != null) {
				isAppUser = true;
			}
		} catch (Exception ex) {
			LOG.error("Error / Exception occured in isAppUser > "+ex.getMessage());
		}
		LOG.info("<< isXMSystemUser");
		return isAppUser;
	}
	
	/* Check if the current user is active/inactive
	 * @return boolean
	 * @see com.magna.xmbackend.mgr.AppAuthorizationMgr#isXMSystemUserActive(java.lang.String)
	 */
	@Override
	public boolean isXMSystemUserActive(String username) {
		LOG.info(">> isXMSystemUserActive");
		boolean isActiveUser = false;
		UsersTbl usersTbl = this.userJpaDao.findByUsernameIgnoreCaseAndStatus(username, Status.ACTIVE.name());
		if (usersTbl != null) {
			isActiveUser = true;
		}
		LOG.info("<< isXMSystemUserActive");
		return isActiveUser;
	}


	/* Check for user permissions to access specific application 
	 * @return boolean
	 * @see com.magna.xmbackend.mgr.AppAuthorizationMgr#checkXMSystemUserPermissionForAdminHotlineBatchAccess(java.lang.String, java.lang.String)
	 */
	@Override
	public boolean checkXMSystemUserPermissionForAdminHotlineBatchAccess(String username, String appName) {
		LOG.info(">> checkXMSystemUserPermissionForAdminHotlineBatchAccess");
		final boolean appUser = this.isXMSystemUser(username);
		Set<String> permissisonNames = new HashSet<>();
		if (appUser) {
			//Get all the permission for the current user
			UsersTbl userTbl = this.userJpaDao.findByUsernameIgnoreCase(username);
			if (null != userTbl) {
				List<RoleUserRelTbl> roleUserTbls = this.roleUserRelJpaDao.findByUserId(userTbl);
				roleUserTbls.forEach(roleUsrTbl -> {
					RolesTbl roleTbl = roleUsrTbl.getRoleId();
					Iterable<PermissionTbl> permissionTbls = this.permissionJpaDao
							.findObjectPermissionByRoleId(roleTbl);
					permissionTbls.forEach(permTbl -> {
						permissisonNames.add(permTbl.getName());
					});
				});
				if (!permissisonNames.isEmpty()) {
					if (!permissisonNames.isEmpty()) {
						//if current user has LOGIN_CAX_START_ADMIN permission then, Admin is accessible
						if (appName.equals(Application.CAX_START_ADMIN.name())) {
							if (permissisonNames.contains("LOGIN_CAX_START_ADMIN")) {
								return true;
							}
						}
						//if current user has LOGIN_CAX_START_HOTLINE permission then, Hotline is accessible
						if (appName.equals(Application.CAX_START_HOTLINE.name())) {
							if (permissisonNames.contains("LOGIN_CAX_START_HOTLINE")) {
								return true;
							}
						}
						//If current user has VIEW_INACTIVE permission and LOGIN_CAX_START_ADMIN/ LOGIN_CAX_START_HOTLINE then Batch is accessible
						if (appName.equals(Application.CAX_START_BATCH.name())) {
							if (permissisonNames.contains("VIEW_INACTIVE") 
									&& permissisonNames.contains("LOGIN_CAX_START_ADMIN") || permissisonNames.contains("LOGIN_CAX_START_HOTLINE")) {
								return true;
							}
						}
						//if current user has CAX_ADMIN_MENU permission then Menu is accessible.
						if (appName.equals(Application.CAX_ADMIN_MENU.name())) {
							if (this.userMgr.isSuperAdmin(username)) {
								return true;
							}
						}
					}}
			}
		}

		LOG.info("<< checkXMSystemUserPermissionForAdminHotlineBatchAccess");
		return false;}
	
}
