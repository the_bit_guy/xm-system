/**
 * 
 */
package com.magna.xmbackend.audit.mgr;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.magna.xmbackend.entities.UsersTbl;
import com.magna.xmbackend.vo.user.UserRequest;
import com.magna.xmbackend.vo.user.UserResponse;

/**
 * @author Bhabadyuti Bal
 *
 */
public interface UserAuditMgr {


	void userMultiSaveFailureAuditor(HttpServletRequest httpServletRequest, Exception ex);

	void userMultiSaveSuccessAuditor(HttpServletRequest httpServletRequest, List<UserRequest> userRequests,
			UserResponse response);

	void userDeleteSuccessAudit(HttpServletRequest httpServletRequest, String userId, String name);
	
	void userDeleteFailureAudit(HttpServletRequest httpServletRequest, String userId, String name);

	void userUpdateSuccessAudit(HttpServletRequest httpServletRequest, UserRequest userRequest);

	void userUpdateFailureAudit(HttpServletRequest httpServletRequest, UserRequest userRequest, String message);

	void userStatusUpdateSuccessAuditor(String userName, String status, HttpServletRequest httpServletRequest);

	void userStatusUpdateFailureAuditor(String userName, String status, Exception ex,
			HttpServletRequest httpServletRequest);

	void createSuccessAuditForBatch(UsersTbl userTbl, HttpServletRequest httpServletRequest);

	void createFailureAuditForBatch(UserRequest userRequest, HttpServletRequest httpServletRequest);

}
