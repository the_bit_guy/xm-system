package com.magna.xmbackend.rel.mgr;

import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import com.magna.xmbackend.vo.permission.ValidationRequest;
import com.magna.xmbackend.vo.rel.RoleUserRelRequest;
import com.magna.xmbackend.vo.rel.RoleUserRelResponse;

/**
 *
 * @author vijay
 */
public interface RoleUserRelMgr {

    /**
     *
     * @param roleUserRelRequestList
     * @param userName
     * @return RoleUserRelResponse
     */
    public RoleUserRelResponse create(final List<RoleUserRelRequest> roleUserRelRequestList,
            final String userName);

    /**
     *
     * @param roleUserRelRequestList
     * @param userName
     * @return RoleUserRelResponse
     */
    public RoleUserRelResponse createAll(final List<RoleUserRelRequest> roleUserRelRequestList,
            final String userName);

    /**
     *
     * @param roleUserRelId
     * @return boolean
     */
    public boolean deleteById(final String roleUserRelId);
    
    
    RoleUserRelResponse multiDelete(final Set<String> ids, HttpServletRequest httpServletRequest);

    /**
     *
     * @param roleId
     * @param validationRequest 
     * @return RoleUserRelResponse
     */
    public RoleUserRelResponse findRoleUserRelTblByRoleId(final String roleId, final ValidationRequest validationRequest);

    /**
     *
     * @param roleId
     * @param roleAdminAreaRelId
     * @param validationRequest 
     * @return RoleUserRelResponse
     */
    public RoleUserRelResponse findRoleUserRelTblByRoleIdRoleAARelId(final String roleId,
            final String roleAdminAreaRelId, final ValidationRequest validationRequest);

}
