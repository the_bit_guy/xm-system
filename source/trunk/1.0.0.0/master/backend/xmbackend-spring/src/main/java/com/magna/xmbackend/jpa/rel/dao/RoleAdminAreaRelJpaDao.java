package com.magna.xmbackend.jpa.rel.dao;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.magna.xmbackend.entities.AdminAreasTbl;
import com.magna.xmbackend.entities.RoleAdminAreaRelTbl;
import com.magna.xmbackend.entities.RolesTbl;

/**
 *
 * @author vijay
 */
@Transactional
public interface RoleAdminAreaRelJpaDao extends CrudRepository<RoleAdminAreaRelTbl, String> {

    /**
     *
     * @param roleId
     * @return RoleUserRelTbl
     */
    @Query("from RoleAdminAreaRelTbl RAART where RAART.roleId=:roleId")
    Iterable<RoleAdminAreaRelTbl> findRoleAdminAreaRelTblByRoleId(@Param("roleId") RolesTbl roleId);
    
    @Query("select aa.adminAreaId, aa.name, u.username, r.name FROM UsersTbl u, RoleUserRelTbl ru, RoleAdminAreaRelTbl ra, AdminAreasTbl aa, RolesTbl r "
    		+ "where u.userId = ru.userId AND ru.roleAdminAreaRelId = ra.roleAdminAreaRelId AND aa.adminAreaId = ra.adminAreaId AND "
    		+ "r.roleId = ra.roleId AND r.roleId = ru.roleId AND  u.username = :username")
    List<Object[]> findRoleAdminAreaUsersRelTblsByRoleId(@Param("username") String username);
    
    
    @Query("select RAART.adminAreaId from RoleAdminAreaRelTbl RAART where RAART.roleId=:roleId")
    List<AdminAreasTbl> findAdminAreasTblByRoleId(@Param("roleId") RolesTbl roleId);
    

    /**
     *
     * @param adminAreaId
     * @return RoleUserRelTbl
     */
    @Query("from RoleAdminAreaRelTbl RAART where RAART.adminAreaId=:adminAreaId")
    RoleAdminAreaRelTbl findRoleAdminAreaRelTblByAAId(@Param("adminAreaId") AdminAreasTbl adminAreaId);
}
