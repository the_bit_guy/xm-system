package com.magna.xmbackend.jpa.rel.dao;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.magna.xmbackend.entities.AdminAreaUserAppRelTbl;
import com.magna.xmbackend.entities.AdminAreasTbl;
import com.magna.xmbackend.entities.SiteAdminAreaRelTbl;
import com.magna.xmbackend.entities.UserApplicationsTbl;

/**
 *
 * @author vijay
 */
@Transactional
public interface AdminAreaUserAppRelJpaDao extends CrudRepository<AdminAreaUserAppRelTbl, String> {

    /**
     *
     * @param status
     * @param id
     * @return integer
     */
    @Modifying
    @Query("UPDATE AdminAreaUserAppRelTbl aauart set aauart.status = :status where aauart.adminAreaUserAppRelId = :id")
    int setStatusForAdminAreaUserAppRelTbl(@Param("status") String status, @Param("id") String id);

    /**
     *
     * @param siteAdminAreaRelId
     * @param relType
     * @return AdminAreaUserAppRelTbl
     */
    @Modifying
    @Query("SELECT adminAreaUsrApp From AdminAreaUserAppRelTbl adminAreaUsrApp where adminAreaUsrApp.siteAdminAreaRelId=:siteAdminAreaRelId and lower(adminAreaUsrApp.relType)=lower(:relType)")
    Iterable<AdminAreaUserAppRelTbl> getAdminAreaUsrAppOnRelType(@Param("siteAdminAreaRelId") SiteAdminAreaRelTbl siteAdminAreaRelId,
            @Param("relType") String relType);

    @Modifying
    @Query("UPDATE AdminAreaUserAppRelTbl aauart set aauart.relType = :relType where aauart.adminAreaUserAppRelId = :id")
    int setRelTypeForAdminAreaUserAppRelTbl(@Param("relType") String relType, @Param("id") String id);
    
    /**
     * 
     * @param userApplicationId
     * @return List<AdminAreaUserAppRelTbl>
     */
    List<AdminAreaUserAppRelTbl> findByUserApplicationId(UserApplicationsTbl userApplicationId);
    
    /**
     * 
     * @param adminAreaRelTbls
     * @param status
     * @return List<AdminAreaUserAppRelTbl>
     */
    List<AdminAreaUserAppRelTbl> findBySiteAdminAreaRelIdInAndStatus(List<SiteAdminAreaRelTbl> adminAreaRelTbls, String status);
    
    /**
     * 
     * @param adminAreaRelTbls
     * @return List<AdminAreaUserAppRelTbl>
     */
    List<AdminAreaUserAppRelTbl> findBySiteAdminAreaRelIdIn(List<SiteAdminAreaRelTbl> adminAreaRelTbls);
    
    /**
     * 
     * @param id
     * @return AdminAreaUserAppRelTbl
     */
    AdminAreaUserAppRelTbl findByAdminAreaUserAppRelId(String id);
    
    
    @Query("from AdminAreaUserAppRelTbl AAUART where AAUART.status IN (:status) and AAUART.siteAdminAreaRelId in "
    		+ "(select SAART.siteAdminAreaRelId from SiteAdminAreaRelTbl SAART where SAART.adminAreaId = :adminAreaId) "
    		+ " and AAUART.userApplicationId in (select UAT.userApplicationId from UserApplicationsTbl UAT where UAT.status IN (:status)"
    		+ " and UAT.position in ('*ButtonTask*', '*IconTask*', '*MenuTask*'))")
    List<AdminAreaUserAppRelTbl> findAdminAreaUserAppRelTbls(@Param("adminAreaId") AdminAreasTbl adminAreaId, @Param("status") List<String> status);
    
    
    List<AdminAreaUserAppRelTbl> findBySiteAdminAreaRelId(SiteAdminAreaRelTbl siteAdminAreaRelId);
    
}
