/**
 * 
 */
package com.magna.xmbackend.mgr;

import com.magna.xmbackend.entities.CssSettingsTbl;
import com.magna.xmbackend.vo.css.CssSettingsRequest;

// TODO: Auto-generated Javadoc
/**
 * The Interface CssSettingsMgr.
 *
 * @author Bhabadyuti Bal
 */
public interface CssSettingsMgr {

	
	/**
	 * Save.
	 *
	 * @param userName the user name
	 * @return true, if successful
	 */
	//boolean save( String isApplied, MultipartFile files, String userName);

	/**
	 * Find css settings for user.
	 *
	 * @param userName the user name
	 * @return the css settings response
	 */
	CssSettingsTbl findCssSettingsForUser(String userName);


	/**
	 * Delete css settings.
	 *
	 * @param userName the user name
	 * @return true, if successful
	 */
	boolean deleteCssSettings(String userName);

	
	/**
	 * Creates the or update.
	 *
	 * @param cssSettingsRequest the css settings request
	 * @param userName the user name
	 * @return true, if successful
	 */
	boolean createOrUpdate(CssSettingsRequest cssSettingsRequest, String userName);


	/**
	 * Update css settings.
	 *
	 * @param userName the user name
	 * @param isApplied the is applied
	 * @return true, if successful
	 */
	boolean updateCssSettings(String userName, String isApplied);
}
