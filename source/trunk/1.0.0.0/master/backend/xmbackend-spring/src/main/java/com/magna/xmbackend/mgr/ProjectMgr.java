package com.magna.xmbackend.mgr;

import java.util.List;
import java.util.Set;

import com.magna.xmbackend.entities.ProjectsTbl;
import com.magna.xmbackend.vo.permission.ValidationRequest;
import com.magna.xmbackend.vo.project.ProjectRequest;
import com.magna.xmbackend.vo.project.ProjectResponse;
import javax.servlet.http.HttpServletRequest;

// TODO: Auto-generated Javadoc
/**
 * The Interface ProjectMgr.
 *
 * @author dhana
 */
public interface ProjectMgr {

    /**
     * Find all.
     *
     * @param validationRequest the validation request
     * @return ProjectResponse
     */
    ProjectResponse findAll(final ValidationRequest validationRequest);

    /**
     * Save.
     *
     * @param projectRequest the project request
     * @return ProjectsTbl
     */
    ProjectsTbl save(final ProjectRequest projectRequest);

    /**
     * Update.
     *
     * @param projectRequest the project request
     * @return ProjectsTbl
     */
    ProjectsTbl update(final ProjectRequest projectRequest);

    /**
     * Find by id.
     *
     * @param id the id
     * @return ProjectsTbl
     */
    ProjectsTbl findById(final String id);

    /**
     * Delete.
     *
     * @param id the id
     * @return boolean
     */
    boolean delete(final String id);
    
    /**
     * Multi delete.
     *
     * @param hsr the hsr
     * @param projectIds the project ids
     * @return ProjectResponse
     */
    ProjectResponse multiDelete(HttpServletRequest hsr, Set<String> projectIds);

    /**
     * Upate status by id.
     *
     * @param status the status
     * @param id the id
     * @return boolean
     */
    boolean upateStatusById(final String status, final String id);

    /**
     * Find projects by user id.
     *
     * @param userId the user id
     * @param validationRequest the validation request
     * @return ProjectResponse
     */
    ProjectResponse findProjectsByUserId(final String userId,
            final ValidationRequest validationRequest);

    /**
     * Find projects by start app id.
     *
     * @param startAppId the start app id
     * @param validationRequest the validation request
     * @return ProjectResponse
     */
    ProjectResponse findProjectsByStartAppId(final String startAppId,
            final ValidationRequest validationRequest);

    /**
     * Find all projects by project app site AA id.
     *
     * @param projectAppId the project app id
     * @param siteId the site id
     * @param adminAreaId the admin area id
     * @return the project response
     */
    ProjectResponse findAllProjectsByProjectAppSiteAAId(final String projectAppId,
            final String siteId, final String adminAreaId);

    /**
     * Find all projects by AA id.
     *
     * @param aaId the aa id
     * @param validationRequest the validation request
     * @return ProjectResponse
     */
    ProjectResponse findAllProjectsByAAId(final String aaId, final ValidationRequest validationRequest);

    /**
     * Find all projects by AA id and active.
     *
     * @param aaId the aa id
     * @param validationRequest the validation request
     * @return ProjectResponse
     */
    ProjectResponse findAllProjectsByAAIdAndActive(final String aaId,
            final ValidationRequest validationRequest);

    /**
     * Find user projects by site id and username.
     *
     * @param siteId the site id
     * @param tkt the tkt
     * @param userName the user name
     * @param validationRequest the validation request
     * @return ProjectResponse
     */
    ProjectResponse findUserProjectsBySiteIdAndUsername(final String siteId, final String tkt,
            final String userName, final ValidationRequest validationRequest);

	/**
	 * Find by name.
	 *
	 * @param name the name
	 * @return the projects tbl
	 */
	ProjectsTbl findByName(String name);

	/**
	 * Multi update.
	 *
	 * @param projectRequests the project requests
	 * @param httpServletRequest the http servlet request
	 * @return the project response
	 */
	ProjectResponse multiUpdate(List<ProjectRequest> projectRequests, HttpServletRequest httpServletRequest);

	/**
	 * Find projects by site id.
	 *
	 * @param id the id
	 * @param validationRequest the validation request
	 * @return the project response
	 */
	ProjectResponse findProjectsBySiteId(String id, ValidationRequest validationRequest);

	/**
	 * Find projects by admin area id.
	 *
	 * @param adminAreaId the admin area id
	 * @param validationRequest the validation request
	 * @return the project response
	 */
	ProjectResponse findProjectsByAdminAreaId(String adminAreaId, ValidationRequest validationRequest);

	/**
	 * Find all projects by user id.
	 *
	 * @param id the id
	 * @param validationRequest the validation request
	 * @return the project response
	 */
	ProjectResponse findAllProjectsByUserId(String id, ValidationRequest validationRequest);

	/**
	 * Update for batch.
	 *
	 * @param projectRequest the project request
	 * @return the projects tbl
	 */
	ProjectsTbl updateForBatch(ProjectRequest projectRequest);

	/**
	 * Find projects by user id and site id.
	 *
	 * @param userId the user id
	 * @param siteId the site id
	 * @param validationRequest the validation request
	 * @return the project response
	 */
	ProjectResponse findProjectsByUserIdAndSiteId(String userId, String siteId, ValidationRequest validationRequest);
}
