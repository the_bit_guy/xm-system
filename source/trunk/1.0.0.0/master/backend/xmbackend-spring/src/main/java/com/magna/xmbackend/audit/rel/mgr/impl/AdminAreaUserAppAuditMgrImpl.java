/**
 * 
 */
package com.magna.xmbackend.audit.rel.mgr.impl;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.magna.xmbackend.audit.mgr.utils.AdminHistoryRelsUtil;
import com.magna.xmbackend.audit.rel.mgr.AdminAreaUserAppAuditMgr;
import com.magna.xmbackend.entities.AdminAreaUserAppRelTbl;
import com.magna.xmbackend.entities.AdminHistoryRelationsTbl;
import com.magna.xmbackend.entities.SiteAdminAreaRelTbl;
import com.magna.xmbackend.entities.UserApplicationsTbl;
import com.magna.xmbackend.jpa.dao.UserApplicationJpaDao;
import com.magna.xmbackend.jpa.rel.dao.AdminAreaUserAppRelJpaDao;
import com.magna.xmbackend.jpa.rel.dao.AdminHistoryRelationJpaDao;
import com.magna.xmbackend.jpa.rel.dao.SiteAdminAreaRelJpaDao;
import com.magna.xmbackend.vo.rel.AdminAreaUserAppRelBatchRequest;
import com.magna.xmbackend.vo.rel.AdminAreaUserAppRelBatchResponse;
import com.magna.xmbackend.vo.rel.AdminAreaUserAppRelRequest;

/**
 * @author Bhabadyuti Bal
 *
 */
@Component
public class AdminAreaUserAppAuditMgrImpl implements AdminAreaUserAppAuditMgr {
	
	private static final Logger LOG = LoggerFactory.getLogger(AdminAreaUserAppAuditMgrImpl.class);
	
	final String RELATION_NAME = "AdminAreaUserApp";
	final String RESULT_SUCCESS = "Success";
	final String RESULT_FAILURE = "Failure";
	
	@Autowired
	private AdminHistoryRelsUtil adminHistoryRelsUtil;
	@Autowired
	private AdminHistoryRelationJpaDao adminHistoryRelationJpaDao;
	@Autowired
	private SiteAdminAreaRelJpaDao siteAdminAreaRelJpaDao;
	@Autowired
	private UserApplicationJpaDao userApplicationJpaDao;
	@Autowired
	private AdminAreaUserAppRelJpaDao adminAreaUserAppRelJpaDao;
	
	

	@Override
	public void adminAreaUserAppMultiSaveAuditor(AdminAreaUserAppRelBatchRequest batchReq,
			AdminAreaUserAppRelBatchResponse batchRes, HttpServletRequest httpServletRequest) {
		LOG.info(">>> adminAreaProjectAppMultiSaveAuditor");

		List<AdminAreaUserAppRelRequest> adminAreaUserAppRelRequests = batchReq.getAdminAreaUserAppRelRequests();

		List<AdminAreaUserAppRelTbl> adminAreaUserAppRelTbls = batchRes.getAdminAreaUserAppRelTbls();
		List<Map<String, String>> statusMap = batchRes.getStatusMap();

		if (statusMap.isEmpty()) {
			// All success
			this.createSuccessAudit(adminAreaUserAppRelTbls, httpServletRequest);

		} else {
			//Partial success case
			//Iterate the status map to log the failed ones
			this.createFailureAudit(statusMap, adminAreaUserAppRelRequests, httpServletRequest);

			//Iterate the userProjectRelTbls for success ones
			this.createSuccessAudit(adminAreaUserAppRelTbls, httpServletRequest);
		}
		LOG.info("<<< adminAreaProjectAppMultiSaveAuditor");
		
	}

	private void createFailureAudit(List<Map<String, String>> statusMap,
			List<AdminAreaUserAppRelRequest> adminAreaUserAppRelRequests, HttpServletRequest httpServletRequest) {
		for (Map<String, String> map : statusMap) {
			for (AdminAreaUserAppRelRequest adminAreaUserAppRelReq : adminAreaUserAppRelRequests) {
				String siteAdminAreaRelId = adminAreaUserAppRelReq.getSiteAdminAreaRelId();
				String userAppId = adminAreaUserAppRelReq.getUserAppId();
				
				String adminAreaName = "";
				String userAppName = "";
				
				SiteAdminAreaRelTbl siteAdminAreaRelTbl = this.siteAdminAreaRelJpaDao.findOne(siteAdminAreaRelId);
				
				if(siteAdminAreaRelTbl != null) {
					adminAreaName = siteAdminAreaRelTbl.getAdminAreaId().getName();
				}
				UserApplicationsTbl userAppTbl = this.userApplicationJpaDao.findOne(userAppId);
				if(userAppTbl != null) {
					userAppName = userAppTbl.getName();
				}
					String message = map.get("en");
					if (message.contains(adminAreaName) && message.contains(userAppName)) {
						AdminHistoryRelationsTbl adminHistoryRelationTbl = this.adminHistoryRelsUtil
								.makeAdminHistoryRelationTbl(httpServletRequest, "AdminAreaUserApp",
										adminAreaName, userAppName,null, 
										null, adminAreaUserAppRelReq.getRelationType(),
										null, message, "AdminAreaUserApp Create", "Failure");
						this.adminHistoryRelationJpaDao.save(adminHistoryRelationTbl);
						break;
					}
			}
		}
		
	}

	private void createSuccessAudit(List<AdminAreaUserAppRelTbl> adminAreaUserAppRelTbls,
			HttpServletRequest httpServletRequest) {
		adminAreaUserAppRelTbls.forEach(adminAreaUserAppRelTbl -> {
			String adminAreaName = adminAreaUserAppRelTbl.getSiteAdminAreaRelId().getAdminAreaId().getName();
			String userAppName = adminAreaUserAppRelTbl.getUserApplicationId().getName();
			String status = adminAreaUserAppRelTbl.getStatus();
			String relType = adminAreaUserAppRelTbl.getRelType();
			
			AdminHistoryRelationsTbl adminHistoryRelationTbl = this.adminHistoryRelsUtil
					.makeAdminHistoryRelationTbl(httpServletRequest, "AdminAreaUserApp", adminAreaName,
							userAppName, null, relType ,
							status, null, null, "AdminAreaUserApp Create",
							"Success");
			this.adminHistoryRelationJpaDao.save(adminHistoryRelationTbl);
		});
		
	}

	@Override
	public void adminAreaUserAppMultiSaveFailureAuditor(HttpServletRequest httpServletRequest, Exception ex) {
		AdminHistoryRelationsTbl adminHistoryRelationTbl = this.adminHistoryRelsUtil.makeAdminHistoryRelationTbl(
				httpServletRequest, "AdminAreaUserApp", null, null, null, null, null, null, ex.getMessage(), "AdminAreaUserApp Create",
				"Failure");
		this.adminHistoryRelationJpaDao.save(adminHistoryRelationTbl);
		
	}

	@Override
	public void adminAreaUserAppMultiDeleteSuccessAudit(AdminAreaUserAppRelTbl adminAreaUserAppRelTbl,
			HttpServletRequest httpServletRequest) {

		String adminAreaName = adminAreaUserAppRelTbl.getSiteAdminAreaRelId().getAdminAreaId().getName();
		String userAppName = adminAreaUserAppRelTbl.getUserApplicationId().getName();
		String status = adminAreaUserAppRelTbl.getStatus();
		String relType = adminAreaUserAppRelTbl.getRelType();
		
		AdminHistoryRelationsTbl adminHistoryRelationTbl = this.adminHistoryRelsUtil
				.makeAdminHistoryRelationTbl(httpServletRequest, "AdminAreaUserApp", adminAreaName,
						userAppName, null, status,
						relType, null, null, "AdminAreaUserApp Delete",
						"Success");
		this.adminHistoryRelationJpaDao.save(adminHistoryRelationTbl);
	
		
	}

	@Override
	public void adminAreaUserAppStatusUpdateAuditor(boolean updateStatusById, String status, String id,
			HttpServletRequest httpServletRequest) {
		if (updateStatusById) {
			AdminAreaUserAppRelTbl adminAreaUserAppRelTbl = this.adminAreaUserAppRelJpaDao.findOne(id);
			if (adminAreaUserAppRelTbl != null) {
				String adminAreaName = adminAreaUserAppRelTbl.getSiteAdminAreaRelId().getAdminAreaId().getName();
				String userAppName = adminAreaUserAppRelTbl.getUserApplicationId().getName();
				String relType = adminAreaUserAppRelTbl.getRelType();
				AdminHistoryRelationsTbl adminHistoryRelationTbl = this.adminHistoryRelsUtil
						.makeAdminHistoryRelationTbl(httpServletRequest, RELATION_NAME, adminAreaName, userAppName,
								null, relType, status, null, null, "AdminAreaUserApp status update", RESULT_SUCCESS);
				this.adminHistoryRelationJpaDao.save(adminHistoryRelationTbl);
			}
		}
	}

	@Override
	public void adminAreaUserAppStatusUpdateFailureAuditor(String id, String status, Exception ex,
			HttpServletRequest httpServletRequest) {
		AdminAreaUserAppRelTbl adminAreaUserAppRelTbl = this.adminAreaUserAppRelJpaDao.findOne(id);
		if (adminAreaUserAppRelTbl != null) {
			String adminAreaName = adminAreaUserAppRelTbl.getSiteAdminAreaRelId()
					.getAdminAreaId().getName();
			String userAppName = adminAreaUserAppRelTbl.getUserApplicationId().getName();
			String errorMsg = ex.getMessage();
			AdminHistoryRelationsTbl adminHistoryRelationTbl = this.adminHistoryRelsUtil
					.makeAdminHistoryRelationTbl(httpServletRequest, RELATION_NAME, adminAreaName,
							userAppName, null, null, status, null, errorMsg, "AdminAreaUserApp status update",
							RESULT_FAILURE);
			this.adminHistoryRelationJpaDao.save(adminHistoryRelationTbl);
		}
		
	}
	
}
