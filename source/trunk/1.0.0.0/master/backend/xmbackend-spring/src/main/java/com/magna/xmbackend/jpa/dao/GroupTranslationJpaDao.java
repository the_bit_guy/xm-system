/**
 * 
 */
package com.magna.xmbackend.jpa.dao;

import javax.transaction.Transactional;

import org.springframework.data.repository.CrudRepository;

import com.magna.xmbackend.entities.GroupTranslationTbl;

/**
 * @author Bhabadyuti Bal
 *
 */
@Transactional
public interface GroupTranslationJpaDao extends CrudRepository<GroupTranslationTbl, String>{

	
}
