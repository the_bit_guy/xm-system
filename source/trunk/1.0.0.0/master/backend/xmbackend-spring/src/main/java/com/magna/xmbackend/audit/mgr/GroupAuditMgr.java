/**
 * 
 */
package com.magna.xmbackend.audit.mgr;

import javax.servlet.http.HttpServletRequest;

import com.magna.xmbackend.entities.GroupsTbl;
import com.magna.xmbackend.vo.group.GroupCreateRequest;

/**
 * @author Bhabadyuti Bal
 *
 */
public interface GroupAuditMgr {

	void groupCreateSuccessAudit(HttpServletRequest httpServletRequest, GroupsTbl gt);

	void groupCreateFailureAudit(HttpServletRequest httpServletRequest, GroupCreateRequest gcr, String errMsg);

	void groupDeleteFailureAudit(HttpServletRequest hsr, String id, String errMsg);

	void groupDeleteSuccessAudit(HttpServletRequest hsr, String id, String name, String groupType);

	void groupUpdateSuccessAudit(HttpServletRequest hsr, GroupsTbl gt);

	void groupUpdateFailureAudit(HttpServletRequest hsr, GroupCreateRequest gcr, String errMsg);

}
