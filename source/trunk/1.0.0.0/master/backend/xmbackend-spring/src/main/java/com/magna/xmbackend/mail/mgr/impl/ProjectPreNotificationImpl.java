/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magna.xmbackend.mail.mgr.impl;

import java.io.UnsupportedEncodingException;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

import com.magna.xmbackend.entities.LanguagesTbl;
import com.magna.xmbackend.entities.MailQueueTbl;
import com.magna.xmbackend.entities.ProjectTranslationTbl;
import com.magna.xmbackend.entities.ProjectsTbl;
import com.magna.xmbackend.jpa.dao.EmailNotificationConfigJpaDao;
import com.magna.xmbackend.jpa.dao.EmailNotificationEventsJpaDao;
import com.magna.xmbackend.jpa.dao.MailQueueJpaDao;
import com.magna.xmbackend.mail.mgr.MailSupportMgr;
import com.magna.xmbackend.mail.mgr.ProjectPreNotification;
import com.magna.xmbackend.rel.mgr.UserProjectRelMgr;
import com.magna.xmbackend.utils.DateUtil;
import com.magna.xmbackend.utils.MailSupport;
import com.magna.xmbackend.vo.enums.NotificationVariables;

/**
 *
 * @author dhana
 */
@Component
public class ProjectPreNotificationImpl
        implements ProjectPreNotification {

    private static final Logger LOG
            = LoggerFactory.getLogger(ProjectPreNotificationImpl.class);

    @Autowired
    private TemplateEngine templateEngine;
    @Autowired
    private MailSupportMgr mailSupportMgr;
    @Autowired
    private MailQueueJpaDao mailQueueJpaDao;
    @Autowired
    private UserProjectRelMgr userProjectRelMgr;
    @Autowired
    private EmailNotificationConfigJpaDao emailNotificationConfigJpaDao;
    @Autowired
    private MailSupport mailSupport;
    @Autowired
	private EmailNotificationEventsJpaDao eventsJpaDao;
    
    @Override
    @Async("threadPoolTaskExecutor")
    @Transactional
	public void postToQueue(ProjectsTbl projectsTbl, String event) {
		LOG.info(">>--postToQueue with event::{}", event);
		LOG.info("Execute method asynchronously. thread name - {}"+ Thread.currentThread().getName());

		// Get the event config params from the table
		// EMAIL_NOTIFICATION_CONFIG_TBL and the user data
		List<UserEventConfigData> userEventConfigDatas = this.mailSupportMgr.getUserEventConfigData(event);
		if (userEventConfigDatas == null || userEventConfigDatas.isEmpty()) {
			return;
		}
		Map<String, String> eventAttributes = new HashMap<>();

		String projectName = projectsTbl.getName();
		Collection<ProjectTranslationTbl> projectTranslationTblCollection = projectsTbl.getProjectTranslationTblCollection();
		projectTranslationTblCollection.forEach((projectTranslationTbl) -> {
			LanguagesTbl languagesTbl = projectTranslationTbl.getLanguageCode();
			String languageCode = languagesTbl.getLanguageCode();
			eventAttributes.put("projectCreatedDate", DateUtil.getFormattedDateAsString(projectsTbl.getCreateDate()));
			eventAttributes.put("projectName", projectName);
			String remarks = projectTranslationTbl.getRemarks();
			String remarksDecoded = null;
			try {
				if (remarks != null) {
					remarksDecoded = java.net.URLDecoder.decode(remarks, "ISO-8859-1");
				}
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			}
			eventAttributes.put("remarks_" + languageCode, remarksDecoded);
			//eventAttributes.put("remarks", remarksDecoded);
		});

		LOG.debug("eventAttribs{}", eventAttributes);
		this.bindProjectCreateTemplate2Data(userEventConfigDatas, eventAttributes, event);

		// store the userEventConfigDatas details in MAIL_QUEUE_TBL
		Collection<MailQueueTbl> mailQueueTbls = this.mailSupportMgr.convert2Entity(userEventConfigDatas);

		//mailQueueTbls.addAll(this.addAssignedUsers2QueueIfApplicable(projectsTbl, eventAttributes, event));

		Iterable<MailQueueTbl> mqtsOut = this.mailQueueJpaDao.save(mailQueueTbls);

		LOG.debug("mqtsOut {}", mqtsOut);
		LOG.info("<<--postToQueue");
	}

    @SuppressWarnings("unused")
	private Collection<MailQueueTbl> addAssignedUsers2QueueIfApplicable(
            ProjectsTbl projectsTbl, Map<String, String> eventAttributes, 
			String event) {/*
		Collection<MailQueueTbl> mailQueueTbls = new ArrayList<>();

		//EmailNotificationConfigTbl configTbl = emailNotificationConfigJpaDao.findByEvent(event);
		
		EmailNotificationEventTbl eventTbl = this.eventsJpaDao.findByEvent(event);
		List<EmailNotificationConfigTbl> configTbls = this.emailNotificationConfigJpaDao.findByEmailNotificationEventId(eventTbl);
		for (EmailNotificationConfigTbl configTbl : configTbls) {
			if (configTbl.getStatus().equals(Status.ACTIVE.name())) {
				String sendToAssignedUser = configTbl.getSendToAssignedUser();
				if (!"true".equalsIgnoreCase(sendToAssignedUser)) {
					return mailQueueTbls;
				}

				String projectId = projectsTbl.getProjectId();
				UserProjectRelResponse userProjectRelResponse;
				try {
					userProjectRelResponse = this.userProjectRelMgr.findUserProjectRelationByProjectId(projectId);
				} catch (RuntimeException rex) {
					return mailQueueTbls;
				}
				Iterable<UserProjectRelTbl> userProjectRelTbls = userProjectRelResponse.getUserProjectRelTbls();
				Date date = new Date();

				String subject = configTbl.getSubject();

				for (UserProjectRelTbl userProjectRelTbl : userProjectRelTbls) {
					UsersTbl usersTbl = userProjectRelTbl.getUserId();
					String userName = usersTbl.getUsername();
					String toMail = usersTbl.getEmailId();
					String textMessage = this.mailSupportMgr.bindProjectTemplateToData(userName,
							eventAttributes.get("PROJECT_NAME_en"), eventAttributes.get("PROJECT_NAME_en"),
							eventAttributes.get("PROJECT_NAME_en"), event);
					MailQueueTbl mailQueueTbl = new MailQueueTbl(UUID.randomUUID().toString(), userName, toMail, subject,
							textMessage, event, "new", date, date);
					mailQueueTbls.add(mailQueueTbl);
				}
			}
		}
	*/	return null;
	}
    
	private void bindProjectCreateTemplate2Data(List<UserEventConfigData> userEventConfigDatas,
			Map<String, String> eventAttributes, String event) {
		for(UserEventConfigData userEventConfigData : userEventConfigDatas){

			this.mailSupport.createDynamicTemplate(event);

			Context ctx = new Context();
			boolean includeRemarks = userEventConfigData.isIncludeRemarks();
			eventAttributes.put("INCLUDE_REMARKS", String.valueOf(includeRemarks));
			//StringBuilder remarks = new StringBuilder();
			if (includeRemarks) {
				String remarks_en = eventAttributes.get("remarks_en");
				//remarks_en = this.mailSupport.textToMailText(remarks_en);
				String remarks_de = eventAttributes.get("remarks_de");
				//remarks_de = this.mailSupport.textToMailText(remarks_de);
				if(remarks_en != null && !remarks_en.equals("")) {
					ctx.setVariable("remarksEn", true);
					ctx.setVariable("remarks_en", remarks_en);
				}
				//remarks.append(remarks_en);
				if(remarks_de != null && !remarks_de.equals("")) {
					ctx.setVariable("remarksDe", true);
					ctx.setVariable("remarks_de", remarks_de);
				}
			}
			//ctx.setVariable("remarks", remarks);
			//ctx.setVariable(NotificationVariables.NOTIFICATION_MESSAGE_BACKEND.toString(), userEventConfigData.getMessage());
			ctx.setVariable("username", userEventConfigData.getUsername());
			ctx.setVariable("projectcreatedate", eventAttributes.get("projectCreatedDate"));
			ctx.setVariable("projectname", eventAttributes.get("projectName"));
			//ctx.setVariable("projExpPeriod", userEventConfigData.getUsername());
			//ctx.setVariable("gracePeriod", userEventConfigData.getUsername());
			this.templateEngine.clearTemplateCache();
			final String htmlContent = this.templateEngine.process("common_template", ctx);
			this.templateEngine.clearTemplateCache();
			LOG.debug("htmlContent {}", htmlContent);
			userEventConfigData.setTextMessage(htmlContent);
		
		}
	}

}
