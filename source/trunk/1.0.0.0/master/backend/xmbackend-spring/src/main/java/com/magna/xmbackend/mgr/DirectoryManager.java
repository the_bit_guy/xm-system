/**
 * 
 */
package com.magna.xmbackend.mgr;

import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import com.magna.xmbackend.entities.DirectoryTbl;
import com.magna.xmbackend.vo.directory.DirectoryRequest;
import com.magna.xmbackend.vo.directory.DirectoryResponse;

/**
 * @author Bhabadyuti Bal
 *
 */
public interface DirectoryManager {

	public DirectoryTbl create(DirectoryRequest directoryRequest);

	public DirectoryResponse findAll();
	
	public DirectoryTbl update(final DirectoryRequest directoryRequest);
	
	public boolean deleteById(final String id);
	
	DirectoryResponse multiDelete(final Set<String> dirIds, HttpServletRequest httpServletRequest);
	
	DirectoryTbl findById(final String id);
}
