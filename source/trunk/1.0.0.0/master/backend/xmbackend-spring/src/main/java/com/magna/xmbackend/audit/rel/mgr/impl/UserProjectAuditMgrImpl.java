/**
 * 
 */
package com.magna.xmbackend.audit.rel.mgr.impl;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.magna.xmbackend.audit.mgr.utils.AdminHistoryRelsUtil;
import com.magna.xmbackend.audit.rel.mgr.UserProjectAuditMgr;
import com.magna.xmbackend.entities.AdminHistoryRelationsTbl;
import com.magna.xmbackend.entities.ProjectsTbl;
import com.magna.xmbackend.entities.UserProjectRelTbl;
import com.magna.xmbackend.entities.UsersTbl;
import com.magna.xmbackend.jpa.dao.ProjectJpaDao;
import com.magna.xmbackend.jpa.dao.UserJpaDao;
import com.magna.xmbackend.jpa.rel.dao.AdminHistoryRelationJpaDao;
import com.magna.xmbackend.jpa.rel.dao.UserProjectRelJpaDao;
import com.magna.xmbackend.vo.rel.UserProjectRelBatchRequest;
import com.magna.xmbackend.vo.rel.UserProjectRelBatchResponse;
import com.magna.xmbackend.vo.rel.UserProjectRelRequest;

/**
 * @author Bhabadyuti Bal
 *
 */
@Component
public class UserProjectAuditMgrImpl implements UserProjectAuditMgr {

	private static final Logger LOG = LoggerFactory.getLogger(UserProjectAuditMgrImpl.class);
	
	final String RELATION_NAME = "UserProject";
	final String RESULT_SUCCESS = "Success";
	final String RESULT_FAILURE = "Failure";

	@Autowired
	private AdminHistoryRelsUtil adminHistoryRelsUtil;
	@Autowired
	private AdminHistoryRelationJpaDao adminHistoryRelationJpaDao;
	@Autowired
	private UserJpaDao userJpaDao;
	@Autowired
	private ProjectJpaDao projectJpaDao;
	@Autowired
	private UserProjectRelJpaDao userProjectRelJpaDao;

	@Override
	public void userProjectMultiSaveAuditor(UserProjectRelBatchRequest uprbReq, UserProjectRelBatchResponse uprbRes,
			HttpServletRequest httpServletRequest) {
		LOG.info(">>> userProjectMultiSaveAuditor");

		List<UserProjectRelRequest> userProjectRelRequests = uprbReq.getUserProjectRelRequests();

		List<UserProjectRelTbl> userProjectRelTbls = uprbRes.getUserProjectRelTbls();
		List<Map<String, String>> statusMap = uprbRes.getStatusMap();

		if (statusMap.isEmpty()) {
			// All success
			userProjectRelTbls.forEach(userProjectRelTbl -> {
				UsersTbl usersTbl = userProjectRelTbl.getUserId();
				String role = this.getRolesForUser(usersTbl);
				ProjectsTbl projectTbl = userProjectRelTbl.getProjectId();
				AdminHistoryRelationsTbl adminHistoryRelationTbl = this.adminHistoryRelsUtil
						.makeAdminHistoryRelationTbl(httpServletRequest, RELATION_NAME, usersTbl.getUsername(),
								projectTbl.getName(), null, null, userProjectRelTbl.getStatus(), role, null, "UserProjectRel Create",
								"Success");
				this.adminHistoryRelationJpaDao.save(adminHistoryRelationTbl);
			});

		} else {
			// iterate the status map to log the failed ones
			for (Map<String, String> map : statusMap) {
				for (UserProjectRelRequest userProjRelReq : userProjectRelRequests) {
					UsersTbl usersTbl = this.userJpaDao.findByUserId(userProjRelReq.getUserId());
					ProjectsTbl projectTbl = this.projectJpaDao.findByProjectId(userProjRelReq.getProjectId());
					if (usersTbl != null && projectTbl != null) {
						String message = map.get("en");
						String role = this.getRolesForUser(usersTbl);
						if (message.contains(usersTbl.getUsername()) && message.contains(projectTbl.getName())) {
							AdminHistoryRelationsTbl adminHistoryRelationTbl = this.adminHistoryRelsUtil
									.makeAdminHistoryRelationTbl(httpServletRequest, RELATION_NAME,
											usersTbl.getUsername(), projectTbl.getName(), null, null,
											userProjRelReq.getStatus(), role, message, "UserProjectRel Create",
											"Failure");
							this.adminHistoryRelationJpaDao.save(adminHistoryRelationTbl);
							break;
						}
					}
				}
			}

			// iterate the userProjectRelTbls for success ones
			userProjectRelTbls.forEach(userProjTbl -> {
				UsersTbl userTbl = userProjTbl.getUserId();
				ProjectsTbl projectTbl = userProjTbl.getProjectId();
				String role = this.getRolesForUser(userTbl);
				AdminHistoryRelationsTbl adminHistoryRelationTbl = this.adminHistoryRelsUtil
						.makeAdminHistoryRelationTbl(httpServletRequest, RELATION_NAME, userTbl.getUsername(),
								projectTbl.getName(), null, null, userProjTbl.getStatus(),  role, null, "UserProjectRel Create",
								"Success");
				this.adminHistoryRelationJpaDao.save(adminHistoryRelationTbl);
			});
		}
		LOG.info("<<< userProjectMultiSaveAuditor");
	}

	private String getRolesForUser(UsersTbl usersTbl) {
		/*
		 * String reduce = usersTbl.getRoleUserRelTblCollection().stream().map(
		 * roleUserTbl -> roleUserTbl.getRoleId().getName()).reduce("", String::concat);
		 */
		String roles = usersTbl.getRoleUserRelTblCollection().stream()
				.map(roleUserTbl -> roleUserTbl.getRoleId().getName()).collect(Collectors.joining(","));
		return roles;
	}

	@Override
	public void userProjectMultiSaveFailureAuditor(HttpServletRequest httpServletRequest, Exception ex) {
		LOG.info(">>> userProjectMultiSaveFailureAuditor");
		AdminHistoryRelationsTbl adminHistoryRelationTbl = this.adminHistoryRelsUtil.makeAdminHistoryRelationTbl(
				httpServletRequest, RELATION_NAME, null, null, null, null, null, null, ex.getMessage(), "UserProjectRel Create",
				"Failure");
		this.adminHistoryRelationJpaDao.save(adminHistoryRelationTbl);
		LOG.info("<<< userProjectMultiSaveFailureAuditor");
	}

	@Override
	public void userProjectMultiDeleteSuccessAudit(UserProjectRelTbl userProjectRelTbl,
			HttpServletRequest httpServletRequest) {
		UsersTbl usersTbl = userProjectRelTbl.getUserId();
		String role = this.getRolesForUser(usersTbl);
		ProjectsTbl projectTbl = userProjectRelTbl.getProjectId();
		AdminHistoryRelationsTbl adminHistoryRelationTbl = this.adminHistoryRelsUtil
				.makeAdminHistoryRelationTbl(httpServletRequest, RELATION_NAME, usersTbl.getUsername(),
						projectTbl.getName(), null, null, userProjectRelTbl.getStatus(), role, null, "UserProjectRel Delete",
						"Success");
		this.adminHistoryRelationJpaDao.save(adminHistoryRelationTbl);
	}

	@Override
	public void userProjectStatusUpdateAuditor(boolean updateStatusById, String status, String id,
			HttpServletRequest httpServletRequest) {
		if (updateStatusById) {
			UserProjectRelTbl userProjectRelTbl = this.userProjectRelJpaDao.findOne(id);
			if (userProjectRelTbl != null) {
				String username = userProjectRelTbl.getUserId().getUsername();
				String projectName = userProjectRelTbl.getProjectId().getName();
				AdminHistoryRelationsTbl adminHistoryRelationTbl = this.adminHistoryRelsUtil
						.makeAdminHistoryRelationTbl(httpServletRequest, RELATION_NAME, username,
								projectName, null, null, status, null, null, "UserProjectRel status update",
								RESULT_SUCCESS);
				this.adminHistoryRelationJpaDao.save(adminHistoryRelationTbl);
			}

		}
		
	}

	@Override
	public void userProjectStatusUpdateFailureAuditor(String id, String status, Exception ex,
			HttpServletRequest httpServletRequest) {
		UserProjectRelTbl userProjectRelTbl = this.userProjectRelJpaDao.findOne(id);
		if (userProjectRelTbl != null) {
			String username = userProjectRelTbl.getUserId().getUsername();
			String projectName = userProjectRelTbl.getProjectId().getName();
			String errorMsg = ex.getMessage();
			AdminHistoryRelationsTbl adminHistoryRelationTbl = this.adminHistoryRelsUtil
					.makeAdminHistoryRelationTbl(httpServletRequest, RELATION_NAME, username,
							projectName, null, null, status, null, errorMsg, "AdminAreaProjectApp status update",
							RESULT_FAILURE);
			this.adminHistoryRelationJpaDao.save(adminHistoryRelationTbl);
		}
		
	}
	
	@Override
	public void userProjectRelCreateSuccess(UserProjectRelTbl userProjectRelTbl,
			HttpServletRequest httpServletRequest) {
		UsersTbl usersTbl = userProjectRelTbl.getUserId();
		String role = this.getRolesForUser(usersTbl);
		ProjectsTbl projectTbl = userProjectRelTbl.getProjectId();
		AdminHistoryRelationsTbl adminHistoryRelationTbl = this.adminHistoryRelsUtil.makeAdminHistoryRelationTbl(
				httpServletRequest, RELATION_NAME, usersTbl.getUsername(), projectTbl.getName(), null, null,
				userProjectRelTbl.getStatus(), role, null, "UserProjectRel Create", "Success");
		this.adminHistoryRelationJpaDao.save(adminHistoryRelationTbl);
	}
	
	@Override
	public void userProjectRelCreateFailed(UserProjectRelRequest userProjectRelRequest,
			HttpServletRequest httpServletRequest, String errMsg) {
		ProjectsTbl projectTbl = this.projectJpaDao.findOne(userProjectRelRequest.getProjectId());
		UsersTbl userTbl = this.userJpaDao.findOne(userProjectRelRequest.getUserId());
		if (projectTbl != null && userTbl != null) {
			AdminHistoryRelationsTbl adminHistoryRelationTbl = this.adminHistoryRelsUtil.makeAdminHistoryRelationTbl(
					httpServletRequest, RELATION_NAME, userTbl.getUsername(), projectTbl.getName(), null, null,
					userProjectRelRequest.getStatus(), null, errMsg, "UserProjectRel Create", "Failure");
			this.adminHistoryRelationJpaDao.save(adminHistoryRelationTbl);
		}
	}

	@Override
	public void userProjectMultiDeleteFailureAudit(UserProjectRelTbl userProjectRelTbl,
			HttpServletRequest httpServletRequest) {
		UsersTbl usersTbl = userProjectRelTbl.getUserId();
		ProjectsTbl projectTbl = userProjectRelTbl.getProjectId();
		AdminHistoryRelationsTbl adminHistoryRelationTbl = this.adminHistoryRelsUtil
				.makeAdminHistoryRelationTbl(httpServletRequest, RELATION_NAME, usersTbl.getUsername(),
						projectTbl.getName(), null, null, userProjectRelTbl.getStatus(), null, null, "UserProjectRel Delete",
						"Failure");
		this.adminHistoryRelationJpaDao.save(adminHistoryRelationTbl);
		
	}
}
