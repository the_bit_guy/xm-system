package com.magna.xmbackend.audit.mgr;

import com.magna.xmbackend.entities.StartApplicationsTbl;
import com.magna.xmbackend.vo.startApplication.StartApplicationRequest;
import com.magna.xmbackend.vo.startApplication.StartApplicationResponse;
import java.util.Set;
import javax.servlet.http.HttpServletRequest;

/**
 *
 * @author Admin
 */
public interface StartAppAuditMgr {

    /**
     *
     * @param startApplicationsTbl
     * @param httpServletRequest
     */
    void startAppCreateSuccessAuditor(final StartApplicationsTbl startApplicationsTbl,
            final HttpServletRequest httpServletRequest);

    /**
     *
     * @param startApplicationRequest
     * @param httpServletRequest
     */
    void startAppCreateFailureAuditor(final StartApplicationRequest startApplicationRequest,
            final HttpServletRequest httpServletRequest);
    
    
    void startAppCreateFailureBatchAuditor(final StartApplicationRequest startApplicationRequest,
            final HttpServletRequest httpServletRequest);

    /**
     *
     * @param startApplicationsTbl
     * @param servletRequest
     */
    void startAppUpdateSuccessAuditor(final StartApplicationsTbl startApplicationsTbl,
            final HttpServletRequest servletRequest);

    /**
     *
     * @param startApplicationRequest
     * @param httpServletRequest
     */
    void startAppUpdateFailureAuditor(final StartApplicationRequest startApplicationRequest,
            final HttpServletRequest httpServletRequest);

    /**
     *
     * @param status
     * @param startAppId
     * @param httpServletRequest
     * @param isSuccess
     * @param sat
     */
    void startAppUpdateStatusAuditor(final String status, final String startAppId,
            final HttpServletRequest httpServletRequest,
            final boolean isSuccess, final StartApplicationsTbl sat);

    /**
     *
     * @param startAppId
     * @param httpServletRequest
     * @param isSuccess
     * @param sat
     */
    void startAppDeleteStatusAuditor(final String startAppId,
            final HttpServletRequest httpServletRequest,
            final boolean isSuccess, final StartApplicationsTbl sat);

    /**
     *
     * @param startApplicationResponse
     * @param startAppIds
     * @param httpServletRequest
     */
    void startAppMultiDeleteAuditor(final StartApplicationResponse startApplicationResponse,
            final Set<String> startAppIds, final HttpServletRequest httpServletRequest);

	void startAppStatusUpdateSuccessAuditor(String startAppName, String status, HttpServletRequest httpServletRequest);

	void startAppStatusUpdateFailureAuditor(String startAppName, String status, Exception ex,
			HttpServletRequest httpServletRequest);
}
