package com.magna.xmbackend.controller;

import com.magna.xmbackend.entities.LanguagesTbl;
import com.magna.xmbackend.jpa.dao.LanguageJpaDao;
import javax.servlet.http.HttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import com.magna.xmbackend.vo.language.LingualResponse;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

@RestController
@RequestMapping(value = "/language")
public class LanguageController {

    private static final Logger LOG
            = LoggerFactory.getLogger(LanguageController.class);

    @Autowired
    private LanguageJpaDao languageJpaDao;

    /**
     *
     * @param httpServletRequest
     * @return LanguageResponse
     */
    @RequestMapping(value = "/findAll",
            method = RequestMethod.GET,
            consumes = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<LingualResponse> findAll(HttpServletRequest httpServletRequest) {
        LOG.info("> fetchAll");
        Iterable<LanguagesTbl> languagesTbls = languageJpaDao.findAll();
        LingualResponse lingualResponse = new LingualResponse(languagesTbls);
        LOG.info("< fetchAll");
        return new ResponseEntity<>(lingualResponse, HttpStatus.OK);
    }

}
