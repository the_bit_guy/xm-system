package com.magna.xmbackend.mgr.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Component;

import com.magna.xmbackend.audit.mgr.AdminMenuConfigAuditMgr;
import com.magna.xmbackend.entities.AdminMenuConfigTbl;
import com.magna.xmbackend.entities.ProjectApplicationsTbl;
import com.magna.xmbackend.entities.UserApplicationsTbl;
import com.magna.xmbackend.entities.UsersTbl;
import com.magna.xmbackend.exception.CannotCreateRelationshipException;
import com.magna.xmbackend.exception.XMObjectNotFoundException;
import com.magna.xmbackend.jpa.dao.AdminMenuConfigJpaDao;
import com.magna.xmbackend.jpa.dao.ProjectApplicationJpaDao;
import com.magna.xmbackend.jpa.dao.UserApplicationJpaDao;
import com.magna.xmbackend.jpa.dao.UserJpaDao;
import com.magna.xmbackend.mgr.AdminMenuConfigMgr;
import com.magna.xmbackend.utils.MessageMaker;
import com.magna.xmbackend.utils.Validator;
import com.magna.xmbackend.vo.adminMenu.AdminMenuConfigCustomeResponse;
import com.magna.xmbackend.vo.adminMenu.AdminMenuConfigCustomeResponseWrapper;
import com.magna.xmbackend.vo.adminMenu.AdminMenuConfigRequest;
import com.magna.xmbackend.vo.adminMenu.AdminMenuConfigResponse;
import com.magna.xmbackend.vo.adminMenu.AdminMenuConfigResponseWrapper;
import com.magna.xmbackend.vo.enums.AdminMenuConfig;
import com.magna.xmbackend.vo.permission.ValidationRequest;
import com.magna.xmbackend.vo.projectApplication.ProjectApplicationMenuResponse;
import com.magna.xmbackend.vo.userApplication.UserApplicationMenuResponse;

/**
 * @author Bhabadyuti Bal
 *
 */
@Component
public class AdminMenuConfigMgrImpl implements AdminMenuConfigMgr {

    private static final Logger LOG
            = LoggerFactory.getLogger(AdminMenuConfigMgrImpl.class);

    @Autowired
    AdminMenuConfigJpaDao adminMenuConfigJpaDao;

    @Autowired
    UserJpaDao userJpaDao;

    @Autowired
    UserApplicationJpaDao userApplicationJpaDao;

    @Autowired
    ProjectApplicationJpaDao projectApplicationJpaDao;

    @Autowired
    private MessageMaker messageMaker;

    @Autowired
    private Validator validator;
    
    @Autowired
    private AdminMenuConfigAuditMgr adminMenuConfigAuditMgr;

    /**
     *
     * @param configRequest
     * @param userName
     * @return AdminMenuConfigTbl
     */
    @Override
    public AdminMenuConfigTbl save(final AdminMenuConfigRequest configRequest,
            final String userName) {
        AdminMenuConfigTbl adminMenuConfigTblOut = null;
        final Map<String, Object> assignmentAllowedMap
                = isInActiveAssignmentsAllowed(configRequest, userName);
        LOG.info("assignmentAllowedMap={}", assignmentAllowedMap);
        if (assignmentAllowedMap.containsKey("isAssignmentAllowed")) {
            boolean isAssignmentAllowed = (boolean) assignmentAllowedMap.get("isAssignmentAllowed");
            if (isAssignmentAllowed) {
                this.checkIfConfigurationAlreadyExist(configRequest.getObjectType().name(), configRequest.getObjectId());
                final AdminMenuConfigTbl adminMenuConfigTblIn = this.convertToEntity(configRequest, false);
                adminMenuConfigTblOut = this.adminMenuConfigJpaDao.save(adminMenuConfigTblIn);
            } else {
                formInactiveAssignmentsErrorMessage(configRequest, assignmentAllowedMap);
            }
        }
        return adminMenuConfigTblOut;
    }

    /**
     *
     * @param configRequest
     * @param userName
     * @return Map
     */
    private Map<String, Object> isInActiveAssignmentsAllowed(final AdminMenuConfigRequest configRequest,
            final String userName) {
        final boolean isViewInactiveAllowed = isViewInActiveAllowed(configRequest, userName);
        LOG.debug("isViewInactiveAllowed={}", isViewInactiveAllowed);
        final Map<String, Object> assignmentAllowedMap
                = validator.isAdminMenuInactiveAssignmentAllowed(configRequest,
                        isViewInactiveAllowed);
        return assignmentAllowedMap;
    }

    /**
     *
     * @param configRequest
     * @param userName
     * @return boolean
     */
    private boolean isViewInActiveAllowed(final AdminMenuConfigRequest configRequest,
            final String userName) {
        ValidationRequest validationRequest = null;
        //final AdminMenuConfig adminMenuConfig = configRequest.getObjectType();
        validationRequest = validator.formSaveRelationValidationRequest(userName, "ADMIN_MENU_CONFIG");
        /*switch (adminMenuConfig) {
            case USER:
                validationRequest = validator.formSaveRelationValidationRequest(userName, "ADMIN_MENU_CONFIG");
                break;
            case USERAPPLICATION:
                validationRequest = validator.formSaveRelationValidationRequest(userName, "ADMIN_MENU_CONFIG");
                break;
            case PROJECTAPPLICATION:
                validationRequest = validator.formSaveRelationValidationRequest(userName, "ADMIN_MENU_CONFIG");
                break;
            default:
                break;
        }*/
        LOG.info("validationRequest={}", validationRequest);
        final boolean isInactiveAssignment = validator.isViewInactiveAllowed(validationRequest);
        LOG.info("isInactiveAssignment={}", isInactiveAssignment);
        return isInactiveAssignment;
    }

    /**
     *
     * @param configRequest
     * @param assignmentAllowedMap
     */
    private void formInactiveAssignmentsErrorMessage(final AdminMenuConfigRequest configRequest,
            final Map<String, Object> assignmentAllowedMap) {
        final AdminMenuConfig adminMenuConfig = configRequest.getObjectType();
        switch (adminMenuConfig) {
            case USER:
                if (assignmentAllowedMap.containsKey("usersTbl")) {
                    final Map<String, String> userTransMap
                            = messageMaker.getUserNames((UsersTbl) assignmentAllowedMap.get("usersTbl"));
                    final Map<String, String[]> paramMap = messageMaker.geti18nCodeMap(userTransMap);
                    LOG.debug("paramMap={}", paramMap);
                    throw new CannotCreateRelationshipException("Inactive Assignments not allowed", "AMU_ERR0001", paramMap);
                }
                break;
            case USERAPPLICATION:
                if (assignmentAllowedMap.containsKey("userApplicationsTbl")) {
                    final Map<String, String> userAppTransMap
                            = messageMaker.getUserAppNames((UserApplicationsTbl) assignmentAllowedMap.get("userApplicationsTbl"));
                    final Map<String, String[]> paramMap = messageMaker.geti18nCodeMap(userAppTransMap);
                    LOG.debug("paramMap={}", paramMap);
                    throw new CannotCreateRelationshipException("Inactive Assignments not allowed", "AMUA_ERR0001", paramMap);
                }
                break;
            case PROJECTAPPLICATION:
                if (assignmentAllowedMap.containsKey("projectApplicationsTbl")) {
                    final Map<String, String> projectAppTransMap
                            = messageMaker.getProjectAppNames((ProjectApplicationsTbl) assignmentAllowedMap.get("projectApplicationsTbl"));
                    final Map<String, String[]> paramMap = messageMaker.geti18nCodeMap(projectAppTransMap);
                    LOG.debug("paramMap={}", paramMap);
                    throw new CannotCreateRelationshipException("Inactive Assignments not allowed", "AMPA_ERR0001", paramMap);
                }
                break;
            default:
                break;
        }
    }

    /**
     *
     * @param configRequestList
     * @param userName
     * @return AdminMenuConfigResponse
     */
    @Override
    public AdminMenuConfigResponse multiSave(List<AdminMenuConfigRequest> configRequestList,
            final String userName) {
        List<AdminMenuConfigTbl> adminMenuConfigTbls = new ArrayList<>();
        final List<Map<String, String>> statusMaps = new ArrayList<>();
        for (AdminMenuConfigRequest configRequest : configRequestList) {
            try {
                AdminMenuConfigTbl configTbl = this.save(configRequest, userName);
                adminMenuConfigTbls.add(configTbl);
            } catch (CannotCreateRelationshipException ccre) {
                final Map<String, String> statusMap = messageMaker.extractFromException(ccre);
                statusMaps.add(statusMap);
            } catch (XMObjectNotFoundException xmObjNotFounde) {
                final Map<String, String> statusMap = messageMaker.extractFromException(xmObjNotFounde);
                statusMaps.add(statusMap);
            }
        }
        //Iterable<AdminMenuConfigTbl> iterable = this.adminMenuConfigJpaDao.save(adminMenuConfigTbls);
        AdminMenuConfigResponse response = new AdminMenuConfigResponse(adminMenuConfigTbls, statusMaps);
        return response;
    }

    /**
     *
     * @param objectType
     * @param objectId
     * @throws CannotCreateRelationshipException
     * @throws XMObjectNotFoundException
     */
    private void checkIfConfigurationAlreadyExist(final String objectType,
            final String objectId) throws CannotCreateRelationshipException,
            XMObjectNotFoundException {
        final AdminMenuConfigTbl adminMenuConfigTbl = this.adminMenuConfigJpaDao.findByObjectTypeAndObjectId(objectType, objectId);
        if (adminMenuConfigTbl != null) {
            if (objectType.equals(AdminMenuConfig.USER.name())) {
                UsersTbl usersTbl = this.userJpaDao.findByUserId(objectId);
                final Map<String, String> userTransMap = messageMaker.getUserNames(usersTbl);
                final Map<String, String[]> paramMap = messageMaker.geti18nCodeMap(userTransMap);
                throw new CannotCreateRelationshipException("Relation already exist", "AMC_ERR0001", paramMap);
            } else if (objectType.equals(AdminMenuConfig.USERAPPLICATION.name())) {
                UserApplicationsTbl userApplicationsTbl = this.userApplicationJpaDao.findByUserApplicationId(objectId);
                final Map<String, String> userAppNames = messageMaker.getUserAppNames(userApplicationsTbl);
                final Map<String, String[]> paramMap = messageMaker.geti18nCodeMap(userAppNames);
                throw new CannotCreateRelationshipException("Relation already exist", "AMC_ERR0002", paramMap);
            } else if (objectType.equals(AdminMenuConfig.PROJECTAPPLICATION.name())) {
                ProjectApplicationsTbl projectApplicationsTbl = this.projectApplicationJpaDao.findByProjectApplicationId(objectId);
                final Map<String, String> projectAppNames = messageMaker.getProjectAppNames(projectApplicationsTbl);
                final Map<String, String[]> paramMap = messageMaker.geti18nCodeMap(projectAppNames);
                throw new CannotCreateRelationshipException("Relation already exist", "AMC_ERR0003", paramMap);
            }

        }
        if (objectType.equalsIgnoreCase(AdminMenuConfig.USER.name())) {
            UsersTbl usersTbl = this.userJpaDao.findByUserId(objectId);
            if (usersTbl == null) {
                String[] param = {objectId};
                throw new XMObjectNotFoundException("No User found", "P_ERR0009", param);
            }
        } else if (objectType.equalsIgnoreCase(AdminMenuConfig.USERAPPLICATION.name())) {
            UserApplicationsTbl userApplicationsTbl = this.userApplicationJpaDao.findByUserApplicationId(objectId);
            if (userApplicationsTbl == null) {
                String[] param = {objectId};
                throw new XMObjectNotFoundException("No User Application found", "S_ERR0010", param);
            }
        } else if (objectType.equalsIgnoreCase(AdminMenuConfig.PROJECTAPPLICATION.name())) {
            ProjectApplicationsTbl projectApplicationsTbl = this.projectApplicationJpaDao.findByProjectApplicationId(objectId);
            if (projectApplicationsTbl == null) {
                String[] param = {objectId};
                throw new XMObjectNotFoundException("No Project Application found", "S_ERR0017", param);
            }
        }
    }

    /**
     *
     * @param id
     * @return boolean
     */
    @Override
    public boolean deleteById(String id) {
        LOG.info(">> deleteById");
        boolean isDeleted = false;
        try {
            this.adminMenuConfigJpaDao.delete(id);
            isDeleted = true;
        } catch (Exception e) {
            if (e instanceof EmptyResultDataAccessException) {
                final String[] param = {id};
                throw new XMObjectNotFoundException("Admin Menu Config not found", "P_ERR0019", param);
            }
        }
        LOG.info("<< deleteById");
        return isDeleted;
    }

    @Override
    public AdminMenuConfigResponse multiDelete(Set<String> adminMenuConfigIds, HttpServletRequest httpServletRequest) {
        LOG.info(">> multiDelete");
        final List<Map<String, String>> statusMaps = new ArrayList<>();
        adminMenuConfigIds.forEach(adminMenuConfigId -> {
        	AdminMenuConfigTbl adminMenuConfigTbl = this.adminMenuConfigJpaDao.findOne(adminMenuConfigId);
            try {
            		this.deleteById(adminMenuConfigId);
            		this.adminMenuConfigAuditMgr.amcMultiDeleteAuditor(adminMenuConfigTbl, httpServletRequest);
            } catch (XMObjectNotFoundException objectNotFound) {
                Map<String, String> statusMap = messageMaker.extractFromException(objectNotFound);
                statusMaps.add(statusMap);
            }
        });
        AdminMenuConfigResponse configResponse = new AdminMenuConfigResponse(statusMaps);
        LOG.info(">> multiDelete");
        return configResponse;
    }

    /**
     *
     * @return AdminMenuConfigResponse
     */
    @Override
    public AdminMenuConfigResponse findAll() {
        LOG.info(">> findAll");
        final Iterable<AdminMenuConfigTbl> adminMenuConfigTbls = this.adminMenuConfigJpaDao.findAll();
        final AdminMenuConfigResponse amcr = new AdminMenuConfigResponse(adminMenuConfigTbls);
        LOG.info("<< findAll");
        return amcr;
    }

    /**
     *
     * @param objectType
     * @param userName
     * @return AdminMenuConfigResponse
     */
    @Override
    public AdminMenuConfigCustomeResponseWrapper findByObjectType(final AdminMenuConfig objectType,
            final String userName) {
        LOG.info(">> findByObjectType");
        List<AdminMenuConfigCustomeResponse> adminMenuConfigCustomeResponses = new ArrayList<>();
        List<AdminMenuConfigTbl> adminMenuConfigTbls = null;
        final ValidationRequest validationRequest = new ValidationRequest();
        validationRequest.setUserName(userName);
        validationRequest.setPermissionName("VIEW_INACTIVE");
        final boolean isInactiveAssignment = validator.isViewInactiveAllowed(validationRequest);
        LOG.info("findByObjectType isInactiveAssignment={}", isInactiveAssignment);
        switch (objectType) {
            case USER:
                adminMenuConfigTbls = this.adminMenuConfigJpaDao.findByObjectType(objectType.toString());
                adminMenuConfigTbls.forEach(adminMenuCTbls -> {
                    try {
                        final UsersTbl usersTbl = validator.validateUser(adminMenuCTbls.getObjectId(), isInactiveAssignment);
                        final AdminMenuConfigCustomeResponse customeResponse = new AdminMenuConfigCustomeResponse();
                        customeResponse.setAdminMenuConfigId(adminMenuCTbls.getAdminMenuConfigId());
                        customeResponse.setUsersTbl(usersTbl);
                        adminMenuConfigCustomeResponses.add(customeResponse);
                    } catch (XMObjectNotFoundException e) {
                        LOG.info("XMObjectNotFoundException={}", e);
                    }
                });
                Collections.sort(adminMenuConfigCustomeResponses, new Comparator<AdminMenuConfigCustomeResponse>() {
                    @Override
                    public int compare(AdminMenuConfigCustomeResponse arg0, AdminMenuConfigCustomeResponse arg1) {
                        return arg0.getUsersTbl().getUsername().compareTo(arg1.getUsersTbl().getUsername());
                    }
                });
                break;
            case USERAPPLICATION:
                adminMenuConfigTbls = this.adminMenuConfigJpaDao.findByObjectType(objectType.toString());
                adminMenuConfigTbls.forEach(adminMenuCTbls -> {
                    try {
                        final UserApplicationsTbl userApplicationsTbl = validator.validateUserApp(adminMenuCTbls.getObjectId(), isInactiveAssignment);
                        final AdminMenuConfigCustomeResponse customeResponse = new AdminMenuConfigCustomeResponse();
                        customeResponse.setAdminMenuConfigId(adminMenuCTbls.getAdminMenuConfigId());
                        customeResponse.setUserApplicationsTbl(userApplicationsTbl);
                        adminMenuConfigCustomeResponses.add(customeResponse);
                    } catch (XMObjectNotFoundException e) {
                        LOG.info("XMObjectNotFoundException={}", e);
                    }
                });
                Collections.sort(adminMenuConfigCustomeResponses, new Comparator<AdminMenuConfigCustomeResponse>() {
                    @Override
                    public int compare(AdminMenuConfigCustomeResponse arg0, AdminMenuConfigCustomeResponse arg1) {
                        return arg0.getUserApplicationsTbl().getName().compareTo(arg1.getUserApplicationsTbl().getName());
                    }
                });
                break;
            case PROJECTAPPLICATION:
                adminMenuConfigTbls = this.adminMenuConfigJpaDao.findByObjectType(objectType.toString());
                adminMenuConfigTbls.forEach(adminMenuCTbls -> {
                    try {
                        final ProjectApplicationsTbl projectApplicationsTbl = validator.validateProjectApp(adminMenuCTbls.getObjectId(), isInactiveAssignment);
                        final AdminMenuConfigCustomeResponse customeResponse = new AdminMenuConfigCustomeResponse();
                        customeResponse.setAdminMenuConfigId(adminMenuCTbls.getAdminMenuConfigId());
                        customeResponse.setProjectApplicationsTbl(projectApplicationsTbl);
                        adminMenuConfigCustomeResponses.add(customeResponse);
                    } catch (XMObjectNotFoundException e) {
                        LOG.info("XMObjectNotFoundException={}", e);
                    }
                });
                Collections.sort(adminMenuConfigCustomeResponses, new Comparator<AdminMenuConfigCustomeResponse>() {
                    @Override
                    public int compare(AdminMenuConfigCustomeResponse arg0, AdminMenuConfigCustomeResponse arg1) {
                        return arg0.getProjectApplicationsTbl().getName().compareTo(arg1.getProjectApplicationsTbl().getName());
                    }
                });
                break;
            default:
                break;
        }
        final AdminMenuConfigCustomeResponseWrapper responseWrapper
                = new AdminMenuConfigCustomeResponseWrapper(adminMenuConfigCustomeResponses);
        LOG.info("<< findByObjectType");
        return responseWrapper;
    }

    /**
     *
     * @param configRequest
     * @param isUpdate
     * @return AdminMenuConfigTbl
     */
    private AdminMenuConfigTbl convertToEntity(final AdminMenuConfigRequest configRequest,
            boolean isUpdate) {
        String id = UUID.randomUUID().toString();
        if (isUpdate) {
            id = configRequest.getId();
        }
        final Date date = new Date();
        AdminMenuConfig objectType = configRequest.getObjectType();
        String objectId = configRequest.getObjectId();

        AdminMenuConfigTbl adminMenuConfigTbl = new AdminMenuConfigTbl(id);
        adminMenuConfigTbl.setObjectType(objectType.toString());
        adminMenuConfigTbl.setObjectId(objectId);
        adminMenuConfigTbl.setCreateDate(date);
        adminMenuConfigTbl.setUpdateDate(date);

        return adminMenuConfigTbl;
    }

    /**
     *
     * @param userName
     * @return AdminMenuConfigResponseWrapper
     */
    @Override
    public AdminMenuConfigResponseWrapper findUserBasedConfiguration(final String userName) {
        final ValidationRequest validationRequest = new ValidationRequest();
        validationRequest.setUserName(userName);
        validationRequest.setPermissionName("VIEW_INACTIVE");
        final boolean isInactiveAssignment = validator.isViewInactiveAllowed(validationRequest);
        LOG.info("findUserBasedConfiguration isInactiveAssignment={}", isInactiveAssignment);
        final UsersTbl usersTbl = this.userJpaDao.findByUsernameIgnoreCase(userName);
        Set<UserApplicationMenuResponse> userApplicationMenuResponses = null;
        Set<ProjectApplicationMenuResponse> projectApplicationMenuResponses = null;
        boolean isAdminMenuUser = false;
        if (null != usersTbl) {
            AdminMenuConfigTbl adminMConfigTbl = this.adminMenuConfigJpaDao.findByObjectTypeAndObjectId(AdminMenuConfig.USER.name(), usersTbl.getUserId());
            if (null != adminMConfigTbl) {
            	isAdminMenuUser = true;
                String[] objectTypes = new String[]{AdminMenuConfig.USERAPPLICATION.name(),
                    AdminMenuConfig.PROJECTAPPLICATION.name()};
                List<AdminMenuConfigTbl> adminMenuConfigTbls = this.adminMenuConfigJpaDao.findByObjectTypeIn(Arrays.asList(objectTypes));
                userApplicationMenuResponses = new HashSet<>();
                projectApplicationMenuResponses = new HashSet<>();
                for (AdminMenuConfigTbl adminMenuConfigTbl : adminMenuConfigTbls) {
                    final String objectType = adminMenuConfigTbl.getObjectType();
                    final String objectId = adminMenuConfigTbl.getObjectId();
                    final AdminMenuConfig adminMenuConfig = AdminMenuConfig.valueOf(objectType);
                    switch (adminMenuConfig) {
                        case USERAPPLICATION:
                            try {
                                final UserApplicationsTbl userApplicationsTbl = validator.validateUserApp(objectId, isInactiveAssignment);
                                UserApplicationMenuResponse userApplicationMenuResponse = new UserApplicationMenuResponse();
                                userApplicationMenuResponse.setUserApplicationsTbl(userApplicationsTbl);
                                final boolean isParent = Boolean.valueOf(userApplicationsTbl.getIsParent());
                                if (isParent) {
                                    Iterable<UserApplicationsTbl> userAppChildren
                                            = this.userApplicationJpaDao.findByPosition(userApplicationsTbl.getUserApplicationId());
                                    if(userAppChildren != null) {
                                    	userAppChildren = validator.filterUserApplicationResponse(isInactiveAssignment, userAppChildren);
                                    }
                                    userApplicationMenuResponse.setUserAppChildren(userAppChildren);
                                }
                                userApplicationMenuResponses.add(userApplicationMenuResponse);
                            } catch (XMObjectNotFoundException e) {
                                LOG.info("XMObjectNotFoundException={}", e);
                            }
                            break;
                        case PROJECTAPPLICATION:
                            try {
                                final ProjectApplicationsTbl projectApplicationsTbl = validator.validateProjectApp(objectId, isInactiveAssignment);
                                ProjectApplicationMenuResponse projectApplicationMenuResponse = new ProjectApplicationMenuResponse();
                                projectApplicationMenuResponse.setProjectApplicationsTbl(projectApplicationsTbl);
                                final boolean isParent = Boolean.valueOf(projectApplicationsTbl.getIsParent());
                                if (isParent) {
                                    Iterable<ProjectApplicationsTbl> projectAppChildren = this.projectApplicationJpaDao.
                                            findByPosition(projectApplicationsTbl.getProjectApplicationId());
                                    if(projectAppChildren != null) {
                                    	projectAppChildren = validator.filterProjectApplicationResponse(isInactiveAssignment, projectAppChildren);
                                    }
                                    projectApplicationMenuResponse.setProjectAppChildren(projectAppChildren);
                                }
                                projectApplicationMenuResponses.add(projectApplicationMenuResponse);
                            } catch (XMObjectNotFoundException e) {
                                LOG.info("XMObjectNotFoundException={}", e);
                            }
                            break;
                        default:
                            break;
                    }
                }
            } else {
                String[] param = {userName};
                throw new XMObjectNotFoundException("No User Found", "USR_ERR0001", param);
            }

        } else {
            String[] param = {userName};
            throw new XMObjectNotFoundException("No User Found", "USR_ERR0001", param);
        }
        final AdminMenuConfigResponseWrapper responseWrapper
                = new AdminMenuConfigResponseWrapper(isAdminMenuUser, userApplicationMenuResponses, projectApplicationMenuResponses);
        return responseWrapper;
    }

}
