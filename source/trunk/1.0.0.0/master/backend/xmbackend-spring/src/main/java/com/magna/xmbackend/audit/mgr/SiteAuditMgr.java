/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magna.xmbackend.audit.mgr;

import javax.servlet.http.HttpServletRequest;

import com.magna.xmbackend.entities.SitesTbl;
import com.magna.xmbackend.vo.jpa.site.SiteRequest;

/**
 *
 * @author dhana
 */
public interface SiteAuditMgr {

    void siteCreateSuccessAudit(HttpServletRequest hsr, SitesTbl st);

    void siteCreateFailureAudit(HttpServletRequest hsr, SiteRequest sr,
            String errMsg);

    void siteDeleteSuccessAudit(HttpServletRequest hsr, String id, String name);

    void siteDeleteFailureAudit(HttpServletRequest hsr, String id,
            String errMsg);

    void siteUpdateSuccessAudit(HttpServletRequest hsr, SiteRequest sr);

    void siteUpdateFailureAudit(HttpServletRequest hsr, SiteRequest sr,
            String errMsg);

    void siteUpdateSuccessAudit(HttpServletRequest hsr, String id, String name, String status);

    void siteUpdateFailureAudit(HttpServletRequest hsr, String id, 
            String errMsg);

	void siteStatusUpdateSuccessAuditor(String siteName, String status, HttpServletRequest httpServletRequest);

	void siteStatusUpdateFailureAuditor(String siteName, String status, Exception ex,
			HttpServletRequest httpServletRequest);
	
	void siteCreateFailureBatchAudit(HttpServletRequest hsr, SiteRequest sr);
}
