/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magna.xmbackend.jpa.dao;

import com.magna.xmbackend.entities.LanguagesTbl;
import com.magna.xmbackend.entities.StartAppTranslationTbl;
import com.magna.xmbackend.entities.StartApplicationsTbl;
import javax.transaction.Transactional;
import org.springframework.data.repository.CrudRepository;

/**
 *
 * @author dana
 */
@Transactional
public interface StartAppTranslationJpaDao extends CrudRepository<StartAppTranslationTbl, String> {
    StartAppTranslationTbl findByStartApplicationIdAndLanguageCode(StartApplicationsTbl startApplicationId, LanguagesTbl languageCode);
}
