package com.magna.xmbackend.rel.mgr.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Component;

import com.magna.xmbackend.audit.rel.mgr.UserProjectAppRelAuditMgr;
import com.magna.xmbackend.entities.AdminAreaProjAppRelTbl;
import com.magna.xmbackend.entities.AdminAreaProjectRelTbl;
import com.magna.xmbackend.entities.AdminAreasTbl;
import com.magna.xmbackend.entities.ProjectApplicationsTbl;
import com.magna.xmbackend.entities.ProjectsTbl;
import com.magna.xmbackend.entities.SiteAdminAreaRelTbl;
import com.magna.xmbackend.entities.UserProjAppRelTbl;
import com.magna.xmbackend.entities.UserProjectRelTbl;
import com.magna.xmbackend.entities.UsersTbl;
import com.magna.xmbackend.exception.CannotCreateRelationshipException;
import com.magna.xmbackend.exception.XMObjectNotFoundException;
import com.magna.xmbackend.jpa.rel.dao.AdminAreaProjectAppRelJpaDao;
import com.magna.xmbackend.jpa.rel.dao.AdminAreaProjectRelJpaDao;
import com.magna.xmbackend.jpa.rel.dao.SiteAdminAreaRelJpaDao;
import com.magna.xmbackend.jpa.rel.dao.UserProjectAppRelJpaDao;
import com.magna.xmbackend.jpa.rel.dao.UserProjectRelJpaDao;
import com.magna.xmbackend.mgr.ProjectApplicationMgr;
import com.magna.xmbackend.rel.mgr.UserProjectAppRelMgr;
import com.magna.xmbackend.rel.mgr.UserProjectRelMgr;
import com.magna.xmbackend.utils.MessageMaker;
import com.magna.xmbackend.utils.Validator;
import com.magna.xmbackend.vo.enums.UserRelationType;
import com.magna.xmbackend.vo.permission.ValidationRequest;
import com.magna.xmbackend.vo.rel.UserProjectAppRelBatchRequest;
import com.magna.xmbackend.vo.rel.UserProjectAppRelBatchResponse;
import com.magna.xmbackend.vo.rel.UserProjectAppRelRequest;
import com.magna.xmbackend.vo.rel.UserProjectAppRelResponse;

/**
 *
 * @author vijay
 */
@Component
public class UserProjectAppRelMgrImpl implements UserProjectAppRelMgr {

    private static final Logger LOG
            = LoggerFactory.getLogger(UserProjectAppRelMgrImpl.class);

    @Autowired
    private UserProjectAppRelJpaDao userProjectAppRelJpaDao;

    @Autowired
    private MessageMaker messageMaker;

    @Autowired
    private UserProjectRelMgr userProjectRelMgr;

    @Autowired
    private AdminAreaProjectRelJpaDao adminAreaProjectRelJpaDao;

    @Autowired
    private SiteAdminAreaRelJpaDao siteAdminAreaRelJpaDao;

    @Autowired
    private UserProjectRelJpaDao userProjectRelJpaDao;

    @Autowired
    private Validator validator;
    @Autowired
    private AdminAreaProjectAppRelJpaDao adminAreaProjectAppRelJpaDao;
    @Autowired
    private ProjectApplicationMgr projectApplicationMgr;
    
    @Autowired
    private UserProjectAppRelAuditMgr userProjectAppRelAuditMgr;

    /**
     *
     * @return UserProjectAppRelResponse
     */
    @Override
    public final UserProjectAppRelResponse findAll(final ValidationRequest validationRequest) {
    	LOG.info(">> findAll");
    	final boolean isInactiveAssignment = validator.isViewInactiveAllowed(validationRequest);
    	LOG.info("findAll isInactiveAssignment={}", isInactiveAssignment);
        final Iterable<UserProjAppRelTbl> userProjectAppRelTbls = userProjectAppRelJpaDao.findAll();
        //userProjectAppRelTbls = this.validator.filterUserProjectAppRel(isInactiveAssignment, userProjectAppRelTbls);
        final UserProjectAppRelResponse userProjectAppRelResponse = new UserProjectAppRelResponse(userProjectAppRelTbls);
        LOG.info("<< findAll");
        return userProjectAppRelResponse;
    }

    /**
     *
     * @param id
     * @return UserProjAppRelTbl
     */
    @Override
    public final UserProjAppRelTbl findById(String id) {
        LOG.info(">> findById {}", id);
        final UserProjAppRelTbl userProjAppRelTbl = this.userProjectAppRelJpaDao.findOne(id);
        LOG.info("<< findById");
        return userProjAppRelTbl;
    }

    /**
     *
     * @param userProjectAppRelRequest
     * @param userName
     * @return UserProjAppRelTbl
     */
    @Override
    public final UserProjAppRelTbl create(final UserProjectAppRelRequest userProjectAppRelRequest,
            final String userName) {
        UserProjAppRelTbl uparOut = null;
        final boolean hotlineRequest = userProjectAppRelRequest.isHotlineRequest();
        final String projectAppId = userProjectAppRelRequest.getProjectAppId();
        LOG.info(">> create");
        final Map<String, Object> assignmentAllowedMap
                = isInActiveAssignmentsAllowed(userProjectAppRelRequest, userName);
        LOG.info("assignmentAllowedMap={}", assignmentAllowedMap);
        if (assignmentAllowedMap.containsKey("isAssignmentAllowed")) {
            boolean isAssignmentAllowed = (boolean) assignmentAllowedMap.get("isAssignmentAllowed");
            if (isAssignmentAllowed) {

            	if(hotlineRequest) {
            		final boolean check = this.checkIfHotlineUserExistsInProjectApp(userProjectAppRelRequest);
            		if(check) {
            			String userRelationType = userProjectAppRelRequest.getUserRelationType();
            			if (UserRelationType.ALLOWED.name().equalsIgnoreCase(userRelationType)) {
            				userRelationType = UserRelationType.FORBIDDEN.name();
            			} else {
            				userRelationType = UserRelationType.ALLOWED.name();
            			}
            			final String userProjectRelId = userProjectAppRelRequest.getUserProjectRelId();
            			final String adminAreaProjRelId = userProjectAppRelRequest.getAdminAreaProjRelId();
            			final UserProjAppRelTbl uprojectRelTbl = this.userProjectAppRelJpaDao.
            					findByUserProjectRelIdAndAdminAreaProjectRelIdAndProjectApplicationIdAndUserRelType(new UserProjectRelTbl(userProjectRelId), 
            							new AdminAreaProjectRelTbl(adminAreaProjRelId), new ProjectApplicationsTbl(projectAppId), userRelationType);
            					//findByUserIdAndSiteAdminAreaRelIdAndUserRelType(new UsersTbl(userId), new SiteAdminAreaRelTbl(siteAdminAreaRelId), userRelationType);
            			if (null != uprojectRelTbl) {
            				final String userProjAppRelId = uprojectRelTbl.getUserProjAppRelId();
            				this.userProjectAppRelJpaDao.updateUserRelType(userProjectAppRelRequest.getUserRelationType(), userProjAppRelId);
            			}
            		} else {
            			final UserProjAppRelTbl upart = this.convert2Entity(userProjectAppRelRequest);
            			uparOut = this.userProjectAppRelJpaDao.save(upart);
            		}
            		
            	} else {
            		this.checkIfUserExistsInProjectApp(userProjectAppRelRequest);
            		final UserProjAppRelTbl upart = this.convert2Entity(userProjectAppRelRequest);
            		uparOut = this.userProjectAppRelJpaDao.save(upart);
            	}
                
                //this.checkIfUserAppNotExistInSiteAdminArea(userUserAppRelRequest);
                
            
            	
            	
                /*this.checkIfUserExistsInProjectApp(userProjectAppRelRequest);
                //this.checkForUserProjectRelCreation(userProjectAppRelRequest);
                final UserProjAppRelTbl uparIn = this.convert2Entity(userProjectAppRelRequest);
                uparOut = userProjectAppRelJpaDao.save(uparIn);*/
            } else {
                if (assignmentAllowedMap.containsKey("projectApplicationsTbl")) {
                    final Map<String, String> projectAppTransMap
                            = messageMaker.getProjectAppNames((ProjectApplicationsTbl) assignmentAllowedMap.get("projectApplicationsTbl"));
                    final Map<String, String> userTransMap
                            = messageMaker.getUserNames((UsersTbl) assignmentAllowedMap.get("usersTbl"));
                    final Map<String, String[]> i18nCodeMap = this.messageMaker.geti18nCodeMap(userTransMap, projectAppTransMap);
                    LOG.debug("i18nCodeMap={}", i18nCodeMap);
                    throw new CannotCreateRelationshipException(
                            "Inactive Assignments not allowed", "UPA_ERR0001", i18nCodeMap);
                }
            }
        }

        LOG.info("<< create");
        return uparOut;
    }

    @SuppressWarnings("unused")
	private void checkForUserProjectRelCreation(final UserProjectAppRelRequest userProjectAppRelRequest) {
        final String adminAreaProjRelId = userProjectAppRelRequest.getAdminAreaProjRelId();
        AdminAreaProjectRelTbl adminAreaProjectRelTbl = this.adminAreaProjectRelJpaDao.findOne(adminAreaProjRelId);
        AdminAreasTbl adminAreasTbl = adminAreaProjectRelTbl.getSiteAdminAreaRelId().getAdminAreaId();

        final String projectAppId = userProjectAppRelRequest.getProjectAppId();

        List<AdminAreaProjAppRelTbl> adminAreaProjAppRelTbls = this.adminAreaProjectAppRelJpaDao.findByAdminAreaProjectRelId(new AdminAreaProjectRelTbl(adminAreaProjRelId));
        List<ProjectApplicationsTbl> projectApplicationsTbls = null;
        if (adminAreaProjAppRelTbls.size() > 0) {
            projectApplicationsTbls = new ArrayList<>();
            for (AdminAreaProjAppRelTbl adminAreaProjAppRelTbl : adminAreaProjAppRelTbls) {
                projectApplicationsTbls.add(adminAreaProjAppRelTbl.getProjectApplicationId());
            }
            ProjectApplicationsTbl projectApplicationsTbl = this.projectApplicationMgr.findById(projectAppId);
            if (!projectApplicationsTbls.contains(projectApplicationsTbl)) {
                final Map<String, String> projectAppNames = this.messageMaker.getProjectAppNames(projectApplicationsTbl);
                final Map<String, String> adminAreas = this.messageMaker.getAdminAreaNames(adminAreasTbl);
                final Map<String, String[]> i18nCodeMap = this.messageMaker.geti18nCodeMap(projectAppNames, adminAreas);
                throw new CannotCreateRelationshipException("Invalid Relation", "ERR0019", i18nCodeMap);
            }
        }
    }

    /**
     *
     * @param userProjectAppRelRequest
     * @param userName
     * @return Map
     */
    private Map<String, Object> isInActiveAssignmentsAllowed(final UserProjectAppRelRequest userProjectAppRelRequest,
            final String userName) {
        final boolean isViewInactiveAllowed = isViewInActiveAllowed(userName);
        LOG.debug("isViewInactiveAllowed={}", isViewInactiveAllowed);
        final Map<String, Object> assignmentAllowedMap
                = validator.isUserProjectAppInactiveAssignmentAllowed(userProjectAppRelRequest,
                        isViewInactiveAllowed);
        return assignmentAllowedMap;
    }

    /**
     *
     * @param userName
     * @return boolean
     */
    private boolean isViewInActiveAllowed(final String userName) {
        final ValidationRequest validationRequest
                = validator.formSaveRelationValidationRequest(userName, "PROJECTAPPLICATION_USER");
        LOG.info("validationRequest={}", validationRequest);
        final boolean isInactiveAssignment = validator.isViewInactiveAllowed(validationRequest);
        LOG.info("isInactiveAssignment={}", isInactiveAssignment);
        return isInactiveAssignment;
    }

    /**
     *
     * @param uprr
     */
    //Error Message: User 'name' already exists in Project Application 'name'.
    private void checkIfUserExistsInProjectApp(final UserProjectAppRelRequest uparr) {
        final String userProjRelationIdIn = uparr.getUserProjectRelId();
        final String projectAppIdIn = uparr.getProjectAppId();
        final String adminAreaProjRelId = uparr.getAdminAreaProjRelId();

        LOG.debug("userProjRelationIdIn {} and projectId {}",
                userProjRelationIdIn, projectAppIdIn);

        final UserProjectRelTbl userProjRelTbl = this.userProjectRelMgr.findById(userProjRelationIdIn);
        String userProjRelIdFromTbl = userProjRelTbl.getUserProjectRelId();
        final Collection<UserProjAppRelTbl> userProjectAppRelTblCollection = userProjRelTbl.getUserProjAppRelTblCollection();
        LOG.debug("size={}", userProjectAppRelTblCollection.size());
        AdminAreaProjectRelTbl adminAreaProjectRelTbl = this.adminAreaProjectRelJpaDao.findOne(adminAreaProjRelId);
        String aaProjectRelIdFromTbl = adminAreaProjectRelTbl.getAdminAreaProjectRelId();
        UserProjAppRelTbl userProjectApplicationRelTbl = this.userProjectAppRelJpaDao.findByUserProjectRelIdAndProjectApplicationId(new UserProjectRelTbl(userProjRelationIdIn), new ProjectApplicationsTbl(projectAppIdIn));
        AdminAreasTbl adminAreaId = null;
        if (null != userProjectApplicationRelTbl) {
            AdminAreaProjectRelTbl adminAreaProjectRelId = userProjectApplicationRelTbl.getAdminAreaProjectRelId();
            adminAreaId = adminAreaProjectRelId.getSiteAdminAreaRelId().getAdminAreaId();
        }

        for (final UserProjAppRelTbl userProjectAppRelTbl : userProjectAppRelTblCollection) {
            final ProjectApplicationsTbl projectsAppTbl = userProjectAppRelTbl.getProjectApplicationId();
            final String projectAppIdFromTbl = projectsAppTbl.getProjectApplicationId();
            LOG.debug("projectAppIdFromTbl={}", projectAppIdFromTbl);
            if (projectAppIdFromTbl != null && projectAppIdFromTbl.equalsIgnoreCase(projectAppIdIn) && aaProjectRelIdFromTbl.equalsIgnoreCase(adminAreaProjRelId)
                    && userProjRelIdFromTbl.equalsIgnoreCase(userProjRelationIdIn)) {
                //throw ex
                //User 'name' already exists in Project App 'name'
                final UsersTbl usersTbl = userProjRelTbl.getUserId();
                LOG.debug("usersTbl={}", usersTbl);
                final ProjectsTbl projectsTbl = userProjRelTbl.getProjectId();
                final Map<String, String> userNamesMap = this.messageMaker.getUserNames(usersTbl);
                LOG.debug("userNames={}", userNamesMap);
                Map<String, String> projectNamesMap = this.messageMaker.getProjectNames(projectsTbl);
                LOG.debug("projectNamesMap={}", projectNamesMap);
                final Map<String, String> projectAppNames = this.messageMaker.getProjectAppNames(projectsAppTbl);
                LOG.debug("projectAppNames={}", projectAppNames);
                Map<String, String> adminAreasMap = this.messageMaker.getAdminAreaNames(adminAreaId);

                final Map<String, String[]> i18nCodeMap = this.messageMaker.geti18nCodeMap(projectAppNames, userNamesMap, projectNamesMap, adminAreasMap);
                throw new CannotCreateRelationshipException(
                        "Relationship exists", "ERR0017", i18nCodeMap);
            }
        }
    }
    
    
    
    private boolean checkIfHotlineUserExistsInProjectApp(final UserProjectAppRelRequest uparr) {
        final String userProjRelationIdIn = uparr.getUserProjectRelId();
        final String projectAppIdIn = uparr.getProjectAppId();
        final String adminAreaProjRelId = uparr.getAdminAreaProjRelId();
        final String userRelationType = uparr.getUserRelationType();

        LOG.debug("userProjRelationIdIn {} and projectId {}",
                userProjRelationIdIn, projectAppIdIn);

        final UserProjectRelTbl userProjRelTbl = this.userProjectRelMgr.findById(userProjRelationIdIn);
        String userProjRelIdFromTbl = userProjRelTbl.getUserProjectRelId();
        
        final List<UserProjAppRelTbl> userProjAppRelTbls = this.userProjectAppRelJpaDao.findByUserProjectRelIdAndAdminAreaProjectRelIdAndProjectApplicationId(new UserProjectRelTbl(userProjRelationIdIn), new AdminAreaProjectRelTbl(adminAreaProjRelId), new ProjectApplicationsTbl(projectAppIdIn));
        //final Collection<UserProjAppRelTbl> userProjectAppRelTblCollection = userProjRelTbl.getUserProjAppRelTblCollection();
        LOG.debug("size={}", userProjAppRelTbls.size());
        AdminAreaProjectRelTbl adminAreaProjectRelTbl = this.adminAreaProjectRelJpaDao.findOne(adminAreaProjRelId);
        String aaProjectRelIdFromTbl = adminAreaProjectRelTbl.getAdminAreaProjectRelId();
        /*UserProjAppRelTbl userProjectApplicationRelTbl = this.userProjectAppRelJpaDao.findByUserProjectRelIdAndProjectApplicationId(new UserProjectRelTbl(userProjRelationIdIn), new ProjectApplicationsTbl(projectAppIdIn));
        String userRelType = null;
        AdminAreasTbl adminAreaId = null;
        if (null != userProjectApplicationRelTbl) {
        	userRelType = userProjectApplicationRelTbl.getUserRelType();
            AdminAreaProjectRelTbl adminAreaProjectRelId = userProjectApplicationRelTbl.getAdminAreaProjectRelId();
            adminAreaId = adminAreaProjectRelId.getSiteAdminAreaRelId().getAdminAreaId();
        }*/

        for (final UserProjAppRelTbl userProjectAppRelTbl : userProjAppRelTbls) {
            final ProjectApplicationsTbl projectsAppTbl = userProjectAppRelTbl.getProjectApplicationId();
            final String projectAppIdFromTbl = projectsAppTbl.getProjectApplicationId();
            LOG.debug("projectAppIdFromTbl={}", projectAppIdFromTbl);
            String userRelType = userProjectAppRelTbl.getUserRelType();
            if (UserRelationType.ALLOWED.name().equalsIgnoreCase(userRelType)) {
            	userRelType = UserRelationType.FORBIDDEN.name();
			} else {
				userRelType = UserRelationType.ALLOWED.name();
			}
            if (projectAppIdFromTbl != null && projectAppIdFromTbl.equalsIgnoreCase(projectAppIdIn) && aaProjectRelIdFromTbl.equalsIgnoreCase(adminAreaProjRelId)
                    && userProjRelIdFromTbl.equalsIgnoreCase(userProjRelationIdIn) && userRelationType.equals(userRelType)) {/*
                //throw ex
                //User 'name' already exists in Project App 'name'
                final UsersTbl usersTbl = userProjRelTbl.getUserId();
                LOG.debug("usersTbl={}", usersTbl);
                final ProjectsTbl projectsTbl = userProjRelTbl.getProjectId();
                final Map<String, String> userNamesMap = this.messageMaker.getUserNames(usersTbl);
                LOG.debug("userNames={}", userNamesMap);
                Map<String, String> projectNamesMap = this.messageMaker.getProjectNames(projectsTbl);
                LOG.debug("projectNamesMap={}", projectNamesMap);
                final Map<String, String> projectAppNames = this.messageMaker.getProjectAppNames(projectsAppTbl);
                LOG.debug("projectAppNames={}", projectAppNames);
                Map<String, String> adminAreasMap = this.messageMaker.getAdminAreaNames(adminAreaId);

                final Map<String, String[]> i18nCodeMap = this.messageMaker.geti18nCodeMap(projectAppNames, userNamesMap, projectNamesMap, adminAreasMap);
                throw new CannotCreateRelationshipException(
                        "Relationship exists", "ERR0017", i18nCodeMap);
            */
            	return true;
            } else {
            	return false;
            }
        }
		return false;
    }
    

    /**
     *
     * @param id
     * @return boolean
     */
    @Override
    public final boolean delete(final String id) {
        LOG.info(">> delete {}", id);
        boolean isDeleted = false;
        try {
            this.userProjectAppRelJpaDao.delete(id);
            isDeleted = true;
        } catch (Exception e) {
            if (e instanceof EmptyResultDataAccessException) {
                final String[] param = {id};
                throw new XMObjectNotFoundException("User not found", "P_ERR0025", param);
            }
        }

        LOG.info("<< deleteById");
        return isDeleted;

    }

    /**
     *
     * @param ids
     * @return UserProjectAppRelResponse
     */
    @Override
    public UserProjectAppRelResponse multiDelete(Set<String> ids, HttpServletRequest httpServletRequest) {
        LOG.info(">> multiDelete");
        final List<Map<String, String>> statusMaps = new ArrayList<>();
        ids.forEach(id -> {
        	UserProjAppRelTbl userProjAppRelTbl = this.userProjectAppRelJpaDao.findOne(id);
            try {
            		this.delete(id);
            		this.userProjectAppRelAuditMgr.userProjectAppMultiDeleteSuccessAudit(userProjAppRelTbl, httpServletRequest);
            } catch (XMObjectNotFoundException objectNotFound) {
                Map<String, String> statusMap = messageMaker.extractFromException(objectNotFound);
                statusMaps.add(statusMap);
            }
        });
        UserProjectAppRelResponse userResponse = new UserProjectAppRelResponse(statusMaps);
        LOG.info(">> multiDelete");
        return userResponse;
    }

    /**
     *
     * @param userProjectAppRelRequest
     * @return UserProjAppRelTbl
     */
    private UserProjAppRelTbl convert2Entity(final UserProjectAppRelRequest userProjectAppRelRequest) {
        final String id = UUID.randomUUID().toString();
        final String userProjectRelId = userProjectAppRelRequest.getUserProjectRelId();
        final String projectAppId = userProjectAppRelRequest.getProjectAppId();
        final String userRelationType = userProjectAppRelRequest.getUserRelationType();
        final String adminAreaProjRelId = userProjectAppRelRequest.getAdminAreaProjRelId();

        final UserProjAppRelTbl userProjAppRelTbl = new UserProjAppRelTbl(id);
        userProjAppRelTbl.setUserProjectRelId(new UserProjectRelTbl(userProjectRelId));
        userProjAppRelTbl.setUserRelType(userRelationType);
        userProjAppRelTbl.setProjectApplicationId(new ProjectApplicationsTbl(projectAppId));
        userProjAppRelTbl.setAdminAreaProjectRelId(new AdminAreaProjectRelTbl(adminAreaProjRelId));
        return userProjAppRelTbl;
    }

    /**
     *
     * @param userProjectAppRelBatchRequest
     * @param userName
     * @return UserProjectAppRelBatchResponse
     */
    @Override
    public final UserProjectAppRelBatchResponse createBatch(final UserProjectAppRelBatchRequest userProjectAppRelBatchRequest,
            final String userName) {
        final List<UserProjAppRelTbl> userProjectAppRelTbls = new ArrayList<>();
        final List<Map<String, String>> statusMaps = new ArrayList<>();
        final List<UserProjectAppRelRequest> userProjectAppRelRequests
                = userProjectAppRelBatchRequest.getUserProjectAppRelRequests();
        for (final UserProjectAppRelRequest userProjectAppRelRequest : userProjectAppRelRequests) {
            try {
                final UserProjAppRelTbl aapartOut = this.create(userProjectAppRelRequest, userName);
                userProjectAppRelTbls.add(aapartOut);
            } catch (CannotCreateRelationshipException ccre) {
                final Map<String, String> statusMap = messageMaker.extractFromException(ccre);
                statusMaps.add(statusMap);
            }
        }
        final UserProjectAppRelBatchResponse userProjectAppRelBatchResponse
                = new UserProjectAppRelBatchResponse(userProjectAppRelTbls, statusMaps);
        return userProjectAppRelBatchResponse;
    }

    /**
     *
     * @param userProjectRelId
     * @return UserProjectAppRelResponse
     */
    @Override
    public UserProjectAppRelResponse findUserProjectAppRelsByUserProjectRelId(String userProjectRelId) {
        List<UserProjAppRelTbl> userProjAppRelTbls = this.userProjectAppRelJpaDao.findByUserProjectRelId(new UserProjectRelTbl(userProjectRelId));
        if (userProjAppRelTbls.isEmpty()) {
            throw new RuntimeException("No Relation Found");
        }
        UserProjectAppRelResponse userProjectAppRelResponse = new UserProjectAppRelResponse(userProjAppRelTbls);
        return userProjectAppRelResponse;
    }

    /**
     *
     * @param userProjectRelId
     * @param adminAreaProjRelId
     * @param validationRequest
     * @return UserProjectAppRelResponse
     */
    @Override
    public UserProjectAppRelResponse findUserProjectAppRelsByUserProjRelIdAndAAPRelId(final String userProjectRelId,
            final String adminAreaProjRelId, final ValidationRequest validationRequest) {
    	final boolean isInactiveAssignment = validator.isViewInactiveAllowed(validationRequest);
    	LOG.info("findAllProjectAppsByAdminAreaProjectRelId isInactiveAssignment={}", isInactiveAssignment);
    	UserProjectRelTbl validatedUserProjectRel = validator.validateUserProjectRel(userProjectRelId, isInactiveAssignment);
    	String userId = validatedUserProjectRel.getUserId().getUserId();
    	String projectId = validatedUserProjectRel.getProjectId().getProjectId();
    	validator.validateUser(userId, isInactiveAssignment);
    	validator.validateProject(projectId, isInactiveAssignment);
    	
    	AdminAreaProjectRelTbl validatedAdminAreaProject = validator.validateAdminAreaProject(adminAreaProjRelId, isInactiveAssignment);
    	SiteAdminAreaRelTbl siteAdminAreaRelTbl = validatedAdminAreaProject.getSiteAdminAreaRelId();
    	siteAdminAreaRelTbl = validator.validateSiteAdminArea(siteAdminAreaRelTbl.getSiteAdminAreaRelId(), isInactiveAssignment);
    	String adminAreaId = siteAdminAreaRelTbl.getAdminAreaId().getAdminAreaId();
    	String siteId = siteAdminAreaRelTbl.getSiteId().getSiteId();
    	validator.validateAdminArea(adminAreaId, isInactiveAssignment);
    	validator.validateSite(siteId, isInactiveAssignment);
    	
    	
    	List<UserProjAppRelTbl> userProjAppRelTbls = this.userProjectAppRelJpaDao.findByUserProjectRelIdAndAdminAreaProjectRelId(validatedUserProjectRel, validatedAdminAreaProject);
    	List<UserProjAppRelTbl> newUserProjAppRelTbls = new ArrayList<>();
    	for (UserProjAppRelTbl userProjAppRelTbl : userProjAppRelTbls) {
        	String projectAppId = userProjAppRelTbl.getProjectApplicationId().getProjectApplicationId();
        	validator.validateProjectApp(projectAppId, isInactiveAssignment);
        	newUserProjAppRelTbls.add(userProjAppRelTbl);
		}
    	if (newUserProjAppRelTbls.isEmpty()) {
            throw new XMObjectNotFoundException("No Relation Found", "U_ERR0006");
        }
        UserProjectAppRelResponse userProjectAppRelResponse = new UserProjectAppRelResponse(newUserProjAppRelTbls);
        return userProjectAppRelResponse;
    }

    

	/**
     *
     * @param userProjectRelId
     * @param userRelType
     * @return UserProjectAppRelResponse
     */
    @Override
    public UserProjectAppRelResponse findUserProjectAppRelsByUserProjectRelIdAndUserRelType(String userProjectRelId,
            String userRelType) {
        List<UserProjAppRelTbl> userProjAppRelTbls = this.userProjectAppRelJpaDao.findByUserProjectRelIdAndUserRelType(new UserProjectRelTbl(userProjectRelId), userRelType);
        if (userProjAppRelTbls.isEmpty()) {
            throw new RuntimeException("No Relation Found");
        }
        UserProjectAppRelResponse userProjectAppRelResponse = new UserProjectAppRelResponse(userProjAppRelTbls);
        return userProjectAppRelResponse;
    }

    /**
     *
     * @param projectAppId
     * @return UserProjectAppRelResponse
     */
    @Override
    public UserProjectAppRelResponse findUserProjectAppRelsByProjectAppId(String projectAppId) {
        LOG.info(">> findUserProjectAppRelsByProjectAppId");
        List<UserProjAppRelTbl> userProjAppRelTbls = this.userProjectAppRelJpaDao.findByProjectApplicationId(new ProjectApplicationsTbl(projectAppId));
        if (userProjAppRelTbls.isEmpty()) {
            throw new RuntimeException("No User Project All Relations Found");
        }
        UserProjectAppRelResponse userProjectAppRelResponse = new UserProjectAppRelResponse(userProjAppRelTbls);
        LOG.info("<< findUserProjectAppRelsByProjectAppId");
        return userProjectAppRelResponse;
    }

    /**
     *
     * @param userProjectRelId
     * @param aaProjRelId
     * @param userRelType
     * @return UserProjectAppRelResponse
     */
    @Override
    public UserProjectAppRelResponse findUserProjectAppRelsByUserProjectRelIdAndAAPRelIdAndUserRelType(
            String userProjectRelId, String adminAreaProjRelId, String userRelType, final ValidationRequest validationRequest) {
        LOG.info(">> findUserProjectAppRelsByUserProjectRelIdSiteAARelIdUserRelType");
        final boolean isInactiveAssignment = validator.isViewInactiveAllowed(validationRequest);
        UserProjectRelTbl validatedUserProjectRel = validator.validateUserProjectRel(userProjectRelId, isInactiveAssignment);
    	String userId = validatedUserProjectRel.getUserId().getUserId();
    	String projectId = validatedUserProjectRel.getProjectId().getProjectId();
    	validator.validateUser(userId, isInactiveAssignment);
    	validator.validateProject(projectId, isInactiveAssignment);
    	
    	AdminAreaProjectRelTbl validatedAdminAreaProject = validator.validateAdminAreaProject(adminAreaProjRelId, isInactiveAssignment);
    	SiteAdminAreaRelTbl siteAdminAreaRelTbl = validatedAdminAreaProject.getSiteAdminAreaRelId();
    	siteAdminAreaRelTbl = validator.validateSiteAdminArea(siteAdminAreaRelTbl.getSiteAdminAreaRelId(), isInactiveAssignment);
    	String adminAreaId = siteAdminAreaRelTbl.getAdminAreaId().getAdminAreaId();
    	String siteId = siteAdminAreaRelTbl.getSiteId().getSiteId();
    	validator.validateAdminArea(adminAreaId, isInactiveAssignment);
    	validator.validateSite(siteId, isInactiveAssignment);
        
        
        
        List<UserProjAppRelTbl> userProjAppRelTbls = this.userProjectAppRelJpaDao.findByUserProjectRelIdAndAdminAreaProjectRelIdAndUserRelType(
        		validatedUserProjectRel, validatedAdminAreaProject, userRelType);
        List<UserProjAppRelTbl> newUserProjAppRelTbls = new ArrayList<>();
        for (UserProjAppRelTbl userProjAppRelTbl : userProjAppRelTbls) {
        	
        	try {
				String projectAppId = userProjAppRelTbl.getProjectApplicationId().getProjectApplicationId();
				validator.validateProjectApp(projectAppId, isInactiveAssignment);
				newUserProjAppRelTbls.add(userProjAppRelTbl);
			} catch (XMObjectNotFoundException e) {
				LOG.info("XMObjectNotFoundException", e);
			}
		}
    	if (newUserProjAppRelTbls.isEmpty()) {
            throw new XMObjectNotFoundException("No Relation Found", "U_ERR0006");
        }
        UserProjectAppRelResponse userProjectAppRelResponse = new UserProjectAppRelResponse(newUserProjAppRelTbls);
        LOG.info("<< findUserProjectAppRelsByUserProjectRelIdSiteAARelIdUserRelType");
        return userProjectAppRelResponse;
    }

    /**
     *
     * @param userId
     * @param projId
     * @param aaId
     * @return UserProjAppRelTbl
     */
    @Override
	public List<UserProjAppRelTbl> findByUserIdProjectIdAAId(String userId, String projId, String aaId,
			ValidationRequest validationRequest) {
		final boolean isViewInActive = validator.isViewInactiveAllowed(validationRequest);
		LOG.info("findByUserIdProjectIdAAId isViewInActive={}", isViewInActive);
		
		final UsersTbl usersTbl = validator.validateUser(userId, isViewInActive);
		final ProjectsTbl projectsTbl = validator.validateProject(projId, isViewInActive);
		final AdminAreasTbl adminAreasTbl = validator.validateAdminArea(aaId, isViewInActive);
		
		List<SiteAdminAreaRelTbl> siteAdminAreaRelTbls = this.siteAdminAreaRelJpaDao.findByAdminAreaId(adminAreasTbl);
		List<SiteAdminAreaRelTbl> filterSiteAdminAreaRels = validator.filterSiteAdminAreaRel(isViewInActive, siteAdminAreaRelTbls);
		List<SiteAdminAreaRelTbl> newFilterSiteAdminAreaRels = new ArrayList<>();
		for (SiteAdminAreaRelTbl siteAdminAreaRelTbl : filterSiteAdminAreaRels) {
			String siteId = siteAdminAreaRelTbl.getSiteId().getSiteId();
			validator.validateSite(siteId, isViewInActive);
			newFilterSiteAdminAreaRels.add(siteAdminAreaRelTbl);
		}
		
		List<AdminAreaProjectRelTbl> adminAreaProjectRelTbls = this.adminAreaProjectRelJpaDao.findBySiteAdminAreaRelIdIn(newFilterSiteAdminAreaRels);
		List<AdminAreaProjectRelTbl> filterAdminAreaProjectRels = validator.filterAdminAreaProjectRel(isViewInActive, adminAreaProjectRelTbls);
		List<UserProjectRelTbl> userProjectRelTbls = this.userProjectRelJpaDao.findByUserIdAndProjectId(usersTbl, projectsTbl);
		List<UserProjectRelTbl> filterUserProjectRels = validator.filterUserProjectRels(isViewInActive, userProjectRelTbls);
		List<UserProjAppRelTbl> userProjAppRelTbls = this.userProjectAppRelJpaDao.findByUserProjectRelIdInAndAdminAreaProjectRelIdIn(filterUserProjectRels, filterAdminAreaProjectRels);
		List<UserProjAppRelTbl> userProjAppRelTblList = new ArrayList<>();
		for (UserProjAppRelTbl userProjAppRelTbl : userProjAppRelTbls) {
			ProjectApplicationsTbl projectApplicationsTbl = userProjAppRelTbl.getProjectApplicationId();
			ProjectApplicationsTbl filterProjectApplicationTbl = this.validator.filterProjectApplicationResponse(isViewInActive, projectApplicationsTbl);
			if (null != filterProjectApplicationTbl) {
				userProjAppRelTblList.add(userProjAppRelTbl);
			}
		}
		if (userProjAppRelTblList.isEmpty()) {
			throw new XMObjectNotFoundException("No Relation Found", "U_ERR0009");
		}
		return userProjAppRelTblList;
	}
    
    /**
     *
     * @param userProjectAppRelBatchRequest
     * @return UserProjectAppRelBatchResponse
     */
    @Override
    public UserProjectAppRelBatchResponse updateUserRelsTypeByIds(UserProjectAppRelBatchRequest userProjectAppRelBatchRequest, HttpServletRequest httpServletRequest) {
        List<UserProjectAppRelRequest> projectAppRelRequests = userProjectAppRelBatchRequest.getUserProjectAppRelRequests();
        List<UserProjAppRelTbl> userProjAppRelTbls = new ArrayList<>();
        if (projectAppRelRequests.size() > 0) {
            projectAppRelRequests.forEach(request -> {
                UserProjAppRelTbl userProjAppRelTbl = this.userProjectAppRelJpaDao.findByUserProjAppRelId(request.getId());
                userProjAppRelTbl.setUserRelType(request.getUserRelationType());
                UserProjAppRelTbl projAppRelTbl = this.userProjectAppRelJpaDao.save(userProjAppRelTbl);
                this.userProjectAppRelAuditMgr.userProjectAppStatusUpdateSuccessAudit(projAppRelTbl, httpServletRequest);
                userProjAppRelTbls.add(projAppRelTbl);
                
            });
        }
        List<Map<String, String>> statusMaps = null;
        UserProjectAppRelBatchResponse response = new UserProjectAppRelBatchResponse(userProjAppRelTbls, statusMaps);
        return response;
    }

}
