/**
 * 
 */
package com.magna.xmbackend.audit.rel.mgr.impl;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.magna.xmbackend.audit.mgr.utils.AdminHistoryRelsUtil;
import com.magna.xmbackend.audit.rel.mgr.UserProjectAppRelAuditMgr;
import com.magna.xmbackend.entities.AdminAreaProjectRelTbl;
import com.magna.xmbackend.entities.AdminAreasTbl;
import com.magna.xmbackend.entities.AdminHistoryRelationsTbl;
import com.magna.xmbackend.entities.ProjectApplicationsTbl;
import com.magna.xmbackend.entities.ProjectsTbl;
import com.magna.xmbackend.entities.UserProjAppRelTbl;
import com.magna.xmbackend.entities.UserProjectRelTbl;
import com.magna.xmbackend.entities.UsersTbl;
import com.magna.xmbackend.jpa.dao.ProjectApplicationJpaDao;
import com.magna.xmbackend.jpa.rel.dao.AdminAreaProjectRelJpaDao;
import com.magna.xmbackend.jpa.rel.dao.AdminHistoryRelationJpaDao;
import com.magna.xmbackend.jpa.rel.dao.UserProjectRelJpaDao;
import com.magna.xmbackend.vo.rel.UserProjectAppRelBatchRequest;
import com.magna.xmbackend.vo.rel.UserProjectAppRelBatchResponse;
import com.magna.xmbackend.vo.rel.UserProjectAppRelRequest;

/**
 * @author Bhabadyuti Bal
 *
 */
@Component
public class UserProjectAppRelAuditMgrImpl implements UserProjectAppRelAuditMgr {
	
	private static final Logger LOG = LoggerFactory.getLogger(UserProjectAppRelAuditMgrImpl.class);
	
	@Autowired
	private AdminHistoryRelsUtil adminHistoryRelsUtil;
	@Autowired
	private AdminHistoryRelationJpaDao adminHistoryRelationJpaDao;
	@Autowired
	private ProjectApplicationJpaDao projAppJpaDao;
	@Autowired
	private UserProjectRelJpaDao userProjectRelJpaDao;
	@Autowired
	private AdminAreaProjectRelJpaDao adminAreaProjectRelJpaDao;
	
	
	

	@Override
	public void userProjectAppMultiSaveAuditor(UserProjectAppRelBatchRequest uparbReq,
			UserProjectAppRelBatchResponse uparbRes, HttpServletRequest httpServletRequest) {
		LOG.info(">>> userProjectAppMultiSaveAuditor");

		List<UserProjectAppRelRequest> userProjectAppRelRequests = uparbReq.getUserProjectAppRelRequests();

		List<UserProjAppRelTbl> userProjectAppRelTbls = uparbRes.getUserProjectAppRelTbls();
		List<Map<String, String>> statusMap = uparbRes.getStatusMap();

		if (statusMap.isEmpty()) {
			// All success
			userProjectAppRelTbls.forEach(userProjectAppRelTbl -> {
				UserProjectRelTbl userProjectRelTbl = userProjectAppRelTbl.getUserProjectRelId();
				UsersTbl usersTbl = userProjectRelTbl.getUserId();
				ProjectsTbl projectTbl = userProjectRelTbl.getProjectId();
				String role = this.getRolesForUser(usersTbl);
				AdminAreasTbl adminAreaTbl = userProjectAppRelTbl.getAdminAreaProjectRelId().getSiteAdminAreaRelId()
						.getAdminAreaId();

				ProjectApplicationsTbl projAppTbl = userProjectAppRelTbl.getProjectApplicationId();
				// ProjectsTbl projectTbl =
				// this.projectJpaDao.findByProjectId(userProjRelReq.getProjectId());
				AdminHistoryRelationsTbl adminHistoryRelationTbl = this.adminHistoryRelsUtil
						.makeAdminHistoryRelationTbl(httpServletRequest, "UserProjectApp", usersTbl.getUsername(),
								projAppTbl.getName(), projectTbl.getName(), adminAreaTbl.getName(),
								userProjectAppRelTbl.getUserRelType(), role, null, "UserProjectAppRelation Create",
								"Success");
				this.adminHistoryRelationJpaDao.save(adminHistoryRelationTbl);
			});

		} else {
			// iterate the status map to log the failed ones
			for (Map<String, String> map : statusMap) {
				for (UserProjectAppRelRequest userProjAppRelReq : userProjectAppRelRequests) {
					String userProjectRelId = userProjAppRelReq.getUserProjectRelId();
					UserProjectRelTbl userProjectRelTbl = this.userProjectRelJpaDao.findOne(userProjectRelId);
					UsersTbl usersTbl = userProjectRelTbl.getUserId();
					ProjectsTbl projectTbl = userProjectRelTbl.getProjectId();
					
					String adminAreaProjRelId = userProjAppRelReq.getAdminAreaProjRelId();
					AdminAreaProjectRelTbl adminAreaProjectRelTbl = this.adminAreaProjectRelJpaDao.findOne(adminAreaProjRelId);
					AdminAreasTbl adminAreaTbl = adminAreaProjectRelTbl.getSiteAdminAreaRelId().getAdminAreaId();
					ProjectApplicationsTbl projectAppTbl = this.projAppJpaDao.findOne(userProjAppRelReq.getProjectAppId());
					
					if (usersTbl != null && projectAppTbl != null) {
						String message = map.get("en");
						String role = this.getRolesForUser(usersTbl);
						if (message.contains(usersTbl.getUsername()) && message.contains(projectAppTbl.getName())) {
							AdminHistoryRelationsTbl adminHistoryRelationTbl = this.adminHistoryRelsUtil
									.makeAdminHistoryRelationTbl(httpServletRequest, "UserProjectApp",
											usersTbl.getUsername(), projectAppTbl.getName(),projectTbl.getName(), 
											adminAreaTbl.getName(), userProjAppRelReq.getUserRelationType(),
											role, message, "UserProjectAppRelation Create", "Failure");
							this.adminHistoryRelationJpaDao.save(adminHistoryRelationTbl);
							break;
						}
					}
				}
			}

			// iterate the userProjectRelTbls for success ones
			userProjectAppRelTbls.forEach(userProjectAppRelTbl -> {
				UserProjectRelTbl userProjectRelTbl = userProjectAppRelTbl.getUserProjectRelId();
				UsersTbl usersTbl = userProjectRelTbl.getUserId();
				ProjectsTbl projectTbl = userProjectRelTbl.getProjectId();
				String role = this.getRolesForUser(usersTbl);
				AdminAreasTbl adminAreaTbl = userProjectAppRelTbl.getAdminAreaProjectRelId().getSiteAdminAreaRelId()
						.getAdminAreaId();

				ProjectApplicationsTbl projAppTbl = userProjectAppRelTbl.getProjectApplicationId();
				
				//ProjectsTbl projectTbl = this.projectJpaDao.findByProjectId(userProjRelReq.getProjectId());
				AdminHistoryRelationsTbl adminHistoryRelationTbl = this.adminHistoryRelsUtil
						.makeAdminHistoryRelationTbl(httpServletRequest, "UserProjectApp", usersTbl.getUsername(),
								projAppTbl.getName(), projectTbl.getName(), adminAreaTbl.getName(),
								userProjectAppRelTbl.getUserRelType(), role, null, "UserProjectAppRelation Create",
								"Success");
					this.adminHistoryRelationJpaDao.save(adminHistoryRelationTbl);
			});
		}
		LOG.info("<<< userProjectAppMultiSaveAuditor");
	}

	private String getRolesForUser(UsersTbl usersTbl) {
		String reduce = usersTbl.getRoleUserRelTblCollection().stream()
				.map(roleUserTbl -> roleUserTbl.getRoleId().getName()).collect(Collectors.joining(","));
		return reduce;
	}

	@Override
	public void userProjectMultiSaveFailureAuditor(HttpServletRequest httpServletRequest, Exception ex) {
		LOG.info(">>> userProjectMultiSaveFailureAuditor");
		AdminHistoryRelationsTbl adminHistoryRelationTbl = this.adminHistoryRelsUtil.makeAdminHistoryRelationTbl(
				httpServletRequest, "UserProjectApp", null, null, null, null, null, null, ex.getMessage(), "UserProjectAppRelation Create",
				"Failure");
		this.adminHistoryRelationJpaDao.save(adminHistoryRelationTbl);
		LOG.info("<<< userProjectMultiSaveFailureAuditor");
	}

	@Override
	public void userProjectAppMultiDeleteSuccessAudit(UserProjAppRelTbl userProjAppRelTbl,
			HttpServletRequest httpServletRequest) {

		UserProjectRelTbl userProjectRelTbl = userProjAppRelTbl.getUserProjectRelId();
		UsersTbl usersTbl = userProjectRelTbl.getUserId();
		ProjectsTbl projectTbl = userProjectRelTbl.getProjectId();
		String role = this.getRolesForUser(usersTbl);
		AdminAreasTbl adminAreaTbl = userProjAppRelTbl.getAdminAreaProjectRelId().getSiteAdminAreaRelId()
				.getAdminAreaId();

		ProjectApplicationsTbl projAppTbl = userProjAppRelTbl.getProjectApplicationId();
		// ProjectsTbl projectTbl =
		// this.projectJpaDao.findByProjectId(userProjRelReq.getProjectId());
		AdminHistoryRelationsTbl adminHistoryRelationTbl = this.adminHistoryRelsUtil
				.makeAdminHistoryRelationTbl(httpServletRequest, "UserProjectApp", usersTbl.getUsername(),
						projAppTbl.getName(), projectTbl.getName(), adminAreaTbl.getName(),
						userProjAppRelTbl.getUserRelType(), role, null, "UserProjectAppRelation Delete",
						"Success");
		this.adminHistoryRelationJpaDao.save(adminHistoryRelationTbl);
	
		
	}
	
	
	@Override
	public void userProjectAppStatusUpdateSuccessAudit(UserProjAppRelTbl userProjAppRelTbl,
			HttpServletRequest httpServletRequest) {

		UserProjectRelTbl userProjectRelTbl = userProjAppRelTbl.getUserProjectRelId();
		UsersTbl usersTbl = userProjectRelTbl.getUserId();
		ProjectsTbl projectTbl = userProjectRelTbl.getProjectId();
		String role = this.getRolesForUser(usersTbl);
		AdminAreasTbl adminAreaTbl = userProjAppRelTbl.getAdminAreaProjectRelId().getSiteAdminAreaRelId()
				.getAdminAreaId();

		ProjectApplicationsTbl projAppTbl = userProjAppRelTbl.getProjectApplicationId();
		// ProjectsTbl projectTbl =
		// this.projectJpaDao.findByProjectId(userProjRelReq.getProjectId());
		AdminHistoryRelationsTbl adminHistoryRelationTbl = this.adminHistoryRelsUtil
				.makeAdminHistoryRelationTbl(httpServletRequest, "UserProjectApp", usersTbl.getUsername(),
						projAppTbl.getName(), projectTbl.getName(), adminAreaTbl.getName(),
						userProjAppRelTbl.getUserRelType(), role, null, "UserProjectAppRelation status update",
						"Success");
		this.adminHistoryRelationJpaDao.save(adminHistoryRelationTbl);
	
		
	}

}
