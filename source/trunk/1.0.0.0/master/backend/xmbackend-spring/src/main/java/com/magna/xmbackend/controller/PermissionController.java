package com.magna.xmbackend.controller;

import com.magna.xmbackend.mgr.PermissionMgr;
import com.magna.xmbackend.vo.permission.PermissionResponse;
import javax.servlet.http.HttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;

@RestController
@RequestMapping(value = "/permission")
public class PermissionController {

    private static final Logger LOG
            = LoggerFactory.getLogger(PermissionController.class);

    @Autowired
    private PermissionMgr permissionMgr;

    /**
     *
     * @param httpServletRequest
     * @param roleId
     * @return PermissionResponse
     */
    @RequestMapping(value = "/findObjectPermissionByRoleId/{roleId}",
            method = RequestMethod.GET,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<PermissionResponse> findObjectPermissionByRoleId(
            HttpServletRequest httpServletRequest,
            @PathVariable String roleId) {
        LOG.info("> findObjectPermissionByRoleId");
        final PermissionResponse permissionResponse = this.permissionMgr.findObjectPermissionByRoleId(roleId);
        LOG.info("< findObjectPermissionByRoleId");
        return new ResponseEntity<>(permissionResponse, HttpStatus.ACCEPTED);
    }

    /**
     *
     * @param httpServletRequest
     * @param roleId
     * @return PermissionResponse
     */
    @RequestMapping(value = "/findObjectRelPermissionByRoleId/{roleId}",
            method = RequestMethod.GET,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<PermissionResponse> findObjectRelPermissionByRoleId(
            HttpServletRequest httpServletRequest,
            @PathVariable String roleId) {
        LOG.info("> findObjectRelPermissionByRoleId");
        final PermissionResponse permissionResponse = this.permissionMgr.findObjectRelPermissionByRoleId(roleId);
        LOG.info("< findObjectRelPermissionByRoleId");
        return new ResponseEntity<>(permissionResponse, HttpStatus.ACCEPTED);
    }

    /**
     *
     * @param httpServletRequest
     * @return PermissionResponse
     */
    @RequestMapping(value = "/findAllObjectPermission",
            method = RequestMethod.GET,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<PermissionResponse> findAllObjectPermission(
            HttpServletRequest httpServletRequest) {
        LOG.info("> findAllObjectPermission");
        final PermissionResponse permissionResponse = this.permissionMgr.findAllObjectPermission();
        LOG.info("< findAllObjectPermission");
        return new ResponseEntity<>(permissionResponse, HttpStatus.ACCEPTED);
    }

    /**
     *
     * @param httpServletRequest
     * @return PermissionResponse
     */
    @RequestMapping(value = "/findAllObjectRelationPermission",
            method = RequestMethod.GET,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<PermissionResponse> findAllObjectRelationPermission(
            HttpServletRequest httpServletRequest) {
        LOG.info("> findAllObjectRelationPermission");
        final PermissionResponse permissionResponse = this.permissionMgr.findAllObjectRelationPermission();
        LOG.info("< findAllObjectRelationPermission");
        return new ResponseEntity<>(permissionResponse, HttpStatus.ACCEPTED);
    }
    
   
   /**
    * Find all AA based relation permission.
    *
    * @param httpServletRequest the http servlet request
    * @return the response entity
    */
   @RequestMapping(value = "/findAllAABasedRelationPermission",
           method = RequestMethod.GET,
           consumes = {MediaType.APPLICATION_JSON_VALUE,
               MediaType.APPLICATION_XML_VALUE,
               MediaType.APPLICATION_FORM_URLENCODED_VALUE},
           produces = {MediaType.APPLICATION_JSON_VALUE,
               MediaType.APPLICATION_XML_VALUE,
               MediaType.APPLICATION_FORM_URLENCODED_VALUE}
   )
   public @ResponseBody
   final ResponseEntity<PermissionResponse> findAllAABasedRelationPermission(
           HttpServletRequest httpServletRequest) {
       LOG.info("> findAllAABasedRelationPermission");
       final PermissionResponse permissionResponse = this.permissionMgr.findAllAABasedRelationPermission();
       LOG.info("< findAllAABasedRelationPermission");
       return new ResponseEntity<>(permissionResponse, HttpStatus.ACCEPTED);
   }
}
