/**
 * 
 */
package com.magna.xmbackend.jpa.rel.dao;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.repository.CrudRepository;

import com.magna.xmbackend.entities.DirectoryRefTbl;
import com.magna.xmbackend.entities.DirectoryTbl;

/**
 * @author Bhabadyuti Bal
 *
 */
@Transactional
public interface DirectoryRelJpaDao extends CrudRepository<DirectoryRefTbl, String>{
	
	List<DirectoryRefTbl> findByDirectoryIdAndObjectId(DirectoryTbl directoryId, String objectId);
	
	List<DirectoryRefTbl> findByDirectoryIdAndObjectType(DirectoryTbl directoryId, String objectType);
	
	List<DirectoryRefTbl> findByObjectId(String objectId);
}
