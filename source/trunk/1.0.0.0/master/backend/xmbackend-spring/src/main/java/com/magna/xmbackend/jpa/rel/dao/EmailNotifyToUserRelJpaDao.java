/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magna.xmbackend.jpa.rel.dao;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.repository.CrudRepository;

import com.magna.xmbackend.entities.EmailNotificationConfigTbl;
import com.magna.xmbackend.entities.EmailNotifyToUserRelTbl;
import com.magna.xmbackend.entities.UsersTbl;

@Transactional
public interface EmailNotifyToUserRelJpaDao extends CrudRepository<EmailNotifyToUserRelTbl, String> {

    //Iterable<EmailNotifyToUserRelTbl> findByUserId(UsersTbl userId);
    
    List<EmailNotifyToUserRelTbl> findByEmailNotificationConfigId(EmailNotificationConfigTbl tbl);
    
}
