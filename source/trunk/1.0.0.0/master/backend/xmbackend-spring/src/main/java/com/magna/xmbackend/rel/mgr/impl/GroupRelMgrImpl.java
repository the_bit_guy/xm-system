/**
 *
 */
package com.magna.xmbackend.rel.mgr.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.magna.xmbackend.audit.rel.mgr.GroupRelationAuditMgr;
import com.magna.xmbackend.entities.GroupRefTbl;
import com.magna.xmbackend.entities.GroupsTbl;
import com.magna.xmbackend.entities.ProjectApplicationsTbl;
import com.magna.xmbackend.entities.ProjectsTbl;
import com.magna.xmbackend.entities.UserApplicationsTbl;
import com.magna.xmbackend.entities.UsersTbl;
import com.magna.xmbackend.exception.CannotCreateRelationshipException;
import com.magna.xmbackend.exception.XMObjectNotFoundException;
import com.magna.xmbackend.jpa.dao.GroupsJpaDao;
import com.magna.xmbackend.jpa.dao.ProjectApplicationJpaDao;
import com.magna.xmbackend.jpa.dao.ProjectJpaDao;
import com.magna.xmbackend.jpa.dao.UserApplicationJpaDao;
import com.magna.xmbackend.jpa.dao.UserJpaDao;
import com.magna.xmbackend.jpa.rel.dao.GroupRelJpaDao;
import com.magna.xmbackend.rel.mgr.GroupRelMgr;
import com.magna.xmbackend.utils.MessageMaker;
import com.magna.xmbackend.utils.Validator;
import com.magna.xmbackend.vo.enums.Groups;
import com.magna.xmbackend.vo.group.GroupRelBatchRequest;
import com.magna.xmbackend.vo.group.GroupRelBatchResponse;
import com.magna.xmbackend.vo.group.GroupRelRequest;
import com.magna.xmbackend.vo.group.GroupRelResponse;
import com.magna.xmbackend.vo.permission.ValidationRequest;

/**
 * @author Bhabadyuti Bal
 *
 */
@Component
public class GroupRelMgrImpl implements GroupRelMgr {

    private static final Logger LOG = LoggerFactory.getLogger(GroupRelMgrImpl.class);

    @Autowired
    private GroupsJpaDao groupsJpaDao;
    @Autowired
    private GroupRelJpaDao groupRelJpaDao;
    @Autowired
    private UserJpaDao userJpaDao;
    @Autowired
    private ProjectJpaDao projectJpaDao;
    @Autowired
    private UserApplicationJpaDao userApplicationJpaDao;
    @Autowired
    private ProjectApplicationJpaDao projectApplicationJpaDao;
    @Autowired
    private MessageMaker messageMaker;

    @Autowired
    private Validator validator;
    
    @Autowired
    private GroupRelationAuditMgr groupRelationAuditMgr;
    

    @Override
    public GroupRelBatchResponse createBatch(GroupRelBatchRequest batchRequest, String userName) {
        LOG.info(">> createBatch");
        final List<GroupRelResponse> groupRelResponses = new ArrayList<>();
        final List<Map<String, String>> statusMaps = new ArrayList<>();
        final List<GroupRelRequest> groupRelRequests = batchRequest.getGroupRelBatchRequest();
        for (GroupRelRequest relRequest : groupRelRequests) {
            GroupRelResponse groupRelResponse = new GroupRelResponse();
            try {
                final GroupRefTbl grpRefOut = this.create(relRequest);
                groupRelResponse.setGroupRefId(grpRefOut.getGroupRefId());
                final String objectId = grpRefOut.getObjectId();
                final String groupType = grpRefOut.getGroupId().getGroupType();
                if (groupType.equals(Groups.USER.name())) {
                    UsersTbl usersTbl = this.userJpaDao.findOne(objectId);
                    groupRelResponse.setUsersTbl(usersTbl);
                } else if (groupType.equals(Groups.PROJECT.name())) {
                    ProjectsTbl projectsTbl = this.projectJpaDao.findOne(objectId);
                    groupRelResponse.setProjectsTbl(projectsTbl);
                } else if (groupType.equals(Groups.USERAPPLICATION.name())) {
                    UserApplicationsTbl userApplicationsTbl = this.userApplicationJpaDao.findOne(objectId);
                    groupRelResponse.setUserApplicationsTbl(userApplicationsTbl);
                } else if (groupType.equals(Groups.PROJECTAPPLICATION.name())) {
                    ProjectApplicationsTbl projectApplicationsTbl = this.projectApplicationJpaDao.findOne(objectId);
                    groupRelResponse.setProjectApplicationsTbl(projectApplicationsTbl);
                }
                groupRelResponses.add(groupRelResponse);
            } catch (CannotCreateRelationshipException ccre) {
                final Map<String, String> statusMap = messageMaker.extractFromException(ccre);
                statusMaps.add(statusMap);
            }
        }
        final GroupRelBatchResponse groupRelBatchResponse = new GroupRelBatchResponse(groupRelResponses, statusMaps);
        LOG.info("<< createBatch");
        return groupRelBatchResponse;
    }

    @Override
    public GroupRefTbl create(GroupRelRequest groupRelRequest) {
        LOG.info(">> create");
        this.isGroupRelCreationValid(groupRelRequest);
        final GroupRefTbl grpRefTblIn = this.convert2Entity(groupRelRequest);
        final GroupRefTbl grpRefTblOut = groupRelJpaDao.save(grpRefTblIn);
        LOG.info(">> create");
        return grpRefTblOut;
    }

    private GroupRefTbl convert2Entity(GroupRelRequest groupRelRequest) {
        final String id = UUID.randomUUID().toString();
        Date date = new Date();
        final String groupId = groupRelRequest.getGroupId();
        final String objectId = groupRelRequest.getObjectId();

        GroupRefTbl groupRefTbl = new GroupRefTbl();
        groupRefTbl.setGroupRefId(id);
        groupRefTbl.setGroupId(new GroupsTbl(groupId));
        groupRefTbl.setObjectId(objectId);
        groupRefTbl.setCreateDate(date);
        groupRefTbl.setUpdateDate(date);

        return groupRefTbl;
    }

    //check if related objects are being dragged to that corresponding groups
    private void isGroupRelCreationValid(GroupRelRequest groupRelRequest) {
        LOG.debug(">> isGroupRelCreationValid");
        final String groupId = groupRelRequest.getGroupId();
        final String objectId = groupRelRequest.getObjectId();
        final GroupsTbl groupsTbl = this.groupsJpaDao.findOne(groupId);
        if (null != groupsTbl) {
            String groupType = groupsTbl.getGroupType();
            if (groupType.equals(Groups.USER.name())) {
                UsersTbl userTbl = this.userJpaDao.findOne(objectId);
                if (null == userTbl) {
                    new CannotCreateRelationshipException("Invalid Relation", "ERR0001");
                }
            } else if (groupType.equals(Groups.PROJECT.name())) {
                ProjectsTbl projectTbl = this.projectJpaDao.findOne(objectId);
                if (null == projectTbl) {
                    new CannotCreateRelationshipException("Invalid Relation", "ERR0001");
                }
            } else if (groupType.equals(Groups.USERAPPLICATION.name())) {
                UserApplicationsTbl userAppTbl = this.userApplicationJpaDao.findOne(objectId);
                if (null == userAppTbl) {
                    new CannotCreateRelationshipException("Invalid Relation", "ERR0001");
                }
            } else if (groupType.equals(Groups.PROJECTAPPLICATION.name())) {
                ProjectApplicationsTbl projectAppTbl = this.projectApplicationJpaDao.findOne(objectId);
                if (null == projectAppTbl) {
                    new CannotCreateRelationshipException("Invalid Relation", "ERR0001");
                }
            }
        } else {
            LOG.info("Invalid group id");
        }

        this.checkIfGroupRelAlreadyExist(groupsTbl, objectId);
        LOG.debug("<< isGroupRelCreationValid");

    }

    /**
     *
     * @param groupsTbl
     * @param objectId
     */
    private void checkIfGroupRelAlreadyExist(GroupsTbl groupsTbl, String objectId) {
        GroupRefTbl groupRefTbl = this.groupRelJpaDao.findByGroupIdAndObjectId(groupsTbl, objectId);
        if (null != groupRefTbl) {
            String groupType = groupRefTbl.getGroupId().getGroupType();
            GroupsTbl groupTbl = groupRefTbl.getGroupId();
            if (groupType.equals(Groups.USER.name())) {
                UsersTbl userTbl = this.userJpaDao.findOne(objectId);
                final Map<String, String[]> paramMap = this.messageMaker.getGroupUserRelWithi18nCode(userTbl.getUsername(), groupTbl.getName());
                throw new CannotCreateRelationshipException("Realtionship exists", "GU_ERR0001", paramMap);
            } else if (groupType.equals(Groups.PROJECT.name())) {
                ProjectsTbl projectsTbl = this.projectJpaDao.findOne(objectId);
                final Map<String, String> projectTransMap = this.messageMaker.getProjectNames(projectsTbl);
                Map<String, String[]> paramMap = this.messageMaker.getGroupProjectRelWithi18nCode(projectTransMap, groupsTbl.getName());
                throw new CannotCreateRelationshipException("Realtionship exists", "GP_ERR0001", paramMap);

            } else if (groupType.equals(Groups.USERAPPLICATION.name())) {
                UserApplicationsTbl userApplicationsTbl = this.userApplicationJpaDao.findOne(objectId);
                final Map<String, String> userAppTransMap = this.messageMaker.getUserAppNames(userApplicationsTbl);
                Map<String, String[]> paramMap = this.messageMaker.getGroupProjectRelWithi18nCode(userAppTransMap, groupsTbl.getName());
                throw new CannotCreateRelationshipException("Realtionship exists", "GUA_ERR001", paramMap);

            } else if (groupType.equals(Groups.PROJECTAPPLICATION.name())) {
                ProjectApplicationsTbl projectApplicationsTbl = this.projectApplicationJpaDao.findOne(objectId);
                final Map<String, String> projAppTransMap = this.messageMaker.getProjectAppNames(projectApplicationsTbl);
                Map<String, String[]> paramMap = this.messageMaker.getGroupProjectRelWithi18nCode(projAppTransMap, groupsTbl.getName());
                throw new CannotCreateRelationshipException("Realtionship exists", "GPA_ERR001", paramMap);
            }

            //throw user 'name' already exist in user group 'name'
            //final Map<String, String[]> paramMap = getDirectoryProjectRelWithi18nCode(directoryTransMap, projectTransMap);
            //new CannotCreateRelationshipException("Realtionship exists", "", paramMap);
        }

    }

    /**
     *
     * @param groupId
     * @param userName
     * @return GroupRelResponse
     */
    @Override
    public List<GroupRelResponse> findGroupRelsByGroupId(final String groupId,
            final String userName) {
        LOG.info(">> group findAll");
        final List<GroupRelResponse> groupRelResponses = new ArrayList<>();
        final GroupsTbl groupTbl = this.groupsJpaDao.findOne(groupId);
        if (null != groupTbl) {
            final ValidationRequest validationRequest = new ValidationRequest();
            validationRequest.setUserName(userName);
            validationRequest.setPermissionName("VIEW_INACTIVE");
            final boolean isInactiveAssignment = validator.isViewInactiveAllowed(validationRequest);
            LOG.info("findGroupRelsByGroupId isInactiveAssignment={}", isInactiveAssignment);
            final Iterable<GroupRefTbl> groupRefTbls = this.groupRelJpaDao.findByGroupId(groupTbl);
            for (final GroupRefTbl groupRefTbl : groupRefTbls) {
                final GroupRelResponse groupRelResponse = new GroupRelResponse();
                groupRelResponse.setGroupRefId(groupRefTbl.getGroupRefId());
                final String objectId = groupRefTbl.getObjectId();
                final String groupType = groupRefTbl.getGroupId().getGroupType();
                final Groups groups = Groups.valueOf(groupType);
                switch (groups) {
                    case USER:
                        try {
                            final UsersTbl usersTbl
                                    = validator.validateUser(objectId, isInactiveAssignment);
                            groupRelResponse.setUsersTbl(usersTbl);
                        } catch (XMObjectNotFoundException e) {
                            LOG.info("XMObjectNotFoundException={}", e);
                        }
                        break;
                    case PROJECT:
                        try {
                            final ProjectsTbl projectsTbl
                                    = validator.validateProject(objectId, isInactiveAssignment);
                            groupRelResponse.setProjectsTbl(projectsTbl);
                        } catch (XMObjectNotFoundException e) {
                            LOG.info("XMObjectNotFoundException={}", e);
                        }
                        break;
                    case USERAPPLICATION:
                        try {
                            final UserApplicationsTbl userApplicationsTbl
                                    = validator.validateUserApp(objectId, isInactiveAssignment);
                            groupRelResponse.setUserApplicationsTbl(userApplicationsTbl);
                        } catch (XMObjectNotFoundException e) {
                            LOG.info("XMObjectNotFoundException={}", e);
                        }
                        break;
                    case PROJECTAPPLICATION:
                        try {
                            final ProjectApplicationsTbl projectApplicationsTbl
                                    = validator.validateProjectApp(objectId, isInactiveAssignment);
                            groupRelResponse.setProjectApplicationsTbl(projectApplicationsTbl);
                        } catch (XMObjectNotFoundException e) {
                            LOG.info("XMObjectNotFoundException={}", e);
                        }
                        break;
                    default:
                        break;
                }
                groupRelResponses.add(groupRelResponse);
            }
        }
        LOG.info("<< findAll");
        return groupRelResponses;
    }

    /**
     *
     * @param groupReIds
     * @return boolean
     */
    @Override
    public boolean multiDelete(final Set<String> groupReIds, HttpServletRequest httpServletRequest) {
        LOG.info(">> multiDelete");
        if (groupReIds.size() > 0) {
            for (String id : groupReIds) {
            	GroupRefTbl groupRefTbl = this.groupRelJpaDao.findOne(id);
                try {
                		this.groupRelJpaDao.delete(id);
                		this.groupRelationAuditMgr.groupRelationMultiDeleteSuccessAudit(groupRefTbl, httpServletRequest);
                } catch (Exception e) {
                    LOG.info("Unable to delete {}", id);
                }
            }
        }
        LOG.info("<< multiDelete");
        return true;
    }
}
