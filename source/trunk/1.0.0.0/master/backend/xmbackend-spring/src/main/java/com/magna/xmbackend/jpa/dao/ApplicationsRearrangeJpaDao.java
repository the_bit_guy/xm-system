/**
 * 
 */
package com.magna.xmbackend.jpa.dao;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.magna.xmbackend.entities.ProjectsTbl;
import com.magna.xmbackend.entities.RearrangeApplicationsTbl;
import com.magna.xmbackend.entities.SitesTbl;
import com.magna.xmbackend.entities.UsersTbl;

/**
 * @author Bhabadyuti Bal
 *
 */
@Repository
public interface ApplicationsRearrangeJpaDao extends JpaRepository<RearrangeApplicationsTbl, String>{

	Optional<RearrangeApplicationsTbl> findByUserIdAndSiteIdAndProjectId(UsersTbl usersTbl, SitesTbl sitesTbl, ProjectsTbl projectsTbl);
	
	@Modifying
	@Query("delete from RearrangeApplicationsTbl rpt where rpt.userId = :usersTbl and rpt.siteId = :sitesTbl and rpt.projectId = :projectsTbl")
	int delete(@Param("usersTbl") UsersTbl usersTbl, @Param("sitesTbl") SitesTbl sitesTbl, @Param("projectsTbl") ProjectsTbl projectsTbl);
}
