/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magna.xmbackend.utils;

import java.util.UUID;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 *
 * @author Admin
 */
@Component
public class GenerateUuid {

    private static final Logger LOG
            = LoggerFactory.getLogger(GenerateUuid.class);

    /**
     *
     * @return unique id
     */
    public String getUuid() {
        final String uuid = String.valueOf(UUID.randomUUID());
        LOG.info("uuid={}", uuid);
        return uuid;
    }
}
