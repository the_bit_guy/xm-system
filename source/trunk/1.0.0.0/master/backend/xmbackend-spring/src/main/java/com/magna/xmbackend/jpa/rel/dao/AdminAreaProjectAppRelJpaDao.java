package com.magna.xmbackend.jpa.rel.dao;

import com.magna.xmbackend.entities.AdminAreaProjAppRelTbl;
import com.magna.xmbackend.entities.AdminAreaProjectRelTbl;
import com.magna.xmbackend.entities.AdminAreasTbl;
import com.magna.xmbackend.entities.ProjectApplicationsTbl;
import com.magna.xmbackend.entities.ProjectsTbl;

import java.util.List;

import javax.transaction.Transactional;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

/**
 *
 * @author vijay
 */
@Transactional
public interface AdminAreaProjectAppRelJpaDao extends CrudRepository<AdminAreaProjAppRelTbl, String> {

    /**
     *
     * @param status
     * @param id
     * @return integer
     */
    @Modifying
    @Query("UPDATE AdminAreaProjAppRelTbl aapart set aapart.status = :status where aapart.adminAreaProjAppRelId = :id")
    int setStatusForAdminAreaProjectAppRelTbl(@Param("status") String status, @Param("id") String id);

    /**
     *
     * @param adminAreaProjectRelId
     * @param relType
     * @return AdminAreaProjAppRelTbl
     */
    @Modifying
    @Query("SELECT adminAreaProjApp From AdminAreaProjAppRelTbl adminAreaProjApp where adminAreaProjApp.adminAreaProjectRelId=:adminAreaProjectRelId and lower(adminAreaProjApp.relType)=lower(:relType)")
    Iterable<AdminAreaProjAppRelTbl> getAdminAreaProjAppOnRelType(@Param("adminAreaProjectRelId") AdminAreaProjectRelTbl adminAreaProjectRelId,
            @Param("relType") String relType);
    
    @Modifying
    @Query("UPDATE AdminAreaProjAppRelTbl aapart set aapart.relType = :relType where aapart.adminAreaProjAppRelId = :id")
    int setRelTypeForAdminAreaProjectAppRelTbl(@Param("relType") String relType, @Param("id") String id);
    
    
    Iterable<AdminAreaProjAppRelTbl> findProjectApplicationIdByAdminAreaProjectRelId(AdminAreaProjectRelTbl adminAreaProjectRelId);

    /**
     * 
     * @param projectApplicationsTbls
     * @param relType
     * @return List<AdminAreaProjAppRelTbl>
     */
    List<AdminAreaProjAppRelTbl> findByProjectApplicationIdInAndRelType(List<ProjectApplicationsTbl> projectApplicationsTbls, String relType);
    
    /**
     * 
     * @param adminAreaProjectRelTbls
     * @return List<AdminAreaProjAppRelTbl>
     */
    List<AdminAreaProjAppRelTbl> findByAdminAreaProjectRelIdIn(List<AdminAreaProjectRelTbl> adminAreaProjectRelTbls);
    
    
    /**
     * 
     * @param id
     * @return AdminAreaProjAppRelTbl
     */
    AdminAreaProjAppRelTbl findByAdminAreaProjAppRelId(String id);
    
    /**
     * 
     * @param aapartStatus
     * @param projectId
     * @param adminAreaId
     * @param paStatus
     * @return List<AdminAreaProjAppRelTbl>
     */
    @Query("from AdminAreaProjAppRelTbl AAPART WHERE AAPART.status IN (:status) and AAPART.adminAreaProjectRelId in "
    		+ "(select AAPRT.adminAreaProjectRelId from AdminAreaProjectRelTbl AAPRT, SiteAdminAreaRelTbl SAART WHERE "
    		+ "AAPRT.siteAdminAreaRelId = SAART.siteAdminAreaRelId AND AAPRT.projectId = :projectId AND SAART.siteAdminAreaRelId = "
    		+ "(SELECT SAART.siteAdminAreaRelId FROM SiteAdminAreaRelTbl SAART WHERE SAART.adminAreaId = :adminAreaId)) AND "
    		+ "AAPART.projectApplicationId IN (SELECT PAT.projectApplicationId FROM ProjectApplicationsTbl PAT WHERE PAT.status IN (:status) "
    		+ "AND PAT.position IN ('*ButtonTask*', '*IconTask*', '*MenuTask*'))")
    List<AdminAreaProjAppRelTbl> findAdminAreaProjAppRelTbls(@Param("adminAreaId") AdminAreasTbl adminAreaId, @Param("projectId") ProjectsTbl projectId, 
    		@Param("status") List<String> status);

    /**
     * 
     * @param adminAreaProjectRelId
     * @return List<AdminAreaProjAppRelTbl>
     */
    List<AdminAreaProjAppRelTbl> findByAdminAreaProjectRelId(AdminAreaProjectRelTbl adminAreaProjectRelId);
    
    /**
     * 
     * @param adminAreaProjectRelId
     * @param projectApplicationId
     * @return AdminAreaProjAppRelTbl
     */
    AdminAreaProjAppRelTbl findByAdminAreaProjectRelIdAndProjectApplicationId(AdminAreaProjectRelTbl adminAreaProjectRelId, ProjectApplicationsTbl projectApplicationId);
    
    
}
