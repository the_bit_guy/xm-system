/**
 * 
 */
package com.magna.xmbackend.mgr.impl;

import java.io.UnsupportedEncodingException;
import java.sql.SQLException;
import java.sql.SQLSyntaxErrorException;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.SQLErrorCodeSQLExceptionTranslator;
import org.springframework.stereotype.Component;

import com.magna.xmbackend.mgr.XMBatchMgr;
import com.magna.xmbackend.xmbatch.XmbatchResponse;

/**
 * @author Bhabadyuti Bal
 *
 */
@Component
public class XMBatchMgrImpl implements XMBatchMgr{

	private static final Logger LOG
    = LoggerFactory.getLogger(XMBatchMgrImpl.class);
	
	@Autowired
	JdbcTemplate jdbcTemplate;
	
	

	@Override
	public XmbatchResponse findResultSet(String query) throws SQLSyntaxErrorException {
		LOG.info(">> findResultSet");
		/*CustomSQLErrorCodeTranslator customSQLErrorCodeTranslator = new CustomSQLErrorCodeTranslator();
		this.jdbcTemplate.setExceptionTranslator(customSQLErrorCodeTranslator);*/
		
		String exceptionMsg = null;
		List<Map<String, Object>> resultSet = null;
		try {
			resultSet = this.jdbcTemplate.queryForList(query);
			for (Map<String, Object> map : resultSet) {
				if(map.containsKey("REMARKS") || map.containsKey("DESCRIPTION") || map.containsKey("DISPLAY_DESCRIPTION") || map.containsKey("DISPLAY_NAME")) {
					if(map.get("REMARKS") != null) {
						String remarks = map.get("REMARKS").toString();
						try {
							String decodedRemarks = java.net.URLDecoder.decode(remarks, "ISO-8859-1");
							map.put("REMARKS", decodedRemarks);
						} catch (UnsupportedEncodingException e) {
							LOG.error("UnsupportedEncodingException {}", e.getMessage());
						}
					}
					if(map.get("DESCRIPTION") != null) {
						String desc = map.get("DESCRIPTION").toString();
						try {
							String decodedDesc = java.net.URLDecoder.decode(desc, "ISO-8859-1");
							map.put("DESCRIPTION", decodedDesc);
						} catch (UnsupportedEncodingException e) {
							LOG.error("UnsupportedEncodingException {}", e.getMessage());
						}
					}
					if(map.get("DISPLAY_DESCRIPTION") != null) {
						String desc = map.get("DISPLAY_DESCRIPTION").toString();
						try {
							String decodedDesc = java.net.URLDecoder.decode(desc, "ISO-8859-1");
							map.put("DISPLAY_DESCRIPTION", decodedDesc);
						} catch (UnsupportedEncodingException e) {
							LOG.error("UnsupportedEncodingException {}", e.getMessage());
						}
					}
					if(map.get("DISPLAY_NAME") != null) {
						String name = map.get("DISPLAY_NAME").toString();
						try {
							String decodedName = java.net.URLDecoder.decode(name, "ISO-8859-1");
							map.put("DISPLAY_NAME", decodedName);
						} catch (UnsupportedEncodingException e) {
							LOG.error("UnsupportedEncodingException {}", e.getMessage());
						}
					}
				}
			}
		} catch (DataAccessException e) {
			exceptionMsg = handleException(e);
		}
		XmbatchResponse xmbatchResponse = new XmbatchResponse(resultSet, exceptionMsg);
		LOG.info("<< findResultSet");
		return xmbatchResponse;
	}
	
	
	private String handleException(Exception e) {
			Throwable cause = e.getCause();
			if (cause instanceof SQLException && cause.getCause() == null) {
				String excetionCause = cause.getMessage().split("ORA-\\d{5}")[1].trim();
				return filterPlainString(excetionCause);
			}
		return null;
	}


	private String filterPlainString(String exceptionCause) {
		Pattern pt = Pattern.compile("[^a-zA-Z0-9]");
		Matcher match = pt.matcher(exceptionCause);
		while (match.find()) {
			String spChar = match.group();
			if (!spChar.equals(" ")) {
				exceptionCause = exceptionCause.replaceAll("\\" + spChar, "");
			}
		}
		return exceptionCause.trim();
	}

	@Deprecated
	class CustomSQLErrorCodeTranslator extends SQLErrorCodeSQLExceptionTranslator {
		
		@Override
	    protected DataAccessException customTranslate
	      (String task, String sql, SQLException sqlException) {
			LOG.error("error code.......{}", sqlException.getErrorCode());
	        if (sqlException.getErrorCode() > 0) {
	        	return new DuplicateKeyException(
	                    "Custom Exception translator - Integrity constraint violation.", sqlException);
	        }
	        return null;
	    }
		
	}
}
