package com.magna.xmbackend.audit.rel.mgr;

import javax.servlet.http.HttpServletRequest;

import com.magna.xmbackend.entities.DirectoryRefTbl;
import com.magna.xmbackend.vo.directory.DirectoryRelBatchRequest;
import com.magna.xmbackend.vo.directory.DirectoryRelBatchResponse;

public interface DirectoryRelAuditMgr {


	void dirRelMultiSaveAuditor(DirectoryRelBatchRequest directoryRelBatchRequest, DirectoryRelBatchResponse drbr,
			HttpServletRequest httpServletRequest);

	void dirRelMultiSaveFailureAuditor(HttpServletRequest httpServletRequest, Exception ex);

	void directoryRelMultiDeleteSuccessAudit(DirectoryRefTbl directoryRefTbl, HttpServletRequest httpServletRequest);

}
