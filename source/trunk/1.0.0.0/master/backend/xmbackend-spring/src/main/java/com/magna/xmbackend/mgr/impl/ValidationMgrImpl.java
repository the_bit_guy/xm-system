package com.magna.xmbackend.mgr.impl;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.magna.xmbackend.entities.AdminAreaProjectRelTbl;
import com.magna.xmbackend.entities.AdminAreasTbl;
import com.magna.xmbackend.entities.PermissionTbl;
import com.magna.xmbackend.entities.ProjectsTbl;
import com.magna.xmbackend.entities.RoleAdminAreaRelTbl;
import com.magna.xmbackend.entities.RolePermissionRelTbl;
import com.magna.xmbackend.entities.RoleUserRelTbl;
import com.magna.xmbackend.entities.RolesTbl;
import com.magna.xmbackend.entities.SiteAdminAreaRelTbl;
import com.magna.xmbackend.entities.UsersTbl;
import com.magna.xmbackend.exception.CannotCreateRelationshipException;
import com.magna.xmbackend.exception.XMObjectNotFoundException;
import com.magna.xmbackend.jpa.dao.AdminAreaJpaDao;
import com.magna.xmbackend.jpa.dao.PermissionJpaDao;
import com.magna.xmbackend.jpa.dao.ProjectJpaDao;
import com.magna.xmbackend.jpa.dao.RolePermissionRelJpaDao;
import com.magna.xmbackend.jpa.dao.UserJpaDao;
import com.magna.xmbackend.jpa.rel.dao.AdminAreaProjectRelJpaDao;
import com.magna.xmbackend.jpa.rel.dao.RoleAdminAreaRelJpaDao;
import com.magna.xmbackend.jpa.rel.dao.RoleUserRelJpaDao;
import com.magna.xmbackend.mgr.PermissionMgr;
import com.magna.xmbackend.mgr.ValidationMgr;
import com.magna.xmbackend.utils.MessageMaker;
import com.magna.xmbackend.utils.PermissionFilter;
import com.magna.xmbackend.validation.vo.ValidationResponse;
import com.magna.xmbackend.vo.permission.ValidationRequest;
import com.magna.xmbackend.vo.rel.UserProjectRelRequest;

/**
 *
 * @author vijay
 */
@Component
public class ValidationMgrImpl implements ValidationMgr {

	private static final Logger LOG = LoggerFactory.getLogger(ValidationMgrImpl.class);

	@Autowired
	PermissionFilter permissionFilter;

	@Autowired
	PermissionMgr permissionMgr;
	@Autowired
	RoleUserRelJpaDao roleUserRelJpaDao;
	@Autowired
	UserJpaDao userJpaDao;
	@Autowired
	RolePermissionRelJpaDao rolePermissionRelJpaDao;
	@Autowired
	private MessageMaker messageMaker;
	@Autowired
	private RoleAdminAreaRelJpaDao roleAdminAreaRelJpaDao;
	@Autowired
	private ProjectJpaDao projectJpaDao;
	@Autowired
	private AdminAreaJpaDao adminAreaJpaDao;
	@Autowired
	private AdminAreaProjectRelJpaDao adminAreaProjectRelJpaDao;
	@Autowired
	PermissionJpaDao permissionJpaDao;

	/**
	 *
	 * @param validationRequest
	 * @return boolean
	 */
	@Override
	public boolean isObjectAccessAllowed(final ValidationRequest validationRequest) {
		LOG.info(">> isObjectAccessAllowed {}", validationRequest);
		final boolean isAccessAllowed = validateObject(validationRequest);
		LOG.info("<< isObjectAccessAllowed={}", isAccessAllowed);
		return isAccessAllowed;
	}

	/**
	 * Invoke permission manager and validate the object
	 *
	 * @param validationRequest
	 * @return boolean
	 */
	private boolean validateObject(final ValidationRequest validationRequest) {
		boolean isAccessAllowed = false;
		final PermissionTbl permissionTbl = permissionMgr.isObjectAccessAllowed(validationRequest);
		if (null != permissionTbl && null != permissionTbl.getName()) {
			isAccessAllowed = true;
		}
		return isAccessAllowed;
	}

	/**
	 * clears the cache
	 */
	@Override
	public boolean clearCache() {
		boolean isCacheClear = false;
		if (!permissionFilter.getObjectPerMap().isEmpty()) {
			permissionFilter.clearCache();
			isCacheClear = true;
		}
		return isCacheClear;
	}

	/*
	 * @Override public ValidationResponse checkHotlinePermissions(String
	 * username) {
	 * 
	 * boolean permissionCheck = false; UsersTbl usersTbl =
	 * this.userJpaDao.findByUsernameIgnoreCase(username); Set<String>
	 * permissisonNames = new HashSet<>(); List<RoleUserRelTbl> roleUserTbls =
	 * this.roleUserRelJpaDao.findByUserId(usersTbl); Set<RolesTbl> roleTbls =
	 * new HashSet<>(); for (RoleUserRelTbl roleUserRelTbl : roleUserTbls) {
	 * roleTbls.add(roleUserRelTbl.getRoleId()); }
	 * 
	 * Iterable<RolePermissionRelTbl> rolePermissionTbls =
	 * this.rolePermissionRelJpaDao.findByRoleIdIn(roleTbls); for
	 * (RolePermissionRelTbl rolePermissionRelTbl : rolePermissionTbls) {
	 * PermissionTbl permissionTbl = rolePermissionRelTbl.getPermissionId();
	 * permissisonNames.add(permissionTbl.getName()); } final List<Map<String,
	 * String>> statusMaps = new ArrayList<>(); if
	 * (permissisonNames.contains("VIEW_INACTIVE")) { try {
	 * checkFor9Permissions(permissisonNames); permissionCheck = true; } catch
	 * (XMObjectNotFoundException exception) { Map<String, String> statusMap =
	 * messageMaker.extractFromException(exception); statusMaps.add(statusMap);
	 * } } else { try { checkFor6Permissions(permissisonNames); permissionCheck
	 * = true; } catch (XMObjectNotFoundException exception) { Map<String,
	 * String> statusMap = messageMaker.extractFromException(exception);
	 * statusMaps.add(statusMap); } } ValidationResponse vr = new
	 * ValidationResponse(permissionCheck, statusMaps); return vr; }
	 */

	void checkFor6Permissions(Set<String> permissisonNames) {
		Set<String> requiredPermissions = new HashSet<>();
		Set<String> totPermissions = getInitial6Permissions();

		for (String permission : totPermissions) {
			if (!permissisonNames.contains(permission)) {
				requiredPermissions.add(permission);
			}
		}
		if (requiredPermissions.size() > 0) {
			String commaSeparatedUsernames = String.join(",", requiredPermissions);
			String[] param = { commaSeparatedUsernames };
			throw new XMObjectNotFoundException("Permissions not found", "PRM_ERR001", param);
		}
	}

	void checkFor9Permissions(Set<String> permissisonNames) {

		Set<String> requiredPermissions = new HashSet<>();
		Set<String> totPermissions = getInitial6Permissions();
		totPermissions.add("USER_TO_PROJECT-INACTIVE_ASSIGNMENT");
		totPermissions.add("USER_APPLICATION_TO_USER-INACTIVE_ASSIGNMENT");
		totPermissions.add("PROJECT_APPLICATION_TO_USER_PROJECT-INACTIVE_ASSIGNMENT");

		for (String permission : totPermissions) {
			if (!permissisonNames.contains(permission)) {
				requiredPermissions.add(permission);
			}
		}

		if (requiredPermissions.size() > 0) {
			String commaSeparatedUsernames = String.join(",", requiredPermissions);
			String[] param = { commaSeparatedUsernames };
			throw new XMObjectNotFoundException("Permissions not found", "PRM_ERR001", param);
		}
	}

	Set<String> getInitial6Permissions() {
		Set<String> first6Permissions = new HashSet<String>();
		first6Permissions.add("USER_TO_PROJECT-ASSIGN");
		first6Permissions.add("USER_TO_PROJECT-REMOVE");
		first6Permissions.add("USER_APPLICATION_TO_USER-REMOVE");
		first6Permissions.add("USER_APPLICATION_TO_USER-ASSIGN");
		first6Permissions.add("PROJECT_APPLICATION_TO_USER_PROJECT-ASSIGN");
		first6Permissions.add("PROJECT_APPLICATION_TO_USER_PROJECT-REMOVE");
		return first6Permissions;
	}

	@Override
	public ValidationResponse checkHotlinePermissions(String username, String adminAreaId) {
		boolean permissionCheck = false;
		UsersTbl usersTbl = this.userJpaDao.findByUsernameIgnoreCase(username);
		Set<String> permissisonNames = new HashSet<>();
		List<RoleUserRelTbl> roleUserTbls = this.roleUserRelJpaDao.findByUserId(usersTbl);
		Set<RolesTbl> roleTbls = new HashSet<>();
		Set<RolesTbl> roleTblsWithoutRoleAA = new HashSet<>();

		for (RoleUserRelTbl roleUserRelTbl : roleUserTbls) {
			RoleAdminAreaRelTbl roleAdminAreaRelTbl = roleUserRelTbl.getRoleAdminAreaRelId();
			// If user is in any role-admin-area then consider only those
			// role-admin-area
			if (null != roleAdminAreaRelTbl) {
				String adminAreaIdfromTbl = roleAdminAreaRelTbl.getAdminAreaId().getAdminAreaId();
				if (adminAreaIdfromTbl.equals(adminAreaId)) {
					roleTbls.add(roleUserRelTbl.getRoleId());
				}
			} else {
				roleTblsWithoutRoleAA.add(roleUserRelTbl.getRoleId());
			}
		}

		// getting all roles
		roleTbls.addAll(roleTblsWithoutRoleAA);
		Iterable<RolePermissionRelTbl> rolePermissionTblsWithoutRAA = this.rolePermissionRelJpaDao
				.findByRoleIdIn(roleTbls);
		for (RolePermissionRelTbl rolePermissionRelTbl : rolePermissionTblsWithoutRAA) {
			PermissionTbl permissionTbl = rolePermissionRelTbl.getPermissionId();
			permissisonNames.add(permissionTbl.getName());
		}

		final List<Map<String, String>> statusMaps = new ArrayList<>();
		if (permissisonNames.contains("VIEW_INACTIVE")) {
			try {
				checkFor9Permissions(permissisonNames);
				permissionCheck = true;
			} catch (XMObjectNotFoundException exception) {
				Map<String, String> statusMap = messageMaker.extractFromException(exception);
				statusMaps.add(statusMap);
			}
		} else {
			try {
				checkFor6Permissions(permissisonNames);
				permissionCheck = true;
			} catch (XMObjectNotFoundException exception) {
				Map<String, String> statusMap = messageMaker.extractFromException(exception);
				statusMaps.add(statusMap);
			}
		}
		ValidationResponse vr = new ValidationResponse(permissionCheck, statusMaps);
		return vr;

	}

	/*public void isUserProjectAssignmentAllowed(UserProjectRelRequest userProjectRelRequest, String adminName) {
		Set<AdminAreasTbl> adminAreasTbls = new HashSet<>();
		LOG.info(">> isUserProjectAssignmentAllowed");
		boolean isGlobalUser = false;
		List<RoleUserRelTbl> roleUserRelTbls = this.roleUserRelJpaDao.findByUsername(adminName);
		for (RoleUserRelTbl roleUserRel : roleUserRelTbls) {
			if (roleUserRel.getRoleAdminAreaRelId() == null) {
				isGlobalUser = true;
			}
			adminAreasTbls.addAll(this.roleAdminAreaRelJpaDao.findAdminAreasTblByRoleId(roleUserRel.getRoleId()));
		}
		LOG.debug("Admin areas: {} ", adminAreasTbls);
		if (!isGlobalUser) {
			Set<String> projects = new HashSet<>();
			for (AdminAreasTbl adminAreasTbl : adminAreasTbls) {
				List<String> findProjectsByAA = this.projectJpaDao.findProjectsByAA(adminAreasTbl);
				projects.addAll(findProjectsByAA);
			}

			UsersTbl usersTbl = this.userJpaDao.findOne(userProjectRelRequest.getUserId());
			ProjectsTbl projectTbl = this.projectJpaDao.findOne(userProjectRelRequest.getProjectId());
			if (projectTbl != null && usersTbl != null) {
				if (!projects.contains(projectTbl.getName())) {
					this.throwException(usersTbl, projectTbl);
				}
			}
		} else {
			String projectId = userProjectRelRequest.getProjectId();
			List<AdminAreaProjectRelTbl> adminAreaProjectRels = this.adminAreaProjectRelJpaDao
					.findByProjectId(new ProjectsTbl(projectId));
			UsersTbl usersTbl = this.userJpaDao.findOne(userProjectRelRequest.getUserId());
			ProjectsTbl projectTbl = this.projectJpaDao.findOne(userProjectRelRequest.getProjectId());

			if (adminAreaProjectRels.size() == 0 || adminAreaProjectRels == null) {
				if (projectTbl != null && usersTbl != null) {
					this.throwException(usersTbl, projectTbl);
				}
			}
		}
	}*/
	
	/*boolean isProjectAAMatchesRoleAA(final String adminName, final String projectId) {
		Set<AdminAreasTbl> adminAreasTbls = new HashSet<>();
        Set<RoleUserRelTbl> globalRolesRel = new HashSet<>();
        List<RoleUserRelTbl> roleUserRelTbls = this.roleUserRelJpaDao.findByUsername(adminName);
        for (RoleUserRelTbl roleUserRelTbl : roleUserRelTbls) {
               if (roleUserRelTbl.getRoleAdminAreaRelId() != null) {
                      adminAreasTbls.addAll(this.roleAdminAreaRelJpaDao.findAdminAreasTblByRoleId(roleUserRelTbl.getRoleId()));
               } else {
                     globalRolesRel.add(roleUserRelTbl);
               }
        }
        Set<AdminAreasTbl> aaListForProject = new HashSet<>();
        SiteAdminAreaRelTbl siteAAID;
        List<AdminAreaProjectRelTbl> adminAreaProjectRels = this.adminAreaProjectRelJpaDao.findByProjectId(new ProjectsTbl(projectId));
        for (AdminAreaProjectRelTbl adminAreaProjectRelTbl : adminAreaProjectRels) {
               if ((siteAAID = adminAreaProjectRelTbl.getSiteAdminAreaRelId()) != null) {
                     aaListForProject.add(siteAAID.getAdminAreaId());
               }
        }
        
		aaListForProject.retainAll(adminAreasTbls);
		return aaListForProject.isEmpty();
	}*/
	
	@Override
	public void isUserProjectAssignmentAllowed(UserProjectRelRequest userProjectRelRequest, String adminName) {
		isUserProjectRelAllowed(userProjectRelRequest, adminName, "USER_TO_PROJECT-ASSIGN");
	}
	
	@Override
	public void isUserProjectRemoveAllowed(UserProjectRelRequest userProjectRelRequest, String adminName) {
		isUserProjectRelAllowed(userProjectRelRequest, adminName, "USER_TO_PROJECT-REMOVE");
	}
	
	@Override
	public void isUserProjectActDeactAllowed(UserProjectRelRequest userProjectRelRequest, String adminName) {
		isUserProjectRelAllowed(userProjectRelRequest, adminName, "USER_TO_PROJECT-ACTIVATE_DEACTIVATE");
			
	}

	private void  isUserProjectRelAllowed(UserProjectRelRequest userProjectRelRequest, String adminName, final String permissionName) {
		Set<AdminAreasTbl> adminAreasTbls = new HashSet<>();
		Set<RoleUserRelTbl> globalRolesRel = new HashSet<>();
		Set<AdminAreasTbl> aaListForProject = getProjectAAMatchesRoleAA(userProjectRelRequest, adminName,
				adminAreasTbls, globalRolesRel);
		if (aaListForProject.isEmpty()) {
			for (RoleUserRelTbl gloRolRel : globalRolesRel) {
				String roleName = gloRolRel.getRoleId().getName();
				String permissionDetails = permissionJpaDao.getPermissionDetails(permissionName, roleName);
				if (permissionDetails != null) {
					// DON'T throw
					return;
				}
			}
			// need to check for global user and permission
			UsersTbl usersTbl = this.userJpaDao.findOne(userProjectRelRequest.getUserId());
			ProjectsTbl projectTbl = this.projectJpaDao.findOne(userProjectRelRequest.getProjectId());
			switch (permissionName) {
			case "USER_TO_PROJECT-REMOVE":
				throwRemoveException(usersTbl, projectTbl);
				break;
			case "USER_TO_PROJECT-ASSIGN":
				throwCreateException(usersTbl, projectTbl);
				break;
			case "USER_TO_PROJECT-ACTIVATE_DEACTIVATE":
				throwActDeActException(usersTbl, projectTbl);
			default:
				break;
			}
			
		} else {
			//get role by admin-area
			RoleAdminAreaRelTbl findRoleAdminAreaRelTblByAAId = new RoleAdminAreaRelTbl();
			for (AdminAreasTbl adminAreasTbl : aaListForProject) {
				findRoleAdminAreaRelTblByAAId = this.roleAdminAreaRelJpaDao.findRoleAdminAreaRelTblByAAId(adminAreasTbl);
				String permissionDetails = permissionJpaDao.getPermissionDetails(permissionName, findRoleAdminAreaRelTblByAAId.getRoleId().getName());
				if (permissionDetails != null) {
					// DON'T throw
					return;
				}
			}
			UsersTbl usersTbl = this.userJpaDao.findOne(userProjectRelRequest.getUserId());
			ProjectsTbl projectTbl = this.projectJpaDao.findOne(userProjectRelRequest.getProjectId());
			switch (permissionName) {
			case "USER_TO_PROJECT-REMOVE":
				throwRemoveException(usersTbl, projectTbl);
				break;
			case "USER_TO_PROJECT-ASSIGN":
				throwCreateException(usersTbl, projectTbl);
				break;
			case "USER_TO_PROJECT-ACTIVATE_DEACTIVATE":
				throwActDeActException(usersTbl, projectTbl);
			default:
				break;
			}
		}
	}

	private Set<AdminAreasTbl> getProjectAAMatchesRoleAA(UserProjectRelRequest userProjectRelRequest, String adminName,
			Set<AdminAreasTbl> adminAreasTbls, Set<RoleUserRelTbl> globalRolesRel) {
		//current user belongs to which all roles
		List<RoleUserRelTbl> roleUserRelTbls = this.roleUserRelJpaDao.findByUsername(adminName);
		for (RoleUserRelTbl roleUserRelTbl : roleUserRelTbls) {
			if (roleUserRelTbl.getRoleAdminAreaRelId() != null) {
				//adminAreasTbls.addAll(this.roleAdminAreaRelJpaDao.findAdminAreasTblByRoleId(roleUserRelTbl.getRoleId()));
				
				//current user having permission on which all admin-areas
				List<Object[]> findRoleAdminAreaUsersRelTblsByRoleId = roleAdminAreaRelJpaDao.findRoleAdminAreaUsersRelTblsByRoleId(adminName);
				for (Object[] objects : findRoleAdminAreaUsersRelTblsByRoleId) {
					adminAreasTbls.add(this.adminAreaJpaDao.findByNameIgnoreCase(objects[1].toString()));
				}
			} else {
				globalRolesRel.add(roleUserRelTbl);
				// check permission for global roles
			}
		}
		Set<AdminAreasTbl> aaListForProject = new HashSet<>();
		SiteAdminAreaRelTbl siteAAID;
		String projectId = userProjectRelRequest.getProjectId();
		//all admin-areas which contains the proect
		List<AdminAreaProjectRelTbl> adminAreaProjectRels = this.adminAreaProjectRelJpaDao.findByProjectId(new ProjectsTbl(projectId));
		for (AdminAreaProjectRelTbl adminAreaProjectRelTbl : adminAreaProjectRels) {
			if ((siteAAID = adminAreaProjectRelTbl.getSiteAdminAreaRelId()) != null) {
				//list of admin-areas containing the project
				aaListForProject.add(siteAAID.getAdminAreaId());
			}
		}

		aaListForProject.retainAll(adminAreasTbls);
		return aaListForProject;
	}

	private void throwCreateException(UsersTbl usersTbl, ProjectsTbl projectTbl) {
		Map<String, String> userNames = this.messageMaker.getUserNames(usersTbl);
		Map<String, String> projectNames = this.messageMaker.getProjectNames(projectTbl);
		final Map<String, String[]> i18nCodeMap = this.messageMaker.geti18nCodeMap(userNames, projectNames);
		throw new CannotCreateRelationshipException("Cannot create relationship", "ERR0023", i18nCodeMap);
	}
	
	private void throwRemoveException(UsersTbl usersTbl, ProjectsTbl projectTbl) {
		Map<String, String> userNames = this.messageMaker.getUserNames(usersTbl);
		Map<String, String> projectNames = this.messageMaker.getProjectNames(projectTbl);
		final Map<String, String[]> i18nCodeMap = this.messageMaker.geti18nCodeMap(userNames, projectNames);
		throw new CannotCreateRelationshipException("Cannot create relationship", "ERR0025", i18nCodeMap);
	}
	
	private void throwActDeActException(UsersTbl usersTbl, ProjectsTbl projectTbl) {
		Map<String, String> userNames = this.messageMaker.getUserNames(usersTbl);
		Map<String, String> projectNames = this.messageMaker.getProjectNames(projectTbl);
		final Map<String, String[]> i18nCodeMap = this.messageMaker.geti18nCodeMap(userNames, projectNames);
		throw new CannotCreateRelationshipException("Cannot create relationship", "ERR0026", i18nCodeMap);
	}
	
	
}


	
