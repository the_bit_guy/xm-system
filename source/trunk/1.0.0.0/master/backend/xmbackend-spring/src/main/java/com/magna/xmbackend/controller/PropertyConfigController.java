/**
 * 
 */
package com.magna.xmbackend.controller;

import java.util.EnumMap;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.magna.xmbackend.audit.mgr.PropertyConfigAuditMgr;
import com.magna.xmbackend.mgr.PropertyConfigManager;
import com.magna.xmbackend.vo.enums.LdapKey;
import com.magna.xmbackend.vo.propConfig.LdapConfigRequest;
import com.magna.xmbackend.vo.propConfig.ProjectExpiryConfigRequest;
import com.magna.xmbackend.vo.propConfig.ProjectPopupConfigRequest;
import com.magna.xmbackend.vo.propConfig.PropertyConfigResponse;
import com.magna.xmbackend.vo.propConfig.SmtpConfigRequest;
import com.magna.xmbackend.vo.propConfig.SingletonConfigRequest;

/**
 * @author Bhabadyuti Bal
 *
 */
@RestController
@RequestMapping(value = "/propertyconfig")
public class PropertyConfigController<T> {

	private static final Logger LOG = LoggerFactory.getLogger(PropertyConfigController.class);
	
	@Autowired
	private PropertyConfigManager propertyConfigManager;
	@Autowired
	private PropertyConfigAuditMgr<T> propertyConfigAuditMgr;
	
	
	@RequestMapping(value = "/getLdapDetails",
            method = RequestMethod.GET,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
	public @ResponseBody final ResponseEntity<PropertyConfigResponse> getLdapPropertyDetails(HttpServletRequest req){
		LOG.info("> getLdapDetails");
		final PropertyConfigResponse ldapConfigResponse = this.propertyConfigManager.findLdapDetails();
		LOG.info("< getLdapDetails");
		return new ResponseEntity<PropertyConfigResponse>(ldapConfigResponse, HttpStatus.OK);
	}
	
	
	
	@RequestMapping(value = "/getSmtpDetails",
            method = RequestMethod.GET,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
	public @ResponseBody final ResponseEntity<PropertyConfigResponse> getSmtpPropertyDetails(HttpServletRequest req){
		LOG.info("> getSmtpDetails");
		final PropertyConfigResponse smtpConfigResponse = this.propertyConfigManager.findSmtpDetails();
		LOG.info("< getSmtpDetails");
		return new ResponseEntity<PropertyConfigResponse>(smtpConfigResponse, HttpStatus.OK);
	}
	
	
	@RequestMapping(value = "/getSingletonDetails",
            method = RequestMethod.GET,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
	public @ResponseBody final ResponseEntity<PropertyConfigResponse> getSingletonDetails(HttpServletRequest req){
		LOG.info("> getSingletonDetails");
		final PropertyConfigResponse singletonConfigResponse = this.propertyConfigManager.findSingletonDetails();
		LOG.info("< getSingletonDetails");
		return new ResponseEntity<PropertyConfigResponse>(singletonConfigResponse, HttpStatus.OK);
	}
	
	
	@RequestMapping(value = "/getProjectPopupDetails",
            method = RequestMethod.GET,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
	public @ResponseBody final ResponseEntity<PropertyConfigResponse> getProjectPopupDetails(HttpServletRequest req){
		LOG.info("> getProjectPopupDetails");
		final PropertyConfigResponse projectPopupConfigResponse = this.propertyConfigManager.findProjectPopupDetails();
		LOG.info("< getProjectPopupDetails");
		return new ResponseEntity<PropertyConfigResponse>(projectPopupConfigResponse, HttpStatus.OK);
	}
	
	
	@RequestMapping(value = "/getProjectExpiryDetails",
            method = RequestMethod.GET,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
	public @ResponseBody final ResponseEntity<PropertyConfigResponse> getProjectExpiryDetails(HttpServletRequest req){
		LOG.info("> getProjectExpiryDetails");
		final PropertyConfigResponse projectExpiryConfigResponse = this.propertyConfigManager.findProjectExpiryDetails();
		LOG.info("< getProjectExpiryDetails");
		return new ResponseEntity<PropertyConfigResponse>(projectExpiryConfigResponse, HttpStatus.OK);
	}
	
	
	@RequestMapping(value = "/updateLdapDetails",
            method = RequestMethod.PUT,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
	public @ResponseBody final ResponseEntity<PropertyConfigResponse> updateLdapDetails(HttpServletRequest httpServletRequest,
            @RequestBody LdapConfigRequest ldapConfigRequest){
		LOG.info("> property update");
		PropertyConfigResponse propertyConfigResponse = null;
		try {
			propertyConfigResponse = this.propertyConfigManager.updatePropertyConfigDetails(ldapConfigRequest);
			this.propertyConfigAuditMgr.propertyConfigUpdateSuccessAudit(httpServletRequest, propertyConfigResponse);
		} catch (Exception ex) {
			EnumMap<LdapKey, String> ldapKeyValue = ldapConfigRequest.getLdapKeyValue();
			for(LdapKey ldapKey : ldapKeyValue.keySet()){
				final String key = ldapKey.toString();
				String value = ldapKeyValue.get(key);
				LOG.error("Exception / Error in updating ldap config with exception msg {}", ex.getMessage());
				String errMsg = "Error in updating ldap config with propery " + key + " and value " + value + " : " + ex.getMessage().split("with ErrorCode")[0];
				this.propertyConfigAuditMgr.propertyConfigUpdateFailureAudit(httpServletRequest, ldapConfigRequest, errMsg);
				throw ex;
			}
			
		}
		LOG.info("> property update");
		return new ResponseEntity<PropertyConfigResponse>(propertyConfigResponse, HttpStatus.ACCEPTED);
	}
	
	
	@RequestMapping(value = "/updateSmtpDetails",
            method = RequestMethod.PUT,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
	public @ResponseBody final ResponseEntity<PropertyConfigResponse> updateSmtpDetails(HttpServletRequest httpServletRequest,
            @RequestBody SmtpConfigRequest smtpConfigRequest){
		LOG.info("> property update");
		PropertyConfigResponse propertyConfigResponse = null;
		try {
			propertyConfigResponse = this.propertyConfigManager.updatePropertyConfigDetails(smtpConfigRequest);
			this.propertyConfigAuditMgr.propertyConfigUpdateSuccessAudit(httpServletRequest, propertyConfigResponse);
		} catch (Exception ex) {
			String errMsg = "Error / Exception occured while updating SmtpDetails with reason: "+ex.getMessage();
			this.propertyConfigAuditMgr.propertyConfigUpdateFailureAudit(httpServletRequest, "SmtpDetails", errMsg);
		}
		LOG.info("> property update");
		return new ResponseEntity<PropertyConfigResponse>(propertyConfigResponse, HttpStatus.ACCEPTED);
	}
	
	
	@RequestMapping(value = "/updateSingletonDetails",
            method = RequestMethod.PUT,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
	public @ResponseBody final ResponseEntity<PropertyConfigResponse> updateSingletonDetails(HttpServletRequest httpServletRequest,
            @RequestBody SingletonConfigRequest singletonConfigRequest){
		LOG.info("> property update");
		PropertyConfigResponse propertyConfigResponse = null;
		try {
			propertyConfigResponse = this.propertyConfigManager.updatePropertyConfigDetails(singletonConfigRequest);
			this.propertyConfigAuditMgr.propertyConfigUpdateSuccessAudit(httpServletRequest, propertyConfigResponse);
		} catch (Exception ex) {
			String errMsg = "Error / Exception occured while updating SingletonDetails with reason: "+ex.getMessage();
			this.propertyConfigAuditMgr.propertyConfigUpdateFailureAudit(httpServletRequest, "SingletonDetails", errMsg);
		}
		LOG.info("> property update");
		return new ResponseEntity<PropertyConfigResponse>(propertyConfigResponse, HttpStatus.ACCEPTED);
	}
	
	
	@RequestMapping(value = "/updateProjectPopupDetails",
            method = RequestMethod.PUT,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
	public @ResponseBody final ResponseEntity<PropertyConfigResponse> updateProjectPopupDetails(HttpServletRequest httpServletRequest,
            @RequestBody ProjectPopupConfigRequest projectpopupConfigRequest){
		LOG.info("> property update");
		PropertyConfigResponse propertyConfigResponse = null;
		try {
			propertyConfigResponse = this.propertyConfigManager.updatePropertyConfigDetails(projectpopupConfigRequest);
			this.propertyConfigAuditMgr.propertyConfigUpdateSuccessAudit(httpServletRequest, propertyConfigResponse);
		} catch (Exception ex) {
			String errMsg = "Error / Exception occured while updating ProjectPopupDetails with reason: "+ex.getMessage();
			this.propertyConfigAuditMgr.propertyConfigUpdateFailureAudit(httpServletRequest, "ProjectPopupDetails", errMsg);
		}
		LOG.info("> property update");
		return new ResponseEntity<PropertyConfigResponse>(propertyConfigResponse, HttpStatus.ACCEPTED);
	}
	
	
	@RequestMapping(value = "/updateProjectExpiryDetails",
            method = RequestMethod.PUT,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
	public @ResponseBody final ResponseEntity<PropertyConfigResponse> updateProjectExpiryDetails(HttpServletRequest httpServletRequest,
            @RequestBody ProjectExpiryConfigRequest projectexpiryConfigRequest){
		LOG.info("> property update");
		PropertyConfigResponse propertyConfigResponse = null;
		try {
			propertyConfigResponse = this.propertyConfigManager.updatePropertyConfigDetails(projectexpiryConfigRequest);
			this.propertyConfigAuditMgr.propertyConfigUpdateSuccessAudit(httpServletRequest, propertyConfigResponse);
		} catch (Exception ex) {
			String errMsg = "Error / Exception occured while updating ProjectExpiryDetails with reason: "+ex.getMessage();
			this.propertyConfigAuditMgr.propertyConfigUpdateFailureAudit(httpServletRequest, "ProjectExpiryDetails", errMsg);
		}
		LOG.info("> property update");
		return new ResponseEntity<PropertyConfigResponse>(propertyConfigResponse, HttpStatus.ACCEPTED);
	}
}
