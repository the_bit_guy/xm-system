/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magna.xmbackend.jpa.dao;

import javax.transaction.Transactional;

import org.springframework.data.repository.CrudRepository;

import com.magna.xmbackend.entities.LanguagesTbl;
import com.magna.xmbackend.entities.UserAppTranslationTbl;
import com.magna.xmbackend.entities.UserApplicationsTbl;

/**
 *
 * @author dana
 */
@Transactional
public interface UserAppTranslationJpaDao extends CrudRepository<UserAppTranslationTbl, String> {

    UserAppTranslationTbl findByUserApplicationIdAndLanguageCode(UserApplicationsTbl userApplicationId, LanguagesTbl languageCode);
    
    UserAppTranslationTbl findByNameIgnoreCaseAndLanguageCode(String userApplicationName, LanguagesTbl languageCode);
}
