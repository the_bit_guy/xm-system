/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magna.xmbackend.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author Admin
 */
@Entity
@Table(name = "USER_USER_APP_REL_TBL")
@NamedQueries({
    @NamedQuery(name = "UserUserAppRelTbl.findAll", query = "SELECT u FROM UserUserAppRelTbl u")})
public class UserUserAppRelTbl implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "USER_USER_APP_REL_ID")
    private String userUserAppRelId;
    @Basic(optional = false)
    @Column(name = "USER_REL_TYPE")
    private String userRelType;
    @JoinColumn(name = "SITE_ADMIN_AREA_REL_ID", referencedColumnName = "SITE_ADMIN_AREA_REL_ID")
    @ManyToOne(optional = false)
    private SiteAdminAreaRelTbl siteAdminAreaRelId;
    @JoinColumn(name = "USER_ID", referencedColumnName = "USER_ID")
    @ManyToOne(optional = false)
    private UsersTbl userId;
    @JoinColumn(name = "USER_APPLICATION_ID", referencedColumnName = "USER_APPLICATION_ID")
    @ManyToOne(optional = false)
    private UserApplicationsTbl userApplicationId;

    public UserUserAppRelTbl() {
    }

    public UserUserAppRelTbl(String userUserAppRelId) {
        this.userUserAppRelId = userUserAppRelId;
    }

    public UserUserAppRelTbl(String userUserAppRelId, String userRelType) {
        this.userUserAppRelId = userUserAppRelId;
        this.userRelType = userRelType;
    }

    public String getUserUserAppRelId() {
        return userUserAppRelId;
    }

    public void setUserUserAppRelId(String userUserAppRelId) {
        this.userUserAppRelId = userUserAppRelId;
    }

    public String getUserRelType() {
        return userRelType;
    }

    public void setUserRelType(String userRelType) {
        this.userRelType = userRelType;
    }

    public SiteAdminAreaRelTbl getSiteAdminAreaRelId() {
        return siteAdminAreaRelId;
    }

    public void setSiteAdminAreaRelId(SiteAdminAreaRelTbl siteAdminAreaRelId) {
        this.siteAdminAreaRelId = siteAdminAreaRelId;
    }

    public UsersTbl getUserId() {
        return userId;
    }

    public void setUserId(UsersTbl userId) {
        this.userId = userId;
    }

    public UserApplicationsTbl getUserApplicationId() {
        return userApplicationId;
    }

    public void setUserApplicationId(UserApplicationsTbl userApplicationId) {
        this.userApplicationId = userApplicationId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (userUserAppRelId != null ? userUserAppRelId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof UserUserAppRelTbl)) {
            return false;
        }
        UserUserAppRelTbl other = (UserUserAppRelTbl) object;
        if ((this.userUserAppRelId == null && other.userUserAppRelId != null) || (this.userUserAppRelId != null && !this.userUserAppRelId.equals(other.userUserAppRelId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.magna.xmbackend.entities.UserUserAppRelTbl[ userUserAppRelId=" + userUserAppRelId + " ]";
    }
    
}
