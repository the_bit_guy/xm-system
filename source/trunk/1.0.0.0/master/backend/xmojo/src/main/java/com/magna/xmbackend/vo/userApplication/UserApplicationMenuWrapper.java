/**
 * 
 */
package com.magna.xmbackend.vo.userApplication;

/**
 * @author Bhabadyuti Bal
 *
 */
public class UserApplicationMenuWrapper {

	private Iterable<UserApplicationMenuResponse> userAppMenuResponses;

	public UserApplicationMenuWrapper() {
	}

	/**
	 * @param userAppMenuResponses
	 */
	public UserApplicationMenuWrapper(Iterable<UserApplicationMenuResponse> userAppMenuResponses) {
		this.userAppMenuResponses = userAppMenuResponses;
	}

	/**
	 * @return the userAppMenuResponses
	 */
	public Iterable<UserApplicationMenuResponse> getUserAppMenuResponses() {
		return userAppMenuResponses;
	}

	/**
	 * @param userAppMenuResponses
	 *            the userAppMenuResponses to set
	 */
	public void setUserAppMenuResponses(Iterable<UserApplicationMenuResponse> userAppMenuResponses) {
		this.userAppMenuResponses = userAppMenuResponses;
	}

}
