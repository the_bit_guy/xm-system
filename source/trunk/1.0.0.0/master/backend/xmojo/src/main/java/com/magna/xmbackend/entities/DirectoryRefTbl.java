/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magna.xmbackend.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author Admin
 */
@Entity
@Table(name = "DIRECTORY_REF_TBL")
@NamedQueries({
    @NamedQuery(name = "DirectoryRefTbl.findAll", query = "SELECT d FROM DirectoryRefTbl d")})
public class DirectoryRefTbl implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "DIRECTORY_REF_ID")
    private String directoryRefId;
    @Column(name = "OBJECT_TYPE")
    private String objectType;
    @Column(name = "OBJECT_ID")
    private String objectId;
    @Basic(optional = false)
    @Column(name = "CREATE_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createDate;
    @Basic(optional = false)
    @Column(name = "UPDATE_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateDate;
    @JoinColumn(name = "DIRECTORY_ID", referencedColumnName = "DIRECTORY_ID")
    @ManyToOne
    private DirectoryTbl directoryId;

    public DirectoryRefTbl() {
    }

    public DirectoryRefTbl(String directoryRefId) {
        this.directoryRefId = directoryRefId;
    }

    public DirectoryRefTbl(String directoryRefId, Date createDate, Date updateDate) {
        this.directoryRefId = directoryRefId;
        this.createDate = createDate;
        this.updateDate = updateDate;
    }

    public String getDirectoryRefId() {
        return directoryRefId;
    }

    public void setDirectoryRefId(String directoryRefId) {
        this.directoryRefId = directoryRefId;
    }

    public String getObjectType() {
        return objectType;
    }

    public void setObjectType(String objectType) {
        this.objectType = objectType;
    }

    public String getObjectId() {
        return objectId;
    }

    public void setObjectId(String objectId) {
        this.objectId = objectId;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public DirectoryTbl getDirectoryId() {
        return directoryId;
    }

    public void setDirectoryId(DirectoryTbl directoryId) {
        this.directoryId = directoryId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (directoryRefId != null ? directoryRefId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DirectoryRefTbl)) {
            return false;
        }
        DirectoryRefTbl other = (DirectoryRefTbl) object;
        if ((this.directoryRefId == null && other.directoryRefId != null) || (this.directoryRefId != null && !this.directoryRefId.equals(other.directoryRefId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.magna.xmbackend.entities.DirectoryRefTbl[ directoryRefId=" + directoryRefId + " ]";
    }
    
}
