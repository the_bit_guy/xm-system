/**
 * 
 */
package com.magna.xmbackend.vo.liveMessage;

import java.io.Serializable;

import com.magna.xmbackend.vo.CreationInfo;

/**
 * @author Bhabadyuti Bal
 *
 */
public class LiveMessageTranslation implements Serializable{

	private static final long serialVersionUID = 1L;

	private String liveMsgTransId;
	private String subject;
	private String message;
	private String languageCode;
	private CreationInfo creationInfo;
	
	public LiveMessageTranslation() {
	}
	

	/**
	 * @return the liveMsgTransId
	 */
	public String getLiveMsgTransId() {
		return liveMsgTransId;
	}


	/**
	 * @param liveMsgTransId the liveMsgTransId to set
	 */
	public void setLiveMsgTransId(String liveMsgTransId) {
		this.liveMsgTransId = liveMsgTransId;
	}


	/**
	 * @return the subject
	 */
	public String getSubject() {
		return subject;
	}
	/**
	 * @param subject the subject to set
	 */
	public void setSubject(String subject) {
		this.subject = subject;
	}
	/**
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}
	/**
	 * @param message the message to set
	 */
	public void setMessage(String message) {
		this.message = message;
	}
	/**
	 * @return the languageCode
	 */
	public String getLanguageCode() {
		return languageCode;
	}
	/**
	 * @param languageCode the languageCode to set
	 */
	public void setLanguageCode(String languageCode) {
		this.languageCode = languageCode;
	}
	/**
	 * @return the creationInfo
	 */
	public CreationInfo getCreationInfo() {
		return creationInfo;
	}
	/**
	 * @param creationInfo the creationInfo to set
	 */
	public void setCreationInfo(CreationInfo creationInfo) {
		this.creationInfo = creationInfo;
	}
	
}
