/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magna.xmbackend.entities;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonBackReference;

/**
 *
 * @author Admin
 */
@Entity
@Table(name = "PERMISSION_TBL")
@NamedQueries({
    @NamedQuery(name = "PermissionTbl.findAll", query = "SELECT p FROM PermissionTbl p")})
public class PermissionTbl implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "PERMISSION_ID")
    private String permissionId;
    @Basic(optional = false)
    @Column(name = "NAME")
    private String name;
    @Basic(optional = false)
    @Column(name = "PERMISSION_TYPE")
    private String permissionType;
    @Basic(optional = false)
    @Column(name = "CREATE_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createDate;
    @Basic(optional = false)
    @Column(name = "UPDATE_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateDate;
    @JsonBackReference
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "permissionId")
    private Collection<PermissionApiMappingTbl> permissionApiMappingTblCollection;
    @JsonBackReference
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "permissionId")
    private Collection<RolePermissionRelTbl> rolePermissionRelTblCollection;

    public PermissionTbl() {
    }

    public PermissionTbl(String permissionId) {
        this.permissionId = permissionId;
    }

    public PermissionTbl(String permissionId, String name, String permissionType, Date createDate, Date updateDate) {
        this.permissionId = permissionId;
        this.name = name;
        this.permissionType = permissionType;
        this.createDate = createDate;
        this.updateDate = updateDate;
    }

    public String getPermissionId() {
        return permissionId;
    }

    public void setPermissionId(String permissionId) {
        this.permissionId = permissionId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPermissionType() {
        return permissionType;
    }

    public void setPermissionType(String permissionType) {
        this.permissionType = permissionType;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public Collection<PermissionApiMappingTbl> getPermissionApiMappingTblCollection() {
        return permissionApiMappingTblCollection;
    }

    public void setPermissionApiMappingTblCollection(Collection<PermissionApiMappingTbl> permissionApiMappingTblCollection) {
        this.permissionApiMappingTblCollection = permissionApiMappingTblCollection;
    }

    public Collection<RolePermissionRelTbl> getRolePermissionRelTblCollection() {
        return rolePermissionRelTblCollection;
    }

    public void setRolePermissionRelTblCollection(Collection<RolePermissionRelTbl> rolePermissionRelTblCollection) {
        this.rolePermissionRelTblCollection = rolePermissionRelTblCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (permissionId != null ? permissionId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PermissionTbl)) {
            return false;
        }
        PermissionTbl other = (PermissionTbl) object;
        if ((this.permissionId == null && other.permissionId != null) || (this.permissionId != null && !this.permissionId.equals(other.permissionId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.magna.xmbackend.entities.PermissionTbl[ permissionId=" + permissionId + " ]";
    }
    
}
