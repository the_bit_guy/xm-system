/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magna.xmbackend.vo.jpa.site;

import com.magna.xmbackend.entities.AdminAreasTbl;

/**
 *
 * @author dana
 */
public class SiteAdminAreaRelIdWithAdminArea {
    private String siteAdminAreaRelId;
    private AdminAreasTbl adminAreasTbl;

    public SiteAdminAreaRelIdWithAdminArea() {
    }

    public SiteAdminAreaRelIdWithAdminArea(String siteAdminAreaRelId, AdminAreasTbl adminAreasTbl) {
        this.siteAdminAreaRelId = siteAdminAreaRelId;
        this.adminAreasTbl = adminAreasTbl;
    }

    /**
     * @return the siteAdminAreaRelId
     */
    public String getSiteAdminAreaRelId() {
        return siteAdminAreaRelId;
    }

    /**
     * @param siteAdminAreaRelId the siteAdminAreaRelId to set
     */
    public void setSiteAdminAreaRelId(String siteAdminAreaRelId) {
        this.siteAdminAreaRelId = siteAdminAreaRelId;
    }

    /**
     * @return the adminAreasTbl
     */
    public AdminAreasTbl getAdminAreasTbl() {
        return adminAreasTbl;
    }

    /**
     * @param adminAreasTbl the adminAreasTbl to set
     */
    public void setAdminAreasTbl(AdminAreasTbl adminAreasTbl) {
        this.adminAreasTbl = adminAreasTbl;
    }
    
    
}
