/**
 * 
 */
package com.magna.xmbackend.vo.directory;

import com.magna.xmbackend.vo.CreationInfo;
import com.magna.xmbackend.vo.enums.DirectoryObjectType;

/**
 * @author Bhabadyuti Bal
 *
 */
public class DirectoryRelRequest {

	private String id;
	private String directoryId;
	private DirectoryObjectType objectType;
	private String objectId;
	private CreationInfo creationInfo;
	
	public DirectoryRelRequest() {
	}
	
	/**
	 * @param id
	 * @param directoryId
	 * @param objectType
	 * @param objectId
	 * @param creationInfo
	 */
	public DirectoryRelRequest(String id, String directoryId, DirectoryObjectType objectType, String objectId,
			CreationInfo creationInfo) {
		this.id = id;
		this.directoryId = directoryId;
		this.objectType = objectType;
		this.objectId = objectId;
		this.creationInfo = creationInfo;
	}



	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * @return the directoryId
	 */
	public String getDirectoryId() {
		return directoryId;
	}

	/**
	 * @param directoryId the directoryId to set
	 */
	public void setDirectoryId(String directoryId) {
		this.directoryId = directoryId;
	}

	/**
	 * @return the objectType
	 */
	public DirectoryObjectType getObjectType() {
		return objectType;
	}

	/**
	 * @param objectType the objectType to set
	 */
	public void setObjectType(DirectoryObjectType objectType) {
		this.objectType = objectType;
	}

	/**
	 * @return the objectId
	 */
	public String getObjectId() {
		return objectId;
	}

	/**
	 * @param objectId the objectId to set
	 */
	public void setObjectId(String objectId) {
		this.objectId = objectId;
	}

	/**
	 * @return the creationInfo
	 */
	public CreationInfo getCreationInfo() {
		return creationInfo;
	}

	/**
	 * @param creationInfo the creationInfo to set
	 */
	public void setCreationInfo(CreationInfo creationInfo) {
		this.creationInfo = creationInfo;
	}
	
	
	
}
