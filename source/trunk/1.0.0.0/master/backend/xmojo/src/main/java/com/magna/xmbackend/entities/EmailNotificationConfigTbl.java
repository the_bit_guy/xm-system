/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magna.xmbackend.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author Admin
 */
@Entity
@Table(name = "EMAIL_NOTIFICATION_CONFIG_TBL")
@NamedQueries({
    @NamedQuery(name = "EmailNotificationConfigTbl.findAll", query = "SELECT e FROM EmailNotificationConfigTbl e")})
public class EmailNotificationConfigTbl implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "EMAIL_NOTIFICATION_CONFIG_ID")
    private String emailNotificationConfigId;
    @Basic(optional = false)
    @Column(name = "NAME")
    private String name;
    @Column(name = "STATUS")
    private String status;
    @Basic(optional = false)
    @Column(name = "CREATE_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createDate;
    @Basic(optional = false)
    @Column(name = "UPDATE_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateDate;
    
    @OneToOne(mappedBy = "emailNotificationConfigId", cascade=CascadeType.ALL)
    private EmailNotifyToUserRelTbl emailNotifyToUserId;
    
    @JoinColumn(name = "EMAIL_NOTIFICATION_EVENT_ID", referencedColumnName = "EMAIL_NOTIFICATION_EVENT_ID")
    @ManyToOne(optional = false)
    private EmailNotificationEventTbl emailNotificationEventId;
    
    @ManyToOne
    @JoinColumn(name = "EMAIL_TEMPLATE_ID", referencedColumnName = "EMAIL_TEMPLATE_ID")
    private EmailTemplateTbl emailTemplateId;
    
    public EmailNotificationConfigTbl(EmailNotificationEventTbl emailNotificationEventId) {
		this.emailNotificationEventId = emailNotificationEventId;
	}


	public EmailNotifyToUserRelTbl getEmailNotifyToUserId() {
		return emailNotifyToUserId;
	}


	public void setEmailNotifyToUserId(EmailNotifyToUserRelTbl emailNotifyToUserId) {
		this.emailNotifyToUserId = emailNotifyToUserId;
	}


	public EmailNotificationEventTbl getEmailNotificationEventId() {
		return emailNotificationEventId;
	}
	

	public void setEmailNotificationEventId(EmailNotificationEventTbl emailNotificationEventId) {
		this.emailNotificationEventId = emailNotificationEventId;
	}

	public EmailNotificationConfigTbl() {
    }

    public String getName() {
 		return name;
 	}

 	public void setName(String name) {
 		this.name = name;
 	}
 	
    public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

    public EmailNotificationConfigTbl(String emailNotificationConfigId) {
        this.emailNotificationConfigId = emailNotificationConfigId;
    }

    public EmailNotificationConfigTbl(String emailNotificationConfigId, String description, String subject, String message, Date createDate, Date updateDate) {
        this.emailNotificationConfigId = emailNotificationConfigId;
        this.createDate = createDate;
        this.updateDate = updateDate;
    }

    public String getEmailNotificationConfigId() {
        return emailNotificationConfigId;
    }

    public void setEmailNotificationConfigId(String emailNotificationConfigId) {
        this.emailNotificationConfigId = emailNotificationConfigId;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public EmailTemplateTbl getEmailTemplateId() {
		return emailTemplateId;
	}

	public void setEmailTemplateId(EmailTemplateTbl emailTemplateId) {
		this.emailTemplateId = emailTemplateId;
	}

	@Override
    public int hashCode() {
        int hash = 0;
        hash += (emailNotificationConfigId != null ? emailNotificationConfigId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof EmailNotificationConfigTbl)) {
            return false;
        }
        EmailNotificationConfigTbl other = (EmailNotificationConfigTbl) object;
        if ((this.emailNotificationConfigId == null && other.emailNotificationConfigId != null) || (this.emailNotificationConfigId != null && !this.emailNotificationConfigId.equals(other.emailNotificationConfigId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.magna.xmbackend.entities.EmailNotificationConfigTbl[ emailNotificationConfigId=" + emailNotificationConfigId + " ]";
    }

    
}
