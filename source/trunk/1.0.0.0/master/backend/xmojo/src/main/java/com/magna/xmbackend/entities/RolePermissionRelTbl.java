/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magna.xmbackend.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author Admin
 */
@Entity
@Table(name = "ROLE_PERMISSION_REL_TBL")
@NamedQueries({
    @NamedQuery(name = "RolePermissionRelTbl.findAll", query = "SELECT r FROM RolePermissionRelTbl r")})
public class RolePermissionRelTbl implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "ROLE_PERMISSION_REL_ID")
    private String rolePermissionRelId;
    @Basic(optional = false)
    @Column(name = "CREATE_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createDate;
    @Basic(optional = false)
    @Column(name = "UPDATE_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateDate;
    @JoinColumn(name = "PERMISSION_ID", referencedColumnName = "PERMISSION_ID")
    @ManyToOne(optional = false)
    private PermissionTbl permissionId;
    @JoinColumn(name = "ROLE_ID", referencedColumnName = "ROLE_ID")
    @ManyToOne(optional = false)
    private RolesTbl roleId;

    public RolePermissionRelTbl() {
    }

    public RolePermissionRelTbl(String rolePermissionRelId) {
        this.rolePermissionRelId = rolePermissionRelId;
    }

    public RolePermissionRelTbl(String rolePermissionRelId, Date createDate, Date updateDate) {
        this.rolePermissionRelId = rolePermissionRelId;
        this.createDate = createDate;
        this.updateDate = updateDate;
    }

    public String getRolePermissionRelId() {
        return rolePermissionRelId;
    }

    public void setRolePermissionRelId(String rolePermissionRelId) {
        this.rolePermissionRelId = rolePermissionRelId;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public PermissionTbl getPermissionId() {
        return permissionId;
    }

    public void setPermissionId(PermissionTbl permissionId) {
        this.permissionId = permissionId;
    }

    public RolesTbl getRoleId() {
        return roleId;
    }

    public void setRoleId(RolesTbl roleId) {
        this.roleId = roleId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (rolePermissionRelId != null ? rolePermissionRelId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof RolePermissionRelTbl)) {
            return false;
        }
        RolePermissionRelTbl other = (RolePermissionRelTbl) object;
        if ((this.rolePermissionRelId == null && other.rolePermissionRelId != null) || (this.rolePermissionRelId != null && !this.rolePermissionRelId.equals(other.rolePermissionRelId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.magna.xmbackend.entities.RolePermissionRelTbl[ rolePermissionRelId=" + rolePermissionRelId + " ]";
    }
    
}
