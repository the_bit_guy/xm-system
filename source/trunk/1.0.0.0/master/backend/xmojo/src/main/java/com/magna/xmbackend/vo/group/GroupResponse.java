/**
 * 
 */
package com.magna.xmbackend.vo.group;

import java.util.List;
import java.util.Map;
import java.util.Set;

import com.magna.xmbackend.entities.GroupsTbl;

/**
 * @author Bhabadyuti Bal
 *
 */
public class GroupResponse {


	private List<GroupsTbl> groupTbls;
	private Set<Map<String, String>> statusMaps;
	
	
	public GroupResponse() {
	}
	
	
	/**
	 * @param groupTbls
	 */
	public GroupResponse(final List<GroupsTbl> groupTbls) {
		this.groupTbls = groupTbls;
	}
	


	/**
	 * @return the statusMaps
	 */
	public Set<Map<String, String>> getStatusMaps() {
		return statusMaps;
	}


	/**
	 * @param statusMaps the statusMaps to set
	 */
	public void setStatusMaps(Set<Map<String, String>> statusMaps) {
		this.statusMaps = statusMaps;
	}


	/**
	 * @param statusMaps
	 */
	public GroupResponse(Set<Map<String, String>> statusMaps) {
		this.statusMaps = statusMaps;
	}


	/**
	 * @return the groupTbls
	 */
	public List<GroupsTbl> getGroupTbls() {
		return groupTbls;
	}

	/**
	 * @param groupTbls the groupTbls to set
	 */
	public void setGroupTbls(List<GroupsTbl> groupTbls) {
		this.groupTbls = groupTbls;
	}


}
