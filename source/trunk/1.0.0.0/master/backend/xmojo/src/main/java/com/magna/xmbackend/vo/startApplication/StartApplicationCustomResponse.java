/**
 * 
 */
package com.magna.xmbackend.vo.startApplication;

import java.util.Date;

/**
 * @author Roshan Ekka
 *
 */
public class StartApplicationCustomResponse {
	
    private String name;
    private String isMessage;
    private Date startMessageExpiryDate;
    private String baseAppName;
    
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getIsMessage() {
		return isMessage;
	}
	public void setIsMessage(String isMessage) {
		this.isMessage = isMessage;
	}
	public Date getStartMessageExpiryDate() {
		return startMessageExpiryDate;
	}
	public void setStartMessageExpiryDate(Date startMessageExpiryDate) {
		this.startMessageExpiryDate = startMessageExpiryDate;
	}
	public String getBaseAppName() {
		return baseAppName;
	}
	public void setBaseAppName(String baseAppName) {
		this.baseAppName = baseAppName;
	}
}
