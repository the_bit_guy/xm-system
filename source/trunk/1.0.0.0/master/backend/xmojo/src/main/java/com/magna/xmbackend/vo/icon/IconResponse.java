package com.magna.xmbackend.vo.icon;

import com.magna.xmbackend.vo.Message;
import java.util.List;

public class IconResponse {

    private List<Icon> icons;
    private Message message;

    public IconResponse() {
    }

    /**
     *
     * @param icons
     * @param message
     */
    public IconResponse(List<Icon> icons, Message message) {
        this.icons = icons;
        this.message = message;
    }

    /**
     * @return the icons
     */
    public final List<Icon> getIcons() {
        return icons;
    }

    /**
     * @param icons the icons to set
     */
    public final void setIcons(final List<Icon> icons) {
        this.icons = icons;
    }

    /**
     * @return the message
     */
    public final Message getMessage() {
        return message;
    }

    /**
     * @param message the message to set
     */
    public final void setMessage(final Message message) {
        this.message = message;
    }

}
