package com.magna.xmbackend.vo.jpa.site;

import java.util.List;

/**
 *
 * @author vijay
 */
public class SiteAdminAreaRelIdWithAdminAreaStartAppRelResponse {

    private List<SiteAdminAreaRelIdWithAdminAreaStartAppRel> siteAdminAreaRelIdWithAdminAreaStartAppRel;

    public SiteAdminAreaRelIdWithAdminAreaStartAppRelResponse() {
    }

    /**
     *
     * @param saariwaauars
     */
    public SiteAdminAreaRelIdWithAdminAreaStartAppRelResponse(List<SiteAdminAreaRelIdWithAdminAreaStartAppRel> saariwaauars) {
        this.siteAdminAreaRelIdWithAdminAreaStartAppRel = saariwaauars;
    }

    /**
     *
     * @return siteAdminAreaRelIdWithAdminAreaStartAppRel
     */
    public final List<SiteAdminAreaRelIdWithAdminAreaStartAppRel> getSiteAdminAreaRelIdWithAdminAreaStartAppRel() {
        return siteAdminAreaRelIdWithAdminAreaStartAppRel;
    }

    /**
     *
     * @param saariwaasars
     */
    public final void setSiteAdminAreaRelIdWithAdminAreaStartAppRel(final List<SiteAdminAreaRelIdWithAdminAreaStartAppRel> saariwaasars) {
        this.siteAdminAreaRelIdWithAdminAreaStartAppRel = saariwaasars;
    }

}
