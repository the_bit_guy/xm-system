/**
 * 
 */
package com.magna.xmbackend.entities;

import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * @author Bhabadyuti Bal
 *
 */
@Entity
@Table(name = "REARRANGE_APPLICATIONS_TBL")
public class RearrangeApplicationsTbl {

	@Id
	@Column(name = "REARRANGE_ID")
	private String rearrangeId;
	
	@JoinColumn(name = "USER_ID", referencedColumnName = "USER_ID")
    @OneToOne(optional = false)
	private UsersTbl userId;
	
	@JoinColumn(name = "SITE_ID", referencedColumnName = "SITE_ID")
    @OneToOne(optional = false)
	private SitesTbl siteId;
	
	@JoinColumn(name = "PROJECT_ID", referencedColumnName = "PROJECT_ID")
    @OneToOne(optional = false)
	private ProjectsTbl projectId;
	
	@Basic(optional = false)
    @Lob
    @Column(name = "USER_APPLICATIONS")	
	private String userApplications;
	
	@Basic(optional = false)
    @Lob
    @Column(name = "PROJECT_APPLICATIONS")
	private String projectApplications;
	
	
	@Basic(optional = false)
    @Column(name = "CREATE_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createDate;
	
	@Basic(optional = false)
    @Column(name = "UPDATE_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateDate;
	
	public RearrangeApplicationsTbl() {
	}

	public RearrangeApplicationsTbl(String rearrangeId) {
		this.rearrangeId = rearrangeId;
	}

	/**
	 * @return the rearrangeId
	 */
	public String getRearrangeId() {
		return rearrangeId;
	}

	/**
	 * @param rearrangeId the rearrangeId to set
	 */
	public void setRearrangeId(String rearrangeId) {
		this.rearrangeId = rearrangeId;
	}

	/**
	 * @return the userId
	 */
	public UsersTbl getUserId() {
		return userId;
	}

	/**
	 * @param userId the userId to set
	 */
	public void setUserId(UsersTbl userId) {
		this.userId = userId;
	}

	/**
	 * @return the siteId
	 */
	public SitesTbl getSiteId() {
		return siteId;
	}

	/**
	 * @param siteId the siteId to set
	 */
	public void setSiteId(SitesTbl siteId) {
		this.siteId = siteId;
	}

	/**
	 * @return the projectId
	 */
	public ProjectsTbl getProjectId() {
		return projectId;
	}

	/**
	 * @param projectId the projectId to set
	 */
	public void setProjectId(ProjectsTbl projectId) {
		this.projectId = projectId;
	}

	/**
	 * @return the userApplications
	 */
	public String getUserApplications() {
		return userApplications;
	}

	/**
	 * @param userApplications the userApplications to set
	 */
	public void setUserApplications(String userApplications) {
		this.userApplications = userApplications;
	}

	/**
	 * @return the projectApplications
	 */
	public String getProjectApplications() {
		return projectApplications;
	}

	/**
	 * @param projectApplications the projectApplications to set
	 */
	public void setProjectApplications(String projectApplications) {
		this.projectApplications = projectApplications;
	}

	/**
	 * @return the createDate
	 */
	public Date getCreateDate() {
		return createDate;
	}

	/**
	 * @param createDate the createDate to set
	 */
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	/**
	 * @return the updateDate
	 */
	public Date getUpdateDate() {
		return updateDate;
	}

	/**
	 * @param updateDate the updateDate to set
	 */
	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}
	
	
	
}
