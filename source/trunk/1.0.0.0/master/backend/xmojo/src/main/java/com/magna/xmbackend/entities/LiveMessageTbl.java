/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magna.xmbackend.entities;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonBackReference;

/**
 *
 * @author Admin
 */
@Entity
@Table(name = "LIVE_MESSAGE_TBL")
@NamedQueries({
    @NamedQuery(name = "LiveMessageTbl.findAll", query = "SELECT l FROM LiveMessageTbl l")})
public class LiveMessageTbl implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "LIVE_MESSAGE_ID")
    private String liveMessageId;
    @Basic(optional = false)
    @Column(name = "LIVE_MESSAGE_NAME")
    private String liveMessageName;
    @Basic(optional = false)
    @Column(name = "ISPOPUP")
    private String ispopup;
    @Basic(optional = false)
    @Column(name = "SUBJECT")
    private String subject;
    @Column(name = "MESSAGE")
    private String message;
    @Basic(optional = false)
    @Column(name = "START_DATETIME")
    @Temporal(TemporalType.TIMESTAMP)
    private Date startDatetime;
    @Basic(optional = false)
    @Column(name = "END_DATETIME")
    @Temporal(TemporalType.TIMESTAMP)
    private Date endDatetime;
    @Basic(optional = false)
    @Column(name = "CREATE_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createDate;
    @Basic(optional = false)
    @Column(name = "UPDATE_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateDate;
    @JsonBackReference
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "liveMessageId")
    private Collection<LiveMessageStatusTbl> liveMessageStatusTblCollection;
    @JsonBackReference
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "liveMessageId")
    private Collection<LiveMessageConfigTbl> liveMessageConfigTblCollection;
    //@JsonBackReference
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "liveMessageId" )
    private Collection<LiveMessageTranslationTbl> liveMessageTranslationTbls;

    public LiveMessageTbl() {
    }

    public LiveMessageTbl(String liveMessageId) {
        this.liveMessageId = liveMessageId;
    }

    public LiveMessageTbl(String liveMessageId, String liveMessageName, String subject, String ispopup, Date startDatetime, Date endDatetime, Date createDate, Date updateDate) {
        this.liveMessageId = liveMessageId;
        this.liveMessageName = liveMessageName;
        this.subject = subject;
        this.ispopup = ispopup;
        this.startDatetime = startDatetime;
        this.endDatetime = endDatetime;
        this.createDate = createDate;
        this.updateDate = updateDate;
    }

    /**
	 * @return the subject
	 */
	public String getSubject() {
		return subject;
	}

	/**
	 * @param subject the subject to set
	 */
	public void setSubject(String subject) {
		this.subject = subject;
	}

	/**
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * @param message the message to set
	 */
	public void setMessage(String message) {
		this.message = message;
	}

	public String getLiveMessageId() {
        return liveMessageId;
    }

    public void setLiveMessageId(String liveMessageId) {
        this.liveMessageId = liveMessageId;
    }

    public String getLiveMessageName() {
        return liveMessageName;
    }

    public void setLiveMessageName(String liveMessageName) {
        this.liveMessageName = liveMessageName;
    }

    public String getIspopup() {
        return ispopup;
    }

    public void setIspopup(String ispopup) {
        this.ispopup = ispopup;
    }

    public Date getStartDatetime() {
        return startDatetime;
    }

    public void setStartDatetime(Date startDatetime) {
        this.startDatetime = startDatetime;
    }

    public Date getEndDatetime() {
        return endDatetime;
    }

    public void setEndDatetime(Date endDatetime) {
        this.endDatetime = endDatetime;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public Collection<LiveMessageStatusTbl> getLiveMessageStatusTblCollection() {
        return liveMessageStatusTblCollection;
    }

    public void setLiveMessageStatusTblCollection(Collection<LiveMessageStatusTbl> liveMessageStatusTblCollection) {
        this.liveMessageStatusTblCollection = liveMessageStatusTblCollection;
    }

    public Collection<LiveMessageConfigTbl> getLiveMessageConfigTblCollection() {
        return liveMessageConfigTblCollection;
    }

    public void setLiveMessageConfigTblCollection(Collection<LiveMessageConfigTbl> liveMessageConfigTblCollection) {
        this.liveMessageConfigTblCollection = liveMessageConfigTblCollection;
    }

    /**
	 * @return the liveMessageTranslationTbls
	 */
	public Collection<LiveMessageTranslationTbl> getLiveMessageTranslationTbls() {
		return liveMessageTranslationTbls;
	}

	/**
	 * @param liveMessageTranslationTbls the liveMessageTranslationTbls to set
	 */
	public void setLiveMessageTranslationTbls(Collection<LiveMessageTranslationTbl> liveMessageTranslationTbls) {
		this.liveMessageTranslationTbls = liveMessageTranslationTbls;
	}

	@Override
    public int hashCode() {
        int hash = 0;
        hash += (liveMessageId != null ? liveMessageId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof LiveMessageTbl)) {
            return false;
        }
        LiveMessageTbl other = (LiveMessageTbl) object;
        if ((this.liveMessageId == null && other.liveMessageId != null) || (this.liveMessageId != null && !this.liveMessageId.equals(other.liveMessageId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.magna.xmbackend.entities.LiveMessageTbl[ liveMessageId=" + liveMessageId + " ]";
    }
    
}
