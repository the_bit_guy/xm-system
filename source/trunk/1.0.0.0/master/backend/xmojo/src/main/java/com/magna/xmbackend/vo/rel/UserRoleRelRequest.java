package com.magna.xmbackend.vo.rel;

import java.util.Date;

/**
 *
 * @author vijay
 */
public class UserRoleRelRequest {

    private String userId;
    private String roleId;
    private Date createDate;
    private Date updateDate;

    public UserRoleRelRequest() {
    }

    /**
     *
     * @param userId
     * @param roleId
     * @param createDate
     * @param updateDate
     */
    public UserRoleRelRequest(String userId, String roleId,
            Date createDate, Date updateDate) {
        this.userId = userId;
        this.roleId = roleId;
        this.createDate = createDate;
        this.updateDate = updateDate;
    }

    /**
     * @return the userId
     */
    public final String getUserId() {
        return userId;
    }

    /**
     * @param userId the userId to set
     */
    public final void setUserId(final String userId) {
        this.userId = userId;
    }

    /**
     * @return the roleId
     */
    public final String getRoleId() {
        return roleId;
    }

    /**
     * @param roleId the roleId to set
     */
    public final void setRoleId(final String roleId) {
        this.roleId = roleId;
    }

    /**
     * @return the createDate
     */
    public final Date getCreateDate() {
        return createDate;
    }

    /**
     * @param createDate the createDate to set
     */
    public final void setCreateDate(final Date createDate) {
        this.createDate = createDate;
    }

    /**
     * @return the updateDate
     */
    public final Date getUpdateDate() {
        return updateDate;
    }

    /**
     * @param updateDate the updateDate to set
     */
    public final void setUpdateDate(final Date updateDate) {
        this.updateDate = updateDate;
    }

    /**
     *
     * @return object as string
     */
    @Override
    public final String toString() {
        return new StringBuilder().append("UserRoleRelRequest{")
                .append("userId=").append(getUserId())
                .append(", roleId=").append(getRoleId())
                .append(", createDate=").append(getCreateDate())
                .append(", updateDate=").append(getUpdateDate())
                .append("}").toString();
    }
}
