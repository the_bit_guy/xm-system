/**
 * 
 */
package com.magna.xmbackend.vo.group;

import com.magna.xmbackend.entities.ProjectApplicationsTbl;
import com.magna.xmbackend.entities.ProjectsTbl;
import com.magna.xmbackend.entities.UserApplicationsTbl;
import com.magna.xmbackend.entities.UsersTbl;

/**
 * @author Bhabadyuti Bal
 *
 */
public class GroupRelResponse {

	private String groupRefId;
	private UsersTbl usersTbl;
	private ProjectsTbl projectsTbl;
	private UserApplicationsTbl userApplicationsTbl;
	private ProjectApplicationsTbl projectApplicationsTbl;
	/**
	 * @return the groupRefId
	 */
	public String getGroupRefId() {
		return groupRefId;
	}
	/**
	 * @param groupRefId the groupRefId to set
	 */
	public void setGroupRefId(String groupRefId) {
		this.groupRefId = groupRefId;
	}
	/**
	 * @return the usersTbl
	 */
	public UsersTbl getUsersTbl() {
		return usersTbl;
	}
	/**
	 * @param usersTbl the usersTbl to set
	 */
	public void setUsersTbl(UsersTbl usersTbl) {
		this.usersTbl = usersTbl;
	}
	/**
	 * @return the projectsTbl
	 */
	public ProjectsTbl getProjectsTbl() {
		return projectsTbl;
	}
	/**
	 * @param projectsTbl the projectsTbl to set
	 */
	public void setProjectsTbl(ProjectsTbl projectsTbl) {
		this.projectsTbl = projectsTbl;
	}
	/**
	 * @return the userApplicationsTbl
	 */
	public UserApplicationsTbl getUserApplicationsTbl() {
		return userApplicationsTbl;
	}
	/**
	 * @param userApplicationsTbl the userApplicationsTbl to set
	 */
	public void setUserApplicationsTbl(UserApplicationsTbl userApplicationsTbl) {
		this.userApplicationsTbl = userApplicationsTbl;
	}
	/**
	 * @return the projectApplicationsTbl
	 */
	public ProjectApplicationsTbl getProjectApplicationsTbl() {
		return projectApplicationsTbl;
	}
	/**
	 * @param projectApplicationsTbl the projectApplicationsTbl to set
	 */
	public void setProjectApplicationsTbl(ProjectApplicationsTbl projectApplicationsTbl) {
		this.projectApplicationsTbl = projectApplicationsTbl;
	}
	
	
}
