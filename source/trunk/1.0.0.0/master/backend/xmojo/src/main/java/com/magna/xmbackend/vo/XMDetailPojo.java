package com.magna.xmbackend.vo;

import java.io.Serializable;

public class XMDetailPojo extends BaseObject implements Serializable {

    private String name;
    private String description;
    private String remarks;
    private String languageCode;

    private CreationInfo creationInfo;

    public XMDetailPojo() {
    }

    /**
     *
     * @param name
     * @param description
     * @param remarks
     * @param languageCode
     * @param creationInfo
     */
    public XMDetailPojo(String name, String description, String remarks,
            String languageCode, CreationInfo creationInfo) {
        this.name = name;
        this.description = description;
        this.remarks = remarks;
        this.languageCode = languageCode;
        this.creationInfo = creationInfo;
    }

    /**
     *
     * @param name
     * @param description
     * @param remarks
     * @param languageCode
     * @param creationInfo
     * @param xMObject
     */
    public XMDetailPojo(String name, String description, String remarks,
            String languageCode, CreationInfo creationInfo, XMObject xMObject) {
        super(xMObject);
        this.name = name;
        this.description = description;
        this.remarks = remarks;
        this.languageCode = languageCode;
        this.creationInfo = creationInfo;
    }

    /**
     * @return the name
     */
    public final String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public final void setName(final String name) {
        this.name = name;
    }

    /**
     * @return the description
     */
    public final String getDescription() {
        return description;
    }

    /**
     * @param description the description to set
     */
    public final void setDescription(final String description) {
        this.description = description;
    }

    /**
     * @return the remarks
     */
    public final String getRemarks() {
        return remarks;
    }

    /**
     * @param remarks the remarks to set
     */
    public final void setRemarks(final String remarks) {
        this.remarks = remarks;
    }

    /**
     * @return the languageCode
     */
    public final String getLanguageCode() {
        return languageCode;
    }

    /**
     * @param languageCode the languageCode to set
     */
    public final void setLanguageCode(final String languageCode) {
        this.languageCode = languageCode;
    }

    /**
     * @return the creationInfo
     */
    public final CreationInfo getCreationInfo() {
        return creationInfo;
    }

    /**
     * @param creationInfo the creationInfo to set
     */
    public final void setCreationInfo(final CreationInfo creationInfo) {
        this.creationInfo = creationInfo;
    }

    /**
     *
     * @return object as string
     */
    @Override
    public final String toString() {
        return new StringBuilder().append("XMDetailPojo{")
                .append("name=").append(name)
                .append(", description=").append(description)
                .append(", remarks=").append(remarks)
                .append(", languageCode=").append(languageCode)
                .append(", creationInfo=").append(creationInfo)
                .append("}").toString();
    }
}
