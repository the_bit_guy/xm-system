package com.magna.xmbackend.vo.rel;

import com.magna.xmbackend.entities.AdminAreaUserAppRelTbl;
import java.util.List;
import java.util.Map;

/**
 *
 * @author vijay
 */
public class AdminAreaUserAppRelBatchResponse {

    private List<AdminAreaUserAppRelTbl> adminAreaUserAppRelTbls;
    private List<Map<String, String>> statusMaps;
    private List<String> statusUpdatationFailedList;

    public AdminAreaUserAppRelBatchResponse() {
    }

    /**
     *
     * @param adminAreaUserAppRelTbls
     * @param statusMaps
     */
    public AdminAreaUserAppRelBatchResponse(List<AdminAreaUserAppRelTbl> adminAreaUserAppRelTbls,
            List<Map<String, String>> statusMaps) {
        this.adminAreaUserAppRelTbls = adminAreaUserAppRelTbls;
        this.statusMaps = statusMaps;
    }

    /**
     * @return the adminAreaUserAppRelTbls
     */
    public final List<AdminAreaUserAppRelTbl> getAdminAreaUserAppRelTbls() {
        return adminAreaUserAppRelTbls;
    }

    /**
     * @param adminAreaUserAppRelTbls the adminAreaStartAppRelTbls to set
     */
    public final void setAdminAreaUserAppRelTbls(final List<AdminAreaUserAppRelTbl> adminAreaUserAppRelTbls) {
        this.adminAreaUserAppRelTbls = adminAreaUserAppRelTbls;
    }

    /**
     * @return the statusMaps
     */
    public final List<Map<String, String>> getStatusMap() {
        return statusMaps;
    }

    /**
     * @param statusMaps the statusMap to set
     */
    public final void setStatusMap(final List<Map<String, String>> statusMaps) {
        this.statusMaps = statusMaps;
    }

	/**
	 * @return the statusMaps
	 */
	public List<Map<String, String>> getStatusMaps() {
		return statusMaps;
	}

	/**
	 * @param statusMaps the statusMaps to set
	 */
	public void setStatusMaps(List<Map<String, String>> statusMaps) {
		this.statusMaps = statusMaps;
	}

	/**
	 * @return the statusUpdatationFailedList
	 */
	public List<String> getStatusUpdatationFailedList() {
		return statusUpdatationFailedList;
	}

	/**
	 * @param statusUpdatationFailedList the statusUpdatationFailedList to set
	 */
	public void setStatusUpdatationFailedList(List<String> statusUpdatationFailedList) {
		this.statusUpdatationFailedList = statusUpdatationFailedList;
	}

}
