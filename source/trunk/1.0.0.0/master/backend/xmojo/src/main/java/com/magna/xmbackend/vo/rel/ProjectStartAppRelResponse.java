package com.magna.xmbackend.vo.rel;

import java.util.List;
import java.util.Map;

import com.magna.xmbackend.entities.ProjectStartAppRelTbl;

/**
 *
 * @author vijay
 */
public class ProjectStartAppRelResponse {

    private Iterable<ProjectStartAppRelTbl> projectStartAppRelTbls;
    private List<Map<String, String>> statusMaps;

    /**
	 * @return the statusMaps
	 */
	public List<Map<String, String>> getStatusMaps() {
		return statusMaps;
	}

	/**
	 * @param statusMaps the statusMaps to set
	 */
	public void setStatusMaps(List<Map<String, String>> statusMaps) {
		this.statusMaps = statusMaps;
	}

	/**
	 * @param statusMaps
	 */
	public ProjectStartAppRelResponse(List<Map<String, String>> statusMaps) {
		this.statusMaps = statusMaps;
	}

	public ProjectStartAppRelResponse() {
    }

    /**
     *
     * @param projectStartAppRelTbl
     */
    public ProjectStartAppRelResponse(final Iterable<ProjectStartAppRelTbl> projectStartAppRelTbl) {
        this.projectStartAppRelTbls = projectStartAppRelTbl;
    }

    /**
     * @return the projectStartAppRelTbls
     */
    public final Iterable<ProjectStartAppRelTbl> getProjectStartAppRelTbls() {
        return projectStartAppRelTbls;
    }

    /**
     * @param projectStartAppRelTbls the projectStartAppRelTbls to set
     */
    public final void setProjectStartAppRelTbls(final Iterable<ProjectStartAppRelTbl> projectStartAppRelTbls) {
        this.projectStartAppRelTbls = projectStartAppRelTbls;
    }

    /**
     *
     * @return object as string
     */
    @Override
    public final String toString() {
        return new StringBuilder().append("ProjectStartAppRelResponse{")
                .append("projectStartAppRelTbls=").append(projectStartAppRelTbls)
                .append("}").toString();
    }
}
