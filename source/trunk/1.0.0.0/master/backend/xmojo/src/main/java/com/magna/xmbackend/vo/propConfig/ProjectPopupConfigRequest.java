/**
 * 
 */
package com.magna.xmbackend.vo.propConfig;

import java.util.EnumMap;

import com.magna.xmbackend.vo.enums.ProjectPopupKey;

/**
 * @author Bhabadyuti Bal
 *
 */
public class ProjectPopupConfigRequest {
	
	private EnumMap<ProjectPopupKey, String> projectPopupKeyValue;
	
	public ProjectPopupConfigRequest() {
	}

	/**
	 * @param projectPopupKeyValue
	 */
	public ProjectPopupConfigRequest(EnumMap<ProjectPopupKey, String> projectPopupKeyValue) {
		this.projectPopupKeyValue = projectPopupKeyValue;
	}

	/**
	 * @return the projectPopupKeyValue
	 */
	public EnumMap<ProjectPopupKey, String> getProjectPopupKeyValue() {
		return projectPopupKeyValue;
	}

	/**
	 * @param projectPopupKeyValue the projectPopupKeyValue to set
	 */
	public void setProjectPopupKeyValue(EnumMap<ProjectPopupKey, String> projectPopupKeyValue) {
		this.projectPopupKeyValue = projectPopupKeyValue;
	}

}
