/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magna.xmbackend.entities;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonBackReference;

/**
 *
 * @author Admin
 */
@Entity
@Table(name = "PROJECT_APPLICATIONS_TBL")
@NamedQueries({
    @NamedQuery(name = "ProjectApplicationsTbl.findAll", query = "SELECT p FROM ProjectApplicationsTbl p")})
public class ProjectApplicationsTbl implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "PROJECT_APPLICATION_ID")
    private String projectApplicationId;
    @Basic(optional = false)
    @Column(name = "NAME")
    private String name;
    @Column(name = "DESCRIPTION")
    private String description;
    @Basic(optional = false)
    @Column(name = "STATUS")
    private String status;
    @Column(name = "IS_PARENT")
    private String isParent;
    @Column(name = "IS_SINGLETON")
    private String isSingleton;
    @Column(name = "POSITION")
    private String position;
    @Basic(optional = false)
    @Column(name = "CREATE_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createDate;
    @Basic(optional = false)
    @Column(name = "UPDATE_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateDate;
    @JsonBackReference
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "projectApplicationId")
    private Collection<AdminAreaProjAppRelTbl> adminAreaProjAppRelTblCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "projectApplicationId")
    private Collection<ProjectAppTranslationTbl> projectAppTranslationTblCollection;
    @JsonBackReference
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "projectApplicationId")
    private Collection<UserProjAppRelTbl> userProjAppRelTblCollection;
    @JoinColumn(name = "BASE_APPLICATION_ID", referencedColumnName = "BASE_APPLICATION_ID")
    @ManyToOne
    private BaseApplicationsTbl baseApplicationId;
    @JoinColumn(name = "ICON_ID", referencedColumnName = "ICON_ID")
    @ManyToOne
    private IconsTbl iconId;

    public ProjectApplicationsTbl() {
    }

    public ProjectApplicationsTbl(String projectApplicationId) {
        this.projectApplicationId = projectApplicationId;
    }

    public ProjectApplicationsTbl(String projectApplicationId, String name, String status, Date createDate, Date updateDate) {
        this.projectApplicationId = projectApplicationId;
        this.name = name;
        this.status = status;
        this.createDate = createDate;
        this.updateDate = updateDate;
    }

    public String getProjectApplicationId() {
        return projectApplicationId;
    }

    public void setProjectApplicationId(String projectApplicationId) {
        this.projectApplicationId = projectApplicationId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getIsParent() {
        return isParent;
    }

    public void setIsParent(String isParent) {
        this.isParent = isParent;
    }

    public String getIsSingleton() {
        return isSingleton;
    }

    public void setIsSingleton(String isSingleton) {
        this.isSingleton = isSingleton;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public Collection<AdminAreaProjAppRelTbl> getAdminAreaProjAppRelTblCollection() {
        return adminAreaProjAppRelTblCollection;
    }

    public void setAdminAreaProjAppRelTblCollection(Collection<AdminAreaProjAppRelTbl> adminAreaProjAppRelTblCollection) {
        this.adminAreaProjAppRelTblCollection = adminAreaProjAppRelTblCollection;
    }

    public Collection<ProjectAppTranslationTbl> getProjectAppTranslationTblCollection() {
        return projectAppTranslationTblCollection;
    }

    public void setProjectAppTranslationTblCollection(Collection<ProjectAppTranslationTbl> projectAppTranslationTblCollection) {
        this.projectAppTranslationTblCollection = projectAppTranslationTblCollection;
    }

    public Collection<UserProjAppRelTbl> getUserProjAppRelTblCollection() {
        return userProjAppRelTblCollection;
    }

    public void setUserProjAppRelTblCollection(Collection<UserProjAppRelTbl> userProjAppRelTblCollection) {
        this.userProjAppRelTblCollection = userProjAppRelTblCollection;
    }

    public BaseApplicationsTbl getBaseApplicationId() {
        return baseApplicationId;
    }

    public void setBaseApplicationId(BaseApplicationsTbl baseApplicationId) {
        this.baseApplicationId = baseApplicationId;
    }

    public IconsTbl getIconId() {
        return iconId;
    }

    public void setIconId(IconsTbl iconId) {
        this.iconId = iconId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (projectApplicationId != null ? projectApplicationId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ProjectApplicationsTbl)) {
            return false;
        }
        ProjectApplicationsTbl other = (ProjectApplicationsTbl) object;
        if ((this.projectApplicationId == null && other.projectApplicationId != null) || (this.projectApplicationId != null && !this.projectApplicationId.equals(other.projectApplicationId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.magna.xmbackend.entities.ProjectApplicationsTbl[ projectApplicationId=" + projectApplicationId + " ]";
    }
    
}
