package com.magna.xmbackend.vo.icon;

import com.magna.xmbackend.entities.IconsTbl;
import java.io.Serializable;

public class IkonResponse implements Serializable {

    private Iterable<IconsTbl> iconsTbls;

    public IkonResponse() {
    }

    public IkonResponse(Iterable<IconsTbl> iconsTbls) {
        this.iconsTbls = iconsTbls;
    }

    public void setIconsTbls(Iterable<IconsTbl> iconsTbls) {
        this.iconsTbls = iconsTbls;
    }

    public Iterable<IconsTbl> getIconsTbls() {
        return iconsTbls;
    }

}
