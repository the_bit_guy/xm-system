/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magna.xmbackend.vo.project;

import com.magna.xmbackend.vo.CreationInfo;
import java.util.List;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author dhana
 */
public class ProjectRequest {

    private String id;
    @NotNull(message = "project name is required")
	@Size(min = 1, max = 30, message = "project name size should be more than 1 & less than 30 charachters")
    private String name;
    private String iconId;
    private String status;
    private CreationInfo creationInfo;

    private List<ProjectTranslation> projectTranslations;

    public ProjectRequest() {
    }

    public ProjectRequest(String id, String name, String iconId, String status, CreationInfo creationInfo, List<ProjectTranslation> projectTranslations) {
        this.id = id;
        this.name = name;
        this.iconId = iconId;
        this.status = status;
        this.creationInfo = creationInfo;
        this.projectTranslations = projectTranslations;
    }

    /**
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
     * @return the iconId
     */
    public String getIconId() {
        return iconId;
    }

    /**
     * @param iconId the iconId to set
     */
    public void setIconId(String iconId) {
        this.iconId = iconId;
    }

    /**
     * @return the status
     */
    public String getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * @return the creationInfo
     */
    public CreationInfo getCreationInfo() {
        return creationInfo;
    }

    /**
     * @param creationInfo the creationInfo to set
     */
    public void setCreationInfo(CreationInfo creationInfo) {
        this.creationInfo = creationInfo;
    }

    /**
     * @return the projectTranslations
     */
    public List<ProjectTranslation> getProjectTranslations() {
        return projectTranslations;
    }

    /**
     * @param projectTranslations the projectTranslations to set
     */
    public void setProjectTranslations(List<ProjectTranslation> projectTranslations) {
        this.projectTranslations = projectTranslations;
    }

}
