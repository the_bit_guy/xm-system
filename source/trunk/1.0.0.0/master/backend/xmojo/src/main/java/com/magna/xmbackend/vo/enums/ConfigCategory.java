package com.magna.xmbackend.vo.enums;

import java.io.Serializable;

/**
 * @author Bhabadyuti Bal
 *
 */
public enum ConfigCategory implements Serializable {
	LDAP, SMTP, SINGLETON, PROJECTPOPUP, PROJECTEXPIRYDAYS
}
