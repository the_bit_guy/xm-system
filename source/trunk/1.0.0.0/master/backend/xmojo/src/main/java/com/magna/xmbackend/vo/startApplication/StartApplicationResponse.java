/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magna.xmbackend.vo.startApplication;

import java.util.List;
import java.util.Map;

import com.magna.xmbackend.entities.StartApplicationsTbl;

/**
 *
 * @author dhana
 */
public class StartApplicationResponse {

    private Iterable<StartApplicationsTbl> startApplicationsTbls;
    private List<Map<String, String>> statusMaps;
    private List<String> statusUpdatationFailedList;

    public StartApplicationResponse() {
    }
    
    

    /**
	 * @return the statusMaps
	 */
	public List<Map<String, String>> getStatusMaps() {
		return statusMaps;
	}



	/**
	 * @param statusMaps the statusMaps to set
	 */
	public void setStatusMaps(List<Map<String, String>> statusMaps) {
		this.statusMaps = statusMaps;
	}



	/**
	 * @param statusMaps
	 */
	public StartApplicationResponse(List<Map<String, String>> statusMaps) {
		this.statusMaps = statusMaps;
	}



	public StartApplicationResponse(Iterable<StartApplicationsTbl> startApplicationsTbls) {
        this.startApplicationsTbls = startApplicationsTbls;
    }

    public void setStartApplicationsTbls(Iterable<StartApplicationsTbl> startApplicationsTbls) {
        this.startApplicationsTbls = startApplicationsTbls;
    }

    public Iterable<StartApplicationsTbl> getStartApplicationsTbls() {
        return startApplicationsTbls;
    }



	/**
	 * @return the statusUpdatationFailedList
	 */
	public List<String> getStatusUpdatationFailedList() {
		return statusUpdatationFailedList;
	}



	/**
	 * @param statusUpdatationFailedList the statusUpdatationFailedList to set
	 */
	public void setStatusUpdatationFailedList(List<String> statusUpdatationFailedList) {
		this.statusUpdatationFailedList = statusUpdatationFailedList;
	}

}
