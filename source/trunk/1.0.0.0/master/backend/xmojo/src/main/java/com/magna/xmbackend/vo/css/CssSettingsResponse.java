/**
 * 
 */
package com.magna.xmbackend.vo.css;

import com.magna.xmbackend.entities.CssSettingsTbl;

/**
 * @author Bhabadyuti Bal
 *
 */
public class CssSettingsResponse {

	private CssSettingsTbl cssSettingsTbl;
	
	public CssSettingsResponse() {
	}

	public CssSettingsResponse(CssSettingsTbl cssSettingsTbl) {
		this.cssSettingsTbl = cssSettingsTbl;
	}

	/**
	 * @return the cssSettingsTbl
	 */
	public CssSettingsTbl getCssSettingsTbl() {
		return cssSettingsTbl;
	}

	/**
	 * @param cssSettingsTbl the cssSettingsTbl to set
	 */
	public void setCssSettingsTbl(CssSettingsTbl cssSettingsTbl) {
		this.cssSettingsTbl = cssSettingsTbl;
	}

	
	
	
}
