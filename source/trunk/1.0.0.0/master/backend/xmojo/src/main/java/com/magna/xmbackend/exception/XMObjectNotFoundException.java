package com.magna.xmbackend.exception;

/**
 *
 * @author dhana
 */
public class XMObjectNotFoundException extends RuntimeException {

    private String errCode;
    private String[] param;

    /**
     *
     */
    public XMObjectNotFoundException() {
        super();
    }

    /**
     *
     * @param message
     * @param errCode
     */
    public XMObjectNotFoundException(String message, String errCode) {
        super(message);
        this.errCode = errCode;
    }

    /**
     *
     * @param message
     * @param errCode
     * @param param
     */
    public XMObjectNotFoundException(String message, String errCode, String[] param) {
        super(message);
        this.errCode = errCode;
        this.param = param;
    }

    /**
     *
     * @return String
     */
    @Override
    public String toString() {
        return super.toString();
    }

    /**
     *
     * @return String
     */
    @Override
    public String getMessage() {
        return super.getMessage() + " with ErrorCode :" + this.errCode;
    }

    /**
     *
     * @return errCode
     */
    public String getErrCode() {
        return this.errCode;
    }

    /**
     * @return the param
     */
    public String[] getParam() {
        return param;
    }
}
