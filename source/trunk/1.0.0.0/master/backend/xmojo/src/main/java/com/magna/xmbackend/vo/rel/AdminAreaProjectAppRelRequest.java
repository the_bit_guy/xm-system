package com.magna.xmbackend.vo.rel;

/**
 *
 * @author vijay
 */
public class AdminAreaProjectAppRelRequest {

	private String id;
    private String adminAreaProjectRelId;
    private String projectAppId;
    private String status;
    private String relationType;

    public AdminAreaProjectAppRelRequest() {
    }

    public AdminAreaProjectAppRelRequest(String adminAreaProjectRelId,
            String projectAppId, String status, String relationType) {
        this.adminAreaProjectRelId = adminAreaProjectRelId;
        this.projectAppId = projectAppId;
        this.status = status;
        this.relationType = relationType;
    }

    
    /**
	 * @param id
	 * @param adminAreaProjectRelId
	 * @param projectAppId
	 * @param status
	 * @param relationType
	 */
	public AdminAreaProjectAppRelRequest(String id, String adminAreaProjectRelId, String projectAppId, String status,
			String relationType) {
		this.id = id;
		this.adminAreaProjectRelId = adminAreaProjectRelId;
		this.projectAppId = projectAppId;
		this.status = status;
		this.relationType = relationType;
	}

	
	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
     * @return the adminAreaProjectRelId
     */
    public final String getAdminAreaProjectRelId() {
        return adminAreaProjectRelId;
    }

    /**
     * @param adminAreaProjectRelId the adminAreaProjectRelId to set
     */
    public final void setAdminAreaProjectRelId(final String adminAreaProjectRelId) {
        this.adminAreaProjectRelId = adminAreaProjectRelId;
    }

    /**
     * @return the projectAppId
     */
    public final String getProjectAppId() {
        return projectAppId;
    }

    /**
     * @param projectAppId the projectAppId to set
     */
    public final void setProjectAppId(final String projectAppId) {
        this.projectAppId = projectAppId;
    }

    /**
     * @return the status
     */
    public final String getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public final void setStatus(final String status) {
        this.status = status;
    }

    /**
     * @return the relationType
     */
    public final String getRelationType() {
        return relationType;
    }

    /**
     * @param relationType the relationType to set
     */
    public final void setRelationType(final String relationType) {
        this.relationType = relationType;
    }

    /**
     *
     * @return object as string
     */
    @Override
    public final String toString() {
        return new StringBuilder().append("AdminAreaProjectAppRelRequest{")
                .append("adminAreaProjectRelId=").append(getAdminAreaProjectRelId())
                .append(", projectAppId=").append(getProjectAppId())
                .append(", status=").append(getStatus())
                .append(", relationType=").append(getRelationType())
                .append("}").toString();
    }

}
