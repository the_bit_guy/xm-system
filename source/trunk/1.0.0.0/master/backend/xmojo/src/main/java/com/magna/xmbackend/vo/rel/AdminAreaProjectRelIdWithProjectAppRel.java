package com.magna.xmbackend.vo.rel;

import com.magna.xmbackend.entities.AdminAreaProjectRelTbl;
import com.magna.xmbackend.entities.ProjectApplicationsTbl;

/**
 *
 * @author vijay
 */
public class AdminAreaProjectRelIdWithProjectAppRel {

    private String adminAreaProjectAppRelId;
    private AdminAreaProjectRelTbl adminAreaProjectRelId;
    private ProjectApplicationsTbl projectApplicationsTbl;
    private String status;
    private String relationType;

    public AdminAreaProjectRelIdWithProjectAppRel() {
    }

    /**
     *
     * @param adminAreaProjectRelId
     * @param projectApplicationsTbl
     * @param adminAreaProjectAppRelId
     * @param status
     * @param relationType
     */
    public AdminAreaProjectRelIdWithProjectAppRel(AdminAreaProjectRelTbl adminAreaProjectRelId,
            ProjectApplicationsTbl projectApplicationsTbl,
            String adminAreaProjectAppRelId, String status, String relationType) {
        this.adminAreaProjectRelId = adminAreaProjectRelId;
        this.projectApplicationsTbl = projectApplicationsTbl;
        this.adminAreaProjectAppRelId = adminAreaProjectAppRelId;
        this.status = status;
        this.relationType = relationType;
    }

    /**
     * @return the adminAreaProjectRelId
     */
    public final AdminAreaProjectRelTbl getAdminAreaProjectRelId() {
        return adminAreaProjectRelId;
    }

    /**
     * @param adminAreaProjectRelId the adminAreaProjectRelId to set
     */
    public final void setAdminAreaProjectRelId(final AdminAreaProjectRelTbl adminAreaProjectRelId) {
        this.adminAreaProjectRelId = adminAreaProjectRelId;
    }

    /**
     * @return the projectApplicationsTbl
     */
    public final ProjectApplicationsTbl getProjectApplicationsTbl() {
        return projectApplicationsTbl;
    }

    /**
     * @param projectApplicationsTbl the projectApplicationsTbl to set
     */
    public final void setProjectApplicationsTbl(final ProjectApplicationsTbl projectApplicationsTbl) {
        this.projectApplicationsTbl = projectApplicationsTbl;
    }

    /**
     * @return the adminAreaProjectAppRelId
     */
    public final String getAdminAreaProjectAppRelId() {
        return adminAreaProjectAppRelId;
    }

    /**
     * @param adminAreaProjectAppRelId the adminAreaProjectAppRelId to set
     */
    public final void setAdminAreaProjectAppRelId(final String adminAreaProjectAppRelId) {
        this.adminAreaProjectAppRelId = adminAreaProjectAppRelId;
    }

    /**
     * @return the status
     */
    public final String getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public final void setStatus(String status) {
        this.status = status;
    }

    /**
     * @return the relationType
     */
    public String getRelationType() {
        return relationType;
    }

    /**
     * @param relationType the relationType to set
     */
    public void setRelationType(String relationType) {
        this.relationType = relationType;
    }
}
