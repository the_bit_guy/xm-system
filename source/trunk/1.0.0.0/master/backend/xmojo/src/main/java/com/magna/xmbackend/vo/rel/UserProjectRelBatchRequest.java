package com.magna.xmbackend.vo.rel;

import java.util.List;

/**
 *
 * @author vijay
 */
public class UserProjectRelBatchRequest {

    private List<UserProjectRelRequest> userProjectRelRequests;

    public UserProjectRelBatchRequest() {
    }

    /**
     *
     * @param userProjectRelRequests
     */
    public UserProjectRelBatchRequest(List<UserProjectRelRequest> userProjectRelRequests) {
        this.userProjectRelRequests = userProjectRelRequests;
    }

    /**
     *
     * @return List
     */
    public final List<UserProjectRelRequest> getUserProjectRelRequests() {
        return userProjectRelRequests;
    }

    /**
     *
     * @param userProjectRelRequests
     */
    public final void setUserProjectRelRequests(final List<UserProjectRelRequest> userProjectRelRequests) {
        this.userProjectRelRequests = userProjectRelRequests;
    }

}
