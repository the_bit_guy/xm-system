/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magna.xmbackend.vo.jpa.site;

import java.util.List;

/**
 *
 * @author dana
 */
public class SiteAdminAreaRelIdWithAdminAreaResponse {

    private List<SiteAdminAreaRelIdWithAdminArea> areaRelIdWithAdminAreas;

    public SiteAdminAreaRelIdWithAdminAreaResponse() {
    }

    public SiteAdminAreaRelIdWithAdminAreaResponse(List<SiteAdminAreaRelIdWithAdminArea> areaRelIdWithAdminAreas) {
        this.areaRelIdWithAdminAreas = areaRelIdWithAdminAreas;
    }

    public List<SiteAdminAreaRelIdWithAdminArea> getAreaRelIdWithAdminAreas() {
        return areaRelIdWithAdminAreas;
    }

    public void setAreaRelIdWithAdminAreas(List<SiteAdminAreaRelIdWithAdminArea> areaRelIdWithAdminAreas) {
        this.areaRelIdWithAdminAreas = areaRelIdWithAdminAreas;
    }

}
