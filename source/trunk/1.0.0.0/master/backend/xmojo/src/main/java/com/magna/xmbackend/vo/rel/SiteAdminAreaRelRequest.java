package com.magna.xmbackend.vo.rel;

/**
 *
 * @author dhana
 */
public class SiteAdminAreaRelRequest {

	private String id;
    private String siteId;
    private String adminAreaId;
    private String status;

    public SiteAdminAreaRelRequest() {
    }

    /**
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
     *
     * @param siteId
     * @param adminAreaId
     * @param status
     */
    public SiteAdminAreaRelRequest(String siteId, String adminAreaId, String status) {
        this.siteId = siteId;
        this.adminAreaId = adminAreaId;
        this.status = status;
    }

    /**
     * @return the siteId
     */
    public String getSiteId() {
        return siteId;
    }

    /**
     * @param siteId the siteId to set
     */
    public void setSiteId(String siteId) {
        this.siteId = siteId;
    }

    /**
     * @return the adminAreaId
     */
    public String getAdminAreaId() {
        return adminAreaId;
    }

    /**
     * @param adminAreaId the adminAreaId to set
     */
    public void setAdminAreaId(String adminAreaId) {
        this.adminAreaId = adminAreaId;
    }

    /**
     * @return the status
     */
    public String getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     *
     * @return object as string
     */
    @Override
    public final String toString() {
        return new StringBuilder().append("SiteAdminAreaRelRequest{")
                .append("siteId=").append(siteId)
                .append(", adminAreaId=").append(adminAreaId)
                .append(", status=").append(status)
                .append("}").toString();
    }
}
