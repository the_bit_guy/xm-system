/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magna.xmbackend.vo;

import com.magna.xmbackend.vo.enums.Status;
import com.magna.xmbackend.vo.icon.Icon;

/**
 *
 * @author dhana
 */
public class Project {

    private Status status;//can be ACTIVE, INACTIVE, DELETED
    private Icon icon;

    /**
     * @return the status
     */
    public Status getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(Status status) {
        this.status = status;
    }

    /**
     * @return the icon
     */
    public Icon getIcon() {
        return icon;
    }

    /**
     * @param icon the icon to set
     */
    public void setIcon(Icon icon) {
        this.icon = icon;
    }
}
