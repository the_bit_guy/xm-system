/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magna.xmbackend.vo.baseApplication;

import java.util.List;
import java.util.Map;

import com.magna.xmbackend.entities.BaseApplicationsTbl;

/**
 *
 * @author dhana
 */
public class BaseApplicationResponse {

    private Iterable<BaseApplicationsTbl> baseApplicationsTbls;
    private List<Map<String, String>> statusMaps;
    private List<String> statusUpdatationFailedList;

    public BaseApplicationResponse() {
    }

    
    
    /**
	 * @param statusMaps
	 */
	public BaseApplicationResponse(List<Map<String, String>> statusMaps) {
		this.statusMaps = statusMaps;
	}



	/**
	 * @return the statusMaps
	 */
	public List<Map<String, String>> getStatusMaps() {
		return statusMaps;
	}



	/**
	 * @param statusMaps the statusMaps to set
	 */
	public void setStatusMaps(List<Map<String, String>> statusMaps) {
		this.statusMaps = statusMaps;
	}



	public BaseApplicationResponse(Iterable<BaseApplicationsTbl> baseApplicationsTbls) {
        this.baseApplicationsTbls = baseApplicationsTbls;
    }

    public void setBaseApplicationsTbls(Iterable<BaseApplicationsTbl> baseApplicationsTbls) {
        this.baseApplicationsTbls = baseApplicationsTbls;
    }

    public Iterable<BaseApplicationsTbl> getBaseApplicationsTbls() {
        return baseApplicationsTbls;
    }



	/**
	 * @return the statusUpdatationFailedList
	 */
	public List<String> getStatusUpdatationFailedList() {
		return statusUpdatationFailedList;
	}



	/**
	 * @param statusUpdatationFailedList the statusUpdatationFailedList to set
	 */
	public void setStatusUpdatationFailedList(List<String> statusUpdatationFailedList) {
		this.statusUpdatationFailedList = statusUpdatationFailedList;
	}

}
