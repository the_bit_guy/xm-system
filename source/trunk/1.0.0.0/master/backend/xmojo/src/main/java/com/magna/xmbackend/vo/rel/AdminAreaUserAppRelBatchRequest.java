package com.magna.xmbackend.vo.rel;

import java.util.List;

/**
 *
 * @author vijay
 */
public class AdminAreaUserAppRelBatchRequest {

    private List<AdminAreaUserAppRelRequest> adminAreaUserAppRelRequests;

    public AdminAreaUserAppRelBatchRequest() {
    }

    /**
     *
     * @param adminAreaUserAppRelRequests
     */
    public AdminAreaUserAppRelBatchRequest(List<AdminAreaUserAppRelRequest> adminAreaUserAppRelRequests) {
        this.adminAreaUserAppRelRequests = adminAreaUserAppRelRequests;
    }

    /**
     *
     * @return List
     */
    public final List<AdminAreaUserAppRelRequest> getAdminAreaUserAppRelRequests() {
        return adminAreaUserAppRelRequests;
    }

    /**
     *
     * @param adminAreaUserAppRelRequests
     */
    public final void setAdminAreaUserAppRelRequests(final List<AdminAreaUserAppRelRequest> adminAreaUserAppRelRequests) {
        this.adminAreaUserAppRelRequests = adminAreaUserAppRelRequests;
    }

}
