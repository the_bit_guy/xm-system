package com.magna.xmbackend.vo.user;

import com.magna.xmbackend.vo.CreationInfo;

/**
 * The Class UserTranslation.
 * 
 * @author shashwat.anand
 */
public class UserTranslation {
	
	/** The id. */
	private String id;
	
	/** The description. */
	private String description;
	
	/** The remarks. */
	private String remarks;

	/**
	 * Instantiates a new user translation.
	 */
	public UserTranslation() {
	}

	/**
	 * Instantiates a new user translation.
	 *
	 * @param id the id
	 * @param description the description
	 * @param remarks the remarks
	 * @param languageCode the language code
	 * @param creationInfo the creation info
	 */
	public UserTranslation(String id, String description, String remarks, String languageCode,
			CreationInfo creationInfo) {
		super();
		this.id = id;
		this.description = description;
		this.remarks = remarks;
		this.languageCode = languageCode;
		this.creationInfo = creationInfo;
	}

	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * Sets the id.
	 *
	 * @param id            the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * Gets the description.
	 *
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Sets the description.
	 *
	 * @param description            the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * Gets the remarks.
	 *
	 * @return the remarks
	 */
	public String getRemarks() {
		return remarks;
	}

	/**
	 * Sets the remarks.
	 *
	 * @param remarks            the remarks to set
	 */
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	/**
	 * Gets the language code.
	 *
	 * @return the languageCode
	 */
	public String getLanguageCode() {
		return languageCode;
	}

	/**
	 * Sets the language code.
	 *
	 * @param languageCode            the languageCode to set
	 */
	public void setLanguageCode(String languageCode) {
		this.languageCode = languageCode;
	}

	/**
	 * Gets the creation info.
	 *
	 * @return the creationInfo
	 */
	public CreationInfo getCreationInfo() {
		return creationInfo;
	}

	/**
	 * Sets the creation info.
	 *
	 * @param creationInfo            the creationInfo to set
	 */
	public void setCreationInfo(CreationInfo creationInfo) {
		this.creationInfo = creationInfo;
	}

	/** The language code. */
	private String languageCode;
	
	/** The creation info. */
	private CreationInfo creationInfo;
}
