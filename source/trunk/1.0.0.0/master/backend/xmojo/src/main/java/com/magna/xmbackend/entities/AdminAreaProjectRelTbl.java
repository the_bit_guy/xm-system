/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magna.xmbackend.entities;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;

/**
 *
 * @author Admin
 */
@Entity
@Table(name = "ADMIN_AREA_PROJECT_REL_TBL")
@NamedQueries({
    @NamedQuery(name = "AdminAreaProjectRelTbl.findAll", query = "SELECT a FROM AdminAreaProjectRelTbl a")})
public class AdminAreaProjectRelTbl implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "ADMIN_AREA_PROJECT_REL_ID")
    private String adminAreaProjectRelId;
    @Basic(optional = false)
    @Column(name = "STATUS")
    private String status;
    @JsonBackReference
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "adminAreaProjectRelId")
    private Collection<AdminAreaProjAppRelTbl> adminAreaProjAppRelTblCollection;
    @JoinColumn(name = "PROJECT_ID", referencedColumnName = "PROJECT_ID")
    @ManyToOne(optional = false)
    private ProjectsTbl projectId;
    @JoinColumn(name = "SITE_ADMIN_AREA_REL_ID", referencedColumnName = "SITE_ADMIN_AREA_REL_ID")
    @ManyToOne(optional = false)
    private SiteAdminAreaRelTbl siteAdminAreaRelId;
    @JsonBackReference
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "adminAreaProjectRelId")
    private Collection<ProjectStartAppRelTbl> projectStartAppRelTblCollection;
    @JsonBackReference
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "adminAreaProjectRelId")
    private Collection<UserProjAppRelTbl> userProjAppRelTblCollection;

    public AdminAreaProjectRelTbl() {
    }

    public AdminAreaProjectRelTbl(String adminAreaProjectRelId) {
        this.adminAreaProjectRelId = adminAreaProjectRelId;
    }

    public AdminAreaProjectRelTbl(String adminAreaProjectRelId, String status) {
        this.adminAreaProjectRelId = adminAreaProjectRelId;
        this.status = status;
    }

    public String getAdminAreaProjectRelId() {
        return adminAreaProjectRelId;
    }

    public void setAdminAreaProjectRelId(String adminAreaProjectRelId) {
        this.adminAreaProjectRelId = adminAreaProjectRelId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Collection<AdminAreaProjAppRelTbl> getAdminAreaProjAppRelTblCollection() {
        return adminAreaProjAppRelTblCollection;
    }

    public void setAdminAreaProjAppRelTblCollection(Collection<AdminAreaProjAppRelTbl> adminAreaProjAppRelTblCollection) {
        this.adminAreaProjAppRelTblCollection = adminAreaProjAppRelTblCollection;
    }

    public ProjectsTbl getProjectId() {
        return projectId;
    }

    public void setProjectId(ProjectsTbl projectId) {
        this.projectId = projectId;
    }

    public SiteAdminAreaRelTbl getSiteAdminAreaRelId() {
        return siteAdminAreaRelId;
    }

    public void setSiteAdminAreaRelId(SiteAdminAreaRelTbl siteAdminAreaRelId) {
        this.siteAdminAreaRelId = siteAdminAreaRelId;
    }

    public Collection<ProjectStartAppRelTbl> getProjectStartAppRelTblCollection() {
        return projectStartAppRelTblCollection;
    }

    public void setProjectStartAppRelTblCollection(Collection<ProjectStartAppRelTbl> projectStartAppRelTblCollection) {
        this.projectStartAppRelTblCollection = projectStartAppRelTblCollection;
    }

    public Collection<UserProjAppRelTbl> getUserProjAppRelTblCollection() {
        return userProjAppRelTblCollection;
    }

    public void setUserProjAppRelTblCollection(Collection<UserProjAppRelTbl> userProjAppRelTblCollection) {
        this.userProjAppRelTblCollection = userProjAppRelTblCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (adminAreaProjectRelId != null ? adminAreaProjectRelId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof AdminAreaProjectRelTbl)) {
            return false;
        }
        AdminAreaProjectRelTbl other = (AdminAreaProjectRelTbl) object;
        if ((this.adminAreaProjectRelId == null && other.adminAreaProjectRelId != null) || (this.adminAreaProjectRelId != null && !this.adminAreaProjectRelId.equals(other.adminAreaProjectRelId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.magna.xmbackend.entities.AdminAreaProjectRelTbl[ adminAreaProjectRelId=" + adminAreaProjectRelId + " ]";
    }
    
}
