package com.magna.xmbackend.response.rel.adminareaprojectapp;

import java.util.List;

/**
 *
 * @author dhana
 */
public class AdminAreaProjectAppResponseWrapper {

    private List<AdminAreaProjectAppResponse> adminAreaProjectAppResponse;
    
    public AdminAreaProjectAppResponseWrapper() {
    }
    
    public AdminAreaProjectAppResponseWrapper(List<AdminAreaProjectAppResponse> adminAreaProjectAppResponse) {
    	this.adminAreaProjectAppResponse = adminAreaProjectAppResponse;
    }

	/**
	 * @return the adminAreaProjectAppResponse
	 */
	public List<AdminAreaProjectAppResponse> getAdminAreaProjectAppResponse() {
		return adminAreaProjectAppResponse;
	}

	/**
	 * @param adminAreaProjectAppResponse the adminAreaProjectAppResponse to set
	 */
	public void setAdminAreaProjectAppResponse(List<AdminAreaProjectAppResponse> adminAreaProjectAppResponse) {
		this.adminAreaProjectAppResponse = adminAreaProjectAppResponse;
	}
}
