package com.magna.xmbackend.vo.rel;

import java.util.List;

/**
 *
 * @author vijay
 */
public class UserUserAppRelBatchRequest {

    private List<UserUserAppRelRequest> userUserAppRelRequests;

    public UserUserAppRelBatchRequest() {
    }

    /**
     *
     * @param userUserAppRelRequests
     */
    public UserUserAppRelBatchRequest(List<UserUserAppRelRequest> userUserAppRelRequests) {
        this.userUserAppRelRequests = userUserAppRelRequests;
    }

    /**
     *
     * @return List
     */
    public final List<UserUserAppRelRequest> getUserUserAppRelRequests() {
        return userUserAppRelRequests;
    }

    /**
     *
     * @param userUserAppRelRequests
     */
    public final void setUserUserAppRelRequests(final List<UserUserAppRelRequest> userUserAppRelRequests) {
        this.userUserAppRelRequests = userUserAppRelRequests;
    }

}
