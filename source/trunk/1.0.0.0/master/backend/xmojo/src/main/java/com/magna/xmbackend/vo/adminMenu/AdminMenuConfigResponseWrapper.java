/**
 * 
 */
package com.magna.xmbackend.vo.adminMenu;

import java.util.Set;

import com.magna.xmbackend.vo.projectApplication.ProjectApplicationMenuResponse;
import com.magna.xmbackend.vo.userApplication.UserApplicationMenuResponse;

/**
 * @author Bhabadyuti Bal
 *
 */
public class AdminMenuConfigResponseWrapper {

	private Set<UserApplicationMenuResponse> userApplicationMenuResponse;
	private Set<ProjectApplicationMenuResponse> projectApplicationMenuResponses;
	private boolean isAdminMenuUser;
	
	public AdminMenuConfigResponseWrapper() {
	}
	
	/**
	 * @param userAppTbls
	 * @param projectAppTbls
	 */
	public AdminMenuConfigResponseWrapper(boolean isAdminMenuUser, Set<UserApplicationMenuResponse> userApplicationMenuResponse,
			Set<ProjectApplicationMenuResponse> projectApplicationMenuResponses) {
		this.isAdminMenuUser = isAdminMenuUser;
		this.userApplicationMenuResponse = userApplicationMenuResponse;
		this.projectApplicationMenuResponses = projectApplicationMenuResponses;
	}

	/**
	 * @return the userApplicationMenuResponse
	 */
	public Set<UserApplicationMenuResponse> getUserApplicationMenuResponse() {
		return userApplicationMenuResponse;
	}

	/**
	 * @param userApplicationMenuResponse the userApplicationMenuResponse to set
	 */
	public void setUserApplicationMenuResponse(Set<UserApplicationMenuResponse> userApplicationMenuResponse) {
		this.userApplicationMenuResponse = userApplicationMenuResponse;
	}

	/**
	 * @return the projectApplicationMenuResponses
	 */
	public Set<ProjectApplicationMenuResponse> getProjectApplicationMenuResponses() {
		return projectApplicationMenuResponses;
	}

	/**
	 * @param projectApplicationMenuResponses the projectApplicationMenuResponses to set
	 */
	public void setProjectApplicationMenuResponses(Set<ProjectApplicationMenuResponse> projectApplicationMenuResponses) {
		this.projectApplicationMenuResponses = projectApplicationMenuResponses;
	}

	/**
	 * @return the isAdminMenuUser
	 */
	public boolean isAdminMenuUser() {
		return isAdminMenuUser;
	}

	/**
	 * @param isAdminMenuUser the isAdminMenuUser to set
	 */
	public void setAdminMenuUser(boolean isAdminMenuUser) {
		this.isAdminMenuUser = isAdminMenuUser;
	}
}
