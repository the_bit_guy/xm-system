/**
 * 
 */
package com.magna.xmbackend.vo.enums;

import java.io.Serializable;

/**
 * @author Bhabadyuti Bal
 *
 */
public enum NotificationVariables implements Serializable {

	TO_USERNAME("%username%"),
	ASSIGNED_USERNAME("%assignedusername%"),
	ASSIGNED_USER("%user%"),
	PROJECT_NAME("%projectname%"),
	ADMIN_AREA_NAME("%adminareaname%"),
	PROJECT_EXPIRY_DAYS("%proexpdays%"),
	GRACE_PERIOD("%graceperiod%"),
	PROJECT_CREATE_DATE("%projectcreatedate%"),
	REMARKS("%remarks%"),
	MANAGER("%manager%"),
	ROLE_AA_USER("%roleaauser%"),
	NOTIFICATION_MESSAGE("%message%"),
	COMMON_MAIL_TEMPLATE("common_template"),
	
	TO_USERNAME_BACKEND("${username}"),
	ASSIGNED_USERNAME_BACKEND("${assignedusername}"),
	ASSIGNED_USER_BACKEND("${user}"),
	PROJECT_NAME_BACKEND("${projectname}"),
	ADMIN_AREA_NAME_BACKEND("${adminareaname}"),
	PROJECT_EXPIRY_DAYS_BACKEND("${proexpdays}"),
	GRACE_PERIOD_BACKEND("${graceperiod}"),
	PROJECT_CREATE_DATE_BACKEND("${projectcreatedate}"),
	REMARKS_BACKEND("${remarks}"),
	MANAGER_BACKEND("${manager}"),
	ROLE_AA_USER_BACKEND("${roleaauser}"),
	NOTIFICATION_MESSAGE_BACKEND("${message}"),
	COMMON_MAIL_TEMPLATE_BACKEND("common_template");

	private String value;

	private NotificationVariables(String value) {
		this.value = value;
	}

	public String toString() {
		return this.value;
	}
}
