/**
 * 
 */
package com.magna.xmbackend.vo.projectApplication;

/**
 * @author Bhabadyuti Bal
 *
 */
public class ProjectApplciationMenuWrapper {

	private Iterable<ProjectApplicationMenuResponse> projectAppMenuResponses;

	public ProjectApplciationMenuWrapper() {
	}

	/**
	 * @param projectAppMenuResponses
	 */
	public ProjectApplciationMenuWrapper(Iterable<ProjectApplicationMenuResponse> projectAppMenuResponses) {
		this.projectAppMenuResponses = projectAppMenuResponses;
	}

	/**
	 * @return the projectAppMenuResponses
	 */
	public Iterable<ProjectApplicationMenuResponse> getProjectAppMenuResponses() {
		return projectAppMenuResponses;
	}

	/**
	 * @param projectAppMenuResponses
	 *            the projectAppMenuResponses to set
	 */
	public void setProjectAppMenuResponses(Iterable<ProjectApplicationMenuResponse> projectAppMenuResponses) {
		this.projectAppMenuResponses = projectAppMenuResponses;
	}

}
