/**
 * 
 */
package com.magna.xmbackend.vo.liveMessage;

import com.magna.xmbackend.entities.AdminAreasTbl;
import com.magna.xmbackend.entities.ProjectsTbl;
import com.magna.xmbackend.entities.SitesTbl;
import com.magna.xmbackend.entities.UsersTbl;


/**
 * @author Bhabadyuti Bal
 *
 */
public class LiveMessageConfigResponse {

	private String liveMessageConfigId;
    private SitesTbl sitesTbl;
    private AdminAreasTbl adminAreasTbl;
    private ProjectsTbl projectsTbl;
    private UsersTbl usersTbl;
    
    public LiveMessageConfigResponse() {
	}

	/**
	 * @return the liveMessageConfigId
	 */
	public String getLiveMessageConfigId() {
		return liveMessageConfigId;
	}

	/**
	 * @param liveMessageConfigId the liveMessageConfigId to set
	 */
	public void setLiveMessageConfigId(String liveMessageConfigId) {
		this.liveMessageConfigId = liveMessageConfigId;
	}

	/**
	 * @return the sitesTbl
	 */
	public SitesTbl getSitesTbl() {
		return sitesTbl;
	}

	/**
	 * @param sitesTbl the sitesTbl to set
	 */
	public void setSitesTbl(SitesTbl sitesTbl) {
		this.sitesTbl = sitesTbl;
	}

	/**
	 * @return the adminAreasTbl
	 */
	public AdminAreasTbl getAdminAreasTbl() {
		return adminAreasTbl;
	}

	/**
	 * @param adminAreasTbl the adminAreasTbl to set
	 */
	public void setAdminAreasTbl(AdminAreasTbl adminAreasTbl) {
		this.adminAreasTbl = adminAreasTbl;
	}

	/**
	 * @return the projectsTbl
	 */
	public ProjectsTbl getProjectsTbl() {
		return projectsTbl;
	}

	/**
	 * @param projectsTbl the projectsTbl to set
	 */
	public void setProjectsTbl(ProjectsTbl projectsTbl) {
		this.projectsTbl = projectsTbl;
	}

	/**
	 * @return the usersTbl
	 */
	public UsersTbl getUsersTbl() {
		return usersTbl;
	}

	/**
	 * @param usersTbl the usersTbl to set
	 */
	public void setUsersTbl(UsersTbl usersTbl) {
		this.usersTbl = usersTbl;
	}

	
    
}
