/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magna.xmbackend.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author Admin
 */
@Entity
@Table(name = "USER_SETTINGS_TBL")
@NamedQueries({
    @NamedQuery(name = "UserSettingsTbl.findAll", query = "SELECT u FROM UserSettingsTbl u")})
public class UserSettingsTbl implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "USER_SETTINGS_ID")
    private String userSettingsId;
    @Column(name = "SETTINGS")
    private String settings;
    @Basic(optional = false)
    @Column(name = "CREATE_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createDate;
    @Basic(optional = false)
    @Column(name = "UPDATE_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateDate;
    @JoinColumn(name = "PROJECT_ID", referencedColumnName = "PROJECT_ID")
    @ManyToOne
    private ProjectsTbl projectId;
    @JoinColumn(name = "SITE_ID", referencedColumnName = "SITE_ID")
    @ManyToOne(optional = false)
    private SitesTbl siteId;
    @JoinColumn(name = "USER_ID", referencedColumnName = "USER_ID")
    @ManyToOne(optional = false)
    private UsersTbl userId;

    public UserSettingsTbl() {
    }

    public UserSettingsTbl(String userSettingsId) {
        this.userSettingsId = userSettingsId;
    }

    public UserSettingsTbl(String userSettingsId, Date createDate, Date updateDate) {
        this.userSettingsId = userSettingsId;
        this.createDate = createDate;
        this.updateDate = updateDate;
    }

    public String getUserSettingsId() {
        return userSettingsId;
    }

    public void setUserSettingsId(String userSettingsId) {
        this.userSettingsId = userSettingsId;
    }

    public String getSettings() {
        return settings;
    }

    public void setSettings(String settings) {
        this.settings = settings;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public ProjectsTbl getProjectId() {
        return projectId;
    }

    public void setProjectId(ProjectsTbl projectId) {
        this.projectId = projectId;
    }

    public SitesTbl getSiteId() {
        return siteId;
    }

    public void setSiteId(SitesTbl siteId) {
        this.siteId = siteId;
    }

    public UsersTbl getUserId() {
        return userId;
    }

    public void setUserId(UsersTbl userId) {
        this.userId = userId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (userSettingsId != null ? userSettingsId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof UserSettingsTbl)) {
            return false;
        }
        UserSettingsTbl other = (UserSettingsTbl) object;
        if ((this.userSettingsId == null && other.userSettingsId != null) || (this.userSettingsId != null && !this.userSettingsId.equals(other.userSettingsId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.magna.xmbackend.entities.UserSettingsTbl[ userSettingsId=" + userSettingsId + " ]";
    }
    
}
