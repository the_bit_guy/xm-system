package com.magna.xmbackend.vo.rel;

import java.util.List;
import java.util.Map;

import com.magna.xmbackend.entities.AdminAreaStartAppRelTbl;

/**
 *
 * @author vijay
 */
public class AdminAreaStartAppRelResponse {

    private Iterable<AdminAreaStartAppRelTbl> adminAreaStartAppRelTbls;
    private List<Map<String, String>> statusMaps;
    private List<String> statusUpdatationFailedList;

    public AdminAreaStartAppRelResponse() {
    }

    /**
	 * @return the statusMaps
	 */
	public List<Map<String, String>> getStatusMaps() {
		return statusMaps;
	}

	/**
	 * @param statusMaps the statusMaps to set
	 */
	public void setStatusMaps(List<Map<String, String>> statusMaps) {
		this.statusMaps = statusMaps;
	}

	/**
	 * @param statusMaps
	 */
	public AdminAreaStartAppRelResponse(List<Map<String, String>> statusMaps) {
		this.statusMaps = statusMaps;
	}

	/**
     *
     * @param adminAreaStartAppRelTbl
     */
    public AdminAreaStartAppRelResponse(Iterable<AdminAreaStartAppRelTbl> adminAreaStartAppRelTbl) {
        this.adminAreaStartAppRelTbls = adminAreaStartAppRelTbl;
    }

    /**
     * @return the AdminAreaProjAppRelTbls
     */
    public Iterable<AdminAreaStartAppRelTbl> getAdminAreaStartAppRelTbls() {
        return adminAreaStartAppRelTbls;
    }

    /**
     * @param adminAreaStartAppRelTbls the adminAreaStartAppRelTbls to set
     */
    public void setAdminAreaStartAppRelTbls(Iterable<AdminAreaStartAppRelTbl> adminAreaStartAppRelTbls) {
        this.adminAreaStartAppRelTbls = adminAreaStartAppRelTbls;
    }
    
    /**
	 * @return the statusUpdatationFailedList
	 */
	public List<String> getStatusUpdatationFailedList() {
		return statusUpdatationFailedList;
	}

	/**
	 * @param statusUpdatationFailedList the statusUpdatationFailedList to set
	 */
	public void setStatusUpdatationFailedList(List<String> statusUpdatationFailedList) {
		this.statusUpdatationFailedList = statusUpdatationFailedList;
	}

	/**
     *
     * @return object as string
     */
    @Override
    public final String toString() {
        return new StringBuilder().append("AdminAreaStartAppRelResponse{")
                .append("adminAreaStartAppRelTbls=").append(adminAreaStartAppRelTbls)
                .append("}").toString();
    }
}
