/**
 * 
 */
package com.magna.xmbackend.vo.rearrange;

/**
 * @author Bhabadyuti Bal
 *
 */
public class RearrangeApplicationsRequest {

	private String id;
	private String userId;
	private String siteId;
	private String projectId;
	String userApplications;
	String projectApplciations;
	
	public RearrangeApplicationsRequest() {
	}
	
	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * @return the userId
	 */
	public String getUserId() {
		return userId;
	}
	/**
	 * @param userId the userId to set
	 */
	public void setUserId(String userId) {
		this.userId = userId;
	}
	/**
	 * @return the siteId
	 */
	public String getSiteId() {
		return siteId;
	}
	/**
	 * @param siteId the siteId to set
	 */
	public void setSiteId(String siteId) {
		this.siteId = siteId;
	}
	/**
	 * @return the projectId
	 */
	public String getProjectId() {
		return projectId;
	}
	/**
	 * @param projectId the projectId to set
	 */
	public void setProjectId(String projectId) {
		this.projectId = projectId;
	}

	/**
	 * @return the userApplications
	 */
	public String getUserApplications() {
		return userApplications;
	}

	/**
	 * @param userApplications the userApplications to set
	 */
	public void setUserApplications(String userApplications) {
		this.userApplications = userApplications;
	}

	/**
	 * @return the projectApplciations
	 */
	public String getProjectApplciations() {
		return projectApplciations;
	}

	/**
	 * @param projectApplciations the projectApplciations to set
	 */
	public void setProjectApplciations(String projectApplciations) {
		this.projectApplciations = projectApplciations;
	}


	
}
