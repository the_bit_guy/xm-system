/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magna.xmbackend.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonBackReference;

/**
 *
 * @author Admin
 */
@Entity
@Table(name = "EMAIL_NOTIFY_TO_USER_REL_TBL")
@NamedQueries({
    @NamedQuery(name = "EmailNotifyToUserRelTbl.findAll", query = "SELECT e FROM EmailNotifyToUserRelTbl e")})
public class EmailNotifyToUserRelTbl implements Serializable {


	private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "EMAIL_NOTIFY_TO_USER_REL_ID")
    private String emailNotifyToUserRelId;
    @Basic(optional = true)
    @Lob
    @Column(name = "TO_USERS")
    private String toUsers;
    @Basic(optional = true)
    @Lob
    @Column(name = "CC_USERS")
    private String ccUsers;
    @JsonBackReference
    @OneToOne
    @JoinColumn(name = "EMAIL_NOTIFICATION_CONFIG_ID")
    private EmailNotificationConfigTbl emailNotificationConfigId;
    
    @Basic(optional = false)
    @Column(name = "CREATE_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createDate;
    @Basic(optional = false)
    @Column(name = "UPDATE_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateDate;
   

	public EmailNotifyToUserRelTbl() {
    }

    public EmailNotifyToUserRelTbl(String emailNotifyToUserRelId) {
        this.emailNotifyToUserRelId = emailNotifyToUserRelId;
    }

	public EmailNotificationConfigTbl getEmailNotificationConfigId() {
		return emailNotificationConfigId;
	}

	public void setEmailNotificationConfigId(EmailNotificationConfigTbl emailNotificationConfigId) {
		this.emailNotificationConfigId = emailNotificationConfigId;
	}

	public EmailNotifyToUserRelTbl(String emailNotifyToUserRelId, Date createDate, Date updateDate) {
        this.emailNotifyToUserRelId = emailNotifyToUserRelId;
        this.createDate = createDate;
        this.updateDate = updateDate;
    }

    public String getEmailNotifyToUserRelId() {
        return emailNotifyToUserRelId;
    }

    public void setEmailNotifyToUserRelId(String emailNotifyToUserRelId) {
        this.emailNotifyToUserRelId = emailNotifyToUserRelId;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public String getToUsers() {
		return toUsers;
	}

	public void setToUsers(String toUsers) {
		this.toUsers = toUsers;
	}

	public String getCcUsers() {
		return ccUsers;
	}

	public void setCcUsers(String ccUsers) {
		this.ccUsers = ccUsers;
	}

	@Override
    public int hashCode() {
        int hash = 0;
        hash += (emailNotifyToUserRelId != null ? emailNotifyToUserRelId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof EmailNotifyToUserRelTbl)) {
            return false;
        }
        EmailNotifyToUserRelTbl other = (EmailNotifyToUserRelTbl) object;
        if ((this.emailNotifyToUserRelId == null && other.emailNotifyToUserRelId != null) || (this.emailNotifyToUserRelId != null && !this.emailNotifyToUserRelId.equals(other.emailNotifyToUserRelId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.magna.xmbackend.entities.EmailNotifyToUserRelTbl[ emailNotifyToUserRelId=" + emailNotifyToUserRelId + " ]";
    }
    
}
