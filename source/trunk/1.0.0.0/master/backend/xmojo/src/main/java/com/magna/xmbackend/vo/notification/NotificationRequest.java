/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magna.xmbackend.vo.notification;

import java.util.List;

import javax.validation.constraints.NotNull;

import com.magna.xmbackend.vo.CreationInfo;
import com.magna.xmbackend.vo.enums.Status;

/**
 *
 * @author dhana
 */
public class NotificationRequest {

    //list of userNames's
    private List<String> usersToNotify;
    private List<String> ccUsersToNotify;
    @NotNull(message="notification event id is requird")
    private String notificationEventId;
    private String notificationConfigId;
    @NotNull(message = "configuration name is required")
    private String notificationConfigActionName;
    private Status notificationActionStatus;
    private Status notificationEventStatus;
    @NotNull(message="email template id is required")
    private String emailTemplateId;
    private CreationInfo creationInfo;
    
    public NotificationRequest() {
	}
    
	public List<String> getUsersToNotify() {
		return usersToNotify;
	}
	public void setUsersToNotify(List<String> usersToNotify) {
		this.usersToNotify = usersToNotify;
	}
	public List<String> getCcUsersToNotify() {
		return ccUsersToNotify;
	}
	public void setCcUsersToNotify(List<String> ccUsersToNotify) {
		this.ccUsersToNotify = ccUsersToNotify;
	}

	public String getNotificationEventId() {
		return notificationEventId;
	}

	public void setNotificationEventId(String notificationEventId) {
		this.notificationEventId = notificationEventId;
	}

	public String getNotificationConfigId() {
		return notificationConfigId;
	}
	public void setNotificationConfigId(String notificationConfigId) {
		this.notificationConfigId = notificationConfigId;
	}
	public String getNotificationConfigActionName() {
		return notificationConfigActionName;
	}
	public void setNotificationConfigActionName(String notificationConfigActionName) {
		this.notificationConfigActionName = notificationConfigActionName;
	}
	public Status getNotificationActionStatus() {
		return notificationActionStatus;
	}
	public void setNotificationActionStatus(Status notificationActionStatus) {
		this.notificationActionStatus = notificationActionStatus;
	}
	public Status getNotificationEventStatus() {
		return notificationEventStatus;
	}
	public void setNotificationEventStatus(Status notificationEventStatus) {
		this.notificationEventStatus = notificationEventStatus;
	}
	public CreationInfo getCreationInfo() {
		return creationInfo;
	}
	public void setCreationInfo(CreationInfo creationInfo) {
		this.creationInfo = creationInfo;
	}

	public String getEmailTemplateId() {
		return emailTemplateId;
	}

	public void setEmailTemplateId(String emailTemplateId) {
		this.emailTemplateId = emailTemplateId;
	}

}
