/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magna.xmbackend.entities;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonBackReference;

/**
 *
 * @author Admin
 */
@Entity
@Table(name = "USER_APP_TRANSLATION_TBL")
@NamedQueries({
    @NamedQuery(name = "UserAppTranslationTbl.findAll", query = "SELECT u FROM UserAppTranslationTbl u")})
public class UserAppTranslationTbl implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "USER_APP_TRANSLATION_ID")
    private String userAppTranslationId;
    @Column(name = "NAME")
    private String name;
    @Column(name = "DESCRIPTION")
    private String description;
    @Column(name = "REMARKS")
    private String remarks;
    @Basic(optional = false)
    @Column(name = "CREATE_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createDate;
    @Basic(optional = false)
    @Column(name = "UPDATE_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateDate;
    @JoinColumn(name = "LANGUAGE_CODE", referencedColumnName = "LANGUAGE_CODE")
    @ManyToOne(optional = false)
    private LanguagesTbl languageCode;
    @JsonBackReference
    @JoinColumn(name = "USER_APPLICATION_ID", referencedColumnName = "USER_APPLICATION_ID")
    @ManyToOne
    private UserApplicationsTbl userApplicationId;

    public UserAppTranslationTbl() {
    }

    public UserAppTranslationTbl(String userAppTranslationId) {
        this.userAppTranslationId = userAppTranslationId;
    }

    public UserAppTranslationTbl(String userAppTranslationId, Date createDate, Date updateDate) {
        this.userAppTranslationId = userAppTranslationId;
        this.createDate = createDate;
        this.updateDate = updateDate;
    }

    public UserAppTranslationTbl(String userAppTranslationId, Date createDate, Date updateDate,
			LanguagesTbl languageCode, UserApplicationsTbl userApplicationId) {
		super();
		this.userAppTranslationId = userAppTranslationId;
		this.createDate = createDate;
		this.updateDate = updateDate;
		this.languageCode = languageCode;
		this.userApplicationId = userApplicationId;
	}

	public String getUserAppTranslationId() {
        return userAppTranslationId;
    }

    public void setUserAppTranslationId(String userAppTranslationId) {
        this.userAppTranslationId = userAppTranslationId;
    }

    public String getName() {
    	String decName = null;
		try {
			if (name != null) {
				decName = java.net.URLDecoder.decode(name, "ISO-8859-1");
			}
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
        return decName;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
    	String decDescription = null;
		try {
			if (description != null) {
				decDescription = java.net.URLDecoder.decode(description, "ISO-8859-1");
			}
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}

        return decDescription;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getRemarks() {
    	String decRemarks = null;
		try {
			if (remarks != null) {
				decRemarks = java.net.URLDecoder.decode(remarks, "ISO-8859-1");
			}
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}

        return decRemarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public LanguagesTbl getLanguageCode() {
        return languageCode;
    }

    public void setLanguageCode(LanguagesTbl languageCode) {
        this.languageCode = languageCode;
    }

    public UserApplicationsTbl getUserApplicationId() {
        return userApplicationId;
    }

    public void setUserApplicationId(UserApplicationsTbl userApplicationId) {
        this.userApplicationId = userApplicationId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (userAppTranslationId != null ? userAppTranslationId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof UserAppTranslationTbl)) {
            return false;
        }
        UserAppTranslationTbl other = (UserAppTranslationTbl) object;
        if ((this.userAppTranslationId == null && other.userAppTranslationId != null) || (this.userAppTranslationId != null && !this.userAppTranslationId.equals(other.userAppTranslationId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.magna.xmbackend.entities.UserAppTranslationTbl[ userAppTranslationId=" + userAppTranslationId + " ]";
    }
    
}
