package com.magna.xmbackend.vo.site;

import com.magna.xmbackend.vo.BaseObject;
import com.magna.xmbackend.vo.Message;
import java.io.Serializable;
import java.util.List;

public class SiteResponseObject extends BaseObject implements Serializable {

    private List<SiteResponse> siteResponseList;
    private Message message;

    /**
     *
     * @return siteResponseList
     */
    public final List<SiteResponse> getSiteResponseList() {
        return siteResponseList;
    }

    /**
     *
     * @param siteResponseList
     */
    public final void setSiteResponseList(final List<SiteResponse> siteResponseList) {
        this.siteResponseList = siteResponseList;
    }

    /**
     * @return the message
     */
    public final Message getMessage() {
        return message;
    }

    /**
     * @param message the message to set
     */
    public final void setMessage(final Message message) {
        this.message = message;
    }

    /**
     *
     * @return object as string
     */
    @Override
    public final String toString() {
        return new StringBuilder().append("SiteResponseObject{")
                .append("siteResponseList=").append(siteResponseList)
                .append("message=").append(message)
                .append("}").toString();
    }
}
