/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magna.xmbackend.vo;

import java.io.Serializable;
import java.util.List;

/**
 *
 * @author dhana
 */
public class AdminAreaPojo implements Serializable {

    private AdminArea adminArea;
    private List<AdminAreaDetail> adminAreaDetails;

    private CreationInfo creationInfo;

    public AdminAreaPojo() {
    }

    public AdminAreaPojo(AdminArea adminArea, List<AdminAreaDetail> adminAreaDetails, CreationInfo creationInfo) {
        this.adminArea = adminArea;
        this.adminAreaDetails = adminAreaDetails;
        this.creationInfo = creationInfo;
    }

    /**
     * @return the adminArea
     */
    public AdminArea getAdminArea() {
        return adminArea;
    }

    /**
     * @param adminArea the adminArea to set
     */
    public void setAdminArea(AdminArea adminArea) {
        this.adminArea = adminArea;
    }

    /**
     * @return the adminAreaDetails
     */
    public List<AdminAreaDetail> getAdminAreaDetails() {
        return adminAreaDetails;
    }

    /**
     * @param adminAreaDetails the adminAreaDetails to set
     */
    public void setAdminAreaDetails(List<AdminAreaDetail> adminAreaDetails) {
        this.adminAreaDetails = adminAreaDetails;
    }

    /**
     * @return the creationInfo
     */
    public CreationInfo getCreationInfo() {
        return creationInfo;
    }

    /**
     * @param creationInfo the creationInfo to set
     */
    public void setCreationInfo(CreationInfo creationInfo) {
        this.creationInfo = creationInfo;
    }

}
