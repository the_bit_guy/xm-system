/**
 * 
 */
package com.magna.xmbackend.vo.enums;

/**
 * @author Bhabadyuti Bal
 *
 */
public enum DirectoryObjectType {
	USER,PROJECT,USERAPPLICATION,PROJECTAPPLICATION
}
