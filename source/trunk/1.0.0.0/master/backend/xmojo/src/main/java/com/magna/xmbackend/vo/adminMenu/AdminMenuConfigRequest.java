/**
 * 
 */
package com.magna.xmbackend.vo.adminMenu;

import com.magna.xmbackend.vo.CreationInfo;
import com.magna.xmbackend.vo.enums.AdminMenuConfig;

/**
 * @author Bhabadyuti Bal
 *
 */
public class AdminMenuConfigRequest {

	private String id;
	private AdminMenuConfig objectType;
	private String objectId;
	private CreationInfo creationInfo;
	
	public AdminMenuConfigRequest() {
	}

	public AdminMenuConfigRequest(String id, AdminMenuConfig objectType, String objectId, CreationInfo creationInfo) {
		this.id = id;
		this.objectType = objectType;
		this.objectId = objectId;
		this.creationInfo = creationInfo;
	}

	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * @return the objectType
	 */
	public AdminMenuConfig getObjectType() {
		return objectType;
	}

	/**
	 * @param objectType the objectType to set
	 */
	public void setObjectType(AdminMenuConfig objectType) {
		this.objectType = objectType;
	}

	/**
	 * @return the objectId
	 */
	public String getObjectId() {
		return objectId;
	}

	/**
	 * @param objectId the objectId to set
	 */
	public void setObjectId(String objectId) {
		this.objectId = objectId;
	}

	/**
	 * @return the creationInfo
	 */
	public CreationInfo getCreationInfo() {
		return creationInfo;
	}

	/**
	 * @param creationInfo the creationInfo to set
	 */
	public void setCreationInfo(CreationInfo creationInfo) {
		this.creationInfo = creationInfo;
	}
	
}
