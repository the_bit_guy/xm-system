/**
 * 
 */
package com.magna.xmbackend.vo.propConfig;

import java.util.EnumMap;

import com.magna.xmbackend.vo.enums.SmtpKey;

/**
 * @author Bhabadyuti Bal
 *
 */
public class SmtpConfigRequest {

	private EnumMap<SmtpKey, String> smtpKeyValue;

	public SmtpConfigRequest() {
	}
	

	/**
	 * @param smtpKeyValue
	 */
	public SmtpConfigRequest(EnumMap<SmtpKey, String> smtpKeyValue) {
		this.smtpKeyValue = smtpKeyValue;
	}


	/**
	 * @return the smtpKeyValue
	 */
	public EnumMap<SmtpKey, String> getSmtpKeyValue() {
		return smtpKeyValue;
	}

	/**
	 * @param smtpKeyValue the smtpKeyValue to set
	 */
	public void setSmtpKeyValue(EnumMap<SmtpKey, String> smtpKeyValue) {
		this.smtpKeyValue = smtpKeyValue;
	}

	
}
