/**
 * 
 */
package com.magna.xmbackend.vo.enums;

import java.io.Serializable;

/**
 * @author Bhabadyuti Bal
 *
 */
public enum SmtpKey implements Serializable {
	SMTP_EMAIL_ID, SMTP_PORT_NUMBER, SMTP_SERVER_NAME, SMTP_PASSWORD
}
