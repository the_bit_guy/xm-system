/**
 * 
 */
package com.magna.xmbackend.vo.directory;

import java.util.List;
import java.util.Map;

import com.magna.xmbackend.entities.DirectoryTbl;

/**
 * @author Bhabadyuti Bal
 *
 */
public class DirectoryResponse {

	Iterable<DirectoryTbl> directoryTbls;
	private List<Map<String, String>> statusMaps;

	public DirectoryResponse() {
	}
	
	
	
	/**
	 * @return the statusMaps
	 */
	public List<Map<String, String>> getStatusMaps() {
		return statusMaps;
	}



	/**
	 * @param statusMaps the statusMaps to set
	 */
	public void setStatusMaps(List<Map<String, String>> statusMaps) {
		this.statusMaps = statusMaps;
	}



	/**
	 * @param statusMaps
	 */
	public DirectoryResponse(List<Map<String, String>> statusMaps) {
		this.statusMaps = statusMaps;
	}



	/**
	 * @param directoryTbls
	 */
	public DirectoryResponse(Iterable<DirectoryTbl> directoryTbls) {
		this.directoryTbls = directoryTbls;
	}

	/**
	 * @return the directoryTbls
	 */
	public Iterable<DirectoryTbl> getDirectoryTbls() {
		return directoryTbls;
	}

	/**
	 * @param directoryTbls the directoryTbls to set
	 */
	public void setDirectoryTbls(Iterable<DirectoryTbl> directoryTbls) {
		this.directoryTbls = directoryTbls;
	}
	
	
}
