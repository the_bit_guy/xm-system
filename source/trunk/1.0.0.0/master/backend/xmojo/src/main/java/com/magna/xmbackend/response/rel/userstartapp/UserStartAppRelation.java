/**
 * 
 */
package com.magna.xmbackend.response.rel.userstartapp;

import com.magna.xmbackend.entities.StartApplicationsTbl;
import com.magna.xmbackend.entities.UsersTbl;

/**
 * @author Bhabadyuti Bal
 *
 */
public class UserStartAppRelation {

	private String userStartAppRelId;
    private String status;
    private StartApplicationsTbl startApplicationId;
    private UsersTbl userId;
    
    public UserStartAppRelation() {
	}

	/**
	 * @param userStartAppRelId
	 * @param status
	 * @param startApplicationId
	 * @param userId
	 */
	public UserStartAppRelation(String userStartAppRelId, String status, StartApplicationsTbl startApplicationId,
			UsersTbl userId) {
		this.userStartAppRelId = userStartAppRelId;
		this.status = status;
		this.startApplicationId = startApplicationId;
		this.userId = userId;
	}

	/**
	 * @return the userStartAppRelId
	 */
	public String getUserStartAppRelId() {
		return userStartAppRelId;
	}

	/**
	 * @param userStartAppRelId the userStartAppRelId to set
	 */
	public void setUserStartAppRelId(String userStartAppRelId) {
		this.userStartAppRelId = userStartAppRelId;
	}

	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @return the startApplicationId
	 */
	public StartApplicationsTbl getStartApplicationId() {
		return startApplicationId;
	}

	/**
	 * @param startApplicationId the startApplicationId to set
	 */
	public void setStartApplicationId(StartApplicationsTbl startApplicationId) {
		this.startApplicationId = startApplicationId;
	}

	/**
	 * @return the userId
	 */
	public UsersTbl getUserId() {
		return userId;
	}

	/**
	 * @param userId the userId to set
	 */
	public void setUserId(UsersTbl userId) {
		this.userId = userId;
	}
    
    
}
