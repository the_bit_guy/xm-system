/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magna.xmbackend.entities;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonBackReference;

/**
 *
 * @author Admin
 */
@Entity
@Table(name = "EMAIL_NOTIFICATION_EVENT_TBL")
@NamedQueries({
    @NamedQuery(name = "EmailNotificationEventTbl.findAll", query = "SELECT e FROM EmailNotificationEventTbl e")})
public class EmailNotificationEventTbl implements Serializable {
	
	private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "EMAIL_NOTIFICATION_EVENT_ID")
    private String emailNotificationEventId;
    @Basic(optional = false)
    @Column(name = "EVENT")
    private String event;
    @Basic(optional = false)
    @Column(name = "DESCRIPTION")
    private String description;
    /*@Basic(optional = false)
    @Column(name = "SUBJECT")
    private String subject;
    @Basic(optional = false)
    @Column(name = "MESSAGE")
    private String message;
    @Column(name = "INCLUDE_REMARKS")
    private String includeRemarks;
    @Column(name = "SEND_TO_ASSIGNED_USER")
    private String sendToAssignedUser;
    @Column(name = "PROJECT_EXPIRY_NOTICE_PERIOD")
    private String projectExpiryNoticePeriod;*/
    @Column(name = "STATUS")
    private String status;
    @Basic(optional = false)
    @Column(name = "CREATE_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createDate;
    @Basic(optional = false)
    @Column(name = "UPDATE_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateDate;
    /*@Basic(optional = true)
    @Lob
    @Column(name = "EMAIL_TEMPLATE")
    private String emailTemplate;*/
    @JsonBackReference
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "emailNotificationEventId")
    private Collection<EmailNotificationConfigTbl> emailNotificationConfigCollection;

    public EmailNotificationEventTbl() {
    }
    
    public Collection<EmailNotificationConfigTbl> getEmailNotificationConfigCollection() {
		return emailNotificationConfigCollection;
	}

	public void setEmailNotificationConfigCollection(
			Collection<EmailNotificationConfigTbl> emailNotificationConfigCollection) {
		this.emailNotificationConfigCollection = emailNotificationConfigCollection;
	}

	public EmailNotificationEventTbl(String emailNotificationEventId) {
    	this.emailNotificationEventId = emailNotificationEventId;
    }
    public String getEmailNotificationEventId() {
		return emailNotificationEventId;
	}

	public void setEmailNotificationEventId(String emailNotificationEventId) {
		this.emailNotificationEventId = emailNotificationEventId;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

    public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	/*public String getEmailTemplate() {
		return emailTemplate;
	}

	public void setEmailTemplate(String emailTemplate) {
		this.emailTemplate = emailTemplate;
	}*/

 

    public EmailNotificationEventTbl(String emailNotificationConfigId, String event, String description, String subject, String message, Date createDate, Date updateDate) {
        this.event = event;
        this.createDate = createDate;
        this.updateDate = updateDate;
    }



    public String getEvent() {
        return event;
    }

    public void setEvent(String event) {
        this.event = event;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

 

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (emailNotificationEventId != null ? emailNotificationEventId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof EmailNotificationEventTbl)) {
            return false;
        }
        EmailNotificationEventTbl other = (EmailNotificationEventTbl) object;
        if ((this.emailNotificationEventId == null && other.emailNotificationEventId != null) || (this.emailNotificationEventId != null && !this.emailNotificationEventId.equals(other.emailNotificationEventId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.magna.xmbackend.entities.EmailNotificationConfigTbl[ emailNotificationConfigId=" + emailNotificationEventId + " ]";
    }
    
}
