package com.magna.xmbackend.vo.site;

import java.io.Serializable;
import java.util.List;

public class SiteObject implements Serializable {

    private Site site;
    private List<SiteDetail> siteDetailsList;

    /**
     *
     * @return site
     */
    public final Site getSite() {
        return site;
    }

    /**
     *
     * @param site
     */
    public final void setSite(final Site site) {
        this.site = site;
    }

    /**
     *
     * @return siteDetailsList
     */
    public final List<SiteDetail> getSiteDetailsList() {
        return siteDetailsList;
    }

    /**
     *
     * @param siteDetailsList
     */
    public final void setSiteDetailsList(final List<SiteDetail> siteDetailsList) {
        this.siteDetailsList = siteDetailsList;
    }

    /**
     *
     * @return object as string
     */
    @Override
    public final String toString() {
        return new StringBuilder().append("SiteObject{")
                .append("site=").append(site)
                .append(", siteDetailsList=").append(siteDetailsList)
                .append("}").toString();
    }

}
