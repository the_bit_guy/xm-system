package com.magna.xmbackend.vo.user;

import java.util.List;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.magna.xmbackend.validation.IsValidStatus;
import com.magna.xmbackend.vo.CreationInfo;
import com.magna.xmbackend.vo.enums.Status;

/**
 * The Class UserRequest.
 * 
 * @author shashwat.anand
 */
public class UserRequest {
	
	/** The id. */
	private String id;
	
	/** The user name. */
	@NotNull(message = "username is required")
	@Size(min = 1, max = 30, message = "username size should be more than 1 & less than 30 charachters")
	private String userName;
	
	/** The full name. */
	private String fullName;
	
	/** The status. */
	@NotNull(message = "status is required")
	@IsValidStatus(acceptedValues={"active", "inactive"}, message="Invalid status provided")
	private Status status;
	
	/** The email. */
	private String email;
	
	/** The department. */
	private String department;
	
	/** The telephone number. */
	private String telephoneNumber;
	
	/** The manager. */
	private String manager;
	
	/** The icon id. */
	//@NotNull(message = "iconId is required")
	private String iconId;
	
	/** The creation info. */
	private CreationInfo creationInfo;
	
	/** The user translation. */
	private List<UserTranslation> userTranslation;
	
	/**
	 * Instantiates a new user request.
	 */
	public UserRequest() {
	}
	

	/**
	 * Instantiates a new user request.
	 *
	 * @param id the id
	 * @param userName the user name
	 * @param fullName of the user
	 * @param status the status
	 * @param email the email
	 * @param telephoneNumber the telephone number
	 * @param department the department
	 * @param iconId the icon id
	 * @param manager the manager string
	 * @param creationInfo the creation info
	 * @param userTranslation the user translation
	 */
	public UserRequest(String id, String userName, String fullName, Status status, String email, String telephoneNumber, String department, String iconId,
			String manager, CreationInfo creationInfo, List<UserTranslation> userTranslation) {
		super();
		this.id = id;
		this.userName = userName;
		this.fullName = fullName;
		this.status = status;
		this.email = email;
		this.department = department;
		this.telephoneNumber = telephoneNumber;
		this.manager = manager;
		this.iconId = iconId;
		this.creationInfo = creationInfo;
		this.userTranslation = userTranslation;
	}


	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * Sets the id.
	 *
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * Gets the user name.
	 *
	 * @return the userName
	 */
	public String getUserName() {
		return userName;
	}

	/**
	 * Sets the user name.
	 *
	 * @param userName the userName to set
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}

	/**
	 * @return the fullName
	 */
	public String getFullName() {
		return fullName;
	}


	/**
	 * @param fullName the fullName to set
	 */
	public void setFullName(String fullName) {
		this.fullName = fullName;
	}


	/**
	 * Gets the status.
	 *
	 * @return the status
	 */
	public Status getStatus() {
		return status;
	}

	/**
	 * Sets the status.
	 *
	 * @param status the status to set
	 */
	public void setStatus(Status status) {
		this.status = status;
	}

	/**
	 * Gets the email.
	 *
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * Sets the email.
	 *
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * Gets the telephone number.
	 *
	 * @return the telephoneNumber
	 */
	public String getTelephoneNumber() {
		return telephoneNumber;
	}


	/**
	 * Sets the telephone number.
	 *
	 * @param telephoneNumber the telephoneNumber to set
	 */
	public void setTelephoneNumber(String telephoneNumber) {
		this.telephoneNumber = telephoneNumber;
	}


	/**
	 * Gets the department.
	 *
	 * @return the department
	 */
	public String getDepartment() {
		return department;
	}

	/**
	 * Sets the department.
	 *
	 * @param department the department to set
	 */
	public void setDepartment(String department) {
		this.department = department;
	}

	/**
	 * Gets the icon id.
	 *
	 * @return the iconId
	 */
	public String getIconId() {
		return iconId;
	}

	/**
	 * Sets the icon id.
	 *
	 * @param iconId the iconId to set
	 */
	public void setIconId(String iconId) {
		this.iconId = iconId;
	}

	/**
	 * @return the manager
	 */
	public String getManager() {
		return manager;
	}


	/**
	 * @param manager the manager to set
	 */
	public void setManager(String manager) {
		this.manager = manager;
	}


	/**
	 * Gets the creation info.
	 *
	 * @return the creationInfo
	 */
	public CreationInfo getCreationInfo() {
		return creationInfo;
	}

	/**
	 * Sets the creation info.
	 *
	 * @param creationInfo the creationInfo to set
	 */
	public void setCreationInfo(CreationInfo creationInfo) {
		this.creationInfo = creationInfo;
	}

	/**
	 * Gets the user translation.
	 *
	 * @return the userTranslation
	 */
	public List<UserTranslation> getUserTranslation() {
		return userTranslation;
	}

	/**
	 * Sets the user translation.
	 *
	 * @param userTranslation the userTranslation to set
	 */
	public void setUserTranslation(List<UserTranslation> userTranslation) {
		this.userTranslation = userTranslation;
	}

	
}
