/**
 * 
 */
package com.magna.xmbackend.vo.propConfig;

import java.util.EnumMap;

import com.magna.xmbackend.vo.enums.Singleton;

/**
 * @author Bhabadyuti Bal
 *
 */
public class SingletonConfigRequest {
	
	private EnumMap<Singleton, String> singletonKeyValue;
	
	public SingletonConfigRequest() {
	}
	
	

	/**
	 * @param singletonKeyValue
	 */
	public SingletonConfigRequest(EnumMap<Singleton, String> singletonKeyValue) {
		this.singletonKeyValue = singletonKeyValue;
	}



	/**
	 * @return the singletonKeyValue
	 */
	public EnumMap<Singleton, String> getSingletonKeyValue() {
		return singletonKeyValue;
	}

	/**
	 * @param singletonKeyValue the singletonKeyValue to set
	 */
	public void setSingletonKeyValue(EnumMap<Singleton, String> singletonKeyValue) {
		this.singletonKeyValue = singletonKeyValue;
	}

}
