package com.magna.xmbackend.vo.rel;

import java.util.Date;

/**
 *
 * @author vijay
 */
public class RolePermissionRelRequest {

    private String permissionId;
    private String roleId;
    private Date createDate;
    private Date updateDate;

    public RolePermissionRelRequest() {
    }

    /**
     *
     * @param permissionId
     * @param roleId
     * @param createDate
     * @param updateDate
     */
    public RolePermissionRelRequest(String permissionId, String roleId,
            Date createDate, Date updateDate) {
        this.permissionId = permissionId;
        this.roleId = roleId;
        this.createDate = createDate;
        this.updateDate = updateDate;
    }

    /**
     * @return the permissionId
     */
    public final String getPermissionId() {
        return permissionId;
    }

    /**
     * @param permissionId the permissionId to set
     */
    public final void setPermissionId(final String permissionId) {
        this.permissionId = permissionId;
    }

    /**
     * @return the roleId
     */
    public final String getRoleId() {
        return roleId;
    }

    /**
     * @param roleId the roleId to set
     */
    public final void setRoleId(final String roleId) {
        this.roleId = roleId;
    }

    /**
     * @return the createDate
     */
    public final Date getCreateDate() {
        return createDate;
    }

    /**
     * @param createDate the createDate to set
     */
    public final void setCreateDate(final Date createDate) {
        this.createDate = createDate;
    }

    /**
     * @return the updateDate
     */
    public final Date getUpdateDate() {
        return updateDate;
    }

    /**
     * @param updateDate the updateDate to set
     */
    public final void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    /**
     *
     * @return object as string
     */
    @Override
    public final String toString() {
        return new StringBuilder().append("RolePermissionRelRequest{")
                .append("permissionId=").append(getPermissionId())
                .append(", roleId=").append(getRoleId())
                .append(", createDate=").append(getCreateDate())
                .append(", updateDate=").append(getUpdateDate())
                .append("}").toString();
    }
}
