/**
 * 
 */
package com.magna.xmbackend.validation;

import java.util.ArrayList;
import java.util.List;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import com.magna.xmbackend.vo.enums.Status;

/**
 * @author Bhabadyuti Bal
 *
 */
public class StatusValidator implements ConstraintValidator<IsValidStatus, Status>{
	
	private List<String> valueList;
	
	public StatusValidator() {
	}

	@Override
	public void initialize(IsValidStatus constraintAnnotation) {
		valueList = new ArrayList<String>();
		for (String val : constraintAnnotation.acceptedValues()) {
			valueList.add(val.toUpperCase());
		}
	}

	@Override
	public boolean isValid(Status value, ConstraintValidatorContext context) {
		if (value == null) {
			/*context.disableDefaultConstraintViolation();
			context.buildConstraintViolationWithTemplate("Status is required")
				.addNode("status").addConstraintViolation();*/
			return false;
		}
		
		if (!value.name().equals(Status.ACTIVE.name()) && !value.name().equals(Status.INACTIVE.name())) {
	/*		context.disableDefaultConstraintViolation();
			context.buildConstraintViolationWithTemplate("Status value should be ACTIVE or INACTIVE")
					.addNode("status").addConstraintViolation();*/
			if(!valueList.contains(Status.ACTIVE) && !valueList.contains(Status.INACTIVE))
				return false;
		}
		
		
		return true;
	}

}
