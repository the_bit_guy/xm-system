package com.magna.xmbackend.vo.rel;

import java.util.List;
import java.util.Map;

import com.magna.xmbackend.entities.UserProjectRelTbl;

/**
 *
 * @author vijay
 */
public class UserProjectRelResponse {

    private Iterable<UserProjectRelTbl> userProjectRelTbls;
    private List<Map<String, String>> statusMaps;
    private List<String> deletedRelIds;

    public UserProjectRelResponse() {
    }

    /**
	 * @return the statusMaps
	 */
	public List<Map<String, String>> getStatusMaps() {
		return statusMaps;
	}

	/**
	 * @param statusMaps the statusMaps to set
	 */
	public void setStatusMaps(List<Map<String, String>> statusMaps) {
		this.statusMaps = statusMaps;
	}

	/**
	 * @param statusMaps
	 */
	public UserProjectRelResponse(List<Map<String, String>> statusMaps) {
		this.statusMaps = statusMaps;
	}

	/**
     *
     * @param userProjectRelTbl
     */
    public UserProjectRelResponse(Iterable<UserProjectRelTbl> userProjectRelTbl) {
        this.userProjectRelTbls = userProjectRelTbl;
    }

    /**
     * @return the userProjectRelTbls
     */
    public final Iterable<UserProjectRelTbl> getUserProjectRelTbls() {
        return userProjectRelTbls;
    }

    /**
     * @param userProjectRelTbls the userProjectRelTbls to set
     */
    public final void setUserProjectRelTbls(final Iterable<UserProjectRelTbl> userProjectRelTbls) {
        this.userProjectRelTbls = userProjectRelTbls;
    }

    public List<String> getDeletedRelIds() {
		return deletedRelIds;
	}

	public void setDeletedRelIds(List<String> deletedRelIds) {
		this.deletedRelIds = deletedRelIds;
	}

	public UserProjectRelResponse(List<Map<String, String>> statusMaps, List<String> deletedRelIds) {
		this.statusMaps = statusMaps;
		this.deletedRelIds = deletedRelIds;
	}

	/**
     *
     * @return object as string
     */
    @Override
    public final String toString() {
        return new StringBuilder().append("UserProjectRelResponse{")
                .append("userProjectRelTbls=").append(userProjectRelTbls)
                .append("}").toString();
    }
}
