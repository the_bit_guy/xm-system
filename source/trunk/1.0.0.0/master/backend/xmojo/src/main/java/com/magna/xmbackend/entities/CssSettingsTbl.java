/**
 * 
 */
package com.magna.xmbackend.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * @author Bhabadyuti Bal
 *
 */
@Entity
@Table(name = "CSS_SETTINGS_TBL")
public class CssSettingsTbl implements Serializable{

	private static final long serialVersionUID = -4649419637200978057L;

	@Id
	@Basic(optional = false)
	@Column(name = "CSS_SETTINGS_ID")
	private String cssSettingsId;
	
	@JoinColumn(name = "USER_ID", referencedColumnName = "USER_ID")
    @OneToOne(optional = false)
	private UsersTbl userId;
	
	@Basic(optional = false)
	@Column(name = "FILENAME")
	private String fileName;
	
	@Basic(optional = false)
    @Lob
    @Column(name = "CSS_CONTENT")
    private String cssContent;
	
	@Basic(optional = false)
	@Column(name = "IS_APPLIED")
	private String isApplied;
	
	@Basic(optional = false)
    @Column(name = "CREATE_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createDate;
	
    @Basic(optional = false)
    @Column(name = "UPDATE_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateDate;
    
    public CssSettingsTbl() {
    }
    
    

	public CssSettingsTbl(String cssSettingsId) {
		super();
		this.cssSettingsId = cssSettingsId;
	}



	/**
	 * @return the cssSettingsId
	 */
	public String getCssSettingsId() {
		return cssSettingsId;
	}

	/**
	 * @param cssSettingsId the cssSettingsId to set
	 */
	public void setCssSettingsId(String cssSettingsId) {
		this.cssSettingsId = cssSettingsId;
	}


	/**
	 * @return the userId
	 */
	public UsersTbl getUserId() {
		return userId;
	}



	/**
	 * @param userId the userId to set
	 */
	public void setUserId(UsersTbl userId) {
		this.userId = userId;
	}



	/**
	 * @return the fileName
	 */
	public String getFileName() {
		return fileName;
	}

	/**
	 * @param fileName the fileName to set
	 */
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}






	/**
	 * @return the cssContent
	 */
	public String getCssContent() {
		return cssContent;
	}



	/**
	 * @param cssContent the cssContent to set
	 */
	public void setCssContent(String cssContent) {
		this.cssContent = cssContent;
	}



	/**
	 * @return the isApplied
	 */
	public String getIsApplied() {
		return isApplied;
	}

	/**
	 * @param isApplied the isApplied to set
	 */
	public void setIsApplied(String isApplied) {
		this.isApplied = isApplied;
	}



	/**
	 * @return the createDate
	 */
	public Date getCreateDate() {
		return createDate;
	}



	/**
	 * @param createDate the createDate to set
	 */
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}



	/**
	 * @return the updateDate
	 */
	public Date getUpdateDate() {
		return updateDate;
	}



	/**
	 * @param updateDate the updateDate to set
	 */
	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}



	@Override
    public int hashCode() {
        int hash = 0;
        hash += (cssSettingsId != null ? cssSettingsId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof AdminAreasTbl)) {
            return false;
        }
        CssSettingsTbl other = (CssSettingsTbl) object;
        if ((this.cssSettingsId == null && other.cssSettingsId != null) || (this.cssSettingsId != null && !this.cssSettingsId.equals(other.cssSettingsId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.magna.xmbackend.entities.CssSettingsTbl[ cssSettingsId=" + cssSettingsId + " ]";
    }

}
