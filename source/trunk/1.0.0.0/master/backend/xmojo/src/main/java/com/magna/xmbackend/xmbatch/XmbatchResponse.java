/**
 * 
 */
package com.magna.xmbackend.xmbatch;

import java.util.List;
import java.util.Map;

/**
 * @author Bhabadyuti Bal
 *
 */
public class XmbatchResponse {
	
	private List<Map<String, Object>> queryResultSet;
	private String exceptionMessage;
	
	public XmbatchResponse() {
	}

	/**
	 * @param queryResultSet
	 */
	public XmbatchResponse(List<Map<String, Object>> queryResultSet, String exceptionMessage) {
		this.queryResultSet = queryResultSet;
		this.exceptionMessage = exceptionMessage;
	}

	/**
	 * @return the queryResultSet
	 */
	public List<Map<String, Object>> getQueryResultSet() {
		return queryResultSet;
	}

	/**
	 * @param queryResultSet the queryResultSet to set
	 */
	public void setQueryResultSet(List<Map<String, Object>> queryResultSet) {
		this.queryResultSet = queryResultSet;
	}

	/**
	 * @return the exceptionMessage
	 */
	public String getExceptionMessage() {
		return exceptionMessage;
	}

	/**
	 * @param exceptionMessage the exceptionMessage to set
	 */
	public void setExceptionMessage(String exceptionMessage) {
		this.exceptionMessage = exceptionMessage;
	}
	
}
