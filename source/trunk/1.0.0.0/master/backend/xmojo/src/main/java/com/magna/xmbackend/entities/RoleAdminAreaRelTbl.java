/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magna.xmbackend.entities;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonBackReference;

/**
 *
 * @author Admin
 */
@Entity
@Table(name = "ROLE_ADMIN_AREA_REL_TBL")
@NamedQueries({
    @NamedQuery(name = "RoleAdminAreaRelTbl.findAll", query = "SELECT r FROM RoleAdminAreaRelTbl r")})
public class RoleAdminAreaRelTbl implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "ROLE_ADMIN_AREA_REL_ID")
    private String roleAdminAreaRelId;
    @Basic(optional = false)
    @Column(name = "CREATE_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createDate;
    @Basic(optional = false)
    @Column(name = "UPDATE_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateDate;
    @JsonBackReference
    @OneToMany(mappedBy = "roleAdminAreaRelId")
    private Collection<RoleUserRelTbl> roleUserRelTblCollection;
    @JoinColumn(name = "ADMIN_AREA_ID", referencedColumnName = "ADMIN_AREA_ID")
    @ManyToOne(optional = false)
    private AdminAreasTbl adminAreaId;
    @JoinColumn(name = "ROLE_ID", referencedColumnName = "ROLE_ID")
    @ManyToOne(optional = false)
    private RolesTbl roleId;

    public RoleAdminAreaRelTbl() {
    }

    public RoleAdminAreaRelTbl(String roleAdminAreaRelId) {
        this.roleAdminAreaRelId = roleAdminAreaRelId;
    }

    public RoleAdminAreaRelTbl(String roleAdminAreaRelId, Date createDate, Date updateDate) {
        this.roleAdminAreaRelId = roleAdminAreaRelId;
        this.createDate = createDate;
        this.updateDate = updateDate;
    }

    public String getRoleAdminAreaRelId() {
        return roleAdminAreaRelId;
    }

    public void setRoleAdminAreaRelId(String roleAdminAreaRelId) {
        this.roleAdminAreaRelId = roleAdminAreaRelId;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public Collection<RoleUserRelTbl> getRoleUserRelTblCollection() {
        return roleUserRelTblCollection;
    }

    public void setRoleUserRelTblCollection(Collection<RoleUserRelTbl> roleUserRelTblCollection) {
        this.roleUserRelTblCollection = roleUserRelTblCollection;
    }

    public AdminAreasTbl getAdminAreaId() {
        return adminAreaId;
    }

    public void setAdminAreaId(AdminAreasTbl adminAreaId) {
        this.adminAreaId = adminAreaId;
    }

    public RolesTbl getRoleId() {
        return roleId;
    }

    public void setRoleId(RolesTbl roleId) {
        this.roleId = roleId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (roleAdminAreaRelId != null ? roleAdminAreaRelId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof RoleAdminAreaRelTbl)) {
            return false;
        }
        RoleAdminAreaRelTbl other = (RoleAdminAreaRelTbl) object;
        if ((this.roleAdminAreaRelId == null && other.roleAdminAreaRelId != null) || (this.roleAdminAreaRelId != null && !this.roleAdminAreaRelId.equals(other.roleAdminAreaRelId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.magna.xmbackend.entities.RoleAdminAreaRelTbl[ roleAdminAreaRelId=" + roleAdminAreaRelId + " ]";
    }
    
}
