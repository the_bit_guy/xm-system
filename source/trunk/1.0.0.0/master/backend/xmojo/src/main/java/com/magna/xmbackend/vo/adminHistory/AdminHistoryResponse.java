package com.magna.xmbackend.vo.adminHistory;

import java.util.List;
import java.util.Map;

public class AdminHistoryResponse {
	private List<Map<String, Object>> queryResultSet;

	public AdminHistoryResponse() {
	}

	public AdminHistoryResponse(List<Map<String, Object>> queryResultSet) {
		this.queryResultSet = queryResultSet;
	}

	/**
	 * @return the queryResultSet
	 */
	public List<Map<String, Object>> getQueryResultSet() {
		return queryResultSet;
	}

	/**
	 * @param queryResultSet
	 *            the queryResultSet to set
	 */
	public void setQueryResultSet(List<Map<String, Object>> queryResultSet) {
		this.queryResultSet = queryResultSet;
	}
}
