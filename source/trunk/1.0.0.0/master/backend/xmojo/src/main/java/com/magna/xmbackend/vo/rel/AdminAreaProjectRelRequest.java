package com.magna.xmbackend.vo.rel;

/**
 *
 * @author vijay
 */
public class AdminAreaProjectRelRequest {

	private String id;
    private String siteAdminAreaRelId;
    private String projectId;
    private String status;

    public AdminAreaProjectRelRequest() {
    }

    /**
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
     *
     * @param siteAdminAreaRelId
     * @param projectId
     * @param status
     */
    public AdminAreaProjectRelRequest(String siteAdminAreaRelId, String projectId,
            String status) {
        this.siteAdminAreaRelId = siteAdminAreaRelId;
        this.projectId = projectId;
        this.status = status;
    }

    /**
     * @return the siteAdminAreaRelId
     */
    public String getSiteAdminAreaRelId() {
        return siteAdminAreaRelId;
    }

    /**
     * @param siteAdminAreaRelId the siteAdminAreaRelId to set
     */
    public void setSiteAdminAreaRelId(String siteAdminAreaRelId) {
        this.siteAdminAreaRelId = siteAdminAreaRelId;
    }

    /**
     * @return the projectId
     */
    public String getProjectId() {
        return projectId;
    }

    /**
     * @param projectId the projectId to set
     */
    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }

    /**
     * @return the status
     */
    public String getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     *
     * @return object as string
     */
    @Override
    public final String toString() {
        return new StringBuilder().append("AdminAreaProjectRelRequest{")
                .append("siteAdminAreaRelId=").append(siteAdminAreaRelId)
                .append(", projectId=").append(projectId)
                .append(", status=").append(status)
                .append("}").toString();
    }
}
