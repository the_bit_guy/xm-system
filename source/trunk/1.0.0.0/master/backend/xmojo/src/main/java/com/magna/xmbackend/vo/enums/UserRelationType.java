package com.magna.xmbackend.vo.enums;

/**
 *
 * @author Admin
 */
public enum UserRelationType {
    ALLOWED, FORBIDDEN
}
