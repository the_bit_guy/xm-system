package com.magna.xmbackend.vo.enums;

public interface IDateTimeFormatConst {
	/** The cdate date time format. */
	String UI_DATE_TIME_FORMAT = "yyyy/MM/dd HH:mm";
	
	/** The database session date time format. */
	String DB_DATE_TIME_FORMAT = "'RRRR/MM/DD HH24:MI:SS'";
}
