/**
 * 
 */
package com.magna.xmbackend.vo.liveMessage;

/**
 * @author Bhabadyuti Bal
 *
 */
public class LiveMessageStatusRequest {
	
	private String liveMessageStatusId;
	private String liveMessageId;
	private String siteId;
    private String adminAreaId;
    private String projectId;
    private String userId;
	private String status;
	
	public LiveMessageStatusRequest() {
	}

	/**
	 * @return the liveMessageStatusId
	 */
	public String getLiveMessageStatusId() {
		return liveMessageStatusId;
	}

	/**
	 * @param liveMessageStatusId the liveMessageStatusId to set
	 */
	public void setLiveMessageStatusId(String liveMessageStatusId) {
		this.liveMessageStatusId = liveMessageStatusId;
	}

	/**
	 * @return the liveMessageId
	 */
	public String getLiveMessageId() {
		return liveMessageId;
	}

	/**
	 * @param liveMessageId the liveMessageId to set
	 */
	public void setLiveMessageId(String liveMessageId) {
		this.liveMessageId = liveMessageId;
	}

	/**
	 * @return the siteId
	 */
	public String getSiteId() {
		return siteId;
	}

	/**
	 * @param siteId the siteId to set
	 */
	public void setSiteId(String siteId) {
		this.siteId = siteId;
	}

	/**
	 * @return the adminAreaId
	 */
	public String getAdminAreaId() {
		return adminAreaId;
	}

	/**
	 * @param adminAreaId the adminAreaId to set
	 */
	public void setAdminAreaId(String adminAreaId) {
		this.adminAreaId = adminAreaId;
	}

	/**
	 * @return the projectId
	 */
	public String getProjectId() {
		return projectId;
	}

	/**
	 * @param projectId the projectId to set
	 */
	public void setProjectId(String projectId) {
		this.projectId = projectId;
	}

	/**
	 * @return the userId
	 */
	public String getUserId() {
		return userId;
	}

	/**
	 * @param userId the userId to set
	 */
	public void setUserId(String userId) {
		this.userId = userId;
	}

	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}
	
	
}
