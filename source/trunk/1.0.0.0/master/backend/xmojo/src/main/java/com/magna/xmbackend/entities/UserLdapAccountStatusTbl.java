/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magna.xmbackend.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author Admin
 */
@Entity @IdClass(UserLdapAccountStatusTbl.class)
@Table(name = "USER_LDAP_ACCOUNT_STATUS_TBL")
@NamedQueries({ @NamedQuery(name = "UserLdapAccountStatusTbl.findAll", query = "SELECT u FROM UserLdapAccountStatusTbl u") })
public class UserLdapAccountStatusTbl implements Serializable {

	private static final long serialVersionUID = 1L;
	/*@Id
	@Basic(optional = false)
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "USER_LDAP_ACCOUNT_STATUS_SEQ")
	@SequenceGenerator(name = "USER_LDAP_ACCOUNT_STATUS_SEQ", sequenceName = "USER_LDAP_ACCOUNT_STATUS_SEQ", allocationSize = 1)
	private Long id;*/
	@Id
	@JoinColumn(name = "USER_ID", referencedColumnName = "USER_ID")
    @OneToOne(optional = false)
	private UsersTbl userId;
	@Id
	@Column(name = "ACCOUNT_STATUS")
	private String accountStatus;
	@Basic(optional = false)
	@Column(name = "CREATE_DATE")
	@Temporal(TemporalType.TIMESTAMP)
	private Date createDate;
	@Basic(optional = false)
	@Column(name = "UPDATE_DATE")
	@Temporal(TemporalType.TIMESTAMP)
	private Date updateDate;
	//@Column(name = "REMARKS")
	//private String remarks;

	public UserLdapAccountStatusTbl() {
		// TODO Auto-generated constructor stub
	}

	public UserLdapAccountStatusTbl(UsersTbl userId) {
		this.userId = userId;
	}

	public UserLdapAccountStatusTbl(UsersTbl userId, Date createDate) {
		this.userId = userId;
		this.createDate = createDate;
	}


	public UsersTbl getUserId() {
		return userId;
	}

	public void setUserId(UsersTbl userId) {
		this.userId = userId;
	}

	public String getAccountStatus() {
		return accountStatus;
	}

	public void setAccountStatus(String accountStatus) {
		this.accountStatus = accountStatus;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	@Override
	public int hashCode() {
		int hash = 0;
		hash += (userId != null ? userId.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object) {
		// TODO: Warning - this method won't work in the case the id fields are
		// not set
		if (!(object instanceof UserLdapAccountStatusTbl)) {
			return false;
		}
		UserLdapAccountStatusTbl other = (UserLdapAccountStatusTbl) object;
		if ((this.userId == null && other.userId != null)
				|| (this.userId != null && !this.userId.equals(other.userId))) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "com.magna.xmbackend.entities.UserLdapAccountStatusTbl[ userId=" + userId + " ]";
	}

}
