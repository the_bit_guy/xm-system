package com.magna.xmbackend.vo.jpa.site;

import com.magna.xmbackend.entities.SiteAdminAreaRelTbl;
import com.magna.xmbackend.entities.StartApplicationsTbl;

/**
 *
 * @author vijay
 */
public class SiteAdminAreaRelIdWithAdminAreaStartAppRel {

    private String adminAreaStartAppRelId;
    private SiteAdminAreaRelTbl siteAdminAreaRelId;
    private StartApplicationsTbl startApplicationsTbl;
    private String status;

    public SiteAdminAreaRelIdWithAdminAreaStartAppRel() {
    }

    /**
     *
     * @param siteAdminAreaRelId
     * @param startApplicationsTbl
     * @param adminAreaProjRelId
     */
    public SiteAdminAreaRelIdWithAdminAreaStartAppRel(SiteAdminAreaRelTbl siteAdminAreaRelId,
            StartApplicationsTbl startApplicationsTbl, String adminAreaProjRelId) {
        this.siteAdminAreaRelId = siteAdminAreaRelId;
        this.startApplicationsTbl = startApplicationsTbl;
        this.adminAreaStartAppRelId = adminAreaProjRelId;
    }

    /**
     * @return the siteAdminAreaRelId
     */
    public final SiteAdminAreaRelTbl getSiteAdminAreaRelId() {
        return siteAdminAreaRelId;
    }

    /**
     * @param siteAdminAreaRelId the siteAdminAreaRelId to set
     */
    public final void setSiteAdminAreaRelId(final SiteAdminAreaRelTbl siteAdminAreaRelId) {
        this.siteAdminAreaRelId = siteAdminAreaRelId;
    }

    /**
     * @return the startApplicationsTbl
     */
    public final StartApplicationsTbl getStartApplicationsTbl() {
        return startApplicationsTbl;
    }

    /**
     * @param startApplicationsTbl the startApplicationsTbl to set
     */
    public final void setStartApplicationsTbl(final StartApplicationsTbl startApplicationsTbl) {
        this.startApplicationsTbl = startApplicationsTbl;
    }

    /**
     * @return the adminAreaStartAppRelId
     */
    public final String getAdminAreaStartAppRelId() {
        return adminAreaStartAppRelId;
    }

    /**
     * @param adminAreaStartAppRelId the adminAreaStartAppRelId to set
     */
    public final void setAdminAreaStartAppRelId(final String adminAreaStartAppRelId) {
        this.adminAreaStartAppRelId = adminAreaStartAppRelId;
    }

    /**
     * @return the status
     */
    public final String getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public final void setStatus(String status) {
        this.status = status;
    }
}
