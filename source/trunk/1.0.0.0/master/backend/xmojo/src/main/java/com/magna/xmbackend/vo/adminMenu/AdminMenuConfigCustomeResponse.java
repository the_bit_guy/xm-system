/**
 * 
 */
package com.magna.xmbackend.vo.adminMenu;

import com.magna.xmbackend.entities.ProjectApplicationsTbl;
import com.magna.xmbackend.entities.UserApplicationsTbl;
import com.magna.xmbackend.entities.UsersTbl;

/**
 * @author Bhabadyuti Bal
 *
 */
public class AdminMenuConfigCustomeResponse {

	private String adminMenuConfigId;
	private UsersTbl usersTbl;
	private UserApplicationsTbl userApplicationsTbl;
	private ProjectApplicationsTbl projectApplicationsTbl;
	
	public AdminMenuConfigCustomeResponse() {
	}

	/**
	 * @return the adminMenuConfigId
	 */
	public String getAdminMenuConfigId() {
		return adminMenuConfigId;
	}

	/**
	 * @param adminMenuConfigId the adminMenuConfigId to set
	 */
	public void setAdminMenuConfigId(String adminMenuConfigId) {
		this.adminMenuConfigId = adminMenuConfigId;
	}

	/**
	 * @return the usersTbl
	 */
	public UsersTbl getUsersTbl() {
		return usersTbl;
	}

	/**
	 * @param usersTbl the usersTbl to set
	 */
	public void setUsersTbl(UsersTbl usersTbl) {
		this.usersTbl = usersTbl;
	}

	/**
	 * @return the userApplicationsTbl
	 */
	public UserApplicationsTbl getUserApplicationsTbl() {
		return userApplicationsTbl;
	}

	/**
	 * @param userApplicationsTbl the userApplicationsTbl to set
	 */
	public void setUserApplicationsTbl(UserApplicationsTbl userApplicationsTbl) {
		this.userApplicationsTbl = userApplicationsTbl;
	}

	/**
	 * @return the projectApplicationsTbl
	 */
	public ProjectApplicationsTbl getProjectApplicationsTbl() {
		return projectApplicationsTbl;
	}

	/**
	 * @param projectApplicationsTbl the projectApplicationsTbl to set
	 */
	public void setProjectApplicationsTbl(ProjectApplicationsTbl projectApplicationsTbl) {
		this.projectApplicationsTbl = projectApplicationsTbl;
	}
	
	
}
