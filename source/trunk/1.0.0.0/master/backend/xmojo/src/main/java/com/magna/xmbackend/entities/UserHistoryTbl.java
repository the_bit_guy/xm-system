/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magna.xmbackend.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author Admin
 */
@Entity
@Table(name = "USER_HISTORY_TBL")
@NamedQueries({
    @NamedQuery(name = "UserHistoryTbl.findAll", query = "SELECT u FROM UserHistoryTbl u ORDER BY u.logTime DESC")})
public class UserHistoryTbl implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "ID")
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="USER_HISTORY_SEQ")
    @SequenceGenerator(name="USER_HISTORY_SEQ", sequenceName="USER_HISTORY_SEQ", allocationSize=1)
    private Long id;
    @Basic(optional = false)
    @Column(name = "LOG_TIME")
    @Temporal(TemporalType.TIMESTAMP)
    private Date logTime;
    @Column(name = "USER_NAME")
    private String userName;
    @Column(name = "HOST")
    private String host;
    @Column(name = "SITE")
    private String site;
    @Column(name = "ADMIN_AREA")
    private String adminArea;
    @Column(name = "PROJECT")
    private String project;
    @Column(name = "APPLICATION")
    private String application;
    @Column(name = "PID")
    private Integer pid;
    @Column(name = "RESULT")
    private String result;
    @Column(name = "ARGS")
    private String args;
    @Basic(optional = false)
    @Column(name = "IS_USER_STATUS")
    private String isUserStatus;

    public UserHistoryTbl() {
    }

    public UserHistoryTbl(Long id) {
        this.id = id;
    }

    public UserHistoryTbl(Long id, Date logTime, String isUserStatus) {
        this.id = id;
        this.logTime = logTime;
        this.isUserStatus = isUserStatus;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getLogTime() {
        return logTime;
    }

    public void setLogTime(Date logTime) {
        this.logTime = logTime;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public String getSite() {
        return site;
    }

    public void setSite(String site) {
        this.site = site;
    }

    public String getAdminArea() {
        return adminArea;
    }

    public void setAdminArea(String adminArea) {
        this.adminArea = adminArea;
    }

    public String getProject() {
        return project;
    }

    public void setProject(String project) {
        this.project = project;
    }

    public String getApplication() {
        return application;
    }

    public void setApplication(String application) {
        this.application = application;
    }

    public Integer getPid() {
        return pid;
    }

    public void setPid(Integer pid) {
        this.pid = pid;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getArgs() {
        return args;
    }

    public void setArgs(String args) {
        this.args = args;
    }

    public String getIsUserStatus() {
        return isUserStatus;
    }

    public void setIsUserStatus(String isUserStatus) {
        this.isUserStatus = isUserStatus;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof UserHistoryTbl)) {
            return false;
        }
        UserHistoryTbl other = (UserHistoryTbl) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.magna.xmbackend.entities.UserHistoryTbl[ id=" + id + " ]";
    }
    
}
