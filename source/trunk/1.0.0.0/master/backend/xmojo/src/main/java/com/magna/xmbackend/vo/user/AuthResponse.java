/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magna.xmbackend.vo.user;

import java.io.Serializable;

/**
 *
 * @author dhana
 */
public class AuthResponse implements Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String username;
    private boolean isValidUser;
    private String message;
    private long ldapResponseTime;

    public AuthResponse() {
    }
    
    /**
     * 
     * @param message
     */
	public AuthResponse(String message) {
		this.message = message;
	}



	/**
	 * @param username
	 * @param isValidUser
	 * @param message
	 */
	public AuthResponse(String username, boolean isValidUser, String message, long ldapResponseTime) {
		this.username = username;
		this.isValidUser = isValidUser;
		this.message = message;
		this.ldapResponseTime = ldapResponseTime;
	}

	/**
	 * @return the username
	 */
	public String getUsername() {
		return username;
	}

	/**
	 * @param username the username to set
	 */
	public void setUsername(String username) {
		this.username = username;
	}

	/**
	 * @return the isValidUser
	 */
	public boolean isValidUser() {
		return isValidUser;
	}

	/**
	 * @param isValidUser the isValidUser to set
	 */
	public void setValidUser(boolean isValidUser) {
		this.isValidUser = isValidUser;
	}

	/**
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * @param message the message to set
	 */
	public void setMessage(String message) {
		this.message = message;
	}

	public long getLdapResponseTime() {
		return ldapResponseTime;
	}

	public void setLdapResponseTime(long ldapResponseTime) {
		this.ldapResponseTime = ldapResponseTime;
	}

}
