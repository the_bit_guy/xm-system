package com.magna.xmbackend.vo.enums;

import java.io.Serializable;

public enum Status implements Serializable {
    ACTIVE, INACTIVE, DELETED, DEACTIVATED, INVALID_CREDENTIALS, VALID_USER, INVALID_USER
}
