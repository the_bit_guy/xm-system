/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magna.xmbackend.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author Admin
 */
@Entity
@Table(name = "USER_START_APP_REL_TBL")
@NamedQueries({
    @NamedQuery(name = "UserStartAppRelTbl.findAll", query = "SELECT u FROM UserStartAppRelTbl u")})
public class UserStartAppRelTbl implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "USER_START_APP_REL_ID")
    private String userStartAppRelId;
    @Basic(optional = false)
    @Column(name = "STATUS")
    private String status;
    @JoinColumn(name = "START_APPLICATION_ID", referencedColumnName = "START_APPLICATION_ID")
    @ManyToOne(optional = false)
    private StartApplicationsTbl startApplicationId;
    @JoinColumn(name = "USER_ID", referencedColumnName = "USER_ID")
    @ManyToOne(optional = false)
    private UsersTbl userId;

    public UserStartAppRelTbl() {
    }

    public UserStartAppRelTbl(String userStartAppRelId) {
        this.userStartAppRelId = userStartAppRelId;
    }

    public UserStartAppRelTbl(String userStartAppRelId, String status) {
        this.userStartAppRelId = userStartAppRelId;
        this.status = status;
    }

    public String getUserStartAppRelId() {
        return userStartAppRelId;
    }

    public void setUserStartAppRelId(String userStartAppRelId) {
        this.userStartAppRelId = userStartAppRelId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public StartApplicationsTbl getStartApplicationId() {
        return startApplicationId;
    }

    public void setStartApplicationId(StartApplicationsTbl startApplicationId) {
        this.startApplicationId = startApplicationId;
    }

    public UsersTbl getUserId() {
        return userId;
    }

    public void setUserId(UsersTbl userId) {
        this.userId = userId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (userStartAppRelId != null ? userStartAppRelId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof UserStartAppRelTbl)) {
            return false;
        }
        UserStartAppRelTbl other = (UserStartAppRelTbl) object;
        if ((this.userStartAppRelId == null && other.userStartAppRelId != null) || (this.userStartAppRelId != null && !this.userStartAppRelId.equals(other.userStartAppRelId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.magna.xmbackend.entities.UserStartAppRelTbl[ userStartAppRelId=" + userStartAppRelId + " ]";
    }
    
}
