/**
 * 
 */
package com.magna.xmbackend.vo.projectApplication;

import com.magna.xmbackend.entities.ProjectApplicationsTbl;

/**
 * @author Bhabadyuti Bal
 *
 */
public class ProjectApplicationMenuResponse {

	private ProjectApplicationsTbl projectApplicationsTbl;
	private Iterable<ProjectApplicationsTbl> projectAppChildren;
	
	public ProjectApplicationMenuResponse() {
	}

	/**
	 * @param projectApplicationsTbl
	 * @param projectAppChildren
	 */
	public ProjectApplicationMenuResponse(ProjectApplicationsTbl projectApplicationsTbl,
			Iterable<ProjectApplicationsTbl> projectAppChildren) {
		this.projectApplicationsTbl = projectApplicationsTbl;
		this.projectAppChildren = projectAppChildren;
	}

	/**
	 * @return the projectApplicationsTbl
	 */
	public ProjectApplicationsTbl getProjectApplicationsTbl() {
		return projectApplicationsTbl;
	}

	/**
	 * @param projectApplicationsTbl the projectApplicationsTbl to set
	 */
	public void setProjectApplicationsTbl(ProjectApplicationsTbl projectApplicationsTbl) {
		this.projectApplicationsTbl = projectApplicationsTbl;
	}

	/**
	 * @return the projectAppChildren
	 */
	public Iterable<ProjectApplicationsTbl> getProjectAppChildren() {
		return projectAppChildren;
	}

	/**
	 * @param projectAppChildren the projectAppChildren to set
	 */
	public void setProjectAppChildren(Iterable<ProjectApplicationsTbl> projectAppChildren) {
		this.projectAppChildren = projectAppChildren;
	}
	
	
}
