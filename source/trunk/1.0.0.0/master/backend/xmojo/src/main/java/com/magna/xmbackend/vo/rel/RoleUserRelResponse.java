package com.magna.xmbackend.vo.rel;

import com.magna.xmbackend.entities.RoleUserRelTbl;
import java.util.List;
import java.util.Map;

/**
 *
 * @author vijay
 */
public class RoleUserRelResponse {

    private Iterable<RoleUserRelTbl> roleUserRelTbls;
    private List<Map<String, String>> statusMaps;

    public RoleUserRelResponse() {
    }

    /**
	 * @param statusMaps
	 */
	public RoleUserRelResponse(List<Map<String, String>> statusMaps) {
		this.statusMaps = statusMaps;
	}

	/**
     *
     * @param roleUserRelTbls
     * @param statusMaps
     */
    public RoleUserRelResponse(Iterable<RoleUserRelTbl> roleUserRelTbls,
            List<Map<String, String>> statusMaps) {
        this.roleUserRelTbls = roleUserRelTbls;
        this.statusMaps = statusMaps;
    }

    /**
     * @return the userRoleRelTbls
     */
    public final Iterable<RoleUserRelTbl> getRoleUserRelTbls() {
        return roleUserRelTbls;
    }

    /**
     * @param userRoleRelTbls the userUserAppRelTbls to set
     */
    public final void setRoleUserRelTbls(final Iterable<RoleUserRelTbl> userRoleRelTbls) {
        this.roleUserRelTbls = userRoleRelTbls;
    }

    /**
     * @return the statusMaps
     */
    public final List<Map<String, String>> getStatusMap() {
        return statusMaps;
    }

    /**
     * @param statusMaps the statusMap to set
     */
    public final void setStatusMap(final List<Map<String, String>> statusMaps) {
        this.statusMaps = statusMaps;
    }

    /**
     *
     * @return object as string
     */
    @Override
    public final String toString() {
        return new StringBuilder().append("RoleUserRelResponse{")
                .append("roleUserRelTbls=").append(getRoleUserRelTbls())
                .append("statusMaps=").append(getStatusMap())
                .append("}").toString();
    }
}
