package com.magna.xmbackend.vo.enums;

import java.io.Serializable;

/**
 *
 * @author Admin
 */
public enum PermissionType implements Serializable {
    OBJECT_PERMISSION, RELATION_PERMISSION, AA_BASED_RELATION_PERMISSION, UNAUTHORIZED
}
