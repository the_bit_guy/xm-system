package com.magna.xmbackend.vo.language;

import com.magna.xmbackend.entities.LanguagesTbl;
import java.io.Serializable;

public class LingualResponse implements Serializable {

    private Iterable<LanguagesTbl> languagesTbls;

    public LingualResponse() {
    }

    public LingualResponse(Iterable<LanguagesTbl> languagesTbls) {
        this.languagesTbls = languagesTbls;
    }

    public void setLanguagesTbls(Iterable<LanguagesTbl> languagesTbls) {
        this.languagesTbls = languagesTbls;
    }

    public Iterable<LanguagesTbl> getLanguagesTbls() {
        return languagesTbls;
    }

}
