/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magna.xmbackend.entities;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonBackReference;

/**
 *
 * @author Admin
 */
@Entity
@Table(name = "ROLES_TBL")
@NamedQueries({
    @NamedQuery(name = "RolesTbl.findAll", query = "SELECT r FROM RolesTbl r")})
public class RolesTbl implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "ROLE_ID")
    private String roleId;
    @Basic(optional = false)
    @Column(name = "NAME")
    private String name;
    @Column(name = "DESCRIPTION")
    private String description;
    @Basic(optional = false)
    @Column(name = "CREATE_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createDate;
    @Basic(optional = false)
    @Column(name = "UPDATE_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateDate;
    @JsonBackReference
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "roleId")
    private Collection<RolePermissionRelTbl> rolePermissionRelTblCollection;
    @JsonBackReference
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "roleId")
    private Collection<RoleUserRelTbl> roleUserRelTblCollection;
    @JsonBackReference
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "roleId")
    private Collection<RoleAdminAreaRelTbl> roleAdminAreaRelTblCollection;

    public RolesTbl() {
    }

    public RolesTbl(String roleId) {
        this.roleId = roleId;
    }

    public RolesTbl(String roleId, String name, Date createDate, Date updateDate) {
        this.roleId = roleId;
        this.name = name;
        this.createDate = createDate;
        this.updateDate = updateDate;
    }

    public String getRoleId() {
        return roleId;
    }

    public void setRoleId(String roleId) {
        this.roleId = roleId;
    }

    public String getName() {
    	String decName = null;
		try {
			if (name != null) {
				decName = java.net.URLDecoder.decode(name, "ISO-8859-1");
			}
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
        return decName;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public Collection<RolePermissionRelTbl> getRolePermissionRelTblCollection() {
        return rolePermissionRelTblCollection;
    }

    public void setRolePermissionRelTblCollection(Collection<RolePermissionRelTbl> rolePermissionRelTblCollection) {
        this.rolePermissionRelTblCollection = rolePermissionRelTblCollection;
    }

    public Collection<RoleUserRelTbl> getRoleUserRelTblCollection() {
        return roleUserRelTblCollection;
    }

    public void setRoleUserRelTblCollection(Collection<RoleUserRelTbl> roleUserRelTblCollection) {
        this.roleUserRelTblCollection = roleUserRelTblCollection;
    }

    public Collection<RoleAdminAreaRelTbl> getRoleAdminAreaRelTblCollection() {
        return roleAdminAreaRelTblCollection;
    }

    public void setRoleAdminAreaRelTblCollection(Collection<RoleAdminAreaRelTbl> roleAdminAreaRelTblCollection) {
        this.roleAdminAreaRelTblCollection = roleAdminAreaRelTblCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (roleId != null ? roleId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof RolesTbl)) {
            return false;
        }
        RolesTbl other = (RolesTbl) object;
        if ((this.roleId == null && other.roleId != null) || (this.roleId != null && !this.roleId.equals(other.roleId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.magna.xmbackend.entities.RolesTbl[ roleId=" + roleId + " ]";
    }
    
}
