/**
 * 
 */
package com.magna.xmbackend.vo.group;

import java.util.List;
import java.util.Map;

/**
 * @author Bhabadyuti Bal
 *
 */
public class GroupRelBatchResponse {

	private List<GroupRelResponse> groupRelResponses;
	private List<Map<String, String>> statusMaps;
	
	public GroupRelBatchResponse() {
	}
	
	/**
	 * @param groupRelResponses
	 */
	public GroupRelBatchResponse(List<GroupRelResponse> groupRelResponses) {
		this.groupRelResponses = groupRelResponses;
	}

	/**
	 * @param groupRelResponses
	 * @param statusMaps
	 */
	public GroupRelBatchResponse(List<GroupRelResponse> groupRelResponses, List<Map<String, String>> statusMaps) {
		this.groupRelResponses = groupRelResponses;
		this.statusMaps = statusMaps;
	}




	/**
	 * @return the groupRelResponses
	 */
	public List<GroupRelResponse> getGroupRelResponses() {
		return groupRelResponses;
	}

	/**
	 * @param groupRelResponses the groupRelResponses to set
	 */
	public void setGroupRelResponses(List<GroupRelResponse> groupRelResponses) {
		this.groupRelResponses = groupRelResponses;
	}



	/**
	 * @return the statusMaps
	 */
	public List<Map<String, String>> getStatusMaps() {
		return statusMaps;
	}

	/**
	 * @param statusMaps the statusMaps to set
	 */
	public void setStatusMaps(List<Map<String, String>> statusMaps) {
		this.statusMaps = statusMaps;
	}

}
