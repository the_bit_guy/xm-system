/**
 * 
 */
package com.magna.xmbackend.vo.liveMessage;

import com.magna.xmbackend.entities.UsersTbl;

/**
 * @author Bhabadyuti Bal
 *
 */
public class LiveMessageStatusResponse {

	private String liveMessageId;
	private String liveMessageStatusId;
	private String liveMessageName;
	private String liveMessageSubject;
	private String liveMessage;
	private boolean isPopUp;
	private UsersTbl usersTbl;
	private String liveMsgStatus;
	
	public LiveMessageStatusResponse() {
	}

	/**
	 * @return the liveMessageId
	 */
	public String getLiveMessageId() {
		return liveMessageId;
	}

	/**
	 * @param liveMessageId the liveMessageId to set
	 */
	public void setLiveMessageId(String liveMessageId) {
		this.liveMessageId = liveMessageId;
	}

	/**
	 * @return the liveMessageStatusId
	 */
	public String getLiveMessageStatusId() {
		return liveMessageStatusId;
	}

	/**
	 * @param liveMessageStatusId the liveMessageStatusId to set
	 */
	public void setLiveMessageStatusId(String liveMessageStatusId) {
		this.liveMessageStatusId = liveMessageStatusId;
	}

	/**
	 * @return the liveMessageName
	 */
	public String getLiveMessageName() {
		return liveMessageName;
	}

	/**
	 * @param liveMessageName the liveMessageName to set
	 */
	public void setLiveMessageName(String liveMessageName) {
		this.liveMessageName = liveMessageName;
	}

	/**
	 * @return the liveMessageSubject
	 */
	public String getLiveMessageSubject() {
		return liveMessageSubject;
	}

	/**
	 * @param liveMessageSubject the liveMessageSubject to set
	 */
	public void setLiveMessageSubject(String liveMessageSubject) {
		this.liveMessageSubject = liveMessageSubject;
	}

	/**
	 * @return the liveMessage
	 */
	public String getLiveMessage() {
		return liveMessage;
	}

	/**
	 * @param liveMessage the liveMessage to set
	 */
	public void setLiveMessage(String liveMessage) {
		this.liveMessage = liveMessage;
	}

	/**
	 * @return the isPopUp
	 */
	public boolean isPopUp() {
		return isPopUp;
	}

	/**
	 * @param isPopUp the isPopUp to set
	 */
	public void setPopUp(boolean isPopUp) {
		this.isPopUp = isPopUp;
	}

	/**
	 * @return the usersTbl
	 */
	public UsersTbl getUsersTbl() {
		return usersTbl;
	}

	/**
	 * @param usersTbl the usersTbl to set
	 */
	public void setUsersTbl(UsersTbl usersTbl) {
		this.usersTbl = usersTbl;
	}

	/**
	 * @return the liveMsgStatus
	 */
	public String getLiveMsgStatus() {
		return liveMsgStatus;
	}

	/**
	 * @param liveMsgStatus the liveMsgStatus to set
	 */
	public void setLiveMsgStatus(String liveMsgStatus) {
		this.liveMsgStatus = liveMsgStatus;
	}
	
}
