package com.magna.xmbackend.vo.rel;

import java.util.List;

/**
 *
 * @author dhana
 */
public class AdminAreaProjectRelBatchRequest {

    private List<AdminAreaProjectRelRequest> adminAreaProjectRelRequests;

    public AdminAreaProjectRelBatchRequest() {
    }

    /**
     *
     * @param adminAreaProjectRelRequests
     */
    public AdminAreaProjectRelBatchRequest(List<AdminAreaProjectRelRequest> adminAreaProjectRelRequests) {
        this.adminAreaProjectRelRequests = adminAreaProjectRelRequests;
    }

    /**
     *
     * @return List
     */
    public final List<AdminAreaProjectRelRequest> getAdminAreaProjectRelRequests() {
        return adminAreaProjectRelRequests;
    }

    /**
     *
     * @param adminAreaProjectRelRequests
     */
    public final void setAdminAreaProjectRelRequests(final List<AdminAreaProjectRelRequest> adminAreaProjectRelRequests) {
        this.adminAreaProjectRelRequests = adminAreaProjectRelRequests;
    }

}
