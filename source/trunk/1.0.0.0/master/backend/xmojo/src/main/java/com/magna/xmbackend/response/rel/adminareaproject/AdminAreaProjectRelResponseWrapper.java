/**
 * 
 */
package com.magna.xmbackend.response.rel.adminareaproject;

import java.util.List;

/**
 * @author Bhabadyuti Bal
 *
 */
public class AdminAreaProjectRelResponseWrapper {

	List<AdminAreaProjectRelationResponse> adminAreaProjRelResps;
	
	public AdminAreaProjectRelResponseWrapper() {
	}

	/**
	 * @param adminAreaProjRelResps
	 */
	public AdminAreaProjectRelResponseWrapper(List<AdminAreaProjectRelationResponse> adminAreaProjRelResps) {
		this.adminAreaProjRelResps = adminAreaProjRelResps;
	}

	/**
	 * @return the adminAreaProjRelResps
	 */
	public List<AdminAreaProjectRelationResponse> getAdminAreaProjRelResps() {
		return adminAreaProjRelResps;
	}

	/**
	 * @param adminAreaProjRelResps the adminAreaProjRelResps to set
	 */
	public void setAdminAreaProjRelResps(List<AdminAreaProjectRelationResponse> adminAreaProjRelResps) {
		this.adminAreaProjRelResps = adminAreaProjRelResps;
	}
	
	
}
