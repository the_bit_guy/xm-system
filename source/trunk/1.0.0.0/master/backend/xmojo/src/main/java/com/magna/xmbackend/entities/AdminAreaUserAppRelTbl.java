/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magna.xmbackend.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author Admin
 */
@Entity
@Table(name = "ADMIN_AREA_USER_APP_REL_TBL")
@NamedQueries({
    @NamedQuery(name = "AdminAreaUserAppRelTbl.findAll", query = "SELECT a FROM AdminAreaUserAppRelTbl a")})
public class AdminAreaUserAppRelTbl implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "ADMIN_AREA_USER_APP_REL_ID")
    private String adminAreaUserAppRelId;
    @Basic(optional = false)
    @Column(name = "STATUS")
    private String status;
    @Basic(optional = false)
    @Column(name = "REL_TYPE")
    private String relType;
    @JoinColumn(name = "SITE_ADMIN_AREA_REL_ID", referencedColumnName = "SITE_ADMIN_AREA_REL_ID")
    @ManyToOne(optional = false)
    private SiteAdminAreaRelTbl siteAdminAreaRelId;
    @JoinColumn(name = "USER_APPLICATION_ID", referencedColumnName = "USER_APPLICATION_ID")
    @ManyToOne(optional = false)
    private UserApplicationsTbl userApplicationId;

    public AdminAreaUserAppRelTbl() {
    }

    public AdminAreaUserAppRelTbl(String adminAreaUserAppRelId) {
        this.adminAreaUserAppRelId = adminAreaUserAppRelId;
    }

    public AdminAreaUserAppRelTbl(String adminAreaUserAppRelId, String status, String relType) {
        this.adminAreaUserAppRelId = adminAreaUserAppRelId;
        this.status = status;
        this.relType = relType;
    }

    public String getAdminAreaUserAppRelId() {
        return adminAreaUserAppRelId;
    }

    public void setAdminAreaUserAppRelId(String adminAreaUserAppRelId) {
        this.adminAreaUserAppRelId = adminAreaUserAppRelId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getRelType() {
        return relType;
    }

    public void setRelType(String relType) {
        this.relType = relType;
    }

    public SiteAdminAreaRelTbl getSiteAdminAreaRelId() {
        return siteAdminAreaRelId;
    }

    public void setSiteAdminAreaRelId(SiteAdminAreaRelTbl siteAdminAreaRelId) {
        this.siteAdminAreaRelId = siteAdminAreaRelId;
    }

    public UserApplicationsTbl getUserApplicationId() {
        return userApplicationId;
    }

    public void setUserApplicationId(UserApplicationsTbl userApplicationId) {
        this.userApplicationId = userApplicationId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (adminAreaUserAppRelId != null ? adminAreaUserAppRelId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof AdminAreaUserAppRelTbl)) {
            return false;
        }
        AdminAreaUserAppRelTbl other = (AdminAreaUserAppRelTbl) object;
        if ((this.adminAreaUserAppRelId == null && other.adminAreaUserAppRelId != null) || (this.adminAreaUserAppRelId != null && !this.adminAreaUserAppRelId.equals(other.adminAreaUserAppRelId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.magna.xmbackend.entities.AdminAreaUserAppRelTbl[ adminAreaUserAppRelId=" + adminAreaUserAppRelId + " ]";
    }
    
}
