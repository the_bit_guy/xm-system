package com.magna.xmbackend.vo.site;

import com.magna.xmbackend.vo.BaseObject;
import java.io.Serializable;

public class SiteDetail extends BaseObject implements Serializable {

    private String siteName;
    private String siteDescription;
    private String siteRemarks;
    private String languageCode;

    /**
     *
     * @return siteName
     */
    public final String getSiteName() {
        return siteName;
    }

    /**
     *
     * @param siteName
     */
    public final void setSiteName(final String siteName) {
        this.siteName = siteName;
    }

    /**
     *
     * @return siteDescription
     */
    public final String getSiteDescription() {
        return siteDescription;
    }

    /**
     *
     * @param siteDescription
     */
    public final void setSiteDescription(final String siteDescription) {
        this.siteDescription = siteDescription;
    }

    /**
     *
     * @return siteRemarks
     */
    public final String getSiteRemarks() {
        return siteRemarks;
    }

    /**
     *
     * @param siteRemarks
     */
    public final void setSiteRemarks(final String siteRemarks) {
        this.siteRemarks = siteRemarks;
    }

    /**
     *
     * @return languageCode
     */
    public final String getLanguageCode() {
        return languageCode;
    }

    /**
     *
     * @param languageCode
     */
    public final void setLanguageCode(final String languageCode) {
        this.languageCode = languageCode;
    }

    /**
     *
     * @return object as string
     */
    @Override
    public final String toString() {
        return new StringBuilder().append("SiteDetail{")
                .append("siteName=").append(siteName)
                .append(", siteDescription=").append(siteDescription)
                .append(", siteRemarks=").append(siteRemarks)
                .append(", languageCode=").append(languageCode)
                .append(", xMObject=").append(getxMObject())
                .append("}").toString();
    }
}
