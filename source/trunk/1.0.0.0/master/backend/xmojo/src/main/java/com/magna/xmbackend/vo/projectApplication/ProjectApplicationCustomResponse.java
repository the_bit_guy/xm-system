/**
 * 
 */
package com.magna.xmbackend.vo.projectApplication;

/**
 * @author Roshan Ekka
 *
 */
public class ProjectApplicationCustomResponse {
	
	private String isSingleton;
    private String isParent;
    private String position;
    private String baseAppName;
    
	public String getIsSingleton() {
		return isSingleton;
	}
	public void setIsSingleton(String isSingleton) {
		this.isSingleton = isSingleton;
	}
	public String getIsParent() {
		return isParent;
	}
	public void setIsParent(String isParent) {
		this.isParent = isParent;
	}
	public String getPosition() {
		return position;
	}
	public void setPosition(String position) {
		this.position = position;
	}
	public String getBaseAppName() {
		return baseAppName;
	}
	public void setBaseAppName(String baseAppName) {
		this.baseAppName = baseAppName;
	}

}
