/**
 * 
 */
package com.magna.xmbackend.vo.enums;

import java.io.Serializable;

/**
 * @author Bhabadyuti Bal
 *
 */
public enum ProjectExpiryKey implements Serializable{
	PROJECT_EXPIRY_DAYS, PROJECT_EXPIRY_GRACE_PERIOD, DATA_MIGRATION_DATE
}
