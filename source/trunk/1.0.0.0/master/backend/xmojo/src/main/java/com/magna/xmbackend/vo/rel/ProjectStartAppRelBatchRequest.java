package com.magna.xmbackend.vo.rel;

import java.util.List;

/**
 *
 * @author vijay
 */
public class ProjectStartAppRelBatchRequest {

    private List<ProjectStartAppRelRequest> projectStartAppRelRequests;

    public ProjectStartAppRelBatchRequest() {
    }

    /**
     *
     * @param projectStartAppRelRequests
     */
    public ProjectStartAppRelBatchRequest(List<ProjectStartAppRelRequest> projectStartAppRelRequests) {
        this.projectStartAppRelRequests = projectStartAppRelRequests;
    }

    /**
     *
     * @return List
     */
    public final List<ProjectStartAppRelRequest> getProjectStartAppRelRequests() {
        return projectStartAppRelRequests;
    }

    /**
     *
     * @param projectStartAppRelRequests
     */
    public final void setProjectStartAppRelRequests(final List<ProjectStartAppRelRequest> projectStartAppRelRequests) {
        this.projectStartAppRelRequests = projectStartAppRelRequests;
    }

}
