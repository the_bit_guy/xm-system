package com.magna.xmbackend.vo;

import java.io.Serializable;

public class ResponseObject implements Serializable {

    private BaseObject baseObject;

    private Message message;

    public ResponseObject() {
    }

    /**
     *
     * @param baseObject
     * @param message
     */
    public ResponseObject(BaseObject baseObject, Message message) {
        this.baseObject = baseObject;
        this.message = message;
    }

    /**
     * @return the baseObject
     */
    public final BaseObject getBaseObject() {
        return baseObject;
    }

    /**
     * @param baseObject the baseObject to set
     */
    public final void setBaseObject(final BaseObject baseObject) {
        this.baseObject = baseObject;
    }

    /**
     * @return the message
     */
    public final Message getMessage() {
        return message;
    }

    /**
     * @param message the message to set
     */
    public final void setMessage(final Message message) {
        this.message = message;
    }

    /**
     *
     * @return object as string
     */
    @Override
    public String toString() {
        return new StringBuilder().append("ResponseObject{")
                .append(", baseObject=")
                .append(baseObject).append(", message=").append(message)
                .append("}").toString();
    }
}
