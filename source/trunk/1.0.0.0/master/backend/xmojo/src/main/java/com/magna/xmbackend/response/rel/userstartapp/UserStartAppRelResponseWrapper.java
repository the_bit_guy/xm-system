package com.magna.xmbackend.response.rel.userstartapp;

import java.util.List;

public class UserStartAppRelResponseWrapper {

	List<UserStartAppRelation> userStartAppRelations;
	
	public UserStartAppRelResponseWrapper() {
	}

	/**
	 * @param userStartAppRelations
	 */
	public UserStartAppRelResponseWrapper(List<UserStartAppRelation> userStartAppRelations) {
		this.userStartAppRelations = userStartAppRelations;
	}

	/**
	 * @return the userStartAppRelations
	 */
	public List<UserStartAppRelation> getUserStartAppRelations() {
		return userStartAppRelations;
	}

	/**
	 * @param userStartAppRelations the userStartAppRelations to set
	 */
	public void setUserStartAppRelations(List<UserStartAppRelation> userStartAppRelations) {
		this.userStartAppRelations = userStartAppRelations;
	}
	
	
}
