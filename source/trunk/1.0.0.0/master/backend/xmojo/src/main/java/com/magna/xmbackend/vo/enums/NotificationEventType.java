/**
 * 
 */
package com.magna.xmbackend.vo.enums;

/**
 * @author Bhabadyuti Bal
 *
 */
public enum NotificationEventType {

	/** The create object. */
	PROJECT_CREATE,
	
	/** The delete objec. */
	PROJECT_DELETE, 
	
	/** The project deactivate. */
	PROJECT_DEACTIVATE,
	
	/** The project activate. */
	PROJECT_ACTIVATE,
	
	/** The user project relation assign. */
	USER_PROJECT_RELATION_ASSIGN,
	
	/** The user project relation remove. */
	USER_PROJECT_RELATION_REMOVE,
	
	/** The project admin area relation assign. */
	PROJECT_ADMIN_AREA_RELATION_ASSIGN,
	
	/** The project admin area relation remove. */
	PROJECT_ADMIN_AREA_RELATION_REMOVE,
	
	/** The user project relation expiry. */
	USER_PROJECT_RELATION_EXPIRY,
	
	/** The user project relation grace expiry. */
	USER_PROJECT_RELATION_GRACE_EXPIRY,
	
	/** The admin notify to project users. */
	EMAIL_NOTIFY_TO_PROJECT_USERS,
	
	
	/** The admin nofiy to aa project users. */
	EMAIL_NOTIFY_TO_AA_PROJECT_USERS;
	

	/**
	 * Gets the notification criteria.
	 *
	 * @return the notification criteria
	 */
	public static String[] getNotificationCriteria() {
		NotificationEventType[] values = values();
		String[] returnArray = new String[values.length];
		for (int index = 0; index < values.length; index++) {
			returnArray[index] = values[index].name();
		}
		return returnArray;
	}
}
