package com.magna.xmbackend.vo.rel;

import com.magna.xmbackend.entities.AdminAreaProjectRelTbl;
import com.magna.xmbackend.entities.StartApplicationsTbl;

/**
 *
 * @author vijay
 */
public class AdminAreaProjectRelIdWithProjectStartAppRel {

    private String projectStartAppRelId;
    private AdminAreaProjectRelTbl adminAreaProjectRelId;
    private StartApplicationsTbl startApplicationsTbl;
    private String status;

    public AdminAreaProjectRelIdWithProjectStartAppRel() {
    }

    /**
     *
     * @param adminAreaProjectRelId
     * @param startApplicationsTbl
     * @param projectStartAppRelId
     * @param status
     */
    public AdminAreaProjectRelIdWithProjectStartAppRel(AdminAreaProjectRelTbl adminAreaProjectRelId,
            StartApplicationsTbl startApplicationsTbl,
            String projectStartAppRelId, String status) {
        this.adminAreaProjectRelId = adminAreaProjectRelId;
        this.startApplicationsTbl = startApplicationsTbl;
        this.projectStartAppRelId = projectStartAppRelId;
        this.status = status;
    }

    /**
     * @return the adminAreaProjectRelId
     */
    public final AdminAreaProjectRelTbl getAdminAreaProjectRelId() {
        return adminAreaProjectRelId;
    }

    /**
     * @param adminAreaProjectRelId the adminAreaProjectRelId to set
     */
    public final void setAdminAreaProjectRelId(final AdminAreaProjectRelTbl adminAreaProjectRelId) {
        this.adminAreaProjectRelId = adminAreaProjectRelId;
    }

    /**
     * @return the startApplicationsTbl
     */
    public final StartApplicationsTbl getStartApplicationsTbl() {
        return startApplicationsTbl;
    }

    /**
     * @param startApplicationsTbl the startApplicationsTbl to set
     */
    public final void setStartApplicationsTbl(final StartApplicationsTbl startApplicationsTbl) {
        this.startApplicationsTbl = startApplicationsTbl;
    }

    /**
     * @return the projectStartAppRelId
     */
    public final String getProjectStartAppRelId() {
        return projectStartAppRelId;
    }

    /**
     * @param projectStartAppRelId the projectStartAppRelId to set
     */
    public final void setProjectStartAppRelId(final String projectStartAppRelId) {
        this.projectStartAppRelId = projectStartAppRelId;
    }

    /**
     * @return the status
     */
    public final String getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public final void setStatus(String status) {
        this.status = status;
    }
}
