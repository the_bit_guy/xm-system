/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magna.xmbackend.entities;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonBackReference;

/**
 *
 * @author Admin
 */
@Entity
@Table(name = "ADMIN_AREAS_TBL")
@NamedQueries({
    @NamedQuery(name = "AdminAreasTbl.findAll", query = "SELECT a FROM AdminAreasTbl a")})
public class AdminAreasTbl implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "ADMIN_AREA_ID")
    private String adminAreaId;
    @Basic(optional = false)
    @Column(name = "NAME")
    private String name;
    @Column(name = "SINGLETON_APP_TIMEOUT")
    private Long singletonAppTimeout;
    @Basic(optional = false)
    @Column(name = "HOTLINE_CONTACT_NUMBER")
    private String hotlineContactNumber;
    @Basic(optional = false)
    @Column(name = "HOTLINE_CONTACT_EMAIL")
    private String hotlineContactEmail;
    @Basic(optional = false)
    @Column(name = "STATUS")
    private String status;
    @Basic(optional = false)
    @Column(name = "CREATE_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createDate;
    @Basic(optional = false)
    @Column(name = "UPDATE_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateDate;
    @JsonBackReference
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "adminAreaId")
    private Collection<RoleAdminAreaRelTbl> roleAdminAreaRelTblCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "adminAreaId")
    private Collection<AdminAreaTranslationTbl> adminAreaTranslationTblCollection;
    @JsonBackReference
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "adminAreaId")
    private Collection<SiteAdminAreaRelTbl> siteAdminAreaRelTblCollection;
    @JoinColumn(name = "ICON_ID", referencedColumnName = "ICON_ID")
    @ManyToOne
    private IconsTbl iconId;

    public AdminAreasTbl() {
    }

    public AdminAreasTbl(String adminAreaId) {
        this.adminAreaId = adminAreaId;
    }

    public AdminAreasTbl(String adminAreaId, String name, String status, Date createDate, Date updateDate) {
        this.adminAreaId = adminAreaId;
        this.name = name;
        this.status = status;
        this.createDate = createDate;
        this.updateDate = updateDate;
    }

    public String getAdminAreaId() {
        return adminAreaId;
    }

    public void setAdminAreaId(String adminAreaId) {
        this.adminAreaId = adminAreaId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getSingletonAppTimeout() {
        return singletonAppTimeout;
    }

    public void setSingletonAppTimeout(Long singletonAppTimeout) {
        this.singletonAppTimeout = singletonAppTimeout;
    }

    public String getHotlineContactNumber() {
        return hotlineContactNumber;
    }

    public void setHotlineContactNumber(String hotlineContactNumber) {
        this.hotlineContactNumber = hotlineContactNumber;
    }

    public String getHotlineContactEmail() {
        return hotlineContactEmail;
    }

    public void setHotlineContactEmail(String hotlineContactEmail) {
        this.hotlineContactEmail = hotlineContactEmail;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public Collection<RoleAdminAreaRelTbl> getRoleAdminAreaRelTblCollection() {
        return roleAdminAreaRelTblCollection;
    }

    public void setRoleAdminAreaRelTblCollection(Collection<RoleAdminAreaRelTbl> roleAdminAreaRelTblCollection) {
        this.roleAdminAreaRelTblCollection = roleAdminAreaRelTblCollection;
    }

    public Collection<AdminAreaTranslationTbl> getAdminAreaTranslationTblCollection() {
        return adminAreaTranslationTblCollection;
    }

    public void setAdminAreaTranslationTblCollection(Collection<AdminAreaTranslationTbl> adminAreaTranslationTblCollection) {
        this.adminAreaTranslationTblCollection = adminAreaTranslationTblCollection;
    }

    public Collection<SiteAdminAreaRelTbl> getSiteAdminAreaRelTblCollection() {
        return siteAdminAreaRelTblCollection;
    }

    public void setSiteAdminAreaRelTblCollection(Collection<SiteAdminAreaRelTbl> siteAdminAreaRelTblCollection) {
        this.siteAdminAreaRelTblCollection = siteAdminAreaRelTblCollection;
    }

    public IconsTbl getIconId() {
        return iconId;
    }

    public void setIconId(IconsTbl iconId) {
        this.iconId = iconId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (adminAreaId != null ? adminAreaId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof AdminAreasTbl)) {
            return false;
        }
        AdminAreasTbl other = (AdminAreasTbl) object;
        if ((this.adminAreaId == null && other.adminAreaId != null) || (this.adminAreaId != null && !this.adminAreaId.equals(other.adminAreaId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.magna.xmbackend.entities.AdminAreasTbl[ adminAreaId=" + adminAreaId + " ]";
    }
    
}
