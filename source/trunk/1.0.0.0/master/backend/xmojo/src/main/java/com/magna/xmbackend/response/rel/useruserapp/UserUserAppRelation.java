/**
 * 
 */
package com.magna.xmbackend.response.rel.useruserapp;

import com.magna.xmbackend.entities.UserApplicationsTbl;
import com.magna.xmbackend.entities.UsersTbl;

/**
 * @author Bhabadyuti Bal
 *
 */
public class UserUserAppRelation {

	private String userUserAppRelId;
    private String userRelType;
    private UsersTbl userId;
    private UserApplicationsTbl userApplicationId;
    
    public UserUserAppRelation() {
	}

	/**
	 * @param userUserAppRelId
	 * @param userRelType
	 * @param userId
	 * @param userApplicationId
	 */
	public UserUserAppRelation(String userUserAppRelId, String userRelType, UsersTbl userId,
			UserApplicationsTbl userApplicationId) {
		this.userUserAppRelId = userUserAppRelId;
		this.userRelType = userRelType;
		this.userId = userId;
		this.userApplicationId = userApplicationId;
	}

	/**
	 * @return the userUserAppRelId
	 */
	public String getUserUserAppRelId() {
		return userUserAppRelId;
	}

	/**
	 * @param userUserAppRelId the userUserAppRelId to set
	 */
	public void setUserUserAppRelId(String userUserAppRelId) {
		this.userUserAppRelId = userUserAppRelId;
	}

	/**
	 * @return the userRelType
	 */
	public String getUserRelType() {
		return userRelType;
	}

	/**
	 * @param userRelType the userRelType to set
	 */
	public void setUserRelType(String userRelType) {
		this.userRelType = userRelType;
	}

	/**
	 * @return the userId
	 */
	public UsersTbl getUserId() {
		return userId;
	}

	/**
	 * @param userId the userId to set
	 */
	public void setUserId(UsersTbl userId) {
		this.userId = userId;
	}

	/**
	 * @return the userApplicationId
	 */
	public UserApplicationsTbl getUserApplicationId() {
		return userApplicationId;
	}

	/**
	 * @param userApplicationId the userApplicationId to set
	 */
	public void setUserApplicationId(UserApplicationsTbl userApplicationId) {
		this.userApplicationId = userApplicationId;
	}
    
    
}
