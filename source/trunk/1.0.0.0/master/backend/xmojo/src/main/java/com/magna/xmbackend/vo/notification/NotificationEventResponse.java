package com.magna.xmbackend.vo.notification;

import java.util.List;

import com.magna.xmbackend.entities.EmailNotificationEventTbl;

public class NotificationEventResponse {

	List<EmailNotificationEventTbl> notificationEventTbls;
	
	public NotificationEventResponse() {
	}

	public NotificationEventResponse(List<EmailNotificationEventTbl> notificationEventTbls) {
		this.notificationEventTbls = notificationEventTbls;
	}

	public List<EmailNotificationEventTbl> getNotificationEventTbls() {
		return notificationEventTbls;
	}

	public void setNotificationEventTbls(List<EmailNotificationEventTbl> notificationEventTbls) {
		this.notificationEventTbls = notificationEventTbls;
	}
	
	
}
