/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magna.xmbackend.entities;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;

/**
 *
 * @author Admin
 */
@Entity
@Table(name = "LANGUAGES_TBL")
@NamedQueries({
    @NamedQuery(name = "LanguagesTbl.findAll", query = "SELECT l FROM LanguagesTbl l")})
public class LanguagesTbl implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "LANGUAGE_CODE")
    private String languageCode;
    @Basic(optional = false)
    @Column(name = "LANGUAGE_NAME")
    private String languageName;
    @JsonBackReference
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "languageCode")
    private Collection<BaseAppTranslationTbl> baseAppTranslationTblCollection;
    @JsonBackReference
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "languageCode")
    private Collection<SiteTranslationTbl> siteTranslationTblCollection;
    @JsonBackReference
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "languageCode")
    private Collection<UserTranslationTbl> userTranslationTblCollection;
    @JsonBackReference
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "languageCode")
    private Collection<DirectoryTranslationTbl> directoryTranslationTblCollection;
    @JsonBackReference
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "languageCode")
    private Collection<GroupTranslationTbl> groupTranslationTblCollection;
    @JsonBackReference
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "languageCode")
    private Collection<AdminAreaTranslationTbl> adminAreaTranslationTblCollection;
    @JsonBackReference
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "languageCode")
    private Collection<StartAppTranslationTbl> startAppTranslationTblCollection;
    @JsonBackReference
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "languageCode")
    private Collection<ProjectAppTranslationTbl> projectAppTranslationTblCollection;
    @JsonBackReference
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "languageCode")
    private Collection<UserAppTranslationTbl> userAppTranslationTblCollection;
    @JsonBackReference
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "languageCode")
    private Collection<ProjectTranslationTbl> projectTranslationTblCollection;

    public LanguagesTbl() {
    }

    public LanguagesTbl(String languageCode) {
        this.languageCode = languageCode;
    }

    public LanguagesTbl(String languageCode, String languageName) {
        this.languageCode = languageCode;
        this.languageName = languageName;
    }

    public String getLanguageCode() {
        return languageCode;
    }

    public void setLanguageCode(String languageCode) {
        this.languageCode = languageCode;
    }

    public String getLanguageName() {
        return languageName;
    }

    public void setLanguageName(String languageName) {
        this.languageName = languageName;
    }

    public Collection<BaseAppTranslationTbl> getBaseAppTranslationTblCollection() {
        return baseAppTranslationTblCollection;
    }

    public void setBaseAppTranslationTblCollection(Collection<BaseAppTranslationTbl> baseAppTranslationTblCollection) {
        this.baseAppTranslationTblCollection = baseAppTranslationTblCollection;
    }

    public Collection<SiteTranslationTbl> getSiteTranslationTblCollection() {
        return siteTranslationTblCollection;
    }

    public void setSiteTranslationTblCollection(Collection<SiteTranslationTbl> siteTranslationTblCollection) {
        this.siteTranslationTblCollection = siteTranslationTblCollection;
    }

    public Collection<UserTranslationTbl> getUserTranslationTblCollection() {
        return userTranslationTblCollection;
    }

    public void setUserTranslationTblCollection(Collection<UserTranslationTbl> userTranslationTblCollection) {
        this.userTranslationTblCollection = userTranslationTblCollection;
    }

    public Collection<DirectoryTranslationTbl> getDirectoryTranslationTblCollection() {
        return directoryTranslationTblCollection;
    }

    public void setDirectoryTranslationTblCollection(Collection<DirectoryTranslationTbl> directoryTranslationTblCollection) {
        this.directoryTranslationTblCollection = directoryTranslationTblCollection;
    }

    public Collection<GroupTranslationTbl> getGroupTranslationTblCollection() {
        return groupTranslationTblCollection;
    }

    public void setGroupTranslationTblCollection(Collection<GroupTranslationTbl> groupTranslationTblCollection) {
        this.groupTranslationTblCollection = groupTranslationTblCollection;
    }

    public Collection<AdminAreaTranslationTbl> getAdminAreaTranslationTblCollection() {
        return adminAreaTranslationTblCollection;
    }

    public void setAdminAreaTranslationTblCollection(Collection<AdminAreaTranslationTbl> adminAreaTranslationTblCollection) {
        this.adminAreaTranslationTblCollection = adminAreaTranslationTblCollection;
    }

    public Collection<StartAppTranslationTbl> getStartAppTranslationTblCollection() {
        return startAppTranslationTblCollection;
    }

    public void setStartAppTranslationTblCollection(Collection<StartAppTranslationTbl> startAppTranslationTblCollection) {
        this.startAppTranslationTblCollection = startAppTranslationTblCollection;
    }

    public Collection<ProjectAppTranslationTbl> getProjectAppTranslationTblCollection() {
        return projectAppTranslationTblCollection;
    }

    public void setProjectAppTranslationTblCollection(Collection<ProjectAppTranslationTbl> projectAppTranslationTblCollection) {
        this.projectAppTranslationTblCollection = projectAppTranslationTblCollection;
    }

    public Collection<UserAppTranslationTbl> getUserAppTranslationTblCollection() {
        return userAppTranslationTblCollection;
    }

    public void setUserAppTranslationTblCollection(Collection<UserAppTranslationTbl> userAppTranslationTblCollection) {
        this.userAppTranslationTblCollection = userAppTranslationTblCollection;
    }

    public Collection<ProjectTranslationTbl> getProjectTranslationTblCollection() {
        return projectTranslationTblCollection;
    }

    public void setProjectTranslationTblCollection(Collection<ProjectTranslationTbl> projectTranslationTblCollection) {
        this.projectTranslationTblCollection = projectTranslationTblCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (languageCode != null ? languageCode.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof LanguagesTbl)) {
            return false;
        }
        LanguagesTbl other = (LanguagesTbl) object;
        if ((this.languageCode == null && other.languageCode != null) || (this.languageCode != null && !this.languageCode.equals(other.languageCode))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.magna.xmbackend.entities.LanguagesTbl[ languageCode=" + languageCode + " ]";
    }
    
}
