/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magna.xmbackend.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author dhana
 */
@Entity
@Table(name = "USER_TKT")
@NamedQueries({
    @NamedQuery(name = "UserTkt.findAll", query = "SELECT u FROM UserTkt u")})
public class UserTkt implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "USER_TKT_ID")
    private String userTktId;
    @Basic(optional = false)
    @Column(name = "USERNAME")
    private String username;
    @Basic(optional = false)
    @Column(name = "APPLICATION_NAME")
    private String applicationName;
    @Basic(optional = false)
    @Column(name = "TKT")
    private String tkt;
    @Basic(optional = false)
    @Column(name="HOSTNAME")
    private String hostName;
    @Basic(optional = false)
    @Column(name = "CREATE_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createDate;
    @Basic(optional = false)
    @Column(name = "UPDATE_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateDate;

    public UserTkt() {
    }

    public UserTkt(String userTktId) {
        this.userTktId = userTktId;
    }

	public UserTkt(String userTktId, String username, String applicationName, String tkt, String hostName, Date createDate, Date updateDate) {
        this.userTktId = userTktId;
        this.username = username;
        this.applicationName = applicationName;
        this.tkt = tkt;
        this.hostName = hostName;
        this.createDate = createDate;
        this.updateDate = updateDate;
    }
	
    public String getUserTktId() {
        return userTktId;
    }

    public void setUserTktId(String userTktId) {
        this.userTktId = userTktId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getTkt() {
        return tkt;
    }

    public void setTkt(String tkt) {
        this.tkt = tkt;
    }
    
	public String getApplicationName() {
		return applicationName;
	}

	public void setApplicationName(String applicationName) {
		this.applicationName = applicationName;
	}

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (userTktId != null ? userTktId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof UserTkt)) {
            return false;
        }
        UserTkt other = (UserTkt) object;
        if ((this.userTktId == null && other.userTktId != null) || (this.userTktId != null && !this.userTktId.equals(other.userTktId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.magna.xmbackend.entities.UserTkt[ userTktId=" + userTktId + " ]";
    }
    
}
