package com.magna.xmbackend.vo.projectApplication;

import java.util.List;
import java.util.Map;

import com.magna.xmbackend.entities.ProjectApplicationsTbl;

public class ProjectApplicationResponse {

    private Iterable<ProjectApplicationsTbl> projectApplicationsTbls;
    private List<Map<String, String>> statusMaps;
    private List<String> statusUpdatationFailedList;

    public ProjectApplicationResponse() {
    }

    
    
    /**
	 * @return the statusMaps
	 */
	public List<Map<String, String>> getStatusMaps() {
		return statusMaps;
	}



	/**
	 * @param statusMaps the statusMaps to set
	 */
	public void setStatusMaps(List<Map<String, String>> statusMaps) {
		this.statusMaps = statusMaps;
	}



	/**
	 * @param statusMaps
	 */
	public ProjectApplicationResponse(List<Map<String, String>> statusMaps) {
		this.statusMaps = statusMaps;
	}



	public ProjectApplicationResponse(Iterable<ProjectApplicationsTbl> projectApplicationsTbls) {
        this.projectApplicationsTbls = projectApplicationsTbls;
    }

    public void setProjectApplicationsTbls(Iterable<ProjectApplicationsTbl> projectApplicationsTbls) {
        this.projectApplicationsTbls = projectApplicationsTbls;
    }

    public Iterable<ProjectApplicationsTbl> getProjectApplicationsTbls() {
        return projectApplicationsTbls;
    }



	/**
	 * @return the statusUpdatationFailedList
	 */
	public List<String> getStatusUpdatationFailedList() {
		return statusUpdatationFailedList;
	}



	/**
	 * @param statusUpdatationFailedList the statusUpdatationFailedList to set
	 */
	public void setStatusUpdatationFailedList(List<String> statusUpdatationFailedList) {
		this.statusUpdatationFailedList = statusUpdatationFailedList;
	}

}
