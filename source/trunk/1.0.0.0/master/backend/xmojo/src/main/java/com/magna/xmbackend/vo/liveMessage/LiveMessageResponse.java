/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magna.xmbackend.vo.liveMessage;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 *
 * @author dhana
 */
public class LiveMessageResponse {
    
	private String liveMessageId;
    private String liveMessageName;
    private String subject;
    private String message;
    private String ispopup;
    private Date startDatetime;
    private Date endDatetime;
    private List<LiveMessageConfigResponse> liveMessageConfigResponses;
    private List<LiveMessageStatusResponse> liveMessageStatusResponses;
    private List<LiveMessageTranslation> liveMessageTranslations;
    private List<Map<String, String>> statusMaps;
    
    
    /**
	 * @return the liveMessageTranslations
	 */
	public List<LiveMessageTranslation> getLiveMessageTranslations() {
		return liveMessageTranslations;
	}

	/**
	 * @param liveMessageTranslations the liveMessageTranslations to set
	 */
	public void setLiveMessageTranslations(List<LiveMessageTranslation> liveMessageTranslations) {
		this.liveMessageTranslations = liveMessageTranslations;
	}

	/**
	 * @return the statusMaps
	 */
	public List<Map<String, String>> getStatusMaps() {
		return statusMaps;
	}

	/**
	 * @param statusMaps the statusMaps to set
	 */
	public void setStatusMaps(List<Map<String, String>> statusMaps) {
		this.statusMaps = statusMaps;
	}

	/**
	 * @param statusMaps
	 */
	public LiveMessageResponse(List<Map<String, String>> statusMaps) {
		this.statusMaps = statusMaps;
	}

	public LiveMessageResponse() {
	}

	/**
	 * @return the liveMessageId
	 */
	public String getLiveMessageId() {
		return liveMessageId;
	}

	/**
	 * @param liveMessageId the liveMessageId to set
	 */
	public void setLiveMessageId(String liveMessageId) {
		this.liveMessageId = liveMessageId;
	}

	/**
	 * @return the liveMessageName
	 */
	public String getLiveMessageName() {
		return liveMessageName;
	}

	/**
	 * @param liveMessageName the liveMessageName to set
	 */
	public void setLiveMessageName(String liveMessageName) {
		this.liveMessageName = liveMessageName;
	}

	/**
	 * @return the subject
	 */
	public String getSubject() {
		return subject;
	}

	/**
	 * @param subject the subject to set
	 */
	public void setSubject(String subject) {
		this.subject = subject;
	}

	/**
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * @param message the message to set
	 */
	public void setMessage(String message) {
		this.message = message;
	}

	/**
	 * @return the ispopup
	 */
	public String getIspopup() {
		return ispopup;
	}

	/**
	 * @param ispopup the ispopup to set
	 */
	public void setIspopup(String ispopup) {
		this.ispopup = ispopup;
	}

	/**
	 * @return the liveMessageConfigResponses
	 */
	public List<LiveMessageConfigResponse> getLiveMessageConfigResponses() {
		return liveMessageConfigResponses;
	}

	/**
	 * @param liveMessageConfigResponses the liveMessageConfigResponses to set
	 */
	public void setLiveMessageConfigResponses(
			List<LiveMessageConfigResponse> liveMessageConfigResponses) {
		this.liveMessageConfigResponses = liveMessageConfigResponses;
	}

	/**
	 * @return the startDatetime
	 */
	public Date getStartDatetime() {
		return startDatetime;
	}

	/**
	 * @param startDatetime the startDatetime to set
	 */
	public void setStartDatetime(Date startDatetime) {
		this.startDatetime = startDatetime;
	}

	/**
	 * @return the endDatetime
	 */
	public Date getEndDatetime() {
		return endDatetime;
	}

	/**
	 * @param endDatetime the endDatetime to set
	 */
	public void setEndDatetime(Date endDatetime) {
		this.endDatetime = endDatetime;
	}

	/**
	 * @return the liveMessageStatusResponses
	 */
	public List<LiveMessageStatusResponse> getLiveMessageStatusResponses() {
		return liveMessageStatusResponses;
	}

	/**
	 * @param liveMessageStatusResponses the liveMessageStatusResponses to set
	 */
	public void setLiveMessageStatusResponses(List<LiveMessageStatusResponse> liveMessageStatusResponses) {
		this.liveMessageStatusResponses = liveMessageStatusResponses;
	}
    
}
