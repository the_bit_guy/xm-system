/**
 * 
 */
package com.magna.xmbackend.vo.notification;

import java.io.Serializable;
import java.util.TreeSet;

import javax.validation.constraints.NotNull;

/**
 * @author Bhabadyuti Bal
 *
 */
public class EmailTemplateRequest implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private String emailTempalteId;
	@NotNull(message="Name cannot be empty")
	private String name;
	@NotNull(message="Subject cannot be empty")
	private String subject;
	@NotNull(message="Message cannot be empty")
	private String message;
	private TreeSet<String> templateVariables;
	
	
	public EmailTemplateRequest() {
	}
	
	public String getEmailTempalteId() {
		return emailTempalteId;
	}

	public void setEmailTempalteId(String emailTempalteId) {
		this.emailTempalteId = emailTempalteId;
	}

	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}

	public TreeSet<String> getTemplateVariables() {
		return templateVariables;
	}

	public void setTemplateVariables(TreeSet<String> templateVariables) {
		this.templateVariables = templateVariables;
	}
	
}
