/**
 * 
 */
package com.magna.xmbackend.vo.enums;

/**
 * @author Bhabadyuti Bal
 *
 */
public enum ApplicationPosition {
	BUTTONTASK("*ButtonTask*"),
	MENUTASK("*MenuTask*"),
	ICONTASK("*IconTask*");
	private String value;

	private ApplicationPosition(String value) {
		this.value = value;
	}

	public String toString() {
		return this.value;
	}
}
