/**
 * 
 */
package com.magna.xmbackend.vo.userApplication;

/**
 * @author Roshan Ekka
 *
 */
public class UserApplicationCustomResponse {
	
	private String isParent;
	private String isSingleTon;
	private String position;
	private String baseAppName;
	
    public String getIsParent() {
		return isParent;
	}
	public void setIsParent(String isParent) {
		this.isParent = isParent;
	}
	public String getIsSingleTon() {
		return isSingleTon;
	}
	public void setIsSingleTon(String isSingleTon) {
		this.isSingleTon = isSingleTon;
	}
	public String getPosition() {
		return position;
	}
	public void setPosition(String position) {
		this.position = position;
	}
	public String getBaseAppName() {
		return baseAppName;
	}
	public void setBaseAppName(String baseAppName) {
		this.baseAppName = baseAppName;
	}

}
