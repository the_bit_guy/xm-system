package com.magna.xmbackend.vo.rel;

import java.util.List;
import java.util.Map;

import com.magna.xmbackend.entities.UserUserAppRelTbl;

/**
 *
 * @author vijay
 */
public class UserUserAppRelResponse {

    private Iterable<UserUserAppRelTbl> userUserAppRelTbls;
    private List<Map<String, String>> statusMaps;

    public UserUserAppRelResponse() {
    }

    
    
    /**
	 * @return the statusMaps
	 */
	public List<Map<String, String>> getStatusMaps() {
		return statusMaps;
	}



	/**
	 * @param statusMaps the statusMaps to set
	 */
	public void setStatusMaps(List<Map<String, String>> statusMaps) {
		this.statusMaps = statusMaps;
	}



	/**
	 * @param statusMaps
	 */
	public UserUserAppRelResponse(List<Map<String, String>> statusMaps) {
		this.statusMaps = statusMaps;
	}



	/**
     *
     * @param userUserAppRelTbl
     */
    public UserUserAppRelResponse(Iterable<UserUserAppRelTbl> userUserAppRelTbl) {
        this.userUserAppRelTbls = userUserAppRelTbl;
    }

    /**
     * @return the userUserAppRelTbls
     */
    public Iterable<UserUserAppRelTbl> getUserUserAppRelTbls() {
        return userUserAppRelTbls;
    }

    /**
     * @param userUserAppRelTbls the userUserAppRelTbls to set
     */
    public void setUserUserAppRelTbls(Iterable<UserUserAppRelTbl> userUserAppRelTbls) {
        this.userUserAppRelTbls = userUserAppRelTbls;
    }

    /**
     *
     * @return object as string
     */
    @Override
    public final String toString() {
        return new StringBuilder().append("UserUserAppRelResponse{")
                .append("userUserAppRelTbls=").append(getUserUserAppRelTbls())
                .append("}").toString();
    }
}
