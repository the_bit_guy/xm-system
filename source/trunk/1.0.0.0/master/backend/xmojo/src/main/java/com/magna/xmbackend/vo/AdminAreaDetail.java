/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magna.xmbackend.vo;

public class AdminAreaDetail {

    private String name;
    private String description;
    private String remarks;
    private String languageCode;

    private CreationInfo creationInfo;

    public AdminAreaDetail() {
    }

    public AdminAreaDetail(String name, String description, String remarks, 
            String languageCode, CreationInfo creationInfo) {
        this.name = name;
        this.description = description;
        this.remarks = remarks;
        this.languageCode = languageCode;
        this.creationInfo = creationInfo;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return the remarks
     */
    public String getRemarks() {
        return remarks;
    }

    /**
     * @param remarks the remarks to set
     */
    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    /**
     * @return the languageCode
     */
    public String getLanguageCode() {
        return languageCode;
    }

    /**
     * @param languageCode the languageCode to set
     */
    public void setLanguageCode(String languageCode) {
        this.languageCode = languageCode;
    }

    /**
     * @return the creationInfo
     */
    public CreationInfo getCreationInfo() {
        return creationInfo;
    }

    /**
     * @param creationInfo the creationInfo to set
     */
    public void setCreationInfo(CreationInfo creationInfo) {
        this.creationInfo = creationInfo;
    }

}
