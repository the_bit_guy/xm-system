/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magna.xmbackend.vo.jpa.site;

import java.util.List;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.magna.xmbackend.vo.CreationInfo;
import com.magna.xmbackend.vo.enums.Status;

/**
 *
 * @author dhana
 */
public class SiteRequest {

    private String id;
    private Status status;
    
    private String iconId;
    @NotNull(message = "site name is required")
    @Size(min = 1, max = 30, message = "site name size should be more than 1 & less than 30 charachters")
    private String name;
    
    private CreationInfo creationInfo;

    private List<SiteTranslation> siteTranslations;

    public SiteRequest() {
    }

    public SiteRequest(String id, String name, Status status, String iconId, CreationInfo creationInfo, List<SiteTranslation> siteTranslations) {
        this.id = id;
        this.name = name;
        this.status = status;
        this.iconId = iconId;
        this.creationInfo = creationInfo;
        this.siteTranslations = siteTranslations;
    }

    /**
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(String id) {
        this.id = id;
    }
    
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

    /**
     * @return the status
     */
    public Status getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(Status status) {
        this.status = status;
    }

    /**
     * @return the iconId
     */
    public String getIconId() {
        return iconId;
    }

    /**
     * @param iconId the iconId to set
     */
    public void setIconId(String iconId) {
        this.iconId = iconId;
    }

    /**
     * @return the creationInfo
     */
    public CreationInfo getCreationInfo() {
        return creationInfo;
    }

    /**
     * @param creationInfo the creationInfo to set
     */
    public void setCreationInfo(CreationInfo creationInfo) {
        this.creationInfo = creationInfo;
    }

    /**
     * @return the siteTranslations
     */
    public List<SiteTranslation> getSiteTranslations() {
        return siteTranslations;
    }

    /**
     * @param siteTranslations the siteTranslations to set
     */
    public void setSiteTranslations(List<SiteTranslation> siteTranslations) {
        this.siteTranslations = siteTranslations;
    }
}
