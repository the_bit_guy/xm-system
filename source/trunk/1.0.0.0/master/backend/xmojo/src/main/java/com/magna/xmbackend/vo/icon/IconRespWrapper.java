package com.magna.xmbackend.vo.icon;

/**
 * The Class IconRespWrapper.
 */
public class IconRespWrapper {
	
	/** The icon id. */
	private String iconId;
	
	/** The icon name. */
	private String iconName;

	/**
	 * Instantiates a new icon resp wrapper.
	 */
	public IconRespWrapper() {
		super();
	}

	/**
	 * Instantiates a new icon resp wrapper.
	 *
	 * @param iconId the icon id
	 */
	public IconRespWrapper(String iconId) {
		super();
		this.iconId = iconId;
	}

	/**
	 * Instantiates a new icon resp wrapper.
	 *
	 * @param iconId the icon id
	 * @param iconName the icon name
	 */
	public IconRespWrapper(String iconId, String iconName) {
		super();
		this.iconId = iconId;
		this.iconName = iconName;
	}

	/**
	 * Gets the icon id.
	 *
	 * @return the iconId
	 */
	public String getIconId() {
		return iconId;
	}

	/**
	 * Sets the icon id.
	 *
	 * @param iconId            the iconId to set
	 */
	public void setIconId(String iconId) {
		this.iconId = iconId;
	}

	/**
	 * Gets the icon name.
	 *
	 * @return the iconName
	 */
	public String getIconName() {
		return iconName;
	}

	/**
	 * Sets the icon name.
	 *
	 * @param iconName            the iconName to set
	 */
	public void setIconName(String iconName) {
		this.iconName = iconName;
	}
}
