package com.magna.xmbackend.vo.roles;

import java.util.List;
import java.util.Map;

import com.magna.xmbackend.entities.RolesTbl;

/**
 *
 * @author vijay
 */
public class RoleResponse {

    private Iterable<RolesTbl> rolesTbls;
    private List<Map<String, String>> statusMaps;

    public RoleResponse() {
    }

    
    
    /**
	 * @param statusMaps
	 */
	public RoleResponse(List<Map<String, String>> statusMaps) {
		this.statusMaps = statusMaps;
	}



	/**
	 * @return the statusMaps
	 */
	public List<Map<String, String>> getStatusMaps() {
		return statusMaps;
	}



	/**
	 * @param statusMaps the statusMaps to set
	 */
	public void setStatusMaps(List<Map<String, String>> statusMaps) {
		this.statusMaps = statusMaps;
	}



	/**
     *
     * @param rolesTbls
     */
    public RoleResponse(Iterable<RolesTbl> rolesTbls) {
        this.rolesTbls = rolesTbls;
    }

    /**
     *
     * @param rolesTbls
     */
    public final void setRolesTbls(final Iterable<RolesTbl> rolesTbls) {
        this.rolesTbls = rolesTbls;
    }

    /**
     *
     * @return rolesTbls
     */
    public final Iterable<RolesTbl> getRolesTbls() {
        return rolesTbls;
    }

}
