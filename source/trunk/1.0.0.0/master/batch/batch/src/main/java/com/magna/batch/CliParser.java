package com.magna.batch;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.springframework.web.client.ResourceAccessException;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.magna.batch.helper.CreateHelper;
import com.magna.batch.helper.DeleteHelper;
import com.magna.batch.helper.Display;
import com.magna.batch.helper.UpdateHelper;
import com.magna.batch.restclient.XMBatchController;
import com.magna.batch.restclient.authorization.AuthController;
import com.magna.batch.restclient.exception.UnauthorizedAccessException;
import com.magna.batch.utils.BatchUtil;
import com.magna.batch.utils.ColumnChecker;
import com.magna.batch.utils.ObjectRelationType;
import com.magna.batch.utils.ObjectType;
import com.magna.batch.utils.SQLGenerator;
import com.magna.xmbackend.exception.CannotCreateObjectException;
import com.magna.xmbackend.vo.enums.Application;
import com.magna.xmbackend.vo.user.AuthResponse;
import com.magna.xmbackend.xmbatch.XmbatchResponse;
/**
 * Class for Cli parser.
 *
 * @author Roshan.Ekka
 */
public class CliParser {
	
	/** Member variable 'args' for {@link String[]}. */
	private String[] args;
	
	/** Member variable 'options' for {@link Options}. */
	private Options options;
	
	/** Member variable 'stb' for {@link StringBuilder}. */
	StringBuilder stb = new StringBuilder();
	/**
	 * Constructor for CliParser Class.
	 *
	 * @param args {@link String[]}
	 */
	public CliParser(String[] args) {
		this.options = new Options();
		this.args = args;
		options.addOption("h", "help", false, "Show help");
		
		Option typeOpt = new Option("type", "TYPE", true, "Used to provide type of command, valid aguments are base_data or relation, Eg: -type base_data or -type relation");
		typeOpt.setRequired(true);
		//typeOpt.setArgs(1);
		typeOpt.setArgs(Option.UNLIMITED_VALUES);
		options.addOption(typeOpt);
		
		Option selectOpt = new Option("select", "SELECT", true, "Used for search objects, valid arguments are object type like user, site etc. Eg: -select user");
		selectOpt.setArgs(Option.UNLIMITED_VALUES);
		options.addOption(selectOpt);
		
		Option createOption = new Option("create", "CREATE", true, "Used to create objects. valid arguments are object type like user, site etc. Eg: -create user");
		createOption.setArgs(Option.UNLIMITED_VALUES);
		options.addOption(createOption);
		
		Option updateOption = new Option("update", "UPDATE", true, "Used to update objects. valid arguments are object type like user, site etc. Eg: -update user");
		updateOption.setArgs(Option.UNLIMITED_VALUES);
		options.addOption(updateOption);
		
		Option deleteOption = new Option("delete", "DELETE",  true, "Used to delete objects. valid arguments are object type like user, site etc. Eg: -delete user ");
		deleteOption.setArgs(Option.UNLIMITED_VALUES);
		options.addOption(deleteOption);
		
		Option addOption = new Option("add", "ADD", true, "Used to add user project relation, valid aguments are user and project, Eg: -add project='<>' user='<>'");
		addOption.setArgs(Option.UNLIMITED_VALUES);
		options.addOption(addOption);
		
		Option removeOption = new Option("remove", "REMOVE", true, "Used to remove user project relation, valid aguments are user and project, Eg: -remove project='<>' user='<>'");
		removeOption.setArgs(Option.UNLIMITED_VALUES);
		options.addOption(removeOption);
		
		Option genExcelOption = new Option("genexcel", "GENEXCEL", false, "-genexcel takes no arguments.");
		options.addOption(genExcelOption);
		
		options.addOption("user_project", "USER_PROJECT", false, "-USER_PROJECT takes no arguments.");
		options.addOption("adminarea_project","ADMINAREA_PROJECT", false, "-ADMINAREA_PROJECT takes no arguments.");
		options.addOption("adminarea_startapp", "ADMINAREA_STARTAPP", false, "-ADMINAREA_STARTAPP takes no arguments.");
		options.addOption("user_project_usage", "USER_PROJECT_USAGE", false, "-USER_PROJECT_USAGE takes no arguments.");
		options.addOption("site_adminarea", "SITE_ADMINAREA", false, "-SITE_ADMINAREA takes no arguments.");
		options.addOption("adminarea_userapp", "ADMINAREA_USERAPP", false, "-ADMINAREA_USERAPP takes no arguments.");
		options.addOption("adminarea_project_projectapp", "ADMINAREA_PROJECT_PROJECTAPP", false, "-ADMINAREA_PROJECT_PROJECTAPP takes no arguments.");
		options.addOption("adminarea_project_startapp", "ADMINAREA_PROJECT_STARTAPP", false, "-ADMINAREA_PROJECT_STARTAPP takes no arguments.");
		options.addOption("user_startapp", "USER_STARTAPP", false, "-USER_STARTAPP takes no arguments.");
		options.addOption("user_project_projectapp", "USER_PROJECT_PROJECTAPP", false, "-USER_PROJECT_PROJECTAPP takes no arguments.");
		options.addOption("user_userapp", "USER_USERAPP", false, "-USER_USERAPP takes no arguments.");
		options.addOption("baseapp_userapp", "BASEAPP_USERAPP", false, "-BASEAPP_USERAPP takes no arguments.");
		options.addOption("baseapp_projectapp", "BASEAPP_PROJECTAPP", false, "-BASEAPP_PROJECTAPP takes no arguments.");
		options.addOption("baseapp_startapp", "BASEAPP_STARTAPP", false, "-BASEAPP_STARTAPP takes no arguments.");
		
		Option whereOption = new Option("where", "WHERE", true, "SQL where clause");
		whereOption.setArgs(Option.UNLIMITED_VALUES);
		options.addOption(whereOption);
		
		Option fieldOption = new Option("fields", "FIELDS", true, "Used to filter results by coloumns or use DISTINCT keyword to get distinct results. Eg: -fields name , email_id");
		fieldOption.setArgs(Option.UNLIMITED_VALUES);
		options.addOption(fieldOption);
	}

	/**
	 * Method for Parses the.
	 */
	public void parse() {
		CommandLineParser parser = new DefaultParser();
		CommandLine cmd = null;
		
		try {
			cmd = parser.parse(this.options, args, true);
			Option[] optionList = cmd.getOptions();
			int length = cmd.getOptions().length;
			for (int index = 0; index < length; index++) {
				String string = optionList[index].getOpt().trim().toString();
				for (int i = 0; i < length; i++) {
					if(index!=i){
						String string2 = optionList[i].getOpt().trim().toString();
						if(string.equalsIgnoreCase(string2)){
							System.out.println("ERROR: Syntax error ! Option "+ "\""+"-"+string+"\""+" should't be multiple times. ");
							return;
						}
					}
				}
			}
			
			if (cmd.hasOption("h")) {
				help();
				return;
			}
			boolean syntaxChecker = BatchUtil.getInstance().syntaxChecker(cmd);
			if(!syntaxChecker) return;
			String encryptTicket = BatchUtil.getInstance().getUserTicket();
			boolean isValidUser = false;
			String errorMessage = null;

			AuthController authController = new AuthController();
			AuthResponse authorizeLogin = authController.authorizeLogin(encryptTicket,
					Application.CAX_START_BATCH.name());
			if(authorizeLogin==null) return;
			else if (authorizeLogin != null) {
				isValidUser = authorizeLogin.isValidUser();
				errorMessage = authorizeLogin.getMessage();
			} 
			if (!isValidUser) {
				// Throw an error "errorMessage" and exit the whole system
				if (errorMessage == null) {
					System.out.println("ERROR: Current User is Unauthorized ! Please contact administrator");
					return;
				}
				System.out.println("Error: " + errorMessage);
				return;
			}
			if (cmd.hasOption("type")) {
				ColumnChecker checker  = new ColumnChecker();
				String[] optionValues = cmd.getOptionValues("type");
				if (optionValues.length > 1) {
					/*Option option = options.getOption("type");
					System.out.println("ERROR: Syntax Error ! " + option.getOpt()  + " : " + option.getDescription());*/
					checker.checkSyntax(optionValues);
					return;
				} else if (optionValues.length == 1) {
					String type = String.valueOf(cmd.getParsedOptionValue("type"));
					ColumnChecker columnChecker = new ColumnChecker();
					boolean column = columnChecker.getType(type, cmd);
					if(!column) return;
				}
				if(cmd.hasOption("select")) {
					String[] selectOptValues = cmd.getOptionValues("select");
					if (selectOptValues.length > 1) {
						/*Option option = options.getOption("select");
						System.out.println("ERROR: Syntax Error ! " + option.getOpt()  + " : " + option.getDescription());*/
						if(!"ORDER".equalsIgnoreCase(selectOptValues[1])) {
							checker.checkSyntax(selectOptValues);
							return;
						}
					} else if(selectOptValues.length == 1) {
						String tableName = String.valueOf(cmd.getParsedOptionValue("select"));
						ColumnChecker columnChecker = new ColumnChecker();
						boolean column = columnChecker.getColumn(tableName, cmd);
						if(!column) return;
					}
				}
				if(cmd.hasOption("delete")) {
					String[] delOptValues = cmd.getOptionValues("delete");
					if (delOptValues.length > 1) {
						checker.checkSyntax(optionValues);
						/*Option option = options.getOption("delete");
						System.out.println("ERROR: Syntax Error ! " + option.getOpt()  + " : " + option.getDescription());*/
						return;
					} else if(delOptValues.length == 1) {
						String tableName = String.valueOf(cmd.getParsedOptionValue("delete"));
						ColumnChecker columnChecker = new ColumnChecker();
						boolean column = columnChecker.getColumn(tableName, cmd);
						if(!column) return;
					} 
				}
				String typeArgument = String.valueOf(cmd.getParsedOptionValue("type"));
				switch (typeArgument) {
					case "base_data":
						processBaseData(cmd);
						break;
					case "relation":
						processRelation(cmd);
						break;
					default:
						help();
						break;
				}
			}
		} catch (ResourceAccessException e) {
			System.out.println("ERROR: Server not reachable !");
		} catch (NullPointerException e) {
			System.out.println("ERROR: Server Error !");
		} catch (UnauthorizedAccessException e) {
			System.out.println("ERROR: Permission error ! You do not have permission for this operation");
		} catch (ParseException e) {
			help();
		}
	}
	
	/**
	 * Method for Process relation.
	 *
	 * @param cmd {@link CommandLine}
	 * @throws ParseException 
	 */
	private void processRelation(final CommandLine cmd) {
		SQLGenerator instance = SQLGenerator.getInstance();
		String tableName = "";
		if (cmd.hasOption("USER_PROJECT")) {
			tableName = "USER_PROJECT";
		}
		if (cmd.hasOption("ADMINAREA_PROJECT")) {
			tableName = "ADMINAREA_PROJECT";
		} 
		if (cmd.hasOption("ADMINAREA_STARTAPP")) {
			tableName = "ADMINAREA_STARTAPP";
		}
		if (cmd.hasOption("USER_STARTAPP")) {
			tableName = "USER_STARTAPP";
		}
		if (cmd.hasOption("USER_PROJECT_PROJECTAPP")) {
			tableName = "USER_PROJECT_PROJECTAPP";
		}
		if (cmd.hasOption("USER_USERAPP")) {
			tableName = "USER_USERAPP";
		}
		if (cmd.hasOption("BASEAPP_USERAPP")) {
			tableName = "BASEAPP_USERAPP";
		}
		if (cmd.hasOption("BASEAPP_PROJECTAPP")) {
			tableName = "BASEAPP_PROJECTAPP";
		}
		if (cmd.hasOption("BASEAPP_STARTAPP")) {
			tableName = "BASEAPP_STARTAPP";
		}
		if (cmd.hasOption("USER_PROJECT_USAGE")) {
			tableName = "USER_PROJECT_USAGE";
		}
		if (cmd.hasOption("SITE_ADMINAREA")) {
			tableName = "SITE_ADMINAREA";
		}
		if (cmd.hasOption("ADMINAREA_USERAPP")) {
			tableName = "ADMINAREA_USERAPP";
		}
		if (cmd.hasOption("ADMINAREA_PROJECT_PROJECTAPP")) {
			tableName = "ADMINAREA_PROJECT_PROJECTAPP";
		}
		if (cmd.hasOption("ADMINAREA_PROJECT_STARTAPP")) {
			tableName = "ADMINAREA_PROJECT_STARTAPP";
		}
		if ((!cmd.hasOption("add") && !cmd.hasOption("remove"))) {
			ObjectRelationType objectRelType = ObjectRelationType.getObjectRelType(tableName);
			if(objectRelType==null){
				System.out.println("ERROR: Syntax error ! use -help option for Help ");
				return;
			}
			String sqlString = instance.getSQLString(tableName, objectRelType, cmd);
			if (BatchUtil.isEmpty(sqlString) && !tableName.equalsIgnoreCase("USER_PROJECT_USAGE")) {
				System.out.println("ERROR: Syntax error ! use -help option for Help ");
				return;
			} else if(BatchUtil.isEmpty(sqlString) && tableName.equalsIgnoreCase("USER_PROJECT_USAGE")) {
				return;
			}
			XMBatchController batchController = new XMBatchController();
			sqlString = sqlString.replace("\"", "");
			XmbatchResponse resultSet = batchController.getResultSet(sqlString);
			if (resultSet == null) {
				return;
			}
			List<Map<String, Object>> queryResultSet = resultSet.getQueryResultSet();
			Display disp = new Display();
			if (queryResultSet == null) {
				System.out.println("ERROR: query failed\n");
				return;
			} else {
				// query succeeded list entries
				if (queryResultSet.isEmpty()) {
					System.out.println("No Result found");
					return;
				} else {
					disp.showResult(queryResultSet, cmd, tableName);
				}
			}
		} else if (cmd.hasOption("add")) {
			CreateHelper createHelper = CreateHelper.getInstance();
			ObjectRelationType objectRelType = ObjectRelationType.getObjectRelType(tableName);
			createHelper.createRelObject(objectRelType, cmd);

		} else if (cmd.hasOption("remove")) {
			DeleteHelper deleteHelper = DeleteHelper.getInstance();
			ObjectRelationType objectRelType = ObjectRelationType.getObjectRelType(tableName);
			deleteHelper.deleteRelObject(objectRelType, tableName, cmd);
		}
	}

	/**
	 * Method for Process base data.
	 *
	 * @param cmd {@link CommandLine}
	 * @throws ParseException the parse exception
	 * @throws IOException 
	 * @throws JsonProcessingException 
	 * @throws UnauthorizedAccessException 
	 * @throws CannotCreateObjectException 
	 */
	private void processBaseData(final CommandLine cmd) throws ParseException {
		SQLGenerator instance = SQLGenerator.getInstance();
		if (cmd.hasOption("select")) {
			String tableName = String.valueOf(cmd.getParsedOptionValue("select"));
			ObjectType objectType = ObjectType.getObjectType(tableName);
			if(objectType==null){
				System.out.println("ERROR: Syntax error ! use -help option for Help ");
				return;
			}
			String sqlString = instance.getSQLString(tableName, objectType, cmd);
			if (BatchUtil.isEmpty(sqlString)) {
				System.out.println("ERROR: Syntax error ! use -help option for Help ");
				return;
			}
			XMBatchController batchController = new XMBatchController();
			sqlString = sqlString.replace("\"", "");
			XmbatchResponse resultSet = batchController.getResultSet(sqlString);
			if (resultSet == null) {
				return;
			}
			List<Map<String, Object>> queryResultSet = resultSet.getQueryResultSet();
			Display disp = new Display();
			if (queryResultSet == null) {
				// query failed
				System.out.println("ERROR: query failed\n");
				return;
			} else {
				// query succeeded  list entries
				if (queryResultSet.isEmpty()) {
					System.out.println("No Result found");
					return;
				} else {
					disp.showResult(queryResultSet, cmd, tableName);
				}
			}
		} else if (cmd.hasOption("create") && !cmd.hasOption("where")) {
			String[] optionValues = cmd.getOptionValues("create");
			if(optionValues.length==1){
				System.out.println("Please provide fields for creating object after -create argument . Eg: -create user name='userName' status='ACTIVE' icon_name='users.png'");
				return;
			}
			CreateHelper createHelper = CreateHelper.getInstance();
			String tableName = String.valueOf(cmd.getParsedOptionValue("create"));
			ObjectType objectType = ObjectType.getObjectType(tableName);
			if(objectType==null){
				System.out.println("ERROR: Syntax error ! use -help option for Help ");
				return;
			}
			createHelper.createObject(objectType, cmd);
		} else if (cmd.hasOption("update") && cmd.hasOption("where")) {
			String[] optionValues = cmd.getOptionValues("update");
			if(optionValues.length==1){
				System.out.println("Please provide fields for updating object after -update argument . Eg: -update user status='ACTIVE'");
				return;
			}
			UpdateHelper updateHelper = UpdateHelper.getInstance();
			String updateTblName = String.valueOf(cmd.getParsedOptionValue("update"));
			ObjectType updateObjType = ObjectType.getObjectType(updateTblName);
			if(updateObjType==null){
				System.out.println("ERROR: Syntax error ! use -help option for Help ");
				return;
			}
			updateHelper.updateObject(updateObjType, cmd);
		} else if (cmd.hasOption("delete") && cmd.hasOption("where")) {
			String[] optionValues = cmd.getOptionValues("delete");
			String[] whereOptionValues = cmd.getOptionValues("where");
			if(optionValues.length!=1){
				Option option = options.getOption("delete");
				System.out.println("ERROR: Syntax Error ! " + option.getOpt()  + " : " + option.getDescription());
				return;
			} else if(whereOptionValues.length>3){
				System.out.println("ERROR: Syntax Error ! Eg -where name='objectName' OR name=\"object name\"");
				return;
			}
			DeleteHelper deleteHelper = DeleteHelper.getInstance();
			String deleteTblName = String.valueOf(cmd.getParsedOptionValue("delete"));
			ObjectType deleteObjType = ObjectType.getObjectType(deleteTblName);
			if(deleteObjType==null){
				System.out.println("ERROR: Syntax error ! use -help option for Help ");
				return;
			}
			deleteHelper.deleteObject(deleteObjType, deleteTblName, cmd);
		} else{
			System.out.println("ERROR: Syntax error ! use -help option for Help ");
			return;
		}
	}
	
	/**
	 * Method for Help.
	 */
	private void help() {
		HelpFormatter formater = new HelpFormatter();
		System.out.println("Linux user please escape the quotes like \\\'");
		formater.setWidth(200);
		formater.printHelp("CaxStartBatch", options, true);
		System.exit(0);
	}
}
