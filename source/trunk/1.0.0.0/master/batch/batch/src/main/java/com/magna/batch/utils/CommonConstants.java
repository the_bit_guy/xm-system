package com.magna.batch.utils;

/**
 * The Class CommonConstants.
 */
public class CommonConstants {

	/** The Constant EMPTY_STR. */
	public static final String EMPTY_STR = "";
	
	/** The Constant TRUE. */
	public static final String TRUE = "true";

	/** The Constant FALSE. */
	public static final String FALSE = "false";
	
	/**
	 * The Class RegularExpressions.
	 */
	public static class RegularExpressions {

		/** The Constant ALLOWED_SITE_NAME_REGEXP. */
		public static final String ALLOWED_SITE_NAME_REGEXP = "^[a-zA-Z0-9_]*$";
		
		
		/** The Constant ALLOWED_NAME_REGEX. */
		public static final String ALLOWED_NAME_REGEX = "^[ a-zA-Z0-9@_\\-]*$";

		/** The Constant ALLOWED_APP_NAME_REGEX. */
		public static final String ALLOWED_APP_NAME_REGEX = "^[ a-zA-Z0-9@_\\-\\\\/\\.\\>\\<]*$";

		/** The Constant ALLOWED_DIALOGBOX_REGEX. */
		public static final String ALLOWED_DIALOGBOX_REGEX = "[0-9a-zA-Z@_\\s\\-]+";

		/** The Constant ALLOWED_PROJECT_NAME_REGEX. */
		public static final String ALLOWED_PROJECT_NAME_REGEX = "^[a-zA-Z0-9_-]*$";

		/** The Constant EMAIL_REGEX. */
		public static final String EMAIL_REGEX = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
				+ "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

		/** The Constant CONTACT_NUMBER_REGEX. */
//		public static final String CONTACT_EMPTY_NUM_REGEX = /*"^\\+[0-9]{1,3}\\ [0-9]{11}(?:x.+)?$"*/"(^\\+\\s*$)";
		
		/** The Constant CONTACT_NUMBER_REGEX. */
		public static final String CONTACT_NUMBER_REGEX = "^\\+([0-9]){1,3}\\-([0-9]){1,3}\\-([0-9]){1,4}\\-([0-9]){1,4}$";
		
	}
	
	/**
	 * The Class Messages.
	 */
	public static class Messages{
		
		/** The Constant NAME_MESSAGE. */
		public static final String NAME_MESSAGE = " - Allowed Special Characters ([A-Z][a-z][0-9]@-_ and Space)";
		
		/** The Constant NAME_MESSAGE. */
		public static final String PROJECT_NAME_MESSAGE = " - Allowed Special Characters ([A-Z][a-z][0-9]@-_)";
		
		/** The Constant SITE_NAME_MESSAGE. */
		public static final String SITE_NAME_MESSAGE = " - Allowed Special Characters ([A-Z][a-z][0-9]_)";
		
		/** The Constant APP_NAME_MESSAGE. */
		public static final String APP_NAME_MESSAGE = " - Allowed Special Characters ([A-Z][a-z][0-9]@.-_ and Space)";

		/** The Constant CONTACT_MESSAGE. */
		public static final String CONTACT_MESSAGE = "(+XX)-(XXX)-(XXXX)-(XXXX)";
		
		/** The Constant EMAIL_MESSAGE. */
		public static final String EMAIL_MESSAGE = " - Eg: example@magna.com";
		
		/** The Constant NAME_LIMIT_MESSAGE. */
		public static final String NAME_LIMIT_MESSAGE = " - size should not be more then 30 characters";
		
		/** The Constant EMAIL_LIMIT_MESSAGE. */
		public static final String LIMIT_MESSAGE = " - size should not be more then 60 characters";
		
		/** The Constant EMAIL_LIMIT_MESSAGE. */
		public static final String HOTLINE_CONTACT_LIMIT_MESSAGE = " - size should not be more then 30 characters";
		
		/** The Constant DESC_LIMIT_MESSAGE. */
		public static final String DESC_LIMIT_MESSAGE = " - size should not be more then 240 characters";
		
		/** The Constant REMARKS_LIMIT_MESSAGE. */
		public static final String REMARKS_LIMIT_MESSAGE = " - size should not be more then 1500 characters";
		
	}	
}
