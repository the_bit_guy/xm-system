package com.magna.batch.model;

/**
 * Class for Site data.
 *
 * @author Roshan.Ekka
 */
public class SiteData {

	/** The user name. */
	private String name;
	
	/** The status. */
	private String status;
	
	/** The description. */
	private String description;
	
	/** The remarks. */
	private String remarks;
	
	/** The lang code. */
	private String lang_code;
	
	/** The icon name. */
	private String iconName;

	/**
	 * Instantiates a new user data.
	 */
	public SiteData() {
		super();

	}
	
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	
	/**
	 * Gets the status.
	 *
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * Sets the status.
	 *
	 * @param status the new status
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * Gets the description.
	 *
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Sets the description.
	 *
	 * @param description the new description
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * Gets the remarks.
	 *
	 * @return the remarks
	 */
	public String getRemarks() {
		return remarks;
	}

	/**
	 * Sets the remarks.
	 *
	 * @param remarks the new remarks
	 */
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	/**
	 * Gets the lang code.
	 *
	 * @return the lang code
	 */
	public String getLang_code() {
		return lang_code;
	}

	/**
	 * Sets the lang code.
	 *
	 * @param lang_code the new lang code
	 */
	public void setLang_code(String lang_code) {
		this.lang_code = lang_code;
	}

	/**
	 * Gets the icon name.
	 *
	 * @return the icon name
	 */
	public String getIconName() {
		return iconName;
	}

	/**
	 * Sets the icon name.
	 *
	 * @param iconName the new icon name
	 */
	public void setIconName(String iconName) {
		this.iconName = iconName;
	}
}
