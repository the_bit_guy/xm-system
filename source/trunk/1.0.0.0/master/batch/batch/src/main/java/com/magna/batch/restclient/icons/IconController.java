package com.magna.batch.restclient.icons;

import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.magna.batch.restclient.BatchRestController;
import com.magna.batch.restclient.exception.UnauthorizedAccessException;
import com.magna.batch.utils.RestClientConstants;
import com.magna.xmbackend.exception.XMObjectNotFoundException;
import com.magna.xmbackend.vo.icon.IconRespWrapper;
import com.magna.xmbackend.vo.icon.IkonRequest;

public class IconController extends BatchRestController {
	/** The Constant LOGGER. */
	private static final Logger LOGGER = LoggerFactory.getLogger(IconController.class);

	public IconRespWrapper getIconByName(final IkonRequest iconRequest) throws UnauthorizedAccessException {
		try {
			final String url = new String(this.serviceUrl + RestClientConstants.GET_ICONID_BY_NAME);
			final RestTemplate restTemplate = new RestTemplate();
			HttpEntity<IkonRequest> requestEntity = new HttpEntity<IkonRequest>(iconRequest, this.headers);
			ResponseEntity<IconRespWrapper> responseEntity = restTemplate.postForEntity(url, requestEntity, IconRespWrapper.class);
			HttpStatus statusCode = responseEntity.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				IconRespWrapper iconRespWrapper = responseEntity.getBody();
				return iconRespWrapper;
			} else {
				LOGGER.error("Error while calling XMSYSTEM Get Iocns By Name REST Service, returns the status code: ", //$NON-NLS-1$
						statusCode.value());
			}
		}  catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				System.out.println(getMsgFromexception(e));
				return null;
			} else if (e instanceof HttpStatusCodeException) {
				System.out.println(getMsgFromexception(e));
				return null;
				//String errCode = RestClientExpHandlerUtil.handleCannotCreateObjectException(LOGGER, (HttpStatusCodeException) e);
				//throw new CannotCreateObjectException(errorResponse, errCode);
		} 
	} catch (Exception e) {
			//LOGGER.error("Error while calling XMSYSTEM Get Iocns by Name REST Service", e); //$NON-NLS-1$
		}
		return null;
	}
	
	/**
	 * Gets the msg fromexception.
	 *
	 * @param ex {@link Exception}
	 * @return the msg fromexception
	 */
	private String getMsgFromexception(final Exception ex) {
		String errorResponse = null;
		String expMsg = null;
		JsonNode jsonNode = null;
		ObjectMapper mapper = new ObjectMapper();
		if (ex instanceof HttpStatusCodeException) {
			errorResponse = ((HttpStatusCodeException) ex).getResponseBodyAsString();
			try {
				jsonNode = mapper.readTree(errorResponse);
			} catch (IOException e1) {
				e1.printStackTrace();
			}

			if (jsonNode.get("lingualMessages") != null) {
				expMsg = jsonNode.get("lingualMessages").get("en").textValue();
			} else if (jsonNode.get("description") != null) {
				expMsg = jsonNode.get("description").asText();
			}
		} else if (ex instanceof HttpClientErrorException) {
			HttpStatus statusCode = ((HttpClientErrorException)ex).getStatusCode();
			if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
				throw new UnauthorizedAccessException(ex.getMessage());
			} else if (statusCode.compareTo(HttpStatus.UNPROCESSABLE_ENTITY) == 0){
				if (ex instanceof XMObjectNotFoundException) {
					expMsg = ((XMObjectNotFoundException) ex).getMessage();
				}
			}
		}
		return expMsg;
	}
}
