package com.magna.batch.restclient;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;

import com.magna.batch.utils.BatchUtil;

/**
 * The Class BatchRestController.
 * 
 * @author shashwat.anand
 */
public class BatchRestController {
	/** The Constant LOGGER. */
	private static final Logger LOGGER = LoggerFactory.getLogger(BatchRestController.class);
	
	/** Member variable 'service url' for {@link String}. */
	public String serviceUrl;

	/** Member variable 'headers' for {@link HttpHeaders}. */
	public HttpHeaders headers;
	
	/**
	 * Instantiates a new batch rest controller.
	 */
	public BatchRestController() {
		this.serviceUrl = BatchUtil.getXMBackendUrl();

		this.headers = new HttpHeaders();
		this.headers.setContentType(MediaType.APPLICATION_JSON);
		initHeaderParams();
	}
	
	/**
	 * Instantiates a new batch rest controller.
	 *
	 * @param adminAreaId the admin area id
	 */
	public BatchRestController(String adminAreaId) {
		this.serviceUrl = BatchUtil.getXMBackendUrl();
		
		this.headers = new HttpHeaders();
		this.headers.setContentType(MediaType.APPLICATION_JSON);
		initHeaderParams(adminAreaId);
	}
	
	/**
	 * Method for Inits the header params.`
	 */
	private void initHeaderParams() {		
		try {
			final String userName = BatchUtil.getSystemUserName();		
			String ticket = BatchUtil.getInstance().getTicket();
			this.headers.set("USER_NAME", userName);
			this.headers.set("TKT", ticket);
		} catch (Exception e) {
			LOGGER.error("Error while initiating the REST Service", e); //$NON-NLS-1$
		}
	}
	
	/**
	 * Method for Inits the header params.
	 * @param adminAreaId {@link String}
	 */
	private void initHeaderParams(String adminAreaId) {
		try {
			initHeaderParams();
			if(adminAreaId != null && !adminAreaId.isEmpty()) {
				this.headers.set("ADMIN_AREA_ID", adminAreaId);
			}
		} catch (Exception e) {
			LOGGER.error("Error while initiating the REST Service", e); //$NON-NLS-1$
		}		
	}

}
