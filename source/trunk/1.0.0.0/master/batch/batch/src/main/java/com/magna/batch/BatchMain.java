package com.magna.batch;

import com.magna.batch.utils.BatchEnvProcess;
import com.magna.batch.utils.BatchUtil;

public class BatchMain {

	public static void main(String[] args) {
		BatchEnvProcess.getInstance().start();
		//new CliParser(args).parse();
		boolean checkOptions = BatchUtil.getInstance().checkOptions(args);
		if(!checkOptions) {
			System.exit(0);
		}
		new CliParser(args).parse();
	}
}
