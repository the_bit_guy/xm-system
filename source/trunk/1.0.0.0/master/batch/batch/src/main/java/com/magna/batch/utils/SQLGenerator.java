package com.magna.batch.utils;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.ParseException;

/**
 * Class for SQL generator.
 *
 * @author Roshan.Ekka
 */
public class SQLGenerator {
	
	/** Member variable 'this ref' for {@link SQLGenerator}. */
	private static SQLGenerator thisRef;
	
	/** Member variable 'stb' for {@link StringBuilder}. */
	StringBuilder stb = new StringBuilder();
	
	/**
	 * Constructor for SQLGenerator Class.
	 */
	private SQLGenerator() {
		thisRef = this;
	}

	/**
	 * Gets the single instance of SQLGenerator.
	 *
	 * @return single instance of SQLGenerator
	 */
	public static SQLGenerator getInstance() {
		if (thisRef == null) {
			new SQLGenerator();
		}
		return thisRef;
	}

	/**
	 * Gets the SQL string.
	 *
	 * @param tableName {@link String}
	 * @param cmd {@link CommandLine}
	 * @return the SQL string
	 * @throws ParseException the parse exception
	 */
	public String getSQLString(final String tableName, final ObjectType objectType, final CommandLine cmd) {
		switch (objectType) {
		case USER:
			return processForUserTable(cmd, tableName);
		case SITE:
			return processForSiteTable(cmd, tableName);
		case ADMINAREA:
			return processForAdminAreaTable(cmd, tableName);
		case PROJECT:
			return processForProjectTable(cmd,tableName);
		case BASEAPP:
			return processForBaseAppTable(cmd, tableName);
		case USERAPP:
			return processForUserAppTable(cmd,tableName);
		case PROJECTAPP:
			return processForProjAppTable(cmd, tableName);
		case STARTAPP:
			return processForStartAppTable(cmd, tableName);
		default:
			break;
		}
		return null;
	}
	
	/**
	 * Gets the SQL string.
	 *
	 * @param relTableName {@link ObjectRelationType}
	 * @param cmd {@link CommandLine}
	 * @return the SQL string
	 * @throws ParseException the parse exception
	 */
	public String getSQLString(final String relTableName, final ObjectRelationType objRelType, final CommandLine cmd) {
		switch (objRelType) {
		case ADMINAREA_PROJECT:
			return processForAAProjRelTbl(cmd, relTableName);
		case ADMINAREA_STARTAPP:
			return processForAAStartAppRelTbl(cmd, relTableName);
		case ADMINAREA_USERAPP:
			return processForAAUserAppRelTbl(cmd, relTableName);
		case ADMINAREA_PROJECT_PROJECTAPP:
			return processForAAProjProjAppRelTbl(cmd, relTableName);
		case ADMINAREA_PROJECT_STARTAPP:
			return processForAAProjStartAppRelTbl(cmd, relTableName);
		case BASEAPP_USERAPP:
			return processForBaseAppUserAppRelTbl(cmd, relTableName);
		case BASEAPP_PROJECTAPP:
			return processForBaseAppProjAppRelTbl(cmd, relTableName);
		case BASEAPP_STARTAPP:
			return processForBaseAppStartAppRelTbl(cmd, relTableName);
		case USER_PROJECT_USAGE:
			return processForUserProjUsageRelTbl(cmd);
		case SITE_ADMINAREA:
			return processForSiteAARelTbl(cmd, relTableName);
		case USER_PROJECT:
			return processForUserProjRelTbl(cmd, relTableName);
		case USER_STARTAPP:
			return processForUserStartAppRelTbl(cmd, relTableName);
		case USER_USERAPP:
			return processForUserUserAppRelTbl(cmd, relTableName);
		case USER_PROJECT_PROJECTAPP:
			return processForUserProjProjAppRelTbl(cmd, relTableName);
		default:
			break;
		}
		return null;
	}
	
	/**
	 * Method for Process for user proj usage rel tbl.
	 *
	 * @param cmd {@link CommandLine}
	 * @return the string {@link String}
	 */
	private String processForUserProjUsageRelTbl(CommandLine cmd) {
		String strVal = "";
		String year = null;
		if (cmd.hasOption("where")) {
			String[] optionValues = cmd.getOptionValues("where");
			if (optionValues.length > 0) {
				for (int index = 0; index < optionValues.length; index++) {
					String copyStrVal = optionValues[index];
					copyStrVal = copyStrVal.replace("'", "");
					copyStrVal = copyStrVal.replace(" ", "");
					copyStrVal = copyStrVal.trim();
					strVal = strVal.concat(copyStrVal);
				}
			}
		} else {
			return null;
		}
		if (strVal.contains("year")) {
			year = strVal.split("=", 2)[1];
			
		} else if(!strVal.contains("year")) {
			String string = strVal.split("=", 2)[0];
			System.out.println("ERROR: Syntax Error ! " +string);
			return null;
		}
		String beginYear = "'" + year + "/01/01'";
		String endYear = "'" + year + "/12/31'";
		String sqlQueryStr = "select PROJECT, usemonth, count(PROJECT) from ( select distinct UHT.USER_NAME, UHT.PROJECT, TO_CHAR(UHT.LOG_TIME,'YYYY/MM') usemonth "
				+ "from USER_HISTORY_TBL UHT where UHT.LOG_TIME>=to_date(" + beginYear
				+ ",'YYYY/MM/DD') and UHT.LOG_TIME<=to_date(" + endYear
				+ ",'YYYY/MM/DD') and UHT.APPLICATION in ('*OpenProject*') and IS_USER_STATUS='false') group by PROJECT, usemonth order by PROJECT, usemonth ";

		return sqlQueryStr;
	}

	/**
	 * Method for Process for AA start app rel tbl.
	 *
	 * @param cmd {@link CommandLine}
	 * @return the string {@link String}
	 */
	private String processForAAStartAppRelTbl(final CommandLine cmd, final String tableName) {
		String selectRelQuery = selectRelQuery(cmd, tableName);
		if(selectRelQuery==null) return null;
		stb.append(" FROM ADMIN_AREA_START_APP_REL_TBL aasart, SITE_ADMIN_AREA_REL_TBL saart, SITES_TBL st, "
				+ "ADMIN_AREAS_TBL aat, START_APPLICATIONS_TBL sat WHERE "
				+ "AASART.SITE_ADMIN_AREA_REL_ID = SAART.SITE_ADMIN_AREA_REL_ID "
				+ "AND SAART.ADMIN_AREA_ID = AAT.ADMIN_AREA_ID AND SAART.SITE_ID = ST.SITE_ID AND "
				+ "AASART.START_APPLICATION_ID = SAT.START_APPLICATION_ID ");
		if (cmd.hasOption("where")) {
			whereRelationQuery(cmd, tableName);
		}
		return stb.toString();
	}

	/**
	 * Method for Process for user user app rel tbl.
	 *
	 * @param cmd {@link CommandLine}
	 * @return the string {@link String}
	 */
	private String processForUserUserAppRelTbl(final CommandLine cmd, final String tableName) {
		String selectRelQuery = selectRelQuery(cmd, tableName);
		if(selectRelQuery==null) return null;
		stb.append(" FROM USER_USER_APP_REL_TBL uuart, SITE_ADMIN_AREA_REL_TBL saart, USERS_TBL ut, USER_APPLICATIONS_TBL uat, "
				+ "ADMIN_AREAS_TBL aat, SITES_TBL st  WHERE UUART.SITE_ADMIN_AREA_REL_ID = SAART.SITE_ADMIN_AREA_REL_ID AND "
				+ "SAART.ADMIN_AREA_ID = AAT.ADMIN_AREA_ID AND SAART.SITE_ID = ST.SITE_ID AND UUART.USER_APPLICATION_ID = UAT.USER_APPLICATION_ID "
				+ "AND UUART.USER_ID = UT.USER_ID ");
		if (cmd.hasOption("where")) {
			whereRelationQuery(cmd, tableName);
		}
		return stb.toString();
	}

	/**
	 * Method for Process for user proj proj app rel tbl.
	 *
	 * @param cmd {@link CommandLine}
	 * @return the string {@link String}
	 */
	private String processForUserProjProjAppRelTbl(final CommandLine cmd, final String tableName) {
		String selectRelQuery = selectRelQuery(cmd, tableName);
		if(selectRelQuery==null) return null;
		stb.append(" FROM USER_PROJ_APP_REL_TBL upart, USER_PROJECT_REL_TBL uprt, ADMIN_AREA_PROJECT_REL_TBL aaprt, "
				+ "SITE_ADMIN_AREA_REL_TBL saart, SITES_TBL st, USERS_TBL ut, PROJECT_APPLICATIONS_TBL pat, PROJECTS_TBL pt, ADMIN_AREAS_TBL aat "
				+ "WHERE UPART.PROJECT_APPLICATION_ID = PAT.PROJECT_APPLICATION_ID AND UPART.USER_PROJECT_REL_ID = UPRT.USER_PROJECT_REL_ID"
				+ " AND UPART.ADMIN_AREA_PROJECT_REL_ID = AAPRT.ADMIN_AREA_PROJECT_REL_ID AND AAPRT.SITE_ADMIN_AREA_REL_ID = "
				+ "SAART.SITE_ADMIN_AREA_REL_ID AND SAART.ADMIN_AREA_ID = AAT.ADMIN_AREA_ID AND SAART.SITE_ID = ST.SITE_ID AND UPRT.USER_ID = UT.USER_ID AND"
				+ " UPRT.PROJECT_ID = AAPRT.PROJECT_ID AND AAPRT.PROJECT_ID = PT.PROJECT_ID ");
		if (cmd.hasOption("where")) {
			whereRelationQuery(cmd, tableName);
		}
		return stb.toString();
	}

	/**
	 * Method for Process for user start app rel tbl.
	 *
	 * @param cmd {@link CommandLine}
	 * @return the string {@link String}
	 */
	private String processForUserStartAppRelTbl(final CommandLine cmd, final String tableName) {
		String selectRelQuery = selectRelQuery(cmd, tableName);
		if(selectRelQuery==null) return null;
		stb.append(" FROM USER_START_APP_REL_TBL usart, USERS_TBL ut, START_APPLICATIONS_TBL sat "
				+ "WHERE USART.USER_ID = UT.USER_ID AND USART.START_APPLICATION_ID = SAT.START_APPLICATION_ID ");
		if (cmd.hasOption("where")) {
			whereRelationQuery(cmd, tableName);
		}
		return stb.toString();
	}

	/**
	 * Method for Process for AA proj start app rel tbl.
	 *
	 * @param cmd {@link CommandLine}
	 * @return the string {@link String}
	 */
	private String processForAAProjStartAppRelTbl(final CommandLine cmd, final String tableName) {
		String selectRelQuery = selectRelQuery(cmd, tableName);
		if(selectRelQuery==null) return null;
		//todo
		/*stb.append(" FROM ADMIN_AREA_START_APP_REL_TBL aasart, SITE_ADMIN_AREA_REL_TBL saart, SITES_TBL st, "
				+ "ADMIN_AREAS_TBL aat, PROJECTS_TBL pt, START_APPLICATIONS_TBL sat WHERE AASART.START_APPLICATION_ID "
				+ "= SAT.START_APPLICATION_ID AND AASART.SITE_ADMIN_AREA_REL_ID"
				+ " =SAART.SITE_ADMIN_AREA_REL_ID AND SAART.ADMIN_AREA_ID = AAT.ADMIN_AREA_ID AND SAART.SITE_ID = ST.SITE_ID ");*/
		stb.append(" FROM ADMIN_AREAS_TBL aat, ADMIN_AREA_PROJECT_REL_TBL aaprt, SITE_ADMIN_AREA_REL_TBL saart, PROJECTS_TBL pt, "
				+ " PROJECT_START_APP_REL_TBL psart, START_APPLICATIONS_TBL sat, SITES_TBL st "
				+ " where aaprt.SITE_ADMIN_AREA_REL_ID = saart.SITE_ADMIN_AREA_REL_ID AND saart.ADMIN_AREA_ID = aat.ADMIN_AREA_ID "
				+ " and pt.PROJECT_ID = aaprt.PROJECT_ID and psart.ADMIN_AREA_PROJECT_REL_ID = aaprt.ADMIN_AREA_PROJECT_REL_ID "
				+ " and SAART.SITE_ID = ST.SITE_ID and sat.START_APPLICATION_ID = psart.START_APPLICATION_ID ");
		if (cmd.hasOption("where")) {
			whereRelationQuery(cmd, tableName);
		}
		return stb.toString();
	}

	/**
	 * Method for Process for AA proj proj app rel tbl.
	 *
	 * @param cmd {@link CommandLine}
	 * @return the string {@link String}
	 */
	private String processForAAProjProjAppRelTbl(final CommandLine cmd, final String tableName) {
		String selectRelQuery = selectRelQuery(cmd, tableName);
		if(selectRelQuery==null) return null;
		stb.append(" FROM ADMIN_AREA_PROJ_APP_REL_TBL aapart, ADMIN_AREA_PROJECT_REL_TBL aaprt, "
				+ "SITE_ADMIN_AREA_REL_TBL saart, SITES_TBL st, ADMIN_AREAS_TBL aat, PROJECTS_TBL pt, PROJECT_APPLICATIONS_TBL pat "
				+ "WHERE AAPART.PROJECT_APPLICATION_ID = PAT.PROJECT_APPLICATION_ID "
				+ "AND AAPART.ADMIN_AREA_PROJECT_REL_ID = AAPRT.ADMIN_AREA_PROJECT_REL_ID AND"
				+ " AAPRT.SITE_ADMIN_AREA_REL_ID = SAART.SITE_ADMIN_AREA_REL_ID "
				+ "AND SAART.ADMIN_AREA_ID = AAT.ADMIN_AREA_ID AND SAART.SITE_ID = ST.SITE_ID "
				+ " AND aaprt.PROJECT_ID = pt.PROJECT_ID ");
		if (cmd.hasOption("where")) {
			whereRelationQuery(cmd, tableName);
		}
		return stb.toString();
	}

	/**
	 * Method for Process for AA user app rel tbl.
	 *
	 * @param cmd {@link CommandLine}
	 * @return the string {@link String}
	 */
	private String processForAAUserAppRelTbl(final CommandLine cmd, final String tableName) {
		String selectRelQuery = selectRelQuery(cmd, tableName);
		if(selectRelQuery==null) return null;
		stb.append(" FROM ADMIN_AREA_USER_APP_REL_TBL aauart,SITE_ADMIN_AREA_REL_TBL saart, SITES_TBL st,"
				+ " ADMIN_AREAS_TBL aat, USER_APPLICATIONS_TBL uat WHERE AAUART.SITE_ADMIN_AREA_REL_ID = SAART.SITE_ADMIN_AREA_REL_ID"
				+ " AND AAUART.USER_APPLICATION_ID = UAT.USER_APPLICATION_ID AND SAART.ADMIN_AREA_ID = AAT.ADMIN_AREA_ID "
				+ "AND SAART.SITE_ID = ST.SITE_ID ");
		if (cmd.hasOption("where")) {
			whereRelationQuery(cmd, tableName);
		}
		return stb.toString();
	}

	/**
	 * Method for Process for site AA rel tbl.
	 *
	 * @param cmd {@link CommandLine}
	 * @return the string {@link String}
	 */
	private String processForSiteAARelTbl(final CommandLine cmd, final String tableName) {
		String selectRelQuery = selectRelQuery(cmd, tableName);
		if(selectRelQuery==null) return null;
		stb.append(" FROM SITE_ADMIN_AREA_REL_TBL saart, SITES_TBL st, ADMIN_AREAS_TBL aat "
				+ "WHERE SAART.SITE_ID = ST.SITE_ID AND SAART.ADMIN_AREA_ID = AAT.ADMIN_AREA_ID ");
		if (cmd.hasOption("where")) {
			whereRelationQuery(cmd, tableName);
		}
		return stb.toString();
	}

	/**
	 * Method for Process for AA proj rel tbl.
	 *
	 * @param cmd {@link CommandLine}
	 * @return the string {@link String}
	 */
	private String processForAAProjRelTbl(final CommandLine cmd, final String tableName) {
		String selectRelQuery = selectRelQuery(cmd, tableName);
		if(selectRelQuery==null) return null;
		stb.append(" FROM ADMIN_AREA_PROJECT_REL_TBL AAPRT, SITE_ADMIN_AREA_REL_TBL SAART, SITES_TBL st, PROJECTS_TBL PT, ADMIN_AREAS_TBL aat"
				+ " WHERE AAPRT.SITE_ADMIN_AREA_REL_ID = SAART.SITE_ADMIN_AREA_REL_ID AND AAPRT.PROJECT_ID = PT.PROJECT_ID"
				+ " AND SAART.ADMIN_AREA_ID = AAT.ADMIN_AREA_ID AND SAART.SITE_ID = ST.SITE_ID ");
		if (cmd.hasOption("where")) {
			whereRelationQuery(cmd, tableName);
		}
		return stb.toString();
	}

	/**
	 * Method for Process for user proj rel tbl.
	 *
	 * @param cmd {@link CommandLine}
	 * @return the string {@link String}
	 */
	private String processForUserProjRelTbl(final CommandLine cmd, final String tableName) {
		String selectRelQuery = selectRelQuery(cmd, tableName);
		if(selectRelQuery==null) return null;
		stb.append(" FROM USER_PROJECT_REL_TBL uprt, USERS_TBL ut, PROJECTS_TBL pt "
				+ "where uprt.USER_ID = ut.USER_ID AND uprt.PROJECT_ID = pt.PROJECT_ID ");
		if (cmd.hasOption("where")) {
			whereRelationQuery(cmd, tableName);
		}
		return stb.toString();
	}
	/**
	 * Method for Process for base app start app rel tbl.
	 *
	 * @param cmd {@link CommandLine}
	 * @return the string {@link String}
	 */
	private String processForBaseAppStartAppRelTbl(final CommandLine cmd, final String tableName) {
		String selectRelQuery = selectRelQuery(cmd, tableName);
		if(selectRelQuery==null) return null;
		stb.append(" FROM BASE_APPLICATIONS_TBL bat, START_APPLICATIONS_TBL sat where bat.BASE_APPLICATION_ID = sat.BASE_APPLICATION_ID ");
		if (cmd.hasOption("where")) {
			whereRelationQuery(cmd, tableName);
		}
		return stb.toString();
	}

	/**
	 * Method for Process for base app proj app rel tbl.
	 *
	 * @param cmd {@link CommandLine}
	 * @return the string {@link String}
	 */
	private String processForBaseAppProjAppRelTbl(final CommandLine cmd, final String tableName) {
		String selectRelQuery = selectRelQuery(cmd, tableName);
		if(selectRelQuery==null) return null;
		stb.append(" FROM BASE_APPLICATIONS_TBL bat, PROJECT_APPLICATIONS_TBL pat where bat.BASE_APPLICATION_ID = pat.BASE_APPLICATION_ID ");
		if (cmd.hasOption("where")) {
			whereRelationQuery(cmd, tableName);
		}
		return stb.toString();
	}

	/**
	 * Method for Process for base app user app rel tbl.
	 *
	 * @param cmd {@link CommandLine}
	 * @return the string {@link String}
	 */
	private String processForBaseAppUserAppRelTbl(final CommandLine cmd, final String tableName) {
		String selectRelQuery = selectRelQuery(cmd, tableName);
		if(selectRelQuery==null) return null;
		stb.append(" FROM BASE_APPLICATIONS_TBL bat, USER_APPLICATIONS_TBL uat where bat.BASE_APPLICATION_ID = uat.BASE_APPLICATION_ID ");
		if (cmd.hasOption("where")) {
			whereRelationQuery(cmd, tableName);
		}
		return stb.toString();
	}
	/**
	 * Method for Process for user table.
	 *
	 * @param cmd {@link CommandLine}
	 * @return the string {@link String}
	 * @throws ParseException the parse exception
	 */
	private String processForUserTable(final CommandLine cmd, final String tableName) {
		String selectQuery = selectQuery(cmd, tableName);
		if(selectQuery==null) return null;
		stb.append(" from USERS_TBL ut, USER_TRANSLATION_TBL utt, ICONS_TBL it "
				+ "where ut.USER_ID = utt.USER_ID AND it.ICON_ID = ut.ICON_ID ");
		if (cmd.hasOption("where")) {
			whereQuery(cmd, tableName);
		}
		return hasSelectOption(cmd, tableName);
	}

	
	/**
	 * Method for Process for site table.
	 *
	 * @param cmd {@link CommandLine}
	 * @return the string {@link String}
	 * @throws ParseException the parse exception
	 */
	private String processForSiteTable(final CommandLine cmd, final String tableName) {
		String selectQuery = selectQuery(cmd, tableName);
		if(selectQuery==null) return null;
		stb.append(" from SITES_TBL st, SITE_TRANSLATION_TBL stt, ICONS_TBL it "
				+ "where st.site_id = stt.site_id AND it.icon_id = st.icon_id ");
		if (cmd.hasOption("where")) {
			whereQuery(cmd, tableName);
		}
		return hasSelectOption(cmd, tableName);
	}
	
	/**
	 * Method for Process for admin area table.
	 *
	 * @param cmd {@link CommandLine}
	 * @return the string {@link String}
	 * @throws ParseException the parse exception
	 */
	private String processForAdminAreaTable(final CommandLine cmd, final String tableName) {
		String selectQuery = selectQuery(cmd, tableName);
		if(selectQuery==null) return null;
		stb.append(" from ADMIN_AREAS_TBL aat, ADMIN_AREA_TRANSLATION_TBL aatt, ICONS_TBL it "
				+ "where aat.admin_area_id = aatt.admin_area_id AND it.icon_id = aat.icon_id ");
		if (cmd.hasOption("where")) {
			whereQuery(cmd, tableName);
		}
		return hasSelectOption(cmd, tableName);
	}

	/**
	 * Method for Process for start app table.
	 *
	 * @param cmd {@link CommandLine}
	 * @return the string {@link String}
	 * @throws ParseException the parse exception
	 */
	private String processForStartAppTable(final CommandLine cmd, final String tableName) {
		String selectQuery = selectQuery(cmd, tableName);
		if(selectQuery==null) return null;
		stb.append(" from START_APPLICATIONS_TBL sat, START_APP_TRANSLATION_TBL satt, ICONS_TBL it "
				+ "where sat.START_APPLICATION_ID = satt.START_APPLICATION_ID AND it.ICON_ID = sat.ICON_ID ");
		if (cmd.hasOption("where")) {
			whereQuery(cmd, tableName);
		}
		return hasSelectOption(cmd, tableName);
	}

	/**
	 * Method for Process for proj app table.
	 *
	 * @param cmd {@link CommandLine}
	 * @return the string {@link String}
	 * @throws ParseException the parse exception
	 */
	private String processForProjAppTable(final CommandLine cmd, final String tableName) {
		String selectQuery = selectQuery(cmd, tableName);
		if(selectQuery==null) return null;
		stb.append(" from PROJECT_APPLICATIONS_TBL pat, PROJECT_APP_TRANSLATION_TBL patt, ICONS_TBL it "
				+ "where pat.PROJECT_APPLICATION_ID = patt.PROJECT_APPLICATION_ID AND it.ICON_ID = pat.ICON_ID ");
		if (cmd.hasOption("where")) {
			whereQuery(cmd, tableName);
		}
		return hasSelectOption(cmd, tableName);
	}

	/**
	 * Method for Process for user app table.
	 *
	 * @param cmd {@link CommandLine}
	 * @return the string {@link String}
	 * @throws ParseException the parse exception
	 */
	private String processForUserAppTable(final CommandLine cmd, final String tableName) {
		String selectQuery = selectQuery(cmd, tableName);
		if(selectQuery==null) return null;
		stb.append(" from USER_APPLICATIONS_TBL uat, USER_APP_TRANSLATION_TBL uatt, ICONS_TBL it "
				+ "where uat.USER_APPLICATION_ID = uatt.USER_APPLICATION_ID AND it.ICON_ID = uat.ICON_ID ");
		if (cmd.hasOption("where")) {
			whereQuery(cmd, tableName);
		}
		return hasSelectOption(cmd, tableName);
	}

	/**
	 * Method for Process for base app table.
	 *
	 * @param cmd {@link CommandLine}
	 * @return the string {@link String}
	 * @throws ParseException the parse exception
	 */
	private String processForBaseAppTable(final CommandLine cmd, final String tableName) {
		String selectQuery = selectQuery(cmd, tableName);
		if(selectQuery==null) return null;
		stb.append("from BASE_APPLICATIONS_TBL bat, BASE_APP_TRANSLATION_TBL batt, ICONS_TBL it "
				+ "where bat.BASE_APPLICATION_ID = batt.BASE_APPLICATION_ID AND it.ICON_ID = bat.ICON_ID ");
		if (cmd.hasOption("where")) {
			whereQuery(cmd, tableName);
		}
		return hasSelectOption(cmd, tableName);
	}

	/**
	 * Method for Process for project table.
	 *
	 * @param cmd {@link CommandLine}
	 * @return the string {@link String}
	 * @throws ParseException the parse exception
	 */
	private String processForProjectTable(final CommandLine cmd, final String tableName) {
		String selectQuery = selectQuery(cmd, tableName);
		if(selectQuery==null) return null;
		stb.append("from PROJECTS_TBL pt, PROJECT_TRANSLATION_TBL ptt, ICONS_TBL it "
				+ "where pt.project_id = ptt.project_id AND it.icon_id = pt.icon_id ");
		if (cmd.hasOption("where")) {
			whereQuery(cmd, tableName);
		}
		return hasSelectOption(cmd, tableName);
	}
	
	/**
	 * Method for Select query.
	 *
	 * @param cmd {@link CommandLine}
	 * @return the string {@link String}
	 * @throws ParseException 
	 */
	private String selectQuery(final CommandLine cmd, final String tableName){
		stb.append("SELECT ");
		String strVal="";
		if (cmd.hasOption("fields")) {
			String[] optionValues = cmd.getOptionValues("fields");
			StringBuilder validColumns = new StringBuilder();
			ObjectType objectType = ObjectType.getObjectType(tableName);
			for (int index = 0; index < optionValues.length; index++) {
				validColumns.append(optionValues[index]).append(" ");
			}
			ColumnChecker columnChecker = new ColumnChecker();
			String validColumnString = validColumns.toString().replace(",", " ");
			boolean column = columnChecker.getColumn(validColumnString, objectType, cmd);
			if(!column){
				return null;
			}
			if (optionValues.length > 0) {
				for (int index = 0; index < optionValues.length; index++) {
					if(index==0 && optionValues[index].equalsIgnoreCase("DISTINCT")){
						stb.append("DISTINCT ");
					} else {
						String copyStrVal = optionValues[index];
						/*copyStrVal = copyStrVal.replace(",", "");
						if(index < optionValues.length-1 && !copyStrVal.contains(",") && !copyStrVal.isEmpty()){
							copyStrVal = copyStrVal.concat(",");
						}*/
						/*if(index < optionValues.length-1 && !optionValues[index].contains(",") && !optionValues[index+1].contains(",")){
							copyStrVal = copyStrVal.concat(",");
						}*/
						if(copyStrVal.contains("'")) return null;
						strVal = strVal.concat(copyStrVal);
					}
				}
				int flag= 0;
				for(int index=0; index<strVal.length(); index++){
					char str='\'';
					char charAt = strVal.charAt(index);
					if(charAt==str){
						flag = flag+1;
						if(flag>=2 && flag%2==0){
							StringBuilder stb = new StringBuilder(strVal);
							stb.setCharAt(index, ' ');
							strVal = stb.toString();
						}
					}
				}
				strVal = strVal.replace("'", "");
				strVal = strVal.replace(",", " , ");
				String[] splittedStrVal = strVal.split(" ");
				String value;
				for (int i = 0; i < splittedStrVal.length; i++) {
					value=splittedStrVal[i];
					if (value.equalsIgnoreCase("name") && "user".equalsIgnoreCase(tableName)) {
						stb.append("UT.USERNAME as USER_NAME ");
					} /*else if (value.equalsIgnoreCase("name") && "userapp".equalsIgnoreCase(tableName)) {
						stb.append("UAT.NAME as USER_APP_NAME ");
					} else if (value.equalsIgnoreCase("name") && "projectapp".equalsIgnoreCase(tableName)) {
						stb.append("PAT.NAME as PROJECT_APP_NAME ");
					}*/ else if (value.equalsIgnoreCase("name") && "startapp".equalsIgnoreCase(tableName)) {
						stb.append("SAT.NAME as START_APP_NAME ");
					} else if (value.equalsIgnoreCase("name") && "baseapp".equalsIgnoreCase(tableName)) {
						stb.append("BAT.NAME as BASE_APP_NAME ");
					} else if (value.equalsIgnoreCase("name") && "site".equalsIgnoreCase(tableName)) {
						stb.append("ST.NAME as SITE_NAME ");
					} else if (value.equalsIgnoreCase("name") && "adminarea".equalsIgnoreCase(tableName)) {
						stb.append("AAT.NAME as ADMINAREA_NAME ");
					} else if (value.equalsIgnoreCase("name") && "project".equalsIgnoreCase(tableName)) {
						stb.append("PT.NAME as PROJECT_NAME ");
					} else if (value.equalsIgnoreCase("status")) {
						selectStatus(tableName);
						stb.append("STATUS ");
					} else if (value.equalsIgnoreCase("name")) {
						if(tableName.equalsIgnoreCase("userapp") || tableName.equalsIgnoreCase("projectapp")) {
							selectNameDescription(tableName);
							stb.append(value).append(", ");
							selectNameDescription_en_de(tableName);
							stb.append("NAME as DISPLAY_NAME").append(" ");
						} else {
							stb.append(value).append(" ");
						}
					} else if (value.equalsIgnoreCase("description")) {
						if(tableName.equalsIgnoreCase("userapp") || tableName.equalsIgnoreCase("projectapp")) {
							selectNameDescription(tableName);
							stb.append(value).append(", ");
							selectNameDescription_en_de(tableName);
							stb.append("DESCRIPTION as DISPLAY_DESCRIPTION").append(" , ").append("LANGUAGE_CODE ");
						} else {
							stb.append(value).append(" , ").append("LANGUAGE_CODE ");
						}
					} else if (value.equalsIgnoreCase("remarks")) {
						stb.append(value).append(" , ").append("LANGUAGE_CODE ");
					} else {
						stb.append(value).append(" ");
					}
				}
			}
		} else {
				selectType(cmd, tableName);
		}
		return stb.append(" ").toString();
	}
	
	
	/**
	 * Method for Select status.
	 *
	 * @param tableName {@link String}
	 * @return the string {@link String}
	 */
	private String selectStatus(String tableName) {
		ObjectType objectType = ObjectType.getObjectType(tableName);
		switch (objectType) {
		case SITE:
			stb.append("st.");
			return stb.toString();
		case ADMINAREA:
			stb.append("aat.");
			return stb.toString();
		case PROJECT:
			stb.append("pt.");
			return stb.toString();
		case USER:
			stb.append("ut.");
			return stb.toString();
		case USERAPP:
			stb.append("uat.");
			return stb.toString();
		case PROJECTAPP:
			stb.append("pat.");
			return stb.toString();
		case STARTAPP:
			stb.append("sat.");
			return stb.toString();
		case BASEAPP:
			stb.append("bat.");
			return stb.toString();
		default:
			break;
		}
		return null;
	}

	/**
	 * Method for Select description.
	 *
	 * @param tableName {@link String}
	 * @return the string {@link String}
	 */
	private String selectNameDescription(String tableName) {
		ObjectType objectType = ObjectType.getObjectType(tableName);
		switch (objectType) {
		case USERAPP:
			stb.append("uat.");
			return stb.toString();
		case PROJECTAPP:
			stb.append("pat.");
			return stb.toString();
		default:
			break;
		}
		return null;
	}
	
	/**
	 * Method for Select description en de.
	 *
	 * @param tableName {@link String}
	 * @return the string {@link String}
	 */
	private String selectNameDescription_en_de(String tableName) {
		ObjectType objectType = ObjectType.getObjectType(tableName);
		switch (objectType) {
		case USERAPP:
			stb.append("uatt.");
			return stb.toString();
		case PROJECTAPP:
			stb.append("patt.");
			return stb.toString();
		default:
			break;
		}
		return null;
	}
	/**
	 * Method for Select rel query.
	 *
	 * @param cmd {@link CommandLine}
	 * @param tableName {@link String}
	 * @return the string {@link String}
	 * @throws ParseException the parse exception
	 */
	private String selectRelQuery(final CommandLine cmd, final String tableName) {
		stb.append("SELECT ");
		String strVal="";
		if (cmd.hasOption("fields")) {
			String[] optionValues = cmd.getOptionValues("fields");
			StringBuilder validColumns = new StringBuilder();
			ObjectRelationType objectRelType = ObjectRelationType.getObjectRelType(tableName);
			for (int index = 0; index < optionValues.length; index++) {
				validColumns.append(optionValues[index]).append(" ");
			}
			ColumnChecker columnChecker = new ColumnChecker();
			String validColumnString = validColumns.toString().replace(",", " ");
			boolean column = columnChecker.getRelColumn(validColumnString, objectRelType, cmd);
			if(!column){
				return null;
			}
			if (optionValues.length > 0) {
				for (int index = 0; index < optionValues.length; index++) {
					if(index==0 && optionValues[index].equalsIgnoreCase("DISTINCT")){
						stb.append("DISTINCT ");
					} else {
						String copyStrVal=optionValues[index];
						strVal = strVal.concat(copyStrVal);
					}
				}
				int flag= 0;
				for(int index=0; index<strVal.length(); index++){
					char str='\'';
					char charAt = strVal.charAt(index);
					if(charAt==str){
						flag = flag+1;
						if(flag>=2 && flag%2==0){
							StringBuilder stb = new StringBuilder(strVal);
							stb.setCharAt(index, ' ');
							strVal = stb.toString();
						}
					}
				}
				strVal = strVal.replace("'", "");
				strVal = strVal.replace(",", " , ");
				String[] splittedStrVal = strVal.split(" ");
				String value;
				for (int i = 0; i < splittedStrVal.length; i++) {
					value=splittedStrVal[i];
					if (value.equalsIgnoreCase("site_name")) {
						stb.append("ST.NAME as SITE_NAME ");
					} else if (value.equalsIgnoreCase("adminarea_name")) {
						stb.append("AAT.NAME as ADMIN_AREA_NAME ");
					} else if (value.equalsIgnoreCase("project_name")) {
						stb.append("PT.NAME as PROJECT_NAME ");
					} else if (value.equalsIgnoreCase("user_name")) {
						stb.append("UT.USERNAME as USER_NAME ");
					} else if (value.equalsIgnoreCase("userapp_name")) {
						stb.append("UAT.NAME as USER_APP_NAME ");
					} else if (value.equalsIgnoreCase("projectapp_name")) {
						stb.append("PAT.NAME as PROJECT_APP_NAME ");
					} else if (value.equalsIgnoreCase("startapp_name")) {
						stb.append("SAT.NAME as START_APP_NAME ");
					} else if (value.equalsIgnoreCase("baseapp_name")) {
						stb.append("BAT.NAME as BASE_APP_NAME ");
					} else if (value.equalsIgnoreCase("status")) {
						selectRelationStatus(tableName);
						stb.append("STATUS ");
					} else {
						stb.append(value);
					}
				}
			}
		} else {
			selectType(cmd, tableName);
		}
		return stb.toString();
	}
	/**
	 * @param cmd
	 * @param tableName
	 * @throws ParseException
	 */
	private String selectType(final CommandLine cmd, final String tableName) {
		String typeArgument = null;
		try {
			typeArgument = String.valueOf(cmd.getParsedOptionValue("type"));
		} catch (ParseException e) {
			System.out.println("ERROR: Syntax error ! use -help option for Help ");
		}
		switch (typeArgument) {
			case "base_data":
				return selectBaseData(cmd, tableName);
			case "relation":
				return selectRelation(cmd, tableName);
			default:
				break;
		}
		return null;
	}
	
	/**
	 * Method for Where query.
	 *
	 * @param cmd {@link CommandLine}
	 * @return the string {@link String}
	 */
	private String whereQuery(final CommandLine cmd, final String tableName) {
		String[] optionValues = cmd.getOptionValues("where");
		if (optionValues.length > 0) {
			stb.append(" ").append("AND").append(" ");
			for (String value : optionValues) {
				value = value.trim();
				if(value.contains(" ")){
					StringBuilder valStb = new StringBuilder();
					valStb.append("\"'").append(value).append("'\"");
					value = valStb.toString();
				}
				if(containsIgnoreCase(value,"name=")/*value.contains("NAME=")||value.contains("name=")*/){
					
					String copyStrVal=value;
					String replace;
					String[] splittedStrVal = null;
					if(copyStrVal.contains("\"")){
						replace =copyStrVal.replace("\"", "");
						replace =replace.replace("'", "");
						
						String[] split = replace.split("=");
						String columnName = split[0];
						String columnValue = split[1];
						columnValue = "='"+columnValue+"'";
						
						if (columnName.equalsIgnoreCase("name") && "user".equalsIgnoreCase(tableName)) {
							stb.append("UT.USERNAME ");
						} else if (columnName.equalsIgnoreCase("name") && "userapp".equalsIgnoreCase(tableName)) {
							stb.append("UAT.NAME ");
						} else if (columnName.equalsIgnoreCase("name") && "projectapp".equalsIgnoreCase(tableName)) {
							stb.append("PAT.NAME ");
						} else if (columnName.equalsIgnoreCase("name") && "startapp".equalsIgnoreCase(tableName)) {
							stb.append("SAT.NAME ");
						} else if (columnName.equalsIgnoreCase("name") && "baseapp".equalsIgnoreCase(tableName)) {
							stb.append("BAT.NAME ");
						} else if (columnName.equalsIgnoreCase("status")) {
							selectStatus(tableName);
							stb.append("STATUS ");
						} else if (columnName.equalsIgnoreCase("name")) {
							stb.append(columnName).append(" ");
						} else {
							stb.append(columnValue).append(" ");
						}
						columnName = "";
						if(!columnValue.isEmpty()){
							stb.append(columnValue).append(" ");
						}
					} else {
						replace = copyStrVal.replace("=", " = ");
						splittedStrVal = replace.split(" ");
						for (int index = 0; index < splittedStrVal.length; index++) {
							value = splittedStrVal[index];
							if (value.equalsIgnoreCase("name") && "user".equalsIgnoreCase(tableName)) {
								stb.append("UT.USERNAME ");
							} else if (value.equalsIgnoreCase("name") && "userapp".equalsIgnoreCase(tableName)) {
								stb.append("UAT.NAME ");
							} else if (value.equalsIgnoreCase("name") && "projectapp".equalsIgnoreCase(tableName)) {
								stb.append("PAT.NAME ");
							} else if (value.equalsIgnoreCase("name") && "startapp".equalsIgnoreCase(tableName)) {
								stb.append("SAT.NAME ");
							} else if (value.equalsIgnoreCase("name") && "baseapp".equalsIgnoreCase(tableName)) {
								stb.append("BAT.NAME ");
							} else if (value.equalsIgnoreCase("status")) {
								selectStatus(tableName);
								stb.append("STATUS ");
							} else if (value.equalsIgnoreCase("name")) {
								stb.append(value).append(" ");
							} else {
								stb.append(value).append(" ");
							}
							value = "";
						}
					}
				} else if (value.equalsIgnoreCase("name") && "user".equalsIgnoreCase(tableName)) {
					stb.append("UT.USERNAME ");
				} else if (value.equalsIgnoreCase("name") && "userapp".equalsIgnoreCase(tableName)) {
					stb.append("UAT.NAME ");
				} else if (value.equalsIgnoreCase("name") && "projectapp".equalsIgnoreCase(tableName)) {
					stb.append("PAT.NAME ");
				} else if (value.equalsIgnoreCase("name") && "startapp".equalsIgnoreCase(tableName)) {
					stb.append("SAT.NAME ");
				} else if (value.equalsIgnoreCase("name") && "baseapp".equalsIgnoreCase(tableName)) {
					stb.append("BAT.NAME ");
				} else if (value.equalsIgnoreCase("status")) {
					selectStatus(tableName);
					stb.append("STATUS ");
				} else if (value.equalsIgnoreCase("name")) {
					stb.append(value).append(" ");
				} else if(!value.isEmpty()){
					stb.append(value).append(" ");
				}
			}
		}
		return stb.toString();
	}
	
	/**
	 * Method for Where relation query.
	 *
	 * @param cmd {@link CommandLine}
	 * @param tableName {@link String}
	 * @return the string {@link String}
	 */
	private String whereRelationQuery(final CommandLine cmd, final String tableName) {
		String[] optionValues = cmd.getOptionValues("where");
		if (optionValues.length > 0) {
			stb.append(" ").append("AND").append(" ");
			for (String value : optionValues) {
				value = value.trim();
				if(value.contains(" ")){
					StringBuilder valStb = new StringBuilder();
					valStb.append("\"'").append(value).append("'\"");
					value = valStb.toString();
				}
				if(containsIgnoreCase(value,"adminarea=")/*value.contains("adminarea=")*/||containsIgnoreCase(value,"baseapp=")/*value.contains("baseapp=")*/||containsIgnoreCase(value,"userapp=")/*value.contains("userapp=")*/
						||containsIgnoreCase(value,"projectapp=")/*value.contains("projectapp=")*/||containsIgnoreCase(value,"startapp=")/*value.contains("startapp=")*/||containsIgnoreCase(value,"user=")/*value.contains("user=")*/
						||containsIgnoreCase(value,"project=")/*value.contains("project=")*/||containsIgnoreCase(value,"site=")/*value.contains("site=")*/){
					String copyStrVal=value;
					String replace;
					String[] splittedStrVal = null;
					if(copyStrVal.contains("\"")){
						replace =copyStrVal.replace("\"", "");
						replace =replace.replace("'", "");
						
						String[] split = replace.split("=");
						String columnName = split[0];
						String columnValue = split[1];
						columnValue = "='"+columnValue+"'";
						
						if("site".equalsIgnoreCase(columnName)){
							stb.append("st.");
							value="NAME";
						} else if("adminarea".equalsIgnoreCase(columnName)){
							stb.append("aat.");
							value="NAME";
						} else if("project".equalsIgnoreCase(columnName)){
							stb.append("pt.");
							value="NAME";
						} else if("baseapp".equalsIgnoreCase(columnName)){
							stb.append("bat.");
							value="NAME";
						} else if("startapp".equalsIgnoreCase(columnName)){
							stb.append("sat.");
							value="NAME";
						} else if("projectapp".equalsIgnoreCase(columnName)){
							stb.append("pat.");
							value="NAME";
						} else if("userapp".equalsIgnoreCase(columnName)){
							stb.append("uat.");
							value="NAME";
						} else if("user".equalsIgnoreCase(columnName)){
							stb.append("ut.");
							value="USERNAME";
						}
						stb.append(value).append(" ");
						value="";
						if(!columnValue.isEmpty()){
							stb.append(columnValue).append(" ");
						}
					} else {
						replace =copyStrVal.replace("=", " = ");
						splittedStrVal = replace.split(" ");
						for (int i = 0; i < splittedStrVal.length; i++) {
							value=splittedStrVal[i];
							if("site".equalsIgnoreCase(value)){
								stb.append("st.");
								value="NAME";
							} else if("adminarea".equalsIgnoreCase(value)){
								stb.append("aat.");
								value="NAME";
							} else if("project".equalsIgnoreCase(value)){
								stb.append("pt.");
								value="NAME";
							} else if("baseapp".equalsIgnoreCase(value)){
								stb.append("bat.");
								value="NAME";
							} else if("startapp".equalsIgnoreCase(value)){
								stb.append("sat.");
								value="NAME";
							} else if("projectapp".equalsIgnoreCase(value)){
								stb.append("pat.");
								value="NAME";
							} else if("userapp".equalsIgnoreCase(value)){
								stb.append("uat.");
								value="NAME";
							} else if("user".equalsIgnoreCase(value)){
								stb.append("ut.");
								value="USERNAME";
							}
							stb.append(value).append(" ");
							value="";
						}
					}
				} else if("site".equalsIgnoreCase(value)){
					stb.append("st.");
					value="NAME";
				} else if("adminarea".equalsIgnoreCase(value)){
						stb.append("aat.");
						value="NAME";
				} else if("project".equalsIgnoreCase(value)){
						stb.append("pt.");
						value="NAME";
				} else if("baseapp".equalsIgnoreCase(value)){
						stb.append("bat.");
						value="NAME";
				} else if("startapp".equalsIgnoreCase(value)){
						stb.append("sat.");
						value="NAME";
				} else if("projectapp".equalsIgnoreCase(value)){
						stb.append("pat.");
						value="NAME";
				} else if("userapp".equalsIgnoreCase(value)){
						stb.append("uat.");
						value="NAME";
				} else if("user".equalsIgnoreCase(value)){
					stb.append("ut.");
					value="USERNAME";
				} else if("DISTINCT".equalsIgnoreCase(value)){
					stb = new StringBuilder();
					return stb.toString();
				} else if(value.contains("status") || value.contains("STATUS")){
						selectRelationStatus(tableName);
				}
				if(!value.isEmpty()){
					stb.append(value).append(" ");
				}
			}
		}
		return stb.toString();
	}
	/**
	 * Method for Select base data.
	 *
	 * @param cmd {@link CommandLine}
	 * @param tableName {@link String}
	 */
	private String selectBaseData(CommandLine cmd, String tableName) {
		ObjectType objectType = ObjectType.getObjectType(tableName);
		switch (objectType) {
		case USER:
			stb.append(" ut.USERNAME AS USER_NAME, ").append("ut.FULL_NAME, ")
					.append("ut.EMAIL_ID, ").append("ut.TELEPHONE_NUMBER, ").append("ut.DEPARTMENT, ")
					.append("ut.STATUS, ").append("it.ICON_NAME ");
			return stb.toString();
		case SITE:
			stb.append(" st.NAME AS SITE_NAME, ").append("st.STATUS, ").append("it.ICON_NAME ");
			return stb.toString();
		case ADMINAREA:
			stb.append(" aat.NAME AS ADMIN_AREA_NAME, ").append("aat.STATUS, ").append("it.ICON_NAME ");
			return stb.toString();
		case PROJECT:
			stb.append(" pt.NAME AS PROJECT_NAME, ").append("pt.STATUS, ").append("it.ICON_NAME ");
			return stb.toString();
		case BASEAPP:
			stb.append(" bat.NAME AS BASE_APP_NAME, ").append("bat.STATUS, ").append("it.ICON_NAME ");
			return stb.toString();
		case USERAPP:
			stb.append(" uat.NAME AS USER_APP_NAME, ").append("uat.STATUS, ").append("it.ICON_NAME ");
			return stb.toString();
		case PROJECTAPP:
			stb.append(" pat.NAME AS PROJECT_APP_NAME, ").append("pat.STATUS, ").append("it.ICON_NAME ");
			return stb.toString();
		case STARTAPP:
			stb.append(" sat.NAME AS START_APP_NAME, ").append("sat.STATUS, ").append("it.ICON_NAME ");
			return stb.toString();
		default:
			break;
		}
		return null;
	}
	
	/**
	 * Method for Select relation.
	 *
	 * @param cmd {@link CommandLine}
	 * @param tableName {@link String}
	 */
	private String selectRelation(CommandLine cmd, String tableName) {
		ObjectRelationType objectRelType = ObjectRelationType.getObjectRelType(tableName);
		switch (objectRelType) {
		case USER_PROJECT:
			stb.append(" ut.USERNAME as USER_NAME, ").append("pt.NAME as PROJECT_NAME, ").append("uprt.STATUS, ").append("PROJECT_EXPIRY_DAYS ");
			return stb.toString();
		case ADMINAREA_PROJECT:
			stb.append("st.NAME as SITE_NAME, ").append("aat.NAME as ADMIN_AREA_NAME, ").append("pt.NAME as PROJECT_NAME, ").append("aaprt.STATUS ");
			return stb.toString();
		case ADMINAREA_USERAPP:
			stb.append("st.NAME as SITE_NAME, ").append("aat.NAME as ADMIN_AREA_NAME, ").append("uat.NAME as USER_APP_NAME, ").append("aauart.STATUS, ").append("aauart.REL_TYPE ");
			return stb.toString();
		case ADMINAREA_STARTAPP:
			stb.append("st.NAME as SITE_NAME, ").append("aat.NAME as ADMIN_AREA_NAME, ").append("sat.NAME as START_APP_NAME, ").append("aasart.STATUS ");
			return stb.toString();
		case SITE_ADMINAREA:
			stb.append("st.NAME as SITE_NAME, ").append("aat.NAME as ADMIN_AREA_NAME, ").append("saart.STATUS ");
			return stb.toString();
		case ADMINAREA_PROJECT_PROJECTAPP:
			stb.append("st.NAME as SITE_NAME, ").append("aat.NAME as ADMIN_AREA_NAME, ").append("pt.NAME as PROJECT_NAME, ").append("pat.NAME as PROJECT_APP_NAME, ").append("aapart.STATUS, ").append("aapart.REL_TYPE ");
			return stb.toString();
		case ADMINAREA_PROJECT_STARTAPP:
			stb.append("st.NAME as SITE_NAME, ").append("aat.NAME as ADMIN_AREA_NAME, ").append("pt.NAME as PROJECT_NAME, ").append("sat.NAME as START_APP_NAME, ").append("psart.STATUS ");
			return stb.toString();
		case USER_STARTAPP:
			stb.append("ut.USERNAME as USER_NAME, ").append("sat.NAME as START_APP_NAME, ").append("usart.STATUS ");
			return stb.toString();
		case USER_PROJECT_PROJECTAPP:
			stb.append("ut.USERNAME as USER_NAME, ").append("st.NAME as SITE_NAME, ").append("aat.NAME as ADMIN_AREA_NAME, ").append("pt.NAME as PROJECT_NAME, ").append("pat.NAME as PROJECT_APP_NAME, ").append("upart.USER_REL_TYPE ");
			return stb.toString();
		case USER_USERAPP:
			stb.append("ut.USERNAME as USER_NAME, ").append("st.NAME as SITE_NAME, ").append("aat.NAME as ADMIN_AREA_NAME, ").append("uat.NAME as USER_APP_NAME, ").append("uuart.USER_REL_TYPE ");
			return stb.toString();
		case BASEAPP_USERAPP:
			stb.append(" bat.NAME as BASE_APP_NAME, ").append("uat.NAME as USER_APP_NAME ");
			return stb.toString();
		case BASEAPP_PROJECTAPP:
			stb.append(" bat.NAME as BASE_APP_NAME, ").append("pat.NAME as PROJECT_APP_NAME ");
			return stb.toString();
		case BASEAPP_STARTAPP:
			stb.append(" bat.NAME as BASE_APP_NAME, ").append("sat.NAME as START_APP_NAME ");
			return stb.toString();
		default:
			break;
		}
		return null;
		
	}
	
	/**
	 * Method for Select relation status.
	 *
	 * @param cmd {@link CommandLine}
	 * @param tableName {@link String}
	 * @return the string {@link String}
	 */
	private String selectRelationStatus(String tableName) {
		ObjectRelationType objectRelType = ObjectRelationType.getObjectRelType(tableName);
		switch (objectRelType) {
		case USER_PROJECT:
			stb.append("uprt.");
			return stb.toString();
		case ADMINAREA_PROJECT:
			stb.append("aaprt.");
			return stb.toString();
		case ADMINAREA_USERAPP:
			stb.append("aauart.");
			return stb.toString();
		case ADMINAREA_STARTAPP:
			stb.append("aasart.");
			return stb.toString();
		case SITE_ADMINAREA:
			stb.append("saart.");
			return stb.toString();
		case ADMINAREA_PROJECT_PROJECTAPP:
			stb.append("aapart.");
			return stb.toString();
		case ADMINAREA_PROJECT_STARTAPP:
			stb.append("psart.");
			return stb.toString();
		case USER_STARTAPP:
			stb.append("usart.");
			return stb.toString();
		case USER_PROJECT_PROJECTAPP:
			stb.append("upart.");
			return stb.toString();
		case USER_USERAPP:
			stb.append("uuart.");
			return stb.toString();
		case BASEAPP_USERAPP:
			return stb.toString();
		case BASEAPP_PROJECTAPP:
			return stb.toString();
		case BASEAPP_STARTAPP:
			return stb.toString();
		default:
			break;
		}
		return null;
		
	}
	
	/**
	 * Method for Checks for select option.
	 *
	 * @param cmd {@link CommandLine}
	 * @param tableName {@link String}
	 * @return the string {@link String}
	 */
	private String hasSelectOption(final CommandLine cmd, final String tableName) {
		if(cmd.hasOption("select")){
			String[] optionValues = cmd.getOptionValues("select");
			for (int index = 0; index < optionValues.length; index++) {
				if(index!=0){
					String value = optionValues[index];
					if (value.equalsIgnoreCase("name") && "user".equalsIgnoreCase(tableName)) {
						stb.append("UT.USERNAME ");
					} else if (value.equalsIgnoreCase("name") && "userapp".equalsIgnoreCase(tableName)) {
						stb.append("UAT.NAME ");
					} else if (value.equalsIgnoreCase("name") && "projectapp".equalsIgnoreCase(tableName)) {
						stb.append("PAT.NAME ");
					} else if (value.equalsIgnoreCase("name") && "startapp".equalsIgnoreCase(tableName)) {
						stb.append("SAT.NAME ");
					} else if (value.equalsIgnoreCase("name") && "baseapp".equalsIgnoreCase(tableName)) {
						stb.append("BAT.NAME ");
					} else if (value.equalsIgnoreCase("status")) {
						selectStatus(tableName);
						stb.append("STATUS ");
					} else if (value.equalsIgnoreCase("name")) {
						stb.append(value).append(" ");
					} else {
						stb.append(value).append(" ");
					}
				}
			}
		}
		return stb.toString();
	}
	
	/*private String selectObjectName(String tableName) {
		ObjectType objectType = ObjectType.getObjectType(tableName);
		switch (objectType) {
		case USER:
			stb.append("ut.");
			return stb.toString();
		case SITE:
			stb.append("st.");
			return stb.toString();
		case ADMINAREA:
			stb.append("aat.");
			return stb.toString();
		case PROJECT:
			stb.append("pt.");
			return stb.toString();
		case BASEAPP:
			stb.append("bat.");
			return stb.toString();
		case USERAPP:
			stb.append("uat.");
			return stb.toString();
		case PROJECTAPP:
			stb.append("pat.");
			return stb.toString();
		case STARTAPP:
			stb.append("sat.");
			return stb.toString();
		default:
			break;
		}
		return null;
		
	}*/
	
	/**
	 * Method for Contains ignore case.
	 *
	 * @param str {@link String}
	 * @param searchStr {@link String}
	 * @return true, if successful
	 */
	public boolean containsIgnoreCase(String str, String searchStr) {
        if (str == null || searchStr == null) {
            return false;
        }
        int len = searchStr.length();
        int max = str.length() - len;
        for (int i = 0; i <= max; i++) {
            if (str.regionMatches(true, i, searchStr, 0, len)) {
                return true;
            }
        }
        return false;
    }
}
