package com.magna.batch.restclient.obj.userapp;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.magna.batch.restclient.BatchRestController;
import com.magna.batch.restclient.exception.UnauthorizedAccessException;
import com.magna.batch.utils.RestClientConstants;
import com.magna.xmbackend.vo.userApplication.UserApplicationCustomResponse;
import com.magna.xmbackend.vo.userApplication.UserApplicationRequest;

/**
 * Class for User app controller.
 *
 * @author Roshan.Ekka
 */
public class UserAppController extends BatchRestController {

	/** The Constant LOGGER. */
	/*private static final Logger LOGGER = LoggerFactory.getLogger(UserAppController.class);*/
	
	/**
	 * Constructor for UserAppController Class.
	 */
	public UserAppController() {
		super();
	}
	
	/**
	 * Method for Save user app.
	 *
	 * @param userAppRequest {@link UserApplicationRequest}
	 * @return the boolean {@link Boolean}
	 */
	public Boolean saveUserApp(UserApplicationRequest userAppRequest) {
		try {
			String url = new String(this.serviceUrl + RestClientConstants.CREATE_USERAPP);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<UserApplicationRequest> request = new HttpEntity<UserApplicationRequest>(userAppRequest, this.headers);
			ResponseEntity<Boolean> responseEntity = restTemplate.exchange(url, HttpMethod.POST,request, new ParameterizedTypeReference<Boolean>() {});
			HttpStatus statusCode = responseEntity.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				return responseEntity.getBody();
			} else {
				//LOGGER.error("Error while calling XMBATCH Create User Application REST Service, returns the status code: ", //$NON-NLS-1$
					//	statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException)e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				} else if (e instanceof HttpStatusCodeException) {
					String errorResponse = ((HttpStatusCodeException) e).getResponseBodyAsString();
					ObjectMapper mapper = new ObjectMapper();
					JsonNode jsonNode = null;
					try {
						jsonNode = mapper.readTree(errorResponse);
					} catch (IOException e1) {
						System.out.println("ERROR: Server Error! Unable to Create Site");
						return false;
					}
					String expMsg = null;
					if(jsonNode.get("lingualMessages") != null) {
						expMsg = jsonNode.get("lingualMessages").get("en").textValue();
					} else if(jsonNode.get("description") != null) {
						expMsg = jsonNode.get("description").asText();
					}
					System.out.println(expMsg);
					return false;
				} 
			} 
		} catch (Exception e) {
			//LOGGER.error("Error while calling XMBATCH Create User Application REST Service", e); //$NON-NLS-1$
		}
		return null;
	}
	
	/**
	 * Gets the user app by name.
	 *
	 * @param userAppName {@link String}
	 * @return the user app by name
	 */
	public UserApplicationCustomResponse getUserAppByName(final String userAppName) {
		try {
			String url = new String(this.serviceUrl + RestClientConstants.GET_USER_APP_BY_NAME + "/" + userAppName);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<Object> requestEntity = new HttpEntity<Object>(headers);
			
			List<HttpMessageConverter<?>> messageConverters = new ArrayList<>();
			messageConverters.add(new MappingJackson2HttpMessageConverter());
			restTemplate.setMessageConverters(messageConverters);

			ResponseEntity<UserApplicationCustomResponse> responseList = restTemplate.exchange(url, HttpMethod.GET, requestEntity,
					UserApplicationCustomResponse.class);
			HttpStatus statusCode = responseList.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				UserApplicationCustomResponse startAppTbl = responseList.getBody();
				return startAppTbl;
			} else {
				//LOGGER.error("Error while calling XMBATCH Get User Application By Name REST Service, returns the status code: " //$NON-NLS-1$
				//		+ statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException)e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				} else if (e instanceof HttpStatusCodeException) {
					String errorResponse = ((HttpStatusCodeException) e).getResponseBodyAsString();
					ObjectMapper mapper = new ObjectMapper();
					JsonNode jsonNode = null;
					try {
						jsonNode = mapper.readTree(errorResponse);
					} catch (IOException e1) {
						System.out.println("ERROR: Server Error! Unable to Get User Application By Name");
						return null;
					}
					String expMsg = null;
					if(jsonNode.get("lingualMessages") != null) {
						expMsg = jsonNode.get("lingualMessages").get("en").textValue();
					} else if(jsonNode.get("description") != null) {
						expMsg = jsonNode.get("description").asText();
					}
					System.out.println(expMsg);
					return null;
				} 
			} 
		} catch (Exception e) {
			//LOGGER.error("Error while calling XMBATCH Get User Application By Name REST Service", e); //$NON-NLS-1$
		}
		return null;
	}
	
	/**
	 * Method for Delete user app.
	 *
	 * @param name {@link String}
	 * @return true, if successful
	 */
	public boolean deleteUserApp(String name) {
		try {
			String url = new String(this.serviceUrl + RestClientConstants.DELETE_USER_APP);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<String> requestEntity = new HttpEntity<String>(name, this.headers);
			ResponseEntity<Boolean> responseEntity = restTemplate.exchange(url, HttpMethod.POST, requestEntity,
					Boolean.class);
			HttpStatus statusCode = responseEntity.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				return responseEntity.getBody();
			} else {
				//LOGGER.error("Error while calling XMBATCH Delete User Application REST Service, returns the status code: ", //$NON-NLS-1$
						//statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException)e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				} else if (e instanceof HttpStatusCodeException) {
					String errorResponse = ((HttpStatusCodeException) e).getResponseBodyAsString();
					ObjectMapper mapper = new ObjectMapper();
					JsonNode jsonNode = null;
					try {
						jsonNode = mapper.readTree(errorResponse);
					} catch (IOException e1) {
						System.out.println("ERROR: Server Error! Unable to Delete User Application");
						return false;
					}
					String expMsg = null;
					if(jsonNode.get("lingualMessages") != null) {
						expMsg = jsonNode.get("lingualMessages").get("en").textValue();
					} else if(jsonNode.get("description") != null) {
						expMsg = jsonNode.get("description").asText();
					}
					System.out.println(expMsg);
					return false;
				} 
			} 
		} catch (Exception e) {
			//LOGGER.error("Error while calling XMBATCH Delete User Application REST Service!", e); //$NON-NLS-1$
		}
		return false;
	}
	
	/**
	 * Method for Update user app.
	 *
	 * @param userAppRequest {@link UserApplicationRequest}
	 * @return the boolean {@link Boolean}
	 */
	public Boolean updateUserApp(UserApplicationRequest userAppRequest){
		try {
			String url = new String(this.serviceUrl + RestClientConstants.UPDATE_USERAPP);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<UserApplicationRequest> request = new HttpEntity<UserApplicationRequest>(userAppRequest, this.headers);
			ResponseEntity<Boolean> responseEntity = restTemplate.exchange(url, HttpMethod.POST,request, new ParameterizedTypeReference<Boolean>() {});
			HttpStatus statusCode = responseEntity.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				return responseEntity.getBody();
			} else {
				//LOGGER.error("Error while calling XMBATCH Update User Application REST Service, returns the status code: ", //$NON-NLS-1$
						//statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException)e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				} else if (e instanceof HttpStatusCodeException) {
					String errorResponse = ((HttpStatusCodeException) e).getResponseBodyAsString();
					ObjectMapper mapper = new ObjectMapper();
					JsonNode jsonNode;
					try {
						jsonNode = mapper.readTree(errorResponse);
					} catch (IOException e1) { 
						System.out.println("ERROR: Server Error! Unable to Update User Application");
					return false;
					}
					String expMsg = null;
					if(jsonNode.get("lingualMessages") != null) {
						expMsg = jsonNode.get("lingualMessages").get("en").textValue();
					} else if(jsonNode.get("description") != null) {
						expMsg = jsonNode.get("description").asText();
					}
					System.out.println(expMsg);
					return false;
				} 
			} 
		} catch (Exception e) {
			//LOGGER.error("Error while calling XMBATCH Update User Application REST Service", e); //$NON-NLS-1$
		}
		return null;
	}
}
