package com.magna.batch.utils;

import java.util.Locale;

/**
 * The Enum LANG_ENUM.
 * 
 * @author shashwat.anand
 */
public enum LANG_ENUM {

	/** The english. */
	ENGLISH(new Locale.Builder().setLanguage("en").setRegion("GB").build()),

	/** The german. */
	GERMAN(new Locale.Builder().setLanguage("de").setRegion("DE").build());

	/** The locale. */
	private Locale locale;

	/**
	 * Instantiates a new lang enum.
	 *
	 * @param locale
	 *            the locale
	 */
	LANG_ENUM(Locale locale) {
		this.locale = locale;
	}

	/**
	 * Gets the lang code.
	 *
	 * @return the lang code
	 */
	public String getLangCode() {
		return this.locale.getLanguage();
	}

	/**
	 * Gets the country.
	 *
	 * @return the country
	 */
	public String getCountry() {
		return this.locale.getCountry();
	}

	/**
	 * Gets the locale.
	 *
	 * @return the locale
	 */
	public Locale getLocale() {
		return this.locale;
	}
	
	/**
	 * Gets the lang enum.
	 *
	 * @param langCode the lang code
	 * @return the lang enum
	 */
	public static LANG_ENUM getLangEnum(String langCode) {
        for (LANG_ENUM value : values()) {
            if (langCode.equalsIgnoreCase(value.getLangCode())) {
                return value;
            }
        }
        return null;
    }
}
