package com.magna.batch.utils;

import java.io.FileInputStream;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The Class CaxStartBatchProps.
 * 
 * @author shashwat.anand
 */
public class CaxStartBatchProps extends Properties {
	
	/** Logger instance. */
	private static final Logger LOGGER = LoggerFactory.getLogger(CaxStartBatchProps.class);
	
	/** serialVersionUID for warning. */
	private static final long serialVersionUID = 1L;
	
	/** The this ref. */
	private static CaxStartBatchProps thisRef;
	
	/**
	 * Constructor.
	 */
	private CaxStartBatchProps() {
		super();
		try {
			thisRef = this;
			String xmConfig = BatchEnvProcess.getInstance().getenv("XM_CONFIG");
			if (!BatchUtil.isEmpty(xmConfig)) { //$NON-NLS-1$
				this.load(new FileInputStream(xmConfig.trim()));
			} 
		} catch (Exception ex) {
			LOGGER.error("Execption ocuured in reading properties file", ex); //$NON-NLS-1$
		}
	}
	
	/**
	 * Gets the single instance of CaxStartBatchProps.
	 *
	 * @return single instance of CaxStartBatchProps
	 */
	public static CaxStartBatchProps getInstance() {
		if (thisRef == null) {
			new CaxStartBatchProps();
		}
		return thisRef;
	}
}
