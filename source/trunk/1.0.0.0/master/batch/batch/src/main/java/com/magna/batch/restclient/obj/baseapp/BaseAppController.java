package com.magna.batch.restclient.obj.baseapp;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.magna.batch.restclient.BatchRestController;
import com.magna.batch.restclient.exception.UnauthorizedAccessException;
import com.magna.batch.utils.RestClientConstants;
import com.magna.xmbackend.vo.baseApplication.BaseApplicationCustomResponse;
import com.magna.xmbackend.vo.baseApplication.BaseApplicationRequest;


/**
 * Class for Base app controller.
 *
 * @author Roshan.Ekka
 */
public class BaseAppController extends BatchRestController {

	/** The Constant LOGGER. */
	/*private static final Logger LOGGER = LoggerFactory.getLogger(BaseAppController.class);*/
	
	/**
	 * Constructor for BaseAppController Class.
	 */
	public BaseAppController() {
		super();
	}
	
	/**
	 * Method for Save base app.
	 *
	 * @param userRequest {@link BaseApplicationRequest}
	 * @return the boolean {@link Boolean}
	 */
	public Boolean saveBaseApp(BaseApplicationRequest userRequest) {
		try {
			String url = new String(this.serviceUrl + RestClientConstants.CREATE_BASEAPP);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<BaseApplicationRequest> request = new HttpEntity<BaseApplicationRequest>(userRequest, this.headers);
			ResponseEntity<Boolean> responseEntity = restTemplate.exchange(url, HttpMethod.POST,request, new ParameterizedTypeReference<Boolean>() {});
			HttpStatus statusCode = responseEntity.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				return responseEntity.getBody();
			} else {
				//LOGGER.error("Error while calling XMBATCH Create Base Application REST Service, returns the status code: ", //$NON-NLS-1$
						//statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException)e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				} else if (e instanceof HttpStatusCodeException) {
					String errorResponse = ((HttpStatusCodeException) e).getResponseBodyAsString();
					ObjectMapper mapper = new ObjectMapper();
					JsonNode jsonNode = null;
					try {
						jsonNode = mapper.readTree(errorResponse);
					} catch (IOException e1) {
						System.out.println("ERROR: Server Error! Unable to Create Base Application");
						return false;
					}
					String expMsg = null;
					if(jsonNode.get("lingualMessages") != null) {
						expMsg = jsonNode.get("lingualMessages").get("en").textValue();
					} else if(jsonNode.get("description") != null) {
						expMsg = jsonNode.get("description").asText();
					}
					System.out.println(expMsg);
					return false;
				} 
			} 
		} catch (Exception e) {
			//LOGGER.error("Error while calling XMBATCH Create Base Application REST Service", e); //$NON-NLS-1$
		}
		return null;
	}
	
	/**
	 * Method for Delete base app.
	 *
	 * @param name {@link String}
	 * @return true, if successful
	 */
	public boolean deleteBaseApp(String name) {
		try {
			String url = new String(this.serviceUrl + RestClientConstants.DELETE_BASE_APP);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<String> requestEntity = new HttpEntity<String>(name, this.headers);
			ResponseEntity<Boolean> responseEntity = restTemplate.exchange(url, HttpMethod.POST, requestEntity,
					Boolean.class);
			HttpStatus statusCode = responseEntity.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				return responseEntity.getBody();
			} else {
				//LOGGER.error("Error while calling XMBATCH Delete Base Application REST Service, returns the status code: ", //$NON-NLS-1$
						//statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException)e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				} else if (e instanceof HttpStatusCodeException) {
					String errorResponse = ((HttpStatusCodeException) e).getResponseBodyAsString();
					ObjectMapper mapper = new ObjectMapper();
					JsonNode jsonNode = null;
					try {
						jsonNode = mapper.readTree(errorResponse);
					} catch (IOException e1) {
						System.out.println("ERROR: Server Error! Unable to Delete Base Application");
						return false;
					}
					String expMsg = null;
					if(jsonNode.get("lingualMessages") != null) {
						expMsg = jsonNode.get("lingualMessages").get("en").textValue();
					} else if(jsonNode.get("description") != null) {
						expMsg = jsonNode.get("description").asText();
					}
					System.out.println(expMsg);
					return false;
				} 
			} 
		} catch (Exception e) {
			//LOGGER.error("Error while calling XMBATCH Delete Base Application REST Service!", e); //$NON-NLS-1$
		}
		return false;
	}
	
	/**
	 * Method for Update base app.
	 *
	 * @param adminAreaRequest {@link BaseApplicationRequest}
	 * @return the boolean {@link Boolean}
	 */
	public Boolean updateBaseApp(BaseApplicationRequest adminAreaRequest) {
		try {
			String url = new String(this.serviceUrl + RestClientConstants.UPDATE_BASEAPP);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<BaseApplicationRequest> request = new HttpEntity<BaseApplicationRequest>(adminAreaRequest, this.headers);
			ResponseEntity<Boolean> responseEntity = restTemplate.exchange(url, HttpMethod.POST,request, new ParameterizedTypeReference<Boolean>() {});
			HttpStatus statusCode = responseEntity.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				return responseEntity.getBody();
			} else {
				//LOGGER.error("Error while calling XMBATCH Update Base Application REST Service, returns the status code: ", //$NON-NLS-1$
						//statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException)e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				} else if (e instanceof HttpStatusCodeException) {
					String errorResponse = ((HttpStatusCodeException) e).getResponseBodyAsString();
					ObjectMapper mapper = new ObjectMapper();
					JsonNode jsonNode = null;
					try {
						jsonNode = mapper.readTree(errorResponse);
					} catch (IOException e1) {
						System.out.println("ERROR: Server Error! Unable to Update Base Application");
						return false;
					}
					String expMsg = null;
					if(jsonNode.get("lingualMessages") != null) {
						expMsg = jsonNode.get("lingualMessages").get("en").textValue();
					} else if(jsonNode.get("description") != null) {
						expMsg = jsonNode.get("description").asText();
					}
					System.out.println(expMsg);
					return false;
				} 
			} 
		} catch (Exception e) {
			//LOGGER.error("Error while calling XMBATCH Update Base Application REST Service", e); //$NON-NLS-1$
		}
		return null;
	}
	
	/**
	 * Gets the base app by name.
	 *
	 * @param name {@link String}
	 * @return the base app by name
	 */
	public String getBaseAppByName(String name) {
		try {
			String url = new String(this.serviceUrl + RestClientConstants.GET_CAX_START_BATCH_BASE_APP_BY_NAME +"/"+ name);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<Object> requestEntity = new HttpEntity<Object>(headers);

			List<HttpMessageConverter<?>> messageConverters = new ArrayList<>();
			messageConverters.add(new MappingJackson2HttpMessageConverter());
			restTemplate.setMessageConverters(messageConverters);

			ResponseEntity<BaseApplicationCustomResponse> responseList = restTemplate.exchange(url, HttpMethod.GET, requestEntity,
					BaseApplicationCustomResponse.class);
			HttpStatus statusCode = responseList.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				BaseApplicationCustomResponse baseAppTblObject = responseList.getBody();
				String baseApplicationId = baseAppTblObject.getBaseApplicationId();
				return baseApplicationId;
			} else {
				//LOGGER.error(
						//"Error while calling XMBATCH Get Base Application By Name REST Service, returns the status code: " //$NON-NLS-1$
								//+ statusCode.value());
			}
		} catch (RestClientException e) {
			HttpStatus statusCode = ((HttpClientErrorException)e).getStatusCode();
			if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
				throw new UnauthorizedAccessException(e.getMessage());
			} else if (e instanceof HttpStatusCodeException) {
				String errorResponse = ((HttpStatusCodeException) e).getResponseBodyAsString();
				ObjectMapper mapper = new ObjectMapper();
				JsonNode jsonNode = null;
				try {
					jsonNode = mapper.readTree(errorResponse);
				} catch (IOException e1) {
					System.out.println("ERROR: Server Error! Unable to Get Base Application");
					return null;
				}
				String expMsg = null;
				if(jsonNode.get("lingualMessages") != null) {
					expMsg = jsonNode.get("lingualMessages").get("en").textValue();
				} else if(jsonNode.get("description") != null) {
					expMsg = jsonNode.get("description").asText();
				}
				System.out.println(expMsg);
				return null;
			} 
		} 
			/*if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException)e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			} else if (e instanceof HttpStatusCodeException) {
				//RestClientExpHandlerUtil.handleNoXMLObjectFoundException(LOGGER, (HttpStatusCodeException) e);
			}
		}*/ catch (Exception e) {
			//LOGGER.error("Error while calling XMBATCH Get Base Application By Name REST Service", e); //$NON-NLS-1$
		}
		return null;
	}
}
