package com.magna.batch.restclient.obj.projectapp;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.magna.batch.restclient.BatchRestController;
import com.magna.batch.restclient.exception.UnauthorizedAccessException;
import com.magna.batch.utils.RestClientConstants;
import com.magna.xmbackend.vo.projectApplication.ProjectApplicationCustomResponse;
import com.magna.xmbackend.vo.projectApplication.ProjectApplicationRequest;

/**
 * Class for Project app controller.
 *
 * @author Roshan.Ekka
 */
public class ProjectAppController extends BatchRestController {

	/** The Constant LOGGER. */
	/*private static final Logger LOGGER = LoggerFactory.getLogger(ProjectAppController.class);*/
	
	/**
	 * Constructor for ProjectAppController Class.
	 */
	public ProjectAppController() {
		super();
	}
	
	/**
	 * Method for Save project app.
	 *
	 * @param userRequest {@link ProjectApplicationRequest}
	 * @return the boolean {@link Boolean}
	 */
	public Boolean saveProjectApp(ProjectApplicationRequest userRequest) {
		try {
			String url = new String(this.serviceUrl + RestClientConstants.CREATE_PRPJECTAPP);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<ProjectApplicationRequest> request = new HttpEntity<ProjectApplicationRequest>(userRequest, this.headers);
			ResponseEntity<Boolean> responseEntity = restTemplate.exchange(url, HttpMethod.POST,request, new ParameterizedTypeReference<Boolean>() {});
			HttpStatus statusCode = responseEntity.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				return responseEntity.getBody();
			} else {
				//LOGGER.error("Error while calling XMBATCH Create Project Application REST Service, returns the status code: ", //$NON-NLS-1$
						//statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException)e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				} else if (e instanceof HttpStatusCodeException) {
					String errorResponse = ((HttpStatusCodeException) e).getResponseBodyAsString();
					ObjectMapper mapper = new ObjectMapper();
					JsonNode jsonNode;
					try {
						jsonNode = mapper.readTree(errorResponse);
					} catch (IOException e1) {
						System.out.println("ERROR: Server Error! Unable to Create Project Application");
						return false;
					}
					String expMsg = null;
					if(jsonNode.get("lingualMessages") != null) {
						expMsg = jsonNode.get("lingualMessages").get("en").textValue();
					} else if(jsonNode.get("description") != null) {
						expMsg = jsonNode.get("description").asText();
					}
					System.out.println(expMsg);
					return false;
				} 
			} 
		} catch (Exception e) {
			//LOGGER.error("Error while calling XMBATCH Create Project Application REST Service", e); //$NON-NLS-1$
		}
		return null;
	}
	
	/**
	 * Gets the project app by name.
	 *
	 * @param projectAppName {@link String}
	 * @return the project app by name
	 */
	public ProjectApplicationCustomResponse getProjectAppByName(final String projectAppName) {
		try {
			String url = new String(this.serviceUrl + RestClientConstants.GET_PROJECT_APP_BY_NAME + "/" + projectAppName);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<Object> requestEntity = new HttpEntity<Object>(headers);
			
			List<HttpMessageConverter<?>> messageConverters = new ArrayList<>();
			messageConverters.add(new MappingJackson2HttpMessageConverter());
			restTemplate.setMessageConverters(messageConverters);

			ResponseEntity<ProjectApplicationCustomResponse> responseList = restTemplate.exchange(url, HttpMethod.GET, requestEntity,
					ProjectApplicationCustomResponse.class);
			HttpStatus statusCode = responseList.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				ProjectApplicationCustomResponse startAppTbl = responseList.getBody();
				return startAppTbl;
			} else {
				//LOGGER.error("Error while calling XMBATCH Get Project Application By Name REST Service, returns the status code: ", //$NON-NLS-1$
						// statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException)e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				} else if (e instanceof HttpStatusCodeException) {
					String errorResponse = ((HttpStatusCodeException) e).getResponseBodyAsString();
					ObjectMapper mapper = new ObjectMapper();
					JsonNode jsonNode = null;
					try {
						jsonNode = mapper.readTree(errorResponse);
					} catch (IOException e1) {
						System.out.println("ERROR: Server Error! Unable to Get Project Application By Name");
						return null;
					}
					String expMsg = null;
					if(jsonNode.get("lingualMessages") != null) {
						expMsg = jsonNode.get("lingualMessages").get("en").textValue();
					} else if(jsonNode.get("description") != null) {
						expMsg = jsonNode.get("description").asText();
					}
					System.out.println(expMsg);
					return null;
				} 
			} 
		} catch (Exception e) {
			//LOGGER.error("Error while calling XMBATCH Get Project Application By Name REST Service", e); //$NON-NLS-1$
		}
		return null;
	}
	
	/**
	 * Method for Delete project app.
	 *
	 * @param name {@link String}
	 * @return true, if successful
	 */
	public boolean deleteProjectApp(String name) {
		try {
			String url = new String(this.serviceUrl + RestClientConstants.DELETE_PROJECT_APP);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<String> requestEntity = new HttpEntity<String>(name, this.headers);
			ResponseEntity<Boolean> responseEntity = restTemplate.exchange(url, HttpMethod.POST, requestEntity,
					Boolean.class);
			HttpStatus statusCode = responseEntity.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				return responseEntity.getBody();
			} else {
				//LOGGER.error("Error while calling XMBATCH Delete Project Application REST Service, returns the status code: ", //$NON-NLS-1$
						//statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException)e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				} else if (e instanceof HttpStatusCodeException) {
					String errorResponse = ((HttpStatusCodeException) e).getResponseBodyAsString();
					ObjectMapper mapper = new ObjectMapper();
					JsonNode jsonNode;
					try {
						jsonNode = mapper.readTree(errorResponse);
					} catch (IOException e1) {
						System.out.println("ERROR: Server Error! Unable to Delete Project Application");
						return false;
					}
					String expMsg = null;
					if(jsonNode.get("lingualMessages") != null) {
						expMsg = jsonNode.get("lingualMessages").get("en").textValue();
					} else if(jsonNode.get("description") != null) {
						expMsg = jsonNode.get("description").asText();
					}
					System.out.println(expMsg);
					return false;
				} 
			} 
		} catch (Exception e) {
			//LOGGER.error("Error while calling XMBATCH Delete Project Application REST Service!", e); //$NON-NLS-1$
		}
		return false;
	}
	
	/**
	 * Method for Update project app.
	 *
	 * @param projectAppRequest {@link ProjectApplicationRequest}
	 * @return the boolean {@link Boolean}
	 */
	public Boolean updateProjectApp(ProjectApplicationRequest projectAppRequest) {
		try {
			String url = new String(this.serviceUrl + RestClientConstants.UPDATE_PROJECTAPP);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<ProjectApplicationRequest> request = new HttpEntity<ProjectApplicationRequest>(projectAppRequest, this.headers);
			ResponseEntity<Boolean> responseEntity = restTemplate.exchange(url, HttpMethod.POST,request, new ParameterizedTypeReference<Boolean>() {});
			HttpStatus statusCode = responseEntity.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				return responseEntity.getBody();
			} else {
				//LOGGER.error("Error while calling XMBATCH Update Project Application REST Service, returns the status code: ", //$NON-NLS-1$
						//statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException)e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				} else if (e instanceof HttpStatusCodeException) {
					String errorResponse = ((HttpStatusCodeException) e).getResponseBodyAsString();
					ObjectMapper mapper = new ObjectMapper();
					JsonNode jsonNode = null;
					try {
						jsonNode = mapper.readTree(errorResponse);
					} catch (IOException e1) {
						System.out.println("ERROR: Server Error! Unable to Update Project Application");
						return false;
					}
					String expMsg = null;
					if(jsonNode.get("lingualMessages") != null) {
						expMsg = jsonNode.get("lingualMessages").get("en").textValue();
					} else if(jsonNode.get("description") != null) {
						expMsg = jsonNode.get("description").asText();
					}
					System.out.println(expMsg);
					return false;
				} 
			} 
		} catch (Exception e) {
			//LOGGER.error("Error while calling XMBATCH Update Project Application REST Service", e); //$NON-NLS-1$
		}
		return null;
	}
}
