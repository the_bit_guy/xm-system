/**
 * 
 */
package com.magna.batch.restclient;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import com.magna.batch.restclient.exception.UnauthorizedAccessException;
import com.magna.batch.utils.RestClientConstants;
import com.magna.xmbackend.exception.CannotCreateObjectException;
import com.magna.xmbackend.xmbatch.XmbatchResponse;

/**
 * Class for XM batch controller.
 *
 * @author Roshan.Ekka
 */
public class XMBatchController extends BatchRestController{
	
	/** The Constant LOGGER. */
	private static final Logger LOGGER = LoggerFactory.getLogger(XMBatchController.class);
	
	/**
	 * Constructor for XMBatchController Class.
	 */
	public XMBatchController() {
		super();
	}

	/**
	 * Gets the result set.
	 *
	 * @param query {@link String}
	 * @return the result set
	 */
	public XmbatchResponse getResultSet(final String query) {
		try {
			String url = new String(this.serviceUrl + RestClientConstants.GET_RESULTS_FROM_QUERY);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<String> request = new HttpEntity<String>(query, this.headers);
			ResponseEntity<XmbatchResponse> responseEntity = restTemplate.exchange(url, HttpMethod.POST,request, new ParameterizedTypeReference<XmbatchResponse>() {});
			HttpStatus statusCode = responseEntity.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				XmbatchResponse xmbatchResponse = responseEntity.getBody();
				if(xmbatchResponse.getExceptionMessage() != null) {
					System.out.println(xmbatchResponse.getExceptionMessage());
					return null;
				}
				return xmbatchResponse;
			} else {
				LOGGER.error("Error while calling XMBATCH get resultset REST Service, returns the status code: ", //$NON-NLS-1$
						statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException)e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				} else if (e instanceof HttpClientErrorException) {
					if (((HttpStatusCodeException) e).getStatusCode() == HttpStatus.BAD_REQUEST) {
						System.out.println("Error : Incorrect syntax : generated SQL is not correct ! processed query is : " + query);
					}
				} else if (e instanceof HttpStatusCodeException) {
					String errorResponse = ((HttpStatusCodeException) e).getResponseBodyAsString();
					String errCode = RestClientExpHandlerUtil.handleCannotCreateObjectException(LOGGER, (HttpStatusCodeException) e);
					throw new CannotCreateObjectException(errorResponse, errCode);
				} 
			} 
		} catch (Exception e) {
			LOGGER.error("Error while calling XMBATCH get result REST Service", e); //$NON-NLS-1$
		}
		return null;
	}
}
