package com.magna.batch.restclient.rel.userproject;

import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.magna.batch.restclient.BatchRestController;
import com.magna.batch.restclient.exception.UnauthorizedAccessException;
import com.magna.batch.utils.RestClientConstants;
import com.magna.xmbackend.vo.rel.UserProjectRelRequest;

/**
 * The Class UserProjectRelController.
 */
public class UserProjectRelController extends BatchRestController {
	
	/** The Constant LOGGER. */
	private static final Logger LOGGER = LoggerFactory.getLogger(UserProjectRelController.class);
	
	
	/**
	 * Method for Adds the user to project.
	 *
	 * @param userProjectRelRequest {@link UserProjectRelRequest}
	 * @return the boolean {@link Boolean}
	 */
	public Boolean addUserToProject(final UserProjectRelRequest userProjectRelRequest) {
		try {
			String url = new String(this.serviceUrl + RestClientConstants.ADD_USER_TO_PROJECT);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<UserProjectRelRequest> request = new HttpEntity<UserProjectRelRequest>(userProjectRelRequest, this.headers);
			ResponseEntity<Boolean> responseEntity = restTemplate.postForEntity(url, request, Boolean.class);
			HttpStatus statusCode = responseEntity.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				return responseEntity.getBody();
			} else {
				LOGGER.error("Error while calling XMBATCH Create User-Project Relation REST Service, returns the status code: ", //$NON-NLS-1$
						statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException) e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				} else if (e instanceof HttpStatusCodeException) {
					String errorResponse = ((HttpStatusCodeException) e).getResponseBodyAsString();
					ObjectMapper mapper = new ObjectMapper();
					JsonNode jsonNode = null;
					try {
						jsonNode = mapper.readTree(errorResponse);
					} catch (IOException e1) {
						System.out.println("ERROR: Server Error! Unable to Create User-Project Relation");
						return false;
					}
					String expMsg = null;
					if(jsonNode.get("lingualMessages") != null) {
						expMsg = jsonNode.get("lingualMessages").get("en").textValue();
					} else if(jsonNode.get("description") != null) {
						expMsg = jsonNode.get("description").asText();
					}
					System.out.println(expMsg);
					return false;
				} 
			} 
		} catch (Exception e) {
			LOGGER.error("Error while calling XMBATCH Create User-Project Relation REST Service", e); //$NON-NLS-1$
		}
		return null;
	}
	
	/**
	 * Method for Removes the user from project.
	 *
	 * @param userProjectRelRequest {@link UserProjectRelRequest}
	 * @return the boolean {@link Boolean}
	 */
	public Boolean removeUserFromProject(final UserProjectRelRequest userProjectRelRequest) {
		try {
			String url = new String(this.serviceUrl + RestClientConstants.REMOVE_USER_FROM_PROJECT);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<UserProjectRelRequest> request = new HttpEntity<UserProjectRelRequest>(userProjectRelRequest, this.headers);
			ResponseEntity<Boolean> responseEntity = restTemplate.exchange(url, HttpMethod.DELETE, request, Boolean.class);
			HttpStatus statusCode = responseEntity.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				return responseEntity.getBody();
			} else {
				LOGGER.error("Error while calling XMBATCH Remove User-Project Relation REST Service, returns the status code: ", //$NON-NLS-1$
						statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException) e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				} else if (e instanceof HttpStatusCodeException) {
					String errorResponse = ((HttpStatusCodeException) e).getResponseBodyAsString();
					ObjectMapper mapper = new ObjectMapper();
					JsonNode jsonNode = null;
					try {
						jsonNode = mapper.readTree(errorResponse);
					} catch (IOException e1) {
						System.out.println("ERROR: Server Error! Unable to Remove User-Project Relation");
						return false;
					}
					String expMsg = null;
					if(jsonNode.get("lingualMessages") != null) {
						expMsg = jsonNode.get("lingualMessages").get("en").textValue();
					} else if(jsonNode.get("description") != null) {
						expMsg = jsonNode.get("description").asText();
					}
					System.out.println(expMsg);
					return false;
				} 
			} 
		} catch (Exception e) {
			LOGGER.error("Error while calling XMBATCH Remove User-Project Relation REST Service", e); //$NON-NLS-1$
		}
		return null;
	}

}
