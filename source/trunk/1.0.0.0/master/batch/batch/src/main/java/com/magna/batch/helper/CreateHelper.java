package com.magna.batch.helper;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.cli.CommandLine;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.magna.batch.restclient.exception.UnauthorizedAccessException;
import com.magna.batch.restclient.icons.IconController;
import com.magna.batch.restclient.obj.baseapp.BaseAppController;
import com.magna.batch.restclient.rel.userproject.UserProjectRelController;
import com.magna.batch.utils.BatchUtil;
import com.magna.batch.utils.CommonConstants;
import com.magna.batch.utils.ObjectRelationType;
import com.magna.batch.utils.ObjectType;
import com.magna.xmbackend.exception.CannotCreateObjectException;
import com.magna.xmbackend.vo.adminArea.AdminAreaRequest;
import com.magna.xmbackend.vo.adminArea.AdminAreaTranslation;
import com.magna.xmbackend.vo.baseApplication.BaseApplicationRequest;
import com.magna.xmbackend.vo.baseApplication.BaseApplicationTransulation;
import com.magna.xmbackend.vo.enums.Status;
import com.magna.xmbackend.vo.icon.IconRespWrapper;
import com.magna.xmbackend.vo.icon.IkonRequest;
import com.magna.xmbackend.vo.jpa.site.SiteRequest;
import com.magna.xmbackend.vo.jpa.site.SiteTranslation;
import com.magna.xmbackend.vo.project.ProjectRequest;
import com.magna.xmbackend.vo.project.ProjectTranslation;
import com.magna.xmbackend.vo.projectApplication.ProjectApplicationRequest;
import com.magna.xmbackend.vo.projectApplication.ProjectApplicationTransulation;
import com.magna.xmbackend.vo.rel.UserProjectRelRequest;
import com.magna.xmbackend.vo.startApplication.StartApplicationRequest;
import com.magna.xmbackend.vo.startApplication.StartApplicationTranslation;
import com.magna.xmbackend.vo.user.UserRequest;
import com.magna.xmbackend.vo.user.UserTranslation;
import com.magna.xmbackend.vo.userApplication.UserApplicationRequest;
import com.magna.xmbackend.vo.userApplication.UserApplicationTranslation;
	
/**
 * Class for Create helper.
 *
 * @author Roshan.Ekka
 */
public class CreateHelper {

	/** Member variable 'this ref' for {@link CreateHelper}. */
	private static CreateHelper thisRef;

	/**
	 * Constructor for CreateHelper Class.
	 */
	private CreateHelper() {
		thisRef = this;
	}

	/**
	 * Gets the single instance of CreateHelper.
	 *
	 * @return single instance of CreateHelper
	 */
	public static CreateHelper getInstance() {
		if (thisRef == null) {
			new CreateHelper();
		}
		return thisRef;
	}

	/**
	 * Method for Creates the object.
	 *
	 * @param createObjType
	 *            {@link ObjectType}
	 * @param cmd
	 *            {@link CommandLine}
	 * @throws CannotCreateObjectException
	 *             the cannot create object exception
	 * @throws UnauthorizedAccessException
	 *             the unauthorized access exception
	 * @throws JsonProcessingException
	 *             the json processing exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public void createObject(ObjectType createObjType, final CommandLine cmd) {
		switch (createObjType) {
		case USER:
			createUser(cmd,createObjType );
			break;
		case SITE:
			createSite(cmd, createObjType);
			break;
		case ADMINAREA:
			createAdminArea(cmd, createObjType);
			break;
		case PROJECT:
			createProject(cmd, createObjType);
			break;
		case BASEAPP:
			createBaseApp(cmd, createObjType);
			break;
		case USERAPP:
			createUserApp(cmd, createObjType);
			break;
		case PROJECTAPP:
			createProjectApp(cmd, createObjType);
			break;
		case STARTAPP:
			createStartApp(cmd, createObjType);
		default:
			break;
		}
	}

	/**
	 * Method for Creates the start app.
	 *
	 * @param cmd
	 *            {@link CommandLine}
	 */
	private void createStartApp(CommandLine cmd, ObjectType createObjType) {
		com.magna.batch.restclient.obj.startapp.StartAppController startAppController = new com.magna.batch.restclient.obj.startapp.StartAppController();
		StartApplicationRequest startAppRequest = new StartApplicationRequest();
		String startAppName = null, statusStr = null, iconStr = null, application = null, is_message = "false", start_message_expiry_date = null, desc_en = null, desc_de = null, remarks_en = null,
				remarks_de = null, strVal = "";
		List<StartApplicationTranslation> startAppTranslation = new ArrayList<>();
		StartApplicationTranslation startAppTranslationTbl_en = new StartApplicationTranslation();
		startAppTranslationTbl_en.setLanguageCode(com.magna.batch.utils.LANG_ENUM.ENGLISH.getLangCode());
		startAppTranslation.add(startAppTranslationTbl_en);
		StartApplicationTranslation startAppTranslationTbl_de = new StartApplicationTranslation();
		startAppTranslationTbl_de.setLanguageCode(com.magna.batch.utils.LANG_ENUM.GERMAN.getLangCode());
		startAppTranslation.add(startAppTranslationTbl_de);
		startAppRequest.setStartApplicationTranslations(startAppTranslation);
		String[] optionValues = cmd.getOptionValues("create");
		if (optionValues.length > 0) {
			strVal = BatchUtil.getInstance().getStringValue(optionValues, cmd, createObjType);
			if(strVal==null)return;
			String[] splittedStrVal = strVal.split("\"");
			String value;
			for (int i = 0; i < splittedStrVal.length; i++) {
				value = splittedStrVal[i];
				value = value.trim();
				if ((value.startsWith("NAME") || value.startsWith("name")) && !value.endsWith("=")) {
					if(value.contains("\"")){
						if (value.contains("=") && value.split("=")[0].trim().equalsIgnoreCase("NAME")) {
							startAppName = value.split("=\"", 2)[1];
							if(!startAppName.matches(CommonConstants.RegularExpressions.ALLOWED_APP_NAME_REGEX)){
								System.out.println("Please provide a valid name !" +CommonConstants.Messages.APP_NAME_MESSAGE);
								return;
							} else if(!(startAppName.length()<30)){
								System.out.println("Please provide a valid name !" +CommonConstants.Messages.NAME_LIMIT_MESSAGE);
								return;
							}
						} else {
							System.out.println("ERROR: Syntax error !");
							return;
						}
					}else{
						if (value.contains("=") && value.split("=")[0].trim().equalsIgnoreCase("NAME")) {
							startAppName = value.split("=", 2)[1];
							if(!startAppName.matches(CommonConstants.RegularExpressions.ALLOWED_APP_NAME_REGEX)){
								System.out.println("Please provide a valid name !" +CommonConstants.Messages.APP_NAME_MESSAGE);
								return;
							} else if(!(startAppName.length()<30)){
								System.out.println("Please provide a valid name !" +CommonConstants.Messages.NAME_LIMIT_MESSAGE);
								return;
							}
						} else {
							System.out.println("ERROR: Syntax error !");
							return;
						}
					}
					continue;
				}
				if ((value.startsWith("STATUS") || value.startsWith("status")) && !value.endsWith("=")) {
					if(value.contains("=") && value.split("=")[0].trim().equalsIgnoreCase("STATUS")){
						statusStr = value.split("=", 2)[1];
						statusStr = statusStr.toUpperCase();
						if(!(statusStr.equalsIgnoreCase("active") || statusStr.equalsIgnoreCase("inactive"))){
							System.out.println("ERROR: Syntax error ! Status should be ACTIVE or INACTIVE");
							return;
						}
					}else{
						System.out.println("ERROR: Syntax error !");
						return;
					}
					continue;
				} else if ((value.startsWith("STATUS") || value.startsWith("status")) && value.endsWith("=")){
						System.out.println("ERROR: Syntax error ! Status should be ACTIVE or INACTIVE");
						return;
				}
				if ((value.startsWith("ICON_NAME") || value.startsWith("icon_name")) && !value.endsWith("=")) {
					if(value.contains("=") && value.split("=")[0].trim().equalsIgnoreCase("ICON_NAME")){
						iconStr = value.split("=", 2)[1];
					}else{
						System.out.println("ERROR: Syntax error !");
						return;
					}
					continue;
				}
				
				if ((value.startsWith("APPLICATION") || value.startsWith("application")) && !value.endsWith("=")) {
					if(value.contains("\"")){
						if (value.contains("=") && value.split("=")[0].trim().equalsIgnoreCase("APPLICATION")) {
							application = value.split("=\"", 2)[1];
						} else {
							System.out.println("ERROR: Syntax error !");
							return;
						}
					}else{
						if (value.contains("=") && value.split("=")[0].trim().equalsIgnoreCase("APPLICATION")) {
							application = value.split("=", 2)[1];
						} else {
							System.out.println("ERROR: Syntax error !");
							return;
						}
					}
					continue;
				}
				if ((value.startsWith("IS_MESSAGE") || value.startsWith("is_message"))&& !value.endsWith("=")) {
					if(value.contains("\"")){
						if (value.contains("=") && value.split("=")[0].trim().equalsIgnoreCase("IS_MESSAGE")) {
							is_message = value.split("=\"", 2)[1];
						} else {
							System.out.println("ERROR: Syntax error !");
							return;
						}
					}else{
						if (value.contains("=") && value.split("=")[0].trim().equalsIgnoreCase("IS_MESSAGE")) {
							is_message = value.split("=", 2)[1];
						} else {
							System.out.println("ERROR: Syntax error !");
							return;
						}
					}
					continue;
				}
				if ((value.startsWith("START_MESSAGE_EXPIRY_DATE") || value.startsWith("start_message_expiry_date"))&& !value.endsWith("=")) {
					if (value.contains("=") && value.split("=")[0].trim().equalsIgnoreCase("START_MESSAGE_EXPIRY_DATE")) {
						start_message_expiry_date = value.split("=", 2)[1];
						start_message_expiry_date = start_message_expiry_date.trim();
					} else {
						System.out.println("ERROR: Syntax error !");
						return;
					}
					continue;
				}
				if ((value.startsWith("DESCRIPTION_EN") || value.startsWith("description_en")) && !value.endsWith("=")) {
					if (value.contains("\"")) {
						if(value.contains("=") && value.split("=")[0].trim().equalsIgnoreCase("DESCRIPTION_EN")){
							desc_en = value.split("=\"", 2)[1];
							if(!(desc_en.length()<240)){
								System.out.println("Please provide a valid description !" +CommonConstants.Messages.DESC_LIMIT_MESSAGE);
								return;
							}
						}else{
							System.out.println("ERROR: Syntax error !");
							return;
						}
					} else {
						if(value.contains("=") && value.split("=")[0].trim().equalsIgnoreCase("DESCRIPTION_EN")){
							desc_en = value.split("=", 2)[1];
							if(!(desc_en.length()<240)){
								System.out.println("Please provide a valid description !" +CommonConstants.Messages.DESC_LIMIT_MESSAGE);
								return;
							}
						}else{
							System.out.println("ERROR: Syntax error !");
							return;
						}
					}
					continue;
				}
				if ((value.startsWith("DESCRIPTION_DE") || value.startsWith("description_de")) && !value.endsWith("=")) {
					if (value.contains("\"")) {
						if(value.contains("=") && value.split("=")[0].trim().equalsIgnoreCase("DESCRIPTION_DE")){
							desc_de = value.split("=\"", 2)[1];
							if(!(desc_de.length()<240)){
								System.out.println("Please provide a valid description !" +CommonConstants.Messages.DESC_LIMIT_MESSAGE);
								return;
							}
						}else{
							System.out.println("ERROR: Syntax error !");
							return;
						}
					} else {
						if(value.contains("=") && value.split("=")[0].trim().equalsIgnoreCase("DESCRIPTION_DE")){
							desc_de = value.split("=", 2)[1];
							if(!(desc_de.length()<240)){
								System.out.println("Please provide a valid description !" +CommonConstants.Messages.DESC_LIMIT_MESSAGE);
								return;
							}
						}else{
							System.out.println("ERROR: Syntax error !");
							return;
						}
					}
					continue;
				} 
				if ((value.startsWith("REMARKS_EN") || value.startsWith("remarks_en")) && !value.endsWith("=")) {
					if (value.contains("\"")) {
						if(value.contains("=") && value.split("=")[0].trim().equalsIgnoreCase("REMARKS_EN")){
							remarks_en = value.split("=\"", 2)[1];
							if(!(remarks_en.length()<1500)){
								System.out.println("Please provide a valid remarks !" +CommonConstants.Messages.REMARKS_LIMIT_MESSAGE);
								return;
							}
						}else{
							System.out.println("ERROR: Syntax error !");
							return;
						}
					} else {
						if(value.contains("=") && value.split("=")[0].trim().equalsIgnoreCase("REMARKS_EN")){
							remarks_en = value.split("=", 2)[1];
							if(!(remarks_en.length()<1500)){
								System.out.println("Please provide a valid remarks !" +CommonConstants.Messages.REMARKS_LIMIT_MESSAGE);
								return;
							}
						}else{
							System.out.println("ERROR: Syntax error !");
							return;
						}
					}
					continue;
				}
				if ((value.startsWith("REMARKS_DE") || value.startsWith("remarks_de")) && !value.endsWith("=")) {
					if (value.contains("\"")) {
						if(value.contains("=") && value.split("=")[0].trim().equalsIgnoreCase("REMARKS_DE")){
							remarks_de = value.split("=\"", 2)[1];
							if(!(remarks_de.length()<1500)){
								System.out.println("Please provide a valid remarks !" +CommonConstants.Messages.REMARKS_LIMIT_MESSAGE);
								return;
							}
						}else{
							System.out.println("ERROR: Syntax error !");
							return;
						}
					} else {
						if(value.contains("=") && value.split("=")[0].trim().equalsIgnoreCase("REMARKS_DE")){
							remarks_de = value.split("=", 2)[1];
							if(!(remarks_de.length()<1500)){
								System.out.println("Please provide a valid remarks !" +CommonConstants.Messages.REMARKS_LIMIT_MESSAGE);
								return;
							}
						}else{
							System.out.println("ERROR: Syntax error !");
							return;
						}
					}
					continue;
				}
			}
			
			if(BatchUtil.isEmpty(startAppName)){
				System.out.println("start application name is required");
				return;
			} else if(BatchUtil.isEmpty(statusStr)){
				System.out.println("status is required");
				return;
			} else if(BatchUtil.isEmpty(iconStr)){
				System.out.println("icon is required");
				return;
			} else if(is_message!=null && is_message.equals("false")){
				if(BatchUtil.isEmpty(application)){
					System.out.println("application is required");
					return;
				} else if(!BatchUtil.isEmpty(start_message_expiry_date)){
					System.out.println("You could not assign start_message_expiry_date, If Application is Not a Start Message");
					return;
				}
			} else if(is_message!=null && is_message.equals("true")){
				if(BatchUtil.isEmpty(start_message_expiry_date)){
					System.out.println("start_message_expiry_date is required");
					return;
				} else if(!BatchUtil.isEmpty(application)){
					System.out.println("You could not assign Base Application, If Application is Start Message");
					return;
				}
			}
			
			IconRespWrapper iconRespWrapper = null;
			if(iconStr!=null){
				IconController iconController = new IconController();
				IkonRequest ikonRequest = new IkonRequest();
				ikonRequest.setIconName(iconStr);
				iconRespWrapper = iconController.getIconByName(ikonRequest);
				if(iconRespWrapper==null) return;
			}
			if (!BatchUtil.isEmpty(startAppName)) {
				startAppRequest.setName(startAppName);
			}
			if (!BatchUtil.isEmpty(iconStr) && iconRespWrapper!=null) {
				startAppRequest.setIconId(iconRespWrapper.getIconId());
			}
			if (!BatchUtil.isEmpty(statusStr)) {
				startAppRequest.setStatus(statusStr);
			}
			String baseAppId = null;
			if (!BatchUtil.isEmpty(application)) {
				BaseAppController baseAppController = new BaseAppController();
				baseAppId = baseAppController.getBaseAppByName(application);
				if(baseAppId==null) return;
				startAppRequest.setBaseAppId(baseAppId);
			}
			if (!BatchUtil.isEmpty(is_message)) {
				if(is_message.equalsIgnoreCase("true") || is_message.equalsIgnoreCase("false")){
					boolean parseBoolean = Boolean.parseBoolean(is_message);
					startAppRequest.setIsMessage(parseBoolean);
				} else {
					System.out.println("ERROR: Syntax error ! is_message should be TRUE or FALSE. Eg: is_message=true");
					return;
				}
			}
			if (!BatchUtil.isEmpty(start_message_expiry_date)) {
				try {
					Date todaysDate = new Date();
					Date endDate = null;
					if(start_message_expiry_date.matches("[0-9][0-9]/[0-9][0-9]/[0-9][0-9][0-9][0-9]")){
						if(!BatchUtil.getInstance().isValidDate(start_message_expiry_date)){
							System.out.println("Please provide a valid date. Eg. start_message_expiry_date = DD-MM-YYYY or DD/MM/YYYY");
							return;
						}else {
							endDate=new SimpleDateFormat("dd/MM/yyyy").parse(start_message_expiry_date);
						}
					} else if(start_message_expiry_date.matches("[0-9][0-9]-[0-9][0-9]-[0-9][0-9][0-9][0-9]")){
						if(!BatchUtil.getInstance().isValidDate(start_message_expiry_date)){
							System.out.println("Please provide a valid date. Eg. start_message_expiry_date = DD-MM-YYYY or DD/MM/YYYY");
							return;
						} else {
							endDate=new SimpleDateFormat("dd-MM-yyyy").parse(start_message_expiry_date);
						}
					}else{
						System.out.println("ERROR: Syntax error ! Eg. start_message_expiry_date = DD-MM-YYYY or DD/MM/YYYY");
						return;
					}
					
					SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.YYYY HH:mm:ss.SSS");
					if (!(endDate.after(todaysDate) || sdf.format(endDate).equals(sdf.format(todaysDate)))) {
						System.out.println("Expiry date must be after your current date time");
						return;
					}
					startAppRequest.setStartMsgExpiryDate(endDate);
				} catch (ParseException e) {
					System.out.println("Unable to Parse start_message_expiry_date. Eg. start_message_expiry_date = DD-MM-YYYY or DD/MM/YYYY");
					return;
				}
			}
			if (!BatchUtil.isEmpty(desc_en)) {
				startAppTranslationTbl_en.setDescription(desc_en);
			}
			if (!BatchUtil.isEmpty(remarks_en)) {
				startAppTranslationTbl_en.setRemarks(remarks_en);
			}
			if (!BatchUtil.isEmpty(desc_de)) {
				startAppTranslationTbl_de.setDescription(desc_de);
			}
			if (!BatchUtil.isEmpty(remarks_de)) {
				startAppTranslationTbl_de.setRemarks(remarks_de);
			}
			List<StartApplicationTranslation> startAppTranslationTblList = new ArrayList<StartApplicationTranslation>();
			startAppTranslationTblList.add(startAppTranslationTbl_en);
			startAppTranslationTblList.add(startAppTranslationTbl_de);
			startAppRequest.setStartApplicationTranslations(startAppTranslationTblList);

				Boolean saveStartApp = startAppController.saveStartApp(startAppRequest);
				if (saveStartApp == null || saveStartApp == Boolean.FALSE) {
					return;
				} else {
					System.out.println("Start Application created Successfully");
				}
		}
	}

	/**
	 * Method for Creates the project app.
	 *
	 * @param cmd
	 *            {@link CommandLine}
	 */
	private void createProjectApp(CommandLine cmd, ObjectType createObjType) {
		com.magna.batch.restclient.obj.projectapp.ProjectAppController projectAppController = new com.magna.batch.restclient.obj.projectapp.ProjectAppController();
		ProjectApplicationRequest projectAppRequest = new ProjectApplicationRequest();
		String projectAppName = null, statusStr = null, iconStr = null, desc = null, is_parent = "false", is_singleton = "false",
				position = "*ButtonTask*", application = null, name_en = null, name_de = null, desc_en = null, desc_de = null, remarks_en = null, remarks_de = null,
				strVal = "";
		List<ProjectApplicationTransulation> projectAppTranslation = new ArrayList<>();
		ProjectApplicationTransulation projectAppTranslationTbl_en = new ProjectApplicationTransulation();
		projectAppTranslationTbl_en.setLanguageCode(com.magna.batch.utils.LANG_ENUM.ENGLISH.getLangCode());
		projectAppTranslation.add(projectAppTranslationTbl_en);
		ProjectApplicationTransulation projectAppTranslationTbl_de = new ProjectApplicationTransulation();
		projectAppTranslationTbl_de.setLanguageCode(com.magna.batch.utils.LANG_ENUM.GERMAN.getLangCode());
		projectAppTranslation.add(projectAppTranslationTbl_de);
		projectAppRequest.setProjectApplicationTransulations(projectAppTranslation);
		String[] optionValues = cmd.getOptionValues("create");
		if (optionValues.length > 0) {
			strVal = BatchUtil.getInstance().getStringValue(optionValues, cmd, createObjType);
			if(strVal==null)return;
			String[] splittedStrVal = strVal.split("\"");
			String value;
			for (int i = 0; i < splittedStrVal.length; i++) {
				value = splittedStrVal[i];
				value = value.trim();
				if ((value.startsWith("NAME_EN") || value.startsWith("name_en")) && !value.endsWith("=")) {
					if(value.contains("\"")){
						if (value.contains("=") && value.split("=")[0].trim().equalsIgnoreCase("NAME_EN")) {
							name_en = value.split("=\"", 2)[1];
							if(!name_en.matches(CommonConstants.RegularExpressions.ALLOWED_APP_NAME_REGEX)){
								System.out.println("Please provide a valid name !" +CommonConstants.Messages.APP_NAME_MESSAGE);
								return;
							} else if(!(name_en.length()<30)){
								System.out.println("Please provide a valid name !" +CommonConstants.Messages.NAME_LIMIT_MESSAGE);
								return;
							}
						} else {
							System.out.println("ERROR: Syntax error !");
							return;
						}
					}else{
						if (value.contains("=") && value.split("=")[0].trim().equalsIgnoreCase("NAME_EN")) {
							name_en = value.split("=", 2)[1];
							if(!name_en.matches(CommonConstants.RegularExpressions.ALLOWED_APP_NAME_REGEX)){
								System.out.println("Please provide a valid name !" +CommonConstants.Messages.APP_NAME_MESSAGE);
								return;
							} else if(!(name_en.length()<30)){
								System.out.println("Please provide a valid name !" +CommonConstants.Messages.NAME_LIMIT_MESSAGE);
								return;
							}
						} else {
							System.out.println("ERROR: Syntax error !");
							return;
						}
					}
					continue;
				} else if ((value.startsWith("NAME_DE") || value.startsWith("name_de")) && !value.endsWith("=")) {
					if(value.contains("\"")){
						if (value.contains("=") && value.split("=")[0].trim().equalsIgnoreCase("NAME_DE")) {
							name_de = value.split("=\"", 2)[1];
							if(!name_de.matches(CommonConstants.RegularExpressions.ALLOWED_APP_NAME_REGEX)){
								System.out.println("Please provide a valid name !" +CommonConstants.Messages.APP_NAME_MESSAGE);
								return;
							} else if(!(name_de.length()<30)){
								System.out.println("Please provide a valid name !" +CommonConstants.Messages.NAME_LIMIT_MESSAGE);
								return;
							}
						} else {
							System.out.println("ERROR: Syntax error !");
							return;
						}
					}else{
						if (value.contains("=") && value.split("=")[0].trim().equalsIgnoreCase("NAME_DE")) {
							name_de = value.split("=", 2)[1];
							if(!name_de.matches(CommonConstants.RegularExpressions.ALLOWED_APP_NAME_REGEX)){
								System.out.println("Please provide a valid name !" +CommonConstants.Messages.APP_NAME_MESSAGE);
								return;
							} else if(!(name_de.length()<30)){
								System.out.println("Please provide a valid name !" +CommonConstants.Messages.NAME_LIMIT_MESSAGE);
								return;
							}
						} else {
							System.out.println("ERROR: Syntax error !");
							return;
						}
					}
					continue;
				} else if ((value.startsWith("NAME") || value.startsWith("name")) && !value.endsWith("=")) {
					if(value.contains("\"")){
						if (value.contains("=") && value.split("=")[0].trim().equalsIgnoreCase("NAME")) {
							projectAppName = value.split("=\"", 2)[1];
							if(!projectAppName.matches(CommonConstants.RegularExpressions.ALLOWED_APP_NAME_REGEX)){
								System.out.println("Please provide a valid name !" +CommonConstants.Messages.APP_NAME_MESSAGE);
								return;
							} else if(!(projectAppName.length()<30)){
								System.out.println("Please provide a valid name !" +CommonConstants.Messages.NAME_LIMIT_MESSAGE);
								return;
							}
						} else {
							System.out.println("ERROR: Syntax error !");
							return;
						}
					}else{
						if (value.contains("=") && value.split("=")[0].trim().equalsIgnoreCase("NAME")) {
							projectAppName = value.split("=", 2)[1];
							if(!projectAppName.matches(CommonConstants.RegularExpressions.ALLOWED_APP_NAME_REGEX)){
								System.out.println("Please provide a valid name !" +CommonConstants.Messages.APP_NAME_MESSAGE);
								return;
							} else if(!(projectAppName.length()<30)){
								System.out.println("Please provide a valid name !" +CommonConstants.Messages.NAME_LIMIT_MESSAGE);
								return;
							}
						} else {
							System.out.println("ERROR: Syntax error !");
							return;
						}
					}
					continue;
				}
				if ((value.startsWith("STATUS") || value.startsWith("status")) && !value.endsWith("=")) {
					if(value.contains("=") && value.split("=")[0].trim().equalsIgnoreCase("STATUS")){
						statusStr = value.split("=", 2)[1];
						statusStr = statusStr.toUpperCase();
						if(!(statusStr.equalsIgnoreCase("active") || statusStr.equalsIgnoreCase("inactive"))){
							System.out.println("ERROR: Syntax error ! Status should be ACTIVE or INACTIVE");
							return;
						}
					}else{
						System.out.println("ERROR: Syntax error !");
						return;
					}
					continue;
				} else if ((value.startsWith("STATUS") || value.startsWith("status")) && value.endsWith("=")){
						System.out.println("ERROR: Syntax error ! Status should be ACTIVE or INACTIVE");
						return;
				}
				if ((value.startsWith("ICON_NAME") || value.startsWith("icon_name")) && !value.endsWith("=")) {
					if(value.contains("=") && value.split("=")[0].trim().equalsIgnoreCase("ICON_NAME")){
						iconStr = value.split("=", 2)[1];
					}else{
						System.out.println("ERROR: Syntax error !");
						return;
					}
					continue;
				}
				if ((value.startsWith("IS_PARENT") || value.startsWith("is_parent")) && !value.endsWith("=")) {
					if (value.contains("=") && value.split("=")[0].trim().equalsIgnoreCase("IS_PARENT")) {
						is_parent = value.split("=", 2)[1];
					} else {
						System.out.println("ERROR: Syntax error !");
						return;
					}
					continue;
				}
				if ((value.startsWith("IS_SINGLETON") || value.startsWith("is_singleton")) && !value.endsWith("=")) {
					if(value.contains("\"")){
						if (value.contains("=") && value.split("=")[0].trim().equalsIgnoreCase("IS_SINGLETON")) {
							is_singleton = value.split("=\"", 2)[1];
						} else {
							System.out.println("ERROR: Syntax error !");
							return;
						}
					}else{
						if (value.contains("=") && value.split("=")[0].trim().equalsIgnoreCase("IS_SINGLETON")) {
							is_singleton = value.split("=", 2)[1];
						} else {
							System.out.println("ERROR: Syntax error !");
							return;
						}
					}
					continue;
				}
				if ((value.startsWith("POSITION") || value.startsWith("position")) && !value.endsWith("=")) {
					if (value.contains("=") && value.split("=")[0].trim().equalsIgnoreCase("POSITION")) {
						position = value.split("=", 2)[1];
						position.toUpperCase();
						if(!(position.equals("*ButtonTask*") || position.equals("*IconTask*") || 
								position.equals("*MenuTask*"))){
							System.out.println("\""+position+"\""+" is not a valid position value! valid position values are  : *ButtonTask*"
									+ ", *IconTask*, *MenuTask* ");
							return;
						}
					} else {
						System.out.println("ERROR: Syntax error !");
						return;
					}
					continue;
				}
				
				if ((value.startsWith("APPLICATION") || value.startsWith("application"))&& !value.endsWith("=")) {
					if (value.contains("=") && value.split("=")[0].trim().equalsIgnoreCase("APPLICATION")) {
						application = value.split("=", 2)[1];
					} else {
						System.out.println("ERROR: Syntax error !");
						return;
					}
					continue;
				}
				
				
				if ((value.startsWith("DESCRIPTION_EN") || value.startsWith("description_en")) && !value.endsWith("=")) {
					if (value.contains("\"")) {
						if(value.contains("=") && value.split("=")[0].trim().equalsIgnoreCase("DESCRIPTION_EN")){
							desc_en = value.split("=\"", 2)[1];
							if(!(desc_en.length()<240)){
								System.out.println("Please provide a valid description !" +CommonConstants.Messages.DESC_LIMIT_MESSAGE);
								return;
							}
						}else{
							System.out.println("ERROR: Syntax error !");
							return;
						}
					} else {
						if(value.contains("=") && value.split("=")[0].trim().equalsIgnoreCase("DESCRIPTION_EN")){
							desc_en = value.split("=", 2)[1];
							if(!(desc_en.length()<240)){
								System.out.println("Please provide a valid description !" +CommonConstants.Messages.DESC_LIMIT_MESSAGE);
								return;
							}
						}else{
							System.out.println("ERROR: Syntax error !");
							return;
						}
					}
					continue;
				}
				if ((value.startsWith("DESCRIPTION_DE") || value.startsWith("description_de")) && !value.endsWith("=")) {
					if (value.contains("\"")) {
						if(value.contains("=") && value.split("=")[0].trim().equalsIgnoreCase("DESCRIPTION_DE")){
							desc_de = value.split("=\"", 2)[1];
							if(!(desc_de.length()<240)){
								System.out.println("Please provide a valid description !" +CommonConstants.Messages.DESC_LIMIT_MESSAGE);
								return;
							}
						}else{
							System.out.println("ERROR: Syntax error !");
							return;
						}
					} else {
						if(value.contains("=") && value.split("=")[0].trim().equalsIgnoreCase("DESCRIPTION_DE")){
							desc_de = value.split("=", 2)[1];
							if(!(desc_de.length()<240)){
								System.out.println("Please provide a valid description !" +CommonConstants.Messages.DESC_LIMIT_MESSAGE);
								return;
							}
						}else{
							System.out.println("ERROR: Syntax error !");
							return;
						}
					}
					continue;
				} else if ((value.startsWith("DESCRIPTION") || value.startsWith("description")) && !value.endsWith("=")) {
					if(value.contains("\"")){
						if (value.contains("=") && value.split("=")[0].trim().equalsIgnoreCase("DESCRIPTION")) {
							desc = value.split("=\"", 2)[1];
							if(!(desc.length()<240)){
								System.out.println("Please provide a valid description !" +CommonConstants.Messages.DESC_LIMIT_MESSAGE);
								return;
							}
						} else {
							System.out.println("ERROR: Syntax error !");
							return;
						}
					}else{
						if (value.contains("=") && value.split("=")[0].trim().equalsIgnoreCase("DESCRIPTION")) {
							desc = value.split("=", 2)[1];
							if(!(desc.length()<240)){
								System.out.println("Please provide a valid description !" +CommonConstants.Messages.DESC_LIMIT_MESSAGE);
								return;
							}
						} else {
							System.out.println("ERROR: Syntax error !");
							return;
						}
					}
					continue;
				}
				if ((value.startsWith("REMARKS_EN") || value.startsWith("remarks_en")) && !value.endsWith("=")) {
					if (value.contains("\"")) {
						if(value.contains("=") && value.split("=")[0].trim().equalsIgnoreCase("REMARKS_EN")){
							remarks_en = value.split("=\"", 2)[1];
							if(!(remarks_en.length()<1500)){
								System.out.println("Please provide a valid remarks !" +CommonConstants.Messages.REMARKS_LIMIT_MESSAGE);
								return;
							}
						}else{
							System.out.println("ERROR: Syntax error !");
							return;
						}
					} else {
						if(value.contains("=") && value.split("=")[0].trim().equalsIgnoreCase("REMARKS_EN")){
							remarks_en = value.split("=", 2)[1];
							if(!(remarks_en.length()<1500)){
								System.out.println("Please provide a valid remarks !" +CommonConstants.Messages.REMARKS_LIMIT_MESSAGE);
								return;
							}
						}else{
							System.out.println("ERROR: Syntax error !");
							return;
						}
					}
					continue;
				}
				if ((value.startsWith("REMARKS_DE") || value.startsWith("remarks_de")) && !value.endsWith("=")) {
					if (value.contains("\"")) {
						if(value.contains("=") && value.split("=")[0].trim().equalsIgnoreCase("REMARKS_DE")){
							remarks_de = value.split("=\"", 2)[1];
							if(!(remarks_de.length()<1500)){
								System.out.println("Please provide a valid remarks !" +CommonConstants.Messages.REMARKS_LIMIT_MESSAGE);
								return;
							}
						}else{
							System.out.println("ERROR: Syntax error !");
							return;
						}
					} else {
						if(value.contains("=") && value.split("=")[0].trim().equalsIgnoreCase("REMARKS_DE")){
							remarks_de = value.split("=", 2)[1];
							if(!(remarks_de.length()<1500)){
								System.out.println("Please provide a valid remarks !" +CommonConstants.Messages.REMARKS_LIMIT_MESSAGE);
								return;
							}
						}else{
							System.out.println("ERROR: Syntax error !");
							return;
						}
					}
					continue;
				}
			}
			
			if(BatchUtil.isEmpty(projectAppName)){
				System.out.println("project application name is required");
				return;
			} else if(BatchUtil.isEmpty(statusStr)){
				System.out.println("status is required");
				return;
			} else if(BatchUtil.isEmpty(iconStr)){
				System.out.println("icon is required");
				return;
			} else if(BatchUtil.isEmpty(position)){
				System.out.println("position is required");
				return;
			} else if(is_parent!=null && is_parent.equalsIgnoreCase("false") && BatchUtil.isEmpty(application)){
				System.out.println("application is required");
				return;
			} else if(is_parent!=null && is_parent.equalsIgnoreCase("true")){
				if(!BatchUtil.isEmpty(application)){
					System.out.println("You could not assign Base Application, if Project Application is parent");
					return;
				} else if(is_singleton!=null && is_singleton.equalsIgnoreCase("true")){
					System.out.println("Application can not be Singleton, if Project Application is parent");
					return;
				} else if(position!=null){
					if(!(position.equals("*ButtonTask*") || position.equals("*MenuTask*"))){
						System.out.print("\""+position+"\""+" is not a valid position value! ");
						System.out.println("If Project Application is parent, Valid positions are : *ButtonTask* and *MenuTask*. Eg: position='*MenuTask*'");
						return;
					}
				}
			}
			
			IconRespWrapper iconRespWrapper = null;
			if(iconStr!=null){
				IconController iconController = new IconController();
				IkonRequest ikonRequest = new IkonRequest();
				ikonRequest.setIconName(iconStr);
				iconRespWrapper = iconController.getIconByName(ikonRequest);
				if(iconRespWrapper==null) return;
			}
			if (!BatchUtil.isEmpty(projectAppName)) {
				projectAppRequest.setName(projectAppName);
			}
			if (!BatchUtil.isEmpty(iconStr)) {
				projectAppRequest.setIconId(iconRespWrapper.getIconId());
			}
			if (!BatchUtil.isEmpty(statusStr)) {
				projectAppRequest.setStatus(statusStr);
			}
			String baseAppId = null;
			if (!BatchUtil.isEmpty(application)) {
				BaseAppController baseAppController = new BaseAppController();
				baseAppId = baseAppController.getBaseAppByName(application);
				if(baseAppId==null)return;
				projectAppRequest.setBaseAppId(baseAppId);
			}
			if (!BatchUtil.isEmpty(desc)) {
				projectAppRequest.setDescription(desc);
			}
			if (!BatchUtil.isEmpty(is_parent)) {
				if(is_parent.equalsIgnoreCase("true") || is_parent.equalsIgnoreCase("false")){
					projectAppRequest.setIsParent(is_parent);
				} else{
					System.out.println("ERROR: Syntax error ! parent should be TRUE or FALSE. Eg: is_parent=true");
					return;
				}
			}
			if (!BatchUtil.isEmpty(iconStr) && iconRespWrapper!=null) {
				projectAppRequest.setIconId(iconRespWrapper.getIconId());
			}
			if (!BatchUtil.isEmpty(is_singleton)) {
				if(is_singleton.equalsIgnoreCase("true") || is_singleton.equalsIgnoreCase("false")){
					projectAppRequest.setIsSingleton(is_singleton);
				} else{
					System.out.println("ERROR: Syntax error ! singleton should be TRUE or FALSE. Eg: is_singleton=true");
					return;
				}
			}
			if (!BatchUtil.isEmpty(position)) {
				projectAppRequest.setPosition(position);
			}
			if (!BatchUtil.isEmpty(name_en)) {
				projectAppTranslationTbl_en.setName(desc_en);
			}
			if (!BatchUtil.isEmpty(name_de)) {
				projectAppTranslationTbl_en.setName(name_de);
			}
			if (!BatchUtil.isEmpty(desc_en)) {
				projectAppTranslationTbl_en.setDescription(desc_en);
			}
			if (!BatchUtil.isEmpty(remarks_en)) {
				projectAppTranslationTbl_en.setRemarks(remarks_en);
			}
			if (!BatchUtil.isEmpty(desc_de)) {
				projectAppTranslationTbl_de.setDescription(desc_de);
			}
			if (!BatchUtil.isEmpty(remarks_de)) {
				projectAppTranslationTbl_de.setRemarks(remarks_de);
			}
			List<ProjectApplicationTransulation> baseAppTranslationTblList = new ArrayList<ProjectApplicationTransulation>();
			baseAppTranslationTblList.add(projectAppTranslationTbl_en);
			baseAppTranslationTblList.add(projectAppTranslationTbl_de);
			projectAppRequest.setProjectApplicationTransulations(baseAppTranslationTblList);

				Boolean saveProjectApp = projectAppController.saveProjectApp(projectAppRequest);
				if (saveProjectApp == null || saveProjectApp == Boolean.FALSE) {
					return;
				} else {
					System.out.println("Project Application created Successfully");

				}
		}

	}

	/**
	 * Method for Creates the user app.
	 *
	 * @param cmd
	 *            {@link CommandLine}
	 */
	private void createUserApp(CommandLine cmd, ObjectType createObjType) {
		com.magna.batch.restclient.obj.userapp.UserAppController userAppController = new com.magna.batch.restclient.obj.userapp.UserAppController();
		UserApplicationRequest userAppRequest = new UserApplicationRequest();
		String userAppName = null, statusStr = null, iconStr = null, application = null, desc = null, is_parent = "false", is_singleton = "false",
				position = "*ButtonTask*", name_en = null, name_de = null, desc_en = null, desc_de = null, remarks_en = null, remarks_de = null, strVal = "";
		List<UserApplicationTranslation> userAppTranslation = new ArrayList<>();
		UserApplicationTranslation userAppTranslationTbl_en = new UserApplicationTranslation();
		userAppTranslationTbl_en.setLanguageCode(com.magna.batch.utils.LANG_ENUM.ENGLISH.getLangCode());
		userAppTranslation.add(userAppTranslationTbl_en);
		UserApplicationTranslation userAppTranslationTbl_de = new UserApplicationTranslation();
		userAppTranslationTbl_de.setLanguageCode(com.magna.batch.utils.LANG_ENUM.GERMAN.getLangCode());
		userAppTranslation.add(userAppTranslationTbl_de);
		userAppRequest.setUserApplicationTranslations(userAppTranslation);
		String[] optionValues = cmd.getOptionValues("create");
		if (optionValues.length > 0) {
			strVal = BatchUtil.getInstance().getStringValue(optionValues, cmd, createObjType);
			if(strVal==null)return;
			String[] splittedStrVal = strVal.split("\"");
			String value;
			for (int i = 0; i < splittedStrVal.length; i++) {
				value = splittedStrVal[i];
				value = value.trim();
				if ((value.startsWith("NAME_EN") || value.startsWith("name_en")) && !value.endsWith("=")) {
					if(value.contains("\"")){
						if (value.contains("=") && value.split("=")[0].trim().equalsIgnoreCase("NAME_EN")) {
							name_en = value.split("=\"", 2)[1];
							if(!name_en.matches(CommonConstants.RegularExpressions.ALLOWED_APP_NAME_REGEX)){
								System.out.println("Please provide a valid name ! " +CommonConstants.Messages.APP_NAME_MESSAGE);
								return;
							}
						} else {
							System.out.println("ERROR: Syntax error !");
							return;
						}
					}else{
						if (value.contains("=") && value.split("=")[0].trim().equalsIgnoreCase("NAME_EN")) {
							name_en = value.split("=", 2)[1];
							if(!name_en.matches(CommonConstants.RegularExpressions.ALLOWED_APP_NAME_REGEX)){
								System.out.println("Please provide a valid name ! " +CommonConstants.Messages.APP_NAME_MESSAGE);
								return;
							}
						} else {
							System.out.println("ERROR: Syntax error !");
							return;
						}
					}
					continue;
				} else if ((value.startsWith("NAME_DE") || value.startsWith("name_de")) && !value.endsWith("=")) {
					if(value.contains("\"")){
						if (value.contains("=") && value.split("=")[0].trim().equalsIgnoreCase("NAME_DE")) {
							name_de = value.split("=\"", 2)[1];
							if(!name_de.matches(CommonConstants.RegularExpressions.ALLOWED_APP_NAME_REGEX)){
								System.out.println("Please provide a valid name ! " +CommonConstants.Messages.APP_NAME_MESSAGE);
								return;
							}
						} else {
							System.out.println("ERROR: Syntax error !");
							return;
						}
					}else{
						if (value.contains("=") && value.split("=")[0].trim().equalsIgnoreCase("NAME_DE")) {
							name_de = value.split("=", 2)[1];
							if(!name_de.matches(CommonConstants.RegularExpressions.ALLOWED_APP_NAME_REGEX)){
								System.out.println("Please provide a valid name ! " +CommonConstants.Messages.APP_NAME_MESSAGE);
								return;
							}
						} else {
							System.out.println("ERROR: Syntax error !");
							return;
						}
					}
					continue;
				} else if ((value.startsWith("NAME") || value.startsWith("name")) && !value.endsWith("=")) {
					if(value.contains("\"")){
						if (value.contains("=") && value.split("=")[0].trim().equalsIgnoreCase("NAME")) {
							userAppName = value.split("=\"", 2)[1];
							if(!userAppName.matches(CommonConstants.RegularExpressions.ALLOWED_APP_NAME_REGEX)){
								System.out.println("Please provide a valid name ! " +CommonConstants.Messages.APP_NAME_MESSAGE);
								return;
							}
						} else {
							System.out.println("ERROR: Syntax error !");
							return;
						}
					} else {
						if (value.contains("=") && value.split("=")[0].trim().equalsIgnoreCase("NAME")) {
							userAppName = value.split("=", 2)[1];
							if(!userAppName.matches(CommonConstants.RegularExpressions.ALLOWED_APP_NAME_REGEX)){
								System.out.println("Please provide a valid name !" +CommonConstants.Messages.APP_NAME_MESSAGE);
								return;
							}
						} else {
							System.out.println("ERROR: Syntax error !");
							return;
						}
					}
					continue;
				}
				
				if ((value.startsWith("STATUS") || value.startsWith("status")) && !value.endsWith("=")) {
					if(value.contains("=") && value.split("=")[0].trim().equalsIgnoreCase("STATUS")){
						statusStr = value.split("=", 2)[1];
						statusStr = statusStr.toUpperCase();
						statusStr = statusStr.toUpperCase();
						if(!(statusStr.equalsIgnoreCase("active") || statusStr.equalsIgnoreCase("inactive"))){
							System.out.println("ERROR: Syntax error ! Status should be ACTIVE or INACTIVE");
							return;
						}
					}else{
						System.out.println("ERROR: Syntax error !");
						return;
					}
					continue;
				} else if ((value.startsWith("STATUS") || value.startsWith("status")) && value.endsWith("=")){
						System.out.println("ERROR: Syntax error ! Status should be ACTIVE or INACTIVE");
						return;
				}
				if ((value.startsWith("ICON_NAME") || value.startsWith("icon_name")) && !value.endsWith("=")) {
					if(value.contains("=") && value.split("=")[0].trim().equalsIgnoreCase("ICON_NAME")){
						iconStr = value.split("=", 2)[1];
					}else{
						System.out.println("ERROR: Syntax error !");
						return;
					}
					continue;
				}
				
				if ((value.startsWith("APPLICATION") || value.startsWith("application"))&& !value.endsWith("=")) {
					if (value.contains("=") && value.split("=")[0].trim().equalsIgnoreCase("APPLICATION")) {
						application = value.split("=", 2)[1];
					} else {
						System.out.println("ERROR: Syntax error !");
						return;
					}
					continue;
				}
				
				if ((value.startsWith("IS_PARENT") || value.startsWith("is_parent")) && !value.endsWith("=")) {
					if (value.contains("=") && value.split("=")[0].trim().equalsIgnoreCase("IS_PARENT")) {
						is_parent = value.split("=", 2)[1];
					} else {
						System.out.println("ERROR: Syntax error !");
						return;
					}
					continue;
				}
				if ((value.startsWith("IS_SINGLETON") || value.startsWith("is_singleton")) && !value.endsWith("=")) {
					if(value.contains("\"")){
						if (value.contains("=") && value.split("=")[0].trim().equalsIgnoreCase("IS_SINGLETON")) {
							is_singleton = value.split("=\"", 2)[1];
						} else {
							System.out.println("ERROR: Syntax error !");
							return;
						}
					}else{
						if (value.contains("=") && value.split("=")[0].trim().equalsIgnoreCase("IS_SINGLETON")) {
							is_singleton = value.split("=", 2)[1];
						} else {
							System.out.println("ERROR: Syntax error !");
							return;
						}
					}
					continue;
				}
				if ((value.startsWith("POSITION") || value.startsWith("position")) && !value.endsWith("=")) {
					if (value.contains("=") && value.split("=")[0].trim().equalsIgnoreCase("POSITION")) {
						position = value.split("=", 2)[1];
						position.toUpperCase();
						if(!(position.equals("*ButtonTask*") || position.equals("*IconTask*") || 
								position.equals("*MenuTask*"))){
							System.out.println("\""+position+"\""+" is not a valid position value! valid position values are  : *ButtonTask*"
									+ ", *IconTask*, *MenuTask* ");
							return;
						}
					} else {
						System.out.println("ERROR: Syntax error !");
						return;
					}
					continue;
				}
				if ((value.startsWith("DESCRIPTION_EN") || value.startsWith("description_en")) && !value.endsWith("=")) {
					if (value.contains("\"")) {
						if(value.contains("=") && value.split("=")[0].trim().equalsIgnoreCase("DESCRIPTION_EN")){
							desc_en = value.split("=\"", 2)[1];
							if(!(desc_en.length()<240)){
								System.out.println("Please provide a valid description !" +CommonConstants.Messages.DESC_LIMIT_MESSAGE);
								return;
							}
						}else{
							System.out.println("ERROR: Syntax error !");
							return;
						}
					} else {
						if(value.contains("=") && value.split("=")[0].trim().equalsIgnoreCase("DESCRIPTION_EN")){
							desc_en = value.split("=", 2)[1];
							if(!(desc_en.length()<240)){
								System.out.println("Please provide a valid description !" +CommonConstants.Messages.DESC_LIMIT_MESSAGE);
								return;
							}
						}else{
							System.out.println("ERROR: Syntax error !");
							return;
						}
					}
					continue;
				} else if ((value.startsWith("DESCRIPTION_DE") || value.startsWith("description_de")) && !value.endsWith("=")) {
					if (value.contains("\"")) {
						if(value.contains("=") && value.split("=")[0].trim().equalsIgnoreCase("DESCRIPTION_DE")){
							desc_de = value.split("=\"", 2)[1];
							if(!(desc_de.length()<240)){
								System.out.println("Please provide a valid description !" +CommonConstants.Messages.DESC_LIMIT_MESSAGE);
								return;
							}
						}else{
							System.out.println("ERROR: Syntax error !");
							return;
						}
					} else {
						if(value.contains("=") && value.split("=")[0].trim().equalsIgnoreCase("DESCRIPTION_DE")){
							desc_de = value.split("=", 2)[1];
							if(!(desc_de.length()<240)){
								System.out.println("Please provide a valid description !" +CommonConstants.Messages.DESC_LIMIT_MESSAGE);
								return;
							}
						}else{
							System.out.println("ERROR: Syntax error !");
							return;
						}
					}
					continue;
				} else if ((value.startsWith("DESCRIPTION") || value.startsWith("description")) && !value.endsWith("=")) {
					if(value.contains("\"")){
						if (value.contains("=") && value.split("=")[0].trim().equalsIgnoreCase("DESCRIPTION")) {
							desc = value.split("=\"", 2)[1];
							if(!(desc.length()<240)){
								System.out.println("Please provide a valid description !" +CommonConstants.Messages.DESC_LIMIT_MESSAGE);
								return;
							}
						} else {
							System.out.println("ERROR: Syntax error !");
							return;
						}
					}else{
						if (value.contains("=") && value.split("=")[0].trim().equalsIgnoreCase("DESCRIPTION")) {
							desc = value.split("=", 2)[1];
							if(!(desc.length()<240)){
								System.out.println("Please provide a valid description !" +CommonConstants.Messages.DESC_LIMIT_MESSAGE);
								return;
							}
						} else {
							System.out.println("ERROR: Syntax error !");
							return;
						}
					}
					continue;
				}
				if ((value.startsWith("REMARKS_EN") || value.startsWith("remarks_en")) && !value.endsWith("=")) {
					if (value.contains("\"")) {
						if(value.contains("=") && value.split("=")[0].trim().equalsIgnoreCase("REMARKS_EN")){
							remarks_en = value.split("=\"", 2)[1];
							if(!(remarks_en.length()<1500)){
								System.out.println("Please provide a valid remarks !" +CommonConstants.Messages.REMARKS_LIMIT_MESSAGE);
								return;
							}
						}else{
							System.out.println("ERROR: Syntax error !");
							return;
						}
					} else {
						if(value.contains("=") && value.split("=")[0].trim().equalsIgnoreCase("REMARKS_EN")){
							remarks_en = value.split("=", 2)[1];
							if(!(remarks_en.length()<1500)){
								System.out.println("Please provide a valid remarks !" +CommonConstants.Messages.REMARKS_LIMIT_MESSAGE);
								return;
							}
						}else{
							System.out.println("ERROR: Syntax error !");
							return;
						}
					}
					continue;
				}
				if ((value.startsWith("REMARKS_DE") || value.startsWith("remarks_de")) && !value.endsWith("=")) {
					if (value.contains("\"")) {
						if(value.contains("=") && value.split("=")[0].trim().equalsIgnoreCase("REMARKS_DE")){
							remarks_de = value.split("=\"", 2)[1];
							if(!(remarks_de.length()<1500)){
								System.out.println("Please provide a valid remarks !" +CommonConstants.Messages.REMARKS_LIMIT_MESSAGE);
								return;
							}
						}else{
							System.out.println("ERROR: Syntax error !");
							return;
						}
					} else {
						if(value.contains("=") && value.split("=")[0].trim().equalsIgnoreCase("REMARKS_DE")){
							remarks_de = value.split("=", 2)[1];
							if(!(remarks_de.length()<1500)){
								System.out.println("Please provide a valid remarks !" +CommonConstants.Messages.REMARKS_LIMIT_MESSAGE);
								return;
							}
						}else{
							System.out.println("ERROR: Syntax error !");
							return;
						}
					}
					continue;
				}
			}
			
			if(BatchUtil.isEmpty(userAppName)){
				System.out.println("user application name is required");
				return;
			} else if(BatchUtil.isEmpty(statusStr)){
				System.out.println("status is required");
				return;
			} else if(BatchUtil.isEmpty(iconStr)){
				System.out.println("icon is required");
				return;
			} else if(BatchUtil.isEmpty(position)){
				System.out.println("position is required");
				return;
			} else if(is_parent!=null && is_parent.equalsIgnoreCase("false") && BatchUtil.isEmpty(application)){
				System.out.println("application is required");
				return;
			} else if(is_parent!=null && is_parent.equalsIgnoreCase("true")){
				if(!BatchUtil.isEmpty(application)){
					System.out.println("You could not assign Base Application, if User Application is parent");
					return;
				} else if(is_singleton!=null && is_singleton.equalsIgnoreCase("true")){
					System.out.println("Application can not be Singleton, if User Application is parent");
					return;
				} else if(position!=null){
					if(!(position.equals("*ButtonTask*") || position.equals("*MenuTask*"))){
						System.out.print("\""+position+"\""+" is not a valid position value! ");
						System.out.println("If Project Application is parent, Valid positions are : *ButtonTask* and *MenuTask*. Eg: position='*MenuTask*'");
						return;
					}
				}
			}			
			IconRespWrapper iconRespWrapper = null;
			if(iconStr!=null){
				IconController iconController = new IconController();
				IkonRequest ikonRequest = new IkonRequest();
				ikonRequest.setIconName(iconStr);
				iconRespWrapper = iconController.getIconByName(ikonRequest);
				if(iconRespWrapper==null) return;
			}
			if (!BatchUtil.isEmpty(userAppName)) {
				userAppRequest.setName(userAppName);
			}
			if (!BatchUtil.isEmpty(iconStr) && iconRespWrapper!=null) {
				userAppRequest.setIconId(iconRespWrapper.getIconId());
			}
			if (!BatchUtil.isEmpty(statusStr)) {
				userAppRequest.setStatus(statusStr);
			}
			String baseAppId = null;
			if (!BatchUtil.isEmpty(application)) {
				BaseAppController baseAppController = new BaseAppController();
				baseAppId = baseAppController.getBaseAppByName(application);
				if(baseAppId==null) return;
				userAppRequest.setBaseAppId(baseAppId);
			}
			if (!BatchUtil.isEmpty(desc)) {
				userAppRequest.setDescription(desc);
			}
			if (!BatchUtil.isEmpty(is_parent)) {
				if(is_parent.equalsIgnoreCase("true") || is_parent.equalsIgnoreCase("false")){
					userAppRequest.setIsParent(is_parent);
				} else{
					System.out.println("ERROR: Syntax error ! parent should be TRUE or FALSE. Eg: is_parent=true");
					return;
				}
			}
			if (!BatchUtil.isEmpty(iconStr) && iconRespWrapper!=null) {
				userAppRequest.setIconId(iconRespWrapper.getIconId());
			}
			if (!BatchUtil.isEmpty(is_singleton)) {
				if(is_singleton.equalsIgnoreCase("true") || is_singleton.equalsIgnoreCase("false")){
					userAppRequest.setIsSingleTon(is_singleton);
				} else{
					System.out.println("ERROR: Syntax error ! singleton should be TRUE or FALSE. Eg: is_singleton=true");
					return;
				}
			}
			if (!BatchUtil.isEmpty(position)) {
				userAppRequest.setPosition(position);
			}
			if (!BatchUtil.isEmpty(name_en)) {
				userAppTranslationTbl_en.setName(desc_en);
			}
			if (!BatchUtil.isEmpty(name_de)) {
				userAppTranslationTbl_en.setName(name_de);
			}
			if (!BatchUtil.isEmpty(desc_en)) {
				userAppTranslationTbl_en.setDescription(desc_en);
			}
			if (!BatchUtil.isEmpty(remarks_en)) {
				userAppTranslationTbl_en.setRemarks(remarks_en);
			}
			if (!BatchUtil.isEmpty(desc_de)) {
				userAppTranslationTbl_de.setDescription(desc_de);
			}
			if (!BatchUtil.isEmpty(remarks_de)) {
				userAppTranslationTbl_de.setRemarks(remarks_de);
			}
			
			List<UserApplicationTranslation> userAppTranslationTblList = new ArrayList<UserApplicationTranslation>();
			userAppTranslationTblList.add(userAppTranslationTbl_en);
			userAppTranslationTblList.add(userAppTranslationTbl_de);
			userAppRequest.setUserApplicationTranslations(userAppTranslationTblList);

				Boolean saveUserApp = userAppController.saveUserApp(userAppRequest);
				if (saveUserApp == null || saveUserApp == Boolean.FALSE) {
					return;
				} else {
					System.out.println("User Application created Successfully");

				}
		}

	}

	/**
	 * Method for Creates the base app.
	 *
	 * @param cmd
	 *            {@link CommandLine}
	 */
	private void createBaseApp(CommandLine cmd, ObjectType createObjType) {
		com.magna.batch.restclient.obj.baseapp.BaseAppController baseAppController = new com.magna.batch.restclient.obj.baseapp.BaseAppController();
		BaseApplicationRequest baseAppRequest = new BaseApplicationRequest();
		String baseAppName = null, statusStr = null, iconStr = null, platforms = "WINDOWS64;WINDOWS32;LINUX64", program = null, desc_en = null, desc_de = null, remarks_en = null,
				remarks_de = null, strVal = "";
		List<BaseApplicationTransulation> baseAppTranslation = new ArrayList<>();
		BaseApplicationTransulation baseAppTranslationTbl_en = new BaseApplicationTransulation();
		baseAppTranslationTbl_en.setLanguageCode(com.magna.batch.utils.LANG_ENUM.ENGLISH.getLangCode());
		baseAppTranslation.add(baseAppTranslationTbl_en);
		BaseApplicationTransulation baseAppTranslationTbl_de = new BaseApplicationTransulation();
		baseAppTranslationTbl_de.setLanguageCode(com.magna.batch.utils.LANG_ENUM.GERMAN.getLangCode());
		baseAppTranslation.add(baseAppTranslationTbl_de);
		baseAppRequest.setBaseApplicationTransulations(baseAppTranslation);
		String[] optionValues = cmd.getOptionValues("create");
		if (optionValues.length > 0) {
			for (int index = 0; index < optionValues.length; index++) {
				if (index != 0) {
					String copyStrVal = optionValues[index];
					strVal = strVal.concat(copyStrVal);
				}
			}
			strVal = BatchUtil.getInstance().getStringValue(optionValues, cmd, createObjType);
			if(strVal==null)return;
			String[] splittedStrVal = strVal.split("\"");
			String value;
			for (int i = 0; i < splittedStrVal.length; i++) {
				value = splittedStrVal[i];
				value = value.trim();
				if ((value.startsWith("NAME") || value.startsWith("name")) && !value.endsWith("=")) {
					if(value.contains("=") && value.split("=")[0].trim().equalsIgnoreCase("NAME")){
						baseAppName = value.split("=", 2)[1];
						if(!baseAppName.matches(CommonConstants.RegularExpressions.ALLOWED_APP_NAME_REGEX)){
							System.out.println("Please provide a valid name !" +CommonConstants.Messages.APP_NAME_MESSAGE);
							return;
						} else if(!(baseAppName.length()<30)){
							System.out.println("Please provide a valid name !" +CommonConstants.Messages.NAME_LIMIT_MESSAGE);
							return;
						}
					}else{
						System.out.println("ERROR: Syntax error !");
						return;
					}
					continue;
				}
				if ((value.startsWith("STATUS") || value.startsWith("status")) && !value.endsWith("=")) {
					if(value.contains("=") && value.split("=")[0].trim().equalsIgnoreCase("STATUS")){
						statusStr = value.split("=", 2)[1];
						statusStr = statusStr.toUpperCase();
						if(!(statusStr.equalsIgnoreCase("active") || statusStr.equalsIgnoreCase("inactive"))){
							System.out.println("ERROR: Syntax error ! Status should be ACTIVE or INACTIVE");
							return;
						}
					}else{
						System.out.println("ERROR: Syntax error !");
						return;
					}
					continue;
				} else if ((value.startsWith("STATUS") || value.startsWith("status")) && value.endsWith("=")){
						System.out.println("ERROR: Syntax error ! Status should be ACTIVE or INACTIVE");
						return;
				}
				if ((value.startsWith("ICON_NAME") || value.startsWith("icon_name")) && !value.endsWith("=")) {
					if(value.contains("=") && value.split("=")[0].trim().equalsIgnoreCase("ICON_NAME")){
						iconStr = value.split("=", 2)[1];
					}else{
						System.out.println("ERROR: Syntax error !");
						return;
					}
					continue;
				}
				if ((value.startsWith("PLATFORMS") || value.startsWith("platforms")) && !value.endsWith("=")) {
					if (value.contains("=") && value.split("=")[0].trim().equalsIgnoreCase("PLATFORMS")) {
						platforms = value.split("=", 2)[1];//WINDOWS64;WINDOWS32;LINUX64
						platforms = platforms.trim().toUpperCase();
						if(platforms.contains(";")){
							String[] split = platforms.split(";");
							for (int index = 0; index < split.length; index++) {
								if(!("WINDOWS64".equals(split[index]) || "WINDOWS32".equals(split[index]) || "LINUX64".equals(split[index]))){
									System.out.println("ERROR: Syntax error ! Platforms should be WINDOWS64, WINDOWS32, LINUX64 - Eg: WINDOWS64;WINDOWS32;LINUX64 ");
									return;
								}
								String string = split[index];
								for (int index2 = 0; index2 < split.length; index2++) {
									if(index!=index2){
										String string2 = split[index2];
										if(string.equalsIgnoreCase(string2)){
											System.out.println("ERROR: Syntax error ! Platform name "+ "\""+"-"+string+"\""+" should't be multiple times. ");
											return;
										}
									}
								}
							}
						} else if(!("WINDOWS64".equals(platforms) || "WINDOWS32".equals(platforms) || "LINUX64".equals(platforms))){
							System.out.println("ERROR: Syntax error ! Platforms should be WINDOWS64, WINDOWS32, LINUX64 - Eg: WINDOWS64;WINDOWS32;LINUX64 ");
							return;
						}
					} else {
						System.out.println("ERROR: Syntax error !");
						return;
					}
					continue;
				}
				if ((value.startsWith("PROGRAM")  || value.startsWith("program"))&& !value.endsWith("=")) {
					if (value.contains("=") && value.split("=")[0].trim().equalsIgnoreCase("PROGRAM")) {
						program = value.split("=", 2)[1];
					} else {
						System.out.println("ERROR: Syntax error !");
						return;
					}
					continue;
				}
				if ((value.startsWith("DESCRIPTION_EN") || value.startsWith("description_en")) && !value.endsWith("=")) {
					if (value.contains("\"")) {
						if(value.contains("=") && value.split("=")[0].trim().equalsIgnoreCase("DESCRIPTION_EN")){
							desc_en = value.split("=\"", 2)[1];
							if(!(desc_en.length()<240)){
								System.out.println("Please provide a valid description !" +CommonConstants.Messages.DESC_LIMIT_MESSAGE);
								return;
							}
						}else{
							System.out.println("ERROR: Syntax error !");
							return;
						}
					} else {
						if(value.contains("=") && value.split("=")[0].trim().equalsIgnoreCase("DESCRIPTION_EN")){
							desc_en = value.split("=", 2)[1];
							if(!(desc_en.length()<240)){
								System.out.println("Please provide a valid description !" +CommonConstants.Messages.DESC_LIMIT_MESSAGE);
								return;
							}
						}else{
							System.out.println("ERROR: Syntax error !");
							return;
						}
					}
					continue;
				}
				if ((value.startsWith("DESCRIPTION_DE") || value.startsWith("description_de")) && !value.endsWith("=")) {
					if (value.contains("\"")) {
						if(value.contains("=") && value.split("=")[0].trim().equalsIgnoreCase("DESCRIPTION_DE")){
							desc_de = value.split("=\"", 2)[1];
							if(!(desc_de.length()<240)){
								System.out.println("Please provide a valid description !" +CommonConstants.Messages.DESC_LIMIT_MESSAGE);
								return;
							}
						}else{
							System.out.println("ERROR: Syntax error !");
							return;
						}
					} else {
						if(value.contains("=") && value.split("=")[0].trim().equalsIgnoreCase("DESCRIPTION_DE")){
							desc_de = value.split("=", 2)[1];
							if(!(desc_de.length()<240)){
								System.out.println("Please provide a valid description !" +CommonConstants.Messages.DESC_LIMIT_MESSAGE);
								return;
							}
						}else{
							System.out.println("ERROR: Syntax error !");
							return;
						}
					}
					continue;
				}
				if ((value.startsWith("REMARKS_EN") || value.startsWith("remarks_en")) && !value.endsWith("=")) {
					if (value.contains("\"")) {
						if(value.contains("=") && value.split("=")[0].trim().equalsIgnoreCase("REMARKS_EN")){
							remarks_en = value.split("=\"", 2)[1];
							if(!(remarks_en.length()<1500)){
								System.out.println("Please provide a valid remarks !" +CommonConstants.Messages.REMARKS_LIMIT_MESSAGE);
								return;
							}
						}else{
							System.out.println("ERROR: Syntax error !");
							return;
						}
					} else {
						if(value.contains("=") && value.split("=")[0].trim().equalsIgnoreCase("REMARKS_EN")){
							remarks_en = value.split("=", 2)[1];
							if(!(remarks_en.length()<1500)){
								System.out.println("Please provide a valid remarks !" +CommonConstants.Messages.REMARKS_LIMIT_MESSAGE);
								return;
							}
						}else{
							System.out.println("ERROR: Syntax error !");
							return;
						}
					}
					continue;
				}
				if ((value.startsWith("REMARKS_DE") || value.startsWith("remarks_de")) && !value.endsWith("=")) {
					if (value.contains("\"")) {
						if(value.contains("=") && value.split("=")[0].trim().equalsIgnoreCase("REMARKS_DE")){
							remarks_de = value.split("=\"", 2)[1];
							if(!(remarks_de.length()<1500)){
								System.out.println("Please provide a valid remarks !" +CommonConstants.Messages.REMARKS_LIMIT_MESSAGE);
								return;
							}
						}else{
							System.out.println("ERROR: Syntax error !");
							return;
						}
					} else {
						if(value.contains("=") && value.split("=")[0].trim().equalsIgnoreCase("REMARKS_DE")){
							remarks_de = value.split("=", 2)[1];
							if(!(remarks_de.length()<1500)){
								System.out.println("Please provide a valid remarks !" +CommonConstants.Messages.REMARKS_LIMIT_MESSAGE);
								return;
							}
						}else{
							System.out.println("ERROR: Syntax error !");
							return;
						}
					}
					continue;
				}
			}
			
			if(BatchUtil.isEmpty(baseAppName)){
				System.out.println("base application name is required");
				return;
			} else if(BatchUtil.isEmpty(statusStr)){
				System.out.println("status is required");
				return;
			} else if(BatchUtil.isEmpty(iconStr)){
				System.out.println("icon is required");
				return;
			} else if(BatchUtil.isEmpty(program)){
				System.out.println("program is required");// ini_test
				return;
			}else if(BatchUtil.isEmpty(platforms)){
				System.out.println("platform is required");// WINDOWS64;WINDOWS32;LINUX64
				return;
			}
			
			IconRespWrapper iconRespWrapper = null;
			if(iconStr!=null){
				IconController iconController = new IconController();
				IkonRequest ikonRequest = new IkonRequest();
				ikonRequest.setIconName(iconStr);
				iconRespWrapper = iconController.getIconByName(ikonRequest);
				if(iconRespWrapper==null) return;
			}
			if (!BatchUtil.isEmpty(baseAppName)) {
				baseAppRequest.setName(baseAppName);
			}
			if (!BatchUtil.isEmpty(iconStr) && iconRespWrapper!=null) {
				baseAppRequest.setIconId(iconRespWrapper.getIconId());
			}
			if (!BatchUtil.isEmpty(statusStr)) {
				baseAppRequest.setStatus(statusStr);
			}
			if (!BatchUtil.isEmpty(platforms)) {
				baseAppRequest.setPlatforms(platforms);
			}
			if (!BatchUtil.isEmpty(program)) {
				baseAppRequest.setProgram(program);
			}
			if (!BatchUtil.isEmpty(desc_en)) {
				baseAppTranslationTbl_en.setDescription(desc_en);
			}
			if (!BatchUtil.isEmpty(remarks_en)) {
				baseAppTranslationTbl_en.setRemarks(remarks_en);
			}
			if (!BatchUtil.isEmpty(desc_de)) {
				baseAppTranslationTbl_de.setDescription(desc_de);
			}
			if (!BatchUtil.isEmpty(remarks_de)) {
				baseAppTranslationTbl_de.setRemarks(remarks_de);
			}
			List<BaseApplicationTransulation> baseAppTranslationTblList = new ArrayList<BaseApplicationTransulation>();
			baseAppTranslationTblList.add(baseAppTranslationTbl_en);
			baseAppTranslationTblList.add(baseAppTranslationTbl_de);
			baseAppRequest.setBaseApplicationTransulations(baseAppTranslationTblList);

				Boolean saveBaseApp = baseAppController.saveBaseApp(baseAppRequest);
				if (saveBaseApp == null || saveBaseApp == Boolean.FALSE) {
					return;
				} else {
					System.out.println("Base Application created Successfully");

				}
		}

	}

	/**
	 * Method for Creates the project.
	 *
	 * @param cmd
	 *            {@link CommandLine}
	 */
	private void createProject(CommandLine cmd, ObjectType createObjType) {
		com.magna.batch.restclient.obj.project.ProjectController projectController = new com.magna.batch.restclient.obj.project.ProjectController();
		ProjectRequest projectRequest = new ProjectRequest();
		String projectName = null, statusStr = null, iconStr = null, desc_en = null, desc_de = null, remarks_en = null,
				remarks_de = null, strVal = "";
		List<ProjectTranslation> projectTranslation = new ArrayList<>();
		ProjectTranslation projectTranslationTbl_en = new ProjectTranslation();
		projectTranslationTbl_en.setLanguageCode(com.magna.batch.utils.LANG_ENUM.ENGLISH.getLangCode());
		projectTranslation.add(projectTranslationTbl_en);
		ProjectTranslation projectTranslationTbl_de = new ProjectTranslation();
		projectTranslationTbl_de.setLanguageCode(com.magna.batch.utils.LANG_ENUM.GERMAN.getLangCode());
		projectTranslation.add(projectTranslationTbl_de);
		projectRequest.setProjectTranslations(projectTranslation);
		String[] optionValues = cmd.getOptionValues("create");
		if (optionValues.length > 0) {
			strVal = BatchUtil.getInstance().getStringValue(optionValues, cmd, createObjType);
			if(strVal==null)return;
			String[] splittedStrVal = strVal.split("\"");
			String value;
			for (int i = 0; i < splittedStrVal.length; i++) {
				value = splittedStrVal[i];
				value = value.trim();
				if ((value.startsWith("NAME") || value.startsWith("name")) && !value.endsWith("=")) {
					if(value.contains("=") && value.split("=")[0].trim().equalsIgnoreCase("NAME")){
						projectName = value.split("=", 2)[1];
						if(!projectName.matches(CommonConstants.RegularExpressions.ALLOWED_PROJECT_NAME_REGEX)){
							System.out.println("Please provide a valid name !" +CommonConstants.Messages.PROJECT_NAME_MESSAGE);
							return;
						} else if(!(projectName.length()<30)){
							System.out.println("Please provide a valid name !" +CommonConstants.Messages.NAME_LIMIT_MESSAGE);
							return;
						}
					}else{
						System.out.println("ERROR: Syntax error !");
						return;
					}
					continue;
				}
				if ((value.startsWith("STATUS") || value.startsWith("status")) && !value.endsWith("=")) {
					if(value.contains("=") && value.split("=")[0].trim().equalsIgnoreCase("STATUS")){
						statusStr = value.split("=", 2)[1];
						statusStr = statusStr.toUpperCase();
						if(!(statusStr.equalsIgnoreCase("active") || statusStr.equalsIgnoreCase("inactive"))){
							System.out.println("ERROR: Syntax error ! Status should be ACTIVE or INACTIVE");
							return;
						}
					}else{
						System.out.println("ERROR: Syntax error !");
						return;
					}
					continue;
				} else if ((value.startsWith("STATUS") || value.startsWith("status")) && value.endsWith("=")){
						System.out.println("ERROR: Syntax error ! Status should be ACTIVE or INACTIVE");
						return;
				}
				if ((value.startsWith("ICON_NAME") || value.startsWith("icon_name")) && !value.endsWith("=")) {
					if(value.contains("=") && value.split("=")[0].trim().equalsIgnoreCase("ICON_NAME")){
						iconStr = value.split("=", 2)[1];
					}else{
						System.out.println("ERROR: Syntax error !");
						return;
					}
					continue;
				}
				if ((value.startsWith("DESCRIPTION_EN") || value.startsWith("description_en")) && !value.endsWith("=")) {
					if (value.contains("\"")) {
						if(value.contains("=") && value.split("=")[0].trim().equalsIgnoreCase("DESCRIPTION_EN")){
							desc_en = value.split("=\"", 2)[1];
							if(!(desc_en.length()<240)){
								System.out.println("Please provide a valid description !" +CommonConstants.Messages.DESC_LIMIT_MESSAGE);
								return;
							}
						}else{
							System.out.println("ERROR: Syntax error !");
							return;
						}
					} else {
						if(value.contains("=") && value.split("=")[0].trim().equalsIgnoreCase("DESCRIPTION_EN")){
							desc_en = value.split("=", 2)[1];
							if(!(desc_en.length()<240)){
								System.out.println("Please provide a valid description !" +CommonConstants.Messages.DESC_LIMIT_MESSAGE);
								return;
							}
						}else{
							System.out.println("ERROR: Syntax error !");
							return;
						}
					}
					continue;
				}
				if ((value.startsWith("DESCRIPTION_DE") || value.startsWith("description_de")) && !value.endsWith("=")) {
					if (value.contains("\"")) {
						if(value.contains("=") && value.split("=")[0].trim().equalsIgnoreCase("DESCRIPTION_DE")){
							desc_de = value.split("=\"", 2)[1];
							if(!(desc_de.length()<240)){
								System.out.println("Please provide a valid description !" +CommonConstants.Messages.DESC_LIMIT_MESSAGE);
								return;
							}
						}else{
							System.out.println("ERROR: Syntax error !");
							return;
						}
					} else {
						if(value.contains("=") && value.split("=")[0].trim().equalsIgnoreCase("DESCRIPTION_DE")){
							desc_de = value.split("=", 2)[1];
							if(!(desc_de.length()<240)){
								System.out.println("Please provide a valid description !" +CommonConstants.Messages.DESC_LIMIT_MESSAGE);
								return;
							}
						}else{
							System.out.println("ERROR: Syntax error !");
							return;
						}
					}
					continue;
				}
				if ((value.startsWith("REMARKS_EN") || value.startsWith("remarks_en")) && !value.endsWith("=")) {
					if (value.contains("\"")) {
						if(value.contains("=") && value.split("=")[0].trim().equalsIgnoreCase("REMARKS_EN")){
							remarks_en = value.split("=\"", 2)[1];
							if(!(remarks_en.length()<1500)){
								System.out.println("Please provide a valid remarks !" +CommonConstants.Messages.REMARKS_LIMIT_MESSAGE);
								return;
							}
						}else{
							System.out.println("ERROR: Syntax error !");
							return;
						}
					} else {
						if(value.contains("=") && value.split("=")[0].trim().equalsIgnoreCase("REMARKS_EN")){
							remarks_en = value.split("=", 2)[1];
							if(!(remarks_en.length()<1500)){
								System.out.println("Please provide a valid remarks !" +CommonConstants.Messages.REMARKS_LIMIT_MESSAGE);
								return;
							}
						}else{
							System.out.println("ERROR: Syntax error !");
							return;
						}
					}
					continue;
				}
				if ((value.startsWith("REMARKS_DE") || value.startsWith("remarks_de")) && !value.endsWith("=")) {
					if (value.contains("\"")) {
						if(value.contains("=") && value.split("=")[0].trim().equalsIgnoreCase("REMARKS_DE")){
							remarks_de = value.split("=\"", 2)[1];
							if(!(remarks_de.length()<1500)){
								System.out.println("Please provide a valid remarks !" +CommonConstants.Messages.REMARKS_LIMIT_MESSAGE);
								return;
							}
						}else{
							System.out.println("ERROR: Syntax error !");
							return;
						}
					} else {
						if(value.contains("=") && value.split("=")[0].trim().equalsIgnoreCase("REMARKS_DE")){
							remarks_de = value.split("=", 2)[1];
							if(!(remarks_de.length()<1500)){
								System.out.println("Please provide a valid remarks !" +CommonConstants.Messages.REMARKS_LIMIT_MESSAGE);
								return;
							}
						}else{
							System.out.println("ERROR: Syntax error !");
							return;
						}
					}
					continue;
				}
			}
			
			if(BatchUtil.isEmpty(projectName)){
				System.out.println("project name is required");
				return;
			} else if(BatchUtil.isEmpty(statusStr)){
				System.out.println("status is required");
				return;
			} else if(BatchUtil.isEmpty(iconStr)){
				System.out.println("icon is required");
				return;
			} 
			
			IconRespWrapper iconRespWrapper = null;
			if(iconStr!=null){
				IconController iconController = new IconController();
				IkonRequest ikonRequest = new IkonRequest();
				ikonRequest.setIconName(iconStr);
				iconRespWrapper = iconController.getIconByName(ikonRequest);
				if(iconRespWrapper==null) return;
			}
			if (!BatchUtil.isEmpty(projectName)) {
				projectRequest.setName(projectName);
			}
			if (!BatchUtil.isEmpty(iconStr) && iconRespWrapper!=null) {
				projectRequest.setIconId(iconRespWrapper.getIconId());
			}
			if (!BatchUtil.isEmpty(statusStr)) {
				projectRequest.setStatus(statusStr);
			}
			if (!BatchUtil.isEmpty(desc_en)) {
				projectTranslationTbl_en.setDescription(desc_en);
			}
			if (!BatchUtil.isEmpty(remarks_en)) {
				projectTranslationTbl_en.setRemarks(remarks_en);
			}
			if (!BatchUtil.isEmpty(desc_de)) {
				projectTranslationTbl_de.setDescription(desc_de);
			}
			if (!BatchUtil.isEmpty(remarks_de)) {
				projectTranslationTbl_de.setRemarks(remarks_de);
			}
			List<ProjectTranslation> projectTranslationTblList = new ArrayList<ProjectTranslation>();
			projectTranslationTblList.add(projectTranslationTbl_en);
			projectTranslationTblList.add(projectTranslationTbl_de);
			projectRequest.setProjectTranslations(projectTranslationTblList);

				Boolean saveProject = projectController.saveProject(projectRequest);
				if (saveProject == null || saveProject == Boolean.FALSE) {
					return;
				} else {
					System.out.println("Project created Successfully");

				}
		}
	}


	/**
	 * Method for Creates the admin area.
	 *
	 * @param cmd {@link CommandLine}
	 */
	private void createAdminArea(CommandLine cmd, ObjectType createObjType) {
		com.magna.batch.restclient.obj.adminarea.AdminAreaController adminAreaController = new com.magna.batch.restclient.obj.adminarea.AdminAreaController();
		AdminAreaRequest adminAreaRequest = new AdminAreaRequest();
		String adminAreaName = null, statusStr = null, iconStr = null, desc_en = null, desc_de = null,
				remarks_en = null, remarks_de = null, singleton_app_timeout = null, hotline_contact_email = null,
				hotline_contact_number = null, strVal = "";
		List<AdminAreaTranslation> adminAreaTranslation = new ArrayList<>();
		AdminAreaTranslation adminATranslationTbl_en = new AdminAreaTranslation();
		adminATranslationTbl_en.setLanguageCode(com.magna.batch.utils.LANG_ENUM.ENGLISH.getLangCode());
		adminAreaTranslation.add(adminATranslationTbl_en);
		AdminAreaTranslation adminATranslationTbl_de = new AdminAreaTranslation();
		adminATranslationTbl_de.setLanguageCode(com.magna.batch.utils.LANG_ENUM.GERMAN.getLangCode());
		adminAreaTranslation.add(adminATranslationTbl_de);
		adminAreaRequest.setAdminAreaTranslations(adminAreaTranslation);
		String[] optionValues = cmd.getOptionValues("create");
		if (optionValues.length > 0) {
			strVal = BatchUtil.getInstance().getStringValue(optionValues, cmd, createObjType);
			if(strVal==null)return;
			String[] splittedStrVal = strVal.split("\"");
			String value;
			for (int i = 0; i < splittedStrVal.length; i++) {
				value = splittedStrVal[i];
				value = value.trim();
				if ((value.startsWith("NAME") || value.startsWith("name")) && !value.endsWith("=")) {
					if (value.contains("=") && value.split("=")[0].trim().equalsIgnoreCase("NAME")) {
						adminAreaName = value.split("=", 2)[1];
						if(!adminAreaName.matches(CommonConstants.RegularExpressions.ALLOWED_NAME_REGEX)){
							System.out.println("Please provide a valid name !" +CommonConstants.Messages.NAME_MESSAGE);
							return;
						} else if(!(adminAreaName.length()<30)){
							System.out.println("Please provide a valid name !" +CommonConstants.Messages.NAME_LIMIT_MESSAGE);
							return;
						}
					} else {
						System.out.println("ERROR: Syntax error !");
						return;
					}
					continue;
				}
				if ((value.startsWith("STATUS") || value.startsWith("status")) && !value.endsWith("=")) {
					if (value.contains("=") && value.split("=")[0].trim().equalsIgnoreCase("STATUS")) {
						statusStr = value.split("=", 2)[1];
						statusStr = statusStr.toUpperCase();
						if(!(statusStr.equalsIgnoreCase("active") || statusStr.equalsIgnoreCase("inactive"))){
							System.out.println("ERROR: Syntax error ! Status should be ACTIVE or INACTIVE");
							return;
						}
					} else {
						System.out.println("ERROR: Syntax error !");
						return;
					}
					continue;
				} else if ((value.startsWith("STATUS") || value.startsWith("status")) && value.endsWith("=")) {
					System.out.println("ERROR: Syntax error ! Status should be ACTIVE or INACTIVE");
					return;
				}
				if ((value.startsWith("ICON_NAME") || value.startsWith("icon_name")) && !value.endsWith("=")) {
					if (value.contains("=") && value.split("=")[0].trim().equalsIgnoreCase("ICON_NAME")) {
						iconStr = value.split("=", 2)[1];
					} else {
						System.out.println("ERROR: Syntax error !");
						return;
					}
					continue;
				}
				if ((value.startsWith("DESCRIPTION_EN") || value.startsWith("description_en")) && !value.endsWith("=")) {
					if(value.contains("\"")){
						if (value.contains("=") && value.split("=")[0].trim().equalsIgnoreCase("DESCRIPTION_EN")) {
							desc_en = value.split("=\"", 2)[1];
							if(!(desc_en.length()<240)){
								System.out.println("Please provide a valid description !" +CommonConstants.Messages.DESC_LIMIT_MESSAGE);
								return;
							}
						} else {
							System.out.println("ERROR: Syntax error !");
							return;
						}
					}else{
						if (value.contains("=") && value.split("=")[0].trim().equalsIgnoreCase("DESCRIPTION_EN")) {
							desc_en = value.split("=", 2)[1];
							if(!(desc_en.length()<240)){
								System.out.println("Please provide a valid description !" +CommonConstants.Messages.DESC_LIMIT_MESSAGE);
								return;
							}
						} else {
							System.out.println("ERROR: Syntax error !");
							return;
						}
					}
					continue;
				}
				if ((value.startsWith("DESCRIPTION_DE")|| value.startsWith("description_de")) && !value.endsWith("=")) {
					if(value.contains("\"")){
						if (value.contains("=") && value.split("=")[0].trim().equalsIgnoreCase("DESCRIPTION_DE")) {
							desc_de = value.split("=\"", 2)[1];
							if(!(desc_de.length()<240)){
								System.out.println("Please provide a valid description !" +CommonConstants.Messages.DESC_LIMIT_MESSAGE);
								return;
							}
						} else {
							System.out.println("ERROR: Syntax error !");
							return;
						}
					}else{
						if (value.contains("=") && value.split("=")[0].trim().equalsIgnoreCase("DESCRIPTION_DE")) {
							desc_de = value.split("=", 2)[1];
							if(!(desc_de.length()<240)){
								System.out.println("Please provide a valid description !" +CommonConstants.Messages.DESC_LIMIT_MESSAGE);
								return;
							}
						} else {
							System.out.println("ERROR: Syntax error !");
							return;
						}
					}
					continue;
				}
				if ((value.startsWith("REMARKS_EN") || value.startsWith("remarks_en")) && !value.endsWith("=")) {
					if(value.contains("\"")){
						if (value.contains("=") && value.split("=")[0].trim().equalsIgnoreCase("REMARKS_EN")) {
							remarks_en = value.split("=\"", 2)[1];
							if(!(remarks_en.length()<1500)){
								System.out.println("Please provide a valid remarks !" +CommonConstants.Messages.REMARKS_LIMIT_MESSAGE);
								return;
							}
						} else {
							System.out.println("ERROR: Syntax error !");
							return;
						}
					}else{
						if (value.contains("=") && value.split("=")[0].trim().equalsIgnoreCase("REMARKS_EN")) {
							remarks_en = value.split("=", 2)[1];
							if(!(remarks_en.length()<1500)){
								System.out.println("Please provide a valid remarks !" +CommonConstants.Messages.REMARKS_LIMIT_MESSAGE);
								return;
							}
						} else {
							System.out.println("ERROR: Syntax error !");
							return;
						}
					}
					continue;
				}
				if ((value.startsWith("REMARKS_DE") || value.startsWith("remarks_de")) && !value.endsWith("=")) {
					if(value.contains("\"")){
						if (value.contains("=") && value.split("=")[0].trim().equalsIgnoreCase("REMARKS_DE")) {
							remarks_de = value.split("=\"", 2)[1];
							if(!(remarks_de.length()<1500)){
								System.out.println("Please provide a valid remarks !" +CommonConstants.Messages.REMARKS_LIMIT_MESSAGE);
								return;
							}
						} else {
							System.out.println("ERROR: Syntax error !");
							return;
						}
					}else{
						if (value.contains("=") && value.split("=")[0].trim().equalsIgnoreCase("REMARKS_DE")) {
							remarks_de = value.split("=", 2)[1];
							if(!(remarks_de.length()<1500)){
								System.out.println("Please provide a valid remarks !" +CommonConstants.Messages.REMARKS_LIMIT_MESSAGE);
								return;
							}
						} else {
							System.out.println("ERROR: Syntax error !");
							return;
						}
					}
					continue;
				}
				if ((value.startsWith("SINGLETON_APP_TIMEOUT") || value.startsWith("singleton_app_timeout")) && !value.endsWith("=")) {
					if (value.contains("=") && value.split("=")[0].trim().equalsIgnoreCase("SINGLETON_APP_TIMEOUT")) {
						singleton_app_timeout = value.split("=", 2)[1];
					} else {
						System.out.println("ERROR: Syntax error !");
						return;
					}
					continue;
				}
				if ((value.startsWith("HOTLINE_CONTACT_NUMBER") || value.startsWith("hotline_contact_number")) && !value.endsWith("=")) {
					if (value.contains("=") && value.split("=")[0].trim().equalsIgnoreCase("HOTLINE_CONTACT_NUMBER")) {
						hotline_contact_number = value.split("=", 2)[1];
						if(!(hotline_contact_number.length()<30)){
							System.out.println("Please provide a valid hotline contact number !" +CommonConstants.Messages.HOTLINE_CONTACT_LIMIT_MESSAGE);
							return;
						}
					} else {
						System.out.println("ERROR: Syntax error !");
						return;
					}
					continue;
				}
				if ((value.startsWith("HOTLINE_CONTACT_EMAIL") || value.startsWith("hotline_contact_email")) && !value.endsWith("=")) {
					if (value.contains("=") && value.split("=")[0].trim().equalsIgnoreCase("HOTLINE_CONTACT_EMAIL")) {
						hotline_contact_email = value.split("=", 2)[1];
						if(!hotline_contact_email.matches(CommonConstants.RegularExpressions.EMAIL_REGEX)){
							System.out.println("Please provide a valid email address ! " +CommonConstants.Messages.EMAIL_MESSAGE);
							return;
						}else if(!(hotline_contact_email.length()<30)){
							System.out.println("Please provide a valid email address !" +CommonConstants.Messages.NAME_LIMIT_MESSAGE);
							return;
						}
					} else {
						System.out.println("ERROR: Syntax error !");
						return;
					}
					continue;
				}
			}
			
			if(BatchUtil.isEmpty(adminAreaName)){
				System.out.println("admin area name is required");
				return;
			} else if(BatchUtil.isEmpty(statusStr)){
				System.out.println("status is required");
				return;
			} else if(BatchUtil.isEmpty(iconStr)){
				System.out.println("icon is required");
				return;
			} else if(BatchUtil.isEmpty(hotline_contact_number)){
				System.out.println("hotline contact number is required");
				return;
			} else if(BatchUtil.isEmpty(hotline_contact_email)){
				System.out.println("hotline contact email is required");
				return;
			}
			
			IconRespWrapper iconRespWrapper = null;
			if(iconStr!=null){
				IconController iconController = new IconController();
				IkonRequest ikonRequest = new IkonRequest();
				ikonRequest.setIconName(iconStr);
				iconRespWrapper = iconController.getIconByName(ikonRequest);
				if(iconRespWrapper==null) return;
			}
			if (!BatchUtil.isEmpty(adminAreaName)) {
				adminAreaRequest.setName(adminAreaName);
			}
			if (!BatchUtil.isEmpty(iconStr) && iconRespWrapper!=null) {
				adminAreaRequest.setIconId(iconRespWrapper.getIconId());
			}
			if (!BatchUtil.isEmpty(statusStr)) {
				adminAreaRequest.setStatus(statusStr);
			}
			if (!BatchUtil.isEmpty(hotline_contact_number)) {
				adminAreaRequest.setHotlineContactNo(hotline_contact_number);
			}
			if (!BatchUtil.isEmpty(hotline_contact_email)) {
				adminAreaRequest.setHotlineContactEmail(hotline_contact_email);
			}
			if (!BatchUtil.isEmpty(desc_en)) {
				adminATranslationTbl_en.setDescription(desc_en);
			}
			if (!BatchUtil.isEmpty(remarks_en)) {
				adminATranslationTbl_en.setRemarks(remarks_en);
			}
			if (!BatchUtil.isEmpty(desc_de)) {
				adminATranslationTbl_de.setDescription(desc_de);
			}
			if (!BatchUtil.isEmpty(remarks_de)) {
				adminATranslationTbl_de.setRemarks(remarks_de);
			}
			if (!BatchUtil.isEmpty(singleton_app_timeout)) {
				if(singleton_app_timeout.matches("[0-9]*")){
					long parseLong = Long.parseLong(singleton_app_timeout);
					if(parseLong <= 7200){
						adminAreaRequest.setSingleTonAppTimeOut(parseLong);
					} else {
						System.out.println("ERROR: Syntax Error ! singleton_app_timeout limit should not exceed to 7200.");
						return;
					}
				} else {
					System.out.println("ERROR: Syntax Error ! singleton_app_timeout accepts only numbers.");
					return;
				}
			}
			
			List<AdminAreaTranslation> adminATranslationTblList = new ArrayList<AdminAreaTranslation>();
			adminATranslationTblList.add(adminATranslationTbl_en);
			adminATranslationTblList.add(adminATranslationTbl_de);
			adminAreaRequest.setAdminAreaTranslations(adminATranslationTblList);
		}
			Boolean saveAdminArea = adminAreaController.saveAdminArea(adminAreaRequest);
			if (saveAdminArea == null || saveAdminArea == Boolean.FALSE) {
				return;
			} else {
				System.out.println("AdminArea created Successfully");

			}

	}

	/**
	 * Method for Creates the site.
	 *
	 * @param cmd
	 *            {@link CommandLine}
	 */
	public void createSite(CommandLine cmd, ObjectType createObjType) {
		com.magna.batch.restclient.obj.site.SiteController siteController = new com.magna.batch.restclient.obj.site.SiteController();
		SiteRequest siteRequest = new SiteRequest();
		String siteName = null, statusStr = null, iconStr = null, desc_en = null, desc_de = null, remarks_en = null,
				remarks_de = null, strVal = "";
		List<SiteTranslation> siteTranslation = new ArrayList<>();
		SiteTranslation siteTranslationTbl_en = new SiteTranslation();
		siteTranslationTbl_en.setLanguageCode(com.magna.batch.utils.LANG_ENUM.ENGLISH.getLangCode());
		siteTranslation.add(siteTranslationTbl_en);
		SiteTranslation siteTranslationTbl_de = new SiteTranslation();
		siteTranslationTbl_de.setLanguageCode(com.magna.batch.utils.LANG_ENUM.GERMAN.getLangCode());
		siteTranslation.add(siteTranslationTbl_de);
		siteRequest.setSiteTranslations(siteTranslation);
		String[] optionValues = cmd.getOptionValues("create");
		if (optionValues.length > 0) {
			strVal = BatchUtil.getInstance().getStringValue(optionValues, cmd, createObjType);
			if(strVal==null)return;
			String[] splittedStrVal = strVal.split("\"");
			String value;
			for (int i = 0; i < splittedStrVal.length; i++) {
				value = splittedStrVal[i];
				value = value.trim();
				if ((value.startsWith("NAME") || value.startsWith("name")) && !value.endsWith("=")) {
					if(value.contains("=") && value.split("=")[0].trim().equalsIgnoreCase("NAME")){
						siteName = value.split("=", 2)[1];
						if(!siteName.matches(CommonConstants.RegularExpressions.ALLOWED_SITE_NAME_REGEXP)){
							System.out.println("Please provide a valid name !" +CommonConstants.Messages.SITE_NAME_MESSAGE);
							return;
						} else if(!(siteName.length()<30)){
							System.out.println("Please provide a valid name !" +CommonConstants.Messages.NAME_LIMIT_MESSAGE);
							return;
						}
					}else{
						System.out.println("ERROR: Syntax error !");
						return;
					}
					continue;
				}
				if ((value.startsWith("STATUS") || value.startsWith("status")) && !value.endsWith("=")) {
					if(value.contains("=") && value.split("=")[0].trim().equalsIgnoreCase("STATUS")){
						statusStr = value.split("=", 2)[1];
						statusStr = statusStr.toUpperCase();
						if(!(statusStr.equalsIgnoreCase("active") || statusStr.equalsIgnoreCase("inactive"))){
							System.out.println("ERROR: Syntax error ! Status should be ACTIVE or INACTIVE");
							return;
						}
					}else{
						System.out.println("ERROR: Syntax error !");
						return;
					}
					continue;
				} else if ((value.startsWith("STATUS") || value.startsWith("status")) && value.endsWith("=")){
						System.out.println("ERROR: Syntax error ! Status should be ACTIVE or INACTIVE");
						return;
				}
				if ((value.startsWith("ICON_NAME") || value.startsWith("icon_name")) && !value.endsWith("=")) {
					if(value.contains("=") && value.split("=")[0].trim().equalsIgnoreCase("ICON_NAME")){
						iconStr = value.split("=", 2)[1];
					}else{
						System.out.println("ERROR: Syntax error !");
						return;
					}
					continue;
				}
				if ((value.startsWith("DESCRIPTION_EN") || value.startsWith("description_en")) && !value.endsWith("=")) {
					if (value.contains("\"")) {
						if(value.contains("=") && value.split("=")[0].trim().equalsIgnoreCase("DESCRIPTION_EN")){
							desc_en = value.split("=\"", 2)[1];
							if(!(desc_en.length()<240)){
								System.out.println("Please provide a valid description !" +CommonConstants.Messages.DESC_LIMIT_MESSAGE);
								return;
							}
						}else{
							System.out.println("ERROR: Syntax error !");
							return;
						}
					} else {
						if(value.contains("=") && value.split("=")[0].trim().equalsIgnoreCase("DESCRIPTION_EN")){
							desc_en = value.split("=", 2)[1];
							if(!(desc_en.length()<240)){
								System.out.println("Please provide a valid description !" +CommonConstants.Messages.DESC_LIMIT_MESSAGE);
								return;
							}
						}else{
							System.out.println("ERROR: Syntax error !");
							return;
						}
					}
					continue;
				}
				if ((value.startsWith("DESCRIPTION_DE") || value.startsWith("description_de")) && !value.endsWith("=")) {
					if (value.contains("\"")) {
						if(value.contains("=") && value.split("=")[0].trim().equalsIgnoreCase("DESCRIPTION_DE")){
							desc_de = value.split("=\"", 2)[1];
							if(!(desc_de.length()<240)){
								System.out.println("Please provide a valid description !" +CommonConstants.Messages.DESC_LIMIT_MESSAGE);
								return;
							}
						}else{
							System.out.println("ERROR: Syntax error !");
							return;
						}
					} else {
						if(value.contains("=") && value.split("=")[0].trim().equalsIgnoreCase("DESCRIPTION_DE")){
							desc_de = value.split("=", 2)[1];
							if(!(desc_de.length()<240)){
								System.out.println("Please provide a valid description !" +CommonConstants.Messages.DESC_LIMIT_MESSAGE);
								return;
							}
						}else{
							System.out.println("ERROR: Syntax error !");
							return;
						}
					}
					continue;
				}
				if ((value.startsWith("REMARKS_EN") || value.startsWith("remarks_en")) && !value.endsWith("=")) {
					if (value.contains("\"")) {
						if(value.contains("=") && value.split("=")[0].trim().equalsIgnoreCase("REMARKS_EN")){
							remarks_en = value.split("=\"", 2)[1];
							if(!(remarks_en.length()<1500)){
								System.out.println("Please provide a valid remarks !" +CommonConstants.Messages.REMARKS_LIMIT_MESSAGE);
								return;
							}
						}else{
							System.out.println("ERROR: Syntax error !");
							return;
						}
					} else {
						if(value.contains("=") && value.split("=")[0].trim().equalsIgnoreCase("REMARKS_EN")){
							remarks_en = value.split("=", 2)[1];
							if(!(remarks_en.length()<1500)){
								System.out.println("Please provide a valid remarks !" +CommonConstants.Messages.REMARKS_LIMIT_MESSAGE);
								return;
							}
						}else{
							System.out.println("ERROR: Syntax error !");
							return;
						}
					}
					continue;
				}
				if ((value.startsWith("REMARKS_DE") || value.startsWith("remarks_de")) && !value.endsWith("=")) {
					if (value.contains("\"")) {
						if(value.contains("=") && value.split("=")[0].trim().equalsIgnoreCase("REMARKS_DE")){
							remarks_de = value.split("=\"", 2)[1];
							if(!(remarks_de.length()<1500)){
								System.out.println("Please provide a valid remarks !" +CommonConstants.Messages.REMARKS_LIMIT_MESSAGE);
								return;
							}
						}else{
							System.out.println("ERROR: Syntax error !");
							return;
						}
					} else {
						if(value.contains("=") && value.split("=")[0].trim().equalsIgnoreCase("REMARKS_DE")){
							remarks_de = value.split("=", 2)[1];
							if(!(remarks_de.length()<1500)){
								System.out.println("Please provide a valid remarks !" +CommonConstants.Messages.REMARKS_LIMIT_MESSAGE);
								return;
							}
						}else{
							System.out.println("ERROR: Syntax error !");
							return;
						}
					}
					continue;
				}
			}
			if(BatchUtil.isEmpty(siteName)){
				System.out.println("site name is required");
				return;
			} else if(BatchUtil.isEmpty(statusStr)){
				System.out.println("status is required");
				return;
			} else if(BatchUtil.isEmpty(iconStr)){
				System.out.println("icon is required");
				return;
			}
			
			IconRespWrapper iconRespWrapper = null;
			if(iconStr!=null){
				IconController iconController = new IconController();
				IkonRequest ikonRequest = new IkonRequest();
				ikonRequest.setIconName(iconStr);
				iconRespWrapper = iconController.getIconByName(ikonRequest);
				if(iconRespWrapper==null) return;
			}
			if (!BatchUtil.isEmpty(siteName)) {
				siteRequest.setName(siteName);
			}
			if (!BatchUtil.isEmpty(iconStr) && iconRespWrapper!=null) {
				siteRequest.setIconId(iconRespWrapper.getIconId());
			}
			if (!BatchUtil.isEmpty(statusStr)) {
				
				if(statusStr.equalsIgnoreCase("active") || statusStr.equalsIgnoreCase("inactive")){
					siteRequest.setStatus(statusStr.toUpperCase().equals("ACTIVE") ? Status.ACTIVE : Status.INACTIVE);
				} else{
					System.out.println("ERROR: Syntax error ! Status should be ACTIVE or INACTIVE");
					return;
				}
			}
			if (!BatchUtil.isEmpty(desc_en)) {
				siteTranslationTbl_en.setDescription(desc_en);
			}
			if (!BatchUtil.isEmpty(desc_de)) {
				siteTranslationTbl_de.setDescription(desc_de);
			}
			if (!BatchUtil.isEmpty(remarks_en)) {
				siteTranslationTbl_en.setRemarks(remarks_en);
			}
			if (!BatchUtil.isEmpty(remarks_de)) {
				siteTranslationTbl_de.setRemarks(remarks_de);
			}
			List<SiteTranslation> siteTranslationTblList = new ArrayList<SiteTranslation>();
			siteTranslationTblList.add(siteTranslationTbl_en);
			siteTranslationTblList.add(siteTranslationTbl_de);
			siteRequest.setSiteTranslations(siteTranslationTblList);
		}
			Boolean saveSite = siteController.saveSite(siteRequest);
			if (saveSite == null || saveSite == Boolean.FALSE) {
				return;
			} else {
				System.out.println("Site created Successfully");

			}
	}

	/**
	 * Method for Creates the rel object.
	 *
	 * @param createRelObjType
	 *            {@link ObjectRelationType}
	 * @param cmd
	 *            {@link CommandLine}
	 */
	public void createRelObject(ObjectRelationType createRelObjType, final CommandLine cmd) {
		Boolean addUserToProject = false;
		String userName = null, projectName = null, statusStr = "ACTIVE", relStringValue = "";
		String[] optionValues = cmd.getOptionValues("add");
		if (optionValues.length > 0) {
			relStringValue = BatchUtil.getInstance().getRelStringValue(optionValues, cmd, createRelObjType);
			if(relStringValue==null)return;
			String[] splittedStrVal = relStringValue.split("\"");
			String value;
			for (int i = 0; i < splittedStrVal.length; i++) {
				value = splittedStrVal[i];
				value = value.trim();
				if (value.split("=")[0].trim().equalsIgnoreCase("USER") && !value.endsWith("=")) {
					if (value.contains("=")) {
						userName = value.split("=", 2)[1];
					} else {
						System.out.println("ERROR: Syntax error !");
						return;
					}
					continue;
				} else if (value.split("=")[0].trim().equalsIgnoreCase("USER") && value.endsWith("=")) {
					userName="";
					continue;
				} else if(value.startsWith("USER") || value.startsWith("user")){
					System.out.println("ERROR: Syntax error !");
					return;
				}
				if (value.split("=")[0].trim().equalsIgnoreCase("PROJECT") && !value.endsWith("=")) {
					if (value.contains("=")) {
						projectName = value.split("=", 2)[1];
					} else {
						System.out.println("ERROR: Syntax error !");
						return;
					}
					continue;
				} else if (value.split("=")[0].trim().equalsIgnoreCase("PROJECT") && value.endsWith("=")) {
					projectName="";
					continue;
				} else if(value.startsWith("PROJECT") || value.startsWith("project")){
					System.out.println("ERROR: Syntax error !");
					return;
				} 
				
				
				if (value.split("=")[0].trim().equalsIgnoreCase("STATUS") && !value.endsWith("=")) {
					if (value.contains("=")) {
						statusStr = value.split("=", 2)[1];
						statusStr = statusStr.toUpperCase();
						if(!(statusStr.equalsIgnoreCase("active") || statusStr.equalsIgnoreCase("inactive"))){
							System.out.println("ERROR: Syntax error ! Status should be ACTIVE or INACTIVE");
							return;
						}
					} else {
						System.out.println("ERROR: Syntax error !");
						return;
					}
					continue;
				} else if (value.split("=")[0].trim().equalsIgnoreCase("STATUS") && value.endsWith("=")) {
					System.out.println("ERROR: Syntax error ! Status should be ACTIVE or INACTIVE");
					return;
				} else if(value.startsWith("STATUS") || value.startsWith("status")) {
					System.out.println("ERROR: Syntax error !");
					return;
				}
			}
			/*for (int index = 0; index < optionValues.length; index++) {
				String copyStrVal = optionValues[index];
				strValue = strValue.concat(copyStrVal);
			}
			int flag = 0;
			for (int index = 0; index < strValue.length(); index++) {
				char str = '\'';
				char charAt = strValue.charAt(index);
				if (charAt == str) {
					flag = flag + 1;
					if (flag >= 2 && flag % 2 == 0) {
						StringBuilder stb = new StringBuilder(strValue);
						stb.setCharAt(index, ' ');
						strValue = stb.toString().trim();
					}
				}
			}
			strValue = strValue.replace("'", "");*/
/*
			String[] splittedStrVal = strValue.split(" ");
			String value;
			for (int i = 0; i < splittedStrVal.length; i++) {
				value = splittedStrVal[i];
				if (value.contains("user=") || value.contains("USER=")) {
					userName = value.split("=")[1];
					continue;
				}
				if (value.contains("project=") || value.contains("PROJECT=")) {
					projectName = value.split("=")[1];
					continue;
				}
				if (value.contains("status=") || value.contains("STATUS=")) {
					statusStr = value.split("=")[1];
					statusStr = statusStr.toUpperCase();
					if(!(statusStr.equalsIgnoreCase("active") || statusStr.equalsIgnoreCase("inactive"))){
						System.out.println("ERROR: Syntax error ! Status should be ACTIVE or INACTIVE");
						return;
					}
					continue;
				}
			}*/
		}
		if(BatchUtil.isEmpty(userName)){
			System.out.println("user name is required");
			return;
		} else if(BatchUtil.isEmpty(projectName)){
			System.out.println("project name is required");
			return;
		}
		UserProjectRelRequest userProjectRelRequest = new UserProjectRelRequest();
		userProjectRelRequest.setUserName(userName);
		userProjectRelRequest.setProjectName(projectName);
		userProjectRelRequest.setStatus(statusStr);
		UserProjectRelController userProjRelCtrl = new UserProjectRelController();
			addUserToProject = userProjRelCtrl.addUserToProject(userProjectRelRequest);
			if (addUserToProject) {
				System.out.println("user_project relation created successfully");
			}
	}

	/**
	 * Method for Creates the user.
	 *
	 * @param cmd
	 *            {@link CommandLine}
	 * @throws CannotCreateObjectException
	 *             the cannot create object exception
	 * @throws UnauthorizedAccessException
	 *             the unauthorized access exception
	 * @throws JsonProcessingException
	 *             the json processing exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	private void createUser(final CommandLine cmd, ObjectType createObjType) {
		com.magna.batch.restclient.obj.users.UserController userController = new com.magna.batch.restclient.obj.users.UserController();
		UserRequest userRequest = new UserRequest();
		String userName = null, statusStr = null, iconStr = null, fullName = null, emailId = null, department = null,
				telephoneNumber = null, desc_en = null, desc_de = null, remarks_en = null, remarks_de = null,
				strVal = "";
		List<UserTranslation> userTranslation = new ArrayList<>();
		UserTranslation userTranslationTbl_en = new UserTranslation();
		userTranslationTbl_en.setLanguageCode(com.magna.batch.utils.LANG_ENUM.ENGLISH.getLangCode());
		userTranslation.add(userTranslationTbl_en);
		UserTranslation userTranslationTbl_de = new UserTranslation();
		userTranslationTbl_de.setLanguageCode(com.magna.batch.utils.LANG_ENUM.GERMAN.getLangCode());
		userTranslation.add(userTranslationTbl_de);
		userRequest.setUserTranslation(userTranslation);
		String[] optionValues = cmd.getOptionValues("create");
		if (optionValues.length > 0) {
			strVal = BatchUtil.getInstance().getStringValue(optionValues, cmd, createObjType);
			if(strVal==null)return;
			String[] splittedStrVal = strVal.split("\"");
			String value;
			for (int i = 0; i < splittedStrVal.length; i++) {
				value = splittedStrVal[i];
				value = value.replace("\"", "");
				value = value.trim();
				if ((value.startsWith("NAME") || value.startsWith("name")) && !value.endsWith("=")) {
					if(value.contains("=") && value.split("=")[0].trim().equalsIgnoreCase("NAME")){
						userName = value.split("=", 2)[1];
						if(!userName.matches(CommonConstants.RegularExpressions.ALLOWED_APP_NAME_REGEX)){
							System.out.println("Please provide a valid name !" +CommonConstants.Messages.APP_NAME_MESSAGE);
							return;
						} else if(!(userName.length()<30)){
							System.out.println("Please provide a valid name !" +CommonConstants.Messages.NAME_LIMIT_MESSAGE);
							return;
						}
					}else{
						System.out.println("ERROR: Syntax error !");
						return;
					}
					continue;
				}
				if ((value.startsWith("STATUS") || value.startsWith("status")) && !value.endsWith("=")) {
					if(value.contains("=") && value.split("=")[0].trim().equalsIgnoreCase("STATUS")){
						statusStr = value.split("=", 2)[1];
						statusStr = statusStr.toUpperCase();
						if(!(statusStr.equalsIgnoreCase("active") || statusStr.equalsIgnoreCase("inactive"))){
							System.out.println("ERROR: Syntax error ! Status should be ACTIVE or INACTIVE");
							return;
						}
					}else{
						System.out.println("ERROR: Syntax error !");
						return;
					}
					continue;
				} else if ((value.startsWith("STATUS") || value.startsWith("status")) && value.endsWith("=")){
						System.out.println("ERROR: Syntax error ! Status should be ACTIVE or INACTIVE");
						return;
				}
				if ((value.startsWith("ICON_NAME") || value.startsWith("icon_name")) && !value.endsWith("=")) {
					if(value.contains("=") && value.split("=")[0].trim().equalsIgnoreCase("ICON_NAME")){
						iconStr = value.split("=", 2)[1];
					}else{
						System.out.println("ERROR: Syntax error !");
						return;
					}
					continue;
				}
				if ((value.startsWith("FULL_NAME") || value.startsWith("full_name")) && !value.endsWith("=")) {
					if(value.contains("\"")){
						if(value.contains("=") && value.split("=")[0].trim().equalsIgnoreCase("FULL_NAME")){
							fullName = value.split("=\"", 2)[1];
							if(!(fullName.length()<30)){
								System.out.println("Please provide a valid full name !" +CommonConstants.Messages.NAME_LIMIT_MESSAGE);
								return;
							}
						}else{
							System.out.println("ERROR: Syntax error !");
							return;
						}
					}else{
						if(value.contains("=") && value.split("=")[0].trim().equalsIgnoreCase("FULL_NAME")){
							fullName = value.split("=", 2)[1];
							if(!(fullName.length()<30)){
								System.out.println("Please provide a valid full name !" +CommonConstants.Messages.NAME_LIMIT_MESSAGE);
								return;
							}
						}else{
							System.out.println("ERROR: Syntax error !");
							return;
						}
					}
					continue;
				}
				if ((value.startsWith("EMAIL_ID") ||value.startsWith("email_id")) && !value.endsWith("=")) {
					if(value.contains("=") && value.split("=")[0].trim().equalsIgnoreCase("EMAIL_ID")){
						emailId = value.split("=", 2)[1];
						if(!emailId.matches(CommonConstants.RegularExpressions.EMAIL_REGEX)){
							System.out.println("Please provide a valid email address ! " +CommonConstants.Messages.EMAIL_MESSAGE);
							return;
						}else if(!(emailId.length()<60)){
							System.out.println("Please provide a valid email address !" +CommonConstants.Messages.LIMIT_MESSAGE);
							return;
						}
					}else{
						System.out.println("ERROR: Syntax error !");
						return;
					}
					continue;
				}
				if ((value.startsWith("DEPARTMENT") || value.startsWith("department")) && !value.endsWith("=")) {
					if (value.contains("\"")) {
						if(value.contains("=") && value.split("=")[0].trim().equalsIgnoreCase("DEPARTMENT")){
							department = value.split("=\"", 2)[1];
							if(!(department.length()<60)){
								System.out.println("Please provide a valid department name !" +CommonConstants.Messages.LIMIT_MESSAGE);
								return;
							}
						}else{
							System.out.println("ERROR: Syntax error !");
							return;
						}
					} else {
						if(value.contains("=") && value.split("=")[0].trim().equalsIgnoreCase("DEPARTMENT")){
							department = value.split("=", 2)[1];
							if(!(department.length()<60)){
								System.out.println("Please provide a valid department name !" +CommonConstants.Messages.LIMIT_MESSAGE);
								return;
							}
						}else{
							System.out.println("ERROR: Syntax error !");
							return;
						}
					}
					continue;
				}
				if ((value.startsWith("TELEPHONE_NUMBER") || value.startsWith("telephone_number")) && !value.endsWith("=")) {
					if(value.contains("=") && value.split("=")[0].trim().equalsIgnoreCase("TELEPHONE_NUMBER")){
						telephoneNumber = value.split("=", 2)[1];
					}else{
						if(!(telephoneNumber.length()<60)){
							System.out.println("Please provide a valid telephone number !" +CommonConstants.Messages.LIMIT_MESSAGE);
							return;
						}
						System.out.println("ERROR: Syntax error !");
						return;
					}
					continue;
				}
				if ((value.startsWith("DESCRIPTION_EN") || value.startsWith("description_en")) && !value.endsWith("=")) {
					if (value.contains("\"")) {
						if(value.contains("=") && value.split("=")[0].trim().equalsIgnoreCase("DESCRIPTION_EN")){
							desc_en = value.split("=\"", 2)[1];
							if(!(desc_en.length()<240)){
								System.out.println("Please provide a valid description !" +CommonConstants.Messages.DESC_LIMIT_MESSAGE);
								return;
							}
						}else{
							System.out.println("ERROR: Syntax error !");
							return;
						}
					} else {
						if(value.contains("=") && value.split("=")[0].trim().equalsIgnoreCase("DESCRIPTION_EN")){
							desc_en = value.split("=", 2)[1];
							if(!(desc_en.length()<240)){
								System.out.println("Please provide a valid description !" +CommonConstants.Messages.DESC_LIMIT_MESSAGE);
								return;
							}
						}else{
							System.out.println("ERROR: Syntax error !");
							return;
						}
					}
					continue;
				}
				if ((value.startsWith("DESCRIPTION_DE") || value.startsWith("description_de")) && !value.endsWith("=")) {
					if (value.contains("\"")) {
						if(value.contains("=") && value.split("=")[0].trim().equalsIgnoreCase("DESCRIPTION_DE")){
							desc_de = value.split("=\"", 2)[1];
							if(!(desc_de.length()<240)){
								System.out.println("Please provide a valid description !" +CommonConstants.Messages.DESC_LIMIT_MESSAGE);
								return;
							}
						}else{
							System.out.println("ERROR: Syntax error !");
							return;
						}
					} else {
						if(value.contains("=") && value.split("=")[0].trim().equalsIgnoreCase("DESCRIPTION_DE")){
							desc_de = value.split("=", 2)[1];
							if(!(desc_de.length()<240)){
								System.out.println("Please provide a valid description !" +CommonConstants.Messages.DESC_LIMIT_MESSAGE);
								return;
							}
						}else{
							System.out.println("ERROR: Syntax error !");
							return;
						}
					}
					continue;
				}
				if ((value.startsWith("REMARKS_EN") || value.startsWith("remarks_en")) && !value.endsWith("=")) {
					if (value.contains("\"")) {
						if(value.contains("=") && value.split("=")[0].trim().equalsIgnoreCase("REMARKS_EN")){
							remarks_en = value.split("=\"", 2)[1];
							if(!(remarks_en.length()<1500)){
								System.out.println("Please provide a valid remarks !" +CommonConstants.Messages.REMARKS_LIMIT_MESSAGE);
								return;
							}
						}else{
							System.out.println("ERROR: Syntax error !");
							return;
						}
					} else {
						if(value.contains("=") && value.split("=")[0].trim().equalsIgnoreCase("REMARKS_EN")){
							remarks_en = value.split("=", 2)[1];
							if(!(remarks_en.length()<1500)){
								System.out.println("Please provide a valid remarks !" +CommonConstants.Messages.REMARKS_LIMIT_MESSAGE);
								return;
							}
						}else{
							System.out.println("ERROR: Syntax error !");
							return;
						}
					}
					continue;
				}
				if ((value.startsWith("REMARKS_DE") || value.startsWith("remarks_de")) && !value.endsWith("=")) {
					if (value.contains("\"")) {
						if(value.contains("=") && value.split("=")[0].trim().equalsIgnoreCase("REMARKS_DE")){
							remarks_de = value.split("=\"", 2)[1];
							if(!(remarks_de.length()<1500)){
								System.out.println("Please provide a valid remarks !" +CommonConstants.Messages.REMARKS_LIMIT_MESSAGE);
								return;
							}
						}else{
							System.out.println("ERROR: Syntax error !");
							return;
						}
					} else {
						if(value.contains("=") && value.split("=")[0].trim().equalsIgnoreCase("REMARKS_DE")){
							remarks_de = value.split("=", 2)[1];
							if(!(remarks_de.length()<1500)){
								System.out.println("Please provide a valid remarks !" +CommonConstants.Messages.REMARKS_LIMIT_MESSAGE);
								return;
							}
						}else{
							System.out.println("ERROR: Syntax error !");
							return;
						}
					}
					continue;
				}
			}
			
			if(BatchUtil.isEmpty(userName)){
				System.out.println("user name is required");
				return;
			} else if(BatchUtil.isEmpty(statusStr)){
				System.out.println("status is required");
				return;
			} else if(BatchUtil.isEmpty(iconStr)){
				System.out.println("icon is required");
				return;
			}
			
			IconRespWrapper iconRespWrapper = null;
			if(iconStr!=null){
				IconController iconController = new IconController();
				IkonRequest ikonRequest = new IkonRequest();
				ikonRequest.setIconName(iconStr);
				iconRespWrapper = iconController.getIconByName(ikonRequest);
				if(iconRespWrapper==null) return;
			}
			if (!BatchUtil.isEmpty(userName)) {
				userRequest.setUserName(userName);
			}
			if (!BatchUtil.isEmpty(iconStr) && iconRespWrapper!=null) {
				userRequest.setIconId(iconRespWrapper.getIconId());
			}
			if (!BatchUtil.isEmpty(statusStr)) {
				if(statusStr.equalsIgnoreCase("active") || statusStr.equalsIgnoreCase("inactive")){
					userRequest.setStatus(statusStr.toUpperCase().equals("ACTIVE") ? Status.ACTIVE : Status.INACTIVE);
				} else{
					System.out.println("ERROR: Syntax error ! Status should be ACTIVE or INACTIVE");
					return;
				}
			}
			if (!BatchUtil.isEmpty(fullName)) {
				userRequest.setFullName(fullName);
			}
			if (!BatchUtil.isEmpty(emailId)) {
				userRequest.setEmail(emailId);
			}
			if (!BatchUtil.isEmpty(department)) {
				userRequest.setDepartment(department);
			}
			if (!BatchUtil.isEmpty(telephoneNumber)) {
				userRequest.setTelephoneNumber(telephoneNumber);
			}

			if (!BatchUtil.isEmpty(desc_en)) {
				userTranslationTbl_en.setDescription(desc_en);
			}
			if (!BatchUtil.isEmpty(remarks_en)) {
				userTranslationTbl_en.setRemarks(remarks_en);
			}
			if (!BatchUtil.isEmpty(desc_de)) {
				userTranslationTbl_de.setDescription(desc_de);
			}
			if (!BatchUtil.isEmpty(remarks_de)) {
				userTranslationTbl_de.setRemarks(remarks_de);
			}
			List<UserTranslation> userTranslationTblList = new ArrayList<UserTranslation>();
			userTranslationTblList.add(userTranslationTbl_en);
			userTranslationTblList.add(userTranslationTbl_de);
			userRequest.setUserTranslation(userTranslationTblList);

			Boolean saveUser = userController.saveUser(userRequest);
			if (saveUser == null || saveUser == Boolean.FALSE) {
				return;
			} else {
				System.out.println("User created Successfully");

			}
		}
	}
}
