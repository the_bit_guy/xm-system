package com.magna.batch.restclient.obj.users;

import java.io.IOException;

import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.magna.batch.restclient.BatchRestController;
import com.magna.batch.restclient.exception.UnauthorizedAccessException;
import com.magna.batch.utils.RestClientConstants;
import com.magna.xmbackend.vo.user.UserRequest;

/**
 * The Class UserController.
 * 
 * @author shashwat.anand
 */
public class UserController extends BatchRestController {
	
	/** The Constant LOGGER. */
	/*private static final Logger LOGGER = LoggerFactory.getLogger(UserController.class);*/

	/**
	 * Constructor for UserController Class.
	 */
	public UserController() {
		super();
	}
	
	/**
	 * Method for Save user.
	 *
	 * @param userRequest {@link UserRequest}
	 * @return the boolean {@link Boolean}
	 */
	public Boolean saveUser(UserRequest userRequest) {
		try {
			String url = new String(this.serviceUrl + RestClientConstants.CREATE_USER);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<UserRequest> request = new HttpEntity<UserRequest>(userRequest, this.headers);
			ResponseEntity<Boolean> responseEntity = restTemplate.exchange(url, HttpMethod.POST,request, new ParameterizedTypeReference<Boolean>() {});
			HttpStatus statusCode = responseEntity.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				return responseEntity.getBody();
			} else {
				//LOGGER.error("Error while calling XMBATCH Create User REST Service, returns the status code: ", //$NON-NLS-1$
						 //+ statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException)e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}else if (e instanceof HttpStatusCodeException) {
					String errorResponse = ((HttpStatusCodeException) e).getResponseBodyAsString();
					ObjectMapper mapper = new ObjectMapper();
					JsonNode jsonNode = null;
					try {
						jsonNode = mapper.readTree(errorResponse);
					} catch (IOException e1) {
						System.out.println("ERROR: Server Error! Unable to Create User");
						return false;
					}
					String expMsg = null;
					if(jsonNode.get("lingualMessages") != null) {
						expMsg = jsonNode.get("lingualMessages").get("en").textValue();
					} else if(jsonNode.get("description") != null) {
						expMsg = jsonNode.get("description").asText();
					}
					System.out.println(expMsg);
					return false;
					
				} 
			} 
		} catch (Exception e) {
			//LOGGER.error("Error while calling XMBATCH Create User REST Service", e); //$NON-NLS-1$
		}
		return null;
	}
	
	/*
	public UsersTbl getUserByName(final String uName) {
		try {
			UserRequest userRequest = new UserRequest();
			userRequest.setUserName(uName);
			String url = new String(this.serviceUrl + RestClientConstants.GET_USER_BY_NAME);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<UserRequest> requestEntity = new HttpEntity<UserRequest>(userRequest, headers);
			List<HttpMessageConverter<?>> messageConverters = new ArrayList<>();
			messageConverters.add(new MappingJackson2HttpMessageConverter());
			restTemplate.setMessageConverters(messageConverters);

			ResponseEntity<UsersTbl> responseList = restTemplate.exchange(url, HttpMethod.POST, requestEntity,
					UsersTbl.class);
			HttpStatus statusCode = responseList.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				UsersTbl usersTbl = responseList.getBody();
				return usersTbl;
			} else {
				LOGGER.error("Error while calling XMBATCH Get Users By User Name REST Service, returns the status code: ", //$NON-NLS-1$
						statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException)e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			} else if (e instanceof HttpStatusCodeException) {
				RestClientExpHandlerUtil.handleNoXMLObjectFoundException(LOGGER, (HttpStatusCodeException) e);
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling XMBATCH Get Users By User Name REST Service", e); //$NON-NLS-1$
		}
		return null;
	}*/
	
	
	/**
	 * Method for Delete user.
	 *
	 * @param uName {@link String}
	 * @return true, if successful
	 */
	public boolean deleteUser(String uName) {
		try {
			String url = new String(this.serviceUrl + RestClientConstants.DELETE_USER);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<String> requestEntity = new HttpEntity<String>(uName, this.headers);
			ResponseEntity<Boolean> responseEntity = restTemplate.exchange(url, HttpMethod.POST, requestEntity,
					Boolean.class);
			HttpStatus statusCode = responseEntity.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				return responseEntity.getBody();
			} else {
				//LOGGER.error("Error while calling XMBATCH delete User REST Service, returns the status code: ", //$NON-NLS-1$
					//	statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException)e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				} else if (e instanceof HttpStatusCodeException) {
					String errorResponse = ((HttpStatusCodeException) e).getResponseBodyAsString();
					ObjectMapper mapper = new ObjectMapper();
					JsonNode jsonNode = null;
					try {
						jsonNode = mapper.readTree(errorResponse);
					} catch (IOException e1) {
						System.out.println("ERROR: Server Error! Unable to Delete User");
						return false;
					}
					String expMsg = jsonNode.get("lingualMessages").get("en").textValue();
					System.out.println(expMsg);
					return false;
				}
			} 
		}catch (Exception e) {
			//LOGGER.error("Error while calling XMBATCH delete User REST Service!", e); //$NON-NLS-1$
		}
		return false;
	}
	
	/**
	 * Method for Update user.
	 *
	 * @param userRequest {@link UserRequest}
	 * @return the boolean {@link Boolean}
	 */
	public Boolean updateUser(UserRequest userRequest) {
		try {
			String url = new String(this.serviceUrl + RestClientConstants.UPDATE_USER);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<UserRequest> request = new HttpEntity<UserRequest>(userRequest, this.headers);
			ResponseEntity<Boolean> responseEntity = restTemplate.exchange(url, HttpMethod.POST,request, new ParameterizedTypeReference<Boolean>() {});
			HttpStatus statusCode = responseEntity.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				return responseEntity.getBody();
			} else {
				//LOGGER.error("Error while calling XMBATCH Update User REST Service, returns the status code: ", //$NON-NLS-1$
						//statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException)e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}else if (e instanceof HttpStatusCodeException) {
					String errorResponse = ((HttpStatusCodeException) e).getResponseBodyAsString();
					ObjectMapper mapper = new ObjectMapper();
					JsonNode jsonNode = null;
					try {
						jsonNode = mapper.readTree(errorResponse);
					} catch (IOException e1) {
						System.out.println("ERROR: Server Error! Unable to Update User");
						return false;
					}
					String expMsg = null;
					if(jsonNode.get("lingualMessages") != null) {
						expMsg = jsonNode.get("lingualMessages").get("en").textValue();
					} else if(jsonNode.get("description") != null) {
						expMsg = jsonNode.get("description").asText();
					}
					System.out.println(expMsg);
					return false;
				} 
			} 
		} catch (Exception e) {
			//LOGGER.error("Error while calling XMBATCH Update User REST Service", e); //$NON-NLS-1$
		}
		return null;
	}
}
