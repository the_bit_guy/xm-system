package com.magna.batch.model;

/**
 * Class for Admin area data.
 *
 * @author Roshan.Ekka
 */
public class AdminAreaData {

	/** The admin area name. */
	private String name;
	
	/** The singleton app timeout. */
	private Integer singleton_app_timeout;
	
	/** The hotline contact number. */
	private String hotline_contact_number;
	
	/** The hotline contact email. */
	private String hotline_contact_email;
	
	/** The status. */
	private String status;
	
	/** The description. */
	private String description;
	
	/** The remarks. */
	private String remarks;
	
	/** The lang code. */
	private String lang_code;
	
	/** The icon name. */
	private String iconName;

	/**
	 * Instantiates a new user data.
	 */
	public AdminAreaData() {
		super();

	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	
	/**
	 * @return the singleton_app_timeout
	 */
	public Integer getSingleton_app_timeout() {
		return singleton_app_timeout;
	}

	/**
	 * @param singleton_app_timeout the singleton_app_timeout to set
	 */
	public void setSingleton_app_timeout(Integer singleton_app_timeout) {
		this.singleton_app_timeout = singleton_app_timeout;
	}

	/**
	 * @return the hotline_contact_number
	 */
	public String getHotline_contact_number() {
		return hotline_contact_number;
	}

	/**
	 * @param hotline_contact_number the hotline_contact_number to set
	 */
	public void setHotline_contact_number(String hotline_contact_number) {
		this.hotline_contact_number = hotline_contact_number;
	}

	/**
	 * @return the hotline_contact_email
	 */
	public String getHotline_contact_email() {
		return hotline_contact_email;
	}

	/**
	 * @param hotline_contact_email the hotline_contact_email to set
	 */
	public void setHotline_contact_email(String hotline_contact_email) {
		this.hotline_contact_email = hotline_contact_email;
	}

	/**
	 * Gets the status.
	 *
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * Sets the status.
	 *
	 * @param status the new status
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * Gets the description.
	 *
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Sets the description.
	 *
	 * @param description the new description
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * Gets the remarks.
	 *
	 * @return the remarks
	 */
	public String getRemarks() {
		return remarks;
	}

	/**
	 * Sets the remarks.
	 *
	 * @param remarks the new remarks
	 */
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	/**
	 * Gets the lang code.
	 *
	 * @return the lang code
	 */
	public String getLang_code() {
		return lang_code;
	}

	/**
	 * Sets the lang code.
	 *
	 * @param lang_code the new lang code
	 */
	public void setLang_code(String lang_code) {
		this.lang_code = lang_code;
	}

	/**
	 * Gets the icon name.
	 *
	 * @return the icon name
	 */
	public String getIconName() {
		return iconName;
	}

	/**
	 * Sets the icon name.
	 *
	 * @param iconName the new icon name
	 */
	public void setIconName(String iconName) {
		this.iconName = iconName;
	}
}
