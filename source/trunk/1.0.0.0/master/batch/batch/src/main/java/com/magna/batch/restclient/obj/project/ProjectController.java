package com.magna.batch.restclient.obj.project;

import java.io.IOException;

import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.magna.batch.restclient.BatchRestController;
import com.magna.batch.restclient.exception.UnauthorizedAccessException;
import com.magna.batch.utils.RestClientConstants;
import com.magna.xmbackend.vo.project.ProjectRequest;


/**
 * Class for Project controller.
 *
 * @author Roshan.Ekka
 */
public class ProjectController extends BatchRestController {

	/** The Constant LOGGER. */
	/*private static final Logger LOGGER = LoggerFactory.getLogger(ProjectController.class);*/
	
	/**
	 * Constructor for ProjectController Class.
	 */
	public ProjectController() {
		super();
	}
	
	/**
	 * Method for Save project.
	 *
	 * @param projectRequest {@link ProjectRequest}
	 * @return the boolean {@link Boolean}
	 */
	public Boolean saveProject(ProjectRequest projectRequest) {
		try {
			String url = new String(this.serviceUrl + RestClientConstants.CREATE_PROJECT);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<ProjectRequest> request = new HttpEntity<ProjectRequest>(projectRequest, this.headers);
			ResponseEntity<Boolean> responseEntity = restTemplate.exchange(url, HttpMethod.POST,request, new ParameterizedTypeReference<Boolean>() {});
			HttpStatus statusCode = responseEntity.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				return responseEntity.getBody();
			} else {
				//LOGGER.error("Error while calling XMBATCH Create Project REST Service, returns the status code: ", //$NON-NLS-1$
						//statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException)e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				} else if (e instanceof HttpStatusCodeException) {
					String errorResponse = ((HttpStatusCodeException) e).getResponseBodyAsString();
					ObjectMapper mapper = new ObjectMapper();
					JsonNode jsonNode;
					try {
						jsonNode = mapper.readTree(errorResponse);
					} catch (IOException e1) {
						System.out.println("ERROR: Server Error! Unable to Create Project");
						return false;
					}
					String expMsg = null;
					if(jsonNode.get("lingualMessages") != null) {
						expMsg = jsonNode.get("lingualMessages").get("en").textValue();
					} else if(jsonNode.get("description") != null) {
						expMsg = jsonNode.get("description").asText();
					}
					System.out.println(expMsg);
					return false;
				} 
			} 
		} catch (Exception e) {
			//LOGGER.error("Error while calling XMBATCH Create Project REST Service", e); //$NON-NLS-1$
		}
		return null;
	}
	
	/**
	 * Method for Delete project.
	 *
	 * @param name {@link String}
	 * @return true, if successful
	 */
	public boolean deleteProject(String name) {
		try {
			String url = new String(this.serviceUrl + RestClientConstants.DELETE_PROJECT);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<String> requestEntity = new HttpEntity<String>(name, this.headers);
			ResponseEntity<Boolean> responseEntity = restTemplate.exchange(url, HttpMethod.POST, requestEntity,
					Boolean.class);
			HttpStatus statusCode = responseEntity.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				return responseEntity.getBody();
			} else {
				//LOGGER.error("Error while calling XMBATCH Delete Project REST Service, returns the status code: ", //$NON-NLS-1$
						//statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException)e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				} else if (e instanceof HttpStatusCodeException) {
					String errorResponse = ((HttpStatusCodeException) e).getResponseBodyAsString();
					ObjectMapper mapper = new ObjectMapper();
					JsonNode jsonNode;
					try {
						jsonNode = mapper.readTree(errorResponse);
					} catch (IOException e1) {
						System.out.println("ERROR: Server Error! Unable to Delete Project");
						return false;
					}
					String expMsg = null;
					if(jsonNode.get("lingualMessages") != null) {
						expMsg = jsonNode.get("lingualMessages").get("en").textValue();
					} else if(jsonNode.get("description") != null) {
						expMsg = jsonNode.get("description").asText();
					}
					System.out.println(expMsg);
					return false;
				} 
			} 
		} catch (Exception e) {
			//LOGGER.error("Error while calling XMBATCH Delete Project REST Service!", e); //$NON-NLS-1$
		}
		return false;
	}
	
	/**
	 * Method for Update project.
	 *
	 * @param projectRequest {@link ProjectRequest}
	 * @return the boolean {@link Boolean}
	 */
	public Boolean updateProject(ProjectRequest projectRequest) {
		try {
			String url = new String(this.serviceUrl + RestClientConstants.UPDATE_PROJECT);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<ProjectRequest> request = new HttpEntity<ProjectRequest>(projectRequest, this.headers);
			ResponseEntity<Boolean> responseEntity = restTemplate.exchange(url, HttpMethod.POST,request, new ParameterizedTypeReference<Boolean>() {});
			HttpStatus statusCode = responseEntity.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				return responseEntity.getBody();
			} else {
				//LOGGER.error("Error while calling XMBATCH Update Project REST Service, returns the status code: ", //$NON-NLS-1$
						//statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException)e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				} else if (e instanceof HttpStatusCodeException) {
					String errorResponse = ((HttpStatusCodeException) e).getResponseBodyAsString();
					ObjectMapper mapper = new ObjectMapper();
					JsonNode jsonNode = null;
					try {
						jsonNode = mapper.readTree(errorResponse);
					} catch (IOException e1) {
						System.out.println("ERROR: Server Error! Unable to Update Project");
						return false;
					}
					String expMsg = null;
					if(jsonNode.get("lingualMessages") != null) {
						expMsg = jsonNode.get("lingualMessages").get("en").textValue();
					} else if(jsonNode.get("description") != null) {
						expMsg = jsonNode.get("description").asText();
					}
					System.out.println(expMsg);
					return false;
				} 
			} 
		} catch (Exception e) {
			//LOGGER.error("Error while calling XMBATCH Update Project REST Service", e); //$NON-NLS-1$
		}
		return null;
	}
}
