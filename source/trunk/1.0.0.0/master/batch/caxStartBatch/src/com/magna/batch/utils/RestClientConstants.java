package com.magna.batch.utils;

// TODO: Auto-generated Javadoc
/**
 * The Interface RestClientConstants.
 */
public interface  RestClientConstants {
	
	/**  SUCCESS constant. */
	String SUCCESS = "Success";

	/** ACCEPT_HEADER constants. */
	String ACCEPT_HEADER = "Accept";

	/** APPLICATION_JSON constants. */
	String APPLICATION_JSON = "application/json";

	/** CONTENT_TYPE constants. */
	String CONTENT_TYPE = "Content-Type";

	/** GET constant. */
	String GET = "GET";

	/** POST constant. */
	String POST = "POST";

	/** PUT constant. */
	String PUT = "PUT";

	/** DELETE constant. */
	String DELETE = "DELETE";
	
	/** GET_USERS constant. */
	String GET_USERS = "/user/findAll";
	
	/** GET_USER_BY_NAME constant. */
	String GET_USER_BY_NAME = "/user/findByName";

	/** GET_USERS_BY_PROJECT_ID constant. */
	String GET_USERS_BY_PROJECT_ID = "/user/findUsersByProjectId";

	/** GET_USERS_BY_USER_APP_ID constant. */
	String GET_USERS_BY_USER_APP_ID = "/user/findUsersByUserAppId";

	/** GET_USERS_BY_PROJECT_APP_ID constant. */
	String GET_USERS_BY_PROJECT_APP_ID = "/user/findUsersByProjectAppId";

	/** GET_USERS_BY_START_APP_ID constant. */
	String GET_USERS_BY_START_APP_ID = "/user/findUsersByStartAppId";
	
	/** The create user. */
	String CREATE_USER = "/user/caxstartbatch/save";
	
	/** GET_ICON_BY_NAME constant. */
	String GET_ICONID_BY_NAME = "/icon/findIconIdByName";

	/** Delete user Constant. */
	String DELETE_USER = "/user/caxstartbatch/delete";
	
	/** The String. */
	String ADD_USER_TO_PROJECT = "/userProjectRel/caxstartbatch/save";
	
	/** The remove user from project. */
	String REMOVE_USER_FROM_PROJECT = "/userProjectRel/caxstartbatch/delete";
	
	/** The remove user from project. */
	String GET_RESULTS_FROM_QUERY = "/xmbatch/findResultSet";
	
	String GET_AUTHENTICATION_RESPONSE = "/auth/login";
}
