package com.magna.batch;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Scanner;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.client.ResourceAccessException;

import com.magna.batch.restclient.XMBatchController;
import com.magna.batch.restclient.authorization.AuthController;
import com.magna.batch.restclient.exception.UnauthorizedAccessException;
import com.magna.batch.restclient.icons.IconController;
import com.magna.batch.restclient.obj.users.UserController;
import com.magna.batch.restclient.rel.userproject.UserProjectRelController;
import com.magna.batch.utils.BatchConstants;
import com.magna.batch.utils.BatchUtil;
import com.magna.batch.utils.CreateExcell;
import com.magna.batch.utils.LANG_ENUM;
import com.magna.batch.utils.RelatonData;
import com.magna.batch.utils.SQLConverter;
import com.magna.batch.utils.UserData;
import com.magna.batch.utils.XmSystemEnvProcess;
import com.magna.xmbackend.exception.CannotCreateObjectException;
import com.magna.xmbackend.vo.enums.Application;
import com.magna.xmbackend.vo.enums.Status;
import com.magna.xmbackend.vo.icon.IconRespWrapper;
import com.magna.xmbackend.vo.icon.IkonRequest;
import com.magna.xmbackend.vo.rel.UserProjectRelRequest;
import com.magna.xmbackend.vo.user.AuthResponse;
import com.magna.xmbackend.vo.user.UserRequest;
import com.magna.xmbackend.vo.user.UserTranslation;
import com.magna.xmbackend.xmbatch.XmbatchResponse;

/**
 * The Class CaxStartBatchMain.
 * 
 * @author shashwat.anand
 */
public class CaxStartBatchMain {

	/** The Constant LOGGER. */
	private static final Logger LOGGER = LoggerFactory.getLogger(CaxStartBatchMain.class);

	/**
	 * Instantiates a new cax start batch main.
	 * 
	 * @param args
	 */
	public CaxStartBatchMain(String[] args) {
		if (args.length == 0) {
			System.out.println("ERROR: Invalid Syntax ! use -help option for Help");
			return;
		}
		processCommandLine(args);
	}

	/*
	 * Help.
	 */
	public void help() {
		// if system language is in German then show help in German
		final Locale local = Locale.getDefault();
		String filePath = BatchConstants.MessageBundle_US_PATH;
		if (local.getLanguage().equals(LANG_ENUM.GERMAN.getLangCode())) {
			filePath = BatchConstants.MessageBundle_DE_PATH;
		}
		try {
			final File file = new File(filePath);
			Scanner scanner = new Scanner(file); 
			while (scanner.hasNextLine()) {
				System.out.println(scanner.nextLine().split(":=")[1]);
			}
			scanner.close();
		} catch (FileNotFoundException e) {
			System.out.println("ERROR: help file not found");
			LOGGER.error("ERROR while opning help atribute file !" + e);
		}
	}

	/**
	 * Process command line.
	 *
	 * @param args
	 *            the args
	 */
	private void processCommandLine(final String[] args) {
		try {
			final List<String> argsList = Arrays.asList(args);
			String usrName = null, usrPasswd = null;
			if (argsList.contains("-help")) {
				help();
				return;
			}
			if (argsList.contains("-u")) {
				int indexOfOptionUser = argsList.indexOf("-u");
				usrName = argsList.get(indexOfOptionUser + 1);
				if (usrName.isEmpty() || usrName == null || usrName.startsWith("-")) {
					System.out.println("ERROR: Username Not Provided");
					return;
				}
				if (argsList.contains("-p")) {
					int indexOfOptionPasswd = argsList.indexOf("-p");
					usrPasswd = argsList.get(indexOfOptionPasswd + 1);

					if (usrPasswd.isEmpty() || usrPasswd == null || usrPasswd.startsWith("-")) {
						System.out.println("ERROR: Password Not Provided");
						return;
					}

					boolean isValidUser = false;
					String errorMessage = null;

					AuthController authController = new AuthController();
					AuthResponse authorizeLogin = authController.authorizeLogin(usrName, usrPasswd,
							Application.CAX_START_BATCH.name());
					if (authorizeLogin != null) {
						isValidUser = authorizeLogin.isValidUser();
						errorMessage = authorizeLogin.getMessage();
					}

					if (!isValidUser) {
						// Throw an error "errorMessage" and exit the whole
						// system
						if (errorMessage == null) {
							System.out.println("Invalid User");
							return;
						}
						System.out.println("Error: " + errorMessage);
						return;
					}
					if (argsList.contains("-type")) {
						int indexOfType = argsList.indexOf("-type");
						if (argsList.contains("-genexcel")) {
							int indexOfGenEx = argsList.indexOf("-type");
							if (indexOfGenEx > 4 && indexOfType > 6) {
								System.out.println("Error : Invalid syntax, -type argument is not at correct place.");
								return;
							}
						} else {
							if (indexOfType > 4) {
								System.out.println("Error : Invalid syntax, -type argument is not at correct place.");
								return;
							}
						}
						String operationType = argsList.get(indexOfType + 1);
						switch (operationType) {
						case "base_data":
							processBaseData(argsList);
							break;
						case "relation":
							processRelation(argsList);
							break;
						default:
							help();
							break;
						}
					} else {
						System.out.println("ERROR: Invalid Syntax ! use -help option for Help");
					}
				} else {
					System.out.println("ERROR: Password not provided");
				}
			} else {
				System.out.println("ERROR: Username not provided");
			}
		} catch (Exception e) {
			if (e instanceof ResourceAccessException) {
				System.out.println("ERROR: XMBackend server not reachable. Please contact your administrator");
				return;
			}
			System.out.println("Exception: Please contact your administrator : " + e);
		}
	}

	/**
	 * Process base data.
	 *
	 * @param argsList
	 *            the args list
	 */
	private void processBaseData(final List<String> argsList) {
		if (argsList.contains("-select")) {
			// select query listing
			try {
				String sqlQueryStr = SQLConverter.convertToSql(argsList);
				// System.out.println(sqlQueryStr);
				if (sqlQueryStr == null || sqlQueryStr.isEmpty()) {
					System.out.println("ERROR: Syntax error ! use -help option for Help ");
					return;
				}

				XMBatchController batchController = new XMBatchController();
				XmbatchResponse resultSet = batchController.getResultSet(sqlQueryStr);
				if (resultSet == null) {
					return;
				}

				List<Map<String, Object>> queryResultSet = resultSet.getQueryResultSet();
				if (queryResultSet == null) {
					// Show unable query
					System.out.println("ERROR: query failed\n");
					return;
				} else {
					// query succeeded
					if (queryResultSet.isEmpty()) {
						System.out.println("No Result found");
						return;
					}
					if (argsList.contains("-genexcel")) {

						int optionfname = argsList.indexOf("-genexcel");

						if (argsList.get(optionfname + 1).equals("-type")) {
							// no path provided
							// create exportExcell in src/resource
							CreateExcell.createExcellforUserCmds(queryResultSet, "users",
									"src/resource/exportExcel_1.xlsx");
							return;
						} else if (argsList.get(optionfname + 1).contains(":/")) {
							String path = argsList.get(optionfname + 1);
							// only path provided
							if (!path.contains(".")) {
								// create exportExcell in "path/exportExcel"
								String filepath = path + "/exportExcel_1.xlsx";
								CreateExcell.createExcellforUserCmds(queryResultSet, "users", filepath);
								return;
							}
						}
						if ((argsList.get(optionfname + 1).contains(".xls")
								|| argsList.get(optionfname + 1).contains(".xlsx")
								|| argsList.get(optionfname + 1).contains(".csv"))) {

							String optname = argsList.get(optionfname + 1);
							if (optname.split("\\.")[1].equals("xls") || optname.split("\\.")[1].equals("xlsx")
									|| optname.split("\\.")[1].equals("csv")) {

								// filename and path provided
								// create excell in "path"
								String fpath = argsList.get(optionfname + 1);
								CreateExcell.createExcellforUserCmds(queryResultSet, "users", fpath);
								return;
							} else {
								System.out.println("Invalid file name provided");
								return;
							}
						} else {

							System.out.println("No path or Incorrect file name provided ");
							return;
						}

					} else {
						// list entries
						List<Map<String, Object>> userlist = queryResultSet;
						if (argsList.contains("DISTINCT")) {
							display(userlist, "userdistinct");
						} else {
							display(userlist, "user");
						}
					}
				}

			} catch (CannotCreateObjectException e) {
				// Show Unable to create object as User already exists
				System.out.println("ERROR: Failed to query User ");
				LOGGER.error("Error while calling  userController !", e);
			} catch (UnauthorizedAccessException e) {
				// Show current user is not authorized
				System.out.println(e.getMessage());
				LOGGER.error("Error while calling   batch Controller !", e);
			}
			// create a user
		} else if (argsList.contains("-create")) {
			int indexOfCreate = argsList.indexOf("-create");
			String objectType = argsList.get(indexOfCreate + 1).toUpperCase();
			switch (objectType) {
			case "USER":
				int indexOfUser;
				UserRequest userRequest;
				UserTranslation userTranslationTbl_en = new UserTranslation();
				userTranslationTbl_en.setLanguageCode(com.magna.batch.utils.LANG_ENUM.ENGLISH.getLangCode());
				UserTranslation userTranslationTbl_de = new UserTranslation();
				userTranslationTbl_de.setLanguageCode(com.magna.batch.utils.LANG_ENUM.GERMAN.getLangCode());
				try {
					String userName = null, statusStr = null, iconStr = null, fullName = null, emailId = null,
							department = null, telephoneNumber = null, desc_en = null, desc_de = null,
							remarks_en = null, remarks_de = null;

					indexOfUser = argsList.indexOf("user");
					for (int index = indexOfUser + 1; index < argsList.size(); index++) {
						String element = argsList.get(index);
						if (element.startsWith("name") && !element.endsWith("=")) {
							userName = element.split("=")[1];
							continue;
						}
						if (element.startsWith("status") && !element.endsWith("=")) {
							statusStr = element.split("=")[1];
							continue;
						}
						if (element.startsWith("icon") && !element.endsWith("=")) {
							iconStr = element.split("=")[1];
							continue;
						}
						if (element.startsWith("fullname") && !element.endsWith("=")) {
							fullName = element.split("=")[1];
							continue;
						}
						if (element.startsWith("emailid") && !element.endsWith("=")) {
							emailId = element.split("=")[1];
							continue;
						}
						if (element.startsWith("department") && !element.endsWith("=")) {
							department = element.split("=")[1];
							continue;
						}
						if (element.startsWith("telephoneNumber") && !element.endsWith("=")) {
							telephoneNumber = element.split("=")[1];
							continue;
						}
						if (element.startsWith("description_en") && !element.endsWith("=")) {
							desc_en = element.split("=")[1];
							continue;
						}
						if (element.startsWith("description_de") && !element.endsWith("=")) {
							desc_de = element.split("=")[1];
							continue;
						}
						if (element.startsWith("remarks_en") && !element.endsWith("=")) {
							remarks_en = element.split("=")[1];
							continue;
						}
						if (element.startsWith("remarks_de") && !element.endsWith("=")) {
							remarks_de = element.split("=")[1];
							continue;
						}
					}
					if (BatchUtil.isEmpty(userName) || BatchUtil.isEmpty(statusStr) || BatchUtil.isEmpty(iconStr)) {
						System.out.println("ERROR: Incorrect Syntax Mandatory Fields not Provided");
						return; // Error incorrect syntax mandatory fields not
								// provided
					}
					Status status = statusStr.toUpperCase().equals("ACTIVE") ? Status.ACTIVE : Status.INACTIVE;
					IconController iconController = new IconController();
					IkonRequest ikonRequest = new IkonRequest();
					ikonRequest.setIconName(iconStr);
					IconRespWrapper iconRespWrapper = iconController.getIconByName(ikonRequest);
					if (iconRespWrapper == null || BatchUtil.isEmpty(iconRespWrapper.getIconId())) {
						System.out.println("ERROR: incorrect icon is given in command line");
						return; // ERROR incorrect icon is given in command
								// line;
					}

					userRequest = new UserRequest();
					userRequest.setUserName(userName);
					userRequest.setIconId(iconRespWrapper.getIconId());
					userRequest.setStatus(status);
					if (!BatchUtil.isEmpty(fullName)) {
						userRequest.setFullName(fullName);
					}
					if (!BatchUtil.isEmpty(emailId)) {
						userRequest.setEmail(emailId);
					}
					if (!BatchUtil.isEmpty(department)) {
						userRequest.setDepartment(department);
					}
					if (!BatchUtil.isEmpty(telephoneNumber)) {
						userRequest.setTelephoneNumber(telephoneNumber);
					}

					if (!BatchUtil.isEmpty(desc_en)) {
						userTranslationTbl_en.setDescription(desc_en);
					}
					if (!BatchUtil.isEmpty(remarks_en)) {
						userTranslationTbl_en.setRemarks(remarks_en);
					}
					if (!BatchUtil.isEmpty(desc_de)) {
						userTranslationTbl_de.setDescription(desc_de);
					}
					if (!BatchUtil.isEmpty(remarks_de)) {
						userTranslationTbl_de.setRemarks(remarks_de);
					}
					List<UserTranslation> userTranslationTblList = new ArrayList<UserTranslation>();
					userTranslationTblList.add(userTranslationTbl_en);
					userTranslationTblList.add(userTranslationTbl_de);
					userRequest.setUserTranslation(userTranslationTblList);

					UserController userController = new UserController();
					try {
						Boolean saveUser = userController.saveUser(userRequest);
						if (saveUser == null || saveUser == Boolean.FALSE) {
							// Show unable to create user
							// System.out.println("ERROR: Unable to Create User,
							// User exists");
							return;
						} else {
							// User created
							System.out.println("User created Successfully");

						}
					} catch (CannotCreateObjectException e) {
						String message = e.getMessage();
						System.out.println(e.getMessage());
						// Show Unable to create object as User already exists
						// System.out.println("ERROR: Failed to create User,
						// User already exists");
						// LOGGER.error("Error while calling saveUser
						// userController !", e);
						return;
					} catch (UnauthorizedAccessException e) {
						// Show current user is not authorized
						System.out.println(e.getMessage());
						// System.out.println("Current User not Authorized to
						// create User");
						// LOGGER.error("Error while calling saveUser
						// userController !", e);
					}
				} catch (Exception e) {
					System.out.println("Error : Invalid syntax !");
				}
				break;

			default:
				break;
			}
			// delete user
		} else if (argsList.contains("-delete")) {
			try {
				int indexOfDelete = argsList.indexOf("-delete");
				final String objectType = argsList.get(indexOfDelete + 1).toUpperCase();
				if (objectType.equals("USER")) {
					int indexOfUser = argsList.indexOf("user");
					final String option = argsList.get(indexOfUser + 1).toUpperCase();
					final UserController controller = new UserController();
					if (option.equals("-WHERE")) {
						int indexOfWhere = argsList.indexOf("-where");
						String uName = null;
						final String element = argsList.get(indexOfWhere + 1);
						if (!element.endsWith("=")) {
							uName = element.split("=")[1];
						}
						try {
							if (!BatchUtil.isEmpty(uName)) {
								final Boolean deleteUser = controller.deleteUser(uName);
								if (deleteUser == null || deleteUser == Boolean.FALSE) {
									// Show unable to delete user
									System.out.println("ERROR: Failed to Delete User, User does not exist");
									return;
								} else {
									// User deleted
									System.out.println("User Deleted Successfully");
								}
							} else {
								System.out.println("ERROR: Incorrect Syntax : User Name is not provided");
							}
						} catch (CannotCreateObjectException e) {
							// Show Unable to create object as User does not
							// exists
							System.out.println("ERROR: Failed to Delete User , User does not exist");
							LOGGER.error("Error while calling deleteUser userController !", e);
						} catch (UnauthorizedAccessException e) {
							// Show current user is not authorized
							System.out.println("ERROR: Current user not Authorized to create User");
							LOGGER.error("Error while calling deleteUser  userController !", e);
						}
					} else {
						System.out.println("ERROR: Incorrect Syntax");
					}
				}
			} catch (Exception e) {
				// Show error incorrect syntax
				System.out.println("ERROR: Incorrect Syntax");
				LOGGER.error("Error while calling deleteUser userController !", e);
			}
		} else if (argsList.contains("-update")) {

		} else {
			System.out.println("ERROR: Incorrect Syntax");
		}
	}

	/**
	 * Process relation.
	 *
	 * @param argsList
	 *            the args list
	 */
	private void processRelation(List<String> argsList) {
		try {

			if (argsList.contains("-ADMINAREA_PROJECT")) {
				// ADMINAREA_PROJECT listing
				String sqlQueryStr = (SQLConverter.convertToSql(argsList));
				// System.out.println(sqlQueryStr);
				if (sqlQueryStr == null || sqlQueryStr.isEmpty()) {
					System.out.println("ERROR: syntax error ! use -help option for help");
					return;
				}
				try {
					XMBatchController batchController = new XMBatchController();
					XmbatchResponse resultSet = batchController.getResultSet(sqlQueryStr);
					List<Map<String, Object>> queryResultSet = resultSet.getQueryResultSet();
					if (queryResultSet == null) {
						// query failed
						System.out.println("ERROR: query failed\n");
					} else {
						// query succeeded

						// list entries
						if (queryResultSet.isEmpty()) {
							System.out.println("No Result found");
							return;
						}

						if (argsList.contains("-genexcel")) {

							int optionfname = argsList.indexOf("-genexcel");

							if (argsList.get(optionfname + 1).equals("-type")) {
								// no path provided
								// create exportExcell in src/resource
								CreateExcell.createExcellforUserCmds(queryResultSet, "adminproject",
										"src/resource/exportExcel_1.xlsx");
								return;
							} else if (argsList.get(optionfname + 1).contains(":/")) {
								String path = argsList.get(optionfname + 1);
								// only path provided
								if (!path.contains(".")) {
									// create exportExcell in "path/exportExcel"
									String filepath = path + "/exportExcel_1.xlsx";
									CreateExcell.createExcellforUserCmds(queryResultSet, "adminproject", filepath);
									return;
								}
							}
							if ((argsList.get(optionfname + 1).contains(".xls")
									|| argsList.get(optionfname + 1).contains(".xlsx")
									|| argsList.get(optionfname + 1).contains(".csv"))) {

								String optname = argsList.get(optionfname + 1);
								if (optname.split("\\.")[1].equals("xls") || optname.split("\\.")[1].equals("xlsx")
										|| optname.split("\\.")[1].equals("csv")) {

									// filename and path provided
									// create excell in "path"
									String fpath = argsList.get(optionfname + 1);
									CreateExcell.createExcellforUserCmds(queryResultSet, "adminproject", fpath);
									return;
								} else {
									System.out.println("Invalid file name provided");
									return;
								}
							} else {

								System.out.println("No path or Incorrect file name provided ");
								return;
							}
						} else {
							display(queryResultSet, "adminproject");
						}
					}
				} catch (CannotCreateObjectException e) {
					// Failed to query User
					System.out.println("ERROR: Failed to query User ");
					LOGGER.error("Error while calling batchController  !", e);
				} catch (UnauthorizedAccessException e) {
					// Show current user is not authorized
					System.out.println("Current user not Authorized to query User");
					LOGGER.error("Error while calling  batchController !", e);
				}
			} else if (argsList.contains("-USER_PROJECT")) {
				String status = null;
				final UserProjectRelRequest userProjectRelRequest = new UserProjectRelRequest();

				int indexOfRelationType = argsList.indexOf("-USER_PROJECT");
				final String option = argsList.get(indexOfRelationType + 1);

				// add user to project
				if (option.contains("-add")) {
					int indexOfOption = argsList.indexOf("-add");
					final String userElement = argsList.get(indexOfOption + 1);
					final String userName = userElement.split("=")[1];
					int indexOfUserNameElement = argsList.indexOf(userElement);
					final String projectElement = argsList.get(indexOfUserNameElement + 1);
					final String projectName = projectElement.split("=")[1];
					int indexOfProjectNameElement = argsList.indexOf(projectElement);
					if (indexOfProjectNameElement + 1 == argsList.size()) {
						status = "ACTIVE";
					} else {
						String andElement = argsList.get(indexOfProjectNameElement + 1);
						if (andElement.equalsIgnoreCase("AND")) {
							String statusElement = argsList.get(indexOfProjectNameElement + 2);
							if (statusElement.contains("status=")) {
								status = statusElement.split("=")[1];
							}
						} else {
							System.out.println("ERROR: Syntax error ! use -help option for Help");
							return;
						}
						//status = argsList.get(indexOfProjectNameElement + 1).split("=")[1];
					}
					final UserProjectRelController userProjectRelController = new UserProjectRelController();
					userProjectRelRequest.setProjectName(projectName);
					userProjectRelRequest.setUserName(userName);
					userProjectRelRequest.setStatus(status);
					try {
						Boolean addUser = userProjectRelController.addUserToProject(userProjectRelRequest);
						if (addUser == null || addUser == Boolean.FALSE) {
							// Show unable to add user to project
							return;
							// System.out.println("ERROR: Unable to Add user to
							// Project, User exists");
						} else {
							// User created
							System.out.println("User added to Project Successfully");
						}
					} catch (CannotCreateObjectException e) {
						// Show Unable to add User
						System.out.println("ERROR: Failed to add User to Project ");
						LOGGER.error("Error while calling addUserToProject userProjectRelController !", e);
					} catch (UnauthorizedAccessException e) {
						// Show current user is not authorized
						System.out.println(e.getMessage());
						LOGGER.error("Error while calling addUserToProject userProjectRelController !", e);
					}
				} else if (option.contains("-where")) {
					// project to user or user to project listing
					final String sqlQueryStr = SQLConverter.convertToSql(argsList);
					if (sqlQueryStr == null || sqlQueryStr.isEmpty()) {
						System.out.println("ERROR: Syntax error ! use -help option for Help");
						return;
					}
					try {
						XMBatchController batchController = new XMBatchController();
						XmbatchResponse resultSet = batchController.getResultSet(sqlQueryStr);
						List<Map<String, Object>> queryResultSet = resultSet.getQueryResultSet();
						if (queryResultSet == null) {
							// Show unable query
							System.out.println("ERROR: query failed\n");
						} else {
							// query succeeded

							if (queryResultSet.isEmpty()) {
								System.out.println("No Result found");
							} else {

								if (argsList.contains("-genexcel")) {
									int optionfname = argsList.indexOf("-genexcel");

									if (argsList.get(optionfname + 1).equals("-type")) {
										// no path provided
										// create exportExcell in src/resource
										CreateExcell.createExcellforUserCmds(queryResultSet, "userproject",
												"src/resource/exportExcel_1.xlsx");
										return;
									} else if (argsList.get(optionfname + 1).contains(":/")) {
										String path = argsList.get(optionfname + 1);
										// only path provided
										if (!path.contains(".")) {
											// create exportExcell in
											// "path/exportExcel"
											String filepath = path + "/exportExcel_1.xlsx";
											CreateExcell.createExcellforUserCmds(queryResultSet, "userproject",
													filepath);
											return;
										}
									}
									if ((argsList.get(optionfname + 1).contains(".xls")
											|| argsList.get(optionfname + 1).contains(".xlsx")
											|| argsList.get(optionfname + 1).contains(".csv"))) {

										String optname = argsList.get(optionfname + 1);
										if (optname.split("\\.")[1].equals("xls")
												|| optname.split("\\.")[1].equals("xlsx")
												|| optname.split("\\.")[1].equals("csv")) {

											// filename and path provided
											// create excell in "path"
											String fpath = argsList.get(optionfname + 1);
											CreateExcell.createExcellforUserCmds(queryResultSet, "userproject", fpath);
											return;
										} else {
											System.out.println("Invalid file name provided");
											return;
										}
									} else {

										System.out.println("No path or Incorrect file name provided ");
										return;
									}
								} else {

									// list entries
									display(queryResultSet, "userProject");
								}

							}
						}
					} catch (CannotCreateObjectException e) {
						// Show
						System.out.println("ERROR: Failed to query ");
						LOGGER.error("Error while calling batchController !", e);
					} catch (UnauthorizedAccessException e) {
						// Show current user is not authorized
						System.out.println(e.getMessage());
						LOGGER.error("Error while calling  BatchController !", e);
					}
					// remove user from project
				} else if (option.contains("-remove")) {
					int indexOfOption = argsList.indexOf("-remove");
					final String userElement = argsList.get(indexOfOption + 1);
					final String userName = userElement.split("=")[1];
					int indexOfUserElement = argsList.indexOf(userElement);
					final String projectElement = argsList.get(indexOfUserElement + 1);
					final String projectName = projectElement.split("=")[1];
					final UserProjectRelController userProjectRelController = new UserProjectRelController();
					userProjectRelRequest.setProjectName(projectName);
					userProjectRelRequest.setUserName(userName);
					try {
						Boolean removeUser = userProjectRelController.removeUserFromProject(userProjectRelRequest);
						if (removeUser == null || removeUser == Boolean.FALSE) {
							return;
							// Show unable to remove user to project
							// System.out.println("ERROR: Unable to remove User
							// from Project, User does not exist");
						} else {
							// User removed
							System.out.println("User removed from Project Successfully");
						}
					} catch (CannotCreateObjectException e) {
						// Show Unable to remove User from project
						System.out.println("ERROR: Failed to remove User from Project ");
						// LOGGER.error("Error while calling
						// removeUserFromProject userProjectRelController !",
						// e);
					} catch (UnauthorizedAccessException e) {
						// Show current user is not authorized
						System.out.println("Current User not Authorized to remove User from Project");
						// LOGGER.error("Error while calling
						// removeUserFromProject userProjectRelController !",
						// e);
					}
				} else {
					System.out.println("ERROR: Incorrect Syntax ! use -help option for Help");
				}
			} else if (argsList.contains("-USER_PROJECT_USAGE")) {
				// USER_PROJECT_USAGE
				String sqlQueryStr = (SQLConverter.convertToSql(argsList));
				// System.out.println(sqlQueryStr);
				if (sqlQueryStr == null || sqlQueryStr.isEmpty()) {
					System.out.println("ERROR: Syntax error ! use -help option for Help");
					return;
				}
				try {
					XMBatchController batchController = new XMBatchController();
					XmbatchResponse resultSet = batchController.getResultSet(sqlQueryStr);
					List<Map<String, Object>> queryResultSet = resultSet.getQueryResultSet();
					if (queryResultSet == null) {
						// query failed
						System.out.println("ERROR: query failed\n");
					} else {
						// query succeeded
						// System.out.println("query successfull\n");
						// list entries
						if (queryResultSet.isEmpty()) {
							System.out.println("No Result found");
							return;
						}

						// deduce date and send the list to create excel

						// get year option from argument list
						String year = argsList.get(argsList.size() - 1).split("=")[1];
						if (argsList.contains("-genexcel")) {
							int optionfname = argsList.indexOf("-genexcel");

							if (argsList.get(optionfname + 1).equals("-type")) {
								// no path provided
								// create exportExcell in src/resource
								String filepath = "src/resource/projectusage.xlsx";
								CreateExcell.createExcellforUserCmds(queryResultSet, "userprojectusage",
										filepath + "_" + year);
								return;
							} else if (argsList.get(optionfname + 1).contains(":/")) {
								String path = argsList.get(optionfname + 1);
								// only path provided
								if (!path.contains(".")) {
									// create exportExcell in "path/exportExcel"
									String filepath = path + "/projectusage.xlsx";
									CreateExcell.createExcellforUserCmds(queryResultSet, "userprojectusage",
											filepath + "_" + year);
									return;
								}
							}
							if ((argsList.get(optionfname + 1).contains(".xls")
									|| argsList.get(optionfname + 1).contains(".xlsx")
									|| argsList.get(optionfname + 1).contains(".csv"))) {

								String optname = argsList.get(optionfname + 1);
								if (optname.split("\\.")[1].equals("xls") || optname.split("\\.")[1].equals("xlsx")
										|| optname.split("\\.")[1].equals("csv")) {

									// filename and path provided
									// create excell in "path"
									String fpath = argsList.get(optionfname + 1);
									CreateExcell.createExcellforUserCmds(queryResultSet, "userprojectusage",
											fpath + "_" + year);
									return;
								} else {
									System.out.println("Invalid file name provided");
									return;
								}
							} else {

								System.out.println("No path or Incorrect file name provided ");
								return;
							}
						} else {

							// list entries
							display(queryResultSet, "userprojectusage" + "_" + year);
						}

					}
				} catch (CannotCreateObjectException e) {
					// Failed to query User
					System.out.println("ERROR: Failed to query User ");
					LOGGER.error("Error while calling batchController  !", e);
				} catch (UnauthorizedAccessException e) {
					// Show current user is not authorized
					System.out.println("Current user not Authorized to query User");
					LOGGER.error("Error while calling  batchController !", e);
				}
			} else {
				System.out.println("ERROR: Syntax Error! use -help option for Help");
			}
		} catch (Exception e) {
			System.out.println("ERROR: Syntax Error! use -help option for Help");
			// LOGGER.error("Error while calling userProjectRelController !",
			// e);
			// e.printStackTrace();
		}
	}

	/**
	 * Display.
	 *
	 * @param list
	 *            the list
	 */
	public void display(final List<Map<String, Object>> list, String type) {

		if (type.equals("user")) {
			for (Map<String, Object> userObjectMap : list) {
				UserData UsrData = new UserData();
				Set<Entry<String, Object>> EntrySet = userObjectMap.entrySet();
				for (Entry<String, Object> entry : EntrySet) {

					switch (entry.getKey()) {

					case "USERNAME":
						UsrData.setUserName((String) entry.getValue());
						break;

					case "FULL_NAME":
						UsrData.setFullName((String) entry.getValue());
						break;
					case "EMAIL_ID":
						UsrData.setEmailID((String) entry.getValue());
						break;
					case "TELEPHONE_NUMBER":
						UsrData.setPhNumber((String) entry.getValue());
						break;
					case "DEPARTMENT":
						UsrData.setDepartment((String) entry.getValue());
						break;
					case "STATUS":
						UsrData.setStatus(((String) entry.getValue()).trim());
						break;
					case "DESCRIPTION":
						UsrData.setDescription((String) entry.getValue());
						break;
					case "REMARKS":
						UsrData.setRemarks((String) entry.getValue());
						break;
					case "LANGUAGE_CODE":
						UsrData.setLang_code((String) entry.getValue());
						break;
					case "ICON_NAME":
						UsrData.setIconName((String) entry.getValue());
						break;

					default:

					}

				}
				System.out.print(String.format("%-15s %-15s %-10s %-10s %-10s %-10s %-10s %-30s %-15s %-15s\n",
						UsrData.getUserName(), UsrData.getFullName(), UsrData.getEmailID(), UsrData.getPhNumber(),
						UsrData.getDepartment(), UsrData.getStatus(), UsrData.getDescription(), UsrData.getRemarks(),
						UsrData.getLang_code(), UsrData.getIconName()));
			}

		} else if (type.equals("userdistinct")) {
			for (Map<String, Object> userObjectMap : list) {
				UserData UsrData = new UserData();
				UsrData.setDepartment(" ");
				UsrData.setDescription(" ");
				UsrData.setEmailID(" ");
				UsrData.setFullName(" ");
				UsrData.setIconName(" ");
				UsrData.setLang_code(" ");
				UsrData.setLang_code(" ");
				UsrData.setPhNumber(" ");
				UsrData.setRemarks(" ");
				UsrData.setStatus(" ");
				UsrData.setUserName(" ");

				Set<Entry<String, Object>> EntrySet = userObjectMap.entrySet();
				for (Entry<String, Object> entry : EntrySet) {

					switch (entry.getKey()) {

					case "USERNAME":
						UsrData.setUserName((String) entry.getValue());
						break;

					case "FULL_NAME":
						UsrData.setFullName((String) entry.getValue());
						break;
					case "EMAIL_ID":
						UsrData.setEmailID((String) entry.getValue());
						break;
					case "TELEPHONE_NUMBER":
						UsrData.setPhNumber((String) entry.getValue());
						break;
					case "DEPARTMENT":
						UsrData.setDepartment((String) entry.getValue());
						break;
					case "STATUS":
						UsrData.setStatus(((String) entry.getValue()).trim());
						break;
					case "DESCRIPTION":
						UsrData.setDescription((String) entry.getValue());
						break;
					case "REMARKS":
						UsrData.setRemarks((String) entry.getValue());
						break;
					case "LANGUAGE_CODE":
						UsrData.setLang_code((String) entry.getValue());
						break;
					case "ICON_NAME":
						UsrData.setIconName((String) entry.getValue());
						break;

					default:

					}

				}
				System.out.print(String.format("%-15s %-15s %-10s %-10s %-10s %-10s %-10s %-30s %-15s %-15s\n",
						UsrData.getUserName(), UsrData.getFullName(), UsrData.getEmailID(), UsrData.getPhNumber(),
						UsrData.getDepartment(), UsrData.getStatus(), UsrData.getDescription(), UsrData.getRemarks(),
						UsrData.getLang_code(), UsrData.getIconName()));
			}

		} else if (type.equals("adminproject")) {
			// ADMIN_AREA_NAME=India_win, PROJECT_NAME=E83-V5, STATUS=ACTIVE

			for (Map<String, Object> userObjectMap : list) {
				RelatonData RelData = new RelatonData();

				Set<Entry<String, Object>> EntrySet = userObjectMap.entrySet();
				for (Entry<String, Object> entry : EntrySet) {

					switch (entry.getKey()) {
					case "ADMIN_AREA_NAME":
						RelData.setAdminAreaName((String) entry.getValue());
						break;
					case "PROJECT_NAME":
						RelData.setProjectName((String) entry.getValue());
						break;
					case "STATUS":
						RelData.setAdminAreaProjectStatus((String) entry.getValue());
						break;
					case "SITE_NAME":
						RelData.setSiteName((String) entry.getValue());

					default:

					}

				}
				System.out.print(String.format("%-15s %-15s %-15s %-15s \n", RelData.getSiteName(),
						RelData.getAdminAreaName(), RelData.getProjectName(), RelData.getAdminAreaProjectStatus()));
				System.out.println();
			}

		} else if (type.equals("userProject")) {

			for (Map<String, Object> userObjectMap : list) {
				RelatonData RelData = new RelatonData();

				Set<Entry<String, Object>> EntrySet = userObjectMap.entrySet();

				for (Entry<String, Object> entry : EntrySet) {
					// System.out.println(entry.getKey()+" "+entry.getValue());

					switch (entry.getKey()) {
					case "USERNAME":
						RelData.setUserName((String) entry.getValue());
						break;
					case "NAME":
						RelData.setProjectName((String) entry.getValue());
						break;
					case "STATUS":
						RelData.setUserProjectReationStatus((String) entry.getValue());
						break;

					default:

					}

				}
				System.out.print(String.format("%-15s %-15s %-15s \n", RelData.getUserName(), RelData.getProjectName(),
						RelData.getUserProjectReationStatus()));
			}

		} else if (type.contains("userprojectusage")) {

			String year = type.split("_")[1];

			List<String> lstr = new ArrayList<String>();

			for (int i = 1; i < 13; i++) {
				if (i >= 1 && i <= 9) {
					lstr.add(year + "/" + "0" + i);
				} else {
					if (i == 12) {
						lstr.add(year + "/" + i);
					} else {
						lstr.add(year + "/" + i);
					}
				}

			}

			System.out.print("Project\t\t");
			for (Object field : lstr) {

				System.out.print(field + "\t");
			}

			List<String> data = new ArrayList<String>();
			String projInfo = "";
			int[] dat = new int[12];
			Arrays.fill(dat, 0);
			int month = 0;
			String prevProjName = null;

			for (Map<String, Object> userObjectMap : list) {

				Set<Entry<String, Object>> EntrySet = userObjectMap.entrySet();
				for (Entry<String, Object> entry : EntrySet) {

					if (entry.getKey().equals("PROJECT")) {
						if (projInfo.isEmpty()) {
							projInfo = ((String) entry.getValue()).trim();
							prevProjName = projInfo;

						}

						if (!entry.getValue().equals(prevProjName)) {
							if (projInfo.length() > 7) {
								System.out.print(projInfo + "\t");
							} else {
								System.out.print(projInfo + "\t\t");
							}
							for (int i = 0; i < 12; i++) {
								System.out.print(dat[i] + "\t");
							}
							System.out.println();
							// data.add(projInfo+";"+info);//
							Arrays.fill(dat, 0);
							projInfo = "";
						}
					}

					if (entry.getKey().equals("USEMONTH")) {
						String mm = ((String) entry.getValue()).split("/")[1];
						month = Integer.valueOf(mm);
					}

					if (entry.getKey().equals("COUNT(PROJECT)")) {
						try {
							int val = (int) entry.getValue();
							dat[month - 1] = val;
						} catch (Exception e) {
							System.out.println(e.getMessage());
						}
					}

				}
			}

			if (projInfo.length() > 7) {
				System.out.print(projInfo + "\t");
			} else {
				System.out.print(projInfo + "\t\t");
			}
			for (int i = 0; i < 12; i++) {
				System.out.print(dat[i] + "\t");

			}
		}

	}

	/**
	 * The main method.
	 *
	 * @param args
	 *            the arguments
	 */
	public static void main(String[] args) {

		XmSystemEnvProcess.getInstance().start();
		new CaxStartBatchMain(args);
	}
}
