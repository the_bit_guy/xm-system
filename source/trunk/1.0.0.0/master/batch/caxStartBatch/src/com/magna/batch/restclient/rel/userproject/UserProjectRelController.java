package com.magna.batch.restclient.rel.userproject;

import java.io.IOException;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.magna.batch.restclient.BatchRestController;
import com.magna.batch.restclient.exception.UnauthorizedAccessException;
import com.magna.batch.utils.RestClientConstants;
import com.magna.xmbackend.exception.CannotCreateObjectException;
import com.magna.xmbackend.vo.rel.UserProjectRelRequest;

/**
 * The Class UserProjectRelController.
 */
public class UserProjectRelController extends BatchRestController {
	/** The Constant LOGGER. */
	//private static final Logger LOGGER = LoggerFactory.getLogger(UserProjectRelController.class);
	
	
	/**
	 * Adds the user to project.
	 *
	 * @param userProjectRelRequest the user project rel request
	 * @return the boolean
	 * @throws UnauthorizedAccessException the unauthorized access exception
	 * @throws CannotCreateObjectException the cannot create object exception
	 * @throws IOException 
	 * @throws JsonProcessingException 
	 */
	public Boolean addUserToProject(final UserProjectRelRequest userProjectRelRequest)
			throws UnauthorizedAccessException, CannotCreateObjectException, JsonProcessingException, IOException {
		try {
			String url = new String(this.serviceUrl + RestClientConstants.ADD_USER_TO_PROJECT);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<UserProjectRelRequest> request = new HttpEntity<UserProjectRelRequest>(userProjectRelRequest, this.headers);
			ResponseEntity<Boolean> responseEntity = restTemplate.postForEntity(url, request, Boolean.class);
			HttpStatus statusCode = responseEntity.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				
				return responseEntity.getBody();
			} else {
				//LOGGER.error("Error while calling XMSYSTEM Create User Project Relation REST Service, returns the status code: ", //$NON-NLS-1$
					//	statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException) e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				} else if (e instanceof HttpStatusCodeException) {
					String errorResponse = ((HttpStatusCodeException) e).getResponseBodyAsString();
					ObjectMapper mapper = new ObjectMapper();
					JsonNode jsonNode = mapper.readTree(errorResponse);
					String expMsg = jsonNode.get("lingualMessages").get("en").textValue();
					System.out.println(expMsg);
					return false;
				}
			} 
		} catch (Exception e) {
			e.printStackTrace();
			//LOGGER.error("Error while calling XMSYSTEM Create User Project Relation REST Service", e); //$NON-NLS-1$
		}
		return null;
	}
	
	/**
	 * Removes the user from project.
	 *
	 * @param userProjectRelRequest the user project rel request
	 * @return the boolean
	 * @throws UnauthorizedAccessException the unauthorized access exception
	 * @throws CannotCreateObjectException the cannot create object exception
	 * @throws IOException 
	 * @throws JsonProcessingException 
	 */
	public Boolean removeUserFromProject(final UserProjectRelRequest userProjectRelRequest)
			throws UnauthorizedAccessException, CannotCreateObjectException, JsonProcessingException, IOException {
		try {
			String url = new String(this.serviceUrl + RestClientConstants.REMOVE_USER_FROM_PROJECT);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<UserProjectRelRequest> request = new HttpEntity<UserProjectRelRequest>(userProjectRelRequest, this.headers);
			ResponseEntity<Boolean> responseEntity = restTemplate.exchange(url, HttpMethod.DELETE, request, Boolean.class);
			HttpStatus statusCode = responseEntity.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				return responseEntity.getBody();
			} else {
			//	LOGGER.error("Error while calling XMSYSTEM Create User Project Relation REST Service, returns the status code: ", //$NON-NLS-1$
					//	statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException) e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				} else if (e instanceof HttpStatusCodeException) {
					String errorResponse = ((HttpStatusCodeException) e).getResponseBodyAsString();
					ObjectMapper mapper = new ObjectMapper();
					JsonNode jsonNode = mapper.readTree(errorResponse);
					String expMsg = jsonNode.get("lingualMessages").get("en").textValue();
					System.out.println(expMsg);
					return false;
				}
			} 
		} catch (Exception e) {
			//LOGGER.error("Error while calling XMSYSTEM Create User Project Relation REST Service", e); //$NON-NLS-1$
		}
		return null;
	}

}
