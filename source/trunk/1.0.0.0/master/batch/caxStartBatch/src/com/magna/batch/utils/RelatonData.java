package com.magna.batch.utils;

public class RelatonData {
	
	private String adminAreaName;
	private String projectName;
	private String adminAreaProjectStatus;
	private String userName;
	private String userProjectReationStatus;
	private String siteName;
	
	public RelatonData() {
		super();
	}

	public String getAdminAreaName() {
		return adminAreaName;
	}

	public void setAdminAreaName(String adminAreaName) {
		this.adminAreaName = adminAreaName;
	}

	public String getProjectName() {
		return projectName;
	}

	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}

	public String getAdminAreaProjectStatus() {
		return adminAreaProjectStatus;
	}

	public void setAdminAreaProjectStatus(String adminAreaProjectStatus) {
		this.adminAreaProjectStatus = adminAreaProjectStatus;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getUserProjectReationStatus() {
		return userProjectReationStatus;
	}

	public void setUserProjectReationStatus(String userProjectReationStatus) {
		this.userProjectReationStatus = userProjectReationStatus;
	}

	public String getSiteName() {
		return siteName;
	}

	public void setSiteName(String siteName) {
		this.siteName = siteName;
	}

	
	
	
	
}
