package com.magna.batch.utils;

/**
 * The Class BatchUtil.
 * 
 * @author shashwat.anand
 */
public class BatchUtil {
	
	
	
	/**
	 * Checks if is empty.
	 *
	 * @param inputStr the input str
	 * @return true, if is empty
	 */
	public static boolean isEmpty(final String inputStr) {
		if (inputStr != null && !inputStr.isEmpty()) {
			return false;
		}
		return true;
	}
	
	/**
	 * Gets the XM backend url.
	 *
	 * @return the XM backend url
	 */
	public static String getXMBackendUrl() {
		return CaxStartBatchProps.getInstance().getProperty("XMBACKEND_SERVICE_URL");
	}
	
	/**
	 * Gets the system user name.
	 *
	 * @return the system user name
	 */
	public static String getSystemUserName() {
		return XmSystemEnvProcess.getInstance().getenv("XM_USER");
	}
}
