package com.magna.xmsystem.xmadmin.restclient.usersettings;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import com.magna.xmbackend.entities.UserSettingsTbl;
import com.magna.xmbackend.exception.CannotCreateObjectException;
import com.magna.xmbackend.vo.userSettings.UserSettingsRequest;
import com.magna.xmsystem.dependencies.customExceptions.UnauthorizedAccessException;
import com.magna.xmsystem.xmadmin.restclient.RestController;
import com.magna.xmsystem.xmadmin.restclient.util.RestClientExpHandlerUtil;
import com.magna.xmsystem.xmadmin.restclient.util.XMAdminRestClientConstants;

/**
 * Class for User settings controller.
 *
 * @author Chiranjeevi.Akula
 */
public class UserSettingsController extends RestController{

	/** The Constant LOGGER. */
	private static final Logger LOGGER = LoggerFactory.getLogger(UserSettingsController.class);

	/**
	 * Constructor for UserSettingsController Class.
	 */
	public UserSettingsController() {
		super();
	}
	
	/**
	 * Gets the user settings by site id.
	 *
	 * @param siteId {@link String}
	 * @return the user settings by site id
	 * @throws UnauthorizedAccessException the unauthorized access exception
	 */
	public UserSettingsTbl getUserSettingsBySiteId(final String siteId) throws UnauthorizedAccessException {
		try {
			String url = new String(this.serviceUrl + XMAdminRestClientConstants.USER_SETTINGS_BY_SITE_ID + "/" + siteId);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<Object> requestEntity = new HttpEntity<Object>(headers);

			List<HttpMessageConverter<?>> messageConverters = new ArrayList<>();
			messageConverters.add(new MappingJackson2HttpMessageConverter());
			restTemplate.setMessageConverters(messageConverters);

			ResponseEntity<UserSettingsTbl> responseEntity = restTemplate.exchange(url, HttpMethod.GET, requestEntity,
					UserSettingsTbl.class);
			HttpStatus statusCode = responseEntity.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				return responseEntity.getBody();
			} else {
				LOGGER.error("Error while calling XMSYSTEM Get User settings by site id REST Service, returns the status code: " //$NON-NLS-1$
						+ statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException)e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			} else if (e instanceof HttpStatusCodeException) {
				RestClientExpHandlerUtil.handleNoXMLObjectFoundException(LOGGER, (HttpStatusCodeException) e);
			} else if (e instanceof ResourceAccessException) {
				throw e;
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM Get User settings by site id REST Service", e); //$NON-NLS-1$
		}
		return null;
	}
	
	/**
	 * Method for Update project in user settings.
	 *
	 * @param userSettingsRequest {@link UserSettingsRequest}
	 * @return the user settings tbl {@link UserSettingsTbl}
	 * @throws CannotCreateObjectException the cannot create object exception
	 * @throws UnauthorizedAccessException the unauthorized access exception
	 */
	public UserSettingsTbl updateProjectInUserSettings(UserSettingsRequest userSettingsRequest) throws CannotCreateObjectException, UnauthorizedAccessException {
		try {
			String url = new String(this.serviceUrl + XMAdminRestClientConstants.UPDATE_PROJECT_IN_USER_SETTINGS);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<UserSettingsRequest> request = new HttpEntity<UserSettingsRequest>(userSettingsRequest, this.headers);
			ResponseEntity<UserSettingsTbl> responseEntity = restTemplate.postForEntity(url, request, UserSettingsTbl.class);
			HttpStatus statusCode = responseEntity.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				UserSettingsTbl userSettingsTbl = responseEntity.getBody();
				return userSettingsTbl;
			} else {
				LOGGER.error("Error while calling saveOrUpdateProject userSettings REST Service, returns the status code: ", //$NON-NLS-1$
						statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException)e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				} else if (e instanceof HttpStatusCodeException) {
					String errorResponse = ((HttpStatusCodeException) e).getResponseBodyAsString();
					String errCode = RestClientExpHandlerUtil.handleCannotCreateObjectException(LOGGER, (HttpStatusCodeException) e);
					throw new CannotCreateObjectException(errorResponse, errCode);
				}
			} else if (e instanceof HttpStatusCodeException) {
				RestClientExpHandlerUtil.handleNoXMLObjectFoundException(LOGGER, (HttpStatusCodeException) e);
			} else if (e instanceof ResourceAccessException) {
				throw e;
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling saveOrUpdateProject userSettings REST Service", e); //$NON-NLS-1$
		}
		return null;
	}
	
	/**
	 * Method for Update settings in user settings.
	 *
	 * @param userSettingsRequest {@link UserSettingsRequest}
	 * @return the user settings tbl {@link UserSettingsTbl}
	 * @throws CannotCreateObjectException the cannot create object exception
	 * @throws UnauthorizedAccessException the unauthorized access exception
	 */
	public UserSettingsTbl updateSettingsInUserSettings(UserSettingsRequest userSettingsRequest) throws CannotCreateObjectException, UnauthorizedAccessException {
		try {
			String url = new String(this.serviceUrl + XMAdminRestClientConstants.UPDATE_SETTINGS_IN_USER_SETTINGS);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<UserSettingsRequest> request = new HttpEntity<UserSettingsRequest>(userSettingsRequest, this.headers);
			ResponseEntity<UserSettingsTbl> responseEntity = restTemplate.postForEntity(url, request, UserSettingsTbl.class);
			HttpStatus statusCode = responseEntity.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				UserSettingsTbl userSettingsTbl = responseEntity.getBody();
				return userSettingsTbl;
			} else {
				LOGGER.error("Error while calling saveOrUpdateSettings userSettings REST Service, returns the status code: ", //$NON-NLS-1$
						statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException)e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				} else if (e instanceof HttpStatusCodeException) {
					String errorResponse = ((HttpStatusCodeException) e).getResponseBodyAsString();
					String errCode = RestClientExpHandlerUtil.handleCannotCreateObjectException(LOGGER, (HttpStatusCodeException) e);
					throw new CannotCreateObjectException(errorResponse, errCode);
				}
			} else if (e instanceof HttpStatusCodeException) {
				RestClientExpHandlerUtil.handleNoXMLObjectFoundException(LOGGER, (HttpStatusCodeException) e);
			} else if (e instanceof ResourceAccessException) {
				throw e;
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling saveOrUpdateSettings userSettings REST Service", e); //$NON-NLS-1$
		}
		return null;
	}
}