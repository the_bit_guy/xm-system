package com.magna.xmsystem.xmadmin.restclient.relation;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import com.magna.xmbackend.exception.CannotCreateObjectException;
import com.magna.xmbackend.vo.directory.DirectoryRelBatchRequest;
import com.magna.xmbackend.vo.directory.DirectoryRelBatchResponse;
import com.magna.xmbackend.vo.directory.DirectoryRelResponse;
import com.magna.xmbackend.vo.directory.DirectoryRelResponseWrapper;
import com.magna.xmsystem.dependencies.customExceptions.UnauthorizedAccessException;
import com.magna.xmsystem.xmadmin.restclient.RestController;
import com.magna.xmsystem.xmadmin.restclient.site.SiteController;
import com.magna.xmsystem.xmadmin.restclient.util.RestClientExpHandlerUtil;
import com.magna.xmsystem.xmadmin.restclient.util.RestClientSortingUtil;
import com.magna.xmsystem.xmadmin.restclient.util.XMAdminRestClientConstants;

/**
 * Class for Directory rel controller.
 *
 * @author Chiranjeevi.Akula
 */
public class DirectoryRelController extends RestController {

	/** The Constant LOGGER. */
	private static final Logger LOGGER = LoggerFactory.getLogger(SiteController.class);

	/**
	 * Constructor for DirectoryRelController Class.
	 */
	public DirectoryRelController() {
		super();
	}

	/**
	 * Method for Creates the directory relations.
	 *
	 * @param directoryRelBatchRequest
	 *            {@link DirectoryRelBatchRequest}
	 * @return the directory rel batch response
	 *         {@link DirectoryRelBatchResponse}
	 */
	public DirectoryRelBatchResponse createDirectoryRelations(DirectoryRelBatchRequest directoryRelBatchRequest)
			throws UnauthorizedAccessException, CannotCreateObjectException {
		try {
			String url = new String(this.serviceUrl + XMAdminRestClientConstants.CREATE_MULTI_DIRECTORY_RELATIONS);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<DirectoryRelBatchRequest> request = new HttpEntity<DirectoryRelBatchRequest>(
					directoryRelBatchRequest, this.headers);
			ResponseEntity<DirectoryRelBatchResponse> responseEntity = restTemplate.postForEntity(url, request,
					DirectoryRelBatchResponse.class);
			HttpStatus statusCode = responseEntity.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				DirectoryRelBatchResponse directoryRelBatchResponse = responseEntity.getBody();
				return directoryRelBatchResponse;
			} else {
				LOGGER.error(
						"Error while calling XMSYSTEM Create Directory Relations REST Service, returns the status code: ", //$NON-NLS-1$
						statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException) e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			} else if (e instanceof HttpStatusCodeException) {
				String errorResponse = ((HttpStatusCodeException) e).getResponseBodyAsString();
				String errCode = RestClientExpHandlerUtil.handleCannotCreateObjectException(LOGGER,
						(HttpStatusCodeException) e);
				throw new CannotCreateObjectException(errorResponse, errCode);
			} else if (e instanceof ResourceAccessException) {
				throw e;
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM Create Directory Relations REST Service", e); //$NON-NLS-1$
		}
		return null;
	}

	/**
	 * Method for Delete multi directory.
	 *
	 * @param directoryRefIds
	 *            {@link String}
	 * @return true, if successful
	 */
	public boolean deleteMultiDirectory(List<String> directoryRefIds) throws UnauthorizedAccessException {
		try {
			String url = new String(this.serviceUrl + XMAdminRestClientConstants.DELETE_MULT_DIRECTORYI_REL);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<Object> requestEntity = new HttpEntity<Object>(directoryRefIds, this.headers);
			ResponseEntity<Boolean> responseEntity = restTemplate.exchange(url, HttpMethod.POST, requestEntity,
					Boolean.class);
			HttpStatus statusCode = responseEntity.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				return responseEntity.getBody();
			} else {
				LOGGER.error("Error while calling XMSYSTEM delete Directory REST Service, returns the status code: ", //$NON-NLS-1$
						statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException) e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM delete Directory REST Service!", e); //$NON-NLS-1$
		}
		return false;
	}

	/**
	 * Gets the directory rel by dir id and obj type.
	 *
	 * @param directoryId
	 *            {@link String}
	 * @param objType
	 *            {@link String}
	 * @return the directory rel by dir id and obj type
	 */
	public DirectoryRelResponseWrapper getDirectoryRelByDirIdAndObjType(String directoryId, String objType)
			throws UnauthorizedAccessException {
		try {
			String url = new String(
					this.serviceUrl + XMAdminRestClientConstants.GET_DIRECTORY_REL_BY_DIR_ID_AND_OBJ_TYPE + "/"
							+ directoryId + "/" + objType);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<Object> requestEntity = new HttpEntity<Object>(headers);

			List<HttpMessageConverter<?>> messageConverters = new ArrayList<>();
			messageConverters.add(new MappingJackson2HttpMessageConverter());
			restTemplate.setMessageConverters(messageConverters);

			ResponseEntity<DirectoryRelResponseWrapper> responseList = restTemplate.exchange(url, HttpMethod.GET,
					requestEntity, DirectoryRelResponseWrapper.class);
			HttpStatus statusCode = responseList.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				DirectoryRelResponseWrapper directoryRelResponseWrapper = responseList.getBody();
				List<DirectoryRelResponse> directoryRelResponses = directoryRelResponseWrapper.getDirectoryRelResponses();
			    RestClientSortingUtil.sortDirectoryRelByDirIdAndObjTypeTbl(directoryRelResponses, objType);
				return directoryRelResponseWrapper;
			} else {
				LOGGER.error(
						"Error while calling XMSYSTEM Get All Directory Relations By DirectoryId and ObjectType REST Service, returns the status code: " //$NON-NLS-1$
								+ statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException) e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			} else if (e instanceof HttpStatusCodeException) {
				RestClientExpHandlerUtil.handleNoXMLObjectFoundException(LOGGER, (HttpStatusCodeException) e);
			} else if(e instanceof ResourceAccessException){
				throw e;
			}
		} catch (Exception e) {
			LOGGER.error(
					"Error while calling XMSYSTEM Get All Directory Relations By DirectoryId and ObjectType REST Service", //$NON-NLS-1$
					e);
		}
		return null;
	}
}
