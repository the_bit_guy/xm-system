package com.magna.xmsystem.xmadmin.restclient.relation;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import com.magna.xmbackend.entities.UserProjectRelTbl;
import com.magna.xmbackend.exception.CannotCreateObjectException;
import com.magna.xmbackend.vo.rel.UserProjectRelBatchRequest;
import com.magna.xmbackend.vo.rel.UserProjectRelBatchResponse;
import com.magna.xmbackend.vo.rel.UserProjectRelRequest;
import com.magna.xmbackend.vo.rel.UserProjectRelResponse;
import com.magna.xmsystem.dependencies.customExceptions.UnauthorizedAccessException;
import com.magna.xmsystem.xmadmin.restclient.RestController;
import com.magna.xmsystem.xmadmin.restclient.util.RestClientExpHandlerUtil;
import com.magna.xmsystem.xmadmin.restclient.util.RestClientSortingUtil;
import com.magna.xmsystem.xmadmin.restclient.util.XMAdminRestClientConstants;

/**
 * Class for Site admin area rel controller.
 *
 * @author Roshan.Ekka
 */
public class UserProjectRelController extends RestController {
	/** The Constant LOGGER. */
	private static final Logger LOGGER = LoggerFactory.getLogger(UserProjectRelController.class);

	/**
	 * Constructor for SiteController Class.
	 */
	public UserProjectRelController() {
		super();
	}

	/**
	 * Method for Creates the user project rel.
	 *
	 * @param userProjectRelRequest
	 *            {@link UserProjectRelRequest}
	 * @return the user project rel tbl {@link UserProjectRelTbl}
	 */
	public UserProjectRelTbl createUserProjectRel(UserProjectRelRequest userProjectRelRequest)
			throws UnauthorizedAccessException, CannotCreateObjectException {
		try {
			String url = new String(this.serviceUrl + XMAdminRestClientConstants.CREATE_USER_PROJECT_REL);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<UserProjectRelRequest> request = new HttpEntity<UserProjectRelRequest>(userProjectRelRequest,
					this.headers);
			ResponseEntity<UserProjectRelTbl> responseEntity = restTemplate.postForEntity(url, request,
					UserProjectRelTbl.class);
			HttpStatus statusCode = responseEntity.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				UserProjectRelTbl userProjectRelTbl = responseEntity.getBody();
				return userProjectRelTbl;
			} else {
				LOGGER.error(
						"Error while calling XMSYSTEM Create User Project Relation REST Service, returns the status code: ", //$NON-NLS-1$
						statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException) e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			} else if (e instanceof HttpStatusCodeException) {
				String errorResponse = ((HttpStatusCodeException) e).getResponseBodyAsString();
				String errCode = RestClientExpHandlerUtil.handleCannotCreateObjectException(LOGGER,
						(HttpStatusCodeException) e);
				throw new CannotCreateObjectException(errorResponse, errCode);
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM Create User Project Relation REST Service", e); //$NON-NLS-1$
		}
		return null;
	}

	/**
	 * Method for Creates the user project rel.
	 *
	 * @param userProjectRelBatchRequest
	 *            {@link UserProjectRelBatchRequest}
	 * @return the user project rel batch response
	 *         {@link UserProjectRelBatchResponse}
	 */
	public UserProjectRelBatchResponse createUserProjectRel(UserProjectRelBatchRequest userProjectRelBatchRequest)
			throws UnauthorizedAccessException, CannotCreateObjectException {
		try {
			String url = new String(this.serviceUrl + XMAdminRestClientConstants.CREATE_MULTI_USER_PROJECT_REL);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<UserProjectRelBatchRequest> request = new HttpEntity<UserProjectRelBatchRequest>(
					userProjectRelBatchRequest, this.headers);
			ResponseEntity<UserProjectRelBatchResponse> responseEntity = restTemplate.postForEntity(url, request,
					UserProjectRelBatchResponse.class);
			HttpStatus statusCode = responseEntity.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				UserProjectRelBatchResponse userProjectRelBatchResponse = responseEntity.getBody();
				return userProjectRelBatchResponse;
			} else {
				LOGGER.error(
						"Error while calling XMSYSTEM Create Multi User Project Relation REST Service, returns the status code: ", //$NON-NLS-1$
						statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException) e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			} else if (e instanceof HttpStatusCodeException) {
				String errorResponse = ((HttpStatusCodeException) e).getResponseBodyAsString();
				String errCode = RestClientExpHandlerUtil.handleCannotCreateObjectException(LOGGER,
						(HttpStatusCodeException) e);
				throw new CannotCreateObjectException(errorResponse, errCode);
			} else if (e instanceof ResourceAccessException) {
				throw e;
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM Create Multi User Project Relation REST Service", e); //$NON-NLS-1$
		}
		return null;
	}

	/**
	 * Gets the all user project rel.
	 *
	 * @param isSorted
	 *            {@link boolean}
	 * @return the all user project rel
	 */
	public List<UserProjectRelTbl> getAllUserProjectRel(final boolean isSorted) throws UnauthorizedAccessException {
		try {
			String url = new String(this.serviceUrl + XMAdminRestClientConstants.GET_USER_PROJECT_REL);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<Object> requestEntity = new HttpEntity<Object>(headers);

			List<HttpMessageConverter<?>> messageConverters = new ArrayList<>();
			messageConverters.add(new MappingJackson2HttpMessageConverter());
			restTemplate.setMessageConverters(messageConverters);

			ResponseEntity<UserProjectRelResponse> responseList = restTemplate.exchange(url, HttpMethod.GET,
					requestEntity, UserProjectRelResponse.class);
			HttpStatus statusCode = responseList.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				UserProjectRelResponse userProjectRelResponse = responseList.getBody();
				List<UserProjectRelTbl> userProjectRelTblList = (List<UserProjectRelTbl>) userProjectRelResponse
						.getUserProjectRelTbls();
				return userProjectRelTblList;
			} else {
				LOGGER.error(
						"Error while calling XMSYSTEM Get All User Project Relation REST Service, returns the status code: " //$NON-NLS-1$
								+ statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException) e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			} else if (e instanceof HttpStatusCodeException) {
				RestClientExpHandlerUtil.handleNoXMLObjectFoundException(LOGGER, (HttpStatusCodeException) e);
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM Get All User Project Relation REST Service", e); //$NON-NLS-1$
		}
		return null;
	}

	/**
	 * Method for Delete user project rel.
	 *
	 * @param id
	 *            {@link String}
	 * @return true, if successful
	 */
	public boolean deleteUserProjectRel(String id) throws UnauthorizedAccessException {
		try {
			String url = new String(this.serviceUrl + XMAdminRestClientConstants.DELETE_USER_PROJECT_REL + "/" + id);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<Object> requestEntity = new HttpEntity<Object>(this.headers);
			ResponseEntity<Boolean> responseEntity = restTemplate.exchange(url, HttpMethod.DELETE, requestEntity,
					Boolean.class);
			HttpStatus statusCode = responseEntity.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				return responseEntity.getBody();
			} else {
				LOGGER.error(
						"Error while calling XMSYSTEM delete User Project Relation REST Service, returns the status code: ", //$NON-NLS-1$
						statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException) e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM delete User Project Relation REST Service!", e); //$NON-NLS-1$
		}
		return false;
	}

	/**
	 * Delete multi user project rel.
	 *
	 * @param ids
	 *            the ids
	 * @return the user project rel response
	 * @throws UnauthorizedAccessException
	 *             the unauthorized access exception
	 */
	public UserProjectRelResponse deleteMultiUserProjectRel(Set<String> ids) throws UnauthorizedAccessException {
		try {
			String url = new String(this.serviceUrl + XMAdminRestClientConstants.DELETE_MULTI_USER_PROJECT_REL);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<Set<String>> requestEntity = new HttpEntity<Set<String>>(ids, this.headers);
			ResponseEntity<UserProjectRelResponse> responseEntity = restTemplate.exchange(url, HttpMethod.DELETE,
					requestEntity, UserProjectRelResponse.class);
			HttpStatus statusCode = responseEntity.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				UserProjectRelResponse userProjectRelResponse = responseEntity.getBody();
				return userProjectRelResponse;
			} else {
				LOGGER.error(
						"Error while calling XMSYSTEM delete Multi UserProjectRel REST Service, returns the status code: ", //$NON-NLS-1$
						statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException) e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				} else if (e instanceof HttpStatusCodeException) {
					String errorResponse = ((HttpStatusCodeException) e).getResponseBodyAsString();
					String errCode = RestClientExpHandlerUtil.handleCannotCreateObjectException(LOGGER,
							(HttpStatusCodeException) e);
					throw new CannotCreateObjectException(errorResponse, errCode);
				} else if (e instanceof ResourceAccessException) {
					throw e;
				}
			} 
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM delete Multi UserProjectRel REST Service!", e); //$NON-NLS-1$
		}
		return null;
	}
	
	/**
	 * Method for Update user project rel status.
	 *
	 * @param id
	 *            {@link String}
	 * @param status
	 *            {@link String}
	 * @return true, if successful
	 */
	public boolean updateUserProjectRelStatus(String id, String status) throws UnauthorizedAccessException {
		try {
			String url = new String(this.serviceUrl + XMAdminRestClientConstants.UPDATE_USER_PROJECT_REL_STATUS + "/"
					+ status + "/" + id);

			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<Object> requestEntity = new HttpEntity<Object>(this.headers);
			ResponseEntity<Boolean> responseEntity = restTemplate.exchange(url, HttpMethod.POST, requestEntity,
					Boolean.class);
			HttpStatus statusCode = responseEntity.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				return responseEntity.getBody();
			} else {
				LOGGER.error(
						"Error while calling XMSYSTEM Update User Project Relation Status REST Service, returns the status code: ", //$NON-NLS-1$
						statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException) e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM Update User Project Relation Status REST Service!", e); //$NON-NLS-1$
		}
		return false;
	}

	/**
	 * Multi update user project rel status.
	 *
	 * @param userProjectRelRequest the user project rel request
	 * @return the user project rel batch response
	 * @throws UnauthorizedAccessException the unauthorized access exception
	 */
	public UserProjectRelBatchResponse multiUpdateUserProjectRelStatus(
			List<UserProjectRelRequest> userProjectRelRequest) throws UnauthorizedAccessException {
		try {
			String url = new String(this.serviceUrl + XMAdminRestClientConstants.MULTI_UPDATE_USER_PROJECT_STATUS);

			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<List<UserProjectRelRequest>> requestEntity = new HttpEntity<List<UserProjectRelRequest>>(
					userProjectRelRequest, this.headers);
			ResponseEntity<UserProjectRelBatchResponse> responseEntity = restTemplate.exchange(url, HttpMethod.POST,
					requestEntity, UserProjectRelBatchResponse.class);
			HttpStatus statusCode = responseEntity.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				return responseEntity.getBody();
			} else {
				LOGGER.error(
						"Error while calling XMSYSTEM Multi Update UserProject Status REST Service, returns the status code: ", //$NON-NLS-1$
						statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException) e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}else if (e instanceof HttpStatusCodeException) {
					String errorResponse = ((HttpStatusCodeException) e).getResponseBodyAsString();
					String errCode = RestClientExpHandlerUtil.handleCannotCreateObjectException(LOGGER,
							(HttpStatusCodeException) e);
					throw new CannotCreateObjectException(errorResponse, errCode);
				} else if (e instanceof ResourceAccessException) {
					throw e;
				}
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM Multi Update UserProject Status REST Service!", e); //$NON-NLS-1$
		}
		return null;
	}
	/**
	 * Gets the user project rel by user id.
	 *
	 * @param userId
	 *            {@link String}
	 * @return the user project rel by user id
	 */
	public List<UserProjectRelTbl> getUserProjectRelByUserId(String userId) throws UnauthorizedAccessException {
		try {
			String url = new String(
					this.serviceUrl + XMAdminRestClientConstants.GET_USER_PROJECT_REL_BY_USER_ID + "/" + userId);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<Object> requestEntity = new HttpEntity<Object>(headers);
			List<UserProjectRelTbl> userProjectRelTblList = new ArrayList<>();
			List<HttpMessageConverter<?>> messageConverters = new ArrayList<>();
			messageConverters.add(new MappingJackson2HttpMessageConverter());
			restTemplate.setMessageConverters(messageConverters);

			ResponseEntity<UserProjectRelResponse> responseList = restTemplate.exchange(url, HttpMethod.GET,
					requestEntity, UserProjectRelResponse.class);
			HttpStatus statusCode = responseList.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				UserProjectRelResponse userProjectRelResponse = responseList.getBody();
				if (userProjectRelResponse != null) {
					Iterable<UserProjectRelTbl> userProjectRelTbls = userProjectRelResponse.getUserProjectRelTbls();
					for (UserProjectRelTbl userProjectRelTbl : userProjectRelTbls) {
						userProjectRelTblList.add(userProjectRelTbl);
					}
				}
				
				return RestClientSortingUtil.sortUserProjectRelByUserIdTbl(userProjectRelTblList);
			} else {
				LOGGER.error(
						"Error while calling XMSYSTEM get User Project Relation by UserId REST Service, returns the status code: ", //$NON-NLS-1$
						statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException) e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			} else if (e instanceof HttpStatusCodeException) {
				RestClientExpHandlerUtil.handleNoXMLObjectFoundException(LOGGER, (HttpStatusCodeException) e);
			} else if(e instanceof ResourceAccessException){
				throw e;
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM get User Project Relation by UserId REST Service!", e); //$NON-NLS-1$
		}
		return null;
	}

	/**
	 * Gets the user project rel by project id.
	 *
	 * @param projectId
	 *            {@link String}
	 * @return the user project rel by project id
	 */
	public List<UserProjectRelTbl> getUserProjectRelByProjectId(String projectId) throws UnauthorizedAccessException {
		try {
			String url = new String(
					this.serviceUrl + XMAdminRestClientConstants.GET_USER_PROJECT_REL_BY_PROJECT_ID + "/" + projectId);
			List<UserProjectRelTbl> userProjectRelTblList = new ArrayList<>();
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<Object> requestEntity = new HttpEntity<Object>(headers);

			List<HttpMessageConverter<?>> messageConverters = new ArrayList<>();
			messageConverters.add(new MappingJackson2HttpMessageConverter());
			restTemplate.setMessageConverters(messageConverters);

			ResponseEntity<UserProjectRelResponse> responseList = restTemplate.exchange(url, HttpMethod.GET,
					requestEntity, UserProjectRelResponse.class);
			HttpStatus statusCode = responseList.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				UserProjectRelResponse userProjectRelResponse = responseList.getBody();
				if (userProjectRelResponse != null) {
					Iterable<UserProjectRelTbl> userProjectRelTbls = userProjectRelResponse.getUserProjectRelTbls();
					for (UserProjectRelTbl userProjectRelTbl : userProjectRelTbls) {
						userProjectRelTblList.add(userProjectRelTbl);
					}
					
				}
				return RestClientSortingUtil.sortUserProjectRelByProjectIdTbl(userProjectRelTblList);
			} else {
				LOGGER.error(
						"Error while calling XMSYSTEM get User Project Relation by projectId REST Service, returns the status code: ", //$NON-NLS-1$
						statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException) e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			} else if (e instanceof HttpStatusCodeException) {
				RestClientExpHandlerUtil.handleNoXMLObjectFoundException(LOGGER, (HttpStatusCodeException) e);
			} else if(e instanceof ResourceAccessException){
				throw e;
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM get User Project Relation by projectId REST Service!", e); //$NON-NLS-1$
		}
		return null;
	}

	/**
	 * Gets the user project expiry days by user project id.
	 *
	 * @param userId
	 *            {@link String}
	 * @param projectId
	 *            {@link String}
	 * @return the user project expiry days by user project id
	 * @throws UnauthorizedAccessException
	 *             the unauthorized access exception
	 */
	public String getUserProjectExpiryDaysByUserProjectId(String userId, String projectId)
			throws UnauthorizedAccessException {
		try {
			String url = new String(this.serviceUrl + XMAdminRestClientConstants.GET_USER_PROJECT_EXPIRY_DAYS_BY_USER_PROJECT_ID + "/"
							+ userId + "/" + projectId);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<Object> requestEntity = new HttpEntity<Object>(headers);

			List<HttpMessageConverter<?>> messageConverters = new ArrayList<>();
			messageConverters.add(new MappingJackson2HttpMessageConverter());
			restTemplate.setMessageConverters(messageConverters);

			ResponseEntity<String> responseList = restTemplate.exchange(url, HttpMethod.GET, requestEntity,
					String.class);
			HttpStatus statusCode = responseList.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				String userProjectExpDays = responseList.getBody();
				return userProjectExpDays;
			} else {
				LOGGER.error(
						"Error while calling XMSYSTEM get User Project Relation Expiry Days by user project Id REST Service, returns the status code: ", //$NON-NLS-1$
						statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException) e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			} else if (e instanceof HttpStatusCodeException) {
				RestClientExpHandlerUtil.handleNoXMLObjectFoundException(LOGGER, (HttpStatusCodeException) e);
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM get Project Relation Expiry Days by user project Id by projectId REST Service!", e); //$NON-NLS-1$
		}
		return null;
	}

	/**
	 * Method for Update user project expiry days by user project rel id.
	 *
	 * @param userProjectRelId
	 *            {@link String}
	 * @param expiryDays
	 *            {@link String}
	 * @return true, if successful
	 * @throws UnauthorizedAccessException
	 *             the unauthorized access exception
	 * @throws CannotCreateObjectException
	 *             the cannot create object exception
	 */
	public boolean updateUserProjectExpiryDaysByUserProjectRelId(String userProjectRelId, String expiryDays)
			throws UnauthorizedAccessException/*, CannotCreateObjectException*/ {
		try {
			String url = new String(this.serviceUrl + XMAdminRestClientConstants.UPDATE_USER_PROJECT_EXPIRY_DAYS_BY_ID
					+ "/" + userProjectRelId + "/" + expiryDays);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<Object> requestEntity = new HttpEntity<Object>(headers);

			List<HttpMessageConverter<?>> messageConverters = new ArrayList<>();
			messageConverters.add(new MappingJackson2HttpMessageConverter());
			restTemplate.setMessageConverters(messageConverters);

			ResponseEntity<Boolean> responseList = restTemplate.exchange(url, HttpMethod.PUT, requestEntity,
					Boolean.class);
			HttpStatus statusCode = responseList.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				return responseList.getBody();
			} else {
				LOGGER.error(
						"Error while calling XMSYSTEM get User Project Relation Expiry Days by user project Id REST Service, returns the status code: ", //$NON-NLS-1$
						statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException) e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				} else if (e instanceof HttpStatusCodeException) {
					String errorResponse = ((HttpStatusCodeException) e).getResponseBodyAsString();
					String errCode = RestClientExpHandlerUtil.handleCannotCreateObjectException(LOGGER,
							(HttpStatusCodeException) e);
					throw new CannotCreateObjectException(errorResponse, errCode);
				} else if (e instanceof ResourceAccessException) {
					throw e;
				}
			} else if (e instanceof HttpStatusCodeException) {
				String errorResponse = ((HttpStatusCodeException) e).getResponseBodyAsString();
				String errCode = RestClientExpHandlerUtil.handleCannotCreateObjectException(LOGGER,
						(HttpStatusCodeException) e);
				throw new CannotCreateObjectException(errorResponse, errCode);
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM get Project Relation Expiry Days by user project Id by projectId REST Service!", e); //$NON-NLS-1$
		}
		return false;
	}
	
	/**
	 * Gets the user project rel by user id and project id.
	 *
	 * @param userId the user id
	 * @param projectId the project id
	 * @return the user project rel by user id and project id
	 */
	public UserProjectRelTbl getUserProjectRelByUserIdAndProjectId(final String userId, final String projectId) {
		try {
			String url = new String(this.serviceUrl + XMAdminRestClientConstants.GET_USER_PROJECT_REL_BY_USER_ID_AND_PROJECT_ID + "/"
							+ userId + "/" + projectId);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<Object> requestEntity = new HttpEntity<Object>(headers);

			List<HttpMessageConverter<?>> messageConverters = new ArrayList<>();
			messageConverters.add(new MappingJackson2HttpMessageConverter());
			restTemplate.setMessageConverters(messageConverters);

			ResponseEntity<UserProjectRelTbl> responseList = restTemplate.exchange(url, HttpMethod.GET, requestEntity, UserProjectRelTbl.class);
			HttpStatus statusCode = responseList.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				UserProjectRelTbl userProjectRelTbl = responseList.getBody();
				return userProjectRelTbl;
			} else {
				LOGGER.error(
						"Error while calling XMSYSTEM get User Project Relation Expiry Days by user project Id REST Service, returns the status code: ", //$NON-NLS-1$
						statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException) e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			} else if (e instanceof HttpStatusCodeException) {
				RestClientExpHandlerUtil.handleNoXMLObjectFoundException(LOGGER, (HttpStatusCodeException) e);
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM get Project Relation Expiry Days by user project Id by projectId REST Service!", e); //$NON-NLS-1$
		}
		return null;
		
	}
}
