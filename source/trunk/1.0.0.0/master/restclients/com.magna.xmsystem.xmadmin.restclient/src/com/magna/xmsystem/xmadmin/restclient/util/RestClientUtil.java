package com.magna.xmsystem.xmadmin.restclient.util;

import com.magna.xmbackend.entities.UsersTbl;
import com.magna.xmsystem.dependencies.utils.XMSystemUtil;
import com.magna.xmsystem.xmadmin.restclient.users.UserController;

/**
 * The Class RestClientUtil.
 * 
 * @author shashwat.anand
 */
public class RestClientUtil {
	/** Member variable 'this ref' for {@link RestClientUtil}. */
	private static RestClientUtil thisRef;

	/** The user name. */
	private String userName;

	/** The user id. */
	private String userId;

	/**
	 * private constructor.
	 */
	private RestClientUtil() {
		thisRef = this;
	}

	/**
	 * Gets the single instance of RestClientClient.
	 *
	 * @return single instance of RestClientClient
	 */
	public static RestClientUtil getInstance() {
		if (thisRef == null) {
			new RestClientUtil();
		}
		return thisRef;
	}

	/**
	 * Gets the user name.
	 *
	 * @return the user name
	 * @throws Exception the exception
	 */
	public String getUserName() throws Exception {
		this.userName = XMSystemUtil.getSystemUserName();
		return this.userName;
	}

	/**
	 * Gets the user id.
	 *
	 * @return the userId
	 * @throws Exception
	 *             the exception
	 */
	public String getUserId() throws Exception {
		if (XMSystemUtil.isEmpty(this.userId) || !XMSystemUtil.getSystemUserName().equals(this.userName)) {
			String uName;
			if ((uName = XMSystemUtil.getSystemUserName()) != null) {
				UserController controller = new UserController();
				final UsersTbl userByName = controller.getUserByName(uName);
				if (userByName == null) {
					throw new Exception("User with " + uName + " not present in database");
				}
				this.userName = userByName.getUsername();
				this.userId = userByName.getUserId();
			}
		}
		return this.userId;
	}

}

