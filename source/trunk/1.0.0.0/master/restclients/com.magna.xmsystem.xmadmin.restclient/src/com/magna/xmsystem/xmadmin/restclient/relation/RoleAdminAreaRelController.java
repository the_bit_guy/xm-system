package com.magna.xmsystem.xmadmin.restclient.relation;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import com.magna.xmbackend.entities.RoleAdminAreaRelTbl;
import com.magna.xmbackend.exception.CannotCreateObjectException;
import com.magna.xmbackend.vo.rel.RoleAdminAreaRelRequest;
import com.magna.xmbackend.vo.rel.RoleAdminAreaRelResponse;
import com.magna.xmsystem.dependencies.customExceptions.UnauthorizedAccessException;
import com.magna.xmsystem.xmadmin.restclient.RestController;
import com.magna.xmsystem.xmadmin.restclient.site.SiteController;
import com.magna.xmsystem.xmadmin.restclient.util.RestClientExpHandlerUtil;
import com.magna.xmsystem.xmadmin.restclient.util.RestClientSortingUtil;
import com.magna.xmsystem.xmadmin.restclient.util.XMAdminRestClientConstants;

// TODO: Auto-generated Javadoc
/**
 * Class for Site admin area rel controller.
 *
 * @author Roshan.Ekka
 */
public class RoleAdminAreaRelController extends RestController {
	/** The Constant LOGGER. */
	private static final Logger LOGGER = LoggerFactory.getLogger(SiteController.class);

	/**
	 * Constructor for SiteController Class.
	 */
	public RoleAdminAreaRelController() {
		super();
	}

	/**
	 * Gets the all role AA rel by role id.
	 *
	 * @param roleId the role id
	 * @return the all role AA rel by role id
	 * @throws UnauthorizedAccessException the unauthorized access exception
	 */
	public Iterable<RoleAdminAreaRelTbl> getAllRoleAARelByRoleId(final String roleId)
			throws UnauthorizedAccessException {
		try {
			String url = new String(
					this.serviceUrl + XMAdminRestClientConstants.GET_ROLE_ADMIN_AREA_REL_BY_ROLE_ID + "/" + roleId);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<Object> requestEntity = new HttpEntity<Object>(headers);

			List<HttpMessageConverter<?>> messageConverters = new ArrayList<>();
			messageConverters.add(new MappingJackson2HttpMessageConverter());
			restTemplate.setMessageConverters(messageConverters);

			ResponseEntity<RoleAdminAreaRelResponse> responseList = restTemplate.exchange(url, HttpMethod.GET,
					requestEntity, RoleAdminAreaRelResponse.class);
			HttpStatus statusCode = responseList.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				RoleAdminAreaRelResponse roleAdminAreaRelResponse = responseList.getBody();
				List<RoleAdminAreaRelTbl> RoleAdminAreaRelTblList = new ArrayList<>();
				Iterable<RoleAdminAreaRelTbl> roleAdminAreaRelTbls = roleAdminAreaRelResponse.getRoleAdminAreaRelTbls();
				for (RoleAdminAreaRelTbl roleAdminAreaRelTbl : roleAdminAreaRelTbls) {
					RoleAdminAreaRelTblList.add(roleAdminAreaRelTbl);
				}
				return RestClientSortingUtil.sortRoleAdminAreaRelTbl(RoleAdminAreaRelTblList);
			} else {
				LOGGER.error(
						"Error while calling XMSYSTEM Get All Role Admin Area Relation By RoleId REST Service, returns the status code: " //$NON-NLS-1$
								+ statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException) e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			} else if (e instanceof HttpStatusCodeException) {
				RestClientExpHandlerUtil.handleNoXMLObjectFoundException(LOGGER, (HttpStatusCodeException) e);
			} else if(e instanceof ResourceAccessException){
				throw e;
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM Get All Role Admin Area Relation By RoleId REST Service", e); //$NON-NLS-1$
		}
		return null;
	}

	/**
	 * Method for Creates the role admin area rel.
	 *
	 * @param roleAdminAreaRelRequest            {@link List<RoleAdminAreaRelRequest>}
	 * @return the role admin area rel response {@link RoleAdminAreaRelResponse}
	 * @throws UnauthorizedAccessException the unauthorized access exception
	 * @throws CannotCreateObjectException the cannot create object exception
	 */
	public RoleAdminAreaRelResponse createRoleAdminAreaRel(List<RoleAdminAreaRelRequest> roleAdminAreaRelRequest)
			throws UnauthorizedAccessException, CannotCreateObjectException {
		try {
			String url = new String(this.serviceUrl + XMAdminRestClientConstants.CREATE_ROLE_ADMIN_AREA_REL);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<List<RoleAdminAreaRelRequest>> request = new HttpEntity<List<RoleAdminAreaRelRequest>>(
					roleAdminAreaRelRequest, this.headers);
			ResponseEntity<RoleAdminAreaRelResponse> responseEntity = restTemplate.postForEntity(url, request,
					RoleAdminAreaRelResponse.class);
			HttpStatus statusCode = responseEntity.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				RoleAdminAreaRelResponse roleAdminAreaRelResponse = responseEntity.getBody();
				return roleAdminAreaRelResponse;
			} else {
				LOGGER.error(
						"Error while calling XMSYSTEM Create Role Administration Area Relation REST Service, returns the status code: ", //$NON-NLS-1$
						statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException) e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			} else if (e instanceof HttpStatusCodeException) {
				String errorResponse = ((HttpStatusCodeException) e).getResponseBodyAsString();
				String errCode = RestClientExpHandlerUtil.handleCannotCreateObjectException(LOGGER,
						(HttpStatusCodeException) e);
				throw new CannotCreateObjectException(errorResponse, errCode);
			} else if (e instanceof ResourceAccessException) {
				throw e;
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM Create Role Administration Area Relation REST Service", e); //$NON-NLS-1$
		}
		return null;
	}

	/**
	 * Method for Delete role admin area rel.
	 *
	 * @param roleAARelId            {@link String}
	 * @return true, if successful
	 * @throws UnauthorizedAccessException the unauthorized access exception
	 */
	public boolean deleteRoleAdminAreaRel(String roleAARelId) throws UnauthorizedAccessException {
		try {
			String url = new String(
					this.serviceUrl + XMAdminRestClientConstants.DELETE_ROLE_AA_REL + "/" + roleAARelId);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<Object> requestEntity = new HttpEntity<Object>(this.headers);
			ResponseEntity<Boolean> responseEntity = restTemplate.exchange(url, HttpMethod.DELETE, requestEntity,
					Boolean.class);
			HttpStatus statusCode = responseEntity.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				return responseEntity.getBody();
			} else {
				LOGGER.error(
						"Error while calling XMSYSTEM delete RoleAARelation by roleAARelationId REST Service, returns the status code: ", //$NON-NLS-1$
						statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException) e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM delete RoleAARelation by roleAARelationId REST Service!", e); //$NON-NLS-1$
		}
		return false;
	}
	
	/**
	 * Delete multi role admin area rel.
	 *
	 * @param ids the ids
	 * @return the role admin area rel response
	 * @throws UnauthorizedAccessException the unauthorized access exception
	 */
	public RoleAdminAreaRelResponse deleteMultiRoleAdminAreaRel(Set<String> ids) throws UnauthorizedAccessException {
		try {
			String url = new String(this.serviceUrl + XMAdminRestClientConstants.DELETE_MULTI_ROLE_AA_REL);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<Set<String>> requestEntity = new HttpEntity<Set<String>>(ids, this.headers);
			ResponseEntity<RoleAdminAreaRelResponse> responseEntity = restTemplate.exchange(url, HttpMethod.DELETE,
					requestEntity, RoleAdminAreaRelResponse.class);
			HttpStatus statusCode = responseEntity.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				RoleAdminAreaRelResponse roleAdminAreaRelResponse = responseEntity.getBody();
				return roleAdminAreaRelResponse;
			} else {
				LOGGER.error(
						"Error while calling XMSYSTEM delete Multi RoleAdminAreaRel REST Service, returns the status code: ", //$NON-NLS-1$
						statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException) e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM delete Multi RoleAdminAreaRel REST Service!", e); //$NON-NLS-1$
		}
		return null;
	}

	/**
	 * Gets the role AA rel by id.
	 *
	 * @param roleAARelId            {@link String}
	 * @return the role AA rel by id
	 * @throws UnauthorizedAccessException the unauthorized access exception
	 */
	public RoleAdminAreaRelTbl getRoleAARelById(String roleAARelId) throws UnauthorizedAccessException {
		try {
			String url = new String(
					this.serviceUrl + XMAdminRestClientConstants.GET_ROLE_AA_REL_BY_ID + "/" + roleAARelId);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<Object> requestEntity = new HttpEntity<Object>(headers);

			List<HttpMessageConverter<?>> messageConverters = new ArrayList<>();
			messageConverters.add(new MappingJackson2HttpMessageConverter());
			restTemplate.setMessageConverters(messageConverters);

			ResponseEntity<RoleAdminAreaRelTbl> responseList = restTemplate.exchange(url, HttpMethod.GET, requestEntity,
					RoleAdminAreaRelTbl.class);
			HttpStatus statusCode = responseList.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				RoleAdminAreaRelTbl roleAdminAreaRelTbl = responseList.getBody();
				return roleAdminAreaRelTbl;
			} else {
				LOGGER.error(
						"Error while calling XMSYSTEM Get RoleAARelation by Id REST Service, returns the status code: " //$NON-NLS-1$
								+ statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException) e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			} else if (e instanceof HttpStatusCodeException) {
				RestClientExpHandlerUtil.handleNoXMLObjectFoundException(LOGGER, (HttpStatusCodeException) e);
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM Get RoleAARelation by Id REST Service", e); //$NON-NLS-1$
		}
		return null;
	}
}
