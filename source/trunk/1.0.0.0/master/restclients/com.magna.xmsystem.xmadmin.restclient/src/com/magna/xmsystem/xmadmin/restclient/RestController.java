package com.magna.xmsystem.xmadmin.restclient;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;

import com.magna.xmsystem.dependencies.utils.XMSystemUtil;
import com.magna.xmsystem.xmadmin.restclient.util.RestClientUtil;

/**
 * Class for Rest controller.
 *
 * @author Chiranjeevi.Akula
 */
public abstract class RestController {
	
	/** The Constant LOGGER. */
	private static final Logger LOGGER = LoggerFactory.getLogger(RestController.class);	
	
	/** Member variable 'service url' for {@link String}. */
	public String serviceUrl;

	/** Member variable 'headers' for {@link HttpHeaders}. */
	public HttpHeaders headers;
	
	/**
	 * Constructor for RestController Class.
	 */
	public RestController() {
		this.serviceUrl = XMSystemUtil.getXMBackendUrl();
		this.headers = new HttpHeaders();
		this.headers.setContentType(MediaType.APPLICATION_JSON);
		initHeaderParams();
	}

	/**
	 * Constructor for RestController Class.
	 *
	 * @param adminAreaId {@link String}
	 */
	public RestController(String adminAreaId) {
		this.serviceUrl = XMSystemUtil.getXMBackendUrl();

		this.headers = new HttpHeaders();
		this.headers.setContentType(MediaType.APPLICATION_JSON);
		initHeaderParams(adminAreaId);
	}

	/**
	 * Method for Inits the header params.`
	 */
	private void initHeaderParams() {		
		try {
			String userName = RestClientUtil.getInstance().getUserName();				
			this.headers.set("USER_NAME", userName);
			final String ticket = XMSystemUtil.getInstance().getTicket();
			this.headers.set("TKT", ticket);
		} catch (Exception e) {
			LOGGER.error("Error while initiating the REST Service", e); //$NON-NLS-1$
		}
	}
	
	/**
	 * Method for Inits the header params.
	 *
	 * @param adminAreaId {@link String}
	 */
	private void initHeaderParams(String adminAreaId) {
		try {
			initHeaderParams();
			if(adminAreaId != null && !adminAreaId.isEmpty()) {
				this.headers.set("ADMIN_AREA_ID", adminAreaId);
			}
		} catch (Exception e) {
			LOGGER.error("Error while initiating the REST Service", e); //$NON-NLS-1$
		}		
	}
}
