package com.magna.xmsystem.xmadmin.restclient.util;

import org.slf4j.Logger;
import org.springframework.web.client.HttpStatusCodeException;

import com.google.gson.Gson;
import com.magna.xmbackend.exception.CannotCreateObjectException;
import com.magna.xmbackend.exception.XMObjectNotFoundException;

/**
 * The Class RestClientExpHandlerUtil.
 * 
 * @author shashwat.anand
 */
public class RestClientExpHandlerUtil {
	
	/**
	 * Handle no XML object found exception.
	 *
	 * @param logger the logger
	 * @param hscExp the hsc exp
	 */
	public static void handleNoXMLObjectFoundException(final org.slf4j.Logger logger, final HttpStatusCodeException hscExp) {
		String errorResponse = ((HttpStatusCodeException) hscExp).getResponseBodyAsString();
		Gson gson = new Gson();
		XMObjectNotFoundException objNotFoundExp = gson.fromJson(errorResponse, XMObjectNotFoundException.class);
		logger.info("No object found : " + objNotFoundExp.getErrCode() + " : " + hscExp);
	}

	public static String handleCannotCreateObjectException(Logger logger, HttpStatusCodeException hscExp) {
		String errorResponse = ((HttpStatusCodeException) hscExp).getResponseBodyAsString();
		Gson gson = new Gson();
		CannotCreateObjectException cannotCreateObjException = gson.fromJson(errorResponse, CannotCreateObjectException.class);
		String errCode = cannotCreateObjException.getErrCode();
		logger.info("Cannot create object : " + errCode + " : " + hscExp);
		return errCode;
	}
}
