package com.magna.xmsystem.xmadmin.restclient.application;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import com.magna.xmbackend.entities.StartApplicationsTbl;
import com.magna.xmbackend.exception.CannotCreateObjectException;
import com.magna.xmbackend.vo.startApplication.StartApplicationRequest;
import com.magna.xmbackend.vo.startApplication.StartApplicationResponse;
import com.magna.xmsystem.dependencies.customExceptions.UnauthorizedAccessException;
import com.magna.xmsystem.xmadmin.restclient.RestController;
import com.magna.xmsystem.xmadmin.restclient.util.RestClientExpHandlerUtil;
import com.magna.xmsystem.xmadmin.restclient.util.RestClientSortingUtil;
import com.magna.xmsystem.xmadmin.restclient.util.XMAdminRestClientConstants;

/**
 * Class for Start app controller.
 *
 * @author Roshan.Ekka
 */
public class StartAppController extends RestController {

	/** The Constant LOGGER. */
	private static final Logger LOGGER = LoggerFactory.getLogger(StartAppController.class);

	/**
	 * Constructor for BaseAppController Class.
	 */

	public StartAppController() {
		super();
	}

	/**
	 * Method for Creates the start application.
	 *
	 * @param startAppRequest
	 *            {@link StartApplicationRequest}
	 * @return the start applications tbl {@link StartApplicationsTbl}
	 */
	public StartApplicationsTbl createStartApplication(StartApplicationRequest startAppRequest) throws UnauthorizedAccessException, CannotCreateObjectException {
		try {
			String url = new String(this.serviceUrl + XMAdminRestClientConstants.CREATE_START_APPLICATION);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<StartApplicationRequest> request = new HttpEntity<StartApplicationRequest>(startAppRequest,
					this.headers);
			ResponseEntity<StartApplicationsTbl> responseEntity = restTemplate.postForEntity(url, request,
					StartApplicationsTbl.class);
			HttpStatus statusCode = responseEntity.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				StartApplicationsTbl startApplicationsTbl = responseEntity.getBody();
				return startApplicationsTbl;
			} else {
				LOGGER.error(
						"Error while calling XMSYSTEM Create Start Application REST Service, returns the status code: ", //$NON-NLS-1$
						statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException)e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			} else if (e instanceof HttpStatusCodeException) {
				String errorResponse = ((HttpStatusCodeException) e).getResponseBodyAsString();
				String errCode = RestClientExpHandlerUtil.handleCannotCreateObjectException(LOGGER, (HttpStatusCodeException) e);
				throw new CannotCreateObjectException(errorResponse, errCode);
			}else if (e instanceof ResourceAccessException) {
				throw e;
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM Create Start Application REST Service", e); //$NON-NLS-1$
		}
		return null;
	}

	/**
	 * Gets the all start apps.
	 *
	 * @param isSorted
	 *            {@link boolean}
	 * @return the all start apps
	 */
	public List<StartApplicationsTbl> getAllStartApps(final boolean isSorted) throws UnauthorizedAccessException {
		try {
			String url = new String(this.serviceUrl + XMAdminRestClientConstants.GET_START_APPLICATIONS);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<Object> requestEntity = new HttpEntity<Object>(headers);

			List<HttpMessageConverter<?>> messageConverters = new ArrayList<>();
			messageConverters.add(new MappingJackson2HttpMessageConverter());
			restTemplate.setMessageConverters(messageConverters);

			ResponseEntity<StartApplicationResponse> responseList = restTemplate.exchange(url, HttpMethod.GET,
					requestEntity, StartApplicationResponse.class);
			HttpStatus statusCode = responseList.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				StartApplicationResponse startAppResponse = responseList.getBody();
				List<StartApplicationsTbl> startAppTblsList = (List<StartApplicationsTbl>) startAppResponse
						.getStartApplicationsTbls();
			
				return RestClientSortingUtil.sortStartAppTbl(startAppTblsList);
			} else {
				LOGGER.error(
						"Error while calling XMSYSTEM Get All Start Applicatios REST Service, returns the status code: " //$NON-NLS-1$
								+ statusCode.value());
			}
		}  catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException)e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			} else if (e instanceof HttpStatusCodeException) {
				RestClientExpHandlerUtil.handleNoXMLObjectFoundException(LOGGER, (HttpStatusCodeException) e);
			} else if(e instanceof ResourceAccessException){
				throw e;
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM Get All Start Applications REST Service", e); //$NON-NLS-1$
		}
		return null;
	}

	/**
	 * Method for Delete start application.
	 *
	 * @param startApplicationId
	 *            {@link String}
	 * @return true, if successful
	 */
	public boolean deleteStartApplication(String startApplicationId) throws UnauthorizedAccessException {
		try {
			String url = new String(
					this.serviceUrl + XMAdminRestClientConstants.DELETE_START_APPLICATION + "/" + startApplicationId);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<Object> requestEntity = new HttpEntity<Object>(this.headers);
			ResponseEntity<Boolean> responseEntity = restTemplate.exchange(url, HttpMethod.DELETE, requestEntity,
					Boolean.class);
			HttpStatus statusCode = responseEntity.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				return responseEntity.getBody();
			} else {
				LOGGER.error(
						"Error while calling XMSYSTEM delete Start Application REST Service, returns the status code: ", //$NON-NLS-1$
						statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException)e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM delete Start Application REST Service!", e); //$NON-NLS-1$
		}
		return false;
	}

	/**
	 * Delete multi start app.
	 *
	 * @param startAppIds the start app ids
	 * @return the start application response
	 * @throws UnauthorizedAccessException the unauthorized access exception
	 */
	public StartApplicationResponse deleteMultiStartApp(Set<String> startAppIds) throws UnauthorizedAccessException {
		try {
			String url = new String(this.serviceUrl + XMAdminRestClientConstants.DELETE_MULTI_START_APP);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<Set<String>> requestEntity = new HttpEntity<Set<String>>(startAppIds,this.headers);
			ResponseEntity<StartApplicationResponse> responseEntity = restTemplate.exchange(url, HttpMethod.DELETE, requestEntity,
					StartApplicationResponse.class);
			HttpStatus statusCode = responseEntity.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				StartApplicationResponse startApplicationResponse = responseEntity.getBody();
				return startApplicationResponse;
			} else {
				LOGGER.error("Error while calling XMSYSTEM delete Multi StartApp REST Service, returns the status code: ", //$NON-NLS-1$
						statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException)e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM delete Multi StartApp REST Service!", e); //$NON-NLS-1$
		}
		return null;
	}
	
	/**
	 * Method for Update start application.
	 *
	 * @param startAppRequest
	 *            {@link StartApplicationRequest}
	 * @return true, if successful
	 */
	public boolean updateStartApplication(StartApplicationRequest startAppRequest) throws UnauthorizedAccessException, CannotCreateObjectException {
		try {
			String url = new String(this.serviceUrl + XMAdminRestClientConstants.UPDATE_START_APPLICATION);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<StartApplicationRequest> request = new HttpEntity<StartApplicationRequest>(startAppRequest,
					this.headers);
			ResponseEntity<StartApplicationsTbl> responseEntity = restTemplate.postForEntity(url, request,
					StartApplicationsTbl.class);
			HttpStatus statusCode = responseEntity.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				return true;
			} else {
				LOGGER.error(
						"Error while calling XMSYSTEM Upadte Start Application REST Service, returns the status code: ", //$NON-NLS-1$
						statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException)e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			} else if (e instanceof HttpStatusCodeException) {
				String errorResponse = ((HttpStatusCodeException) e).getResponseBodyAsString();
				String errCode = RestClientExpHandlerUtil.handleCannotCreateObjectException(LOGGER, (HttpStatusCodeException) e);
				throw new CannotCreateObjectException(errorResponse, errCode);
			}else if (e instanceof ResourceAccessException) {
				throw e;
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM Upadte Start Application REST Service", e); //$NON-NLS-1$
		}
		return false;

	}

	public boolean updateStartAppStatus(String startApplicationId, String status) throws UnauthorizedAccessException {
		try {
			String url = new String(this.serviceUrl + XMAdminRestClientConstants.UPDATE_START_APPLICATION_STATUS + "/"
					+ status + "/" + startApplicationId);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<Object> requestEntity = new HttpEntity<Object>(this.headers);
			ResponseEntity<Boolean> responseEntity = restTemplate.exchange(url, HttpMethod.POST, requestEntity,
					Boolean.class);
			HttpStatus statusCode = responseEntity.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				return responseEntity.getBody();
			} else {
				LOGGER.error(
						"Error while calling XMSYSTEM Update Start Application Status REST Service, returns the status code: ", //$NON-NLS-1$
						statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException)e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM Update Start Application Status REST Service!", e); //$NON-NLS-1$
		}
		return false;
	}
	
	/**
	 * Multi update start app status.
	 *
	 * @param baseAppRequest the base app request
	 * @return the start application response
	 * @throws UnauthorizedAccessException the unauthorized access exception
	 */
	public StartApplicationResponse multiUpdateStartAppStatus(List<StartApplicationRequest> baseAppRequest) throws UnauthorizedAccessException {
		try {
			String url = new String(
					this.serviceUrl + XMAdminRestClientConstants.MULTI_UPDATE_START_APP_STATUS );

			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<List<StartApplicationRequest>> requestEntity = new HttpEntity<List<StartApplicationRequest>>(baseAppRequest, this.headers);
			ResponseEntity<StartApplicationResponse> responseEntity = restTemplate.exchange(url, HttpMethod.POST, requestEntity,
					StartApplicationResponse.class);
			HttpStatus statusCode = responseEntity.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				return responseEntity.getBody();
			} else {
				LOGGER.error("Error while calling XMSYSTEM Multi Update StartApp Status REST Service, returns the status code: ", //$NON-NLS-1$
						statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException)e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM Multi Update StartApp Status REST Service!", e); //$NON-NLS-1$
		}
		return null;
	}

	/**
	 * Gets the start app by id.
	 *
	 * @param startAppId
	 *            {@link String}
	 * @return the start app by id
	 */
	public StartApplicationsTbl getStartAppById(String startAppId) throws UnauthorizedAccessException {
		try {
			String url = new String(
					this.serviceUrl + XMAdminRestClientConstants.GET_START_APP_BY_ID + "/" + startAppId);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<Object> requestEntity = new HttpEntity<Object>(headers);

			List<HttpMessageConverter<?>> messageConverters = new ArrayList<>();
			messageConverters.add(new MappingJackson2HttpMessageConverter());
			restTemplate.setMessageConverters(messageConverters);

			ResponseEntity<StartApplicationsTbl> responseList = restTemplate.exchange(url, HttpMethod.GET,
					requestEntity, StartApplicationsTbl.class);
			HttpStatus statusCode = responseList.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				StartApplicationsTbl startAppRespObj = responseList.getBody();
				return startAppRespObj;
			} else {
				LOGGER.error(
						"Error while calling XMSYSTEM Get Start Application By Id REST Service, returns the status code: " //$NON-NLS-1$
								+ statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException)e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			} else if (e instanceof HttpStatusCodeException) {
				RestClientExpHandlerUtil.handleNoXMLObjectFoundException(LOGGER, (HttpStatusCodeException) e);
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM Get All Sites By ProjectAppId REST Service", e); //$NON-NLS-1$
		}
		return null;
	}

	/**
	 * Gets the start app by base app id.
	 *
	 * @param baseAppId
	 *            {@link String}
	 * @return the start app by base app id
	 */
	public List<StartApplicationsTbl> getStartAppByBaseAppId(String baseAppId) throws UnauthorizedAccessException {
		try {
			String url = new String(
					this.serviceUrl + XMAdminRestClientConstants.GET_START_APP_BY_BASE_APP_ID + "/" + baseAppId);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<Object> requestEntity = new HttpEntity<Object>(headers);
			List<StartApplicationsTbl> baseStartAppRelTblList = new ArrayList<>();

			List<HttpMessageConverter<?>> messageConverters = new ArrayList<>();
			messageConverters.add(new MappingJackson2HttpMessageConverter());
			restTemplate.setMessageConverters(messageConverters);

			ResponseEntity<StartApplicationResponse> responseList = restTemplate.exchange(url, HttpMethod.GET,
					requestEntity, StartApplicationResponse.class);
			HttpStatus statusCode = responseList.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				StartApplicationResponse startAppRespObj = responseList.getBody();
				Iterable<StartApplicationsTbl> startApplicationsTbls = startAppRespObj.getStartApplicationsTbls();
				for (StartApplicationsTbl siteAdminTbl : startApplicationsTbls) {
					baseStartAppRelTblList.add(siteAdminTbl);
				}
				List<StartApplicationsTbl> sortStartAppTbl = RestClientSortingUtil.sortStartAppTbl(baseStartAppRelTblList);
				/*Collections.sort(baseStartAppRelTblList,(StartApplicationsTbl sar1, StartApplicationsTbl sar2) -> {
							Collection<StartAppTranslationTbl> adminAreaStartAppRelTblCollection1 = sar1.getStartAppTranslationTblCollection();
							Collection<StartAppTranslationTbl> adminAreaStartAppRelTblCollection2 = sar2.getStartAppTranslationTblCollection();

							Collection<com.magna.xmbackend.entities.StartAppTranslationTbl> adminAreaTranslation1;
							Collection<com.magna.xmbackend.entities.StartAppTranslationTbl> adminAreaTranslation2;
							if ((adminAreaTranslation1 = adminAreaStartAppRelTblCollection1) != null
									&& (adminAreaTranslation2 = adminAreaStartAppRelTblCollection2) != null) {
								String adminAreaName1 = getNameByLanguageBaseStartApp(LANG_ENUM.ENGLISH,
										adminAreaTranslation1);
								String adminAreaName2 = getNameByLanguageBaseStartApp(LANG_ENUM.ENGLISH,
										adminAreaTranslation2);
								return adminAreaName1.compareTo(adminAreaName2);
							}
							return 0;
						});*/
				
				return sortStartAppTbl;
			} else {
				LOGGER.error(
						"Error while calling XMSYSTEM Get Start Application By BaseAppId REST Service, returns the status code: " //$NON-NLS-1$
								+ statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException)e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			} else if (e instanceof HttpStatusCodeException) {
				RestClientExpHandlerUtil.handleNoXMLObjectFoundException(LOGGER, (HttpStatusCodeException) e);
			} else if(e instanceof ResourceAccessException){
				throw e;
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM Get All Sites By BaseAppId REST Service", e); //$NON-NLS-1$
		}
		return null;
	}

	/**
	 * Gets the user start applications.
	 *
	 * @return the user start applications
	 * @throws UnauthorizedAccessException the unauthorized access exception
	 */
	public List<StartApplicationsTbl> getUserStartApplications() throws UnauthorizedAccessException {
		try {
			String url = new String(this.serviceUrl + XMAdminRestClientConstants.GET_USER_START_APPS);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<Object> requestEntity = new HttpEntity<Object>(headers);

			List<HttpMessageConverter<?>> messageConverters = new ArrayList<>();
			messageConverters.add(new MappingJackson2HttpMessageConverter());
			restTemplate.setMessageConverters(messageConverters);

			ResponseEntity<StartApplicationResponse> responseList = restTemplate.exchange(url, HttpMethod.GET,
					requestEntity, StartApplicationResponse.class);
			HttpStatus statusCode = responseList.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				StartApplicationResponse startApplicationResponse = responseList.getBody();
				Iterable<StartApplicationsTbl> startApplicationsTbls = startApplicationResponse.getStartApplicationsTbls();
				if(startApplicationsTbls != null) {
					List<StartApplicationsTbl> startAppTblsList = (List<StartApplicationsTbl>) startApplicationsTbls;
					return RestClientSortingUtil.sortStartAppTbl(startAppTblsList);
				}				
			} else {
				LOGGER.error(
						"Error while calling XMSYSTEM Get User Start Application REST Service, returns the status code: " //$NON-NLS-1$
								+ statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException)e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			} else if (e instanceof HttpStatusCodeException) {
				RestClientExpHandlerUtil.handleNoXMLObjectFoundException(LOGGER, (HttpStatusCodeException) e);
			} else if (e instanceof ResourceAccessException) {
				throw e;
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM Get User Start Application REST Service", e); //$NON-NLS-1$
		}
		return null;
	}

	/**
	 * Gets the start apps by AA id project id.
	 *
	 * @param adminAreaId {@link String}
	 * @param projectId {@link String}
	 * @return the start apps by AA id project id
	 * @throws UnauthorizedAccessException the unauthorized access exception
	 */
	public List<StartApplicationsTbl> getStartAppsByAAIdProjectId(String adminAreaId, String projectId) throws UnauthorizedAccessException {
		try {
			String url = new String(
					this.serviceUrl + XMAdminRestClientConstants.GET_START_APPS_BY_ADMIN_AREA_ID_PROJECT_ID + "/" + adminAreaId + "/" + projectId);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<Object> requestEntity = new HttpEntity<Object>(headers);
			List<HttpMessageConverter<?>> messageConverters = new ArrayList<>();
			messageConverters.add(new MappingJackson2HttpMessageConverter());
			restTemplate.setMessageConverters(messageConverters);
			ResponseEntity<StartApplicationResponse> responseList = restTemplate.exchange(url, HttpMethod.GET,
					requestEntity, StartApplicationResponse.class);
			HttpStatus statusCode = responseList.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				StartApplicationResponse startApplicationResponse = responseList.getBody();
				Iterable<StartApplicationsTbl> startApplicationsTbls = startApplicationResponse.getStartApplicationsTbls();
				if(startApplicationsTbls != null) {
					List<StartApplicationsTbl> startAppTblsList = (List<StartApplicationsTbl>) startApplicationsTbls;
					
					return RestClientSortingUtil.sortStartAppTbl(startAppTblsList);
				}
			} else {
				LOGGER.error(
						"Error while calling XMSYSTEM Get Start Application By AdminArea Id and ProjectId REST Service, returns the status code: " //$NON-NLS-1$
								+ statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException)e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			} else if (e instanceof HttpStatusCodeException) {
				RestClientExpHandlerUtil.handleNoXMLObjectFoundException(LOGGER, (HttpStatusCodeException) e);
			} else if (e instanceof ResourceAccessException) {
				throw e;
			}
		} catch (Exception e) {
			LOGGER.error(
					"Error while calling XMSYSTEM Get Start Application By AdminArea Id and ProjectId REST Service", e); //$NON-NLS-1$
		}
		return null;
	}
	
	/**
	 * Gets the start app by AA id.
	 *
	 * @param adminAreaId {@link String}
	 * @return the start app by AA id
	 * @throws UnauthorizedAccessException the unauthorized access exception
	 */
	public List<StartApplicationsTbl> getStartAppByAAId(String adminAreaId) throws UnauthorizedAccessException{
		try {
			String url = new String(
					this.serviceUrl + XMAdminRestClientConstants.GET_USER_APP_BY_AA_ID + "/" + adminAreaId);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<Object> requestEntity = new HttpEntity<Object>(headers);
			List<HttpMessageConverter<?>> messageConverters = new ArrayList<>();
			messageConverters.add(new MappingJackson2HttpMessageConverter());
			restTemplate.setMessageConverters(messageConverters);
			ResponseEntity<StartApplicationResponse> responseList = restTemplate.exchange(url, HttpMethod.GET,
					requestEntity, StartApplicationResponse.class);
			HttpStatus statusCode = responseList.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				StartApplicationResponse startApplicationResponse = responseList.getBody();
				Iterable<StartApplicationsTbl> startApplicationsTbls = startApplicationResponse.getStartApplicationsTbls();
				if(startApplicationsTbls != null) {
					List<StartApplicationsTbl> startAppTblsList = (List<StartApplicationsTbl>) startApplicationsTbls;
					return RestClientSortingUtil.sortStartAppTbl(startAppTblsList);
				}
			} else {
				LOGGER.error(
						"Error while calling XMSYSTEM Get Start Application By AdminArea Id REST Service, returns the status code: " //$NON-NLS-1$
								+ statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException)e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			} else if (e instanceof HttpStatusCodeException) {
				RestClientExpHandlerUtil.handleNoXMLObjectFoundException(LOGGER, (HttpStatusCodeException) e);
			} else if (e instanceof ResourceAccessException) {
				throw e;
			}
		} catch (Exception e) {
			LOGGER.error(
					"Error while calling XMSYSTEM Get All Start Application by AdminArea Id REST Service", e); //$NON-NLS-1$
		}
		
		return null;
	}
	
	/**
	 * Gets the start app by project id.
	 *
	 * @param projectId
	 *            the project id
	 * @return the start app by project id
	 */
	public StartApplicationResponse getStartAppByProjectId(String projectId) throws UnauthorizedAccessException {
		try {
			String url = new String(
					this.serviceUrl + XMAdminRestClientConstants.GET_USER_APP_BY_PROJECT_ID + "/" + projectId);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<Object> requestEntity = new HttpEntity<Object>(headers);
			List<HttpMessageConverter<?>> messageConverters = new ArrayList<>();
			messageConverters.add(new MappingJackson2HttpMessageConverter());
			restTemplate.setMessageConverters(messageConverters);
			ResponseEntity<StartApplicationResponse> responseList = restTemplate.exchange(url, HttpMethod.GET,
					requestEntity, StartApplicationResponse.class);
			HttpStatus statusCode = responseList.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				StartApplicationResponse startAppRespObj = responseList.getBody();
				return startAppRespObj;
			} else {
				LOGGER.error(
						"Error while calling XMSYSTEM Get Start Application By Project Id REST Service, returns the status code: " //$NON-NLS-1$
								+ statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException)e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			} else if (e instanceof HttpStatusCodeException) {
				RestClientExpHandlerUtil.handleNoXMLObjectFoundException(LOGGER, (HttpStatusCodeException) e);
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM Get All Start Application by Project Id REST Service", e); //$NON-NLS-1$
		}

		return null;
	}
}
