package com.magna.xmsystem.xmadmin.restclient.relation;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import com.magna.xmbackend.entities.UserStartAppRelTbl;
import com.magna.xmbackend.exception.CannotCreateObjectException;
import com.magna.xmbackend.response.rel.userstartapp.UserStartAppRelResponseWrapper;
import com.magna.xmbackend.response.rel.userstartapp.UserStartAppRelation;
import com.magna.xmbackend.vo.rel.UserStartAppRelBatchRequest;
import com.magna.xmbackend.vo.rel.UserStartAppRelBatchResponse;
import com.magna.xmbackend.vo.rel.UserStartAppRelRequest;
import com.magna.xmbackend.vo.rel.UserStartAppRelResponse;
import com.magna.xmsystem.dependencies.customExceptions.UnauthorizedAccessException;
import com.magna.xmsystem.xmadmin.restclient.RestController;
import com.magna.xmsystem.xmadmin.restclient.site.SiteController;
import com.magna.xmsystem.xmadmin.restclient.util.RestClientExpHandlerUtil;
import com.magna.xmsystem.xmadmin.restclient.util.RestClientSortingUtil;
import com.magna.xmsystem.xmadmin.restclient.util.XMAdminRestClientConstants;

// TODO: Auto-generated Javadoc
/**
 * The Class UserStartAppRelController.
 */
public class UserStartAppRelController extends RestController {
	/** The Constant LOGGER. */
	private static final Logger LOGGER = LoggerFactory.getLogger(SiteController.class);

	/**
	 * Constructor for SiteController Class.
	 */
	public UserStartAppRelController() {
		super();
	}

	/**
	 * Method for Creates the user start app rel.
	 *
	 * @param userStartAppRelRequest            {@link UserStartAppRelRequest}
	 * @return the user start app rel tbl {@link UserStartAppRelTbl}
	 * @throws UnauthorizedAccessException the unauthorized access exception
	 * @throws CannotCreateObjectException the cannot create object exception
	 */
	public UserStartAppRelTbl createUserStartAppRel(UserStartAppRelRequest userStartAppRelRequest)
			throws UnauthorizedAccessException, CannotCreateObjectException {
		try {
			String url = new String(this.serviceUrl + XMAdminRestClientConstants.CREATE_USER_START_APP_REL);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<UserStartAppRelRequest> request = new HttpEntity<UserStartAppRelRequest>(userStartAppRelRequest,
					this.headers);
			ResponseEntity<UserStartAppRelTbl> responseEntity = restTemplate.postForEntity(url, request,
					UserStartAppRelTbl.class);
			HttpStatus statusCode = responseEntity.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				UserStartAppRelTbl userStartAppRelTbl = responseEntity.getBody();
				return userStartAppRelTbl;
			} else {
				LOGGER.error(
						"Error while calling XMSYSTEM Create User Start Application Relation REST Service, returns the status code: ", //$NON-NLS-1$
						statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException) e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			} else if (e instanceof HttpStatusCodeException) {
				String errorResponse = ((HttpStatusCodeException) e).getResponseBodyAsString();
				String errCode = RestClientExpHandlerUtil.handleCannotCreateObjectException(LOGGER,
						(HttpStatusCodeException) e);
				throw new CannotCreateObjectException(errorResponse, errCode);
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM Create User Start Application Relation REST Service", e); //$NON-NLS-1$
		}
		return null;
	}

	/**
	 * Method for Creates the user start app rel.
	 *
	 * @param userStartAppRelBatchRequest            {@link UserStartAppRelBatchRequest}
	 * @return the user start app rel batch response
	 *         {@link UserStartAppRelBatchResponse}
	 * @throws UnauthorizedAccessException the unauthorized access exception
	 * @throws CannotCreateObjectException the cannot create object exception
	 */
	public UserStartAppRelBatchResponse createUserStartAppRel(UserStartAppRelBatchRequest userStartAppRelBatchRequest)
			throws UnauthorizedAccessException, CannotCreateObjectException {
		try {
			String url = new String(this.serviceUrl + XMAdminRestClientConstants.CREATE_MULTI_USER_START_APP_REL);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<UserStartAppRelBatchRequest> request = new HttpEntity<UserStartAppRelBatchRequest>(
					userStartAppRelBatchRequest, this.headers);
			ResponseEntity<UserStartAppRelBatchResponse> responseEntity = restTemplate.postForEntity(url, request,
					UserStartAppRelBatchResponse.class);
			HttpStatus statusCode = responseEntity.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				UserStartAppRelBatchResponse userStartAppRelBatchResponse = responseEntity.getBody();
				return userStartAppRelBatchResponse;
			} else {
				LOGGER.error(
						"Error while calling XMSYSTEM Create Multi User Start Application Relation REST Service, returns the status code: ", //$NON-NLS-1$
						statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException) e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			} else if (e instanceof HttpStatusCodeException) {
				String errorResponse = ((HttpStatusCodeException) e).getResponseBodyAsString();
				String errCode = RestClientExpHandlerUtil.handleCannotCreateObjectException(LOGGER,
						(HttpStatusCodeException) e);
				throw new CannotCreateObjectException(errorResponse, errCode);
			} else if (e instanceof ResourceAccessException) {
				throw e;
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM Create Multi User Start Application Relation REST Service", e); //$NON-NLS-1$
		}
		return null;
	}

	/**
	 * Method for Update user start app rel status.
	 *
	 * @param status            {@link String}
	 * @param relId            {@link String}
	 * @return true, if successful
	 * @throws UnauthorizedAccessException the unauthorized access exception
	 */
	public boolean updateUserStartAppRelStatus(String status, String relId) throws UnauthorizedAccessException {
		try {
			String url = new String(this.serviceUrl + XMAdminRestClientConstants.UPDATE_USER_START_APP_REL_STATUS + "/"
					+ status + "/" + relId);

			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<Object> requestEntity = new HttpEntity<Object>(this.headers);
			ResponseEntity<Boolean> responseEntity = restTemplate.exchange(url, HttpMethod.POST, requestEntity,
					Boolean.class);
			HttpStatus statusCode = responseEntity.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				return responseEntity.getBody();
			} else {
				LOGGER.error(
						"Error while calling XMSYSTEM Update User Start Application Relation Status REST Service, returns the status code: ", //$NON-NLS-1$
						statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException) e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM Update User Start Application Relation Status REST Service!", e); //$NON-NLS-1$
		}
		return false;
	}
	
	/**
	 * Multi update user start app rel status.
	 *
	 * @param userStartAppRelRequest the admin area proj app requests
	 * @return the user start app rel batch response
	 * @throws UnauthorizedAccessException the unauthorized access exception
	 */
	public UserStartAppRelBatchResponse multiUpdateUserStartAppRelStatus(
			List<UserStartAppRelRequest> userStartAppRelRequest) throws UnauthorizedAccessException {
		try {
			String url = new String(this.serviceUrl + XMAdminRestClientConstants.MULTI_UPDATE_USER_STARTAPP_STATUS);

			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<List<UserStartAppRelRequest>> requestEntity = new HttpEntity<List<UserStartAppRelRequest>>(
					userStartAppRelRequest, this.headers);
			ResponseEntity<UserStartAppRelBatchResponse> responseEntity = restTemplate.exchange(url, HttpMethod.POST,
					requestEntity, UserStartAppRelBatchResponse.class);
			HttpStatus statusCode = responseEntity.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				return responseEntity.getBody();
			} else {
				LOGGER.error(
						"Error while calling XMSYSTEM Multi Update UserStartApp Status REST Service, returns the status code: ", //$NON-NLS-1$
						statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException) e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM Multi Update UserStartApp Status REST Service!", e); //$NON-NLS-1$
		}
		return null;
	}

	/**
	 * Gets the all user start app rel by user id.
	 *
	 * @param userId            {@link String}
	 * @return the all user start app rel by user id
	 * @throws UnauthorizedAccessException the unauthorized access exception
	 */
	public List<UserStartAppRelTbl> getUserStartAppRelByUserId(String userId) throws UnauthorizedAccessException {
		try {
			String url = new String(
					this.serviceUrl + XMAdminRestClientConstants.GET_USER_START_APP_REL_BY_USER_ID + "/" + userId);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<Object> requestEntity = new HttpEntity<Object>(headers);
			List<UserStartAppRelTbl> userStartAppRelTblList = new ArrayList<>();
			List<HttpMessageConverter<?>> messageConverters = new ArrayList<>();
			messageConverters.add(new MappingJackson2HttpMessageConverter());
			restTemplate.setMessageConverters(messageConverters);

			ResponseEntity<UserStartAppRelResponse> responseList = restTemplate.exchange(url, HttpMethod.GET,
					requestEntity, UserStartAppRelResponse.class);
			HttpStatus statusCode = responseList.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				UserStartAppRelResponse userStartAppRelResponse = responseList.getBody();
				Iterable<UserStartAppRelTbl> UserStartAppRelTbls = userStartAppRelResponse
						.getUserStartAppRelTbls();
				for (UserStartAppRelTbl userStartAppRelTbl : UserStartAppRelTbls) {
					userStartAppRelTblList.add(userStartAppRelTbl);
				}

				return RestClientSortingUtil.sortUserStartAppRelByUserIdTbl(userStartAppRelTblList);
			} else {
				LOGGER.error(
						"Error while calling XMSYSTEM Get All User Start Application Relation by userId REST Service, returns the status code: " //$NON-NLS-1$
								+ statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException) e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			} else if (e instanceof HttpStatusCodeException) {
				RestClientExpHandlerUtil.handleNoXMLObjectFoundException(LOGGER, (HttpStatusCodeException) e);
			} else if(e instanceof ResourceAccessException){
				throw e;
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM Get All User Start Application Relation by userId REST Service", //$NON-NLS-1$
					e);
		}
		return null;
	}

	/**
	 * Gets the user start app rel by start app id.
	 *
	 * @param startAppId            {@link String}
	 * @return the user start app rel by start app id
	 * @throws UnauthorizedAccessException the unauthorized access exception
	 */
	public List<UserStartAppRelation> getUserStartAppRelByStartAppId(String startAppId)
			throws UnauthorizedAccessException {
		try {
			String url = new String(this.serviceUrl + XMAdminRestClientConstants.GET_USER_START_APP_REL_BY_START_APP_ID
					+ "/" + startAppId);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<Object> requestEntity = new HttpEntity<Object>(headers);

			List<HttpMessageConverter<?>> messageConverters = new ArrayList<>();
			messageConverters.add(new MappingJackson2HttpMessageConverter());
			restTemplate.setMessageConverters(messageConverters);

			ResponseEntity<UserStartAppRelResponseWrapper> responseList = restTemplate.exchange(url, HttpMethod.GET,
					requestEntity, UserStartAppRelResponseWrapper.class);
			HttpStatus statusCode = responseList.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				UserStartAppRelResponseWrapper userStartAppRelResponseWrapper = responseList.getBody();
				List<UserStartAppRelation> userStartAppRelations = userStartAppRelResponseWrapper.getUserStartAppRelations();
				
				return RestClientSortingUtil.sortUserStartAppRelByStartAppIdTbl(userStartAppRelations);
			} else {
				LOGGER.error(
						"Error while calling XMSYSTEM Get All User Start Application Relation by startAppId REST Service, returns the status code: " //$NON-NLS-1$
								+ statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException) e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			} else if (e instanceof HttpStatusCodeException) {
				RestClientExpHandlerUtil.handleNoXMLObjectFoundException(LOGGER, (HttpStatusCodeException) e);
			} else if(e instanceof ResourceAccessException){
				throw e;
			}
		} catch (Exception e) {
			LOGGER.error(
					"Error while calling XMSYSTEM Get All User Start Application Relation by startAppId REST Service", //$NON-NLS-1$
					e);
		}
		return null;
	}

	/**
	 * Delete UserStartAppRel.
	 *
	 * @param id the id
	 * @return boolean
	 * @throws UnauthorizedAccessException the unauthorized access exception
	 */
	public boolean deleteUserStartAppRel(String id) throws UnauthorizedAccessException {
		try {
			String url = new String(
					this.serviceUrl + XMAdminRestClientConstants.DELETE_USER_START_APP_REL_BY_ID + "/" + id);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<Object> requestEntity = new HttpEntity<Object>(this.headers);
			ResponseEntity<Boolean> responseEntity = restTemplate.exchange(url, HttpMethod.DELETE, requestEntity,
					Boolean.class);
			HttpStatus statusCode = responseEntity.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				return responseEntity.getBody();
			} else {
				LOGGER.error(
						"Error while calling XMSYSTEM delete UserStartAppRel REST Service, returns the status code: ", //$NON-NLS-1$
						statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException) e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM delete UserStartAppRel REST Service!", e); //$NON-NLS-1$
		}
		return false;
	}
	
	/**
	 * Delete multi user start app rel.
	 *
	 * @param ids the ids
	 * @return the user start app rel response
	 * @throws UnauthorizedAccessException the unauthorized access exception
	 */
	public UserStartAppRelResponse deleteMultiUserStartAppRel(Set<String> ids) throws UnauthorizedAccessException {
		try {
			String url = new String(this.serviceUrl + XMAdminRestClientConstants.DELETE_MULTI_USER_START_APP_REL_BY_ID);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<Set<String>> requestEntity = new HttpEntity<Set<String>>(ids, this.headers);
			ResponseEntity<UserStartAppRelResponse> responseEntity = restTemplate.exchange(url, HttpMethod.DELETE,
					requestEntity, UserStartAppRelResponse.class);
			HttpStatus statusCode = responseEntity.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				UserStartAppRelResponse userStartAppRelResponse = responseEntity.getBody();
				return userStartAppRelResponse;
			} else {
				LOGGER.error(
						"Error while calling XMSYSTEM delete Multi UserStartAppRel REST Service, returns the status code: ", //$NON-NLS-1$
						statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException) e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM delete Multi UserStartAppRel REST Service!", e); //$NON-NLS-1$
		}
		return null;
	}
}
