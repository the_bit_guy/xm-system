package com.magna.xmsystem.xmadmin.restclient.relation;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import com.magna.xmbackend.entities.AdminAreaUserAppRelTbl;
import com.magna.xmbackend.exception.CannotCreateObjectException;
import com.magna.xmbackend.response.rel.adminareauserapp.AdminAreaUserAppRelWrapper;
import com.magna.xmbackend.response.rel.adminareauserapp.AdminAreaUserAppRelation;
import com.magna.xmbackend.vo.rel.AdminAreaUserAppRelBatchRequest;
import com.magna.xmbackend.vo.rel.AdminAreaUserAppRelBatchResponse;
import com.magna.xmbackend.vo.rel.AdminAreaUserAppRelRequest;
import com.magna.xmbackend.vo.rel.AdminAreaUserAppRelResponse;
import com.magna.xmsystem.dependencies.customExceptions.UnauthorizedAccessException;
import com.magna.xmsystem.xmadmin.restclient.RestController;
import com.magna.xmsystem.xmadmin.restclient.site.SiteController;
import com.magna.xmsystem.xmadmin.restclient.util.RestClientExpHandlerUtil;
import com.magna.xmsystem.xmadmin.restclient.util.RestClientSortingUtil;
import com.magna.xmsystem.xmadmin.restclient.util.XMAdminRestClientConstants;

/**
 * Class for Site admin area rel controller.
 *
 * @author Roshan.Ekka
 */
public class AdminAreaUserAppRelController extends RestController {
	/** The Constant LOGGER. */
	private static final Logger LOGGER = LoggerFactory.getLogger(SiteController.class);

	/**
	 * Constructor for SiteController Class.
	 */
	public AdminAreaUserAppRelController() {
		super();
	}
	
	/**
	 * Constructor for AdminAreaUserAppRelController Class.
	 *
	 * @param adminAreaId {@link String}
	 */
	public AdminAreaUserAppRelController(String adminAreaId) {
		super(adminAreaId);
	}

	/**
	 * Method for Creates the admin area user app rel.
	 *
	 * @param adminAreaUserAppRelRequest
	 *            {@link AdminAreaUserAppRelRequest}
	 * @return the admin area user app rel tbl {@link AdminAreaUserAppRelTbl}
	 */
	public AdminAreaUserAppRelTbl createAdminAreaUserAppRel(AdminAreaUserAppRelRequest adminAreaUserAppRelRequest)
			throws UnauthorizedAccessException, CannotCreateObjectException {
		try {
			String url = new String(this.serviceUrl + XMAdminRestClientConstants.CREATE_ADMIN_AREA_USER_APP_REL);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<AdminAreaUserAppRelRequest> request = new HttpEntity<AdminAreaUserAppRelRequest>(
					adminAreaUserAppRelRequest, this.headers);
			ResponseEntity<AdminAreaUserAppRelTbl> responseEntity = restTemplate.postForEntity(url, request,
					AdminAreaUserAppRelTbl.class);
			HttpStatus statusCode = responseEntity.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				AdminAreaUserAppRelTbl adminAreaUserAppRelTbl = responseEntity.getBody();
				return adminAreaUserAppRelTbl;
			} else {
				LOGGER.error(
						"Error while calling XMSYSTEM Create Administration Area user Application Relations REST Service, returns the status code: ", //$NON-NLS-1$
						statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException) e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			} else if (e instanceof HttpStatusCodeException) {
				String errorResponse = ((HttpStatusCodeException) e).getResponseBodyAsString();
				String errCode = RestClientExpHandlerUtil.handleCannotCreateObjectException(LOGGER,
						(HttpStatusCodeException) e);
				throw new CannotCreateObjectException(errorResponse, errCode);
			}
		} catch (Exception e) {
			LOGGER.error(
					"Error while calling XMSYSTEM Create Administration Area user Application Relation REST Service", //$NON-NLS-1$
					e);
		}
		return null;
	}

	/**
	 * Method for Creates the admin area user app rel.
	 *
	 * @param adminAreaUserAppRelRequest
	 *            {@link AdminAreaUserAppRelRequest}
	 * @return the admin area user app rel tbl {@link AdminAreaUserAppRelTbl}
	 */
	public AdminAreaUserAppRelBatchResponse createAdminAreaUserAppRel(
			AdminAreaUserAppRelBatchRequest adminAreaUserAppRelBatchRequest)
			throws UnauthorizedAccessException, CannotCreateObjectException {
		try {
			String url = new String(this.serviceUrl + XMAdminRestClientConstants.CREATE_MULTI_USER_APP_ADMIN_AREA_REL);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<AdminAreaUserAppRelBatchRequest> request = new HttpEntity<AdminAreaUserAppRelBatchRequest>(
					adminAreaUserAppRelBatchRequest, this.headers);
			ResponseEntity<AdminAreaUserAppRelBatchResponse> responseEntity = restTemplate.postForEntity(url, request,
					AdminAreaUserAppRelBatchResponse.class);
			HttpStatus statusCode = responseEntity.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				AdminAreaUserAppRelBatchResponse adminAreaUserAppRelBatchResponse = responseEntity.getBody();
				return adminAreaUserAppRelBatchResponse;
			} else {
				LOGGER.error(
						"Error while calling XMSYSTEM Create Administration Area user Application Relations REST Service, returns the status code: ", //$NON-NLS-1$
						statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException) e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			} else if (e instanceof HttpStatusCodeException) {
				String errorResponse = ((HttpStatusCodeException) e).getResponseBodyAsString();
				String errCode = RestClientExpHandlerUtil.handleCannotCreateObjectException(LOGGER,
						(HttpStatusCodeException) e);
				throw new CannotCreateObjectException(errorResponse, errCode);
			} else if (e instanceof ResourceAccessException) {
				throw e;
			}
		} catch (Exception e) {
			LOGGER.error(
					"Error while calling XMSYSTEM Create Administration Area user Application Relation REST Service", //$NON-NLS-1$
					e);
		}
		return null;
	}

	public List<AdminAreaUserAppRelTbl> getAllAdminAreaUserAppRel(final boolean isSorted)
			throws UnauthorizedAccessException {
		try {
			String url = new String(this.serviceUrl + XMAdminRestClientConstants.GET_ADMIN_AREA_USER_APP_REL);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<Object> requestEntity = new HttpEntity<Object>(headers);

			List<HttpMessageConverter<?>> messageConverters = new ArrayList<>();
			messageConverters.add(new MappingJackson2HttpMessageConverter());
			restTemplate.setMessageConverters(messageConverters);

			ResponseEntity<AdminAreaUserAppRelResponse> responseList = restTemplate.exchange(url, HttpMethod.GET,
					requestEntity, AdminAreaUserAppRelResponse.class);
			HttpStatus statusCode = responseList.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				AdminAreaUserAppRelResponse adminAreaUserAppRelResponse = responseList.getBody();
				List<AdminAreaUserAppRelTbl> adminAreaUserAppRelTbl = (List<AdminAreaUserAppRelTbl>) adminAreaUserAppRelResponse
						.getAdminAreaUserAppRelTbls();
				/*
				 * if (isSorted) { siteTblList.sort((siteResponse1,
				 * siteResponse2) -> {
				 * Collection<com.magna.xmbackend.entities.SiteTranslationTbl>
				 * siteTranslation1;
				 * Collection<com.magna.xmbackend.entities.SiteTranslationTbl>
				 * siteTranslation2; if ((siteTranslation1 =
				 * siteResponse1.getSiteTranslationTblCollection()) != null &&
				 * (siteTranslation2 =
				 * siteResponse2.getSiteTranslationTblCollection()) != null) {
				 * String siteName1 = getNameByLanguage(LANG_ENUM.ENGLISH,
				 * siteTranslation1); String siteName2 =
				 * getNameByLanguage(LANG_ENUM.ENGLISH, siteTranslation2);
				 * return siteName1.compareToIgnoreCase(siteName2); } return 0;
				 * }); }
				 */
				return adminAreaUserAppRelTbl;
			} else {
				LOGGER.error(
						"Error while calling XMSYSTEM Get All Administration Area user Application Relation REST Service, returns the status code: " //$NON-NLS-1$
								+ statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException) e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			} else if (e instanceof HttpStatusCodeException) {
				RestClientExpHandlerUtil.handleNoXMLObjectFoundException(LOGGER, (HttpStatusCodeException) e);
			}
		} catch (Exception e) {
			LOGGER.error(
					"Error while calling XMSYSTEM Get All Administration Area user Application Relation REST Service", //$NON-NLS-1$
					e);
		}
		return null;
	}

	/*
	 * private String getNameByLanguage(final LANG_ENUM langEnum, final
	 * Collection<SiteTranslationTbl> siteTranslations) { for
	 * (SiteTranslationTbl siteTranslation : siteTranslations) { LANG_ENUM
	 * foundLangEnum =
	 * LANG_ENUM.getLangEnum(siteTranslation.getLanguageCode().getLanguageCode()
	 * ); String name; if (foundLangEnum == langEnum && (name =
	 * siteTranslation.getName()) != null) { if (!XMSystemUtil.isEmpty(name)) {
	 * return name; } } } return ""; }
	 */

	/**
	 * Method for Delete admin area user app rel.
	 *
	 * @param adminAreaUserAppRelId
	 *            {@link String}
	 * @return true, if successful
	 */
	public boolean deleteAdminAreaUserAppRel(String adminAreaUserAppRelId) throws UnauthorizedAccessException {
		try {
			String url = new String(this.serviceUrl + XMAdminRestClientConstants.DELETE_ADMIN_AREA_USER_APP_REL + "/"
					+ adminAreaUserAppRelId);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<Object> requestEntity = new HttpEntity<Object>(this.headers);
			ResponseEntity<Boolean> responseEntity = restTemplate.exchange(url, HttpMethod.DELETE, requestEntity,
					Boolean.class);
			HttpStatus statusCode = responseEntity.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				return responseEntity.getBody();
			} else {
				LOGGER.error(
						"Error while calling XMSYSTEM delete Administration Area user Application Relation REST Service, returns the status code: ", //$NON-NLS-1$
						statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException) e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			}
		} catch (Exception e) {
			LOGGER.error(
					"Error while calling XMSYSTEM delete Administration Area user Application Relation REST Service!", //$NON-NLS-1$
					e);
		}
		return false;
	}

	/**
	 * Delete multi admin area user app rel.
	 *
	 * @param ids the ids
	 * @return the admin area user app rel response
	 * @throws UnauthorizedAccessException the unauthorized access exception
	 */
	public AdminAreaUserAppRelResponse deleteMultiAdminAreaUserAppRel(Set<String> ids)
			throws UnauthorizedAccessException {
		try {
			String url = new String(this.serviceUrl + XMAdminRestClientConstants.DELETE_MULTI_ADMIN_AREA_USER_APP_REL);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<Set<String>> requestEntity = new HttpEntity<Set<String>>(ids, this.headers);
			ResponseEntity<AdminAreaUserAppRelResponse> responseEntity = restTemplate.exchange(url, HttpMethod.DELETE,
					requestEntity, AdminAreaUserAppRelResponse.class);
			HttpStatus statusCode = responseEntity.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				AdminAreaUserAppRelResponse adminAreaUserAppRelResponse = responseEntity.getBody();
				return adminAreaUserAppRelResponse;
			} else {
				LOGGER.error(
						"Error while calling XMSYSTEM delete Multi AdminAreaUserAppRel REST Service, returns the status code: ", //$NON-NLS-1$
						statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException) e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM delete Multi AdminAreaUserAppRel REST Service!", e); //$NON-NLS-1$
		}
		return null;
	}
	/**
	 * Method for Update admin area user app rel status.
	 *
	 * @param adminAreaUserAppRelId
	 *            {@link String}
	 * @param status
	 *            {@link String}
	 * @return true, if successful
	 */
	public boolean updateAdminAreaUserAppRelStatus(String adminAreaUserAppRelId, String status)
			throws UnauthorizedAccessException {
		try {
			String url = new String(this.serviceUrl + XMAdminRestClientConstants.UPDATE_ADMIN_AREA_USER_APP_REL_STATUS
					+ "/" + status + "/" + adminAreaUserAppRelId);

			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<Object> requestEntity = new HttpEntity<Object>(this.headers);
			ResponseEntity<Boolean> responseEntity = restTemplate.exchange(url, HttpMethod.POST, requestEntity,
					Boolean.class);
			HttpStatus statusCode = responseEntity.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				return responseEntity.getBody();
			} else {
				LOGGER.error(
						"Error while calling XMSYSTEM Update Administration Area user Application Relation Status REST Service, returns the status code: ", //$NON-NLS-1$
						statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException) e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			}
		} catch (Exception e) {
			LOGGER.error(
					"Error while calling XMSYSTEM Update Administration Area user Application Relation Status REST Service!", //$NON-NLS-1$
					e);
		}
		return false;
	}

	/**
	 * Multi update AA user app rel status.
	 *
	 * @param adminAreaUserAppRelRequest
	 *            the admin area user app rel request
	 * @return the admin area user app rel batch response
	 * @throws UnauthorizedAccessException
	 *             the unauthorized access exception
	 */
	public AdminAreaUserAppRelBatchResponse multiUpdateAAUserAppRelStatus(
			List<AdminAreaUserAppRelRequest> adminAreaUserAppRelRequest) throws UnauthorizedAccessException {
		try {
			String url = new String(this.serviceUrl + XMAdminRestClientConstants.MULTI_UPDATE_AA_USER_APP_STATUS);

			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<List<AdminAreaUserAppRelRequest>> requestEntity = new HttpEntity<List<AdminAreaUserAppRelRequest>>(
					adminAreaUserAppRelRequest, this.headers);
			ResponseEntity<AdminAreaUserAppRelBatchResponse> responseEntity = restTemplate.exchange(url,
					HttpMethod.POST, requestEntity, AdminAreaUserAppRelBatchResponse.class);
			HttpStatus statusCode = responseEntity.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				return responseEntity.getBody();
			} else {
				LOGGER.error(
						"Error while calling XMSYSTEM Multi Update AdminAreaUserApp Status REST Service, returns the status code: ", //$NON-NLS-1$
						statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException) e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM Multi Update AdminAreaUserApp Status REST Service!", e); //$NON-NLS-1$
		}
		return null;
	}
	
	/**
	 * Gets the AA user app by id.
	 *
	 * @param adminAreaUserAppRelId
	 *            {@link String}
	 * @return the AA user app by id
	 */
	public AdminAreaUserAppRelTbl getAAUserAppById(String adminAreaUserAppRelId) throws UnauthorizedAccessException {
		try {
			String url = new String(this.serviceUrl + XMAdminRestClientConstants.GET_ADMIN_AREA_USER_APP_REL_BY_ID + "/"
					+ adminAreaUserAppRelId);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<Object> requestEntity = new HttpEntity<Object>(headers);

			List<HttpMessageConverter<?>> messageConverters = new ArrayList<>();
			messageConverters.add(new MappingJackson2HttpMessageConverter());
			restTemplate.setMessageConverters(messageConverters);

			ResponseEntity<AdminAreaUserAppRelTbl> responseList = restTemplate.exchange(url, HttpMethod.GET,
					requestEntity, AdminAreaUserAppRelTbl.class);
			HttpStatus statusCode = responseList.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				AdminAreaUserAppRelTbl adminAreaUserAppRelTblObject = responseList.getBody();
				return adminAreaUserAppRelTblObject;
			} else {
				LOGGER.error(
						"Error while calling XMSYSTEM Get Administration Area User Application Relation by Id REST Service, returns the status code: " //$NON-NLS-1$
								+ statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException) e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			} else if (e instanceof HttpStatusCodeException) {
				RestClientExpHandlerUtil.handleNoXMLObjectFoundException(LOGGER, (HttpStatusCodeException) e);
			}
		} catch (Exception e) {
			LOGGER.error(
					"Error while calling XMSYSTEM Get Administration Area User Application Relation by Id REST Service", //$NON-NLS-1$
					e);
		}
		return null;
	}

	/**
	 * Gets the AA user app by user app id.
	 *
	 * @param userAppId
	 *            {@link String}
	 * @return the AA user app by user app id
	 */
	public List<AdminAreaUserAppRelation> getAAUserAppByUserAppId(String userAppId) throws UnauthorizedAccessException {
		try {
			String url = new String(this.serviceUrl
					+ XMAdminRestClientConstants.GET_ADMIN_AREA_USER_APP_REL_BY_USER_APP_ID + "/" + userAppId);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<Object> requestEntity = new HttpEntity<Object>(headers);

			List<HttpMessageConverter<?>> messageConverters = new ArrayList<>();
			messageConverters.add(new MappingJackson2HttpMessageConverter());
			restTemplate.setMessageConverters(messageConverters);

			ResponseEntity<AdminAreaUserAppRelWrapper> responseList = restTemplate.exchange(url, HttpMethod.GET,
					requestEntity, AdminAreaUserAppRelWrapper.class);
			HttpStatus statusCode = responseList.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				AdminAreaUserAppRelWrapper adminAreaUserAppRelWrapper = responseList.getBody();
				List<AdminAreaUserAppRelation> adminAreaUserAppList = adminAreaUserAppRelWrapper.getAdminAreaUserAppRels();
				
				return RestClientSortingUtil.sortAdminAreaUserAppTbl(adminAreaUserAppList);
			} else {
				LOGGER.error(
						"Error while calling XMSYSTEM Get Administration Area User Application Relation by userAppId REST Service, returns the status code: " //$NON-NLS-1$
								+ statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException) e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			} else if (e instanceof HttpStatusCodeException) {
				RestClientExpHandlerUtil.handleNoXMLObjectFoundException(LOGGER, (HttpStatusCodeException) e);
			} else if(e instanceof ResourceAccessException){
				throw e;
			}
		} catch (Exception e) {
			LOGGER.error(
					"Error while calling XMSYSTEM Get Administration Area User Application Relation by userAppId REST Service", //$NON-NLS-1$
					e);
		}
		return null;
	}

	/**
	 * Gets the AA user app by user app id.
	 *
	 * @param adminAreaId
	 *            {@link String}
	 * @return the AdminAreaUserAppRel by adminareaId
	 */
	public AdminAreaUserAppRelWrapper getAdminAreaUserAppRelationsByAdminAreaId(String adminAreaId)
			throws UnauthorizedAccessException {
		try {
			String url = new String(this.serviceUrl
					+ XMAdminRestClientConstants.GET_ADMIN_AREA_USER_APP_REL_BY_ADMIN_AREA_ID + "/" + adminAreaId);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<Object> requestEntity = new HttpEntity<Object>(headers);

			List<HttpMessageConverter<?>> messageConverters = new ArrayList<>();
			messageConverters.add(new MappingJackson2HttpMessageConverter());
			restTemplate.setMessageConverters(messageConverters);

			ResponseEntity<AdminAreaUserAppRelWrapper> responseList = restTemplate.exchange(url, HttpMethod.GET,
					requestEntity, AdminAreaUserAppRelWrapper.class);
			HttpStatus statusCode = responseList.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				AdminAreaUserAppRelWrapper adminAreaUserAppRelWrapper = responseList.getBody();
				return adminAreaUserAppRelWrapper;
			} else {
				LOGGER.error(
						"Error while calling XMSYSTEM Get Administration Area User Application Relation by adminAreaId REST Service, returns the status code: " //$NON-NLS-1$
								+ statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException) e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			} else if (e instanceof HttpStatusCodeException) {
				RestClientExpHandlerUtil.handleNoXMLObjectFoundException(LOGGER, (HttpStatusCodeException) e);
			}
		} catch (Exception e) {
			LOGGER.error(
					"Error while calling XMSYSTEM Get Administration Area User Application Relation by adminAreaId REST Service", //$NON-NLS-1$
					e);
		}
		return null;
	}

	/**
	 * Method for Update rel types by ids.
	 *
	 * @param adminAreaUserAppRelBatchRequest
	 *            {@link AdminAreaUserAppRelBatchRequest}
	 * @return the admin area user app rel batch response
	 *         {@link AdminAreaUserAppRelBatchResponse}
	 */
	public AdminAreaUserAppRelBatchResponse updateRelTypesByIds(
			AdminAreaUserAppRelBatchRequest adminAreaUserAppRelBatchRequest)
			throws UnauthorizedAccessException, CannotCreateObjectException {
		try {
			String url = new String(
					this.serviceUrl + XMAdminRestClientConstants.UPDATE_ADMIN_AREA_USER_APP_REL_TYPE_BY_IDS);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<AdminAreaUserAppRelBatchRequest> request = new HttpEntity<AdminAreaUserAppRelBatchRequest>(
					adminAreaUserAppRelBatchRequest, this.headers);
			ResponseEntity<AdminAreaUserAppRelBatchResponse> responseEntity = restTemplate.postForEntity(url, request,
					AdminAreaUserAppRelBatchResponse.class);
			HttpStatus statusCode = responseEntity.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				AdminAreaUserAppRelBatchResponse adminAreaUserAppRelBatchResponse = responseEntity.getBody();
				return adminAreaUserAppRelBatchResponse;
			} else {
				LOGGER.error(
						"Error while calling XMSYSTEM Update Administartion Area User Application Relation Types By IDs REST Service, returns the status code: ", //$NON-NLS-1$
						statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException) e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			} else if (e instanceof HttpStatusCodeException) {
				String errorResponse = ((HttpStatusCodeException) e).getResponseBodyAsString();
				String errCode = RestClientExpHandlerUtil.handleCannotCreateObjectException(LOGGER,
						(HttpStatusCodeException) e);
				throw new CannotCreateObjectException(errorResponse, errCode);
			} else if (e instanceof ResourceAccessException) {
				throw e;
			}
		} catch (Exception e) {
			LOGGER.error(
					"Error while calling XMSYSTEM Update Administartion Area User Application Relation Types By IDs REST Service", //$NON-NLS-1$
					e);
		}
		return null;
	}
	
	/**
	 * Gets the user app relations by AA id.
	 *
	 * @param adminAreaId the admin area id
	 * @return the user app relations by AA id
	 * @throws UnauthorizedAccessException the unauthorized access exception
	 */
	public AdminAreaUserAppRelWrapper getUserAppRelationsByAAId(final String adminAreaId)
			throws UnauthorizedAccessException {
		try {
			String url = new String(this.serviceUrl
					+ XMAdminRestClientConstants.GET_USERAPP_BY_AA_RELATION + "/" + adminAreaId);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<Object> requestEntity = new HttpEntity<Object>(headers);

			List<HttpMessageConverter<?>> messageConverters = new ArrayList<>();
			messageConverters.add(new MappingJackson2HttpMessageConverter());
			restTemplate.setMessageConverters(messageConverters);

			ResponseEntity<AdminAreaUserAppRelWrapper> responseList = restTemplate.exchange(url, HttpMethod.GET,
					requestEntity, AdminAreaUserAppRelWrapper.class);
			HttpStatus statusCode = responseList.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				AdminAreaUserAppRelWrapper adminAreaUserAppRelWrapper = responseList.getBody();
				return adminAreaUserAppRelWrapper;
			} else {
				LOGGER.error("Error while calling XMSYSTEM Get UserApp relation by adminAreaId REST Service, returns the status code: " //$NON-NLS-1$
								+ statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException) e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			} else if (e instanceof HttpStatusCodeException) {
				RestClientExpHandlerUtil.handleNoXMLObjectFoundException(LOGGER, (HttpStatusCodeException) e);
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM Get  UserApp relation by adminAreaId REST Service", e); //$NON-NLS-1$
		}
		return null;
	}
}
