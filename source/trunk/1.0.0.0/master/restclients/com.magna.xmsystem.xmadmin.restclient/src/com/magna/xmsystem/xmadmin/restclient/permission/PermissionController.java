package com.magna.xmsystem.xmadmin.restclient.permission;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import com.magna.xmbackend.vo.permission.PermissionResponse;
import com.magna.xmsystem.dependencies.customExceptions.UnauthorizedAccessException;
import com.magna.xmsystem.xmadmin.restclient.RestController;
import com.magna.xmsystem.xmadmin.restclient.util.RestClientExpHandlerUtil;
import com.magna.xmsystem.xmadmin.restclient.util.XMAdminRestClientConstants;

/**
 * The Class PermissionController.
 * 
 * @author shashwat.anand
 */
public class PermissionController extends RestController {
	/** The Constant LOGGER. */
	private static final Logger LOGGER = LoggerFactory.getLogger(PermissionController.class);

	/**
	 * Instantiates a new permission controller.
	 */
	public PermissionController() {
		super();
	}

	/**
	 * Method for Find object permission by role id.
	 *
	 * @param roleId
	 *            {@link String}
	 * @return the permission response {@link PermissionResponse}
	 */
	public PermissionResponse getObjectPermissionByRoleId(final String roleId) throws UnauthorizedAccessException {
		try {
			String url = new String(this.serviceUrl + XMAdminRestClientConstants.GET_OBJECT_PERMISSION_BY_ROLE_ID + "/" + roleId);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<Object> requestEntity = new HttpEntity<Object>(headers);

			List<HttpMessageConverter<?>> messageConverters = new ArrayList<>();
			messageConverters.add(new MappingJackson2HttpMessageConverter());
			restTemplate.setMessageConverters(messageConverters);

			ResponseEntity<PermissionResponse> responseList = restTemplate.exchange(url, HttpMethod.GET, requestEntity,
					PermissionResponse.class);
			HttpStatus statusCode = responseList.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				PermissionResponse permissionResponse = responseList.getBody();
				return permissionResponse;
			} else {
				LOGGER.error(
						"Error while calling XMSYSTEM Get ObjectPermission by Id REST Service, returns the status code: " //$NON-NLS-1$
								+ statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException)e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			} else if (e instanceof HttpStatusCodeException) {
				RestClientExpHandlerUtil.handleNoXMLObjectFoundException(LOGGER, (HttpStatusCodeException) e);
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM Get ObjectPermission by Id REST Service", e); //$NON-NLS-1$
		}
		return null;
	}
	
	/**
	 * Method for Find object rel permission by role id.
	 *
	 * @param roleId {@link String}
	 * @return the permission response {@link PermissionResponse}
	 */
	public PermissionResponse getObjectRelPermissionByRoleId(final String roleId) throws UnauthorizedAccessException {
		try {
			String url = new String(this.serviceUrl + XMAdminRestClientConstants.GET_OBJECT_REL_PERMISSION_BY_ROLE_ID + "/" + roleId);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<Object> requestEntity = new HttpEntity<Object>(headers);

			List<HttpMessageConverter<?>> messageConverters = new ArrayList<>();
			messageConverters.add(new MappingJackson2HttpMessageConverter());
			restTemplate.setMessageConverters(messageConverters);

			ResponseEntity<PermissionResponse> responseList = restTemplate.exchange(url, HttpMethod.GET, requestEntity,
					PermissionResponse.class);
			HttpStatus statusCode = responseList.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				PermissionResponse permissionResponse = responseList.getBody();
				return permissionResponse;
			} else {
				LOGGER.error(
						"Error while calling XMSYSTEM Get Object Relation Permission by Id REST Service, returns the status code: " //$NON-NLS-1$
								+ statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException)e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			} else if (e instanceof HttpStatusCodeException) {
				RestClientExpHandlerUtil.handleNoXMLObjectFoundException(LOGGER, (HttpStatusCodeException) e);
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM Get Object Relation Permission by Id REST Service", e); //$NON-NLS-1$
		}
		return null;
	}
	
	/**
	 * Gets the all object permission.
	 *
	 * @param isSorted {@link boolean}
	 * @return the all object permission
	 */
	public PermissionResponse getAllObjectPermission(final boolean isSorted) throws UnauthorizedAccessException {
		try {
			String url = new String(this.serviceUrl + XMAdminRestClientConstants.GET_ALL_OBJECT_PERMISSION);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<Object> requestEntity = new HttpEntity<Object>(headers);

			List<HttpMessageConverter<?>> messageConverters = new ArrayList<>();
			messageConverters.add(new MappingJackson2HttpMessageConverter());
			restTemplate.setMessageConverters(messageConverters);

			ResponseEntity<PermissionResponse> responseList = restTemplate.exchange(url, HttpMethod.GET, requestEntity,
					PermissionResponse.class);
			HttpStatus statusCode = responseList.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				PermissionResponse permissionResponse = responseList.getBody();
				return permissionResponse;
			} else {
				LOGGER.error(
						"Error while calling XMSYSTEM Get All Object Permission REST Service, returns the status code: " //$NON-NLS-1$
								+ statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException)e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			} else if (e instanceof HttpStatusCodeException) {
				RestClientExpHandlerUtil.handleNoXMLObjectFoundException(LOGGER, (HttpStatusCodeException) e);
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM Get All Object Permission REST Service", e); //$NON-NLS-1$
		}
		return null;
	}

	/**
	 * Gets the all object rel permission.
	 *
	 * @param isSorted {@link boolean}
	 * @return the all object rel permission
	 */
	public PermissionResponse getAllObjectRelPermission(final boolean isSorted) throws UnauthorizedAccessException {
		try {
			String url = new String(this.serviceUrl + XMAdminRestClientConstants.GET_ALL_REL_OBJECT_PERMISSION);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<Object> requestEntity = new HttpEntity<Object>(headers);

			List<HttpMessageConverter<?>> messageConverters = new ArrayList<>();
			messageConverters.add(new MappingJackson2HttpMessageConverter());
			restTemplate.setMessageConverters(messageConverters);

			ResponseEntity<PermissionResponse> responseList = restTemplate.exchange(url, HttpMethod.GET, requestEntity,
					PermissionResponse.class);
			HttpStatus statusCode = responseList.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				PermissionResponse permissionResponse = responseList.getBody();
				return permissionResponse;
			} else {
				LOGGER.error(
						"Error while calling XMSYSTEM Get All Object Relation Permission REST Service, returns the status code: " //$NON-NLS-1$
								+ statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException)e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			} else if (e instanceof HttpStatusCodeException) {
				RestClientExpHandlerUtil.handleNoXMLObjectFoundException(LOGGER, (HttpStatusCodeException) e);
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM Get All Object Relation Permission REST Service", e); //$NON-NLS-1$
		}
		return null;
	}
	
	/**
	 * Gets the all AA based rel permission.
	 *
	 * @param isSorted the is sorted
	 * @return the all AA based rel permission
	 * @throws UnauthorizedAccessException the unauthorized access exception
	 */
	public PermissionResponse getAllAABasedRelPermission(final boolean isSorted) throws UnauthorizedAccessException {
		try {
			final String url = new String(this.serviceUrl + XMAdminRestClientConstants.GET_ALL_AA_BASED_REL_PERMISSION);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<Object> requestEntity = new HttpEntity<Object>(headers);

			List<HttpMessageConverter<?>> messageConverters = new ArrayList<>();
			messageConverters.add(new MappingJackson2HttpMessageConverter());
			restTemplate.setMessageConverters(messageConverters);

			ResponseEntity<PermissionResponse> responseList = restTemplate.exchange(url, HttpMethod.GET, requestEntity,
					PermissionResponse.class);
			HttpStatus statusCode = responseList.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				PermissionResponse permissionResponse = responseList.getBody();
				return permissionResponse;
			} else {
				LOGGER.error(
						"Error while calling XMSYSTEM Get All AA Based Relation Permission REST Service, returns the status code: " //$NON-NLS-1$
								+ statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException)e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			} else if (e instanceof HttpStatusCodeException) {
				RestClientExpHandlerUtil.handleNoXMLObjectFoundException(LOGGER, (HttpStatusCodeException) e);
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM Get All AA Based Relation Permission REST Service", e); //$NON-NLS-1$
		}
		return null;
	}
}
