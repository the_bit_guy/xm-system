package com.magna.xmsystem.xmadmin.restclient.directory;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import com.magna.xmbackend.entities.DirectoryTbl;
import com.magna.xmbackend.exception.CannotCreateObjectException;
import com.magna.xmbackend.vo.directory.DirectoryRequest;
import com.magna.xmbackend.vo.directory.DirectoryResponse;
import com.magna.xmsystem.dependencies.customExceptions.UnauthorizedAccessException;
import com.magna.xmsystem.xmadmin.restclient.RestController;
import com.magna.xmsystem.xmadmin.restclient.util.RestClientExpHandlerUtil;
import com.magna.xmsystem.xmadmin.restclient.util.RestClientSortingUtil;
import com.magna.xmsystem.xmadmin.restclient.util.XMAdminRestClientConstants;


/**
 * Class for Directory controller.
 *
 * @author Roshan.Ekka
 */
public class DirectoryController extends RestController {

	/** The Constant LOGGER. */
	private static final Logger LOGGER = LoggerFactory.getLogger(DirectoryController.class);

	/**
	 * Constructor for SiteController Class.
	 */
	public DirectoryController() {
		super();
	}

	
	/**
	 * Method for Creates the directory.
	 *
	 * @param directoryRequest {@link DirectoryRequest}
	 * @return the directory tbl {@link DirectoryTbl}
	 */
	public DirectoryTbl createDirectory(DirectoryRequest directoryRequest) throws UnauthorizedAccessException, CannotCreateObjectException {
		try {
			String url = new String(this.serviceUrl + XMAdminRestClientConstants.CREATE_DIRECTORY);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<DirectoryRequest> request = new HttpEntity<DirectoryRequest>(directoryRequest, this.headers);
			ResponseEntity<DirectoryTbl> responseEntity = restTemplate.postForEntity(url, request, DirectoryTbl.class);
			HttpStatus statusCode = responseEntity.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				DirectoryTbl directoryTbl = responseEntity.getBody();
				return directoryTbl;
			} else {
				LOGGER.error("Error while calling XMSYSTEM Create Directory REST Service, returns the status code: ", //$NON-NLS-1$
						statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException)e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			} else if (e instanceof HttpStatusCodeException) {
				String errorResponse = ((HttpStatusCodeException) e).getResponseBodyAsString();
				String errCode = RestClientExpHandlerUtil.handleCannotCreateObjectException(LOGGER, (HttpStatusCodeException) e);
				throw new CannotCreateObjectException(errorResponse, errCode);
			}else if (e instanceof ResourceAccessException) {
				throw e;
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM Create Directory REST Service", e); //$NON-NLS-1$
		}
		return null;
	}

	
	/**
	 * Gets the all directory.
	 *
	 * @param isSorted the is sorted
	 * @return the all directory
	 */
	public List<DirectoryTbl> getAllDirectory(final boolean isSorted) throws UnauthorizedAccessException {
		try {
			String url = new String(this.serviceUrl + XMAdminRestClientConstants.GET_ALL_DIRECTORY);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<Object> requestEntity = new HttpEntity<Object>(headers);

			List<HttpMessageConverter<?>> messageConverters = new ArrayList<>();
			messageConverters.add(new MappingJackson2HttpMessageConverter());
			restTemplate.setMessageConverters(messageConverters);

			ResponseEntity<DirectoryResponse> responseList = restTemplate.exchange(url, HttpMethod.GET, requestEntity,
					DirectoryResponse.class);
			HttpStatus statusCode = responseList.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				DirectoryResponse directoryRespObj = responseList.getBody();
				List<DirectoryTbl> directoryTblList = (List<DirectoryTbl>) directoryRespObj.getDirectoryTbls();
				return RestClientSortingUtil.sortDirectoryTbl(directoryTblList);
			} else {
				LOGGER.error("Error while calling XMSYSTEM Get All Directory REST Service, returns the status code: " //$NON-NLS-1$
						+ statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException)e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			} else if (e instanceof HttpStatusCodeException) {
				RestClientExpHandlerUtil.handleNoXMLObjectFoundException(LOGGER, (HttpStatusCodeException) e);
			} else if(e instanceof ResourceAccessException){
				throw e;
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM Get All Directory REST Service", e); //$NON-NLS-1$
		}
		return null;
	}
	
	/**
	 * Delete directory.
	 *
	 * @param directoryId the directory id
	 * @return true, if successful
	 */
	public boolean deleteDirectory(String directoryId) throws UnauthorizedAccessException {
		try {
			String url = new String(this.serviceUrl + XMAdminRestClientConstants.DELETE_DIRECTORY + "/" + directoryId);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<Object> requestEntity = new HttpEntity<Object>(this.headers);
			ResponseEntity<Boolean> responseEntity = restTemplate.exchange(url, HttpMethod.DELETE, requestEntity,
					Boolean.class);
			HttpStatus statusCode = responseEntity.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				return responseEntity.getBody();
			} else {
				LOGGER.error("Error while calling XMSYSTEM delete Directory REST Service, returns the status code: ", //$NON-NLS-1$
						statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException)e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM delete Directory REST Service!", e); //$NON-NLS-1$
		}
		return false;
	}

	/**
	 * Delete multi directory.
	 *
	 * @param directoryIds the directory ids
	 * @return the directory response
	 * @throws UnauthorizedAccessException the unauthorized access exception
	 */
	public DirectoryResponse deleteMultiDirectory(Set<String> directoryIds) throws UnauthorizedAccessException {
		try {
			String url = new String(this.serviceUrl + XMAdminRestClientConstants.DELETE_MULTI_DIRECTORY);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<Set<String>> requestEntity = new HttpEntity<Set<String>>(directoryIds,this.headers);
			ResponseEntity<DirectoryResponse> responseEntity = restTemplate.exchange(url, HttpMethod.DELETE, requestEntity,
					DirectoryResponse.class);
			HttpStatus statusCode = responseEntity.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				DirectoryResponse directoryResponse = responseEntity.getBody();
				return directoryResponse;
			} else {
				LOGGER.error("Error while calling XMSYSTEM delete Multi Directory REST Service, returns the status code: ", //$NON-NLS-1$
						statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException)e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM delete Multi Directory REST Service!", e); //$NON-NLS-1$
		}
		return null;
	}
	
	/**
	 * Update site.
	 *
	 * @param directoryRequest the directory request
	 * @return true, if successful
	 */
	public boolean updateDirectory(com.magna.xmbackend.vo.directory.DirectoryRequest directoryRequest) throws UnauthorizedAccessException, CannotCreateObjectException {
		try {
			String url = new String(this.serviceUrl + XMAdminRestClientConstants.UPDATE_DIRECTORY);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<DirectoryRequest> request = new HttpEntity<DirectoryRequest>(directoryRequest, this.headers);
			ResponseEntity<DirectoryTbl> responseEntity = restTemplate.postForEntity(url, request, DirectoryTbl.class);
			HttpStatus statusCode = responseEntity.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				return true;
			} else {
				LOGGER.error("Error while calling XMSYSTEM Upadte Directory REST Service, returns the status code: ", //$NON-NLS-1$
						statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException)e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			} else if (e instanceof HttpStatusCodeException) {
				String errorResponse = ((HttpStatusCodeException) e).getResponseBodyAsString();
				String errCode = RestClientExpHandlerUtil.handleCannotCreateObjectException(LOGGER, (HttpStatusCodeException) e);
				throw new CannotCreateObjectException(errorResponse, errCode);
			}else if (e instanceof ResourceAccessException) {
				throw e;
			}
		} catch (Exception e) {
		}
		return false;
	}
}