package com.magna.xmsystem.xmadmin.restclient.livemsg;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import com.magna.xmbackend.entities.LiveMessageTbl;
import com.magna.xmbackend.exception.CannotCreateObjectException;
import com.magna.xmbackend.vo.liveMessage.LiveMessageCreateRequest;
import com.magna.xmbackend.vo.liveMessage.LiveMessageResponse;
import com.magna.xmbackend.vo.liveMessage.LiveMessageResponseWrapper;
import com.magna.xmbackend.vo.liveMessage.LiveMessageStatusRequest;
import com.magna.xmbackend.vo.liveMessage.LiveMessageStatusResponseWrapper;
import com.magna.xmsystem.dependencies.customExceptions.UnauthorizedAccessException;
import com.magna.xmsystem.xmadmin.restclient.RestController;
import com.magna.xmsystem.xmadmin.restclient.util.RestClientExpHandlerUtil;
import com.magna.xmsystem.xmadmin.restclient.util.RestClientSortingUtil;
import com.magna.xmsystem.xmadmin.restclient.util.XMAdminRestClientConstants;

// TODO: Auto-generated Javadoc
/**
 * The Class LiveMessageController.
 */
public class LiveMessageController extends RestController {

	/** The Constant LOGGER. */
	private static final Logger LOGGER = LoggerFactory.getLogger(LiveMessageController.class);

	/**
	 * Instantiates a new live message controller.
	 */
	public LiveMessageController() {
		super();
	}

	/**
	 * Save live message.
	 *
	 * @param liveMsgRequest
	 *            the live msg request
	 * @return the live message tbl
	 * @throws UnauthorizedAccessException
	 *             the unauthorized access exception
	 * @throws CannotCreateObjectException
	 *             the cannot create object exception
	 */
	public LiveMessageTbl saveLiveMessage(final LiveMessageCreateRequest liveMsgRequest)
			throws UnauthorizedAccessException, CannotCreateObjectException {
		try {
			final String url = new String(this.serviceUrl + XMAdminRestClientConstants.SAVE_LIVE_MESSAGE);
			final RestTemplate restTemplate = new RestTemplate();
			final HttpEntity<LiveMessageCreateRequest> request = new HttpEntity<LiveMessageCreateRequest>(
					liveMsgRequest, this.headers);
			final ResponseEntity<LiveMessageTbl> responseEntity = restTemplate.exchange(url, HttpMethod.POST, request,
					new ParameterizedTypeReference<LiveMessageTbl>() {
					});
			HttpStatus statusCode = responseEntity.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				LiveMessageTbl LiveMessageTbl = responseEntity.getBody();
				return LiveMessageTbl;
			} else {
				LOGGER.error("Error while calling XMSYSTEM Save LiveMessage REST Service, returns the status code: ", //$NON-NLS-1$
						statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException) e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			} else if (e instanceof HttpStatusCodeException) {
				String errorResponse = ((HttpStatusCodeException) e).getResponseBodyAsString();
				String errCode = RestClientExpHandlerUtil.handleCannotCreateObjectException(LOGGER,
						(HttpStatusCodeException) e);
				throw new CannotCreateObjectException(errorResponse, errCode);
			} else if (e instanceof ResourceAccessException) {
				throw e;
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM Save LiveMessage REST Service", e); //$NON-NLS-1$
		}
		return null;
	}

	/**
	 * Gets the all live message.
	 *
	 * @return the all live message
	 * @throws UnauthorizedAccessException
	 *             the unauthorized access exception
	 */
	public LiveMessageResponseWrapper getAllLiveMessage() throws UnauthorizedAccessException {
		try {
			String url = new String(this.serviceUrl + XMAdminRestClientConstants.GET_LIVE_MESSAGES);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<Object> requestEntity = new HttpEntity<Object>(headers);

			List<HttpMessageConverter<?>> messageConverters = new ArrayList<>();
			messageConverters.add(new MappingJackson2HttpMessageConverter());
			restTemplate.setMessageConverters(messageConverters);

			ResponseEntity<LiveMessageResponseWrapper> responseList = restTemplate.exchange(url, HttpMethod.GET,
					requestEntity, LiveMessageResponseWrapper.class);
			HttpStatus statusCode = responseList.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				LiveMessageResponseWrapper liveMessageResponseWrapper = responseList.getBody();
				List<LiveMessageResponse> liveMessageResponseWrapperList = liveMessageResponseWrapper
						.getLiveMessageResponseWrapper();
				RestClientSortingUtil.sortLiveMessageTbl(liveMessageResponseWrapperList);
				return liveMessageResponseWrapper;
			} else {
				LOGGER.error("Error while calling XMSYSTEM Get All LiveMessage REST Service, returns the status code: " //$NON-NLS-1$
						+ statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException) e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			} else if (e instanceof HttpStatusCodeException) {
				RestClientExpHandlerUtil.handleNoXMLObjectFoundException(LOGGER, (HttpStatusCodeException) e);
			} else if(e instanceof ResourceAccessException){
				throw e;
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM Get All LiveMessage REST Service", e); //$NON-NLS-1$
		}
		return null;
	}

	/**
	 * Update live message.
	 *
	 * @param liveMsgRequest
	 *            the live msg request
	 * @return true, if successful
	 * @throws UnauthorizedAccessException
	 *             the unauthorized access exception
	 * @throws CannotCreateObjectException
	 *             the cannot create object exception
	 */
	public boolean updateLiveMessage(com.magna.xmbackend.vo.liveMessage.LiveMessageCreateRequest liveMsgRequest)
			throws UnauthorizedAccessException, CannotCreateObjectException {
		try {
			String url = new String(this.serviceUrl + XMAdminRestClientConstants.UPDATE_LIVE_MESSAGE);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<LiveMessageCreateRequest> request = new HttpEntity<LiveMessageCreateRequest>(liveMsgRequest,
					this.headers);
			ResponseEntity<Boolean> responseEntity = restTemplate.exchange(url, HttpMethod.PUT, request, Boolean.class);
			HttpStatus statusCode = responseEntity.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				return true;
			} else {
				LOGGER.error("Error while calling XMSYSTEM Upadte LiveMessage REST Service, returns the status code: ", //$NON-NLS-1$
						statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException) e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			} else if (e instanceof HttpStatusCodeException) {
				String errorResponse = ((HttpStatusCodeException) e).getResponseBodyAsString();
				String errCode = RestClientExpHandlerUtil.handleCannotCreateObjectException(LOGGER,
						(HttpStatusCodeException) e);
				throw new CannotCreateObjectException(errorResponse, errCode);
			} else if (e instanceof ResourceAccessException) {
				throw e;
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM Upadte LiveMessage REST Service", e); //$NON-NLS-1$
		}
		return false;
	}

	/**
	 * Delete live message.
	 *
	 * @param liveMsgId
	 *            the live msg id
	 * @return true, if successful
	 * @throws UnauthorizedAccessException
	 *             the unauthorized access exception
	 */
	public boolean deleteLiveMessage(String liveMsgId) throws UnauthorizedAccessException {
		try {
			String url = new String(this.serviceUrl + XMAdminRestClientConstants.DELETE_LIVEMESSAGE + "/" + liveMsgId);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<Object> requestEntity = new HttpEntity<Object>(this.headers);
			ResponseEntity<Boolean> responseEntity = restTemplate.exchange(url, HttpMethod.DELETE, requestEntity,
					Boolean.class);
			HttpStatus statusCode = responseEntity.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				return responseEntity.getBody();
			} else {
				LOGGER.error("Error while calling XMSYSTEM delete LiveMessage REST Service, returns the status code: ", //$NON-NLS-1$
						statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException) e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM delete LiveMessage REST Service!", e); //$NON-NLS-1$
		}
		return false;
	}
	
	/**
	 * Delete multi live msg.
	 *
	 * @param liveMsgIds the live msg ids
	 * @return the live message response
	 * @throws UnauthorizedAccessException the unauthorized access exception
	 */
	public LiveMessageResponse deleteMultiLiveMsg(Set<String> liveMsgIds) throws UnauthorizedAccessException {
		try {
			String url = new String(this.serviceUrl + XMAdminRestClientConstants.DELETE_MULTI_LIVE_MESSAGE);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<Set<String>> requestEntity = new HttpEntity<Set<String>>(liveMsgIds,this.headers);
			ResponseEntity<LiveMessageResponse> responseEntity = restTemplate.exchange(url, HttpMethod.DELETE, requestEntity,
					LiveMessageResponse.class);
			HttpStatus statusCode = responseEntity.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				LiveMessageResponse liveMessageResponse = responseEntity.getBody();
				return liveMessageResponse;
			} else {
				LOGGER.error("Error while calling XMSYSTEM delete Multi LiveMessage REST Service, returns the status code: ", //$NON-NLS-1$
						statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException)e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM delete Multi LiveMessage REST Service!", e); //$NON-NLS-1$
		}
		return null;
	}
	
	/**
	 * Gets the live message by status.
	 *
	 * @param liveMsgId the live msg id
	 * @return the live message by status
	 * @throws UnauthorizedAccessException the unauthorized access exception
	 */
	public LiveMessageStatusResponseWrapper getLiveMessageByStatus(final LiveMessageStatusRequest liveMsgRequest) throws UnauthorizedAccessException {
		try {
			String url = new String(this.serviceUrl + XMAdminRestClientConstants.LIVE_MESSAGE_BY_STATUS);
			RestTemplate restTemplate = new RestTemplate();
			final HttpEntity<LiveMessageStatusRequest> request = new HttpEntity<LiveMessageStatusRequest>(liveMsgRequest, this.headers);
			ResponseEntity<LiveMessageStatusResponseWrapper> responseEntity = restTemplate.exchange(url, HttpMethod.POST, request,
					LiveMessageStatusResponseWrapper.class);
			HttpStatus statusCode = responseEntity.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				return responseEntity.getBody();
			} else {
				LOGGER.error("Error while calling XMSYSTEM get live message by status REST Service, returns the status code: ", //$NON-NLS-1$
						statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException) e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM get live message by status REST Service!", e); //$NON-NLS-1$
		}
		return null;
	}
	
	/**
	 * Update live message status.
	 *
	 * @param liveMsgRequest the live msg request
	 * @return true, if successful
	 * @throws UnauthorizedAccessException the unauthorized access exception
	 */
	public boolean updateLiveMessageStatus(final LiveMessageStatusRequest liveMsgRequest)
			throws UnauthorizedAccessException {

		try {
			String url = new String(this.serviceUrl + XMAdminRestClientConstants.UPDATE_LIVE_MESSAGE_STATUS);
			RestTemplate restTemplate = new RestTemplate();
			final HttpEntity<LiveMessageStatusRequest> requestEntity = new HttpEntity<LiveMessageStatusRequest>(
					liveMsgRequest, this.headers);
			ResponseEntity<Boolean> responseEntity = restTemplate.exchange(url, HttpMethod.POST, requestEntity,
					Boolean.class);
			HttpStatus statusCode = responseEntity.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				return responseEntity.getBody();
			} else {
				LOGGER.error("Error while updating LiveMessage status REST Service, returns the status code: ", //$NON-NLS-1$
						statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException) e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			}
		} catch (Exception e) {
			LOGGER.error("Error while updating LiveMessage status REST Service!", e); //$NON-NLS-1$
		}
		return false;
	}
}
