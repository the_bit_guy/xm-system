package com.magna.xmsystem.xmadmin.restclient.site;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import com.magna.xmbackend.entities.SitesTbl;
import com.magna.xmbackend.exception.CannotCreateObjectException;
import com.magna.xmbackend.vo.jpa.site.SiteAdminAreaRelIdWithAdminArea;
import com.magna.xmbackend.vo.jpa.site.SiteAdminAreaRelIdWithAdminAreaResponse;
import com.magna.xmbackend.vo.jpa.site.SiteRequest;
import com.magna.xmbackend.vo.jpa.site.SiteResponse;
import com.magna.xmbackend.vo.project.ProjectResponse;
import com.magna.xmsystem.dependencies.customExceptions.UnauthorizedAccessException;
import com.magna.xmsystem.xmadmin.restclient.RestController;
import com.magna.xmsystem.xmadmin.restclient.util.RestClientExpHandlerUtil;
import com.magna.xmsystem.xmadmin.restclient.util.RestClientSortingUtil;
import com.magna.xmsystem.xmadmin.restclient.util.XMAdminRestClientConstants;

/**
 * Class for Site controller.
 *
 * @author Chiranjeevi.Akula
 */
public class SiteController extends RestController{

	/** The Constant LOGGER. */
	private static final Logger LOGGER = LoggerFactory.getLogger(SiteController.class);

	/**
	 * Constructor for SiteController Class.
	 */
	public SiteController() {
		super();
	}

	/**
	 * Method for Creates the site.
	 *
	 * @param siteRequest
	 *            {@link SiteRequest}
	 * @return the sites tbl {@link SitesTbl}
	 */
	public SitesTbl createSite(SiteRequest siteRequest) {
		try {
			String url = new String(this.serviceUrl + XMAdminRestClientConstants.CREATE_SITE);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<SiteRequest> request = new HttpEntity<SiteRequest>(siteRequest, this.headers);
			ResponseEntity<SitesTbl> responseEntity = restTemplate.postForEntity(url, request, SitesTbl.class);
			HttpStatus statusCode = responseEntity.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				SitesTbl sitesTbl = responseEntity.getBody();
				return sitesTbl;
			} else {
				LOGGER.error("Error while calling XMSYSTEM Create Site REST Service, returns the status code: ", //$NON-NLS-1$
						statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException)e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			} else if (e instanceof HttpStatusCodeException) {
				String errorResponse = ((HttpStatusCodeException) e).getResponseBodyAsString();
				String errCode = RestClientExpHandlerUtil.handleCannotCreateObjectException(LOGGER, (HttpStatusCodeException) e);
				throw new CannotCreateObjectException(errorResponse, errCode);
			}else if (e instanceof ResourceAccessException) {
				throw e;
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM Create Site REST Service", e); //$NON-NLS-1$
		}
		return null;
	}

	/**
	 * Gets the all sites.
	 *
	 * @param isSorted
	 *            {@link boolean}
	 * @return the all sites
	 */
	public List<SitesTbl> getAllSites(final boolean isSorted) throws UnauthorizedAccessException {
		try {
			String url = new String(this.serviceUrl + XMAdminRestClientConstants.GET_SITES);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<Object> requestEntity = new HttpEntity<Object>(headers);

			List<HttpMessageConverter<?>> messageConverters = new ArrayList<>();
			messageConverters.add(new MappingJackson2HttpMessageConverter());
			restTemplate.setMessageConverters(messageConverters);

			ResponseEntity<SiteResponse> responseList = restTemplate.exchange(url, HttpMethod.GET, requestEntity,
					SiteResponse.class);
			HttpStatus statusCode = responseList.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				SiteResponse siteResponseObject = responseList.getBody();
				List<SitesTbl> siteTblList = (List<SitesTbl>) siteResponseObject.getSitesTbls();
				 return RestClientSortingUtil.sortSiteTbl(siteTblList);
			} else {
				LOGGER.error("Error while calling XMSYSTEM Get All Sites REST Service, returns the status code: " //$NON-NLS-1$
						+ statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException)e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			} else if (e instanceof HttpStatusCodeException) {
				RestClientExpHandlerUtil.handleNoXMLObjectFoundException(LOGGER, (HttpStatusCodeException) e);
			} else if(e instanceof ResourceAccessException){
				throw e;
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM Get All Sites REST Service", e); //$NON-NLS-1$
		}
		return null;
	}

	/**
	 * Method for Delete site.
	 *
	 * @param siteId
	 *            {@link String}
	 * @return true, if successful
	 */
	public boolean deleteSite(String siteId) throws UnauthorizedAccessException {
		try {
			String url = new String(this.serviceUrl + XMAdminRestClientConstants.DELETE_SITE + "/" + siteId);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<Object> requestEntity = new HttpEntity<Object>(this.headers);
			ResponseEntity<Boolean> responseEntity = restTemplate.exchange(url, HttpMethod.POST, requestEntity,
					Boolean.class);
			HttpStatus statusCode = responseEntity.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				return responseEntity.getBody();
			} else {
				LOGGER.error("Error while calling XMSYSTEM delete Site REST Service, returns the status code: ", //$NON-NLS-1$
						statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException)e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM delete Site REST Service!", e); //$NON-NLS-1$
		}
		return false;
	}

	/**
	 * Delete multi site.
	 *
	 * @param siteIds the site ids
	 * @return the site response
	 * @throws UnauthorizedAccessException the unauthorized access exception
	 */
	public SiteResponse deleteMultiSite(Set<String> siteIds) throws UnauthorizedAccessException {
		try {
			String url = new String(this.serviceUrl + XMAdminRestClientConstants.DELETE_MULTI_SITE);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<Set<String>> requestEntity = new HttpEntity<Set<String>>(siteIds,this.headers);
			ResponseEntity<SiteResponse> responseEntity = restTemplate.exchange(url, HttpMethod.POST, requestEntity,
					SiteResponse.class);
			HttpStatus statusCode = responseEntity.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				SiteResponse siteResponseObject = responseEntity.getBody();
				return siteResponseObject;
			} else {
				LOGGER.error("Error while calling XMSYSTEM delete Multi Site REST Service, returns the status code: ", //$NON-NLS-1$
						statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException)e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM delete Multi Site REST Service!", e); //$NON-NLS-1$
		}
		return null;
	}
	/**
	 * Method for Update site.
	 *
	 * @param siteRequest
	 *            {@link SiteRequest}
	 * @return true, if successful
	 */
	public boolean updateSite(com.magna.xmbackend.vo.jpa.site.SiteRequest siteRequest) throws UnauthorizedAccessException, CannotCreateObjectException {
		try {
			String url = new String(this.serviceUrl + XMAdminRestClientConstants.UPDATE_SITE);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<SiteRequest> request = new HttpEntity<SiteRequest>(siteRequest, this.headers);
			ResponseEntity<SitesTbl> responseEntity = restTemplate.postForEntity(url, request, SitesTbl.class);
			HttpStatus statusCode = responseEntity.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				return true;
			} else {
				LOGGER.error("Error while calling XMSYSTEM Upadte Site REST Service, returns the status code: ", //$NON-NLS-1$
						statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException)e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			} else if (e instanceof HttpStatusCodeException) {
				String errorResponse = ((HttpStatusCodeException) e).getResponseBodyAsString();
				String errCode = RestClientExpHandlerUtil.handleCannotCreateObjectException(LOGGER, (HttpStatusCodeException) e);
				throw new CannotCreateObjectException(errorResponse, errCode);
			}else if (e instanceof ResourceAccessException) {
				throw e;
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM Upadte Site REST Service", e); //$NON-NLS-1$
		}
		return false;
	}

	/**
	 * Method for Update site status.
	 *
	 * @param siteId
	 *            {@link String}
	 * @param status
	 *            {@link String}
	 * @return true, if successful
	 */
	public boolean updateSiteStatus(String siteId, String status) throws UnauthorizedAccessException {
		try {
			String url = new String(
					this.serviceUrl + XMAdminRestClientConstants.UPDATE_SITE_STATUS + "/" + status + "/" + siteId);

			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<Object> requestEntity = new HttpEntity<Object>(this.headers);
			ResponseEntity<Boolean> responseEntity = restTemplate.exchange(url, HttpMethod.POST, requestEntity,
					Boolean.class);
			HttpStatus statusCode = responseEntity.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				return responseEntity.getBody();
			} else {
				LOGGER.error("Error while calling XMSYSTEM Update Site Status REST Service, returns the status code: ", //$NON-NLS-1$
						statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException)e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM Update Site Status REST Service!", e); //$NON-NLS-1$
		}
		return false;
	}
	
	/**
	 * Multi update site status.
	 *
	 * @param siteRequests the site requests
	 * @return the site response
	 * @throws UnauthorizedAccessException the unauthorized access exception
	 */
	public SiteResponse multiUpdateSiteStatus(List<SiteRequest> siteRequests) throws UnauthorizedAccessException {
		try {
			String url = new String(
					this.serviceUrl + XMAdminRestClientConstants.MULTI_UPDATE_SITE_STATUS );

			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<List<SiteRequest>> requestEntity = new HttpEntity<List<SiteRequest>>(siteRequests, this.headers);
			ResponseEntity<SiteResponse> responseEntity = restTemplate.exchange(url, HttpMethod.POST, requestEntity,
					SiteResponse.class);
			HttpStatus statusCode = responseEntity.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				return responseEntity.getBody();
			} else {
				LOGGER.error("Error while calling XMSYSTEM Multi Update Site Status REST Service, returns the status code: ", //$NON-NLS-1$
						statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException)e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM Multi Update Site Status REST Service!", e); //$NON-NLS-1$
		}
		return null;
	}
	/**
	 * Gets the site by id.
	 *
	 * @param siteId {@link String}
	 * @return the site by id
	 */
	public SitesTbl getSiteById(String siteId) throws UnauthorizedAccessException {
		try {
			String url = new String(this.serviceUrl + XMAdminRestClientConstants.GET_SITE_BY_ID + "/" + siteId);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<Object> requestEntity = new HttpEntity<Object>(headers);

			List<HttpMessageConverter<?>> messageConverters = new ArrayList<>();
			messageConverters.add(new MappingJackson2HttpMessageConverter());
			restTemplate.setMessageConverters(messageConverters);

			ResponseEntity<SitesTbl> responseList = restTemplate.exchange(url, HttpMethod.GET, requestEntity,
					SitesTbl.class);
			HttpStatus statusCode = responseList.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				SitesTbl siteTblObject = responseList.getBody();
				return siteTblObject;
			} else {
				LOGGER.error("Error while calling XMSYSTEM Get Site by Id REST Service, returns the status code: " //$NON-NLS-1$
						+ statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException)e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			} else if (e instanceof HttpStatusCodeException) {
				RestClientExpHandlerUtil.handleNoXMLObjectFoundException(LOGGER, (HttpStatusCodeException) e);
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM Get Site by Id REST Service", e); //$NON-NLS-1$
		}
		return null;
	}
	
	/**
	 * Gets the admin area by id.
	 *
	 * @param siteId {@link String}
	 * @return the admin area by id
	 */
	public List<SiteAdminAreaRelIdWithAdminArea> getAllAdminAreasBySiteId(String siteId) throws UnauthorizedAccessException {
		List<SiteAdminAreaRelIdWithAdminArea> adminAreaTblList = new ArrayList<SiteAdminAreaRelIdWithAdminArea>();
		try {
			String url = new String(this.serviceUrl + XMAdminRestClientConstants.GET_ADMIN_AREAS_BY_SITE_ID + "/" + siteId);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<Object> requestEntity = new HttpEntity<Object>(headers);

			List<HttpMessageConverter<?>> messageConverters = new ArrayList<>();
			messageConverters.add(new MappingJackson2HttpMessageConverter());
			restTemplate.setMessageConverters(messageConverters);

			ResponseEntity<SiteAdminAreaRelIdWithAdminAreaResponse> responseList = restTemplate.exchange(url, HttpMethod.GET, requestEntity,
					SiteAdminAreaRelIdWithAdminAreaResponse.class);
			HttpStatus statusCode = responseList.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				SiteAdminAreaRelIdWithAdminAreaResponse adminAreaResponseObject = responseList.getBody();
				adminAreaTblList = adminAreaResponseObject.getAreaRelIdWithAdminAreas();
			} else {
				LOGGER.error("Error while calling XMSYSTEM Get All Administration Area by Id REST Service, returns the status code: " //$NON-NLS-1$
						+ statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException)e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			} else if (e instanceof HttpStatusCodeException) {
				RestClientExpHandlerUtil.handleNoXMLObjectFoundException(LOGGER, (HttpStatusCodeException) e);
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM Get All Administration Area by Id REST Service", e); //$NON-NLS-1$
		}
		return adminAreaTblList;
	}
	
	/**
	 * Gets the all admin areas by site name.
	 *
	 * @param siteName {@link String}
	 * @return the all admin areas by site name
	 */
	public List<SiteAdminAreaRelIdWithAdminArea> getAllAdminAreasBySiteName(String siteName) throws UnauthorizedAccessException {
		List<SiteAdminAreaRelIdWithAdminArea> adminAreaTblList = new ArrayList<SiteAdminAreaRelIdWithAdminArea>();
		try {
			String url = new String(this.serviceUrl + XMAdminRestClientConstants.GET_ADMIN_AREAS_BY_SITE_NAME + "/" + siteName);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<Object> requestEntity = new HttpEntity<Object>(headers);

			List<HttpMessageConverter<?>> messageConverters = new ArrayList<>();
			messageConverters.add(new MappingJackson2HttpMessageConverter());
			restTemplate.setMessageConverters(messageConverters);

			ResponseEntity<SiteAdminAreaRelIdWithAdminAreaResponse> responseList = restTemplate.exchange(url, HttpMethod.GET, requestEntity,
					SiteAdminAreaRelIdWithAdminAreaResponse.class);
			SiteAdminAreaRelIdWithAdminAreaResponse body = responseList.getBody();
			HttpStatus statusCode = responseList.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				SiteAdminAreaRelIdWithAdminAreaResponse siteAdminAreaRespObj = body;
				adminAreaTblList = siteAdminAreaRespObj.getAreaRelIdWithAdminAreas();
			} else {
				LOGGER.error("Error while calling XMSYSTEM Get All Administration Area by Site Name REST Service, returns the status code: " //$NON-NLS-1$
						+ statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException)e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			} else if (e instanceof HttpStatusCodeException) {
				RestClientExpHandlerUtil.handleNoXMLObjectFoundException(LOGGER, (HttpStatusCodeException) e);
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM Get All Administration Area by Site Name REST Service", e); //$NON-NLS-1$
		}
		return RestClientSortingUtil.sortAllAdminAreasBySiteNameTbl(adminAreaTblList);
	}
	
	
	/**
	 * Gets the site by name.
	 *
	 * @param siteName {@link String}
	 * @return the site by name
	 */
	public SitesTbl getSiteByName(final String siteName) throws UnauthorizedAccessException {
		try {
			String url = new String(this.serviceUrl + XMAdminRestClientConstants.GET_SITE_BY_NAME + "/" + siteName);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<Object> requestEntity = new HttpEntity<Object>(headers);

			List<HttpMessageConverter<?>> messageConverters = new ArrayList<>();
			messageConverters.add(new MappingJackson2HttpMessageConverter());
			restTemplate.setMessageConverters(messageConverters);

			ResponseEntity<SitesTbl> responseList = restTemplate.exchange(url, HttpMethod.GET, requestEntity,
					SitesTbl.class);
			HttpStatus statusCode = responseList.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				SitesTbl siteTblObject = responseList.getBody();
				return siteTblObject;
			} else {
				LOGGER.error("Error while calling XMSYSTEM Get Site by Name REST Service, returns the status code: " //$NON-NLS-1$
						+ statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException)e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			} else if (e instanceof HttpStatusCodeException) {
				RestClientExpHandlerUtil.handleNoXMLObjectFoundException(LOGGER, (HttpStatusCodeException) e);
			} else if (e instanceof ResourceAccessException) {
				throw e;
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM Get Site by Name REST Service", e); //$NON-NLS-1$
		}
		return null;
	}
	
	/**
	 * Gets the all project by site name.
	 *
	 * @param siteName the site name
	 * @return the all project by site name
	 */
	public ProjectResponse getAllProjectBySiteName(final String siteName) throws UnauthorizedAccessException {
		try {
			String url = new String(this.serviceUrl + XMAdminRestClientConstants.GET_PROJECTS_BY_SITE_NAME + "/" + siteName);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<Object> requestEntity = new HttpEntity<Object>(headers);

			List<HttpMessageConverter<?>> messageConverters = new ArrayList<>();
			messageConverters.add(new MappingJackson2HttpMessageConverter());
			restTemplate.setMessageConverters(messageConverters);

			ResponseEntity<ProjectResponse> responseList = restTemplate.exchange(url, HttpMethod.GET, requestEntity, ProjectResponse.class);
			HttpStatus statusCode = responseList.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				ProjectResponse projectResponse = responseList.getBody();
				return projectResponse;
			} else {
				LOGGER.error("Error while calling XMSYSTEM Get Projects by Site Name REST Service, returns the status code: " //$NON-NLS-1$
						+ statusCode.value());
			}
		}  catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException)e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			} else if (e instanceof HttpStatusCodeException) {
				RestClientExpHandlerUtil.handleNoXMLObjectFoundException(LOGGER, (HttpStatusCodeException) e);
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM Get Projects by Site Name REST Service", e); //$NON-NLS-1$
		}
		return null;
	}
	

	/**
	 * Gets the projects by site id.
	 *
	 * @param siteId {@link String}
	 * @return the projects by site id
	 */
	public ProjectResponse getProjectsBySiteId(final String siteId) throws UnauthorizedAccessException {
		try {
			String url = new String(this.serviceUrl + XMAdminRestClientConstants.GET_PROJECTS_BY_SITE_ID + "/" + siteId);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<Object> requestEntity = new HttpEntity<Object>(headers);

			List<HttpMessageConverter<?>> messageConverters = new ArrayList<>();
			messageConverters.add(new MappingJackson2HttpMessageConverter());
			restTemplate.setMessageConverters(messageConverters);

			ResponseEntity<ProjectResponse> responseList = restTemplate.exchange(url, HttpMethod.GET, requestEntity, ProjectResponse.class);
			HttpStatus statusCode = responseList.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				ProjectResponse projectResponse = responseList.getBody();
				return projectResponse;
			} else {
				LOGGER.error("Error while calling XMSYSTEM Get Projects by SiteId REST Service, returns the status code: " //$NON-NLS-1$
						+ statusCode.value());
			} 
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException)e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			} else if (e instanceof HttpStatusCodeException) {
				RestClientExpHandlerUtil.handleNoXMLObjectFoundException(LOGGER, (HttpStatusCodeException) e);
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM Get Projects by SiteId REST Service", e); //$NON-NLS-1$
		}
		return null;
	}
	
	/**
	 * Gets the site by project id.
	 *
	 * @param projectId {@link String}
	 * @return the site by project id
	 */
	public SiteResponse getSitesByProjectId(String projectId) throws UnauthorizedAccessException {
		try {
			String url = new String(this.serviceUrl + XMAdminRestClientConstants.GET_SITES_BY_PROJECT_ID + "/" + projectId);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<Object> requestEntity = new HttpEntity<Object>(headers);

			List<HttpMessageConverter<?>> messageConverters = new ArrayList<>();
			messageConverters.add(new MappingJackson2HttpMessageConverter());
			restTemplate.setMessageConverters(messageConverters);

			ResponseEntity<SiteResponse> responseList = restTemplate.exchange(url, HttpMethod.GET, requestEntity,
					SiteResponse.class);
			HttpStatus statusCode = responseList.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				SiteResponse siteResponseObject = responseList.getBody();
				return siteResponseObject;
			} else {
				LOGGER.error("Error while calling XMSYSTEM Get All Sites By Project Id REST Service, returns the status code: " //$NON-NLS-1$
						+ statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException)e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			} else if (e instanceof HttpStatusCodeException) {
				RestClientExpHandlerUtil.handleNoXMLObjectFoundException(LOGGER, (HttpStatusCodeException) e);
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM Get All Sites By Project Id REST Service", e); //$NON-NLS-1$
		}
		return null;
	}
	
	/**
	 * Gets the sites by user app id.
	 *
	 * @param userAppId {@link String}
	 * @return the sites by user app id
	 */
	public List<SitesTbl> getSitesByUserAppId(String userAppId) throws UnauthorizedAccessException {
		try {
			String url = new String(this.serviceUrl + XMAdminRestClientConstants.GET_SITES_BY_USER_APP_ID + "/" + userAppId);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<Object> requestEntity = new HttpEntity<Object>(headers);

			List<HttpMessageConverter<?>> messageConverters = new ArrayList<>();
			messageConverters.add(new MappingJackson2HttpMessageConverter());
			restTemplate.setMessageConverters(messageConverters);

			ResponseEntity<SiteResponse> responseList = restTemplate.exchange(url, HttpMethod.GET, requestEntity,
					SiteResponse.class);
			HttpStatus statusCode = responseList.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				SiteResponse siteResponseObject = responseList.getBody();
				List<SitesTbl> siteTblList = (List<SitesTbl>) siteResponseObject.getSitesTbls();
				return siteTblList;
			} else {
				LOGGER.error("Error while calling XMSYSTEM Get All Sites By UserAppId REST Service, returns the status code: " //$NON-NLS-1$
						+ statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException)e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			} else if (e instanceof HttpStatusCodeException) {
				RestClientExpHandlerUtil.handleNoXMLObjectFoundException(LOGGER, (HttpStatusCodeException) e);
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM Get All Sites By UserAppId REST Service", e); //$NON-NLS-1$
		}
		return null;
	}
	
	/**
	 * Gets the sites by project app id.
	 *
	 * @param projectAppId {@link String}
	 * @return the sites by project app id
	 */
	public SiteResponse getSitesByProjectAppId(final String projectAppId) throws UnauthorizedAccessException {
		try {
			String url = new String(this.serviceUrl + XMAdminRestClientConstants.GET_SITES_BY_PROJECT_APP_ID + "/" + projectAppId);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<Object> requestEntity = new HttpEntity<Object>(headers);

			List<HttpMessageConverter<?>> messageConverters = new ArrayList<>();
			messageConverters.add(new MappingJackson2HttpMessageConverter());
			restTemplate.setMessageConverters(messageConverters);

			ResponseEntity<SiteResponse> responseList = restTemplate.exchange(url, HttpMethod.GET, requestEntity,
					SiteResponse.class);
			HttpStatus statusCode = responseList.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				SiteResponse siteResponseObject = responseList.getBody();
				return siteResponseObject;
			} else {
				LOGGER.error("Error while calling XMSYSTEM Get All Sites By ProjectAppId REST Service, returns the status code: " //$NON-NLS-1$
						+ statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException)e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			} else if (e instanceof HttpStatusCodeException) {
				RestClientExpHandlerUtil.handleNoXMLObjectFoundException(LOGGER, (HttpStatusCodeException) e);
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM Get All Sites By ProjectAppId REST Service", e); //$NON-NLS-1$
		}
		return null;
	}
	
	/**
	 * Gets the sites by start app id.
	 *
	 * @param startAppId {@link String}
	 * @return the sites by start app id
	 */
	public SiteResponse getSitesByStartAppId(final String startAppId) throws UnauthorizedAccessException {
		try {
			String url = new String(this.serviceUrl + XMAdminRestClientConstants.GET_SITES_BY_START_APP_ID + "/" + startAppId);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<Object> requestEntity = new HttpEntity<Object>(headers);

			List<HttpMessageConverter<?>> messageConverters = new ArrayList<>();
			messageConverters.add(new MappingJackson2HttpMessageConverter());
			restTemplate.setMessageConverters(messageConverters);

			ResponseEntity<SiteResponse> responseList = restTemplate.exchange(url, HttpMethod.GET, requestEntity,
					SiteResponse.class);
			HttpStatus statusCode = responseList.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				SiteResponse siteResponseObject = responseList.getBody();
				return siteResponseObject;
			} else {
				LOGGER.error("Error while calling XMSYSTEM Get All Sites By StartAppId REST Service, returns the status code: " //$NON-NLS-1$
						+ statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException)e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			} else if (e instanceof HttpStatusCodeException) {
				RestClientExpHandlerUtil.handleNoXMLObjectFoundException(LOGGER, (HttpStatusCodeException) e);
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM Get All Sites By StartAppId REST Service", e); //$NON-NLS-1$
		}
		return null;
	}
	
	/**
	 * Gets the sites by AA id.
	 *
	 * @param adminAreaId {@link String}
	 * @return the sites by AA id
	 */
	public SiteResponse getSitesByAAId(final String adminAreaId) throws UnauthorizedAccessException {
		try {
			String url = new String(this.serviceUrl + XMAdminRestClientConstants.GET_SITES_BY_ADMIN_AREA_ID + "/" + adminAreaId);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<Object> requestEntity = new HttpEntity<Object>(headers);

			List<HttpMessageConverter<?>> messageConverters = new ArrayList<>();
			messageConverters.add(new MappingJackson2HttpMessageConverter());
			restTemplate.setMessageConverters(messageConverters);

			ResponseEntity<SiteResponse> responseList = restTemplate.exchange(url, HttpMethod.GET, requestEntity,
					SiteResponse.class);
			HttpStatus statusCode = responseList.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				SiteResponse siteResponseObject = responseList.getBody();
				return siteResponseObject;
			} else {
				LOGGER.error("Error while calling XMSYSTEM Get All Sites By AdminAreaId REST Service, returns the status code: " //$NON-NLS-1$
						+ statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException)e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			} else if (e instanceof HttpStatusCodeException) {
				RestClientExpHandlerUtil.handleNoXMLObjectFoundException(LOGGER, (HttpStatusCodeException) e);
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM Get All Sites By AdminAreaId REST Service", e); //$NON-NLS-1$
		}
		return null;
	}
	
}