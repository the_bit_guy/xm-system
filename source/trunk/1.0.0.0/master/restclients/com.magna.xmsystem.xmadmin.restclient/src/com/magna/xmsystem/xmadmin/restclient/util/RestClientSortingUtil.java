package com.magna.xmsystem.xmadmin.restclient.util;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import com.magna.xmbackend.entities.AdminAreasTbl;
import com.magna.xmbackend.entities.BaseApplicationsTbl;
import com.magna.xmbackend.entities.DirectoryTbl;
import com.magna.xmbackend.entities.EmailNotificationConfigTbl;
import com.magna.xmbackend.entities.GroupsTbl;
import com.magna.xmbackend.entities.ProjectApplicationsTbl;
import com.magna.xmbackend.entities.ProjectsTbl;
import com.magna.xmbackend.entities.RoleAdminAreaRelTbl;
import com.magna.xmbackend.entities.RoleUserRelTbl;
import com.magna.xmbackend.entities.RolesTbl;
import com.magna.xmbackend.entities.SiteAdminAreaRelTbl;
import com.magna.xmbackend.entities.SitesTbl;
import com.magna.xmbackend.entities.StartApplicationsTbl;
import com.magna.xmbackend.entities.UserApplicationsTbl;
import com.magna.xmbackend.entities.UserProjectRelTbl;
import com.magna.xmbackend.entities.UserStartAppRelTbl;
import com.magna.xmbackend.entities.UserUserAppRelTbl;
import com.magna.xmbackend.entities.UsersTbl;
import com.magna.xmbackend.response.rel.adminareaproject.AdminAreaProjectResponse;
import com.magna.xmbackend.response.rel.adminareaprojectapp.AdminAreaProjectAppResponse;
import com.magna.xmbackend.response.rel.adminareastartapp.AdminAreaStartAppRelation;
import com.magna.xmbackend.response.rel.adminareauserapp.AdminAreaUserAppRelation;
import com.magna.xmbackend.response.rel.userstartapp.UserStartAppRelation;
import com.magna.xmbackend.response.rel.useruserapp.UserUserAppRelation;
import com.magna.xmbackend.vo.directory.DirectoryRelResponse;
import com.magna.xmbackend.vo.enums.DirectoryObjectType;
import com.magna.xmbackend.vo.group.GroupRelResponse;
import com.magna.xmbackend.vo.jpa.site.SiteAdminAreaRelIdWithAdminArea;
import com.magna.xmbackend.vo.jpa.site.SiteAdminAreaRelIdWithAdminAreaProjRel;
import com.magna.xmbackend.vo.jpa.site.SiteAdminAreaRelIdWithAdminAreaStartAppRel;
import com.magna.xmbackend.vo.jpa.site.SiteAdminAreaRelIdWithAdminAreaUsrAppRel;
import com.magna.xmbackend.vo.liveMessage.LiveMessageResponse;
import com.magna.xmbackend.vo.notification.NotificationResponse;
import com.magna.xmbackend.vo.rel.AdminAreaProjectRelIdWithProjectAppRel;
import com.magna.xmbackend.vo.rel.AdminAreaProjectRelIdWithProjectStartAppRel;
import com.magna.xmsystem.dependencies.utils.XMSystemUtil;

/**
 * The Class RestClientSortingUtil.
 * 
 * @author Deepak Upadhyay
 */
public class RestClientSortingUtil {

	/**
	 * Sort site tbl.
	 *
	 * @param siteTblList
	 *            the site tbl list
	 * @return the list
	 */
	public static List<SitesTbl> sortSiteTbl(List<SitesTbl> siteTblList) {
		siteTblList.sort((siteResponse1, siteResponse2) -> {
			if (siteResponse1 != null && siteResponse2 != null) {
				final String siteName1 = siteResponse1.getName();
				final String siteName2 = siteResponse2.getName();
				if (!XMSystemUtil.isEmpty(siteName1) && !XMSystemUtil.isEmpty(siteName2)) {
					return siteName1.compareTo(siteName2);
				}
			}
			return 0;
		});
		return siteTblList;
	}

	/**
	 * Sort site admin area tbl.
	 *
	 * @param siteAdminAreaRelTblList
	 *            the site admin area rel tbl list
	 * @return the list
	 */
	public static List<SiteAdminAreaRelTbl> sortSiteAdminAreaTbl(List<SiteAdminAreaRelTbl> siteAdminAreaRelTblList) {
		Collections.sort(siteAdminAreaRelTblList, (SiteAdminAreaRelTbl sar1, SiteAdminAreaRelTbl sar2) -> {
			final AdminAreasTbl adminAreasTbl1 = sar1.getAdminAreaId();
			final AdminAreasTbl adminAreasTbl2 = sar2.getAdminAreaId();
			return compareAdminAreaByName(adminAreasTbl1, adminAreasTbl2);
		});
		return siteAdminAreaRelTblList;
	}

	/**
	 * @param adminAreasTbl1
	 * @param adminAreasTbl2
	 * @return
	 */
	public static int compareAdminAreaByName(final AdminAreasTbl adminAreasTbl1, final AdminAreasTbl adminAreasTbl2) {
		if (adminAreasTbl1 != null && adminAreasTbl2 != null) {
			final String adminAreaName1 = adminAreasTbl1.getName();
			final String adminAreaName2 = adminAreasTbl2.getName();
			if (!XMSystemUtil.isEmpty(adminAreaName1) && !XMSystemUtil.isEmpty(adminAreaName2)) {
				return adminAreaName1.compareTo(adminAreaName2);
			}
		}
		return 0;
	}

	/**
	 * Sort projects by site admin rel tbl.
	 *
	 * @param siteTblList
	 *            the site tbl list
	 * @return the list
	 */
	public static List<SiteAdminAreaRelIdWithAdminAreaProjRel> sortProjectsBySiteAdminRelTbl(
			List<SiteAdminAreaRelIdWithAdminAreaProjRel> siteTblList) {
		Collections.sort(siteTblList,
				(SiteAdminAreaRelIdWithAdminAreaProjRel sa1, SiteAdminAreaRelIdWithAdminAreaProjRel sa2) -> {
					final ProjectsTbl projectsTbl1 = sa1.getProjectsTbl();
					final ProjectsTbl projectsTbl2 = sa2.getProjectsTbl();
					return compareProjectByName(projectsTbl1, projectsTbl2);
				});
		return siteTblList;
	}

	/**
	 * @param projectsTbl1
	 * @param projectsTbl2
	 * @return
	 */
	public static int compareProjectByName(final ProjectsTbl projectsTbl1, final ProjectsTbl projectsTbl2) {
		if (projectsTbl1 != null && projectsTbl2 != null) {
			final String projectName1 = projectsTbl1.getName();
			final String projectName2 = projectsTbl2.getName();
			if (!XMSystemUtil.isEmpty(projectName1) && !XMSystemUtil.isEmpty(projectName2)) {
				return projectName1.compareTo(projectName2);
			}
		}
		return 0;
	}

	/**
	 * Sort all project apps by site admin proj rel tbl.
	 *
	 * @param adminAreaProjectRelIdWithProjectAppRels
	 *            the admin area project rel id with project app rels
	 * @return the list
	 */
	public static List<AdminAreaProjectRelIdWithProjectAppRel> sortAllProjectAppsBySiteAdminProjRelTbl(
			List<AdminAreaProjectRelIdWithProjectAppRel> adminAreaProjectRelIdWithProjectAppRels) {
		Collections.sort(adminAreaProjectRelIdWithProjectAppRels,
				(AdminAreaProjectRelIdWithProjectAppRel aapr1, AdminAreaProjectRelIdWithProjectAppRel aapr2) -> {
					final ProjectApplicationsTbl projectApplicationsTbl1 = aapr1.getProjectApplicationsTbl();
					final ProjectApplicationsTbl projectApplicationsTbl2 = aapr2.getProjectApplicationsTbl();
					return compareProjectAppByName(projectApplicationsTbl1, projectApplicationsTbl2);
				});
		return adminAreaProjectRelIdWithProjectAppRels;
	}

	/**
	 * @param projectApplicationsTbl1
	 * @param projectApplicationsTbl2
	 * @return
	 */
	public static int compareProjectAppByName(final ProjectApplicationsTbl projectApplicationsTbl1,
			final ProjectApplicationsTbl projectApplicationsTbl2) {
		if (projectApplicationsTbl1 != null && projectApplicationsTbl2 != null) {
			final String proAppName1 = projectApplicationsTbl1.getName();
			final String proAppName2 = projectApplicationsTbl2.getName();
			if (!XMSystemUtil.isEmpty(proAppName1) && !XMSystemUtil.isEmpty(proAppName2)) {
				return proAppName1.compareTo(proAppName2);
			}
		}
		return 0;
	}

	/**
	 * Sort site admin area rel id with admin area usr app rel tbl.
	 *
	 * @param siteAdminAreaRelIdWithAdminAreaUsrAppRel
	 *            the site admin area rel id with admin area usr app rel
	 * @return the list
	 */
	public static List<SiteAdminAreaRelIdWithAdminAreaUsrAppRel> sortSiteAdminAreaRelIdWithAdminAreaUsrAppRelTbl(
			List<SiteAdminAreaRelIdWithAdminAreaUsrAppRel> siteAdminAreaRelIdWithAdminAreaUsrAppRel) {
		Collections.sort(siteAdminAreaRelIdWithAdminAreaUsrAppRel,
				(SiteAdminAreaRelIdWithAdminAreaUsrAppRel sa1, SiteAdminAreaRelIdWithAdminAreaUsrAppRel sa2) -> {
					final UserApplicationsTbl userAppsTbl1 = sa1.getUserApplicationsTbl();
					final UserApplicationsTbl userAppsTbl2 = sa2.getUserApplicationsTbl();
					return compareUserAppByName(userAppsTbl1, userAppsTbl2);
				});
		return siteAdminAreaRelIdWithAdminAreaUsrAppRel;

	}

	/**
	 * Compare user app by name.
	 *
	 * @param userAppsTbl1 the user apps tbl 1
	 * @param userAppsTbl2 the user apps tbl 2
	 * @return the int
	 */
	public static int compareUserAppByName(final UserApplicationsTbl userAppsTbl1,
			final UserApplicationsTbl userAppsTbl2) {
		if (userAppsTbl1 != null && userAppsTbl2 != null) {
			final String userAppName1 = userAppsTbl1.getName();
			final String userAppName2 = userAppsTbl2.getName();
			if (!XMSystemUtil.isEmpty(userAppName1) && !XMSystemUtil.isEmpty(userAppName2)) {
				return userAppName1.compareTo(userAppName2);
			}
		}
		return 0;
	}

	/**
	 * Sort start apps by site admin rel tbl.
	 *
	 * @param siteAdminAreaRelIdWithAdminAreaStartAppRel
	 *            the site admin area rel id with admin area start app rel
	 * @return the list
	 */
	public static List<SiteAdminAreaRelIdWithAdminAreaStartAppRel> sortStartAppsBySiteAdminRelTbl(
			List<SiteAdminAreaRelIdWithAdminAreaStartAppRel> siteAdminAreaRelIdWithAdminAreaStartAppRel) {
		Collections.sort(siteAdminAreaRelIdWithAdminAreaStartAppRel,
				(SiteAdminAreaRelIdWithAdminAreaStartAppRel sa1, SiteAdminAreaRelIdWithAdminAreaStartAppRel sa2) -> {
					StartApplicationsTbl startAppsTbl1 = sa1.getStartApplicationsTbl();
					StartApplicationsTbl startAppsTbl2 = sa2.getStartApplicationsTbl();
					return compareStartAppByName(startAppsTbl1, startAppsTbl2);
				});
		return siteAdminAreaRelIdWithAdminAreaStartAppRel;
	}

	/**
	 * Sort admin area tbl.
	 *
	 * @param adminAreaTblList
	 *            the admin area tbl list
	 * @return the list
	 */
	public static List<AdminAreasTbl> sortAdminAreaTbl(List<AdminAreasTbl> adminAreaTblList) {
		adminAreaTblList.sort((adminAreasTbl1, adminAreasTbl2) -> {
			return compareAdminAreaByName(adminAreasTbl1, adminAreasTbl2);
		});
		return adminAreaTblList;
	}

	/**
	 * Sort all start apps by site admin rel tbl.
	 *
	 * @param adminAreaProjectRelIdWithProjectStartAppRels
	 *            the admin area project rel id with project start app rels
	 * @return the list
	 */
	public static List<AdminAreaProjectRelIdWithProjectStartAppRel> sortAllStartAppsBySiteAdminRelTbl(
			List<AdminAreaProjectRelIdWithProjectStartAppRel> adminAreaProjectRelIdWithProjectStartAppRels) {
		Collections.sort(adminAreaProjectRelIdWithProjectStartAppRels,
				(AdminAreaProjectRelIdWithProjectStartAppRel aapr1, AdminAreaProjectRelIdWithProjectStartAppRel aapr2) -> {
					StartApplicationsTbl startsTbl1 = aapr1.getStartApplicationsTbl();
					StartApplicationsTbl startsTbl2 = aapr2.getStartApplicationsTbl();
					return compareStartAppByName(startsTbl1, startsTbl2);
				});
		return adminAreaProjectRelIdWithProjectStartAppRels;
	}

	/**
	 * Sort AA project rel by project id tbl.
	 *
	 * @param adminAreaProjectAppResponse
	 *            the admin area project app response
	 * @return the list
	 */
	public static List<AdminAreaProjectResponse> sortAAProjectRelByProjectIdTbl(
			List<AdminAreaProjectResponse> adminAreaProjectAppResponse) {
		Collections.sort(adminAreaProjectAppResponse, new Comparator<AdminAreaProjectResponse>() {

			@Override
			public int compare(AdminAreaProjectResponse aap1, AdminAreaProjectResponse aap2) {
				AdminAreasTbl adminAreasTbl1 = aap1.getAdminAreasTbls();
				AdminAreasTbl adminAreasTbl2 = aap2.getAdminAreasTbls();
				return compareAdminAreaByName(adminAreasTbl1, adminAreasTbl2);
			}
		});
		return adminAreaProjectAppResponse;
	}

	/**
	 * Sort project tbl.
	 *
	 * @param projectTblList
	 *            the project tbl list
	 * @return the list
	 */
	public static List<ProjectsTbl> sortProjectTbl(List<ProjectsTbl> projectTblList) {
		projectTblList.sort((projectTbl1, projectTbl2) -> {
			return compareProjectByName(projectTbl1, projectTbl2);
		});
		return projectTblList;

	}

	/**
	 * Sort user project rel by project id tbl.
	 *
	 * @param userProjectRelTblList
	 *            the user project rel tbl list
	 * @return the list
	 */
	public static List<UserProjectRelTbl> sortUserProjectRelByProjectIdTbl(
			List<UserProjectRelTbl> userProjectRelTblList) {
		Collections.sort(userProjectRelTblList, (UserProjectRelTbl upr1, UserProjectRelTbl upr2) -> {
			UsersTbl userTbl1 = upr1.getUserId();
			UsersTbl userTbl2 = upr2.getUserId();
			return compareUserByUsername(userTbl1, userTbl2);
		});
		return userProjectRelTblList;
	}

	/**
	 * @param userTbl1
	 * @param userTbl2
	 * @return
	 */
	public static int compareUserByUsername(final UsersTbl userTbl1, final UsersTbl userTbl2) {
		if (userTbl1 != null && userTbl2 != null) {
			final String username1 = userTbl1.getUsername();
			final String username2 = userTbl2.getUsername();
			if (!XMSystemUtil.isEmpty(username1) && !XMSystemUtil.isEmpty(username2)) {
				return username1.compareTo(username2);
			}
		}
		return 0;
	}

	/**
	 * Sort project app by project id tbl.
	 *
	 * @param projectProjectRelTblList
	 *            the project project rel tbl list
	 * @return the list
	 */
	public static List<ProjectApplicationsTbl> sortProjectAppByProjectIdTbl(
			List<ProjectApplicationsTbl> projectProjectRelTblList) {
		Collections.sort(projectProjectRelTblList, (ProjectApplicationsTbl pa1, ProjectApplicationsTbl pa2) -> {
			return compareProjectAppByName(pa1, pa2);
		});
		return projectProjectRelTblList;
	}

	/**
	 * Sort user list.
	 *
	 * @param usersTblList
	 *            the users tbl list
	 */
	public static void sortUserList(List<UsersTbl> usersTblList) {
		usersTblList.sort((userTbl1, userTbl2) -> {
			return compareUserByUsername(userTbl1, userTbl2);
		});
	}

	/**
	 * Sort user project rel by user id tbl.
	 *
	 * @param userProjectRelTblList
	 *            the user project rel tbl list
	 * @return the list
	 */
	public static List<UserProjectRelTbl> sortUserProjectRelByUserIdTbl(List<UserProjectRelTbl> userProjectRelTblList) {
		Collections.sort(userProjectRelTblList, (UserProjectRelTbl up1, UserProjectRelTbl up2) -> {
			ProjectsTbl projectId1 = up1.getProjectId();
			ProjectsTbl projectId2 = up2.getProjectId();
			return compareProjectByName(projectId1, projectId2);
		});
		return userProjectRelTblList;
	}

	/**
	 * Sort user app tbl.
	 *
	 * @param userApplicationsTblList
	 *            the user applications tbl list
	 * @return the list
	 */
	public static List<UserApplicationsTbl> sortUserAppTbl(List<UserApplicationsTbl> userApplicationsTblList) {
		userApplicationsTblList.sort((userAppsTbl1, userAppsTbl2) -> {
			return compareUserAppByName(userAppsTbl1, userAppsTbl2);
		});
		return userApplicationsTblList;
	}

	/**
	 * Sort admin area user app tbl.
	 *
	 * @param adminAreaUserAppList
	 *            the admin area user app list
	 * @return the list
	 */
	public static List<AdminAreaUserAppRelation> sortAdminAreaUserAppTbl(
			List<AdminAreaUserAppRelation> adminAreaUserAppList) {
		Collections.sort(adminAreaUserAppList, new Comparator<AdminAreaUserAppRelation>() {
			@Override
			public int compare(AdminAreaUserAppRelation aau1, AdminAreaUserAppRelation aau2) {
				AdminAreasTbl adminAreasTbl1 = aau1.getSiteAdminAreaRelId().getAdminAreaId();
				AdminAreasTbl adminAreasTbl2 = aau2.getSiteAdminAreaRelId().getAdminAreaId();
				return compareAdminAreaByName(adminAreasTbl1, adminAreasTbl2);
			}
		});
		return adminAreaUserAppList;
	}

	/**
	 * Sort user user app tbl.
	 *
	 * @param userUserAppRelations
	 *            the user user app relations
	 * @return the list
	 */
	public static List<UserUserAppRelation> sortUserUserAppTbl(List<UserUserAppRelation> userUserAppRelations) {
		Collections.sort(userUserAppRelations, (UserUserAppRelation sa1, UserUserAppRelation sa2) -> {
			final UsersTbl usersTbl1 = sa1.getUserId();
			final UsersTbl usersTbl2 = sa2.getUserId();
			if (usersTbl1 != null && usersTbl2 != null) {
				return compareUserByUsername(usersTbl1, usersTbl2);
			}
			return 0;
		});
		return userUserAppRelations;
	}

	/**
	 * Sort admin area by proj app id tbl.
	 *
	 * @param adminAreaProjectAppResponse
	 *            the admin area project app response
	 * @return the list
	 */
	public static List<AdminAreaProjectAppResponse> sortAdminAreaByProjAppIdTbl(
			List<AdminAreaProjectAppResponse> adminAreaProjectAppResponse) {
		Collections.sort(adminAreaProjectAppResponse, new Comparator<AdminAreaProjectAppResponse>() {

			@Override
			public int compare(AdminAreaProjectAppResponse aap1, AdminAreaProjectAppResponse aap2) {
				AdminAreasTbl adminAreasTbl1 = aap1.getAdminAreasTbl();
				AdminAreasTbl adminAreasTbl2 = aap2.getAdminAreasTbl();
				compareAdminAreaByName(adminAreasTbl1, adminAreasTbl2);
				return 0;
			}
		});
		return adminAreaProjectAppResponse;
	}

	/**
	 * Sort project app tbl.
	 *
	 * @param projectApplicationsTblList
	 *            the project applications tbl list
	 * @return the list
	 */
	public static List<ProjectApplicationsTbl> sortProjectAppTbl(
			List<ProjectApplicationsTbl> projectApplicationsTblList) {
		projectApplicationsTblList.sort((projectApplicationsTbl1, projectApplicationsTbl2) -> {
			return compareProjectAppByName(projectApplicationsTbl1, projectApplicationsTbl2);
		});
		return projectApplicationsTblList;
	}

	/**
	 * Sort user start app rel by user id tbl.
	 *
	 * @param userStartAppRelTblList
	 *            the user start app rel tbl list
	 * @return the list
	 */
	public static List<UserStartAppRelTbl> sortUserStartAppRelByUserIdTbl(
			List<UserStartAppRelTbl> userStartAppRelTblList) {
		Collections.sort(userStartAppRelTblList, (UserStartAppRelTbl us1, UserStartAppRelTbl us2) -> {
			StartApplicationsTbl startAppsTbl1 = us1.getStartApplicationId();
			StartApplicationsTbl startAppsTbl2 = us2.getStartApplicationId();
			return compareStartAppByName(startAppsTbl1, startAppsTbl2);
		});
		return userStartAppRelTblList;
	}

	/**
	 * Compare start app translation tbl.
	 *
	 * @param startAppsTbl1
	 *            the start apps tbl 1
	 * @param startAppsTbl2
	 *            the start apps tbl 2
	 * @return the int
	 */
	public static int compareStartAppByName(StartApplicationsTbl startAppsTbl1,
			StartApplicationsTbl startAppsTbl2) {
		if (startAppsTbl1 != null && startAppsTbl2 != null) {
			String startAppName1 = startAppsTbl1.getName();
			String startAppName2 = startAppsTbl2.getName();
			if (!XMSystemUtil.isEmpty(startAppName1) && !XMSystemUtil.isEmpty(startAppName2)) {
				return startAppName1.compareTo(startAppName2);
			}
		}
		return 0;
	}

	/**
	 * Sort base app tbl.
	 *
	 * @param baseAppTblsList
	 *            the base app tbls list
	 * @return the list
	 */
	public static List<BaseApplicationsTbl> sortBaseAppTbl(List<BaseApplicationsTbl> baseAppTblsList) {
		baseAppTblsList.sort((baseAppResponse1, baseAppResponse2) -> {
			if (baseAppResponse1 != null && baseAppResponse2 != null) {
				final String baseAppName1 = baseAppResponse1.getName();
				final String baseAppName2 = baseAppResponse2.getName();
				if (!XMSystemUtil.isEmpty(baseAppName1) && !XMSystemUtil.isEmpty(baseAppName2)) {
					return baseAppName1.compareTo(baseAppName2);
				}
			}
			return 0;
		});
		return baseAppTblsList;

	}

	/**
	 * Sort all user app by base app id tbl.
	 *
	 * @param baseUserAppRelTblList
	 *            the base user app rel tbl list
	 * @return the list
	 */
	public static List<UserApplicationsTbl> sortAllUserAppByBaseAppIdTbl(
			List<UserApplicationsTbl> baseUserAppRelTblList) {
		Collections.sort(baseUserAppRelTblList, (UserApplicationsTbl ua1, UserApplicationsTbl ua2) -> {
			return compareUserAppByName(ua1, ua2);
		});
		return baseUserAppRelTblList;
	}

	/**
	 * Sort start app tbl.
	 *
	 * @param startAppTblsList
	 *            the start app tbls list
	 * @return the list
	 */
	public static List<StartApplicationsTbl> sortStartAppTbl(List<StartApplicationsTbl> startAppTblsList) {
		startAppTblsList.sort((startAppResponse1, startAppResponse2) -> {
			return compareStartAppByName(startAppResponse1, startAppResponse2);
		});
		return startAppTblsList;
	}

	/**
	 * Sort AA start app rel by start app id tbl.
	 *
	 * @param adminAreaStartAppRelations
	 *            the admin area start app relations
	 * @return the list
	 */
	public static List<AdminAreaStartAppRelation> sortAAStartAppRelByStartAppIdTbl(
			List<AdminAreaStartAppRelation> adminAreaStartAppRelations) {
		Collections.sort(adminAreaStartAppRelations, new Comparator<AdminAreaStartAppRelation>() {
			@Override
			public int compare(AdminAreaStartAppRelation aa1, AdminAreaStartAppRelation aa2) {
				AdminAreasTbl adminAreasTbl1 = aa1.getSiteAdminAreaRelId().getAdminAreaId();
				AdminAreasTbl adminAreasTbl2 = aa2.getSiteAdminAreaRelId().getAdminAreaId();
				return compareAdminAreaByName(adminAreasTbl1, adminAreasTbl2);
			}
		});
		return adminAreaStartAppRelations;
	}

	/**
	 * Sort user start app rel by start app id tbl.
	 *
	 * @param userStartAppRelations
	 *            the user start app relations
	 * @return the list
	 */
	public static List<UserStartAppRelation> sortUserStartAppRelByStartAppIdTbl(
			List<UserStartAppRelation> userStartAppRelations) {
		Collections.sort(userStartAppRelations, new Comparator<UserStartAppRelation>() {
			@Override
			public int compare(UserStartAppRelation us1, UserStartAppRelation us2) {
				final UsersTbl usersTbl1 = us1.getUserId();
				final UsersTbl usersTbl2 = us2.getUserId();
				return compareUserByUsername(usersTbl1, usersTbl2);
			}
		});
		return userStartAppRelations;
	}

	/**
	 * Sort directory tbl.
	 *
	 * @param directoryTblList
	 *            the directory tbl list
	 * @return the list
	 */
	public static List<DirectoryTbl> sortDirectoryTbl(List<DirectoryTbl> directoryTblList) {
		directoryTblList.sort((directoryTbl1, directoryTbl2) -> {
			return compareDirectoryByName(directoryTbl1, directoryTbl2);
		});
		return directoryTblList;
	}

	/**
	 * Compare directory by name.
	 *
	 * @param directoryTbl1 the directory tbl 1
	 * @param directoryTbl2 the directory tbl 2
	 * @return the int
	 */
	private static int compareDirectoryByName(DirectoryTbl directoryTbl1, DirectoryTbl directoryTbl2) {
		if (directoryTbl1 != null && directoryTbl2 != null) {
			String dirName1 = directoryTbl1.getName();
			String dirName2 = directoryTbl2.getName();
			if (!XMSystemUtil.isEmpty(dirName1) && !XMSystemUtil.isEmpty(dirName2)) {
				return dirName1.compareTo(dirName2);
			}
		}
		return 0;
	}

	/**
	 * Sort directory rel by dir id and obj type tbl.
	 *
	 * @param directoryRelResponses            the directory rel responses
	 * @param objType the obj type
	 * @return the list
	 */
	public static List<DirectoryRelResponse> sortDirectoryRelByDirIdAndObjTypeTbl(
			List<DirectoryRelResponse> directoryRelResponses, final String objType) {
		Collections.sort(directoryRelResponses, new Comparator<DirectoryRelResponse>() {

			@Override
			public int compare(DirectoryRelResponse dr1, DirectoryRelResponse dr2) {
				if (DirectoryObjectType.USER.name().equals(objType)) {
					final UsersTbl userTbl1 = dr1.getUsersTbl();
					final UsersTbl userTbl2 = dr2.getUsersTbl();
					return compareUserByUsername(userTbl1, userTbl2);
				} else if (DirectoryObjectType.PROJECT.name().equals(objType)) {
					final ProjectsTbl projectTbl1 = dr1.getProjectsTbl();
					final ProjectsTbl projectTbl2 = dr2.getProjectsTbl();
					return compareProjectByName(projectTbl1, projectTbl2);
				} else if (DirectoryObjectType.USERAPPLICATION.name().equals(objType)) {
					final UserApplicationsTbl userApplicationsTbl = dr1.getUserApplicationsTbl();
					final UserApplicationsTbl userApplicationsTb2 = dr2.getUserApplicationsTbl();
					return compareUserAppByName(userApplicationsTbl, userApplicationsTb2);
				} else if (DirectoryObjectType.PROJECTAPPLICATION.name().equals(objType)) {
					final ProjectApplicationsTbl projectApplicationsTbl1 = dr1.getProjectApplicationsTbl();
					final ProjectApplicationsTbl projectApplicationsTbl2 = dr2.getProjectApplicationsTbl();
					return compareProjectAppByName(projectApplicationsTbl1, projectApplicationsTbl2);
				}
				return 0;
			}

		});

		return directoryRelResponses;
	}

	/**
	 * Sort role user rel tbl.
	 *
	 * @param roleUserTblList
	 *            the role user tbl list
	 * @return the list
	 */
	public static List<RoleUserRelTbl> sortRoleUserRelTbl(List<RoleUserRelTbl> roleUserTblList) {
		Collections.sort(roleUserTblList, (RoleUserRelTbl Ru1, RoleUserRelTbl Ru2) -> {
			final UsersTbl userId1 = Ru1.getUserId();
			final UsersTbl userId2 = Ru2.getUserId();
			return compareUserByUsername(userId1, userId2);
		});
		return roleUserTblList;

	}

	/**
	 * Sort group rel by group id.
	 *
	 * @param groupRelResponse the group rel response
	 * @return the list
	 */
	public static List<GroupRelResponse> sortGroupRelByGroupId(List<GroupRelResponse> groupRelResponse) {
		Collections.sort(groupRelResponse, new Comparator<GroupRelResponse>() {

			@Override
			public int compare(GroupRelResponse gr1, GroupRelResponse gr2) {
				if (gr1.getUsersTbl() != null) {
					final UsersTbl usersTbl = gr1.getUsersTbl();
					final UsersTbl usersTb2 = gr2.getUsersTbl();
					return compareUserByUsername(usersTbl, usersTb2);
				} else if (gr1.getProjectsTbl() != null) {
					final ProjectsTbl projectTbl1 = gr1.getProjectsTbl();
					final ProjectsTbl projectTbl2 = gr2.getProjectsTbl();
					return compareProjectByName(projectTbl1, projectTbl2);
				} else if (gr1.getUserApplicationsTbl() != null) {
					final UserApplicationsTbl userApplicationsTbl = gr1.getUserApplicationsTbl();
					final UserApplicationsTbl userApplicationsTb2 = gr2.getUserApplicationsTbl();
					return compareUserAppByName(userApplicationsTbl, userApplicationsTb2);
				} else if (gr1.getProjectApplicationsTbl() != null) {
					final ProjectApplicationsTbl projectApplicationsTbl1 = gr1.getProjectApplicationsTbl();
					final ProjectApplicationsTbl projectApplicationsTbl2 = gr2.getProjectApplicationsTbl();
					return compareProjectAppByName(projectApplicationsTbl1, projectApplicationsTbl2);
				}
				return 0;
			}
		});
		return groupRelResponse;
	}

	
	public static List<GroupsTbl> sortGroupsName(List<GroupsTbl> groupTbls) {
		Collections.sort(groupTbls, new Comparator<GroupsTbl>() {
			@Override
			public int compare(GroupsTbl grp1, GroupsTbl grp2) {
				return compareGroupByName(grp1, grp2);
			}
		});
		return groupTbls;
	}
	
	/**
	 * @param grp1
	 * @param grp2
	 * @return
	 */
	private static int compareGroupByName(GroupsTbl grp1, GroupsTbl grp2) {
		String name1 = grp1.getName();
		String name2 = grp2.getName();
		if (name1 != null && name2 != null) {
			return name1.compareTo(name2);
		}
		return 0;
	}
	
	/**
	 * Sort all admin areas by site name tbl.
	 *
	 * @param areaRelIdWithAdminAreas the area rel id with admin areas
	 * @return the list
	 */
	public static List<SiteAdminAreaRelIdWithAdminArea> sortAllAdminAreasBySiteNameTbl(List<SiteAdminAreaRelIdWithAdminArea> areaRelIdWithAdminAreas) {
		Collections.sort(areaRelIdWithAdminAreas, (SiteAdminAreaRelIdWithAdminArea sar1, SiteAdminAreaRelIdWithAdminArea sar2) -> {
			final AdminAreasTbl adminAreaId1 = sar1.getAdminAreasTbl();
			final AdminAreasTbl adminAreaId2 = sar2.getAdminAreasTbl();
			return compareAdminAreaByName(adminAreaId1, adminAreaId2);
		});
		return areaRelIdWithAdminAreas;
	}
	
	/**
	 * Sort live message tbl.
	 *
	 * @param liveMessageResponseWrapperList the live message response wrapper list
	 * @return the list
	 */
	public static List<LiveMessageResponse> sortLiveMessageTbl(List<LiveMessageResponse> liveMessageResponseWrapperList) {
		Collections.sort(liveMessageResponseWrapperList, new Comparator<LiveMessageResponse>() {
			@Override
			public int compare(LiveMessageResponse lm1, LiveMessageResponse lm2) {
				String liveMessageName = lm1.getLiveMessageName();
				String liveMessageName2 = lm2.getLiveMessageName();
				return liveMessageName.compareTo(liveMessageName2);
			}
		});
		return liveMessageResponseWrapperList;
		
	}
	
	/**
	 * Sort role tbl.
	 *
	 * @param roleTblList the role tbl list
	 * @return the list
	 */
	public static List<RolesTbl> sortRoleTbl(List<RolesTbl> roleTblList) {
		roleTblList.sort((roleResponse1, roleResponse2) -> {
			if (roleResponse1 != null
					&& roleResponse2 != null) {
				String roleName1 = roleResponse1.getName();
				String roleName2 = roleResponse2.getName();
				if(!(("SuperAdmin").equalsIgnoreCase(roleName1) || ("SuperAdmin").equalsIgnoreCase(roleName2) )){
				return roleName1.compareTo(roleName2);
				}
			}
			return 0;
		});
		return roleTblList;
	}

	/**
	 * Sort role admin area rel tbl.
	 *
	 * @param roleAdminAreaRelTblList the role admin area rel tbl list
	 * @return the list
	 */
	public static List<RoleAdminAreaRelTbl> sortRoleAdminAreaRelTbl(List<RoleAdminAreaRelTbl> roleAdminAreaRelTblList) {
		Collections.sort(roleAdminAreaRelTblList, (RoleAdminAreaRelTbl rar1, RoleAdminAreaRelTbl rar2) -> {
			final AdminAreasTbl adminAreaId1 = rar1.getAdminAreaId();
			final AdminAreasTbl adminAreaId2 = rar2.getAdminAreaId();
			return compareAdminAreaByName(adminAreaId1, adminAreaId2);
		});
		return roleAdminAreaRelTblList;
	}

	/**
	 * Sort user user app rel tbl.
	 *
	 * @param userUserAppRelTblsList the user user app rel tbls list
	 * @return the list
	 */
	public static List<UserUserAppRelTbl> sortUserUserAppRelTbl(List<UserUserAppRelTbl> userUserAppRelTblsList) {
		Collections.sort(userUserAppRelTblsList, (UserUserAppRelTbl uuapp1, UserUserAppRelTbl uuapp2) -> {
			final UserApplicationsTbl userAppId1 = uuapp1.getUserApplicationId();
			final UserApplicationsTbl userAppId2 = uuapp2.getUserApplicationId();
			return compareUserAppByName(userAppId1, userAppId2);
		});
		return userUserAppRelTblsList;
	}

	/**
	 * Sort notification response.
	 *
	 * @param notificationResponses the notification responses
	 * @return the list
	 */
	public static List<EmailNotificationConfigTbl> sortNotificationResponse(NotificationResponse notificationResponses) {
		List<EmailNotificationConfigTbl> config = null;
		if ((config = notificationResponses.getEmailNotificationConfigTbls()) != null) {
			Collections.sort(config, (EmailNotificationConfigTbl c1, EmailNotificationConfigTbl c2) -> {
				return compareEmailNotificationConfig(c1, c2);
			});
		}
		return config;
	}
	
	/**
	 * Compare email notification config.
	 *
	 * @param enc1 the enc 1
	 * @param enc2 the enc 2
	 * @return the int
	 */
	public static int compareEmailNotificationConfig(final EmailNotificationConfigTbl enc1, final EmailNotificationConfigTbl enc2) {
		if (enc1 != null && enc2 != null) {
			final String actionName1 = enc1.getName();
			final String actionName2 = enc2.getName();
			if (!XMSystemUtil.isEmpty(actionName1) && !XMSystemUtil.isEmpty(actionName2)) {
				return actionName1.compareTo(actionName2);
			}
		}
		return 0;
	}
}
