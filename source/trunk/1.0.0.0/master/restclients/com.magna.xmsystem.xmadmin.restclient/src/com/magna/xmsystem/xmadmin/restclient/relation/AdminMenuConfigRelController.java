package com.magna.xmsystem.xmadmin.restclient.relation;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import com.magna.xmbackend.entities.AdminMenuConfigTbl;
import com.magna.xmbackend.exception.CannotCreateObjectException;
import com.magna.xmbackend.vo.adminMenu.AdminMenuConfigCustomeResponseWrapper;
import com.magna.xmbackend.vo.adminMenu.AdminMenuConfigRequest;
import com.magna.xmbackend.vo.adminMenu.AdminMenuConfigResponse;
import com.magna.xmbackend.vo.adminMenu.AdminMenuConfigResponseWrapper;
import com.magna.xmbackend.vo.enums.AdminMenuConfig;
import com.magna.xmsystem.dependencies.customExceptions.UnauthorizedAccessException;
import com.magna.xmsystem.xmadmin.restclient.RestController;
import com.magna.xmsystem.xmadmin.restclient.util.RestClientExpHandlerUtil;
import com.magna.xmsystem.xmadmin.restclient.util.XMAdminRestClientConstants;

/**
 * The Class AdminMenuConfigRelController.
 */
public class AdminMenuConfigRelController extends RestController {

	/** The Constant LOGGER. */
	private static final Logger LOGGER = LoggerFactory.getLogger(AdminMenuConfigRelController.class);

	/**
	 * Instantiates a new admin menu config rel controller.
	 */
	public AdminMenuConfigRelController() {
		super();
	}

	/**
	 * Creates the admin menu relation.
	 *
	 * @param adminMenuConfigRequest
	 *            the admin menu config request
	 * @return the admin menu config tbl
	 */
	public AdminMenuConfigTbl createAdminMenuRelation(AdminMenuConfigRequest adminMenuConfigRequest)
			throws UnauthorizedAccessException, CannotCreateObjectException {
		try {
			String url = new String(this.serviceUrl + XMAdminRestClientConstants.CREATE_ADMIN_MENU_RELATION);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<AdminMenuConfigRequest> request = new HttpEntity<AdminMenuConfigRequest>(adminMenuConfigRequest,
					this.headers);
			ResponseEntity<AdminMenuConfigTbl> responseEntity = restTemplate.postForEntity(url, request,
					AdminMenuConfigTbl.class);
			HttpStatus statusCode = responseEntity.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				AdminMenuConfigTbl adminMenuConfigRelResponse = responseEntity.getBody();
				return adminMenuConfigRelResponse;
			} else {
				LOGGER.error(
						"Error while calling XMSYSTEM Create AdminMenu Relations REST Service, returns the status code: ", //$NON-NLS-1$
						statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException) e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			} else if (e instanceof HttpStatusCodeException) {
				String errorResponse = ((HttpStatusCodeException) e).getResponseBodyAsString();
				String errCode = RestClientExpHandlerUtil.handleCannotCreateObjectException(LOGGER,
						(HttpStatusCodeException) e);
				throw new CannotCreateObjectException(errorResponse, errCode);
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM Create AdminMenu Relations REST Service", e); //$NON-NLS-1$
		}
		return null;
	}

	/**
	 * Creates the admin menu multi relation.
	 *
	 * @param adminMenuConfigRequest
	 *            the admin menu config request
	 * @return the iterable
	 */
	public AdminMenuConfigResponse createAdminMenuMultiRelation(
			List<AdminMenuConfigRequest> adminMenuConfigRequest)
			throws UnauthorizedAccessException, CannotCreateObjectException {
		try {
			String url = new String(this.serviceUrl + XMAdminRestClientConstants.CREATE_ADMIN_MENU_MULTI_RELATION);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<List<AdminMenuConfigRequest>> request = new HttpEntity<List<AdminMenuConfigRequest>>(
					adminMenuConfigRequest, this.headers);
			ResponseEntity<AdminMenuConfigResponse> responseEntity = restTemplate.exchange(url, HttpMethod.POST,
					request, new ParameterizedTypeReference<AdminMenuConfigResponse>() {
					});
			HttpStatus statusCode = responseEntity.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				AdminMenuConfigResponse adminMenuConfigRelResponse = responseEntity.getBody();
				return adminMenuConfigRelResponse;
			} else {
				LOGGER.error(
						"Error while calling XMSYSTEM Create Multi AdminMenu Relations REST Service, returns the status code: ", //$NON-NLS-1$
						statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException) e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			} else if (e instanceof HttpStatusCodeException) {
				String errorResponse = ((HttpStatusCodeException) e).getResponseBodyAsString();
				String errCode = RestClientExpHandlerUtil.handleCannotCreateObjectException(LOGGER,
						(HttpStatusCodeException) e);
				throw new CannotCreateObjectException(errorResponse, errCode);
			} else if (e instanceof ResourceAccessException) {
				throw e;
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM Create Multi AdminMenu Relations REST Service", e); //$NON-NLS-1$
		}
		return null;
	}

	/**
	 * Gets the admin menu rel by obj type.
	 *
	 * @param objectType
	 *            the object type
	 * @return the admin menu rel by obj type
	 */
	public AdminMenuConfigCustomeResponseWrapper getAdminMenuRelByObjType(AdminMenuConfig objectType)
			throws UnauthorizedAccessException {
		try {
			String url = new String(
					this.serviceUrl + XMAdminRestClientConstants.FIND_ADMIN_MENU_BY_OBJTYPE + "/" + objectType);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<AdminMenuConfig> request = new HttpEntity<AdminMenuConfig>(objectType, this.headers);
			ResponseEntity<AdminMenuConfigCustomeResponseWrapper> responseEntity = restTemplate.exchange(url, HttpMethod.GET, request,
					AdminMenuConfigCustomeResponseWrapper.class);
			HttpStatus statusCode = responseEntity.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				final AdminMenuConfigCustomeResponseWrapper adminMenuConfigCustomeResponseWrapper = responseEntity.getBody();
				return adminMenuConfigCustomeResponseWrapper;
			} else {
				LOGGER.error("Error while calling XMSYSTEM find AdminMenu  REST Service, returns the status code: ", //$NON-NLS-1$
						statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException) e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			} else if (e instanceof HttpStatusCodeException) {
				RestClientExpHandlerUtil.handleNoXMLObjectFoundException(LOGGER, (HttpStatusCodeException) e);
			} else if(e instanceof ResourceAccessException){
				throw e;
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM Create AdminMenu  REST Service", e); //$NON-NLS-1$
		}
		return null;
	}

	/**
	 * Delete admin menu rel.
	 *
	 * @param id
	 *            the id
	 * @return the boolean
	 */
	public boolean deleteAdminMenuRel(String id) throws UnauthorizedAccessException {
		try {
			String url = new String(this.serviceUrl + XMAdminRestClientConstants.DELETE_ADMIN_MENU_BY_ID + "/" + id);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<Object> requestEntity = new HttpEntity<Object>(this.headers);
			ResponseEntity<Boolean> responseEntity = restTemplate.exchange(url, HttpMethod.DELETE, requestEntity,
					Boolean.class);
			HttpStatus statusCode = responseEntity.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				Boolean adminMenuConfigRelResponse = responseEntity.getBody();
				return adminMenuConfigRelResponse;
			} else {
				LOGGER.error(
						"Error while calling XMSYSTEM delete AdminMenu Relation REST Service, returns the status code: ", //$NON-NLS-1$
						statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException) e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM delete AdminMenu Relation REST Service", e); //$NON-NLS-1$
		}
		return false;
	}

	/**
	 * Delete multi admin menu rel.
	 *
	 * @param ids the ids
	 * @return the admin menu config response
	 * @throws UnauthorizedAccessException the unauthorized access exception
	 */
	public AdminMenuConfigResponse deleteMultiAdminMenuRel(Set<String> ids) throws UnauthorizedAccessException {
		try {
			String url = new String(this.serviceUrl + XMAdminRestClientConstants.DELETE_MULTI_ADMIN_MENU_REL_BY_ID);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<Set<String>> requestEntity = new HttpEntity<Set<String>>(ids,this.headers);
			ResponseEntity<AdminMenuConfigResponse> responseEntity = restTemplate.exchange(url, HttpMethod.DELETE, requestEntity,
					AdminMenuConfigResponse.class);
			HttpStatus statusCode = responseEntity.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				AdminMenuConfigResponse adminMenuConfigResponse = responseEntity.getBody();
				return adminMenuConfigResponse;
			} else {
				LOGGER.error("Error while calling XMSYSTEM delete Multi AdminMenuRel REST Service, returns the status code: ", //$NON-NLS-1$
						statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException)e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM delete Multi AdminMenuRel REST Service!", e); //$NON-NLS-1$
		}
		return null;
	}
	/**
	 * Gets the all admin menu config rel.
	 *
	 * @return the all admin menu config rel
	 * @throws UnauthorizedAccessException the unauthorized access exception
	 */
	public AdminMenuConfigResponse getAllAdminMenuConfigRel() throws UnauthorizedAccessException {
		try {
			String url = new String(this.serviceUrl + XMAdminRestClientConstants.GET_ALL_ADMIN_MENU_CONFIG_REL);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<Object> requestEntity = new HttpEntity<Object>(headers);

			List<HttpMessageConverter<?>> messageConverters = new ArrayList<>();
			messageConverters.add(new MappingJackson2HttpMessageConverter());
			restTemplate.setMessageConverters(messageConverters);

			ResponseEntity<AdminMenuConfigResponse> responseList = restTemplate.exchange(url, HttpMethod.GET,
					requestEntity, AdminMenuConfigResponse.class);
			HttpStatus statusCode = responseList.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				AdminMenuConfigResponse adminMenuConfigResponse = responseList.getBody();
				return adminMenuConfigResponse;
			} else {
				LOGGER.error(
						"Error while calling XMSYSTEM Get All AdminMenuConfig Relation REST Service, returns the status code: " //$NON-NLS-1$
								+ statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException) e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			} else if (e instanceof HttpStatusCodeException) {
				RestClientExpHandlerUtil.handleNoXMLObjectFoundException(LOGGER, (HttpStatusCodeException) e);
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM Get All AdminMenuConfig Relation REST Service", e); //$NON-NLS-1$
		}
		return null;
	}

	/**
	 * Gets the all admin menu config rel applications.
	 *
	 * @return the all admin menu config rel applications
	 * @throws UnauthorizedAccessException the unauthorized access exception
	 */
	public AdminMenuConfigResponseWrapper getAllAdminMenuConfigRelApplications() throws UnauthorizedAccessException {
		try {
			String url = new String(this.serviceUrl + XMAdminRestClientConstants.ADMIN_MENU_CONFIG_APPS);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<Object> requestEntity = new HttpEntity<Object>(headers);

			List<HttpMessageConverter<?>> messageConverters = new ArrayList<>();
			messageConverters.add(new MappingJackson2HttpMessageConverter());
			restTemplate.setMessageConverters(messageConverters);

			ResponseEntity<AdminMenuConfigResponseWrapper> responseList = restTemplate.exchange(url, HttpMethod.GET,
					requestEntity, AdminMenuConfigResponseWrapper.class);
			HttpStatus statusCode = responseList.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				AdminMenuConfigResponseWrapper adminMenuConfigResponseWrapper = responseList.getBody();
				return adminMenuConfigResponseWrapper;
			} else {
				LOGGER.error(
						"Error while calling XMSYSTEM Get All AdminMenuConfig Relation REST Service, returns the status code: " //$NON-NLS-1$
								+ statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException) e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			} else if (e instanceof HttpStatusCodeException) {
				RestClientExpHandlerUtil.handleNoXMLObjectFoundException(LOGGER, (HttpStatusCodeException) e);
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM Get All AdminMenuConfig Relation REST Service", e); //$NON-NLS-1$
		}
		return null;
	}
}
