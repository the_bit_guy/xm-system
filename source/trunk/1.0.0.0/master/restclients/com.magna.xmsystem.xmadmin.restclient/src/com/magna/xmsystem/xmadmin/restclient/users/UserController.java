package com.magna.xmsystem.xmadmin.restclient.users;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import com.magna.xmbackend.entities.UsersTbl;
import com.magna.xmbackend.exception.CannotCreateObjectException;
import com.magna.xmbackend.vo.user.UserRequest;
import com.magna.xmbackend.vo.user.UserResponse;
import com.magna.xmsystem.dependencies.customExceptions.UnauthorizedAccessException;
import com.magna.xmsystem.xmadmin.restclient.RestController;
import com.magna.xmsystem.xmadmin.restclient.util.RestClientExpHandlerUtil;
import com.magna.xmsystem.xmadmin.restclient.util.RestClientSortingUtil;
import com.magna.xmsystem.xmadmin.restclient.util.XMAdminRestClientConstants;

// TODO: Auto-generated Javadoc
/**
 * Class for User controller.
 *
 * @author Roshan.Ekka
 */
public class UserController extends RestController{

	/** The Constant LOGGER. */
	private static final Logger LOGGER = LoggerFactory.getLogger(UserController.class);

	/**
	 * Constructor for UserController Class.
	 */
	public UserController() {
		super();
	}

	
	/**
	 * Gets the all users.
	 *
	 * @param isSorted {@link boolean}
	 * @return the all users
	 * @throws UnauthorizedAccessException the unauthorized access exception
	 */
	public List<UsersTbl> getAllUsers(final boolean isSorted) throws UnauthorizedAccessException {
		try {
			String url = new String(this.serviceUrl + XMAdminRestClientConstants.GET_USERS);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<Object> requestEntity = new HttpEntity<Object>(headers);

			List<HttpMessageConverter<?>> messageConverters = new ArrayList<>();
			messageConverters.add(new MappingJackson2HttpMessageConverter());
			restTemplate.setMessageConverters(messageConverters);
			ResponseEntity<UserResponse> responseList = restTemplate.exchange(url, HttpMethod.GET, requestEntity,
					UserResponse.class);
			HttpStatus statusCode = responseList.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				UserResponse userResponseObject = responseList.getBody();
				List<UsersTbl> usersTblList = (List<UsersTbl>) userResponseObject.getUserTbls();
				if (isSorted) {
					RestClientSortingUtil.sortUserList(usersTblList);
				}
				return usersTblList;
			} else {
				LOGGER.error("Error while calling XMSYSTEM Get All Users REST Service, returns the status code: " //$NON-NLS-1$
						+ statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException)e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			} else if (e instanceof HttpStatusCodeException) {
				RestClientExpHandlerUtil.handleNoXMLObjectFoundException(LOGGER, (HttpStatusCodeException) e);
			} else if(e instanceof ResourceAccessException){
				throw e;
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM Get All Users REST Service", e); //$NON-NLS-1$
		}
		return null;
	}


	/**
	 * Gets the users.
	 *
	 * @param firstChar {@link String}
	 * @return the users
	 * @throws UnauthorizedAccessException the unauthorized access exception
	 */
	public List<UsersTbl> getUsers(final String firstChar) throws UnauthorizedAccessException {
		try {
			String url = new String(this.serviceUrl + XMAdminRestClientConstants.GET_USERS_USING_FIRST_CHAR + "/" + firstChar);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<Object> requestEntity = new HttpEntity<Object>(headers);

			List<HttpMessageConverter<?>> messageConverters = new ArrayList<>();
			messageConverters.add(new MappingJackson2HttpMessageConverter());
			restTemplate.setMessageConverters(messageConverters);

			ResponseEntity<UserResponse> responseList = restTemplate.exchange(url, HttpMethod.GET, requestEntity,
					UserResponse.class);
			HttpStatus statusCode = responseList.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				UserResponse userResponseObject = responseList.getBody();
				List<UsersTbl> usersTblList = (List<UsersTbl>) userResponseObject.getUserTbls();
				RestClientSortingUtil.sortUserList(usersTblList);
				return usersTblList;
			} else {
				LOGGER.error("Error while calling XMSYSTEM Get All Users With First Char REST Service, returns the status code: " //$NON-NLS-1$
						+ statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException)e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			} else if (e instanceof HttpStatusCodeException) {
				RestClientExpHandlerUtil.handleNoXMLObjectFoundException(LOGGER, (HttpStatusCodeException) e);
			} else if(e instanceof ResourceAccessException){
				throw e;
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM Get All Users With First Char REST Service", e); //$NON-NLS-1$
		}
		return null;
	}
	
	/**
	 * Gets the all users by project id.
	 *
	 * @param projectId {@link String}
	 * @return the all users by project id
	 * @throws UnauthorizedAccessException the unauthorized access exception
	 */
	public List<UsersTbl> getAllUsersByProjectId(String projectId) throws UnauthorizedAccessException {
		try {
			String url = new String(this.serviceUrl + XMAdminRestClientConstants.GET_USERS_BY_PROJECT_ID + "/" + projectId);
			List<UsersTbl> userTblList = new ArrayList<>();
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<Object> requestEntity = new HttpEntity<Object>(headers);

			List<HttpMessageConverter<?>> messageConverters = new ArrayList<>();
			messageConverters.add(new MappingJackson2HttpMessageConverter());
			restTemplate.setMessageConverters(messageConverters);

			ResponseEntity<UserResponse> responseList = restTemplate.exchange(url, HttpMethod.GET, requestEntity,
					UserResponse.class);
			
			HttpStatus statusCode = responseList.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				UserResponse userResponseObject = responseList.getBody();
				Iterable<UsersTbl> usersTbls = userResponseObject.getUserTbls();
				for (UsersTbl usersTbl : usersTbls) {
					userTblList.add(usersTbl);
				}
				RestClientSortingUtil.sortUserList(userTblList);
				return userTblList;
			} else {
				LOGGER.error("Error while calling XMSYSTEM Get All Users By ProjectId REST Service, returns the status code: " //$NON-NLS-1$
						+ statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException)e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				} else if (e instanceof HttpStatusCodeException) {
					RestClientExpHandlerUtil.handleNoXMLObjectFoundException(LOGGER, (HttpStatusCodeException) e);
				}
			} 
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM Get All Users By ProjectId REST Service", e); //$NON-NLS-1$
		}
		return null;
	}
	
	/**
	 * Gets the all users by user app id.
	 *
	 * @param userAppId {@link String}
	 * @return the all users by user app id
	 * @throws UnauthorizedAccessException the unauthorized access exception
	 */
	public UserResponse getAllUsersByUserAppId(String userAppId) throws UnauthorizedAccessException {
		try {
			String url = new String(this.serviceUrl + XMAdminRestClientConstants.GET_USERS_BY_USER_APP_ID + "/" + userAppId);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<Object> requestEntity = new HttpEntity<Object>(headers);

			List<HttpMessageConverter<?>> messageConverters = new ArrayList<>();
			messageConverters.add(new MappingJackson2HttpMessageConverter());
			restTemplate.setMessageConverters(messageConverters);

			ResponseEntity<UserResponse> responseList = restTemplate.exchange(url, HttpMethod.GET, requestEntity,
					UserResponse.class);
			HttpStatus statusCode = responseList.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				UserResponse userResponseObject = responseList.getBody();
				return userResponseObject;
			} else {
				LOGGER.error("Error while calling XMSYSTEM Get All Users By UserAppId REST Service, returns the status code: " //$NON-NLS-1$
						+ statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException)e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			} else if (e instanceof HttpStatusCodeException) {
				RestClientExpHandlerUtil.handleNoXMLObjectFoundException(LOGGER, (HttpStatusCodeException) e);
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM Get All Users By UserAppId REST Service", e); //$NON-NLS-1$
		}
		return null;
	}
	
	/**
	 * Gets the all users by project app id.
	 *
	 * @param projectAppId {@link String}
	 * @return the all users by project app id
	 * @throws UnauthorizedAccessException the unauthorized access exception
	 */
	public List<UsersTbl> getAllUsersByProjectAppId(String projectAppId) throws UnauthorizedAccessException {
		try {
			String url = new String(this.serviceUrl + XMAdminRestClientConstants.GET_USERS_BY_PROJECT_APP_ID + "/" + projectAppId);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<Object> requestEntity = new HttpEntity<Object>(headers);
			List<UsersTbl> userTblList = new ArrayList<>();
			List<HttpMessageConverter<?>> messageConverters = new ArrayList<>();
			messageConverters.add(new MappingJackson2HttpMessageConverter());
			restTemplate.setMessageConverters(messageConverters);

			ResponseEntity<UserResponse> responseList = restTemplate.exchange(url, HttpMethod.GET, requestEntity,
					UserResponse.class);
			HttpStatus statusCode = responseList.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				UserResponse userResponseObject = responseList.getBody();
				Iterable<UsersTbl> usersTbls = userResponseObject.getUserTbls();
				for(UsersTbl usersTbl:usersTbls)
				{
					userTblList.add(usersTbl);
				}
				RestClientSortingUtil.sortUserList(userTblList);
				return userTblList;
			} else {
				LOGGER.error("Error while calling XMSYSTEM Get All Users By ProjectAppId REST Service, returns the status code: " //$NON-NLS-1$
						+ statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException)e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			} else if (e instanceof HttpStatusCodeException) {
				RestClientExpHandlerUtil.handleNoXMLObjectFoundException(LOGGER, (HttpStatusCodeException) e);
			} else if(e instanceof ResourceAccessException){
				throw e;
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM Get All Users By ProjectAppId REST Service", e); //$NON-NLS-1$
		}
		return null;
	}
	
	/**
	 * Gets the all users by start app id.
	 *
	 * @param startAppId {@link String}
	 * @return the all users by start app id
	 * @throws UnauthorizedAccessException the unauthorized access exception
	 */
	public UserResponse getAllUsersByStartAppId(String startAppId) throws UnauthorizedAccessException {
		try {
			String url = new String(this.serviceUrl + XMAdminRestClientConstants.GET_USERS_BY_START_APP_ID + "/" + startAppId);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<Object> requestEntity = new HttpEntity<Object>(headers);

			List<HttpMessageConverter<?>> messageConverters = new ArrayList<>();
			messageConverters.add(new MappingJackson2HttpMessageConverter());
			restTemplate.setMessageConverters(messageConverters);

			ResponseEntity<UserResponse> responseList = restTemplate.exchange(url, HttpMethod.GET, requestEntity,
					UserResponse.class);
			HttpStatus statusCode = responseList.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				UserResponse userResponseObject = responseList.getBody();
				return userResponseObject;
			} else {
				LOGGER.error("Error while calling XMSYSTEM Get All Users By ProjectAppId REST Service, returns the status code: " //$NON-NLS-1$
						+ statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException)e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			} else if (e instanceof HttpStatusCodeException) {
				RestClientExpHandlerUtil.handleNoXMLObjectFoundException(LOGGER, (HttpStatusCodeException) e);
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM Get All Users By ProjectAppId REST Service", e); //$NON-NLS-1$
		}
		return null;
	}
	
	/**
	 * Gets the user by name.
	 *
	 * @param uName the u name
	 * @return the user by name
	 * @throws UnauthorizedAccessException the unauthorized access exception
	 */
	public UsersTbl getUserByName(final String uName) throws UnauthorizedAccessException {
		try {
			UserRequest userRequest = new UserRequest();
			userRequest.setUserName(uName);
			String url = new String(this.serviceUrl + XMAdminRestClientConstants.GET_USER_BY_NAME);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<UserRequest> requestEntity = new HttpEntity<UserRequest>(userRequest, headers);
			List<HttpMessageConverter<?>> messageConverters = new ArrayList<>();
			messageConverters.add(new MappingJackson2HttpMessageConverter());
			restTemplate.setMessageConverters(messageConverters);

			ResponseEntity<UsersTbl> responseList = restTemplate.exchange(url, HttpMethod.POST, requestEntity,
					UsersTbl.class);
			HttpStatus statusCode = responseList.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				UsersTbl usersTbl = responseList.getBody();
				return usersTbl;
			} else {
				LOGGER.error("Error while calling XMSYSTEM Get All Users By ProjectAppId REST Service, returns the status code: " //$NON-NLS-1$
						+ statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException)e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			} else if (e instanceof HttpStatusCodeException) {
				RestClientExpHandlerUtil.handleNoXMLObjectFoundException(LOGGER, (HttpStatusCodeException) e);
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM Get All Users By ProjectAppId REST Service", e); //$NON-NLS-1$
		}
		return null;
	}
	
	/**
	 * Method for Save user.
	 *
	 * @param UserRequest {@link List<UserRequest>}
	 * @return the iterable {@link Iterable<UsersTbl>}
	 * @throws CannotCreateObjectException the cannot create object exception
	 * @throws UnauthorizedAccessException the unauthorized access exception
	 */
	public UserResponse saveUser(List<UserRequest> UserRequest) throws CannotCreateObjectException, UnauthorizedAccessException {
		try {
			String url = new String(this.serviceUrl + XMAdminRestClientConstants.SAVE_USER);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<List<UserRequest>> request = new HttpEntity<List<UserRequest>>(UserRequest, this.headers);
			ResponseEntity<UserResponse> responseEntity = restTemplate.exchange(url, HttpMethod.POST,request, new ParameterizedTypeReference<UserResponse>() {});
			HttpStatus statusCode = responseEntity.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				UserResponse usersTbl = responseEntity.getBody();
				return usersTbl;
			} else {
				LOGGER.error("Error while calling XMSYSTEM Save User REST Service, returns the status code: ", //$NON-NLS-1$
						statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException)e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}else if (e instanceof HttpStatusCodeException) {
					String errorResponse = ((HttpStatusCodeException) e).getResponseBodyAsString();
					String errCode = RestClientExpHandlerUtil.handleCannotCreateObjectException(LOGGER, (HttpStatusCodeException) e);
					throw new CannotCreateObjectException(errorResponse, errCode);
				}
			} 
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM Save User REST Service", e); //$NON-NLS-1$
		}
		return null;
	}

	
	/**
	 * Method for Delete user.
	 *
	 * @param userId {@link String}
	 * @return true, if successful
	 */
	public boolean deleteUser(String userId) {
		try {
			String url = new String(this.serviceUrl + XMAdminRestClientConstants.DELETE_USER + "/" + userId);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<Object> requestEntity = new HttpEntity<Object>(this.headers);
			ResponseEntity<Boolean> responseEntity = restTemplate.exchange(url, HttpMethod.POST, requestEntity,
					Boolean.class);
			HttpStatus statusCode = responseEntity.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				return responseEntity.getBody();
			} else {
				LOGGER.error("Error while calling XMSYSTEM delete User REST Service, returns the status code: ", //$NON-NLS-1$
						statusCode.value());
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM delete User REST Service!", e); //$NON-NLS-1$
		}
		return false;
	}
	
	/**
	 * Delete multi user.
	 *
	 * @param userIds the user ids
	 * @return the user response
	 * @throws UnauthorizedAccessException the unauthorized access exception
	 */
	public UserResponse deleteMultiUser(Set<String> userIds) throws UnauthorizedAccessException {
		try {
			String url = new String(this.serviceUrl + XMAdminRestClientConstants.DELETE_MULTI_USER);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<Set<String>> requestEntity = new HttpEntity<Set<String>>(userIds,this.headers);
			ResponseEntity<UserResponse> responseEntity = restTemplate.exchange(url, HttpMethod.POST, requestEntity,
					UserResponse.class);
			HttpStatus statusCode = responseEntity.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				UserResponse userResponse = responseEntity.getBody();
				return userResponse;
			} else {
				LOGGER.error("Error while calling XMSYSTEM delete Multi User REST Service, returns the status code: ", //$NON-NLS-1$
						statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException)e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			} else if (e instanceof ResourceAccessException) {
				throw e;
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM delete Multi User REST Service!", e); //$NON-NLS-1$
		}
		return null;
	}

	/**
	 * Method for Update user.
	 *
	 * @param UserRequest {@link UserRequest}
	 * @return true, if successful
	 * @throws CannotCreateObjectException the cannot create object exception
	 * @throws UnauthorizedAccessException the unauthorized access exception
	 */
	public boolean updateUser(UserRequest UserRequest) throws CannotCreateObjectException, UnauthorizedAccessException {
		try {
			String url = new String(this.serviceUrl + XMAdminRestClientConstants.UPDATE_USER);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<UserRequest> request = new HttpEntity<UserRequest>(UserRequest, this.headers);
			ResponseEntity<UsersTbl> responseEntity = restTemplate.postForEntity(url, request, UsersTbl.class);
			HttpStatus statusCode = responseEntity.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				return true;
			} else {
				LOGGER.error("Error while calling XMSYSTEM Upadte User REST Service, returns the status code: ", //$NON-NLS-1$
						statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException)e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			} else if (e instanceof HttpStatusCodeException) {
				String errorResponse = ((HttpStatusCodeException) e).getResponseBodyAsString();
				String errCode = RestClientExpHandlerUtil.handleCannotCreateObjectException(LOGGER, (HttpStatusCodeException) e);
				throw new CannotCreateObjectException(errorResponse, errCode);
			}else if (e instanceof ResourceAccessException) {
				throw e;
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM Upadte User REST Service", e); //$NON-NLS-1$
		}
		return false;
	}
	
	/**
	 * Method for Update user status.
	 *
	 * @param userId {@link String}
	 * @param status {@link String}
	 * @return true, if successful
	 */
	public boolean updateUserStatus(String userId, String status) {
		try {
			String url = new String(
					this.serviceUrl + XMAdminRestClientConstants.UPDATE_USER_STATUS + "/" + status + "/" + userId);

			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<Object> requestEntity = new HttpEntity<Object>(this.headers);
			ResponseEntity<Boolean> responseEntity = restTemplate.exchange(url, HttpMethod.POST, requestEntity,
					Boolean.class);
			HttpStatus statusCode = responseEntity.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				return responseEntity.getBody();
			} else {
				LOGGER.error("Error while calling XMSYSTEM Update User Status REST Service, returns the status code: ", //$NON-NLS-1$
						statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException) e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM Update User Status REST Service!", e); //$NON-NLS-1$
		}
		return false;
	}
	
	/**
	 * Multi update user status.
	 *
	 * @param userRequest the user request
	 * @return the user response
	 * @throws UnauthorizedAccessException the unauthorized access exception
	 */
	public UserResponse multiUpdateUserStatus(List<UserRequest> userRequest) throws UnauthorizedAccessException {
		try {
			String url = new String(
					this.serviceUrl + XMAdminRestClientConstants.MULTI_UPDATE_USER_STATUS );

			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<List<UserRequest>> requestEntity = new HttpEntity<List<UserRequest>>(userRequest, this.headers);
			ResponseEntity<UserResponse> responseEntity = restTemplate.exchange(url, HttpMethod.POST, requestEntity,
					UserResponse.class);
			HttpStatus statusCode = responseEntity.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				return responseEntity.getBody();
			} else {
				LOGGER.error("Error while calling XMSYSTEM Multi Update user Status REST Service, returns the status code: ", //$NON-NLS-1$
						statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException)e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			} else if (e instanceof ResourceAccessException) {
				throw e;
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM Multi Update user Status REST Service!", e); //$NON-NLS-1$
		}
		return null;
	}
	
	/**
	 * Gets the user by id.
	 *
	 * @param userId {@link String}
	 * @return the user by id
	 */
	public UsersTbl getUserById(final String userId) {
		try {
			String url = new String(this.serviceUrl + XMAdminRestClientConstants.GET_USER_BY_ID + "/" + userId);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<Object> requestEntity = new HttpEntity<Object>(headers);

			List<HttpMessageConverter<?>> messageConverters = new ArrayList<>();
			messageConverters.add(new MappingJackson2HttpMessageConverter());
			restTemplate.setMessageConverters(messageConverters);

			ResponseEntity<UsersTbl> responseList = restTemplate.exchange(url, HttpMethod.GET, requestEntity,
					UsersTbl.class);
			HttpStatus statusCode = responseList.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				UsersTbl usersTbl = responseList.getBody();
				return usersTbl;
			} else {
				LOGGER.error("Error while calling XMSYSTEM Get All Users By userId REST Service, returns the status code: " //$NON-NLS-1$
						+ statusCode.value());
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM Get All Users By userId REST Service", e); //$NON-NLS-1$
		}
		return null;
	}
	
	/**
	 * Gets the users by admin area.
	 *
	 * @param adminAreaId the admin area id
	 * @return the users by admin area
	 * @throws UnauthorizedAccessException the unauthorized access exception
	 */
	public List<UsersTbl> getUsersByAdminArea(final String adminAreaId) throws UnauthorizedAccessException {
		try {
			String url = new String(this.serviceUrl + XMAdminRestClientConstants.GET_USERS_BY_ADMINAREAID + "/" + adminAreaId);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<Object> requestEntity = new HttpEntity<Object>(headers);

			List<HttpMessageConverter<?>> messageConverters = new ArrayList<>();
			messageConverters.add(new MappingJackson2HttpMessageConverter());
			restTemplate.setMessageConverters(messageConverters);
			ResponseEntity<UserResponse> responseList = restTemplate.exchange(url, HttpMethod.GET, requestEntity,
					UserResponse.class);
			HttpStatus statusCode = responseList.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				UserResponse userResponseObject = responseList.getBody();
				List<UsersTbl> usersTblList = (List<UsersTbl>) userResponseObject.getUserTbls();
				return usersTblList;
			} else {
				LOGGER.error("Error while calling XMSYSTEM Get users by admin area REST Service, returns the status code: " //$NON-NLS-1$
						+ statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException)e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			} else if (e instanceof HttpStatusCodeException) {
				RestClientExpHandlerUtil.handleNoXMLObjectFoundException(LOGGER, (HttpStatusCodeException) e);
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM Get by admin area REST Service", e); //$NON-NLS-1$
		}
		return null;
	}
	
	/**
	 * Gets the users by site id.
	 *
	 * @param siteId the site id
	 * @return the users by site id
	 * @throws UnauthorizedAccessException the unauthorized access exception
	 */
	public UserResponse getUsersBySiteId(final String siteId) throws UnauthorizedAccessException {
		try {
			String url = new String(this.serviceUrl + XMAdminRestClientConstants.GET_USERS_BY_SITEID + "/" + siteId);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<Object> requestEntity = new HttpEntity<Object>(headers);

			List<HttpMessageConverter<?>> messageConverters = new ArrayList<>();
			messageConverters.add(new MappingJackson2HttpMessageConverter());
			restTemplate.setMessageConverters(messageConverters);

			ResponseEntity<UserResponse> responseList = restTemplate.exchange(url, HttpMethod.GET, requestEntity,
					UserResponse.class);
			HttpStatus statusCode = responseList.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				UserResponse userResponse = responseList.getBody();
				return userResponse;
			} else {
				LOGGER.error("Error while calling XMSYSTEM Get All Users By siteId REST Service, returns the status code: " //$NON-NLS-1$
						+ statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException)e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			} else if (e instanceof HttpStatusCodeException) {
				RestClientExpHandlerUtil.handleNoXMLObjectFoundException(LOGGER, (HttpStatusCodeException) e);
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM Get All Users By siteId REST Service", e); //$NON-NLS-1$
		}
		return null;
	}
	
	
	/**
	 * Checks if is super admin.
	 *
	 * @return true, if is super admin
	 */
	public boolean isSuperAdmin() {
		try {
			String url = new String(this.serviceUrl + XMAdminRestClientConstants.IS_SUPER_ADMIN);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<Object> request = new HttpEntity<Object>(this.headers);
			ResponseEntity<Boolean> responseEntity = restTemplate.exchange(url, HttpMethod.GET, request, Boolean.class);
			HttpStatus statusCode = responseEntity.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				return responseEntity.getBody();
			} else {
				LOGGER.error("Error while calling XMSYSTEM isAdminUser REST Service, returns the status code: ", //$NON-NLS-1$
						statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException) e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			} else if (e instanceof HttpStatusCodeException) {
				RestClientExpHandlerUtil.handleNoXMLObjectFoundException(LOGGER, (HttpStatusCodeException) e);
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM isAdminUser REST Service", e); //$NON-NLS-1$
		}
		return false;
	}
}