package com.magna.xmsystem.xmadmin.restclient.relation;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import com.magna.xmbackend.entities.ProjectApplicationsTbl;
import com.magna.xmbackend.entities.UserProjAppRelTbl;
import com.magna.xmbackend.exception.CannotCreateObjectException;
import com.magna.xmbackend.vo.rel.UserProjectAppRelBatchRequest;
import com.magna.xmbackend.vo.rel.UserProjectAppRelBatchResponse;
import com.magna.xmbackend.vo.rel.UserProjectAppRelRequest;
import com.magna.xmbackend.vo.rel.UserProjectAppRelResponse;
import com.magna.xmsystem.dependencies.customExceptions.UnauthorizedAccessException;
import com.magna.xmsystem.dependencies.utils.XMSystemUtil;
import com.magna.xmsystem.xmadmin.restclient.RestController;
import com.magna.xmsystem.xmadmin.restclient.site.SiteController;
import com.magna.xmsystem.xmadmin.restclient.util.RestClientExpHandlerUtil;
import com.magna.xmsystem.xmadmin.restclient.util.XMAdminRestClientConstants;

/**
 * Class for Site admin area rel controller.
 *
 * @author Roshan.Ekka
 */
public class UserProjectAppRelController extends RestController {
	/** The Constant LOGGER. */
	private static final Logger LOGGER = LoggerFactory.getLogger(SiteController.class);

	/**
	 * Constructor for SiteController Class.
	 */
	public UserProjectAppRelController() {
		super();
	}
	
	/**
	 * Instantiates a new user project app rel controller.
	 *
	 * @param adminAreaId the admin area id
	 */
	public UserProjectAppRelController(final String adminAreaId) {
		super(adminAreaId);
	}

	/**
	 * Method for Creates the user project app rel.
	 *
	 * @param userProjAppRelRequest
	 *            {@link UserProjectAppRelRequest}
	 * @return the user proj app rel tbl {@link UserProjAppRelTbl}
	 */
	public UserProjAppRelTbl createUserProjectAppRel(UserProjectAppRelRequest userProjAppRelRequest)
			throws UnauthorizedAccessException, CannotCreateObjectException {
		try {
			String url = new String(this.serviceUrl + XMAdminRestClientConstants.CREATE_PROJECT_APP_USER_REL);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<UserProjectAppRelRequest> request = new HttpEntity<UserProjectAppRelRequest>(
					userProjAppRelRequest, this.headers);
			ResponseEntity<UserProjAppRelTbl> responseEntity = restTemplate.postForEntity(url, request,
					UserProjAppRelTbl.class);
			HttpStatus statusCode = responseEntity.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				UserProjAppRelTbl userProjAppRelTbl = responseEntity.getBody();
				return userProjAppRelTbl;
			} else {
				LOGGER.error(
						"Error while calling XMSYSTEM Create User Project Application Relation REST Service, returns the status code: ", //$NON-NLS-1$
						statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException) e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			} else if (e instanceof HttpStatusCodeException) {
				String errorResponse = ((HttpStatusCodeException) e).getResponseBodyAsString();
				String errCode = RestClientExpHandlerUtil.handleCannotCreateObjectException(LOGGER,
						(HttpStatusCodeException) e);
				throw new CannotCreateObjectException(errorResponse, errCode);
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM Create User Project Application Relation REST Service", e); //$NON-NLS-1$
		}
		return null;
	}

	/**
	 * Method for Creates the user project app rel.
	 *
	 * @param userProjectAppRelBatchRequest
	 *            {@link UserProjectAppRelBatchRequest}
	 * @return the user project app rel batch response
	 *         {@link UserProjectAppRelBatchResponse}
	 */
	public UserProjectAppRelBatchResponse createUserProjectAppRel(
			UserProjectAppRelBatchRequest userProjectAppRelBatchRequest)
			throws UnauthorizedAccessException, CannotCreateObjectException {
		try {
			String url = new String(this.serviceUrl + XMAdminRestClientConstants.CREATE_MULTI_PROJECT_APP_USER_REL);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<UserProjectAppRelBatchRequest> request = new HttpEntity<UserProjectAppRelBatchRequest>(
					userProjectAppRelBatchRequest, this.headers);
			ResponseEntity<UserProjectAppRelBatchResponse> responseEntity = restTemplate.postForEntity(url, request,
					UserProjectAppRelBatchResponse.class);
			HttpStatus statusCode = responseEntity.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				UserProjectAppRelBatchResponse userProjectAppRelBatchResponse = responseEntity.getBody();
				return userProjectAppRelBatchResponse;
			} else {
				LOGGER.error(
						"Error while calling XMSYSTEM Create Multi User Project Application Relation REST Service, returns the status code: ", //$NON-NLS-1$
						statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException) e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			} else if (e instanceof HttpStatusCodeException) {
				String errorResponse = ((HttpStatusCodeException) e).getResponseBodyAsString();
				String errCode = RestClientExpHandlerUtil.handleCannotCreateObjectException(LOGGER,
						(HttpStatusCodeException) e);
				throw new CannotCreateObjectException(errorResponse, errCode);
			} else if (e instanceof ResourceAccessException) {
				throw e;
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM Create Multi User Project Application Relation REST Service", e); //$NON-NLS-1$
		}
		return null;
	}

	/**
	 * Gets the all user proj app rel.
	 *
	 * @param isSorted
	 *            {@link boolean}
	 * @return the all user proj app rel
	 */
	public List<UserProjAppRelTbl> getAllUserProjAppRel(final boolean isSorted) throws UnauthorizedAccessException {
		try {
			String url = new String(this.serviceUrl + XMAdminRestClientConstants.GET_USER_PROJECT_APP_REL);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<Object> requestEntity = new HttpEntity<Object>(headers);

			List<HttpMessageConverter<?>> messageConverters = new ArrayList<>();
			messageConverters.add(new MappingJackson2HttpMessageConverter());
			restTemplate.setMessageConverters(messageConverters);

			ResponseEntity<UserProjectAppRelResponse> responseList = restTemplate.exchange(url, HttpMethod.GET,
					requestEntity, UserProjectAppRelResponse.class);
			HttpStatus statusCode = responseList.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				UserProjectAppRelResponse userProjAppResponseObject = responseList.getBody();
				List<UserProjAppRelTbl> userProjAppRelTbl = (List<UserProjAppRelTbl>) userProjAppResponseObject
						.getUserProjAppRelTbls();
				return userProjAppRelTbl;
			} else {
				LOGGER.error(
						"Error while calling XMSYSTEM Get All User Project Application Relation REST Service, returns the status code: " //$NON-NLS-1$
								+ statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException) e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			} else if (e instanceof HttpStatusCodeException) {
				RestClientExpHandlerUtil.handleNoXMLObjectFoundException(LOGGER, (HttpStatusCodeException) e);
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM Get All User Project Application Relation REST Service", e); //$NON-NLS-1$
		}
		return null;
	}

	/**
	 * Method for Delete user proj app rel.
	 *
	 * @param id
	 *            {@link String}
	 * @return true, if successful
	 */
	public boolean deleteUserProjAppRel(String id) throws UnauthorizedAccessException {
		try {
			String url = new String(
					this.serviceUrl + XMAdminRestClientConstants.DELETE_USER_PROJECT_APP_REL + "/" + id);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<Object> requestEntity = new HttpEntity<Object>(this.headers);
			ResponseEntity<Boolean> responseEntity = restTemplate.exchange(url, HttpMethod.DELETE, requestEntity,
					Boolean.class);
			HttpStatus statusCode = responseEntity.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				return responseEntity.getBody();
			} else {
				LOGGER.error(
						"Error while calling XMSYSTEM delete User Project Application Relation REST Service, returns the status code: ", //$NON-NLS-1$
						statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException) e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM delete User Project Application Relation REST Service!", e); //$NON-NLS-1$
		}
		return false;
	}

	/**
	 * Delete multi user proj app rel.
	 *
	 * @param ids the ids
	 * @return the user project app rel response
	 * @throws UnauthorizedAccessException the unauthorized access exception
	 */
	public UserProjectAppRelResponse deleteMultiUserProjAppRel(Set<String> ids) throws UnauthorizedAccessException {
		try {
			String url = new String(this.serviceUrl + XMAdminRestClientConstants.DELETE_MULTI_USER_PROJECT_APP_REL);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<Set<String>> requestEntity = new HttpEntity<Set<String>>(ids, this.headers);
			ResponseEntity<UserProjectAppRelResponse> responseEntity = restTemplate.exchange(url, HttpMethod.DELETE,
					requestEntity, UserProjectAppRelResponse.class);
			HttpStatus statusCode = responseEntity.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				UserProjectAppRelResponse userProjectAppRelResponse = responseEntity.getBody();
				return userProjectAppRelResponse;
			} else {
				LOGGER.error(
						"Error while calling XMSYSTEM delete Multi UserProjAppRel REST Service, returns the status code: ", //$NON-NLS-1$
						statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException) e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM delete Multi UserProjAppRel REST Service!", e); //$NON-NLS-1$
		}
		return null;
	}
	/**
	 * Method for Update user proj app rel status.
	 *
	 * @param id
	 *            {@link String}
	 * @param status
	 *            {@link String}
	 * @return true, if successful
	 */
	public boolean updateUserProjAppRelStatus(String id, String status) throws UnauthorizedAccessException {
		try {
			String url = new String(this.serviceUrl + XMAdminRestClientConstants.UPDATE_USER_PROJECT_APP_REL_STATUS
					+ "/" + status + "/" + id);

			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<Object> requestEntity = new HttpEntity<Object>(this.headers);
			ResponseEntity<Boolean> responseEntity = restTemplate.exchange(url, HttpMethod.POST, requestEntity,
					Boolean.class);
			HttpStatus statusCode = responseEntity.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				return responseEntity.getBody();
			} else {
				LOGGER.error(
						"Error while calling XMSYSTEM Update User Project Application Relation Status REST Service, returns the status code: ", //$NON-NLS-1$
						statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException) e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM Update User Project Application Relation Status REST Service!", //$NON-NLS-1$
					e);
		}
		return false;
	}

	/**
	 * Multi update user project app rel status.
	 *
	 * @param userProjectAppRelRequest the user project app rel request
	 * @return the user project app rel batch response
	 * @throws UnauthorizedAccessException the unauthorized access exception
	 */
	public UserProjectAppRelBatchResponse multiUpdateUserProjectAppRelStatus(
			List<UserProjectAppRelRequest> userProjectAppRelRequest) throws UnauthorizedAccessException {
		try {
			String url = new String(this.serviceUrl + XMAdminRestClientConstants.MULTI_UPDATE_USER_PROJECT_APP_STATUS);

			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<List<UserProjectAppRelRequest>> requestEntity = new HttpEntity<List<UserProjectAppRelRequest>>(
					userProjectAppRelRequest, this.headers);
			ResponseEntity<UserProjectAppRelBatchResponse> responseEntity = restTemplate.exchange(url, HttpMethod.POST,
					requestEntity, UserProjectAppRelBatchResponse.class);
			HttpStatus statusCode = responseEntity.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				return responseEntity.getBody();
			} else {
				LOGGER.error(
						"Error while calling XMSYSTEM Multi Update UserProjectApp Status REST Service, returns the status code: ", //$NON-NLS-1$
						statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException) e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM Multi Update UserProjectApp Status REST Service!", e); //$NON-NLS-1$
		}
		return null;
	}
	
	/**
	 * Gets the user proj app rels by user proj rel id AA proj rel id.
	 *
	 * @param userProjectRelId
	 *            {@link String}
	 * @param aaProjectRelId
	 *            {@link String}
	 * @return the user proj app rels by user proj rel id AA proj rel id
	 */
	public List<UserProjAppRelTbl> getUserProjAppRelsByUserProjRelIdAAProjRelId(String userProjectRelId,
			String aaProjectRelId) throws UnauthorizedAccessException {
		try {
			String url = new String(this.serviceUrl
					+ XMAdminRestClientConstants.GET_USER_PROJ_APP_REL_BY_USER_PROJ_REL_ID_AA_PROJ_REL_ID + "/"
					+ userProjectRelId + "/" + aaProjectRelId);
			List<UserProjAppRelTbl> userProjectAppTblList = new ArrayList<>();
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<Object> requestEntity = new HttpEntity<Object>(headers);

			List<HttpMessageConverter<?>> messageConverters = new ArrayList<>();
			messageConverters.add(new MappingJackson2HttpMessageConverter());
			restTemplate.setMessageConverters(messageConverters);

			ResponseEntity<UserProjectAppRelResponse> responseList = restTemplate.exchange(url, HttpMethod.GET,
					requestEntity, UserProjectAppRelResponse.class);
			HttpStatus statusCode = responseList.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				UserProjectAppRelResponse userProjectAppRelResponse = responseList.getBody();
				Iterable<UserProjAppRelTbl> userProjAppRelTbls = userProjectAppRelResponse.getUserProjAppRelTbls();
				for (UserProjAppRelTbl userProjAppRelTbl : userProjAppRelTbls) {
					userProjectAppTblList.add(userProjAppRelTbl);
				}
				Collections.sort(userProjectAppTblList, (UserProjAppRelTbl pa1, UserProjAppRelTbl pa2) -> {
					ProjectApplicationsTbl projectApplicationId1 = pa1.getProjectApplicationId();
					ProjectApplicationsTbl projectApplicationId2 = pa2.getProjectApplicationId();
					
					if (projectApplicationId1 != null && projectApplicationId2 != null) {
						final String proAppName1 = projectApplicationId1.getName();
						final String proAppName2 = projectApplicationId2.getName();
						if (!XMSystemUtil.isEmpty(proAppName1) && !XMSystemUtil.isEmpty(proAppName2)) {
							return proAppName1.compareTo(proAppName2);
						}
					}
					return 0;
				});
				return userProjectAppTblList;
			} else {
				LOGGER.error(
						"Error while calling XMSYSTEM Get User Project Application Rel By User Project Relation Id and AA Project Rel Id REST Service, returns the status code: " //$NON-NLS-1$
								+ statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException) e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			} else if (e instanceof HttpStatusCodeException) {
				RestClientExpHandlerUtil.handleNoXMLObjectFoundException(LOGGER, (HttpStatusCodeException) e);
			} else if(e instanceof ResourceAccessException){
				throw e;
			}
		} catch (Exception e) {
			LOGGER.error(
					"Error while calling XMSYSTEM Get User Project Application Rel By User Project Relation Id and AA Project Rel Id REST Service", //$NON-NLS-1$
					e);
		}
		return null;
	}

	/**
	 * Gets the user proj app rels by user proj rel id AA proj rel id rel type.
	 *
	 * @param userProjectRelId
	 *            {@link String}
	 * @param aaProjectRelId
	 *            {@link String}
	 * @param relationType
	 *            {@link String}
	 * @return the user proj app rels by user proj rel id AA proj rel id rel
	 *         type
	 */
	public List<UserProjAppRelTbl> getUserProjAppRelsByUserProjRelIdAAProjRelIdRelType(String userProjectRelId,
			String aaProjectRelId, String relationType) throws UnauthorizedAccessException {
		try {
			String url = new String(this.serviceUrl
					+ XMAdminRestClientConstants.GET_USER_PROJ_APP_REL_BY_USER_PROJ_REL_ID_AA_PROJ_REL_ID_REL_TYPE + "/"
					+ userProjectRelId + "/" + aaProjectRelId + "/" + relationType);
			List<UserProjAppRelTbl> userAAProjectAppTblList = new ArrayList<>();
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<Object> requestEntity = new HttpEntity<Object>(headers);

			List<HttpMessageConverter<?>> messageConverters = new ArrayList<>();
			messageConverters.add(new MappingJackson2HttpMessageConverter());
			restTemplate.setMessageConverters(messageConverters);

			ResponseEntity<UserProjectAppRelResponse> responseList = restTemplate.exchange(url, HttpMethod.GET,
					requestEntity, UserProjectAppRelResponse.class);
			HttpStatus statusCode = responseList.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				UserProjectAppRelResponse userProjectAppRelResponse = responseList.getBody();
				
				Iterable<UserProjAppRelTbl> userProjAppRelTbls = userProjectAppRelResponse.getUserProjAppRelTbls();
				for (UserProjAppRelTbl userProjAppRelTbl : userProjAppRelTbls) {
					userAAProjectAppTblList.add(userProjAppRelTbl);
				}
				
				Collections.sort(userAAProjectAppTblList, (UserProjAppRelTbl pa1, UserProjAppRelTbl pa2) -> {
					ProjectApplicationsTbl projectApplicationsTbl1 = pa1.getProjectApplicationId();
					ProjectApplicationsTbl projectApplicationsTbl2 = pa2.getProjectApplicationId();
					if (projectApplicationsTbl1 != null && projectApplicationsTbl2 != null) {
						final String proAppName1 = projectApplicationsTbl1.getName();
						final String proAppName2 = projectApplicationsTbl2.getName();
						if (!XMSystemUtil.isEmpty(proAppName1) && !XMSystemUtil.isEmpty(proAppName2)) {
							return proAppName1.compareTo(proAppName2);
						}
					}
					return 0;
				});
				
				return userAAProjectAppTblList;
			} else {
				LOGGER.error(
						"Error while calling XMSYSTEM Get User Project Application Rel By User Project Relation Id, AA Project Rel Id and Relation Type REST Service, returns the status code: " //$NON-NLS-1$
								+ statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException) e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			} else if (e instanceof HttpStatusCodeException) {
				RestClientExpHandlerUtil.handleNoXMLObjectFoundException(LOGGER, (HttpStatusCodeException) e);
			} else if(e instanceof ResourceAccessException){
				throw e;
			}
		} catch (Exception e) {
			LOGGER.error(
					"Error while calling XMSYSTEM Get User Project Application Rel By User Project Relation Id, AA Project Rel Id and Relation Type REST Service", //$NON-NLS-1$
					e);
		}
		return null;
	}

	/**
	 * Gets the user proj app rels by project app id.
	 *
	 * @param projectAppId
	 *            {@link String}
	 * @return the user proj app rels by project app id
	 */
	public UserProjectAppRelResponse getUserProjAppRelsByProjectAppId(String projectAppId)
			throws UnauthorizedAccessException {
		try {
			String url = new String(this.serviceUrl + XMAdminRestClientConstants.GET_USER_PROJ_APP_REL_BY_PROJECT_APP_ID
					+ "/" + projectAppId);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<Object> requestEntity = new HttpEntity<Object>(headers);

			List<HttpMessageConverter<?>> messageConverters = new ArrayList<>();
			messageConverters.add(new MappingJackson2HttpMessageConverter());
			restTemplate.setMessageConverters(messageConverters);

			ResponseEntity<UserProjectAppRelResponse> responseList = restTemplate.exchange(url, HttpMethod.GET,
					requestEntity, UserProjectAppRelResponse.class);
			HttpStatus statusCode = responseList.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				UserProjectAppRelResponse userProjectAppRelResponse = responseList.getBody();
				return userProjectAppRelResponse;
			} else {
				LOGGER.error(
						"Error while calling XMSYSTEM Get User Project Application Rel By projectAppId REST Service, returns the status code: " //$NON-NLS-1$
								+ statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException) e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			} else if (e instanceof HttpStatusCodeException) {
				RestClientExpHandlerUtil.handleNoXMLObjectFoundException(LOGGER, (HttpStatusCodeException) e);
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM Get User Project Application Rel By projectAppId REST Service", //$NON-NLS-1$
					e);
		}
		return null;
	}

	/**
	 * Method for Update user rel types by ids.
	 *
	 * @param userProjectAppRelBatchRequest
	 *            {@link UserProjectAppRelBatchRequest}
	 * @return the user project app rel batch response
	 *         {@link UserProjectAppRelBatchResponse}
	 */
	public UserProjectAppRelBatchResponse updateUserRelTypesByIds(
			UserProjectAppRelBatchRequest userProjectAppRelBatchRequest)
			throws UnauthorizedAccessException, CannotCreateObjectException {
		try {
			String url = new String(
					this.serviceUrl + XMAdminRestClientConstants.UPDATE_PROJECT_APP_USER_REL_TYPES_BY_IDS);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<UserProjectAppRelBatchRequest> request = new HttpEntity<UserProjectAppRelBatchRequest>(
					userProjectAppRelBatchRequest, this.headers);
			ResponseEntity<UserProjectAppRelBatchResponse> responseEntity = restTemplate.postForEntity(url, request,
					UserProjectAppRelBatchResponse.class);
			HttpStatus statusCode = responseEntity.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				UserProjectAppRelBatchResponse userProjectAppRelBatchResponse = responseEntity.getBody();
				return userProjectAppRelBatchResponse;
			} else {
				LOGGER.error(
						"Error while calling XMSYSTEM Update User Project Application Relation Types By IDs REST Service, returns the status code: ", //$NON-NLS-1$
						statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException) e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			} else if (e instanceof HttpStatusCodeException) {
				String errorResponse = ((HttpStatusCodeException) e).getResponseBodyAsString();
				String errCode = RestClientExpHandlerUtil.handleCannotCreateObjectException(LOGGER,
						(HttpStatusCodeException) e);
				throw new CannotCreateObjectException(errorResponse, errCode);
			} else if (e instanceof ResourceAccessException) {
				throw e;
			}
		} catch (Exception e) {
			LOGGER.error(
					"Error while calling XMSYSTEM Update User Project Application Relation Types By IDs REST Service", //$NON-NLS-1$
					e);
		}
		return null;
	}

	/**
	 * Gets the user proj app rels by userId, ProjId and aaId.
	 *
	 * @param userId
	 *            {@link String}
	 * @param projId
	 *            {@link String
	 * @param aaId
	 *            {@link String}
	 * @return the user proj app rels by user id, project id and aa id
	 */
	public UserProjectAppRelResponse getUserProjAppRelsByUserProjAAId(final String userId, final String projId,
			final String aaId) throws UnauthorizedAccessException {
		try {
			String url = new String(
					this.serviceUrl + XMAdminRestClientConstants.GET_USER_PROJ_APP_REL_BY_USER_ID_PROJ_ID_AA_ID + "/"
							+ userId + "/" + projId + "/" + aaId);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<Object> requestEntity = new HttpEntity<Object>(headers);

			List<HttpMessageConverter<?>> messageConverters = new ArrayList<>();
			messageConverters.add(new MappingJackson2HttpMessageConverter());
			restTemplate.setMessageConverters(messageConverters);

			ResponseEntity<UserProjectAppRelResponse> responseList = restTemplate.exchange(url, HttpMethod.GET,
					requestEntity, UserProjectAppRelResponse.class);
			HttpStatus statusCode = responseList.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				UserProjectAppRelResponse userProjectAppRelResponse = responseList.getBody();
				return userProjectAppRelResponse;
			} else {
				LOGGER.error(
						"Error while calling XMSYSTEM Get User Project Application Rel By userId, projId, aaId REST Service, returns the status code: " //$NON-NLS-1$
								+ statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException) e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			} else if (e instanceof HttpStatusCodeException) {
				RestClientExpHandlerUtil.handleNoXMLObjectFoundException(LOGGER, (HttpStatusCodeException) e);
			}
		} catch (Exception e) {
			LOGGER.error(
					"Error while calling XMSYSTEM Get User Project Application Rel By userId, projId, aaId REST Service", //$NON-NLS-1$
					e);
		}
		return null;
	}
}
