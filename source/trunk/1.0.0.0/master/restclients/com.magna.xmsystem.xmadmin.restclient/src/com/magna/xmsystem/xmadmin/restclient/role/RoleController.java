package com.magna.xmsystem.xmadmin.restclient.role;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import com.magna.xmbackend.entities.RoleAdminAreaRelTbl;
import com.magna.xmbackend.entities.RoleUserRelTbl;
import com.magna.xmbackend.entities.RolesTbl;
import com.magna.xmbackend.entities.UsersTbl;
import com.magna.xmbackend.exception.CannotCreateObjectException;
import com.magna.xmbackend.vo.roles.RoleRequest;
import com.magna.xmbackend.vo.roles.RoleResponse;
import com.magna.xmsystem.dependencies.customExceptions.UnauthorizedAccessException;
import com.magna.xmsystem.xmadmin.restclient.RestController;
import com.magna.xmsystem.xmadmin.restclient.users.UserController;
import com.magna.xmsystem.xmadmin.restclient.util.RestClientExpHandlerUtil;
import com.magna.xmsystem.xmadmin.restclient.util.RestClientSortingUtil;
import com.magna.xmsystem.xmadmin.restclient.util.RestClientUtil;
import com.magna.xmsystem.xmadmin.restclient.util.XMAdminRestClientConstants;

/**
 * Class for Role controller.
 *
 * @author Roshan.Ekka
 */
public class RoleController extends RestController {

	/** The Constant LOGGER. */
	private static final Logger LOGGER = LoggerFactory.getLogger(RoleController.class);

	/**
	 * Constructor for RoleController Class.
	 */
	public RoleController() {
		super();
	}

	/**
	 * Method for Creates the role.
	 *
	 * @param roleRequest
	 *            {@link RoleRequest}
	 * @return the roles tbl {@link RolesTbl}
	 */
	public Iterable<RolesTbl> createRole(final List<RoleRequest> roleRequest) throws UnauthorizedAccessException, CannotCreateObjectException {
		try {
			final String url = new String(this.serviceUrl + XMAdminRestClientConstants.CREATE_ROLE);
			final RestTemplate restTemplate = new RestTemplate();
			HttpEntity<List<RoleRequest>> request = new HttpEntity<List<RoleRequest>>(roleRequest, this.headers);
			ResponseEntity<RoleResponse> responseEntity = restTemplate.postForEntity(url, request, RoleResponse.class);
			HttpStatus statusCode = responseEntity.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				RoleResponse response = responseEntity.getBody();
				return response.getRolesTbls();
			} else {
				LOGGER.error("Error while calling XMSYSTEM Create Role REST Service, returns the status code: ", //$NON-NLS-1$
						statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException)e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			} else if (e instanceof HttpStatusCodeException) {
				String errorResponse = ((HttpStatusCodeException) e).getResponseBodyAsString();
				String errCode = RestClientExpHandlerUtil.handleCannotCreateObjectException(LOGGER, (HttpStatusCodeException) e);
				throw new CannotCreateObjectException(errorResponse, errCode);
			}else if (e instanceof ResourceAccessException) {
				throw e;
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM Create Role REST Service", e); //$NON-NLS-1$
		}
		return null;
	}

	/**
	 * Method for Update role.
	 *
	 * @param list
	 *            {@link RoleRequest}
	 * @return true, if successful
	 */
	public boolean updateRole(List<RoleRequest> roleRequest)
			throws UnauthorizedAccessException, CannotCreateObjectException {
		try {
			String url = new String(this.serviceUrl + XMAdminRestClientConstants.UPDATE_ROLE);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<List<RoleRequest>> request = new HttpEntity<List<RoleRequest>>(roleRequest, this.headers);
			ResponseEntity<RoleResponse> responseEntity = restTemplate.postForEntity(url, request, RoleResponse.class);
			HttpStatus statusCode = responseEntity.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				return true;
			} else {
				LOGGER.error("Error while calling XMSYSTEM Upadte Role REST Service, returns the status code: ", //$NON-NLS-1$
						statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException) e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			} else if (e instanceof HttpStatusCodeException) {
				String errorResponse = ((HttpStatusCodeException) e).getResponseBodyAsString();
				String errCode = RestClientExpHandlerUtil.handleCannotCreateObjectException(LOGGER,
						(HttpStatusCodeException) e);
				throw new CannotCreateObjectException(errorResponse, errCode);
			}else if (e instanceof ResourceAccessException) {
				throw e;
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM Upadte Role REST Service", e); //$NON-NLS-1$
		}
		return false;
	}

	/**
	 * Gets the role by id.
	 *
	 * @param roleId
	 *            {@link String}
	 * @return the role by id
	 */
	public RolesTbl getRoleById(String roleId) throws UnauthorizedAccessException {
		try {
			String url = new String(this.serviceUrl + XMAdminRestClientConstants.GET_ROLE_BY_ID + "/" + roleId);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<Object> requestEntity = new HttpEntity<Object>(headers);

			List<HttpMessageConverter<?>> messageConverters = new ArrayList<>();
			messageConverters.add(new MappingJackson2HttpMessageConverter());
			restTemplate.setMessageConverters(messageConverters);

			ResponseEntity<RolesTbl> responseList = restTemplate.exchange(url, HttpMethod.GET, requestEntity,
					RolesTbl.class);
			HttpStatus statusCode = responseList.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				RolesTbl roleTblObject = responseList.getBody();
				return roleTblObject;
			} else {
				LOGGER.error("Error while calling XMSYSTEM Get Role by Id REST Service, returns the status code: " //$NON-NLS-1$
						+ statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException) e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			} else if (e instanceof HttpStatusCodeException) {
				RestClientExpHandlerUtil.handleNoXMLObjectFoundException(LOGGER, (HttpStatusCodeException) e);
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM Get Role by Id REST Service", e); //$NON-NLS-1$
		}
		return null;
	}

	/**
	 * Gets the all roles.
	 *
	 * @param isSorted
	 *            {@link boolean}
	 * @return the all roles
	 */
	public List<RolesTbl> getAllRoles(final boolean isSorted) throws UnauthorizedAccessException {
		try {
			String url = new String(this.serviceUrl + XMAdminRestClientConstants.GET_ROLES);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<Object> requestEntity = new HttpEntity<Object>(headers);

			List<HttpMessageConverter<?>> messageConverters = new ArrayList<>();
			messageConverters.add(new MappingJackson2HttpMessageConverter());
			restTemplate.setMessageConverters(messageConverters);

			ResponseEntity<RoleResponse> responseList = restTemplate.exchange(url, HttpMethod.GET, requestEntity,
					RoleResponse.class);
			HttpStatus statusCode = responseList.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				RoleResponse roleResponseObject = responseList.getBody();
				List<RolesTbl> roleTblList = (List<RolesTbl>) roleResponseObject.getRolesTbls();
				return RestClientSortingUtil.sortRoleTbl(roleTblList);
			} else {
				LOGGER.error("Error while calling XMSYSTEM Get All Roles REST Service, returns the status code: " //$NON-NLS-1$
						+ statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException) e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			} else if (e instanceof HttpStatusCodeException) {
				RestClientExpHandlerUtil.handleNoXMLObjectFoundException(LOGGER, (HttpStatusCodeException) e);
			} else if(e instanceof ResourceAccessException){
				throw e;
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM Get All Roles REST Service", e); //$NON-NLS-1$
		}
		return null;
	}

	/**
	 * Method for Delete role.
	 *
	 * @param roleId
	 *            {@link String}
	 * @return true, if successful
	 */
	public boolean deleteRole(String roleId) throws UnauthorizedAccessException {
		try {
			String url = new String(this.serviceUrl + XMAdminRestClientConstants.DELETE_ROLE + "/" + roleId);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<Object> requestEntity = new HttpEntity<Object>(this.headers);
			ResponseEntity<Boolean> responseEntity = restTemplate.exchange(url, HttpMethod.DELETE, requestEntity,
					Boolean.class);
			HttpStatus statusCode = responseEntity.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				return responseEntity.getBody();
			} else {
				LOGGER.error("Error while calling XMSYSTEM delete Role REST Service, returns the status code: ", //$NON-NLS-1$
						statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException) e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM delete Role REST Service!", e); //$NON-NLS-1$
		}
		return false;
	}
	
	/**
	 * Delete multi role.
	 *
	 * @param roleIds the role ids
	 * @return the role response
	 * @throws UnauthorizedAccessException the unauthorized access exception
	 */
	public RoleResponse deleteMultiRole(Set<String> roleIds) throws UnauthorizedAccessException {
		try {
			String url = new String(this.serviceUrl + XMAdminRestClientConstants.DELETE_MULTI_ROLE);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<Set<String>> requestEntity = new HttpEntity<Set<String>>(roleIds,this.headers);
			ResponseEntity<RoleResponse> responseEntity = restTemplate.exchange(url, HttpMethod.DELETE, requestEntity,
					RoleResponse.class);
			HttpStatus statusCode = responseEntity.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				RoleResponse roleResponse = responseEntity.getBody();
				return roleResponse;
			} else {
				LOGGER.error("Error while calling XMSYSTEM delete Multi Role REST Service, returns the status code: ", //$NON-NLS-1$
						statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException)e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM delete Multi Role REST Service!", e); //$NON-NLS-1$
		}
		return null;
	}
	
	/**
	 * Gets the scope object admin area id by current user.
	 *
	 * @return the scope object admin area id by current user
	 * @throws Exception the exception
	 */
	public String getScopeObjectAdminAreaIdByCurrentUser() throws Exception {
		RoleController roleController = new RoleController();
		List<RolesTbl> allRoles = roleController.getAllRoles(true);
		String userName = RestClientUtil.getInstance().getUserName();
		UsersTbl userByName = new UserController().getUserByName(userName);
		
		for (RolesTbl rolesTbl : allRoles) {
			Collection<RoleAdminAreaRelTbl> roleAdminAreaRelTblCollection = rolesTbl.getRoleAdminAreaRelTblCollection();
			for (RoleAdminAreaRelTbl roleAdminAreaRelTbl : roleAdminAreaRelTblCollection) {
				Collection<RoleUserRelTbl> roleUserRelTblCollection = roleAdminAreaRelTbl.getRoleUserRelTblCollection();
				for (RoleUserRelTbl roleUserRelTbl : roleUserRelTblCollection) {
					if (roleUserRelTbl.getUserId().getUserId().equals(userByName.getUserId())) {
						return roleAdminAreaRelTbl.getAdminAreaId().getAdminAreaId();
					}
				}
			}
		}
		return null;
	}
}