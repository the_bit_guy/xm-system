package com.magna.xmsystem.xmadmin.restclient.util;

/**
 * XMAdmin rest client related constants.
 *
 * @author shashwat.anand
 */
public interface XMAdminRestClientConstants {
	/** SUCCESS constant */
	String SUCCESS = "Success";

	/** ACCEPT_HEADER constants. */
	String ACCEPT_HEADER = "Accept";

	/** APPLICATION_JSON constants. */
	String APPLICATION_JSON = "application/json";

	/** CONTENT_TYPE constants. */
	String CONTENT_TYPE = "Content-Type";

	/** GET constant. */
	String GET = "GET";

	/** POST constant. */
	String POST = "POST";

	/** PUT constant. */
	String PUT = "PUT";

	/** DELETE constant. */
	String DELETE = "DELETE";

	/** GET_ICONS constant. */
	String GET_ICONS = "/icon/findAll";

	/** GET_DEFAULT_ICONS constant. */
	String GET_DEFAULT_ICONS = "/icon/type/default";

	/** GET_NON_DEFAULT_ICONS constant. */
	String GET_NON_DEFAULT_ICONS = "/icon/type/non-default";

	/** GET_ICON_BY_NAME constant. */
	String GET_ICON_BY_NAME = "/icon/findByName";

	/** GET_ICON_BY_ID constant. */
	String GET_ICON_BY_ID = "/icon/findById";

	/** CREATE_ICON constant. */
	String CREATE_ICON = "/icon/save";

	/** DELETE_ICON constant. */
	String DELETE_ICON = "/icon/delete";

	/** CREATE_SITE constant. */
	String CREATE_SITE = "/site/save";

	/** CREATE_ROLE constant. */
	String CREATE_ROLE = "/role/save";

	/** CREATE_DIRECTORY constant. */
	String CREATE_DIRECTORY = "/directory/save";

	/** CREATE_SITE_ADMIN_AREA_REL constant. */
	String CREATE_SITE_ADMIN_AREA_REL = "/siteAdminAreaRel/save";

	/** CREATE_ROLE_USER_REL constant. */
	String CREATE_ROLE_USER_REL = "/roleUserRelation/save";

	/** CREATE_ROLE_ADMIN_AREA_REL constant. */
	String CREATE_ROLE_ADMIN_AREA_REL = "/roleAdminAreaRelation/save";

	/** CREATE_PROJECT_START_APP_REL constant. */
	String CREATE_PROJECT_START_APP_REL = "/projectStartAppRel/save";

	/** CREATE_ADMIN_AREA_USER_APP_REL constant. */
	String CREATE_ADMIN_AREA_USER_APP_REL = "/adminAreaUserAppRel/save";

	/** CREATE_ADMIN_AREA_START_APP_REL constant. */
	String CREATE_ADMIN_AREA_START_APP_REL = "/adminAreaStartAppRel/save";

	/** CREATE_ADMIN_AREA_PROJECT_REL constant. */
	String CREATE_ADMIN_AREA_PROJECT_REL = "/adminAreaProjectRel/save";

	/** CREATE_USER_PROJECT_REL constant. */
	String CREATE_USER_PROJECT_REL = "/userProjectRel/save";

	/** CREATE_USER_START_APP_REL constant. */
	String CREATE_USER_START_APP_REL = "/userStartAppRel/save";

	/** CREATE_PROJECT_APP_USER_REL constant. */
	String CREATE_PROJECT_APP_USER_REL = "/userProjectAppRel/save";

	/** CREATE_ADMIN_AREA_PROJ_APP_REL constant. */
	String CREATE_ADMIN_AREA_PROJ_APP_REL = "/adminAreaProjectAppRel/save";

	/** CREATE_ADMINISTRATION_AREA constant. */
	String CREATE_ADMIN_AREA = "/area/save";

	/** CREATE_PROJECTS constant. */
	String CREATE_PROJECT = "/project/save";

	/** CREATE_BASE_APPLICATION constant. */
	String CREATE_BASE_APPLICATION = "/baseApplication/save";

	/** CREATE_USER_APPLICATION constant. */
	String CREATE_USER_APPLICATION = "/userApplication/save";

	/** CREATE_START_APPLICATION constant. */
	String CREATE_START_APPLICATION = "/startApplication/save";

	/** CREATE_EVENT_APPLICATION constant. */
	String CREATE_EVENT_APPLICATION = "/eventApplication/save";

	/** CREATE_PROJECT_APPLICATION constant. */
	String CREATE_PROJECT_APPLICATION = "/projectApplication/save";

	/** CREATE_USER_APP_USER_REL constant. */
	String CREATE_USER_APP_USER_REL = "/userUserAppRel/save";

	/** CREATE_USER_ROLE_REL constant. */
	String CREATE_USER_ROLE_REL = "/userRoleRelation/save";

	/** CREATE_MULTI_SITE_ADMIN_AREA_REL constant. */
	String CREATE_MULTI_SITE_ADMIN_AREA_REL = "/siteAdminAreaRel/multi/save";

	/** CREATE_MULTI_ROLE_USER_REL constant. */
	String CREATE_MULTI_ROLE_USER_REL = "/roleUserRelation/saveAll";

	/** CREATE_MULTI_PROJECT_ADMIN_AREA_REL constant. */
	String CREATE_MULTI_PROJECT_ADMIN_AREA_REL = "/adminAreaProjectRel/multi/save";

	/** CREATE_MULTI_USER_APP_ADMIN_AREA_REL constant. */
	String CREATE_MULTI_USER_APP_ADMIN_AREA_REL = "/adminAreaUserAppRel/multi/save";

	/** CREATE_MULTI_PROJECT_APP_ADMIN_AREA_REL constant. */
	String CREATE_MULTI_PROJECT_APP_ADMIN_AREA_REL = "/adminAreaProjectAppRel/multi/save";

	/** CREATE_MULTI_START_APP_ADMIN_AREA_REL constant. */
	String CREATE_MULTI_START_APP_ADMIN_AREA_REL = "/adminAreaStartAppRel/multi/save";

	/** CREATE_MULTI_START_APP_PROJECT_REL constant. */
	String CREATE_MULTI_START_APP_PROJECT_REL = "/projectStartAppRel/multi/save";

	/** CREATE_MULTI_USER_PROJECT_REL constant. */
	String CREATE_MULTI_USER_PROJECT_REL = "/userProjectRel/multi/save";

	/** CREATE_MULTI_USER_APP_USER_REL constant. */
	String CREATE_MULTI_USER_APP_USER_REL = "/userUserAppRel/multi/save";

	/** CREATE_MULTI_USER_PROJECT_APP_REL constant. */
	String CREATE_MULTI_PROJECT_APP_USER_REL = "/userProjectAppRel/multi/save";

	/** CREATE_MULTI_DIRECTORY_RELATIONS constant. */
	String CREATE_MULTI_DIRECTORY_RELATIONS = "/directoryRelation/multi/save";

	/** CREATE_MULTI_USER_START_APP_REL constant. */
	String CREATE_MULTI_USER_START_APP_REL = "/userStartAppRel/multi/save";

	/** CREATE_DIRECTORY_RELATIONS constant. */
	String CREATE_DIRECTORY_RELATIONS = "/directoryRelation/multi/save";

	/** GET_SITES constant. */
	String GET_SITES = "/site/findAll";

	/** GET_ROLES constant. */
	String GET_ROLES = "/role/findAll";

	/** GET_ALL_DIRECTORY constant. */
	String GET_ALL_DIRECTORY = "/directory/findAll";

	/** GET_ALL_DIRECTORY constant. */
	String GET_ALL_SMTP = "/propertyconfig/getSmtpDetails";

	/** GET_ALL_DIRECTORY constant. */
	String GET_ALL_LDAP = "/propertyconfig/getLdapDetails";

	/** GET_ALL_SINGLETON constant. */
	String GET_ALL_SINGLETON = "/propertyconfig/getSingletonDetails";

	/** GET_ALL_SINGLETON constant. */
	String GET_ALL_NO_PROJECT_POP_MESSAGE = "/propertyconfig/getProjectPopupDetails";

	/** GET_ALL_SINGLETON constant. */
	String GET_ALL_PROJECT_EXPIRY = "/propertyconfig/getProjectExpiryDetails";

	/** GET_USERS constant. */
	String GET_USERS = "/user/findAll";
	
	/** The get users by adminareaid. */
	String GET_USERS_BY_ADMINAREAID = "/user/findUsersByAdminAreaId";

	/** GET_USERS_USING_FIRST_CHAR constant. */
	String GET_USERS_USING_FIRST_CHAR = "/user/findAllUserNameWith";

	/** GET_SITE_ADMIN_AREA_REL constant. */
	String GET_SITE_ADMIN_AREA_REL = "/siteAdminAreaRel/findAll";

	/** GET_PROJECT_START_APP_REL constant. */
	String GET_PROJECT_START_APP_REL = "/projectStartAppRel/findAll";

	/** GET_USER_START_APP_REL constant. */
	String GET_USER_START_APP_REL_BY_USER_ID = "/userStartAppRel/findUserStartAppRelByUserId";

	/** GET_USER_START_APP_REL_BY_START_APP_ID constant. */
	String GET_USER_START_APP_REL_BY_START_APP_ID = "/userStartAppRel/findUserStartAppRelByStartAppId";

	/** GET_ADMIN_AREA_USER_APP_REL constant. */
	String GET_ADMIN_AREA_USER_APP_REL = "/adminAreaUserAppRel/findAll";

	/** GET_ADMIN_AREA_START_APP_REL constant. */
	String GET_ADMIN_AREA_START_APP_REL = "/adminAreaStartAppRel/findAll";

	/** GET_ADMIN_AREA_PROJECT_REL constant. */
	String GET_ADMIN_AREA_PROJECT_REL = "/adminAreaProjectRel/findAll";

	/** GET_USER_PROJECT_REL constant. */
	String GET_USER_PROJECT_REL = "/userProjectRel/findAll";

	/** GET_USER_PROJECT_REL_BY_USER_ID constant. */
	String GET_USER_PROJECT_REL_BY_USER_ID = "/userProjectRel/findUserProjectRelationByUserId";

	/** GET_USER_PROJECT_REL_BY_PROJECT_ID constant. */
	String GET_USER_PROJECT_REL_BY_PROJECT_ID = "/userProjectRel/findUserProjectRelationByProjectId";

	/** GET_USER_PROJECT_APP_REL constant. */
	String GET_USER_PROJECT_APP_REL = "/userProjectAppRel/findAll";

	/** GET_ADMIN_AREA_PROJECT_APP_REL constant. */
	String GET_ADMIN_AREA_PROJECT_APP_REL = "/adminAreaProjectAppRel/findAll";

	/** GET_ADMIN_AREA_PROJECT_APP_REL_BY_ID constant. */
	String GET_ADMIN_AREA_PROJECT_APP_REL_BY_ID = "/adminAreaProjectAppRel/find";

	/** GET_ADMIN_AREA_PROJECT_APP_REL_BY_ADMIN_AREA_PROJ_REL_ID constant. */
	String GET_ADMIN_AREA_PROJECT_APP_REL_BY_ADMIN_AREA_PROJ_REL_ID = "/adminAreaProjectAppRel/findAdminProjectAppRelByAAProRel";

	/** GET_ADMIN_AREA_PROJECT_APP_REL_BY_ADMIN_AREA_ID_AND_PROJ_ID constant. */
	String GET_ADMIN_AREA_PROJECT_APP_REL_BY_ADMIN_AREA_ID_AND_PROJ_ID = "/adminAreaProjectAppRel/findByAAIdAndProjectId";

	/** GET_ADMIN_AREA_BY_PROJ_APP_ID constant. */
	String GET_ADMIN_AREA_BY_PROJ_APP_ID = "/adminAreaProjectAppRel/findAdminAreaByProjectApplicationId";

	/** GET_ADMIN_AREA_USER_APP_REL_BY_ID constant. */
	String GET_ADMIN_AREA_USER_APP_REL_BY_ID = "/adminAreaUserAppRel/find";

	/** GET_ADMIN_AREA_USER_APP_REL_BY_USER_APP_ID constant. */
	String GET_ADMIN_AREA_USER_APP_REL_BY_USER_APP_ID = "/adminAreaUserAppRel/findByUserAppId";

	/** GET_ADMIN_AREA_USER_APP_REL_BY_ADMIN_AREA_ID constant. */
	String GET_ADMIN_AREA_USER_APP_REL_BY_ADMIN_AREA_ID = "/adminAreaUserAppRel/findAAUserAppRelsByAAId";

	/** GET_ADMIN_AREA_START_APP_REL_BY_ID constant. */
	String GET_ADMIN_AREA_START_APP_REL_BY_ID = "/adminAreaStartAppRel/find";

	/** GET_ADMIN_AREA_START_APP_REL_BY_START_APP_ID constant. */
	String GET_ADMIN_AREA_START_APP_REL_BY_START_APP_ID = "/adminAreaStartAppRel/findAdminAreaStartAppRelByStartAppId";

	/** GET_ADMIN_AREA_PROJECT_REL_BY_ID constant. */
	String GET_ADMIN_AREA_PROJECT_REL_BY_ID = "/adminAreaProjectRel/find";

	/** GET_PROJECT_START_APP_REL_BY_ID constant. */
	String GET_PROJECT_START_APP_REL_BY_ID = "/projectStartAppRel/find";

	/** GET_START_APP_BY_AA_PROJ_REL_ID constant. */
	String GET_START_APP_BY_AA_PROJ_REL_ID = "/projectStartAppRel/findStartAppByAdminAreaProjectRelId";

	/** GET_ADMIN_AREA_PROJECT_REL_BY_PROJECT_ID constant. */
	String GET_ADMIN_AREA_PROJECT_REL_BY_PROJECT_ID = "/adminAreaProjectRel/findByProject";

	/** GET_ADMIN_AREA_PROJ_REL_BY_SITE_AA_ID_PROJECT_ID constant. */
	String GET_ADMIN_AREA_PROJ_REL_BY_SITE_AA_ID_PROJECT_ID = "/adminAreaProjectRel/findBySiteAdminAreaProjectAppId";
	
	/** The get admin area project rel by site aa rel id project id. */
	String GET_ADMIN_AREA_PROJECT_REL_BY_SITE_AA_REL_ID_PROJECT_ID = "/adminAreaProjectRel/findBySiteAdminAreaRelIdAndProjectId";

	/** GET_ADMIN_AREA constant. */
	String GET_ADMIN_AREAS = "/area/findAll";

	/** GET_PROJECTS constant. */
	String GET_PROJECTS = "/project/findAll";

	/** GET_BASE_APPLICATIONS constant. */
	String GET_BASE_APPLICATIONS = "/baseApplication/findAll";

	/** GET_USER_APPLICATIONS constant. */
	String GET_USER_APPLICATIONS = "/userApplication/findAll";

	/** GET_START_APPLICATIONS constant. */
	String GET_START_APPLICATIONS = "/startApplication/findAll";

	/** GET_EVENT_APPLICATIONS constant. */
	String GET_EVENT_APPLICATIONS = "/eventApplication/findAll";

	/** GET_PROJECT_APPLICATIONS constant. */
	String GET_PROJECT_APPLICATIONS = "/projectApplication/findAll";

	/** DELETE_SITE constant. */
	String DELETE_SITE = "/site/delete";

	/** DELETE_ROLE constant. */
	String DELETE_ROLE = "/role/delete";

	/** The delete mult directoryi rel. */
	String DELETE_MULT_DIRECTORYI_REL = "/directoryRelation/multi/delete";

	/** DELETE_SITE_ADMIN_AREA_REL constant. */
	String DELETE_SITE_ADMIN_AREA_REL = "/siteAdminAreaRel/delete";

	/** DELETE_ROLE_USER_REL constant. */
	String DELETE_ROLE_USER_REL = "/roleUserRelation/delete";

	/** DELETE_ADMIN_AREA_PROJECT_REL constant. */
	String DELETE_ADMIN_AREA_PROJECT_REL = "/adminAreaProjectRel/delete";

	/** DELETE_PROJECT_START_APP_REL constant. */
	String DELETE_PROJECT_START_APP_REL = "/projectStartAppRel/delete";

	/** DELETE_ADMIN_AREA_USER_APP_REL constant. */
	String DELETE_ADMIN_AREA_USER_APP_REL = "/adminAreaUserAppRel/delete";

	/** DELETE_ADMIN_AREA_START_APP_REL constant. */
	String DELETE_ADMIN_AREA_START_APP_REL = "/adminAreaStartAppRel/delete";

	/** DELETE_USER_PROJECT_REL constant. */
	String DELETE_USER_PROJECT_REL = "/userProjectRel/delete";

	/** DELETE_USER_PROJECT_APP_REL constant. */
	String DELETE_USER_PROJECT_APP_REL = "/userProjectAppRel/delete";

	/** DELETE_ADMIN_AREA_PROJECT_APP_REL constant. */
	String DELETE_ADMIN_AREA_PROJECT_APP_REL = "/adminAreaProjectAppRel/delete";

	/** DELETE_ADMINAREA constant. */
	String DELETE_ADMIN_AREA = "/area/delete";

	/** DELETE_ADMINAREA constant. */
	String DELETE_PROJECT = "/project/delete";

	/** DELETE_BASE_APPLICATION constant. */
	String DELETE_BASE_APPLICATION = "/baseApplication/delete";

	/** DELETE_USER_APPLICATION constant. */
	String DELETE_USER_APPLICATION = "/userApplication/delete";

	/** DELETE_START_APPLICATION constant. */
	String DELETE_START_APPLICATION = "/startApplication/delete";

	/** DELETE_EVENT_APPLICATION constant. */
	String DELETE_EVENT_APPLICATION = "/eventApplication/delete";

	/** DELETE_PROJECT_APPLICATION constant. */
	String DELETE_PROJECT_APPLICATION = "/projectApplication/delete";

	/** DELETE_DIRECTORY constant . */
	String DELETE_DIRECTORY = "/directory/delete";

	/** UPDATE_DIRECTORY constant. */
	String UPDATE_SMTP_DETAILS = "/propertyconfig/updateSmtpDetails";

	/** UPDATE_DIRECTORY constant. */
	String UPDATE_LDAP_DETAILS = "/propertyconfig/updateLdapDetails";

	/** UPDATE_SINGLETON constant. */
	String UPDATE_SINGLETON_DETAILS = "/propertyconfig/updateSingletonDetails";

	/** UPDATE_SINGLETON constant. */
	String UPDATE_PROJECT_POP_DETAILS = "/propertyconfig/updateProjectPopupDetails";

	/** UPDATE_SINGLETON constant. */
	String UPDATE_PROJECT_EXPIRY = "/propertyconfig/updateProjectExpiryDetails";

	/** UPDATE_SITE constant. */
	String UPDATE_SITE = "/site/update";

	/** UPDATE_ROLE constant. */
	String UPDATE_ROLE = "/role/update";

	/** UPDATE_ADMINAREA constant. */
	String UPDATE_ADMIN_AREA = "/area/update";

	/** UPDATE_PROJECT constant. */
	String UPDATE_PROJECT = "/project/update";

	/** UPDATE_BASE_APPLICATION constant. */
	String UPDATE_BASE_APPLICATION = "/baseApplication/update";

	/** UPDATE_USER_APPLICATION constant. */
	String UPDATE_USER_APPLICATION = "/userApplication/update";

	/** UPDATE_START_APPLICATION constant. */
	String UPDATE_START_APPLICATION = "/startApplication/update";

	/** UPDATE_PROJECT_APPLICATION constant. */
	String UPDATE_PROJECT_APPLICATION = "/projectApplication/update";

	/** UPDATE_EVENT_APPLICATION constant. */
	String UPDATE_EVENT_APPLICATION = "/eventApplication/update";

	/** UPDATE_SITE_STATUS constant. */
	String UPDATE_SITE_STATUS = "/site/updateStatus";

	/** UPDATE_USER_PROJECT_APP_REL_STATUS constant. */
	String UPDATE_USER_PROJECT_APP_REL_STATUS = "/userProjectAppRel/updateStatus";

	/** UPDATE_SITE_ADMIN_AREA_REL_STATUS constant. */
	String UPDATE_SITE_ADMIN_AREA_REL_STATUS = "/siteAdminAreaRel/updateStatus";

	/** UPDATE_ADMIN_AREA_USER_APP_REL_STATUS constant. */
	String UPDATE_ADMIN_AREA_USER_APP_REL_STATUS = "/adminAreaUserAppRel/updateStatus";

	/** UPDATE_ADMIN_AREA_START_APP_REL_STATUS constant. */
	String UPDATE_ADMIN_AREA_START_APP_REL_STATUS = "/adminAreaStartAppRel/updateStatus";

	/** UPDATE_PROJECT_START_APP_REL_STATUS constant. */
	String UPDATE_PROJECT_START_APP_REL_STATUS = "/projectStartAppRel/updateStatus";

	/** UPDATE_USER_START_APP_REL_STATUS constant. */
	String UPDATE_USER_START_APP_REL_STATUS = "/userStartAppRel/updateStatusByUSARelId";

	/** UPDATE_ADMIN_AREA_PROJECT_REL_STATUS constant. */
	String UPDATE_ADMIN_AREA_PROJECT_REL_STATUS = "/adminAreaProjectRel/updateStatus";

	/** UPDATE_USER_PROJECT_REL_STATUS constant. */
	String UPDATE_USER_PROJECT_REL_STATUS = "/userProjectRel/updateStatus";

	/** UPDATE_ADMIN_AREA_PROJECT_APP_REL_STATUS constant. */
	String UPDATE_ADMIN_AREA_PROJECT_APP_REL_STATUS = "/adminAreaProjectAppRel/updateStatus";

	/** UPDATE_ADMIN_AREA_PROJECT_APP_REL_TYPE_BY_IDS constant. */
	String UPDATE_ADMIN_AREA_PROJECT_APP_REL_TYPE_BY_IDS = "/adminAreaProjectAppRel/updateRelTypesByIds";

	/** UPDATE_ADMIN_AREA_USER_APP_REL_TYPE_BY_IDS constant. */
	String UPDATE_ADMIN_AREA_USER_APP_REL_TYPE_BY_IDS = "/adminAreaUserAppRel/updateRelTypesByIds";

	/** UPDATE_ADMIN_AREA_STATUS constant. */
	String UPDATE_ADMIN_AREA_STATUS = "/area/updateStatus";

	/** UPDATE_PROJECT_STATUS constant. */
	String UPDATE_PROJECT_STATUS = "/project/updateStatus";

	/** UPDATE_BASE_APPLICATION_STATUS constant. */
	String UPDATE_BASE_APPLICATION_STATUS = "/baseApplication/updateStatus";

	/** UPDATE_USER_APPLICATION_STATUS constant. */
	String UPDATE_USER_APPLICATION_STATUS = "/userApplication/updateStatus";

	/** UPDATE_START_APPLICATION_STATUS constant. */
	String UPDATE_START_APPLICATION_STATUS = "/startApplication/updateStatus";

	/** UPDATE_EVENT_APPLICATION_STATUS constant. */
	String UPDATE_EVENT_APPLICATION_STATUS = "/eventApplication/updateStatus";

	/** UPDATE_USER_APPLICATION_STATUS constant. */
	String UPDATE_PROJECT_APPLICATION_STATUS = "/projectApplication/updateStatus";

	/** UPDATE_DIRECTORY constant. */
	String UPDATE_DIRECTORY = "/directory/update";

	/** ICON_USED_BY_ID constant. */
	String ICON_USED_BY_ID = "/icon/usedById";

	/** ICON_USED_BY_NAME constant. */
	String ICON_USED_BY_NAME = "/icon/usedByName";

	/** GET_ADMIN_AREAS_BY_ID constant. */
	String GET_ADMIN_AREAS_BY_ID = "/area/find";

	/** GET_PROJECT_BY_ID constant. */
	String GET_PROJECT_BY_ID = "/project/find";

	/** GET_PROJECT_BY_START_APP_ID constant. */
	String GET_PROJECT_BY_START_APP_ID = "/project/findProjectsByStartAppId";

	/** GET_PROJECT_BY_USER_ID constant. */
	String GET_PROJECT_BY_USER_ID = "/project/findProjectsByUserId";

	/** GET_PROJECT_BY_ADMIN_AREA_ID constant. */
	String GET_PROJECT_BY_ADMIN_AREA_ID = "/project/findAllProjectsByAAId";

	/** GET_PROJECT_BY_PROJECT_APP_SITE_ADMINAREA_ID constant. */
	String GET_PROJECT_BY_PROJECT_APP_SITE_ADMINAREA_ID = "/project/findAllProjectsByProjectAppSiteAAId";

	/** GET_BASE_APP_BY_ID constant. */
	String GET_BASE_APP_BY_ID = "/baseApplication/find";

	/** GET_SITE_BY_ID constant. */
	String GET_SITE_BY_ID = "/site/find";

	/** GET_ROLE_BY_ID constant. */
	String GET_ROLE_BY_ID = "/role/find";

	/** GET_OBJECT_PERMISSION_BY_ROLE_ID constant. */
	String GET_OBJECT_PERMISSION_BY_ROLE_ID = "/permission/findObjectPermissionByRoleId";

	/** GET_OBJECT_REL_PERMISSION_BY_ROLE_ID constant. */
	String GET_OBJECT_REL_PERMISSION_BY_ROLE_ID = "/permission/findObjectRelPermissionByRoleId";

	/** GET_ALL_OBJECT_PERMISSION constant. */
	String GET_ALL_OBJECT_PERMISSION = "/permission/findAllObjectPermission";

	/** GET_ALL_REL_OBJECT_PERMISSION constant. */
	String GET_ALL_REL_OBJECT_PERMISSION = "/permission/findAllObjectRelationPermission";

	/** GET_ALL_AA_BASED_REL_PERMISSION constant. */
	String GET_ALL_AA_BASED_REL_PERMISSION = "/permission/findAllAABasedRelationPermission";

	/** GET_SITE_BY_NAME constant. */
	String GET_SITE_BY_NAME = "/site/findByName";

	/** GET_PROJECTS_BY_SITE_ID constant. */
	String GET_PROJECTS_BY_SITE_ID = "/site/findProjectsById";

	/** GET_PROJECTS_BY_SITE_NAME constant. */
	String GET_PROJECTS_BY_SITE_NAME = "/site/findProjectsByName";

	/** GET_SITES_BY_PROJECT_ID constant. */
	String GET_SITES_BY_PROJECT_ID = "/site/findSitesByProjectId";

	/** GET_SITES_BY_USER_APP_ID constant. */
	String GET_SITES_BY_USER_APP_ID = "/site/findSitesByUserAppId";

	/** GET_SITES_BY_PROJECT_APP_ID constant. */
	String GET_SITES_BY_PROJECT_APP_ID = "/site/findSitesByProjectAppId";

	/** GET_SITES_BY_PROJECT_APP_ID constant. */
	String GET_ADMIN_AREA_BY_PROJECT_ID = "/area/findAAByProjectId";

	/** GET_ADMIN_AREA_BY_USER_APP_ID constant. */
	String GET_ADMIN_AREA_BY_USER_APP_ID = "/area/findAAByUserAppId";

	/** GET_ADMIN_AREA_BY_PROJECT_APP_ID constant. */
	String GET_ADMIN_AREA_BY_PROJECT_APP_ID = "/area/findAAByUserProjectAppId";

	/** GET_ADMIN_AREA_BY_START_APP_ID constant. */
	String GET_ADMIN_AREA_BY_START_APP_ID = "/area/findAAByStartAppId";

	/** GET_ADMIN_AREA_BY_PROJECT_ID_AND_SITE_ID constant. */
	String GET_ADMIN_AREA_BY_PROJECT_ID_AND_SITE_ID = "/area/findAAByProjectIdAndSiteId";

	/** GET_ADMIN_AREA_BY_USER_APP_ID_AND_SITE_ID constant. */
	String GET_ADMIN_AREA_BY_USER_APP_ID_AND_SITE_ID = "/area/findAAByUserAppIdAndSiteId";

	/** GET_ADMIN_AREA_BY_START_APP_ID_AND_SITE_ID constant. */
	String GET_ADMIN_AREA_BY_START_APP_ID_AND_SITE_ID = "/area/findAAByStartAppIdAndSiteId";

	/** GET_ADMIN_AREA_BY_PROJECT_APP_ID_AND_SITE_ID constant. */
	String GET_ADMIN_AREA_BY_PROJECT_APP_ID_AND_SITE_ID = "/area/findAAByProjectAppIdAndSiteId";

	/** GET_START_APP_BY_ID constant. */
	String GET_START_APP_BY_ID = "/startApplication/find";

	/** GET_START_APP_BY_BASE_APP_ID constant. */
	String GET_START_APP_BY_BASE_APP_ID = "/startApplication/findStartApplicationsByBaseAppId";

	/** GET_USER_START_APPS constant. */
	String GET_USER_START_APPS = "/startApplication/findUserStartApplications";

	/** GET_START_APPS_BY_ADMIN_AREA_ID_PROJECT_ID constant. */
	String GET_START_APPS_BY_ADMIN_AREA_ID_PROJECT_ID = "/startApplication/findStartAppByAAIdProjectId";

	/** GET_PROJECT_APP_BY_PROJECT_ID constant. */
	String GET_PROJECT_APP_BY_PROJECT_ID = "/projectApplication/findProjectApplicationsByProjectId";

	/** GET_PROJECT_APP_BY_BASE_APP_ID constant. */
	String GET_PROJECT_APP_BY_BASE_APP_ID = "/projectApplication/findProjectApplicationsByBaseAppId";

	/** GET_PROJECT_APP_BY_POSITION constant. */
	String GET_PROJECT_APP_BY_POSITION = "/projectApplication/findProjectAppByPosition";

	/** GET_PROJECT_APP_BY_PROJECT_SITE_AA_ID constant. */
	String GET_PROJECT_APP_BY_PROJECT_SITE_AA_ID = "/projectApplication/findProjAppByProjectSiteAAId";

	/** GET_PROJECT_APP_BY_USER_PROJECT_ID constant. */
	String GET_PROJECT_APP_BY_USER_PROJECT_ID = "/projectApplication/findProjAppByUserProjectId";

	/** GET_PROJECT_APP_BY_PROJECT_SITE_ASMINAREA_ID_AND_RELTYPE constant. */
	String GET_PROJECT_APP_BY_PROJECT_SITE_ASMINAREA_ID_AND_RELTYPE = "/projectApplication/findProjAppByProjectSiteAAIdAndRelType";

	/** GET_CHILD_PROJECT_APP_BY_PROJECT_APP_ID constant. */
	String GET_CHILD_PROJECT_APP_BY_PROJECT_APP_ID = "/projectApplication/findProjectAppByProjectAppId";

	/** GET_PROJECT_APP_BY_STATUS constant. */
	String GET_PROJECT_APP_BY_STATUS = "/projectApplication/findAllProjectApplicationsByStatus";

	/** GET_PROJECT_APP_BY_POSITIONS constant. */
	String GET_PROJECT_APP_BY_POSITIONS = "/projectApplication/findAllProjectApplicationsByPositions";

	/** GET_USER_APP_BY_BASE_APP_ID constant. */
	String GET_USER_APP_BY_BASE_APP_ID = "/userApplication/findUserApplicationsByBaseAppId";

	/** GET_USER_APP_BY_POSITION constant. */
	String GET_USER_APP_BY_POSITION = "/userApplication/findUserAppByPosition";

	/** GET_ALL_USERAPPS_BY_AA_ID_AND_ACTIVE */
	String GET_ALL_USERAPPS_BY_AA_ID_AND_ACTIVE = "/userApplication/findAllUserAppsByAAIDAndActive";

	/** GET_USER_APP_BY_USER_ID constant. */
	String GET_USER_APP_BY_USER_ID = "/userApplication/findUserApplicationsByUserId";

	/** GET_CHILD_USER_APP_BY_USER_APP_ID constant. */
	String GET_CHILD_USER_APP_BY_USER_APP_ID = "/userApplication/findChildUserAppByUserAppId";

	/** GET_USER_APP_BY_STATUS constant. */
	String GET_USER_APP_BY_STATUS = "/userApplication/findAllUserAppByStatus";

	/** GET_USER_APP_BY_POSITIONS constant. */
	String GET_USER_APP_BY_POSITIONS = "/userApplication/findAllUserAppByPositions";

	/** GET_PROJECT_APP_BY_PROJECT_ID_AND_TYPE constant. */
	String GET_PROJECT_APP_BY_PROJECT_ID_AND_TYPE = "/projectApplication/findProjectApplicationsByProjectIdAndType";

	/** GET_PROJECT_APP_BY_ID constant. */
	String GET_PROJECT_APP_BY_ID = "/projectApplication/find";

	/** GET_SITES_BY_START_APP_ID constant. */
	String GET_SITES_BY_START_APP_ID = "/site/findSitesByStartAppId";

	/** GET_SITES_BY_ADMIN_AREA_ID constant. */
	String GET_SITES_BY_ADMIN_AREA_ID = "/site/findSitesByAdminAreaId";

	/** GET_DIRECTORY_REL_BY_DIR_ID_AND_OBJ_TYPE constant. */
	String GET_DIRECTORY_REL_BY_DIR_ID_AND_OBJ_TYPE = "/directoryRelation/findDirectoryRelByDirIdAndObjectType";

	/** GET_USER_BY_NAME constant. */
	String GET_USER_BY_NAME = "/user/findByName";

	/** GET_USERS_BY_PROJECT_ID constant. */
	String GET_USERS_BY_PROJECT_ID = "/user/findUsersByProjectId";

	/** GET_USERS_BY_USER_APP_ID constant. */
	String GET_USERS_BY_USER_APP_ID = "/user/findUsersByUserAppId";

	/** GET_USERS_BY_PROJECT_APP_ID constant. */
	String GET_USERS_BY_PROJECT_APP_ID = "/user/findUsersByProjectAppId";

	/** GET_USERS_BY_START_APP_ID constant. */
	String GET_USERS_BY_START_APP_ID = "/user/findUsersByStartAppId";

	/** The save user. */
	String SAVE_USER = "/user/batchSave";

	/** The update user. */
	String UPDATE_USER = "/user/update";

	/** The update user status. */
	String UPDATE_USER_STATUS = "/user/updateStatus";

	/** The delete user. */
	String DELETE_USER = "/user/delete";

	/** GET_ADMIN_AREAS_BY_SITE_ID constant. */
	String GET_ADMIN_AREAS_BY_SITE_ID = "/site/findAdminAreaBySiteId";

	/** GET_ADMIN_AREAS_BY_SITE_NAME constant. */
	String GET_ADMIN_AREAS_BY_SITE_NAME = "/site/findAdminAreaBySiteName";

	/** GET_USER_USER_APP_REL_BY_USER_ID_SITE_AA_REL_ID constant. */
	String GET_USER_USER_APP_REL_BY_USER_ID_SITE_AA_REL_ID = "/userUserAppRel/findByUserIdSiteAARelId";

	/** GET_USER_USER_APP_REL_BY_USER_ID_SITE_AA_REL_ID_REL_TYPE constant. */
	String GET_USER_USER_APP_REL_BY_USER_ID_SITE_AA_REL_ID_REL_TYPE = "/userUserAppRel/findByUserIdSiteAARelIdRelType";

	/** GET_USER_USER_APP_REL_BY_USER_APP_ID constant. */
	String GET_USER_USER_APP_REL_BY_USER_APP_ID = "/userUserAppRel/findUserUserAppRelsByUserAppId";

	/** GET_USER_USER_APP_REL_BY_USER_ID_AA_ID constant. */
	String GET_USER_USER_APP_REL_BY_USER_ID_AA_ID = "/userUserAppRel/findByUserIdAAId";

	/** GET_USER_PROJ_APP_REL_BY_USER_PROJ_REL_ID_AA_PROJ_REL_ID constant. */
	String GET_USER_PROJ_APP_REL_BY_USER_PROJ_REL_ID_AA_PROJ_REL_ID = "/userProjectAppRel/findByUserProjectRelIdAndAAProjectRelId";

	/**
	 * GET_USER_PROJ_APP_REL_BY_USER_PROJ_REL_ID_AA_PROJ_REL_ID_REL_TYPE
	 * constant.
	 */
	String GET_USER_PROJ_APP_REL_BY_USER_PROJ_REL_ID_AA_PROJ_REL_ID_REL_TYPE = "/userProjectAppRel/findByUserProjectRelIdAAProjRelIdUserRelType";

	/** GET_USER_PROJ_APP_REL_BY_PROJECT_APP_ID constant. */
	String GET_USER_PROJ_APP_REL_BY_PROJECT_APP_ID = "/userProjectAppRel/findUserProjectAppRelsByProjectAppId";

	/** GET_USER_PROJ_APP_REL_BY_USER_ID_PROJ_ID_AA_ID constant. */
	String GET_USER_PROJ_APP_REL_BY_USER_ID_PROJ_ID_AA_ID = "/userProjectAppRel/findByUserIdProjectIdAAId";

	/** UPDATE_ID constant. */
	String UPDATE_ID = "id";

	/** UPDATE_STATUS constant. */
	String UPDATE_STATUS = "status";

	/** GET_PROJECTS_BY_SITE_ADMIN_REL_ID constant. */
	String GET_PROJECTS_BY_SITE_ADMIN_REL_ID = "/siteAdminAreaRel/findAllProjectsBySiteAdminAreaRelId";

	/** GET_USER_APPS_BY_SITE_ADMIN_REL_ID constant. */
	String GET_USER_APPS_BY_SITE_ADMIN_REL_ID = "/siteAdminAreaRel/findAllUserAppsBySiteAdminAreaRelId";

	/** GET_ROLE_USER_REL_BY_ROLE_ID constant. */
	String GET_ROLE_USER_REL_BY_ROLE_ID = "/roleUserRelation/findRoleUserRelTblByRoleId";

	/** GET_ROLE_ADMIN_AREA_REL_BY_ROLE_ID constant. */
	String GET_ROLE_ADMIN_AREA_REL_BY_ROLE_ID = "/roleAdminAreaRelation/findRoleAdminAreaRelTblByRoleId";

	/** GET_ROLE_USER_REL_BY_ROLE_ID_AND_ROLE_ADMINAREA_REL_ID constant. */
	String GET_ROLE_USER_REL_BY_ROLE_ID_AND_ROLE_ADMINAREA_REL_ID = "/roleUserRelation/findRoleUserRelTblByRoleIdRoleAARelId";

	/** GET_START_APPS_BY_SITE_ADMIN_REL_ID constant. */
	String GET_START_APPS_BY_SITE_ADMIN_REL_ID = "/siteAdminAreaRel/findAllStartAppsBySiteAdminAreaRelId";

	/** GET_SITE_ADMIN_AREA_REL_BY_SITE_ID constant. */
	String GET_SITE_ADMIN_AREA_REL_BY_SITE_ID = "/siteAdminAreaRel/findSiteAdminAreaRelBySiteId";
	
	/** The get site admin area rel by admin area id. */
	String GET_SITE_ADMIN_AREA_REL_BY_ADMIN_AREA_ID = "/siteAdminAreaRel/findSiteAdminAreaRelByAAId";

	/** GET_START_APPS_BY_SITE_ADMIN_PROJ_REL_ID constant. */
	String GET_START_APPS_BY_SITE_ADMIN_PROJ_REL_ID = "/adminAreaProjectRel/findAllStartAppsByAdminAreaProjectRelId";

	/** GET_PROJECT_APPS_BY_SITE_ADMIN_PROJ_REL_ID constant. */
	String GET_PROJECT_APPS_BY_SITE_ADMIN_PROJ_REL_ID = "/adminAreaProjectRel/findAllProjectAppsByAdminAreaProjectRelId";

	/** GET_PROJECT_START_APP_BY_PROJECT_SITE_ADMIN_AREA_ID constant. */
	String GET_PROJECT_START_APP_BY_PROJECT_SITE_ADMIN_AREA_ID = "/projectStartAppRel/findProjectStartAppByProjectSiteAAId";

	/** DELETE_USER_USER_APP_REL_BY_USER_ID constant */
	String DELETE_USER_USER_APP_REL_BY_ID = "/userUserAppRel/delete";

	/** DELETE_USER_ROLE_REL_BY_ID constant */
	String DELETE_USER_ROLE_REL_BY_ID = "/userRoleRelation/delete";

	/** DELETE_USER_USER_APP_REL_BY_USER_ID constant */
	String DELETE_USER_START_APP_REL_BY_ID = "/userStartAppRel/delete";

	/** GET_ALL_LDAP_USERNAMES constant */
	String GET_ALL_LDAP_USERNAMES = "/ldap/all/userNames";

	/** GET_ALL_LDAP_OUS constant */
	String GET_ALL_LDAP_OUS = "/ldap/all/ous";

	/** GET_ALL_LDAP_USERS constant */
	String GET_ALL_LDAP_USERS = "/ldap/all/ldapUsers";

	/** GET_LDAP_USER_DETAILS constant */
	String GET_LDAP_USER_DETAIL = "/ldap/ldapUserDetail";

	String SEARCH_LDAP_USER_NMAES = "/ldap/search";

	String SEARCH_LDAP_USER = "/ldap/searchUsers";

	String GET_PERMISSION_BY_ROLEID = "/rolePermissionRel/findPermissionByRoleId";

	/** GET_USER_BY_ID constant. */
	String GET_USER_BY_ID = "/user/find";

	/** GET_USER_APP_BY_AA_ID_AND_USERNAME constant. */
	String GET_USER_APP_BY_AA_ID_AND_USERNAME = "/userApplication/findUserAppByAAIdAndUserName";

	/** GET_PROJECT_APP_BY_AA_ID_PROJ_ID_AND_USERNAME constant. */
	String GET_PROJECT_APP_BY_AA_ID_PROJ_ID_AND_USERNAME = "/projectApplication/findProjectAppsByAAIdProjectIdAndUserName";

	/** The create admin menu relation. */
	String CREATE_ADMIN_MENU_RELATION = "/adminMenuConfig/save";

	/** The find admin menu by objtype. */
	String FIND_ADMIN_MENU_BY_OBJTYPE = "/adminMenuConfig/findByObjectType";

	/** The delete admin menu by id. */
	String DELETE_ADMIN_MENU_BY_ID = "/adminMenuConfig/delete";

	/** The create admin menu multi relation. */
	String CREATE_ADMIN_MENU_MULTI_RELATION = "/adminMenuConfig/multi/save";

	/** GET_USER_APP_BY_ID constant. */
	String GET_USER_APP_BY_ID = "/userApplication/find";

	/** GET_USER_PROJECT_EXPIRY_DAYS_BY_USER_PROJECT_ID constant. */
	String GET_USER_PROJECT_EXPIRY_DAYS_BY_USER_PROJECT_ID = "/userProjectRel/findUserProjectExpiryDaysByUserProjectId";
	
	/** The get user project rel by userid and projectid. */
	String GET_USER_PROJECT_REL_BY_USER_ID_AND_PROJECT_ID = "/userProjectRel/findUserProjectRelByUserIdProjectId";

	/** UPDATE_USER_PROJECT_EXPIRY_DAYS_BY_ID constant. */
	String UPDATE_USER_PROJECT_EXPIRY_DAYS_BY_ID = "/userProjectRel/updateProjectExpiryDaysById";

	/** GET_ALL_ADMIN_MENU_CONFIG_REL constant. */
	String GET_ALL_ADMIN_MENU_CONFIG_REL = "/adminMenuConfig/findAll";

	/** The save notification. */
	String SAVE_NOTIFICATION = "/notify/save";

	/** The update notification. */
	String UPDATE_NOTIFICATION = "/notify/updateNotificationConfig";

	/** The sendmail notification. */
	String SENDMAIL_NOTIFICATION = "/notify/sendMail";

	/** The find notification by event. */
	String FIND_NOTIFICATION_BY_EVENT_NAME = "/notify/findByEvent";

	/** The UPDATE_USER_APP_USER_REL_TYPES_BY_IDS. */
	String UPDATE_USER_APP_USER_REL_TYPES_BY_IDS = "/userUserAppRel/updateUserRelTypesByIds";

	/** The UPDATE_PROJECT_APP_USER_REL_TYPES_BY_IDS. */
	String UPDATE_PROJECT_APP_USER_REL_TYPES_BY_IDS = "/userProjectAppRel/updateUserRelTypesByIds";

	/** DELETE_ROLE_AA_REL constant. */
	String DELETE_ROLE_AA_REL = "/roleAdminAreaRelation/delete";

	/** GET_ROLE_AA_REL_BY_ID constant. */
	String GET_ROLE_AA_REL_BY_ID = "/roleAdminAreaRelation/find";

	/** The get user app by aa id. */
	String GET_USER_APP_BY_AA_ID = "/startApplication/findStartAppsByAAId";

	/** GET_ACCESS_ALLOWED constant. */
	String GET_ACCESS_ALLOWED = "/validation/accessAllowed";
	
	/** The get hotline permission. */
	String GET_HOTLINE_PERMISSION = "/validation/checkHotlinePermissions";

	/** The get user app by project id. */
	String GET_USER_APP_BY_PROJECT_ID = "/startApplication/findStartAppsByProjectId";
	
	/** GET_USER_PROJECTS_BY_SITE_ID constant. */
	String GET_USER_PROJECTS_BY_SITE_ID = "/project/findUserProjectsBySiteId";
	
	/** The save live message. */
	String SAVE_LIVE_MESSAGE = "/liveMessage/save";
	
	/** The get live messages. */
	String GET_LIVE_MESSAGES = "/liveMessage/findAll";

	/** The delete livemessage. */
	String DELETE_LIVEMESSAGE = "/liveMessage/delteById";

	/** The update live message. */
	String UPDATE_LIVE_MESSAGE = "/liveMessage/update";
	
	/** USER_SETTINGS_BY_SITE_ID constant. */
	String USER_SETTINGS_BY_SITE_ID = "/userSettings/findUserSettingsBySiteId";
	
	/** UPDATE_USER_SETTINGS constant. */
	String UPDATE_PROJECT_IN_USER_SETTINGS = "/userSettings/saveOrUpdateProject";
	
	/** UPDATE_SETTINGS_IN_USER_SETTINGS constant. */
	String UPDATE_SETTINGS_IN_USER_SETTINGS = "/userSettings/saveOrUpdateSettings";

	/** The get users by siteid. */
	String GET_USERS_BY_SITEID = "/user/findUserBySiteId";

	/** The create group. */
	String CREATE_GROUP = "/groups/save";

	/** The get all groups. */
	String GET_ALL_GROUPS = "/groups/findAll";

	/** The delete group. */
	String DELETE_GROUP = "/groups/delete";

	/** The update group. */
	String UPDATE_GROUP = "/groups/update";

	/** The get groupby grouptype. */
	String GET_GROUPBY_GROUPTYPE = "/groups/findByGroupType";

	/** The create multi group relations. */
	String CREATE_MULTI_GROUP_RELATIONS = "/groupRelation/multi/save";

	/** The get group rel by group id. */
	String GET_GROUP_REL_BY_GROUP_ID = "/groupRelation/findGroupRelsByGroupId";

	/** The delete multi grouprel. */
	String DELETE_MULTI_GROUPREL = "/groupRelation/multi/delete";
	
	/** The admin menu config apps. */
	String ADMIN_MENU_CONFIG_APPS = "/adminMenuConfig/findUserAdminsMenuConfiguration";

	/** The get users account status. */
	String GET_USERS_ACCOUNT_STATUS = "/userAccountStatus/findAll";
	
	/** The live message by status. */
	String LIVE_MESSAGE_BY_STATUS = "/liveMessage/findUnreadLiveMessages";
	
	/** The update live message status. */
	String UPDATE_LIVE_MESSAGE_STATUS = "/liveMessage/updateLiveMessageStatus";

	/** The delete multi site. */
	String DELETE_MULTI_SITE = "/site/multiDelete";

	/** The delete multi adminarea. */
	String DELETE_MULTI_ADMIN_AREA = "/area/multiDelete";

	/** The delete multi project. */
	String DELETE_MULTI_PROJECT = "/project/multiDelete";

	/** The delete multi user. */
	String DELETE_MULTI_USER = "/user/multiDelete";

	/** The delete multi base app. */
	String DELETE_MULTI_BASE_APP = "/baseApplication/multiDelete";

	/** The delete multi project app. */
	String DELETE_MULTI_PROJECT_APP = "/projectApplication/multiDelete";

	/** The delete multi start app. */
	String DELETE_MULTI_START_APP = "/startApplication/multiDelete";

	/** The delete multi user app. */
	String DELETE_MULTI_USER_APP = "/userApplication/multiDelete";

	/** The delete multi group. */
	String DELETE_MULTI_GROUP = "/groups/multiDelete";

	/** The delete multi directory. */
	String DELETE_MULTI_DIRECTORY = "/directory/multiDelete";

	/** The delete multi role. */
	String DELETE_MULTI_ROLE = "/role/multiDelete";

	/** The delete multi live message. */
	String DELETE_MULTI_LIVE_MESSAGE = "/liveMessage/multiDelete";

	/** The delete multi admin area project app rel. */
	String DELETE_MULTI_ADMIN_AREA_PROJECT_APP_REL = "/adminAreaProjectAppRel/multiDelete";

	/** The delete multi admin area project rel. */
	String DELETE_MULTI_ADMIN_AREA_PROJECT_REL = "/adminAreaProjectRel/multiDelete";

	/** The delete multi admin area start app rel. */
	String DELETE_MULTI_ADMIN_AREA_START_APP_REL = "/adminAreaStartAppRel/multiDelete";

	/** The delete multi admin area user app rel. */
	String DELETE_MULTI_ADMIN_AREA_USER_APP_REL = "/adminAreaUserAppRel/multiDelete";

	/** The delete multi admin menu by id. */
	String DELETE_MULTI_ADMIN_MENU_REL_BY_ID = "/adminMenuConfig/multiDelete";

	/** The delete multi project start app rel. */
	String DELETE_MULTI_PROJECT_START_APP_REL = "/projectStartAppRel/multiDelete";

	/** The delete multi role aa rel. */
	String DELETE_MULTI_ROLE_AA_REL = "/roleAdminAreaRelation/multiDelete";

	/** The delete multi role user rel. */
	String DELETE_MULTI_ROLE_USER_REL = "/roleUserRelation/multiDelete";

	/** The delete multi site admin area rel. */
	String DELETE_MULTI_SITE_ADMIN_AREA_REL = "/siteAdminAreaRel/multiDelete";

	/** The delete multi user project app rel. */
	String DELETE_MULTI_USER_PROJECT_APP_REL = "/userProjectAppRel/multiDelete";

	/** The delete multi user project rel. */
	String DELETE_MULTI_USER_PROJECT_REL = "/userProjectRel/multiDelete";

	/** The delete multi user start app rel by id. */
	String DELETE_MULTI_USER_START_APP_REL_BY_ID = "/userStartAppRel/multiDelete";

	/** The delete multi user user app rel by id. */
	String DELETE_MULTI_USER_USER_APP_REL_BY_ID = "/userUserAppRel/multiDelete";
	
	/** The user history save. */
	String USER_HISTORY_SAVE = "/userHistory/save";
    
    /** update user status to history. */
    String UPDATE_USER_STATUS_HISTORY = "/userHistory/updateStatusToHistory";
    
    /** The get all user history. */
    String GET_ALL_USER_HISTORY = "/userHistory/findUserHistory";
    
    /** The get authentication response. */
    String GET_AUTHENTICATION_RESPONSE = "/auth/login";

	/** The get all user status. */
	String GET_ALL_USER_STATUS = "/userHistory/findUserStatus";
	
	/** The get all admin base history. */
	String GET_ALL_ADMIN_BASE_HISTORY = "/adminHistory/findAdminBaseHistory";
	 
	/** The get all admin relation history. */
	String GET_ALL_ADMIN_RELATION_HISTORY = "/adminHistory/findAdminRelHistory";

	/** The is super admin. */
	String IS_SUPER_ADMIN = "/user/isSuperAdmin";
	
	/** The get project by site relation. */
	String GET_PROJECT_BY_SITE_RELATION = "/project/findProjectsBySiteId";
	
	/** The get project by aa relation. */
	String GET_PROJECT_BY_AA_RELATION = "/project/findProjectsByAdminAreaId";
	
	/** The get project by user relation. */
	String GET_PROJECT_BY_USER_RELATION = "/project/findAllProjectsByUserId";
	
	String GET_PROJECT_BY_USER_SITE_RELATION = "/project/findAProjectsByUserIdAndSiteId";
	
	/** The get userapp by aa relation. */
	String GET_USERAPP_BY_AA_RELATION = "/adminAreaUserAppRel/findUserAppsByAAId";
	
	/** The get project by aa project relation. */
	String GET_PROJECT_BY_AA_PROJECT_RELATION = "/adminAreaProjectAppRel/findProjectAppsByAAIdAndProjectId";
	
	/** The multi update site status. */
	String MULTI_UPDATE_SITE_STATUS = "/site/multiStatusUpdate";

	/** The multi update admin area status. */
	String MULTI_UPDATE_ADMIN_AREA_STATUS = "/area/multiStatusUpdate";

	/** The multi update project status. */
	String MULTI_UPDATE_PROJECT_STATUS = "/project/multiStatusUpdate";

	/** The multi update user status. */
	String MULTI_UPDATE_USER_STATUS = "/user/multiStatusUpdate";

	/** The multi update user app status. */
	String MULTI_UPDATE_USER_APP_STATUS = "/userApplication/multiStatusUpdate";

	/** The multi update project app status. */
	String MULTI_UPDATE_PROJECT_APP_STATUS = "/projectApplication/multiStatusUpdate";
	
	/** The multi update base app status. */
	String MULTI_UPDATE_BASE_APP_STATUS = "/baseApplication/multiStatusUpdate";

	/** The multi update start app status. */
	String MULTI_UPDATE_START_APP_STATUS = "/startApplication/multiStatusUpdate";

	/** The multi update aa project status. */
	String MULTI_UPDATE_AA_PROJECT_STATUS = "/adminAreaProjectRel/multiStatusUpdate";

	/** The multi update user startapp status. */
	String MULTI_UPDATE_USER_STARTAPP_STATUS = "/userStartAppRel/multiStatusUpdate";

	/** The multi update user project status. */
	String MULTI_UPDATE_USER_PROJECT_STATUS = "/userProjectRel/multiStatusUpdate";

	/** The multi update user projectapp status. */
	String MULTI_UPDATE_USER_PROJECT_APP_STATUS = "/userProjectAppRel/multiStatusUpdate";

	/** The multi update project startapp status. */
	String MULTI_UPDATE_PROJECT_START_APP_STATUS = "/projectStartAppRel/multiStatusUpdate";

	/** The multi update aa startapp status. */
	String MULTI_UPDATE_AA_START_APP_STATUS = "/adminAreaStartAppRel/multiStatusUpdate";

	/** The multi update aa projectapp status. */
	String MULTI_UPDATE_AA_PROJECT_APP_STATUS = "/adminAreaProjectAppRel/multiStatusUpdate";

	/** The multi update aa user app status. */
	String MULTI_UPDATE_AA_USER_APP_STATUS = "/adminAreaUserAppRel/multiStatusUpdate";

	/** The multi update site aa status. */
	String MULTI_UPDATE_SITE_AA_STATUS = "/siteAdminAreaRel/multiStatusUpdate";
	
	/** The create css file. */
	String CREATE_CSS_FILE = "/css/save";
	
	/** The delete css file. */
	String DELETE_CSS_FILE = "/css/deleteCssSettings";
	
	/** The css file existence. */
	String CSS_FILE_EXISTENCE = "/css/findCssSettings";
	
	/** The update css applied status. */
	String UPDATE_CSS_APPLIED_STATUS = "/css/updateCssSettings";
	
	/** The create update rearrange settings. */
	String CREATE_UPDATE_REARRANGE_SETTINGS = "/rearrange/save";
	
	/** The delete rearrange settings. */
	String DELETE_REARRANGE_SETTINGS = "/rearrange/deleteCssSettings";
	
	/** The find rearrange settings. */
	String FIND_REARRANGE_SETTINGS = "/rearrange/findSettings";
	
	/** The get user app by name. */
	String GET_USER_APP_BY_NAME = "/userApplication/findByName";
	
	/** The get project app by name. */
	String GET_PROJECT_APP_BY_NAME = "/projectApplication/findByName";
	
	/** The get user app by user app name. */
	String GET_USER_APP_BY_USER_APP_NAME = "/userApplication/findUserAppAndChildByName";
	
	/** The get project app by project app name. */
	String GET_PROJECT_APP_BY_PROJECT_APP_NAME = "/projectApplication/findProjectAppsAndChildBydProjectAppName";
	
	/** The get aa by site id. */
	String GET_AA_BY_SITE_ID = "/area/findAndminAreasBySiteId";
	
	/** The get project by name. */
	String GET_PROJECT_BY_NAME = "/project/findByName";

	/** The save template. */
	String SAVE_TEMPLATE = "/notify/saveTemplate";

	/** The find notification templates. */
	String FIND_NOTIFICATION_TEMPLATES = "/notify/findAllTemplates";

	/** The update template. */
	String UPDATE_TEMPLATE = "/notify/updateTemplate";

	/** The delete multi template. */
	String DELETE_MULTI_TEMPLATE = "/notify/templates/multiDelete";

	String FIND_ALL_NOTIFICATION_EVENTS = "/notify/findAllEvents";

	String DELETE_MULTI_ACTION = "/notify/configs/multiDelete";

	/** The update notification status. */
	String UPDATE_NOTIFICATION_STATUS = "/notify/updateNotificationEventStatus";

	String UPDATE_NOTIFY_ACTION_STATUS = "/notify/updateNotificationConfigStatus";
}
