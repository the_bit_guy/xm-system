package com.magna.xmsystem.xmadmin.restclient.icons;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import com.magna.xmbackend.entities.IconsTbl;
import com.magna.xmbackend.exception.CannotCreateObjectException;
import com.magna.xmbackend.vo.icon.Icon;
import com.magna.xmbackend.vo.icon.IkonRequest;
import com.magna.xmbackend.vo.icon.IkonResponse;
import com.magna.xmsystem.dependencies.customExceptions.UnauthorizedAccessException;
import com.magna.xmsystem.xmadmin.restclient.RestController;
import com.magna.xmsystem.xmadmin.restclient.util.RestClientExpHandlerUtil;
import com.magna.xmsystem.xmadmin.restclient.util.XMAdminRestClientConstants;

/**
 * Class for Icon controller.
 *
 * @author Chiranjeevi.Akula
 */
public class IconController extends RestController{

	/** The Constant LOGGER. */
	private static final Logger LOGGER = LoggerFactory.getLogger(IconController.class);

	/**
	 * Constructor for IconController Class.
	 */
	public IconController() {
		super();
	}

	/**
	 * Method for Creates the icon.
	 *
	 * @param ikonRequest {@link Icon}
	 * @return the string {@link String}
	 */
	public String createIcon(final IkonRequest ikonRequest) throws UnauthorizedAccessException, CannotCreateObjectException {
		try {
			final String url = new String(this.serviceUrl + XMAdminRestClientConstants.CREATE_ICON);
			final RestTemplate restTemplate = new RestTemplate();
			final HttpEntity<IkonRequest> requestEntity = new HttpEntity<IkonRequest>(ikonRequest, this.headers);
			final ResponseEntity<IconsTbl> responseEntity = restTemplate.postForEntity(url, requestEntity,
					IconsTbl.class);
			final HttpStatus statusCode = responseEntity.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				IconsTbl iconResponse = responseEntity.getBody();
				String iconsList = iconResponse.getIconId();
				return iconsList;
			} else {
				LOGGER.error("Error while calling XMSYSTEM Create Icon REST Service, returns the status code: ", //$NON-NLS-1$
						statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException)e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			} else if (e instanceof HttpStatusCodeException) {
				String errorResponse = ((HttpStatusCodeException) e).getResponseBodyAsString();
				String errCode = RestClientExpHandlerUtil.handleCannotCreateObjectException(LOGGER, (HttpStatusCodeException) e);
				throw new CannotCreateObjectException(errorResponse, errCode);
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM Create Icon REST Service", e); //$NON-NLS-1$
		}
		return null;
	}

	/**
	 * Gets the all icons.
	 *
	 * @param isSorted {@link boolean}
	 * @return the all icons
	 */
	public List<IconsTbl> getAllIcons(final boolean isSorted) throws UnauthorizedAccessException {
		List<IconsTbl> returnList = new ArrayList<>();
		try {
			String url = new String(this.serviceUrl + XMAdminRestClientConstants.GET_ICONS);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<Object> requestEntity = new HttpEntity<Object>(this.headers);
			ResponseEntity<IkonResponse> responseEntity = restTemplate.exchange(url, HttpMethod.GET, requestEntity,
					IkonResponse.class);
			HttpStatus statusCode = responseEntity.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				IkonResponse iconResponse = responseEntity.getBody();
				returnList = (List<IconsTbl>) iconResponse.getIconsTbls();
				if (isSorted) {
					returnList.sort((icon1, icon2) -> {
						return icon1.getIconName().compareToIgnoreCase(icon2.getIconName());
					});
				}
			} else {
				LOGGER.error("Error while calling XMSYSTEM get all Icon REST Service, returns the status code: ", //$NON-NLS-1$
						statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException)e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			} else if (e instanceof HttpStatusCodeException) {
				RestClientExpHandlerUtil.handleNoXMLObjectFoundException(LOGGER, (HttpStatusCodeException) e);
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM get all Icon REST Service", e);
		}
		return returnList;
	}

	
	/**
	 * Gets the all default icons.
	 *
	 * @param isSorted {@link boolean}
	 * @return the all default icons
	 */
	public List<IconsTbl> getAllDefaultIcons(final boolean isSorted) throws UnauthorizedAccessException {
		List<IconsTbl> returnList = new ArrayList<>();
		try {
			String url = new String(this.serviceUrl + XMAdminRestClientConstants.GET_DEFAULT_ICONS);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<Object> requestEntity = new HttpEntity<Object>(this.headers);
			ResponseEntity<IkonResponse> responseEntity = restTemplate.exchange(url, HttpMethod.GET, requestEntity,
					IkonResponse.class);
			HttpStatus statusCode = responseEntity.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				IkonResponse iconResponse = responseEntity.getBody();
				returnList = (List<IconsTbl>) iconResponse.getIconsTbls();
				if (isSorted) {
					returnList.sort((icon1, icon2) -> {
						return icon1.getIconName().compareToIgnoreCase(icon2.getIconName());
					});
				}
			} else {
				LOGGER.error("Error while calling XMSYSTEM get all Default Icon REST Service, returns the status code: ", //$NON-NLS-1$
						statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException)e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			} else if (e instanceof HttpStatusCodeException) {
				RestClientExpHandlerUtil.handleNoXMLObjectFoundException(LOGGER, (HttpStatusCodeException) e);
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM get all Default Icon REST Service", e);
		}
		return returnList;
	}
	
 	/**
	  * Gets the all non default icons.
	  *
	  * @param isSorted {@link boolean}
	  * @return the all non default icons
	  */
	 public List<IconsTbl> getAllNonDefaultIcons(final boolean isSorted) throws UnauthorizedAccessException {
			List<IconsTbl> returnList = new ArrayList<>();
			try {
				final String url = new String(this.serviceUrl + XMAdminRestClientConstants.GET_NON_DEFAULT_ICONS);
				final RestTemplate restTemplate = new RestTemplate();
				HttpEntity<Object> requestEntity = new HttpEntity<Object>(this.headers);
				ResponseEntity<IkonResponse> responseEntity = restTemplate.exchange(url, HttpMethod.GET, requestEntity,
						IkonResponse.class);
				HttpStatus statusCode = responseEntity.getStatusCode();
				if (statusCode.is2xxSuccessful()) {
					IkonResponse iconResponse = responseEntity.getBody();
					returnList = (List<IconsTbl>) iconResponse.getIconsTbls();
					if (isSorted) {
						returnList.sort((icon1, icon2) -> {
							return icon1.getIconName().compareToIgnoreCase(icon2.getIconName());
						});
					}
				} else {
					LOGGER.error("Error while calling XMSYSTEM get all NON-Default Icon REST Service, returns the status code: ", //$NON-NLS-1$
							statusCode.value());
				}
			} catch (RestClientException e) {
				if (e instanceof HttpClientErrorException) {
					HttpStatus statusCode = ((HttpClientErrorException)e).getStatusCode();
					if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
						throw new UnauthorizedAccessException(e.getMessage());
					}
				} else if (e instanceof HttpStatusCodeException) {
					RestClientExpHandlerUtil.handleNoXMLObjectFoundException(LOGGER, (HttpStatusCodeException) e);
				}
			} catch (Exception e) {
				LOGGER.error("Error while calling XMSYSTEM get all NON-Default Icon REST Service", e);
			}
			return returnList;
		}
	 
	/**
	 * Method for Delete icon.
	 *
	 * @param iconId {@link String}
	 * @return true, if successful
	 */
	public boolean deleteIcon(final String iconId) throws UnauthorizedAccessException {
		try {
			final String url = new String(this.serviceUrl + XMAdminRestClientConstants.DELETE_ICON + "/" + iconId);
			final RestTemplate restTemplate = new RestTemplate();
			final HttpEntity<Object> requestEntity = new HttpEntity<Object>(this.headers);
			final ResponseEntity<Boolean> responseEntity = restTemplate.exchange(url, HttpMethod.DELETE, requestEntity,
					Boolean.class);
			HttpStatus statusCode = responseEntity.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				return responseEntity.getBody();
			} else {
				LOGGER.error("Error while calling XMSYSTEM delete Icon REST Service, returns the status code: ", //$NON-NLS-1$
						statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException)e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM delete Icon REST Service", e); //$NON-NLS-1$
		}
		return false;
	}


	/**
	 * Checks if is icon used by id.
	 *
	 * @param iconId {@link String}
	 * @return true, if is icon used by id
	 */
	public boolean isIconUsedById(final String iconId) throws UnauthorizedAccessException {
		try {
			String url = new String(this.serviceUrl + XMAdminRestClientConstants.ICON_USED_BY_ID + "/" + iconId);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<Object> requestEntity = new HttpEntity<Object>(this.headers);
			ResponseEntity<Boolean> responseEntity = restTemplate.exchange(url, HttpMethod.GET, requestEntity,
					Boolean.class);
			HttpStatus statusCode = responseEntity.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				return responseEntity.getBody();
			} else {
				LOGGER.error("Error while calling XMSYSTEM Icon Used By Id REST Service, returns the status code: ", //$NON-NLS-1$
						statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException)e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM Icon Used By Id REST Service", e); //$NON-NLS-1$
		}
		return false;
	}
	
	/**
	 * Checks if is icon used by name.
	 *
	 * @param iconName {@link String}
	 * @return true, if is icon used by name
	 */
	public boolean isIconUsedByName(final String iconName) throws UnauthorizedAccessException {
		try {
			final String url = new String(this.serviceUrl + XMAdminRestClientConstants.ICON_USED_BY_NAME + "/" + iconName);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<Object> requestEntity = new HttpEntity<Object>(this.headers);
			ResponseEntity<Boolean> responseEntity = restTemplate.exchange(url, HttpMethod.POST, requestEntity,
					Boolean.class);
			HttpStatus statusCode = responseEntity.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				return responseEntity.getBody();
			} else {
				LOGGER.error("Error while calling XMSYSTEM Icon Used By Name REST Service, returns the status code: ", //$NON-NLS-1$
						statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException)e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM Icon Used By Name REST Service", e); //$NON-NLS-1$
		}
		return false;
	}
	/**
	 * Gets the icon by name.
	 *
	 * @param iconRequest {@link String}
	 * @return the icon by name
	 * 
	 */
	public IconsTbl getIconByName(final IkonRequest iconRequest) throws UnauthorizedAccessException {
		try {
			final String url = new String(this.serviceUrl + XMAdminRestClientConstants.GET_ICON_BY_NAME);
			final RestTemplate restTemplate = new RestTemplate();
			HttpEntity<IkonRequest> requestEntity = new HttpEntity<IkonRequest>(iconRequest, this.headers);
			
			
			ResponseEntity<IconsTbl> responseEntity = restTemplate.postForEntity(url, requestEntity, IconsTbl.class);
			HttpStatus statusCode = responseEntity.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				
				IconsTbl iconsTbl = responseEntity.getBody();
				return iconsTbl;
			} else {
				LOGGER.error("Error while calling XMSYSTEM Get Iocns By Name REST Service, returns the status code: ", //$NON-NLS-1$
						statusCode.value());
			}
		}  catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException)e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			} else if (e instanceof HttpStatusCodeException) {
				RestClientExpHandlerUtil.handleNoXMLObjectFoundException(LOGGER, (HttpStatusCodeException) e);
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM Get Iocns by Name REST Service", e); //$NON-NLS-1$
		}
		return null;
	}

	/**
	 * Get Icon by Id
	 * 
	 * @param iconId {@link String}
	 * @return {@link IconsTbl}
	 */
	public IconsTbl getIconById(final String iconId) throws UnauthorizedAccessException {
		try {
			String url = new String(this.serviceUrl + XMAdminRestClientConstants.GET_ICON_BY_ID + "/" + iconId);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<Object> requestEntity = new HttpEntity<Object>(headers);

			List<HttpMessageConverter<?>> messageConverters = new ArrayList<>();
			messageConverters.add(new MappingJackson2HttpMessageConverter());
			restTemplate.setMessageConverters(messageConverters);

			ResponseEntity<IconsTbl> responseList = restTemplate.exchange(url, HttpMethod.GET, requestEntity,
					IconsTbl.class);
			HttpStatus statusCode = responseList.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				IconsTbl iconsTbl = responseList.getBody();
				return iconsTbl;
			} else {
				LOGGER.error("Error while calling XMSYSTEM Get Icon by Id REST Service, returns the status code: " //$NON-NLS-1$
						+ statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException)e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			} else if (e instanceof HttpStatusCodeException) {
				RestClientExpHandlerUtil.handleNoXMLObjectFoundException(LOGGER, (HttpStatusCodeException) e);
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM Get Icon by Id REST Service", e); //$NON-NLS-1$
		}
		return null;
	}
}
