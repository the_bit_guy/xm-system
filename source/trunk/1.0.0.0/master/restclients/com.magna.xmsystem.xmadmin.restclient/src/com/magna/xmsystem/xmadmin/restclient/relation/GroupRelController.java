package com.magna.xmsystem.xmadmin.restclient.relation;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import com.magna.xmbackend.exception.CannotCreateObjectException;
import com.magna.xmbackend.vo.group.GroupRelBatchRequest;
import com.magna.xmbackend.vo.group.GroupRelBatchResponse;
import com.magna.xmsystem.dependencies.customExceptions.UnauthorizedAccessException;
import com.magna.xmsystem.xmadmin.restclient.RestController;
import com.magna.xmsystem.xmadmin.restclient.util.RestClientExpHandlerUtil;
import com.magna.xmsystem.xmadmin.restclient.util.RestClientSortingUtil;
import com.magna.xmsystem.xmadmin.restclient.util.XMAdminRestClientConstants;

/**
 * The Class GroupRelController.
 */
public class GroupRelController extends RestController{
	/** The Constant LOGGER. */
	private static final Logger LOGGER = LoggerFactory.getLogger(GroupRelController.class);

	/**
	 * Instantiates a new group rel controller.
	 */
	public GroupRelController() {
		super();
	}
	
	/**
	 * Creates the group relations.
	 *
	 * @param groupRelBatchRequest the group rel batch request
	 * @return the group rel batch response
	 * @throws UnauthorizedAccessException the unauthorized access exception
	 * @throws CannotCreateObjectException the cannot create object exception
	 */
	public GroupRelBatchResponse createGroupRelations( GroupRelBatchRequest groupRelBatchRequest)
			throws UnauthorizedAccessException, CannotCreateObjectException {
		try {
			String url = new String(this.serviceUrl + XMAdminRestClientConstants.CREATE_MULTI_GROUP_RELATIONS);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<GroupRelBatchRequest> request = new HttpEntity<GroupRelBatchRequest>(
					groupRelBatchRequest, this.headers);
			ResponseEntity<GroupRelBatchResponse> responseEntity = restTemplate.postForEntity(url, request,
					GroupRelBatchResponse.class);
			HttpStatus statusCode = responseEntity.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				GroupRelBatchResponse groupRelResponse = responseEntity.getBody();
				return groupRelResponse;
			} else {
				LOGGER.error(
						"Error while calling XMSYSTEM Create Group Relations REST Service, returns the status code: ", //$NON-NLS-1$
						statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException) e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			} else if (e instanceof HttpStatusCodeException) {
				String errorResponse = ((HttpStatusCodeException) e).getResponseBodyAsString();
				String errCode = RestClientExpHandlerUtil.handleCannotCreateObjectException(LOGGER,
						(HttpStatusCodeException) e);
				throw new CannotCreateObjectException(errorResponse, errCode);
			} else if (e instanceof ResourceAccessException) {
				throw e;
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM Create Group Relations REST Service", e); //$NON-NLS-1$
		}
		return null;
	}
	
	/**
	 * Gets the group rel by group id.
	 *
	 * @param groupId the group id
	 * @return the group rel by group id
	 * @throws UnauthorizedAccessException the unauthorized access exception
	 */
	public GroupRelBatchResponse getGroupRelByGroupId(String groupId) throws UnauthorizedAccessException {
		try {
			String url = new String(
					this.serviceUrl + XMAdminRestClientConstants.GET_GROUP_REL_BY_GROUP_ID + "/" + groupId);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<Object> requestEntity = new HttpEntity<Object>(headers);

			List<HttpMessageConverter<?>> messageConverters = new ArrayList<>();
			messageConverters.add(new MappingJackson2HttpMessageConverter());
			restTemplate.setMessageConverters(messageConverters);

			ResponseEntity<GroupRelBatchResponse> responseList = restTemplate.exchange(url, HttpMethod.GET,
					requestEntity, GroupRelBatchResponse.class);
			HttpStatus statusCode = responseList.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				GroupRelBatchResponse groupRelBatchResponse = responseList.getBody();
				RestClientSortingUtil.sortGroupRelByGroupId(groupRelBatchResponse.getGroupRelResponses());
				return groupRelBatchResponse;
			} else {
				LOGGER.error(
						"Error while calling XMSYSTEM Get All Group Relations By GroupId and ObjectType REST Service, returns the status code: " //$NON-NLS-1$
								+ statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException) e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			} else if (e instanceof HttpStatusCodeException) {
				RestClientExpHandlerUtil.handleNoXMLObjectFoundException(LOGGER, (HttpStatusCodeException) e);
			} else if(e instanceof ResourceAccessException){
				throw e;
			}
		} catch (Exception e) {
			LOGGER.error(
					"Error while calling XMSYSTEM Get All Group Relations By GroupId and ObjectType REST Service", //$NON-NLS-1$
					e);
		}
		return null;
	}
	
	/**
	 * Delete multi group rel.
	 *
	 * @param groupRefIds the group ref ids
	 * @return true, if successful
	 * @throws UnauthorizedAccessException the unauthorized access exception
	 */
	public boolean deleteMultiGroupRel(Set<String> groupRefIds) throws UnauthorizedAccessException {
		try {
			String url = new String(this.serviceUrl + XMAdminRestClientConstants.DELETE_MULTI_GROUPREL);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<Object> requestEntity = new HttpEntity<Object>(groupRefIds, this.headers);
			ResponseEntity<Boolean> responseEntity = restTemplate.exchange(url, HttpMethod.POST, requestEntity,
					Boolean.class);
			HttpStatus statusCode = responseEntity.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				return responseEntity.getBody();
			} else {
				LOGGER.error("Error while calling XMSYSTEM delete Group Rel REST Service, returns the status code: ", //$NON-NLS-1$
						statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException) e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM delete  Group Rel REST Service!", e); //$NON-NLS-1$
		}
		return false;
	}
}
