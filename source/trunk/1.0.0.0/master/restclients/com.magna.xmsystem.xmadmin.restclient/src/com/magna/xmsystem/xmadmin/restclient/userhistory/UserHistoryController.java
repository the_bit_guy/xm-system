package com.magna.xmsystem.xmadmin.restclient.userhistory;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import com.magna.xmbackend.entities.UserHistoryTbl;
import com.magna.xmbackend.exception.CannotCreateObjectException;
import com.magna.xmbackend.vo.userHistory.UserHistoryRequest;
import com.magna.xmbackend.vo.userHistory.UserHistoryResponseWrapper;
import com.magna.xmsystem.dependencies.customExceptions.UnauthorizedAccessException;
import com.magna.xmsystem.xmadmin.restclient.RestController;
import com.magna.xmsystem.xmadmin.restclient.util.RestClientExpHandlerUtil;
import com.magna.xmsystem.xmadmin.restclient.util.XMAdminRestClientConstants;

/**
 * The Class UserHistoryController.
 */
public class UserHistoryController extends RestController{
	
	/** The Constant LOGGER. */
	private static final Logger LOGGER = LoggerFactory.getLogger(UserHistoryController.class);

	/**
	 * Instantiates a new user history controller.
	 */
	public UserHistoryController() {
		super();
	}
	
	/**
	 * Method for Log user status.
	 *
	 * @param userHistoryRequest {@link UserHistoryRequest}
	 * @return the user history tbl {@link UserHistoryTbl}
	 * @throws UnauthorizedAccessException the unauthorized access exception
	 */
	public UserHistoryTbl logUserStatus(final UserHistoryRequest userHistoryRequest) throws UnauthorizedAccessException {
		try {
			String url = new String(this.serviceUrl + XMAdminRestClientConstants.USER_HISTORY_SAVE);
			RestTemplate restTemplate = new RestTemplate();
			final HttpEntity<UserHistoryRequest> requestEntity = new HttpEntity<UserHistoryRequest>(userHistoryRequest, this.headers);
			ResponseEntity<UserHistoryTbl> responseEntity = restTemplate.exchange(url, HttpMethod.POST, requestEntity,
					UserHistoryTbl.class);
			HttpStatus statusCode = responseEntity.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				return responseEntity.getBody();
			} else {
				LOGGER.error("Error while calling USER_HISTORY logging REST Service, returns the status code: " //$NON-NLS-1$
						+ statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException)e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			} else if (e instanceof HttpStatusCodeException) {
				RestClientExpHandlerUtil.handleNoXMLObjectFoundException(LOGGER, (HttpStatusCodeException) e);
			} else if (e instanceof ResourceAccessException) {
				throw e;
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling USER_HISTORY logging REST Service", e); //$NON-NLS-1$
		}
		return null;
	}
	
	/**
	 * Update user history status.
	 *
	 * @param isUserStatus the is user status
	 * @return true, if successful
	 * @throws UnauthorizedAccessException the unauthorized access exception
	 * @throws CannotCreateObjectException the cannot create object exception
	 */
	public boolean updateUserStatusToHistory()
			throws UnauthorizedAccessException, CannotCreateObjectException {
		try {
			String url = new String(this.serviceUrl + XMAdminRestClientConstants.UPDATE_USER_STATUS_HISTORY);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<Object> request = new HttpEntity<Object>(this.headers);
			ResponseEntity<Boolean> responseEntity = restTemplate.exchange(url, HttpMethod.PUT, request, Boolean.class);
			HttpStatus statusCode = responseEntity.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				return true;
			} else {
				LOGGER.error("Error while calling updateUserHistoryStatus, returns the status code: ", //$NON-NLS-1$
						statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException) e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			} else if (e instanceof HttpStatusCodeException) {
				String errorResponse = ((HttpStatusCodeException) e).getResponseBodyAsString();
				String errCode = RestClientExpHandlerUtil.handleCannotCreateObjectException(LOGGER,
						(HttpStatusCodeException) e);
				throw new CannotCreateObjectException(errorResponse, errCode);
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling updateUserHistoryStatus REST Service", e); //$NON-NLS-1$
		}
		return false;
	}
	
	public UserHistoryResponseWrapper getAllUserHistory(UserHistoryRequest historyRequest) throws UnauthorizedAccessException {
		try {
			String url = new String(this.serviceUrl + XMAdminRestClientConstants.GET_ALL_USER_HISTORY);
			RestTemplate restTemplate = new RestTemplate();
			final HttpEntity<UserHistoryRequest> requestEntity = new HttpEntity<UserHistoryRequest>(historyRequest, this.headers);

			List<HttpMessageConverter<?>> messageConverters = new ArrayList<>();
			messageConverters.add(new MappingJackson2HttpMessageConverter());
			restTemplate.setMessageConverters(messageConverters);

			ResponseEntity<UserHistoryResponseWrapper> responseList = restTemplate.exchange(url, HttpMethod.POST, requestEntity, UserHistoryResponseWrapper.class);
			HttpStatus statusCode = responseList.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				UserHistoryResponseWrapper userHistoryRes = responseList.getBody();
				return userHistoryRes;
			} else {
				LOGGER.error("Error while calling XMSYSTEM Get All userHistory REST Service, returns the status code: " //$NON-NLS-1$
						+ statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException)e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			} else if (e instanceof HttpStatusCodeException) {
				RestClientExpHandlerUtil.handleNoXMLObjectFoundException(LOGGER, (HttpStatusCodeException) e);
			} else if (e instanceof ResourceAccessException) {
				throw e;
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM Get All userHistory REST Service", e); //$NON-NLS-1$
		}
		return null;
	}
	
	
	public UserHistoryResponseWrapper getAllUserStatus(UserHistoryRequest historyRequest) throws UnauthorizedAccessException {
		try {
			String url = new String(this.serviceUrl + XMAdminRestClientConstants.GET_ALL_USER_STATUS);
			RestTemplate restTemplate = new RestTemplate();
			final HttpEntity<UserHistoryRequest> requestEntity = new HttpEntity<UserHistoryRequest>(historyRequest, this.headers);
			List<HttpMessageConverter<?>> messageConverters = new ArrayList<>();
			messageConverters.add(new MappingJackson2HttpMessageConverter());
			restTemplate.setMessageConverters(messageConverters);

			ResponseEntity<UserHistoryResponseWrapper> responseList = restTemplate.exchange(url, HttpMethod.POST, requestEntity, UserHistoryResponseWrapper.class);
			HttpStatus statusCode = responseList.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				UserHistoryResponseWrapper userHistoryRes = responseList.getBody();
				return userHistoryRes;
			} else {
				LOGGER.error("Error while calling XMSYSTEM Get All userStatus REST Service, returns the status code: " //$NON-NLS-1$
						+ statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException)e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			} else if (e instanceof HttpStatusCodeException) {
				RestClientExpHandlerUtil.handleNoXMLObjectFoundException(LOGGER, (HttpStatusCodeException) e);
			}else if (e instanceof ResourceAccessException) {
				throw e;
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM Get All userStatus REST Service", e); //$NON-NLS-1$
		}
		return null;
	}
}
