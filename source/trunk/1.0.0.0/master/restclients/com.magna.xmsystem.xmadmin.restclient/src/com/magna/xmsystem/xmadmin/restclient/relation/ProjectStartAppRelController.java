package com.magna.xmsystem.xmadmin.restclient.relation;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import com.magna.xmbackend.entities.ProjectStartAppRelTbl;
import com.magna.xmbackend.exception.CannotCreateObjectException;
import com.magna.xmbackend.vo.rel.ProjectStartAppRelBatchRequest;
import com.magna.xmbackend.vo.rel.ProjectStartAppRelBatchResponse;
import com.magna.xmbackend.vo.rel.ProjectStartAppRelRequest;
import com.magna.xmbackend.vo.rel.ProjectStartAppRelResponse;
import com.magna.xmsystem.dependencies.customExceptions.UnauthorizedAccessException;
import com.magna.xmsystem.xmadmin.restclient.RestController;
import com.magna.xmsystem.xmadmin.restclient.site.SiteController;
import com.magna.xmsystem.xmadmin.restclient.util.RestClientExpHandlerUtil;
import com.magna.xmsystem.xmadmin.restclient.util.XMAdminRestClientConstants;

// TODO: Auto-generated Javadoc
/**
 * Class for Site admin area rel controller.
 *
 * @author Roshan.Ekka
 */
public class ProjectStartAppRelController extends RestController {
	/** The Constant LOGGER. */
	private static final Logger LOGGER = LoggerFactory.getLogger(SiteController.class);

	/**
	 * Constructor for SiteController Class.
	 */
	public ProjectStartAppRelController() {
		super();
	}
	
	/**
	 * Constructor for ProjectStartAppRelController Class.
	 *
	 * @param adminAreaId {@link String}
	 */
	public ProjectStartAppRelController(String adminAreaId) {
		super(adminAreaId);
	}

	/**
	 * Method for Creates the project start app rel.
	 *
	 * @param projectStartAppRelRequest            {@link ProjectStartAppRelRequest}
	 * @return the project start app rel tbl {@link ProjectStartAppRelTbl}
	 * @throws UnauthorizedAccessException the unauthorized access exception
	 * @throws CannotCreateObjectException the cannot create object exception
	 */
	public ProjectStartAppRelTbl createProjectStartAppRel(ProjectStartAppRelRequest projectStartAppRelRequest)
			throws UnauthorizedAccessException, CannotCreateObjectException {
		try {
			String url = new String(this.serviceUrl + XMAdminRestClientConstants.CREATE_PROJECT_START_APP_REL);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<ProjectStartAppRelRequest> request = new HttpEntity<ProjectStartAppRelRequest>(
					projectStartAppRelRequest, this.headers);
			ResponseEntity<ProjectStartAppRelTbl> responseEntity = restTemplate.postForEntity(url, request,
					ProjectStartAppRelTbl.class);
			HttpStatus statusCode = responseEntity.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				ProjectStartAppRelTbl siteAdminAreaRelTbl = responseEntity.getBody();
				return siteAdminAreaRelTbl;
			} else {
				LOGGER.error(
						"Error while calling XMSYSTEM Create Project Start Application Relation REST Service, returns the status code: ", //$NON-NLS-1$
						statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException) e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			} else if (e instanceof HttpStatusCodeException) {
				String errorResponse = ((HttpStatusCodeException) e).getResponseBodyAsString();
				String errCode = RestClientExpHandlerUtil.handleCannotCreateObjectException(LOGGER,
						(HttpStatusCodeException) e);
				throw new CannotCreateObjectException(errorResponse, errCode);
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM Create Project Start Application Relation REST Service", e); //$NON-NLS-1$
		}
		return null;
	}

	/**
	 * Method for Creates the project start app rel.
	 *
	 * @param projectStartAppRelBatchRequest            {@link ProjectStartAppRelBatchRequest}
	 * @return the project start app rel batch response
	 *         {@link ProjectStartAppRelBatchResponse}
	 * @throws UnauthorizedAccessException the unauthorized access exception
	 * @throws CannotCreateObjectException the cannot create object exception
	 */
	public ProjectStartAppRelBatchResponse createProjectStartAppRel(
			ProjectStartAppRelBatchRequest projectStartAppRelBatchRequest)
			throws UnauthorizedAccessException, CannotCreateObjectException {
		try {
			String url = new String(this.serviceUrl + XMAdminRestClientConstants.CREATE_MULTI_START_APP_PROJECT_REL);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<ProjectStartAppRelBatchRequest> request = new HttpEntity<ProjectStartAppRelBatchRequest>(
					projectStartAppRelBatchRequest, this.headers);
			ResponseEntity<ProjectStartAppRelBatchResponse> responseEntity = restTemplate.postForEntity(url, request,
					ProjectStartAppRelBatchResponse.class);
			HttpStatus statusCode = responseEntity.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				ProjectStartAppRelBatchResponse projectStartAppRelBatchResponse = responseEntity.getBody();
				return projectStartAppRelBatchResponse;
			} else {
				LOGGER.error(
						"Error while calling XMSYSTEM Create Multi Project Start Application Relation REST Service, returns the status code: ", //$NON-NLS-1$
						statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException) e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			} else if (e instanceof HttpStatusCodeException) {
				String errorResponse = ((HttpStatusCodeException) e).getResponseBodyAsString();
				String errCode = RestClientExpHandlerUtil.handleCannotCreateObjectException(LOGGER,
						(HttpStatusCodeException) e);
				throw new CannotCreateObjectException(errorResponse, errCode);
			} else if (e instanceof ResourceAccessException) {
				throw e;
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM Create Multi Project Start Application Relation REST Service", //$NON-NLS-1$
					e);
		}
		return null;
	}

	/**
	 * Gets the all project start app rel.
	 *
	 * @param isSorted            {@link boolean}
	 * @return the all project start app rel
	 * @throws UnauthorizedAccessException the unauthorized access exception
	 */
	public List<ProjectStartAppRelTbl> getAllProjectStartAppRel(final boolean isSorted)
			throws UnauthorizedAccessException {
		try {
			String url = new String(this.serviceUrl + XMAdminRestClientConstants.GET_PROJECT_START_APP_REL);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<Object> requestEntity = new HttpEntity<Object>(headers);

			List<HttpMessageConverter<?>> messageConverters = new ArrayList<>();
			messageConverters.add(new MappingJackson2HttpMessageConverter());
			restTemplate.setMessageConverters(messageConverters);

			ResponseEntity<ProjectStartAppRelResponse> responseList = restTemplate.exchange(url, HttpMethod.GET,
					requestEntity, ProjectStartAppRelResponse.class);
			HttpStatus statusCode = responseList.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				ProjectStartAppRelResponse projectStartAppRelResponse = responseList.getBody();
				List<ProjectStartAppRelTbl> projectStartAppRelTblList = (List<ProjectStartAppRelTbl>) projectStartAppRelResponse
						.getProjectStartAppRelTbls();
				/*
				 * if (isSorted) { siteTblList.sort((siteResponse1,
				 * siteResponse2) -> {
				 * Collection<com.magna.xmbackend.entities.SiteTranslationTbl>
				 * siteTranslation1;
				 * Collection<com.magna.xmbackend.entities.SiteTranslationTbl>
				 * siteTranslation2; if ((siteTranslation1 =
				 * siteResponse1.getSiteTranslationTblCollection()) != null &&
				 * (siteTranslation2 =
				 * siteResponse2.getSiteTranslationTblCollection()) != null) {
				 * String siteName1 = getNameByLanguage(LANG_ENUM.ENGLISH,
				 * siteTranslation1); String siteName2 =
				 * getNameByLanguage(LANG_ENUM.ENGLISH, siteTranslation2);
				 * return siteName1.compareToIgnoreCase(siteName2); } return 0;
				 * }); }
				 */
				return projectStartAppRelTblList;
			} else {
				LOGGER.error(
						"Error while calling XMSYSTEM Get All Project Start Application Relation REST Service, returns the status code: " //$NON-NLS-1$
								+ statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException) e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			} else if (e instanceof HttpStatusCodeException) {
				RestClientExpHandlerUtil.handleNoXMLObjectFoundException(LOGGER, (HttpStatusCodeException) e);
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM Get All Project Start Application Relation REST Service", e); //$NON-NLS-1$
		}
		return null;
	}

	/*
	 * private String getNameByLanguage(final LANG_ENUM langEnum, final
	 * Collection<SiteTranslationTbl> siteTranslations) { for
	 * (SiteTranslationTbl siteTranslation : siteTranslations) { LANG_ENUM
	 * foundLangEnum =
	 * LANG_ENUM.getLangEnum(siteTranslation.getLanguageCode().getLanguageCode()
	 * ); String name; if (foundLangEnum == langEnum && (name =
	 * siteTranslation.getName()) != null) { if (!XMSystemUtil.isEmpty(name)) {
	 * return name; } } } return ""; }
	 */

	/**
	 * Method for Delete project start app rel.
	 *
	 * @param id            {@link String}
	 * @return true, if successful
	 * @throws UnauthorizedAccessException the unauthorized access exception
	 */
	public boolean deleteProjectStartAppRel(String id) throws UnauthorizedAccessException {
		try {
			String url = new String(
					this.serviceUrl + XMAdminRestClientConstants.DELETE_PROJECT_START_APP_REL + "/" + id);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<Object> requestEntity = new HttpEntity<Object>(this.headers);
			ResponseEntity<Boolean> responseEntity = restTemplate.exchange(url, HttpMethod.DELETE, requestEntity,
					Boolean.class);
			HttpStatus statusCode = responseEntity.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				return responseEntity.getBody();
			} else {
				LOGGER.error(
						"Error while calling XMSYSTEM delete Project Start Application Relation REST Service, returns the status code: ", //$NON-NLS-1$
						statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException) e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM delete Project Start Application Relation REST Service!", e); //$NON-NLS-1$
		}
		return false;
	}
	
	/**
	 * Delete multi project start app rel.
	 *
	 * @param ids the ids
	 * @return the project start app rel response
	 * @throws UnauthorizedAccessException the unauthorized access exception
	 */
	public ProjectStartAppRelResponse deleteMultiProjectStartAppRel(Set<String> ids)
			throws UnauthorizedAccessException {
		try {
			String url = new String(this.serviceUrl + XMAdminRestClientConstants.DELETE_MULTI_PROJECT_START_APP_REL);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<Set<String>> requestEntity = new HttpEntity<Set<String>>(ids, this.headers);
			ResponseEntity<ProjectStartAppRelResponse> responseEntity = restTemplate.exchange(url, HttpMethod.DELETE,
					requestEntity, ProjectStartAppRelResponse.class);
			HttpStatus statusCode = responseEntity.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				ProjectStartAppRelResponse projectStartAppRelResponse = responseEntity.getBody();
				return projectStartAppRelResponse;
			} else {
				LOGGER.error(
						"Error while calling XMSYSTEM delete Multi ProjectStartAppRel REST Service, returns the status code: ", //$NON-NLS-1$
						statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException) e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM delete Multi ProjectStartAppRel REST Service!", e); //$NON-NLS-1$
		}
		return null;
	}

	/**
	 * Method for Update project start app rel status.
	 *
	 * @param id            {@link String}
	 * @param status            {@link String}
	 * @return true, if successful
	 * @throws UnauthorizedAccessException the unauthorized access exception
	 */
	public boolean updateProjectStartAppRelStatus(String id, String status) throws UnauthorizedAccessException {
		try {
			String url = new String(this.serviceUrl + XMAdminRestClientConstants.UPDATE_PROJECT_START_APP_REL_STATUS
					+ "/" + status + "/" + id);

			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<Object> requestEntity = new HttpEntity<Object>(this.headers);
			ResponseEntity<Boolean> responseEntity = restTemplate.exchange(url, HttpMethod.POST, requestEntity,
					Boolean.class);
			HttpStatus statusCode = responseEntity.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				return responseEntity.getBody();
			} else {
				LOGGER.error(
						"Error while calling XMSYSTEM Update Project Start Application Relation Status REST Service, returns the status code: ", //$NON-NLS-1$
						statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException) e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM Update Project Start Application Relation Status REST Service!", //$NON-NLS-1$
					e);
		}
		return false;
	}


	/**
	 * Multi update project start app rel status.
	 *
	 * @param projectStartAppRelRequest the project start app rel request
	 * @return the project start app rel batch response
	 * @throws UnauthorizedAccessException the unauthorized access exception
	 */
	public ProjectStartAppRelBatchResponse multiUpdateProjectStartAppRelStatus(
			List<ProjectStartAppRelRequest> projectStartAppRelRequest) throws UnauthorizedAccessException {
		try {
			String url = new String(this.serviceUrl + XMAdminRestClientConstants.MULTI_UPDATE_PROJECT_START_APP_STATUS);

			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<List<ProjectStartAppRelRequest>> requestEntity = new HttpEntity<List<ProjectStartAppRelRequest>>(
					projectStartAppRelRequest, this.headers);
			ResponseEntity<ProjectStartAppRelBatchResponse> responseEntity = restTemplate.exchange(url, HttpMethod.POST,
					requestEntity, ProjectStartAppRelBatchResponse.class);
			HttpStatus statusCode = responseEntity.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				return responseEntity.getBody();
			} else {
				LOGGER.error(
						"Error while calling XMSYSTEM Multi Update ProjectStartApp Status REST Service, returns the status code: ", //$NON-NLS-1$
						statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException) e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM Multi Update ProjectStartApp Status REST Service!", e); //$NON-NLS-1$
		}
		return null;
	}
	
	/**
	 * Gets the project start app by project site AA id.
	 *
	 * @param siteId            {@link String}
	 * @param adminAreaId            {@link String}
	 * @param projectId            {@link String}
	 * @return the project start app by project site AA id
	 * @throws UnauthorizedAccessException the unauthorized access exception
	 */
	public ProjectStartAppRelResponse getProjectStartAppByProjectSiteAAId(String siteId, String adminAreaId,
			String projectId) throws UnauthorizedAccessException {
		try {
			String url = new String(
					this.serviceUrl + XMAdminRestClientConstants.GET_PROJECT_START_APP_BY_PROJECT_SITE_ADMIN_AREA_ID
							+ "/" + siteId + "/" + adminAreaId + "/" + projectId);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<Object> requestEntity = new HttpEntity<Object>(headers);

			List<HttpMessageConverter<?>> messageConverters = new ArrayList<>();
			messageConverters.add(new MappingJackson2HttpMessageConverter());
			restTemplate.setMessageConverters(messageConverters);

			ResponseEntity<ProjectStartAppRelResponse> responseList = restTemplate.exchange(url, HttpMethod.GET,
					requestEntity, ProjectStartAppRelResponse.class);
			HttpStatus statusCode = responseList.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				ProjectStartAppRelResponse projectStartAppRelResponse = responseList.getBody();
				return projectStartAppRelResponse;
			} else {
				LOGGER.error(
						"Error while calling XMSYSTEM Get Project Start Application By ProjectId, SiteId and AdminArea Id REST Service, returns the status code: " //$NON-NLS-1$
								+ statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException) e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			} else if (e instanceof HttpStatusCodeException) {
				RestClientExpHandlerUtil.handleNoXMLObjectFoundException(LOGGER, (HttpStatusCodeException) e);
			} else if(e instanceof ResourceAccessException){
				throw e;
			}
		} catch (Exception e) {
			LOGGER.error(
					"Error while calling XMSYSTEM Get All Sites By ProjectId, SiteId and AdminArea Id REST Service", e); //$NON-NLS-1$
		}
		return null;
	}

	/**
	 * Gets the all project start app by id.
	 *
	 * @param projectStartAppRelId            {@link String}
	 * @return the all project start app by id
	 * @throws UnauthorizedAccessException the unauthorized access exception
	 */
	public ProjectStartAppRelTbl getAllProjectStartAppById(String projectStartAppRelId)
			throws UnauthorizedAccessException {
		try {
			String url = new String(this.serviceUrl + XMAdminRestClientConstants.GET_PROJECT_START_APP_REL_BY_ID + "/"
					+ projectStartAppRelId);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<Object> requestEntity = new HttpEntity<Object>(headers);

			List<HttpMessageConverter<?>> messageConverters = new ArrayList<>();
			messageConverters.add(new MappingJackson2HttpMessageConverter());
			restTemplate.setMessageConverters(messageConverters);

			ResponseEntity<ProjectStartAppRelTbl> responseList = restTemplate.exchange(url, HttpMethod.GET,
					requestEntity, ProjectStartAppRelTbl.class);
			HttpStatus statusCode = responseList.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				ProjectStartAppRelTbl projectStartAppRelTbl = responseList.getBody();
				return projectStartAppRelTbl;
			} else {
				LOGGER.error(
						"Error while calling XMSYSTEM Get Project Start App Relation by Id REST Service, returns the status code: " //$NON-NLS-1$
								+ statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException) e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			} else if (e instanceof HttpStatusCodeException) {
				RestClientExpHandlerUtil.handleNoXMLObjectFoundException(LOGGER, (HttpStatusCodeException) e);
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM GetProject Start App Relation by Id REST Service", e); //$NON-NLS-1$
		}
		return null;
	}

	/**
	 * Gets the start app by AA proj rel id.
	 *
	 * @param adminAreaProjectRelId            {@link String}
	 * @return the start app by AA proj rel id
	 * @throws UnauthorizedAccessException the unauthorized access exception
	 */
	public ProjectStartAppRelResponse getStartAppByAAProjRelId(String adminAreaProjectRelId)
			throws UnauthorizedAccessException {
		try {
			String url = new String(this.serviceUrl + XMAdminRestClientConstants.GET_START_APP_BY_AA_PROJ_REL_ID + "/"
					+ adminAreaProjectRelId);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<Object> requestEntity = new HttpEntity<Object>(headers);

			List<HttpMessageConverter<?>> messageConverters = new ArrayList<>();
			messageConverters.add(new MappingJackson2HttpMessageConverter());
			restTemplate.setMessageConverters(messageConverters);

			ResponseEntity<ProjectStartAppRelResponse> responseList = restTemplate.exchange(url, HttpMethod.GET,
					requestEntity, ProjectStartAppRelResponse.class);
			HttpStatus statusCode = responseList.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				ProjectStartAppRelResponse projectStartAppRelResponse = responseList.getBody();
				return projectStartAppRelResponse;
			} else {
				LOGGER.error(
						"Error while calling XMSYSTEM Get Project Start App Relation by adminAreaProjectRelId REST Service, returns the status code: " //$NON-NLS-1$
								+ statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException) e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			} else if (e instanceof HttpStatusCodeException) {
				RestClientExpHandlerUtil.handleNoXMLObjectFoundException(LOGGER, (HttpStatusCodeException) e);
			}
		} catch (Exception e) {
			LOGGER.error(
					"Error while calling XMSYSTEM GetProject Start App Relation by adminAreaProjectRelId REST Service", //$NON-NLS-1$
					e);
		}
		return null;
	}
}
