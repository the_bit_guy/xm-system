package com.magna.xmsystem.xmadmin.restclient.relation;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import com.magna.xmbackend.entities.UserUserAppRelTbl;
import com.magna.xmbackend.exception.CannotCreateObjectException;
import com.magna.xmbackend.response.rel.useruserapp.UserUserAppRelResponseWrapper;
import com.magna.xmbackend.response.rel.useruserapp.UserUserAppRelation;
import com.magna.xmbackend.vo.rel.UserUserAppRelBatchRequest;
import com.magna.xmbackend.vo.rel.UserUserAppRelBatchResponse;
import com.magna.xmbackend.vo.rel.UserUserAppRelRequest;
import com.magna.xmbackend.vo.rel.UserUserAppRelResponse;
import com.magna.xmsystem.dependencies.customExceptions.UnauthorizedAccessException;
import com.magna.xmsystem.xmadmin.restclient.RestController;
import com.magna.xmsystem.xmadmin.restclient.util.RestClientExpHandlerUtil;
import com.magna.xmsystem.xmadmin.restclient.util.RestClientSortingUtil;
import com.magna.xmsystem.xmadmin.restclient.util.XMAdminRestClientConstants;

public class UserUserAppRelController extends RestController {
	/** The Constant LOGGER. */
	private static final Logger LOGGER = LoggerFactory.getLogger(UserUserAppRelController.class);

	/**
	 * Constructor for SiteController Class.
	 */
	public UserUserAppRelController() {
		super();
	}
	
	/**
	 * Instantiates a new user user app rel controller.
	 *
	 * @param adminAreaId the admin area id
	 */
	public UserUserAppRelController(final String adminAreaId) {
		super(adminAreaId);
	}

	/**
	 * Method for Creates the user user app rel.
	 *
	 * @param userUserAppRelRequest
	 *            {@link UserUserAppRelRequest}
	 * @return the user user app rel tbl {@link UserUserAppRelTbl}
	 */
	public UserUserAppRelTbl createUserUserAppRel(UserUserAppRelRequest userUserAppRelRequest)
			throws UnauthorizedAccessException, CannotCreateObjectException {
		try {
			String url = new String(this.serviceUrl + XMAdminRestClientConstants.CREATE_USER_APP_USER_REL);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<UserUserAppRelRequest> request = new HttpEntity<UserUserAppRelRequest>(userUserAppRelRequest,
					this.headers);
			ResponseEntity<UserUserAppRelTbl> responseEntity = restTemplate.postForEntity(url, request,
					UserUserAppRelTbl.class);
			HttpStatus statusCode = responseEntity.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				UserUserAppRelTbl userStartAppRelTbl = responseEntity.getBody();
				return userStartAppRelTbl;
			} else {
				LOGGER.error(
						"Error while calling XMSYSTEM Create User User Application Relation REST Service, returns the status code: ", //$NON-NLS-1$
						statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException) e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			} else if (e instanceof HttpStatusCodeException) {
				String errorResponse = ((HttpStatusCodeException) e).getResponseBodyAsString();
				String errCode = RestClientExpHandlerUtil.handleCannotCreateObjectException(LOGGER,
						(HttpStatusCodeException) e);
				throw new CannotCreateObjectException(errorResponse, errCode);
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM Create User User Application Relation REST Service", e); //$NON-NLS-1$
		}
		return null;
	}

	/**
	 * Method for Creates the user user app rel.
	 *
	 * @param userUserAppRelBatchRequest
	 *            {@link UserUserAppRelBatchRequest}
	 * @return the user user app rel batch response
	 *         {@link UserUserAppRelBatchResponse}
	 */
	public UserUserAppRelBatchResponse createUserUserAppRel(UserUserAppRelBatchRequest userUserAppRelBatchRequest)
			throws UnauthorizedAccessException, CannotCreateObjectException {
		try {
			String url = new String(this.serviceUrl + XMAdminRestClientConstants.CREATE_MULTI_USER_APP_USER_REL);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<UserUserAppRelBatchRequest> request = new HttpEntity<UserUserAppRelBatchRequest>(
					userUserAppRelBatchRequest, this.headers);
			ResponseEntity<UserUserAppRelBatchResponse> responseEntity = restTemplate.postForEntity(url, request,
					UserUserAppRelBatchResponse.class);
			HttpStatus statusCode = responseEntity.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				UserUserAppRelBatchResponse userUserAppRelBatchResponse = responseEntity.getBody();
				return userUserAppRelBatchResponse;
			} else {
				LOGGER.error(
						"Error while calling XMSYSTEM Create Multi User Application to User Relation REST Service, returns the status code: ", //$NON-NLS-1$
						statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException) e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			} else if (e instanceof HttpStatusCodeException) {
				String errorResponse = ((HttpStatusCodeException) e).getResponseBodyAsString();
				String errCode = RestClientExpHandlerUtil.handleCannotCreateObjectException(LOGGER,
						(HttpStatusCodeException) e);
				throw new CannotCreateObjectException(errorResponse, errCode);
			} else if (e instanceof ResourceAccessException) {
				throw e;
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM Create Multi User Application to User Relation REST Service", e); //$NON-NLS-1$
		}
		return null;
	}

	/**
	 * Gets the user user app rels by user id site AA rel id.
	 *
	 * @param userId
	 *            {@link String}
	 * @param siteAARelId
	 *            {@link String}
	 * @return the user user app rels by user id site AA rel id
	 */
	public List<UserUserAppRelTbl> getUserUserAppRelsByUserIdSiteAARelId(String userId, String siteAARelId)
			throws UnauthorizedAccessException {
		try {
			String url = new String(
					this.serviceUrl + XMAdminRestClientConstants.GET_USER_USER_APP_REL_BY_USER_ID_SITE_AA_REL_ID + "/"
							+ userId + "/" + siteAARelId);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<Object> requestEntity = new HttpEntity<Object>(headers);

			List<HttpMessageConverter<?>> messageConverters = new ArrayList<>();
			messageConverters.add(new MappingJackson2HttpMessageConverter());
			restTemplate.setMessageConverters(messageConverters);

			ResponseEntity<UserUserAppRelResponse> responseList = restTemplate.exchange(url, HttpMethod.GET,
					requestEntity, UserUserAppRelResponse.class);
			HttpStatus statusCode = responseList.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				UserUserAppRelResponse userUserAppRelResponse = responseList.getBody();
				Iterable<UserUserAppRelTbl> userUserAppRelTbls = userUserAppRelResponse.getUserUserAppRelTbls();
				List<UserUserAppRelTbl> userUserAppRelTblsList = new ArrayList<>();
				for (UserUserAppRelTbl userUserAppRelTbl : userUserAppRelTbls) {
					userUserAppRelTblsList.add(userUserAppRelTbl);
				}
				return RestClientSortingUtil.sortUserUserAppRelTbl(userUserAppRelTblsList);
			} else {
				LOGGER.error(
						"Error while calling XMSYSTEM Get User User Application Rel By User Id REST Service, returns the status code: " //$NON-NLS-1$
								+ statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException) e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			} else if (e instanceof HttpStatusCodeException) {
				RestClientExpHandlerUtil.handleNoXMLObjectFoundException(LOGGER, (HttpStatusCodeException) e);
			} else if(e instanceof ResourceAccessException){
				throw e;
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM Get User User Appliocation Rel By User Id REST Service", e); //$NON-NLS-1$
		}
		return null;
	}

	/**
	 * Gets the user user app rels by user id site AA rel id rel type.
	 *
	 * @param userId
	 *            {@link String}
	 * @param siteAARelId
	 *            {@link String}
	 * @param relationType
	 *            {@link String}
	 * @return the user user app rels by user id site AA rel id rel type
	 */
	public List<UserUserAppRelTbl> getUserUserAppRelsByUserIdSiteAARelIdRelType(String userId, String siteAARelId,
			String relationType) throws UnauthorizedAccessException {
		try {
			String url = new String(this.serviceUrl
					+ XMAdminRestClientConstants.GET_USER_USER_APP_REL_BY_USER_ID_SITE_AA_REL_ID_REL_TYPE + "/" + userId
					+ "/" + siteAARelId + "/" + relationType);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<Object> requestEntity = new HttpEntity<Object>(headers);

			List<HttpMessageConverter<?>> messageConverters = new ArrayList<>();
			messageConverters.add(new MappingJackson2HttpMessageConverter());
			restTemplate.setMessageConverters(messageConverters);

			ResponseEntity<UserUserAppRelResponse> responseList = restTemplate.exchange(url, HttpMethod.GET,
					requestEntity, UserUserAppRelResponse.class);
			HttpStatus statusCode = responseList.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				UserUserAppRelResponse userUserAppRelResponse = responseList.getBody();
				Iterable<UserUserAppRelTbl> userUserAppRelTbls = userUserAppRelResponse.getUserUserAppRelTbls();
				List<UserUserAppRelTbl> userUserAppRelTblsList = new ArrayList<>();
				for (UserUserAppRelTbl userUserAppRelTbl : userUserAppRelTbls) {
					userUserAppRelTblsList.add(userUserAppRelTbl);
				}
				return RestClientSortingUtil.sortUserUserAppRelTbl(userUserAppRelTblsList);
			} else {
				LOGGER.error(
						"Error while calling XMSYSTEM Get User User Application Rel By User Id and Relation Type REST Service, returns the status code: " //$NON-NLS-1$
								+ statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException) e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			} else if (e instanceof HttpStatusCodeException) {
				RestClientExpHandlerUtil.handleNoXMLObjectFoundException(LOGGER, (HttpStatusCodeException) e);
			} else if(e instanceof ResourceAccessException){
				throw e;
			}
		} catch (Exception e) {
			LOGGER.error(
					"Error while calling XMSYSTEM Get User User Appliocation Rel By User Id and Relation Type REST Service", //$NON-NLS-1$
					e);
		}
		return null;
	}

	/**
	 * Gets the user user app rels by user app id.
	 *
	 * @param userAppId
	 *            {@link String}
	 * @return the user user app rels by user app id
	 */
	public UserUserAppRelResponseWrapper getUserUserAppRelsByUserAppId(String userAppId)
			throws UnauthorizedAccessException {
		try {
			String url = new String(this.serviceUrl + XMAdminRestClientConstants.GET_USER_USER_APP_REL_BY_USER_APP_ID
					+ "/" + userAppId);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<Object> requestEntity = new HttpEntity<Object>(headers);

			List<HttpMessageConverter<?>> messageConverters = new ArrayList<>();
			messageConverters.add(new MappingJackson2HttpMessageConverter());
			restTemplate.setMessageConverters(messageConverters);

			ResponseEntity<UserUserAppRelResponseWrapper> responseList = restTemplate.exchange(url, HttpMethod.GET,
					requestEntity, UserUserAppRelResponseWrapper.class);
			HttpStatus statusCode = responseList.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				UserUserAppRelResponseWrapper userUserAppRelResponseWrapper = responseList.getBody();
				List<UserUserAppRelation> userUserAppRelations = userUserAppRelResponseWrapper
						.getUserUserAppRelations();
				RestClientSortingUtil.sortUserUserAppTbl(userUserAppRelations);
				return userUserAppRelResponseWrapper;
			} else {
				LOGGER.error(
						"Error while calling XMSYSTEM Get User User Application Rel By userAppId REST Service, returns the status code: " //$NON-NLS-1$
								+ statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException) e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			} else if (e instanceof HttpStatusCodeException) {
				RestClientExpHandlerUtil.handleNoXMLObjectFoundException(LOGGER, (HttpStatusCodeException) e);
			} else if(e instanceof ResourceAccessException){
				throw e;
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM Get User User Appliocation Rel By userAppId REST Service", e); //$NON-NLS-1$
		}
		return null;
	}
	/**
	 * Gets the user user app rels by user id and aa id.
	 *
	 * @param userId
	 *            {@link String}
	 * @param aaId
	 *            {@link String}
	 * 
	 * @return the user user app rels by user id and aa id
	 */
	public UserUserAppRelResponseWrapper getUserUserAppRelsByUserIdAAId(final String userId, final String aaId)
			throws UnauthorizedAccessException {
		try {
			String url = new String(this.serviceUrl + XMAdminRestClientConstants.GET_USER_USER_APP_REL_BY_USER_ID_AA_ID
					+ "/" + userId + "/" + aaId);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<Object> requestEntity = new HttpEntity<Object>(headers);

			List<HttpMessageConverter<?>> messageConverters = new ArrayList<>();
			messageConverters.add(new MappingJackson2HttpMessageConverter());
			restTemplate.setMessageConverters(messageConverters);

			ResponseEntity<UserUserAppRelResponseWrapper> responseList = restTemplate.exchange(url, HttpMethod.GET,
					requestEntity, UserUserAppRelResponseWrapper.class);
			HttpStatus statusCode = responseList.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				UserUserAppRelResponseWrapper userUserAppRelResponseWrapper = responseList.getBody();
				return userUserAppRelResponseWrapper;
			} else {
				LOGGER.error(
						"Error while calling XMSYSTEM Get User User Application Rel By userId and aaId REST Service, returns the status code: " //$NON-NLS-1$
								+ statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException) e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			} else if (e instanceof HttpStatusCodeException) {
				RestClientExpHandlerUtil.handleNoXMLObjectFoundException(LOGGER, (HttpStatusCodeException) e);
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM Get User User Appliocation Rel By userId and aaId REST Service", //$NON-NLS-1$
					e);
		}
		return null;
	}

	/**
	 * Delete UserUserAppRel
	 * 
	 * @param id
	 * @return boolean
	 */
	public boolean deleteUserUserAppRel(String id) throws UnauthorizedAccessException {
		try {
			String url = new String(this.serviceUrl + XMAdminRestClientConstants.DELETE_USER_USER_APP_REL_BY_ID + "/" + id);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<Object> requestEntity = new HttpEntity<Object>(this.headers);
			ResponseEntity<Boolean> responseEntity = restTemplate.exchange(url, HttpMethod.DELETE, requestEntity,
					Boolean.class);
			HttpStatus statusCode = responseEntity.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				return responseEntity.getBody();
			} else {
				LOGGER.error("Error while calling XMSYSTEM delete UserUserAppRel REST Service, returns the status code: ", //$NON-NLS-1$
						statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException) e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM delete UserUserAppRel REST Service!", e); //$NON-NLS-1$
		}
		return false;
	}

	/**
	 * Delete multi user user app rel.
	 *
	 * @param ids the ids
	 * @return the user user app rel response
	 * @throws UnauthorizedAccessException the unauthorized access exception
	 */
	public UserUserAppRelResponse deleteMultiUserUserAppRel(Set<String> ids) throws UnauthorizedAccessException {
		try {
			String url = new String(this.serviceUrl + XMAdminRestClientConstants.DELETE_MULTI_USER_USER_APP_REL_BY_ID);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<Set<String>> requestEntity = new HttpEntity<Set<String>>(ids, this.headers);
			ResponseEntity<UserUserAppRelResponse> responseEntity = restTemplate.exchange(url, HttpMethod.DELETE,
					requestEntity, UserUserAppRelResponse.class);
			HttpStatus statusCode = responseEntity.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				UserUserAppRelResponse userUserAppRelResponse = responseEntity.getBody();
				return userUserAppRelResponse;
			} else {
				LOGGER.error(
						"Error while calling XMSYSTEM delete Multi UserUserAppRel REST Service, returns the status code: ", //$NON-NLS-1$
						statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException) e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM delete Multi UserUserAppRel REST Service!", e); //$NON-NLS-1$
		}
		return null;
	}
	
	/**
	 * Method for Update user rel types by ids.
	 *
	 * @param userUserAppRelBatchRequest
	 *            {@link UserUserAppRelBatchRequest}
	 * @return the user user app rel batch response
	 *         {@link UserUserAppRelBatchResponse}
	 */
	public UserUserAppRelBatchResponse updateUserRelTypesByIds(UserUserAppRelBatchRequest userUserAppRelBatchRequest) {
		try {
			String url = new String(this.serviceUrl + XMAdminRestClientConstants.UPDATE_USER_APP_USER_REL_TYPES_BY_IDS);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<UserUserAppRelBatchRequest> request = new HttpEntity<UserUserAppRelBatchRequest>(
					userUserAppRelBatchRequest, this.headers);
			ResponseEntity<UserUserAppRelBatchResponse> responseEntity = restTemplate.postForEntity(url, request,
					UserUserAppRelBatchResponse.class);
			HttpStatus statusCode = responseEntity.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				UserUserAppRelBatchResponse userAppRelBatchResponse = responseEntity.getBody();
				return userAppRelBatchResponse;
			} else {
				LOGGER.error(
						"Error while calling XMSYSTEM Update User User Application Relation Types By IDs REST Service, returns the status code: ", //$NON-NLS-1$
						statusCode.value());
			}
		} catch (Exception e) {
			  if (e instanceof ResourceAccessException) {
					throw e;
				}
			LOGGER.error("Error while calling XMSYSTEM Update User User Application Relation Types By IDs REST Service", //$NON-NLS-1$
					e);
		}
		return null;
	}
}
