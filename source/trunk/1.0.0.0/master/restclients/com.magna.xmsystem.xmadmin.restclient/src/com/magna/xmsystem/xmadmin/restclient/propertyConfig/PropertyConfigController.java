package com.magna.xmsystem.xmadmin.restclient.propertyConfig;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import com.magna.xmbackend.entities.PropertyConfigTbl;
import com.magna.xmbackend.exception.CannotCreateObjectException;
import com.magna.xmbackend.vo.propConfig.LdapConfigRequest;
import com.magna.xmbackend.vo.propConfig.ProjectExpiryConfigRequest;
import com.magna.xmbackend.vo.propConfig.ProjectPopupConfigRequest;
import com.magna.xmbackend.vo.propConfig.PropertyConfigResponse;
import com.magna.xmbackend.vo.propConfig.SingletonConfigRequest;
import com.magna.xmbackend.vo.propConfig.SmtpConfigRequest;
import com.magna.xmsystem.dependencies.customExceptions.UnauthorizedAccessException;
import com.magna.xmsystem.xmadmin.restclient.RestController;
import com.magna.xmsystem.xmadmin.restclient.util.RestClientExpHandlerUtil;
import com.magna.xmsystem.xmadmin.restclient.util.XMAdminRestClientConstants;

/**
 * The Class PropertyConfigController.
 * 
 *  @author Deepak Upadhyay
 */
public class PropertyConfigController extends RestController {

	/** The Constant LOGGER. */
	private static final Logger LOGGER = LoggerFactory.getLogger(PropertyConfigController.class);

	/**
	 * Instantiates a new property config controller.
	 */
	public PropertyConfigController() {
		super();
	}

	/**
	 * Gets the all SMTP details.
	 *
	 * @param isSorted
	 *            the is sorted
	 * @return the all SMTP details
	 */
	public List<PropertyConfigTbl> getAllSMTPDetails(final boolean isSorted) throws UnauthorizedAccessException {
		try {
			final String url = new String(this.serviceUrl + XMAdminRestClientConstants.GET_ALL_SMTP);
			final RestTemplate restTemplate = new RestTemplate();
			final HttpEntity<Object> requestEntity = new HttpEntity<Object>(headers);

			List<HttpMessageConverter<?>> messageConverters = new ArrayList<>();
			messageConverters.add(new MappingJackson2HttpMessageConverter());
			restTemplate.setMessageConverters(messageConverters);

			ResponseEntity<PropertyConfigResponse> responseList = restTemplate.exchange(url, HttpMethod.GET,
					requestEntity, PropertyConfigResponse.class);
			HttpStatus statusCode = responseList.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				PropertyConfigResponse smtpResponseObject = responseList.getBody();
				List<PropertyConfigTbl> smptTblList = (List<PropertyConfigTbl>) smtpResponseObject
						.getPropertyConfigTbls();

				return smptTblList;
			} else {
				LOGGER.error("Error while calling XMSYSTEM Get All SMTP Details REST Service, returns the status code: " //$NON-NLS-1$
						+ statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException)e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			} else if (e instanceof HttpStatusCodeException) {
				RestClientExpHandlerUtil.handleNoXMLObjectFoundException(LOGGER, (HttpStatusCodeException) e);
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM Get All SMTP Details REST Service", e); //$NON-NLS-1$
		}
		return null;
	}

	/**
	 * Update property value.
	 *
	 * @param smtpConfigRequest the smtp config request
	 * @return true, if successful
	 * @throws CannotCreateObjectException the cannot create object exception
	 * @throws UnauthorizedAccessException the unauthorized access exception
	 */
	public boolean updatePropertyValue(com.magna.xmbackend.vo.propConfig.SmtpConfigRequest smtpConfigRequest) throws CannotCreateObjectException, UnauthorizedAccessException {
		try {
			final String url = new String(this.serviceUrl + XMAdminRestClientConstants.UPDATE_SMTP_DETAILS);
			final RestTemplate restTemplate = new RestTemplate();
			final HttpEntity<SmtpConfigRequest> request = new HttpEntity<SmtpConfigRequest>(smtpConfigRequest, this.headers);
			final ResponseEntity<PropertyConfigResponse> responseEntity = restTemplate.exchange(url, HttpMethod.PUT, request,
					PropertyConfigResponse.class);
			final HttpStatus statusCode = responseEntity.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				return true;
			} else {
				LOGGER.error("Error while calling XMSYSTEM Upadte SMTP REST Service, returns the status code: ", //$NON-NLS-1$
						statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException)e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			} else if (e instanceof HttpStatusCodeException) {
				String errorResponse = ((HttpStatusCodeException) e).getResponseBodyAsString();
				String errCode = RestClientExpHandlerUtil.handleCannotCreateObjectException(LOGGER, (HttpStatusCodeException) e);
				throw new CannotCreateObjectException(errorResponse, errCode);
			}else if (e instanceof ResourceAccessException) {
				throw e;
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM Upadte SMTP REST Service", e); //$NON-NLS-1$
		}
		return false;
	}
	

	/**
	 * Gets the all LDAP details.
	 *
	 * @param isSorted the is sorted
	 * @return the all LDAP details
	 */
	public List<PropertyConfigTbl> getAllLDAPDetails(final boolean isSorted) throws UnauthorizedAccessException {
		try {
			String url = new String(this.serviceUrl + XMAdminRestClientConstants.GET_ALL_LDAP);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<Object> requestEntity = new HttpEntity<Object>(headers);

			List<HttpMessageConverter<?>> messageConverters = new ArrayList<>();
			messageConverters.add(new MappingJackson2HttpMessageConverter());
			restTemplate.setMessageConverters(messageConverters);

			ResponseEntity<PropertyConfigResponse> responseList = restTemplate.exchange(url, HttpMethod.GET,
					requestEntity, PropertyConfigResponse.class);
			HttpStatus statusCode = responseList.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				PropertyConfigResponse ldapResponseObject = responseList.getBody();
				List<PropertyConfigTbl> ldapTblList = (List<PropertyConfigTbl>) ldapResponseObject
						.getPropertyConfigTbls();

				return ldapTblList;
			} else {
				LOGGER.error("Error while calling XMSYSTEM Get All LDAP Details REST Service, returns the status code: " //$NON-NLS-1$
						+ statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException)e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			} else if (e instanceof HttpStatusCodeException) {
				RestClientExpHandlerUtil.handleNoXMLObjectFoundException(LOGGER, (HttpStatusCodeException) e);
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM Get All LDAP Details REST Service", e); //$NON-NLS-1$
		}
		return null;
	}
	
	/**
	 * Update property value ldap.
	 *
	 * @param ldapConfigRequest the ldap config request
	 * @return true, if successful
	 * @throws UnauthorizedAccessException the unauthorized access exception
	 * @throws CannotCreateObjectException the cannot create object exception
	 */
	public boolean updatePropertyValueLdap(final com.magna.xmbackend.vo.propConfig.LdapConfigRequest ldapConfigRequest) throws UnauthorizedAccessException, CannotCreateObjectException{
		try {
			final String url = new String(this.serviceUrl + XMAdminRestClientConstants.UPDATE_LDAP_DETAILS);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<LdapConfigRequest> request = new HttpEntity<LdapConfigRequest>(ldapConfigRequest, this.headers);
			ResponseEntity<PropertyConfigResponse> responseEntity = restTemplate.exchange(url, HttpMethod.PUT, request,PropertyConfigResponse.class);
			HttpStatus statusCode = responseEntity.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				return true;
			} else {
				LOGGER.error("Error while calling XMSYSTEM Upadte LDAP REST Service, returns the status code: ", //$NON-NLS-1$
						statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException)e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			} else if (e instanceof HttpStatusCodeException) {
				String errorResponse = ((HttpStatusCodeException) e).getResponseBodyAsString();
				String errCode = RestClientExpHandlerUtil.handleCannotCreateObjectException(LOGGER, (HttpStatusCodeException) e);
				throw new CannotCreateObjectException(errorResponse, errCode);
			}else if (e instanceof ResourceAccessException) {
				throw e;
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM Upadte LDAP REST Service", e); //$NON-NLS-1$
		}
		return false;
	}
	
	/**
	 * Gets the all singleton time.
	 *
	 * @param isSorted the is sorted
	 * @return the all singleton time
	 */
	public List<PropertyConfigTbl> getAllSingletonTime(final boolean isSorted) throws UnauthorizedAccessException {
		try {
			String url = new String(this.serviceUrl + XMAdminRestClientConstants.GET_ALL_SINGLETON);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<Object> requestEntity = new HttpEntity<Object>(headers);

			List<HttpMessageConverter<?>> messageConverters = new ArrayList<>();
			messageConverters.add(new MappingJackson2HttpMessageConverter());
			restTemplate.setMessageConverters(messageConverters);

			ResponseEntity<PropertyConfigResponse> responseList = restTemplate.exchange(url, HttpMethod.GET,
					requestEntity, PropertyConfigResponse.class);
			HttpStatus statusCode = responseList.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				PropertyConfigResponse singletonResponseObject = responseList.getBody();
				List<PropertyConfigTbl> singletonList = (List<PropertyConfigTbl>) singletonResponseObject
						.getPropertyConfigTbls();

				return singletonList;
			} else {
				LOGGER.error("Error while calling XMSYSTEM Get All LDAP Details REST Service, returns the status code: " //$NON-NLS-1$
						+ statusCode.value());
			}
			
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException)e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			} else if (e instanceof HttpStatusCodeException) {
				RestClientExpHandlerUtil.handleNoXMLObjectFoundException(LOGGER, (HttpStatusCodeException) e);
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM Get All Singleton Details REST Service", e); //$NON-NLS-1$
		}
		return null;
	}
	
	/**
	 * Update single app time.
	 *
	 * @param singletonRequest the singleton request
	 * @return true, if successful
	 */
	public boolean updateSingleAppTime(com.magna.xmbackend.vo.propConfig.SingletonConfigRequest singletonRequest) throws UnauthorizedAccessException, CannotCreateObjectException {
		try {
			String url = new String(this.serviceUrl + XMAdminRestClientConstants.UPDATE_SINGLETON_DETAILS);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<SingletonConfigRequest> request = new HttpEntity<SingletonConfigRequest>(singletonRequest, this.headers);
			ResponseEntity<PropertyConfigResponse> responseEntity = restTemplate.exchange(url, HttpMethod.PUT, request,PropertyConfigResponse.class);
			HttpStatus statusCode = responseEntity.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				return true;
			} else {
				LOGGER.error("Error while calling XMSYSTEM Upadte Singleton REST Service, returns the status code: ", //$NON-NLS-1$
						statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException)e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			} else if (e instanceof HttpStatusCodeException) {
				String errorResponse = ((HttpStatusCodeException) e).getResponseBodyAsString();
				String errCode = RestClientExpHandlerUtil.handleCannotCreateObjectException(LOGGER, (HttpStatusCodeException) e);
				throw new CannotCreateObjectException(errorResponse, errCode);
			} else if (e instanceof ResourceAccessException) {
				throw e;
			}
		}
		return false;
	}
	
	/**
	 * Gets the no project pop message.
	 *
	 * @return the no project pop message
	 * @throws UnauthorizedAccessException the unauthorized access exception
	 */
	public List<PropertyConfigTbl> getNoProjectPopMessage() throws UnauthorizedAccessException {
		try {
			String url = new String(this.serviceUrl + XMAdminRestClientConstants.GET_ALL_NO_PROJECT_POP_MESSAGE);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<Object> requestEntity = new HttpEntity<Object>(headers);

			List<HttpMessageConverter<?>> messageConverters = new ArrayList<>();
			messageConverters.add(new MappingJackson2HttpMessageConverter());
			restTemplate.setMessageConverters(messageConverters);

			ResponseEntity<PropertyConfigResponse> responseList = restTemplate.exchange(url, HttpMethod.GET,
					requestEntity, PropertyConfigResponse.class);
			HttpStatus statusCode = responseList.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				PropertyConfigResponse projectPopResponseObject = responseList.getBody();
				List<PropertyConfigTbl> projectPopList = (List<PropertyConfigTbl>) projectPopResponseObject
						.getPropertyConfigTbls();

				return projectPopList;
			} else {
				LOGGER.error("Error while calling XMSYSTEM Get All NO PROJECT Message Details REST Service, returns the status code: " //$NON-NLS-1$
						+ statusCode.value());
			}
			
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException)e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			} else if (e instanceof HttpStatusCodeException) {
				RestClientExpHandlerUtil.handleNoXMLObjectFoundException(LOGGER, (HttpStatusCodeException) e);
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM Get All NO PROJECT Message Details REST Service", e); //$NON-NLS-1$
		}
		return null;
	}
	
	/**
	 * Update no project pop message.
	 *
	 * @param projectPopRequest the project pop request
	 * @return true, if successful
	 */
	public boolean UpdateNoProjectPopMessage(final com.magna.xmbackend.vo.propConfig.ProjectPopupConfigRequest projectPopRequest) throws UnauthorizedAccessException, CannotCreateObjectException {
		try {
			String url = new String(this.serviceUrl + XMAdminRestClientConstants.UPDATE_PROJECT_POP_DETAILS);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<ProjectPopupConfigRequest> request = new HttpEntity<ProjectPopupConfigRequest>(projectPopRequest, this.headers);
			ResponseEntity<PropertyConfigResponse> responseEntity = restTemplate.exchange(url, HttpMethod.PUT, request,PropertyConfigResponse.class);
			HttpStatus statusCode = responseEntity.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				return true;
			} else {
				LOGGER.error("Error while calling XMSYSTEM Upadte No Project Pop message REST Service, returns the status code: ", //$NON-NLS-1$
						statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException)e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			} else if (e instanceof HttpStatusCodeException) {
				String errorResponse = ((HttpStatusCodeException) e).getResponseBodyAsString();
				String errCode = RestClientExpHandlerUtil.handleCannotCreateObjectException(LOGGER, (HttpStatusCodeException) e);
				throw new CannotCreateObjectException(errorResponse, errCode);
			} else if (e instanceof ResourceAccessException) {
				throw e;
			}
		}
		return false;
	}
	
	/**
	 * Gets the all project expiry.
	 *
	 * @param isSorted the is sorted
	 * @return the all project expiry
	 */
	public List<PropertyConfigTbl> getAllProjectExpiry(final boolean isSorted) {
		try {
			String url = new String(this.serviceUrl + XMAdminRestClientConstants.GET_ALL_PROJECT_EXPIRY);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<Object> requestEntity = new HttpEntity<Object>(headers);

			List<HttpMessageConverter<?>> messageConverters = new ArrayList<>();
			messageConverters.add(new MappingJackson2HttpMessageConverter());
			restTemplate.setMessageConverters(messageConverters);

			ResponseEntity<PropertyConfigResponse> responseList = restTemplate.exchange(url, HttpMethod.GET,
					requestEntity, PropertyConfigResponse.class);
			HttpStatus statusCode = responseList.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				PropertyConfigResponse projectExpiryResponseObject = responseList.getBody();
				List<PropertyConfigTbl> projectExpiryList = (List<PropertyConfigTbl>) projectExpiryResponseObject
						.getPropertyConfigTbls();

				return projectExpiryList;
			} else {
				LOGGER.error("Error while calling XMSYSTEM GET ALL PROJECT EXPIRY Message Details REST Service, returns the status code: " //$NON-NLS-1$
								+ statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException)e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			} else if (e instanceof HttpStatusCodeException) {
				RestClientExpHandlerUtil.handleNoXMLObjectFoundException(LOGGER, (HttpStatusCodeException) e);
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM GET ALL PROJECT EXPIRY Message Details REST Service", e); //$NON-NLS-1$
		}
		return null;
	}
	
	/**
	 * Update project expiry.
	 *
	 * @param projectExpiryRequest the project expiry request
	 * @return true, if successful
	 */
	public boolean UpdateProjectExpiry(com.magna.xmbackend.vo.propConfig.ProjectExpiryConfigRequest projectExpiryRequest){
		try {
			String url = new String(this.serviceUrl + XMAdminRestClientConstants.UPDATE_PROJECT_EXPIRY);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<ProjectExpiryConfigRequest> request = new HttpEntity<ProjectExpiryConfigRequest>(projectExpiryRequest, this.headers);
			ResponseEntity<PropertyConfigResponse> responseEntity = restTemplate.exchange(url, HttpMethod.PUT, request,PropertyConfigResponse.class);
			HttpStatus statusCode = responseEntity.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				return true;
			} else {
				LOGGER.error("Error while calling XMSYSTEM Upadte projectExpiry REST Service, returns the status code: ", //$NON-NLS-1$
						statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException)e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			} else if (e instanceof HttpStatusCodeException) {
				String errorResponse = ((HttpStatusCodeException) e).getResponseBodyAsString();
				String errCode = RestClientExpHandlerUtil.handleCannotCreateObjectException(LOGGER, (HttpStatusCodeException) e);
				throw new CannotCreateObjectException(errorResponse, errCode);
			} else if (e instanceof ResourceAccessException) {
				throw e;
			}
		}
		return false;
	}
}
