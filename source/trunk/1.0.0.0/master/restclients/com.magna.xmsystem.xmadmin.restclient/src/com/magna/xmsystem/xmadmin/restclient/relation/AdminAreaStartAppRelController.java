package com.magna.xmsystem.xmadmin.restclient.relation;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import com.magna.xmbackend.entities.AdminAreaStartAppRelTbl;
import com.magna.xmbackend.exception.CannotCreateObjectException;
import com.magna.xmbackend.response.rel.adminareastartapp.AdminAreaStartAppRelation;
import com.magna.xmbackend.response.rel.adminareastartapp.AdminAreaStartAppRelationWrapper;
import com.magna.xmbackend.vo.rel.AdminAreaStartAppRelBatchRequest;
import com.magna.xmbackend.vo.rel.AdminAreaStartAppRelBatchResponse;
import com.magna.xmbackend.vo.rel.AdminAreaStartAppRelRequest;
import com.magna.xmbackend.vo.rel.AdminAreaStartAppRelResponse;
import com.magna.xmsystem.dependencies.customExceptions.UnauthorizedAccessException;
import com.magna.xmsystem.xmadmin.restclient.RestController;
import com.magna.xmsystem.xmadmin.restclient.site.SiteController;
import com.magna.xmsystem.xmadmin.restclient.util.RestClientExpHandlerUtil;
import com.magna.xmsystem.xmadmin.restclient.util.RestClientSortingUtil;
import com.magna.xmsystem.xmadmin.restclient.util.XMAdminRestClientConstants;

// TODO: Auto-generated Javadoc
/**
 * Class for Site admin area rel controller.
 *
 * @author Roshan.Ekka
 */
public class AdminAreaStartAppRelController extends RestController {
	/** The Constant LOGGER. */
	private static final Logger LOGGER = LoggerFactory.getLogger(SiteController.class);

	/**
	 * Constructor for SiteController Class.
	 */
	public AdminAreaStartAppRelController() {
		super();
	}
	
	/**
	 * Constructor for AdminAreaStartAppRelController Class.
	 *
	 * @param adminAreaId {@link String}
	 */
	public AdminAreaStartAppRelController(String adminAreaId) {
		super(adminAreaId);
	}

	/**
	 * Method for Creates the admin area start app rel.
	 *
	 * @param adminAreaStartAppRelRequest            {@link AdminAreaStartAppRelRequest}
	 * @return the admin area start app rel tbl {@link AdminAreaStartAppRelTbl}
	 * @throws UnauthorizedAccessException the unauthorized access exception
	 * @throws CannotCreateObjectException the cannot create object exception
	 */
	public AdminAreaStartAppRelTbl createAdminAreaStartAppRel(AdminAreaStartAppRelRequest adminAreaStartAppRelRequest)
			throws UnauthorizedAccessException, CannotCreateObjectException {
		try {
			String url = new String(this.serviceUrl + XMAdminRestClientConstants.CREATE_ADMIN_AREA_START_APP_REL);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<AdminAreaStartAppRelRequest> request = new HttpEntity<AdminAreaStartAppRelRequest>(
					adminAreaStartAppRelRequest, this.headers);
			ResponseEntity<AdminAreaStartAppRelTbl> responseEntity = restTemplate.postForEntity(url, request,
					AdminAreaStartAppRelTbl.class);
			HttpStatus statusCode = responseEntity.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				AdminAreaStartAppRelTbl adminAreaStartAppRelTbl = responseEntity.getBody();
				return adminAreaStartAppRelTbl;
			} else {
				LOGGER.error(
						"Error while calling XMSYSTEM Create Administration Area Start Application Relation REST Service, returns the status code: ", //$NON-NLS-1$
						statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException) e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			} else if (e instanceof HttpStatusCodeException) {
				String errorResponse = ((HttpStatusCodeException) e).getResponseBodyAsString();
				String errCode = RestClientExpHandlerUtil.handleCannotCreateObjectException(LOGGER,
						(HttpStatusCodeException) e);
				throw new CannotCreateObjectException(errorResponse, errCode);
			}
		} catch (Exception e) {
			LOGGER.error(
					"Error while calling XMSYSTEM Create Administration Area Start Application Relation REST Service", //$NON-NLS-1$
					e);
		}
		return null;
	}

	/**
	 * Method for Creates the admin area start app rel.
	 *
	 * @param adminAreaStartAppRelBatchRequest            {@link AdminAreaStartAppRelBatchRequest}
	 * @return the admin area start app rel batch response
	 *         {@link AdminAreaStartAppRelBatchResponse}
	 * @throws UnauthorizedAccessException the unauthorized access exception
	 * @throws CannotCreateObjectException the cannot create object exception
	 */
	public AdminAreaStartAppRelBatchResponse createAdminAreaStartAppRel(
			AdminAreaStartAppRelBatchRequest adminAreaStartAppRelBatchRequest)
			throws UnauthorizedAccessException, CannotCreateObjectException {
		try {
			String url = new String(this.serviceUrl + XMAdminRestClientConstants.CREATE_MULTI_START_APP_ADMIN_AREA_REL);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<AdminAreaStartAppRelBatchRequest> request = new HttpEntity<AdminAreaStartAppRelBatchRequest>(
					adminAreaStartAppRelBatchRequest, this.headers);
			ResponseEntity<AdminAreaStartAppRelBatchResponse> responseEntity = restTemplate.postForEntity(url, request,
					AdminAreaStartAppRelBatchResponse.class);
			HttpStatus statusCode = responseEntity.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				AdminAreaStartAppRelBatchResponse adminAreaStartAppRelBatchResponse = responseEntity.getBody();
				return adminAreaStartAppRelBatchResponse;
			} else {
				LOGGER.error(
						"Error while calling XMSYSTEM Create MultiAdministration Area Start Application Relation REST Service, returns the status code: ", //$NON-NLS-1$
						statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException) e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			} else if (e instanceof HttpStatusCodeException) {
				String errorResponse = ((HttpStatusCodeException) e).getResponseBodyAsString();
				String errCode = RestClientExpHandlerUtil.handleCannotCreateObjectException(LOGGER,
						(HttpStatusCodeException) e);
				throw new CannotCreateObjectException(errorResponse, errCode);
			} else if (e instanceof ResourceAccessException) {
				throw e;
			}
		} catch (Exception e) {
			LOGGER.error(
					"Error while calling XMSYSTEM Create Multi Administration Area Start Application Relation REST Service", //$NON-NLS-1$
					e);
		}
		return null;
	}

	/**
	 * Gets the all admin area start app rel.
	 *
	 * @param isSorted            {@link boolean}
	 * @return the all admin area start app rel
	 * @throws UnauthorizedAccessException the unauthorized access exception
	 */
	public List<AdminAreaStartAppRelTbl> getAllAdminAreaStartAppRel(final boolean isSorted)
			throws UnauthorizedAccessException {
		try {
			String url = new String(this.serviceUrl + XMAdminRestClientConstants.GET_ADMIN_AREA_START_APP_REL);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<Object> requestEntity = new HttpEntity<Object>(headers);

			List<HttpMessageConverter<?>> messageConverters = new ArrayList<>();
			messageConverters.add(new MappingJackson2HttpMessageConverter());
			restTemplate.setMessageConverters(messageConverters);

			ResponseEntity<AdminAreaStartAppRelResponse> responseList = restTemplate.exchange(url, HttpMethod.GET,
					requestEntity, AdminAreaStartAppRelResponse.class);
			HttpStatus statusCode = responseList.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				AdminAreaStartAppRelResponse siteResponseObject = responseList.getBody();
				List<AdminAreaStartAppRelTbl> siteTblList = (List<AdminAreaStartAppRelTbl>) siteResponseObject
						.getAdminAreaStartAppRelTbls();
				/*
				 * if (isSorted) { siteTblList.sort((siteResponse1,
				 * siteResponse2) -> {
				 * Collection<com.magna.xmbackend.entities.SiteTranslationTbl>
				 * siteTranslation1;
				 * Collection<com.magna.xmbackend.entities.SiteTranslationTbl>
				 * siteTranslation2; if ((siteTranslation1 =
				 * siteResponse1.getSiteTranslationTblCollection()) != null &&
				 * (siteTranslation2 =
				 * siteResponse2.getSiteTranslationTblCollection()) != null) {
				 * String siteName1 = getNameByLanguage(LANG_ENUM.ENGLISH,
				 * siteTranslation1); String siteName2 =
				 * getNameByLanguage(LANG_ENUM.ENGLISH, siteTranslation2);
				 * return siteName1.compareToIgnoreCase(siteName2); } return 0;
				 * }); }
				 */
				return siteTblList;
			} else {
				LOGGER.error(
						"Error while calling XMSYSTEM Get All Administration Area Start Application Relation REST Service, returns the status code: " //$NON-NLS-1$
								+ statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException) e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			} else if (e instanceof HttpStatusCodeException) {
				RestClientExpHandlerUtil.handleNoXMLObjectFoundException(LOGGER, (HttpStatusCodeException) e);
			}
		} catch (Exception e) {
			LOGGER.error(
					"Error while calling XMSYSTEM Get All Administration Area Start Application Relation REST Service", //$NON-NLS-1$
					e);
		}
		return null;
	}

	/*
	 * private String getNameByLanguage(final LANG_ENUM langEnum, final
	 * Collection<SiteTranslationTbl> siteTranslations) { for
	 * (SiteTranslationTbl siteTranslation : siteTranslations) { LANG_ENUM
	 * foundLangEnum =
	 * LANG_ENUM.getLangEnum(siteTranslation.getLanguageCode().getLanguageCode()
	 * ); String name; if (foundLangEnum == langEnum && (name =
	 * siteTranslation.getName()) != null) { if (!XMSystemUtil.isEmpty(name)) {
	 * return name; } } } return ""; }
	 */

	/**
	 * Method for Delete admin area start app rel.
	 *
	 * @param adminAreaStartAppRelId            {@link String}
	 * @return true, if successful
	 * @throws UnauthorizedAccessException the unauthorized access exception
	 */
	public boolean deleteAdminAreaStartAppRel(String adminAreaStartAppRelId) throws UnauthorizedAccessException {
		try {
			String url = new String(this.serviceUrl + XMAdminRestClientConstants.DELETE_ADMIN_AREA_START_APP_REL + "/"
					+ adminAreaStartAppRelId);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<Object> requestEntity = new HttpEntity<Object>(this.headers);
			ResponseEntity<Boolean> responseEntity = restTemplate.exchange(url, HttpMethod.DELETE, requestEntity,
					Boolean.class);
			HttpStatus statusCode = responseEntity.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				return responseEntity.getBody();
			} else {
				LOGGER.error(
						"Error while calling XMSYSTEM delete Administration Area Start Application Relation REST Service, returns the status code: ", //$NON-NLS-1$
						statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException) e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			}
		} catch (Exception e) {
			LOGGER.error(
					"Error while calling XMSYSTEM delete Administration Area Start Application Relation REST Service!", //$NON-NLS-1$
					e);
		}
		return false;
	}
	
	/**
	 * Delete multi admin area start app rel.
	 *
	 * @param ids the ids
	 * @return the admin area start app rel response
	 * @throws UnauthorizedAccessException the unauthorized access exception
	 */
	public AdminAreaStartAppRelResponse deleteMultiAdminAreaStartAppRel(Set<String> ids)
			throws UnauthorizedAccessException {
		try {
			String url = new String(this.serviceUrl + XMAdminRestClientConstants.DELETE_MULTI_ADMIN_AREA_START_APP_REL);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<Set<String>> requestEntity = new HttpEntity<Set<String>>(ids, this.headers);
			ResponseEntity<AdminAreaStartAppRelResponse> responseEntity = restTemplate.exchange(url, HttpMethod.DELETE,
					requestEntity, AdminAreaStartAppRelResponse.class);
			HttpStatus statusCode = responseEntity.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				AdminAreaStartAppRelResponse adminAreaStartAppRelResponse = responseEntity.getBody();
				return adminAreaStartAppRelResponse;
			} else {
				LOGGER.error(
						"Error while calling XMSYSTEM delete Multi AdminAreaStartAppRel REST Service, returns the status code: ", //$NON-NLS-1$
						statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException) e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM delete Multi AdminAreaStartAppRel REST Service!", e); //$NON-NLS-1$
		}
		return null;
	}

	/**
	 * Method for Update admin area start app rel status.
	 *
	 * @param adminAreaStartAppRelId            {@link String}
	 * @param status            {@link String}
	 * @return true, if successful
	 * @throws UnauthorizedAccessException the unauthorized access exception
	 */
	public boolean updateAdminAreaStartAppRelStatus(String adminAreaStartAppRelId, String status)
			throws UnauthorizedAccessException {
		try {
			String url = new String(this.serviceUrl + XMAdminRestClientConstants.UPDATE_ADMIN_AREA_START_APP_REL_STATUS
					+ "/" + status + "/" + adminAreaStartAppRelId);

			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<Object> requestEntity = new HttpEntity<Object>(this.headers);
			ResponseEntity<Boolean> responseEntity = restTemplate.exchange(url, HttpMethod.POST, requestEntity,
					Boolean.class);
			HttpStatus statusCode = responseEntity.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				return responseEntity.getBody();
			} else {
				LOGGER.error(
						"Error while calling XMSYSTEM Update Administration Area Start Application Relation Status REST Service, returns the status code: ", //$NON-NLS-1$
						statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException) e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			}
		} catch (Exception e) {
			LOGGER.error(
					"Error while calling XMSYSTEM Update Administration Area Start Application Relation Status REST Service!", //$NON-NLS-1$
					e);
		}
		return false;
	}

	
	/**
	 * Multi update AA start app rel status.
	 *
	 * @param adminAreaStartAppRelRequest the admin area start app rel request
	 * @return the admin area start app rel response
	 * @throws UnauthorizedAccessException the unauthorized access exception
	 */
	public AdminAreaStartAppRelResponse multiUpdateAAStartAppRelStatus(
			List<AdminAreaStartAppRelRequest> adminAreaStartAppRelRequest) throws UnauthorizedAccessException {
		try {
			String url = new String(this.serviceUrl + XMAdminRestClientConstants.MULTI_UPDATE_AA_START_APP_STATUS);

			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<List<AdminAreaStartAppRelRequest>> requestEntity = new HttpEntity<List<AdminAreaStartAppRelRequest>>(
					adminAreaStartAppRelRequest, this.headers);
			ResponseEntity<AdminAreaStartAppRelResponse> responseEntity = restTemplate.exchange(url, HttpMethod.POST,
					requestEntity, AdminAreaStartAppRelResponse.class);
			HttpStatus statusCode = responseEntity.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				return responseEntity.getBody();
			} else {
				LOGGER.error(
						"Error while calling XMSYSTEM Multi Update AdminAreaStartApp Status REST Service, returns the status code: ", //$NON-NLS-1$
						statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException) e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM Multi Update AdminAreaStartApp Status REST Service!", e); //$NON-NLS-1$
		}
		return null;
	}
	
	/**
	 * Gets the AA start app by id.
	 *
	 * @param adminAreaStartAppRelId            {@link String}
	 * @return the AA start app by id
	 * @throws UnauthorizedAccessException the unauthorized access exception
	 */
	public AdminAreaStartAppRelTbl getAAStartAppById(String adminAreaStartAppRelId) throws UnauthorizedAccessException {
		try {
			String url = new String(this.serviceUrl + XMAdminRestClientConstants.GET_ADMIN_AREA_START_APP_REL_BY_ID
					+ "/" + adminAreaStartAppRelId);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<Object> requestEntity = new HttpEntity<Object>(headers);

			List<HttpMessageConverter<?>> messageConverters = new ArrayList<>();
			messageConverters.add(new MappingJackson2HttpMessageConverter());
			restTemplate.setMessageConverters(messageConverters);

			ResponseEntity<AdminAreaStartAppRelTbl> responseList = restTemplate.exchange(url, HttpMethod.GET,
					requestEntity, AdminAreaStartAppRelTbl.class);
			HttpStatus statusCode = responseList.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				AdminAreaStartAppRelTbl adminAreaStartAppRelTblObj = responseList.getBody();
				return adminAreaStartAppRelTblObj;
			} else {
				LOGGER.error(
						"Error while calling XMSYSTEM Get Administration Area Start Application Relation by Id REST Service, returns the status code: " //$NON-NLS-1$
								+ statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException) e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			} else if (e instanceof HttpStatusCodeException) {
				RestClientExpHandlerUtil.handleNoXMLObjectFoundException(LOGGER, (HttpStatusCodeException) e);
			}
		} catch (Exception e) {
			LOGGER.error(
					"Error while calling XMSYSTEM Get Administration Area Start Application Relation by Id REST Service", //$NON-NLS-1$
					e);
		}
		return null;
	}

	/**
	 * Gets the AA start app rel by start app id.
	 *
	 * @param startApp            {@link String}
	 * @return the AA start app rel by start app id
	 * @throws UnauthorizedAccessException the unauthorized access exception
	 */
	public List<AdminAreaStartAppRelation> getAAStartAppRelByStartAppId(String startApp)
			throws UnauthorizedAccessException {
		try {
			String url = new String(this.serviceUrl
					+ XMAdminRestClientConstants.GET_ADMIN_AREA_START_APP_REL_BY_START_APP_ID + "/" + startApp);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<Object> requestEntity = new HttpEntity<Object>(headers);

			List<HttpMessageConverter<?>> messageConverters = new ArrayList<>();
			messageConverters.add(new MappingJackson2HttpMessageConverter());
			restTemplate.setMessageConverters(messageConverters);
			ResponseEntity<AdminAreaStartAppRelationWrapper> responseList = restTemplate.exchange(url, HttpMethod.GET,
					requestEntity, AdminAreaStartAppRelationWrapper.class);
			HttpStatus statusCode = responseList.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				AdminAreaStartAppRelationWrapper adminAreaStartAppRelationWrapper = responseList.getBody();
				List<AdminAreaStartAppRelation> adminAreaStartAppRelations = adminAreaStartAppRelationWrapper.getAdminAreaStartAppRelations();
				return RestClientSortingUtil.sortAAStartAppRelByStartAppIdTbl(adminAreaStartAppRelations);
			} else {
				LOGGER.error(
						"Error while calling XMSYSTEM Get Administration Area Start Application Relation by startAppId REST Service, returns the status code: " //$NON-NLS-1$
								+ statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException) e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			} else if (e instanceof HttpStatusCodeException) {
				RestClientExpHandlerUtil.handleNoXMLObjectFoundException(LOGGER, (HttpStatusCodeException) e);
			} else if(e instanceof ResourceAccessException){
				throw e;
			}
		} catch (Exception e) {
			LOGGER.error(
					"Error while calling XMSYSTEM Get Administration Area Start Application Relation by startAppId REST Service", //$NON-NLS-1$
					e);
		}
		return null;
	}
	
}
