package com.magna.xmsystem.xmadmin.restclient.group;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import com.magna.xmbackend.entities.GroupsTbl;
import com.magna.xmbackend.exception.CannotCreateObjectException;
import com.magna.xmbackend.vo.group.GroupCreateRequest;
import com.magna.xmbackend.vo.group.GroupResponse;
import com.magna.xmsystem.dependencies.customExceptions.UnauthorizedAccessException;
import com.magna.xmsystem.xmadmin.restclient.RestController;
import com.magna.xmsystem.xmadmin.restclient.util.RestClientExpHandlerUtil;
import com.magna.xmsystem.xmadmin.restclient.util.RestClientSortingUtil;
import com.magna.xmsystem.xmadmin.restclient.util.XMAdminRestClientConstants;

/**
 * Controller class for Group model which exchange data with REST Service.
 * 
 * @author subash.janarthanan
 *
 */
public class GroupController extends RestController {

	/** The Constant LOGGER. */
	private static final Logger LOGGER = LoggerFactory.getLogger(GroupController.class);

	/**
	 * Constructor for GroupController Class.
	 */

	public GroupController() {
		super();
	}

	
	/**
	 * Creates the group.
	 *
	 * @param groupRequest the group request
	 * @return the groups tbl
	 */
	public GroupsTbl createGroup(GroupCreateRequest groupRequest) {
		try {
			String url = new String(this.serviceUrl + XMAdminRestClientConstants.CREATE_GROUP);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<GroupCreateRequest> request = new HttpEntity<GroupCreateRequest>(groupRequest, this.headers);
			ResponseEntity<GroupsTbl> responseEntity = restTemplate.postForEntity(url, request, GroupsTbl.class);
			HttpStatus statusCode = responseEntity.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				GroupsTbl groupTbl = responseEntity.getBody();
				return groupTbl;
			} else {
				LOGGER.error("Error while calling XMSYSTEM Create Group REST Service, returns the status code: ", //$NON-NLS-1$
						statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException) e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			} else if (e instanceof HttpStatusCodeException) {
				String errorResponse = ((HttpStatusCodeException) e).getResponseBodyAsString();
				String errCode = RestClientExpHandlerUtil.handleCannotCreateObjectException(LOGGER,
						(HttpStatusCodeException) e);
				throw new CannotCreateObjectException(errorResponse, errCode);
			}else if (e instanceof ResourceAccessException) {
				throw e;
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM Create Group REST Service", e); //$NON-NLS-1$
		}
		return null;
	}

	/**
	 * Method to update group.
	 *
	 * @param groupJson the group json
	 * @return {@link String} Return the status
	 */
	public boolean updateGroup(GroupCreateRequest groupCreateRequest) {
		try {
			String url = new String(this.serviceUrl + XMAdminRestClientConstants.UPDATE_GROUP);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<GroupCreateRequest> request = new HttpEntity<GroupCreateRequest>(groupCreateRequest, this.headers);
			ResponseEntity<Boolean> responseEntity = restTemplate.postForEntity(url, request, Boolean.class);
			HttpStatus statusCode = responseEntity.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				return true;
			} else {
				LOGGER.error("Error while calling XMSYSTEM Upadte Group REST Service, returns the status code: ", //$NON-NLS-1$
						statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException)e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			} else if (e instanceof HttpStatusCodeException) {
				String errorResponse = ((HttpStatusCodeException) e).getResponseBodyAsString();
				String errCode = RestClientExpHandlerUtil.handleCannotCreateObjectException(LOGGER, (HttpStatusCodeException) e);
				throw new CannotCreateObjectException(errorResponse, errCode);
			}else if (e instanceof ResourceAccessException) {
				throw e;
			}
		} catch (Exception e) {
		}
		return false;
	}
	

	/**
	 * Method to get list of groups.
	 *
	 * @return String json structure of all group
	 */
	public GroupResponse getAllGroups() {
		try {
			String url = new String(this.serviceUrl + XMAdminRestClientConstants.GET_ALL_GROUPS);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<Object> requestEntity = new HttpEntity<Object>(headers);
			List<HttpMessageConverter<?>> messageConverters = new ArrayList<>();
			messageConverters.add(new MappingJackson2HttpMessageConverter());
			restTemplate.setMessageConverters(messageConverters);
			ResponseEntity<GroupResponse> responseList = restTemplate.exchange(url, HttpMethod.GET, requestEntity,
					GroupResponse.class);
			HttpStatus statusCode = responseList.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				GroupResponse groupRespObj = responseList.getBody();
				return groupRespObj;
			} else {
				LOGGER.error("Error while calling XMSYSTEM Get All Groups REST Service, returns the status code: " //$NON-NLS-1$
						+ statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException)e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			} else if (e instanceof HttpStatusCodeException) {
				RestClientExpHandlerUtil.handleNoXMLObjectFoundException(LOGGER, (HttpStatusCodeException) e);
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM Get All Groups REST Service", e); //$NON-NLS-1$
		}
		return null;
	}

	/**
	 * Gets the group by group type.
	 *
	 * @param groupType the group type
	 * @return the group by group type
	 */
	public GroupResponse getGroupByGroupType(String groupType) {
		try {
			String url = new String(this.serviceUrl + XMAdminRestClientConstants.GET_GROUPBY_GROUPTYPE+ "/" + groupType);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<Object> requestEntity = new HttpEntity<Object>(headers);
			List<HttpMessageConverter<?>> messageConverters = new ArrayList<>();
			messageConverters.add(new MappingJackson2HttpMessageConverter());
			restTemplate.setMessageConverters(messageConverters);
			ResponseEntity<GroupResponse> responseList = restTemplate.exchange(url, HttpMethod.GET, requestEntity,
					GroupResponse.class);
			HttpStatus statusCode = responseList.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				GroupResponse groupRespObj = responseList.getBody();
				RestClientSortingUtil.sortGroupsName(groupRespObj.getGroupTbls());
				return groupRespObj;
			} else {
				LOGGER.error("Error while calling XMSYSTEM Get Group By Group Type REST Service, returns the status code: " //$NON-NLS-1$
						+ statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException)e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			} else if (e instanceof HttpStatusCodeException) {
				RestClientExpHandlerUtil.handleNoXMLObjectFoundException(LOGGER, (HttpStatusCodeException) e);
			} else if(e instanceof ResourceAccessException){
				throw e;
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM Get  Group By Group Type REST Service", e); //$NON-NLS-1$
		}
		return null;
	}

	
	/**
	 * Method to delete group.
	 *
	 * @param groupId
	 *            {@link String}
	 * @return {@link String} Return the status
	 */
	public boolean deleteGroup(String groupId) {
		try {
			String url = new String(this.serviceUrl + XMAdminRestClientConstants.DELETE_GROUP + "/" + groupId);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<Object> requestEntity = new HttpEntity<Object>(this.headers);
			ResponseEntity<Boolean> responseEntity = restTemplate.exchange(url, HttpMethod.DELETE, requestEntity,
					Boolean.class);
			HttpStatus statusCode = responseEntity.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				return responseEntity.getBody();
			} else {
				LOGGER.error("Error while calling XMSYSTEM delete Group REST Service, returns the status code: ", //$NON-NLS-1$
						statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException)e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM delete Group REST Service!", e); //$NON-NLS-1$
		}
		return false;
	}
	
	/**
	 * Delete multi group.
	 *
	 * @param groupIds the group ids
	 * @return the group response
	 * @throws UnauthorizedAccessException the unauthorized access exception
	 */
	public GroupResponse deleteMultiGroup(Set<String> groupIds) throws UnauthorizedAccessException {
		try {
			String url = new String(this.serviceUrl + XMAdminRestClientConstants.DELETE_MULTI_GROUP);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<Set<String>> requestEntity = new HttpEntity<Set<String>>(groupIds,this.headers);
			ResponseEntity<GroupResponse> responseEntity = restTemplate.exchange(url, HttpMethod.DELETE, requestEntity,
					GroupResponse.class);
			HttpStatus statusCode = responseEntity.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				GroupResponse groupResponse = responseEntity.getBody();
				return groupResponse;
			} else {
				LOGGER.error("Error while calling XMSYSTEM delete Multi Group REST Service, returns the status code: ", //$NON-NLS-1$
						statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException)e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM delete Multi Group REST Service!", e); //$NON-NLS-1$
		}
		return null;
	}
}
