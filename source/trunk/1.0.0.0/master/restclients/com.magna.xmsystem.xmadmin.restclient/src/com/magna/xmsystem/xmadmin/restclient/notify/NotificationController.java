package com.magna.xmsystem.xmadmin.restclient.notify;

import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import com.magna.xmbackend.entities.EmailTemplateTbl;
import com.magna.xmbackend.exception.CannotCreateObjectException;
import com.magna.xmbackend.vo.notification.EmailTemplateRequest;
import com.magna.xmbackend.vo.notification.EmailTemplateResponse;
import com.magna.xmbackend.vo.notification.NotificationConfigResponse;
import com.magna.xmbackend.vo.notification.NotificationEventResponse;
import com.magna.xmbackend.vo.notification.NotificationRequest;
import com.magna.xmbackend.vo.notification.NotificationResponse;
import com.magna.xmbackend.vo.notification.SendMailRequest;
import com.magna.xmbackend.vo.notification.SendMailResponse;
import com.magna.xmsystem.dependencies.customExceptions.UnauthorizedAccessException;
import com.magna.xmsystem.xmadmin.restclient.RestController;
import com.magna.xmsystem.xmadmin.restclient.util.RestClientExpHandlerUtil;
import com.magna.xmsystem.xmadmin.restclient.util.XMAdminRestClientConstants;

// TODO: Auto-generated Javadoc
/**
 * The Class NotificationController.
 * 
 * @author shashwat.anand
 */
public class NotificationController extends RestController {
	/** The Constant LOGGER. */
	private static final Logger LOGGER = LoggerFactory.getLogger(NotificationController.class);

	/**
	 * Constructor for NotificationController Class.
	 */
	public NotificationController() {
		super();
	}

	/**
	 * Creates the notification.
	 *
	 * @param notificationRequest
	 *            the notification request
	 * @return the notification response
	 * @throws UnauthorizedAccessException
	 *             the unauthorized access exception
	 * @throws CannotCreateObjectException
	 *             the cannot create object exception
	 */
	public NotificationConfigResponse createNotification(final NotificationRequest notificationRequest)
			throws UnauthorizedAccessException, CannotCreateObjectException {
		try {
			final String url = new String(this.serviceUrl + XMAdminRestClientConstants.SAVE_NOTIFICATION);
			final RestTemplate restTemplate = new RestTemplate();
			final HttpEntity<NotificationRequest> request = new HttpEntity<NotificationRequest>(notificationRequest,
					this.headers);
			final ResponseEntity<NotificationConfigResponse> responseEntity = restTemplate.exchange(url, HttpMethod.POST,
					request, new ParameterizedTypeReference<NotificationConfigResponse>() {
					});
			HttpStatus statusCode = responseEntity.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				NotificationConfigResponse notificationResponse = responseEntity.getBody();
				return notificationResponse;
			} else {
				LOGGER.error("Error while calling XMSYSTEM Save Notification REST Service, returns the status code: ", //$NON-NLS-1$
						statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException) e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			} else if (e instanceof HttpStatusCodeException) {
				String errorResponse = ((HttpStatusCodeException) e).getResponseBodyAsString();
				String errCode = RestClientExpHandlerUtil.handleCannotCreateObjectException(LOGGER,
						(HttpStatusCodeException) e);
				throw new CannotCreateObjectException(errorResponse, errCode);
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM Save notification REST Service", e); //$NON-NLS-1$
		}
		return null;
	}

	/**
	 * updates the notification.
	 *
	 * @param notificationRequest
	 *            the notification request
	 * @return the notification response
	 * @throws UnauthorizedAccessException
	 *             the unauthorized access exception
	 * @throws CannotCreateObjectException
	 *             the cannot create object exception
	 */
	public NotificationConfigResponse updateNotification(final NotificationRequest notificationRequest)
			throws UnauthorizedAccessException, CannotCreateObjectException {
		try {
			String url = new String(this.serviceUrl + XMAdminRestClientConstants.UPDATE_NOTIFICATION);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<NotificationRequest> request = new HttpEntity<NotificationRequest>(notificationRequest,
					this.headers);
			ResponseEntity<NotificationConfigResponse> responseEntity = restTemplate.exchange(url, HttpMethod.PUT, request,
					new ParameterizedTypeReference<NotificationConfigResponse>() {
					});
			HttpStatus statusCode = responseEntity.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				NotificationConfigResponse notificationResponse = responseEntity.getBody();
				return notificationResponse;
			} else {
				LOGGER.error("Error while calling XMSYSTEM Update Notification REST Service, returns the status code: ", //$NON-NLS-1$
						statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException) e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			} else if (e instanceof HttpStatusCodeException) {
				String errorResponse = ((HttpStatusCodeException) e).getResponseBodyAsString();
				String errCode = RestClientExpHandlerUtil.handleCannotCreateObjectException(LOGGER,
						(HttpStatusCodeException) e);
				throw new CannotCreateObjectException(errorResponse, errCode);
			} else if (e instanceof ResourceAccessException) {
				throw e;
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM Update notification REST Service", e); //$NON-NLS-1$
		}
		return null;
	}

	/**
	 * find notification by event.
	 *
	 * @param event
	 *            the event
	 * @return EmailNotificationConfigTbl
	 */
	public List<NotificationConfigResponse> findNotificationByEvent(final String event) {
		try {
			String url = new String(
					this.serviceUrl + XMAdminRestClientConstants.FIND_NOTIFICATION_BY_EVENT_NAME + "/" + event);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<Object> requestEntity = new HttpEntity<Object>(headers);
			ResponseEntity<List<NotificationConfigResponse>> responseEntity = restTemplate.exchange(url, HttpMethod.GET,
					requestEntity, new ParameterizedTypeReference<List<NotificationConfigResponse>>() {
					});
			HttpStatus statusCode = responseEntity.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				List<NotificationConfigResponse> notificationResponse = responseEntity.getBody();
				/*List<EmailNotificationConfigTbl> emailNotificationConfigTbls = RestClientSortingUtil
						.sortNotificationResponse(notificationResponse);
				notificationResponse.setEmailNotificationConfigTbls(emailNotificationConfigTbls);*/
				return notificationResponse;
			} else {
				LOGGER.error(
						"Error while calling XMSYSTEM Find Notification By Event REST Service, returns the status code: ", //$NON-NLS-1$
						statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException) e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			} else if (e instanceof HttpStatusCodeException) {
				RestClientExpHandlerUtil.handleNoXMLObjectFoundException(LOGGER, (HttpStatusCodeException) e);
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM Find Notification By REST Service", e); //$NON-NLS-1$
		}
		return null;
	}

	/**
	 * Send direct mail.
	 *
	 * @param sendMailRequest
	 *            the send mail request
	 * @return the send mail response
	 * @throws UnauthorizedAccessException
	 *             the unauthorized access exception
	 */
	public SendMailResponse sendDirectMail(final SendMailRequest sendMailRequest) throws UnauthorizedAccessException {
		try {
			final String url = new String(this.serviceUrl + XMAdminRestClientConstants.SENDMAIL_NOTIFICATION);
			final RestTemplate restTemplate = new RestTemplate();
			final HttpEntity<SendMailRequest> request = new HttpEntity<SendMailRequest>(sendMailRequest, this.headers);
			final ResponseEntity<SendMailResponse> responseEntity = restTemplate.exchange(url, HttpMethod.POST, request,
					new ParameterizedTypeReference<SendMailResponse>() {
					});
			HttpStatus statusCode = responseEntity.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				SendMailResponse sendMailResponse = responseEntity.getBody();
				return sendMailResponse;
			} else {
				LOGGER.error(
						"Error while calling XMSYSTEM send mail Notification REST Service, returns the status code: ", //$NON-NLS-1$
						statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException) e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			} else if (e instanceof ResourceAccessException) {
				throw e;
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM send mail notification REST Service", e); //$NON-NLS-1$
		}
		return null;
	}

	/**
	 * Create template.
	 *
	 * @param emailTemplateRequest
	 *            the email template request
	 * @return the email template tbl
	 * @throws UnauthorizedAccessException
	 *             the unauthorized access exception
	 * @throws CannotCreateObjectException
	 *             the cannot create object exception
	 */
	public EmailTemplateTbl createTemplate(final EmailTemplateRequest emailTemplateRequest)
			throws UnauthorizedAccessException, CannotCreateObjectException {
		try {
			final String url = new String(this.serviceUrl + XMAdminRestClientConstants.SAVE_TEMPLATE);
			final RestTemplate restTemplate = new RestTemplate();
			final HttpEntity<EmailTemplateRequest> request = new HttpEntity<EmailTemplateRequest>(emailTemplateRequest,
					this.headers);
			final ResponseEntity<EmailTemplateTbl> responseEntity = restTemplate.exchange(url, HttpMethod.POST, request,
					new ParameterizedTypeReference<EmailTemplateTbl>() {
					});
			HttpStatus statusCode = responseEntity.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				EmailTemplateTbl notificationResponse = responseEntity.getBody();
				return notificationResponse;
			} else {
				LOGGER.error("Error while calling XMSYSTEM Save Template REST Service, returns the status code: ", //$NON-NLS-1$
						statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException) e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			} else if (e instanceof HttpStatusCodeException) {
				String errorResponse = ((HttpStatusCodeException) e).getResponseBodyAsString();
				String errCode = RestClientExpHandlerUtil.handleCannotCreateObjectException(LOGGER,
						(HttpStatusCodeException) e);
				throw new CannotCreateObjectException(errorResponse, errCode);
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM Save Template REST Service", e); //$NON-NLS-1$
		}
		return null;
	}

	/**
	 * Find all notification template.
	 *
	 * @return the email template response
	 * @throws UnauthorizedAccessException
	 *             the unauthorized access exception
	 */
	public EmailTemplateResponse findAllNotificationTemplate() throws UnauthorizedAccessException {
		try {
			String url = new String(this.serviceUrl + XMAdminRestClientConstants.FIND_NOTIFICATION_TEMPLATES);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<Object> requestEntity = new HttpEntity<Object>(headers);
			ResponseEntity<EmailTemplateResponse> responseEntity = restTemplate.exchange(url, HttpMethod.GET,
					requestEntity, EmailTemplateResponse.class);
			HttpStatus statusCode = responseEntity.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				EmailTemplateResponse notificationResponse = responseEntity.getBody();
				return notificationResponse;
			} else {
				LOGGER.error(
						"Error while calling XMSYSTEM Find Notification Template REST Service, returns the status code: ", //$NON-NLS-1$
						statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException) e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			} else if (e instanceof HttpStatusCodeException) {
				RestClientExpHandlerUtil.handleNoXMLObjectFoundException(LOGGER, (HttpStatusCodeException) e);
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM Find Notification Template REST Service", e); //$NON-NLS-1$
		}
		return null;
	}

	/**
	 * Update template.
	 *
	 * @param emailTemplateRequest
	 *            the email template request
	 * @return the email template tbl
	 * @throws UnauthorizedAccessException
	 *             the unauthorized access exception
	 * @throws CannotCreateObjectException
	 *             the cannot create object exception
	 */
	public EmailTemplateTbl updateTemplate(final EmailTemplateRequest emailTemplateRequest)
			throws UnauthorizedAccessException, CannotCreateObjectException {
		try {
			String url = new String(this.serviceUrl + XMAdminRestClientConstants.UPDATE_TEMPLATE);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<EmailTemplateRequest> request = new HttpEntity<EmailTemplateRequest>(emailTemplateRequest,
					this.headers);
			ResponseEntity<EmailTemplateTbl> responseEntity = restTemplate.exchange(url, HttpMethod.POST, request,
					new ParameterizedTypeReference<EmailTemplateTbl>() {
					});
			HttpStatus statusCode = responseEntity.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				EmailTemplateTbl notificationResponse = responseEntity.getBody();
				return notificationResponse;
			} else {
				LOGGER.error("Error while calling XMSYSTEM Update template REST Service, returns the status code: ", //$NON-NLS-1$
						statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException) e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			} else if (e instanceof HttpStatusCodeException) {
				String errorResponse = ((HttpStatusCodeException) e).getResponseBodyAsString();
				String errCode = RestClientExpHandlerUtil.handleCannotCreateObjectException(LOGGER,
						(HttpStatusCodeException) e);
				throw new CannotCreateObjectException(errorResponse, errCode);
			} else if (e instanceof ResourceAccessException) {
				throw e;
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM Update template REST Service", e); //$NON-NLS-1$
		}
		return null;
	}

	/**
	 * Delete multi template.
	 *
	 * @param templateIds
	 *            the template ids
	 * @return the boolean
	 */
	public EmailTemplateResponse deleteMultiTemplate(Set<String> templateIds) {
		try {
			String url = new String(this.serviceUrl + XMAdminRestClientConstants.DELETE_MULTI_TEMPLATE);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<Set<String>> requestEntity = new HttpEntity<Set<String>>(templateIds, this.headers);
			ResponseEntity<EmailTemplateResponse> responseEntity = restTemplate.exchange(url, HttpMethod.POST, requestEntity,
					EmailTemplateResponse.class);
			HttpStatus statusCode = responseEntity.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				EmailTemplateResponse templateResponseObject = responseEntity.getBody();
				return templateResponseObject;
			} else {
				LOGGER.error(
						"Error while calling XMSYSTEM delete Multi template REST Service, returns the status code: ", //$NON-NLS-1$
						statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException) e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM delete Multi template REST Service!", e); //$NON-NLS-1$
		}
		return null;

	}
	
	/**
	 * Find all events.
	 *
	 * @return the notification event response
	 */
	public NotificationEventResponse findAllEvents() {
		try {
			String url = new String(
					this.serviceUrl + XMAdminRestClientConstants.FIND_ALL_NOTIFICATION_EVENTS);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<Object> requestEntity = new HttpEntity<Object>(headers);
			ResponseEntity<NotificationEventResponse> responseEntity = restTemplate.exchange(url, HttpMethod.GET,
					requestEntity, new ParameterizedTypeReference<NotificationEventResponse>() {
					});
			HttpStatus statusCode = responseEntity.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				NotificationEventResponse notificationResponse = responseEntity.getBody();
				return notificationResponse;
			} else {
				LOGGER.error(
						"Error while calling XMSYSTEM Find All Notification Events REST Service, returns the status code: ", //$NON-NLS-1$
						statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException) e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			} else if (e instanceof HttpStatusCodeException) {
				RestClientExpHandlerUtil.handleNoXMLObjectFoundException(LOGGER, (HttpStatusCodeException) e);
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM Find All Notification Events REST Service", e); //$NON-NLS-1$
		}
		return null;
	}
	
	/**
	 * Delete multi action.
	 *
	 * @param actionIds the action ids
	 * @return the notification response
	 */
	public NotificationResponse deleteMultiAction(Set<String> actionIds) {
		try {
			String url = new String(this.serviceUrl + XMAdminRestClientConstants.DELETE_MULTI_ACTION);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<Set<String>> requestEntity = new HttpEntity<Set<String>>(actionIds, this.headers);
			ResponseEntity<NotificationResponse> responseEntity = restTemplate.exchange(url, HttpMethod.POST, requestEntity,
					NotificationResponse.class);
			HttpStatus statusCode = responseEntity.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				NotificationResponse templateResponseObject = responseEntity.getBody();
				return templateResponseObject;
			} else {
				LOGGER.error(
						"Error while calling XMSYSTEM delete Multi Action REST Service, returns the status code: ", //$NON-NLS-1$
						statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException) e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM delete Multi Action REST Service!", e); //$NON-NLS-1$
		}
		return null;

	}

	/**
	 * Update notification status.
	 *
	 * @param event the event
	 * @param status the status
	 * @return the boolean
	 */
	public Boolean updateNotificationStatus(String event, String status) {
		try {
			String url = new String(
					this.serviceUrl + XMAdminRestClientConstants.UPDATE_NOTIFICATION_STATUS + "/" + event + "/" + status);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<Object> requestEntity = new HttpEntity<Object>(this.headers);
			ResponseEntity<Boolean> responseEntity = restTemplate.exchange(url, HttpMethod.PUT, requestEntity,
					Boolean.class);
			HttpStatus statusCode = responseEntity.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				Boolean response = responseEntity.getBody();
				return response;
			} else {
				LOGGER.error("Error while calling XMSYSTEM update event status REST Service, returns the status code: ", //$NON-NLS-1$
						statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException) e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM update event status REST Service!", e); //$NON-NLS-1$
		}
		return null;

	}

	/**
	 * Update notif action status.
	 *
	 * @param configId the config id
	 * @param status the status
	 * @return the boolean
	 */
	public Boolean updateNotifActionStatus(String configId, String status) {
		try {
			String url = new String(
					this.serviceUrl + XMAdminRestClientConstants.UPDATE_NOTIFY_ACTION_STATUS + "/" + configId + "/" + status);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<Object> requestEntity = new HttpEntity<Object>(this.headers);
			ResponseEntity<Boolean> responseEntity = restTemplate.exchange(url, HttpMethod.PUT, requestEntity,
					Boolean.class);
			HttpStatus statusCode = responseEntity.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				Boolean response = responseEntity.getBody();
				return response;
			} else {
				LOGGER.error("Error while calling XMSYSTEM update action status REST Service, returns the status code: ", //$NON-NLS-1$
						statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException) e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM update action status REST Service!", e); //$NON-NLS-1$
		}
		return null;

	}

}
