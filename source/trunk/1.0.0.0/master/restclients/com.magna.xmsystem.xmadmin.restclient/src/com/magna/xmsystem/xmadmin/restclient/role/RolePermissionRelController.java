package com.magna.xmsystem.xmadmin.restclient.role;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import com.magna.xmbackend.entities.RolePermissionRelTbl;
import com.magna.xmbackend.vo.rel.RolePermissionRelResponse;
import com.magna.xmsystem.dependencies.customExceptions.UnauthorizedAccessException;
import com.magna.xmsystem.xmadmin.restclient.RestController;
import com.magna.xmsystem.xmadmin.restclient.util.RestClientExpHandlerUtil;
import com.magna.xmsystem.xmadmin.restclient.util.XMAdminRestClientConstants;

/**
 * The Class RolePermissionRelController.
 * 
 * @author shashwat.anand
 */
public class RolePermissionRelController extends RestController {
	/** The Constant LOGGER. */
	private static final Logger LOGGER = LoggerFactory.getLogger(RolePermissionRelController.class);

	/**
	 * Instantiates a new role permission rel controller.
	 */
	public RolePermissionRelController() {
		super();
	}

	/**
	 * Gets the permission by role id.
	 *
	 * @param roleId
	 *            the role id
	 * @return the permission by role id
	 */
	public Iterable<RolePermissionRelTbl> getPermissionByRoleId(final String roleId) throws UnauthorizedAccessException {
		try {
			final String url = new String(
					this.serviceUrl + XMAdminRestClientConstants.GET_PERMISSION_BY_ROLEID + "/" + roleId);
			final RestTemplate restTemplate = new RestTemplate();
			HttpEntity<Object> requestEntity = new HttpEntity<Object>(headers);

			List<HttpMessageConverter<?>> messageConverters = new ArrayList<>();
			messageConverters.add(new MappingJackson2HttpMessageConverter());
			restTemplate.setMessageConverters(messageConverters);

			ResponseEntity<RolePermissionRelResponse> responseList = restTemplate.exchange(url, HttpMethod.GET,
					requestEntity, RolePermissionRelResponse.class);
			HttpStatus statusCode = responseList.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				RolePermissionRelResponse rolePermissionRelResponse = responseList.getBody();
				return rolePermissionRelResponse.getRolePermissionRelTbls();
			} else {
				LOGGER.error(
						"Error while calling XMSYSTEM Get Permission by Role Id REST Service, returns the status code: " //$NON-NLS-1$
								+ statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException) e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			} else if (e instanceof HttpStatusCodeException) {
				RestClientExpHandlerUtil.handleNoXMLObjectFoundException(LOGGER, (HttpStatusCodeException) e);
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM Get Permission by Role Id REST Service", e); //$NON-NLS-1$
		}
		return null;
	}
}
