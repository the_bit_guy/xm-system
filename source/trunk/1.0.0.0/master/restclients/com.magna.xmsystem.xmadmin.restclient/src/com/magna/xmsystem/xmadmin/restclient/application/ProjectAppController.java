package com.magna.xmsystem.xmadmin.restclient.application;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import com.magna.xmbackend.entities.ProjectApplicationsTbl;
import com.magna.xmbackend.exception.CannotCreateObjectException;
import com.magna.xmbackend.vo.projectApplication.ProjectApplciationMenuWrapper;
import com.magna.xmbackend.vo.projectApplication.ProjectApplicationRequest;
import com.magna.xmbackend.vo.projectApplication.ProjectApplicationResponse;
import com.magna.xmsystem.dependencies.customExceptions.UnauthorizedAccessException;
import com.magna.xmsystem.xmadmin.restclient.RestController;
import com.magna.xmsystem.xmadmin.restclient.util.RestClientExpHandlerUtil;
import com.magna.xmsystem.xmadmin.restclient.util.RestClientSortingUtil;
import com.magna.xmsystem.xmadmin.restclient.util.XMAdminRestClientConstants;

/**
 * Class for Project app controller.
 *
 * @author Chiranjeevi.Akula
 */
public class ProjectAppController extends RestController {

	/** The Constant LOGGER. */
	private static final Logger LOGGER = LoggerFactory.getLogger(ProjectAppController.class);

	/**
	 * Constructor for ProjectAppController Class.
	 */

	public ProjectAppController() {
		super();
	}

	/**
	 * Method for Creates the project application.
	 *
	 * @param projectAppRequest
	 *            {@link ProjectApplicationRequest}
	 * @return the project applications tbl {@link ProjectApplicationsTbl}
	 */
	public ProjectApplicationsTbl createProjectApplication(final ProjectApplicationRequest projectAppRequest)
			throws UnauthorizedAccessException, CannotCreateObjectException {
		try {
			String url = new String(this.serviceUrl + XMAdminRestClientConstants.CREATE_PROJECT_APPLICATION);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<ProjectApplicationRequest> request = new HttpEntity<ProjectApplicationRequest>(projectAppRequest,
					this.headers);
			ResponseEntity<ProjectApplicationsTbl> responseEntity = restTemplate.postForEntity(url, request,
					ProjectApplicationsTbl.class);
			HttpStatus statusCode = responseEntity.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				ProjectApplicationsTbl projectApplicationsTbl = responseEntity.getBody();
				return projectApplicationsTbl;
			} else {
				LOGGER.error(
						"Error while calling XMSYSTEM Create Project Application REST Service, returns the status code: ", //$NON-NLS-1$
						statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException) e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			} else if (e instanceof HttpStatusCodeException) {
				String errorResponse = ((HttpStatusCodeException) e).getResponseBodyAsString();
				String errCode = RestClientExpHandlerUtil.handleCannotCreateObjectException(LOGGER,
						(HttpStatusCodeException) e);
				throw new CannotCreateObjectException(errorResponse, errCode);
			}else if (e instanceof ResourceAccessException) {
				throw e;
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM Create Project Application REST Service", e); //$NON-NLS-1$
		}
		return null;
	}

	/**
	 * Gets the all project apps.
	 *
	 * @param isSorted
	 *            {@link boolean}
	 * @return the all project apps
	 */
	public List<ProjectApplicationsTbl> getAllProjectApps(final boolean isSorted) throws UnauthorizedAccessException {
		try {
			String url = new String(this.serviceUrl + XMAdminRestClientConstants.GET_PROJECT_APPLICATIONS);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<Object> requestEntity = new HttpEntity<Object>(headers);

			List<HttpMessageConverter<?>> messageConverters = new ArrayList<>();
			messageConverters.add(new MappingJackson2HttpMessageConverter());
			restTemplate.setMessageConverters(messageConverters);

			ResponseEntity<ProjectApplicationResponse> responseList = restTemplate.exchange(url, HttpMethod.GET,
					requestEntity, ProjectApplicationResponse.class);
			HttpStatus statusCode = responseList.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				ProjectApplicationResponse projectAppResponse = responseList.getBody();
				List<ProjectApplicationsTbl> projectAppTblsList = (List<ProjectApplicationsTbl>) projectAppResponse
						.getProjectApplicationsTbls();
				return RestClientSortingUtil.sortProjectAppByProjectIdTbl(projectAppTblsList);
			} else {
				LOGGER.error(
						"Error while calling XMSYSTEM Get All Project Applicatios REST Service, returns the status code: " //$NON-NLS-1$
								+ statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException) e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			} else if (e instanceof HttpStatusCodeException) {
				RestClientExpHandlerUtil.handleNoXMLObjectFoundException(LOGGER, (HttpStatusCodeException) e);
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM Get All Project Applications REST Service", e); //$NON-NLS-1$
		}
		return null;
	}

	/**
	 * Method for Delete project application.
	 *
	 * @param projectApplicationId
	 *            {@link String}
	 * @return true, if successful
	 */
	public boolean deleteProjectApplication(String projectApplicationId) throws UnauthorizedAccessException {
		try {
			String url = new String(this.serviceUrl + XMAdminRestClientConstants.DELETE_PROJECT_APPLICATION + "/"
					+ projectApplicationId);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<Object> requestEntity = new HttpEntity<Object>(this.headers);
			ResponseEntity<Boolean> responseEntity = restTemplate.exchange(url, HttpMethod.DELETE, requestEntity,
					Boolean.class);
			HttpStatus statusCode = responseEntity.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				return responseEntity.getBody();
			} else {
				LOGGER.error(
						"Error while calling XMSYSTEM delete Project Application REST Service, returns the status code: ", //$NON-NLS-1$
						statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException) e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM delete Project Application REST Service!", e); //$NON-NLS-1$
		}
		return false;
	}

	/**
	 * Delete multi project app.
	 *
	 * @param projectAppIds the project app ids
	 * @return the project application response
	 * @throws UnauthorizedAccessException the unauthorized access exception
	 */
	public ProjectApplicationResponse deleteMultiProjectApp(Set<String> projectAppIds) throws UnauthorizedAccessException {
		try {
			String url = new String(this.serviceUrl + XMAdminRestClientConstants.DELETE_MULTI_PROJECT_APP);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<Set<String>> requestEntity = new HttpEntity<Set<String>>(projectAppIds,this.headers);
			ResponseEntity<ProjectApplicationResponse> responseEntity = restTemplate.exchange(url, HttpMethod.DELETE, requestEntity,
					ProjectApplicationResponse.class);
			HttpStatus statusCode = responseEntity.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				ProjectApplicationResponse projectApplicationResponse = responseEntity.getBody();
				return projectApplicationResponse;
			} else {
				LOGGER.error("Error while calling XMSYSTEM delete Multi ProjectApp REST Service, returns the status code: ", //$NON-NLS-1$
						statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException)e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM delete Multi ProjectApp REST Service!", e); //$NON-NLS-1$
		}
		return null;
	}
	
	/**
	 * Method for Update project application.
	 *
	 * @param projectAppRequest
	 *            {@link ProjectApplicationRequest}
	 * @return true, if successful
	 */
	public boolean updateProjectApplication(ProjectApplicationRequest projectAppRequest)
			throws UnauthorizedAccessException, CannotCreateObjectException {
		try {
			String url = new String(this.serviceUrl + XMAdminRestClientConstants.UPDATE_PROJECT_APPLICATION);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<ProjectApplicationRequest> request = new HttpEntity<ProjectApplicationRequest>(projectAppRequest,
					this.headers);
			ResponseEntity<ProjectApplicationsTbl> responseEntity = restTemplate.postForEntity(url, request,
					ProjectApplicationsTbl.class);
			HttpStatus statusCode = responseEntity.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				return true;
			} else {
				LOGGER.error(
						"Error while calling XMSYSTEM Upadte Project Application REST Service, returns the status code: ", //$NON-NLS-1$
						statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException) e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			} else if (e instanceof HttpStatusCodeException) {
				String errorResponse = ((HttpStatusCodeException) e).getResponseBodyAsString();
				String errCode = RestClientExpHandlerUtil.handleCannotCreateObjectException(LOGGER,
						(HttpStatusCodeException) e);
				throw new CannotCreateObjectException(errorResponse, errCode);
			}else if (e instanceof ResourceAccessException) {
				throw e;
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM Upadte Project Application REST Service", e); //$NON-NLS-1$
		}
		return false;
	}

	/**
	 * Method for Update project app status.
	 *
	 * @param projectApplicationId
	 *            {@link String}
	 * @param status
	 *            {@link String}
	 * @return true, if successful
	 */
	public boolean updateProjectAppStatus(String projectApplicationId, String status)
			throws UnauthorizedAccessException {
		try {
			String url = new String(this.serviceUrl + XMAdminRestClientConstants.UPDATE_PROJECT_APPLICATION_STATUS + "/"
					+ status + "/" + projectApplicationId);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<Object> requestEntity = new HttpEntity<Object>(this.headers);
			ResponseEntity<Boolean> responseEntity = restTemplate.exchange(url, HttpMethod.POST, requestEntity,
					Boolean.class);
			HttpStatus statusCode = responseEntity.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				return responseEntity.getBody();
			} else {
				LOGGER.error(
						"Error while calling XMSYSTEM Update Project Application Status REST Service, returns the status code: ", //$NON-NLS-1$
						statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException) e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM Update Project Application Status REST Service!", e); //$NON-NLS-1$
		}
		return false;
	}

	/**
	 * Multi update project app status.
	 *
	 * @param userAppRequest the user app request
	 * @return the project application response
	 * @throws UnauthorizedAccessException the unauthorized access exception
	 */
	public ProjectApplicationResponse multiUpdateProjectAppStatus(List<ProjectApplicationRequest> userAppRequest) throws UnauthorizedAccessException {
		try {
			String url = new String(
					this.serviceUrl + XMAdminRestClientConstants.MULTI_UPDATE_PROJECT_APP_STATUS );

			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<List<ProjectApplicationRequest>> requestEntity = new HttpEntity<List<ProjectApplicationRequest>>(userAppRequest, this.headers);
			ResponseEntity<ProjectApplicationResponse> responseEntity = restTemplate.exchange(url, HttpMethod.POST, requestEntity,
					ProjectApplicationResponse.class);
			HttpStatus statusCode = responseEntity.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				return responseEntity.getBody();
			} else {
				LOGGER.error("Error while calling XMSYSTEM Multi Update ProjectApp Status REST Service, returns the status code: ", //$NON-NLS-1$
						statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException)e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM Multi Update projectApp Status REST Service!", e); //$NON-NLS-1$
		}
		return null;
	}
	
	/**
	 * Gets the project app by id.
	 *
	 * @param projectAppId
	 *            {@link String}
	 * @return the project app by id
	 */
	public ProjectApplicationsTbl getProjectAppById(String projectAppId) throws UnauthorizedAccessException {
		try {
			String url = new String(
					this.serviceUrl + XMAdminRestClientConstants.GET_PROJECT_APP_BY_ID + "/" + projectAppId);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<Object> requestEntity = new HttpEntity<Object>(headers);

			List<HttpMessageConverter<?>> messageConverters = new ArrayList<>();
			messageConverters.add(new MappingJackson2HttpMessageConverter());
			restTemplate.setMessageConverters(messageConverters);

			ResponseEntity<ProjectApplicationsTbl> responseList = restTemplate.exchange(url, HttpMethod.GET,
					requestEntity, ProjectApplicationsTbl.class);
			HttpStatus statusCode = responseList.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				ProjectApplicationsTbl projectAppTblObj = responseList.getBody();
				return projectAppTblObj;
			} else {
				LOGGER.error(
						"Error while calling XMSYSTEM Get Project Application By Id REST Service, returns the status code: " //$NON-NLS-1$
								+ statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException) e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			} else if (e instanceof HttpStatusCodeException) {
				RestClientExpHandlerUtil.handleNoXMLObjectFoundException(LOGGER, (HttpStatusCodeException) e);
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM Get All Project Application By Id REST Service", e); //$NON-NLS-1$
		}
		return null;
	}

	/**
	 * Gets the project app by project id.
	 *
	 * @param projectId
	 *            {@link String}
	 * @return the project app by project id
	 */
	public List<ProjectApplicationsTbl> getProjectAppByProjectId(String projectId) throws UnauthorizedAccessException {
		try {
			String url = new String(
					this.serviceUrl + XMAdminRestClientConstants.GET_PROJECT_APP_BY_PROJECT_ID + "/" + projectId);
			List<ProjectApplicationsTbl> projectProjectRelTblList = new ArrayList<>();
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<Object> requestEntity = new HttpEntity<Object>(headers);

			List<HttpMessageConverter<?>> messageConverters = new ArrayList<>();
			messageConverters.add(new MappingJackson2HttpMessageConverter());
			restTemplate.setMessageConverters(messageConverters);

			ResponseEntity<ProjectApplicationResponse> responseList = restTemplate.exchange(url, HttpMethod.GET,
					requestEntity, ProjectApplicationResponse.class);
			HttpStatus statusCode = responseList.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				ProjectApplicationResponse projectAppRespObj = responseList.getBody();
				Iterable<ProjectApplicationsTbl> projectApplicationsTbls = projectAppRespObj
						.getProjectApplicationsTbls();
				for (ProjectApplicationsTbl projectApplicationsTbl : projectApplicationsTbls) {
					projectProjectRelTblList.add(projectApplicationsTbl);
				}

				return RestClientSortingUtil.sortProjectAppByProjectIdTbl(projectProjectRelTblList);
			} else {
				LOGGER.error(
						"Error while calling XMSYSTEM Get Project Application By projectAppId REST Service, returns the status code: " //$NON-NLS-1$
								+ statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException) e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			} else if (e instanceof HttpStatusCodeException) {
				RestClientExpHandlerUtil.handleNoXMLObjectFoundException(LOGGER, (HttpStatusCodeException) e);
			} else if(e instanceof ResourceAccessException){
				throw e;
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM Get All Project By projectAppId REST Service", e); //$NON-NLS-1$
		}
		return null;
	}

	/**
	 * Gets the project app by proj id and type.
	 *
	 * @param projectId
	 *            {@link String}
	 * @param type
	 *            {@link String}
	 * @return the project app by proj id and type
	 */
	public ProjectApplicationResponse getProjectAppByProjIdAndType(String projectId, String type)
			throws UnauthorizedAccessException {
		try {
			String url = new String(this.serviceUrl + XMAdminRestClientConstants.GET_PROJECT_APP_BY_PROJECT_ID_AND_TYPE
					+ "/" + projectId + "/" + type);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<Object> requestEntity = new HttpEntity<Object>(headers);

			List<HttpMessageConverter<?>> messageConverters = new ArrayList<>();
			messageConverters.add(new MappingJackson2HttpMessageConverter());
			restTemplate.setMessageConverters(messageConverters);

			ResponseEntity<ProjectApplicationResponse> responseList = restTemplate.exchange(url, HttpMethod.GET,
					requestEntity, ProjectApplicationResponse.class);
			HttpStatus statusCode = responseList.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				ProjectApplicationResponse projectAppRespObj = responseList.getBody();
				return projectAppRespObj;
			} else {
				LOGGER.error(
						"Error while calling XMSYSTEM Get Project Application By BaseAppId REST Service, returns the status code: " //$NON-NLS-1$
								+ statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException) e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			} else if (e instanceof HttpStatusCodeException) {
				RestClientExpHandlerUtil.handleNoXMLObjectFoundException(LOGGER, (HttpStatusCodeException) e);
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM Get All Project Appliocation By BaseAppId REST Service", e); //$NON-NLS-1$
		}
		return null;
	}

	/**
	 * Gets the project app by base app id.
	 *
	 * @param baseAppId
	 *            {@link String}
	 * @return the project app by base app id
	 */
	public List<ProjectApplicationsTbl> getProjectAppByBaseAppId(String baseAppId) {
		try {
			String url = new String(
					this.serviceUrl + XMAdminRestClientConstants.GET_PROJECT_APP_BY_BASE_APP_ID + "/" + baseAppId);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<Object> requestEntity = new HttpEntity<Object>(headers);

			List<ProjectApplicationsTbl> baseProjectAppRelTblList = new ArrayList<>();

			List<HttpMessageConverter<?>> messageConverters = new ArrayList<>();
			messageConverters.add(new MappingJackson2HttpMessageConverter());
			restTemplate.setMessageConverters(messageConverters);

			ResponseEntity<ProjectApplicationResponse> responseList = restTemplate.exchange(url, HttpMethod.GET,
					requestEntity, ProjectApplicationResponse.class);
			HttpStatus statusCode = responseList.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				ProjectApplicationResponse projectAppRespObj = responseList.getBody();
				Iterable<ProjectApplicationsTbl> projectApplicationsTbls = projectAppRespObj
						.getProjectApplicationsTbls();

				for (ProjectApplicationsTbl siteAdminTbl : projectApplicationsTbls) {
					baseProjectAppRelTblList.add(siteAdminTbl);
				}

				return RestClientSortingUtil.sortProjectAppByProjectIdTbl(baseProjectAppRelTblList);
			} else {
				LOGGER.error(
						"Error while calling XMSYSTEM Get Project Application By BaseAppId REST Service, returns the status code: " //$NON-NLS-1$
								+ statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException) e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			} else if (e instanceof HttpStatusCodeException) {
				RestClientExpHandlerUtil.handleNoXMLObjectFoundException(LOGGER, (HttpStatusCodeException) e);
			} else if(e instanceof ResourceAccessException){
				throw e;
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM Get All Project Appliocation By BaseAppId REST Service", e); //$NON-NLS-1$
		}
		return null;
	}


	/**
	 * Gets the all project app by position.
	 *
	 * @param position
	 *            {@link String}
	 * @return the all project app by position
	 */
	public ProjectApplicationResponse getAllProjectAppByPosition(String position) {
		try {
			String url = new String(
					this.serviceUrl + XMAdminRestClientConstants.GET_PROJECT_APP_BY_POSITION + "/" + position);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<Object> requestEntity = new HttpEntity<Object>(headers);

			List<HttpMessageConverter<?>> messageConverters = new ArrayList<>();
			messageConverters.add(new MappingJackson2HttpMessageConverter());
			restTemplate.setMessageConverters(messageConverters);

			ResponseEntity<ProjectApplicationResponse> responseList = restTemplate.exchange(url, HttpMethod.GET,
					requestEntity, ProjectApplicationResponse.class);
			HttpStatus statusCode = responseList.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				ProjectApplicationResponse projectAppRespObj = responseList.getBody();
				return projectAppRespObj;
			} else {
				LOGGER.error(
						"Error while calling XMSYSTEM Get Project Application By Position REST Service, returns the status code: " //$NON-NLS-1$
								+ statusCode.value());
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM Get All Project Appliocation By Position REST Service", e); //$NON-NLS-1$
		}
		return null;
	}

	/**
	 * Gets the all child proj app by proj app id.
	 *
	 * @param projAppId
	 *            {@link String}
	 * @return the all child proj app by proj app id
	 */
	public List<ProjectApplicationsTbl> getAllChildProjAppByProjAppId(String projAppId) {
		try {
			String url = new String(this.serviceUrl + XMAdminRestClientConstants.GET_CHILD_PROJECT_APP_BY_PROJECT_APP_ID
					+ "/" + projAppId);
			
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<Object> requestEntity = new HttpEntity<Object>(headers);

			List<HttpMessageConverter<?>> messageConverters = new ArrayList<>();
			messageConverters.add(new MappingJackson2HttpMessageConverter());
			restTemplate.setMessageConverters(messageConverters);

			ResponseEntity<ProjectApplicationResponse> responseList = restTemplate.exchange(url, HttpMethod.GET,
					requestEntity, ProjectApplicationResponse.class);
			HttpStatus statusCode = responseList.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				ProjectApplicationResponse projectAppRespObj = responseList.getBody();
				List<ProjectApplicationsTbl> projectAppTblList = (List<ProjectApplicationsTbl>) projectAppRespObj
						.getProjectApplicationsTbls();
				return RestClientSortingUtil.sortProjectAppByProjectIdTbl(projectAppTblList);
			} else {
				LOGGER.error(
						"Error while calling XMSYSTEM Get All Child Project Application By ProjectAppId REST Service, returns the status code: " //$NON-NLS-1$
								+ statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException) e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			} else if (e instanceof HttpStatusCodeException) {
				RestClientExpHandlerUtil.handleNoXMLObjectFoundException(LOGGER, (HttpStatusCodeException) e);
			} else if(e instanceof ResourceAccessException){
				throw e;
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM Get All Child Project Appliocation By ProjectAppId REST Service", //$NON-NLS-1$
					e);
		}
		return null;
	}

	/**
	 * Gets the all proj app by status.
	 *
	 * @param status
	 *            {@link String}
	 * @return the all proj app by status
	 */
	public ProjectApplicationResponse getAllProjAppByStatus(String status) throws UnauthorizedAccessException {
		try {
			String url = new String(
					this.serviceUrl + XMAdminRestClientConstants.GET_PROJECT_APP_BY_STATUS + "/" + status);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<Object> requestEntity = new HttpEntity<Object>(headers);

			List<HttpMessageConverter<?>> messageConverters = new ArrayList<>();
			messageConverters.add(new MappingJackson2HttpMessageConverter());
			restTemplate.setMessageConverters(messageConverters);

			ResponseEntity<ProjectApplicationResponse> responseList = restTemplate.exchange(url, HttpMethod.GET,
					requestEntity, ProjectApplicationResponse.class);
			HttpStatus statusCode = responseList.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				ProjectApplicationResponse projectAppRespObj = responseList.getBody();
				return projectAppRespObj;
			} else {
				LOGGER.error(
						"Error while calling XMSYSTEM Get All Project Application By Status REST Service, returns the status code: " //$NON-NLS-1$
								+ statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException) e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			} else if (e instanceof HttpStatusCodeException) {
				RestClientExpHandlerUtil.handleNoXMLObjectFoundException(LOGGER, (HttpStatusCodeException) e);
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM Get All Project Appliocation By Status REST Service", e); //$NON-NLS-1$
		}
		return null;
	}

	public List<ProjectApplicationsTbl> getAllProjAppsByPositions() throws UnauthorizedAccessException {
		try {
			String url = new String(this.serviceUrl + XMAdminRestClientConstants.GET_PROJECT_APP_BY_POSITIONS);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<Object> requestEntity = new HttpEntity<Object>(headers);

			List<HttpMessageConverter<?>> messageConverters = new ArrayList<>();
			messageConverters.add(new MappingJackson2HttpMessageConverter());
			restTemplate.setMessageConverters(messageConverters);

			ResponseEntity<ProjectApplicationResponse> responseList = restTemplate.exchange(url, HttpMethod.GET,
					requestEntity, ProjectApplicationResponse.class);
			HttpStatus statusCode = responseList.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				ProjectApplicationResponse projectAppRespObj = responseList.getBody();
				List<ProjectApplicationsTbl> projectAppTblList = (List<ProjectApplicationsTbl>) projectAppRespObj
						.getProjectApplicationsTbls();
				return RestClientSortingUtil.sortProjectAppTbl(projectAppTblList);
			} else {
				LOGGER.error(
						"Error while calling XMSYSTEM Get All Project Application By Positions REST Service, returns the status code: " //$NON-NLS-1$
								+ statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException) e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			} else if (e instanceof HttpStatusCodeException) {
				RestClientExpHandlerUtil.handleNoXMLObjectFoundException(LOGGER, (HttpStatusCodeException) e);
			} else if(e instanceof ResourceAccessException){
				throw e;
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM Get All Project Appliocation By Positions REST Service", e); //$NON-NLS-1$
		}
		return null;
	}

	/**
	 * Gets the project app by project site AA id.
	 *
	 * @param siteId
	 *            {@link String}
	 * @param adminAreaId
	 *            {@link String}
	 * @param projectId
	 *            {@link String}
	 * @return the project app by project site AA id
	 */
	public ProjectApplicationResponse getProjectAppByProjectSiteAAId(String siteId, String adminAreaId,
			String projectId) throws UnauthorizedAccessException {
		try {
			String url = new String(this.serviceUrl + XMAdminRestClientConstants.GET_PROJECT_APP_BY_PROJECT_SITE_AA_ID
					+ "/" + siteId + "/" + adminAreaId + "/" + projectId);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<Object> requestEntity = new HttpEntity<Object>(headers);

			List<HttpMessageConverter<?>> messageConverters = new ArrayList<>();
			messageConverters.add(new MappingJackson2HttpMessageConverter());
			restTemplate.setMessageConverters(messageConverters);

			ResponseEntity<ProjectApplicationResponse> responseList = restTemplate.exchange(url, HttpMethod.GET,
					requestEntity, ProjectApplicationResponse.class);
			HttpStatus statusCode = responseList.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				ProjectApplicationResponse projectAppRespObj = responseList.getBody();
				return projectAppRespObj;
			} else {
				LOGGER.error(
						"Error while calling XMSYSTEM Get Project Application By ProjectId, SiteId and AdminAreaId REST Service, returns the status code: " //$NON-NLS-1$
								+ statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException) e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			} else if (e instanceof HttpStatusCodeException) {
				RestClientExpHandlerUtil.handleNoXMLObjectFoundException(LOGGER, (HttpStatusCodeException) e);
			}
		} catch (Exception e) {
			LOGGER.error(
					"Error while calling XMSYSTEM Get All Project Appliocation By ProjectId, SiteId and AdminAreaId REST Service", //$NON-NLS-1$
					e);
		}
		return null;
	}

	/**
	 * Gets the project app by user project id.
	 *
	 * @param userId
	 *            {@link String}
	 * @param projectId
	 *            {@link String}
	 * @return the project app by user project id
	 */
	public ProjectApplicationResponse getProjectAppByUserProjectId(String userId, String projectId)
			throws UnauthorizedAccessException {
		try {
			String url = new String(this.serviceUrl + XMAdminRestClientConstants.GET_PROJECT_APP_BY_USER_PROJECT_ID
					+ "/" + userId + "/" + projectId);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<Object> requestEntity = new HttpEntity<Object>(headers);

			List<HttpMessageConverter<?>> messageConverters = new ArrayList<>();
			messageConverters.add(new MappingJackson2HttpMessageConverter());
			restTemplate.setMessageConverters(messageConverters);

			ResponseEntity<ProjectApplicationResponse> responseList = restTemplate.exchange(url, HttpMethod.GET,
					requestEntity, ProjectApplicationResponse.class);
			HttpStatus statusCode = responseList.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				ProjectApplicationResponse projectAppRespObj = responseList.getBody();
				return projectAppRespObj;
			} else {
				LOGGER.error(
						"Error while calling XMSYSTEM Get Project Application By userId and projectId REST Service, returns the status code: " //$NON-NLS-1$
								+ statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException) e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			} else if (e instanceof HttpStatusCodeException) {
				RestClientExpHandlerUtil.handleNoXMLObjectFoundException(LOGGER, (HttpStatusCodeException) e);
			}
		} catch (Exception e) {
			LOGGER.error(
					"Error while calling XMSYSTEM Get All Project Appliocation By userId and projectId REST Service", //$NON-NLS-1$
					e);
		}
		return null;
	}

	/**
	 * Gets the project app by project site AA rel type.
	 *
	 * @param siteId
	 *            {@link String}
	 * @param adminAreaId
	 *            {@link String}
	 * @param projectId
	 *            {@link String}
	 * @param relType
	 *            {@link String}
	 * @return the project app by project site AA rel type
	 */
	public ProjectApplicationResponse getProjectAppByProjectSiteAARelType(String siteId, String adminAreaId,
			String projectId, String relType) throws UnauthorizedAccessException {
		try {
			String url = new String(this.serviceUrl
					+ XMAdminRestClientConstants.GET_PROJECT_APP_BY_PROJECT_SITE_ASMINAREA_ID_AND_RELTYPE + "/" + siteId
					+ "/" + adminAreaId + "/" + projectId + "/" + relType);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<Object> requestEntity = new HttpEntity<Object>(headers);

			List<HttpMessageConverter<?>> messageConverters = new ArrayList<>();
			messageConverters.add(new MappingJackson2HttpMessageConverter());
			restTemplate.setMessageConverters(messageConverters);

			ResponseEntity<ProjectApplicationResponse> responseList = restTemplate.exchange(url, HttpMethod.GET,
					requestEntity, ProjectApplicationResponse.class);
			HttpStatus statusCode = responseList.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				ProjectApplicationResponse projectAppRespObj = responseList.getBody();
				return projectAppRespObj;
			} else {
				LOGGER.error(
						"Error while calling XMSYSTEM Get Project Application By siteId, adminAreaId, projectId and relType REST Service, returns the status code: " //$NON-NLS-1$
								+ statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException) e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			} else if (e instanceof HttpStatusCodeException) {
				RestClientExpHandlerUtil.handleNoXMLObjectFoundException(LOGGER, (HttpStatusCodeException) e);
			}
		} catch (Exception e) {
			LOGGER.error(
					"Error while calling XMSYSTEM Get All Project Appliocation By siteId, adminAreaId, projectId and relType REST Service", //$NON-NLS-1$
					e);
		}
		return null;
	}

	/**
	 * Gets the project app by AA id project id user name.
	 *
	 * @param adminAreaId
	 *            {@link String}
	 * @param projectId
	 *            {@link String}
	 * @return the project app by AA id project id user name
	 * @throws UnauthorizedAccessException
	 *             the unauthorized access exception
	 */
	public ProjectApplciationMenuWrapper getProjectAppByAAIdProjectIdUserName(String adminAreaId, String projectId)
			throws UnauthorizedAccessException {
		try {
			String url = new String(
					this.serviceUrl + XMAdminRestClientConstants.GET_PROJECT_APP_BY_AA_ID_PROJ_ID_AND_USERNAME + "/"
							+ adminAreaId + "/" + projectId);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<Object> requestEntity = new HttpEntity<Object>(headers);

			List<HttpMessageConverter<?>> messageConverters = new ArrayList<>();
			messageConverters.add(new MappingJackson2HttpMessageConverter());
			restTemplate.setMessageConverters(messageConverters);

			ResponseEntity<ProjectApplciationMenuWrapper> responseList = restTemplate.exchange(url, HttpMethod.GET,
					requestEntity, ProjectApplciationMenuWrapper.class);
			HttpStatus statusCode = responseList.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				ProjectApplciationMenuWrapper projectApplciationMenuWrapper = responseList.getBody();
				return projectApplciationMenuWrapper;
			} else {
				LOGGER.error(
						"Error while calling XMSYSTEM Get Project Application By adminAreaId, projectId and userName REST Service, returns the status code: " //$NON-NLS-1$
								+ statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException) e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			} else if (e instanceof HttpStatusCodeException) {
				RestClientExpHandlerUtil.handleNoXMLObjectFoundException(LOGGER, (HttpStatusCodeException) e);
			} else if (e instanceof ResourceAccessException) {
				throw e;
			}
		} catch (Exception e) {
			LOGGER.error(
					"Error while calling XMSYSTEM Get All Project Appliocation By adminAreaId, projectId and userName REST Service", //$NON-NLS-1$
					e);
		}
		return null;
	}
	
	/**
	 * Get project app by project app name
	 * 
	 * @param projectAppName
	 * @return
	 * @throws UnauthorizedAccessException
	 */
	public ProjectApplicationsTbl getProjectAppByName(final String projectAppName) throws UnauthorizedAccessException {
		try {
			String url = new String(this.serviceUrl + XMAdminRestClientConstants.GET_PROJECT_APP_BY_NAME + "/" + projectAppName);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<Object> requestEntity = new HttpEntity<Object>(headers);

			List<HttpMessageConverter<?>> messageConverters = new ArrayList<>();
			messageConverters.add(new MappingJackson2HttpMessageConverter());
			restTemplate.setMessageConverters(messageConverters);

			ResponseEntity<ProjectApplicationsTbl> responseList = restTemplate.exchange(url, HttpMethod.GET,
					requestEntity, ProjectApplicationsTbl.class);
			HttpStatus statusCode = responseList.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				ProjectApplicationsTbl projectAppTblObj = responseList.getBody();
				return projectAppTblObj;
			} else {
				LOGGER.error(
						"Error while calling XMSYSTEM Get Project Application By Name REST Service, returns the status code: " //$NON-NLS-1$
								+ statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException) e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			} else if (e instanceof HttpStatusCodeException) {
				RestClientExpHandlerUtil.handleNoXMLObjectFoundException(LOGGER, (HttpStatusCodeException) e);
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM Get All Project Application By Name REST Service", e); //$NON-NLS-1$
		}
		return null;
	}
	
	/**
	 * Gets the project app by project app name.
	 *
	 * @param projectAppName the project app name
	 * @return the project app by project app name
	 * @throws UnauthorizedAccessException the unauthorized access exception
	 */
	public ProjectApplciationMenuWrapper getProjectAppByProjectAppName(final String projectAppName)
			throws UnauthorizedAccessException {
		try {
			String url = new String(this.serviceUrl + XMAdminRestClientConstants.GET_PROJECT_APP_BY_PROJECT_APP_NAME + "/" + projectAppName);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<Object> requestEntity = new HttpEntity<Object>(headers);

			List<HttpMessageConverter<?>> messageConverters = new ArrayList<>();
			messageConverters.add(new MappingJackson2HttpMessageConverter());
			restTemplate.setMessageConverters(messageConverters);

			ResponseEntity<ProjectApplciationMenuWrapper> responseList = restTemplate.exchange(url, HttpMethod.GET,
					requestEntity, ProjectApplciationMenuWrapper.class);
			HttpStatus statusCode = responseList.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				ProjectApplciationMenuWrapper projectApplciationMenuWrapper = responseList.getBody();
				return projectApplciationMenuWrapper;
			} else {
				LOGGER.error(
						"Error while calling XMSYSTEM Get Project Application By projectApp name REST Service, returns the status code: " //$NON-NLS-1$
								+ statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException) e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			} else if (e instanceof HttpStatusCodeException) {
				RestClientExpHandlerUtil.handleNoXMLObjectFoundException(LOGGER, (HttpStatusCodeException) e);
			} else if (e instanceof ResourceAccessException) {
				throw e;
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM Get All Project Appliocation By projectApp name REST Service", e); //$NON-NLS-1$
		}
		return null;
	}
	
}
