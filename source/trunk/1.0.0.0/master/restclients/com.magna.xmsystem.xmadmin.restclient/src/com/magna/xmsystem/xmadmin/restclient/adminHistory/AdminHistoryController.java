package com.magna.xmsystem.xmadmin.restclient.adminHistory;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import com.magna.xmbackend.vo.adminHistory.AdminHistoryRequest;
import com.magna.xmbackend.vo.adminHistory.AdminHistoryResponse;
import com.magna.xmsystem.dependencies.customExceptions.UnauthorizedAccessException;
import com.magna.xmsystem.xmadmin.restclient.RestController;
import com.magna.xmsystem.xmadmin.restclient.util.RestClientExpHandlerUtil;
import com.magna.xmsystem.xmadmin.restclient.util.XMAdminRestClientConstants;

/**
 * The Class AdminHistoryController.
 * 
 * @author subash.janarthanan
 * 
 */
public class AdminHistoryController extends RestController{
	
	/** The Constant LOGGER. */
	private static final Logger LOGGER = LoggerFactory.getLogger(AdminHistoryController.class);
	
	/**
	 * Instantiates a new admin history controller.
	 */
	public AdminHistoryController() {
		super();
	}
	
	/**
	 * Gets the all admin base history.
	 *
	 * @param adminHistoryRequest the admin history request
	 * @return the all admin base history
	 * @throws UnauthorizedAccessException the unauthorized access exception
	 */
	public AdminHistoryResponse getAllAdminBaseHistory(final AdminHistoryRequest adminHistoryRequest) throws UnauthorizedAccessException {
		try {
			String url = new String(this.serviceUrl + XMAdminRestClientConstants.GET_ALL_ADMIN_BASE_HISTORY);
			RestTemplate restTemplate = new RestTemplate();
			final HttpEntity<AdminHistoryRequest> requestEntity = new HttpEntity<AdminHistoryRequest>(adminHistoryRequest, this.headers);

			List<HttpMessageConverter<?>> messageConverters = new ArrayList<>();
			messageConverters.add(new MappingJackson2HttpMessageConverter());
			restTemplate.setMessageConverters(messageConverters);

			ResponseEntity<AdminHistoryResponse> responseList = restTemplate.exchange(url, HttpMethod.POST, requestEntity, AdminHistoryResponse.class);
			HttpStatus statusCode = responseList.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				AdminHistoryResponse adminHistoryResponse = responseList.getBody();
				return adminHistoryResponse;
			} else {
				LOGGER.error("Error while calling XMSYSTEM Get All Admin base history REST Service, returns the status code: " //$NON-NLS-1$
						+ statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException)e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			} else if (e instanceof HttpStatusCodeException) {
				RestClientExpHandlerUtil.handleNoXMLObjectFoundException(LOGGER, (HttpStatusCodeException) e);
			}else if (e instanceof ResourceAccessException) {
				throw e;
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM Get All Admin relation history userHistory REST Service", e); //$NON-NLS-1$
		}
		
		return null;
	}
	
	/**
	 * Gets the all admin relation history.
	 *
	 * @param adminHistoryRequest the admin history request
	 * @return the all admin relation history
	 * @throws UnauthorizedAccessException the unauthorized access exception
	 */
	public AdminHistoryResponse getAllAdminRelationHistory(final AdminHistoryRequest adminHistoryRequest) throws UnauthorizedAccessException {
		try {
			String url = new String(this.serviceUrl + XMAdminRestClientConstants.GET_ALL_ADMIN_RELATION_HISTORY);
			RestTemplate restTemplate = new RestTemplate();
			final HttpEntity<AdminHistoryRequest> requestEntity = new HttpEntity<AdminHistoryRequest>(adminHistoryRequest, this.headers);

			List<HttpMessageConverter<?>> messageConverters = new ArrayList<>();
			messageConverters.add(new MappingJackson2HttpMessageConverter());
			restTemplate.setMessageConverters(messageConverters);

			ResponseEntity<AdminHistoryResponse> responseList = restTemplate.exchange(url, HttpMethod.POST, requestEntity, AdminHistoryResponse.class);
			HttpStatus statusCode = responseList.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				AdminHistoryResponse adminHistoryResponse = responseList.getBody();
				return adminHistoryResponse;
			} else {
				LOGGER.error("Error while calling XMSYSTEM Get All Admin relation history REST Service, returns the status code: " //$NON-NLS-1$
						+ statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException)e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			} else if (e instanceof HttpStatusCodeException) {
				RestClientExpHandlerUtil.handleNoXMLObjectFoundException(LOGGER, (HttpStatusCodeException) e);
			}else if (e instanceof ResourceAccessException) {
				throw e;
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM Get All Admin relation history userHistory REST Service", e); //$NON-NLS-1$
		}
		
		return null;
	}

}
