package com.piterion.security.manager;

import java.security.Key;
import java.util.Base64;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

// TODO: Auto-generated Javadoc
/**
 * The Class TicketManager.
 */
public final class TicketManager {

	/** The delimiter. */
	private final String delimiter = "@@@@@@@@@@@@@@@";
	
	/** The algo. */
	private final String ALGO = "AES";
	
	/** The key. */
	private final String KEY = "o84A7AZG9%#O$1pe";

	/**
	 * Validate ticket.
	 *
	 * @param ticket the ticket
	 * @return the user details
	 * @throws Exception 
	 */
	public UserDetails validateTicket(final String ticket) throws Exception {
		if (ticket != null && !ticket.isEmpty()) {
			String decrypt = decrypt(ticket);
			if (decrypt != null) {
				String[] split = decrypt.split(delimiter);
				if (split.length == 2) {
					UserDetails userDetails = new UserDetails();
					userDetails.setUsername(split[0]);
					userDetails.setMachinename(split[1]);
					return userDetails;
				}
			}
		}

		return null;
	}
	
	/**
	 * Decrypt.
	 *
	 * @param encryptedData the encrypted data
	 * @return the string
	 */
	private String decrypt(String encryptedData) throws Exception {
		String decryptedValue = null;
		Key key = generateKey();
		Cipher c = Cipher.getInstance(ALGO);
		c.init(Cipher.DECRYPT_MODE, key);
		byte[] decordedValue = Base64.getDecoder().decode(encryptedData);
		byte[] decValue = c.doFinal(decordedValue);
		decryptedValue = new String(decValue);
		return decryptedValue;
	}

	/**
	 * Generate key.
	 *
	 * @return the key
	 * @throws Exception the exception
	 */
	private Key generateKey() throws Exception {
		Key key = new SecretKeySpec(KEY.getBytes(), ALGO);
		return key;
	}
}
