package com.piterion.security.manager;

// TODO: Auto-generated Javadoc
/**
 * The Class UserDetails.
 */
public final class UserDetails {

	/** The username. */
	private String username;

	/** The machinename. */
	private String machinename;

	/**
	 * Gets the username.
	 *
	 * @return the username
	 */
	public String getUsername() {
		return username;
	}

	/**
	 * Sets the username.
	 *
	 * @param username
	 *            the new username
	 */
	public void setUsername(String username) {
		this.username = username;
	}

	/**
	 * Gets the machinename.
	 *
	 * @return the machinename
	 */
	public String getMachinename() {
		return machinename;
	}

	/**
	 * Sets the machinename.
	 *
	 * @param machinename
	 *            the new machinename
	 */
	public void setMachinename(String machinename) {
		this.machinename = machinename;
	}
}
