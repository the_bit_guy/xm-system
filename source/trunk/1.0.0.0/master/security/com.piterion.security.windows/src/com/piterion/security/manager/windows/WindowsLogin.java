package com.piterion.security.manager.windows;

import com.sun.security.auth.module.NTSystem;

/**
 * The Class WindowsLogin.
 */
public class WindowsLogin {

	/**
	 * Gets the username.
	 *
	 * @return the username
	 */
	public String getUsername() {
		NTSystem ntSystem = new NTSystem();
		return ntSystem != null ? ntSystem.getName() : null;
	}
}
