package com.piterion.security.manager.linux;

import com.sun.security.auth.module.NTSystem;

/**
 * The Class WindowsLogin.
 */
public class UnixLogin {

	/**
	 * Gets the username.
	 *
	 * @return the username
	 */
	public String getUsername() {
		UnixSystem unixSystem = new UnixSystem();
		return unixSystem != null ? unixSystem.getUsername() : null;
	}
}
