package com.magna.xmsystem.datamigration.enums;

import java.io.Serializable;

/**
 * Enum for platforms.
 *
 * @author Chiranjeevi.Akula
 */
public enum PLATFORMS implements Serializable {

	WINDOWS32, WINDOWS64, LINUX64
}
