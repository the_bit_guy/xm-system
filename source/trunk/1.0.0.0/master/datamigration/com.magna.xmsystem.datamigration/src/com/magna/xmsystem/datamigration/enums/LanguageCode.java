package com.magna.xmsystem.datamigration.enums;

import java.io.Serializable;

/**
 * Enum for Language code.
 *
 * @author Chiranjeevi.Akula
 */
public enum LanguageCode implements Serializable {
	
	/** Member variable 'en' for {@link LanguageCode}. */
	en, 
 /** Member variable 'de' for {@link LanguageCode}. */
 de
}
