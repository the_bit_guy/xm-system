package com.magna.xmsystem.datamigration.vo.directory;

import java.util.ArrayList;
import java.util.List;

import com.magna.xmsystem.datamigration.util.GenerateUuid;

/**
 * Class for Directory.
 *
 * @author Chiranjeevi.Akula
 */
public class Directory {

	/** Member variable 'id' for {@link String}. */
	private String id;
	
	/** Member variable 'name' for {@link String}. */
	private String name;
	
	/** Member variable 'icon name' for {@link String}. */
	private String iconName;

	/** Member variable 'directory translations' for {@link List<DirectoryTranslation>}. */
	private List<DirectoryTranslation> directoryTranslations;

	/**
	 * Constructor for Directory Class.
	 */
	public Directory() {
		this.id = GenerateUuid.getUuid();
		this.directoryTranslations = new ArrayList<>();
	}

	/**
	 * Constructor for Directory Class.
	 *
	 * @param iconId {@link String}
	 * @param directoryTranslations {@link List<DirectoryTranslation>}
	 */
	public Directory(String iconId, List<DirectoryTranslation> directoryTranslations) {
		this.id = GenerateUuid.getUuid();
		this.iconName = iconId;
		this.directoryTranslations = directoryTranslations;
	}

	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * Sets the id.
	 *
	 * @param id the new id
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Sets the name.
	 *
	 * @param name the new name
	 */
	public void setName(String name) {
		this.name = name;
	}
	
	/**
	 * Gets the icon name.
	 *
	 * @return the icon name
	 */
	public String getIconName() {
		return iconName;
	}

	/**
	 * Sets the icon name.
	 *
	 * @param iconName the new icon name
	 */
	public void setIconName(String iconName) {
		this.iconName = iconName;
	}

	/**
	 * Gets the directory translations.
	 *
	 * @return the directory translations
	 */
	public List<DirectoryTranslation> getDirectoryTranslations() {
		return directoryTranslations;
	}

	/**
	 * Sets the directory translations.
	 *
	 * @param directoryTranslations the new directory translations
	 */
	public void setDirectoryTranslations(List<DirectoryTranslation> directoryTranslations) {
		this.directoryTranslations = directoryTranslations;
	}

	/**
	 * Sets the directory translation.
	 *
	 * @param directoryTranslation the new directory translation
	 */
	public void setDirectoryTranslation(DirectoryTranslation directoryTranslation) {
		this.directoryTranslations.add(directoryTranslation);
	}

}
