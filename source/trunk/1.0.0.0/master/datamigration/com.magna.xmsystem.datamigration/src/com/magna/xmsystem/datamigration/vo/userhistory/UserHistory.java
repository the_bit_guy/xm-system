package com.magna.xmsystem.datamigration.vo.userhistory;

import java.sql.Date;
import java.util.List;

import com.magna.xmsystem.datamigration.vo.user.UserTranslation;

/**
 * Class for User history.
 *
 * @author Chiranjeevi.Akula
 */
public class UserHistory {
	
	/** Member variable 'log time' for {@link Date}. */
	private Date logTime;

	/** Member variable 'username' for {@link String}. */
	private String username;

	/** Member variable 'host' for {@link String}. */
	private String host;

	/** Member variable 'site' for {@link String}. */
	private String site;

	/** Member variable 'admin area' for {@link String}. */
	private String adminArea;

	/** Member variable 'project' for {@link String}. */
	private String project;

	/** Member variable 'application' for {@link String}. */
	private String application;
	
	/** Member variable 'pid' for {@link Int}. */
	private int pid;
	
	/** Member variable 'result' for {@link String}. */
	private String result;
	
	/** Member variable 'args' for {@link String}. */
	private String args;
	
	/** Member variable 'is user status' for {@link Boolean}. */
	private boolean isUserStatus;

	/** Member variable 'user translations' for {@link List<UserTranslation>}. */
	private List<UserTranslation> userTranslations;

	/**
	 * Constructor for UserHistory Class.
	 */
	public UserHistory() {
		
	}

	/**
	 * Gets the log time.
	 *
	 * @return the log time
	 */
	public Date getLogTime() {
		return logTime;
	}

	/**
	 * Sets the log time.
	 *
	 * @param logTime the new log time
	 */
	public void setLogTime(Date logTime) {
		this.logTime = logTime;
	}

	/**
	 * Gets the username.
	 *
	 * @return the username
	 */
	public String getUsername() {
		return username;
	}

	/**
	 * Sets the username.
	 *
	 * @param username the new username
	 */
	public void setUsername(String username) {
		this.username = username;
	}

	/**
	 * Gets the host.
	 *
	 * @return the host
	 */
	public String getHost() {
		return host;
	}

	/**
	 * Sets the host.
	 *
	 * @param host the new host
	 */
	public void setHost(String host) {
		this.host = host;
	}

	/**
	 * Gets the site.
	 *
	 * @return the site
	 */
	public String getSite() {
		return site;
	}

	/**
	 * Sets the site.
	 *
	 * @param site the new site
	 */
	public void setSite(String site) {
		this.site = site;
	}

	/**
	 * Gets the admin area.
	 *
	 * @return the admin area
	 */
	public String getAdminArea() {
		return adminArea;
	}

	/**
	 * Sets the admin area.
	 *
	 * @param adminArea the new admin area
	 */
	public void setAdminArea(String adminArea) {
		this.adminArea = adminArea;
	}

	/**
	 * Gets the project.
	 *
	 * @return the project
	 */
	public String getProject() {
		return project;
	}

	/**
	 * Sets the project.
	 *
	 * @param project the new project
	 */
	public void setProject(String project) {
		this.project = project;
	}

	/**
	 * Gets the application.
	 *
	 * @return the application
	 */
	public String getApplication() {
		return application;
	}

	/**
	 * Sets the application.
	 *
	 * @param application the new application
	 */
	public void setApplication(String application) {
		this.application = application;
	}

	/**
	 * Gets the pid.
	 *
	 * @return the pid
	 */
	public int getPid() {
		return pid;
	}

	/**
	 * Sets the pid.
	 *
	 * @param pid the new pid
	 */
	public void setPid(int pid) {
		this.pid = pid;
	}

	/**
	 * Gets the result.
	 *
	 * @return the result
	 */
	public String getResult() {
		return result;
	}

	/**
	 * Sets the result.
	 *
	 * @param result the new result
	 */
	public void setResult(String result) {
		this.result = result;
	}

	/**
	 * Gets the args.
	 *
	 * @return the args
	 */
	public String getArgs() {
		return args;
	}

	/**
	 * Sets the args.
	 *
	 * @param args the new args
	 */
	public void setArgs(String args) {
		this.args = args;
	}

	/**
	 * Checks if is user status.
	 *
	 * @return true, if is user status
	 */
	public boolean isUserStatus() {
		return isUserStatus;
	}

	/**
	 * Sets the user status.
	 *
	 * @param isUserStatus the new user status
	 */
	public void setUserStatus(boolean isUserStatus) {
		this.isUserStatus = isUserStatus;
	}

	/**
	 * Gets the user translations.
	 *
	 * @return the user translations
	 */
	public List<UserTranslation> getUserTranslations() {
		return userTranslations;
	}

	/**
	 * Sets the user translations.
	 *
	 * @param userTranslations the new user translations
	 */
	public void setUserTranslations(List<UserTranslation> userTranslations) {
		this.userTranslations = userTranslations;
	}	
}
