package com.magna.xmsystem.datamigration.enums;

/**
 * Enum for Application relation type.
 *
 * @author Chiranjeevi.Akula
 */
public enum ApplicationRelationType {
	
	NOTFIXED, FIXED, PROTECTED
}
