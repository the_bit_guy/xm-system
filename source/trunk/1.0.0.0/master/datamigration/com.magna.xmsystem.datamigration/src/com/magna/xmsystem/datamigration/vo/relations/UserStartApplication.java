package com.magna.xmsystem.datamigration.vo.relations;

import com.magna.xmsystem.datamigration.enums.Status;
import com.magna.xmsystem.datamigration.util.GenerateUuid;

/**
 * Class for User start application.
 *
 * @author Chiranjeevi.Akula
 */
public class UserStartApplication {

	/** Member variable 'id' for {@link String}. */
	private String id;

	/** Member variable 'user id' for {@link String}. */
	private String userId;

	/** Member variable 'start app id' for {@link String}. */
	private String startAppId;

	/** Member variable 'status' for {@link String}. */
	private String status;

	/**
	 * Constructor for UserStartApplication Class.
	 */
	public UserStartApplication() {
		this.id = GenerateUuid.getUuid();
	}

	/**
	 * Constructor for UserStartApplication Class.
	 *
	 * @param userId
	 *            {@link String}
	 * @param startAppId
	 *            {@link String}
	 * @param status
	 *            {@link String}
	 */
	public UserStartApplication(String userId, String startAppId, String status) {
		this.id = GenerateUuid.getUuid();
		this.userId = userId;
		this.startAppId = startAppId;
		this.status = status;
	}

	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * Sets the id.
	 *
	 * @param id
	 *            the new id
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * Gets the user id.
	 *
	 * @return the user id
	 */
	public String getUserId() {
		return userId;
	}

	/**
	 * Sets the user id.
	 *
	 * @param userId
	 *            the new user id
	 */
	public void setUserId(String userId) {
		this.userId = userId;
	}

	/**
	 * Gets the start app id.
	 *
	 * @return the start app id
	 */
	public String getStartAppId() {
		return startAppId;
	}

	/**
	 * Sets the start app id.
	 *
	 * @param startAppId
	 *            the new start app id
	 */
	public void setStartAppId(String startAppId) {
		this.startAppId = startAppId;
	}

	/**
	 * Gets the status.
	 *
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * Sets the status.
	 *
	 * @param statusCode
	 *            the new status
	 */
	public void setStatus(String statusCode) {
		if ("1".equals(statusCode)) {
			this.status = Status.ACTIVE.toString();
		} else {
			this.status = Status.INACTIVE.toString();
		}
	}
}
