package com.magna.xmsystem.datamigration.enums;

import java.io.Serializable;

/**
 * Enum for Status.
 *
 * @author Chiranjeevi.Akula
 */
public enum Status implements Serializable {

	ACTIVE, INACTIVE, DELETED
}
