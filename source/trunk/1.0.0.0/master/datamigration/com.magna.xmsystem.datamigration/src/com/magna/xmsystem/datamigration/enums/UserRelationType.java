package com.magna.xmsystem.datamigration.enums;

/**
 * Enum for User relation type.
 *
 * @author Chiranjeevi.Akula
 */
public enum UserRelationType {
    ALLOWED, NOTALLOWED
}
