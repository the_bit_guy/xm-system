package com.magna.caxsystem.datamigration.vo.relations;

import com.magna.caxsystem.datamigration.enums.UserRelationType;
import com.magna.caxsystem.datamigration.util.GenerateUuid;

/**
 * Class for User admin area user application.
 *
 * @author Chiranjeevi.Akula
 */
public class UserAdminAreaUserApplication {

	/** Member variable 'id' for {@link String}. */
	private String id;

	/** Member variable 'user id' for {@link String}. */
	private String userId;

	/** Member variable 'site admin area id' for {@link String}. */
	private String siteAdminAreaId;
	
	/** Member variable 'user app id' for {@link String}. */
	private String userAppId;

	/** Member variable 'rel type' for {@link String}. */
	private String relType;

	/**
	 * Constructor for UserAdminAreaUserApplication Class.
	 */
	public UserAdminAreaUserApplication() {
		this.id = GenerateUuid.getUuid();
	}

	/**
	 * Constructor for UserAdminAreaUserApplication Class.
	 *
	 * @param userId {@link String}
	 * @param siteAdminAreaId {@link String}
	 * @param userAppId {@link String}
	 * @param relType {@link String}
	 */
	public UserAdminAreaUserApplication(String userId, String siteAdminAreaId, String userAppId, String relType) {
		this.id = GenerateUuid.getUuid();
		this.userId = userId;
		this.siteAdminAreaId = siteAdminAreaId;
		this.userAppId = userAppId;
		this.relType = relType;
	}

	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * Sets the id.
	 *
	 * @param id the new id
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * Gets the user id.
	 *
	 * @return the user id
	 */
	public String getUserId() {
		return userId;
	}

	/**
	 * Sets the user id.
	 *
	 * @param userId the new user id
	 */
	public void setUserId(String userId) {
		this.userId = userId;
	}

	/**
	 * Gets the site admin area id.
	 *
	 * @return the site admin area id
	 */
	public String getSiteAdminAreaId() {
		return siteAdminAreaId;
	}

	/**
	 * Sets the site admin area id.
	 *
	 * @param siteAdminAreaId the new site admin area id
	 */
	public void setSiteAdminAreaId(String siteAdminAreaId) {
		this.siteAdminAreaId = siteAdminAreaId;
	}

	/**
	 * Gets the user app id.
	 *
	 * @return the user app id
	 */
	public String getUserAppId() {
		return userAppId;
	}

	/**
	 * Sets the user app id.
	 *
	 * @param userAppId the new user app id
	 */
	public void setUserAppId(String userAppId) {
		this.userAppId = userAppId;
	}

	/**
	 * Gets the rel type.
	 *
	 * @return the rel type
	 */
	public String getRelType() {
		return relType;
	}

	/**
	 * Sets the rel type.
	 *
	 * @param relCode the new rel type
	 */
	public void setRelType(String relCode) {
		if ("1".equals(relCode)) {
			this.relType = UserRelationType.NOTALLOWED.toString();
		} else {
			this.relType = UserRelationType.ALLOWED.toString();
		}
	}
}
