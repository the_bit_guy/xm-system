package com.magna.caxsystem.datamigration.vo.directory;

import com.magna.caxsystem.datamigration.util.GenerateUuid;

/**
 * Class for Directory translation.
 *
 * @author Chiranjeevi.Akula
 */
public class DirectoryTranslation {

	/** Member variable 'id' for {@link String}. */
	private String id;

	/** Member variable 'description' for {@link String}. */
	private String description;

	/** Member variable 'remarks' for {@link String}. */
	private String remarks;

	/** Member variable 'language code' for {@link String}. */
	private String languageCode;

	/**
	 * Constructor for DirectoryTranslation Class.
	 *
	 * @param languageCode {@link String}
	 */
	public DirectoryTranslation(String languageCode) {
		this.id = GenerateUuid.getUuid();
		this.languageCode = languageCode;
	}

	/**
	 * Constructor for DirectoryTranslation Class.
	 *
	 * @param description {@link String}
	 * @param remarks {@link String}
	 * @param languageCode {@link String}
	 */
	public DirectoryTranslation(String description, String remarks, String languageCode) {
		this.id = GenerateUuid.getUuid();
		this.description = description;
		this.remarks = remarks;
		this.languageCode = languageCode;
	}

	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * Sets the id.
	 *
	 * @param id the new id
	 */
	public void setId(String id) {
		this.id = id;
	}	

	/**
	 * Gets the description.
	 *
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Sets the description.
	 *
	 * @param description the new description
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * Gets the remarks.
	 *
	 * @return the remarks
	 */
	public String getRemarks() {
		return remarks;
	}

	/**
	 * Sets the remarks.
	 *
	 * @param remarks the new remarks
	 */
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	/**
	 * Gets the language code.
	 *
	 * @return the language code
	 */
	public String getLanguageCode() {
		return languageCode;
	}

	/**
	 * Sets the language code.
	 *
	 * @param languageCode the new language code
	 */
	public void setLanguageCode(String languageCode) {
		this.languageCode = languageCode;
	}
}
