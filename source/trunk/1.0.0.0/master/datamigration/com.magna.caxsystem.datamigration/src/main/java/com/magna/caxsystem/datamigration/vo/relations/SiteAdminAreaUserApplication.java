package com.magna.caxsystem.datamigration.vo.relations;

import com.magna.caxsystem.datamigration.enums.ApplicationRelationType;
import com.magna.caxsystem.datamigration.enums.Status;
import com.magna.caxsystem.datamigration.util.GenerateUuid;

/**
 * Class for Site admin area user application.
 *
 * @author Chiranjeevi.Akula
 */
public class SiteAdminAreaUserApplication {

	/** Member variable 'id' for {@link String}. */
	private String id;

	/** Member variable 'site admin area id' for {@link String}. */
	private String siteAdminAreaId;
	
	/** Member variable 'user application id' for {@link String}. */
	private String userApplicationId;
	
	/** Member variable 'status' for {@link String}. */
	private String status;
	
	/** Member variable 'rel type' for {@link String}. */
	private String relType;
	
	/**
	 * Constructor for SiteAdminAreaUserApplication Class.
	 */
	public SiteAdminAreaUserApplication() {
		this.id = GenerateUuid.getUuid();
	}

	/**
	 * Constructor for SiteAdminAreaUserApplication Class.
	 *
	 * @param siteAdminAreaId {@link String}
	 * @param userApplicationId {@link String}
	 * @param status {@link String}
	 * @param relType {@link String}
	 */
	public SiteAdminAreaUserApplication(String siteAdminAreaId, String userApplicationId, String status, String relType) {
		this.id = GenerateUuid.getUuid();
		this.siteAdminAreaId = siteAdminAreaId;
		this.userApplicationId = userApplicationId;
		this.status = status;
		this.relType = relType;		
	}	

	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * Sets the id.
	 *
	 * @param id the new id
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * Gets the site admin area id.
	 *
	 * @return the site admin area id
	 */
	public String getSiteAdminAreaId() {
		return siteAdminAreaId;
	}

	/**
	 * Sets the site admin area id.
	 *
	 * @param siteAdminAreaId the new site admin area id
	 */
	public void setSiteAdminAreaId(String siteAdminAreaId) {
		this.siteAdminAreaId = siteAdminAreaId;
	}

	

	/**
	 * Gets the user application id.
	 *
	 * @return the user application id
	 */
	public String getUserApplicationId() {
		return userApplicationId;
	}

	/**
	 * Sets the user application id.
	 *
	 * @param userApplicationId the new user application id
	 */
	public void setUserApplicationId(String userApplicationId) {
		this.userApplicationId = userApplicationId;
	}

	/**
	 * Gets the status.
	 *
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * Sets the status.
	 *
	 * @param statusCode the new status
	 */
	public void setStatus(String statusCode) {
		if ("1".equals(statusCode)) {
			this.status = Status.ACTIVE.toString();
		} else {
			this.status = Status.INACTIVE.toString();
		}
	}

	/**
	 * Gets the rel type.
	 *
	 * @return the rel type
	 */
	public String getRelType() {
		return relType;
	}

	/**
	 * Sets the rel type.
	 *
	 * @param relCode the new rel type
	 */
	public void setRelType(String relCode) {
		if ("1".equals(relCode)) {
			this.relType = ApplicationRelationType.FIXED.toString();
		} else if ("2".equals(relCode)) {
			this.relType = ApplicationRelationType.PROTECTED.toString();
		} else {
			this.relType = ApplicationRelationType.NOTFIXED.toString();
		}
	}
}
