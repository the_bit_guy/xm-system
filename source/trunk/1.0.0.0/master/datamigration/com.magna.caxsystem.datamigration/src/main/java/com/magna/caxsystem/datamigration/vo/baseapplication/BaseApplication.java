package com.magna.caxsystem.datamigration.vo.baseapplication;

import java.util.ArrayList;
import java.util.List;

import com.magna.caxsystem.datamigration.enums.Status;
import com.magna.caxsystem.datamigration.util.GenerateUuid;

/**
 * Class for Base application.
 *
 * @author Chiranjeevi.Akula
 */
public class BaseApplication {

	/** Member variable 'id' for {@link String}. */
	private String id;

	/** Member variable 'name' for {@link String}. */
	private String name;
	
	/** Member variable 'status' for {@link String}. */
	private String status;

	/** Member variable 'icon name' for {@link String}. */
	private String iconName;

	/** Member variable 'platforms' for {@link String}. */
	private String platforms;

	/** Member variable 'program' for {@link String}. */
	private String program;

	/** Member variable 'base app translations' for {@link List<BaseAppTranslation>}. */
	private List<BaseAppTranslation> baseAppTranslations;

	/**
	 * Constructor for BaseApplication Class.
	 */
	public BaseApplication() {
		this.id = GenerateUuid.getUuid();
		this.baseAppTranslations = new ArrayList<>();
	}

	/**
	 * Constructor for BaseApplication Class.
	 *
	 * @param name {@link String}
	 * @param status {@link String}
	 * @param iconId {@link String}
	 * @param baseAppTranslations {@link List<BaseAppTranslation>}
	 */
	public BaseApplication(String name, String status, String iconId, List<BaseAppTranslation> baseAppTranslations) {
		this.id = GenerateUuid.getUuid();
		this.name = name;
		this.status = status;
		this.iconName = iconId;
		this.baseAppTranslations = baseAppTranslations;
	}

	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * Sets the id.
	 *
	 * @param id the new id
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Sets the name.
	 *
	 * @param name the new name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Gets the status.
	 *
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * Sets the status.
	 *
	 * @param statusCode the new status
	 */
	public void setStatus(int statusCode) {
		if (statusCode == 1) {
			this.status = Status.ACTIVE.toString();
		} else {
			this.status = Status.INACTIVE.toString();
		}
	}

	/**
	 * Gets the icon name.
	 *
	 * @return the icon name
	 */
	public String getIconName() {
		return iconName;
	}

	/**
	 * Sets the icon name.
	 *
	 * @param iconName the new icon name
	 */
	public void setIconName(String iconName) {
		this.iconName = iconName;
	}

	/**
	 * Gets the base app translations.
	 *
	 * @return the base app translations
	 */
	public List<BaseAppTranslation> getBaseAppTranslations() {
		return baseAppTranslations;
	}

	/**
	 * Sets the base app translation.
	 *
	 * @param baseAppTranslation the new base app translation
	 */
	public void setBaseAppTranslation(BaseAppTranslation baseAppTranslation) {
		this.baseAppTranslations.add(baseAppTranslation);
	}

	/**
	 * Sets the base app translations.
	 *
	 * @param baseAppTranslations the new base app translations
	 */
	public void setBaseAppTranslations(List<BaseAppTranslation> baseAppTranslations) {
		this.baseAppTranslations = baseAppTranslations;
	}

	/**
	 * Gets the platforms.
	 *
	 * @return the platforms
	 */
	public String getPlatforms() {
		return platforms;
	}

	/**
	 * Sets the platforms.
	 *
	 * @param platforms the new platforms
	 */
	public void setPlatforms(String platforms) {
		this.platforms = platforms;
	}

	/**
	 * Gets the program.
	 *
	 * @return the program
	 */
	public String getProgram() {
		return program;
	}

	/**
	 * Sets the program.
	 *
	 * @param program the new program
	 */
	public void setProgram(String program) {
		this.program = program;
	}
}
