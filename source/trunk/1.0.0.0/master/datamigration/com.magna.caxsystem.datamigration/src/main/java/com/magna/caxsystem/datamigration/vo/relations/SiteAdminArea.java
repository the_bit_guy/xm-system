package com.magna.caxsystem.datamigration.vo.relations;

import com.magna.caxsystem.datamigration.enums.Status;
import com.magna.caxsystem.datamigration.util.GenerateUuid;

/**
 * Class for Site admin area.
 *
 * @author Chiranjeevi.Akula
 */
public class SiteAdminArea {

	/** Member variable 'id' for {@link String}. */
	private String id;

	/** Member variable 'site id' for {@link String}. */
	private String siteId;
	
	/** Member variable 'admin area id' for {@link String}. */
	private String adminAreaId;
	
	/** Member variable 'status' for {@link String}. */
	private String status;
	
	/**
	 * Constructor for SiteAdminArea Class.
	 */
	public SiteAdminArea() {
		this.id = GenerateUuid.getUuid();
	}

	/**
	 * Constructor for SiteAdminArea Class.
	 *
	 * @param siteId {@link String}
	 * @param adminAreaId {@link String}
	 * @param status {@link String}
	 */
	public SiteAdminArea(String siteId, String adminAreaId, String status) {
		this.id = GenerateUuid.getUuid();
		this.siteId = siteId;
		this.adminAreaId = adminAreaId;
		this.status = status;
	}

	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * Sets the id.
	 *
	 * @param id the new id
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * Gets the site id.
	 *
	 * @return the site id
	 */
	public String getSiteId() {
		return siteId;
	}

	/**
	 * Sets the site id.
	 *
	 * @param siteId the new site id
	 */
	public void setSiteId(String siteId) {
		this.siteId = siteId;
	}

	/**
	 * Gets the admin area id.
	 *
	 * @return the admin area id
	 */
	public String getAdminAreaId() {
		return adminAreaId;
	}

	/**
	 * Sets the admin area id.
	 *
	 * @param adminAreaId the new admin area id
	 */
	public void setAdminAreaId(String adminAreaId) {
		this.adminAreaId = adminAreaId;
	}

	/**
	 * Gets the status.
	 *
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * Sets the status.
	 *
	 * @param statusCode the new status
	 */
	public void setStatus(String statusCode) {
		if ("1".equals(statusCode)) {
			this.status = Status.ACTIVE.toString();
		} else {
			this.status = Status.INACTIVE.toString();
		}
	}

}
