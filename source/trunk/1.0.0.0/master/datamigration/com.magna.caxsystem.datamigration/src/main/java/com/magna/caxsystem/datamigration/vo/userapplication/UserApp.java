package com.magna.caxsystem.datamigration.vo.userapplication;

import java.util.ArrayList;
import java.util.List;

import com.magna.caxsystem.datamigration.enums.Status;
import com.magna.caxsystem.datamigration.util.GenerateUuid;

/**
 * Class for User app.
 *
 * @author Chiranjeevi.Akula
 */
public class UserApp {

	/** Member variable 'id' for {@link String}. */
	private String id;

	 /** Member variable 'name' for {@link String}. */
    private String name;
    
    /** Member variable 'description' for {@link String}. */
    private String description;
    
	/** Member variable 'status' for {@link String}. */
	private String status;

	/** Member variable 'icon name' for {@link String}. */
	private String iconName;

	/** Member variable 'base app name' for {@link String}. */
	private String baseAppName;

	/** Member variable 'is parent' for {@link String}. */
	private String isParent;

	/** Member variable 'is singleton' for {@link String}. */
	private String isSingleton;

	/** Member variable 'position' for {@link String}. */
	private String position;

	/** Member variable 'user app translations' for {@link List<UserAppTranslation>}. */
	private List<UserAppTranslation> userAppTranslations;

	/**
	 * Constructor for UserApp Class.
	 */
	public UserApp() {
		this.id = GenerateUuid.getUuid();
		this.userAppTranslations = new ArrayList<>();
	}

	/**
	 * Constructor for UserApp Class.
	 *
	 * @param status {@link String}
	 * @param iconId {@link String}
	 * @param userAppTranslations {@link List<UserAppTranslation>}
	 */
	public UserApp(String name, String description, String status, String iconId, List<UserAppTranslation> userAppTranslations) {
		this.id = GenerateUuid.getUuid();
		this.name = name;
		this.description = description;
		this.status = status;
		this.iconName = iconId;
		this.userAppTranslations = userAppTranslations;
	}

	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * Sets the id.
	 *
	 * @param id the new id
	 */
	public void setId(String id) {
		this.id = id;
	}

	 /**
     * Gets the name.
     *
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the name.
     *
     * @param name the new name
     */
    public void setName(String name) {
        this.name = name;
    }
    
    /**
     * Gets the description.
     *
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the description.
     *
     * @param description the new description
     */
    public void setDescription(String description) {
        this.description = description;
    }
    
	/**
	 * Gets the status.
	 *
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * Sets the status.
	 *
	 * @param statusCode the new status
	 */
	public void setStatus(int statusCode) {
		if (statusCode == 1) {
			this.status = Status.ACTIVE.toString();
		} else {
			this.status = Status.INACTIVE.toString();
		}
	}

	/**
	 * Gets the icon name.
	 *
	 * @return the icon name
	 */
	public String getIconName() {
		return iconName;
	}

	/**
	 * Sets the icon name.
	 *
	 * @param iconName the new icon name
	 */
	public void setIconName(String iconName) {
		this.iconName = iconName;
	}

	/**
	 * Gets the base app name.
	 *
	 * @return the base app name
	 */
	public String getBaseAppName() {
		return baseAppName;
	}

	/**
	 * Sets the base app name.
	 *
	 * @param baseAppName the new base app name
	 */
	public void setBaseAppName(String baseAppName) {
		this.baseAppName = baseAppName;
	}

	/**
	 * Gets the checks if is parent.
	 *
	 * @return the checks if is parent
	 */
	public String getIsParent() {
		return isParent;
	}

	/**
	 * Sets the checks if is parent.
	 *
	 * @param isParent the new checks if is parent
	 */
	public void setIsParent(String isParent) {
		this.isParent = isParent;
	}

	/**
	 * Gets the checks if is singleton.
	 *
	 * @return the checks if is singleton
	 */
	public String getIsSingleton() {
		return isSingleton;
	}

	/**
	 * Sets the checks if is singleton.
	 *
	 * @param isSingleton the new checks if is singleton
	 */
	public void setIsSingleton(String isSingleton) {
		this.isSingleton = isSingleton;
	}

	/**
	 * Gets the position.
	 *
	 * @return the position
	 */
	public String getPosition() {
		return position;
	}

	/**
	 * Sets the position.
	 *
	 * @param position the new position
	 */
	public void setPosition(String position) {
		this.position = position;
	}

	/**
	 * Gets the user app translations.
	 *
	 * @return the user app translations
	 */
	public List<UserAppTranslation> getUserAppTranslations() {
		return userAppTranslations;
	}

	/**
	 * Sets the user app translation.
	 *
	 * @param userAppTranslation the new user app translation
	 */
	public void setUserAppTranslation(UserAppTranslation userAppTranslation) {
		this.userAppTranslations.add(userAppTranslation);
	}

	/**
	 * Sets the user app translations.
	 *
	 * @param userAppTranslations the new user app translations
	 */
	public void setUserAppTranslations(List<UserAppTranslation> userAppTranslations) {
		this.userAppTranslations = userAppTranslations;
	}

}
