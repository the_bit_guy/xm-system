package com.magna.caxsystem.datamigration.vo.relations;

import com.magna.caxsystem.datamigration.enums.Status;
import com.magna.caxsystem.datamigration.util.GenerateUuid;

/**
 * Class for Site admin area project.
 *
 * @author Chiranjeevi.Akula
 */
public class SiteAdminAreaProject {

	/** Member variable 'id' for {@link String}. */
	private String id;

	/** Member variable 'site admin area id' for {@link String}. */
	private String siteAdminAreaId;
	
	/** Member variable 'project id' for {@link String}. */
	private String projectId;
	
	/** Member variable 'status' for {@link String}. */
	private String status;
	
	/**
	 * Constructor for SiteAdminAreaProject Class.
	 */
	public SiteAdminAreaProject() {
		this.id = GenerateUuid.getUuid();
	}

	/**
	 * Constructor for SiteAdminAreaProject Class.
	 *
	 * @param siteAdminAreaId {@link String}
	 * @param projectId {@link String}
	 * @param status {@link String}
	 */
	public SiteAdminAreaProject(String siteAdminAreaId, String projectId, String status) {
		this.id = GenerateUuid.getUuid();
		this.siteAdminAreaId = siteAdminAreaId;
		this.projectId = projectId;
		this.status = status;
	}	

	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * Sets the id.
	 *
	 * @param id the new id
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * Gets the site admin area id.
	 *
	 * @return the site admin area id
	 */
	public String getSiteAdminAreaId() {
		return siteAdminAreaId;
	}

	/**
	 * Sets the site admin area id.
	 *
	 * @param siteAdminAreaId the new site admin area id
	 */
	public void setSiteAdminAreaId(String siteAdminAreaId) {
		this.siteAdminAreaId = siteAdminAreaId;
	}

	/**
	 * Gets the project id.
	 *
	 * @return the project id
	 */
	public String getProjectId() {
		return projectId;
	}

	/**
	 * Sets the project id.
	 *
	 * @param projectId the new project id
	 */
	public void setProjectId(String projectId) {
		this.projectId = projectId;
	}

	/**
	 * Gets the status.
	 *
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * Sets the status.
	 *
	 * @param statusCode the new status
	 */
	public void setStatus(String statusCode) {
		if ("1".equals(statusCode)) {
			this.status = Status.ACTIVE.toString();
		} else {
			this.status = Status.INACTIVE.toString();
		}
	}

}
