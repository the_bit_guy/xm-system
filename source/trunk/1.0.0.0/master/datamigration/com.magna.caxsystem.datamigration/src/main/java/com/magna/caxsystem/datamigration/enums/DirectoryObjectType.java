package com.magna.caxsystem.datamigration.enums;

/**
 * Enum for Directory object type.
 *
 * @author Chiranjeevi.Akula
 */
public enum DirectoryObjectType {
	USER,PROJECT,USERAPPLICATION,PROJECTAPPLICATION
}
