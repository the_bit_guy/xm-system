package com.magna.caxsystem.datamigration.jdbc;

import java.io.File;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.magna.caxsystem.datamigration.jdbc.newdb.NewDatabase;
import com.magna.caxsystem.datamigration.jdbc.olddb.OldDatabase;
import com.magna.caxsystem.datamigration.util.MigrationProperties;
import com.magna.caxsystem.datamigration.util.MigrationUtil;
import com.magna.caxsystem.datamigration.vo.adminarea.AdminArea;
import com.magna.caxsystem.datamigration.vo.baseapplication.BaseApplication;
import com.magna.caxsystem.datamigration.vo.directory.Directory;
import com.magna.caxsystem.datamigration.vo.directory.DirectoryObjectReference;
import com.magna.caxsystem.datamigration.vo.project.Project;
import com.magna.caxsystem.datamigration.vo.projectapplication.ProjectApp;
import com.magna.caxsystem.datamigration.vo.relations.SiteAdminArea;
import com.magna.caxsystem.datamigration.vo.relations.SiteAdminAreaProject;
import com.magna.caxsystem.datamigration.vo.relations.SiteAdminAreaProjectProjectApplication;
import com.magna.caxsystem.datamigration.vo.relations.SiteAdminAreaProjectStartApplication;
import com.magna.caxsystem.datamigration.vo.relations.SiteAdminAreaStartApplication;
import com.magna.caxsystem.datamigration.vo.relations.SiteAdminAreaUserApplication;
import com.magna.caxsystem.datamigration.vo.relations.UserAdminAreaProjectProjectApplication;
import com.magna.caxsystem.datamigration.vo.relations.UserAdminAreaUserApplication;
import com.magna.caxsystem.datamigration.vo.relations.UserProject;
import com.magna.caxsystem.datamigration.vo.relations.UserStartApplication;
import com.magna.caxsystem.datamigration.vo.site.Site;
import com.magna.caxsystem.datamigration.vo.startapplication.StartApp;
import com.magna.caxsystem.datamigration.vo.user.User;
import com.magna.caxsystem.datamigration.vo.userapplication.UserApp;
import com.magna.caxsystem.datamigration.vo.userhistory.UserHistory;

/**
 * Class for JDBC execution.
 *
 * @author Chiranjeevi.Akula
 */
public class JDBCExecution {
	
	/** The Constant LOGGER. */
	private static final Logger LOGGER = LoggerFactory.getLogger(JDBCExecution.class);
	
	/** Member variable 'propery instance' for {@link MigrationProperties}. */
	private static MigrationProperties properyInstance;
	
	/** The Constant JDBC_DRIVER. */
	private final static String JDBC_DRIVER = "oracle.jdbc.driver.OracleDriver";

	/** Member variable 'old db url' for {@link String}. */
	private static String OLD_DB_URL;
	
	/** Member variable 'old db username' for {@link String}. */
	private static String OLD_DB_USERNAME;
	
	/** Member variable 'old db password' for {@link String}. */
	private static String OLD_DB_PASSWORD;

	/** Member variable 'new db url' for {@link String}. */
	private static String NEW_DB_URL;
	
	/** Member variable 'new db username' for {@link String}. */
	private static String NEW_DB_USERNAME;
	
	/** Member variable 'new db password' for {@link String}. */
	private static String NEW_DB_PASSWORD;

	/**
	 * The main method.
	 *
	 * @param args the arguments
	 */
	public static void main(String[] args) {

		try {
			properyInstance = MigrationProperties.getInstance();			
			
			if (properyInstance != null) {
				
				final String EXCEL_PATH = properyInstance.getProperty("EXCEL_PATH");
				if(EXCEL_PATH == null) {
					LOGGER.error("Data migration failed. Missing EXCEL_PATH location in property configuration file...");
					System.exit(1);
				} else if(!(new File(EXCEL_PATH)).exists()) {
					LOGGER.error("Data migration failed. Given EXCEL_PATH location in not reachable...");
					System.exit(1);
				}
				
				MigrationUtil.getInstance().setExcelsPath(EXCEL_PATH);
				
				final String ICONS_PATH = properyInstance.getProperty("ICONS_PATH");
				if(ICONS_PATH == null) {
					LOGGER.error("Data migration failed. Missing ICONS_PATH location in property configuration file...");
					System.exit(1);
				} else if(!(new File(ICONS_PATH)).exists()) {
					LOGGER.error("Data migration failed. Given ICONS_PATH location in not reachable...");
					System.exit(1);
				}
				
				MigrationUtil.getInstance().setIconsPath(ICONS_PATH);
				
				Class.forName(JDBC_DRIVER);

				initOldDBDetails();
				initNewDBDetails();

				OldDatabase oldDatabase = new OldDatabase(OLD_DB_URL, OLD_DB_USERNAME, OLD_DB_PASSWORD);
				NewDatabase newDatabase = new NewDatabase(NEW_DB_URL, NEW_DB_USERNAME, NEW_DB_PASSWORD);

				LOGGER.info("Data Migration Started on " + (new Date()) + "....");

				if (newDatabase.clearTablesData()) {

					newDatabase.loadPropertyConfig();

					newDatabase.insertCustomIcons();

					oldDatabase.loadTranslationData();

					LOGGER.info("Migrating Base Data....");

					LOGGER.info("Importing Sites...");
					List<Site> sites = oldDatabase.getSiteData();
					if (!sites.isEmpty()) {
						newDatabase.insertSiteData(sites);
					}
					LOGGER.info("Importing Projects...");
					List<Project> projects = oldDatabase.getProjectData();
					if (!projects.isEmpty()) {
						newDatabase.insertProjectData(projects);
					}
					LOGGER.info("Importing Users...");
					List<User> users = oldDatabase.getUserData();
					if (!users.isEmpty()) {
						newDatabase.insertUserData(users);
					}
					LOGGER.info("Importing Base Applications...");
					List<BaseApplication> baseApplications = oldDatabase.getBaseAppData();
					if (!baseApplications.isEmpty()) {
						newDatabase.insertBaseAppData(baseApplications);
					}
					LOGGER.info("Importing User Applications...");
					List<UserApp> userApplications = oldDatabase.getUserApplicationData();
					if (!userApplications.isEmpty()) {
						newDatabase.insertUserApplicationData(userApplications);
					}
					LOGGER.info("Importing Project Applications...");
					List<ProjectApp> projectApplications = oldDatabase.getProjectApplicationData();
					if (!projectApplications.isEmpty()) {
						newDatabase.insertProjectApplicationData(projectApplications);
					}
					LOGGER.info("Importing Start Applications...");
					List<StartApp> startApplications = oldDatabase.getStartApplicationData();
					if (!startApplications.isEmpty()) {
						newDatabase.insertStartApplicationData(startApplications);
					}
					LOGGER.info("Importing Directories...");
					List<Directory> directories = oldDatabase.getDirectoryData();
					if (!directories.isEmpty()) {
						newDatabase.insertDirectoryData(directories);
					}

					LOGGER.info("Migrating Relation Data....");

					// Making Relations
					LOGGER.info("Importing Directory References...");
					List<DirectoryObjectReference> directoryObjectRefs = oldDatabase.getDirectoryObjectRefData();
					if (!directoryObjectRefs.isEmpty()) {
						newDatabase.insertDirectoryObjectRefData(directoryObjectRefs);
					}
					LOGGER.info("Importing Admins Menu Configuration References...");
					List<DirectoryObjectReference> adminMenuDirectoryObjectRefs = oldDatabase
							.getAdminMenuDirectoryObjectRefData();
					if (!adminMenuDirectoryObjectRefs.isEmpty()) {
						newDatabase.insertAdminMenuDirectoryObjectRefData(adminMenuDirectoryObjectRefs);
					}
					LOGGER.info("Importing User-Project Relations...");
					List<UserProject> userProjectRels = oldDatabase.getUserProjectRel();
					if (!userProjectRels.isEmpty()) {
						newDatabase.insertUserProjectRelData(userProjectRels);
					}
					LOGGER.info("Importing User-StartApplication Relations...");
					List<UserStartApplication> userStartAppRels = oldDatabase.getUserStartAppRel();
					if (!userStartAppRels.isEmpty()) {
						newDatabase.insertUserStartAppRelData(userStartAppRels);
					}

					LOGGER.info("Migrating Administration Area Based Relation Data....");

					// Making AA Based Relations
					List<AdminArea> adminAreas = newDatabase.getAdminAreaData();
					if (adminAreas != null && !adminAreas.isEmpty()) {
						LOGGER.info("Importing Site-AdministrationArea Relations...");
						List<SiteAdminArea> siteAARels = oldDatabase.getSiteAARel(adminAreas);
						if (!siteAARels.isEmpty()) {
							newDatabase.insertSiteAdminAreaRelData(siteAARels);
						}

						LOGGER.info("Importing Site-AdministrationArea-Project Relations...");
						List<SiteAdminAreaProject> siteAAProjectRels = oldDatabase.getSiteAAProjectRel(adminAreas);
						if (!siteAAProjectRels.isEmpty()) {
							newDatabase.insertSiteAdminAreaProjectRelData(siteAAProjectRels);
						}

						LOGGER.info("Importing Site-AdministrationArea-UserApplication Relations...");
						List<SiteAdminAreaUserApplication> siteAAUserAppRels = oldDatabase
								.getSiteAAUserAppRel(adminAreas);
						if (!siteAAUserAppRels.isEmpty()) {
							newDatabase.insertSiteAdminAreaUserAppRelData(siteAAUserAppRels);
						}

						LOGGER.info("Importing Site-AdministrationArea-StartApplication Relations...");
						List<SiteAdminAreaStartApplication> siteAAStartAppRels = oldDatabase
								.getSiteAAStartAppRel(adminAreas);
						if (!siteAAStartAppRels.isEmpty()) {
							newDatabase.insertSiteAdminAreaStartAppRelData(siteAAStartAppRels);
						}

						LOGGER.info("Importing Site-AdministrationArea-Project-ProjectApplication Relations...");
						List<SiteAdminAreaProjectProjectApplication> siteAAProjectProjectAppRels = oldDatabase
								.getSiteAAProjectProjectAppRel(adminAreas);
						if (!siteAAProjectProjectAppRels.isEmpty()) {
							newDatabase.insertSiteAdminAreaProjectProjectAppRelData(siteAAProjectProjectAppRels);
						}

						LOGGER.info("Importing Site-AdministrationArea-Project-StartApplication Relations...");
						List<SiteAdminAreaProjectStartApplication> siteAAProjectStartAppRels = oldDatabase
								.getSiteAAProjectStartAppRel(adminAreas);
						if (!siteAAProjectStartAppRels.isEmpty()) {
							newDatabase.insertSiteAdminAreaProjectStartAppRelData(siteAAProjectStartAppRels);
						}

						LOGGER.info("Importing User-Site-AdministrationArea-UserApplication Relations...");
						List<UserAdminAreaUserApplication> userSiteAAUserAppRels = oldDatabase
								.getUserSiteAAUserAppRel();
						if (!userSiteAAUserAppRels.isEmpty()) {
							newDatabase.insertUserSiteAdminAreaUserAppRelData(userSiteAAUserAppRels);
						}

						LOGGER.info("Importing User-Site-AdministrationArea-Project-ProjectApplication Relations...");
						List<UserAdminAreaProjectProjectApplication> userSiteAAProjectProjectAppRels = oldDatabase
								.getUserSiteAAProjectProjectAppRel();
						if (!userSiteAAProjectProjectAppRels.isEmpty()) {
							newDatabase
									.insertUserSiteAdminAreaProjectProjectAppRelData(userSiteAAProjectProjectAppRels);
						}
					}

					LOGGER.info("Migrating User History Data. Please wait, this may take a while....");

					List<UserHistory> userStatus = oldDatabase.getUserStatus();
					if (!userStatus.isEmpty()) {
						newDatabase.insertUserHistoryData(userStatus);
					}

					List<UserHistory> userHistory = oldDatabase.getUserHistory();
					if (!userHistory.isEmpty()) {
						newDatabase.insertUserHistoryData(userHistory);
					}
				}

				LOGGER.info("Data Migration Successful on " + (new Date()) + "....");
			}
		} catch (ClassNotFoundException e) {
			LOGGER.error(e.toString());
		}
	}

	/**
	 * Method for Inits the new DB details.
	 */
	private static void initNewDBDetails() {
		NEW_DB_URL = properyInstance.getProperty("NEW_DB_URL");
		NEW_DB_USERNAME = properyInstance.getProperty("NEW_DB_USERNAME");
		NEW_DB_PASSWORD = properyInstance.getProperty("NEW_DB_PASSWORD");
		
		if(NEW_DB_URL == null || NEW_DB_USERNAME == null || NEW_DB_PASSWORD == null) {
			LOGGER.error("Data migration failed. Missing new database details in property configuration file...");
			System.exit(1);
		}		
		
		/*NEW_DB_URL = "jdbc:oracle:thin:@192.168.1.107:1521:XE";
		NEW_DB_USERNAME = "magnadb";
		NEW_DB_PASSWORD = "magnadb";*/
		
		/*boolean isDebug = java.lang.management.ManagementFactory.getRuntimeMXBean().getInputArguments().toString()
				.indexOf("-agentlib:jdwp") > 0;
		System.out.println("Debug Mode : " + isDebug);*/
	}

	/**
	 * Method for Inits the old DB details.
	 */
	private static void initOldDBDetails() {
		OLD_DB_URL = properyInstance.getProperty("OLD_DB_URL");
		OLD_DB_USERNAME = properyInstance.getProperty("OLD_DB_USERNAME");
		OLD_DB_PASSWORD = properyInstance.getProperty("OLD_DB_PASSWORD");
		
		if(OLD_DB_URL == null || OLD_DB_USERNAME == null || OLD_DB_PASSWORD == null) {
			LOGGER.error("Data migration failed. Missing old database details in property configuration file...");
			System.exit(1);
		}
	}
}
