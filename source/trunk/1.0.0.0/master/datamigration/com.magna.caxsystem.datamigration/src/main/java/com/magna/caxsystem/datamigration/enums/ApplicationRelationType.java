package com.magna.caxsystem.datamigration.enums;

/**
 * Enum for Application relation type.
 *
 * @author Chiranjeevi.Akula
 */
public enum ApplicationRelationType {
	
	NOTFIXED, FIXED, PROTECTED
}
