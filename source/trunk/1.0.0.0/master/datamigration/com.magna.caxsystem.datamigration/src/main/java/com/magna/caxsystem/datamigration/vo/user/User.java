package com.magna.caxsystem.datamigration.vo.user;

import java.util.ArrayList;
import java.util.List;

import com.magna.caxsystem.datamigration.enums.Status;
import com.magna.caxsystem.datamigration.util.GenerateUuid;

/**
 * Class for User.
 *
 * @author Chiranjeevi.Akula
 */
public class User {

	/** Member variable 'id' for {@link String}. */
	private String id;

	/** Member variable 'user name' for {@link String}. */
	private String userName;

	/** Member variable 'full name' for {@link String}. */
	private String fullName;

	/** Member variable 'email' for {@link String}. */
	private String email;

	/** Member variable 'telephone' for {@link String}. */
	private String telephone;

	/** Member variable 'department' for {@link String}. */
	private String department;

	/** Member variable 'status' for {@link String}. */
	private String status;

	/** Member variable 'icon name' for {@link String}. */
	private String iconName;

	/** Member variable 'user translations' for {@link List<UserTranslation>}. */
	private List<UserTranslation> userTranslations;

	/**
	 * Constructor for User Class.
	 */
	public User() {
		this.id = GenerateUuid.getUuid();
		this.userTranslations = new ArrayList<>();
	}

	/**
	 * Constructor for User Class.
	 *
	 * @param status {@link String}
	 * @param iconId {@link String}
	 * @param userTranslations {@link List<UserTranslation>}
	 */
	public User(String status, String iconId, List<UserTranslation> userTranslations) {
		this.id = GenerateUuid.getUuid();
		this.status = status;
		this.iconName = iconId;
		this.userTranslations = userTranslations;
	}

	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * Sets the id.
	 *
	 * @param id the new id
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * Gets the user name.
	 *
	 * @return the user name
	 */
	public String getUserName() {
		return userName;
	}

	/**
	 * Sets the user name.
	 *
	 * @param userName the new user name
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}

	/**
	 * Gets the full name.
	 *
	 * @return the full name
	 */
	public String getFullName() {
		return fullName;
	}

	/**
	 * Sets the full name.
	 *
	 * @param fullName the new full name
	 */
	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	/**
	 * Gets the email.
	 *
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * Sets the email.
	 *
	 * @param email the new email
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * Gets the telephone.
	 *
	 * @return the telephone
	 */
	public String getTelephone() {
		return telephone;
	}

	/**
	 * Sets the telephone.
	 *
	 * @param telephone the new telephone
	 */
	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}

	/**
	 * Gets the department.
	 *
	 * @return the department
	 */
	public String getDepartment() {
		return department;
	}

	/**
	 * Sets the department.
	 *
	 * @param department the new department
	 */
	public void setDepartment(String department) {
		this.department = department;
	}

	/**
	 * Gets the status.
	 *
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * Sets the status.
	 *
	 * @param statusCode the new status
	 */
	public void setStatus(int statusCode) {
		if (statusCode == 1) {
			this.status = Status.ACTIVE.toString();
		} else {
			this.status = Status.INACTIVE.toString();
		}
	}

	/**
	 * Gets the icon name.
	 *
	 * @return the icon name
	 */
	public String getIconName() {
		return iconName;
	}

	/**
	 * Sets the icon name.
	 *
	 * @param iconName the new icon name
	 */
	public void setIconName(String iconName) {
		this.iconName = iconName;
	}

	/**
	 * Gets the user translations.
	 *
	 * @return the user translations
	 */
	public List<UserTranslation> getUserTranslations() {
		return userTranslations;
	}

	/**
	 * Sets the user translation.
	 *
	 * @param userTranslation the new user translation
	 */
	public void setUserTranslation(UserTranslation userTranslation) {
		this.userTranslations.add(userTranslation);
	}

	/**
	 * Sets the user translations.
	 *
	 * @param userTranslations the new user translations
	 */
	public void setUserTranslations(List<UserTranslation> userTranslations) {
		this.userTranslations = userTranslations;
	}

}
