package com.magna.caxsystem.datamigration.vo.adminarea;

import java.util.ArrayList;
import java.util.List;

/**
 * Class for Admin area.
 *
 * @author Chiranjeevi.Akula
 */
public class AdminArea {

	/** Member variable 'id' for {@link String}. */
	private String id;
	
	/** Member variable 'name' for {@link String}. */
    private String name;
    
	/** Member variable 'singleton app timeout' for {@link String}. */
	private String singletonAppTimeout;
	
	/** Member variable 'hotline contact number' for {@link String}. */
	private String hotlineContactNumber;
	
	/** Member variable 'hotline contact email' for {@link String}. */
	private String hotlineContactEmail;
	
	/** Member variable 'status' for {@link String}. */
	private String status;
	
	/** Member variable 'icon id' for {@link String}. */
	private String iconId;

	/** Member variable 'admin area translations' for {@link List<AdminAreaTranslation>}. */
	private List<AdminAreaTranslation> adminAreaTranslations;

	/**
	 * Constructor for AdminArea Class.
	 */
	public AdminArea() {
		this.adminAreaTranslations = new ArrayList<>();
	}

	/**
	 * Constructor for AdminArea Class.
	 *
	 * @param id {@link String}
	 * @param name {@link String}
	 * @param status {@link String}
	 * @param iconId {@link String}
	 * @param singletonAppTimeout {@link String}
	 * @param hotlineContactNumber {@link String}
	 * @param hotlineContactEmail {@link String}
	 * @param siteTranslations {@link List<AdminAreaTranslation>}
	 */
	public AdminArea(String id, String name, String status, String iconId, String singletonAppTimeout, String hotlineContactNumber, String hotlineContactEmail, List<AdminAreaTranslation> siteTranslations) {
		this.id = id;
		this.name = name;
		this.status = status;
		this.iconId = iconId;
		this.singletonAppTimeout = singletonAppTimeout;
		this.hotlineContactNumber = hotlineContactNumber;
		this.hotlineContactEmail = hotlineContactEmail;
		this.adminAreaTranslations = siteTranslations;
	}

	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * Sets the id.
	 *
	 * @param id the new id
	 */
	public void setId(String id) {
		this.id = id;
	}

	
	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Sets the name.
	 *
	 * @param name the new name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Gets the singleton app timeout.
	 *
	 * @return the singleton app timeout
	 */
	public String getSingletonAppTimeout() {
		return singletonAppTimeout;
	}

	/**
	 * Sets the singleton app timeout.
	 *
	 * @param singletonAppTimeout the new singleton app timeout
	 */
	public void setSingletonAppTimeout(String singletonAppTimeout) {
		this.singletonAppTimeout = singletonAppTimeout;
	}

	/**
	 * Gets the hotline contact number.
	 *
	 * @return the hotline contact number
	 */
	public String getHotlineContactNumber() {
		return hotlineContactNumber;
	}

	/**
	 * Sets the hotline contact number.
	 *
	 * @param hotlineContactNumber the new hotline contact number
	 */
	public void setHotlineContactNumber(String hotlineContactNumber) {
		this.hotlineContactNumber = hotlineContactNumber;
	}

	/**
	 * Gets the hotline contact email.
	 *
	 * @return the hotline contact email
	 */
	public String getHotlineContactEmail() {
		return hotlineContactEmail;
	}

	/**
	 * Sets the hotline contact email.
	 *
	 * @param hotlineContactEmail the new hotline contact email
	 */
	public void setHotlineContactEmail(String hotlineContactEmail) {
		this.hotlineContactEmail = hotlineContactEmail;
	}

	/**
	 * Gets the status.
	 *
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * Sets the status.
	 *
	 * @param status the new status
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * Gets the icon id.
	 *
	 * @return the icon id
	 */
	public String getIconId() {
		return iconId;
	}

	/**
	 * Sets the icon id.
	 *
	 * @param iconId the new icon id
	 */
	public void setIconId(String iconId) {
		this.iconId = iconId;
	}

	/**
	 * Gets the admin area translations.
	 *
	 * @return the admin area translations
	 */
	public List<AdminAreaTranslation> getAdminAreaTranslations() {
		return adminAreaTranslations;
	}

	/**
	 * Sets the admin area translation.
	 *
	 * @param adminAreaTranslation the new admin area translation
	 */
	public void setAdminAreaTranslation(AdminAreaTranslation adminAreaTranslation) {
		this.adminAreaTranslations.add(adminAreaTranslation);
	}

	
	/**
	 * Sets the admin area translations.
	 *
	 * @param adminAreaTranslations the new admin area translations
	 */
	public void setAdminAreaTranslations(List<AdminAreaTranslation> adminAreaTranslations) {
		this.adminAreaTranslations = adminAreaTranslations;
	}

}
