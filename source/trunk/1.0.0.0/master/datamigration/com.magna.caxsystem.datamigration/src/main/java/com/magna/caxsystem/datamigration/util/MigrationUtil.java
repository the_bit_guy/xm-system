package com.magna.caxsystem.datamigration.util;

/**
 * Class for Migration util.
 *
 * @author Chiranjeevi.Akula
 */
public final class MigrationUtil {

	/** Member variable 'this ref' for {@link MigrationUtil}. */
	private static MigrationUtil thisRef;

	/** Member variable 'excels path' for {@link String}. */
	private String excelsPath;
	
	/** Member variable 'icons path' for {@link String}. */
	private String iconsPath;

	/**
	 * Constructor for MigrationUtil Class.
	 */
	private MigrationUtil() {
		thisRef = this;
	}

	/**
	 * Gets the single instance of MigrationUtil.
	 *
	 * @return single instance of MigrationUtil
	 */
	public static MigrationUtil getInstance() {
		if (thisRef == null) {
			new MigrationUtil();
		}
		return thisRef;
	}

	/**
	 * Gets the excels path.
	 *
	 * @return the excels path
	 */
	public String getExcelsPath() {
		return excelsPath;
	}

	/**
	 * Sets the excels path.
	 *
	 * @param excelsPath the new excels path
	 */
	public void setExcelsPath(String excelsPath) {
		this.excelsPath = excelsPath;
	}

	/**
	 * Gets the icons path.
	 *
	 * @return the icons path
	 */
	public String getIconsPath() {
		return iconsPath;
	}

	/**
	 * Sets the icons path.
	 *
	 * @param iconsPath the new icons path
	 */
	public void setIconsPath(String iconsPath) {
		this.iconsPath = iconsPath;
	}
}
