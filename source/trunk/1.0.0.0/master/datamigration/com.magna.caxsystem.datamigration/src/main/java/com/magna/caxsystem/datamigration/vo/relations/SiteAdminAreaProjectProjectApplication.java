package com.magna.caxsystem.datamigration.vo.relations;

import com.magna.caxsystem.datamigration.enums.ApplicationRelationType;
import com.magna.caxsystem.datamigration.enums.Status;
import com.magna.caxsystem.datamigration.util.GenerateUuid;

/**
 * Class for Site admin area project project application.
 *
 * @author Chiranjeevi.Akula
 */
public class SiteAdminAreaProjectProjectApplication {

	/** Member variable 'id' for {@link String}. */
	private String id;

	/** Member variable 'site admin area project id' for {@link String}. */
	private String siteAdminAreaProjectId;

	/** Member variable 'project application id' for {@link String}. */
	private String projectApplicationId;

	/** Member variable 'status' for {@link String}. */
	private String status;

	/** Member variable 'rel type' for {@link String}. */
	private String relType;

	/**
	 * Constructor for SiteAdminAreaProjectProjectApplication Class.
	 */
	public SiteAdminAreaProjectProjectApplication() {
		this.id = GenerateUuid.getUuid();
	}

	/**
	 * Constructor for SiteAdminAreaProjectProjectApplication Class.
	 *
	 * @param siteAdminAreaProjectId
	 *            {@link String}
	 * @param projectApplicationId
	 *            {@link String}
	 * @param status
	 *            {@link String}
	 * @param relType
	 *            {@link String}
	 */
	public SiteAdminAreaProjectProjectApplication(String siteAdminAreaProjectId, String projectApplicationId,
			String status, String relType) {
		this.id = GenerateUuid.getUuid();
		this.siteAdminAreaProjectId = siteAdminAreaProjectId;
		this.projectApplicationId = projectApplicationId;
		this.status = status;
		this.relType = relType;
	}

	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * Sets the id.
	 *
	 * @param id
	 *            the new id
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * Gets the site admin area project id.
	 *
	 * @return the site admin area project id
	 */
	public String getSiteAdminAreaProjectId() {
		return siteAdminAreaProjectId;
	}

	/**
	 * Sets the site admin area project id.
	 *
	 * @param siteAdminAreaProjectId
	 *            the new site admin area project id
	 */
	public void setSiteAdminAreaProjectId(String siteAdminAreaProjectId) {
		this.siteAdminAreaProjectId = siteAdminAreaProjectId;
	}

	/**
	 * Gets the project application id.
	 *
	 * @return the project application id
	 */
	public String getProjectApplicationId() {
		return projectApplicationId;
	}

	/**
	 * Sets the project application id.
	 *
	 * @param projectApplicationId
	 *            the new project application id
	 */
	public void setProjectApplicationId(String projectApplicationId) {
		this.projectApplicationId = projectApplicationId;
	}

	/**
	 * Gets the status.
	 *
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * Sets the status.
	 *
	 * @param statusCode
	 *            the new status
	 */
	public void setStatus(String statusCode) {
		if ("1".equals(statusCode)) {
			this.status = Status.ACTIVE.toString();
		} else {
			this.status = Status.INACTIVE.toString();
		}
	}

	/**
	 * Gets the rel type.
	 *
	 * @return the rel type
	 */
	public String getRelType() {
		return relType;
	}

	/**
	 * Sets the rel type.
	 *
	 * @param relCode
	 *            the new rel type
	 */
	public void setRelType(String relCode) {
		if ("1".equals(relCode)) {
			this.relType = ApplicationRelationType.FIXED.toString();
		} else if ("2".equals(relCode)) {
			this.relType = ApplicationRelationType.PROTECTED.toString();
		} else {
			this.relType = ApplicationRelationType.NOTFIXED.toString();
		}
	}
}
