package com.magna.caxsystem.datamigration.util;

import java.io.File;
import java.io.FileInputStream;
import java.net.URLDecoder;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Class for Migration properties.
 *
 * @author Chiranjeevi.Akula
 */
public final class MigrationProperties extends Properties {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The Constant LOGGER. */
	private static final Logger LOGGER = LoggerFactory.getLogger(MigrationProperties.class);
	
	/** Member variable 'this ref' for {@link MigrationProperties}. */
	private static MigrationProperties thisRef;

	/**
	 * Constructor for MigrationProperties Class.
	 */
	private MigrationProperties() {
		super();
		try {			
			final String path = MigrationProperties.class.getProtectionDomain().getCodeSource().getLocation().getPath();
			final String decodedPath = URLDecoder.decode(path, "UTF-8");
			if (decodedPath != null && !decodedPath.isEmpty()) {
				final File currentFile = new File(decodedPath);
				if (currentFile != null && currentFile.exists()) {
					final String propertyFile = currentFile.getParent() + File.separator + MigrationConstents.MIGRATION_CONFIG;
					this.load(new FileInputStream(propertyFile));
					thisRef = this;
				} else {
					LOGGER.error("Data migration failed. Unable to find the migration property configuration file"); //$NON-NLS-1$
				}
			}
		} catch (Exception ex) {
			LOGGER.error("Execption ocuured in reading the migration property configuration file", ex); //$NON-NLS-1$
		}
	}

	/**
	 * Gets the single instance of MigrationProperties.
	 *
	 * @return single instance of MigrationProperties
	 */
	public static MigrationProperties getInstance() {
		if (thisRef == null) {
			new MigrationProperties();
		}
		return thisRef;
	}
}
