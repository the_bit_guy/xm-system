package com.magna.caxsystem.datamigration.util;

import java.util.List;

import com.magna.caxsystem.datamigration.vo.adminarea.AdminArea;
import com.magna.caxsystem.datamigration.vo.directory.Directory;
import com.magna.caxsystem.datamigration.vo.project.Project;
import com.magna.caxsystem.datamigration.vo.projectapplication.ProjectApp;
import com.magna.caxsystem.datamigration.vo.site.Site;
import com.magna.caxsystem.datamigration.vo.startapplication.StartApp;
import com.magna.caxsystem.datamigration.vo.user.User;
import com.magna.caxsystem.datamigration.vo.userapplication.UserApp;

public class ObjectNameIdDetails {

	public static String getAAIdByName(final List<AdminArea> adminAreas, final String name) {
		if(name != null && adminAreas !=null ){
			for(AdminArea adminArea : adminAreas) {
				if(name.equals(adminArea.getName())) {
					return adminArea.getId();
				}
			}
		}
		
		return null;
	}
	
	public static String getProjectIdByName(final List<Project> projects, final String name) {
		if(name != null && projects !=null ){
			for(Project project : projects) {
				if(name.equals(project.getName())) {
					return project.getId();
				}
			}
		}
		
		return null;
	}
	
	public static String getUserAppIdByName(final List<UserApp> userApps, final String name) {
		if(name != null && userApps !=null ){
			for(UserApp userApp : userApps) {
				if(name.equals(userApp.getName())) {
					return userApp.getId();
				}
			}
		}
		
		return null;
	}
	
	public static String getProjectAppIdByName(final List<ProjectApp> projectApps, final String name) {
		if(name != null && projectApps !=null ){
			for(ProjectApp projectApp : projectApps) {
				if(name.equals(projectApp.getName())) {
					return projectApp.getId();
				}
			}
		}
		
		return null;
	}
	
	public static String getStartAppIdByName(final List<StartApp> startApps, final String name) {
		if(name != null && startApps !=null ){
			for(StartApp startApp : startApps) {
				if(name.equals(startApp.getName())) {
					return startApp.getId();
				}
			}
		}
		
		return null;
	}
	
	public static String getUserIdByName(final List<User> users, final String name) {
		if(name != null && users !=null ){
			for(User user : users) {
				if(name.equals(user.getUserName())) {
					return user.getId();
				}
			}
		}
		
		return null;
	}
	
	public static String getDirectoryIdByName(final List<Directory> directories, final String name) {
		if(name != null && directories !=null ){
			for(Directory directory : directories) {
				if(name.equals(directory.getName())) {
					return directory.getId();
				}
			}
		}
		
		return null;
	}
	
	public static String getSiteNameById(final List<Site> sites, final String id) {
		if(id != null && sites !=null ){
			for(Site site : sites) {
				if(id.equals(site.getId())) {
					return site.getName();
				}
			}
		}
		
		return null;
	}
	
	public static String getAANameById(final List<AdminArea> adminAreas, final String id) {
		if(id != null && adminAreas !=null ){
			for(AdminArea adminArea : adminAreas) {
				if(id.equals(adminArea.getId())) {
					return adminArea.getName();
				}
			}
		}
		
		return null;
	}
	
	public static String getProjectNameById(final List<Project> projects, final String id) {
		if(id != null && projects !=null ){
			for(Project project : projects) {
				if(id.equals(project.getId())) {
					return project.getName();
				}
			}
		}
		
		return null;
	}
}
