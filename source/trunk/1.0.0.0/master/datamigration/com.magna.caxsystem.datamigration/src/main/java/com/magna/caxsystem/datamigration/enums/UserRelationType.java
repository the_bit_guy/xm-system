package com.magna.caxsystem.datamigration.enums;

/**
 * Enum for User relation type.
 *
 * @author Chiranjeevi.Akula
 */
public enum UserRelationType {
    ALLOWED, NOTALLOWED
}
