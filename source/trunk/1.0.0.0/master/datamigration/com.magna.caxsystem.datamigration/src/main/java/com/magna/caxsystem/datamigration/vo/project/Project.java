package com.magna.caxsystem.datamigration.vo.project;

import java.util.ArrayList;
import java.util.List;

import com.magna.caxsystem.datamigration.enums.Status;
import com.magna.caxsystem.datamigration.util.GenerateUuid;

/**
 * Class for Project.
 *
 * @author Chiranjeevi.Akula
 */
public class Project {

	/** Member variable 'id' for {@link String}. */
	private String id;

	/** Member variable 'name' for {@link String}. */
	private String name;

	/** Member variable 'status' for {@link String}. */
	private String status;

	/** Member variable 'icon name' for {@link String}. */
	private String iconName;

	/** Member variable 'project translations' for {@link List<ProjectTranslation>}. */
	private List<ProjectTranslation> projectTranslations;

	/**
	 * Constructor for Project Class.
	 */
	public Project() {
		this.id = GenerateUuid.getUuid();
		this.projectTranslations = new ArrayList<>();
	}

	/**
	 * Constructor for Project Class.
	 *
	 * @param name {@link String}
	 * @param status {@link String}
	 * @param iconId {@link String}
	 * @param projectTranslations {@link List<ProjectTranslation>}
	 */
	public Project(String name, String status, String iconId, List<ProjectTranslation> projectTranslations) {
		this.id = GenerateUuid.getUuid();
		this.name = name;
		this.status = status;
		this.iconName = iconId;
		this.projectTranslations = projectTranslations;
	}

	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * Sets the id.
	 *
	 * @param id the new id
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Sets the name.
	 *
	 * @param name the new name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Gets the status.
	 *
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * Sets the status.
	 *
	 * @param statusCode the new status
	 */
	public void setStatus(int statusCode) {
		if (statusCode == 1) {
			this.status = Status.ACTIVE.toString();
		} else {
			this.status = Status.INACTIVE.toString();
		}
	}

	/**
	 * Gets the icon name.
	 *
	 * @return the icon name
	 */
	public String getIconName() {
		return iconName;
	}

	/**
	 * Sets the icon name.
	 *
	 * @param iconName the new icon name
	 */
	public void setIconName(String iconName) {
		this.iconName = iconName;
	}

	/**
	 * Gets the project translations.
	 *
	 * @return the project translations
	 */
	public List<ProjectTranslation> getProjectTranslations() {
		return projectTranslations;
	}

	/**
	 * Sets the project translation.
	 *
	 * @param projectTranslation the new project translation
	 */
	public void setProjectTranslation(ProjectTranslation projectTranslation) {
		this.projectTranslations.add(projectTranslation);
	}

	/**
	 * Sets the project translations.
	 *
	 * @param projectTranslations the new project translations
	 */
	public void setProjectTranslations(List<ProjectTranslation> projectTranslations) {
		this.projectTranslations = projectTranslations;
	}

}
