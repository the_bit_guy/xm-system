package com.magna.caxsystem.datamigration.vo.startapplication;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import com.magna.caxsystem.datamigration.enums.Status;
import com.magna.caxsystem.datamigration.util.GenerateUuid;

/**
 * Class for Start app.
 *
 * @author Chiranjeevi.Akula
 */
public class StartApp {

	/** Member variable 'id' for {@link String}. */
	private String id;

	/** Member variable 'name' for {@link String}. */
	private String name;

	/** Member variable 'message' for {@link Boolean}. */
	private boolean message;

	/** Member variable 'message expity date' for {@link Date}. */
	private Date messageExpityDate;

	/** Member variable 'status' for {@link String}. */
	private String status;

	/** Member variable 'icon name' for {@link String}. */
	private String iconName;

	/** Member variable 'base app name' for {@link String}. */
	private String baseAppName;

	/**
	 * Member variable 'start app translations' for
	 * {@link List<StartAppTranslation>}.
	 */
	private List<StartAppTranslation> startAppTranslations;

	/**
	 * Constructor for StartApp Class.
	 */
	public StartApp() {
		this.id = GenerateUuid.getUuid();
		this.startAppTranslations = new ArrayList<>();
	}

	/**
	 * Constructor for StartApp Class.
	 *
	 * @param name
	 *            {@link String}
	 * @param status
	 *            {@link String}
	 * @param iconId
	 *            {@link String}
	 * @param startAppTranslations
	 *            {@link List<StartAppTranslation>}
	 */
	public StartApp(String name, String status, String iconId, List<StartAppTranslation> startAppTranslations) {
		this.id = GenerateUuid.getUuid();
		this.name = name;
		this.status = status;
		this.iconName = iconId;
		this.startAppTranslations = startAppTranslations;
	}

	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * Sets the id.
	 *
	 * @param id
	 *            the new id
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Sets the name.
	 *
	 * @param name
	 *            the new name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Gets the status.
	 *
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * Sets the status.
	 *
	 * @param statusCode
	 *            the new status
	 */
	public void setStatus(int statusCode) {
		if (statusCode == 1) {
			this.status = Status.ACTIVE.toString();
		} else {
			this.status = Status.INACTIVE.toString();
		}
	}

	/**
	 * Gets the icon name.
	 *
	 * @return the icon name
	 */
	public String getIconName() {
		return iconName;
	}

	/**
	 * Sets the icon name.
	 *
	 * @param iconName
	 *            the new icon name
	 */
	public void setIconName(String iconName) {
		this.iconName = iconName;
	}

	/**
	 * Gets the base app name.
	 *
	 * @return the base app name
	 */
	public String getBaseAppName() {
		return baseAppName;
	}

	/**
	 * Sets the base app name.
	 *
	 * @param baseAppName
	 *            the new base app name
	 */
	public void setBaseAppName(String baseAppName) {
		this.baseAppName = baseAppName;
	}

	/**
	 * Checks if is message.
	 *
	 * @return true, if is message
	 */
	public boolean isMessage() {
		return message;
	}

	/**
	 * Sets the message.
	 *
	 * @param message
	 *            the new message
	 */
	public void setMessage(boolean message) {
		this.message = message;
	}

	/**
	 * Gets the message expity date.
	 *
	 * @return the message expity date
	 */
	public Date getMessageExpityDate() {
		return messageExpityDate;
	}

	/**
	 * Sets the message expity date.
	 *
	 * @param messageExpityDate
	 *            the new message expity date
	 */
	public void setMessageExpityDate(Date messageExpityDate) {
		this.messageExpityDate = messageExpityDate;
	}

	/**
	 * Gets the start app translations.
	 *
	 * @return the start app translations
	 */
	public List<StartAppTranslation> getStartAppTranslations() {
		return startAppTranslations;
	}

	/**
	 * Sets the start app translation.
	 *
	 * @param startAppTranslation
	 *            the new start app translation
	 */
	public void setStartAppTranslation(StartAppTranslation startAppTranslation) {
		this.startAppTranslations.add(startAppTranslation);
	}

	/**
	 * Sets the start app translations.
	 *
	 * @param startAppTranslations
	 *            the new start app translations
	 */
	public void setStartAppTranslations(List<StartAppTranslation> startAppTranslations) {
		this.startAppTranslations = startAppTranslations;
	}

}
