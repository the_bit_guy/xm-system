--------------------------------------------------------
--  File created - Friday-July-28-2017   
--------------------------------------------------------
REM INSERTING into ICONS_TBL
SET DEFINE OFF;
Insert into ICONS_TBL (ICON_ID,ICON_NAME,ICON_TYPE) values ('db4b378c-ac2e-4e82-b62f-6cc41ecf26c6','sites.png','DEFAULT');
Insert into ICONS_TBL (ICON_ID,ICON_NAME,ICON_TYPE) values ('897be1a4-16d4-4806-ae3b-56b0280069dc','administrationarea.png','DEFAULT');
Insert into ICONS_TBL (ICON_ID,ICON_NAME,ICON_TYPE) values ('6e994d92-2efe-4328-9d96-f04e1fa24dd2','projects.png','DEFAULT');
Insert into ICONS_TBL (ICON_ID,ICON_NAME,ICON_TYPE) values ('d3de285b-afa3-4cbd-a4ad-26369c03e087','aprojecttasks.png','DEFAULT');
Insert into ICONS_TBL (ICON_ID,ICON_NAME,ICON_TYPE) values ('797be1a4-16d4-4806-ae3b-56b0280069dc','startapplications.png','DEFAULT');
Insert into ICONS_TBL (ICON_ID,ICON_NAME,ICON_TYPE) values ('797be1a4-16d4-4806-ae3b-56b0290069dc','users.png','DEFAULT');
Insert into ICONS_TBL (ICON_ID,ICON_NAME,ICON_TYPE) values ('84e8bd76-679e-40ed-b0e2-d0dd2d36b089','basictasks.png','DEFAULT');
Insert into ICONS_TBL (ICON_ID,ICON_NAME,ICON_TYPE) values ('7bb8f88b-1f54-4ab2-ab22-1fa04595c2d6','directory.png','DEFAULT');
Insert into ICONS_TBL (ICON_ID,ICON_NAME,ICON_TYPE) values ('7bb8f88b-1f54-4ab2-ab33-1fa04595c2d6','ausertasks.png','DEFAULT');
Insert into ICONS_TBL (ICON_ID,ICON_NAME,ICON_TYPE) values ('3fac5911-ee76-4d2f-89a2-ce6531a1ab35','superadmin.png','DEFAULT');
Insert into ICONS_TBL (ICON_ID,ICON_NAME,ICON_TYPE) values ('85b8f88b-1f54-4ab2-ab22-1fa04595c2f9','roles.png','DEFAULT');
Insert into ICONS_TBL (ICON_ID,ICON_NAME,ICON_TYPE) values ('b191g793-6648-46a4-a07d-562cdh390d82','user_group.png','DEFAULT');
Insert into ICONS_TBL (ICON_ID,ICON_NAME,ICON_TYPE) values ('F747416C-4ED0-4E5C-A3A0-19A6660BE30C','project_group.png','DEFAULT');
Insert into ICONS_TBL (ICON_ID,ICON_NAME,ICON_TYPE) values ('D68198BC-1ACB-4124-AC2E-98D6B44B8CD8','userapp_grp.png','DEFAULT');
Insert into ICONS_TBL (ICON_ID,ICON_NAME,ICON_TYPE) values ('65FCEE2F-EA76-458C-BF4F-5D8ACC799A44','projectapp_grp.png','DEFAULT');
Insert into ICONS_TBL (ICON_ID,ICON_NAME,ICON_TYPE) values ('732AEEB6-9BE4-4A47-BC7F-F5BAE0A475F9','livemessagesconfig.png','DEFAULT');
COMMIT;