CREATE TABLE transactions (
         id      NUMBER(11) PRIMARY KEY,
         name      VARCHAR2(32) NOT NULL,
         active        NUMBER(11),
         description        VARCHAR2(254),
         helptext        VARCHAR2(4000),
         site   VARCHAR2(64),
         project VARCHAR2(64),
         username VARCHAR2(64),
         address VARCHAR2(254),
         status NUMBER(11),
         taskmsg VARCHAR2(254) );


CREATE TABLE tasklist (
         id  NUMBER(11) PRIMARY KEY,
         trans_id   NUMBER(11),
         name      VARCHAR2(32),
         status NUMBER(11),
         task VARCHAR2(32),
         taskorder NUMBER(11) );


CREATE TABLE eventtasks (
         name      VARCHAR2(32) PRIMARY KEY,
         name_lang      VARCHAR2(32),
         active NUMBER(11),
         description VARCHAR2(254),
         description_lang VARCHAR2(254),
         iconp VARCHAR2(64),
         helptext VARCHAR2(4000),
         task VARCHAR2(32) );

CREATE TABLE translation (
         id      NUMBER(11) PRIMARY KEY,
         projecttasks VARCHAR2(32),
         inittasks VARCHAR2(32),
         eventtasks VARCHAR2(32),
         language VARCHAR2(32),
         name VARCHAR2(64),
         description VARCHAR2(508)
          );


CREATE TABLE projecttasks (
         name VARCHAR2(32) PRIMARY KEY,
         name_lang VARCHAR2(32),
         active NUMBER(11),
         description VARCHAR2(254),
         description_lang VARCHAR2(254), 
         iconp VARCHAR2(64),
         helptext VARCHAR2(4000),
         usertask NUMBER(11),
         parentname VARCHAR2(32),
         task VARCHAR(32)        
          );

CREATE TABLE projects (
         name VARCHAR2(32) PRIMARY KEY,
         active NUMBER(11),
         description VARCHAR2(254),
         iconp VARCHAR2(64),
         helptext VARCHAR2(4000)                 
          );


CREATE TABLE site_rel (
         id      NUMBER(11) PRIMARY KEY,
         site VARCHAR2(32),
         project VARCHAR2(32),
         p_active NUMBER(11),
         inittask VARCHAR2(32),
         it_active NUMBER(11),
         projecttask VARCHAR2(32) ,
         pt_active NUMBER(11),
         pt_fixed NUMBER(11)
          );



CREATE TABLE usersettings (
         actsite VARCHAR2(32) PRIMARY KEY,
         actproject VARCHAR2(32),
         actuser VARCHAR2(32),
         settings VARCHAR2(3000)        
          );


CREATE TABLE sites (
         name VARCHAR2(32) PRIMARY KEY,
         active NUMBER(11),
         description VARCHAR2(254),
         iconp VARCHAR2(64),
         helptext VARCHAR2(4000) 
          );


CREATE TABLE users (
         name VARCHAR2(32) PRIMARY KEY,
         active NUMBER(11),
         description VARCHAR2(254),
         iconp VARCHAR2(64),
         helptext VARCHAR2(4000) 
          );


CREATE TABLE user_rel (
         id      NUMBER(11) PRIMARY KEY,
         username VARCHAR2(32),
         inittask VARCHAR2(32),
         it_active NUMBER(11),
         project VARCHAR2(32),
         p_active NUMBER(11),
         projecttask VARCHAR2(32),
         pt_denied NUMBER(11)
          );

CREATE TABLE inittasks (
         name VARCHAR2(32) PRIMARY KEY,
         name_lang VARCHAR2(32),
         active NUMBER(11),
         description VARCHAR2(254),
         description_lang VARCHAR2(254),
         iconp VARCHAR2(64),
         helptext VARCHAR2(4000),
         task VARCHAR2(32)
          );

CREATE TABLE groupref (
         groupname VARCHAR2(32),
         objecttype VARCHAR2(32),
         objectid VARCHAR2(32)         
          );


CREATE TABLE tasks (
         name VARCHAR2(32) PRIMARY KEY,
         active NUMBER(11),
         description VARCHAR2(254),
         iconp VARCHAR2(64),
         helptext VARCHAR2(4000),
         program VARCHAR2(254) 
          );


CREATE TABLE groups (
         name VARCHAR2(32) PRIMARY KEY,
         active NUMBER(11),
         description VARCHAR2(254),
         iconp VARCHAR2(64),
         helptext VARCHAR2(4000)        
          );

CREATE TABLE adminhistory (
         id      NUMBER(11) PRIMARY KEY,
         admtime DATE,
         admname   VARCHAR2(32),
         admclass   VARCHAR2(254),
         param1 VARCHAR2(4000),
         param2 VARCHAR2(4000),
         param3 VARCHAR2(4000),
         param4 VARCHAR2(4000),
         param5 VARCHAR2(4000),
         param6 VARCHAR2(4000),
         param7 VARCHAR2(4000),
         param8 VARCHAR2(4000),
         errormessage VARCHAR2(4000),
         result NUMBER(11)
          );




CREATE TABLE events (
         id      NUMBER(11) PRIMARY KEY,
         name   VARCHAR2(32),
         description   VARCHAR2(254),
         helptext   VARCHAR2(4000),
         infodate DATE,
         address   VARCHAR2(254),
         site   VARCHAR2(32),
         project   VARCHAR2(32),
         username   VARCHAR2(32),
         prjusrtask   VARCHAR2(32),
         task   VARCHAR2(32)    
          );



CREATE TABLE user_status (
         id      NUMBER(11) PRIMARY KEY,
         usetime DATE,
         site   VARCHAR2(32),
         project   VARCHAR2(32),
         username   VARCHAR2(32),
         host   VARCHAR2(32),
         task   VARCHAR2(32),
         pid NUMBER(11),
         result   VARCHAR2(32),                
         args   VARCHAR2(254)  
          );


CREATE TABLE eventhistory (
         id      NUMBER(11) PRIMARY KEY,
         name   VARCHAR2(32),
         description   VARCHAR2(254),
         helptext   VARCHAR2(4000),
         infodate DATE,
         address   VARCHAR2(254),
         site   VARCHAR2(32),
         project   VARCHAR2(32),
         username   VARCHAR2(32),
         prjusrtask   VARCHAR2(32),
         task   VARCHAR2(32)    
          );


CREATE TABLE transacthistory (
         id      NUMBER(11) PRIMARY KEY,
         usetime DATE,
         task_id NUMBER(11),
         task_name   VARCHAR2(32),
         task_status NUMBER(11),
         task   VARCHAR2(32),
         taskorder NUMBER(11),
         trans_id NUMBER(11),
         trans_name   VARCHAR2(32),
         site   VARCHAR2(64),
         project   VARCHAR2(64),
         username   VARCHAR2(64),
         address   VARCHAR2(254),
         trans_status NUMBER(11),         
         message   VARCHAR2(254)
           
          );


