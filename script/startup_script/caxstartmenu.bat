@echo off

net use /delete %CAD_DRIVE_LETTER% /Y > NUL
if "X%USERDOMAIN%"=="XMAGNA" (
   net use %CAD_DRIVE_LETTER% \\%CAD_DRIVE_PATH%\%CAD_GLOBAL_LOCATION%
) else (
   net use %CAD_DRIVE_LETTER% \\%CAD_DRIVE_PATH%\%CAD_LOCATION%
)

call "\\magna.global\all\cad\grz\adm\env\das\GL2.1_cad_basic_var.bat"

SET TCLLIBPATH=C:/cax/sys/XMenu200x/V2.4/Tcl/lib %CAD_DRIVE_LETTER%/adm/app/common/scripts/3_lib
SET PATH=%PATH%;C:\cax\sys\XMenu200x\V2.4\Perl\bin;C:\cax\sys\XMenu200x\V2.4\Tcl\bin

set CAD_DRIVE_LETTER=G:
set XM_PATH=%~dp0..
set XM_CONFIG=%XM_PATH%/xmsystem.properties
set XM_SCRIPTS=%CAD_DRIVE_LETTER%/adm/app/common/scripts
set XM_ICONS=G:/temp/miliker/xmsystem/icons
set XM_SITE=%XM_SITENAME%
set XM_USER=%USERNAME%
set CAD_SCRIPT_DIR=%XM_SCRIPTS%
set SYSTEM_PATH=%PATH%
REM cd %TEMP%
start %XM_PATH%\caxstartmenu\build\caxstartmenu.exe

